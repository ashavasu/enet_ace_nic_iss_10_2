/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssnpdb.h,v 1.26 2017/09/26 13:52:10 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSNPDB_H
#define _FSSNPDB_H

UINT1 FsSnoopInstanceGlobalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsSnoopInstanceConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsSnoopVlanMcastMacFwdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsSnoopVlanMcastIpFwdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsSnoopVlanRouterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsSnoopVlanFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsSnoopVlanMcastGroupTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsSnoopVlanMcastReceiverTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsSnoopVlanIpFwdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsSnoopVlanFilterXTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsSnoopVlanStaticMcastGrpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsSnoopStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsSnoopPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsSnoopEnhPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsSnoopRtrPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsSnoopTrapObjectsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fssnp [] ={1,3,6,1,4,1,2076,105};
tSNMP_OID_TYPE fssnpOID = {8, fssnp};


/* Generated OID's for tables */
UINT4 FsSnoopInstanceGlobalTable [] ={1,3,6,1,4,1,2076,105,2,1};
tSNMP_OID_TYPE FsSnoopInstanceGlobalTableOID = {10, FsSnoopInstanceGlobalTable};


UINT4 FsSnoopInstanceConfigTable [] ={1,3,6,1,4,1,2076,105,2,2};
tSNMP_OID_TYPE FsSnoopInstanceConfigTableOID = {10, FsSnoopInstanceConfigTable};


UINT4 FsSnoopVlanMcastMacFwdTable [] ={1,3,6,1,4,1,2076,105,3,1};
tSNMP_OID_TYPE FsSnoopVlanMcastMacFwdTableOID = {10, FsSnoopVlanMcastMacFwdTable};


UINT4 FsSnoopVlanMcastIpFwdTable [] ={1,3,6,1,4,1,2076,105,3,2};
tSNMP_OID_TYPE FsSnoopVlanMcastIpFwdTableOID = {10, FsSnoopVlanMcastIpFwdTable};


UINT4 FsSnoopVlanRouterTable [] ={1,3,6,1,4,1,2076,105,3,3};
tSNMP_OID_TYPE FsSnoopVlanRouterTableOID = {10, FsSnoopVlanRouterTable};


UINT4 FsSnoopVlanFilterTable [] ={1,3,6,1,4,1,2076,105,3,4};
tSNMP_OID_TYPE FsSnoopVlanFilterTableOID = {10, FsSnoopVlanFilterTable};


UINT4 FsSnoopVlanMcastGroupTable [] ={1,3,6,1,4,1,2076,105,3,5};
tSNMP_OID_TYPE FsSnoopVlanMcastGroupTableOID = {10, FsSnoopVlanMcastGroupTable};


UINT4 FsSnoopVlanMcastReceiverTable [] ={1,3,6,1,4,1,2076,105,3,6};
tSNMP_OID_TYPE FsSnoopVlanMcastReceiverTableOID = {10, FsSnoopVlanMcastReceiverTable};


UINT4 FsSnoopVlanIpFwdTable [] ={1,3,6,1,4,1,2076,105,3,7};
tSNMP_OID_TYPE FsSnoopVlanIpFwdTableOID = {10, FsSnoopVlanIpFwdTable};


UINT4 FsSnoopVlanFilterXTable [] ={1,3,6,1,4,1,2076,105,3,8};
tSNMP_OID_TYPE FsSnoopVlanFilterXTableOID = {10, FsSnoopVlanFilterXTable};


UINT4 FsSnoopVlanStaticMcastGrpTable [] ={1,3,6,1,4,1,2076,105,3,9};
tSNMP_OID_TYPE FsSnoopVlanStaticMcastGrpTableOID = {10, FsSnoopVlanStaticMcastGrpTable};


UINT4 FsSnoopStatsTable [] ={1,3,6,1,4,1,2076,105,4,1};
tSNMP_OID_TYPE FsSnoopStatsTableOID = {10, FsSnoopStatsTable};


UINT4 FsSnoopPortTable [] ={1,3,6,1,4,1,2076,105,5,1};
tSNMP_OID_TYPE FsSnoopPortTableOID = {10, FsSnoopPortTable};


UINT4 FsSnoopEnhPortTable [] ={1,3,6,1,4,1,2076,105,5,2};
tSNMP_OID_TYPE FsSnoopEnhPortTableOID = {10, FsSnoopEnhPortTable};


UINT4 FsSnoopRtrPortTable [] ={1,3,6,1,4,1,2076,105,5,3};
tSNMP_OID_TYPE FsSnoopRtrPortTableOID = {10, FsSnoopRtrPortTable};


UINT4 FsXSnoopRtrPortTable [] ={1,3,6,1,4,1,2076,105,5,4};
tSNMP_OID_TYPE FsXSnoopRtrPortTableOID = {10, FsXSnoopRtrPortTable};


UINT4 FsSnoopTrapObjectsTable [] ={1,3,6,1,4,1,2076,105,6,5};
tSNMP_OID_TYPE FsSnoopTrapObjectsTableOID = {10, FsSnoopTrapObjectsTable};




UINT4 FsSnoopInstanceGlobalInstId [ ] ={1,3,6,1,4,1,2076,105,2,1,1,1};
UINT4 FsSnoopInstanceGlobalMcastFwdMode [ ] ={1,3,6,1,4,1,2076,105,2,1,1,2};
UINT4 FsSnoopInstanceGlobalSystemControl [ ] ={1,3,6,1,4,1,2076,105,2,1,1,3};
UINT4 FsSnoopInstanceGlobalLeaveConfigLevel [ ] ={1,3,6,1,4,1,2076,105,2,1,1,4};
UINT4 FsSnoopInstanceGlobalEnhancedMode [ ] ={1,3,6,1,4,1,2076,105,2,1,1,5};
UINT4 FsSnoopInstanceGlobalReportProcessConfigLevel [ ] ={1,3,6,1,4,1,2076,105,2,1,1,6};
UINT4 FsSnoopInstanceGlobalSparseMode [ ] ={1,3,6,1,4,1,2076,105,2,1,1,7};
UINT4 FsSnoopInstanceControlPlaneDriven [ ] ={1,3,6,1,4,1,2076,105,2,1,1,8};
UINT4 FsSnoopInstanceConfigInstId [ ] ={1,3,6,1,4,1,2076,105,2,2,1,1};
UINT4 FsSnoopInetAddressType [ ] ={1,3,6,1,4,1,2076,105,2,2,1,2};
UINT4 FsSnoopStatus [ ] ={1,3,6,1,4,1,2076,105,2,2,1,3};
UINT4 FsSnoopProxyReportingStatus [ ] ={1,3,6,1,4,1,2076,105,2,2,1,4};
UINT4 FsSnoopRouterPortPurgeInterval [ ] ={1,3,6,1,4,1,2076,105,2,2,1,5};
UINT4 FsSnoopPortPurgeInterval [ ] ={1,3,6,1,4,1,2076,105,2,2,1,6};
UINT4 FsSnoopReportForwardInterval [ ] ={1,3,6,1,4,1,2076,105,2,2,1,7};
UINT4 FsSnoopRetryCount [ ] ={1,3,6,1,4,1,2076,105,2,2,1,8};
UINT4 FsSnoopGrpQueryInterval [ ] ={1,3,6,1,4,1,2076,105,2,2,1,9};
UINT4 FsSnoopReportFwdOnAllPorts [ ] ={1,3,6,1,4,1,2076,105,2,2,1,10};
UINT4 FsSnoopTraceOption [ ] ={1,3,6,1,4,1,2076,105,2,2,1,11};
UINT4 FsSnoopOperStatus [ ] ={1,3,6,1,4,1,2076,105,2,2,1,12};
UINT4 FsSnoopSendQueryOnTopoChange [ ] ={1,3,6,1,4,1,2076,105,2,2,1,13};
UINT4 FsSnoopSendLeaveOnTopoChange [ ] ={1,3,6,1,4,1,2076,105,2,2,1,14};
UINT4 FsSnoopFilterStatus [ ] ={1,3,6,1,4,1,2076,105,2,2,1,15};
UINT4 FsSnoopMulticastVlanStatus [ ] ={1,3,6,1,4,1,2076,105,2,2,1,16};
UINT4 FsSnoopProxyStatus [ ] ={1,3,6,1,4,1,2076,105,2,2,1,17};
UINT4 FsSnoopQueryFwdOnAllPorts [ ] ={1,3,6,1,4,1,2076,105,2,2,1,18};
UINT4 FsSnoopFwdGroupsCnt [ ] ={1,3,6,1,4,1,2076,105,2,2,1,19};
UINT4 FsSnoopDebugOption [ ] ={1,3,6,1,4,1,2076,105,2,2,1,20};
UINT4 FsSnoopVlanMcastMacFwdInstId [ ] ={1,3,6,1,4,1,2076,105,3,1,1,1};
UINT4 FsSnoopVlanMcastMacFwdVlanId [ ] ={1,3,6,1,4,1,2076,105,3,1,1,2};
UINT4 FsSnoopVlanMcastMacFwdInetAddressType [ ] ={1,3,6,1,4,1,2076,105,3,1,1,3};
UINT4 FsSnoopVlanMcastMacFwdGroupAddress [ ] ={1,3,6,1,4,1,2076,105,3,1,1,4};
UINT4 FsSnoopVlanMcastMacFwdPortList [ ] ={1,3,6,1,4,1,2076,105,3,1,1,5};
UINT4 FsSnoopVlanMcastMacFwdLocalPortList [ ] ={1,3,6,1,4,1,2076,105,3,1,1,6};
UINT4 FsSnoopVlanMcastMacFwdEntryFlag [ ] ={1,3,6,1,4,1,2076,105,3,1,1,7};
UINT4 FsSnoopVlanMcastIpFwdInstId [ ] ={1,3,6,1,4,1,2076,105,3,2,1,1};
UINT4 FsSnoopVlanMcastIpFwdVlanId [ ] ={1,3,6,1,4,1,2076,105,3,2,1,2};
UINT4 FsSnoopVlanMcastIpFwdAddressType [ ] ={1,3,6,1,4,1,2076,105,3,2,1,3};
UINT4 FsSnoopVlanMcastIpFwdSourceAddress [ ] ={1,3,6,1,4,1,2076,105,3,2,1,4};
UINT4 FsSnoopVlanMcastIpFwdGroupAddress [ ] ={1,3,6,1,4,1,2076,105,3,2,1,5};
UINT4 FsSnoopVlanMcastIpFwdPortList [ ] ={1,3,6,1,4,1,2076,105,3,2,1,6};
UINT4 FsSnoopVlanMcastIpFwdEntryFlag [ ] ={1,3,6,1,4,1,2076,105,3,2,1,7};
UINT4 FsSnoopVlanRouterInstId [ ] ={1,3,6,1,4,1,2076,105,3,3,1,1};
UINT4 FsSnoopVlanRouterVlanId [ ] ={1,3,6,1,4,1,2076,105,3,3,1,2};
UINT4 FsSnoopVlanRouterInetAddressType [ ] ={1,3,6,1,4,1,2076,105,3,3,1,3};
UINT4 FsSnoopVlanRouterPortList [ ] ={1,3,6,1,4,1,2076,105,3,3,1,4};
UINT4 FsSnoopVlanRouterLocalPortList [ ] ={1,3,6,1,4,1,2076,105,3,3,1,5};
UINT4 FsSnoopVlanFilterInstId [ ] ={1,3,6,1,4,1,2076,105,3,4,1,1};
UINT4 FsSnoopVlanFilterVlanId [ ] ={1,3,6,1,4,1,2076,105,3,4,1,2};
UINT4 FsSnoopVlanFilterInetAddressType [ ] ={1,3,6,1,4,1,2076,105,3,4,1,3};
UINT4 FsSnoopVlanSnoopStatus [ ] ={1,3,6,1,4,1,2076,105,3,4,1,4};
UINT4 FsSnoopVlanOperatingVersion [ ] ={1,3,6,1,4,1,2076,105,3,4,1,5};
UINT4 FsSnoopVlanCfgOperVersion [ ] ={1,3,6,1,4,1,2076,105,3,4,1,6};
UINT4 FsSnoopVlanFastLeave [ ] ={1,3,6,1,4,1,2076,105,3,4,1,7};
UINT4 FsSnoopVlanQuerier [ ] ={1,3,6,1,4,1,2076,105,3,4,1,8};
UINT4 FsSnoopVlanCfgQuerier [ ] ={1,3,6,1,4,1,2076,105,3,4,1,9};
UINT4 FsSnoopVlanQueryInterval [ ] ={1,3,6,1,4,1,2076,105,3,4,1,10};
UINT4 FsSnoopVlanRtrPortList [ ] ={1,3,6,1,4,1,2076,105,3,4,1,11};
UINT4 FsSnoopVlanRowStatus [ ] ={1,3,6,1,4,1,2076,105,3,4,1,12};
UINT4 FsSnoopVlanStartupQueryCount [ ] ={1,3,6,1,4,1,2076,105,3,4,1,13};
UINT4 FsSnoopVlanStartupQueryInterval [ ] ={1,3,6,1,4,1,2076,105,3,4,1,14};
UINT4 FsSnoopVlanOtherQuerierPresentInterval [ ] ={1,3,6,1,4,1,2076,105,3,4,1,15};
UINT4 FsSnoopVlanQuerierIpAddress [ ] ={1,3,6,1,4,1,2076,105,3,4,1,16};
UINT4 FsSnoopVlanQuerierIpFlag [ ] ={1,3,6,1,4,1,2076,105,3,4,1,17};
UINT4 FsSnoopVlanElectedQuerier [ ] ={1,3,6,1,4,1,2076,105,3,4,1,18};
UINT4 FsSnoopVlanRobustnessValue [ ] ={1,3,6,1,4,1,2076,105,3,4,1,19};
UINT4 FsSnoopVlanMcastGroupInstanceId [ ] ={1,3,6,1,4,1,2076,105,3,5,1,1};
UINT4 FsSnoopVlanMcastGroupOuterVlanId [ ] ={1,3,6,1,4,1,2076,105,3,5,1,2};
UINT4 FsSnoopVlanMcastGroupInetAddressType [ ] ={1,3,6,1,4,1,2076,105,3,5,1,3};
UINT4 FsSnoopVlanMcastGroupAddress [ ] ={1,3,6,1,4,1,2076,105,3,5,1,4};
UINT4 FsSnoopVlanMcastGroupInnerVlanId [ ] ={1,3,6,1,4,1,2076,105,3,5,1,5};
UINT4 FsSnoopVlanMcastGroupPortList [ ] ={1,3,6,1,4,1,2076,105,3,5,1,6};
UINT4 FsSnoopVlanMcastGroupLocalPortList [ ] ={1,3,6,1,4,1,2076,105,3,5,1,7};
UINT4 FsSnoopVlanMcastReceiverPortIndex [ ] ={1,3,6,1,4,1,2076,105,3,6,1,1};
UINT4 FsSnoopVlanMcastReceiverHostAddress [ ] ={1,3,6,1,4,1,2076,105,3,6,1,2};
UINT4 FsSnoopVlanMcastReceiverSourceAddress [ ] ={1,3,6,1,4,1,2076,105,3,6,1,3};
UINT4 FsSnoopVlanMcastReceiverFilterMode [ ] ={1,3,6,1,4,1,2076,105,3,6,1,4};
UINT4 FsSnoopVlanIpFwdInstanceId [ ] ={1,3,6,1,4,1,2076,105,3,7,1,1};
UINT4 FsSnoopVlanIpFwdOuterVlanId [ ] ={1,3,6,1,4,1,2076,105,3,7,1,2};
UINT4 FsSnoopVlanIpFwdInetAddressType [ ] ={1,3,6,1,4,1,2076,105,3,7,1,3};
UINT4 FsSnoopVlanIpFwdSourceAddress [ ] ={1,3,6,1,4,1,2076,105,3,7,1,4};
UINT4 FsSnoopVlanIpFwdGroupAddress [ ] ={1,3,6,1,4,1,2076,105,3,7,1,5};
UINT4 FsSnoopVlanIpFwdInnerVlanId [ ] ={1,3,6,1,4,1,2076,105,3,7,1,6};
UINT4 FsSnoopVlanIpFwdPortList [ ] ={1,3,6,1,4,1,2076,105,3,7,1,7};
UINT4 FsSnoopVlanIpFwdLocalPortList [ ] ={1,3,6,1,4,1,2076,105,3,7,1,8};
UINT4 FsSnoopVlanBlkRtrPortList [ ] ={1,3,6,1,4,1,2076,105,3,8,1,1};
UINT4 FsSnoopVlanFilterMaxLimitType [ ] ={1,3,6,1,4,1,2076,105,3,8,1,2};
UINT4 FsSnoopVlanFilterMaxLimit [ ] ={1,3,6,1,4,1,2076,105,3,8,1,3};
UINT4 FsSnoopVlanFilter8021pPriority [ ] ={1,3,6,1,4,1,2076,105,3,8,1,4};
UINT4 FsSnoopVlanFilterDropReports [ ] ={1,3,6,1,4,1,2076,105,3,8,1,5};
UINT4 FsSnoopVlanMulticastProfileId [ ] ={1,3,6,1,4,1,2076,105,3,8,1,6};
UINT4 FsSnoopVlanPortPurgeInterval [ ] ={1,3,6,1,4,1,2076,105,3,8,1,7};
UINT4 FsSnoopVlanMaxResponseTime [ ] ={1,3,6,1,4,1,2076,105,3,8,1,8};
UINT4 FsSnoopVlanRtrLocalPortList [ ] ={1,3,6,1,4,1,2076,105,3,8,1,9};
UINT4 FsSnoopVlanBlkRtrLocalPortList [ ] ={1,3,6,1,4,1,2076,105,3,8,1,10};
UINT4 FsSnoopVlanStaticMcastGrpInstId [ ] ={1,3,6,1,4,1,2076,105,3,9,1,1};
UINT4 FsSnoopVlanStaticMcastGrpVlanId [ ] ={1,3,6,1,4,1,2076,105,3,9,1,2};
UINT4 FsSnoopVlanStaticMcastGrpAddressType [ ] ={1,3,6,1,4,1,2076,105,3,9,1,3};
UINT4 FsSnoopVlanStaticMcastGrpSourceAddress [ ] ={1,3,6,1,4,1,2076,105,3,9,1,4};
UINT4 FsSnoopVlanStaticMcastGrpGroupAddress [ ] ={1,3,6,1,4,1,2076,105,3,9,1,5};
UINT4 FsSnoopVlanStaticMcastGrpPortList [ ] ={1,3,6,1,4,1,2076,105,3,9,1,6};
UINT4 FsSnoopVlanStaticMcastGrpRowStatus [ ] ={1,3,6,1,4,1,2076,105,3,9,1,7};
UINT4 FsSnoopStatsInstId [ ] ={1,3,6,1,4,1,2076,105,4,1,1,1};
UINT4 FsSnoopStatsVlanId [ ] ={1,3,6,1,4,1,2076,105,4,1,1,2};
UINT4 FsSnoopStatsInetAddressType [ ] ={1,3,6,1,4,1,2076,105,4,1,1,3};
UINT4 FsSnoopStatsRxGenQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,4};
UINT4 FsSnoopStatsRxGrpQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,5};
UINT4 FsSnoopStatsRxGrpAndSrcQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,6};
UINT4 FsSnoopStatsRxAsmReports [ ] ={1,3,6,1,4,1,2076,105,4,1,1,7};
UINT4 FsSnoopStatsRxSsmReports [ ] ={1,3,6,1,4,1,2076,105,4,1,1,8};
UINT4 FsSnoopStatsRxSsmIsInMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,9};
UINT4 FsSnoopStatsRxSsmIsExMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,10};
UINT4 FsSnoopStatsRxSsmToInMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,11};
UINT4 FsSnoopStatsRxSsmToExMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,12};
UINT4 FsSnoopStatsRxSsmAllowMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,13};
UINT4 FsSnoopStatsRxSsmBlockMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,14};
UINT4 FsSnoopStatsRxAsmLeaves [ ] ={1,3,6,1,4,1,2076,105,4,1,1,15};
UINT4 FsSnoopStatsTxGenQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,16};
UINT4 FsSnoopStatsTxGrpQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,17};
UINT4 FsSnoopStatsTxGrpAndSrcQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,18};
UINT4 FsSnoopStatsTxAsmReports [ ] ={1,3,6,1,4,1,2076,105,4,1,1,19};
UINT4 FsSnoopStatsTxSsmReports [ ] ={1,3,6,1,4,1,2076,105,4,1,1,20};
UINT4 FsSnoopStatsTxAsmLeaves [ ] ={1,3,6,1,4,1,2076,105,4,1,1,21};
UINT4 FsSnoopStatsDroppedPkts [ ] ={1,3,6,1,4,1,2076,105,4,1,1,22};
UINT4 FsSnoopStatsUnsuccessfulJoins [ ] ={1,3,6,1,4,1,2076,105,4,1,1,23};
UINT4 FsSnoopStatsActiveJoins [ ] ={1,3,6,1,4,1,2076,105,4,1,1,24};
UINT4 FsSnoopStatsActiveGroups [ ] ={1,3,6,1,4,1,2076,105,4,1,1,25};
UINT4 FsSnoopStatsRxV1Report [ ] ={1,3,6,1,4,1,2076,105,4,1,1,26};
UINT4 FsSnoopStatsRxV2Report [ ] ={1,3,6,1,4,1,2076,105,4,1,1,27};
UINT4 FsSnoopStatsTxV1Report [ ] ={1,3,6,1,4,1,2076,105,4,1,1,28};
UINT4 FsSnoopStatsTxV2Report [ ] ={1,3,6,1,4,1,2076,105,4,1,1,29};
UINT4 FsSnoopStatsDropV1GenQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,31};                                                      UINT4 FsSnoopStatsDropV2GenQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,32};
UINT4 FsSnoopStatsDropV3GenQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,33};
UINT4 FsSnoopStatsDropV2GrpQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,34};
UINT4 FsSnoopStatsDropV3GrpQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,35};
UINT4 FsSnoopStatsDropGrpAndSrcQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,36};
UINT4 FsSnoopStatsDropV1Reports [ ] ={1,3,6,1,4,1,2076,105,4,1,1,37};
UINT4 FsSnoopStatsDropV2Reports [ ] ={1,3,6,1,4,1,2076,105,4,1,1,38};
UINT4 FsSnoopStatsDropSsmIsInMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,39};
UINT4 FsSnoopStatsDropSsmIsExMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,40};
UINT4 FsSnoopStatsDropSsmToInMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,41};
UINT4 FsSnoopStatsDropSsmToExMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,42};
UINT4 FsSnoopStatsDropSsmAllowMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,43};
UINT4 FsSnoopStatsDropSsmBlockMsgs [ ] ={1,3,6,1,4,1,2076,105,4,1,1,44};
UINT4 FsSnoopStatsDropAsmLeaves [ ] ={1,3,6,1,4,1,2076,105,4,1,1,45};
UINT4 FsSnoopStatsFwdGenQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,46};
UINT4 FsSnoopStatsFwdGrpQueries [ ] ={1,3,6,1,4,1,2076,105,4,1,1,47};
UINT4 FsSnoopPortIndex [ ] ={1,3,6,1,4,1,2076,105,5,1,1,1};
UINT4 FsSnoopPortInetAddressType [ ] ={1,3,6,1,4,1,2076,105,5,1,1,2};
UINT4 FsSnoopPortLeaveMode [ ] ={1,3,6,1,4,1,2076,105,5,1,1,3};
UINT4 FsSnoopPortRateLimit [ ] ={1,3,6,1,4,1,2076,105,5,1,1,4};
UINT4 FsSnoopPortMaxLimitType [ ] ={1,3,6,1,4,1,2076,105,5,1,1,5};
UINT4 FsSnoopPortMaxLimit [ ] ={1,3,6,1,4,1,2076,105,5,1,1,6};
UINT4 FsSnoopPortProfileId [ ] ={1,3,6,1,4,1,2076,105,5,1,1,7};
UINT4 FsSnoopPortMemberCnt [ ] ={1,3,6,1,4,1,2076,105,5,1,1,8};
UINT4 FsSnoopPortMaxBandwidthLimit [ ] ={1,3,6,1,4,1,2076,105,5,1,1,9};
UINT4 FsSnoopPortDropReports [ ] ={1,3,6,1,4,1,2076,105,5,1,1,10};
UINT4 FsSnoopPortRowStatus [ ] ={1,3,6,1,4,1,2076,105,5,1,1,11};
UINT4 FsSnoopEnhPortIndex [ ] ={1,3,6,1,4,1,2076,105,5,2,1,1};
UINT4 FsSnoopEnhPortInnerVlanId [ ] ={1,3,6,1,4,1,2076,105,5,2,1,2};
UINT4 FsSnoopEnhPortInetAddressType [ ] ={1,3,6,1,4,1,2076,105,5,2,1,3};
UINT4 FsSnoopEnhPortLeaveMode [ ] ={1,3,6,1,4,1,2076,105,5,2,1,4};
UINT4 FsSnoopEnhPortRateLimit [ ] ={1,3,6,1,4,1,2076,105,5,2,1,5};
UINT4 FsSnoopEnhPortMaxLimitType [ ] ={1,3,6,1,4,1,2076,105,5,2,1,6};
UINT4 FsSnoopEnhPortMaxLimit [ ] ={1,3,6,1,4,1,2076,105,5,2,1,7};
UINT4 FsSnoopEnhPortProfileId [ ] ={1,3,6,1,4,1,2076,105,5,2,1,8};
UINT4 FsSnoopEnhPortMemberCnt [ ] ={1,3,6,1,4,1,2076,105,5,2,1,9};
UINT4 FsSnoopEnhPortMaxBandwidthLimit [ ] ={1,3,6,1,4,1,2076,105,5,2,1,10};
UINT4 FsSnoopEnhPortDropReports [ ] ={1,3,6,1,4,1,2076,105,5,2,1,11};
UINT4 FsSnoopEnhPortRowStatus [ ] ={1,3,6,1,4,1,2076,105,5,2,1,12};
UINT4 FsSnoopRtrPortIndex [ ] ={1,3,6,1,4,1,2076,105,5,3,1,1};
UINT4 FsSnoopRtrPortVlanId [ ] ={1,3,6,1,4,1,2076,105,5,3,1,2};
UINT4 FsSnoopRtrPortInetAddressType [ ] ={1,3,6,1,4,1,2076,105,5,3,1,3};
UINT4 FsSnoopRtrPortOperVersion [ ] ={1,3,6,1,4,1,2076,105,5,3,1,4};
UINT4 FsSnoopRtrPortCfgOperVersion [ ] ={1,3,6,1,4,1,2076,105,5,3,1,5};
UINT4 FsSnoopOlderQuerierInterval [ ] ={1,3,6,1,4,1,2076,105,5,3,1,6};
UINT4 FsSnoopV3QuerierInterval [ ] ={1,3,6,1,4,1,2076,105,5,3,1,7};
UINT4 FsSnoopTrapHwErrType [ ] ={1,3,6,1,4,1,2076,105,6,5,1,1};


tMbDbEntry fssnpMibEntry[]= {

{{12,FsSnoopInstanceGlobalInstId}, GetNextIndexFsSnoopInstanceGlobalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopInstanceGlobalTableINDEX, 1, 0, 0, NULL},

{{12,FsSnoopInstanceGlobalMcastFwdMode}, GetNextIndexFsSnoopInstanceGlobalTable, FsSnoopInstanceGlobalMcastFwdModeGet, FsSnoopInstanceGlobalMcastFwdModeSet, FsSnoopInstanceGlobalMcastFwdModeTest, FsSnoopInstanceGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceGlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsSnoopInstanceGlobalSystemControl}, GetNextIndexFsSnoopInstanceGlobalTable, FsSnoopInstanceGlobalSystemControlGet, FsSnoopInstanceGlobalSystemControlSet, FsSnoopInstanceGlobalSystemControlTest, FsSnoopInstanceGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsSnoopInstanceGlobalLeaveConfigLevel}, GetNextIndexFsSnoopInstanceGlobalTable, FsSnoopInstanceGlobalLeaveConfigLevelGet, FsSnoopInstanceGlobalLeaveConfigLevelSet, FsSnoopInstanceGlobalLeaveConfigLevelTest, FsSnoopInstanceGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsSnoopInstanceGlobalEnhancedMode}, GetNextIndexFsSnoopInstanceGlobalTable, FsSnoopInstanceGlobalEnhancedModeGet, FsSnoopInstanceGlobalEnhancedModeSet, FsSnoopInstanceGlobalEnhancedModeTest, FsSnoopInstanceGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceGlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsSnoopInstanceGlobalReportProcessConfigLevel}, GetNextIndexFsSnoopInstanceGlobalTable, FsSnoopInstanceGlobalReportProcessConfigLevelGet, FsSnoopInstanceGlobalReportProcessConfigLevelSet, FsSnoopInstanceGlobalReportProcessConfigLevelTest, FsSnoopInstanceGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceGlobalTableINDEX, 1, 0, 0, "1"},

{{12,FsSnoopInstanceGlobalSparseMode}, GetNextIndexFsSnoopInstanceGlobalTable, FsSnoopInstanceGlobalSparseModeGet, FsSnoopInstanceGlobalSparseModeSet, FsSnoopInstanceGlobalSparseModeTest, FsSnoopInstanceGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceGlobalTableINDEX, 1, 0, 0, "2"},

{{12,FsSnoopInstanceControlPlaneDriven}, GetNextIndexFsSnoopInstanceGlobalTable, FsSnoopInstanceControlPlaneDrivenGet, FsSnoopInstanceControlPlaneDrivenSet, FsSnoopInstanceControlPlaneDrivenTest, FsSnoopInstanceGlobalTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceGlobalTableINDEX, 1, 0, 0, "2"},
{{12,FsSnoopInstanceConfigInstId}, GetNextIndexFsSnoopInstanceConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, NULL},

{{12,FsSnoopInetAddressType}, GetNextIndexFsSnoopInstanceConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, NULL},

{{12,FsSnoopStatus}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopStatusGet, FsSnoopStatusSet, FsSnoopStatusTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "2"},

{{12,FsSnoopProxyReportingStatus}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopProxyReportingStatusGet, FsSnoopProxyReportingStatusSet, FsSnoopProxyReportingStatusTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "1"},

{{12,FsSnoopRouterPortPurgeInterval}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopRouterPortPurgeIntervalGet, FsSnoopRouterPortPurgeIntervalSet, FsSnoopRouterPortPurgeIntervalTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 1, 0, "125"},

{{12,FsSnoopPortPurgeInterval}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopPortPurgeIntervalGet, FsSnoopPortPurgeIntervalSet, FsSnoopPortPurgeIntervalTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 1, 0, "260"},

{{12,FsSnoopReportForwardInterval}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopReportForwardIntervalGet, FsSnoopReportForwardIntervalSet, FsSnoopReportForwardIntervalTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "5"},

{{12,FsSnoopRetryCount}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopRetryCountGet, FsSnoopRetryCountSet, FsSnoopRetryCountTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "2"},

{{12,FsSnoopGrpQueryInterval}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopGrpQueryIntervalGet, FsSnoopGrpQueryIntervalSet, FsSnoopGrpQueryIntervalTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "2"},

{{12,FsSnoopReportFwdOnAllPorts}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopReportFwdOnAllPortsGet, FsSnoopReportFwdOnAllPortsSet, FsSnoopReportFwdOnAllPortsTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "2"},

{{12,FsSnoopTraceOption}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopTraceOptionGet, FsSnoopTraceOptionSet, FsSnoopTraceOptionTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, NULL},

{{12,FsSnoopOperStatus}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, NULL},

{{12,FsSnoopSendQueryOnTopoChange}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopSendQueryOnTopoChangeGet, FsSnoopSendQueryOnTopoChangeSet, FsSnoopSendQueryOnTopoChangeTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "2"},

{{12,FsSnoopSendLeaveOnTopoChange}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopSendLeaveOnTopoChangeGet, FsSnoopSendLeaveOnTopoChangeSet, FsSnoopSendLeaveOnTopoChangeTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "2"},

{{12,FsSnoopFilterStatus}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopFilterStatusGet, FsSnoopFilterStatusSet, FsSnoopFilterStatusTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "2"},

{{12,FsSnoopMulticastVlanStatus}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopMulticastVlanStatusGet, FsSnoopMulticastVlanStatusSet, FsSnoopMulticastVlanStatusTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "2"},

{{12,FsSnoopProxyStatus}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopProxyStatusGet, FsSnoopProxyStatusSet, FsSnoopProxyStatusTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "2"},

{{12,FsSnoopQueryFwdOnAllPorts}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopQueryFwdOnAllPortsGet, FsSnoopQueryFwdOnAllPortsSet, FsSnoopQueryFwdOnAllPortsTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, "2"},

{{12,FsSnoopFwdGroupsCnt}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopFwdGroupsCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, NULL},

{{12,FsSnoopDebugOption}, GetNextIndexFsSnoopInstanceConfigTable, FsSnoopDebugOptionGet, FsSnoopDebugOptionSet, FsSnoopDebugOptionTest, FsSnoopInstanceConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopInstanceConfigTableINDEX, 2, 0, 0, NULL},

{{12,FsSnoopVlanMcastMacFwdInstId}, GetNextIndexFsSnoopVlanMcastMacFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanMcastMacFwdTableINDEX, 4, 0, 0, NULL},

{{12,FsSnoopVlanMcastMacFwdVlanId}, GetNextIndexFsSnoopVlanMcastMacFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanMcastMacFwdTableINDEX, 4, 0, 0, NULL},

{{12,FsSnoopVlanMcastMacFwdInetAddressType}, GetNextIndexFsSnoopVlanMcastMacFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopVlanMcastMacFwdTableINDEX, 4, 0, 0, NULL},

{{12,FsSnoopVlanMcastMacFwdGroupAddress}, GetNextIndexFsSnoopVlanMcastMacFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsSnoopVlanMcastMacFwdTableINDEX, 4, 0, 0, NULL},

{{12,FsSnoopVlanMcastMacFwdPortList}, GetNextIndexFsSnoopVlanMcastMacFwdTable, FsSnoopVlanMcastMacFwdPortListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSnoopVlanMcastMacFwdTableINDEX, 4, 0, 0, NULL},

{{12,FsSnoopVlanMcastMacFwdLocalPortList}, GetNextIndexFsSnoopVlanMcastMacFwdTable, FsSnoopVlanMcastMacFwdLocalPortListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSnoopVlanMcastMacFwdTableINDEX, 4, 0, 0, NULL},

{{12,FsSnoopVlanMcastMacFwdEntryFlag}, GetNextIndexFsSnoopVlanMcastMacFwdTable, FsSnoopVlanMcastMacFwdEntryFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsSnoopVlanMcastMacFwdTableINDEX, 4, 0, 0, NULL},

{{12,FsSnoopVlanMcastIpFwdInstId}, GetNextIndexFsSnoopVlanMcastIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanMcastIpFwdTableINDEX, 5, 1, 0, NULL},

{{12,FsSnoopVlanMcastIpFwdVlanId}, GetNextIndexFsSnoopVlanMcastIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanMcastIpFwdTableINDEX, 5, 1, 0, NULL},

{{12,FsSnoopVlanMcastIpFwdAddressType}, GetNextIndexFsSnoopVlanMcastIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopVlanMcastIpFwdTableINDEX, 5, 1, 0, NULL},

{{12,FsSnoopVlanMcastIpFwdSourceAddress}, GetNextIndexFsSnoopVlanMcastIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSnoopVlanMcastIpFwdTableINDEX, 5, 1, 0, NULL},

{{12,FsSnoopVlanMcastIpFwdGroupAddress}, GetNextIndexFsSnoopVlanMcastIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSnoopVlanMcastIpFwdTableINDEX, 5, 1, 0, NULL},

{{12,FsSnoopVlanMcastIpFwdPortList}, GetNextIndexFsSnoopVlanMcastIpFwdTable, FsSnoopVlanMcastIpFwdPortListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSnoopVlanMcastIpFwdTableINDEX, 5, 1, 0, NULL},

{{12,FsSnoopVlanMcastIpFwdEntryFlag}, GetNextIndexFsSnoopVlanMcastIpFwdTable, FsSnoopVlanMcastIpFwdEntryFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsSnoopVlanMcastIpFwdTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanRouterInstId}, GetNextIndexFsSnoopVlanRouterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanRouterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanRouterVlanId}, GetNextIndexFsSnoopVlanRouterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanRouterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanRouterInetAddressType}, GetNextIndexFsSnoopVlanRouterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopVlanRouterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanRouterPortList}, GetNextIndexFsSnoopVlanRouterTable, FsSnoopVlanRouterPortListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSnoopVlanRouterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanRouterLocalPortList}, GetNextIndexFsSnoopVlanRouterTable, FsSnoopVlanRouterLocalPortListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSnoopVlanRouterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanFilterInstId}, GetNextIndexFsSnoopVlanFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanFilterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanFilterVlanId}, GetNextIndexFsSnoopVlanFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanFilterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanFilterInetAddressType}, GetNextIndexFsSnoopVlanFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopVlanFilterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanSnoopStatus}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanSnoopStatusGet, FsSnoopVlanSnoopStatusSet, FsSnoopVlanSnoopStatusTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, "1"},

{{12,FsSnoopVlanOperatingVersion}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanOperatingVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSnoopVlanFilterTableINDEX, 3, 1, 0, NULL},

{{12,FsSnoopVlanCfgOperVersion}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanCfgOperVersionGet, FsSnoopVlanCfgOperVersionSet, FsSnoopVlanCfgOperVersionTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 1, 0, "3"},

{{12,FsSnoopVlanFastLeave}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanFastLeaveGet, FsSnoopVlanFastLeaveSet, FsSnoopVlanFastLeaveTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, "2"},

{{12,FsSnoopVlanQuerier}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanQuerierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSnoopVlanFilterTableINDEX, 3, 0, 0, "2"},

{{12,FsSnoopVlanCfgQuerier}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanCfgQuerierGet, FsSnoopVlanCfgQuerierSet, FsSnoopVlanCfgQuerierTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, "2"},

{{12,FsSnoopVlanQueryInterval}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanQueryIntervalGet, FsSnoopVlanQueryIntervalSet, FsSnoopVlanQueryIntervalTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, "125"},

{{12,FsSnoopVlanRtrPortList}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanRtrPortListGet, FsSnoopVlanRtrPortListSet, FsSnoopVlanRtrPortListTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanRowStatus}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanRowStatusGet, FsSnoopVlanRowStatusSet, FsSnoopVlanRowStatusTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 1, NULL},

{{12,FsSnoopVlanStartupQueryCount}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanStartupQueryCountGet, FsSnoopVlanStartupQueryCountSet, FsSnoopVlanStartupQueryCountTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, "2"},

{{12,FsSnoopVlanStartupQueryInterval}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanStartupQueryIntervalGet, FsSnoopVlanStartupQueryIntervalSet, FsSnoopVlanStartupQueryIntervalTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, "32"},

{{12,FsSnoopVlanOtherQuerierPresentInterval}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanOtherQuerierPresentIntervalGet, FsSnoopVlanOtherQuerierPresentIntervalSet, FsSnoopVlanOtherQuerierPresentIntervalTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, "255"},

{{12,FsSnoopVlanQuerierIpAddress}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanQuerierIpAddressGet, FsSnoopVlanQuerierIpAddressSet, FsSnoopVlanQuerierIpAddressTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanQuerierIpFlag}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanQuerierIpFlagGet, FsSnoopVlanQuerierIpFlagSet, FsSnoopVlanQuerierIpFlagTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanElectedQuerier}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanElectedQuerierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsSnoopVlanFilterTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanRobustnessValue}, GetNextIndexFsSnoopVlanFilterTable, FsSnoopVlanRobustnessValueGet, FsSnoopVlanRobustnessValueSet, FsSnoopVlanRobustnessValueTest, FsSnoopVlanFilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopVlanFilterTableINDEX, 3, 0, 0, "2"},


{{12,FsSnoopVlanMcastGroupInstanceId}, GetNextIndexFsSnoopVlanMcastGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanMcastGroupTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanMcastGroupOuterVlanId}, GetNextIndexFsSnoopVlanMcastGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsSnoopVlanMcastGroupTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanMcastGroupInetAddressType}, GetNextIndexFsSnoopVlanMcastGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopVlanMcastGroupTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanMcastGroupAddress}, GetNextIndexFsSnoopVlanMcastGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSnoopVlanMcastGroupTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanMcastGroupInnerVlanId}, GetNextIndexFsSnoopVlanMcastGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsSnoopVlanMcastGroupTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanMcastGroupPortList}, GetNextIndexFsSnoopVlanMcastGroupTable, FsSnoopVlanMcastGroupPortListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSnoopVlanMcastGroupTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanMcastGroupLocalPortList}, GetNextIndexFsSnoopVlanMcastGroupTable, FsSnoopVlanMcastGroupLocalPortListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSnoopVlanMcastGroupTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanMcastReceiverPortIndex}, GetNextIndexFsSnoopVlanMcastReceiverTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopVlanMcastReceiverTableINDEX, 8, 0, 0, NULL},

{{12,FsSnoopVlanMcastReceiverHostAddress}, GetNextIndexFsSnoopVlanMcastReceiverTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSnoopVlanMcastReceiverTableINDEX, 8, 0, 0, NULL},

{{12,FsSnoopVlanMcastReceiverSourceAddress}, GetNextIndexFsSnoopVlanMcastReceiverTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSnoopVlanMcastReceiverTableINDEX, 8, 0, 0, NULL},

{{12,FsSnoopVlanMcastReceiverFilterMode}, GetNextIndexFsSnoopVlanMcastReceiverTable, FsSnoopVlanMcastReceiverFilterModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSnoopVlanMcastReceiverTableINDEX, 8, 0, 0, NULL},

{{12,FsSnoopVlanIpFwdInstanceId}, GetNextIndexFsSnoopVlanIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanIpFwdTableINDEX, 6, 0, 0, NULL},

{{12,FsSnoopVlanIpFwdOuterVlanId}, GetNextIndexFsSnoopVlanIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsSnoopVlanIpFwdTableINDEX, 6, 0, 0, NULL},

{{12,FsSnoopVlanIpFwdInetAddressType}, GetNextIndexFsSnoopVlanIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopVlanIpFwdTableINDEX, 6, 0, 0, NULL},

{{12,FsSnoopVlanIpFwdSourceAddress}, GetNextIndexFsSnoopVlanIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSnoopVlanIpFwdTableINDEX, 6, 0, 0, NULL},

{{12,FsSnoopVlanIpFwdGroupAddress}, GetNextIndexFsSnoopVlanIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSnoopVlanIpFwdTableINDEX, 6, 0, 0, NULL},

{{12,FsSnoopVlanIpFwdInnerVlanId}, GetNextIndexFsSnoopVlanIpFwdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsSnoopVlanIpFwdTableINDEX, 6, 0, 0, NULL},

{{12,FsSnoopVlanIpFwdPortList}, GetNextIndexFsSnoopVlanIpFwdTable, FsSnoopVlanIpFwdPortListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSnoopVlanIpFwdTableINDEX, 6, 0, 0, NULL},

{{12,FsSnoopVlanIpFwdLocalPortList}, GetNextIndexFsSnoopVlanIpFwdTable, FsSnoopVlanIpFwdLocalPortListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsSnoopVlanIpFwdTableINDEX, 6, 0, 0, NULL},

{{12,FsSnoopVlanBlkRtrPortList}, GetNextIndexFsSnoopVlanFilterXTable, FsSnoopVlanBlkRtrPortListGet, FsSnoopVlanBlkRtrPortListSet, FsSnoopVlanBlkRtrPortListTest, FsSnoopVlanFilterXTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSnoopVlanFilterXTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanFilterMaxLimitType}, GetNextIndexFsSnoopVlanFilterXTable, FsSnoopVlanFilterMaxLimitTypeGet, FsSnoopVlanFilterMaxLimitTypeSet, FsSnoopVlanFilterMaxLimitTypeTest, FsSnoopVlanFilterXTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopVlanFilterXTableINDEX, 3, 0, 0, "0"},

{{12,FsSnoopVlanFilterMaxLimit}, GetNextIndexFsSnoopVlanFilterXTable, FsSnoopVlanFilterMaxLimitGet, FsSnoopVlanFilterMaxLimitSet, FsSnoopVlanFilterMaxLimitTest, FsSnoopVlanFilterXTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopVlanFilterXTableINDEX, 3, 0, 0, "0"},

{{12,FsSnoopVlanFilter8021pPriority}, GetNextIndexFsSnoopVlanFilterXTable, FsSnoopVlanFilter8021pPriorityGet, FsSnoopVlanFilter8021pPrioritySet, FsSnoopVlanFilter8021pPriorityTest, FsSnoopVlanFilterXTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopVlanFilterXTableINDEX, 3, 0, 0, "7"},

{{12,FsSnoopVlanFilterDropReports}, GetNextIndexFsSnoopVlanFilterXTable, FsSnoopVlanFilterDropReportsGet, FsSnoopVlanFilterDropReportsSet, FsSnoopVlanFilterDropReportsTest, FsSnoopVlanFilterXTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopVlanFilterXTableINDEX, 3, 0, 0, "0"},

{{12,FsSnoopVlanMulticastProfileId}, GetNextIndexFsSnoopVlanFilterXTable, FsSnoopVlanMulticastProfileIdGet, FsSnoopVlanMulticastProfileIdSet, FsSnoopVlanMulticastProfileIdTest, FsSnoopVlanFilterXTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopVlanFilterXTableINDEX, 3, 0, 0, "0"},

{{12,FsSnoopVlanPortPurgeInterval}, GetNextIndexFsSnoopVlanFilterXTable, FsSnoopVlanPortPurgeIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsSnoopVlanFilterXTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanMaxResponseTime}, GetNextIndexFsSnoopVlanFilterXTable, FsSnoopVlanMaxResponseTimeGet, FsSnoopVlanMaxResponseTimeSet, FsSnoopVlanMaxResponseTimeTest, FsSnoopVlanFilterXTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopVlanFilterXTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanRtrLocalPortList}, GetNextIndexFsSnoopVlanFilterXTable, FsSnoopVlanRtrLocalPortListGet, FsSnoopVlanRtrLocalPortListSet, FsSnoopVlanRtrLocalPortListTest, FsSnoopVlanFilterXTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSnoopVlanFilterXTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanBlkRtrLocalPortList}, GetNextIndexFsSnoopVlanFilterXTable, FsSnoopVlanBlkRtrLocalPortListGet, FsSnoopVlanBlkRtrLocalPortListSet, FsSnoopVlanBlkRtrLocalPortListTest, FsSnoopVlanFilterXTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSnoopVlanFilterXTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopVlanStaticMcastGrpInstId}, GetNextIndexFsSnoopVlanStaticMcastGrpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanStaticMcastGrpTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanStaticMcastGrpVlanId}, GetNextIndexFsSnoopVlanStaticMcastGrpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopVlanStaticMcastGrpTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanStaticMcastGrpAddressType}, GetNextIndexFsSnoopVlanStaticMcastGrpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopVlanStaticMcastGrpTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanStaticMcastGrpSourceAddress}, GetNextIndexFsSnoopVlanStaticMcastGrpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSnoopVlanStaticMcastGrpTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanStaticMcastGrpGroupAddress}, GetNextIndexFsSnoopVlanStaticMcastGrpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsSnoopVlanStaticMcastGrpTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanStaticMcastGrpPortList}, GetNextIndexFsSnoopVlanStaticMcastGrpTable, FsSnoopVlanStaticMcastGrpPortListGet, FsSnoopVlanStaticMcastGrpPortListSet, FsSnoopVlanStaticMcastGrpPortListTest, FsSnoopVlanStaticMcastGrpTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsSnoopVlanStaticMcastGrpTableINDEX, 5, 0, 0, NULL},

{{12,FsSnoopVlanStaticMcastGrpRowStatus}, GetNextIndexFsSnoopVlanStaticMcastGrpTable, FsSnoopVlanStaticMcastGrpRowStatusGet, FsSnoopVlanStaticMcastGrpRowStatusSet, FsSnoopVlanStaticMcastGrpRowStatusTest, FsSnoopVlanStaticMcastGrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopVlanStaticMcastGrpTableINDEX, 5, 0, 1, NULL},

{{12,FsSnoopStatsInstId}, GetNextIndexFsSnoopStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsVlanId}, GetNextIndexFsSnoopStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsInetAddressType}, GetNextIndexFsSnoopStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxGenQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxGenQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxGrpQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxGrpQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxGrpAndSrcQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxGrpAndSrcQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxAsmReports}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxAsmReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxSsmReports}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxSsmReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxSsmIsInMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxSsmIsInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxSsmIsExMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxSsmIsExMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxSsmToInMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxSsmToInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxSsmToExMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxSsmToExMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxSsmAllowMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxSsmAllowMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxSsmBlockMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxSsmBlockMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxAsmLeaves}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxAsmLeavesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsTxGenQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsTxGenQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsTxGrpQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsTxGrpQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsTxGrpAndSrcQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsTxGrpAndSrcQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsTxAsmReports}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsTxAsmReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsTxSsmReports}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsTxSsmReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsTxAsmLeaves}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsTxAsmLeavesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDroppedPkts}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDroppedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsUnsuccessfulJoins}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsUnsuccessfulJoinsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsActiveJoins}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsActiveJoinsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsActiveGroups}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsActiveGroupsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxV1Report}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxV1ReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsRxV2Report}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsRxV2ReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsTxV1Report}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsTxV1ReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsTxV2Report}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsTxV2ReportGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropV1GenQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropV1GenQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropV2GenQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropV2GenQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropV3GenQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropV3GenQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropV2GrpQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropV2GrpQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropV3GrpQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropV3GrpQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropGrpAndSrcQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropGrpAndSrcQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropV1Reports}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropV1ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropV2Reports}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropV2ReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropSsmIsInMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropSsmIsInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropSsmIsExMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropSsmIsExMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropSsmToInMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropSsmToInMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropSsmToExMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropSsmToExMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropSsmAllowMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropSsmAllowMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropSsmBlockMsgs}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropSsmBlockMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsDropAsmLeaves}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsDropAsmLeavesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopStatsFwdGenQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsFwdGenQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL}, 
{{12,FsSnoopStatsFwdGrpQueries}, GetNextIndexFsSnoopStatsTable, FsSnoopStatsFwdGrpQueriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsSnoopStatsTableINDEX, 3, 0, 0, NULL},


{{12,FsSnoopPortIndex}, GetNextIndexFsSnoopPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopPortTableINDEX, 2, 0, 0, NULL},

{{12,FsSnoopPortInetAddressType}, GetNextIndexFsSnoopPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopPortTableINDEX, 2, 0, 0, NULL},

{{12,FsSnoopPortLeaveMode}, GetNextIndexFsSnoopPortTable, FsSnoopPortLeaveModeGet, FsSnoopPortLeaveModeSet, FsSnoopPortLeaveModeTest, FsSnoopPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopPortTableINDEX, 2, 0, 0, "3"},

{{12,FsSnoopPortRateLimit}, GetNextIndexFsSnoopPortTable, FsSnoopPortRateLimitGet, FsSnoopPortRateLimitSet, FsSnoopPortRateLimitTest, FsSnoopPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopPortTableINDEX, 2, 0, 0, "2147483647"},

{{12,FsSnoopPortMaxLimitType}, GetNextIndexFsSnoopPortTable, FsSnoopPortMaxLimitTypeGet, FsSnoopPortMaxLimitTypeSet, FsSnoopPortMaxLimitTypeTest, FsSnoopPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopPortTableINDEX, 2, 0, 0, "0"},

{{12,FsSnoopPortMaxLimit}, GetNextIndexFsSnoopPortTable, FsSnoopPortMaxLimitGet, FsSnoopPortMaxLimitSet, FsSnoopPortMaxLimitTest, FsSnoopPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopPortTableINDEX, 2, 0, 0, "0"},

{{12,FsSnoopPortProfileId}, GetNextIndexFsSnoopPortTable, FsSnoopPortProfileIdGet, FsSnoopPortProfileIdSet, FsSnoopPortProfileIdTest, FsSnoopPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopPortTableINDEX, 2, 0, 0, "0"},

{{12,FsSnoopPortMemberCnt}, GetNextIndexFsSnoopPortTable, FsSnoopPortMemberCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsSnoopPortTableINDEX, 2, 0, 0, NULL},

{{12,FsSnoopPortMaxBandwidthLimit}, GetNextIndexFsSnoopPortTable, FsSnoopPortMaxBandwidthLimitGet, FsSnoopPortMaxBandwidthLimitSet, FsSnoopPortMaxBandwidthLimitTest, FsSnoopPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopPortTableINDEX, 2, 0, 0, NULL},

{{12,FsSnoopPortDropReports}, GetNextIndexFsSnoopPortTable, FsSnoopPortDropReportsGet, FsSnoopPortDropReportsSet, FsSnoopPortDropReportsTest, FsSnoopPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopPortTableINDEX, 2, 0, 0, "1"},

{{12,FsSnoopPortRowStatus}, GetNextIndexFsSnoopPortTable, FsSnoopPortRowStatusGet, FsSnoopPortRowStatusSet, FsSnoopPortRowStatusTest, FsSnoopPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopPortTableINDEX, 2, 0, 1, NULL},

{{12,FsSnoopEnhPortIndex}, GetNextIndexFsSnoopEnhPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopEnhPortTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopEnhPortInnerVlanId}, GetNextIndexFsSnoopEnhPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsSnoopEnhPortTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopEnhPortInetAddressType}, GetNextIndexFsSnoopEnhPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopEnhPortTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopEnhPortLeaveMode}, GetNextIndexFsSnoopEnhPortTable, FsSnoopEnhPortLeaveModeGet, FsSnoopEnhPortLeaveModeSet, FsSnoopEnhPortLeaveModeTest, FsSnoopEnhPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopEnhPortTableINDEX, 3, 0, 0, "3"},

{{12,FsSnoopEnhPortRateLimit}, GetNextIndexFsSnoopEnhPortTable, FsSnoopEnhPortRateLimitGet, FsSnoopEnhPortRateLimitSet, FsSnoopEnhPortRateLimitTest, FsSnoopEnhPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopEnhPortTableINDEX, 3, 0, 0, "2147483647"},

{{12,FsSnoopEnhPortMaxLimitType}, GetNextIndexFsSnoopEnhPortTable, FsSnoopEnhPortMaxLimitTypeGet, FsSnoopEnhPortMaxLimitTypeSet, FsSnoopEnhPortMaxLimitTypeTest, FsSnoopEnhPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopEnhPortTableINDEX, 3, 0, 0, "0"},

{{12,FsSnoopEnhPortMaxLimit}, GetNextIndexFsSnoopEnhPortTable, FsSnoopEnhPortMaxLimitGet, FsSnoopEnhPortMaxLimitSet, FsSnoopEnhPortMaxLimitTest, FsSnoopEnhPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopEnhPortTableINDEX, 3, 0, 0, "0"},

{{12,FsSnoopEnhPortProfileId}, GetNextIndexFsSnoopEnhPortTable, FsSnoopEnhPortProfileIdGet, FsSnoopEnhPortProfileIdSet, FsSnoopEnhPortProfileIdTest, FsSnoopEnhPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopEnhPortTableINDEX, 3, 0, 0, "0"},

{{12,FsSnoopEnhPortMemberCnt}, GetNextIndexFsSnoopEnhPortTable, FsSnoopEnhPortMemberCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsSnoopEnhPortTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopEnhPortMaxBandwidthLimit}, GetNextIndexFsSnoopEnhPortTable, FsSnoopEnhPortMaxBandwidthLimitGet, FsSnoopEnhPortMaxBandwidthLimitSet, FsSnoopEnhPortMaxBandwidthLimitTest, FsSnoopEnhPortTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsSnoopEnhPortTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopEnhPortDropReports}, GetNextIndexFsSnoopEnhPortTable, FsSnoopEnhPortDropReportsGet, FsSnoopEnhPortDropReportsSet, FsSnoopEnhPortDropReportsTest, FsSnoopEnhPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopEnhPortTableINDEX, 3, 0, 0, "1"},

{{12,FsSnoopEnhPortRowStatus}, GetNextIndexFsSnoopEnhPortTable, FsSnoopEnhPortRowStatusGet, FsSnoopEnhPortRowStatusSet, FsSnoopEnhPortRowStatusTest, FsSnoopEnhPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopEnhPortTableINDEX, 3, 0, 1, NULL},

{{12,FsSnoopRtrPortIndex}, GetNextIndexFsSnoopRtrPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopRtrPortTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopRtrPortVlanId}, GetNextIndexFsSnoopRtrPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsSnoopRtrPortTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopRtrPortInetAddressType}, GetNextIndexFsSnoopRtrPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsSnoopRtrPortTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopRtrPortOperVersion}, GetNextIndexFsSnoopRtrPortTable, FsSnoopRtrPortOperVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSnoopRtrPortTableINDEX, 3, 0, 0, "3"},

{{12,FsSnoopRtrPortCfgOperVersion}, GetNextIndexFsSnoopRtrPortTable, FsSnoopRtrPortCfgOperVersionGet, FsSnoopRtrPortCfgOperVersionSet, FsSnoopRtrPortCfgOperVersionTest, FsSnoopRtrPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsSnoopRtrPortTableINDEX, 3, 0, 0, "3"},

{{12,FsSnoopOlderQuerierInterval}, GetNextIndexFsSnoopRtrPortTable, FsSnoopOlderQuerierIntervalGet, FsSnoopOlderQuerierIntervalSet, FsSnoopOlderQuerierIntervalTest, FsSnoopRtrPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsSnoopRtrPortTableINDEX, 3, 0, 0, "125"},

{{12,FsSnoopV3QuerierInterval}, GetNextIndexFsSnoopRtrPortTable, FsSnoopV3QuerierIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsSnoopRtrPortTableINDEX, 3, 0, 0, NULL},

{{12,FsSnoopTrapHwErrType}, GetNextIndexFsSnoopTrapObjectsTable, FsSnoopTrapHwErrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsSnoopTrapObjectsTableINDEX, 6, 0, 0, NULL},
};
tMibData fssnpEntry = { 179, fssnpMibEntry };

#endif /* _FSSNPDB_H */

