/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: snpinc.h,v 1.20 2015/05/25 12:38:57 siva Exp $
 *
 * Description: This file contains all header files that are to be included
 *              in the source files for snooping(MLD/IGMP) module.
 *****************************************************************************/
#ifndef _SNPINC_H
#define _SNPINC_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/*****************************************************************************/
/*    FILE  NAME            : snpinc.h                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                 */
/*    SUBSYSTEM NAME        : Snooping                                        */
/*    MODULE NAME           : Snooping (MLD/IGMP) header files                */
/*    LANGUAGE              : C                                               */
/*    TARGET ENVIRONMENT    : Any                                             */
/*    DATE OF FIRST RELEASE :                                                 */
/*    AUTHOR                : Aricent Inc.                                 */
/*    DESCRIPTION           : This file contains all header files that are to */
/*                            be included in the source files                 */
/*                            for snooping(MLD/IGMP) module                   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                       */ 
/*---------------------------------------------------------------------------*/
#include "lr.h"
#include "ip.h"
#include "cfa.h"
#include "msr.h"
#include "bridge.h"
#include "la.h"
#include "redblack.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "fsvlan.h"
#include "ip6util.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#include "l2iwf.h"
#include "garp.h"    
#include "mrp.h"
#include "utilipvx.h"    
#include "pim.h"
#include "fssnmp.h"
#include "tac.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

#include "ipv6.h"
#include "snp.h"
#include "snpcli.h"
#include "snpsys.h"
#include "iss.h"

#ifdef L2RED_WANTED
#include "rmgr.h"
#endif

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "nputil.h"
#endif

#ifdef ICCH_WANTED
#include "icch.h"
#include "snpicch.h"
#endif /* ICCH_WANTED */

#ifdef MLDS_WANTED
#ifdef NPAPI_WANTED
#ifndef NP_BACKWD_COMPATIBILITY
#include "mldsminp.h"
#else
#include "mldsnp.h"
#include "snpnpwr.h"
#endif
#endif
#endif

#ifdef IGS_WANTED
#ifdef NPAPI_WANTED
#ifndef NP_BACKWD_COMPATIBILITY
#include "igsminp.h"
#else
#include "igsnp.h"
#include "snpnpwr.h"
#endif
#endif
#endif

#include "vcm.h"
#include "snpdefn.h"
#include "snpmacs.h"
#include "snptdfs.h"
#include "snppt.h" 
#include "snptrc.h"
#include "fssnplow.h" 
#include "fssnpwr.h" 
#include "snpclipt.h"
#include "snpred.h"
#include "snpextn.h"
#include "fssyslog.h"
#include "snpsz.h"
#ifdef SNMP_2_WANTED
#include "snmputil.h"
#endif

#endif /* _SNPINC_H */
