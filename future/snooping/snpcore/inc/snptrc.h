#ifndef _SNPTRC_H
#define _SNPTRC_H
/* $Id: snptrc.h,v 1.14 2017/08/01 13:50:25 siva Exp $*/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/*****************************************************************************/
/*    FILE  NAME            : snptrc.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping (MLD/IGMP) TRACE definitions          */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains TRACE definitions           */
/*                            for snooping(MLD/IGMP) module                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                       */ 
/*---------------------------------------------------------------------------*/
#include "trace.h"
#include "utltrci.h"
extern CHR1 gacSnpSyslogmsg[MAX_LOG_STR_LEN];
/* Trace and debug flags */
#define   SNOOP_TRC_FLAG     SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->SnoopInfo[SNOOP_INFO_IGS_ENTRY].u4TraceOption   | SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->SnoopInfo[SNOOP_INFO_MLDS_ENTRY].u4TraceOption 
/* Trace definitions */

#define   SNOOP_TRC_STR_LEN      44 /* SNOOP_INSTANCE_ALIAS_LEN + 
                                       12 characters for type of message */
#define   SNOOP_IGS_TRC_FLAG     SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->SnoopInfo[SNOOP_INFO_IGS_ENTRY].u4TraceOption   

#define  SNOOP_MLDS_TRC_FLAG     SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->SnoopInfo[SNOOP_INFO_MLDS_ENTRY].u4TraceOption 

#define   SNOOP_DBG_FLAG     SNOOP_INSTANCE_INFO (gu4SnoopDbgInstId)->SnoopInfo[SNOOP_INFO_IGS_ENTRY].u4DebugOption   | SNOOP_INSTANCE_INFO (gu4SnoopDbgInstId)->SnoopInfo[SNOOP_INFO_MLDS_ENTRY].u4DebugOption

#define   SNOOP_IGS_DBG_FLAG     SNOOP_INSTANCE_INFO (gu4SnoopDbgInstId)->SnoopInfo[SNOOP_INFO_IGS_ENTRY].u4DebugOption

#define  SNOOP_MLDS_DBG_FLAG     SNOOP_INSTANCE_INFO (gu4SnoopDbgInstId)->SnoopInfo[SNOOP_INFO_MLDS_ENTRY].u4DebugOption

#define   SNOOP_DBG_FLAG_INST(u4Inst)     SNOOP_INSTANCE_INFO (u4Inst)->SnoopInfo[SNOOP_INFO_IGS_ENTRY].u4DebugOption   | SNOOP_INSTANCE_INFO (u4Inst)->SnoopInfo[SNOOP_INFO_MLDS_ENTRY].u4DebugOption

#define  SNOOP_DATA_TRC           SnoopUtilGetModuleNameTrace(SNOOP_TRC_DATA)
#define  SNOOP_CONTROL_TRC         SnoopUtilGetModuleNameTrace(SNOOP_TRC_CONTROL)
#define  SNOOP_Tx_TRC            SnoopUtilGetModuleNameTrace(SNOOP_TRC_Tx)
#define  SNOOP_Rx_TRC              SnoopUtilGetModuleNameTrace(SNOOP_TRC_Rx)

#define  SNOOP_INIT_DBG        SnoopUtilGetModuleName(SNOOP_DBG_INIT)
#define  SNOOP_OS_RES_DBG      SnoopUtilGetModuleName(SNOOP_DBG_RESRC)
#define  SNOOP_TMR_DBG         SnoopUtilGetModuleName(SNOOP_DBG_TMR)
#define  SNOOP_SRC_DBG         SnoopUtilGetModuleName(SNOOP_DBG_SRC)
#define  SNOOP_GRP_DBG         SnoopUtilGetModuleName(SNOOP_DBG_GRP)
#define  SNOOP_QRY_DBG         SnoopUtilGetModuleName(SNOOP_DBG_QRY)
#define  SNOOP_RED_DBG         SnoopUtilGetModuleName(SNOOP_DBG_RED)
#define  SNOOP_PKT_DBG         SnoopUtilGetModuleName(SNOOP_DBG_PKT)
#define  SNOOP_FWD_DBG         SnoopUtilGetModuleName(SNOOP_DBG_FWD)
#define  SNOOP_VLAN_DBG        SnoopUtilGetModuleName(SNOOP_DBG_VLAN)
#define  SNOOP_ENTRY_DBG       SnoopUtilGetModuleName(SNOOP_DBG_ENTRY)
#define  SNOOP_EXIT_DBG        SnoopUtilGetModuleName(SNOOP_DBG_EXIT)
#define  SNOOP_NP_DBG          SnoopUtilGetModuleName(SNOOP_DBG_NP)
#define  SNOOP_MGMT_DBG        SnoopUtilGetModuleName(SNOOP_DBG_MGMT)
#define  SNOOP_BUFFER_DBG      SnoopUtilGetModuleName(SNOOP_DBG_BUFFER)
#define  SNOOP_ICCH_DBG        SnoopUtilGetModuleName(SNOOP_DBG_ICCH)
#define  SNOOP_ALL_FAILURE_DBG SnoopUtilGetModuleName(SNOOP_DBG_ALL_FAILURE)

#define  SNOOP_INIT_TRC    SnoopUtilGetModuleName(SNOOP_DBG_INIT)
#define  SNOOP_VLAN_TRC    SnoopUtilGetModuleName(SNOOP_DBG_VLAN)
#define  SNOOP_GRP_TRC     SnoopUtilGetModuleName(SNOOP_DBG_GRP)
#define  SNOOP_QRY_TRC     SnoopUtilGetModuleName(SNOOP_DBG_QRY)
#define  SNOOP_FWD_TRC     SnoopUtilGetModuleName(SNOOP_DBG_FWD)
#define  SNOOP_PKT_TRC     SnoopUtilGetModuleName(SNOOP_DBG_PKT)
#define  SNOOP_RED_TRC     SnoopUtilGetModuleName(SNOOP_DBG_RED)
#define  SNOOP_ICCH_TRC    SnoopUtilGetModuleName(SNOOP_DBG_ICCH)




#define  SYSLOG_SNOOP_MSG_ARG2(level,flg, mod, modname, fmt, arg1, arg2) \
{\
    MEMSET (gacSnpSyslogmsg, 0, sizeof (gacSnpSyslogmsg));\
    if ((STRLEN (fmt) + STRLEN (arg1)) < MAX_LOG_STR_LEN)\
    {\
    STRCAT (gacSnpSyslogmsg, fmt);\
    STRCAT (gacSnpSyslogmsg, arg1);\
    if (modname != NULL) \
    {\
    UtlSysTrcLog(level, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, gacSnpSyslogmsg, arg2);\
    }\
    }\
}
#define  SYSLOG_SNOOP_MSG_ARG1(level,flg, mod, modname, fmt, arg1) \
{\
    MEMSET (gacSnpSyslogmsg, 0, sizeof (gacSnpSyslogmsg));\
    if ((STRLEN (fmt) + STRLEN (arg1)) < MAX_LOG_STR_LEN)\
    {\
    STRCAT (gacSnpSyslogmsg, fmt);\
    STRCAT (gacSnpSyslogmsg, arg1);\
    if (modname != NULL) \
    {\
    UtlSysTrcLog(level, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, gacSnpSyslogmsg);\
    }\
    }\
}

#define  SYSLOG_SNOOP_MSG(level,flg, mod, modname, fmt) \
{\
 if (modname != NULL) \
 {\
  UtlSysTrcLog(level, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt);\
 }\
}

#define SNOOP_PKT_DUMP(TraceLevel, TraceFlag, modname, pBuf, Length, fmt, arg1)                           \
{\
    if ((TraceLevel & SNOOP_DBG_PKT) == SNOOP_DBG_PKT)  \
    {\
 UtlTrcLog(TraceLevel, SNOOP_DBG_PKT , modname, fmt, arg1); \
 MOD_PKT_DUMP(DUMP_TRC, DUMP_TRC, "", pBuf, Length, "");   \
    }\
}

#define SNOOP_DUMP(cmod, mode,mod,fmt) \
{\
 if((cmod & (SNOOP_TRC_Tx| SNOOP_TRC_Rx)) == (SNOOP_TRC_Tx| SNOOP_TRC_Rx))\
 {\
  UtlDmpMsg(cmod, 0, fmt, cmod, 0, SNOOP_TRC_ALL, FALSE);\
 }\
}
#define  SNOOP_TRC(flg, mod, modname, fmt) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
      UtlSysTrcLog(SYSLOG_INVAL_LEVEL, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt);\
       }\
}

#define  SNOOP_TRC_ARG1(flg, mod, modname, fmt, arg1) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
      UtlSysTrcLog(SYSLOG_INVAL_LEVEL, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt, arg1);\
 }\
}
#define  SNOOP_TRC_ARG2(flg, mod, modname, fmt ,arg1 ,arg2) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt, arg1, arg2);\
 }\
}
#define  SNOOP_TRC_ARG3(flg, mod, modname, fmt ,arg1 ,arg2, arg3) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt, arg1, arg2, arg3);\
 }\
}
#define  SNOOP_TRC_ARG4(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt,\
     arg1, arg2, arg3, arg4);\
 }\
}
#define  SNOOP_TRC_ARG5(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt,\
     arg1, arg2, arg3, arg4, arg5);\
 }\
}
#define  SNOOP_TRC_ARG6(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt, \
    arg1, arg2, arg3, arg4, arg5, arg6);\
 }\
}
#define  SNOOP_DBG_EXT(flg, mod, modname, fmt) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, mod, SNOOP_TASK_NAME, modname, fmt);\
 }\
}

#define  SNOOP_DBG(flg, mod, modname, fmt) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt);\
 }\
}
#define  SNOOP_DBG_ARG1(flg, mod, modname, fmt, arg1) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt, arg1);\
 }\
}
#define  SNOOP_DBG_ARG2(flg, mod, modname, fmt ,arg1 ,arg2) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt, arg1, arg2);\
 }\
}
#define  SNOOP_DBG_ARG3(flg, mod, modname, fmt ,arg1 ,arg2, arg3) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt,\
     arg1, arg2, arg3);\
 }\
}
#define  SNOOP_DBG_ARG4(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt,\
     arg1, arg2, arg3, arg4);\
 }\
}
#define  SNOOP_DBG_ARG5(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt, \
    arg1, arg2, arg3, arg4, arg5);\
 }\
}
#define  SNOOP_DBG_ARG6(flg, mod, modname, fmt ,arg1 ,arg2, arg3, arg4, arg5, arg6) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL, flg, gu4SnoopTrcModule, SNOOP_TASK_NAME, modname, fmt,\
     arg1, arg2, arg3, arg4, arg5, arg6);\
 }\
}



#define  SNOOP_GBL_NAME          ((const char *)"SNOOP:GBL")
#define  SNOOP_MEM_NAME          ((const char *)"SNOOP:MEM")
#define  SNOOP_MGMT_NAME         ((const char *)"SNOOP:MGMT")
#define  SNOOP_RED_NAME          (( const char *)"SNOOP:RED")
#define  SNOOP_INIT_NAME         (( const char *)"SNOOP:INIT")
#define  SNOOP_OS_RES_NAME       (( const char *)"SNOOP:RESRC")
#define  SNOOP_GRP_NAME          ((const char *)"SNOOP:GRP")
#define  SNOOP_VLAN_NAME         ((const char *)"SNOOP:VLAN")
#define  SNOOP_PKT_NAME          ((const char *)"SNOOP:PKT")
#define  SNOOP_TMR_NAME          ((const char *)"SNOOP:TMR")
#define  SNOOP_FWD_NAME          ((const char *)"SNOOP:FWD")
#define  SNOOP_DATA_NAME         ((const char *)"SNOOP:DATA")
#define  SNOOP_EXIT_NAME         ((const char *)"SNOOP:EXIT")
#define  SNOOP_QRY_NAME          ((const char *)"SNOOP:QRY")
#define  SNOOP_NP_NAME          ((const char *)"SNOOP:NP")

#define  SNOOP_GBL_TRC_FLAG      ALL_FAILURE_TRC
#define SNOOP_GBL_DBG_FLAG  SNOOP_INSTANCE_INFO (gu4SnoopDbgInstId)->SnoopInfo[SNOOP_INFO_IGS_ENTRY].u4DebugOption | SNOOP_INSTANCE_INFO (gu4SnoopDbgInstId)->SnoopInfo[SNOOP_INFO_MLDS_ENTRY].u4DebugOption


#define  SNOOP_GBL_TRC(flg, mod, modname, fmt) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, mod, SNOOP_TASK_NAME, modname, fmt);\
 }\
}
#define  SNOOP_GBL_DBG(flg, mod, modname, fmt) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, mod, SNOOP_TASK_NAME, modname, fmt);\
       }\
}
#define  SNOOP_GBL_DBG_ARG1(flg, mod, modname, fmt, arg1) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, mod, SNOOP_TASK_NAME, modname, fmt, arg1);\
       }\
}
#define  SNOOP_GBL_DBG_ARG2(flg, mod, modname, fmt, arg1, arg2) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, mod, SNOOP_TASK_NAME, modname, fmt, arg1, arg2);\
       }\
}

#define  SNOOP_GBL_DBG_ARG3(flg, mod, modname, fmt, arg1, arg2, arg3) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, mod, SNOOP_TASK_NAME, modname, fmt, arg1, arg2, arg3);\
       }\
}

#define  SNOOP_GBL_DBG_ARG4(flg, mod, modname, fmt, arg1, arg2, arg3, arg4) \
{\
       if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)\
       {\
             UtlSysTrcLog(SYSLOG_INVAL_LEVEL,flg, mod, SNOOP_TASK_NAME, modname, fmt, arg1, arg2, arg3, arg4);\
       }\
}
#define  SNOOP_INIT_SHUT_TRC     INIT_SHUT_TRC     
#define  SNOOP_MANAGEMENT_TRC    MGMT_TRC          
#define  SNOOP_DATA_PATH_TRC     DATA_PATH_TRC     
#define  SNOOP_CONTROL_PATH_TRC  CONTROL_PLANE_TRC 
#define  SNOOP_DUMP_TRC          DUMP_TRC          
#define  SNOOP_OS_RESOURCE_TRC   OS_RESOURCE_TRC  
#define  SNOOP_OS_RES_TRC        SNOOP_OS_RES_DBG
#define  SNOOP_ALL_FAILURE_TRC   ALL_FAILURE_TRC
#define  SNOOP_BUFFER_TRC        SNOOP_OS_RES_DBG
#define  SNOOP_DUMPTx_TRC   DUMPTx_TRC
#define  SNOOP_DUMPRx_TRC  DUMPRx_TRC
#define  SNOOP_TMR_TRC           SNOOP_TMR_DBG
#endif /* SNPTRC_H */

