/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* $Id: snpred.h,v 1.29 2017/11/20 13:30:46 siva Exp $         */
/* Licensee Aricent Inc., 2005                              */
/*****************************************************************************/
/*    FILE  NAME            : snpred.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping Redundancy                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Definitions                 */
/*                            for snooping redundancy implementation         */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifndef _SNOOPRED_H_
#define _SNOOPRED_H_

#include "snp.h"
#include "rmgr.h"

/* Structure definition to contain the variables added to
 * support redundancy
 */
typedef struct _tSnoopRedGlobalInstInfo 
{
    tSnoopTmrNode  SnoopRedSwitchOverQueryTmr;    /* Timer which track the
                                                     software updation after
                                                     switch over. */
    UINT4                u4BulkUpdNextContext;   /*To store the next contextid 
                                                   for which sync-up infm needs
                                                   to be send*/
    tRBElem *            BulkUpdNextSnoopVlanEntry;
                                                 /* Pointer to the next Vlan
                                                    Entry for which bulk update
                                                    has to be generated */
    tRBElem *            BulkUpdNextSnoopMacGrpFwdEntry;
                                                 /* Pointer to the next Mac Fwd
                                                    Entry for which bulk update
                                                    has to be generated */
    tRBElem *            BulkUpdNextSnoopIpFwdEntry;
                                                 /* Pointer to the next IP Fwd
                                                    Entry for which bulk update
                                                    has to be generated */
    tRBElem *            BulkUpdNextSnoopRtrPortEntry; /* Pointer to the next 
                                                          rtr port entry for 
                                                          which bulk update
                                                          has to be generated. */
    tRBTree            SnoopRedMcastCacheTable;

    BOOL1                bBulkReqRcvd;           /* To check whether bulk req
                                                    msg recieved before
                                                    RM_STANDBY_UP event */
    BOOL1                bIsHwAuditReq; /* Is Hw Audit required. Used for MAC
                                           based IGS */
    UINT1                u1BulkUpdFlag;          /*This Flag is will be set when 
                                                   all the entires in VlanTable or 
                                                   GroupInfo Table were send to 
                                                   Standby for a particular context
                                                  */
    UINT1                u1RtrPortBulkUpdFlag;  /* This flag will be set when all
                                                   the router port entries are 
                                                   send across in bulk updated */
} tSnoopRedGlobalInfo;

typedef struct _SnoopRedMcastCacheNode
{
    tRBNodeEmbd    RbNode;  /* RbNode for the cache entry */
    tLocalPortList PortList;
    tLocalPortList UnTagPortList;
    UINT4          u4ContextId; /* Context Identifier */
    UINT4          u4GrpAddr; /* Context Identifier */
    UINT4          u4SrcAddr; /* Context Identifier */
    tSnoopVlanId   VlanId;  /* FDB Identifier */
    INT1           i1RedSyncState; /* Hardware status - Present in NP or not */
    UINT1          au1Pad[1];
}tSnoopRedMcastCacheNode;

/* Macro definition to use data structure and manipulate the same
 * for redundancy implemention. */
#define  SNOOP_RED_SW_OVER_QUERY_TMR_INTERVAL  30
#define  SNOOP_RED_SWITCHOVER_TMR_NODE()  \
            gSnoopRedGlobalInfo.SnoopRedSwitchOverQueryTmr
#define  SNOOP_RED_BULK_UPD_VLAN_ENTRY()  \
              gSnoopRedGlobalInfo.BulkUpdNextSnoopVlanEntry
                 
#define  SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY()  \
              gSnoopRedGlobalInfo.BulkUpdNextSnoopMacGrpFwdEntry

#define  SNOOP_RED_BULK_UPD_IP_FWD_ENTRY()  \
              gSnoopRedGlobalInfo.BulkUpdNextSnoopIpFwdEntry

#define  SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY()  \
              gSnoopRedGlobalInfo.BulkUpdNextSnoopRtrPortEntry
#define  SNOOP_RED_BULK_REQ_RECD()  gSnoopRedGlobalInfo.bBulkReqRcvd

#define  SNOOP_RED_BULK_UPD_NEXT_CONTEXTID()  \
              gSnoopRedGlobalInfo.u4BulkUpdNextContext

#define SNOOP_RED_BULK_UPD_FLAG    gSnoopRedGlobalInfo.u1BulkUpdFlag
#define SNOOP_RED_RTR_PORT_BULK_UPD_FLAG    \
              gSnoopRedGlobalInfo.u1RtrPortBulkUpdFlag

#define  SNOOP_RED_NO_OF_VLANS_PER_BULK_UPDATE             10
#define  SNOOP_RED_NO_OF_RTR_PORTS_PER_BULK_UPDATE         10
#define  SNOOP_RED_NO_OF_MAC_FWD_ENTRIES_PER_SUB_UPDATE    10
#define  SNOOP_RED_NO_OF_IP_FWD_ENTRIES_PER_SUB_UPDATE    10

#define SNOOP_NUM_STANDBY_NODES()      gu1NumPeerPresent

#define SNOOP_INIT_NUM_STANDBY_NODES() gu1NumPeerPresent = 0;

#define SNOOP_RED_IS_HW_AUDIT_REQ() (gSnoopRedGlobalInfo.bIsHwAuditReq)

#define SNOOP_IS_STANDBY_UP() \
                 ((gu1NumPeerPresent > 0) ? SNOOP_TRUE : SNOOP_FALSE)

#define SNOOP_RM_GET_NODE_STATUS()  RmGetNodeState ()

#define SNOOP_RM_GET_NUM_STANDBY_NODES_UP() gu1NumPeerPresent = RmGetStandbyNodeCount ()

#define SNOOP_RM_GET_STATIC_CONFIG_STATUS() SnoopRmGetStaticConfigStatus()
    
#define SNOOP_INIT_PROTOCOL_ADMIN_STATUS(u4InstId, u1AddressType) \
                 SNOOP_PROTOCOL_ADMIN_STATUS(u4InstId, u1AddressType) \
   = SNOOP_DISABLE;

/* SNOOP Redundancy sync up message types */
enum tSnoopMsgTypes {
    SNOOP_RED_MCAST_FORWARD_MODE = 1,
    IGS_RED_DEL_VLAN_SYNC,
    IGS_RED_RTR_PORT,
    IGS_RED_RTR_PORT_LIST,
    IGS_RED_GRP_INFO_MSG,
    IGS_RED_QUERIER_CONFIG,
    IGS_RED_IGMP_VERSION,
    IGS_RED_BULK_REQ_MSG,
    IGS_RED_BULK_UPD_TAIL_MSG,
    IGS_RED_RTR_PORT_INTR,
    IGS_RED_IP_INFO_MSG,
    IGS_RED_ADD_VLAN_SYNC,
    SNOOP_RED_SYNC_MCAST_CACHE_INFO,
    IGS_RED_STARTUP_QRY_COUNT_SYNC
};

/* SNOOP Bulk update sync up types */
typedef enum {
    SNOOP_RED_BULK_UPD_VLAN_REMOVE = 1,
    SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE,
    SNOOP_RED_BULK_UPD_RTR_PORT_REMOVE,
    SNOOP_RED_BULK_UPD_IP_FWD_REMOVE
} tSnoopBulkUpdSyncTypes;

#define SNOOP_MSG_FIELD_SIZE        1
#define SNOOP_SYNC_LEN_FIELD_SIZE   2
#define SNOOP_INST_ID_FIELD_SIZE    4
#define SNOOP_VLANID_FIELD_SIZE     sizeof(tSnoopTag)
#define SNOOP_PORT_FIELD_SIZE   sizeof (UINT4)
#define SNOOP_RED_RTR_PORT_LIST_MSG_LEN SNOOP_MSG_FIELD_SIZE + \
                                        SNOOP_SYNC_LEN_FIELD_SIZE + \
                                        SNOOP_INST_ID_FIELD_SIZE + \
                                        SNOOP_VLANID_FIELD_SIZE + \
                                        SNOOP_PORT_LIST_SIZE + \
                                        SNOOP_PORT_LIST_SIZE

#define SNOOP_RED_RTR_PORT_MSG_LEN SNOOP_MSG_FIELD_SIZE + \
                                   SNOOP_SYNC_LEN_FIELD_SIZE + \
                                   SNOOP_INST_ID_FIELD_SIZE + \
                                   SNOOP_VLANID_FIELD_SIZE + \
                                   SNOOP_PORT_FIELD_SIZE + 3

#define SNOOP_RED_RTR_INTR_MSG_LEN SNOOP_MSG_FIELD_SIZE + \
                                       SNOOP_SYNC_LEN_FIELD_SIZE + \
                                   SNOOP_INST_ID_FIELD_SIZE + \
                                   SNOOP_VLANID_FIELD_SIZE + \
                                   SNOOP_PORT_FIELD_SIZE + 5

#define SNOOP_RED_GRP_INFO_MSG_LEN SNOOP_MSG_FIELD_SIZE + \
                                   SNOOP_SYNC_LEN_FIELD_SIZE +\
                                   SNOOP_INST_ID_FIELD_SIZE + \
                                   SNOOP_VLANID_FIELD_SIZE + \
                                   sizeof (tIPvXAddr) +\
                                   SNOOP_PORT_LIST_SIZE +\
                                   SNOOP_PORT_LIST_SIZE

#define SNOOP_RED_IP_INFO_MSG_LEN SNOOP_MSG_FIELD_SIZE + \
                                   SNOOP_SYNC_LEN_FIELD_SIZE +\
                                   SNOOP_INST_ID_FIELD_SIZE + \
                                   SNOOP_VLANID_FIELD_SIZE + \
                                   sizeof (tIPvXAddr) +\
                                   sizeof (tIPvXAddr) +\
                                   sizeof (UINT4) +\
       SNOOP_PORT_LIST_SIZE

#define SNOOP_RED_QUER_MSG_LEN  SNOOP_MSG_FIELD_SIZE + \
                                SNOOP_INST_ID_FIELD_SIZE + \
                                SNOOP_VLANID_FIELD_SIZE + 5

#define SNOOP_RED_VER_MSG_LEN   SNOOP_MSG_FIELD_SIZE + \
                                SNOOP_INST_ID_FIELD_SIZE + \
                                SNOOP_VLANID_FIELD_SIZE + \
                                SNOOP_PORT_FIELD_SIZE + 1

#define SNOOP_RED_FRWD_MSG_LEN  SNOOP_MSG_FIELD_SIZE + \
                                SNOOP_INST_ID_FIELD_SIZE + 1

#define SNOOP_RED_DEL_VLAN_MSG_LEN  SNOOP_MSG_FIELD_SIZE + \
                                    SNOOP_INST_ID_FIELD_SIZE + \
                                    SNOOP_VLANID_FIELD_SIZE

#define SNOOP_RED_ADD_VLAN_MSG_LEN  SNOOP_MSG_FIELD_SIZE + \
                                    SNOOP_INST_ID_FIELD_SIZE + \
                                    SNOOP_VLANID_FIELD_SIZE



#define SNOOP_RED_STARTUP_QRY_MSG_LEN SNOOP_MSG_FIELD_SIZE + \
                                        SNOOP_INST_ID_FIELD_SIZE + \
                                        sizeof (UINT2) +\
                                        sizeof (UINT1)


#define SNOOP_RED_BULK_REQ_MSG_LEN   SNOOP_MSG_FIELD_SIZE

#define SNOOP_RED_BULK_UPD_TAIL_MSG_LEN  SNOOP_MSG_FIELD_SIZE 

/* Constant declaration */         
#define SNOOP_BULK_SPLIT_MSG_SIZE   1500
#define SNOOP_RM_OFFSET             0
#define SNOOP_RED_NONE              0xFF

#define SNOOP_RED_MCAST_IPADDR_BYTE1      224

#define SNOOP_RED_IP_FROM_MAC(IpAddr, MacAddr) \
{\
    IpAddr = (SNOOP_RED_MCAST_IPADDR_BYTE1 << 24) | \
             (MacAddr[3] << 16) | (MacAddr[4] << 8) | \
             (MacAddr[5]);\
}

/* Macros to write into RM buffer. */
#define SNOOP_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), (u4MesgType)); \
        *(pu4Offset) += 4;\
}while (0)

#define SNOOP_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), (u2MesgType)); \
        *(pu4Offset) += 2;\
}while (0)

#define SNOOP_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), (u1MesgType)); \
        *(pu4Offset) += 1;\
}while (0)

#define SNOOP_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET((pdest), (psrc), *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define SNOOP_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define SNOOP_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define SNOOP_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define SNOOP_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define SNOOP_RED_SYNC_HW_NOT_UPDATED  1
#define SNOOP_RED_SYNC_HW_UPDATED      2
#define SNOOP_RED_SYNC_SW_UPDATED      3

#define SNOOP_RED_MCAST_CACHE_INFO_SIZE  18 + (2*CONTEXT_PORT_LIST_SIZE);

/* SNOOP Redundancy implementation related function declaration */

INT4 SnoopRedHandleRmEvents (tSnoopMsgNode  *pMsgNode);
INT4 SnoopRedRegisterWithRM (VOID);
INT4 SnoopRedDeRegisterWithRM PROTO ((VOID));

INT4 SnoopRedMakeNodeActive (VOID);
INT4 SnoopRedMakeNodeActiveFromIdle (VOID);
INT4 SnoopRedMakeNodeActiveFromStandby (VOID);

INT4 SnoopRedMakeNodeStandbyFromIdle (VOID);
INT4 SnoopRedMakeNodeStandbyFromActive (VOID);

INT4 SnoopRedProcessConfigRestoreEvent (VOID);

INT4 SnoopRedActiveProcessBulkReq (VOID *pvPeerId);
INT4 SnoopRedVlanSnoopSyncBulkReq (UINT4 u4Instance);
INT4 SnoopRedRtrPortSnoopSyncBulkReq (VOID);
INT4 SnoopRedGrpInfoSyncBulkReq (UINT4 u4Instance);
INT4 SnoopActiveRedGrpBulkSync (UINT4 u4Instance);
INT4 SnoopActiveRedGrpSync (UINT4 u4InstId, tIPvXAddr SrcIpAddr, 
                            tSnoopGroupEntry * pSnoopGroupEntry, 
                            tSnoopPortBmp PortBitmap);
INT4 SnoopRedActiveSendRtrPortListSync (UINT4 u4InstId,
                                        tSnoopVlanEntry *pSnoopVlanEntry);

INT4
SnoopRedActiveSendRtrPortVerSync (UINT4 u4InstId, UINT4 u4Port, UINT1 u1Version,
                                   tSnoopVlanEntry *pSnoopVlanEntry);

INT4
SnoopRedActiveSendV3RtrPortIntrSync (UINT4 u4InstId, UINT4 u4Port, UINT4 u4Interval,
                                  tSnoopVlanEntry * pSnoopVlanEntry);

INT4 SnoopRedActiveSendGrpInfoSync (UINT4 u4InstId,
                                   tSnoopGroupEntry *pSnoopGroupEntry);

INT4 SnoopRedActiveSendQuerierSync (UINT4 u4InstId, tSnoopTag VlanId, 
                                   UINT1 u1Querier, UINT1 u1AddressType, UINT4 u4QuerierIP);
INT4 SnoopRedActiveSendVerSync (UINT4 u4InstId, tSnoopTag VlanId, 
                                 UINT1 u1OperVersion, UINT1 u1AddressType,
                                 UINT4 u4Port);
INT4 SnoopRedActiveSendForwardModeSync (UINT4 u4InstId, UINT1 u1McastFwdMode);
INT4 SnoopRedActiveSendDelVlanSync (UINT4 u4InstId, tSnoopTag VlanId,
                                    UINT1 u1AddressType);
INT4 SnoopRedHandleDelVlanSync (UINT4 u4Instance, tSnoopTag VlanId,
                                UINT1 u1MsgType);
INT4
SnoopRedHandleAddVlanSync (UINT4 u4Instance, tSnoopTag VlanId);

INT4
SnoopRedActiveSendAddVlanSync (UINT4 u4InstId, tSnoopTag VlanId, UINT1 u1AddressType);

INT4 SnoopRedStandbySendBulkReq (VOID);

INT4 SnoopRedProcessSyncUpdate (tRmMsg *pMsg, UINT2 u2Length);
INT4 SnoopRedUpdateRtrPortListSync (UINT4 u4Instance, tSnoopTag VlanId, 
                               tSnoopPortBmp RtrPortList, 
                               tSnoopPortBmp IgmpRtrPortList,
          UINT1 u1MsgType);

INT4
SnoopRedUpdateRtrPortVerSync (UINT4 u4Instance, tSnoopTag VlanId,
                           UINT4 u4Port, UINT1 u1AddressType, UINT1 u1Version,
                           UINT1 u1RtrPortType);

INT4
SnoopRedUpdateRtrPortIntervalSync (UINT4 u4Instance, tSnoopTag VlanId,
                              UINT4 u4Port, UINT1 u1AddressType,
                              UINT4 u4Interval);

INT4
SnoopRedUpdateMemPortSync (UINT4 u4Instance, tSnoopTag VlanId,
                           tIPvXAddr SnoopGrpIpAddr,
                           tSnoopPortBmp FwdPortList,
                           tSnoopPortBmp V1FwdPortList,
                           UINT1 u1MsgType);
INT4
SnoopRedApplyFwdListOnExistingGrp (UINT4 u4Instance,
                                   tSnoopGroupEntry *pSnoopGroupEntry,
                                   tSnoopPortBmp FwdPortList,
                                   tSnoopPortBmp V1FwdPortList);

INT4
SnoopRedApplyFwdListOnNewGrp (UINT4 u4Instance, tSnoopTag VlanId,
                              tIPvXAddr SnoopGrpIpAddr,
                              tSnoopPortBmp FwdPortList,
                              tSnoopPortBmp V1FwdPortList);
INT4
SnoopRedSyncFwdListOnNewGrp (UINT4 u4Instance, tSnoopTag VlanId,
                              tIPvXAddr SnoopGrpIpAddr,
                              tIPvXAddr SnoopSrcIpAddr,
                              tSnoopPortBmp FwdPortList, UINT4 u4SnoopHwId);
INT4
SnoopSyncIpFwdTable (UINT4 u4Instance, tSnoopTag VlanId,
                           tIPvXAddr SnoopGrpIpAddr,
                           tIPvXAddr SnoopSrcIpAddr,
                           tSnoopPortBmp FwdPortList, UINT4 u4SnoopHwId);

INT4 SnoopRedUpdateQuerierSync (UINT4 u4Instance, tSnoopTag VlanId, 
                                UINT1 u1Querier, UINT1 u1MsgType,
                                UINT4 u4QuerierAddress);
INT4 SnoopRedUpdateMcastFwdModeSync (UINT4 u4Instance, 
                                    UINT1 u1McastFwdMode);

INT4 SnoopRedGrpDeletePortEntry (UINT4 u4Instance, tSnoopTag VlanId, 
                               UINT4 u4Port, tMacAddr McastAddr);

INT4 SnoopRedSwitchOver (UINT4 u4Instance, UINT1 u1AddressType); 

INT4 SnoopRedAudit(VOID);
INT4 SnoopRedHandleMcastAuditSyncMsg (tSnoopMsgNode * pMsgNode);

INT4 SnoopRedShowDebuggingInfo (tCliHandle CliHandle);
INT4 SnoopRedShowForwardEntries (tCliHandle CliHandle, UINT4 u4Context, 
                    tSnoopTag VlanId,
                    UINT1 u1AddressType);
INT4 SnoopRedShowVlanRouterPorts (tCliHandle CliHandle, UINT4 u4Context,
                    tSnoopTag VlanId,
                     UINT1 u1AddressType,
                                   INT4 i4PrintRtrPortInfo);
INT4 SnoopRedShowVlanInfo (tCliHandle CliHandle, UINT4 u4Context, 
              tSnoopTag VlanId,
               UINT1 u1AddressType);
VOID SnoopRedHandleBulkUpdEvent (VOID);
VOID SnoopRedSyncBulkUpdNextEntry (UINT4 u4Instance, tRBElem * pSnoopEntry,
                               tSnoopBulkUpdSyncTypes SnoopBulkUpdSyncType);
INT4 SnoopRedSendBulkUpdTailMsg (VOID);
VOID SnoopRedTrigHigherLayer (UINT1 u1TrigType);
VOID SnoopRedSwitchOverQueryTmrExpiry PROTO ((VOID));
VOID SnoopRedStartIPMCAudit (VOID);
VOID SnoopRedAuditTaskMain (VOID);
VOID SnoopRedSendGeneralQuery (UINT4 u4Instance, UINT1 u1AddressType);
VOID SnoopRedSendConsolidatedReport (UINT4 u4Instance, UINT1 u1AddressType);
VOID SnoopRedSendConsolidatedReportOnRtrPorts (UINT4 u4Instance, 
                                               tSnoopVlanEntry *pSnoopVlanEntry);
INT4
SnoopRedHwUpdateMcastSync (UINT4 u4Instance, tSnoopVlanId VlanId,
                           UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                           tPortList PortList, tPortList UnTagPortList, UINT1 i1RedSyncState);

VOID
SnoopRedProcessMcastCacheInfo (tRmMsg * pMsg, UINT4 u4ContextId,
                               UINT4 *pu4OffSet);

INT4
SnoopRedProcessMcastSyncInfo (tSnoopRedMcastCacheNode * pUpdtSnoopRedCacheNode);

tSnoopRedMcastCacheNode *
SnoopRedGetMcastCacheEntry (UINT4 u4ContextId,
                            tVlanId VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr);

INT4
SnoopRedInitGlobalInfo (VOID);

INT4
SnoopRedAddMcastCacheEntry (tSnoopRedMcastCacheNode * pUpdtSnoopRedCacheNode);

VOID
SnoopRedSyncPendingMcastEntries (UINT4 u4Context);

INT4
SnoopUtilRBTreeRedFwdEntryFree (tRBElem * IpRedEntryNode, UINT4 u4Arg);

INT4
SnoopActiveRedGrpDeleteSync (UINT4 u4Instance, tIPvXAddr SrcIpAddr, tSnoopGroupEntry * pSnoopGroupEntry);

INT4
SnoopRedSyncStartUpQryCount (UINT4 u4InstId, tSnoopVlanEntry *pSnoopVlanEntry);

INT4
SnoopRedUpdateStartUpQryCount (UINT4 u4Instance, tSnoopTag VlanId,
                                UINT1 u1StartUpQryCount);

#endif 
