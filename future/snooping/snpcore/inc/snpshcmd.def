/*****************************************************************************/
/*    FILE  NAME            : snpshcmd.def                                   */
/* $Id: snpshcmd.def,v 1.25 2017/10/09 12:07:58 siva Exp $                   */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping(IGMP/MLD) CLI commands                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains CLI commands specific to    */
/*                            for snooping(MLD/IGMP) module                  */
/*---------------------------------------------------------------------------*/
DEFINE GROUP: SNOOP_EXEC_CMDS

/* EXEC commands */
   /* NOTE: For EXEC mode commands, we have to pass the context-name/NULL 
    * as 4th argument for cli_process_snoop_show_cmd function always.*/

#ifdef IGS_WANTED
#if ! defined (L2RED_WANTED)
   
   COMMAND : show ip igmp snooping mrouter [Vlan <vlan_vfi_id>] [detail]
             [switch <string(32)>]
   ACTION  : {
                INT4 i4PrintRtrPort = SNOOP_CLI_SHOW_RTR_PORT;

                if ($7 != NULL)
                {
                   i4PrintRtrPort = SNOOP_CLI_SHOW_ALL_RTR_PORT_INFO;
                }

                if ($5 != NULL)
                {
                   cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_VLAN_RTR_PORTS, 
                                                 NULL, $9, $6, i4PrintRtrPort);
                }
                else
                {
                   cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_VLAN_RTR_PORTS, 
                                                     NULL, $9, NULL, i4PrintRtrPort);
                }
             }
   SYNTAX  : show ip igmp snooping mrouter [Vlan <vlan-id/vfi-id>] [detail]
             [switch <switch_name>]
   PRVID   : 1
   HELP    : Display the router ports for all VLANs or a specific VLAN for a 
             given switch or for all the switch (if no switch is specified),
             The optional switch is not applicable for SI case.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ip IP related protocol information|
              igmp IGMP related information|
              snooping Snooping related information|
              mrouter MLD router related information|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              detail Detailed information|
              switch Protocol specific information for switch |
              DYNswitchname|
              <CR> Display the router ports for all VLANs or a specific VLAN for a given switch or for all the switch (if no switch is specified),The optional switch is not applicable for SI case.


#else

   COMMAND : show ip igmp snooping mrouter [Vlan <vlan_vfi_id>] [redundancy] [detail]  [switch <string (32)>] 
   ACTION  : {
                INT4 i4PrintRtrPort = SNOOP_CLI_SHOW_RTR_PORT;

                if ($8 != NULL)
                {
                   i4PrintRtrPort = SNOOP_CLI_SHOW_ALL_RTR_PORT_INFO;
                }

                if ($7 != NULL)
                {
                    if ($5 != NULL)
                    {
                       cli_process_snoop_show_cmd (CliHandle,
                       IGS_RED_SHOW_VLAN_RTR_PORTS, NULL, $10, $6, i4PrintRtrPort);
                    }
                    else
                    {
                       cli_process_snoop_show_cmd (CliHandle,
                       IGS_RED_SHOW_VLAN_RTR_PORTS, NULL, $10, NULL, i4PrintRtrPort);
                    }
                }
                else
                {
                    if ($5 != NULL)
                    {
                       cli_process_snoop_show_cmd (CliHandle,
                       IGS_SHOW_VLAN_RTR_PORTS, NULL, $10, $6, i4PrintRtrPort);
                    }
                    else
                    {
                       cli_process_snoop_show_cmd (CliHandle,
                       IGS_SHOW_VLAN_RTR_PORTS, NULL, $10, NULL, i4PrintRtrPort);
                    }
                }
             }
             
   SYNTAX  : show ip igmp snooping mrouter [Vlan <vlan-id/vfi-id>] [redundancy]
             [detail] [switch <switch_name>]
   PRVID   : 1
   HELP    : Display the router ports for all VLANs or a specific VLAN for 
             a given switch or for all switch (if no switch is specified).
             The optional switch name is not applicable for SI.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ip IP related details for the protocol|
              igmp IGMP related information|
              snooping Snooping related information|
              mrouter MLD router related information|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              redundancy Synced Messages|
              detail Detailed information|
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Display the router ports for all VLANs or a specific VLAN for a given switch or for all switch (if no switch is specified).The optional switch name is not applicable for SI.

#endif


   COMMAND : show ip igmp snooping blocked-router [Vlan <vlan_vfi_id>] [switch <string(32)>]
   ACTION  : {
                 if ($5 != NULL)
                 {
                     cli_process_snoop_show_cmd (CliHandle,
                                               IGS_SHOW_VLAN_BLOCKED_RTR_PORTS,                                                                                                 NULL, $8, $6);
                 }
                 else
                 {
                     cli_process_snoop_show_cmd (CliHandle,
                                                IGS_SHOW_VLAN_BLOCKED_RTR_PORTS,
                                                 NULL, $8, NULL);
                 }
             }
    SYNTAX  : show ip igmp snooping blocked-router [Vlan <vlan-id/vfi-id>] [switch <switch_name>]
    PRVID   : 1
    HELP    : Display the blocked router ports for all VLANs or a specific VLAN
              for a given switch or for all the switch (if no switch is 
              specified).The optional switch is not applicable for SI case.
    CXT_HELP : show Displays the configuration/statistics/general information|
               ip IP related details for the protocol|
               igmp IGMP related information|
               snooping Snooping related information|
               blocked-router Blocked router ports related information|
               vlan Protocol specific information for vlan|
               vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
               switch Protocol specific information for switch|
               DYNswitchname|
               <CR> Display the blocked router ports for all VLANs or a specific VLAN for a given switch or for all the switch (if no switch is specified).The optional switch is not applicable for SI case.
              

   COMMAND : show ip igmp snooping globals [switch <string (32)>]
   ACTION  : cli_process_snoop_show_cmd (CliHandle, IGS_SHOW_GLOBALS_INFO, NULL, $6);
   SYNTAX  : show ip igmp snooping globals [switch  <switch_name>]
   PRVID   : 1
   HELP    :  Display IGMP snooping global information for a given switch or for all switches (if switch is not specified),the optional switch name is not applicable for SI case.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ip IP related details for the protocol|
              igmp IGMP related information|
              snooping Snooping related information|
              globals Global IGMP/MLD snooping information|
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Display IGMP snooping information for all VLANs or a specific VLAN for a given switch or for all switches (if switch is not specified),The optional switch name is not applicable for SI case.

#if ! defined (L2RED_WANTED)
   COMMAND : show ip igmp snooping [Vlan <vlan_vfi_id>]
             [switch <string (32)>]
   ACTION  : {
                if ($4 != NULL)
                {
                  cli_process_snoop_show_cmd (CliHandle, IGS_SHOW_VLAN_INFO, 
                                                                 NULL, $7, $5);
                }
                else
                {
                  cli_process_snoop_show_cmd (CliHandle, IGS_SHOW_VLAN_INFO, 
                                                       NULL, $7, NULL);
                }
             }
   SYNTAX  : show ip igmp snooping [Vlan <vlan-id/vfi-id>] [switch <switch_name>]
   PRVID   : 1
   HELP    : Display IGMP snooping information for all VLANs or a specific VLAN for a given context or for all the context (if no switch is specified). The optional switch name is not applicable for SI.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ip IP related details for the protocol|
              igmp IGMP related information|
              snooping Snooping related information|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Display IGMP snooping information for all VLANs or a specific VLAN for a given context or for all the context (if no switch is specified). The optional switch name is not applicable for SI.

#else

   COMMAND : show ip igmp snooping [Vlan <vlan_vfi_id>] [redundancy]
             [switch <string (32)>]
   ACTION  : {
                if ($6 != NULL)
                {
                    if ($4 != NULL)
                    {
                        cli_process_snoop_show_cmd (CliHandle, 
                                             IGS_RED_SHOW_VLAN_INFO, 
                                             NULL, $8, $5);
                    }
                    else
                    {
                        cli_process_snoop_show_cmd (CliHandle, 
                                             IGS_RED_SHOW_VLAN_INFO, 
                                             NULL, $8, NULL);
                    }
                }
                else
                {
                    if ($4 != NULL)
                    {
                        cli_process_snoop_show_cmd (CliHandle, IGS_SHOW_VLAN_INFO, 
                                                             NULL, $8, $5);
                    }
                    else
                    {
                        cli_process_snoop_show_cmd (CliHandle, IGS_SHOW_VLAN_INFO, 
                                                            NULL, $8, NULL);
                    }
                }
             }
   SYNTAX  : show ip igmp snooping [Vlan <vlan-id/vfi-id>] [redundancy]
             [switch <switch_name>]
   PRVID   : 1
   HELP    : Display IGMP snooping information for all VLANs or a specific VLAN,for a  given switch or for all switches (if no switch is specified),In SI case the optional switch is not supported.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ip IP related details for the protocol|
              igmp IGMP related information|
              snooping Snooping related information|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              redundancy Synced Messages|
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Display IGMP snooping information for all VLANs or a specific VLAN,for a  given switch or for all switches (if no switch is specified),In SI case the optional switch is not supported.

#endif

   COMMAND :  show ip igmp snooping groups [{[[Vlan <vlan_vfi_id>
             [Group <mcast_addr>]] [{static | dynamic}]
             [switch <string (32)>]] | [summary]}]
    ACTION  : {
        if ($13 != NULL)
        {
            cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_GRP_ENTRIES,
                    NULL, $12, NULL, SNOOP_CLI_SHOW_SUMMARY,NULL , NULL);
        }
        else
        {
            if ($5 != NULL)
            {
                if ($7 != NULL)
                {
                    cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_GRP_ENTRIES, 
                            NULL, $12, $6, $8, $9, $10);
                }
                else
                {
                    cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_GRP_ENTRIES, 
                            NULL, $12, $6, NULL, $9, $10);
                }
            }
            else
            {
                cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_GRP_ENTRIES, 
                        NULL, $12, NULL, NULL, $9, $10);
            }
        }
    }
   SYNTAX  :  show ip igmp snooping groups [{[[Vlan <vlan_vfi_id>
             [Group <mcast_addr>]] [{static | dynamic}]
             [switch <string (32)>]] | [summary]}]
   PRVID   : 1
   HELP    : Displays IGMP group information for all VLANs or a specific VLAN
             or specific VLAN and group address for a given switch or for all
             switch (if no switch is specified). The optional switch name is not
             applicable for Single Instance case.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ip IP related details for the protocol|
              igmp IGMP related information|
              snooping Snooping related information|
              groups MLDS group information|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              Group Group address of the vlan id|
              A.B.C.D(<mcast_addr>) Multicast group address|
              static Displays only static multicast entries|
              dynamic Displays only dynamic multicast entries| 
              switch Protocol specific information for switch|
              DYNswitchname|
              summary Summary of IGS Group entries|
             <CR> Displays IGMP group information for all VLANs or a specific VLAN or specific VLAN and group address for a given switch or for all switch (if no switch is specified). The optional switch name is not
               

   COMMAND : show ip igmp snooping multicast-receivers [Vlan <vlan_vfi_id> 
             [Group <mcast_addr>]] [switch <string (32)>]
   ACTION  : {
                if ($5 != NULL)
                {
                   if ($7 != NULL)
                   {
                      cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_HOST_ENTRIES,
                                                        NULL, $10, $6, $8);
                   }
                   else
                   {
                      cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_HOST_ENTRIES, 
                                                       NULL, $10, $6, NULL);
                   }
                }
                else
                {
                   cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_HOST_ENTRIES, 
                                                 NULL, $10, NULL, NULL);
                }
             }
   SYNTAX  : show ip igmp snooping multicast-receivers [Vlan <vlan-id/vfi-id> [Group <Address>]]
             [switch <switch_name>]
   PRVID   : 1
   HELP    : Displays IGMP multicast host information for all VLANs or a specific vlan or specific VLAN and group address for a given switch or for all switches (if no switch is specified). The optional switch name is not applicable for single Instance case.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ip IP related details for the protocol|
              igmp IGMP related information|
              snooping Snooping related information|
              multicast-receivers Displays multicast receivers|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              Group Group address of the vlan id|
              A.B.C.D(<mcast_addr>) Multicast group address|
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Displays IGMP multicast host information for all VLANs or a specific vlan or specific VLAN and group address for a given switch or for all switch (if no switch is specified). The optional switch name is not applicable for single Instance case.

COMMAND : show ip igmp snooping port-cfg [{interface <ifXtype> <ifnum> [InnerVlanId <short(1-4094)>] | switch <string (32)>}]
  ACTION  : {
                UINT4    u4Port = 0;
                if ($5 != NULL)
                {
                    if (CfaCliGetIfIndex ($6, $7, &u4Port) == CLI_FAILURE)    
                    {
                         CliPrintf (CliHandle, "%% Invalid Interface \r\n");
                         return (CLI_FAILURE);
                    }
                }

                cli_process_snoop_show_cmd (CliHandle,SNOOP_SHOW_PORT_CFG_ENTRIES, 
                                            u4Port, $11, $9);
             }
   SYNTAX  : show ip igmp snooping port-cfg [{interface <interface-type> <interface-id> [InnerVlanId vlan-id(1-4094)] | switch <switch_name>}]
   PRVID : 1
   HELP    : Displays IGS Port configuration information for all Inner VLANs 
             or  a specific Inner VlanId or a given switch.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ip IP related details for the protocol|
              igmp IGMP related information|
              snooping Snooping related information|
              port-cfg IGS port configuration related information|
              interface Interface type and identifier|
              DYNiftype|
              DYNifnum|
              InnerVlanId Inner vlan identifier|
              (1-4094) Vlan index|
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Displays IGS Port configuration information for all Inner VLANs or  a specific Inner VlanId or a given switch.
#if ! defined (L2RED_WANTED)

   COMMAND : show ip igmp snooping forwarding-database [{[[Vlan <vlan_vfi_id>] 
       [{static | dynamic}]] | [summary]}] [switch <string (32)>]
    ACTION  : {
        if ( $9 != NULL)
        {
            cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_FWD_ENTRIES,
                    NULL, NULL, NULL, NULL,SNOOP_CLI_SHOW_SUMMARY , NULL);
        }
        else
        {

            if ($5 != NULL)
            {
                cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_FWD_ENTRIES, 
                        NULL, $11, $6, $7, $8);
            }
            else
            {
                cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_FWD_ENTRIES, 
                        NULL, $11, NULL, $7, $8);
            }
        }
    }
   SYNTAX  : show ip igmp snooping forwarding-database [{[[Vlan <vlan_vfi_id>] 
       [{static | dynamic}]] | [summary]}] [switch <string (32)>]
   PRVID   : 1
   HELP    : Displays multicast forwarding entries for all VLANs or a
             specific VLAN for a given switch or for all switch (if no
             switch is specified). The optional switch name is not 
             applicable for Single Instance case.
  CXT_HELP : show Displays the configuration/statistics/general information|
             ip IP related details for the protocol|
             igmp IGMP related information|
             snooping Snooping related information|
             forwarding-database Multicast forwarding entries|
             vlan Protocol specific information for vlan|
             vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
             static Displays only static multicast entries|
             dynamic Displays only dynamic multicast entries|
             summary Summary of Multicast forwarding entries|
             switch Protocol specific information for switch|
             DYNswitchname|
             <CR> Displays multicast forwarding entries for all VLANs or a specific VLAN for a given switch or for all switch (if no switch is specified). The optional switch name is not applicable for Single Instance case.

#else

             COMMAND : show ip igmp snooping forwarding-database [{[[Vlan <vlan_vfi_id>] 
                 [{static | dynamic}] [redundancy]] | [summary]}] [switch <string (32)>]
    ACTION  : {
        if ( $10 != NULL)
        {
            cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_FWD_ENTRIES,
                    NULL, NULL, NULL, NULL,SNOOP_CLI_SHOW_SUMMARY , NULL);                                                                             }
        else
        {

            if ($9 != NULL)
            {
                if ($5 != NULL)
                {
                    cli_process_snoop_show_cmd (CliHandle, 
                            IGS_RED_SHOW_FWD_ENTRIES, 
                            NULL, $12, $6, $7, $8);
                }
                else
                {
                    cli_process_snoop_show_cmd (CliHandle,
                            IGS_RED_SHOW_FWD_ENTRIES, 
                            NULL, $12, NULL, $7, $8);
                }
            }
            else
            {
                if ($5 != NULL)
                {
                    cli_process_snoop_show_cmd (CliHandle,
                            IGS_SHOW_FWD_ENTRIES, 
                            NULL, $12, $6, $7, $8);
                }
                else
                {
                    cli_process_snoop_show_cmd (CliHandle,
                            IGS_SHOW_FWD_ENTRIES, 
                            NULL, $12, NULL, $7, $8);
                }
            }
        }
    }

   SYNTAX  : show ip igmp snooping forwarding-database [{[[Vlan <vlan_vfi_id>]
                 [{static | dynamic}] [redundancy]] | [summary]}] [switch <string (32)>]
   PRVID   : 1
   HELP    : Displays multicast forwarding entries for all VLANs or a
             specific VLAN for a given switch or for all switch (if no 
             switch is specified). The optional switch name is not 
             applicable for Single Instance case.
  CXT_HELP : show Displays the configuration/statistics/general information|
             ip IP related details for the protocol|
             igmp IGMP related information|
             snooping Snooping related information|
             forwarding-database Multicast forwarding entries|
             vlan Protocol specific information for vlan|
             vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
             static Displays only static multicast entries|
             dynamic Displays only dynamic multicast entries|
             redundancy Synced messages|
             summary Summary of Multicast forwarding entries|
             switch Protocol specific information for switch|
             DYNswitchname|
             <CR> Displays multicast forwarding entries for all VLANs or a specific VLAN for a given switch or for all switch (if no switch is specified). The optional switch name is not applicable for Single Instance case.

#endif
            
   COMMAND : show ip igmp snooping statistics [Vlan <vlan_vfi_id>] [switch <string (32)>]
   ACTION  : {
                if ($5 != NULL)
                {
                   cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_STATS, NULL, 
                                          $8, $6);
                }
                else
                {
                   cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_STATS, NULL, 
                                                       $8, NULL);

                }
             }
   SYNTAX  : show ip igmp snooping statistics [Vlan <vlan-id/vfi-id>] [switch <switch_name>]
   PRVID   : 1
   HELP    : Displays IGMP snooping statistics for all VLANs or a 
             specific VLAN  for a given switch or for all
             switch (if no switch is specified). The optional switch 
             name is not applicable for Single Instance case.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ip IP related details for the protocol|
              igmp IGMP related information|
              snooping Snooping related information|
              statistics Snooping related statistics|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Displays IGMP snooping statistics for all VLANs or a specific VLAN  for a given switch or for all switch (if no switch is specified).The optional switch name is not applicable for Single Instance case.

   COMMAND : show ip igmp snooping multicast-vlan [switch <string (32)>]
   ACTION  : {
                 cli_process_snoop_show_cmd (CliHandle,IGS_SHOW_MVLAN_MAPPING, 
                                             NULL, $6, NULL);
             }
   SYNTAX  : show ip igmp snooping multicast-vlan [switch <switch_name>]
   PRVID   : 1
   HELP    : Displays Multicast VLAN statistics in a switch. Also displays
             various profiles mapped to the Multicast VLANs.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ip IP related details for the protocol|
              igmp IGMP related information|
              snooping Snooping related information|
              multicast-vlan Multicast vlan related statistics|
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Displays Multicast VLAN statistics in a switch. Also displaysvarious profiles mapped to the Multicast VLANs.
   
   COMMAND : ip igmp snooping clear counters [Vlan <vlan_vfi_id>]
   ACTION  : {
                 if ($5 != NULL)
                 {
                     cli_process_snoop_cmd (CliHandle, IGS_VLAN_STATS_CLEAR_COUNTERS,
                                                                          NULL, $6);
                 }
                 else
                 {
                     cli_process_snoop_cmd (CliHandle, IGS_VLAN_STATS_CLEAR_COUNTERS,                                                                              NULL, NULL);
                 }
            }
   SYNTAX  : ip igmp snooping clear counters [Vlan <vlanid/vfi_id>]
   PRVID   : 1
   HELP    : Clears the IGMP snooping statistics maintained for  Vlan(s)
   CXT_HELP : ip Configures IP related protocol |
              igmp IGMP related configuration |
              snooping Snooping related configuration |
              clear Performs clear operation |
              counters Counter related information |
              Vlan VLAN related configuration |
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              <CR> Clears the IGMP snooping statistics maintained for  Vlan(s)

#endif

#ifdef MLDS_WANTED

   COMMAND : show ipv6 mld snooping mrouter [Vlan <vlan_vfi_id>] [detail] [switch <string (32)>]
   ACTION  : {
                INT4 i4PrintRtrPort = SNOOP_CLI_SHOW_RTR_PORT;
                if ($7 != NULL)
                {
                   i4PrintRtrPort = SNOOP_CLI_SHOW_ALL_RTR_PORT_INFO;
                }
                if ($5 != NULL)
                {
                   cli_process_snoop_show_cmd (CliHandle,MLDS_SHOW_VLAN_RTR_PORTS, 
                                                            NULL, $9, $6, i4PrintRtrPort);
                }
                else
                {
                   cli_process_snoop_show_cmd (CliHandle,MLDS_SHOW_VLAN_RTR_PORTS, 
                                                              NULL, $9, NULL, i4PrintRtrPort);
                }
             }
SYNTAX  : show ipv6 mld snooping mrouter [Vlan <vlan-id/vfi-id>] [detail] [switch <switch_name>]
   PRVID   : 1
   HELP    : Display the router ports for all VLANs or a specific VLAN for
             a given switch or for all switch (if no switch is specified).
             The optional switch name is not applicable for Single Instance 
             case. 
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPv6 related details for the protocol|
              mld Multicast listener discovery reinformation|
              snooping Snooping related information|
              mrouter MLD router related information|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              detail Detailed information|
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Display the router ports for all VLANs or a specific VLAN fora given switch or for all switch (if no switch is specified).The optional switchname is not applicable for Single Instance case. 

   COMMAND : show ipv6 mld snooping globals [switch <string (32)>]
   ACTION  : cli_process_snoop_show_cmd (CliHandle, MLDS_SHOW_GLOBALS_INFO, NULL, $6);
   SYNTAX  : show ipv6 mld snooping globals [switch <switch_name>]
   PRVID   : 1
   HELP    : Display MLD snooping information for all VLANs or a specific VLAN 
             for a given switch or for all switch (if no switch is specified).
             The optional switch name is not applicable for Single 
             Instance case.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPv6 related details for the protocol|
              mld Multicast listener discovery related information|
              snooping Snooping related information|
              globals Global MLD snooping information|
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Display MLD snooping information for all VLANs or a specific VLAN for a given switch or for all switch (if no switch is specified).The optional switch name is not applicable for Single Instance case.


   COMMAND : show ipv6 mld snooping [Vlan <vlan_vfi_id>] [switch <string (32)>]
   ACTION  : {
                if ($4 != NULL)
                {
                  cli_process_snoop_show_cmd (CliHandle, MLDS_SHOW_VLAN_INFO, 
                                                          NULL, $7, $5);
                }
                else
                {
                  cli_process_snoop_show_cmd (CliHandle, MLDS_SHOW_VLAN_INFO, 
                                                        NULL, $7, NULL);
                }
             }
   SYNTAX  : show ipv6 mld snooping [Vlan <vlan-id/vfi-id>] [switch <switch_name>]
   PRVID   : 1
   HELP    : Display MLD snooping information for all VLANs or a specific VLAN
             for a given switch or for all switch (if no switch is specified). 
             The optional switch name is not applicable for Single Instance
             case.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPv6 related details for the protocol|
              mld Multicast listener discovery related information|
              snooping Snooping related information|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Display MLD snooping information for all VLANs or a specific VLAN for a given switch or for all switch (if no switch is specified). The optional switch name is not applicable for Single Instance case.


   COMMAND : show ipv6 mld snooping groups [Vlan <vlan_vfi_id> [Group <ip6_addr>]] [switch <string (32)>]
   ACTION  : {
                if ($5 != NULL)
                {
                   if ($7 != NULL)
                   {
                      cli_process_snoop_show_cmd (CliHandle,MLDS_SHOW_GRP_ENTRIES, 
                                                           NULL, $10, $6, $8);
                   }
                   else
                   {
                      cli_process_snoop_show_cmd (CliHandle,MLDS_SHOW_GRP_ENTRIES, 
                                                          NULL, $10, $6, NULL);
                   }
                }
                else
                {
                   cli_process_snoop_show_cmd (CliHandle,MLDS_SHOW_GRP_ENTRIES, 
                                                   NULL, $10, NULL, NULL);
                }
             }
   SYNTAX  : show ipv6 mld snooping groups [Vlan <vlan-id/vfi-id> [Group <Address>]] [switch <string (32)>]
   PRVID   : 1
   HELP    : Displays MLDS group information for all VLANs or a specific VLAN
             or a specific VLAN and group address for a given switch or for 
             all switch (if no switch is specified). The optional switch name 
             is not applicable for Single Instance case.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPv6 related details for the protocol|
              mld Multicast listener discovery related information|
              snooping Snooping related information|
              groups MLDS group information|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              Group Group address of the vlan id|
              AAAA::BBBB IPV6 address|
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Displays MLDS group information for all VLANs or a specific VLAN or a specific VLAN and group address for a given switch or for all switch (if no switch is specified). The optional switch name is not applicable for Single Instance case.
 
   COMMAND : show ipv6 mld snooping forwarding-database [Vlan <vlan_vfi_id>] [switch <string (32)>]
   ACTION  : {
                if ($5 != NULL)
                {
                   cli_process_snoop_show_cmd (CliHandle,MLDS_SHOW_FWD_ENTRIES, 
                                                             NULL, $8, $6);
                }
                else
                {
                   cli_process_snoop_show_cmd (CliHandle,MLDS_SHOW_FWD_ENTRIES, 
                                                         NULL, $8, NULL);
                }
             }
   SYNTAX  : show ipv6 mld snooping forwarding-database [Vlan <vlan-id/vfi-id>] [switch <switch_name>]
   PRVID   : 1
   HELP    : Displays multicast forwarding entries for all VLANs or a
             specific VLAN for a given switch or for all switch (if no 
             switch is specified). The optional switch name is not applicable
             for Single Instance case.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPv6 related details for the protocol|
              mld Multicast listener discovery related information|
              snooping Snooping related information|
              forwarding-database Multicast forwarding entries|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Displays multicast forwarding entries for all VLANs or a
specific VLAN for a given switch or for all switch (if no switch is specified). The optional switch name is not applicable for Single Instance case.

   COMMAND : show ipv6 mld snooping statistics [Vlan <vlan_vfi_id>] 
 [switch <string (32)>]
   ACTION  : {
                if ($5 != NULL)
                {
                   cli_process_snoop_show_cmd (CliHandle,MLDS_SHOW_STATS, NULL,
                                          $8, $6);
                }
                else
                {
                   cli_process_snoop_show_cmd (CliHandle,MLDS_SHOW_STATS, NULL, 
                                                               $8, NULL);

                }
             }
   SYNTAX  : show ipv6 mld snooping statistics [Vlan <vlan-id/vfi-id>] [switch <string (32)>]
   PRVID   : 1
   HELP    : Displays MLD snooping statistics for all VLANs or a 
             specific VLAN for a given switch or for all switch (if no 
             switch is specified). The optional switch_name is not 
             applicable for Single Instance case.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPv6 related details for the protocol|
              mld Multicast listener discovery related information|
              snooping Snooping related information|
              statistics Mld related statistics|
              vlan Protocol specific information for vlan|
              vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
              switch Protocol specific information for switch|
              DYNswitchname|
              <CR> Displays MLD snooping statistics for all VLANs or a specific VLAN for a given switch or for all switch (if no switch is specified). The optional switch_name is not applicable for Single Instance case.

#endif

END GROUP        
