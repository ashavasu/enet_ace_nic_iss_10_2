/*****************************************************************************/
/* Copyright (C) 2015 Aricent Inc . All Rights Reserved     */
/*  $Id: snpicch.h,v 1.6 2015/09/29 10:38:59 siva Exp $     */
/* Licensee Aricent Inc., 2006-2015                         */
/*****************************************************************************/
/*    FILE  NAME            : snpicch.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : VLAN ICCH                                      */
/*    MODULE NAME           : VLAN/GARP                                      */
/*                                                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 02 Jan 2015                                   */
/*    AUTHOR                : ICCH TEAM                                      */
/*    DESCRIPTION           : This file contains all constants and typedefs  */
/*                            for Snooping ICCH module.                      */
/*---------------------------------------------------------------------------*/


#ifndef _SNPICCH_H
#define _SNPICCH_H

#define SNOOP_ICCH_TYPE_FIELD_SIZE 1
#define SNOOP_ICCH_LEN_FIELD_SIZE 2
#define SNOOP_ICCH_BULK_REQ_MSG_SIZE 3

#define SNOOP_ICCH_SYNC_LEAVE 1
#define SNOOP_ICCH_SYNC_CONS_REPORT 2

#define SNOOP_AUX_DATA_SIZE 4
#define SNOOP_ICCH_SYNC_LEAVE_DATA_SIZE 4

#define SNOOP_ICCH_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    ICCH_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define SNOOP_ICCH_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    ICCH_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define SNOOP_ICCH_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    ICCH_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define SNOOP_ICCH_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    ICCH_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define SNOOP_ICCH_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    ICCH_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define SNOOP_ICCH_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    ICCH_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define SNOOP_ICCH_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    ICCH_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define SNOOP_ICCH_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    ICCH_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size; \
}while (0)


/* tSnoopIcchData - Used to enqueue message to Icch */
typedef struct _SnoopIcchData {
    UINT4         u4Events;  /* Events from ICCH to VLAN */
    tIcchMsg     *pIcchMsg;  /* Message buffer sent from ICCH to VLAN */
    UINT2         u2DataLen; /* Buffer length */
    UINT1         au1Reserved[2];
}tSnoopIcchData;

/* Prototypes */
INT4 SnoopIcchRegisterWithICCH PROTO ((VOID));

UINT4 SnoopIcchRegisterProtocols PROTO ((tIcchRegParams *pIcchReg));

INT4 SnoopIcchHandleUpdateEvents PROTO ((UINT1 u1Event, tIcchMsg * pData, UINT2 u2DataLen ));

VOID  SnoopIcchSendBulkReqMsg PROTO ((VOID));

UINT4 SnoopIcchEnqMsgToIcchFromAppl (tIcchMsg * pIcchMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                          UINT4 u4DestEntId);
INT4 SnoopIcchSendMsgToIcch (tIcchMsg * pMsg , UINT2 u2Len );
INT4 SnoopIcchProcessPeerDown (VOID) ;
VOID SnoopIcchProcessPeerUp PROTO ((UINT2 u2VlanId));
VOID SnoopIcchProcessMCLAGEnableStatus PROTO ((INT4 i4VlanId));
INT4 SnoopIcchProcessMCLAGDisableStatus (VOID);

#endif /*_SNPICCH_H*/
