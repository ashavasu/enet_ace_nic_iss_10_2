/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snppt.h,v 1.93.4.1 2018/04/12 12:53:42 siva Exp $
 *
 * Description: This file contains prototypes for functions defined in 
 *              snooping(MLD/IGMP) module.
 *****************************************************************************/
#ifndef _SNPPT_H
#define _SNPPT_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/*****************************************************************************/
/*    FILE  NAME            : snppt.h                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping (MLD/IGMP) Prototype definitions      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains ProtoType definitions       */
/*                            for snooping(MLD/IGMP) module                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                       */ 
/*---------------------------------------------------------------------------*/


/* snpsi.c */
INT4 SnoopInitInstance (UINT4 u4Instance, UINT1 u1McastMode);
VOID SnoopDeInitInstance (UINT4 u4Instance);

INT4 
SnoopGetSwitchName (INT4 i4NextInstId, INT4 i4CurrInstId, UINT1 *pu1Name);
/* snpmi.c */
INT4
SnoopHandleCreateContext (UINT4 u4Instance);
VOID
SnoopHandleDeleteContext (UINT4 u4Instance);
INT4
SnoopHandleUpdateContextName (UINT4 u4Instance);
VOID
SnoopMiMapActivePorts (UINT4 u4Instance);

VOID
SnoopHandleMapPort (UINT4 u4Instance, UINT4 u4IfIndex, UINT2 u2LocalPort);
VOID
SnoopHandleUnmapPort (UINT4 u4Instance, UINT4 u4Port);

INT4 
SnoopHandleMcastModeChange (UINT4 u4Instance, UINT4 u4McastMode);
/* snpmain.c */

VOID SnoopTaskDeInit (VOID);
INT4 SnoopMainModuleInit PROTO ((UINT1 u1McastMode));
INT4 SnoopMainStartModule PROTO ((VOID));
INT4 SnoopMainModuleShutdown PROTO ((VOID));
VOID SnoopMainSetDefaultInstInfo PROTO ((UINT4, UINT1));
VOID SnoopMainProcessPktArrivalEvent PROTO ((UINT1));
VOID SnoopMainProcessQMsgEvent PROTO ((VOID));
VOID SnoopMainEnableSnooping PROTO ((UINT4, UINT1));
INT4 SnoopInitInstInfo (UINT4 u4Instance, UINT1 u1McastMode);
VOID SnoopDeInitInstInfo (UINT4 u4Instance);
INT4 SnoopModuleInitInstance (UINT4 u4Instance);
VOID SnoopModuleShutdownInstance (UINT4 u4Instance);
VOID SnoopGetAllPorts PROTO ((VOID));
INT4 SnoopTaskInit (VOID);

/* snpmem.c */
VOID SnoopMemGlobalMemPoolDelete PROTO ((VOID));
INT4 SnoopMemCreateInstanceInfo PROTO ((UINT4 u4Instance));
VOID SnoopMemDeleteInstanceInfo PROTO ((UINT4 u4Instance));
VOID SnoopSystemModeChange PROTO ((UINT4 u4Instance));
VOID SnoopMemAssignMempoolIds PROTO ((UINT4 u4Instance));
INT4 SnoopMemCreateGlbRBTreePortConfEntry PROTO((VOID));
VOID SnoopMemDeleteGlbRBTreePortConfEntry PROTO((VOID));
INT4
SnoopGrpClearPortEntry PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                            UINT4 u4Port,
       UINT1 AddressType));

INT4 SnoopGetPortCfgEntry PROTO  ((UINT4 u4Instance,
                                   UINT4 u4PortIndex, 
                                   tSnoopTag VlanId, 
                              UINT1 u1AddressType, 
                                   tSnoopPortCfgEntry **ppSnoopPortCfgEntry));
VOID SnoopPortCfgDeleteEntry PROTO ((UINT4 u4Instance,UINT4 u4PortIndex, 
                                     tSnoopTag VlanId,UINT1 u1AddressType));

VOID SnoopPortCfgInsDeleteEntry PROTO ((UINT4 u4Instance, UINT4 u4PortIndex,
                         tSnoopTag VlanId, UINT1 u1AddressType));

VOID SnoopPortCfgGlbDeleteEntry PROTO ((UINT4 u4Instance, UINT4 u4PortIndex,
                         tSnoopTag VlanId, UINT1 u1AddressType));

INT4 SnoopPortCfgCreateEntry PROTO ((UINT4 u4Instance, UINT4 u4PortIndex,
                                     tSnoopTag VlanId, UINT1 u1AddressType, 
                                     tSnoopPortCfgEntry ** ppRetSnoopPortCfgEntry));

INT4 SnoopPortCfgAddEntry PROTO ((UINT4 u4Instance, tSnoopPortCfgEntry *pSnoopPortCfgEntry));
VOID SnoopPortCfgDeleteEntries PROTO ((UINT4 u4Instance, UINT1 u1AddressType));

/* snputil.c */
INT4 SnoopUtilMiGetMcIgsFwdPorts PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                         UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                                         tPortList PortList, 
                                         tPortList UntagPortList));
INT4 SnoopGetInstInfoFromPort PROTO ((UINT4 u4Port, UINT4 *pu4Instance,
                                          UINT4 *pu4InstPort));
VOID SnoopUtilGetInstIdFromVlan PROTO ((tSnoopTag VlanId, 
                                         UINT4 *pu4Instance));
INT4 SnoopGetIfPortBmp PROTO ((UINT4 u4Instance, tSnoopPortBmp PortBitmap,
                                    tSnoopPortBmp PhyPortBitmap));
VOID
SnoopConvertToLocalPortList (tSnoopIfPortBmp PhyPortBmp, 
               tSnoopPortBmp LocalPortBmp);
VOID
SnoopInitInstName PROTO ((UINT4 u4Instance));

INT4 SnoopUtilRBTreeConsGroupEntryCmp PROTO ((tRBElem * ConsGroupEntryNode, 
                                              tRBElem * ConsGroupEntryIn));
INT4
SnoopUtilRBTreeRtrPortEntryCmp PROTO ((tRBElem * rbElem1, tRBElem * rbElem2));
INT4 SnoopUtilRBTreeGroupEntryCmp PROTO ((tRBElem * GroupEntryNode, 
                                          tRBElem * GroupEntryIn));
INT4 SnoopUtilRBTreeMacFwdEntryCmp PROTO ((tRBElem * MacFwdEntryNode, 
                                           tRBElem * MacFwdEntryIn));
INT4 SnoopUtilRBTreeIpFwdEntryCmp PROTO ((tRBElem * IpFwdEntryNode, 
                                          tRBElem * IpFwdEntryIn));
INT4 SnoopUtilRBTreeVlanEntryCmp PROTO ((tRBElem * VlanEntryNode, 
                                         tRBElem * VlanEntryIn));
INT4 SnoopUtilRBTreeConsGroupEntryFree PROTO ((tRBElem * ConsGroupEntryNode, 
                                               UINT4));
INT4 SnoopUtilRBTreeGroupEntryFree PROTO ((tRBElem * GroupEntryNode, UINT4));
INT4 SnoopUtilRBTreeVlanEntryFree PROTO ((tRBElem * VlanEntryNode, UINT4));
INT4 SnoopUtilRBTreeMacFwdEntryFree PROTO ((tRBElem * MacFwdEntryNode, UINT4));
INT4 SnoopUtilRBTreeIpFwdEntryFree PROTO ((tRBElem * IpFwdEntryNode, UINT4));
INT4 SnoopUtilRBTreePortCfgEntryCmp PROTO((tRBElem * PortCfgEntryNode, 
                                                     tRBElem * PortCfgEntryIn));
INT4 SnoopUtilRBTreePortCfgEntryFree PROTO ((tRBElem *PortCfgEntryNode,
                                                                UINT4 u4Arg));
INT4
SnoopUtilUpdateReportDropStats (UINT4 u4Instance, tSnoopVlanEntry * pSnoopVlanEntry,
                              UINT1 u1PktType);

VOID SnoopValidatePacketType PROTO ((UINT4 u4Instance,tSnoopPktInfo *pSnoopPktInfo,
                                    tSnoopVlanEntry *pSnoopVlanEntry, UINT1* u1DropPkt));

VOID SnoopValidateSourceInfo PROTO ((UINT4 u4Instance,tSnoopVlanEntry *pSnoopVlanEntry,
                        UINT1 u1RecordType, UINT2 u2NoSources, UINT1 *u1ToDropPkt));

INT4 SnoopUtilVlanUpdateDropStats (UINT4 u4Instance, tSnoopVlanEntry *pSnoopVlanEntry,
                                   UINT1 u1PktType, UINT1 u1Version, UINT1 QueryType);


#ifdef L2RED_WANTED
INT4
SnoopUtilRBTreeRedMcastEntryCmp PROTO ((tRBElem * SnoopRedCacheEntryNode,
                                 tRBElem * SnoopRedCacheEntryIn));
#endif

CONST CHR1 * SnoopUtilGetModuleName PROTO ((UINT4 u4TraceModule));
CONST CHR1 * SnoopUtilGetModuleNameTrace PROTO ((UINT4 u4TraceModule));

tCRU_BUF_CHAIN_HEADER *
SnoopUtilDuplicateBuffer PROTO ((tCRU_BUF_CHAIN_HEADER * pSrcBuf));

VOID
SnoopUtilUpdateSendSrcInfo PROTO ((UINT4 u4Instance, UINT1 u1RecordType,
                          UINT1 u1OldFilterMode,
                          tSnoopSrcBmp OldInclSrcBmp,
                          tSnoopSrcBmp OldExclSrcBmp,
                          tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry));

VOID
SnoopUtilUpdateRecordType PROTO ((UINT1 u1RecordType, UINT1 u1OldFilterMode,
                         tSnoopSrcBmp OldInclSrcBmp,
                         tSnoopSrcBmp OldExclSrcBmp,
                         tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry));

INT4
SnoopUtilGetHostEntry PROTO ((tSnoopGroupEntry * pSnoopGroupEntry,
                      tSnoopPktInfo * pSnoopPktInfo,
                     tSnoopHostEntry ** ppRetSnoopHostNode));

VOID
SnoopUtilGetForwardPorts PROTO ((tSnoopGroupEntry * pSnoopGroupEntry,
                                UINT2 u2SrcIndex,
                                UINT1 u1ASMHostPresent,
                                tSnoopPortBmp PortBitmap));

VOID
SnoopUtilUpdateRefCount PROTO ((UINT4 u4Instance, UINT2 u2SrcIndex,
                        tSnoopGroupEntry * pSnoopGroupEntry,
                       UINT1 *pu1Incr));

VOID
SnoopUtilCheckIsSourceUsed PROTO ((UINT4 u4Instance, UINT2 u2SrcIndex, 
                                   UINT1 *pu1SrcUsed));

BOOL1 SnoopUtilFilterGrpsAndNotify(tSnoopFilter *pSnoopFilterInfo);
INT4
SnoopUtilGetMcastIgsIpFwdPorts PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                                       UINT4 u4GrpAddr, 
                                UINT4 u4SrcAddr, tPortList PortList, 
                                tPortList UntagPortList));
INT4
SnoopUtilGetMcastMldsIpFwdPorts PROTO ((UINT4 u4Instance, tSnoopTag VlanId, UINT1 *pu1GrpAddr, 
                                UINT1 *pu1SrcAddr, tPortList PortList, 
                                tPortList UntagPortList));

UINT1
SnoopUtilIsPortListValid PROTO ((UINT4 u4Instance, UINT1 *pu1Ports, INT4 i4Length));

INT4
SnoopUtilValidateInstanceId PROTO ((UINT4 u4InstanceId));

INT4
SnoopUtilValidateVlanId PROTO ((tSnoopTag VlanId));

INT4
SnoopUtilValidateAddressType PROTO ((UINT1 u1AddressType));

INT1 
SnoopUtilSendGeneralQuery PROTO ((UINT4 u4Instance, 
                                  UINT1 u1AddressType, 
                                  tSnoopVlanEntry *pSnoopVlanEntry,
                                  UINT1 u1StartUpQueryFlag));

INT1 
SnoopUtilSendGeneralQueries PROTO ((UINT4 u4Instance, 
                                    UINT1 u1AddressType));

INT4
SnoopUtilVlanResetRtrPorts (INT4 i4Instance, tSnoopTag VlanId,
                            UINT1 u1AddrType , tSnoopPortBmp PortBitmap);

INT4
SnoopUtilRBTreeHostEntryCmp PROTO ((tRBElem *, tRBElem *));
    
INT1
SnoopUtilAddSource PROTO ((UINT4, tSnoopSourceInfo *));

INT1
SnoopUtilGetNextRegisteredSource PROTO ((UINT4 u4Instance,
                                         tSnoopHostEntry * pSnoopHostEntry,
                                         tIPvXAddr SrcAddr,
                                         UINT1 u1Status,
                                         UINT1 * pu1NextSrcAddr));
INT1
SnoopUtilAddSourceInfo PROTO ((UINT4, tSnoopPktInfo *, tSnoopTag));

INT4
SnoopUtilCleanUpSrcList (tSnoopTmrNode  *pExpiredTmr);

INT1
SnoopUtilGetFirstPortCfg PROTO ((INT4 *, UINT4 *, INT4 *));

INT1
SnoopUtilGetNextPortCfg PROTO ((INT4, INT4 *, UINT4, UINT4 *,
                                INT4, INT4 *));

UINT1
SnoopUtilIsPortListVlanMember PROTO ((UINT4 u4Instance, INT4 i4VlanId,
                                      UINT1 * pu1Ports));

INT1
SnoopUtilUpdateAggStatusInHW PROTO ((UINT4 u4Instance,
                                     UINT2 u2AggId,
                                     UINT4 u4Port, UINT1  u1Status));


INT4
SnoopUtilHandleSparseMode PROTO ((UINT4 u4Instance,INT4 i4SparseMode));

INT4
SnoopUtilEnableSnoopInVlan PROTO((UINT4 u4Instance, tSnoopVlanEntry *pSnoopVlanEntry));

INT4
SnoopUtilIsSrcAndGrpPresentInPim PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                         tSnoopPktInfo *pSnoopPktInfo,UINT1 u1EventType));
INT4
SnoopUtilGetPIMInterfaceStatus PROTO ((tSnoopTag SnoopVlanId));

INT4
SnoopUtilNpUpdateMcastFwdEntry PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                       tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                                       tSnoopPortBmp PortBitmap, UINT1 u1EventType));

INT4
SnoopUtilGetForwardingEntryFromPim PROTO ((UINT4 u4Instance,tSnoopTag VlanId,
                                           tSnoopPortBmp RtrPortBitMap));

VOID
SnoopUtilCleanIpForwardingEntry PROTO ((UINT4 u4Instance, 
                                tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry));

/* snptmr.c */

INT4 SnoopTmrStartTimer PROTO ((tSnoopTmrNode * pSnoopTmrNode, 
                                UINT4 u4Duration));
INT4 SnoopTmrStopTimer PROTO ((tSnoopTmrNode * pSnoopTmrNode));
INT4 SnoopTmrGetRemainingTime PROTO ((tSnoopTmrNode * pSnoopTmrNode));
VOID SnoopTmrExpiryHandler PROTO ((VOID));
INT4 SnoopTmrQueryTimerExpiry PROTO ((UINT4 u4InstId, VOID *pArg));
INT4 SnoopTmrV1RtrPortPurgeTimerExpiry PROTO ((UINT4 u4InstId, VOID *pArg));
INT4 SnoopTmrV2RtrPortPurgeTimerExpiry PROTO ((UINT4 u4InstId, VOID *pArg));
INT4 SnoopTmrV3RtrPortPurgeTimerExpiry PROTO ((UINT4 u4InstId, VOID *pArg));
INT4 SnoopTmrASMPortPurgeTimerExpiry PROTO ((UINT4 u4InstId, VOID *pArg));
INT4 SnoopTmrGrpQueryTimerExpiry PROTO ((UINT4 u4InstId, VOID *pArg));
INT4 SnoopTmrIpFwdEntryTimerExpiry PROTO ((UINT4 u4InstId, VOID *pArg));
INT4 SnoopTmrHostPresentTimerExpiry (UINT4 u4Instance, VOID *pArg);
INT4 SnoopTmrOtherQuerierPresentTimerExpiry PROTO ((UINT4 u4InstId, VOID *pArg));
INT4 SnoopTmrGroupSourceInfoTimerExpiry PROTO ((UINT4 u4InstId, VOID *pArg));
INT4 SnoopHandleRtrPortPurge PROTO((UINT4 u4InstId, tSnoopRtrPortEntry *pRtrPortEntry));
INT4 SnoopHandleSSMPortPurge PROTO ((UINT4 u4InstId, 
                                     tSnoopPortEntry *pSnoopSSMPortEntry));

/* Prototypes to be added fo snpproc.c */

INT4
SnoopProcessMulticastPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT4 u4InstanceId, UINT4 u4Port,
                                tSnoopTag VlanId,
                                tSnoopPktInfo *pSnoopPktInfo));

INT4
SnoopProcessMulticastData PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                  tSnoopPktInfo * pSnoopPktInfo, UINT4 u4Port, 
                                  tCRU_BUF_CHAIN_HEADER * pBuf, 
                                  UINT1 *pu1FwdData, UINT1 *pu1IsUnReg, 
                                  tSnoopPortBmp PortBitmap));

INT4
SnoopProcessMulticastDataOnIpMode PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                          tSnoopVlanEntry *pSnoopVlanEntry,
                                          tSnoopPktInfo * pSnoopPktInfo, 
                                          UINT4 u4Port, 
                                          tCRU_BUF_CHAIN_HEADER * pBuf,
                                          UINT1 *pu1FwdData, UINT1 *pu1IsUnReg));

INT4
SnoopProcessDoubleTaggedData PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                                     tSnoopVlanEntry * pSnoopVlanEntry,
                                     tSnoopPktInfo * pSnoopPktInfo, UINT4 u4Port, 
                                     tCRU_BUF_CHAIN_HEADER * pBuf, 
                                     UINT1 *pu1FwdData, UINT1 *pu1IsUnReg));

INT4
SnoopProcessSingleTaggedData PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                                     tSnoopVlanEntry * pSnoopVlanEntry,
                                     tSnoopPktInfo * pSnoopPktInfo, UINT4 u4Port, 
                                     tCRU_BUF_CHAIN_HEADER * pBuf, 
                                     UINT1 *pu1FwdData, UINT1 *pu1IsUnReg));

INT4
SnoopForwardDataonIpFwdEntry PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                                     tSnoopIpGrpFwdEntry * pSnoopIpGrpFwdEntry,
                                     UINT4 u4Port, tCRU_BUF_CHAIN_HEADER * pBuf));


INT4
SnoopCreateAndFwdDataonIpFwdEntry PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                                          tSnoopPktInfo *pSnoopPktInfo, 
                                          tSnoopGroupEntry * pSnoopGroupEntry,
                                          UINT4 u4Port, tCRU_BUF_CHAIN_HEADER * pBuf, 
                                          tSnoopPortBmp FwdPortBitmap));

INT4
SnoopCheckToCreateIpFwdEntry PROTO ((UINT4 u4Instance, 
                                     tSnoopPktInfo *pSnoopPktInfo, 
                                     tSnoopGroupEntry *pSnoopGroupEntry, 
                                     UINT2 *pu2SrcIndex, 
                                     UINT1 *pu1SGEntry, UINT1 *pu1IsUnReg));

INT4
SnoopProcessIgmpOrMldPkt PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                                 tSnoopVlanEntry * pSnoopVlanEntry,
                                 tSnoopPktInfo * pSnoopPktInfo,
                                 tCRU_BUF_CHAIN_HEADER * pBuf));

INT4
SnoopProcessQueryMessageHandler PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                                        tSnoopVlanEntry * pSnoopVlanEntry,
                                        tSnoopPktInfo * pSnoopPktInfo,
                                        tCRU_BUF_CHAIN_HEADER * pBuf,
                                        UINT1 u1Version));
INT4
SnoopProcessGeneralQuery PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                 tSnoopVlanEntry * pSnoopVlanEntry,
                                 tSnoopPktInfo * pSnoopPktInfo, 
                                 UINT1 u1Version));

INT4
SnoopProcessGroupQuery PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                               tSnoopVlanEntry * pSnoopVlanEntry,
                               tSnoopPktInfo * pSnoopPktInfo,
                               UINT1 u1Version));

INT4
SnoopProcessGrpAndSrcQry PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                                 tSnoopVlanEntry * pSnoopVlanEntry,
                                 tSnoopPktInfo * pSnoopPktInfo, UINT2 u2SrcCount,
                                 tIPvXAddr *pSources));

INT4
SnoopProcessReportMessageHandler PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                                         tSnoopVlanEntry * pSnoopVlanEntry,
                                         tSnoopPktInfo * pSnoopPktInfo,
                                         tSnoopPortCfgEntry
                                                 *pSnoopPortCfgEntry,
                                         UINT1 *pu1Forward,
                                         tSnoopPortBmp PortBitmap,
                                         tCRU_BUF_CHAIN_HEADER * pBuf));

INT4
SnoopProcessSSMReportMessageHandler PROTO ((UINT4 u4Instance,
                                            tSnoopTag VlanId, 
                                            tSnoopVlanEntry * pSnoopVlanEntry,
                                            tSnoopPktInfo * pSnoopPktInfo,
                                            tSnoopPortCfgEntry
                                                      *pSnoopPortCfgEntry,
                                            UINT1 *pu1Forward,
                                            tSnoopPortBmp PortBitmap,
                                            tCRU_BUF_CHAIN_HEADER * pBuf));

INT4
SnoopProcessLeaveOrDoneMessage PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                                       tSnoopVlanEntry * pSnoopVlanEntry,
                                       tSnoopPktInfo * pSnoopPktInfo,
                                         tSnoopPortCfgEntry
                                                   *pSnoopPortCfgEntry,
                                       UINT1 *pu1Forward,
                                       tSnoopPortBmp OutPortBitmap));

INT4
SnoopProcessDeletePortEvent PROTO ((UINT4 u4Instance,
                                    UINT4 u4Port, UINT1 u1Action));

INT4
SnoopProcessEnablePortEvent PROTO ((UINT4 u4Instance, UINT4 u4Port));

INT4
SnoopProcessVlanUpdatePortsEvent PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                       tMacAddr McastAddr,
                                       tSnoopPortBmp AddPortBitmap,
                                       tSnoopPortBmp DelPortBitmap));
VOID
SnoopHandleTransitionToNonQuerier PROTO ((UINT4 u4Instance, tSnoopVlanEntry * pSnoopVlanEntry));

VOID
SnoopHandleTransitionToQuerier PROTO ((UINT4 u4Instance,                tSnoopVlanEntry * pSnoopVlanEntry));

/*****For Static********************/
INT4
SnoopGrpCreateStaticGroupEntry (UINT1 u1Instance, tSnoopTag VlanId, tIPvXAddr GrpAddr, tSnoopGroupEntry ** ppRetSnoopGroupEntry);

/* Prototypes for snpvlan.c */
INT4
SnoopVlanCreateVlanEntry PROTO ((UINT4 u4Instance,
                          tSnoopTag VlanId,
                          UINT1 u1AddressType,
                          tSnoopVlanEntry ** ppRetSnoopVlanEntry));

INT4
SnoopVlanGetVlanEntry PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                       UINT1 u1AddressType,
                       tSnoopVlanEntry ** ppSnoopVlanEntry));

INT4
SnoopVlanDeleteVlanEntry PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                          UINT1 u1AddressType));

VOID
SnoopVlanDeleteVlanEntries PROTO ((UINT4 u4Instance, UINT1 u1AddressType));

VOID
SnoopVlanDeleteRtrPortList PROTO ((UINT4 u4Instance,
                            tSnoopVlanEntry * pSnoopVlanEntry,
                            UINT1 u1DelStaticRtrPort));

INT4
SnoopVlanUpdateLearntRtrPort PROTO ((UINT4 u4Instance,
                              tSnoopVlanEntry * pSnoopVlanEntry,
                              UINT4 u4Port, UINT1 u1PktType,
                              UINT1 u1Version, UINT4 u4QuerierV3QryInt));

INT4
SnoopVlanDeleteRtrPortEntry PROTO ((UINT4 u4Instance,
                             tSnoopVlanEntry * pSnoopVlanEntry,
                             UINT4 u4Port, tMacAddr McastAddr, UINT1 u1Action));

INT4
SnoopVlanUpdateStats PROTO ((UINT4 u4Instance,
                             tSnoopVlanEntry * pSnoopVlanEntry,
                             UINT1 u1StatType));

INT4
SnoopVlanUpdateVlanTable PROTO ((UINT4 u4Instance, UINT1 u1AddressType));

INT4
SnoopVlanUpdateStatusChange PROTO ((UINT4 u4Instance,
                                    tSnoopVlanEntry * pSnoopVlanEntry));

INT4
SnoopVlanGetStaticRtrPorts PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                  UINT1 u1AddressType,
                                  tSNMP_OCTET_STRING_TYPE * pRtrPortList));

VOID
SnoopVlanForwardPacket PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                               UINT1 u1AddressType,  UINT4 u4Port,
                               UINT1 u1Forward, tCRU_BUF_CHAIN_HEADER * pBuf,
                               tSnoopPortBmp PortBitmap, UINT4 u4StatType));

VOID
SnoopVlanForwardQuery PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                              UINT1 u1AddressType, UINT4 u4Port, 
                              UINT1 u1Forward,  tCRU_BUF_CHAIN_HEADER * pBuf, 
                              tSnoopPortBmp PortBitmap, UINT4 u4StatType));

VOID
SnoopVlanDupAndFwdQuery PROTO ((tSnoopTag VlanId, UINT4 u4Port, 
                               tCRU_BUF_CHAIN_HEADER * pBuf, BOOL1 bTag));

VOID
SnoopVlanInitializeRtrPort PROTO ((UINT4 u4Instance,
                             tSnoopRtrPortEntry *pRtrPortNode,
                             UINT4 u4Port, tSnoopVlanEntry *pSnoopVlanEntry));

INT4
SnoopVlanAddRemRtrPort PROTO ((UINT4 u4Instance, UINT4 u4Port,
                        tSnoopVlanEntry *pSnoopVlanEntry, UINT1 u1AddRemFlag));

/* Prototypes for snpgrp.c */

INT4
SnoopGrpCreateGroupEntry PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                        tIPvXAddr GrpAddr, tSnoopPktInfo * pSnoopPktInfo,
                        tSnoopPortCfgEntry *pSnoopPortCfgEntry,
                        UINT2 u2NumSrcs, UINT1 u1RecordType,
                        tSnoopGroupEntry ** ppRetSnoopGroupEntry));

INT4
SnoopGrpDeleteGroupEntry PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                tIPvXAddr GrpAddr));

INT4
SnoopGrpDelGroupOnOuterVlan PROTO ((UINT4 u4Instance, tSnoopTag VlanId));

INT4
SnoopGrpGetGroupEntry PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                              tIPvXAddr GrpAddr,
                              tSnoopGroupEntry ** ppRetSnoopGroupEntry));

INT4
SnoopGrpUpdatePortToGroupEntry PROTO ((UINT4 u4Instance,
                                tSnoopGroupEntry * pSnoopGroupEntry,
                                tSnoopPktInfo * pSnoopPktInfo,
tSnoopPortCfgEntry *pSnoopPortCfgEntry, BOOL1 bNewGroup));

VOID SnoopGrpDeleteSSMHostEntry PROTO ((UINT4 u4Instance,
                                tSnoopPortEntry   * pSnoopSSMPortNode,
                               tSnoopGroupEntry * pSnoopGroupEntry,
                               tSnoopHostEntry * pSnoopHostNode));
VOID SnoopGrpDeleteASMHostEntry PROTO ((UINT4 u4Instance,
                                tSnoopPortEntry   * pSnoopSSMPortNode,
                               tSnoopGroupEntry * pSnoopGroupEntry,
                               tSnoopHostEntry * pSnoopHostNode));

VOID
SnoopGrpDeleteGroupEntries PROTO ((UINT4 u4Instance, UINT1 u1AddressType));

VOID
SnoopGrpDeletePortEntries PROTO ((UINT4 u4Instance,
                                 tSnoopGroupEntry * pSnoopGroupEntry));

INT4
SnoopGrpDeletePortEntry PROTO ((UINT4 u4Instance, tSnoopVlanEntry *pSnpVlEntry,
                              UINT4 u4Port, tMacAddr McastAddr));

INT4
SnoopGrpHandleSSMReports PROTO ((UINT4 u4Instance, UINT1 u1RecordType,
                                 tSnoopGroupEntry * pSnoopGroupEntry,
                                 UINT2 u2NumSrcs,
                                 tSnoopPktInfo * pSnoopPktInfo,
                                 tSnoopPortCfgEntry *pSnoopPortCfgEntry));

INT4
SnoopGrpHandleToInclude PROTO ((UINT4 u4Instance,
                                tSnoopGroupEntry * pSnoopGroupEntry,
                                tSnoopVlanEntry * pSnoopVlanEntry,
                                UINT2 u2NumSrcs,
                                tSnoopPktInfo * pSnoopPktInfo,
                                tSnoopPortCfgEntry *pSnoopPortCfgEntry,
                                UINT1 *pu1UpdateMrp));

INT4
SnoopGrpHandleToExclude PROTO ((UINT4 u4Instance,
                                tSnoopGroupEntry * pSnoopGroupEntry,
                                UINT2 u2NumSrcs,
                                tSnoopPktInfo * pSnoopPktInfo,
                                tSnoopPortCfgEntry *pSnoopPortCfgEntry,
                                UINT1 *pu1UpdateMrp));

INT4
SnoopGrpHandleAllow PROTO ((UINT4 u4Instance,
                            tSnoopGroupEntry * pSnoopGroupEntry,
                            UINT2 u2NumSrcs, tSnoopPktInfo * pSnoopPktInfo,
                            tSnoopPortCfgEntry *pSnoopPortCfgEntry));

INT4
SnoopGrpHandleBlock PROTO ((UINT4 u4Instance,
                            tSnoopGroupEntry * pSnoopGroupEntry,
                            UINT2 u2NumSrcs, tSnoopPktInfo * pSnoopPktInfo,
                            tSnoopPortCfgEntry *pSnoopPortCfgEntry));

INT4
SnoopGrpUpdateSSMPortToGroupEntry PROTO ((UINT4 u4Instance,
                                tSnoopGroupEntry * pSnoopGroupEntry,
                                UINT2 u2SrcIndex,
                                tSnoopPktInfo * pSnoopPktInfo,
                                tSnoopPortCfgEntry *pSnoopPortCfgEntry,
                                UINT1 u1ResetHost, UINT1 u1RecordType));

VOID
SnoopGrpConsolidateSrcInfo PROTO ((UINT4 u4Instance, UINT1 u1RecordType, 
                                   UINT1 u1CurrFilterMode,
                                   tSnoopSrcBmp OldInclBmp, 
                                   tSnoopSrcBmp OldExclBmp,
                                   tSnoopGroupEntry * pSnoopGroupEntry, 
                                   UINT1 u1ConsReqFlag));

INT4
SnoopGrpHandleToIncludeNone PROTO ((UINT4 u4Instance,
                             tSnoopGroupEntry * pSnoopGroupEntry,
                                    tSnoopVlanEntry * pSnoopVlanEntry,
                             tSnoopPktInfo * pSnoopPktInfo,
                             tSnoopPortCfgEntry *pSnoopPortCfgEntry));

INT4
SnoopGrpHandleToExcludeNone PROTO ((UINT4 u4Instance,
                           tSnoopGroupEntry * pSnoopGroupEntry,
                           tSnoopPktInfo * pSnoopPktInfo,
                           tSnoopPortCfgEntry *pSnoopPortCfgEntry));

INT4
SnoopConsGroupEntryUpdate PROTO ((UINT4 u4Instance, 
                                  tSnoopGroupEntry *pSnoopGroupEntry, 
                                  UINT1 u1Action));

INT4
SnoopGrpGetConsGroupEntry PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                  tIPvXAddr GrpAddr,
                                  tSnoopConsolidatedGroupEntry **
                                  ppRetSnoopConsGroupEntry));

VOID
SnoopGrpConsolidateGroupInfo PROTO ((UINT4 u4Instance,
                                     tSnoopConsolidatedGroupEntry *
                                     pSnoopConsGroupEntry));

INT4
SnoopProcessUpdateFwdDbase (UINT4 u4Instance,tSnoopTag VlanId,UINT2 u2NoSources,
                            tSnoopVlanEntry * pSnoopVlanEntry,
                            tSnoopGroupEntry * pSnoopGroupEntry,
                            tCRU_BUF_CHAIN_HEADER * pBuf,
                            tSnoopPktInfo * pSnoopPktInfo);


VOID
SnoopUpdateConsGrpOnFilterChange PROTO ((UINT4 u4Instance, UINT1 u1RecordType, 
                                         tSnoopConsolidatedGroupEntry *
                                         pSnoopConsGroupEntry, 
                                         UINT1 u1UpdateFlag));

VOID
SnoopGrpCheckAndSendConsReport PROTO ((UINT4 u4Instance, 
                                       tSnoopConsolidatedGroupEntry *
                                       pSnoopConsGroupEntry));

INT4
SnoopGrpGetHostEntry PROTO ((UINT4, tSnoopTag, tIPvXAddr, UINT4,
                             tIPvXAddr, tSnoopHostEntry **));

INT4
SnpProcessIpAddrChangeEvent PROTO ((UINT4, UINT4, UINT2,UINT1));
/* snpfwd.c */

INT4 SnoopFwdUpdateMacFwdTable PROTO ((UINT4, tSnoopGroupEntry *,
                                       UINT4, UINT1, UINT1));
INT4 SnoopFwdUpdateIpFwdTable PROTO ((UINT4, tSnoopGroupEntry *, tIPvXAddr, 
                                      UINT4, tSnoopPortBmp, UINT1));
INT4 SnoopFwdIpFwdEntryDelPort PROTO ((UINT4 u4Instance, 
                                       tSnoopIpGrpFwdEntry *
                                       pSnoopIpGrpFwdEntry, 
                                       UINT4 u4Port, INT4 i4MRPPresent, UINT1 *pu1TableDelete));
INT4 SnoopFwdUpdateIpFwdRtrEntryOnDelPort PROTO ((UINT4 u4Instance,
                                                  tSnoopGroupEntry *
                                                  pSnoopGroupEntry, 
                                                  tSnoopIpGrpFwdEntry *
                                                  pIpGrpFwdEntry, 
                                                  tIPvXAddr SrcIpAddr));
INT4 SnoopFwdGetMacForwardingEntry PROTO ((UINT4, tSnoopTag, tSnoopMacAddr, 
                                      UINT1, tSnoopMacGrpFwdEntry **));

INT4 SnoopFwdGetIpForwardingEntryForAllSrc PROTO ((UINT4, tSnoopTag, tIPvXAddr,
                                      tIPvXAddr, tSnoopIpGrpFwdEntry **));


INT4 SnoopFwdGetIpForwardingEntry PROTO ((UINT4, tSnoopTag, tIPvXAddr, 
                                     tIPvXAddr, tSnoopIpGrpFwdEntry ** ));

INT4 SnoopFwdDeleteMcastFwdEntries PROTO ((UINT4, UINT1));

INT4 SnoopFwdDeleteVlanMcastFwdEntries PROTO ((UINT4, tSnoopTag, UINT1));
INT4 SnoopFwdUpdateRtrPortToMacFwdTable PROTO ((UINT4, UINT4, tSnoopTag, 
                                                UINT1, tMacAddr, UINT1));

INT4 SnoopFwdUpdateRtrPortToIpFwdTable PROTO ((UINT4, UINT4, tSnoopTag, 
                                               UINT1, UINT1));

INT4 SnoopUtilProcessDoubleTagDataForOuterVlan (UINT4 u4InstId, tSnoopPktInfo *pSnoopPktInfo,
                                                tSnoopGroupEntry * pSnoopGroupEntry);

INT4 SnoopUtilAddPortToVlanIpFwdTable (UINT4 u4InstId, tSnoopGroupEntry * pSnoopGroupEntry);

INT4 SnoopFwdUpdateFwdBitmap PROTO ((UINT4, tSnoopPortBmp, tSnoopVlanEntry *, 
                                     tIPvXAddr, UINT1));

INT4 SnoopFwdAddRemovePorts PROTO ((UINT4, tSnoopPortBmp, 
                                    tSnoopMacGrpFwdEntry *,
                                    tSnoopIpGrpFwdEntry *, UINT1));

VOID SnoopFwdSummarizeIpFwdTable PROTO ((UINT4, tSnoopGroupEntry *, UINT1));
INT4 SnoopDeleteIpFwdTable PROTO ((UINT4 u4Instance,
                                   tSnoopIpGrpFwdEntry * pSnoopIpGrpFwdEntry));
INT4
SnoopGetIfOperStatus PROTO ((UINT4 u4Port, UINT1 *pu1OperStatus));

INT4
SnoopUtilGetIgsStatus (tVlanId VlanId);

INT4
SnoopUtilGetForwardingDataBase (tVlanId VlanId,tIPvXAddr *pSrcAddr,
                                tIPvXAddr *pGrpAddr);

INT4 SnoopCfaGetIfaceType PROTO ((UINT4 u4Port, UINT1 *pu1IfaceType));

#ifdef MLDS_WANTED
/* mldsdec.c */

INT4
MldsDecodeIncomingPacket PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT4 u4Instance, UINT2 u2LocalPort, 
                                 tSnoopVlanInfo VlanInfo, UINT2 u2TagLen));

INT4
MldsUtilValidatePkt PROTO ((UINT4 u4Instance, tCRU_BUF_CHAIN_HEADER * pBuf,
                            tSnoopPktInfo * pMldsPktInfo));

INT4
MldsUtilExtractIpHdrInfo PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, tSnoopMLDHdr *pMldPkt,
                                 tSnoopPktInfo * pMldspPktInfo));


INT4
MldsUtilPrcsIp6HbyhOptHdr PROTO (( tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4TotLen,
                           UINT4 *pu4Len, UINT1 *pu1IsMldRtrAlert, UINT1 *pu1NextHdr));


INT4
MldsUtilPrcsIp6Pad1Opt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1OptType,
                        UINT4 *pu4BalanceBytes, UINT4 *pu4ProcessedLen));


INT4
MldsUtilPrcsIp6UnRecOpt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1OptType,
                         UINT4 *pu4BalanceBytes));


INT4
MldsUtilIp6JmbHdrValid PROTO ((tSnoopJmbHdr * pJmbHdr,
                UINT4 u4TotLen));



INT4
MldsUtilPrcsIp6JmbOpt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 *pu4BalanceBytes,
                       UINT4 *pu4TotLen, UINT1 *pu1OptValue));

INT4
MldsUtilPrcsIp6RtAlertOpt PROTO (( tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BalanceBytes, UINT1 *pu1IsMldRtrAlert, UINT1 *pu1OptValue));


INT4
MldsUtilPrcsIp6PadNOpt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                        UINT4 *pu4BalanceBytes, UINT1 *pu1OptValue));

INT4
MldsUtilVerifyMldPkt PROTO ((tSnoopPktInfo * pMldsPktInfo,
                             tSnoopMLDHdr MldPkt,
                             UINT4 u4MldHdrOffset));

INT4
MldsUtilVerifyRtrControlMsg PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                    tSnoopPktInfo * pMldsPktInfo));


VOID               *
SnoopBufRead PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pData, UINT4 u4Offset, UINT4 u4Size, UINT1 u1Copy));
/* mldsenc.c */


INT4 MldsEncodeQuery PROTO ((UINT4, UINT1 *, UINT1, UINT1, 
        tCRU_BUF_CHAIN_HEADER **,
                             tSnoopVlanEntry *));
INT4 MldsFormGrpQueryFrmGrpSrcQuery PROTO ((UINT4 u4Instance, 
                                            tCRU_BUF_CHAIN_HEADER *pInBuf,
                                            tSnoopPktInfo *pSnoopPktInfo,
                                            tCRU_BUF_CHAIN_HEADER * pOutBuf));

INT4 MldsEncodeDoneMsg PROTO ((UINT4, UINT1 *, tCRU_BUF_CHAIN_HEADER **));
INT4 MldsEncodeAndSendDoneMsg PROTO ((UINT4, tSnoopTag, 
                                      tSnoopConsolidatedGroupEntry *));
INT4 MldsEncodeQueryResponse PROTO ((UINT4, tSnoopVlanEntry *, 
                                     tSnoopConsolidatedGroupEntry *, UINT4));

INT4 MldsEncodeQueryRespMsg PROTO ((UINT4, tSnoopVlanEntry *,
                         tSnoopConsolidatedGroupEntry *,
                         UINT1 , tSnoopPortBmp, UINT1));
INT4 MldsEncodeV1Report PROTO ((UINT4, UINT1, tIPvXAddr, 
           tCRU_BUF_CHAIN_HEADER **));
INT4 MldsEncodeV2Report PROTO ((UINT4, tSnoopConsolidatedGroupEntry *, UINT1, 
           tCRU_BUF_CHAIN_HEADER **));
INT4 MldsEncodeAggV2Report PROTO ((UINT4, UINT2, UINT4, 
              tCRU_BUF_CHAIN_HEADER **));

/* mldsutil.c */

VOID MldsUtilFillV2SourceInfo PROTO ((UINT4 , tSnoopConsolidatedGroupEntry *,
                                      UINT2, UINT1, UINT4 *));
UINT2 MldsUtilCalculateCheckSum PROTO ((tCRU_BUF_CHAIN_HEADER *, UINT4, 
                   UINT4, UINT1 *, UINT1 *));
VOID MldsUtilEncodeIpv6Packet PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                 UINT1 *, UINT1 *, UINT1, UINT2));
#endif


#ifdef IGS_WANTED
/* igsdec.c */

INT4
IgsDecodeIncomingPacket PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Instance,
                                UINT2 u2LocalPort, tSnoopVlanInfo VlanInfo, 
                                UINT2 u2TagLen));
INT4
IgsUtilValidatePacket PROTO ((UINT4 u4Instance, 
                      tCRU_BUF_CHAIN_HEADER * pBuf, tSnoopPktInfo *pIgsPktInfo, tSnoopTag VlanId));

INT4
IgsUtilExtractIpHdrInfo PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                         tSnoopPktInfo * pIgsPktInfo, UINT1 *pu1Ttl,
                         UINT1 *pu1IsIgsRtrAlert));
UINT2
IgsUtilCalculateIpCheckSum PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                            UINT4 u4Size, UINT4 u4Offset));
INT4
IgsUtilVerifyIgmpPacket PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, tSnoopPktInfo *pIgsPktInfo));
UINT2
IgsUtilIgmpCalcCheckSum PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                         UINT4 u4Size, UINT4 u4Offset));
INT4
IgsUtilVerifyRtrControlMsg PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,  
                            tSnoopPktInfo *pIgsPktInfo));
INT4
IgsMVlanLookupAndProcessPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                    UINT4 u4Instance, UINT2 u2LocalPort, 
                                    tSnoopTag VlanId,
                                    tSnoopPktInfo * pSnoopPktInfo));
INT4
IgsMvlanFormGrpSrcRecordsFromPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                         tSnoopPktInfo * pSnoopPktInfo));
INT4
IgsEncodeIgmpGrpRec PROTO ((UINT4 u4GrpAddr, UINT2 u2NumSrcs,UINT1 u1RecordType,
                    tCRU_BUF_CHAIN_HEADER ** ppOutBuf));

INT4
IgsEncodeIgmpV3Header PROTO ((tCRU_BUF_CHAIN_HEADER ** ppOutBuf));

INT4
IgsUtilFormNewBufUptoIp PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                tCRU_BUF_CHAIN_HEADER **pIpBuf,
                                tSnoopPktInfo * pSnoopPktInfo));

INT4
IgsMVlanProcessV1V2Packet PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                  UINT4 u4Instance, UINT2 u2LocalPort, 
                                  tSnoopTag VlanId,
                                  tSnoopPktInfo * pSnoopPktInfo));

INT4
IgsMVlanProcessV3Packet PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                UINT4 u4Instance, UINT2 u2LocalPort, 
                                tSnoopTag VlanId,       
                                tSnoopPktInfo * pSnoopPktInfo));
UINT1
IgsMVlanTranslationNeeded PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,UINT4 u4Instance,
                                  UINT2 u2LocalPort, tSnoopVlanInfo VlanInfo,
                                  tSnoopPktInfo * pSnoopPktInfo));

INT4
IgsUtilScanGrpRecordsAndFormPkt PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf,
                                        tSnoopPktInfo * pSnoopPktInfo,
                                        UINT1 u1SearchFlag,
                                        tCRU_BUF_CHAIN_HEADER **pOutBuf));
/* igsenc.c */
INT4
IgsEncodeLeaveMsg PROTO ((UINT4 u4Instance, UINT4 u4GrpAddr, 
                     tCRU_BUF_CHAIN_HEADER **ppOutBuf));
INT4
IgsEncodeAndSendLeaveMsg PROTO ((UINT4 u4Instance, tSnoopTag Vlan, 
                                 tSnoopConsolidatedGroupEntry *
                                 pIgsConsGroupEntry));
INT4
IgsEncodeV3Report PROTO ((UINT4 u4Instance, 
                          tSnoopConsolidatedGroupEntry *pIgsConsGroupEntry, 
                          UINT1 u1RecordType, tCRU_BUF_CHAIN_HEADER **ppOutBuf));
INT4
IgsEncodeAggV3Report PROTO ((UINT4 u4Instance, UINT2 u2NumGrpRec, UINT4 u4MaxLength, 
                    tCRU_BUF_CHAIN_HEADER **ppOutBuf));

INT4
IgsEncodeQueryResponse PROTO ((UINT4 u4Instance, tSnoopVlanEntry * pIgsVlanEntry,
                      tSnoopConsolidatedGroupEntry * pIgsConsGroupEntry, 
                      UINT4 u4InPort));
INT4
IgsEncodeQueryRespMsg PROTO ((UINT4 u4Instance, tSnoopVlanEntry * pIgsVlanEntry,
                        tSnoopConsolidatedGroupEntry * pIgsConsGroupEntry,
                        UINT1 u1Version, tSnoopPortBmp PortBitmap,
                        UINT1 u1RecordType));

INT4
IgsEncodeQuery PROTO ((UINT4 u4Instance, UINT4 u4GrpAddr, UINT1 u1PktType,
                       UINT1 u1Version, tCRU_BUF_CHAIN_HEADER ** ppOutBuf,
                       tSnoopVlanEntry *pSnoopVlanEntry));
INT4
IgsFormGrpQueryFromGrpSrcQuery PROTO ((UINT4 u4Instance,
                                       tCRU_BUF_CHAIN_HEADER * pInBuf,
                                       tSnoopPktInfo * pSnoopPktInfo,
                                       tCRU_BUF_CHAIN_HEADER * pOutBuf));

INT4
IgsEncodeV1V2Report PROTO ((UINT4 u4Instance, UINT1 u1PktType, UINT4 u4GroupAddr, 
                         tCRU_BUF_CHAIN_HEADER **ppOutBuf));

VOID
IgsUtilIpFormPacket PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT4 u4Src, UINT4 u4Dest, UINT1 u1PktType, UINT2 u2Len));

VOID
IgsUtilFillV3SourceInfo PROTO ((UINT4 , tSnoopConsolidatedGroupEntry *, 
                                UINT1 , UINT2 , UINT4 *));
#endif

/* snpport.c */
VOID
SnoopVlanApiEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex,tVlanEvbSbpArray *pSbpArray);

INT4
SnoopCfaGetInterfaceBrgPortType (UINT4 u4IfIndex, INT4 *pi4IfBrgPortType);

INT4
SnoopProcessTopoChangeEvent PROTO ((UINT4 u4Instance, UINT4 u4Port,
                                    tSnoopVlanList VlanList, UINT1 u1AddrType));

INT4 
SnoopVlanSendGeneralQuery PROTO ((UINT4 u4Instance, UINT4 u4Port,
                                     tSnoopVlanEntry *pSnoopVlanEntry));



INT4
SnoopUtilVlanResetStats PROTO (( UINT4 i4InstId,
                                 UINT1 u1AddrType, tVlanId VlanId));

VOID
SnoopUtilUpdateRtrPortOperVer PROTO ((tSnoopRtrPortEntry * pRtrPortNode,
                               UINT1 u1Version));

VOID
SnoopUtilStopRtrPortPurgeTmr PROTO ((tSnoopRtrPortEntry * pRtrPortNode,
                                      UINT1 u1TmrVer));

VOID
SnoopUtilStartRtrPortPurgeTmr PROTO ((UINT4 u4Instance,
                               tSnoopRtrPortEntry * pRtrPortNode,
                               UINT1 u1TmrVer, UINT4 u4QuerierV3QryInt));

VOID SnoopMiVlanDelDynamicMcastInfoForVlan PROTO ((UINT4 u4Instance,
                                                   tSnoopTag VlanId));

INT4
SnoopMiVlanSnoopGetTxPortList PROTO ((UINT4 u4Instance, tMacAddr DestMacAddr, 
          UINT4 u4Port, tSnoopTag VlanId, 
          UINT1 u1Forward,
                             tSnoopIfPortBmp PortBitmap,
                             tSnoopIfPortBmp TaggedPortBitmap, 
                             tSnoopIfPortBmp UntaggedPortBitmap));
INT4
SnoopMiVlanSnoopFwdPacketOnPortList PROTO ((tCRU_BUF_CHAIN_DESC *pBuf, UINT4 u4ContextId, tMacAddr DestMacAddr,
                               UINT2 u2Port, tSnoopTag VlanId, UINT1 u1IsQuery, UINT1 u1Forward, tSnoopPortBmp PortBitmap));

VOID
SnoopVlanTagFrame PROTO ((tCRU_BUF_CHAIN_HEADER *pDupBuf, tVlanId VlanId, 
                          UINT1 u1Priority));

INT4
SnoopVlanUpdateTagFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 *pu4IfIndex,
        tVlanTag*VlanTag,UINT2 *pu2TagLen);

VOID
SnoopVlanUntagFrame PROTO ((tCRU_BUF_CHAIN_HEADER *pDupBuf));

VOID
SnoopVlanAddOuterTagInFrame PROTO ((tCRU_BUF_CHAIN_HEADER * pDupBuf, 
                                    tSnoopTag VlanId, UINT1 u1Priority, 
                                    UINT4 u4IfIndex));

INT4   
SnoopMiVlanUpdateDynamicMcastInfo PROTO ((UINT4 u4Instance, tMacAddr MacGroupAddr, 
                                        tSnoopTag VlanId,
                                        UINT4 u4Port, UINT1 u1Action));
VOID
SnoopMiVlanDeleteAllDynamicMcastInfo PROTO ((UINT4 u4Instance, UINT4 u4Port, UINT1 u1MacAddr));

INT4
SnoopVlanGetCVlanIdList PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                tCVlanInfo *pSnoopCVlanInfo));

INT4
SnoopVlanApiForwardOnPorts PROTO ((tCRU_BUF_CHAIN_DESC *pBuf , tVlanFwdInfo *pVlanFwdInfo));

VOID
SnoopGetSysMacAddress PROTO ((UINT4 u4Instance, tMacAddr pSwitchMac));
VOID
SnoopConfigSwitchIp PROTO ((VOID));
UINT4 
SnoopGetFwdModeFromNvRam PROTO ((VOID));
VOID
SnoopSetFwdModeToNvRam PROTO ((UINT4 u4McastForwardingMode));
INT4
SnpPortRegisterWithNetip PROTO ((VOID));
INT4
SnpPortDeRegisterWithNetip PROTO ((VOID));
VOID
SnpPortGetDefaultRouterIfIndex PROTO ((UINT4 *pu4DefIfIndex));
INT4
SnpPortGetVlanIpInfo PROTO ((UINT4 u4IfIndex, UINT4 *pu4VlanIp));
INT4
SnpApiIndicateIpAddrChange PROTO ((tNetIpv4IfInfo * pIpIfInfo, UINT1 u1Bitmap));
INT4
SnoopUpdateVlanSnoopModuleStatus PROTO ((INT4 i4InstId, INT4 i4VlanId,
     INT4 i4AddrType, INT4 i4SnoopStatus));
INT4
SnoopUpdateIpFwdTable PROTO ((UINT4 u4Instance, tSnoopGroupEntry * pSnoopGroupEntry, 
    tSnoopPktInfo * pSnoopPktInfo));

INT4
SnoopDeleteStaticFwdEntries (UINT4 u4Instance,
                       tSnoopGroupEntry * pSnoopGroupEntry);

/**** UNKNOWN_MULTICAST_CHANGE - START ****/
INT4
SnoopUpdateVlanFwdUnRegPorts (UINT4 u4Instance,tSnoopTag InVlanId, INT4 i4IsShutDown);

INT4
SnoopUpdateRtrPortToVlanFwdUnRegPorts (UINT4 u4Instance, INT4 i4VlanId,UINT4 u4port,
                                       tSnoopPortBmp * pRouterPortBmp,INT4 i4AddOrDel);

INT4 SnoopMiGetEgressPorts (tVlanId VlanId, tPortList pPortBitmap);

/**** UNKNOWN_MULTICAST_CHANGE - END ****/

#ifdef SRCMAC_CTP_WANTED
VOID
SnoopHandleOutgoingPkt PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                               tCRU_BUF_CHAIN_HEADER * pDupBuf,
   tSnoopPortBmp PortBitmap,INT4 u4PktSize, 
   UINT2 u2Protocol,UINT1 u1Encap));

VOID
SnoopGetSrcMacAddress PROTO ((UINT2 u2Port, tSnoopTag VlanId, tMacAddr SrcMacAddr));
 
VOID
SnoopHandlePortOutgoingPkt PROTO ((UINT4 u4Instance, tSnoopTag VlanId,
                                   tCRU_BUF_CHAIN_HEADER * pBuf, 
                                   UINT4 u4IfIndex, UINT4 u4PktSize, 
                                   UINT2 u2Protocol, UINT1 u1Encap));
#endif

INT4
SnoopMiGetVlanEgressPorts PROTO ((UINT4 u4Instance, tSnoopTag VlanId, 
                               tSnoopPortBmp EgressPortBitmap));
INT4
SnoopMiGetVlanLocalEgressPorts (UINT4 u4Instance, tSnoopTag VlanId,
                                tSnoopPortBmp EgressPortBitmap);
VOID
SnoopHandleOutgoingPktOnPortList PROTO ((tCRU_BUF_CHAIN_HEADER *pDupBuf, 
                                  tSnoopIfPortBmp TaggedPortBitmap,
                                  UINT4 u4PktSize, UINT2 u2Protocol, 
                                  UINT1 u1Encap));

VOID
SnoopHandleOutgoingPktOnPort PROTO ((tCRU_BUF_CHAIN_HEADER * pDupBuf, 
                                     UINT2 u2IfIndex, UINT4 u4PktSize, 
                                     UINT2 u2Protocol, UINT1 u1Encap));

INT4 SnoopVlanGetCpvidForCep PROTO ((UINT4 u4Port, UINT2 *pu2CVid));

VOID
SnoopVlanModReportOnCep PROTO ((tSnoopTag VlanId, UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf));

INT4
SnoopVlanApplyVidTransInFrame PROTO ((UINT4 u4Port, tSnoopTag VlanId, tCRU_BUF_CHAIN_HEADER *pDupBuf));

BOOL1
SnoopMiIsVlanActive PROTO ((UINT4 u4Instance, tSnoopTag VlanId));

BOOL1
SnoopIsVlanMemberPort PROTO ((tSnoopTag VlanId, UINT2 u2PortIndex));

BOOL1
SnoopMiIsVlanMemberPort PROTO ((UINT4 u4Instance, tSnoopTag VlanId, UINT4 u4PortIndex));

INT4
SnoopFwdUpdateFwdEntries PROTO((tSnoopGroupEntry *pSnoopGroupEntry));

INT4
SnoopFwdDeleteGroupMcastFwdEntries (UINT4 u4Instance, tSnoopGroupEntry * pSnoopGroupEntry);

INT4
SnoopFwdDeleteStaticMcastFwdEntries (UINT4 u4Instance, tSnoopGroupEntry * pSnoopGroupEntry);

INT4
SnoopNoOfStaticAndDynamicGrpEntry PROTO ((UINT4 u4Port,UINT2 *pu2StaticCnt, 
                                          UINT2 *pu2DynamicCnt));
INT4
SnoopDeleteExceededGrpEntry PROTO ((UINT4 u4Port, UINT2 u2NoOfEntriesToDelete));


INT4
SnoopDeleteAllGroupEntryInfo PROTO ((UINT1 u1Instance,tSnoopGroupEntry *pSnoopGroupEntry));

INT4
SnoopCfaCliGetIfList PROTO ((INT1 *pi1IfName, INT1 *pi1IfListStr,
                             UINT1 *pu1IfListArray, UINT4 u4IfListArrayLen));

INT4
SnoopCfaCliGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));

INT4
SnoopCfaGetIfInfo PROTO ((UINT4 u4IfIndex, tCfaIfInfo * pIfInfo));

INT4
SnoopGmrpIsGmrpEnabledInContext PROTO ((UINT4 u4ContextId));

INT4
SnoopMmrpIsMmrpEnabledInContext PROTO ((UINT4 u4ContextId));

INT4
SnoopVcmGetAliasName PROTO ((UINT4 u4ContextId, UINT1 *pu1Alias));

INT4
SnoopVcmGetContextInfoFromIfIndex PROTO ((UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                          UINT2 *pu2LocalPortId));

INT4
SnoopVcmIsSwitchExist PROTO ((UINT1 *pu1Alias, UINT4 *pu4VcNum));

INT4
SnoopVcmGetSystemMode PROTO ((UINT2 u2ProtocolId));


INT4
SnoopVcmGetSystemModeExt PROTO ((UINT2 u2ProtocolId));

INT4
SnoopL2IwfGetNextValidPortForContext PROTO ((UINT4 u4ContextId, 
                                             UINT2 u2LocalPortId,
                                             UINT2 *pu2NextLocalPort,
                                             UINT4 *pu4NextIfIndex));

INT4
SnoopL2IwfIsIgmpTunnelEnabledOnAnyPort PROTO ((UINT4 u4ContextId));

INT4
SnoopL2IwfIsPortInPortChannel PROTO ((UINT4 u4IfIndex));

INT4 
SnoopTacSearchGrpAddressInProfile PROTO ((UINT4 u4ProfileId, UINT4 u4GrpAddr));

INT4
SnoopTacApiGetPrfStats PROTO ((UINT4 u4Instance, UINT4 u4ProfileId, 
                               UINT1 u1AddressType, 
                               tTacmPrfStats *pTacmPrfStats));
INT4
SnoopTacApiUpdateVlanRefCount PROTO ((UINT4 u4Instance, UINT4 u4ProfileId, 
                                      UINT1 u1AddressType, UINT1 u1UpdtFlag));
INT4
SnoopTacApiGetPrfAction PROTO ((UINT4 u4ProfileId, UINT1 u1AddressType,
                                UINT1  *pu1ProfileAction));
INT4
SnoopTacSearchGrpSrcRecordInProfile PROTO ((UINT4 u4ProfileId, 
                                            tGrpRecords * pGrpRecords));

INT4 SnoopTacApiUpdatePortRefCount
PROTO ((UINT4 u4ProfileId, UINT1 u1AddressType, UINT1 u1UpdtFlag));

#ifdef L2RED_WANTED
UINT4
SnoopRmEnqMsgToRmFromAppl PROTO ((tRmMsg * pRmMsg, UINT2 u2DataLen, 
                                  UINT4 u4SrcEntId, UINT4 u4DestEntId));

UINT4
SnoopRmGetNodeState PROTO ((VOID));

UINT1
SnoopRmGetStandbyNodeCount PROTO ((VOID));

UINT1
SnoopRmGetStaticConfigStatus PROTO ((VOID));

UINT1
SnoopRmHandleProtocolEvent PROTO ((tRmProtoEvt *pEvt));

UINT4
SnoopRmRegisterProtocols PROTO ((tRmRegParams * pRmReg));


UINT4
SnoopRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));

UINT4
SnoopRmSendEventToRmTask PROTO ((UINT4 u4Event));

INT4
SnoopRmSetBulkUpdatesStatus PROTO ((UINT4 u4AppId));

VOID
SnoopVlanRedStartMcastAudit PROTO ((VOID));
#ifdef LLDP_WANTED
INT4
SnoopLldpRedRcvPktFromRm PROTO ((UINT1 u1Event, tRmMsg * pData, 
                                 UINT2 u2DataLen));
#endif
#endif

INT4 SnoopTacApiFilterChannel PROTO ((tIPvXAddr * pGroupAddr,
                                      UINT4 u4ProfileId,
                                      UINT2 u2NumSrc, UINT1 u1Version,
                                      UINT1 u1PktType, UINT1 u1RecordType));

/* snpnpapi.c */
#ifdef NPAPI_WANTED
INT1 SnoopHwEnableIgmpSnooping (UINT4 u4Instance);
INT1 SnoopHwDisableIgmpSnooping (UINT4 u4Instance);
INT1 SnoopHwEnableMldSnooping (UINT4 u4Instance);
INT1 SnoopHwDisableMldSnooping (UINT4 u4Instance);
INT4
SnoopHwPortRateLimit (UINT4 u4Instance, UINT4 u4Port, UINT1 u1AddrType,
                      tSnoopTag VlanId, UINT4 u4Rate, UINT1 u1Action);
INT4
SnoopNpUpdateMcastFwdEntry (UINT4 u4Instance, tSnoopTag VlanId, tIPvXAddr SrcAddr, 
                          tIPvXAddr GrpAddr, tSnoopPortBmp PortBitmap, 
                          UINT1 u1EventType);
INT4
SnoopNpGetMulticastEgressPorts (UINT4 u4Instance, tSnoopTag VlanId, 
                                tIPvXAddr GrpAddr,
                                tIPvXAddr SrcAddr, tSnoopIfPortBmp PortBitmap);


VOID SnoopNpClearHwIPMcastTable  (UINT4 u4ContextId);
INT4
SnoopNpIndicateMRP (tIPvXAddr GrpIpAddr, tSnoopTag VlanId);

INT4 IgsProcNPCallback (tSnoopMsgNode * pMsgNode);

INT4 SnoopHandleGroupCreateFailure (tSnoopTag VlanId, tIPvXAddr GrpAddr,
                                    tIPvXAddr SrcAddr, UINT4 u4Instance);

INT4 SnoopHandleGroupDeleteFailure(tSnoopTag VlanId, tIPvXAddr GrpAddr,
                                   tIPvXAddr SrcAddr, UINT4 u4Instance);

INT4 SnoopHandleAddPortFailure (tSnoopTag VlanId, tIPvXAddr GrpAddr,
                                tIPvXAddr SrcAddr, UINT4 u4Instance);

INT4 SnoopHandleDeletePortFailure (tSnoopTag VlanId, tIPvXAddr GrpAddr,
                                   tIPvXAddr SrcAddr, UINT4 u4Instance);
VOID IgsNpFailLog (UINT1 u1EventType, tIPvXAddr GrpAddr, tIPvXAddr SrcAddr);

VOID IgsNpFailNotify (UINT1 u1EventType,tSnoopTag VlanId, tIPvXAddr GrpAddr,
                      tIPvXAddr SrcAddr, UINT4 u4Instance);
#endif

/* fssnplw.c */
VOID SnoopNotifyProtocolShutdownStatus (INT4);

#ifdef PIM_WANTED
extern VOID PimVlanPortBmpChgNotify (tMacAddr MacAddr, tVlanId VlanId, UINT1 u1AddrType);
#endif

#ifdef DVMRP_WANTED
extern INT4 DvmrpIsDvmrpEnabled(VOID);
extern VOID DvmrpVlanPortBmpChgNotify (tMacAddr MacAddr, tVlanId VlanId);
#endif

#ifdef IGMPPRXY_WANTED
extern UINT1 IgpPortIsIgmpPrxyEnabled (VOID);
extern VOID IgpPortBmpChgNotify (tMacAddr GrpMacAddr, UINT2 u2VlanId);
#endif

/* snpmbsm.c */
#ifdef MBSM_WANTED
INT4 SnoopMbsmUpdateCardInsertion (tMbsmPortInfo * pPortInfo,
                                  tMbsmSlotInfo * pSlotInfo);
#endif
INT4 SnoopUtilHandleSparseModeChange (UINT4 u4Instance);

UINT1 SnoopUtilGetNonEdgePortList (UINT4 u4Instance, tSnoopPortBmp PortBitmap,
                             tSnoopPortBmp NonEdgePortBitmap);

UINT1
SnoopUtilIsLocalPortListValid (UINT4 u4Instance, UINT1 *pu1Ports, 
          INT4 i4Length);
INT4
SnoopGetPhyPortBmp (UINT4 u4Instance, tSnoopPortBmp PortBitmap,
      tSnoopIfPortBmp PhyPortBitmap);

INT4 SnoopDeriveSispDefaults (UINT4  u4IfIndex);

INT4 SnoopVcmSispGetPhysicalPortOfSispPort (UINT4 u4IfIndex, 
         UINT4 *pu4PhyIndex);

UINT1 SnoopUtilIsLocalPortListVlanMember (UINT4 u4Instance, INT4 i4VlanId, 
       UINT1 *pu1Ports);
INT4 SnoopUtilUpdateFirstQueryRespStatus (UINT4 u4Instance);

VOID
SnoopVlanGetRtrPortFromPVlanMappingInfo (UINT4 u4Instance, tVlanId InVlanId,
                                         UINT1 u1AddressType, UINT1 u1Version,
tSnoopPortBmp SnoopRtrPortBmp, tSnoopVlanEntry *pSnoopVlanEntry);

INT1
SnoopIsVlanPresentInFwdTbl (tVlanId VlanId, tVlanId InVlanId,
                            tL2PvlanMappingInfo *pL2PvlanMappingInfo);

VOID
SnoopUtilGetPortBmpFromPVlanMapInfo PROTO ((UINT4 u4Instance, tVlanId InVlanId,
                                           tIPvXAddr GrpAddr, tIPvXAddr SrcIpAddr,
                                           tSnoopPortBmp SnoopPortBmp));

UINT4
SnoopUtilFetchFourBytesFromPointer (UINT1 *pu1Ptr);


INT4 SnoopRegisterWithPacketHandler PROTO ((VOID));

INT4 SnoopUtilDeleteIpFwdEntriesForOuterVlan (UINT4 u4InstId,
                                              tSnoopGroupEntry * pSnoopGroupEntry);

INT4
SnoopVlanCheckWildCardEntry PROTO ((tMacAddr WildCardMacAddr));

INT4
SnoopClassifyPacketType PROTO ((UINT4 u4Instance, UINT1 u1RecordType,
                     tSnoopGroupEntry * pSnoopGroupEntry,
                     UINT2 u2NumSrcs, tSnoopPktInfo * pSnoopPktInfo,
                     tSnoopPortCfgEntry * pSnoopPortCfgEntry));
INT4
SnoopClassifyNonePacketType PROTO ((UINT4 u4Instance, UINT1 u1RecordType,
                      tSnoopVlanEntry *pSnoopVlanEntry));

INT4 SnoopUtilHandleStaticConfig (UINT4 u4Instance);
INT4 SnoopIsOuterVlanIpFwdEntryExists PROTO ((UINT4 u4InstId,
                                      tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry));
#ifdef ICCH_WANTED
VOID 
SnoopIcchHandleUpdates PROTO ((tIcchMsg * pMsg, UINT2 u2DataLen));

INT4
SnoopSendConsolidatedReportOnIccl PROTO ((UINT4 u4Port, UINT4 u4Instance, tSnoopTag VlanId));

INT4
SnoopCheckBitMapForMcLagInterface PROTO ((tSnoopPortBmp PortBitMap, UINT1 *pu1McLagInterfacePresent,
                                          UINT1 *pu1NonMcLagInterfacePresent, UINT4 *pu4PhyPort));

INT4
SnoopCheckBitMapForFirstNonMcLagInterface PROTO ((tSnoopPortBmp  AsmPortBitmap, UINT1 *pu1FirstNonMcLagInterface));

#endif
INT4
SnoopProcessLeaveSyncMessage (tIPvXAddr GrpAddr, UINT2 u2AdminKey, tSnoopTag VlanId);

INT4
SnoopSendConsolidatedGroupReportOnIccl (UINT4 u4Instance, tSnoopVlanEntry * pIgsVlanEntry,
                        tSnoopConsolidatedGroupEntry * pIgsConsGroupEntry,
                        UINT4 u4InPort, tSnoopTag VlanId);

VOID
SnoopIcchProcessUpdateMsg (tSnoopMsgNode* pSnoopQMsg);

INT4
SnoopIcchUpdateRtrPort (UINT4 u4InstanceId, tSnoopTag u2VlanId,
                        UINT2 u2LocalPort);
#ifdef ICCH_WANTED
INT4
IgsEncodeAggV3ReportOnIccl PROTO ((UINT4 u4Instance, UINT2 u2NumGrpRec, UINT4 u4MaxLength,
                             tCRU_BUF_CHAIN_HEADER ** ppOutBuf, UINT4 u4PhyPort, tSnoopIcchFillInfo *pSnoopIcchInfo));
INT4
IgsEncodeV3ReportOnIccl PROTO ((UINT4 u4Instance,
                          tSnoopConsolidatedGroupEntry *pIgsConsGroupEntry,
                          UINT1 u1RecordType, tCRU_BUF_CHAIN_HEADER **ppOutBuf, 
                          UINT4 u4PhyPort, tSnoopIcchFillInfo *pSnoopIcchInfo));

VOID
IgsUtilFillV3SourceInfoForIccl PROTO ((UINT4 , tSnoopGroupEntry *,
                                       UINT1 , UINT2 , UINT4 *, UINT2, UINT1 , tSnoopIcchFillInfo *pSnoopIcchInfo));

INT4
SnoopSendGeneralQueryOnIccl (UINT2 u2LocalIcclPort, UINT4 u4Instance, tSnoopTag VlanId);

INT4
SnoopIcchSyncLeaveMessage (UINT4 u4Instance, tIPvXAddr GrpAddr, tSnoopIcchFillInfo *pSnoopIcchInfo, 
                           UINT2 u2AdminKey);

INT4
SnoopIcchSyncMclagStatusChange (UINT2 u2VlanId);

INT4
SnoopCheckBitMapForIcclInterface (tSnoopPortBmp  PortBitmap, UINT4 u4InstanceId);

INT4
MldsEncodeV2ReportOnIccl (UINT4 u4Instance,
                    tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry,
                    UINT1 u1RecordType, tCRU_BUF_CHAIN_HEADER ** ppOutBuf, UINT4 u4PhyPort);
INT4
MldsEncodeAggV2ReportOnIccl (UINT4 u4Instance, UINT2 u2NumGrpRec, UINT4 u4MaxLength,
                       tCRU_BUF_CHAIN_HEADER ** ppOutBuf, UINT4 u4PhyPort);
VOID
MldsUtilFillV2SourceInfoOnIccl (UINT4 u4Instance,
                          tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry,
                          UINT1 u1RecordType, UINT2 u2NumSrcs, 
                          UINT4 *pu4MaxLength,UINT2 u2AdminKey, UINT1 u1NonMcLagInterface);

INT4
SnoopUpdateGroupEntryInEnhMode (UINT4 u4Instance, tSnoopGroupEntry *pSnoopGroupEntry,
                                tSnoopPktInfo * pSnoopPktInfo,
                                tSnoopPortCfgEntry * pSnoopPortCfgEntry);
INT4
SnoopIcchUtilGetHostEntry (UINT4 u4Instance, tSnoopGroupEntry * pSnoopGroupEntry,
                           tSnoopPktInfo * pSnoopPktInfo);

INT4
SnoopSendAllHostReportsOnIccl (UINT4 u4Instance, tSnoopGroupEntry * pSnoopGroupEntry);

INT4 
SnoopIcchUpdateSrcIpCheckSum (UINT4 u4Instance, tCRU_BUF_CHAIN_HEADER *pOutBuf, 
                              tSnoopHostEntry *pSnoopHostEntry);

#endif
VOID
SnoopRedDeInitGlobalInfo (VOID);
PUBLIC INT4                                                                                                       
SnoopRedMcastCacheFreeNodes (tRBElem * pRBElem, UINT4 u4Arg);

VOID
SnoopUtilHandleQuerierOnChange (tSnoopVlanEntry *pSnoopVlanEntry,
                                           UINT4 u4Instance);
VOID SnoopHandleMsrRestoreEvent(VOID);

#endif /* _SNPPT_H */
