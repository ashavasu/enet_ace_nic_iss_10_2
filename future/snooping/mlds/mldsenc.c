/*****************************************************************************/
/* $Id: mldsenc.c,v 1.18 2015/03/11 11:00:09 siva Exp $*/
/*    FILE  NAME            : mldsenc.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : MLD Snooping                                   */
/*    MODULE NAME           : MLD snooping Encoding                          */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains processing routines         */
/*                            for MLD snooping Encoding module               */
/*---------------------------------------------------------------------------*/
#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : MldsEncodeQuery                                      */
/*                                                                           */
/* Description        : This function forms the MLD query messages           */
/*                      (General/ Grp specific Query)                        */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance identifier                    */
/*                    : pu1GrpAddr  - Group Address                          */
/*                    : u1PktType   - Packet type (general / Grp spec qry)   */
/*                      u1Version   - V1/V2 query                            */
/*                      pSnoopVlanEntry - Pointer to vlan entry              */
/*                                                                           */
/* Output(s)          : ppOutBuf    - pointer to output buffer               */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsEncodeQuery (UINT4 u4Instance, UINT1 *pu1GrpAddr, UINT1 u1PktType,
                 UINT1 u1Version, tCRU_BUF_CHAIN_HEADER ** ppOutBuf,
                 tSnoopVlanEntry * pSnoopVlanEntry)
{
    tSnoopMLDHdr        MLDHdr;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4PktSize = 0;
    UINT2               u2EthType;
    UINT1               u1V2HdrLen = 0;
    UINT1               au1MLDV2Hdr[SNOOP_MLDV2_QRY_HDR];
    UINT1               au1Dst[IPVX_MAX_INET_ADDR_LEN];

    SNOOP_MEM_SET (&MLDHdr, 0, sizeof (tSnoopMLDHdr));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (au1MLDV2Hdr, 0, SNOOP_MLDV2_QRY_HDR);
    SNOOP_MEM_SET (au1Dst, 0, IPVX_MAX_INET_ADDR_LEN);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* Allocate buffer for MAC (14) + IPV6 (48) with router alert option
     * + MLD (24) header = 86 bytes. */
    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IPV6_HDR_RTR_OPT_LEN + SNOOP_MLD_HDR_LEN;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buffer allocation failure while constructing  "
                   "MLD query messages\r\n");
        return SNOOP_FAILURE;
    }

    /* Construct MLD header for query message */
    if (u1PktType == SNOOP_GRP_QUERY)
    {
        if (pu1GrpAddr != NULL)
        {
            SNOOP_MEM_CPY (au1Dst, pu1GrpAddr, IPVX_IPV6_ADDR_LEN);
            SNOOP_INET_NTOHL (pu1GrpAddr);
            SNOOP_MLDS_GRP_ADDR_COPY (MLDHdr.GrpAddr, pu1GrpAddr);
        }
    }
    else
    {
        SNOOP_MEM_CPY (au1Dst, gau1MldsAllHostIp, IPVX_MAX_INET_ADDR_LEN);
        SNOOP_INET_NTOHL (au1Dst);
    }

    MLDHdr.u1PktType = SNOOP_MLD_QUERY;
    MLDHdr.u2MaxRespTime = pSnoopVlanEntry->u2MaxRespCode;
    MLDHdr.u2MaxRespTime = SNOOP_HTONS (MLDHdr.u2MaxRespTime);
    MLDHdr.u2CheckSum = 0;

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &MLDHdr,
                               SNOOP_MLD_PACKET_START_OFFSET,
                               SNOOP_MLD_HDR_LEN);

    /* If the query version is v2 then add the query related information
     * ( 2 bytes reserved + 2 bytes refers number of sources )
     * at the end of the packet */

    if (u1Version == SNOOP_MLD_VERSION2)
    {
        au1MLDV2Hdr[0] = 0;
        au1MLDV2Hdr[1] = (UINT1) pSnoopVlanEntry->u2QueryInterval;
        /* Forming QQIC from QQI */
        if (pSnoopVlanEntry->u2QueryInterval >= SNOOP_QUERY_INTERVAL_CODE)
        {
            SNOOP_GET_CODE_FROM_TIME (au1MLDV2Hdr[1],
                                      pSnoopVlanEntry->u2QueryInterval);
        }
        au1MLDV2Hdr[2] = 0;
        au1MLDV2Hdr[3] = 0;

        u1V2HdrLen = SNOOP_MLDV2_QRY_HDR;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &au1MLDV2Hdr,
                                   (SNOOP_MLD_PACKET_START_OFFSET +
                                    SNOOP_MLD_HDR_LEN), SNOOP_MLDV2_QRY_HDR);
    }

    /* Calculate MLD checksum */
    MLDHdr.u2CheckSum = MldsUtilCalculateCheckSum (pBuf,
                                                   (SNOOP_MLD_HDR_LEN +
                                                    u1V2HdrLen),
                                                   SNOOP_MLD_PACKET_START_OFFSET,
                                                   gNullSrcAddr, au1Dst);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &MLDHdr.u2CheckSum,
                               (SNOOP_MLD_PACKET_START_OFFSET + 2),
                               sizeof (UINT2));

    /* Form IPv6 Header */
    MldsUtilEncodeIpv6Packet (pBuf, gNullSrcAddr, (UINT1 *) pu1GrpAddr,
                              u1PktType,
                              (UINT2) (SNOOP_MLD_HDR_LEN + u1V2HdrLen));

    /* Fill MAC Header */
    /* Fill 33:33::01 as a destination MAC address */
    SNOOP_GET_MAC_FROM_IPV6 (au1Dst, MacGroupAddr);

    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);

    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr,
                               SNOOP_ETH_ADDR_SIZE, SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IPV6_PKT_TYPE);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, sizeof (UINT2));

    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsFormGrpQueryFrmGrpSrcQuery                       */
/*                                                                           */
/* Description        : This function forms the MLD query messages           */
/*                      (General/ Grp specific Query)                        */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance identifier                    */
/*                      pInBuf  - Pointer to grp and src specific query      */
/*                      pSnoopPktInfo - Snoop Packet Info                    */
/*                      pOutBuf    - pointer to output buffer               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsFormGrpQueryFrmGrpSrcQuery (UINT4 u4Instance,
                                tCRU_BUF_CHAIN_HEADER * pInBuf,
                                tSnoopPktInfo * pSnoopPktInfo,
                                tCRU_BUF_CHAIN_HEADER * pOutBuf)
{
    tSnoopMLDHdr        MLDHdr;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    UINT2               u2EthType;
    UINT2               u2QQICOffSet = 0;
    UINT1               u1V2HdrLen = 0;
    UINT1               u1QQIC = 0;
    UINT1               u1QRV = 0;
    UINT1               au1MLDV2Hdr[SNOOP_MLDV2_QRY_HDR];
    UINT1               au1Dst[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1Src[IPVX_MAX_INET_ADDR_LEN];

    SNOOP_MEM_SET (&MLDHdr, 0, sizeof (tSnoopMLDHdr));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (au1MLDV2Hdr, 0, SNOOP_MLDV2_QRY_HDR);
    SNOOP_MEM_SET (au1Dst, 0, IPVX_MAX_INET_ADDR_LEN);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* Construct MLD header for query message */

    SNOOP_MEM_CPY (au1Dst, pSnoopPktInfo->GroupAddr.au1Addr,
                   IPVX_IPV6_ADDR_LEN);
    SNOOP_INET_NTOHL (au1Dst) SNOOP_MLDS_GRP_ADDR_COPY (MLDHdr.GrpAddr, au1Dst);

    SNOOP_MEM_CPY (au1Src, pSnoopPktInfo->SrcIpAddr.au1Addr,
                   IPVX_IPV6_ADDR_LEN);
    SNOOP_INET_NTOHL (au1Src) MLDHdr.u1PktType = SNOOP_MLD_QUERY;
    MLDHdr.u2MaxRespTime = pSnoopPktInfo->u2MaxRespCode;
    MLDHdr.u2MaxRespTime = SNOOP_HTONS (MLDHdr.u2MaxRespTime);
    MLDHdr.u2CheckSum = 0;

    CRU_BUF_Copy_OverBufChain (pOutBuf, (UINT1 *) &MLDHdr,
                               SNOOP_MLD_PACKET_START_OFFSET,
                               SNOOP_MLD_HDR_LEN);

    /* If the query version is v2 then add the query related information
     * ( 2 bytes reserved + 2 bytes refers number of sources )
     * at the end of the packet */

    u2QQICOffSet = (UINT2) (SNOOP_DATA_LINK_HDR_LEN +
                            pSnoopPktInfo->u1TagLen +
                            pSnoopPktInfo->u1IpHdrLen + SNOOP_MLD_HDR_LEN);

    CRU_BUF_Copy_FromBufChain (pInBuf, (UINT1 *) &u1QQIC,
                               u2QQICOffSet + SNOOP_V3_QQIC_OFFSET,
                               sizeof (UINT1));

    CRU_BUF_Copy_FromBufChain (pInBuf, (UINT1 *) &u1QRV,
                               u2QQICOffSet + SNOOP_V3_QRV_OFFSET,
                               sizeof (UINT1));

    au1MLDV2Hdr[0] = u1QRV;
    au1MLDV2Hdr[1] = u1QQIC;
    au1MLDV2Hdr[2] = 0;
    au1MLDV2Hdr[3] = 0;

    u1V2HdrLen = SNOOP_MLDV2_QRY_HDR;
    CRU_BUF_Copy_OverBufChain (pOutBuf, (UINT1 *) &au1MLDV2Hdr,
                               (SNOOP_MLD_PACKET_START_OFFSET +
                                SNOOP_MLD_HDR_LEN), SNOOP_MLDV2_QRY_HDR);

    /* Calculate MLD checksum */
    MLDHdr.u2CheckSum = MldsUtilCalculateCheckSum (pOutBuf,
                                                   (SNOOP_MLD_HDR_LEN +
                                                    u1V2HdrLen),
                                                   SNOOP_MLD_PACKET_START_OFFSET,
                                                   pSnoopPktInfo->SrcIpAddr.
                                                   au1Addr, pSnoopPktInfo->
                                                   GroupAddr.au1Addr);

/*    MLDHdr.u2CheckSum = SNOOP_HTONS (MLDHdr.u2CheckSum);*/

    CRU_BUF_Copy_OverBufChain (pOutBuf, (UINT1 *) &MLDHdr.u2CheckSum,
                               (SNOOP_MLD_PACKET_START_OFFSET + 2),
                               sizeof (UINT2));

    /* Form IPv6 Header */
    MldsUtilEncodeIpv6Packet (pOutBuf, (UINT1 *) au1Src, (UINT1 *) au1Dst,
                              SNOOP_GRP_QUERY,
                              (UINT2) (SNOOP_MLD_HDR_LEN + u1V2HdrLen));

    /* Fill MAC Header */
    /* Fill 33:33::01 as a destination MAC address */

    SNOOP_GET_MAC_FROM_IPV6 (pSnoopPktInfo->GroupAddr.au1Addr, MacGroupAddr);

    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);

    CRU_BUF_Copy_OverBufChain (pOutBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pOutBuf, SrcMacAddr,
                               SNOOP_ETH_ADDR_SIZE, SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IPV6_PKT_TYPE);
    CRU_BUF_Copy_OverBufChain (pOutBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, sizeof (UINT2));

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsEncodeDoneMsg                                    */
/*                                                                           */
/* Description        : This function forms Done message                     */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                    : pu1GrpAddr  - Group Address                          */
/*                                                                           */
/* Output(s)          : ppOutBuf    - pointer to the output buffer           */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsEncodeDoneMsg (UINT4 u4Instance, UINT1 *pu1GrpAddr,
                   tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    tSnoopMLDHdr        MLDHdr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tSnoopMacAddr       SrcMacAddr;
    tSnoopMacAddr       DestMacAddr;
    UINT4               u4PktSize = 0;
    UINT2               u2EthType = 0;
    UINT1               au1DstIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];

    SNOOP_MEM_SET (&MLDHdr, 0, sizeof (tSnoopMLDHdr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&DestMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (au1DstIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* To form leave message allocate buf for MAC (14) header + IPv6 (48 bytes)
     * + MLD (24) header = 86 bytes. 
     */
    SNOOP_MEM_CPY (au1DstIpAddr, gau1MldsAllRtrIp, IPVX_IPV6_ADDR_LEN);

    SNOOP_INET_NTOHL (au1DstIpAddr);

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IPV6_HDR_RTR_OPT_LEN + SNOOP_MLD_HDR_LEN;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buffer allocation failure while constructing MLD done "
                   "message \r\n");
        return SNOOP_FAILURE;
    }

    /* copy 132 (0x83) as a packet type for the MLD done message */
    MLDHdr.u1PktType = SNOOP_MLD_DONE;

    SNOOP_MEM_CPY (au1GrpAddr, pu1GrpAddr, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_INET_HTONL (au1GrpAddr);

    SNOOP_MLDS_GRP_ADDR_COPY (MLDHdr.GrpAddr, au1GrpAddr);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &MLDHdr,
                               SNOOP_MLD_PACKET_START_OFFSET,
                               SNOOP_MLD_HDR_LEN);

    /* Calculate MLD checksum */
    MLDHdr.u2CheckSum = MldsUtilCalculateCheckSum (pBuf,
                                                   SNOOP_MLD_HDR_LEN,
                                                   SNOOP_MLD_PACKET_START_OFFSET,
                                                   gNullSrcAddr, au1DstIpAddr);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &MLDHdr.u2CheckSum,
                               (SNOOP_MLD_PACKET_START_OFFSET + 2),
                               sizeof (UINT2));

    /* Form IPv6 header */
    MldsUtilEncodeIpv6Packet (pBuf, gNullSrcAddr, (UINT1 *) au1GrpAddr,
                              SNOOP_MLD_DONE, SNOOP_MLD_HDR_LEN);

    /* Assign system MAC address as a source MAC address */
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);

    /* Assign 33:33::02 as a destination MAC address */
    SNOOP_GET_MAC_FROM_IPV6 (au1DstIpAddr, DestMacAddr);

    /* Fill Ethernet header */
    CRU_BUF_Copy_OverBufChain (pBuf, DestMacAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IPV6_PKT_TYPE);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, sizeof (UINT2));

    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsEncodeAndSendDoneMsg                             */
/*                                                                           */
/* Description        : This function forms and send the Leave/              */
/*                      INCL() v2 report message                             */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                    : Vlan        - VLAN Identifier                        */
/*                    : pSnoopConsGroupEntry - pointer to the MLDS           */
/*                                             consolidated group entry      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsEncodeAndSendDoneMsg (UINT4 u4Instance, tSnoopTag VlanId,
                          tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry)
{
    tCRU_BUF_CHAIN_HEADER *pV2ReportBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pV1ReportBuf = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1               u1AddressType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* Get Address type from the pSnoopConsGroupEntry */
    u1AddressType = pSnoopConsGroupEntry->GroupIpAddr.u1Afi;

    /* Get the VLAN entry from the VLAN id this is used for constructing 
     * and sending a done / INCL () V2 report based on the operating version */
    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_VLAN_TRC,
                   "Unable to get MLD Vlan record for the given instance "
                   "and the VlanId \r\n");
        return SNOOP_FAILURE;
    }

    /* Check if the Proxy Reporting is enable, if it is enabled then
     * construct the Done/v3 Report message based on the version information
     * available in the MLD VLAN table and forward that packet only on to
     * the MLD Router ports */
    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance, SNOOP_INFO_MLDS_ENTRY)
        == SNOOP_TRUE)
    {
        /* Check if the Version is V2 then construct the INCLUDE NONE
         * report message */
        if (SNOOP_MEM_CMP (pSnoopVlanEntry->V3RtrPortBitmap, gNullPortBitMap,
                           sizeof (tSnoopPortBmp)) != 0)
        {
            if (MldsEncodeV2Report (u4Instance,
                                    pSnoopConsGroupEntry,
                                    SNOOP_TO_INCLUDE, &pV2ReportBuf)
                != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "Forming MLD V2 report (INCL ()) "
                           "message failed \r\n");
                return SNOOP_FAILURE;
            }

            if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                      SNOOP_SSMREPORT_SENT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC, "Unable to update MLDS "
                                "statistics for VLAN ID %d\r\n",
                                pSnoopVlanEntry->VlanId);
            }

            SnoopVlanForwardPacket (u4Instance, pSnoopVlanEntry->VlanId,
                                    SNOOP_ADDR_TYPE_IPV6, SNOOP_INVALID_PORT,
                                    VLAN_FORWARD_SPECIFIC, pV2ReportBuf,
                                    pSnoopVlanEntry->V3RtrPortBitmap, 0);
        }
    }
    if (SNOOP_MEM_CMP (pSnoopVlanEntry->V2RtrPortBitmap, gNullPortBitMap,
                       sizeof (tSnoopPortBmp)) != 0)
    {
        /* If proxy is disabled or if proxy is enabled and oper version
         * is v1 then contruct snd send the done message on to the 
         * router ports */
        if (MldsEncodeDoneMsg (u4Instance,
                               pSnoopConsGroupEntry->GroupIpAddr.au1Addr,
                               &pV1ReportBuf) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "Forming MLD V1 Done message - failed\r\n");
            return SNOOP_FAILURE;
        }

        if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                  SNOOP_LEAVE_SENT) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_VLAN_TRC, "Unable to update MLDS statistics "
                            "for VLAN ID %d\r\n", pSnoopVlanEntry->VlanId);
        }
        SnoopVlanForwardPacket (u4Instance, pSnoopVlanEntry->VlanId,
                                SNOOP_ADDR_TYPE_IPV6, SNOOP_INVALID_PORT,
                                VLAN_FORWARD_SPECIFIC, pV1ReportBuf,
                                pSnoopVlanEntry->V2RtrPortBitmap, 0);
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsEncodeQueryResponse                              */
/*                                                                           */
/* Description        : This function sends response to the query messages   */
/*                                                                           */
/* Input(s)           : u4Instance    - Instance number                      */
/*                      pSnoopVlanEntry - VLAN information                   */
/*                      pSnoopConsGroupEntry- Consolidated Group Information */
/*                      u4InPort - the port on which the query received      */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsEncodeQueryResponse (UINT4 u4Instance, tSnoopVlanEntry * pSnoopVlanEntry,
                         tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry,
                         UINT4 u4InPort)
{
    UINT1              *pPortBitmap = NULL;
    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    UINT1               u1Version = SNOOP_MLD_VERSION2;
    UINT1               u1RecordType = SNOOP_MLD_V1REPORT;

    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "MldsEncodeQueryResponse: "
                   "Error in allocating memory for pPortBitmap\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* if the incoming port is zero, we need to send the message on the 
     * router ports else send it to a single port */
    if (u4InPort != 0)
    {
        SNOOP_ADD_TO_PORT_LIST (u4InPort, pPortBitmap);

        SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                        pRtrPortNode, tSnoopRtrPortEntry *)
        {
            if (pRtrPortNode->u4Port == u4InPort)
            {
                u1Version = pRtrPortNode->u1OperVer;
                break;
            }
        }

        if (u1Version == SNOOP_MLD_VERSION2)
        {
            /* If the oper version is V2 and the forwarding mode is MAC based
             * then set the recordtype as IS_EXCLUDE */
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                u1RecordType = SNOOP_TO_EXCLUDE;
            }
            else
            {
                u1RecordType =
                    ((pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                     SNOOP_IS_INCLUDE : SNOOP_IS_EXCLUDE);
            }
        }

        if (MldsEncodeQueryRespMsg (u4Instance, pSnoopVlanEntry,
                                    pSnoopConsGroupEntry, u1Version,
                                    pPortBitmap, u1RecordType) != SNOOP_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pPortBitmap);
            return SNOOP_FAILURE;
        }

    }
    else
    {
        /* Send the report on all router ports
         * based on the operating version of the router ports
         */
        if (SNOOP_MEM_CMP (pSnoopVlanEntry->V3RtrPortBitmap, gNullPortBitMap,
                           sizeof (tSnoopPortBmp)) != 0)
        {
            /* If the oper version is V2 and the forwarding mode is MAC based
             * then set the recordtype as IS_EXCLUDE */
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                u1RecordType = SNOOP_TO_EXCLUDE;
            }
            else
            {
                u1RecordType =
                    ((pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                     SNOOP_TO_INCLUDE : SNOOP_TO_EXCLUDE);
            }

            u1Version = SNOOP_MLD_VERSION2;

            SNOOP_MEM_CPY (pPortBitmap, pSnoopVlanEntry->V3RtrPortBitmap,
                           sizeof (tSnoopPortBmp));

            if (MldsEncodeQueryRespMsg (u4Instance, pSnoopVlanEntry,
                                        pSnoopConsGroupEntry, u1Version,
                                        pPortBitmap,
                                        u1RecordType) != SNOOP_SUCCESS)
            {
                UtilPlstReleaseLocalPortList (pPortBitmap);
                return SNOOP_FAILURE;
            }
        }

        if (SNOOP_MEM_CMP (pSnoopVlanEntry->V2RtrPortBitmap, gNullPortBitMap,
                           sizeof (tSnoopPortBmp)) != 0)
        {
            u1Version = SNOOP_MLD_VERSION1;
            SNOOP_MEM_CPY (pPortBitmap, pSnoopVlanEntry->V2RtrPortBitmap,
                           sizeof (tSnoopPortBmp));

            if (MldsEncodeQueryRespMsg (u4Instance, pSnoopVlanEntry,
                                        pSnoopConsGroupEntry, u1Version,
                                        pPortBitmap,
                                        u1RecordType) != SNOOP_SUCCESS)
            {
                UtilPlstReleaseLocalPortList (pPortBitmap);
                return SNOOP_FAILURE;
            }
        }

    }

    UtilPlstReleaseLocalPortList (pPortBitmap);
    return SNOOP_SUCCESS;
}

/****************************************************************************/
/* Function Name      : MldsEncodeQueryRespMsg                               */
/*                                                                           */
/* Description        : This function sends response to the query messages   */
/*                      based on the operating version of  the router ports  */
/*                      on which the report will be sent out                 */
/*                                                                           */
/* Input(s)           : u4Instance    - Instance number                      */
/*                      pSnoopVlanEntry - VLAN information                   */
/*                      pSnoopConsGroupEntry- Consolidated Group Information */
/*                      u1Version          - Operating version of router port*/
/*                                           (PortBitmap)                    */
/*                      PortBitmap         - Router port list on which report*/
/*                                           will be sent out                */
/*                      u1RecodeType       - Record Type to be used when v3  */
/*                                           reports are sent out            */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsEncodeQueryRespMsg (UINT4 u4Instance, tSnoopVlanEntry * pSnoopVlanEntry,
                        tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry,
                        UINT1 u1Version, tSnoopPortBmp PortBitmap,
                        UINT1 u1RecordType)
{
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    UINT1               u1StatType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (u1Version == SNOOP_MLD_VERSION2)
    {
        if (MldsEncodeV2Report (u4Instance, pSnoopConsGroupEntry, u1RecordType,
                                &pOutBuf) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "Unable to generate and send MLD V2 report on to "
                       "router ports\r\n");
            return SNOOP_FAILURE;

        }
        /* This u1StatType is used for updating MLDS statistics */
        u1StatType = SNOOP_SSMREPORT_SENT;
    }

    if (u1Version == SNOOP_MLD_VERSION1)
    {
        if (MldsEncodeV1Report (u4Instance, u1RecordType,
                                pSnoopConsGroupEntry->GroupIpAddr,
                                &pOutBuf) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "Unable to generate and send MLD V1 report on to "
                       "router ports\r\n");
            return SNOOP_FAILURE;
        }

        u1StatType = SNOOP_ASMREPORT_SENT;
    }

    SnoopVlanForwardPacket (u4Instance, pSnoopVlanEntry->VlanId,
                            SNOOP_ADDR_TYPE_IPV6, SNOOP_INVALID_PORT,
                            VLAN_FORWARD_SPECIFIC, pOutBuf, PortBitmap,
                            u1StatType);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsEncodeV1Report                                   */
/*                                                                           */
/* Description        : This function frames the V1 packet for the           */
/*                      given Group entry                                    */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      u1PktType   - MLDS packet type                       */
/*                      pu1GroupEntry - Pointer to the Group entry           */
/*                                                                           */
/* Output(s)          : ppOutBuf -  pointer to buffer                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsEncodeV1Report (UINT4 u4Instance, UINT1 u1PktType, tIPvXAddr GroupAddr,
                    tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tSnoopMLDHdr        MLDHdr;
    tSnoopMacAddr       SrcMacAddr;
    tSnoopMacAddr       DestMacAddr;
    UINT4               u4PktSize = 0;
    UINT2               u2EthType = 0;
    UINT1               au1TmpAddr[IPVX_MAX_INET_ADDR_LEN];

    SNOOP_MEM_SET (&MLDHdr, 0, sizeof (tSnoopMLDHdr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&DestMacAddr, 0, sizeof (tSnoopMacAddr));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (au1TmpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    SNOOP_MEM_CPY (au1TmpAddr, GroupAddr.au1Addr, IPVX_MAX_INET_ADDR_LEN);

    /* Get the destination MAC from the incoming group address */
    SNOOP_GET_MAC_FROM_IPV6 (au1TmpAddr, DestMacAddr);

    SNOOP_INET_NTOHL (au1TmpAddr);

    /* Allocate buffer for MAC (14) + IPV6 (48) with router alert option +
     * MLD (24) header = 86 bytes. */
    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IPV6_HDR_RTR_OPT_LEN + SNOOP_MLD_HDR_LEN;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);
    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buffer allocation failure while constructing  "
                   "MLD V1 report message\r\n");
        return SNOOP_FAILURE;
    }

    /* Assign packet type as 131 (0x83) for MLD v1 report messages */
    MLDHdr.u1PktType = u1PktType;

    /* Copy the incoming group address as MLD group address in  MLD header */
    SNOOP_MLDS_GRP_ADDR_COPY (MLDHdr.GrpAddr, au1TmpAddr);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &MLDHdr,
                               SNOOP_MLD_PACKET_START_OFFSET,
                               SNOOP_MLD_HDR_LEN);

    /* Calculate MLD checksum */
    MLDHdr.u2CheckSum = MldsUtilCalculateCheckSum (pBuf, SNOOP_MLD_HDR_LEN,
                                                   SNOOP_MLD_PACKET_START_OFFSET,
                                                   gNullSrcAddr,
                                                   (UINT1 *) GroupAddr.au1Addr);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &MLDHdr.u2CheckSum,
                               SNOOP_MLD_PACKET_START_OFFSET + 2,
                               sizeof (UINT2));

    /* Form IPv6 header */
    MldsUtilEncodeIpv6Packet (pBuf, gNullSrcAddr, au1TmpAddr,
                              SNOOP_MLD_V1REPORT, SNOOP_MLD_HDR_LEN);

    /* Get the source MAC address from the system MAC */
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);

    /* Form ethernet header */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &DestMacAddr, 0,
                               SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SrcMacAddr,
                               SNOOP_ETH_ADDR_SIZE, SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IPV6_PKT_TYPE);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, sizeof (UINT2));

    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsEncodeV2Report                                   */
/*                                                                           */
/* Description        : This function frames the MLD V2 packet for the Group */
/*                      entry                                                */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      pSnoopConsGroupEntry - Pointer to the consolidated   */
/*                                             group entry                   */
/*                      u1RecordType - MLD V2 packet type                    */
/*                                                                           */
/* Output(s)          : ppOutBuf -  pointer to buffer                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsEncodeV2Report (UINT4 u4Instance,
                    tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry,
                    UINT1 u1RecordType, tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    tSnoopSSMReport     SSMReport;
    tSnoopMLDGroupRec   MldGroupRec;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4MaxLength = 0;
    UINT4               u4MldPktSize = 0;
    UINT4               u4PktSize = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2EthType;
    UINT2               u2Index = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               au1Dst[IPVX_IPV6_ADDR_LEN];

    SNOOP_MEM_SET (&SSMReport, 0, sizeof (tSnoopSSMReport));
    SNOOP_MEM_SET (&MldGroupRec, 0, sizeof (tSnoopMLDGroupRec));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (au1Dst, 0, IPVX_IPV6_ADDR_LEN);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8); u2Index++)
    {
        if ((u1RecordType == SNOOP_IS_INCLUDE) ||
            (u1RecordType == SNOOP_TO_INCLUDE))
        {
            OSIX_BITLIST_IS_BIT_SET (pSnoopConsGroupEntry->InclSrcBmp, u2Index,
                                     SNOOP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                u2NoSources++;
            }
        }
        else
        {
            OSIX_BITLIST_IS_BIT_SET (pSnoopConsGroupEntry->ExclSrcBmp, u2Index,
                                     SNOOP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                u2NoSources++;
            }
        }
    }

    /*
     * For MLDS packets the size depends upon the number of sources for 
     * the group.
     * So allocate buf for MAC (14) header + IPV6 with router alert
     * option (48 bytes)
     *  + MLD (24) header + Group Record (20) + (No of srcs * 16). 
     */

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IPV6_HDR_RTR_OPT_LEN + SNOOP_MLD_HDR_LEN +
        SNOOP_MLD_SSMGROUP_RECD_SIZE +
        (u2NoSources * SNOOP_MLD_SSMSOURCE_RECD_SIZE);

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, MLDS_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buffer allocation failure in MLD V2 Aggregation report "
                   "formation\r\n");
        return SNOOP_FAILURE;
    }

    SSMReport.u1PktType = SNOOP_MLD_V2REPORT;

    SSMReport.u2NumGrps = SNOOP_HTONS (0x0001);

    /* Copy MLD v2 header information */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SSMReport,
                               SNOOP_MLD_PACKET_START_OFFSET,
                               SNOOP_MLD_HDR_LEN);

    /* Fill group and source records information in gpSSMRepSendBuffer */
    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);

    MldsUtilFillV2SourceInfo (u4Instance, pSnoopConsGroupEntry, u2NoSources,
                              u1RecordType, &u4MaxLength);

    /* Copy group and source records information */
    CRU_BUF_Copy_OverBufChain (pBuf, gpSSMRepSendBuffer,
                               (SNOOP_MLD_PACKET_START_OFFSET +
                                SNOOP_MLDV2_HDR_LEN), u4MaxLength);

    u4MldPktSize = SNOOP_MLDV2_HDR_LEN + u4MaxLength;

    /* Fill MAC Header, v2 Reports are sent to 33:33::16 (All MLD V2 routers)
     * fill corresponding Dest MAC address */
    SNOOP_MEM_CPY (au1Dst, gau1MldsAllMldv2Rtr, IPVX_IPV6_ADDR_LEN);

    SNOOP_INET_NTOHL (au1Dst);

    /* Calculate MLD checksum */
    SSMReport.u2CheckSum = MldsUtilCalculateCheckSum (pBuf, u4MldPktSize,
                                                      SNOOP_MLD_PACKET_START_OFFSET,
                                                      gNullSrcAddr, au1Dst);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SSMReport.u2CheckSum,
                               (SNOOP_MLD_PACKET_START_OFFSET + 2),
                               sizeof (UINT2));

    /* Form IPV6 Header */
    MldsUtilEncodeIpv6Packet (pBuf, gNullSrcAddr,
                              pSnoopConsGroupEntry->GroupIpAddr.au1Addr,
                              SNOOP_MLD_V2REPORT, (UINT2) u4MldPktSize);

    SNOOP_GET_MAC_FROM_IPV6 (au1Dst, MacGroupAddr);

    SNOOP_MEM_SET (&SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

    /* Set the Switch base MAC Address as Source MAC address */
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);

    /* Form ethernet header */
    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IPV6_PKT_TYPE);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);
    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsEncodeAggV2Report                                */
/*                                                                           */
/* Description        : This function forms the aggregated MLD v2 report     */
/*                      entry                                                */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      u2NumGrpRec - Number of group records                */
/*                      u4MaxLength - packet size (group+source records size)*/
/*                                                                           */
/* Output(s)          : ppOutBuf -  pointer to buffer                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsEncodeAggV2Report (UINT4 u4Instance, UINT2 u2NumGrpRec, UINT4 u4MaxLength,
                       tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    tSnoopSSMReport     SSMReport;
    tSnoopMLDGroupRec   MldGroupRec;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4MldPktSize = 0;
    UINT4               u4PktSize = 0;
    UINT2               u2EthType;
    UINT1               au1DstIpAddr[IPVX_IPV6_ADDR_LEN];

    SNOOP_MEM_SET (&SSMReport, 0, sizeof (tSnoopSSMReport));
    SNOOP_MEM_SET (&MldGroupRec, 0, sizeof (tSnoopMLDGroupRec));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (au1DstIpAddr, 0, IPVX_IPV6_ADDR_LEN);

    /*
     * For MLDS packets the size depends upon the number of sources for 
     * the group.
     * So allocate buf for MAC (14) header + IPV6 with router alert
     * option (48 bytes)
     *  + MLD (24) header + Group Record (20) + (No of srcs * 16). 
     */

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_CPY (au1DstIpAddr, gau1MldsAllMldv2Rtr, IPVX_IPV6_ADDR_LEN);

    SNOOP_INET_NTOHL (au1DstIpAddr);

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IPV6_HDR_RTR_OPT_LEN + SNOOP_MLDV2_HDR_LEN + u4MaxLength;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, MLDS_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buffer allocation failure in MLD V2 Aggregation report "
                   "formation\r\n");
        return SNOOP_FAILURE;
    }

    SSMReport.u1PktType = SNOOP_MLD_V2REPORT;
    SSMReport.u2NumGrps = SNOOP_HTONS (u2NumGrpRec);

    /* Copy MLD v2 header information */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SSMReport,
                               SNOOP_MLD_PACKET_START_OFFSET,
                               SNOOP_MLDV2_HDR_LEN);

    /* Copy group and source records information */
    CRU_BUF_Copy_OverBufChain (pBuf, gpSSMRepSendBuffer,
                               (SNOOP_MLD_PACKET_START_OFFSET +
                                SNOOP_MLDV2_HDR_LEN), u4MaxLength);

    u4MldPktSize = SNOOP_MLDV2_HDR_LEN + u4MaxLength;

    /* Calculate MLD checksum */
    SSMReport.u2CheckSum = MldsUtilCalculateCheckSum (pBuf, u4MldPktSize,
                                                      SNOOP_MLD_PACKET_START_OFFSET,
                                                      gNullSrcAddr,
                                                      au1DstIpAddr);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SSMReport.u2CheckSum,
                               (SNOOP_MLD_PACKET_START_OFFSET + 2),
                               sizeof (UINT2));

    /* Form IPV6 Header */
    MldsUtilEncodeIpv6Packet (pBuf, gNullSrcAddr, gau1MldsAllMldv2Rtr,
                              SNOOP_MLD_V2REPORT, (UINT2) u4MldPktSize);

    /* Fill MAC Header, v2 Reports are sent to 33:33::16 (All MLD V2 routers)
     * fill corresponding Dest MAC address */
    SNOOP_GET_MAC_FROM_IPV6 (au1DstIpAddr, MacGroupAddr);

    SNOOP_MEM_SET (&SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

    /* Set the Switch base MAC Address as Source MAC address */
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);

    /* Form ethernet header */
    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IPV6_PKT_TYPE);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);
    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}
