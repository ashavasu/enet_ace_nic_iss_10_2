/*****************************************************************************/
/*    FILE  NAME            : mldsenc.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : MLD Snooping                                   */
/*    MODULE NAME           : MLD snooping Utility                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                  */
/*    $Id: mldsutil.c,v 1.22 2017/07/27 13:22:29 siva Exp $                */
/*    DESCRIPTION           : This file contains processing routines         */
/*                            for MLD snooping Encoding/Decoding module      */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : MldsUtilEncodeV2SourceInfo                           */
/*                                                                           */
/* Description        : This function Fills source information in the global */
/*                      buffer for a given group record                      */
/*                                                                           */
/* Input(s)           : pSnoopConsGroupEntry - Pointer to the consolidated   */
/*                                             group entry                   */
/*                      u1RecordType   - Group record type to be sent        */
/*                                                                           */
/* Output(s)          : u4MaxLength - Maximum length to be filled            */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MldsUtilFillV2SourceInfo (UINT4 u4Instance,
                          tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry,
                          UINT2 u2NumSrcs, UINT1 u1RecordType,
                          UINT4 *pu4MaxLength)
{
    tSnoopMLDGroupRec   MldGrpRec;
    UINT2               u2Count = 0;
    UINT1               SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&MldGrpRec, 0, sizeof (tSnoopMLDGroupRec));
    SNOOP_MEM_SET (SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    /* Copy group record information to the global MLD v2 send buffer */
    MldGrpRec.u1RecordType = u1RecordType;
    MldGrpRec.u2NumSrcs = SNOOP_HTONS (u2NumSrcs);
    MldGrpRec.u1AuxDataLen = 0;

    SNOOP_MEM_CPY (&MldGrpRec.u1GrpAddr,
                   pSnoopConsGroupEntry->GroupIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (MldGrpRec.u1GrpAddr);

    SNOOP_MEM_CPY ((gpSSMRepSendBuffer + *pu4MaxLength), &MldGrpRec,
                   SNOOP_MLD_SSMGROUP_RECD_SIZE);

    *pu4MaxLength += SNOOP_MLD_SSMGROUP_RECD_SIZE;

    /* for MAC based forwarding source count will be zero */
    if (MldGrpRec.u2NumSrcs != 0)
    {
        for (u2Count = 1; u2Count <= (SNOOP_SRC_LIST_SIZE * 8); u2Count++)
        {
            u1RecordType = pSnoopConsGroupEntry->u1FilterMode;

            /* Sources can be taken based on the Incl/Excl bitmap set
             * for the group. This can be done based on the filter mode
             * set for the group entry  */
            if (u1RecordType == SNOOP_INCLUDE)
            {
                OSIX_BITLIST_IS_BIT_SET (pSnoopConsGroupEntry->InclSrcBmp,
                                         u2Count, SNOOP_SRC_LIST_SIZE, bResult);
            }
            else
            {
                OSIX_BITLIST_IS_BIT_SET (pSnoopConsGroupEntry->ExclSrcBmp,
                                         u2Count, SNOOP_SRC_LIST_SIZE, bResult);
            }

            /* copy the source info to the global MLD v2 send buffer */
            if ((bResult == OSIX_TRUE) && (u2Count <= SNOOP_MAX_MCAST_SRCS))
            {
                SNOOP_MEM_CPY (SrcAddr,
                               SNOOP_SRC_INFO (u4Instance,
                                               (u2Count - 1)).SrcIpAddr.au1Addr,
                               IPVX_IPV6_ADDR_LEN);

                SNOOP_INET_HTONL (SrcAddr);

                SNOOP_MEM_CPY ((gpSSMRepSendBuffer + *pu4MaxLength),
                               SrcAddr, IPVX_MAX_INET_ADDR_LEN);

                *pu4MaxLength += IPVX_MAX_INET_ADDR_LEN;
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : MldsUtilCalculateCheckSum                            */
/*                                                                           */
/* Description        : This function used for calculating the checksum      */
/*                                                                           */
/* Input(s)           : pBuf - pointer buffer                                */
/*                      u4Len - Length of the Packet                         */
/*                      u4Offset - packet offset                             */
/*                      pSrc - Pointer to Source Address                     */
/*                      pDst - Pointer to Destination Address                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Checksum                                             */
/*****************************************************************************/
UINT2
MldsUtilCalculateCheckSum (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Len,
                           UINT4 u4Offset, UINT1 *pSrc, UINT1 *pDst)
{
    tCRU_BUF_DATA_DESC *pCRU = NULL;
    UINT4               u4Sum = 0;
    UINT4               u4DataLen = 0;
    UINT4               u4Count = 0;
    UINT4               u4HalfLen = 0;
    UINT4               u4Length = 0;
    UINT2              *pu2Pointer = NULL;
    UINT2              *pu2Data = NULL;
    UINT2               u2Temp = 0;
    UINT2              *pu2Temp = NULL;
    UINT2               u2Data;
    UINT2               u2Tmp = 0;
    UINT1               au1Data[2];
    UINT1               u1Proto = SNOOP_MLD_NH_ICMPV6;

    SNOOP_MEM_SET (au1Data, 0, 2);

    for (u4Count = 0; u4Count < IPVX_MAX_INET_ADDR_LEN; u4Count = u4Count + 2)
    {
        u2Tmp = *(UINT2 *) (VOID *) &pSrc[u4Count];
        u4Sum += u2Tmp;
    }

    for (u4Count = 0; u4Count < IPVX_MAX_INET_ADDR_LEN; u4Count = u4Count + 2)
    {
        u2Tmp = *(UINT2 *) (VOID *) &pDst[u4Count];
        u4Sum += u2Tmp;
    }

    u4Sum += u4Len >> 16;
    u4Sum += u4Len & 0xffff;
    u4Sum += u1Proto;

    pCRU = pBuf->pFirstValidDataDesc;
    /* Finding in which chain descriptor the read offset present */
    while (pCRU != 0 && (u4Offset + 1) > pCRU->u4_ValidByteCount)
    {
        u4Offset -= pCRU->u4_ValidByteCount;
        pCRU = pCRU->pNext;
    }

    if (pCRU)
    {
        pu2Pointer = (UINT2 *) (VOID *) (pCRU->pu1_FirstValidByte + u4Offset);
        u4Length = pCRU->u4_ValidByteCount - u4Offset;
    }

    u4DataLen = u4Len;
    while (pCRU != 0 && u4DataLen != 0)
    {
        if (u4DataLen < u4Length)
        {
            u4Length = u4DataLen;
        }
        u4DataLen -= u4Length;
        u4HalfLen = u4Length / 2;
        pu2Temp = pu2Pointer;
        while (u4HalfLen != 0)
        {
            u2Temp = *pu2Temp;
            u2Temp = SNOOP_NTOHS (u2Temp);
            u4Sum += u2Temp;
            u4HalfLen -= 1;
            pu2Temp += 1;
        }
        pCRU = pCRU->pNext;
        if (pCRU)
        {
            /* Add the last byte of one chain descriptor to the next for
             * check sum calculation. This will occur if the first
             * descriptor length is odd number */
            if (u4Length % 2 != 0)
            {
                au1Data[0] = (UINT1) (*(pu2Pointer + (u4Length / 2)));
                au1Data[1] = *pCRU->pu1_FirstValidByte;;
                pu2Data = (UINT2 *) (VOID *) au1Data;
                *pu2Data = SNOOP_NTOHS (*pu2Data);
                u4Sum += *pu2Data;
                u4Length = pCRU->u4_ValidByteCount - 1;
                pu2Pointer = (UINT2 *) (VOID *) (pCRU->pu1_FirstValidByte + 1);
                u4DataLen -= 1;
            }
            else
            {
                u4Length = pCRU->u4_ValidByteCount;
                pu2Pointer = (UINT2 *) (VOID *) pCRU->pu1_FirstValidByte;
            }
        }
        else
        {
            /* add the last byte to the check sum if total size 
             * is odd number */
            if (u4Length % 2 != 0)
            {
                u2Data = *(pu2Pointer + (u4Length / 2));
                u2Data = SNOOP_NTOHS (u2Data);
                u2Data = (UINT2) (u2Data & 0xff00);
                u4Sum += u2Data;
            }
        }

    }
    SNOOP_BUF_READ_OFFSET (pBuf) = SNOOP_BUF_READ_OFFSET (pBuf) + u4Len;

    /* Repeat while overflow bit exists */
    while (u4Sum >> 16)
        u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    return SNOOP_NTOHS ((UINT2) ~(u4Sum & 0xffff));
}

/****************************************************************************/
/* Function           : MldsUtilEncodeIpv6Packet                            */
/*  Description       : This function forms the IP header of IGMP Query     */
/*                    : packet.                                             */
/* Input(s)           : pBuf - IGMP PDU buffer                              */
/*                    : pu1Src - pointer to the source IPv6 address         */
/*                    : pu1Dest - pointer to the Destination IPv6 address   */
/*                    : u2Len - Length of PDU                               */
/*                                                                          */
/* Output(s)          : None.                                               */
/*                                                                          */
/* Returns            : None                                                */
/****************************************************************************/
VOID
MldsUtilEncodeIpv6Packet (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Src,
                          UINT1 *pu1Dest, UINT1 u1PktType, UINT2 u2Len)
{
    tIp6Hdr             Ipv6Hdr;
    UINT1               au1HopByHop[SNOOP_MLD_HOP_BY_HOP_HDR_LEN];
    UINT2               u2Word = 0;
    SNOOP_MEM_SET (au1HopByHop, 0, SNOOP_MLD_HOP_BY_HOP_HDR_LEN);

    /* Set ipv6 version as 6 in u4Head */
    Ipv6Hdr.u4Head = SNOOP_IPV6_HDR_VERSION;

    /* Set MLD packet length as a payload length in u2Len */
    u2Len += SNOOP_MLD_HOP_BY_HOP_HDR_LEN;
    Ipv6Hdr.u2Len = SNOOP_HTONS (u2Len);

    /* Set ICMPv6 as a next header protocol value */
    Ipv6Hdr.u1Nh = SNOOP_MLD_NH_HOP_BY_HOP;

    /* Set Hop limit as 1 */
    Ipv6Hdr.u1Hlim = 1;

    /* Source ip address is always set as 0 */
    SNOOP_MLDS_GRP_ADDR_COPY (Ipv6Hdr.srcAddr, pu1Src);

    switch (u1PktType)
    {
        case SNOOP_MLD_QUERY:
            SNOOP_MLDS_GRP_ADDR_COPY (Ipv6Hdr.dstAddr, gau1MldsAllHostIp);
            break;

        case SNOOP_MLD_V2REPORT:
            SNOOP_MLDS_GRP_ADDR_COPY (Ipv6Hdr.dstAddr, gau1MldsAllMldv2Rtr);
            break;

        case SNOOP_MLD_DONE:
            SNOOP_MLDS_GRP_ADDR_COPY (Ipv6Hdr.dstAddr, gau1MldsAllRtrIp);
            break;

        default:
            SNOOP_MLDS_GRP_ADDR_COPY (Ipv6Hdr.dstAddr, pu1Dest);
            break;
    }

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &Ipv6Hdr,
                               SNOOP_DATA_LINK_HDR_LEN,
                               (SNOOP_IPV6_HDR_RTR_OPT_LEN -
                                SNOOP_MLD_HOP_BY_HOP_HDR_LEN));

    /* Fill Hop by Hop header */
    au1HopByHop[0] = SNOOP_MLD_NH_ICMPV6;    /* next header - ICMPv6 */
    au1HopByHop[1] = 0;            /* Length */

    /* Fill 0 for padding 2 bytes */
    SNOOP_MEM_CPY (&au1HopByHop[2], &u2Word, sizeof (UINT2));

    /* indicates router alert option */
    au1HopByHop[4] = SNOOP_MLD_HOP_BY_HOP_OPT_VAL;
    /* Router alert option length */
    au1HopByHop[5] = SNOOP_MLD_HOP_BY_HOP_OPT_LEN;

    /* Fill 0 to indicate the next header is the MLD */
    SNOOP_MEM_CPY (&au1HopByHop[6], &u2Word, sizeof (UINT2));

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) au1HopByHop,
                               (SNOOP_DATA_LINK_HDR_LEN +
                                SNOOP_IPV6_HDR_RTR_OPT_LEN -
                                SNOOP_MLD_HOP_BY_HOP_HDR_LEN),
                               SNOOP_MLD_HOP_BY_HOP_HDR_LEN);
    return;
}

/*****************************************************************************/
/* Function Name      : MldsUtilValidatePacket                               */
/*                                                                           */
/* Description        : This function validates the IPv6 mutlicast packets   */
/*                      and extract the header information                   */
/*                                                                           */
/* Input(s)           : pBuf   - packet  whose type is to be identified      */
/*                      u4Instance - Instance Identifier                     */
/*                      pMldsPktInfo - Packet information where already      */
/*                      decoded info is present                              */
/*                                                                           */
/* Output(s)          : pMldsPktInfo - The structure contains the MLD packet */
/*                                    information                            */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsUtilValidatePkt (UINT4 u4Instance, tCRU_BUF_CHAIN_HEADER * pBuf,
                     tSnoopPktInfo * pMldsPktInfo)
{
    UINT2              *pu2EthType = NULL;
    tSnoopMLDHdr        MldPkt;
    UINT4               u4MldHdrOffset = 0;
    UINT2               u2EthType = 0;

    MEMSET (&MldPkt, 0, sizeof (tSnoopMLDHdr));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* Copy Ethernet type from the ethernet header */
    pu2EthType = (UINT2 *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
        (pBuf, SNOOP_ETH_TYPE_OFFSET + pMldsPktInfo->u1TagLen,
         SNOOP_ETH_TYPE_SIZE);

    if (pu2EthType == NULL)
    {
        /* The header is not contiguous in the buffer */
        pu2EthType = &u2EthType;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pu2EthType,
                                   SNOOP_ETH_TYPE_OFFSET, sizeof (UINT2));
    }

    if (SNOOP_NTOHS (*pu2EthType) != SNOOP_IPV6_PKT_TYPE)
    {
        pMldsPktInfo->u1PktType = SNOOP_NON_IP_PKT;
        SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Received a non-ipv6 packet\r\n");
        return SNOOP_FAILURE;
    }

    if (MldsUtilExtractIpHdrInfo (pBuf, &MldPkt, pMldsPktInfo) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Unable to extract IPv6 header information\r\n");
        return SNOOP_FAILURE;
    }

    if (pMldsPktInfo->u1Protocol == SNOOP_MLD_PROTOCOL)
    {
        u4MldHdrOffset = pMldsPktInfo->u2PktLen;
        if (MldsUtilVerifyMldPkt (pMldsPktInfo, MldPkt, u4MldHdrOffset)
            != SNOOP_SUCCESS)
        {
            /* Probably IGMP hdr checksum failure */
            SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC, "MLD packet verification failure due "
                       "to invalid checksum or packet type\r\n");
            return SNOOP_FAILURE;
        }
    }
    else
    {
        if (MldsUtilVerifyRtrControlMsg (pBuf, pMldsPktInfo) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC, "Invalid router control message\r\n");
            return SNOOP_FAILURE;
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsUtilExtractIpHdrInfo                             */
/*                                                                           */
/* Description        : This routine gets the IPv6 header in the packet.     */
/*                                                                           */
/* Input(s)           : pBuf - IPv6 Message Buffer                           */
/*                                                                           */
/* Output(s)          : pMldsPktInfo - structure in which the header info    */
/*                      is filled                                            */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
MldsUtilExtractIpHdrInfo (tCRU_BUF_CHAIN_HEADER * pBuf, tSnoopMLDHdr * pMldPkt,
                          tSnoopPktInfo * pMldsPktInfo)
{
    tIp6Hdr            *pIp6Hdr = NULL;
    tIp6Hdr             TmpIp6Hdr;
    tIp6Addr            tempAddr;
    UINT1               u1NextHdr;
    UINT4               u4ProcessedLen, u4TotalLen;
    UINT4               u4MldHdrOffset = 0;
    UINT1               u1IsMldRtrAlert = 0;
    UINT1               u1TagLen = 0;
    UINT1               u1AhLen = 0;

    SNOOP_MEM_SET (&TmpIp6Hdr, 0, sizeof (tIp6Hdr));

    u1TagLen = pMldsPktInfo->u1TagLen;

    pIp6Hdr = (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
        (pBuf, SNOOP_DATA_LINK_HDR_LEN + u1TagLen, sizeof (tIp6Hdr));

    if (pIp6Hdr == NULL)
    {
        /* The header is not contiguous in the buffer */
        pIp6Hdr = &TmpIp6Hdr;
        /* Copy the header */
        CRU_BUF_Copy_FromBufChain
            (pBuf, (UINT1 *) pIp6Hdr,
             SNOOP_DATA_LINK_HDR_LEN + u1TagLen, sizeof (tIp6Hdr));
    }
    else
    {
        /* MEMCPY done here to avoid alignment issue in ARM processor
         * ie. Ip6Hdr starts from 12th byte of MLD packet. It should
         * start from 14 byte of MLD packet */

        MEMCPY ((UINT1 *) (&TmpIp6Hdr), pIp6Hdr, sizeof (tIp6Hdr));
        pIp6Hdr = &TmpIp6Hdr;
    }

    pMldsPktInfo->u2PktLen = SNOOP_NTOHS (pIp6Hdr->u2Len);

/*
    if (pMldsPktInfo->u2PktLen == 0)
    {
       Indicates Jumbogram hop-by-hop option 
      return SNOOP_FAILURE;
    }
*/
    pMldsPktInfo->SrcIpAddr.u1Afi = SNOOP_ADDR_TYPE_IPV6;
    pMldsPktInfo->SrcIpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;

    tempAddr.u4_addr[0] = SNOOP_NTOHL (pIp6Hdr->srcAddr.u4_addr[0]);
    tempAddr.u4_addr[1] = SNOOP_NTOHL (pIp6Hdr->srcAddr.u4_addr[1]);
    tempAddr.u4_addr[2] = SNOOP_NTOHL (pIp6Hdr->srcAddr.u4_addr[2]);
    tempAddr.u4_addr[3] = SNOOP_NTOHL (pIp6Hdr->srcAddr.u4_addr[3]);

    SNOOP_MEM_CPY (pMldsPktInfo->SrcIpAddr.au1Addr, &tempAddr,
                   IPVX_IPV6_ADDR_LEN);

    pMldsPktInfo->DestIpAddr.u1Afi = SNOOP_ADDR_TYPE_IPV6;
    pMldsPktInfo->DestIpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;

    tempAddr.u4_addr[0] = SNOOP_NTOHL (pIp6Hdr->dstAddr.u4_addr[0]);
    tempAddr.u4_addr[1] = SNOOP_NTOHL (pIp6Hdr->dstAddr.u4_addr[1]);
    tempAddr.u4_addr[2] = SNOOP_NTOHL (pIp6Hdr->dstAddr.u4_addr[2]);
    tempAddr.u4_addr[3] = SNOOP_NTOHL (pIp6Hdr->dstAddr.u4_addr[3]);

    SNOOP_MEM_CPY (pMldsPktInfo->DestIpAddr.au1Addr, &tempAddr,
                   IPVX_IPV6_ADDR_LEN);

    u4ProcessedLen = SNOOP_IP6_HDR_LEN + SNOOP_DATA_LINK_HDR_LEN + u1TagLen;

    if (pIp6Hdr->u1Nh == 0)        /* If Next Header is Hop-by-Hop
                                   Options header */
    {
        u1NextHdr = pIp6Hdr->u1Nh;
        u4TotalLen = SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
            SNOOP_IP6_HDR_LEN + pMldsPktInfo->u2PktLen;
        if (MldsUtilPrcsIp6HbyhOptHdr (pBuf, &u4TotalLen, &u4ProcessedLen,
                                       &u1IsMldRtrAlert, &u1NextHdr)
            == SNOOP_FAILURE)
        {
            return SNOOP_FAILURE;
        }

        /* If Nextheader is ICMP6 and If MLD rtr alert option
           set in the packet validate the MLD protocol message */
        if ((u1NextHdr == SNOOP_MLD_NH_ICMPV6) && (u1IsMldRtrAlert == 1))
        {

            u4MldHdrOffset = u4ProcessedLen;
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pMldPkt,
                                       u4MldHdrOffset, SNOOP_MLD_HDR_LEN);
            if (SNOOP_IS_MLD_PACKET (pMldPkt))
            {
                pMldsPktInfo->u1Protocol = SNOOP_MLD_PROTOCOL;
            }
            else
            {
                pMldsPktInfo->u1Protocol = SNOOP_DATA_PKT;
            }
        }

        pMldsPktInfo->u1IpHdrLen =
            (UINT1) (u4ProcessedLen - (SNOOP_DATA_LINK_HDR_LEN + u1TagLen));

        return SNOOP_SUCCESS;
    }

/* To check how to handle the snooped ND6 packet 
  
    if ((u1NextHdr == SNOOP_MLD_NH_ICMPV6) && SNOOP_IS_ND6_PACKET(pMldPkt) )
    {
               pMldsPktInfo->u1Protocol = SNOOP_DATA_PKT;
    }
*/

    pMldsPktInfo->u1IpHdrLen =
        (UINT1) (u4ProcessedLen - (SNOOP_DATA_LINK_HDR_LEN + u1TagLen));

    if (pIp6Hdr->u1Nh == SNOOP_PIM_CTRL_MSG)
    {
        pMldsPktInfo->u1Protocol = SNOOP_PIM_CTRL_MSG;
    }
    else if (pIp6Hdr->u1Nh == SNOOP_OSPF_CTRL_MSG)
    {
        pMldsPktInfo->u1Protocol = SNOOP_OSPF_CTRL_MSG;
    }
    else if (pIp6Hdr->u1Nh == SNOOP_IPV6_AUTH_HDR)    /* value 51 */
    {
        u1NextHdr = 0;

        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1NextHdr,
                                   u4ProcessedLen, SNOOP_OFFSET_ONE);
        u4ProcessedLen++;

        if (u1NextHdr == SNOOP_OSPF_CTRL_MSG)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1AhLen,
                                       u4ProcessedLen, SNOOP_OFFSET_ONE);

            /* SNOOP_AH_FIXED_PART_LEN is a defined constant */
            pMldsPktInfo->u1IpHdrLen = (UINT1) (pMldsPktInfo->u1IpHdrLen +
                                                SNOOP_AH_FIXED_PART_LEN +
                                                (u1AhLen * 4));

            pMldsPktInfo->u1Protocol = SNOOP_OSPF_CTRL_MSG;
        }
        else
        {
            pMldsPktInfo->u1Protocol = SNOOP_DATA_PKT;
        }
    }
    else
    {
        pMldsPktInfo->u1Protocol = SNOOP_DATA_PKT;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsUtilPrcsIp6HbyhOptHdr                            */
/*                                                                           */
/* DESCRIPTION        : Handles the processing of the HBYH header on the     */
/*                      input side                                           */
/*                                                                           */
/* Input(s)           : pBuf - IPv6 Message Buffer                           */
/*               : pu4TotlLen - Total Length of the packet including the*/
/*                                   IPv6 Header                             */
/*                    : pIp6 - IPv6 Header Pointer                           */
/*                                                                           */
/* Output(s)          : pu4Len - Processed Length                            */
/*                                                                           */
/*                      pu1IsMldRtrAlert - Flag to check the presence of     */
/*                                         router alert option               */
/*                      pu1NextHdr       - Next Header Value                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsUtilPrcsIp6HbyhOptHdr (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4TotLen,
                           UINT4 *pu4Len, UINT1 *pu1IsMldRtrAlert,
                           UINT1 *pu1NextHdr)
{
    /* structure for DestOptHeader and Hob-by-Hop are same */
    tSnoopExtHdr       *pHbyhHdr, HbyhHdr;
    UINT1               u1OptType;
    UINT4               u4BalanceBytes = 0;
    UINT4               u4ProcessedLen;
    INT4                i4Status = SNOOP_SUCCESS;

    SNOOP_BUF_READ_OFFSET (pBuf) = *pu4Len;

    if ((pHbyhHdr = (tSnoopExtHdr *) SnoopBufRead (pBuf, (UINT1 *) &HbyhHdr,
                                                   SNOOP_BUF_READ_OFFSET (pBuf),
                                                   SNOOP_IP6_TYPE_LEN_BYTES,
                                                   FALSE)) == 0)
    {
        return SNOOP_FAILURE;
    }

    *pu4Len += ((pHbyhHdr->u1HdrLen + 1) * SNOOP_BYTES_IN_OCTECT);
    *pu1NextHdr = pHbyhHdr->u1NextHdr;

    EXTRACT_1_BYTE (pBuf, u1OptType);
    u4BalanceBytes = ((pHbyhHdr->u1HdrLen + 1) * SNOOP_BYTES_IN_OCTECT) - 2;

    while (u4BalanceBytes != 0)
    {
        switch (u1OptType)
        {

            case SNOOP_MLDS_PAD1_OPTION:
                u4ProcessedLen = SNOOP_BUF_READ_OFFSET (pBuf);
                MldsUtilPrcsIp6Pad1Opt (pBuf, &u1OptType, &u4BalanceBytes,
                                        &u4ProcessedLen);
                SNOOP_BUF_READ_OFFSET (pBuf) = u4ProcessedLen;
                break;

            case SNOOP_MLDS_PADN_OPTION:
                MldsUtilPrcsIp6PadNOpt (pBuf, &u4BalanceBytes, &u1OptType);
                break;

            case SNOOP_MLDS_JMB_OPT_TYPE:
                if (MldsUtilPrcsIp6JmbOpt
                    (pBuf, &u4BalanceBytes, pu4TotLen,
                     &u1OptType) == SNOOP_FAILURE)
                    return SNOOP_FAILURE;
                break;

            case SNOOP_MLDS_ROUTE_ALERT_OPT:
                i4Status = MldsUtilPrcsIp6RtAlertOpt (pBuf, &u4BalanceBytes,
                                                      pu1IsMldRtrAlert,
                                                      &u1OptType);
                if (i4Status == SNOOP_FAILURE)
                {
                    /* Error in handling option. */
                    return SNOOP_FAILURE;
                }
                break;

            default:
                if (MldsUtilPrcsIp6UnRecOpt (pBuf, &u1OptType,
                                             &u4BalanceBytes) == SNOOP_FAILURE)
                    return SNOOP_FAILURE;
                break;
        }
    }
    return i4Status;
}

/*****************************************************************************/
/* Function Name      : MldsUtilPrcsIp6Pad1Opt                               */
/*                                                                           */
/* DESCRIPTION        : Handles the processing of Pad1 Option                */
/*                                                                           */
/* Input(s)           : pBuf - IPv6 Message Buffer                           */
/*                                                                           */
/* Output(s)          : pu1OptType      -  Next Option value                 */
/*                                                                           */
/*                    : pu4BalanceBytes -   Balance bytes remaining          */
/*                                                                           */
/*                    : pu4ProcessedLen - Processed Length                   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsUtilPrcsIp6Pad1Opt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1OptType,
                        UINT4 *pu4BalanceBytes, UINT4 *pu4ProcessedLen)
{
    UINT1               u1TmpOptType;
    UINT1              *pu1TmpOptType = NULL;

    *pu4ProcessedLen += 1;

    /* u4BalanceByte is decremented for the Bytes Extracted */
    *pu4BalanceBytes -= 1;

    if ((pu1TmpOptType = SnoopBufRead (pBuf, &u1TmpOptType, *pu4ProcessedLen,
                                       sizeof (UINT1), TRUE)) == NULL)
    {
        return SNOOP_FAILURE;
    }
    *pu1OptType = *pu1TmpOptType;
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsUtilPrcsIp6UnRecOpt                              */
/*                                                                           */
/* DESCRIPTION        : Handles the processing of Unrecognized Options       */
/*                                                                           */
/* Input(s)           : pBuf            - IPv6 Message Buffer                */
/*                                                                           */
/* Output(s)          : pu1OptType      -  Next Option value                 */
/*                                                                           */
/*                    : pu4BalanceBytes -   Balance bytes remaining          */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsUtilPrcsIp6UnRecOpt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1OptType,
                         UINT4 *pu4BalanceBytes)
{
    UINT2               u2OptDataLen;
    UINT4               u4OptDataLen = 0;

    if ((*pu1OptType & SNOOP_MLDS_DEST_HDR_OPTION_00) == *pu1OptType)
    {
        /* skip and continue processing the header */
        SNOOP_BUF_READ_OFFSET (pBuf) += 1;
        EXTRACT_1_BYTE (pBuf, u2OptDataLen);
        SNOOP_BUF_READ_OFFSET (pBuf) += (u2OptDataLen + 1);
        EXTRACT_1_BYTE (pBuf, *pu1OptType);

        if (*pu4BalanceBytes < (u4OptDataLen = u2OptDataLen + 2))
        {
            return SNOOP_FAILURE;
        }

        *pu4BalanceBytes -= (u2OptDataLen + 2);
        return SNOOP_SUCCESS;
    }

    if ((*pu1OptType & SNOOP_MLDS_DEST_HDR_OPTION_01) == *pu1OptType)
    {
        /* discard the packet */
        return SNOOP_FAILURE;
    }

    if ((*pu1OptType & SNOOP_MLDS_DEST_HDR_OPTION_10) == *pu1OptType)
    {
        /* discard the packet */
        return SNOOP_FAILURE;
    }

    if ((*pu1OptType & SNOOP_MLDS_DEST_HDR_OPTION_11) == *pu1OptType)
    {
        /* discard the packet */
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsUtilIp6JmbHdrValid                               */
/*                                                                           */
/* DESCRIPTION : Validates the jumbo header in the received ipv6 pkt.        */
/*                                                                           */
/* Input(s)           :                                                      */
/*                    : pIp6            - IPv6 Header Pointer                */
/*                                                                           */
/*                    : pJmbHdr         -  IPv6 Jumbo Header                 */
/*                                                                           */
/*                    : u4TotLen        - Total Length                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsUtilIp6JmbHdrValid (tSnoopJmbHdr * pJmbHdr, UINT4 u4TotLen)
{
    UINT1               u1HbyhFlag;
    UINT1               u1ErrFlag;
    UINT1               u1PayloadLenFlag = FALSE;
    UINT1               u1JmbFlag;

    u1HbyhFlag = TRUE;
    u1ErrFlag = FALSE;
    u1JmbFlag = FALSE;

    if (u4TotLen - SNOOP_IP6_HDR_LEN == 0)
    {
        u1PayloadLenFlag = TRUE;
    }
    if (pJmbHdr->u1OptType == SNOOP_MLDS_JMB_OPT_TYPE)
    {
        u1JmbFlag = TRUE;
    }
    if (u1PayloadLenFlag && u1HbyhFlag && !u1JmbFlag)
    {

        u1ErrFlag = TRUE;
    }
    if (!u1PayloadLenFlag && u1JmbFlag)
    {
        u1ErrFlag = TRUE;
    }
    if (u1JmbFlag && ((SNOOP_NTOHL (pJmbHdr->u4JmbLen) <
                       SNOOP_MLDS_JMB_MIN_PAYLOAD_LEN)))
    {
        u1ErrFlag = TRUE;
    }
    if (u1ErrFlag == TRUE)
    {
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsUtilPrcsIp6JmbOpt                                */
/*                                                                           */
/* DESCRIPTION        : Handles the processing of Jumbogram option           */
/*                                                                           */
/* Input(s)           : pBuf            - IPv6 Message Buffer                */
/*                                                                           */
/*                    : pIp6            - IPv6 Header Pointer                */
/*                                                                           */
/*                    : pu4BalanceBytes - balance remaining bytes            */
/*                                                                           */
/*                    : pu4TotLen        - Total Length                      */
/*                                                                           */
/*                    : pu1OptValue      - Next Option Value                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsUtilPrcsIp6JmbOpt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BalanceBytes,
                       UINT4 *pu4TotLen, UINT1 *pu1OptValue)
{
    tSnoopJmbHdr       *pJmbHdr, JmbHdr;

    if ((pJmbHdr =
         (tSnoopJmbHdr *) SnoopBufRead (pBuf, (UINT1 *) &JmbHdr,
                                        SNOOP_BUF_READ_OFFSET (pBuf),
                                        sizeof (tSnoopJmbHdr) - 2, FALSE)) == 0)
    {
        return SNOOP_FAILURE;
    }

    if (MldsUtilIp6JmbHdrValid (pJmbHdr, *pu4TotLen) == SNOOP_FAILURE)
        return SNOOP_FAILURE;

    /* Payload length of the IPv6 packet is taken from Jumbogram header */
    *pu4TotLen = pJmbHdr->u4JmbLen;

    /*Next u1ByteToRead is the Next Byte */

    EXTRACT_1_BYTE (pBuf, *pu1OptValue);

    /* u4BalanceByte is decremented for the Bytes Extracted */
    *pu4BalanceBytes -= SNOOP_MLDS_JMB_HDR_LEN;

    return SNOOP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : MldsUtilPrcsIp6RtAlertOpt                            */
/*                                                                           */
/* DESCRIPTION        : Handles the processing of Router Alert Option        */
/*                                                                           */
/* Input(s)           : pBuf            - IPv6 Message Buffer                */
/*                                                                           */
/*                    : pIp6            - IPv6 Header Pointer                */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pu4BalanceBytes  - balance remaining bytes           */
/*                                                                           */
/*                    : pu1IfsMldRtrAlert- Flag to denote the presence of    */
/*                                         Router Alert Option               */
/*                                                                           */
/*                    : pu1OptValue      - Next Option Value                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsUtilPrcsIp6RtAlertOpt (tCRU_BUF_CHAIN_HEADER * pBuf,
                           UINT4 *pu4BalanceBytes, UINT1 *pu1IsMldRtrAlert,
                           UINT1 *pu1OptValue)
{
    tSnoopRtAlertHdr    RtAlertHdr;

    if (SnoopBufRead (pBuf, (UINT1 *) &RtAlertHdr, SNOOP_BUF_READ_OFFSET (pBuf),
                      sizeof (tSnoopRtAlertHdr), TRUE) == NULL)
    {
        return SNOOP_FAILURE;
    }

    *pu4BalanceBytes -= sizeof (tSnoopRtAlertHdr);
    *pu1OptValue += sizeof (tSnoopRtAlertHdr);

    if (RtAlertHdr.u2OptValue == SNOOP_MLDS_ICMP6_MLD_PKT)
    {
        *pu1IsMldRtrAlert = 1;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsUtilPrcsIp6PadNOpt                               */
/*                                                                           */
/* DESCRIPTION        : Handles the processing of PadN Option                */
/*                                                                           */
/* Input(s)           : pBuf            - IPv6 Message Buffer                */
/*                                                                           */
/* Output(s)          : pu4BalanceBytes  - balance remaining bytes           */
/*                                                                           */
/*                    : pu1OptValue      - Next Option Value                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsUtilPrcsIp6PadNOpt (tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT4 *pu4BalanceBytes, UINT1 *pu1OptValue)
{
    UINT1               u1OptDataLen;

    SNOOP_BUF_READ_OFFSET (pBuf) += 1;

    EXTRACT_1_BYTE (pBuf, u1OptDataLen);

    SNOOP_BUF_READ_OFFSET (pBuf) += (u1OptDataLen + 1);

    /*Next optiontype is in the Next Byte */
    EXTRACT_1_BYTE (pBuf, *pu1OptValue);

    /* u4BalanceByte is decremented for the Bytes Extracted */
    *pu4BalanceBytes -= (u1OptDataLen + 2);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsUtilVerifyMldPkt                                 */
/*                                                                           */
/* Description        : This routine verifies the MLD  packet type.          */
/*                                                                           */
/* Input(s)           : pMldsPktInfo - Packet inforamtion                    */
/*                      MldPkt        - Mld Header Info                      */
/*                      u4MldHdrOffset- Offset of MLD header in pBuf         */
/*                                                                           */
/* Output(s)          : pMldsPktInfo                                         */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsUtilVerifyMldPkt (tSnoopPktInfo * pMldsPktInfo, tSnoopMLDHdr MldPkt,
                      UINT4 u4MldHdrOffset)
{
    UINT2               u2MldHdrLen = 0;
    tIp6Addr            tempAddr;
    tIp6Addr            tmpAddr;
    UINT1               bResult;

    MEMSET (&tempAddr, 0, sizeof (tIp6Addr));
    MEMSET (&tmpAddr, 0, sizeof (tIp6Addr));

    pMldsPktInfo->GroupAddr.u1Afi = SNOOP_ADDR_TYPE_IPV6;
    pMldsPktInfo->GroupAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
    SNOOP_MEM_CPY (pMldsPktInfo->GroupAddr.au1Addr, &(MldPkt.GrpAddr),
                   IPVX_IPV6_ADDR_LEN);
    SNOOP_INET_NTOHL (pMldsPktInfo->GroupAddr.au1Addr);

    pMldsPktInfo->u2MaxRespCode = MldPkt.u2MaxRespTime;
    pMldsPktInfo->u2MaxRespCode = SNOOP_NTOHS (pMldsPktInfo->u2MaxRespCode);

    u2MldHdrLen = (UINT2) (u4MldHdrOffset - SNOOP_MLD_HOP_BY_HOP_HDR_LEN);

    SNOOP_MEM_CPY (&tempAddr, pMldsPktInfo->DestIpAddr.au1Addr,
                   IPVX_IPV6_ADDR_LEN);

    tmpAddr.u4_addr[0] = SNOOP_NTOHL (tempAddr.u4_addr[0]);
    tmpAddr.u4_addr[1] = SNOOP_NTOHL (tempAddr.u4_addr[1]);
    tmpAddr.u4_addr[2] = SNOOP_NTOHL (tempAddr.u4_addr[2]);
    tmpAddr.u4_addr[3] = SNOOP_NTOHL (tempAddr.u4_addr[3]);

    if (IS_ADDR_MULTI (tmpAddr) == FALSE)
    {
        /*Address not in multicast range */
        SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_PKT_TRC, "Group address is not a mcast address\r\n");
        return SNOOP_FAILURE;
    }

    if (SNOOP_IS_ALL_ROUTERS (tempAddr))
    {
        if (MldPkt.u1PktType == SNOOP_MLD_DONE)
        {
            pMldsPktInfo->u1PktType = SNOOP_MLD_DONE;
        }
        else
        {
            return SNOOP_FAILURE;
        }
    }
    else if ((SNOOP_IS_ALL_NODES (tempAddr))
             || (SNOOP_IS_RESERVED_LINK_SCOPE_MULTI (tempAddr))
             || (SNOOP_IS_RESERVED_NODE_SCOPE_MULTI (tempAddr)))

    {
        if ((pMldsPktInfo->u2MaxRespCode == 0) &&
            (u2MldHdrLen > SNOOP_MLD_HDR_LEN))
        {
            return SNOOP_FAILURE;
        }

        if (MldPkt.u1PktType == SNOOP_MLD_QUERY)
        {

            /* Since all the query messages have the same type 
             * just differntiate the general query message */
            pMldsPktInfo->u1PktType = SNOOP_MLD_QUERY;
            pMldsPktInfo->u1QueryType = SNOOP_GENERAL_QUERY;
        }
        else
        {
            return SNOOP_FAILURE;
        }
    }
    else if (SNOOP_IS_ALL_V2_ROUTERS (tempAddr))
    {
        if (MldPkt.u1PktType == SNOOP_MLD_V2REPORT)
        {
            pMldsPktInfo->u1PktType = SNOOP_MLD_V2REPORT;
        }
        else
        {
            return SNOOP_FAILURE;
        }
    }
    else
    {
        SNOOP_IS_RESERVED_IPV6_MULTI_ADDR (pMldsPktInfo->DestIpAddr, bResult);

        if (bResult == SNOOP_TRUE)
        {
            pMldsPktInfo->u1PktType = SNOOP_INVALID_MLD_PKT;
            return SNOOP_FAILURE;
        }
        else if (bResult == SNOOP_FALSE)
        {
            if (MldPkt.u1PktType == SNOOP_MLD_V2REPORT)
            {
                return SNOOP_FAILURE;
            }

            if ((MldPkt.u1PktType == SNOOP_MLD_V1REPORT) ||
                (MldPkt.u1PktType == SNOOP_MLD_QUERY))
            {
                pMldsPktInfo->u1PktType = MldPkt.u1PktType;
                if (MldPkt.u1PktType == SNOOP_MLD_QUERY)
                {
                    if ((u2MldHdrLen == SNOOP_MLD_HDR_LEN) ||
                        (u2MldHdrLen == SNOOP_MLD_HDR_LEN +
                         SNOOP_MLDV2_QRY_HDR))
                    {
                        pMldsPktInfo->u1QueryType = SNOOP_GRP_QUERY;
                    }
                    else
                    {
                        pMldsPktInfo->u1QueryType = SNOOP_GRP_SRC_QUERY;
                    }
                }
            }
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsUtilVerifyRtrControlMsg                          */
/*                                                                           */
/* Description        : This function verifies whether the incoming message  */
/*                      is a router control message                          */
/*                                                                           */
/* Input(s)           : pBuf        - Message                                */
/*                      pMldsPktInfo - Packet Inforamrtion                   */
/*                                                                           */
/* Output(s)          : pu1PktType - Packet type                             */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsUtilVerifyRtrControlMsg (tCRU_BUF_CHAIN_HEADER * pBuf,
                             tSnoopPktInfo * pMldsPktInfo)
{
    UINT1               u1PktInfo = 0;
    tIp6Addr            tempAddr;

    MEMSET (&tempAddr, 0, IPVX_IPV6_ADDR_LEN);
    SNOOP_MEM_CPY (&tempAddr, pMldsPktInfo->DestIpAddr.au1Addr,
                   IPVX_IPV6_ADDR_LEN);

    if (SNOOP_IS_ALL_PIM6_ROUTERS (tempAddr))
    {
        if (pMldsPktInfo->u1Protocol == SNOOP_PIM_CTRL_MSG)
        {
            /* Not checking for linear because only one byte is copied */
            CRU_BUF_Copy_FromBufChain (pBuf, &u1PktInfo,
                                       (pMldsPktInfo->u1IpHdrLen +
                                        SNOOP_DATA_LINK_HDR_LEN +
                                        pMldsPktInfo->u1TagLen),
                                       sizeof (UINT1));

            /*
             * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
             * |PIM Ver| Type  | Reserved   |     Checksum           |
             * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
             */

            /* Check PIM Version number is 2 and type is 0 (Hello) */
            if (((u1PktInfo & 0xf0) == SNOOP_PIM_VERSION2)
                && ((u1PktInfo & 0x0f) == SNOOP_PIMV2_HELLO))
            {
                pMldsPktInfo->u1PktType = SNOOP_ROUTER_MSG;
            }
            else
            {
                return SNOOP_FAILURE;
            }
        }
        else
        {
            return SNOOP_FAILURE;
        }
    }

    else if (SNOOP_IS_ALL_OSPFV3_ROUTERS (tempAddr))
    {
        if (pMldsPktInfo->u1Protocol == SNOOP_OSPF_CTRL_MSG)
        {
            /*
             * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
             * |OSPF Version   | Type     |     packet length        |
             * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
             */

            /* Not checking for linear because only one byte is copied */
            CRU_BUF_Copy_FromBufChain (pBuf, &u1PktInfo,
                                       (pMldsPktInfo->u1IpHdrLen +
                                        SNOOP_DATA_LINK_HDR_LEN +
                                        pMldsPktInfo->u1TagLen),
                                       sizeof (UINT1));

            if ((u1PktInfo & 0xff) != SNOOP_OSPF_VERSION_3)
            {
                return SNOOP_FAILURE;
            }

            CRU_BUF_Copy_FromBufChain (pBuf, &u1PktInfo,
                                       (pMldsPktInfo->u1IpHdrLen +
                                        SNOOP_DATA_LINK_HDR_LEN +
                                        pMldsPktInfo->u1TagLen + 1),
                                       sizeof (UINT1));

            /* Check OSPF packet type is 1 (Hello) */
            if ((u1PktInfo & 0xff) != SNOOP_OSPF_HELLO)
            {
                return SNOOP_FAILURE;
            }

            pMldsPktInfo->u1PktType = SNOOP_ROUTER_MSG;
        }
        else
        {
            return SNOOP_FAILURE;
        }
    }
    else
    {
        pMldsPktInfo->u1PktType = SNOOP_DATA_PKT;
    }
    return SNOOP_SUCCESS;
}

VOID               *
SnoopBufRead (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pData, UINT4 u4Offset,
              UINT4 u4Size, UINT1 u1Copy)
{
    UINT1              *pStart;

    SNOOP_DBG_ARG5 (SNOOP_DBG_FLAG, SNOOP_DATA_PATH_TRC, SNOOP_OS_RES_DBG,
                    "IP6LIB:Ip6BufRead:Reading from buf= %p data= %p offset= %d "
                    "size= %d copy= %d\n", pBuf, pData, u4Offset, u4Size,
                    u1Copy);

    /* check if a copy is requested */

    if (u1Copy)
    {
        if (CRU_BUF_Copy_FromBufChain (pBuf, pData, u4Offset, u4Size) == 0)
        {
            SNOOP_DBG_ARG5 (SNOOP_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                            "IP6LIB:Ip6BufRead:CRU_BUF_Copy_FromBufChain fail, "
                            "buf=%p data=%p offset=%d size=%d copy=%d",
                            pBuf, pData, u4Offset, u4Size, u1Copy);
            return (NULL);
        }
        SNOOP_BUF_READ_OFFSET (pBuf) = SNOOP_BUF_READ_OFFSET (pBuf) + u4Size;
        return ((VOID *) pData);
    }

    pStart = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4Size);

    if (pStart == NULL)
    {
        SNOOP_DBG_ARG5 (SNOOP_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                        "IP6LIB:Ip6BufRead:CRU_BUF_Read_FromBufChain fail, "
                        "buf=%p data=%p offset=%d size=%d copy=%d",
                        pBuf, pData, u4Offset, u4Size, u1Copy);

        if (CRU_BUF_Copy_FromBufChain (pBuf, pData, u4Offset, u4Size) == 0)
        {
            SNOOP_DBG_ARG5 (SNOOP_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                            "IP6LIB:Ip6BufRead:CRU_BUF_Copy_FromBufChain fail, "
                            "buf=%p data=%p offset=%d size=%d copy=%d",
                            pBuf, pData, u4Offset, u4Size, u1Copy);
            return (NULL);
        }
        SNOOP_BUF_READ_OFFSET (pBuf) = SNOOP_BUF_READ_OFFSET (pBuf) + u4Size;
        return ((VOID *) pData);
    }

    SNOOP_BUF_READ_OFFSET (pBuf) = SNOOP_BUF_READ_OFFSET (pBuf) + u4Size;

    SNOOP_DBG_ARG2 (SNOOP_DBG_FLAG, SNOOP_DATA_PATH_TRC, SNOOP_OS_RES_DBG,
                    "IP6LIB:Ip6BufRead: Returning pointer = %p local data = %p\n",
                    pStart, pData);

    return ((VOID *) pStart);
}
