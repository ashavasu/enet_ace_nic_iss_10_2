/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldsnpapi.c,v 1.2 2012/12/13 14:40:22 siva Exp $
 *
 * Description: This file contains wrapper function  definitions  for
 *              MLDS module.
 *****************************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : MldsFsMiMldsHwDisableIpMldSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMldsHwDisableIpMldSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMldsHwDisableIpMldSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __MLDS_NP_API_C
#define __MLDS_NP_API_C

#include "snpinc.h"

UINT1
MldsFsMiMldsHwDisableIpMldSnooping (UINT4 u4Instance)
{
    tFsHwNp             FsHwNp;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsNpWrFsMiMldsHwDisableIpMldSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLDS_MOD,    /* Module ID */
                         FS_MI_MLDS_HW_DISABLE_IP_MLD_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMldsNpModInfo = &(FsHwNp.MldsNpModInfo);
    pEntry = &pMldsNpModInfo->MldsNpFsMiMldsHwDisableIpMldSnooping;

    pEntry->u4Instance = u4Instance;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MldsFsMiMldsHwDisableMldSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMldsHwDisableMldSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMldsHwDisableMldSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldsFsMiMldsHwDisableMldSnooping (UINT4 u4Instance)
{
    tFsHwNp             FsHwNp;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsNpWrFsMiMldsHwDisableMldSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLDS_MOD,    /* Module ID */
                         FS_MI_MLDS_HW_DISABLE_MLD_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMldsNpModInfo = &(FsHwNp.MldsNpModInfo);
    pEntry = &pMldsNpModInfo->MldsNpFsMiMldsHwDisableMldSnooping;

    pEntry->u4Instance = u4Instance;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MldsFsMiMldsHwEnableIpMldSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMldsHwEnableIpMldSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMldsHwEnableIpMldSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldsFsMiMldsHwEnableIpMldSnooping (UINT4 u4Instance)
{
    tFsHwNp             FsHwNp;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsNpWrFsMiMldsHwEnableIpMldSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLDS_MOD,    /* Module ID */
                         FS_MI_MLDS_HW_ENABLE_IP_MLD_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMldsNpModInfo = &(FsHwNp.MldsNpModInfo);
    pEntry = &pMldsNpModInfo->MldsNpFsMiMldsHwEnableIpMldSnooping;

    pEntry->u4Instance = u4Instance;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MldsFsMiMldsHwEnableMldSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMldsHwEnableMldSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMldsHwEnableMldSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldsFsMiMldsHwEnableMldSnooping (UINT4 u4Instance)
{
    tFsHwNp             FsHwNp;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsNpWrFsMiMldsHwEnableMldSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLDS_MOD,    /* Module ID */
                         FS_MI_MLDS_HW_ENABLE_MLD_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMldsNpModInfo = &(FsHwNp.MldsNpModInfo);
    pEntry = &pMldsNpModInfo->MldsNpFsMiMldsHwEnableMldSnooping;

    pEntry->u4Instance = u4Instance;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MldsFsMiNpGetIp6FwdEntryHitBitStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiNpGetIp6FwdEntryHitBitStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiNpGetIp6FwdEntryHitBitStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldsFsMiNpGetIp6FwdEntryHitBitStatus (UINT4 u4Instance, tSnoopVlanId VlanId,
                                      UINT1 *pGrpAddr, UINT1 *pSrcAddr)
{
    tFsHwNp             FsHwNp;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsNpWrFsMiNpGetIp6FwdEntryHitBitStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLDS_MOD,    /* Module ID */
                         FS_MI_NP_GET_IP6_FWD_ENTRY_HIT_BIT_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMldsNpModInfo = &(FsHwNp.MldsNpModInfo);
    pEntry = &pMldsNpModInfo->MldsNpFsMiNpGetIp6FwdEntryHitBitStatus;

    pEntry->u4Instance = u4Instance;
    pEntry->VlanId = VlanId;
    pEntry->pGrpAddr = pGrpAddr;
    pEntry->pSrcAddr = pSrcAddr;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MldsFsMiNpUpdateIp6mcFwdEntries                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiNpUpdateIp6mcFwdEntries
 *                                                                          
 *    Input(s)            : Arguments of FsMiNpUpdateIp6mcFwdEntries
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldsFsMiNpUpdateIp6mcFwdEntries (UINT4 u4Instance, tSnoopVlanId VlanId,
                                 UINT1 *pGrpAddr, UINT1 *pSrcAddr,
                                 tPortList PortList, tPortList UntagPortList,
                                 UINT1 u1EventType)
{
    tFsHwNp             FsHwNp;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsNpWrFsMiNpUpdateIp6mcFwdEntries *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLDS_MOD,    /* Module ID */
                         FS_MI_NP_UPDATE_IP6MC_FWD_ENTRIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         2);    /* No. of PortList Parms */
    pMldsNpModInfo = &(FsHwNp.MldsNpModInfo);
    pEntry = &pMldsNpModInfo->MldsNpFsMiNpUpdateIp6mcFwdEntries;

    pEntry->u4Instance = u4Instance;
    pEntry->VlanId = VlanId;
    pEntry->pGrpAddr = pGrpAddr;
    pEntry->pSrcAddr = pSrcAddr;
    pEntry->PortList = PortList;
    pEntry->UntagPortList = UntagPortList;
    pEntry->u1EventType = u1EventType;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : MldsFsMiMldsMbsmHwEnableIpMldSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMldsMbsmHwEnableIpMldSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMldsMbsmHwEnableIpMldSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldsFsMiMldsMbsmHwEnableIpMldSnooping (UINT4 u4ContextId,
                                       tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsNpWrFsMiMldsMbsmHwEnableIpMldSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLDS_MOD,    /* Module ID */
                         FS_MI_MLDS_MBSM_HW_ENABLE_IP_MLD_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMldsNpModInfo = &(FsHwNp.MldsNpModInfo);
    pEntry = &pMldsNpModInfo->MldsNpFsMiMldsMbsmHwEnableIpMldSnooping;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MldsFsMiMldsMbsmHwEnableMldSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiMldsMbsmHwEnableMldSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiMldsMbsmHwEnableMldSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MldsFsMiMldsMbsmHwEnableMldSnooping (UINT4 u4ContextId,
                                     tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;
    tMldsNpWrFsMiMldsMbsmHwEnableMldSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MLDS_MOD,    /* Module ID */
                         FS_MI_MLDS_MBSM_HW_ENABLE_MLD_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMldsNpModInfo = &(FsHwNp.MldsNpModInfo);
    pEntry = &pMldsNpModInfo->MldsNpFsMiMldsMbsmHwEnableMldSnooping;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif

#endif /* __MLDS_NP_API_C */
