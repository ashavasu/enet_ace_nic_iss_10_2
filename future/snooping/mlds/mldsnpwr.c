/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mldsnpwr.c,v 1.3 2017/12/07 09:32:01 siva Exp $
 *
 * Description: This file contains wrapper functions for
 *              MLDS module.
 *****************************************************************************/
/***************************************************************************
 *                                                                          
 *    Function Name       : MldsNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tMldsNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __MLDS_NP_WR_C
#define __MLDS_NP_WR_C

#include "snpinc.h"

PUBLIC UINT1
MldsNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT4               u4RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMldsNpModInfo     *pMldsNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pMldsNpModInfo = &(pFsHwNp->MldsNpModInfo);

    if (NULL == pMldsNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MI_MLDS_HW_DISABLE_IP_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwDisableIpMldSnooping *pEntry = NULL;
            pEntry = &pMldsNpModInfo->MldsNpFsMiMldsHwDisableIpMldSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal = FsMiMldsHwDisableIpMldSnooping (pEntry->u4Instance);
            break;
        }
        case FS_MI_MLDS_HW_DISABLE_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwDisableMldSnooping *pEntry = NULL;
            pEntry = &pMldsNpModInfo->MldsNpFsMiMldsHwDisableMldSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal = FsMiMldsHwDisableMldSnooping (pEntry->u4Instance);
            break;
        }
        case FS_MI_MLDS_HW_ENABLE_IP_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwEnableIpMldSnooping *pEntry = NULL;
            pEntry = &pMldsNpModInfo->MldsNpFsMiMldsHwEnableIpMldSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal = FsMiMldsHwEnableIpMldSnooping (pEntry->u4Instance);
            break;
        }
        case FS_MI_MLDS_HW_ENABLE_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsHwEnableMldSnooping *pEntry = NULL;
            pEntry = &pMldsNpModInfo->MldsNpFsMiMldsHwEnableMldSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal = FsMiMldsHwEnableMldSnooping (pEntry->u4Instance);
            break;
        }
        case FS_MI_NP_GET_IP6_FWD_ENTRY_HIT_BIT_STATUS:
        {
            tMldsNpWrFsMiNpGetIp6FwdEntryHitBitStatus *pEntry = NULL;
            pEntry = &pMldsNpModInfo->MldsNpFsMiNpGetIp6FwdEntryHitBitStatus;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiNpGetIp6FwdEntryHitBitStatus (pEntry->u4Instance,
                                                  pEntry->VlanId,
                                                  pEntry->pGrpAddr,
                                                  pEntry->pSrcAddr);
            break;
        }
        case FS_MI_NP_UPDATE_IP6MC_FWD_ENTRIES:
        {
            tMldsNpWrFsMiNpUpdateIp6mcFwdEntries *pEntry = NULL;
            pEntry = &pMldsNpModInfo->MldsNpFsMiNpUpdateIp6mcFwdEntries;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiNpUpdateIp6mcFwdEntries (pEntry->u4Instance, pEntry->VlanId,
                                             pEntry->pGrpAddr, pEntry->pSrcAddr,
                                             pEntry->PortList,
                                             pEntry->UntagPortList,
                                             pEntry->u1EventType);
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_MLDS_MBSM_HW_ENABLE_IP_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsMbsmHwEnableIpMldSnooping *pEntry = NULL;
            pEntry = &pMldsNpModInfo->MldsNpFsMiMldsMbsmHwEnableIpMldSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiMldsMbsmHwEnableIpMldSnooping (pEntry->u4ContextId,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MI_MLDS_MBSM_HW_ENABLE_MLD_SNOOPING:
        {
            tMldsNpWrFsMiMldsMbsmHwEnableMldSnooping *pEntry = NULL;
            pEntry = &pMldsNpModInfo->MldsNpFsMiMldsMbsmHwEnableMldSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiMldsMbsmHwEnableMldSnooping (pEntry->u4ContextId,
                                                 pEntry->pSlotInfo);
            break;
        }
#endif
        default:
            u4RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return ((UINT1) u4RetVal);
}

#endif /* __MLDS_NP_WR_C */
