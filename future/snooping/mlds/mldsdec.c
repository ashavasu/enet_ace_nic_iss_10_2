/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/* $Id: mldsdec.c,v 1.9 2015/03/11 11:00:09 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : mldsdec.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping MLDS decode module                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains MLDS decoding routines      */
/*                            for snooping (MLD) module                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : MldsDecodeIncomingPacket                             */
/*                                                                           */
/* Description        : This function processes the incoming IPv6  multicast */
/*                      control packets and data packets.                    */
/*                                                                           */
/* Input(s)           : pBuf     - multicast packet                          */
/*                      u4Port   - Physical port on which the pacjet was     */
/*                                 recevied                                  */
/*                                                                           */
/*                      VlanInfo - Vlan info present in received packet      */
/*                      u2TagLen - Tag Length                                */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsDecodeIncomingPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Instance,
                          UINT2 u2LocalPort, tSnoopVlanInfo VlanInfo,
                          UINT2 u2TagLen)
{
    tSnoopPktInfo       MldsPktInfo;
    tSnoopTag           VlanId;

    SNOOP_MEM_SET (&MldsPktInfo, 0, sizeof (tSnoopPktInfo));

    /* 
     * Snooping will be maintaining the virtual port bitmap corresponding to 
     * the physical port bitmap obtained from the multiple instance manager
     * so get the Virtual port number and instance number corresponding to 
     * the physical port from the multiple instance manager
     */

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* Check whether SNOOP  module is started */
    if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_INIT_SHUT_TRC, SNOOP_GBL_NAME,
                       "SNOOP is not started, Packet dropped\r\n");
        return SNOOP_FAILURE;
    }

    /* Check whether MLDS is globally enabled or disabled */
    if (SNOOP_SNOOPING_STATUS (u4Instance, SNOOP_INFO_MLDS_ENTRY)
        == SNOOP_DISABLE)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_INIT_SHUT_TRC, SNOOP_GBL_NAME,
                       "MLDS globally disabled, Packet dropped\r\n");
        return SNOOP_FAILURE;
    }

    MldsPktInfo.u4InPort = (UINT4) u2LocalPort;

    SNOOP_OUTER_VLAN (VlanId) = VlanInfo.OuterVlanTag.u2VlanId;

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        SNOOP_INNER_VLAN (VlanId) = VlanInfo.InnerVlanTag.u2VlanId;
    }
    else
    {
        SNOOP_INNER_VLAN (VlanId) = SNOOP_INVALID_VLAN_ID;
    }

    MldsPktInfo.u1TagLen = (UINT1) u2TagLen;
    /* 
     * Check whether verification of the packet for 
     * 1. Reserved IPv6 address
     * 2. MLD Checksum in case of MLD packet
     * 3. Check for VALID packet type (MLD / ROUTER control message) 
     */
    if (MldsUtilValidatePkt (u4Instance, pBuf, &MldsPktInfo) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Packet validation failed:\r\n");
        return SNOOP_FAILURE;
    }

    if (SnoopProcessMulticastPkt (pBuf, u4Instance, u2LocalPort,
                                  VlanId, &MldsPktInfo) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Packet processing failed:\r\n");
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}
