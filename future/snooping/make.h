#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 27 March 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

SNOOP_SWITCHES = -USRCMAC_CTP_WANTED -USNOOP_EXTN_FORWARD

TOTAL_OPNS =  ${SNOOP_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR    = ${BASE_DIR}/cfa2
CFA_INCD        = ${CFA_BASE_DIR}/inc

BRIDGE_BASE_DIR = ${BASE_DIR}/bridge

SNOOP_BASE_DIR = ${BASE_DIR}/snooping
SNOOP_SRC_DIR  = ${SNOOP_BASE_DIR}/snpcore/src
SNOOP_INC_DIR  = ${SNOOP_BASE_DIR}/snpcore/inc
SNOOP_OBJ_DIR  = ${SNOOP_BASE_DIR}/obj

IGS_SRC_DIR  = ${SNOOP_BASE_DIR}/igs
IGS_INC_DIR  = ${SNOOP_BASE_DIR}/igs

MLDS_SRC_DIR  = ${SNOOP_BASE_DIR}/mlds
MLDS_INC_DIR  = ${SNOOP_BASE_DIR}/mlds

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${SNOOP_INC_DIR} -I${CFA_INCD}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
