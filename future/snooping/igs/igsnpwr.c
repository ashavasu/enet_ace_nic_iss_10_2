/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igsnpwr.c,v 1.4 2017/08/11 13:52:37 siva Exp $
 *
 * Description: This file contains wrapper functions for
 *              snooping(MLDS/IGS) module.
 *****************************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIgsNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

#ifndef __IGS_NP_WR_C
#define __IGS_NP_WR_C

#include "snpinc.h"

PUBLIC UINT1
IgsNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT4               u4RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIgsNpModInfo = &(pFsHwNp->IgsNpModInfo);

    if (NULL == pIgsNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MI_IGS_HW_CLEAR_IPMC_FWD_ENTRIES:
        {
            tIgsNpWrFsMiIgsHwClearIpmcFwdEntries *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwClearIpmcFwdEntries;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            FsMiIgsHwClearIpmcFwdEntries (pEntry->u4Instance);
            break;
        }
        case FS_MI_IGS_HW_DISABLE_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwDisableIgmpSnooping *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwDisableIgmpSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal = FsMiIgsHwDisableIgmpSnooping (pEntry->u4Instance);
            break;
        }
        case FS_MI_IGS_HW_DISABLE_IP_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwDisableIpIgmpSnooping *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwDisableIpIgmpSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal = FsMiIgsHwDisableIpIgmpSnooping (pEntry->u4Instance);
            break;
        }
        case FS_MI_IGS_HW_ENABLE_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwEnableIgmpSnooping *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwEnableIgmpSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal = FsMiIgsHwEnableIgmpSnooping (pEntry->u4Instance);
            break;
        }
        case FS_MI_IGS_HW_ENABLE_IP_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsHwEnableIpIgmpSnooping *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwEnableIpIgmpSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal = FsMiIgsHwEnableIpIgmpSnooping (pEntry->u4Instance);
            break;
        }
        case FS_MI_IGS_HW_ENHANCED_MODE:
        {
            tIgsNpWrFsMiIgsHwEnhancedMode *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwEnhancedMode;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsHwEnhancedMode (pEntry->u4ContextId,
                                       pEntry->u1EnhStatus);
            break;
        }
        case FS_MI_IGS_HW_PORT_RATE_LIMIT:
        {
            tIgsNpWrFsMiIgsHwPortRateLimit *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwPortRateLimit;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsHwPortRateLimit (pEntry->u4Instance,
                                        pEntry->pIgsHwRateLmt,
                                        pEntry->u1Action);
            break;
        }
        case FS_MI_IGS_HW_UPDATE_IPMC_ENTRY:
        {
            tIgsNpWrFsMiIgsHwUpdateIpmcEntry *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwUpdateIpmcEntry;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsHwUpdateIpmcEntry (pEntry->u4Instance,
                                          pEntry->pIgsHwIpFwdInfo,
                                          pEntry->u1Action);
            break;
        }
        case FS_MI_NP_GET_FWD_ENTRY_HIT_BIT_STATUS:
        {
            tIgsNpWrFsMiNpGetFwdEntryHitBitStatus *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiNpGetFwdEntryHitBitStatus;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiNpGetFwdEntryHitBitStatus (pEntry->u4Instance,
                                               pEntry->VlanId,
                                               pEntry->u4GrpAddr,
                                               pEntry->u4SrcAddr);
            break;
        }
        case FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES:
        {
            tIgsNpWrFsMiNpUpdateIpmcFwdEntries *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiNpUpdateIpmcFwdEntries;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiNpUpdateIpmcFwdEntries (pEntry->u4Instance, pEntry->VlanId,
                                            pEntry->u4GrpAddr,
                                            pEntry->u4SrcAddr, pEntry->PortList,
                                            pEntry->UntagPortList,
                                            pEntry->u1EventType);
            break;
        }
        case FS_MI_IGS_HW_ADD_RTR_PORT:
        {
            tIgsNpWrFsMiIgsHwAddRtrPort *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwAddRtrPort;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsHwAddRtrPort (pEntry->u4Instance, pEntry->VlanId,
                                     pEntry->u4Port);
            break;
        }
        case FS_MI_IGS_HW_DEL_RTR_PORT:
        {
            tIgsNpWrFsMiIgsHwDelRtrPort *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwDelRtrPort;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsHwDelRtrPort (pEntry->u4Instance, pEntry->VlanId,
                                     pEntry->u4Port);
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_IGS_MBSM_HW_ENABLE_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsMbsmHwEnableIgmpSnooping *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwEnableIgmpSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsMbsmHwEnableIgmpSnooping (pEntry->u4ContextId,
                                                 pEntry->pSlotInfo);
            break;
        }
        case FS_MI_IGS_MBSM_HW_ENABLE_IP_IGMP_SNOOPING:
        {
            tIgsNpWrFsMiIgsMbsmHwEnableIpIgmpSnooping *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwEnableIpIgmpSnooping;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsMbsmHwEnableIpIgmpSnooping (pEntry->u4ContextId,
                                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MI_IGS_MBSM_HW_ENHANCED_MODE:
        {
            tIgsNpWrFsMiIgsMbsmHwEnhancedMode *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwEnhancedMode;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsMbsmHwEnhancedMode (pEntry->u4ContextId,
                                           pEntry->u1EnhStatus,
                                           pEntry->pSlotInfo);
            break;
        }
        case FS_MI_IGS_MBSM_HW_PORT_RATE_LIMIT:
        {
            tIgsNpWrFsMiIgsMbsmHwPortRateLimit *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwPortRateLimit;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsMbsmHwPortRateLimit (pEntry->u4Instance,
                                            pEntry->pIgsHwRateLmt,
                                            pEntry->u1Action,
                                            pEntry->pSlotInfo);
            break;
        }
        case FS_MI_IGS_MBSM_HW_SPARSE_MODE:
        {
            tIgsNpWrFsMiIgsMbsmHwSparseMode *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwSparseMode;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsMbsmHwSparseMode (pEntry->u4ContextId,
                                         pEntry->u1SparseStatus,
                                         pEntry->pSlotInfo);
            break;
        }
#endif /* MBSM_WANTED */
        case FS_MI_IGS_HW_GET_IP_FWD_ENTRY_HIT_BIT_STATUS:
        {
            tIgsNpWrFsMiIgsHwGetIpFwdEntryHitBitStatus *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwGetIpFwdEntryHitBitStatus;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsHwGetIpFwdEntryHitBitStatus (pEntry->u4Instance,
                                                    pEntry->pIgsHwIpFwdInfo);
            break;
        }
        case FS_MI_IGS_HW_SPARSE_MODE:
        {
            tIgsNpWrFsMiIgsHwSparseMode *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwSparseMode;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            u4RetVal =
                FsMiIgsHwSparseMode (pEntry->u4ContextId,
                                     pEntry->u1SparseStatus);
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_IGS_MBSM_SYNC_I_P_M_C_INFO:
        {
            tIgsNpWrFsMiIgsMbsmSyncIPMCInfo *pEntry = NULL;
            pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmSyncIPMCInfo;
            if (NULL == pEntry)
            {
                u4RetVal = FNP_FAILURE;
                break;
            }
            break;
        }
#endif

        default:
            u4RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return ((UINT1) u4RetVal);
}

#endif /* __IGS_NP_WR_C */
