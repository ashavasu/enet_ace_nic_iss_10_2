/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: igsnpapi.c,v 1.9 2017/09/26 13:12:05 siva Exp $
 *
 * Description: This file contains wrapper functions for
 *              snooping(IGS) module.
 *****************************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwClearIpmcFwdEntries                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwClearIpmcFwdEntries
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwClearIpmcFwdEntries
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __IGS_NP_API_C
#define __IGS_NP_API_C

#include "snpinc.h"

UINT1
IgsFsMiIgsHwClearIpmcFwdEntries (UINT4 u4Instance)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwClearIpmcFwdEntries *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_CLEAR_IPMC_FWD_ENTRIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwClearIpmcFwdEntries;

    pEntry->u4Instance = u4Instance;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwDisableIgmpSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwDisableIgmpSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwDisableIgmpSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwDisableIgmpSnooping (UINT4 u4Instance)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwDisableIgmpSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_DISABLE_IGMP_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwDisableIgmpSnooping;

    pEntry->u4Instance = u4Instance;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwDisableIpIgmpSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwDisableIpIgmpSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwDisableIpIgmpSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwDisableIpIgmpSnooping (UINT4 u4Instance)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwDisableIpIgmpSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_DISABLE_IP_IGMP_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwDisableIpIgmpSnooping;

    pEntry->u4Instance = u4Instance;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwEnableIgmpSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwEnableIgmpSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwEnableIgmpSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwEnableIgmpSnooping (UINT4 u4Instance)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwEnableIgmpSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_ENABLE_IGMP_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwEnableIgmpSnooping;

    pEntry->u4Instance = u4Instance;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwEnableIpIgmpSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwEnableIpIgmpSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwEnableIpIgmpSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwEnableIpIgmpSnooping (UINT4 u4Instance)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwEnableIpIgmpSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_ENABLE_IP_IGMP_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwEnableIpIgmpSnooping;

    pEntry->u4Instance = u4Instance;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwEnhancedMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwEnhancedMode
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwEnhancedMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwEnhancedMode (UINT4 u4ContextId, UINT1 u1EnhStatus)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwEnhancedMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_ENHANCED_MODE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwEnhancedMode;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u1EnhStatus = u1EnhStatus;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwPortRateLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwPortRateLimit
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwPortRateLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwPortRateLimit (UINT4 u4Instance, tIgsHwRateLmt * pIgsHwRateLmt,
                           UINT1 u1Action)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwPortRateLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_PORT_RATE_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwPortRateLimit;

    pEntry->u4Instance = u4Instance;
    pEntry->pIgsHwRateLmt = pIgsHwRateLmt;
    pEntry->u1Action = u1Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwUpdateIpmcEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwUpdateIpmcEntry
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwUpdateIpmcEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwUpdateIpmcEntry (UINT4 u4Instance,
                             tIgsHwIpFwdInfo * pIgsHwIpFwdInfo, UINT1 u1Action)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwUpdateIpmcEntry *pEntry = NULL;

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) != SNOOP_ENABLE)
    {
        return FNP_SUCCESS;
    }

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_UPDATE_IPMC_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwUpdateIpmcEntry;

    pEntry->u4Instance = u4Instance;
    pEntry->pIgsHwIpFwdInfo = pIgsHwIpFwdInfo;
    pEntry->u1Action = u1Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiNpGetFwdEntryHitBitStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiNpGetFwdEntryHitBitStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiNpGetFwdEntryHitBitStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiNpGetFwdEntryHitBitStatus (UINT4 u4Instance, tSnoopVlanId VlanId,
                                  UINT4 u4GrpAddr, UINT4 u4SrcAddr)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiNpGetFwdEntryHitBitStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_NP_GET_FWD_ENTRY_HIT_BIT_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiNpGetFwdEntryHitBitStatus;

    pEntry->u4Instance = u4Instance;
    pEntry->VlanId = VlanId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4SrcAddr = u4SrcAddr;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiNpUpdateIpmcFwdEntries                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiNpUpdateIpmcFwdEntries
 *                                                                          
 *    Input(s)            : Arguments of FsMiNpUpdateIpmcFwdEntries
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiNpUpdateIpmcFwdEntries (UINT4 u4Instance, tSnoopVlanId VlanId,
                               UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                               tPortList PortList, tPortList UntagPortList,
                               UINT1 u1EventType)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiNpUpdateIpmcFwdEntries *pEntry = NULL;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         2);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiNpUpdateIpmcFwdEntries;

    pEntry->u4Instance = u4Instance;
    pEntry->VlanId = VlanId;
    pEntry->u4GrpAddr = u4GrpAddr;
    pEntry->u4SrcAddr = u4SrcAddr;
    pEntry->PortList = PortList;
    pEntry->UntagPortList = UntagPortList;
    pEntry->u1EventType = u1EventType;

#ifdef L2RED_WANTED
    SnoopRedHwUpdateMcastSync (u4Instance, VlanId, u4GrpAddr, u4SrcAddr,
                               PortList, UntagPortList,
                               SNOOP_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }

#ifdef L2RED_WANTED
    SnoopRedHwUpdateMcastSync (u4Instance, VlanId, u4GrpAddr, u4SrcAddr,
                               PortList, UntagPortList,
                               SNOOP_RED_SYNC_HW_UPDATED);
#endif

    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwAddRtrPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwAddRtrPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwAddRtrPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwAddRtrPort (UINT4 u4Instance, tSnoopVlanId VlanId, UINT2 u2Port)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwAddRtrPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_ADD_RTR_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwAddRtrPort;

    pEntry->u4Instance = u4Instance;
    pEntry->VlanId = VlanId;
    pEntry->u4Port = u2Port;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwDelRtrPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwDelRtrPort
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwDelRtrPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwDelRtrPort (UINT4 u4Instance, tSnoopVlanId VlanId, UINT2 u2Port)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwDelRtrPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_DEL_RTR_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwDelRtrPort;

    pEntry->u4Instance = u4Instance;
    pEntry->VlanId = VlanId;
    pEntry->u4Port = u2Port;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsMbsmHwEnableIgmpSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsMbsmHwEnableIgmpSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsMbsmHwEnableIgmpSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsMbsmHwEnableIgmpSnooping (UINT4 u4ContextId,
                                    tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsMbsmHwEnableIgmpSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_MBSM_HW_ENABLE_IGMP_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwEnableIgmpSnooping;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsMbsmHwEnableIpIgmpSnooping                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsMbsmHwEnableIpIgmpSnooping
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsMbsmHwEnableIpIgmpSnooping
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsMbsmHwEnableIpIgmpSnooping (UINT4 u4ContextId,
                                      tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsMbsmHwEnableIpIgmpSnooping *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_MBSM_HW_ENABLE_IP_IGMP_SNOOPING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwEnableIpIgmpSnooping;

    pEntry->u4ContextId = u4ContextId;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsMbsmHwEnhancedMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsMbsmHwEnhancedMode
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsMbsmHwEnhancedMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsMbsmHwEnhancedMode (UINT4 u4ContextId, UINT1 u1EnhStatus,
                              tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsMbsmHwEnhancedMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_MBSM_HW_ENHANCED_MODE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwEnhancedMode;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u1EnhStatus = u1EnhStatus;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsMbsmHwPortRateLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsMbsmHwPortRateLimit
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsMbsmHwPortRateLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsMbsmHwPortRateLimit (UINT4 u4Instance, tIgsHwRateLmt * pIgsHwRateLmt,
                               UINT1 u1Action, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsMbsmHwPortRateLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_MBSM_HW_PORT_RATE_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwPortRateLimit;

    pEntry->u4Instance = u4Instance;
    pEntry->pIgsHwRateLmt = pIgsHwRateLmt;
    pEntry->u1Action = u1Action;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsMbsmHwSparseMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsMbsmHwSparseMode
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsMbsmHwSparseMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsMbsmHwSparseMode (UINT4 u4ContextId, UINT1 u1SparseStatus,
                            tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsMbsmHwSparseMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_MBSM_HW_SPARSE_MODE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmHwSparseMode;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u1SparseStatus = u1SparseStatus;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* MBSM_WANTED */

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwGetIpFwdEntryHitBitStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwGetIpFwdEntryHitBitStatus
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwGetIpFwdEntryHitBitStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwGetIpFwdEntryHitBitStatus (UINT4 u4Instance,
                                       tIgsHwIpFwdInfo * pIgsHwIpFwdInfo)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwGetIpFwdEntryHitBitStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_GET_IP_FWD_ENTRY_HIT_BIT_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwGetIpFwdEntryHitBitStatus;

    pEntry->u4Instance = u4Instance;
    pEntry->pIgsHwIpFwdInfo = pIgsHwIpFwdInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiIgsHwSparseMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsHwSparseMode
 *                                                                          
 *    Input(s)            : Arguments of FsMiIgsHwSparseMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsHwSparseMode (UINT4 u4ContextId, UINT1 u1SparseStatus)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwSparseMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_SPARSE_MODE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwSparseMode;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u1SparseStatus = u1SparseStatus;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MBSM_WANTED
/***************************************************************************
 *
 *    Function Name       : IgsFsMiIgsMbsmSyncIPMCInfo
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiIgsMbsmSyncIPMCInfo
 *
 *    Input(s)            : Arguments of FsMiIgsMbsmSyncIPMCInfo
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
IgsFsMiIgsMbsmSyncIPMCInfo (tIgsIPMCInfoArray * pIgsIPMCInfoArray,
                            tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsMbsmSyncIPMCInfo *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_MBSM_SYNC_I_P_M_C_INFO,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsMbsmSyncIPMCInfo;

    pEntry->pIgsIPMCInfoArray = pIgsIPMCInfoArray;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif
/***************************************************************************
 *                                                                          
 *    Function Name       : IgsFsMiNpCreateFwdEntries                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiNpUpdateIpmcFwdEntries
 *                                                                          
 *    Input(s)            : Arguments of FsMiNpUpdateIpmcFwdEntries
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
UINT1
IgsFsMiNpCreateFwdEntries (UINT4 u4Instance, tSnoopVlanId VlanId,
                           UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                           tPortList PortList, tPortList UntagPortList)
{
    tIgsHwIpFwdInfo     IgsHwIpFwdInfo;
    tFsHwNp             FsHwNp;
    tIgsNpModInfo      *pIgsNpModInfo = NULL;
    tIgsNpWrFsMiIgsHwUpdateIpmcEntry *pEntry = NULL;

    MEMSET (&IgsHwIpFwdInfo, 0, sizeof (tIgsHwIpFwdInfo));

    IgsHwIpFwdInfo.u4GrpAddr = u4GrpAddr;
    IgsHwIpFwdInfo.OuterVlanId = VlanId;
    IgsHwIpFwdInfo.u4SrcAddr = u4SrcAddr;
    MEMCPY (IgsHwIpFwdInfo.PortList, PortList, sizeof (tPortList));
    MEMCPY (IgsHwIpFwdInfo.UntagPortList, UntagPortList, sizeof (tPortList));

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IGS_MOD,    /* Module ID */
                         FS_MI_IGS_HW_UPDATE_IPMC_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIgsNpModInfo = &(FsHwNp.IgsNpModInfo);
    pEntry = &pIgsNpModInfo->IgsNpFsMiIgsHwUpdateIpmcEntry;

    pEntry->u4Instance = u4Instance;
    pEntry->pIgsHwIpFwdInfo = &IgsHwIpFwdInfo;
    pEntry->u1Action = SNOOP_HW_CREATE_ENTRY;

#ifdef L2RED_WANTED
    SnoopRedHwUpdateMcastSync (u4Instance, VlanId, u4GrpAddr, u4SrcAddr,
                               PortList, UntagPortList,
                               SNOOP_RED_SYNC_HW_NOT_UPDATED);
#endif /* L2RED_WANTED */
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }

    SnoopUtilUpdateHwIdForIPMC (&IgsHwIpFwdInfo);

#ifdef L2RED_WANTED
    SnoopRedHwUpdateMcastSync (u4Instance, VlanId, u4GrpAddr, u4SrcAddr,
                               PortList, UntagPortList,
                               SNOOP_RED_SYNC_HW_UPDATED);
#endif

    return (FNP_SUCCESS);
}

#endif /* __IGS_NP_API_C */
