/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/* $Id: igsutil.c,v 1.32 2017/08/11 13:52:37 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : igsutil.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping IGS Util module                       */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains IGS Utility routines        */
/*                            for snooping (IGMP) module                     */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                       */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

UINT1               gu1SnpIpHdr[SNOOP_MAX_IP_BUF];
/*********************************************************************/
/* Function           :     IgsUtilIgmpCalcCheckSum                  */
/*                                                                   */
/* Description        :  This function calculate the CheckSum for    */
/*                       IGMP packet  given the MRP identifier       */
/*                                                                   */
/* Input(s)           :  pBuf - Pointer to the packet buffer         */
/*                       u4Size - Total pBuf Size                    */
/*                       u4OffSet - Offset value                     */
/* Output(s)          :  None                                        */
/*                                                                   */
/* Returns            :  checksum                                    */
/*                                                                   */
/*********************************************************************/
UINT2
IgsUtilIgmpCalcCheckSum (tCRU_BUF_CHAIN_HEADER * pBuf,
                         UINT4 u4Size, UINT4 u4Offset)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT1               u1Byte = 0;
    UINT1              *pu1Buf = NULL;
    UINT1               au1TmpArray[SNOOP_TMP_ARRAY_SIZE];
    UINT4               u4TmpSize = SNOOP_TMP_ARRAY_SIZE;

    SNOOP_MEM_SET (au1TmpArray, 0, SNOOP_TMP_ARRAY_SIZE);

    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4Size);

    if (pu1Buf == NULL)
    {
        while (u4Size > SNOOP_TMP_ARRAY_SIZE)
        {
            u4TmpSize = SNOOP_TMP_ARRAY_SIZE;
            pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4TmpSize);
            if (pu1Buf == NULL)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) au1TmpArray,
                                           u4Offset, u4TmpSize);
                pu1Buf = au1TmpArray;
            }

            while (u4TmpSize > 0)
            {
                u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
                u4Sum += u2Tmp;
                pu1Buf += SNOOP_OFFSET_TWO;
                u4TmpSize -= SNOOP_OFFSET_TWO;
            }

            u4Size -= SNOOP_TMP_ARRAY_SIZE;
            u4Offset += SNOOP_TMP_ARRAY_SIZE;
        }
        while (u4Size > 1)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Tmp, u4Offset,
                                       SNOOP_OFFSET_TWO);
            u4Sum += u2Tmp;
            u4Size -= SNOOP_OFFSET_TWO;
            u4Offset += SNOOP_OFFSET_TWO;
        }

        if (u4Size == 1)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, &u1Byte, u4Offset,
                                       SNOOP_OFFSET_ONE);
            u2Tmp = 0;
            *((UINT1 *) &u2Tmp) = u1Byte;
            u4Sum += u2Tmp;
        }
    }
    else
    {
        while (u4Size > 1)
        {
            u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
            u4Sum += u2Tmp;
            pu1Buf += SNOOP_OFFSET_TWO;
            u4Size -= SNOOP_OFFSET_TWO;
        }
        if (u4Size == 1)
        {
            u2Tmp = 0;
            *((UINT1 *) &u2Tmp) = *pu1Buf;
            u4Sum += u2Tmp;
        }
    }
    u4Sum = (u4Sum >> 16) + (u4Sum & 0xFFFF);
    u4Sum += (u4Sum >> 16);
    u2Tmp = (UINT2) ~((UINT2) u4Sum);

    return (((UINT2) SNOOP_NTOHS (u2Tmp)));

}

/*****************************************************************************/
/* Function Name      : IgsUtilVerifyRtrControlMsg                           */
/*                                                                           */
/* Description        : This routine verifies the incoming router control    */
/*                      message                                              */
/*                                                                           */
/* Input(s)           : pBuf        - Message                                */
/*                      pIgsPktInfo - Packet Information                     */
/*                                                                           */
/* Output(s)          : pu1PktType - Packet type                             */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsUtilVerifyRtrControlMsg (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tSnoopPktInfo * pIgsPktInfo)
{
    UINT1               u1PktInfo = 0;
    UINT4               u4DestAddr = 0;

    u4DestAddr = (SNOOP_PTR_FETCH_4 (pIgsPktInfo->DestIpAddr.au1Addr));

    switch (SNOOP_PTR_FETCH_4 (pIgsPktInfo->DestIpAddr.au1Addr))
    {
        case SNOOP_ALL_PIM_ROUTERS:

            if (pIgsPktInfo->u1Protocol == SNOOP_PIM_CTRL_MSG)
            {
                /* Not checking for linear because only one byte is copied */
                CRU_BUF_Copy_FromBufChain (pBuf, &u1PktInfo,
                                           (pIgsPktInfo->u1IpHdrLen +
                                            SNOOP_DATA_LINK_HDR_LEN +
                                            pIgsPktInfo->u1TagLen),
                                           SNOOP_OFFSET_ONE);

                /*
                 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                 * |PIM Ver| Type  | Reserved   |     Checksum           |
                 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                 */

                /* Check PIM Version number is 2 and type is 0 (Hello) */
                if (((u1PktInfo & 0xf0) == SNOOP_PIM_VERSION2)
                    && ((u1PktInfo & 0x0f) == SNOOP_PIMV2_HELLO))
                {
                    pIgsPktInfo->u1PktType = SNOOP_ROUTER_MSG;
                }
                else
                {
                    return SNOOP_FAILURE;
                }
            }
            else
            {
                return SNOOP_FAILURE;
            }
            break;

        case SNOOP_ALL_OSPF_ROUTERS:

            if (pIgsPktInfo->u1Protocol == SNOOP_OSPF_CTRL_MSG)
            {
                /* Not checking for linear because only one byte is copied */
                CRU_BUF_Copy_FromBufChain (pBuf, &u1PktInfo,
                                           (pIgsPktInfo->u1IpHdrLen +
                                            SNOOP_DATA_LINK_HDR_LEN +
                                            pIgsPktInfo->u1TagLen +
                                            SNOOP_OFFSET_ONE),
                                           SNOOP_OFFSET_ONE);

                /*
                 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                 * |OSPF Version   | Type     |     packet length        |
                 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
                 */

                /* Check OSPF packet type is 1 (Hello) */
                if ((u1PktInfo & 0x00ff) == SNOOP_OSPF_HELLO)
                {
                    pIgsPktInfo->u1PktType = SNOOP_ROUTER_MSG;
                }
                else
                {
                    return SNOOP_FAILURE;
                }
            }
            else
            {
                return SNOOP_FAILURE;
            }
            break;

        default:
            /* Any other reserved addresses from 
             * 224.0.0.3 to 224.0.0.255 */
            if ((u4DestAddr >= SNOOP_RESVD_MCAST_ADDR_RANGE_MIN) &&
                (u4DestAddr <= SNOOP_RESVD_MCAST_ADDR_RANGE_MAX))
            {
                pIgsPktInfo->u1PktType = SNOOP_INVALID_IGMP_PKT;

                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC,
                           "Packet with reserved dest address received\r\n");
                return SNOOP_FAILURE;
            }

            pIgsPktInfo->u1PktType = SNOOP_DATA_PKT;
            break;
    }

    return SNOOP_SUCCESS;
}

/****************************************************************************/
/* Function           : IgsUtilIpFormPacket                                 */
/*  Description       : This function forms the IP header of IGMP Query     */
/*                    : packet.                                             */
/* Input(s)           : pBuf - IGMP PDU buffer                              */
/*                    : u4Src - Source IP address                           */
/*                    : u4Dest - Destination Ip address                     */
/*                    : u2Len - Length of PDU                               */
/*                                                                          */
/* Output(s)          : None.                                               */
/*                                                                          */
/* Returns            : None                                                */
/****************************************************************************/
VOID
IgsUtilIpFormPacket (tCRU_BUF_CHAIN_HEADER * pBuf,
                     UINT4 u4Src, UINT4 u4Dest, UINT1 u1PktType, UINT2 u2Len)
{
    t_IP_HEADER         IpHdr;

    SNOOP_MEM_SET (&IpHdr, 0, sizeof (t_IP_HEADER));

    IpHdr.u1Ver_hdrlen = SNOOP_IP_RTR_OPT_VERS_AND_HLEN;
    IpHdr.u1Tos = SNOOP_IGMP_TOS;
    /* SNOOP_IP_HDR_RTR_OPT_LEN is a defined constant */
    IpHdr.u2Totlen = (UINT2) (SNOOP_IP_HDR_RTR_OPT_LEN + u2Len);
    IpHdr.u2Totlen = SNOOP_HTONS (IpHdr.u2Totlen);
    IpHdr.u2Id = 0;
    IpHdr.u2Fl_offs = 0;
    IpHdr.u2Fl_offs = SNOOP_HTONS (IpHdr.u2Fl_offs);
    IpHdr.u1Ttl = SNOOP_IGMP_PACKET_TTL;
    IpHdr.u1Proto = SNOOP_IGMP_PROTOCOL;
    IpHdr.u2Cksum = 0;            /* Clear Checksum */
    IpHdr.u4Src = SNOOP_HTONL (u4Src);
    if (u1PktType == SNOOP_IGMP_QUERY)
    {
        IpHdr.u4Dest = SNOOP_HTONL (SNOOP_ALL_HOST_IP);
    }
    else if (u1PktType == SNOOP_IGMP_LEAVE)
    {
        IpHdr.u4Dest = SNOOP_HTONL (SNOOP_ALL_ROUTER_IP);
    }
    else if (u1PktType == SNOOP_IGMP_V3REPORT)
    {
        IpHdr.u4Dest = SNOOP_HTONL (SNOOP_ALL_IGMPV3_ROUTERS);
    }
    else
    {
        IpHdr.u4Dest = SNOOP_HTONL (u4Dest);
    }

    /* Router alert option */
    IpHdr.u1Options[0] = 0x94;
    IpHdr.u1Options[1] = 0x04;
    IpHdr.u1Options[2] = 0x00;
    IpHdr.u1Options[3] = 0x00;

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IpHdr,
                               SNOOP_DATA_LINK_HDR_LEN,
                               SNOOP_IP_HDR_RTR_OPT_LEN);

    IpHdr.u2Cksum =
        IgsUtilCalculateIpCheckSum (pBuf, SNOOP_IP_HDR_RTR_OPT_LEN,
                                    SNOOP_DATA_LINK_HDR_LEN);

    IpHdr.u2Cksum = SNOOP_HTONS (IpHdr.u2Cksum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IpHdr.u2Cksum,
                               SNOOP_IGMP_CHKSUM_OFFSET, SNOOP_OFFSET_TWO);

    return;
}

/*****************************************************************************/
/* Function Name      : IgsUtilFillV3SourceInfo                              */
/*                                                                           */
/* Description        : This function Fills source information in the global */
/*                      buffer for a given group record                      */
/*                                                                           */
/* Input(s)           : pIgsConsGroupEntry - Pointer to the consolidated     */
/*                                           group entry                     */
/*                      u1RecordType   - Group record type to be sent        */
/*                      u2NumSrcs      - Number of sources                   */
/*                                                                           */
/* Output(s)          : pu4MaxLength - pointer to Filled length              */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgsUtilFillV3SourceInfo (UINT4 u4Instance,
                         tSnoopConsolidatedGroupEntry * pIgsConsGroupEntry,
                         UINT1 u1RecordType,
                         UINT2 u2NumSrcs, UINT4 *pu4MaxLength)
{
    tSnoopIgmpGroupRec  IgmpGrpRec;
    UINT4               u4SrcAddr = 0;
    UINT2               u2Count = 0;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&IgmpGrpRec, 0, sizeof (tSnoopIgmpGroupRec));

    IgmpGrpRec.u1RecordType = u1RecordType;

    IgmpGrpRec.u2NumSrcs = SNOOP_HTONS (u2NumSrcs);
    if (pIgsConsGroupEntry->u1FilterMode == SNOOP_EXCLUDE)
    {
        if (SNOOP_MEM_CMP (pIgsConsGroupEntry->ExclSrcBmp, gNullSrcBmp,
                           SNOOP_SRC_LIST_SIZE) == 0)
        {
            IgmpGrpRec.u2NumSrcs = 0;
        }
    }

    IgmpGrpRec.u1AuxDataLen = 0;
    IgmpGrpRec.u4GroupAddress =
        SNOOP_HTONL (SNOOP_PTR_FETCH_4
                     (pIgsConsGroupEntry->GroupIpAddr.au1Addr));

    SNOOP_MEM_CPY (gpSSMRepSendBuffer + *pu4MaxLength,
                   &IgmpGrpRec, SNOOP_IGMP_SSMGROUP_RECD_SIZE);

    *pu4MaxLength += SNOOP_IGMP_SSMGROUP_RECD_SIZE;

    if (IgmpGrpRec.u2NumSrcs != 0)
    {
        for (u2Count = 1; u2Count <= (SNOOP_SRC_LIST_SIZE * 8); u2Count++)
        {
            if (pIgsConsGroupEntry->u1FilterMode == SNOOP_INCLUDE)
            {
                OSIX_BITLIST_IS_BIT_SET (pIgsConsGroupEntry->InclSrcBmp,
                                         u2Count, SNOOP_SRC_LIST_SIZE, bResult);
            }
            else
            {
                OSIX_BITLIST_IS_BIT_SET (pIgsConsGroupEntry->ExclSrcBmp,
                                         u2Count, SNOOP_SRC_LIST_SIZE, bResult);
            }

            if ((bResult == OSIX_TRUE) && (u2Count <= SNOOP_MAX_MCAST_SRCS))
            {
                u4SrcAddr =
                    SNOOP_HTONL (SNOOP_PTR_FETCH_4
                                 (SNOOP_SRC_INFO (u4Instance,
                                                  (u2Count -
                                                   1)).SrcIpAddr.au1Addr));

                SNOOP_MEM_CPY (gpSSMRepSendBuffer + *pu4MaxLength,
                               &u4SrcAddr, SNOOP_IP_ADDR_SIZE);
                *pu4MaxLength += SNOOP_IP_ADDR_SIZE;
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : IgsUtilValidatePacket                                */
/*                                                                           */
/* Description        : This function validates IGMP mutlicast packets and   */
/*                      extract the header information                       */
/*                                                                           */
/* Input(s)           : pBuf   - packet  whose type is to be identified      */
/*                      u4Instance - Instance Identifier                     */
/*                      pIgsPktInfo - Packet information where already       */
/*                      decoded info is present                              */
/*                      VlanId      - Vlan Identifier                        */
/*                                                                           */
/* Output(s)          : pIgsPktInfo - The structure contains the IGMP packet */
/*                                    information                            */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsUtilValidatePacket (UINT4 u4Instance, tCRU_BUF_CHAIN_HEADER * pBuf,
                       tSnoopPktInfo * pIgsPktInfo, tSnoopTag VlanId)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT2              *pu2EthType = NULL;
    UINT2               u2EthType = 0;
    UINT1               u1Ttl = 0;
    UINT1               u1TagLen = 0;
    UINT1               u1IsIgsRtrAlert = 0;
    INT4                i4RetVal = SNOOP_SUCCESS;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    u1TagLen = pIgsPktInfo->u1TagLen;

    /* Copy Ethernet type from the ethernet header */
    pu2EthType = (UINT2 *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
        (pBuf, SNOOP_ETH_TYPE_OFFSET + u1TagLen, SNOOP_ETH_TYPE_SIZE);

    if (pu2EthType == NULL)
    {
        /* The header is not contiguous in the buffer */
        pu2EthType = &u2EthType;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pu2EthType,
                                   SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);
    }

    if (SNOOP_NTOHS (*pu2EthType) != SNOOP_IP_PKT_TYPE)
    {
        pIgsPktInfo->u1PktType = SNOOP_NON_IP_PKT;
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Received a non-ip packet\r\n");
        return SNOOP_FAILURE;
    }

    IgsUtilExtractIpHdrInfo (pBuf, pIgsPktInfo, &u1Ttl, &u1IsIgsRtrAlert);

    if (pIgsPktInfo->u2PktLen < pIgsPktInfo->u1IpHdrLen)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Total Length is invalid in IP packet\r\n");
        return SNOOP_FAILURE;
    }

    i4RetVal = SnoopVlanGetVlanEntry (u4Instance, VlanId,
                                      (UINT1) SNOOP_ADDR_TYPE_IPV4,
                                      &pSnoopVlanEntry);
    if (IgsUtilCalculateIpCheckSum (pBuf, pIgsPktInfo->u1IpHdrLen,
                                    SNOOP_DATA_LINK_HDR_LEN + u1TagLen) != 0)
    {

        if (i4RetVal == SNOOP_SUCCESS)
        {
            if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                      SNOOP_PKT_ERROR) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_PKT_TRC, "Unable to update statistics "
                                "for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }
        }
        /* IP Checksum verification failed */
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Received a IP packet with invalid checksum\r\n");
        return SNOOP_FAILURE;
    }

    if (pIgsPktInfo->u1Protocol == SNOOP_IGMP_PROTOCOL)
    {
        if (IgsUtilVerifyIgmpPacket (pBuf, pIgsPktInfo) != SNOOP_SUCCESS)
        {
            if (i4RetVal == SNOOP_SUCCESS)
            {
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_PKT_ERROR) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
            }

            /* Probably IGMP hdr checksum failure */
            SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC, "IGMP packet verification failure due "
                       "to invalid checksum or packet type\r\n");
            return SNOOP_FAILURE;
        }
    }
    else
    {
        if (IgsUtilVerifyRtrControlMsg (pBuf, pIgsPktInfo) != SNOOP_SUCCESS)
        {
            /* Probably IGMP hdr checksum failure */
            SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC, "Invalid router control message\r\n");
            return SNOOP_FAILURE;
        }
    }

    /* IGMPv2/v3 REPORT should be validated for router alert option */
    if ((pIgsPktInfo->u1PktType == SNOOP_IGMP_V2REPORT) ||
        (pIgsPktInfo->u1PktType == SNOOP_IGMP_V3REPORT))
    {

        if (u1IsIgsRtrAlert == 0)
        {
            if (i4RetVal == SNOOP_SUCCESS)
            {
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_PKT_ERROR) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
            }
            SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC, "Invalid router alert\r\n");
            return SNOOP_FAILURE;
        }
    }

    if (pIgsPktInfo->u1PktType != SNOOP_DATA_PKT)
    {
        if (u1Ttl != SNOOP_IGMP_PACKET_TTL)
        {
            if (i4RetVal == SNOOP_SUCCESS)
            {
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_PKT_ERROR) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics for VLAN ID "
                                    "%d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
            }

            /* IP TTL verification failed  */
            SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC,
                       "Unable to process the incoming packet -  "
                       "Invalid ttl value\r\n");
            return SNOOP_FAILURE;
        }

    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsUtilExtractIpHdrInfo                              */
/*                                                                           */
/* Description        : This routine gets the IP header in the packet.       */
/*                                                                           */
/* Input(s)           : pBuf - IP Message Buffer                             */
/*                                                                           */
/* Output(s)          : pIgsPktInfo - structure in which the IGS header info */
/*                                    is filled.                             */
/*                      pu1Ttl      - pointer to store TTL value             */
/*                      pu1IsIgsRtrAlert - pointer to store router           */
/*                                         alert value                       */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsUtilExtractIpHdrInfo (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tSnoopPktInfo * pIgsPktInfo, UINT1 *pu1Ttl,
                         UINT1 *pu1IsIgsRtrAlert)
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT4               u4SrcAddr = 0;
    UINT4               u4DstAddr = 0;
    tSnoopRtAlertHdr    RtAlertHdr;

    SNOOP_MEM_SET (&TmpIpHdr, 0, sizeof (t_IP_HEADER));
    SNOOP_MEM_SET (&RtAlertHdr, 0, sizeof (tSnoopRtAlertHdr));

    /* The header is not contiguous in the buffer */
    pIpHdr = &TmpIpHdr;
    /* Copy the header */
    CRU_BUF_Copy_FromBufChain
        (pBuf, (UINT1 *) pIpHdr,
         SNOOP_DATA_LINK_HDR_LEN + pIgsPktInfo->u1TagLen, sizeof (t_IP_HEADER));

    pIgsPktInfo->u1IpHdrLen = (UINT1) ((pIpHdr->u1Ver_hdrlen & 0x0f) << 2);
    pIgsPktInfo->u1Protocol = pIpHdr->u1Proto;
    pIgsPktInfo->u2PktLen = SNOOP_NTOHS (pIpHdr->u2Totlen);
    RtAlertHdr.u1OptType = pIpHdr->u1Options[0];

    if (RtAlertHdr.u1OptType == 0x94)
    {
        *pu1IsIgsRtrAlert = 1;
    }

    u4SrcAddr = SNOOP_NTOHL (pIpHdr->u4Src);
    u4DstAddr = SNOOP_NTOHL (pIpHdr->u4Dest);

    *pu1Ttl = pIpHdr->u1Ttl;

    IPVX_ADDR_INIT_IPV4 (pIgsPktInfo->SrcIpAddr, SNOOP_ADDR_TYPE_IPV4,
                         (UINT1 *) &u4SrcAddr);
    IPVX_ADDR_INIT_IPV4 (pIgsPktInfo->DestIpAddr, SNOOP_ADDR_TYPE_IPV4,
                         (UINT1 *) &u4DstAddr);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*  Function Name   : IgsUtilCalculateIpCheckSum                             */
/*  Description     : Routine to calculate IP checksum.                      */
/*                    The checksum field is the 16 bit one's complement of   */
/*                    the one's   complement sum of all 16 bit words in the  */
/*                    header.                                                */
/* Input(s)         : pBuf         - pointer to buffer with data over        */
/*                                   which checksum is to be calculated      */
/*                    u4Size       - size of data in the buffer              */
/*                    u4Offset     - offset from which the data starts       */
/*  Output(s)       : None.                                                  */
/*                                                                           */
/*  Returns         : Checksum                                               */
/*****************************************************************************/
UINT2
IgsUtilCalculateIpCheckSum (tCRU_BUF_CHAIN_HEADER * pBuf,
                            UINT4 u4Size, UINT4 u4Offset)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp = 0;
    UINT1               u1Byte = 0;
    UINT1              *pu1Buf = NULL;
    UINT1               au1TmpArray[SNOOP_TMP_ARRAY_SIZE];
    UINT4               u4TmpSize = 0;

    SNOOP_MEM_SET (au1TmpArray, 0, SNOOP_TMP_ARRAY_SIZE);

    pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4Size);

    if (pu1Buf == NULL)
    {
        while (u4Size > SNOOP_TMP_ARRAY_SIZE)
        {
            u4TmpSize = SNOOP_TMP_ARRAY_SIZE;
            pu1Buf = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4TmpSize);
            if (pu1Buf == NULL)
            {
                CRU_BUF_Copy_FromBufChain (pBuf, au1TmpArray,
                                           u4Offset, u4TmpSize);
                pu1Buf = au1TmpArray;
            }

            while (u4TmpSize > 0)
            {
                u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
                u4Sum += u2Tmp;
                pu1Buf += SNOOP_OFFSET_TWO;
                u4TmpSize -= SNOOP_OFFSET_TWO;
            }
            u4Size -= SNOOP_TMP_ARRAY_SIZE;
            u4Offset += SNOOP_TMP_ARRAY_SIZE;
        }

        while (u4Size > 1)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Tmp,
                                       u4Offset, SNOOP_OFFSET_TWO);
            u4Sum += u2Tmp;
            u4Size -= SNOOP_OFFSET_TWO;
            u4Offset += SNOOP_OFFSET_TWO;
        }

        if (u4Size == 1)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, &u1Byte, u4Offset,
                                       SNOOP_OFFSET_ONE);
            u2Tmp = 0;
            *((UINT1 *) &u2Tmp) = u1Byte;
            u4Sum += u2Tmp;
        }
    }
    else
    {
        while (u4Size > 1)
        {
            u2Tmp = *((UINT2 *) (VOID *) pu1Buf);
            u4Sum += u2Tmp;
            pu1Buf += SNOOP_OFFSET_TWO;
            u4Size -= SNOOP_OFFSET_TWO;
        }
        if (u4Size == 1)
        {
            u2Tmp = 0;
            *((UINT1 *) &u2Tmp) = *pu1Buf;
            u4Sum += u2Tmp;
        }
    }
    /* Shift by 16 bits */
    u4Sum = (u4Sum >> 16) + (u4Sum & 0xffff);
    u4Sum += (u4Sum >> 16);
    u2Tmp = (UINT2) (~((UINT2) u4Sum));

    return (SNOOP_NTOHS (u2Tmp));
}

/*****************************************************************************/
/* Function Name      : IgsUtilVerifyPacketType                              */
/*                                                                           */
/* Description        : This routine verifies the IGMP packet type and       */
/*                       Checksum                                            */
/*                      packet buffer.                                       */
/*                                                                           */
/* Input(s)           : pBuf        - Message                                */
/*                      pIgsPktInfo - Packet inforamtion                     */
/*                                                                           */
/* Output(s)          : pu1PktType - Packet type                             */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsUtilVerifyIgmpPacket (tCRU_BUF_CHAIN_HEADER * pBuf,
                         tSnoopPktInfo * pIgsPktInfo)
{
    tSnoopIgmpHdr       IgmpPkt;
    UINT2               u2IgmpHdrLen = 0;
    UINT2               u2SrcOffSet = 0;
    UINT2               u2SrcCount = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4DestAddr = 0;

    MEMSET (&IgmpPkt, 0, sizeof (tSnoopIgmpHdr));

    /* The header is not contiguous in the buffer */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IgmpPkt,
                               SNOOP_DATA_LINK_HDR_LEN + pIgsPktInfo->u1TagLen +
                               pIgsPktInfo->u1IpHdrLen, SNOOP_IGMP_HEADER_LEN);

    u4GrpAddr = SNOOP_NTOHL (IgmpPkt.u4GrpAddr);
    u4DestAddr = SNOOP_PTR_FETCH_4 (pIgsPktInfo->DestIpAddr.au1Addr);
    IPVX_ADDR_INIT_IPV4 (pIgsPktInfo->GroupAddr, SNOOP_ADDR_TYPE_IPV4,
                         (UINT1 *) &u4GrpAddr);

    pIgsPktInfo->u2MaxRespCode = IgmpPkt.u1MaxRespTime;

    u2IgmpHdrLen = (UINT2) (pIgsPktInfo->u2PktLen - pIgsPktInfo->u1IpHdrLen);

    switch (u4DestAddr)
    {
        case SNOOP_ALL_ROUTER_IP:

            if (IgmpPkt.u1PktType == SNOOP_IGMP_LEAVE)
            {
                pIgsPktInfo->u1PktType = SNOOP_IGMP_LEAVE;
            }
            else
            {
                return SNOOP_FAILURE;
            }
            break;

        case SNOOP_ALL_DVMRP_ROUTERS:

            if (IgmpPkt.u1PktType == SNOOP_DVMRP_CTRL_MSG)
            {
                pIgsPktInfo->u1PktType = SNOOP_ROUTER_MSG;
            }
            else
            {
                return SNOOP_FAILURE;
            }
            break;

        case SNOOP_ALL_HOST_IP:

            /* Invalid IGMP general query received */
            if ((pIgsPktInfo->u2MaxRespCode == 0) &&
                (u2IgmpHdrLen > SNOOP_IGMP_HEADER_LEN))
            {
                return SNOOP_FAILURE;
            }

            if (IgmpPkt.u1PktType == SNOOP_IGMP_QUERY)
            {
                /* Since all the query messages have the same type 
                 * just differntiate the general query message */
                pIgsPktInfo->u1PktType = SNOOP_IGMP_QUERY;
                pIgsPktInfo->u1QueryType = SNOOP_GENERAL_QUERY;
            }
            else
            {
                return SNOOP_FAILURE;
            }
            break;

        case SNOOP_ALL_IGMPV3_ROUTERS:

            if (IgmpPkt.u1PktType == SNOOP_IGMP_V3REPORT)
            {
                pIgsPktInfo->u1PktType = SNOOP_IGMP_V3REPORT;
            }
            else
            {
                return SNOOP_FAILURE;
            }
            break;

        default:
            /* Any other reserved addresses from 
             * 224.0.0.3 to 224.0.0.255 */
            if ((u4DestAddr >= SNOOP_RESVD_MCAST_ADDR_RANGE_MIN) &&
                (u4DestAddr <= SNOOP_RESVD_MCAST_ADDR_RANGE_MAX))
            {
                pIgsPktInfo->u1PktType = SNOOP_INVALID_IGMP_PKT;
                return SNOOP_FAILURE;
            }
            else if ((u4DestAddr >= SNOOP_VALID_MCAST_IP_RANGE_MIN) &&
                     (u4DestAddr <= SNOOP_VALID_MCAST_IP_RANGE_MAX))
            {
                /* Valid Mcast Address from 224.0.1.0 
                 * to 239.255.255.255 */
                if ((IgmpPkt.u1PktType == SNOOP_IGMP_V3REPORT) ||
                    (IgmpPkt.u1PktType == SNOOP_IGMP_LEAVE))
                {
                    return SNOOP_FAILURE;
                }

                if ((IgmpPkt.u1PktType == SNOOP_IGMP_V1REPORT) ||
                    (IgmpPkt.u1PktType == SNOOP_IGMP_V2REPORT))
                {
                    /* Invalid v1/v2 report if the IP Destination address
                     * differs from the IGMP group address */
                    if (u4GrpAddr != u4DestAddr)
                    {
                        return SNOOP_FAILURE;
                    }
                }

                if ((IgmpPkt.u1PktType == SNOOP_IGMP_V1REPORT) ||
                    (IgmpPkt.u1PktType == SNOOP_IGMP_V2REPORT) ||
                    (IgmpPkt.u1PktType == SNOOP_IGMP_QUERY))
                {
                    pIgsPktInfo->u1PktType = IgmpPkt.u1PktType;

                    if (IgmpPkt.u1PktType == SNOOP_IGMP_QUERY)
                    {
                        if ((u2IgmpHdrLen == SNOOP_IGMP_HEADER_LEN) ||
                            (u2IgmpHdrLen == (SNOOP_IGMP_HEADER_LEN +
                                              SNOOP_IGMPV3_QRY_HDR)))
                        {
                            pIgsPktInfo->u1QueryType = SNOOP_GRP_QUERY;
                        }
                        else
                        {
                            pIgsPktInfo->u1QueryType = SNOOP_GRP_SRC_QUERY;

                            u2SrcOffSet =
                                (UINT2) (SNOOP_DATA_LINK_HDR_LEN +
                                         pIgsPktInfo->u1TagLen +
                                         pIgsPktInfo->u1IpHdrLen +
                                         SNOOP_IGMP_HEADER_LEN);

                            CRU_BUF_Copy_FromBufChain (pBuf,
                                                       (UINT1 *) &u2SrcCount,
                                                       (UINT4) (u2SrcOffSet +
                                                                SNOOP_SRC_CNT_OFFSET),
                                                       SNOOP_SRC_CNT_OFFSET);
                            u2SrcCount = SNOOP_NTOHS (u2SrcCount);

                            if (pIgsPktInfo->u2PktLen <
                                (pIgsPktInfo->u1IpHdrLen +
                                 SNOOP_IGMP_HEADER_LEN +
                                 (u2SrcCount * SNOOP_IP_ADDR_SIZE)))
                            {
                                return SNOOP_FAILURE;
                            }
                        }
                    }
                }
            }
            else
            {
                return SNOOP_FAILURE;
            }
            break;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsMVlanTranslationNeeded                            */
/*                                                                           */
/* Description        : This routine validates the packet, and return whether*/
/*                      M-VLAN translation is needed of not.                 */
/*                      packet buffer.                                       */
/*                      This function, returns TRUE if,                      */
/*                           * MVLAN Feature is enabled.                     */
/*                           * If its Report or Leave Packet                 */
/*                           * If the packet is not tagged.                  */
/*                                                                           */
/* Input(s)           : pBuf         - Message                               */
/*                      u4InstanceId - Instance Identifier                   */
/*                      u2LocalPort  - Incoming port number                  */
/*                      VlanInfo     - Vlan Information of the packeti       */
/*                      pSnoopPktInfo - Structure contains IGMP/MLD packet   */
/*                                      information.                         */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE / SNOOP_FALSE                             */
/*****************************************************************************/
UINT1
IgsMVlanTranslationNeeded (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Instance,
                           UINT2 u2LocalPort, tSnoopVlanInfo VlanInfo,
                           tSnoopPktInfo * pSnoopPktInfo)
{
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (pBuf);

    /* Check Multicast VLAN feature is enabled or not */

    if (SNOOP_MULTICAST_VLAN_STATUS (u4Instance, SNOOP_INFO_IGS_ENTRY)
        == SNOOP_DISABLE)
    {
        return SNOOP_FALSE;
    }

    /* Multicast VLAN translation only for report or leave packets */

    switch (pSnoopPktInfo->u1PktType)
    {
        case SNOOP_IGMP_V1REPORT:
        case SNOOP_IGMP_V2REPORT:
        case SNOOP_IGMP_V3REPORT:
        case SNOOP_IGMP_LEAVE:
            break;

        case SNOOP_ROUTER_MSG:
        case SNOOP_DATA_PKT:
        case SNOOP_IGMP_QUERY:
        case SNOOP_IGMP_PROXY_QUERY:
        default:
            return SNOOP_FALSE;
    }

    /* Only Priority tagged and Untagged packets will be associated
     * with a Multicast VLAN */

    if (VlanInfo.OuterVlanTag.u1TagType == SNOOP_VLAN_TAGGED)
    {
        return SNOOP_FALSE;
    }
    return SNOOP_TRUE;
}

/*****************************************************************************/
/* Function Name      : IgsMVlanLookupAndProcessPkt                          */
/*                                                                           */
/* Description        : This routine will look for the M-VLAN translation    */
/*                      for the incoming packet. If there is any Multicast   */
/*                      VLAN configured for the incoming Group/Source address*/
/*                      The particular report/leave will be associated with  */
/*                      the M-VLAN.                                          */
/*                                                                           */
/* Input(s)           : pBuf         - Message                               */
/*                      u4InstanceId - Instance Identifier                   */
/*                      u2LocalPort  - Incoming port number                  */
/*                      VlanId       - Vlan Information of the packeti       */
/*                      pSnoopPktInfo - Structure contains IGMP/MLD packet   */
/*                                      information.                         */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsMVlanLookupAndProcessPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Instance,
                             UINT2 u2LocalPort, tSnoopTag VlanId,
                             tSnoopPktInfo * pSnoopPktInfo)
{
    INT4                i4RetStatus;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    switch (pSnoopPktInfo->u1PktType)
    {
        case SNOOP_IGMP_V1REPORT:
        case SNOOP_IGMP_V2REPORT:
        case SNOOP_IGMP_LEAVE:

            i4RetStatus = IgsMVlanProcessV1V2Packet (pBuf, u4Instance,
                                                     u2LocalPort, VlanId,
                                                     pSnoopPktInfo);
            if (i4RetStatus == SNOOP_FAILURE)
            {
                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "IgsMVlanProcessV1V2Packet fail\r\n");
            }
            break;

        case SNOOP_IGMP_V3REPORT:

            i4RetStatus =
                IgsMVlanProcessV3Packet (pBuf, u4Instance, u2LocalPort, VlanId,
                                         pSnoopPktInfo);
            if (i4RetStatus == SNOOP_FAILURE)
            {
                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "IgsMVlanProcessV1V2Packet fail\r\n");
            }
            break;

        default:
            i4RetStatus = SNOOP_FAILURE;
    }
    return i4RetStatus;
}

/*****************************************************************************/
/* Function Name      : IgsMVlanProcessV1V2Packet                            */
/*                                                                           */
/* Description        : This routine will look for the M-VLAN translation    */
/*                      for the incoming V1/V2 packet.                       */
/*                                                                           */
/* Input(s)           : pBuf         - Message                               */
/*                      u4InstanceId - Instance Identifier                   */
/*                      u2LocalPort  - Incoming port number                  */
/*                      VlanId       - Vlan Information of the packet        */
/*                      pSnoopPktInfo - Structure contains IGMP/MLD packet   */
/*                                      information.                         */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsMVlanProcessV1V2Packet (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Instance,
                           UINT2 u2LocalPort, tSnoopTag VlanId,
                           tSnoopPktInfo * pSnoopPktInfo)
{
    tMVlanMapping      *pMVlanMapping = NULL;
    UINT4               u4GrpAddr;

    u4GrpAddr = SNOOP_PTR_FETCH_4 (pSnoopPktInfo->GroupAddr.au1Addr);

    /* Loop for all the Profiles configured, to search for the match */
    SNOOP_SLL_SCAN (&(SNOOP_INSTANCE_INFO (u4Instance)->MVlanMappingList),
                    pMVlanMapping, tMVlanMapping *)
    {
        /* Pass the ProfileId and GroupAddress to TAC module, to find
         * whether the GroupAddress is configured in the particular 
         * ProfileId */
        if (SnoopTacSearchGrpAddressInProfile (pMVlanMapping->u4ProfileId,
                                               u4GrpAddr) == SNOOP_TRUE)
        {
            /* We got match in this profile. Learn this entry in the M-VLAN */
            SNOOP_OUTER_VLAN (VlanId) = pMVlanMapping->VlanId;
            break;
        }
    }
    if (SnoopProcessMulticastPkt (pBuf, u4Instance, u2LocalPort, VlanId,
                                  pSnoopPktInfo) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Packet processing failed:\r\n");
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsMVlanProcessV3Packet                              */
/*                                                                           */
/* Description        : This routine will look for the M-VLAN translation    */
/*                      for the incoming V3 packet.                          */
/*                                                                           */
/* Input(s)           : pBuf         - Message                               */
/*                      u4InstanceId - Instance Identifier                   */
/*                      u2LocalPort  - Incoming port number                  */
/*                      VlanId       - Vlan Information of the packeti       */
/*                      pSnoopPktInfo - Structure contains IGMP/MLD packet   */
/*                                      information.                         */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsMVlanProcessV3Packet (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Instance,
                         UINT2 u2LocalPort, tSnoopTag VlanId,
                         tSnoopPktInfo * pSnoopPktInfo)
{
    tCRU_BUF_CHAIN_HEADER *pNewBuf = NULL;
    tMVlanMapping      *pMVlanMapping = NULL;
    tGrpRecords         GrpRecords;
    tSnoopTag           MVlanId;
    UINT4               u4TotalChannels = 0;
    UINT2               u2MulticastVlanId = 0;

    MEMSET (&GrpRecords, 0, sizeof (tGrpRecords));

    SNOOP_MEM_CPY (MVlanId, VlanId, sizeof (tSnoopTag));

    /* Form the SLL of Group & Source (Channels) received in the pkti an SLL */

    if (IgsMvlanFormGrpSrcRecordsFromPkt (pBuf, pSnoopPktInfo) == SNOOP_FAILURE)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "IgsMvlanFormGrpSrcRecordsFromPkt failed\r\n");
        return SNOOP_FAILURE;
    }
    /* Find number of Channels received in the packet */
    u4TotalChannels = SNOOP_SLL_COUNT (&gGrpRecList);

    GrpRecords.pGrpRecList = &gGrpRecList;

    /* Loop for all the Profiles configured, to search for the match */
    SNOOP_SLL_SCAN (&(SNOOP_INSTANCE_INFO (u4Instance)->MVlanMappingList),
                    pMVlanMapping, tMVlanMapping *)
    {
        GrpRecords.u4NumOfMatchFound = 0;

        if (SnoopTacSearchGrpSrcRecordInProfile (pMVlanMapping->u4ProfileId,
                                                 &GrpRecords) == SNOOP_FAILURE)
        {
            /* Profile is not in TAC module.. Strange !!! */
            continue;
        }
        if (GrpRecords.u4NumOfMatchFound == 0)
        {
            /* No Group/Source records are in this profile.. 
             * Go for the next profile :-(  */
            continue;
        }
        if (GrpRecords.u4NumOfMatchFound == u4TotalChannels)
        {
            /* All the records are classified to the same M-VLAN 
             * No need to look in the next profile.. Exit*/
            u2MulticastVlanId = pMVlanMapping->VlanId;
            break;
        }

        /* Some records are matched in this profile... Scan and find those */
        if (IgsUtilScanGrpRecordsAndFormPkt (pBuf, pSnoopPktInfo, TACM_TRUE,
                                             &pNewBuf) == SNOOP_FAILURE)
        {
            SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC,
                       "IgsUtilScanGrpRecordsAndFormPkt failed\r\n");
            SNOOP_FREE_GRP_SRC_RECORDLIST ();
            return SNOOP_FAILURE;
        }

        /* Associate the  packet to M-VLAN */
        SNOOP_OUTER_VLAN (MVlanId) = pMVlanMapping->VlanId;

        if (pNewBuf != NULL)
        {
            if (SnoopProcessMulticastPkt (pNewBuf, u4Instance, u2LocalPort,
                                          MVlanId,
                                          pSnoopPktInfo) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "Packet processing failed:\r\n");
                SNOOP_FREE_GRP_SRC_RECORDLIST ();
                SNOOP_RELEASE_CRU_BUF (pNewBuf, FALSE);
                return SNOOP_FAILURE;
            }
        }
    }                            /* Profiles SLL_SCAN */

    if ((GrpRecords.u4NumOfMatchFound == u4TotalChannels) ||
        (SNOOP_SLL_COUNT (&gGrpRecList) == u4TotalChannels))
    {
        if (GrpRecords.u4NumOfMatchFound == u4TotalChannels)
        {
            /* All the records are classified to same M-VLAN
             * No splitting of packet. Directly associate the 
             * packet to M-VLAN */
            SNOOP_OUTER_VLAN (MVlanId) = u2MulticastVlanId;
        }
        if (SnoopProcessMulticastPkt (pBuf, u4Instance, u2LocalPort, MVlanId,
                                      pSnoopPktInfo) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC, "Packet processing failed:\r\n");
            SNOOP_FREE_GRP_SRC_RECORDLIST ();
            return SNOOP_FAILURE;
        }
        SNOOP_FREE_GRP_SRC_RECORDLIST ();
        return SNOOP_SUCCESS;
    }

    if (SNOOP_SLL_COUNT (&gGrpRecList) > 0)
    {
        /* Still there are some pending entries, not classified to any
         * M-VLANs.... All those records should be classified to the
         * default incoming VLAN */
        if (IgsUtilScanGrpRecordsAndFormPkt (pBuf, pSnoopPktInfo, TACM_FALSE,
                                             &pNewBuf) == SNOOP_FAILURE)
        {
            SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC,
                       "IgsUtilScanGrpRecordsAndFormPkt failed\r\n");
            SNOOP_FREE_GRP_SRC_RECORDLIST ();
            return SNOOP_FAILURE;
        }

        if (pNewBuf != NULL)
        {
            /* Use the incoming  VLAN */
            if (SnoopProcessMulticastPkt
                (pNewBuf, u4Instance, u2LocalPort, VlanId,
                 pSnoopPktInfo) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "Packet processing failed:\r\n");
                SNOOP_FREE_GRP_SRC_RECORDLIST ();
                SNOOP_RELEASE_CRU_BUF (pNewBuf, FALSE);
                return SNOOP_FAILURE;
            }
        }
    }
    SNOOP_RELEASE_CRU_BUF (pBuf, FALSE);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsMvlanFormGrpSrcRecordsFromPkt                     */
/*                                                                           */
/* Description        : This routine will form the SLL of Group & Source     */
/*                      (Channels) received in the incoming V3 Packet.       */
/*                                                                           */
/* Input(s)           : pBuf         - Message                               */
/*                      u4InstanceId - Instance Identifier                   */
/*                      pSnoopPktInfo - Structure contains IGMP/MLD packet   */
/*                                      information.                         */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsMvlanFormGrpSrcRecordsFromPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  tSnoopPktInfo * pSnoopPktInfo)
{
    tSnoopSSMReport     SSMReport;
    tSnoopIgmpGroupRec  IgmpGrpRec;
    tGrpSrcStatusEntry *pGrpSrcStatusEntry = NULL;
    UINT4               u4GrpAddr;
    UINT4               u4SrcAddr;
    UINT2               u2GrpRecCount = 0;
    UINT2               u2SrcCount = 0;
    UINT2               u2NumGrpRec = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2OffSet = 0;
    UINT1               u1RecordType;
    INT1                i1Offset = 0;

    SNOOP_MEM_SET (&SSMReport, 0, sizeof (tSnoopSSMReport));

    SNOOP_MEM_SET (&IgmpGrpRec, 0, sizeof (tSnoopIgmpGroupRec));

    SNOOP_SLL_INIT (&gGrpRecList);

    SNOOP_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &SSMReport,
                             SNOOP_DATA_LINK_HDR_LEN + pSnoopPktInfo->u1TagLen +
                             pSnoopPktInfo->u1IpHdrLen,
                             sizeof (tSnoopSSMReport));

    u2NumGrpRec = SNOOP_NTOHS (SSMReport.u2NumGrps);

    /* Loop for all the Group Records */
    for (u2GrpRecCount = 0; u2GrpRecCount < u2NumGrpRec; u2GrpRecCount++)
    {
        SNOOP_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) &IgmpGrpRec,
                                 (SNOOP_DATA_LINK_HDR_LEN +
                                  pSnoopPktInfo->u1TagLen +
                                  pSnoopPktInfo->u1IpHdrLen +
                                  SNOOP_IGMP_HEADER_LEN + u2OffSet),
                                 sizeof (tSnoopIgmpGroupRec));

        u4GrpAddr = SNOOP_NTOHL (IgmpGrpRec.u4GroupAddress);
        u2NoSources = SNOOP_NTOHS (IgmpGrpRec.u2NumSrcs);
        u1RecordType = IgmpGrpRec.u1RecordType;

        if (u2NoSources != 0)
        {
            SNOOP_MEM_SET (gpSSMSrcInfoBuffer, 0, SNOOP_SRC_MEMBLK_SIZE);

            SNOOP_COPY_FROM_CRU_BUF (pBuf, (UINT1 *) gpSSMSrcInfoBuffer,
                                     (SNOOP_DATA_LINK_HDR_LEN +
                                      pSnoopPktInfo->u1TagLen +
                                      pSnoopPktInfo->u1IpHdrLen +
                                      SNOOP_IGMP_HEADER_LEN + u2OffSet
                                      + SNOOP_IGMP_SSMGROUP_RECD_SIZE),
                                     (UINT4) (u2NoSources * 4));

            for (u2SrcCount = 0, i1Offset = 0; u2SrcCount < u2NoSources;
                 u2SrcCount++)
            {
                SNOOP_MEM_CPY (&u4SrcAddr, gpSSMSrcInfoBuffer + i1Offset,
                               SNOOP_IP_ADDR_SIZE);
                u4SrcAddr = SNOOP_NTOHL (u4SrcAddr);
                i1Offset += (INT1) SNOOP_IP_ADDR_SIZE;

                if (SNOOP_GRPSRC_STATUS_ENTRY_ALLOC_MEMBLK (pGrpSrcStatusEntry)
                    == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_OS_RESOURCE_TRC,
                               SNOOP_OS_RES_DBG,
                               "Memory alloc for Host entry for "
                               "group failed\n");
                    SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                      SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                      SnoopSysErrString
                                      [SYS_LOG_MEM_ALLOC_FAIL]);
                    SNOOP_FREE_GRP_SRC_RECORDLIST ();
                    return SNOOP_FAILURE;
                }

                SNOOP_MEM_SET (pGrpSrcStatusEntry, 0,
                               sizeof (tGrpSrcStatusEntry));

                pGrpSrcStatusEntry->u4GroupAddress = u4GrpAddr;
                pGrpSrcStatusEntry->u4SourceAddress = u4SrcAddr;
                pGrpSrcStatusEntry->u1RecordType = u1RecordType;

                SNOOP_SLL_ADD (&gGrpRecList,
                               &pGrpSrcStatusEntry->NextGrpSrcStatusEntry);
            }
        }
        else
        {
            if (SNOOP_GRPSRC_STATUS_ENTRY_ALLOC_MEMBLK (pGrpSrcStatusEntry)
                == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_OS_RESOURCE_TRC,
                           SNOOP_OS_RES_DBG, "Memory alloc for Host entry for "
                           "group failed\n");
                SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                  SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                  SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
                SNOOP_FREE_GRP_SRC_RECORDLIST ();
                return SNOOP_FAILURE;
            }

            SNOOP_MEM_SET (pGrpSrcStatusEntry, 0, sizeof (tGrpSrcStatusEntry));

            pGrpSrcStatusEntry->u4GroupAddress = u4GrpAddr;
            pGrpSrcStatusEntry->u4SourceAddress = 0;
            pGrpSrcStatusEntry->u1RecordType = u1RecordType;

            SNOOP_SLL_ADD (&gGrpRecList,
                           &pGrpSrcStatusEntry->NextGrpSrcStatusEntry);
        }
        u2OffSet += (UINT2) ((u2NoSources * SNOOP_IP_ADDR_SIZE) +
                             SNOOP_IGMP_SSMGROUP_RECD_SIZE);
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsEncodeIgmpGrpRec                                  */
/*                                                                           */
/* Description        : This routine will form the new IGMP Group Record     */
/*                      header, using the incoming information.              */
/*                                                                           */
/* Input(s)           : u4GrpAddr    - Group Address in GrpRec Header        */
/*                      u2NumSrcs    - Number of sources in the GrpRec Header*/
/*                                                                           */
/* Output(s)          : ppOutBuf     - Pointer to Igmp Group Record Buffer   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeIgmpGrpRec (UINT4 u4GrpAddr, UINT2 u2NumSrcs, UINT1 u1RecordType,
                     tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tSnoopIgmpGroupRec  IgmpGrpRec;
    UINT4               u4PktSize;

    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);

    u4PktSize = (UINT4) (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                         (u2NumSrcs * SNOOP_IGMP_SSMSOURCE_RECD_SIZE));

    pBuf = SNOOP_ALLOC_CRU_BUF (u4PktSize, 0);
    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buf allocation failure in encoding group rec header\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                          SNOOP_BUFFER_TRC,
                          SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (&IgmpGrpRec, 0, sizeof (tSnoopIgmpGroupRec));

    IgmpGrpRec.u1RecordType = u1RecordType;
    IgmpGrpRec.u2NumSrcs = SNOOP_HTONS (u2NumSrcs);
    IgmpGrpRec.u1AuxDataLen = 0;
    IgmpGrpRec.u4GroupAddress = SNOOP_HTONL (u4GrpAddr);

    SNOOP_MEM_CPY (gpSSMRepSendBuffer, &IgmpGrpRec,
                   SNOOP_IGMP_SSMGROUP_RECD_SIZE);
    SNOOP_MEM_CPY (gpSSMRepSendBuffer + SNOOP_IGMP_SSMGROUP_RECD_SIZE,
                   gpSSMSrcInfoBuffer, u2NumSrcs * SNOOP_IP_ADDR_SIZE);

    SNOOP_COPY_OVER_CRU_BUF (pBuf, gpSSMRepSendBuffer, 0,
                             (UINT4) (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                      (u2NumSrcs * SNOOP_IP_ADDR_SIZE)));
    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsEncodeIgmpV3Header                                */
/*                                                                           */
/* Description        : This routine will form the new IGMP V3 Header        */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : ppOutBuf     - Pointer to Igmp V3 Header Buffer      */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeIgmpV3Header (tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tSnoopSSMReport     IgmpV3Hdr;

    pBuf = SNOOP_ALLOC_CRU_BUF (SNOOP_IGMP_HEADER_LEN, 0);
    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buf allocation failure in encoding V3 header\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                          SNOOP_BUFFER_TRC,
                          SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    IgmpV3Hdr.u1PktType = SNOOP_IGMP_V3REPORT;
    IgmpV3Hdr.u1Resvd = 0;
    IgmpV3Hdr.u2CheckSum = 0;
    IgmpV3Hdr.u2Resvd = 0;
    IgmpV3Hdr.u2NumGrps = 0;

    SNOOP_COPY_OVER_CRU_BUF (pBuf, (UINT1 *) &IgmpV3Hdr, 0,
                             SNOOP_IGMP_HEADER_LEN);
    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsUtilFormNewBufUptoIp                              */
/*                                                                           */
/* Description        : This routine will copy the buffer upto IP Header     */
/*                      from the incoming pBuf to pIpBuf                     */
/*                                                                           */
/* Input(s)           : pBuf         - Pointer to incoming packet.           */
/*                                                                           */
/* Output(s)          : ppOutBuf     - Pointer to Igmp V3 Header Buffer      */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsUtilFormNewBufUptoIp (tCRU_BUF_CHAIN_HEADER * pBuf, tCRU_BUF_CHAIN_HEADER
                         ** pIpBuf, tSnoopPktInfo * pSnoopPktInfo)
{
    UINT4               u4PktSize;

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN + pSnoopPktInfo->u1TagLen +
        pSnoopPktInfo->u1IpHdrLen;

    SNOOP_MEM_SET (gu1SnpIpHdr, 0, SNOOP_MAX_IP_BUF);

    *pIpBuf = SNOOP_ALLOC_CRU_BUF (u4PktSize, 0);
    if (*pIpBuf == NULL)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buf allocation failure in IgsUtilFormNewBufUptoIp\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                          SNOOP_BUFFER_TRC,
                          SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    SNOOP_COPY_FROM_CRU_BUF (pBuf, gu1SnpIpHdr, 0, u4PktSize);
    SNOOP_COPY_OVER_CRU_BUF (*pIpBuf, gu1SnpIpHdr, 0, u4PktSize);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsUtilScanGrpRecordsAndFormPkt                      */
/*                                                                           */
/* Description        : This routine will search for the u1SerchFlagged items*/
/*                      (TRUE or FALSE) in the gGrpRecList and form them to  */
/*                      a New Buffer.                                        */
/*                                                                           */
/* Input(s)           : pBuf         - Message                               */
/*                      pSnoopPktInfo- Structure contains IGMP/MLD packet    */
/*                                      information.                         */
/*                      u1SearchFlag - What flag to searh in the gGrpRecList,*/
/*                                     Flag can be either TRUE or FLASE      */
/*                                                                           */
/* Output(s)          : ppOutBuf     - Pointer to complete packet            */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsUtilScanGrpRecordsAndFormPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                 tSnoopPktInfo * pSnoopPktInfo,
                                 UINT1 u1SearchFlag,
                                 tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    tCRU_BUF_CHAIN_HEADER *pNewBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pIgmpHdrBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pIgmpGrpRecBuf = NULL;
    tGrpSrcStatusEntry *pGrpSrcStatusNode = NULL;
    tGrpSrcStatusEntry *pTempGrpSrcStatusNode = NULL;
    UINT4               u4PrevGrpAddr = 0;
    UINT4               u4IgmpPktSize = 0;
    UINT4               u4IpTotlen;
    UINT2               u2CheckSum;
    UINT2               u2NewGrpRecCnt = 0;
    UINT2               u2NoSourcesForThisRec = 0;
    UINT1               u1PrevRecType = 0;
    UINT1               u1Offset = 0;

    SNOOP_MEM_SET (gpSSMSrcInfoBuffer, 0, SNOOP_SRC_MEMBLK_SIZE);

    SNOOP_DYN_SLL_SCAN (&gGrpRecList, pGrpSrcStatusNode, pTempGrpSrcStatusNode,
                        tGrpSrcStatusEntry *)
    {
        if (pGrpSrcStatusNode->u1Status != u1SearchFlag)
            continue;

        /* Creating Group Record List(s) for a single profile */
        if (u2NewGrpRecCnt == 0)
        {
            /* Here comes the first Group Record for this packet *
             * Copy the Incoming buffer (Upto IP Header) to a new buffer */
            if (IgsUtilFormNewBufUptoIp (pBuf, &pNewBuf, pSnoopPktInfo)
                == SNOOP_FAILURE)
            {
                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "IgsUtilFormNewBufUptoIp failed\r\n");
                return SNOOP_FAILURE;
            }
            /* Form the IGMP Header */
            if (IgsEncodeIgmpV3Header (&pIgmpHdrBuf) == SNOOP_FAILURE)
            {
                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "IgsEncodeIgmpV3Header fail\r\n");
                SNOOP_RELEASE_CRU_BUF (pNewBuf, FALSE);
                return SNOOP_FAILURE;
            }
            u2NewGrpRecCnt++;
        }
        else if (u4PrevGrpAddr != pGrpSrcStatusNode->u4GroupAddress)
        {
            /* We got new group Record List-Form the Buf for old Record */
            if (IgsEncodeIgmpGrpRec (u4PrevGrpAddr, u2NoSourcesForThisRec,
                                     u1PrevRecType, &pIgmpGrpRecBuf)
                == SNOOP_FAILURE)
            {
                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "IgsEncodeIgmpGrpRec fail\r\n");
                SNOOP_RELEASE_CRU_BUF (pNewBuf, FALSE);
                SNOOP_RELEASE_CRU_BUF (pIgmpHdrBuf, FALSE);
                return SNOOP_FAILURE;
            }

            /* Join the newly formed GroupRecord Buffer with the IGMP
             * header Buffer */
            SNOOP_CRU_BUF_CONCAT_BUFCHAINS (pIgmpHdrBuf, pIgmpGrpRecBuf);

            /* Now be prepared for the next group record */
            SNOOP_MEM_SET (gpSSMSrcInfoBuffer, 0, SNOOP_SRC_MEMBLK_SIZE);
            u2NoSourcesForThisRec = 0;
            u1Offset = 0;
            u2NewGrpRecCnt++;
        }
        if (pGrpSrcStatusNode->u4SourceAddress != 0)
        {
            /* We got some source address in the packet, Store him
             * in the gpSSMSrcInfoBuffer, so that at end gpSSMSrcInfoBuffer
             * will be concat with the pIgmpGrpRecBuf */
            u2NoSourcesForThisRec++;
            pGrpSrcStatusNode->u4SourceAddress =
                SNOOP_HTONL (pGrpSrcStatusNode->u4SourceAddress);
            SNOOP_MEM_CPY (gpSSMSrcInfoBuffer + u1Offset,
                           &pGrpSrcStatusNode->u4SourceAddress,
                           SNOOP_IP_ADDR_SIZE);
            u1Offset = (UINT1) (u1Offset + SNOOP_IP_ADDR_SIZE);
        }

        /* Store the Current Group address, while moving for the
         * next one */
        u4PrevGrpAddr = pGrpSrcStatusNode->u4GroupAddress;
        u1PrevRecType = pGrpSrcStatusNode->u1RecordType;

        /* Delete the Processed nodes */
        SNOOP_SLL_DELETE (&gGrpRecList, pGrpSrcStatusNode);
        SNOOP_GRPSRC_STATUS_ENTRY_FREE_MEMBLK (pGrpSrcStatusNode);
    }                            /* Group Record List SLL_SCAN */

    /* In the above SLL_SCAN, last node will be missed from concating
     * to the buffer, doing that now.... */
    if (IgsEncodeIgmpGrpRec (u4PrevGrpAddr, u2NoSourcesForThisRec,
                             u1PrevRecType, &pIgmpGrpRecBuf) == SNOOP_FAILURE)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_PKT_TRC, "IgsEncodeIgmpGrpRec fail\r\n");
        SNOOP_RELEASE_CRU_BUF (pNewBuf, FALSE);
        SNOOP_RELEASE_CRU_BUF (pIgmpHdrBuf, FALSE);
        return SNOOP_FAILURE;
    }

    if (pIgmpHdrBuf == NULL)
    {
        SNOOP_RELEASE_CRU_BUF (pNewBuf, FALSE);
        SNOOP_RELEASE_CRU_BUF (pIgmpGrpRecBuf, FALSE);
        return SNOOP_FAILURE;
    }

    SNOOP_CRU_BUF_CONCAT_BUFCHAINS (pIgmpHdrBuf, pIgmpGrpRecBuf);

    /* We got the Group Record Lists for a report... Now prepare
     * the complete packet buffer */

    /* Copy the Number of Group Records to the buffer */
    u2NewGrpRecCnt = SNOOP_HTONS (u2NewGrpRecCnt);
    SNOOP_COPY_OVER_CRU_BUF (pIgmpHdrBuf, (UINT1 *) &u2NewGrpRecCnt,
                             6, SNOOP_OFFSET_TWO);

    u4IgmpPktSize = SNOOP_CRU_BUF_GET_BYTECOUNT (pIgmpHdrBuf);

    /* Calculate IP Packet Length */
    u4IpTotlen = pSnoopPktInfo->u1IpHdrLen + u4IgmpPktSize;

    /* Update the pSnoopPktInfo for Packet Length */
    pSnoopPktInfo->u2PktLen = (UINT2) u4IpTotlen;

    u4IpTotlen = SNOOP_HTONS (u4IpTotlen);

    if (pNewBuf == NULL)
    {
        SNOOP_RELEASE_CRU_BUF (pIgmpHdrBuf, FALSE);
        SNOOP_RELEASE_CRU_BUF (pIgmpGrpRecBuf, FALSE);
        return SNOOP_FAILURE;
    }

    SNOOP_COPY_OVER_CRU_BUF (pNewBuf, (UINT1 *) &u4IpTotlen,
                             (SNOOP_DATA_LINK_HDR_LEN +
                              pSnoopPktInfo->u1TagLen + SNOOP_OFFSET_TWO),
                             SNOOP_OFFSET_TWO);

    /* Calculate the IGMP Checksum & Update the buffer */
    u2CheckSum = IgsUtilIgmpCalcCheckSum (pIgmpHdrBuf, u4IgmpPktSize, 0);

    u2CheckSum = SNOOP_HTONS (u2CheckSum);

    SNOOP_COPY_OVER_CRU_BUF (pIgmpHdrBuf, (UINT1 *) &u2CheckSum,
                             SNOOP_OFFSET_TWO, SNOOP_OFFSET_TWO);

    /* Complete IGMP Header is ready, concat with the NewBuf.
     * (NewBuf is containing packet upto IP Header) */

    SNOOP_CRU_BUF_CONCAT_BUFCHAINS (pNewBuf, pIgmpHdrBuf);

    *ppOutBuf = pNewBuf;

    return SNOOP_SUCCESS;
}
