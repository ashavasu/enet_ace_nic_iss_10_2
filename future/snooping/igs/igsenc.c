/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/* $Id: igsenc.c,v 1.35 2017/09/11 13:33:29 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : igsenc.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping IGS decode module                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains IGS Encoding routines       */
/*                            for snooping (IGMP) module                     */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                       */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : IgsEncodeV1V2Report                                  */
/*                                                                           */
/* Description        : This function frames the V1 / V2 packet for the      */
/*                      Group entry and sends on to ports                    */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      u1PktType   - Indicates the pkt type                 */
/*                      u4GrpAddr   - Group Address                          */
/*                                                                           */
/* Output(s)          : ppOutBuf -  pointer to buffer                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeV1V2Report (UINT4 u4Instance, UINT1 u1PktType, UINT4 u4GroupAddr,
                     tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    tSnoopIgmpHdr       IgmpHdr;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4PktSize = 0;
    UINT4               u4SrcAddr;
    UINT2               u2EthType;

    SNOOP_MEM_SET (&IgmpHdr, 0, sizeof (tSnoopIgmpHdr));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));

    /*
     * For IGMP V1/ V2 packet size is just 8 bytes.
     * So allocate buf for MAC (14) header + IP (20 bytes)
     *  + IGMP (8) header  = 42 bytes. 
     */

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IP_HDR_RTR_OPT_LEN + SNOOP_IGMP_HEADER_LEN;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buffer allocation failure in forming V1/V2 report messages\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                          SNOOP_BUFFER_TRC,
                          SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    IgmpHdr.u1PktType = u1PktType;

    IgmpHdr.u1MaxRespTime = 0;

    IgmpHdr.u2CheckSum = 0;
    IgmpHdr.u4GrpAddr = SNOOP_HTONL (u4GroupAddr);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpHdr,
                               SNOOP_IGMP_PACKET_START_OFFSET,
                               SNOOP_IGMP_HEADER_LEN);

    IgmpHdr.u2CheckSum =
        IgsUtilIgmpCalcCheckSum (pBuf, SNOOP_IGMP_HEADER_LEN,
                                 SNOOP_IGMP_PACKET_START_OFFSET);

    IgmpHdr.u2CheckSum = SNOOP_HTONS (IgmpHdr.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpHdr.u2CheckSum,
                               SNOOP_IGMP_PACKET_START_OFFSET +
                               SNOOP_OFFSET_TWO, SNOOP_OFFSET_TWO);
    /* Assign 0.0.0.0 for Source IP as per standard */
    u4SrcAddr = 0;
    /* Source IP address will be set to Switch IP in Proxy mode */
    if (SNOOP_PROXY_STATUS
        (u4Instance, SNOOP_ADDR_TYPE_IPV4 - 1) == SNOOP_ENABLE)
    {
        u4SrcAddr = gu4SwitchIp;
    }
    /* Form IP Header */
    IgsUtilIpFormPacket (pBuf, u4SrcAddr, u4GroupAddr,
                         u1PktType, SNOOP_IGMP_HEADER_LEN);

    /* Fill MAC Header */
    SNOOP_GET_MAC_FROM_IPV4 (u4GroupAddr, MacGroupAddr);

    SNOOP_MEM_SET (&SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

#ifndef SRCMAC_CTP_WANTED
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);
#endif

    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IP_PKT_TYPE);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);

    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsEncodeV3Report                                    */
/*                                                                           */
/* Description        : This function frames the V3 packet for the Group     */
/*                      entry and sends on to router ports                   */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      pIgsConsGroupEntry - Pointer to the Consolidated     */
/*                                           group entry                     */
/*                      u1RecordType - Type of the group record to be sent   */
/*                                                                           */
/* Output(s)          : ppOutBuf -  pointer to buffer                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeV3Report (UINT4 u4Instance,
                   tSnoopConsolidatedGroupEntry * pIgsConsGroupEntry,
                   UINT1 u1RecordType, tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    tSnoopSSMReport     IgmpV3Hdr;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4IgmpPktSize = 0;
    UINT4               u4MaxLength = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4SrcAddr = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2EthType;
    UINT2               u2Index = 0;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&IgmpV3Hdr, 0, sizeof (tSnoopSSMReport));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));

    /*
     * For IGMP V3 packets the size depends upon the number of sources for 
     * the group.
     * So allocate buf for MAC (14) header + IP (20 bytes)
     *  + IGMP (8) header + Group Record (8) + (No of srcs * 4). 
     */

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8); u2Index++)
    {
        if ((u1RecordType == SNOOP_IS_INCLUDE) ||
            (u1RecordType == SNOOP_TO_INCLUDE))
        {
            OSIX_BITLIST_IS_BIT_SET (pIgsConsGroupEntry->InclSrcBmp, u2Index,
                                     SNOOP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                u2NoSources++;
            }
        }
        else
        {
            OSIX_BITLIST_IS_BIT_SET (pIgsConsGroupEntry->ExclSrcBmp, u2Index,
                                     SNOOP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                u2NoSources++;
            }
        }
    }

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IP_HDR_RTR_OPT_LEN +
        SNOOP_IGMP_HEADER_LEN + SNOOP_IGMP_SSMGROUP_RECD_SIZE +
        (u2NoSources * SNOOP_IGMP_SSMSOURCE_RECD_SIZE);

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buffer allocation failure in forming V3 report message\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                          SNOOP_BUFFER_TRC,
                          SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    IgmpV3Hdr.u1PktType = SNOOP_IGMP_V3REPORT;

    IgmpV3Hdr.u1Resvd = 0;

    IgmpV3Hdr.u2CheckSum = 0;
    IgmpV3Hdr.u2Resvd = 0;
    IgmpV3Hdr.u2NumGrps = SNOOP_HTONS (0x0001);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Hdr,
                               SNOOP_IGMP_PACKET_START_OFFSET,
                               SNOOP_IGMP_HEADER_LEN);

    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);

    IgsUtilFillV3SourceInfo (u4Instance, pIgsConsGroupEntry, u1RecordType,
                             u2NoSources, &u4MaxLength);

    CRU_BUF_Copy_OverBufChain (pBuf, gpSSMRepSendBuffer,
                               (SNOOP_IGMP_PACKET_START_OFFSET +
                                SNOOP_IGMP_HEADER_LEN), u4MaxLength);

    u4IgmpPktSize = SNOOP_IGMP_HEADER_LEN + u4MaxLength;

    /* Calculate IGMP checksum */
    IgmpV3Hdr.u2CheckSum =
        IgsUtilIgmpCalcCheckSum (pBuf, u4IgmpPktSize,
                                 SNOOP_IGMP_PACKET_START_OFFSET);

    IgmpV3Hdr.u2CheckSum = SNOOP_HTONS (IgmpV3Hdr.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Hdr.u2CheckSum,
                               SNOOP_IGMP_PACKET_START_OFFSET +
                               SNOOP_OFFSET_TWO, SNOOP_OFFSET_TWO);

    /* Assign 0.0.0.0 for Source IP as per standard */
    u4SrcAddr = 0;
    /* Source IP address will be set to Switch IP in Proxy mode */
    if (SNOOP_PROXY_STATUS
        (u4Instance, SNOOP_ADDR_TYPE_IPV4 - 1) == SNOOP_ENABLE)
    {
        u4SrcAddr = gu4SwitchIp;
    }
    /* Form IP Header */
    IgsUtilIpFormPacket (pBuf, u4SrcAddr,
                         SNOOP_PTR_FETCH_4 (pIgsConsGroupEntry->GroupIpAddr.
                                            au1Addr), SNOOP_IGMP_V3REPORT,
                         (UINT2) u4IgmpPktSize);

    /* Fill MAC Header, v3 Reports are sent to 224.0.0.16 (All IGMP v3 routers)
     * fill corresponding Dest MAC address */
    SNOOP_GET_MAC_FROM_IPV4 (SNOOP_ALL_IGMPV3_ROUTERS, MacGroupAddr);

    SNOOP_MEM_SET (&SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

    /* Set the Switch base MAC Address as Source MAC address */

#ifndef SRCMAC_CTP__WANTED
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);
#endif

    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IP_PKT_TYPE);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);
    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsEncodeQueryResponse                               */
/*                                                                           */
/* Description        : This function sends response to the query messages   */
/*                                                                           */
/* Input(s)           : u4Instance         - Instance number                 */
/*                      pIgsVlanEntry      - VLAN information                */
/*                      pIgsConsGroupEntry - Consolidated Group Information  */
/*                      u4InPort -  Port                                     */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeQueryResponse (UINT4 u4Instance, tSnoopVlanEntry * pIgsVlanEntry,
                        tSnoopConsolidatedGroupEntry * pIgsConsGroupEntry,
                        UINT4 u4InPort)
{
    UINT1              *pPortBitmap = NULL;
    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    UINT1               u1Version = SNOOP_IGS_IGMP_VERSION3;
    UINT1               u1RecordType = 0;
#ifdef ICCH_WANTED
    UINT4               u4IcchPort = 0;
    UINT2               u2LocalPort = 0;
#endif

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "IgsEncodeQueryResponse: "
                   "Error in allocating memory for pPortBitmap\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    /* if port is zero, we need to send the message on the router ports
     * else to single port */
    if (u4InPort != 0)
    {
        SNOOP_SLL_SCAN (&pIgsVlanEntry->RtrPortList,
                        pRtrPortNode, tSnoopRtrPortEntry *)
        {
            if (pRtrPortNode->u4Port == u4InPort)
            {
                u1Version = pRtrPortNode->u1OperVer;
                break;
            }
        }

        if (u1Version == SNOOP_IGS_IGMP_VERSION3)
        {
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                u1RecordType = SNOOP_IS_EXCLUDE;
            }
            else
            {
                u1RecordType =
                    ((pIgsConsGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                     SNOOP_IS_INCLUDE : SNOOP_IS_EXCLUDE);
            }
        }

        SNOOP_ADD_TO_PORT_LIST (u4InPort, pPortBitmap);
        /* Send the response on a single router port */
        if (IgsEncodeQueryRespMsg (u4Instance, pIgsVlanEntry,
                                   pIgsConsGroupEntry, u1Version, pPortBitmap,
                                   u1RecordType) != SNOOP_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pPortBitmap);
            return SNOOP_FAILURE;
        }

    }
    else
    {

        /* if v3 Router port list is not empty then send a V3 Report */
        if (SNOOP_MEM_CMP (pIgsVlanEntry->V3RtrPortBitmap, gNullPortBitMap,
                           sizeof (tSnoopPortBmp)) != 0)
        {
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                u1RecordType = SNOOP_IS_EXCLUDE;
            }
            else
            {

                u1RecordType =
                    ((pIgsConsGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                     SNOOP_TO_INCLUDE : SNOOP_TO_EXCLUDE);
            }

            u1Version = SNOOP_IGS_IGMP_VERSION3;
            SNOOP_MEM_CPY (pPortBitmap, pIgsVlanEntry->V3RtrPortBitmap,
                           sizeof (tSnoopPortBmp));

            /* If atleast one group entry is present, do not send consolidated
               report via ICCL. This is done, to avoid sending a report which will be
               learnt as a new group on ICCL interface in remote node */

#ifdef ICCH_WANTED
            if ((SNOOP_DLL_COUNT (&pIgsConsGroupEntry->GroupEntryList) == 1) &&
                (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE))
            {
                SnoopIcchGetIcclIfIndex (&u4IcchPort);

                if (SnoopVcmGetContextInfoFromIfIndex (u4IcchPort,
                                                       &u4Instance,
                                                       &u2LocalPort) ==
                    VCM_FAILURE)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_GRP_TRC,
                                    "IgsEncodeQueryResponse: "
                                    "Error in getting Context info for port %d \r\n",
                                    u4IcchPort);
                    UtilPlstReleaseLocalPortList (pPortBitmap);
                    return SNOOP_FAILURE;
                }

                SNOOP_DEL_FROM_PORT_LIST (u2LocalPort, pPortBitmap);
            }
#endif
            if (IgsEncodeQueryRespMsg (u4Instance, pIgsVlanEntry,
                                       pIgsConsGroupEntry, u1Version,
                                       pPortBitmap,
                                       u1RecordType) != SNOOP_SUCCESS)
            {
                UtilPlstReleaseLocalPortList (pPortBitmap);
                return SNOOP_FAILURE;
            }

        }
        /* if v2 Router port list is not empty then send a V2 Report */
        if (SNOOP_MEM_CMP (pIgsVlanEntry->V2RtrPortBitmap, gNullPortBitMap,
                           sizeof (tSnoopPortBmp)) != 0)
        {
            u1Version = SNOOP_IGS_IGMP_VERSION2;
            SNOOP_MEM_CPY (pPortBitmap, pIgsVlanEntry->V2RtrPortBitmap,
                           sizeof (tSnoopPortBmp));
            if (IgsEncodeQueryRespMsg (u4Instance, pIgsVlanEntry,
                                       pIgsConsGroupEntry, u1Version,
                                       pPortBitmap, 0) != SNOOP_SUCCESS)
            {
                UtilPlstReleaseLocalPortList (pPortBitmap);
                return SNOOP_FAILURE;
            }

        }
        /* if v1 Router port list is not empty then send a V1 Report */
        if (SNOOP_MEM_CMP (pIgsVlanEntry->V1RtrPortBitmap, gNullPortBitMap,
                           sizeof (tSnoopPortBmp)) != 0)
        {
            u1Version = SNOOP_IGS_IGMP_VERSION1;
            SNOOP_MEM_CPY (pPortBitmap, pIgsVlanEntry->V1RtrPortBitmap,
                           sizeof (tSnoopPortBmp));
            if (IgsEncodeQueryRespMsg (u4Instance, pIgsVlanEntry,
                                       pIgsConsGroupEntry, u1Version,
                                       pPortBitmap, 0) != SNOOP_SUCCESS)
            {
                UtilPlstReleaseLocalPortList (pPortBitmap);
                return SNOOP_FAILURE;
            }

        }

    }

    UtilPlstReleaseLocalPortList (pPortBitmap);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsEncodeQueryRespMsg                                */
/*                                                                           */
/* Description        : This function sends response to the query messages   */
/*                      based on the operating version of  the router ports  */
/*                      on which the report will be sent out                 */
/*                                                                           */
/* Input(s)           : u4Instance         - Instance number                 */
/*                      pIgsVlanEntry      - VLAN information                */
/*                      pIgsConsGroupEntry - Consolidated Group Information  */
/*                      u1Version          - Operating version of router port*/
/*                                           (PortBitmap)                    */
/*                      PortBitmap         - Router port list on which report*/
/*                                           will be sent out                */
/*                      u1RecodeType       - Record Type to be used when v3  */
/*                                           reports are sent out            */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeQueryRespMsg (UINT4 u4Instance, tSnoopVlanEntry * pIgsVlanEntry,
                       tSnoopConsolidatedGroupEntry * pIgsConsGroupEntry,
                       UINT1 u1Version, tSnoopPortBmp PortBitmap,
                       UINT1 u1RecordType)
{
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    UINT1               u1PktType = 0;
    UINT1               u1StatType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (u1Version == SNOOP_IGS_IGMP_VERSION3)
    {
        if (IgsEncodeV3Report (u4Instance, pIgsConsGroupEntry, u1RecordType,
                               &pOutBuf) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC,
                       "Unable to generate and send V3 report on to"
                       " router ports\r\n");
            return SNOOP_FAILURE;

        }
        u1StatType = SNOOP_SSMREPORT_SENT;
    }

    if ((u1Version == SNOOP_IGS_IGMP_VERSION1) ||
        (u1Version == SNOOP_IGS_IGMP_VERSION2))
    {
        u1PktType = ((u1Version == SNOOP_IGS_IGMP_VERSION2) ?
                     SNOOP_IGMP_V2REPORT : SNOOP_IGMP_V1REPORT);

        if (IgsEncodeV1V2Report
            (u4Instance, u1PktType,
             SNOOP_PTR_FETCH_4 (pIgsConsGroupEntry->GroupIpAddr.au1Addr),
             &pOutBuf) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC,
                       "Unable to generate and send V1/V2 report on to"
                       " router ports\r\n");
            return SNOOP_FAILURE;
        }

        u1StatType = SNOOP_ASMREPORT_SENT;
        if (u1PktType == SNOOP_IGMP_V2REPORT)
        {
            if (SnoopVlanUpdateStats (u4Instance, pIgsVlanEntry,
                                      SNOOP_V2REPORT_SENT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_PKT_TRC, "Unable to update statistics "
                                "for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pIgsVlanEntry->VlanId));
            }

        }
        else if (u1PktType == SNOOP_IGMP_V1REPORT)
        {
            if (SnoopVlanUpdateStats (u4Instance, pIgsVlanEntry,
                                      SNOOP_V1REPORT_SENT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_PKT_TRC, "Unable to update statistics "
                                "for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pIgsVlanEntry->VlanId));
            }

        }

    }

    SnoopVlanForwardPacket (u4Instance, pIgsVlanEntry->VlanId,
                            SNOOP_ADDR_TYPE_IPV4,
                            SNOOP_INVALID_PORT,
                            VLAN_FORWARD_SPECIFIC, pOutBuf, PortBitmap,
                            u1StatType);

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : IgsFormGrpQueryFromGrpSrcQuery                       */
/*                                                                           */
/* Description        : This function forms group query from group and       */
/*                      and source specific query                            */
/*                                                                           */
/* Input(s)           : u4Instance - The instance Id.                        */
/*                      pInBuf  - Pointer to grp and src specific query      */
/*                      pSnoopPktInfo - Snoop Packet Info                    */
/*                      pOutBuf    - pointer to output buffer                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsFormGrpQueryFromGrpSrcQuery (UINT4 u4Instance,
                                tCRU_BUF_CHAIN_HEADER * pInBuf,
                                tSnoopPktInfo * pSnoopPktInfo,
                                tCRU_BUF_CHAIN_HEADER * pOutBuf)
{
    tSnoopIgmpHdr       IgmpHdr;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    UINT4               u4SrcAddr = 0;
    UINT4               u4GrpAddr = 0;
    UINT2               u2EthType;
    UINT2               u2QQICOffSet = 0;
    UINT1               u1V3HdrLen = 0;
    UINT1               u1QQIC = 0;
    UINT1               u1QRV = 0;
    UINT1               au1IgmpV3Hdr[SNOOP_IGMPV3_QRY_HDR];

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (&IgmpHdr, 0, sizeof (tSnoopIgmpHdr));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&au1IgmpV3Hdr, 0, SNOOP_IGMPV3_QRY_HDR);

    /* Fill the packet type as Query (0x11) */
    /* Retrieve the information from incoming grp and src specific query and 
     * form the group specific query */

    IgmpHdr.u1PktType = SNOOP_IGMP_QUERY;

    IgmpHdr.u1MaxRespTime = (UINT1) pSnoopPktInfo->u2MaxRespCode;

    /* For Specific Query IGMP Pkt's Group field should u4Group */
    u4GrpAddr = SNOOP_PTR_FETCH_4 (pSnoopPktInfo->GroupAddr.au1Addr);
    IgmpHdr.u4GrpAddr = SNOOP_HTONL (u4GrpAddr);

    IgmpHdr.u2CheckSum = 0;

    CRU_BUF_Copy_OverBufChain (pOutBuf, (UINT1 *) &IgmpHdr,
                               SNOOP_IGMP_PACKET_START_OFFSET,
                               SNOOP_IGMP_HEADER_LEN);

    u2QQICOffSet = (UINT2) (SNOOP_DATA_LINK_HDR_LEN + pSnoopPktInfo->u1TagLen +
                            pSnoopPktInfo->u1IpHdrLen + SNOOP_IGMP_HEADER_LEN);

    CRU_BUF_Copy_FromBufChain (pInBuf, (UINT1 *) &u1QQIC,
                               u2QQICOffSet + SNOOP_V3_QQIC_OFFSET,
                               sizeof (UINT1));

    CRU_BUF_Copy_FromBufChain (pInBuf, (UINT1 *) &u1QRV,
                               u2QQICOffSet + SNOOP_V3_QRV_OFFSET,
                               sizeof (UINT1));

    au1IgmpV3Hdr[0] = u1QRV;
    au1IgmpV3Hdr[1] = u1QQIC;
    au1IgmpV3Hdr[2] = 0;
    au1IgmpV3Hdr[3] = 0;

    u1V3HdrLen = SNOOP_IGMPV3_QRY_HDR;

    CRU_BUF_Copy_OverBufChain (pOutBuf, (UINT1 *) &au1IgmpV3Hdr,
                               (SNOOP_IGMP_PACKET_START_OFFSET +
                                SNOOP_IGMP_HEADER_LEN), SNOOP_IGMPV3_QRY_HDR);

    /* Calculate IGMP checksum */
    IgmpHdr.u2CheckSum =
        IgsUtilCalculateIpCheckSum (pOutBuf,
                                    SNOOP_IGMP_HEADER_LEN + u1V3HdrLen,
                                    SNOOP_IGMP_PACKET_START_OFFSET);

    IgmpHdr.u2CheckSum = SNOOP_HTONS (IgmpHdr.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pOutBuf, (UINT1 *) &IgmpHdr.u2CheckSum,
                               SNOOP_IGMP_PACKET_START_OFFSET + 2, 2);
    u4SrcAddr = SNOOP_PTR_FETCH_4 (pSnoopPktInfo->SrcIpAddr.au1Addr);

    /* Form IP Header */
    IgsUtilIpFormPacket (pOutBuf, u4SrcAddr, u4GrpAddr,
                         SNOOP_GRP_QUERY,
                         (UINT2) (SNOOP_IGMP_HEADER_LEN + u1V3HdrLen));

    SNOOP_GET_MAC_FROM_IPV4 (u4GrpAddr, MacGroupAddr);

    SNOOP_MEM_SET (SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

#ifndef SRCMAC_CTP_WANTED
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);
#endif

    CRU_BUF_Copy_OverBufChain (pOutBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pOutBuf, SrcMacAddr,
                               SNOOP_ETH_ADDR_SIZE, SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IP_PKT_TYPE);
    CRU_BUF_Copy_OverBufChain (pOutBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsEncodeQuery                                       */
/*                                                                           */
/* Description        : This function forms query messages                   */
/*                      (General/ Grp specific Query)                        */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance identifier                    */
/*                    : u4GrpAddr   - Group Address                          */
/*                    : u1PktType   - Packet type (general / Grp spec qry)   */
/*                    : u1Version   -  Query version                         */
/*                    : pSnoopVlanEntry - Vlan entry                         */
/*                                                                           */
/* Output(s)          : ppOutBuf    - pointer to output buffer               */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeQuery (UINT4 u4Instance, UINT4 u4GrpAddr, UINT1 u1PktType,
                UINT1 u1Version, tCRU_BUF_CHAIN_HEADER ** ppOutBuf,
                tSnoopVlanEntry * pSnoopVlanEntry)
{
    UINT4               u4SrcAddr = 0;
    tSnoopIgmpHdr       IgmpHdr;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT2               u2EthType;
    UINT4               u4PktSize = 0;
    UINT1               u1V3HdrLen = 0;
    UINT1               au1IgmpV3Hdr[SNOOP_IGMPV3_QRY_HDR];

    /* For IGMP IGMP packet size is 8 bytes.
     * So allocate buf for MAC (14) header + IP (24 bytes)
     *  + IGMP (8) header = 46 bytes. */

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (&IgmpHdr, 0, sizeof (tSnoopIgmpHdr));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&au1IgmpV3Hdr, 0, SNOOP_IGMPV3_QRY_HDR);

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IP_HDR_RTR_OPT_LEN + SNOOP_IGMP_HEADER_LEN;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buffer allocation failure in forming query messages\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                          SNOOP_BUFFER_TRC,
                          SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    /* Fill the packet type as Query (0x11) */
    IgmpHdr.u1PktType = SNOOP_IGMP_QUERY;

    /* If the version is v1 set the max response time to 0 */
    if (u1Version == SNOOP_IGS_IGMP_VERSION1)
    {
        IgmpHdr.u1MaxRespTime = 0;
    }
    else
    {
        IgmpHdr.u1MaxRespTime = (UINT1) pSnoopVlanEntry->u2MaxRespCode;
    }

    /* For Specific Query IGMP Pkt's Group field should u4Group */
    if (u1PktType == SNOOP_GRP_QUERY)
    {
        if (u1Version != SNOOP_IGS_IGMP_VERSION1)
        {
            IgmpHdr.u1MaxRespTime = (UINT1) (SNOOP_INSTANCE_INFO (u4Instance)->
                                             SnoopInfo[pSnoopVlanEntry->
                                                       u1AddressType -
                                                       1].u4GrpQueryInt *
                                             SNOOP_IGS_MRC_UNIT);
        }
        IgmpHdr.u4GrpAddr = SNOOP_HTONL (u4GrpAddr);
    }
    else
    {
        IgmpHdr.u4GrpAddr = 0;
    }

    IgmpHdr.u2CheckSum = 0;

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpHdr,
                               SNOOP_IGMP_PACKET_START_OFFSET,
                               SNOOP_IGMP_HEADER_LEN);

    if (u1Version == SNOOP_IGS_IGMP_VERSION3)
    {
        au1IgmpV3Hdr[0] = SNOOP_ROB_VARIABLE;

        au1IgmpV3Hdr[1] = (UINT1) pSnoopVlanEntry->u2QueryInterval;
        /* Forming QQIC from QQI and inserting into v3 query */
        if (pSnoopVlanEntry->u2QueryInterval >= SNOOP_QUERY_INTERVAL_CODE)
        {
            SNOOP_GET_CODE_FROM_TIME (au1IgmpV3Hdr[1],
                                      pSnoopVlanEntry->u2QueryInterval);
        }
        au1IgmpV3Hdr[2] = 0;
        au1IgmpV3Hdr[3] = 0;

        u1V3HdrLen = SNOOP_IGMPV3_QRY_HDR;

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &au1IgmpV3Hdr,
                                   (SNOOP_IGMP_PACKET_START_OFFSET +
                                    SNOOP_IGMP_HEADER_LEN),
                                   SNOOP_IGMPV3_QRY_HDR);
    }

    /* Calculate IGMP checksum */
    IgmpHdr.u2CheckSum =
        IgsUtilCalculateIpCheckSum (pBuf,
                                    SNOOP_IGMP_HEADER_LEN + u1V3HdrLen,
                                    SNOOP_IGMP_PACKET_START_OFFSET);

    IgmpHdr.u2CheckSum = SNOOP_HTONS (IgmpHdr.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpHdr.u2CheckSum,
                               SNOOP_IGMP_PACKET_START_OFFSET + 2, 2);

    /* u4SrcAddr is filled with the below logic: (CISCO complaince) */
    if (pSnoopVlanEntry->u4QuerierIp != SNOOP_FALSE)
    {
        /* If user has configured querier IP, u4SrcAddr should
         * be assigned querier IP */
        u4SrcAddr = pSnoopVlanEntry->u4QuerierIp;
    }
    else if (pSnoopVlanEntry->u1IpFlag == SNOOP_TRUE)

    {
        /*If ipflag is set, either ip address of
         *interface vlan/switch ip can be used*/
        if (pSnoopVlanEntry->u4InterfaceIp != SNOOP_FALSE)
        {
            u4SrcAddr = pSnoopVlanEntry->u4InterfaceIp;
        }
        else
        {
            u4SrcAddr = gu4SwitchIp;
        }

    }
    else
    {
        if (SNOOP_PROXY_REPORTING_STATUS (u4Instance, SNOOP_ADDR_TYPE_IPV4 - 1)
            == SNOOP_ENABLE)
        {
            /* If ipflag is not set, 0.0.0.0 can be used
             * (To acheive backward compalitibilty in Proxy Reporting mode) */
            u4SrcAddr = 0;
        }
        else
        {
            u4SrcAddr = gu4SwitchIp;
        }
    }

    /*In a network only querier will send Query Packets.
       Since u4SrcAddr will be used in query messages, update
       elected querier with u4SrcAddr */
    pSnoopVlanEntry->u4ElectedQuerier = u4SrcAddr;

    /* Form IP Header */
    IgsUtilIpFormPacket (pBuf, u4SrcAddr, u4GrpAddr,
                         u1PktType,
                         (UINT2) (SNOOP_IGMP_HEADER_LEN + u1V3HdrLen));

    /* Fill MAC Header */
    if (u1PktType == SNOOP_GRP_QUERY)
    {
        SNOOP_GET_MAC_FROM_IPV4 (u4GrpAddr, MacGroupAddr);
    }
    else
    {
        SNOOP_GET_MAC_FROM_IPV4 (SNOOP_ALL_HOST_IP, MacGroupAddr);
    }

    SNOOP_MEM_SET (SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

#ifndef SRCMAC_CTP_WANTED
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);
#endif

    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr,
                               SNOOP_ETH_ADDR_SIZE, SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IP_PKT_TYPE);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);

    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsEncodeAggV3Report                                 */
/*                                                                           */
/* Description        : This function forms the Aggregated V3 report         */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      u2NumGrpRec - Number of groups                       */
/*                      u4MaxLength - Packet size                            */
/*                                                                           */
/* Output(s)          : ppOutBuf -  pointer to buffer                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeAggV3Report (UINT4 u4Instance, UINT2 u2NumGrpRec, UINT4 u4MaxLength,
                      tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    tSnoopSSMReport     IgmpV3Hdr;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4IgmpPktSize = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u2PktSize = 0;
    UINT2               u2EthType;

    SNOOP_MEM_SET (&IgmpV3Hdr, 0, sizeof (tSnoopSSMReport));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* If the mode is MAC 14 (MAC) + 24 (Ip with Router Alert option)
       + 8 (IGMP) +  group record (s) size  */
    u2PktSize = (SNOOP_DATA_LINK_HDR_LEN +
                 (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
                 SNOOP_IP_HDR_RTR_OPT_LEN + SNOOP_IGMP_HEADER_LEN +
                 u4MaxLength);

    pBuf = CRU_BUF_Allocate_MsgBufChain (u2PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buffer allocation failure in forming V3 report message\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                          SNOOP_BUFFER_TRC,
                          SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    IgmpV3Hdr.u1PktType = SNOOP_IGMP_V3REPORT;

    IgmpV3Hdr.u1Resvd = 0;

    IgmpV3Hdr.u2CheckSum = 0;
    IgmpV3Hdr.u2Resvd = 0;
    IgmpV3Hdr.u2NumGrps = SNOOP_HTONS (u2NumGrpRec);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Hdr,
                               SNOOP_IGMP_PACKET_START_OFFSET,
                               SNOOP_IGMP_HEADER_LEN);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) gpSSMRepSendBuffer,
                               (SNOOP_IGMP_PACKET_START_OFFSET +
                                SNOOP_IGMP_HEADER_LEN), u4MaxLength);

    u4IgmpPktSize = SNOOP_IGMP_HEADER_LEN + u4MaxLength;
    /* Calculate IGMP checksum */
    IgmpV3Hdr.u2CheckSum =
        IgsUtilIgmpCalcCheckSum (pBuf, u4IgmpPktSize,
                                 SNOOP_IGMP_PACKET_START_OFFSET);

    IgmpV3Hdr.u2CheckSum = SNOOP_HTONS (IgmpV3Hdr.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Hdr.u2CheckSum,
                               SNOOP_IGMP_PACKET_START_OFFSET + 2, 2);

    /* Assign 0.0.0.0 for Source IP as per standard */
    u4SrcAddr = 0;
    /* Source IP address will be set to Switch IP in Proxy mode */
    if (SNOOP_PROXY_STATUS
        (u4Instance, SNOOP_ADDR_TYPE_IPV4 - 1) == SNOOP_ENABLE)
    {
        u4SrcAddr = gu4SwitchIp;
    }
    /* Form IP Header , V3 Report should go to All V3 Routers 224.0.0.22 */
    IgsUtilIpFormPacket (pBuf, u4SrcAddr, SNOOP_ALL_IGMPV3_ROUTERS,
                         SNOOP_IGMP_V3REPORT, (UINT2) u4IgmpPktSize);

    /* Fill MAC Header */
    SNOOP_GET_MAC_FROM_IPV4 (SNOOP_ALL_IGMPV3_ROUTERS, MacGroupAddr);

    SNOOP_MEM_SET (SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

#ifndef SRCMAC_CTP_WANTED
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);
#endif

    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IP_PKT_TYPE);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);
    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsEncodeLeaveMsg                                    */
/*                                                                           */
/* Description        : This function forms leave  messages                  */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                    : u4GrpAddr   - Group Address                          */
/*                                                                           */
/* Output(s)          : ppOutBuf        - pointer to output buffer           */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeLeaveMsg (UINT4 u4Instance, UINT4 u4GrpAddr,
                   tCRU_BUF_CHAIN_HEADER ** ppOutBuf)
{
    UINT4               u4SrcAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tSnoopIgmpHdr       IgmpHdr;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    UINT4               u4PktSize = 0;
    UINT2               u2EthType = 0;

    SNOOP_MEM_SET (&IgmpHdr, 0, sizeof (tSnoopIgmpHdr));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));

    /* Form leave message allocate buf for MAC (14) header + * IP (20 bytes)
     * + IGMP (8) header = 42 bytes. 
     */

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IP_HDR_RTR_OPT_LEN + SNOOP_IGMP_HEADER_LEN;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "Buffer allocation failure in forming leave message\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                          SNOOP_BUFFER_TRC,
                          SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    IgmpHdr.u1PktType = SNOOP_IGMP_LEAVE;

    /* Maximum Response time will be set as 0 for a Leave message */
    IgmpHdr.u1MaxRespTime = 0;

    /* In Leave message, the IGMP Packet's Group field should be the group 
     * addres for which the leave is sent  */
    IgmpHdr.u4GrpAddr = SNOOP_HTONL (u4GrpAddr);

    IgmpHdr.u2CheckSum = 0;

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpHdr,
                               SNOOP_IGMP_PACKET_START_OFFSET,
                               SNOOP_IGMP_HEADER_LEN);

    /* Calculate IGMP checksum */
    IgmpHdr.u2CheckSum =
        IgsUtilCalculateIpCheckSum (pBuf, SNOOP_IGMP_HEADER_LEN,
                                    SNOOP_IGMP_PACKET_START_OFFSET);

    IgmpHdr.u2CheckSum = SNOOP_HTONS (IgmpHdr.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpHdr.u2CheckSum,
                               SNOOP_IGMP_PACKET_START_OFFSET + 2, 2);
    /* Assign 0.0.0.0 for Source IP as per standard */
    u4SrcAddr = 0;
    /* Source IP address will be set to Switch IP in Proxy mode */
    if (SNOOP_PROXY_STATUS
        (u4Instance, SNOOP_ADDR_TYPE_IPV4 - 1) == SNOOP_ENABLE)
    {
        u4SrcAddr = gu4SwitchIp;
    }
    /* Form IP Header */
    IgsUtilIpFormPacket (pBuf, u4SrcAddr, u4GrpAddr, SNOOP_IGMP_LEAVE,
                         SNOOP_IGMP_HEADER_LEN);

    /* Get source MAC address */
    SNOOP_MEM_SET (SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

#ifndef SRCMAC_CTP_WANTED
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);
#endif

    /* Fill Destination MAC Header */
    SNOOP_GET_MAC_FROM_IPV4 (SNOOP_ALL_ROUTER_IP, MacGroupAddr);

    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    /* Fill Ethernet type as IP packet */
    u2EthType = SNOOP_HTONS (SNOOP_IP_PKT_TYPE);
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);

    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsEncodeAndSendLeaveMsg                             */
/*                                                                           */
/* Description        : This function forms and send the Leave/Report        */
/*                      message                                              */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                    : Vlan        - VLAN Identifier                        */
/*                    : pIgsConsGroupEntry - pointer to the IGS consolidated */
/*                                           group entry                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeAndSendLeaveMsg (UINT4 u4Instance, tSnoopTag VlanId,
                          tSnoopConsolidatedGroupEntry * pIgsConsGroupEntry)
{
    UINT1              *pTempPortBitMap = NULL;
    UINT1              *pPortBitmap = NULL;
    UINT1              *pFwdPortBitmap = NULL;
    UINT1              *pVlanPortBitmap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    tCRU_BUF_CHAIN_HEADER *pReportBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pLeaveMsgBuf = NULL;
    tSnoopVlanEntry    *pIgsVlanEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGroupEntry   *pSnpTmpGroupEntry = NULL;
    tIPvXAddr           GrpIpAddr;
    tIPvXAddr           DestIpAddr;
    UINT1               u1ReportFwd = SNOOP_FALSE;
    UINT1               u1LeaveFwd = SNOOP_FALSE;
    UINT1               u1LeaveFrwd = SNOOP_FALSE;
    UINT1               u1SnpLeaveFwd = SNOOP_FALSE;
    UINT1               u1Forward = VLAN_FORWARD_SPECIFIC;
    UINT1               u1Version = SNOOP_IGS_IGMP_VERSION3;
    UINT4               u4LearntPort = 0;
    UINT2               u2BytIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT4               u4LrtPortCount = 0;
    UINT4               u4RtrPortCount = 0;
    BOOL1               bRes = OSIX_FALSE;

    SNOOP_MEM_SET (&DestIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&GrpIpAddr, 0, sizeof (tIPvXAddr));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* Get the VLAN entry  from the VLAN id this is used for constucting 
     * and sending a leave or V3 report based on the operating version */

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                               SNOOP_ADDR_TYPE_IPV4,
                               &pIgsVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_VLAN_TRC,
                   "Unable to get Vlan record for the given instance "
                   "and the VlanId \r\n");
        return SNOOP_FAILURE;
    }

    if (pIgsVlanEntry->pSnoopVlanCfgEntry != NULL)
    {
        u1Version = pIgsVlanEntry->pSnoopVlanCfgEntry->u1ConfigOperVer;
    }

    if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
    {
        /* Get the Group entry */
        IPVX_ADDR_COPY (&GrpIpAddr, &(pIgsConsGroupEntry->GroupIpAddr));
        if (SnoopGrpGetGroupEntry (u4Instance, VlanId, GrpIpAddr,
                                   &pSnpTmpGroupEntry) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_VLAN_TRC,
                       "Unable to get group record for the given instance "
                       "and the VlanId \r\n");
            return SNOOP_FAILURE;
        }
    }

    pTempPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pTempPortBitMap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "IgsEncodeAndSendLeaveMsg: "
                   "Error in allocating memory for pTempPortBitMap\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pTempPortBitMap, 0, sizeof (tSnoopPortBmp));

    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "IgsEncodeAndSendLeaveMsg: "
                   "Error in allocating memory for pPortBitmap\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        UtilPlstReleaseLocalPortList (pTempPortBitMap);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    pFwdPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pFwdPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "IgsEncodeAndSendLeaveMsg: "
                   "Error in allocating memory for pFwdPortBitmap\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        UtilPlstReleaseLocalPortList (pTempPortBitMap);
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_FAILURE;
    }
    MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));

    pVlanPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pVlanPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "IgsEncodeAndSendLeaveMsg: "
                   "Error in allocating memory for pVlanPortBitmap\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        UtilPlstReleaseLocalPortList (pTempPortBitMap);
        UtilPlstReleaseLocalPortList (pPortBitmap);
        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
        return SNOOP_FALSE;
    }
    SNOOP_MEM_SET (pVlanPortBitmap, 0, sizeof (tSnoopPortBmp));

    pSnoopRtrPortBmp = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pSnoopRtrPortBmp == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "IgsEncodeAndSendLeaveMsg: "
                   "Error in allocating memory for pSnoopRtrPortBmp\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        UtilPlstReleaseLocalPortList (pTempPortBitMap);
        UtilPlstReleaseLocalPortList (pPortBitmap);
        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
        return SNOOP_FAILURE;
    }
    MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

    /* Check if the Proxy Reporting/Proxy is enable, if it is enabled then
     * construct the Leave/Report message based on the version information
     * available in the IGS VLAN table and forward that packet only on to
     * the Router ports */
    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance, SNOOP_INFO_IGS_ENTRY)
        == SNOOP_TRUE)
    {
        /* Check if the Version is V3 then construct the INCLUDE NONE
         * report message */

        if (((SNOOP_INSTANCE_INFO (u4Instance)->
              SnoopInfo[pIgsVlanEntry->u1AddressType - 1].
              u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
             (u1Version == SNOOP_IGS_IGMP_VERSION3) &&
             (SNOOP_INSTANCE_INFO (u4Instance)->
              SnoopInfo[pIgsVlanEntry->u1AddressType - 1].
              u1QueryFwdAll == SNOOP_FORWARD_ALL_PORTS)) ||
            ((u1Version == SNOOP_IGS_IGMP_VERSION3) &&
             (SNOOP_INSTANCE_INFO (u4Instance)->
              SnoopInfo[pIgsVlanEntry->u1AddressType - 1].u1ReportFwdAll
              == SNOOP_FORWARD_NONEDGE_PORTS)))
        {
            IPVX_ADDR_COPY (&DestIpAddr, &(pIgsConsGroupEntry->GroupIpAddr));

            if (SnoopGrpGetGroupEntry (u4Instance, VlanId, DestIpAddr,
                                       &pSnoopGroupEntry) == SNOOP_SUCCESS)
            {
                SNOOP_MEM_CPY (pTempPortBitMap, pSnoopGroupEntry->PortBitmap,
                               sizeof (tSnoopPortBmp));

                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         pIgsVlanEntry->
                                                         u1AddressType, 0,
                                                         pSnoopRtrPortBmp,
                                                         pIgsVlanEntry);

                for (u2BytIndex = 0; u2BytIndex < SNOOP_PORT_LIST_SIZE;
                     u2BytIndex++)
                {
                    u1PortFlag = pTempPortBitMap[u2BytIndex];
                    for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                          && (u1PortFlag != 0)); u2BitIndex++)
                    {
                        if ((u1PortFlag & SNOOP_BIT8) != 0)
                        {
                            u4LearntPort =
                                (UINT4) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);

                            SNOOP_IS_PORT_PRESENT (u4LearntPort,
                                                   pSnoopRtrPortBmp, bRes);
                            if (bRes == OSIX_TRUE)
                            {
                                u4RtrPortCount++;
                            }
                            else
                            {
                                u4LrtPortCount++;
                            }
                        }
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                    }
                }

                if ((u4LrtPortCount == 0) && (u4RtrPortCount <= 1))
                {
                    u1ReportFwd = SNOOP_TRUE;
                }
            }
        }

        if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
            (u1ReportFwd == SNOOP_TRUE))
        {
            /* If sparse mode is enabled, the report forwarding is done based on
             * learnt source port information */

            if ((pSnpTmpGroupEntry != NULL) &&
                (pSnpTmpGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE))
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC,
                                "Reports are not forwarded because of learnt source port information for Vlan %d\n",
                                SNOOP_OUTER_VLAN (pIgsVlanEntry->VlanId));
                u1ReportFwd = SNOOP_FALSE;
            }
        }

        if ((u1ReportFwd == SNOOP_TRUE) && (SNOOP_INSTANCE_INFO (u4Instance)->
                                            SnoopInfo[pIgsVlanEntry->
                                                      u1AddressType -
                                                      1].u1ReportFwdAll ==
                                            SNOOP_FORWARD_NONEDGE_PORTS))
        {
            SNOOP_MEM_SET (pVlanPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
            SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

            if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                pIgsVlanEntry->VlanId,
                                                pVlanPortBitmap) ==
                SNOOP_SUCCESS)
            {
                SnoopUtilGetNonEdgePortList (u4Instance, pVlanPortBitmap,
                                             pFwdPortBitmap);
                if (SNOOP_MEM_CMP
                    (pFwdPortBitmap, gNullPortBitMap,
                     sizeof (tSnoopPortBmp)) == 0)
                {
                    u1ReportFwd = SNOOP_FALSE;
                }
                if (u1ReportFwd == SNOOP_TRUE)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_VLAN_TRC,
                                    "Reports are forwarded to non-edge port list for Vlan %d\n",
                                    SNOOP_OUTER_VLAN (pIgsVlanEntry->VlanId));
                }
            }
        }
        else if ((SNOOP_INSTANCE_INFO (u4Instance)->
                  SnoopInfo[pIgsVlanEntry->u1AddressType - 1].u1ReportFwdAll
                  == SNOOP_FORWARD_RTR_PORTS))
        {
            SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                     SNOOP_OUTER_VLAN (VlanId),
                                                     pIgsVlanEntry->
                                                     u1AddressType,
                                                     SNOOP_IGS_IGMP_VERSION3,
                                                     pSnoopRtrPortBmp,
                                                     pIgsVlanEntry);
            if (SNOOP_MEM_CMP
                (pSnoopRtrPortBmp, gNullPortBitMap,
                 sizeof (tSnoopPortBmp)) != 0)
            {
                u1ReportFwd = SNOOP_TRUE;
            }
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_VLAN_TRC,
                            "Reports are forwarded on router port list for Vlan %d\n",
                            SNOOP_OUTER_VLAN (pIgsVlanEntry->VlanId));
        }

        if ((u1ReportFwd == SNOOP_TRUE)
            && (u1Version == SNOOP_IGS_IGMP_VERSION3))
        {
            if (IgsEncodeV3Report (u4Instance, pIgsConsGroupEntry,
                                   SNOOP_TO_INCLUDE,
                                   &pReportBuf) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC,
                           "Forming V3 report message failed\r\n");
                UtilPlstReleaseLocalPortList (pTempPortBitMap);
                UtilPlstReleaseLocalPortList (pPortBitmap);
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                return SNOOP_FAILURE;
            }

            if (SnoopVlanUpdateStats (u4Instance, pIgsVlanEntry,
                                      SNOOP_SSMREPORT_SENT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC,
                                "Unable to update IGS statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pIgsVlanEntry->VlanId));
            }
        }

        if (((SNOOP_INSTANCE_INFO (u4Instance)->
              SnoopInfo[pIgsVlanEntry->u1AddressType - 1].
              u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
             (u1Version == SNOOP_IGS_IGMP_VERSION2) &&
             (SNOOP_INSTANCE_INFO (u4Instance)->
              SnoopInfo[pIgsVlanEntry->u1AddressType - 1].
              u1QueryFwdAll == SNOOP_FORWARD_ALL_PORTS)) ||
            ((u1Version == SNOOP_IGS_IGMP_VERSION2) &&
             (SNOOP_INSTANCE_INFO (u4Instance)->
              SnoopInfo[pIgsVlanEntry->u1AddressType - 1].u1ReportFwdAll
              == SNOOP_FORWARD_NONEDGE_PORTS)))
        {
            IPVX_ADDR_COPY (&DestIpAddr, &(pIgsConsGroupEntry->GroupIpAddr));

            if (SnoopGrpGetGroupEntry (u4Instance, VlanId, DestIpAddr,
                                       &pSnoopGroupEntry) == SNOOP_SUCCESS)
            {
                SNOOP_MEM_CPY (pTempPortBitMap, pSnoopGroupEntry->PortBitmap,
                               sizeof (tSnoopPortBmp));

                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         pIgsVlanEntry->
                                                         u1AddressType, 0,
                                                         pSnoopRtrPortBmp,
                                                         pIgsVlanEntry);

                for (u2BytIndex = 0; u2BytIndex < SNOOP_PORT_LIST_SIZE;
                     u2BytIndex++)
                {
                    u1PortFlag = pTempPortBitMap[u2BytIndex];
                    for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                          && (u1PortFlag != 0)); u2BitIndex++)
                    {
                        if ((u1PortFlag & SNOOP_BIT8) != 0)
                        {
                            u4LearntPort =
                                (UINT4) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);

                            SNOOP_IS_PORT_PRESENT (u4LearntPort,
                                                   pSnoopRtrPortBmp, bRes);
                            if (bRes == OSIX_TRUE)
                            {
                                u4RtrPortCount++;
                            }
                            else
                            {
                                u4LrtPortCount++;
                            }
                        }
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                    }
                }

                if ((u4LrtPortCount == 0) && (u4RtrPortCount <= 1))
                {
                    u1LeaveFwd = SNOOP_TRUE;
                }
            }
        }

        if ((u1LeaveFwd == SNOOP_TRUE) && (SNOOP_INSTANCE_INFO (u4Instance)->
                                           SnoopInfo[pIgsVlanEntry->
                                                     u1AddressType -
                                                     1].u1ReportFwdAll ==
                                           SNOOP_FORWARD_NONEDGE_PORTS))
        {
            SNOOP_MEM_SET (pVlanPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
            SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

            if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                pIgsVlanEntry->VlanId,
                                                pVlanPortBitmap) ==
                SNOOP_SUCCESS)
            {
                SnoopUtilGetNonEdgePortList (u4Instance, pVlanPortBitmap,
                                             pFwdPortBitmap);
                if (SNOOP_MEM_CMP
                    (pFwdPortBitmap, gNullPortBitMap,
                     sizeof (tSnoopPortBmp)) == 0)
                {
                    u1LeaveFwd = SNOOP_FALSE;
                }
                if (u1LeaveFwd == SNOOP_TRUE)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_VLAN_TRC,
                                    "Leave messages are forwarded on non-edge port list for Vlan %d\n",
                                    SNOOP_OUTER_VLAN (pIgsVlanEntry->VlanId));
                }
            }
        }
        else if ((SNOOP_INSTANCE_INFO (u4Instance)->
                  SnoopInfo[pIgsVlanEntry->u1AddressType - 1].u1ReportFwdAll
                  == SNOOP_FORWARD_RTR_PORTS))
        {
            SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                     SNOOP_OUTER_VLAN (VlanId),
                                                     pIgsVlanEntry->
                                                     u1AddressType,
                                                     SNOOP_IGS_IGMP_VERSION2,
                                                     pSnoopRtrPortBmp,
                                                     pIgsVlanEntry);
            if (SNOOP_MEM_CMP
                (pSnoopRtrPortBmp, gNullPortBitMap,
                 sizeof (tSnoopPortBmp)) != 0)
            {
                u1LeaveFwd = SNOOP_TRUE;
            }
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_VLAN_TRC,
                            "Leaves are forwarded on router port list for Vlan %d\n",
                            SNOOP_OUTER_VLAN (pIgsVlanEntry->VlanId));
        }

        if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
            (u1LeaveFwd == SNOOP_TRUE))
        {
            /* If sparse mode is enabled, the leave forwarding is done based on
             * learnt source port information */

            if ((pSnpTmpGroupEntry != NULL) &&
                (pSnpTmpGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE))
            {
                u1LeaveFwd = SNOOP_FALSE;
            }
        }

        if (u1LeaveFwd == SNOOP_TRUE)
        {
            if (IgsEncodeLeaveMsg
                (u4Instance,
                 SNOOP_PTR_FETCH_4 (pIgsConsGroupEntry->GroupIpAddr.au1Addr),
                 &pLeaveMsgBuf) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC,
                           "Forming V2 leave message - failed\r\n");
                UtilPlstReleaseLocalPortList (pTempPortBitMap);
                UtilPlstReleaseLocalPortList (pPortBitmap);
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                CRU_BUF_Release_MsgBufChain (pReportBuf, FALSE);
                return SNOOP_FAILURE;
            }

            if (SnoopVlanUpdateStats (u4Instance, pIgsVlanEntry,
                                      SNOOP_LEAVE_SENT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC,
                                "Unable to update IGS statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pIgsVlanEntry->VlanId));
            }
        }
        /* forward the contructed packet on to the V3 router ports */
        if (pReportBuf != NULL)
        {
            if ((SNOOP_INSTANCE_INFO (u4Instance)->
                 SnoopInfo[pIgsVlanEntry->u1AddressType - 1].
                 u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
                (SNOOP_INSTANCE_INFO (u4Instance)->
                 SnoopInfo[pIgsVlanEntry->u1AddressType - 1].
                 u1QueryFwdAll == SNOOP_FORWARD_ALL_PORTS))
            {
                if (u4LearntPort != 0)
                {
                    SNOOP_ADD_TO_PORT_LIST (u4LearntPort, pPortBitmap);
                }
                else
                {
                    if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                        pIgsVlanEntry->VlanId,
                                                        pVlanPortBitmap) ==
                        SNOOP_SUCCESS)
                    {
                        SNOOP_MEM_CPY (pPortBitmap, pVlanPortBitmap,
                                       sizeof (tSnoopPortBmp));
                    }
                }

                SnoopVlanForwardPacket (u4Instance, pIgsVlanEntry->VlanId,
                                        SNOOP_ADDR_TYPE_IPV4,
                                        SNOOP_INVALID_PORT, u1Forward,
                                        pReportBuf, pPortBitmap, 0);
            }
            else
            {
                if (SNOOP_INSTANCE_INFO (u4Instance)->
                    SnoopInfo[pIgsVlanEntry->u1AddressType - 1].u1ReportFwdAll
                    == SNOOP_FORWARD_RTR_PORTS)
                {
                    SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pIgsVlanEntry->
                                                             u1AddressType,
                                                             SNOOP_IGS_IGMP_VERSION3,
                                                             pSnoopRtrPortBmp,
                                                             pIgsVlanEntry);
                    SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                                   sizeof (tSnoopPortBmp));
                }
                SnoopVlanForwardPacket (u4Instance, pIgsVlanEntry->VlanId,
                                        SNOOP_ADDR_TYPE_IPV4,
                                        SNOOP_INVALID_PORT, u1Forward,
                                        pReportBuf, pFwdPortBitmap, 0);
            }
        }
        /* forward the contructed packet on to the V2 router ports */
        if (pLeaveMsgBuf != NULL)
        {
            if ((SNOOP_INSTANCE_INFO (u4Instance)->
                 SnoopInfo[pIgsVlanEntry->u1AddressType - 1].
                 u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
                (SNOOP_INSTANCE_INFO (u4Instance)->
                 SnoopInfo[pIgsVlanEntry->u1AddressType - 1].
                 u1QueryFwdAll == SNOOP_FORWARD_ALL_PORTS))
            {
                if (u4LearntPort != 0)
                {
                    SNOOP_ADD_TO_PORT_LIST (u4LearntPort, pPortBitmap);
                }
                else
                {
                    if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                        pIgsVlanEntry->VlanId,
                                                        pVlanPortBitmap) ==
                        SNOOP_SUCCESS)
                    {
                        SNOOP_MEM_CPY (pPortBitmap, pVlanPortBitmap,
                                       sizeof (tSnoopPortBmp));
                    }
                }

                SnoopVlanForwardPacket (u4Instance, pIgsVlanEntry->VlanId,
                                        SNOOP_ADDR_TYPE_IPV4,
                                        SNOOP_INVALID_PORT, u1Forward,
                                        pLeaveMsgBuf, pPortBitmap, 0);
            }
            else
            {
                if (SNOOP_INSTANCE_INFO (u4Instance)->
                    SnoopInfo[pIgsVlanEntry->u1AddressType - 1].u1ReportFwdAll
                    == SNOOP_FORWARD_RTR_PORTS)
                {
                    SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pIgsVlanEntry->
                                                             u1AddressType,
                                                             SNOOP_IGS_IGMP_VERSION2,
                                                             pSnoopRtrPortBmp,
                                                             pIgsVlanEntry);
                    SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                                   sizeof (tSnoopPortBmp));
                }
                SnoopVlanForwardPacket (u4Instance, pIgsVlanEntry->VlanId,
                                        SNOOP_ADDR_TYPE_IPV4,
                                        SNOOP_INVALID_PORT, u1Forward,
                                        pLeaveMsgBuf, pFwdPortBitmap, 0);
            }
        }
    }
    else
    {                            /* Proxy reporting is disabled so form a V2 leave and send
                                   on to the router ports */
        u1SnpLeaveFwd = SNOOP_TRUE;

        /* If sparse mode is enabled, the report/leave forwarding is done based on
         * learnt source port information */
        if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
        {
            if ((pSnpTmpGroupEntry != NULL) &&
                (pSnpTmpGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE))
            {
                u1SnpLeaveFwd = SNOOP_FALSE;
            }
        }
        if (u1SnpLeaveFwd == SNOOP_TRUE)
        {
            if (SNOOP_INSTANCE_INFO (u4Instance)->
                SnoopInfo[pIgsVlanEntry->u1AddressType - 1].u1ReportFwdAll
                == SNOOP_FORWARD_NONEDGE_PORTS)
            {
                SNOOP_MEM_SET (pVlanPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

                if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                    pIgsVlanEntry->VlanId,
                                                    pVlanPortBitmap) ==
                    SNOOP_SUCCESS)
                {
                    SnoopUtilGetNonEdgePortList (u4Instance, pVlanPortBitmap,
                                                 pFwdPortBitmap);
                    if (SNOOP_MEM_CMP
                        (pFwdPortBitmap, gNullPortBitMap,
                         sizeof (tSnoopPortBmp)) == 0)
                    {
                        u1SnpLeaveFwd = SNOOP_FALSE;
                    }
                    if (u1SnpLeaveFwd == SNOOP_TRUE)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_VLAN_TRC,
                                        "Leave messages are forwarded on non-edge port list for Vlan %d\n",
                                        SNOOP_OUTER_VLAN (pIgsVlanEntry->
                                                          VlanId));
                    }
                }
            }
        }

        if (u1SnpLeaveFwd == SNOOP_TRUE)
        {
            if (IgsEncodeLeaveMsg
                (u4Instance,
                 SNOOP_PTR_FETCH_4 (pIgsConsGroupEntry->GroupIpAddr.au1Addr),
                 &pLeaveMsgBuf) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC,
                           "Forming V2 leave message - failed\r\n");
                UtilPlstReleaseLocalPortList (pTempPortBitMap);
                UtilPlstReleaseLocalPortList (pPortBitmap);
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                return SNOOP_FAILURE;
            }

            if (SnoopVlanUpdateStats (u4Instance, pIgsVlanEntry,
                                      SNOOP_LEAVE_SENT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC,
                                "Unable to update IGS statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pIgsVlanEntry->VlanId));
            }
            /* forward the contructed packet on all the router ports */
            if (pLeaveMsgBuf != NULL)
            {
                if ((SNOOP_INSTANCE_INFO (u4Instance)->
                     SnoopInfo[pIgsVlanEntry->u1AddressType - 1].
                     u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
                    (SNOOP_INSTANCE_INFO (u4Instance)->
                     SnoopInfo[pIgsVlanEntry->u1AddressType - 1].
                     u1QueryFwdAll == SNOOP_FORWARD_ALL_PORTS))
                {
                    IPVX_ADDR_COPY (&DestIpAddr,
                                    &(pIgsConsGroupEntry->GroupIpAddr));

                    if (SnoopGrpGetGroupEntry (u4Instance, VlanId, DestIpAddr,
                                               &pSnoopGroupEntry) ==
                        SNOOP_SUCCESS)
                    {
                        SNOOP_MEM_CPY (pTempPortBitMap,
                                       pSnoopGroupEntry->PortBitmap,
                                       sizeof (tSnoopPortBmp));

                        SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                                 SNOOP_OUTER_VLAN
                                                                 (VlanId),
                                                                 pIgsVlanEntry->
                                                                 u1AddressType,
                                                                 0,
                                                                 pSnoopRtrPortBmp,
                                                                 pIgsVlanEntry);

                        for (u2BytIndex = 0; u2BytIndex < SNOOP_PORT_LIST_SIZE;
                             u2BytIndex++)
                        {
                            u1PortFlag = pTempPortBitMap[u2BytIndex];
                            for (u2BitIndex = 0;
                                 ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                if ((u1PortFlag & SNOOP_BIT8) != 0)
                                {
                                    u4LearntPort =
                                        (UINT4) ((u2BytIndex *
                                                  SNOOP_PORTS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    SNOOP_IS_PORT_PRESENT (u4LearntPort,
                                                           pSnoopRtrPortBmp,
                                                           bRes);
                                    if (bRes == OSIX_TRUE)
                                    {
                                        u4RtrPortCount++;
                                    }
                                    else
                                    {
                                        u4LrtPortCount++;
                                    }
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }

                        if ((u4LrtPortCount == 0) && (u4RtrPortCount <= 1))
                        {
                            u1LeaveFrwd = SNOOP_TRUE;
                        }
                    }

                    if (u4LearntPort != 0)
                    {
                        SNOOP_ADD_TO_PORT_LIST (u4LearntPort, pPortBitmap);
                    }
                    else
                    {
                        if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                            pIgsVlanEntry->
                                                            VlanId,
                                                            pVlanPortBitmap) ==
                            SNOOP_SUCCESS)
                        {
                            SNOOP_MEM_CPY (pPortBitmap, pVlanPortBitmap,
                                           sizeof (tSnoopPortBmp));
                        }
                    }
                    if (u1LeaveFrwd == SNOOP_TRUE)
                    {
                        SnoopVlanForwardPacket (u4Instance,
                                                pIgsVlanEntry->VlanId,
                                                SNOOP_ADDR_TYPE_IPV4,
                                                SNOOP_INVALID_PORT, u1Forward,
                                                pLeaveMsgBuf, pPortBitmap, 0);
                    }
                }
                else
                {
                    if (SNOOP_INSTANCE_INFO (u4Instance)->
                        SnoopInfo[pIgsVlanEntry->u1AddressType -
                                  1].u1ReportFwdAll == SNOOP_FORWARD_RTR_PORTS)
                    {
                        SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                        SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                                 SNOOP_OUTER_VLAN
                                                                 (VlanId),
                                                                 pIgsVlanEntry->
                                                                 u1AddressType,
                                                                 0,
                                                                 pSnoopRtrPortBmp,
                                                                 pIgsVlanEntry);
                        SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                                       sizeof (tSnoopPortBmp));
                    }
                    SnoopVlanForwardPacket (u4Instance, pIgsVlanEntry->VlanId,
                                            SNOOP_ADDR_TYPE_IPV4,
                                            SNOOP_INVALID_PORT, u1Forward,
                                            pLeaveMsgBuf, pFwdPortBitmap, 0);
                }
            }
        }
    }

    UtilPlstReleaseLocalPortList (pTempPortBitMap);
    UtilPlstReleaseLocalPortList (pPortBitmap);
    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
    KW_FALSEPOSITIVE_FIX (pLeaveMsgBuf);
    return SNOOP_SUCCESS;
}
