/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/* $Id: igsdec.c,v 1.11 2017/08/01 13:50:14 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : igsdec.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping IGS decode module                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains IGS decoding routines       */
/*                            for snooping (IGMP) module                     */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : IgsDecodeIncomingPacket                              */
/*                                                                           */
/* Description        : This function processes the incoming IGMP multicast  */
/*                      control packets and data packets.                    */
/*                                                                           */
/* Input(s)           : pBuf     - multicast packet                          */
/*                      u4Port   - Physical port on which the pacjet was     */
/*                                 recevied                                  */
/*                                                                           */
/*                      VlanInfo - Vlan info present in received packet      */
/*                      u2TagLen - Tag Length                                */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsDecodeIncomingPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Instance,
                         UINT2 u2LocalPort, tSnoopVlanInfo VlanInfo,
                         UINT2 u2TagLen)
{
    tSnoopPktInfo       IgsPktInfo;
    tSnoopPortBmp       PortBitmap;
    tSnoopTag           VlanId;
#ifndef IGS_OPTIMIZE
    INT4                i4RetStatus;
#endif
    UINT4               u4FrameSize = 0;

    SNOOP_MEM_SET (&PortBitmap, 0, SNOOP_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&IgsPktInfo, 0, sizeof (tSnoopPktInfo));

    /* Check whether Snooping module is started */
    if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_INIT_SHUT_TRC, SNOOP_GBL_NAME,
                       "Snooping is not started, Packet dropped\r\n");
        return SNOOP_FAILURE;
    }
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* Check whether IGS is globally enabled or disabled */
    if (SNOOP_SNOOPING_STATUS (u4Instance, SNOOP_INFO_IGS_ENTRY)
        == SNOOP_DISABLE)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_INIT_SHUT_TRC, SNOOP_GBL_NAME,
                       "IGS globally disabled, Packet dropped\r\n");
        return SNOOP_FAILURE;
    }

    IgsPktInfo.u4InPort = (UINT4) u2LocalPort;

    SNOOP_OUTER_VLAN (VlanId) = VlanInfo.OuterVlanTag.u2VlanId;

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        SNOOP_INNER_VLAN (VlanId) = VlanInfo.InnerVlanTag.u2VlanId;
    }
    else
    {
        SNOOP_INNER_VLAN (VlanId) = SNOOP_INVALID_VLAN_ID;
    }

    IgsPktInfo.u1TagLen = (UINT1) u2TagLen;
    /* 
     * Check whether verification of the packet for 
     * 1. Reserved IP address
     * 2. IP Checksum 
     * 3. Check for VALID packet type (IGMP/ROUTER control message) 
     */

    u4FrameSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
    SNOOP_PKT_DUMP (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                    pBuf, u4FrameSize,
                    "IgsDecodeIncomingPacket : Received Packet "
                    "on interface %d\r\n", u2LocalPort);

    if (IgsUtilValidatePacket (u4Instance, pBuf, &IgsPktInfo, VlanId)
        != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Packet validation failed:\r\n");
        return SNOOP_FAILURE;
    }

    /* Check whether any M-VLAN translation is needed. If TRUE, do the
     * M-VLAN translation. Or else, directly pass the packet for processing */
#ifndef IGS_OPTIMIZE
    if (IgsMVlanTranslationNeeded (pBuf, u4Instance, u2LocalPort, VlanInfo,
                                   &IgsPktInfo) == SNOOP_TRUE)
    {
        i4RetStatus =
            IgsMVlanLookupAndProcessPkt (pBuf, u4Instance, u2LocalPort, VlanId,
                                         &IgsPktInfo);
        return i4RetStatus;
    }
#endif
    if (SnoopProcessMulticastPkt (pBuf, u4Instance, u2LocalPort, VlanId,
                                  &IgsPktInfo) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Packet processing failed:\r\n");
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}
