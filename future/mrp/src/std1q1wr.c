/* $Id: std1q1wr.c,v 1.4 2011/10/13 10:14:23 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "std1q1wr.h"
# include  "std1q1db.h"
# include  "mrpinc.h"

INT4
GetNextIndexIeee8021QBridgeTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterSTD1Q1 ()
{
    SNMPRegisterMibWithLock (&std1q1OID, &std1q1Entry, MrpLock, MrpUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&std1q1OID, (const UINT1 *) "std1q1ap");
}

VOID
UnRegisterSTD1Q1 ()
{
    SNMPUnRegisterMib (&std1q1OID, &std1q1Entry);
    SNMPDelSysorEntry (&std1q1OID, (const UINT1 *) "std1q1ap");
}

INT4
Ieee8021QBridgeVlanVersionNumberGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanVersionNumber
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeMaxVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeMaxVlanId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeMaxSupportedVlansGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeMaxSupportedVlans
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgeNumVlansGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeNumVlans (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgeMvrpEnabledStatusGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeMvrpEnabledStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeMvrpEnabledStatusSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeMvrpEnabledStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeMvrpEnabledStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeMvrpEnabledStatus (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
Ieee8021QBridgeTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgeCVlanPortTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeCVlanPortTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeCVlanPortTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeCVlanPortRowStatusGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeCVlanPortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeCVlanPortRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeCVlanPortRowStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeCVlanPortRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeCVlanPortRowStatusTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeCVlanPortRowStatus (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
Ieee8021QBridgeCVlanPortTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeCVlanPortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgeFdbTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeFdbTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeFdbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeFdbDynamicCountGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeFdbDynamicCount
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgeFdbLearnedEntryDiscardsGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeFdbLearnedEntryDiscards
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021QBridgeFdbAgingTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeFdbAgingTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeFdbAgingTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeFdbAgingTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeFdbAgingTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeFdbAgingTime (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeFdbTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeFdbTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgeTpFdbTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeTpFdbTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeTpFdbTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeTpFdbPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpFdbPort
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgeTpFdbStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpFdbStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexIeee8021QBridgeTpGroupTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeTpGroupTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeTpGroupTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeTpGroupEgressPortsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpGroupEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeTpGroupLearntGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpGroupLearnt
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiData->pOctetStrValue));

}

INT4
GetNextIndexIeee8021QBridgeForwardAllTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeForwardAllTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeForwardAllTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeForwardAllPortsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardAllPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllStaticPortsGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardAllStaticPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllForbiddenPortsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardAllForbiddenPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllStaticPortsSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeForwardAllStaticPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllForbiddenPortsSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeForwardAllForbiddenPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllStaticPortsTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeForwardAllStaticPorts (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllForbiddenPortsTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeForwardAllForbiddenPorts (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardAllTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeForwardAllTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgeForwardUnregisteredTable (tSnmpIndex *
                                                     pFirstMultiIndex,
                                                     tSnmpIndex *
                                                     pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeForwardUnregisteredTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeForwardUnregisteredTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeForwardUnregisteredPortsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardUnregisteredPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredStaticPortsGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardUnregisteredStaticPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredForbiddenPortsGet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeForwardUnregisteredForbiddenPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredStaticPortsSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeForwardUnregisteredStaticPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredForbiddenPortsSet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeForwardUnregisteredForbiddenPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredStaticPortsTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeForwardUnregisteredStaticPorts (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredForbiddenPortsTest (UINT4 *pu4Error,
                                                      tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeForwardUnregisteredForbiddenPorts (pu4Error,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [0].
                                                                       u4_ULongValue,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [1].
                                                                       u4_ULongValue,
                                                                       pMultiData->
                                                                       pOctetStrValue));

}

INT4
Ieee8021QBridgeForwardUnregisteredTableDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeForwardUnregisteredTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgeStaticUnicastTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeStaticUnicastTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeStaticUnicastTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeStaticUnicastStaticEgressPortsGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticUnicastStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastForbiddenEgressPortsGet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticUnicastForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastStorageTypeGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticUnicastStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeStaticUnicastRowStatusGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticUnicastRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeStaticUnicastStaticEgressPortsSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticUnicastStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastForbiddenEgressPortsSet (tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticUnicastForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastStorageTypeSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticUnicastStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticUnicastRowStatusSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticUnicastRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticUnicastStaticEgressPortsTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticUnicastStaticEgressPorts (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    (*
                                                                     (tMacAddr
                                                                      *)
                                                                     pMultiIndex->
                                                                     pIndex[2].
                                                                     pOctetStrValue->
                                                                     pu1_OctetList),
                                                                    pMultiIndex->
                                                                    pIndex[3].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastForbiddenEgressPortsTest (UINT4 *pu4Error,
                                                      tSnmpIndex * pMultiIndex,
                                                      tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticUnicastForbiddenEgressPorts (pu4Error,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [0].
                                                                       u4_ULongValue,
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [1].
                                                                       u4_ULongValue,
                                                                       (*
                                                                        (tMacAddr
                                                                         *)
                                                                        pMultiIndex->
                                                                        pIndex
                                                                        [2].
                                                                        pOctetStrValue->
                                                                        pu1_OctetList),
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [3].
                                                                       u4_ULongValue,
                                                                       pMultiData->
                                                                       pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticUnicastStorageTypeTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticUnicastStorageType (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              (*(tMacAddr *)
                                                               pMultiIndex->
                                                               pIndex[2].
                                                               pOctetStrValue->
                                                               pu1_OctetList),
                                                              pMultiIndex->
                                                              pIndex[3].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticUnicastRowStatusTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticUnicastRowStatus (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            u4_ULongValue,
                                                            (*(tMacAddr *)
                                                             pMultiIndex->
                                                             pIndex[2].
                                                             pOctetStrValue->
                                                             pu1_OctetList),
                                                            pMultiIndex->
                                                            pIndex[3].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticUnicastTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeStaticUnicastTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgeStaticMulticastTable (tSnmpIndex * pFirstMultiIndex,
                                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeStaticMulticastTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeStaticMulticastTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList, pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeStaticMulticastStaticEgressPortsGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticMulticastStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastForbiddenEgressPortsGet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticMulticastForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastStorageTypeGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticMulticastStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeStaticMulticastRowStatusGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList),
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeStaticMulticastRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeStaticMulticastStaticEgressPortsSet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticMulticastStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastForbiddenEgressPortsSet (tSnmpIndex * pMultiIndex,
                                                       tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticMulticastForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastStorageTypeSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticMulticastStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticMulticastRowStatusSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeStaticMulticastRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticMulticastStaticEgressPortsTest (UINT4 *pu4Error,
                                                     tSnmpIndex * pMultiIndex,
                                                     tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticMulticastStaticEgressPorts (pu4Error,
                                                                      pMultiIndex->
                                                                      pIndex[0].
                                                                      u4_ULongValue,
                                                                      pMultiIndex->
                                                                      pIndex[1].
                                                                      u4_ULongValue,
                                                                      (*
                                                                       (tMacAddr
                                                                        *)
                                                                       pMultiIndex->
                                                                       pIndex
                                                                       [2].
                                                                       pOctetStrValue->
                                                                       pu1_OctetList),
                                                                      pMultiIndex->
                                                                      pIndex[3].
                                                                      u4_ULongValue,
                                                                      pMultiData->
                                                                      pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastForbiddenEgressPortsTest (UINT4 *pu4Error,
                                                        tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticMulticastForbiddenEgressPorts
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
              pu1_OctetList), pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeStaticMulticastStorageTypeTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticMulticastStorageType (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                u4_ULongValue,
                                                                (*(tMacAddr *)
                                                                 pMultiIndex->
                                                                 pIndex[2].
                                                                 pOctetStrValue->
                                                                 pu1_OctetList),
                                                                pMultiIndex->
                                                                pIndex[3].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticMulticastRowStatusTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeStaticMulticastRowStatus (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              (*(tMacAddr *)
                                                               pMultiIndex->
                                                               pIndex[2].
                                                               pOctetStrValue->
                                                               pu1_OctetList),
                                                              pMultiIndex->
                                                              pIndex[3].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
Ieee8021QBridgeStaticMulticastTableDep (UINT4 *pu4Error,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeStaticMulticastTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
Ieee8021QBridgeVlanNumDeletesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIeee8021QBridgeVlanNumDeletes
            (&(pMultiData->u8_Counter64Value)));
}

INT4
GetNextIndexIeee8021QBridgeVlanCurrentTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeVlanCurrentTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeVlanCurrentTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeVlanFdbIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanFdbId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgeVlanCurrentEgressPortsGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanCurrentEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanCurrentUntaggedPortsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanCurrentUntaggedPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeVlanCreationTimeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanCreationTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexIeee8021QBridgeVlanStaticTable (tSnmpIndex * pFirstMultiIndex,
                                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeVlanStaticTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeVlanStaticTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeVlanStaticNameGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanStaticName
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticEgressPortsGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanForbiddenEgressPortsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticUntaggedPortsGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanStaticUntaggedPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticRowStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeVlanStaticRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeVlanStaticNameSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeVlanStaticName
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticEgressPortsSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeVlanStaticEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanForbiddenEgressPortsSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeVlanForbiddenEgressPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticUntaggedPortsSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeVlanStaticUntaggedPorts
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticRowStatusSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeVlanStaticRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeVlanStaticNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeVlanStaticName (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticEgressPortsTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeVlanStaticEgressPorts (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanForbiddenEgressPortsTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeVlanForbiddenEgressPorts (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticUntaggedPortsTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeVlanStaticUntaggedPorts (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiIndex->
                                                             pIndex[1].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             pOctetStrValue));

}

INT4
Ieee8021QBridgeVlanStaticRowStatusTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeVlanStaticRowStatus (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
Ieee8021QBridgeVlanStaticTableDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeVlanStaticTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgeNextFreeLocalVlanTable (tSnmpIndex *
                                                   pFirstMultiIndex,
                                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeNextFreeLocalVlanTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeNextFreeLocalVlanTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeNextFreeLocalVlanIndexGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeNextFreeLocalVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeNextFreeLocalVlanIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexIeee8021QBridgePortVlanTable (tSnmpIndex * pFirstMultiIndex,
                                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgePortVlanTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgePortVlanTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgePvidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePvid (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021QBridgePortAcceptableFrameTypesGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePortAcceptableFrameTypes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgePortIngressFilteringGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePortIngressFiltering
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgePortMvrpEnabledStatusGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePortMvrpEnabledStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgePortMvrpFailedRegistrationsGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePortMvrpFailedRegistrations
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021QBridgePortMvrpLastPduOriginGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIeee8021QBridgePortMvrpLastPduOrigin
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

}

INT4
Ieee8021QBridgePortRestrictedVlanRegistrationGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgePortRestrictedVlanRegistration
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgePvidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgePvid (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
Ieee8021QBridgePortAcceptableFrameTypesSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgePortAcceptableFrameTypes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgePortIngressFilteringSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgePortIngressFiltering
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgePortMvrpEnabledStatusSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgePortMvrpEnabledStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgePortRestrictedVlanRegistrationSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgePortRestrictedVlanRegistration
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgePvidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgePvid (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
Ieee8021QBridgePortAcceptableFrameTypesTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgePortAcceptableFrameTypes (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiIndex->
                                                              pIndex[1].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue));

}

INT4
Ieee8021QBridgePortIngressFilteringTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgePortIngressFiltering (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
Ieee8021QBridgePortMvrpEnabledStatusTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgePortMvrpEnabledStatus (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Ieee8021QBridgePortRestrictedVlanRegistrationTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgePortRestrictedVlanRegistration (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    i4_SLongValue));

}

INT4
Ieee8021QBridgePortVlanTableDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgePortVlanTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgePortVlanStatisticsTable (tSnmpIndex *
                                                    pFirstMultiIndex,
                                                    tSnmpIndex *
                                                    pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgePortVlanStatisticsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgePortVlanStatisticsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeTpVlanPortInFramesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpVlanPortInFrames
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021QBridgeTpVlanPortOutFramesGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpVlanPortOutFrames
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
Ieee8021QBridgeTpVlanPortInDiscardsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeTpVlanPortInDiscards
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
GetNextIndexIeee8021QBridgeLearningConstraintsTable (tSnmpIndex *
                                                     pFirstMultiIndex,
                                                     tSnmpIndex *
                                                     pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeLearningConstraintsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeLearningConstraintsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeLearningConstraintsTypeGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeLearningConstraintsType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeLearningConstraintsStatusGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeLearningConstraintsStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeLearningConstraintsTypeSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeLearningConstraintsType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintsStatusSet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeLearningConstraintsStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintsTypeTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeLearningConstraintsType (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiIndex->
                                                             pIndex[1].
                                                             u4_ULongValue,
                                                             pMultiIndex->
                                                             pIndex[2].
                                                             i4_SLongValue,
                                                             pMultiData->
                                                             i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintsStatusTest (UINT4 *pu4Error,
                                              tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeLearningConstraintsStatus (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               u4_ULongValue,
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               u4_ULongValue,
                                                               pMultiIndex->
                                                               pIndex[2].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintsTableDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeLearningConstraintsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable (tSnmpIndex *
                                                            pFirstMultiIndex,
                                                            tSnmpIndex *
                                                            pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeLearningConstraintDefaultsTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeLearningConstraintDefaultsSetGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeLearningConstraintDefaultsSet
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsTypeGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeLearningConstraintDefaultsType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsSetSet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeLearningConstraintDefaultsSet
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsTypeSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeLearningConstraintDefaultsType
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsSetTest (UINT4 *pu4Error,
                                                  tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsSet (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsTypeTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsType (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    i4_SLongValue));

}

INT4
Ieee8021QBridgeLearningConstraintDefaultsTableDep (UINT4 *pu4Error,
                                                   tSnmpIndexList *
                                                   pSnmpIndexList,
                                                   tSNMP_VAR_BIND *
                                                   pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeLearningConstraintDefaultsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgeProtocolGroupTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeProtocolGroupTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeProtocolGroupTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeProtocolGroupIdGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeProtocolGroupId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeProtocolGroupRowStatusGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeProtocolGroupRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeProtocolGroupIdSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeProtocolGroupId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolGroupRowStatusSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeProtocolGroupRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolGroupIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeProtocolGroupId (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     pOctetStrValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolGroupRowStatusTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeProtocolGroupRowStatus (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            i4_SLongValue,
                                                            pMultiIndex->
                                                            pIndex[2].
                                                            pOctetStrValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolGroupTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeProtocolGroupTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021QBridgeProtocolPortTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021QBridgeProtocolPortTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021QBridgeProtocolPortTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021QBridgeProtocolPortGroupVidGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeProtocolPortGroupVid
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeProtocolPortRowStatusGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021QBridgeProtocolPortRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021QBridgeProtocolPortGroupVidSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeProtocolPortGroupVid
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolPortRowStatusSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetIeee8021QBridgeProtocolPortRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolPortGroupVidTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeProtocolPortGroupVid (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[2].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolPortRowStatusTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021QBridgeProtocolPortRowStatus (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[2].
                                                           i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Ieee8021QBridgeProtocolPortTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021QBridgeProtocolPortTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
