/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpapp.c,v 1.10 2014/01/24 12:23:50 siva Exp $
 *
 * Description: This file contains the APP related common routines.
 ******************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppEnrolApplFunctions
 *
 *    DESCRIPTION      : This function Enrols the application (MVRP/MMRP)
 *                       specific functions in MRP framework.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : VOID
 *
 ****************************************************************************/
VOID
MrpAppEnrolApplFunctions (VOID)
{
    /* Enrolling MVRP specific functions */
    gMrpGlobalInfo.aMrpAppnFn[MRP_MVRP_APP_ID].pAttrIndFn = MrpMvrpAttrIndFn;
    gMrpGlobalInfo.aMrpAppnFn[MRP_MVRP_APP_ID].pAttrTypeValidateFn =
        MrpMvrpTypeValidateFn;
    gMrpGlobalInfo.aMrpAppnFn[MRP_MVRP_APP_ID].pAttrLenValidateFn =
        MrpMvrpLenValidateFn;
    gMrpGlobalInfo.aMrpAppnFn[MRP_MVRP_APP_ID].pAttrWildCardValidateFn =
        MrpMvrpWildCardAttrRegValidateFn;

    /* Enrolling MMRP specific functions */
    gMrpGlobalInfo.aMrpAppnFn[MRP_MMRP_APP_ID].pAttrIndFn = MrpMmrpAttrIndFn;
    gMrpGlobalInfo.aMrpAppnFn[MRP_MMRP_APP_ID].pAttrTypeValidateFn =
        MrpMmrpTypeValidateFn;
    gMrpGlobalInfo.aMrpAppnFn[MRP_MMRP_APP_ID].pAttrLenValidateFn =
        MrpMmrpLenValidateFn;
    gMrpGlobalInfo.aMrpAppnFn[MRP_MMRP_APP_ID].pAttrWildCardValidateFn =
        MrpMmrpWildCardAttrRegValidateFn;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppModuleEnable
 *
 *    DESCRIPTION      : This function enables MVRP/MMRP applications.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info
 *                                         structure.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
MrpAppModuleEnable (tMrpContextInfo * pMrpContextInfo)
{
    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_TRUE)
    {
        if (gMrpGlobalInfo.au1MvrpStatus[pMrpContextInfo->u4ContextId]
            == MRP_ENABLED)
        {
            if (MrpMvrpEnable (pMrpContextInfo) == OSIX_FAILURE)
            {
                MRP_TRC ((pMrpContextInfo,
                          (INIT_SHUT_TRC | ALL_FAILURE_TRC | MRP_PROTOCOL_TRC),
                          "MrpAppModuleEnable: Enabling MVRP " "Failed \r\n"));
            }
        }

        if (gMrpGlobalInfo.au1MmrpStatus[pMrpContextInfo->u4ContextId]
            == MRP_ENABLED)
        {
            if (MrpMmrpEnable (pMrpContextInfo) == OSIX_FAILURE)
            {
                MRP_TRC ((pMrpContextInfo,
                          (INIT_SHUT_TRC | ALL_FAILURE_TRC | MRP_PROTOCOL_TRC),
                          "MrpAppModuleEnable: Enabling MMRP " "Failed \r\n"));
            }
        }
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppInitMrpAppEntry
 *
 *    DESCRIPTION      : This function intializes the APP entry.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context structure
 *                       pMrpAppEntry    - Pointer to the Appliction structure
 *                       u1AppId         - Application ID
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpAppInitMrpAppEntry (tMrpContextInfo * pMrpContextInfo,
                       tMrpAppEntry * pMrpAppEntry, UINT1 u1AppId)
{
    /* Filling the Back Pointer to the context structure */
    pMrpAppEntry->pMrpContextInfo = pMrpContextInfo;
    pMrpAppEntry->u1AppId = u1AppId;

    if (pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        pMrpAppEntry->ppMapTable =
            MemAllocMemBlk (gMrpGlobalInfo.MrpMvrpMapPoolId);

        if (pMrpAppEntry->ppMapTable == NULL)
        {
            MRP_TRC ((pMrpAppEntry->pMrpContextInfo, MRP_CRITICAL_TRC,
                      "MrpAppInitMrpAppEntry: Memory allocation for Appl. "
                      "MVRP map entry Failed. \r\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pMrpAppEntry->ppMapTable, 0,
                ((sizeof (tMrpMapEntry *)) * (AST_MAX_MST_INSTANCES)));

        pMrpAppEntry->u2MapArraySize = AST_MAX_MST_INSTANCES;
    }
    else if (pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        pMrpAppEntry->ppMapTable =
            MemAllocMemBlk (gMrpGlobalInfo.MrpMmrpMapPoolId);

        if (pMrpAppEntry->ppMapTable == NULL)
        {
            MRP_TRC ((pMrpAppEntry->pMrpContextInfo, MRP_CRITICAL_TRC,
                      "MrpAppInitMrpAppEntry: Memory allocation for Appl. "
                      "MMRP map entry Failed. \r\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pMrpAppEntry->ppMapTable, 0,
                ((sizeof (tMrpMapEntry *)) * (VLAN_DEV_MAX_VLAN_ID + 1)));
        pMrpAppEntry->u2MapArraySize = (VLAN_DEV_MAX_VLAN_ID + 1);
    }

    pMrpAppEntry->MapAttrTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMrpAttrEntry, MapAttrEntryNode),
                              (tRBCompareFn) MrpAttrMapAttrTblCmpFn);

    if (pMrpAppEntry->MapAttrTable == NULL)
    {
        MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                  "MrpMapDSInitMrpMapEntry: Attribute table RB Tree creation "
                  "Failed. \r\n"));
        return OSIX_FAILURE;
    }
    /* No Semaphore creation is needed inside this RBTree. */
    RBTreeDisableMutualExclusion (pMrpAppEntry->MapAttrTable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppDeInitMrpAppEntry
 *
 *    DESCRIPTION      : This function de-intializes the APP entry.
 *
 *    INPUT            : pMrpAppEntry    - Pointer to the Appliction structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
MrpAppDeInitMrpAppEntry (tMrpAppEntry * pMrpAppEntry)
{
    if (pMrpAppEntry->ppMapTable != NULL)
    {
        if (pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
        {
            MemReleaseMemBlock (gMrpGlobalInfo.MrpMvrpMapPoolId,
                                (UINT1 *) pMrpAppEntry->ppMapTable);
        }
        else if (pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
        {
            MemReleaseMemBlock (gMrpGlobalInfo.MrpMmrpMapPoolId,
                                (UINT1 *) pMrpAppEntry->ppMapTable);
        }
        pMrpAppEntry->ppMapTable = NULL;
    }
    if (pMrpAppEntry->MapAttrTable != NULL)
    {
        RBTreeDestroy (pMrpAppEntry->MapAttrTable,
                       (tRBKeyFreeFn) MrpAttrFreeMapAttrEntry, 0);
        pMrpAppEntry->MapAttrTable = NULL;
    }
    pMrpAppEntry->u2MapArraySize = MRP_INIT_VAL;
    /* Resetting the Back Pointer of the context structure */
    pMrpAppEntry->pMrpContextInfo = NULL;
    pMrpAppEntry->u1AppId = MRP_INIT_VAL;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppCreateMrpAppPortEntry
 *
 *    DESCRIPTION      : This function creates and intializes the Application
 *                       Port entry. And also this function will take care of
 *                       adding this to the port array present in
 *                       tMrpAppEntry structure.
 *
 *    INPUT            : pMrpAppEntry - Pointer to the Application entry
 *                       u2Port       - Local Port Identifier
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Pointer to the App Port Entry
 *
 ****************************************************************************/
tMrpAppPortEntry   *
MrpAppCreateMrpAppPortEntry (tMrpAppEntry * pMrpAppEntry, UINT2 u2Port)
{
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;

    if (MRP_IS_PORT_VALID (u2Port) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC,
                     "MrpAppCreateMrpAppPortEntry: "
                     "Invalid Port.Port NOT created.\n");
        return NULL;
    }

    if (MRP_GET_APP_PORT_ENTRY (pMrpAppEntry, u2Port) == NULL)
    {
        pMrpAppPortEntry =
            (tMrpAppPortEntry *) MemAllocMemBlk (gMrpGlobalInfo.AppPortPoolId);

        if (pMrpAppPortEntry == NULL)
        {
            MRP_TRC ((pMrpAppEntry->pMrpContextInfo, MRP_CRITICAL_TRC,
                      "MrpAppCreateMrpAppPortEntry: Memory allocation for Appl. "
                      "port entry Failed. \r\n"));
            return NULL;
        }

        MEMSET (pMrpAppPortEntry, 0, sizeof (tMrpAppPortEntry));

        if (MrpAppInitMrpAppPortEntry (pMrpAppEntry, pMrpAppPortEntry, u2Port)
            == OSIX_FAILURE)
        {
            MemReleaseMemBlock (gMrpGlobalInfo.AppPortPoolId,
                                (UINT1 *) pMrpAppPortEntry);
            return NULL;
        }

        pMrpAppEntry->apAppPortEntry[u2Port] = pMrpAppPortEntry;
    }

    return pMrpAppPortEntry;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppAddMapEntryToAppEntry
 *
 *    DESCRIPTION      : This function adds the MAP entry to the Application
 *                       structure.
 *
 *    INPUT            : pMrpAppEntry - Pointer to the Application structure
 *                       pMrpMapEntry - Pointer to the MAP structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpAppAddMapEntryToAppEntry (tMrpAppEntry * pMrpAppEntry,
                             tMrpMapEntry * pMrpMapEntry)
{
    if (MRP_GET_MAP_ENTRY (pMrpAppEntry, pMrpMapEntry->u2MapId) == NULL)
    {
        pMrpAppEntry->ppMapTable[pMrpMapEntry->u2MapId] = pMrpMapEntry;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppDelAppPortEntryFrmAppEntry
 *
 *    DESCRIPTION      : This function deletes the Port entry from the
 *                       application structure.
 *
 *    INPUT            : pMrpAppEntry - Pointer to the Application structure
 *                       pMrpAppPortEntry - Pointer to the App Port structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpAppDelAppPortEntryFrmAppEntry (tMrpAppEntry * pMrpAppEntry,
                                  tMrpAppPortEntry * pMrpAppPortEntry)
{
    if (MRP_GET_APP_PORT_ENTRY (pMrpAppEntry,
                                pMrpAppPortEntry->pMrpPortEntry->
                                u2LocalPortId) != NULL)
    {
        pMrpAppEntry->apAppPortEntry
            [pMrpAppPortEntry->pMrpPortEntry->u2LocalPortId] = NULL;

    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppDelMapEntryFromAppEntry
 *
 *    DESCRIPTION      : This function deletes the MAP entry from the
 *                       Application structure.
 *
 *    INPUT            : pMrpAppEntry - Pointer to the Application structure
 *                       pMrpMapEntry - Pointer to the MAP structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpAppDelMapEntryFromAppEntry (tMrpAppEntry * pMrpAppEntry,
                               tMrpMapEntry * pMrpMapEntry)
{
    pMrpAppEntry->ppMapTable[pMrpMapEntry->u2MapId] = NULL;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppAddAndEnablePortsInAppl
 *
 *    DESCRIPTION      : This function is used to add and enable the port
 *                       in the specified application.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context structure
 *                       pMrpAppEntry    - Pointer to the Appliction structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpAppAddAndEnablePortsInAppl (tMrpContextInfo * pMrpContextInfo,
                               tMrpAppEntry * pMrpAppEntry)
{
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1PortAppStatus = 0;
    UINT2               u2Port = 0;

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

        if (pMrpPortEntry == NULL)
        {
            continue;
        }

        if (pMrpPortEntry->u1OperStatus == MRP_OPER_UP)
        {
            if (MrpAppAddPortToAppl (pMrpAppEntry, u2Port) == OSIX_FAILURE)
            {
                MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                          (INIT_SHUT_TRC | MRP_PROTOCOL_TRC),
                          "MrpAppAddAndEnablePortsInAppl: Adding port %d "
                          "to Application  table failed \n", u2Port));
                return OSIX_FAILURE;
            }

            if (pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
            {
                u1PortAppStatus = pMrpPortEntry->u1PortMvrpStatus;
            }
            else
            {
                u1PortAppStatus = pMrpPortEntry->u1PortMmrpStatus;
            }

            if (u1PortAppStatus == MRP_ENABLED)
            {
                if (MrpAppEnableApplOnPort (pMrpContextInfo, pMrpAppEntry,
                                            u2Port) == OSIX_FAILURE)
                {
                    MRP_TRC ((pMrpPortEntry->pMrpContextInfo,
                              (INIT_SHUT_TRC | MRP_PROTOCOL_TRC),
                              "MrpAppAddAndEnablePortsInAppl: Enabling "
                              "the application on port %d failed \n", u2Port));
                    return OSIX_FAILURE;
                }
            }
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppAddPortToAppl
 *
 *    DESCRIPTION      : This function is used to add the port in the
 *                       specified application.
 *
 *    INPUT            : pMrpAppEntry    - Pointer to the Appliction structure
 *                       u2Port          - Local-port Identifier
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpAppAddPortToAppl (tMrpAppEntry * pMrpAppEntry, UINT2 u2Port)
{
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;

    pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pMrpAppEntry, u2Port);

    if (pMrpAppPortEntry != NULL)
    {
        MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | MRP_PROTOCOL_TRC),
                  "MrpAppAddPortToAppl: Port %d already exists\n", u2Port));
        return OSIX_SUCCESS;
    }

    pMrpAppPortEntry = MrpAppCreateMrpAppPortEntry (pMrpAppEntry, u2Port);

    if (pMrpAppPortEntry == NULL)
    {
        MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | MRP_PROTOCOL_TRC),
                  "MrpAppAddPortToAppl: Adding port %d to Application "
                  "failed\n", u2Port));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppDelPortFromAppl
 *
 *    DESCRIPTION      : This function is used to deletes the port from the
 *                       specified application.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context structure
 *                       u1AppId         - Application Identifier
 *                       u2Port          - Local-port Identifier
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpAppDelPortFromAppl (tMrpContextInfo * pMrpContextInfo, UINT1 u1AppId,
                       UINT2 u2Port)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    UINT2               u2MapId = 0;

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return OSIX_FAILURE;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, u1AppId);

    pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pMrpAppEntry, u2Port);

    if (pMrpAppPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo,
                  (INIT_SHUT_TRC | MRP_PROTOCOL_TRC),
                  "MrpAppDelPortFromAppl: Port %d is not present\n", u2Port));
        return OSIX_SUCCESS;
    }

    for (u2MapId = 0; u2MapId < pMrpAppEntry->u2MapArraySize; u2MapId++)
    {
        pMrpMapEntry = pMrpAppEntry->ppMapTable[u2MapId];

        if (pMrpMapEntry == NULL)
        {
            continue;
        }

        pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, u2Port);

        if (pMrpMapPortEntry != NULL)
        {
            if (pMrpMapPortEntry->u1PortState == AST_PORT_STATE_FORWARDING)
            {
                /* Application Delete port can be treated as port being moved
                 * to Blocking state */
                MrpUtilPortStateChgedToBlocking (pMrpContextInfo, pMrpAppEntry,
                                                 pMrpMapEntry,
                                                 pMrpMapPortEntry);
            }
            MrpMapDSDelAllAttrInMapOnPort (pMrpMapEntry, pMrpMapPortEntry);
            MrpMapDSDeleteMrpMapPortEntry (pMrpMapPortEntry);
            MrpMapDsCheckAndDeleteMapEntry (pMrpMapEntry);
        }
    }

    MrpTmrStop (&(pMrpAppPortEntry->JoinTmrNode));

    MrpTmrStop (&(pMrpAppPortEntry->LeaveAllTmrNode));

    MrpAppDeInitMrpAppPortEntry (pMrpAppEntry, pMrpAppPortEntry);
    MrpAppDelAppPortEntryFrmAppEntry (pMrpAppEntry, pMrpAppPortEntry);

    MemReleaseMemBlock (gMrpGlobalInfo.AppPortPoolId,
                        (UINT1 *) pMrpAppPortEntry);

    pMrpAppPortEntry = NULL;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppEnableApplOnPort
 *
 *    DESCRIPTION      : This function is used to enable the specified
 *                       application in the port.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context structure
 *                       pMrpAppEntry    - Pointer to the Appliction structure
 *                       u2Port          - Local-port Identifier
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpAppEnableApplOnPort (tMrpContextInfo * pMrpContextInfo,
                        tMrpAppEntry * pMrpAppEntry, UINT2 u2Port)
{
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    UINT1              *pu1AttrPtr = NULL;
    UINT2               u2MapId = 0;
    UINT2               u2Index = 0;

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);
    pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pMrpAppEntry, u2Port);

    if ((pMrpPortEntry == NULL) || (pMrpAppPortEntry == NULL))
    {
        MRP_TRC ((pMrpContextInfo,
                  (INIT_SHUT_TRC | MRP_PROTOCOL_TRC),
                  "MrpAppEnableApplOnPort: Port entry %d not exists\n",
                  u2Port));
        return OSIX_FAILURE;
    }
    if (MrpTmrStartLeaveAllTmr (pMrpAppPortEntry) != OSIX_SUCCESS)
    {
        MRP_TRC ((pMrpContextInfo,
                  (INIT_SHUT_TRC | MRP_PROTOCOL_TRC),
                  "MrpAppEnableApplOnPort: Starting LeaveAll timer "
                  " for Port %d failed\n", u2Port));
        return OSIX_FAILURE;
    }

    /* Need to apply REQ_JOIN for all the attributes which are in REG_FIXED
     * state */

    for (u2MapId = 0; u2MapId < pMrpAppEntry->u2MapArraySize; u2MapId++)
    {
        pMrpMapEntry = MRP_GET_MAP_ENTRY (pMrpAppEntry, u2MapId);

        if (pMrpMapEntry == NULL)
        {
            continue;
        }

        pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, u2Port);

        if (pMrpMapPortEntry == NULL)
        {
            continue;
        }

        pu1AttrPtr = pMrpMapPortEntry->pu1AttrInfoList;

        for (u2Index = 1; u2Index <= pMrpMapPortEntry->u2AttrArraySize;
             u2Index++)
        {
            if (MRP_GET_ATTR_REG_ADMIN_CTRL (pu1AttrPtr[u2Index]) ==
                MRP_REG_FIXED)
            {
                /* Make Observers as Members. We propagate JOIN IN's on ports
                 * in which static registrations exist. */
                MrpSmApplicantSem (pMrpMapPortEntry, u2Index, MRP_APP_REQ_NEW);
            }
        }

    }

    /* Attributes registered on other ports must be propagated on this port. */
    MrpUtilSyncAttributeTable (pMrpAppEntry, pMrpAppPortEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppDisableApplOnPort
 *
 *    DESCRIPTION      : This function is used to disable the specified
 *                       application in the port.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context structure
 *                       pMrpAppEntry    - Pointer to the Appliction structure
 *                       u2Port          - Local-port Identifier
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpAppDisableApplOnPort (tMrpContextInfo * pMrpContextInfo,
                         tMrpAppEntry * pMrpAppEntry, UINT2 u2Port)
{
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    tMrpAttrEntry      *pMrpAttrEntry = NULL;
    UINT1              *pu1AttrPtr = NULL;
    tMrpIfMsg           IfMsg;
    UINT2               u2MapId = 0;
    UINT2               u2Index = 0;
    UINT1               u1AdmRegControl = (UINT1) MRP_REG_NORMAL;

    pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pMrpAppEntry, u2Port);

    if (pMrpAppPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo,
                  (INIT_SHUT_TRC | MRP_PROTOCOL_TRC),
                  "MrpAppDisableApplOnPort: Port entry %d not exists\n",
                  u2Port));
        return OSIX_FAILURE;
    }
    IfMsg.u4ContextId = pMrpContextInfo->u4ContextId;
    IfMsg.pAppEntry = pMrpAppEntry;
    IfMsg.u2Port = u2Port;

    MrpTmrStop (&(pMrpAppPortEntry->JoinTmrNode));

    MrpTmrStop (&(pMrpAppPortEntry->LeaveAllTmrNode));

    for (u2MapId = 0; u2MapId < pMrpAppEntry->u2MapArraySize; u2MapId++)
    {
        pMrpMapEntry = MRP_GET_MAP_ENTRY (pMrpAppEntry, u2MapId);

        if (pMrpMapEntry == NULL)
        {
            continue;
        }

        pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, u2Port);

        if (pMrpMapPortEntry == NULL)
        {
            continue;
        }

        IfMsg.u2MapId = u2MapId;

        pu1AttrPtr = pMrpMapPortEntry->pu1AttrInfoList;

        for (u2Index = 1; u2Index <= pMrpMapPortEntry->u2AttrArraySize;
             u2Index++)
        {
            pMrpAttrEntry = MRP_GET_ATTR_ENTRY (pMrpMapEntry, u2Index);
            if (pMrpAttrEntry == NULL)
            {
                continue;
            }
            /* Propagate LEAVE message for ATTRIBUTES which are dynamically
             * registered on this port */
            u1AdmRegControl = MRP_GET_ATTR_REG_ADMIN_CTRL (pu1AttrPtr[u2Index]);

            if (u1AdmRegControl == MRP_REG_NORMAL)
            {
                /* Either this attribute entry is dynamically registered on
                 * this port or we had propagated the registration of this
                 * attribute from some other port or this port might have
                 * received a leave for the attribute */
                if (MRP_GET_ATTR_REG_SEM_STATE (pu1AttrPtr[u2Index]) != MRP_MT)
                {
                    MrpMapMadAttributeLeaveInd (&(pMrpAttrEntry->Attr), &IfMsg);
                    MrpMapDSDelAttrFromMapPortEntry (pMrpMapPortEntry,
                                                     pMrpAttrEntry);
                    MrpMapDsCheckAndDeleteMapEntry (pMrpMapEntry);
                }
            }
            else if (u1AdmRegControl == MRP_REG_FIXED)
            {
                /* Make Partipants as observer */
                MrpSmApplicantSem (pMrpMapPortEntry, u2Index,
                                   MRP_APP_REQ_LEAVE);
            }
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppInitMrpAppPortEntry
 *
 *    DESCRIPTION      : This function is used initialize the Application
 *                       port entry structure.
 *
 *    INPUT            : pMrpAppEntry     - Pointer to the Appliction structure
 *                       pMrpAppPortEntry - Pointer to the App port structure
 *                       u2Port          - Local-port Identifier
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
INT4
MrpAppInitMrpAppPortEntry (tMrpAppEntry * pMrpAppEntry,
                           tMrpAppPortEntry * pMrpAppPortEntry, UINT2 u2Port)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpAppPortEntry->u1LeaveAllSemState = MRP_PASSIVE;

    pMrpContextInfo = pMrpAppEntry->pMrpContextInfo;

    pMrpAppPortEntry->pMrpAppEntry = pMrpAppEntry;
    pMrpAppPortEntry->pMrpPortEntry =
        MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

    if (pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        /* Mvrp Peer Mac Adrress List Memory */
        pMrpAppPortEntry->pPeerMacAddrList =
            MemAllocMemBlk (gMrpGlobalInfo.MvrpAppPortPeerMacPoolId);

        if (pMrpAppPortEntry->pPeerMacAddrList == NULL)
        {
            MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                      MRP_CRITICAL_TRC,
                      "MrpAppInitMrpAppPortEntry: Memory Allocate for"
                      " MvrpPeerMacAddrList Failed. \r\n"));
            return OSIX_FAILURE;
        }

        MEMSET (pMrpAppPortEntry->pPeerMacAddrList, 0,
                (sizeof (tMacAddr) * (VLAN_DEV_MAX_VLAN_ID + 1)));
    }
    else if (pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        /* Mmmrp Peer Mac Adrress List Memory */
        pMrpAppPortEntry->pPeerMacAddrList =
            MemAllocMemBlk (gMrpGlobalInfo.MmrpAppPortPeerMacPoolId);

        if (pMrpAppPortEntry->pPeerMacAddrList == NULL)
        {
            MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                      MRP_CRITICAL_TRC,
                      "MrpAppInitMrpAppPortEntry: Memory Allocate for"
                      " MmrpPeerMacAddrList Failed. \r\n"));
            return OSIX_FAILURE;
        }

        MEMSET (pMrpAppPortEntry->pPeerMacAddrList, 0,
                (sizeof (tMacAddr) * (MRP_MAX_MMRP_ATTR_IN_SYS + 1)));
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppDeInitMrpAppPortEntry
 *
 *    DESCRIPTION      : This function is used Deinitialize the Application
 *                       port entry structure.
 *
 *    INPUT            : pMrpAppEntry     - Pointer to the Appliction structure
 *                       pMrpAppPortEntry - Pointer to the App port structure
 *                       u2Port          - Local-port Identifier
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
INT4
MrpAppDeInitMrpAppPortEntry (tMrpAppEntry * pMrpAppEntry,
                             tMrpAppPortEntry * pMrpAppPortEntry)
{
    if (pMrpAppPortEntry->pPeerMacAddrList != NULL)
    {
        if (pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
        {
            MemReleaseMemBlock (gMrpGlobalInfo.MvrpAppPortPeerMacPoolId,
                                (UINT1 *) pMrpAppPortEntry->pPeerMacAddrList);
        }
        else if (pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
        {
            MemReleaseMemBlock (gMrpGlobalInfo.MmrpAppPortPeerMacPoolId,
                                (UINT1 *) pMrpAppPortEntry->pPeerMacAddrList);
        }
        pMrpAppPortEntry->pPeerMacAddrList = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpAppGetAppEntryFrmAppAddr
 *
 *    DESCRIPTION      : This function returns the Application Entry based on
 *                       the Application Address.
 *
 *    INPUT            : AppAddr        - Application Address
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : pAppEntry      - Pointer to the Application Entry
 *
 ****************************************************************************/
tMrpAppEntry       *
MrpAppGetAppEntryFrmAppAddr (UINT4 u4ContextId, tMacAddr AppAddr)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpAppEntry       *pAppEntry = NULL;

    pContextInfo = MRP_CONTEXT_PTR (u4ContextId);

    if (pContextInfo == NULL)
    {
        return NULL;
    }

    if ((MEMCMP (AppAddr, pContextInfo->MvrpAddr, ETHERNET_ADDR_SIZE) == 0))
    {
        pAppEntry =
            &(MRP_CONTEXT_PTR (u4ContextId)->aMrpAppTable[MRP_MVRP_APP_ID]);
    }
    else if (MEMCMP (AppAddr, gMmrpAddr, ETHERNET_ADDR_SIZE) == 0)
    {
        pAppEntry =
            &(MRP_CONTEXT_PTR (u4ContextId)->aMrpAppTable[MRP_MMRP_APP_ID]);
    }

    return pAppEntry;
}

/*****************************************************************************
 * Function Name      : MrpAppGetNextAppAddFrmCurAppAdd
 *
 * Description        : This function is used to get the Next Application
 *                      Address from Current Application Address.
 *
 * Input(s)           : u4ContextId    - Context Identifier
 *                      CurrAppAddr    - Current App Addr
 *                      pu1NextAppAddr - Next App Addr
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
INT4
MrpAppGetNextAppAddFrmCurAppAdd (UINT4 u4ContextId, tMacAddr CurrAppAddr,
                                 UINT1 *pu1NextAppAddr)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMacAddr            ZeroMacAddr;
    INT4                i4RetVal = 0;

    MEMSET (ZeroMacAddr, 0, sizeof (tMacAddr));

    MEMSET (pu1NextAppAddr, 0, MRP_MAC_ADDR_LEN);

    pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

    if (MRP_IS_802_1AD_1AH_BRIDGE (pMrpContextInfo) == OSIX_TRUE)
    {
        if (MEMCMP (CurrAppAddr, ZeroMacAddr, MRP_MAC_ADDR_LEN) == 0)
        {
            MEMCPY (pu1NextAppAddr, pMrpContextInfo->MvrpAddr,
                    MRP_MAC_ADDR_LEN);
        }
        else
        {
            i4RetVal = MEMCMP (CurrAppAddr, pMrpContextInfo->MvrpAddr,
                               MRP_MAC_ADDR_LEN);
            if ((i4RetVal == 0) || ((i4RetVal > 0)
                                    && (MEMCMP (CurrAppAddr, gMmrpAddr,
                                                MRP_MAC_ADDR_LEN) < 0)))
            {
                MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
            }
            else if (i4RetVal < 0)
            {
                MEMCPY (pu1NextAppAddr, pMrpContextInfo->MvrpAddr,
                        MRP_MAC_ADDR_LEN);
            }
            else
            {
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        if (MEMCMP (CurrAppAddr, ZeroMacAddr, MRP_MAC_ADDR_LEN) == 0)
        {
            MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
        }
        else
        {
            i4RetVal = MEMCMP (CurrAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);

            if (i4RetVal == 0)
            {
                MEMCPY (pu1NextAppAddr, pMrpContextInfo->MvrpAddr,
                        MRP_MAC_ADDR_LEN);
            }
            else if (i4RetVal < 0)
            {
                MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
            }
            else
            {
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : MrpAppGetNextAppAddrAndAttrType
 *
 *
 * Description        : This function is used to get the Application Addr and
 *                      the  Corresponding Attr Type from the Given App Addr
 *                      and  Attr Type.
 *
 * Input(s)           : u4ContextId    - Context Identifier
 *                      CurrAppAddr    - Current App Address
 *                      pu1NextAppAddr - Next App Address
 *                      i4CurrAttrType - Current Attr Type
 *                      pi4NextAttrType - Next Attr Type
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE
 *****************************************************************************/
INT4
MrpAppGetNextAppAddrAndAttrType (UINT4 u4ContextId,
                                 tMacAddr CurrAppAddr,
                                 UINT1 *pu1NextAppAddr,
                                 INT4 i4CurrAttrType, INT4 *pi4NextAttrType)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMacAddr            ZeroMacAddr;
    INT4                i4RetVal = 0;

    MEMSET (ZeroMacAddr, 0, sizeof (tMacAddr));

    MEMSET (pu1NextAppAddr, 0, MRP_MAC_ADDR_LEN);

    pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

    if (MRP_IS_802_1AD_BRIDGE (pMrpContextInfo) == OSIX_TRUE)
    {
        if (MEMCMP (CurrAppAddr, ZeroMacAddr, MRP_MAC_ADDR_LEN) == 0)
        {
            MEMCPY (pu1NextAppAddr, pMrpContextInfo->MvrpAddr,
                    MRP_MAC_ADDR_LEN);
            *pi4NextAttrType = MRP_VID_ATTR_TYPE;
        }
        else
        {
            i4RetVal = MEMCMP (CurrAppAddr, pMrpContextInfo->MvrpAddr,
                               MRP_MAC_ADDR_LEN);
            if (i4RetVal == 0)
            {
                if (i4CurrAttrType == MRP_INIT_VAL)
                {
                    MEMCPY (pu1NextAppAddr, pMrpContextInfo->MvrpAddr,
                            MRP_MAC_ADDR_LEN);
                    *pi4NextAttrType = MRP_VID_ATTR_TYPE;
                }
                else
                {
                    MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);

                    *pi4NextAttrType = MRP_SERVICE_REQ_ATTR_TYPE;
                }
            }
            else if (i4RetVal < 0)
            {
                MEMCPY (pu1NextAppAddr, pMrpContextInfo->MvrpAddr,
                        MRP_MAC_ADDR_LEN);
                *pi4NextAttrType = MRP_VID_ATTR_TYPE;
            }
            else if ((i4RetVal > 0) && (MEMCMP (CurrAppAddr, gMmrpAddr,
                                                MRP_MAC_ADDR_LEN) < 0))
            {
                MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
                *pi4NextAttrType = MRP_SERVICE_REQ_ATTR_TYPE;
            }
            else if (MEMCMP (CurrAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN) == 0)
            {
                if (i4CurrAttrType == MRP_INIT_VAL)
                {
                    MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
                    *pi4NextAttrType = MRP_SERVICE_REQ_ATTR_TYPE;
                }
                else if (i4CurrAttrType == MRP_SERVICE_REQ_ATTR_TYPE)
                {
                    MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
                    *pi4NextAttrType = MRP_MAC_ADDR_ATTR_TYPE;
                }
                else
                {
                    return OSIX_FAILURE;
                }
            }
            else
            {
                return OSIX_FAILURE;
            }

        }
    }
    else
    {
        if (MEMCMP (CurrAppAddr, ZeroMacAddr, MRP_MAC_ADDR_LEN) == 0)
        {
            MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
            *pi4NextAttrType = MRP_SERVICE_REQ_ATTR_TYPE;
        }
        else
        {
            i4RetVal = MEMCMP (CurrAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);

            if (i4RetVal == 0)
            {
                if ((i4CurrAttrType == MRP_MAC_ADDR_ATTR_TYPE) ||
                    (i4CurrAttrType == MRP_INVALID_VALUE))
                {
                    MEMCPY (pu1NextAppAddr, pMrpContextInfo->MvrpAddr,
                            MRP_MAC_ADDR_LEN);
                    *pi4NextAttrType = MRP_VID_ATTR_TYPE;
                }
                else if (i4CurrAttrType == MRP_INIT_VAL)
                {
                    MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
                    *pi4NextAttrType = MRP_SERVICE_REQ_ATTR_TYPE;
                }
                else
                {
                    MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
                    *pi4NextAttrType = MRP_MAC_ADDR_ATTR_TYPE;
                }
            }
            else if (i4RetVal < 0)
            {
                MEMCPY (pu1NextAppAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
                *pi4NextAttrType = MRP_SERVICE_REQ_ATTR_TYPE;
            }
            else if (MEMCMP (CurrAppAddr, pMrpContextInfo->MvrpAddr,
                             MRP_MAC_ADDR_LEN) == 0)
            {
                if (i4CurrAttrType == MRP_INIT_VAL)
                {
                    MEMCPY (pu1NextAppAddr, pMrpContextInfo->MvrpAddr,
                            MRP_MAC_ADDR_LEN);
                    *pi4NextAttrType = MRP_VID_ATTR_TYPE;
                }
                else
                {
                    return OSIX_FAILURE;
                }
            }
            else
            {
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : MrpAppGetAppIdFromAppAddr
 *
 *
 * Description        : This function is used to get AppId for the Given
 *                      App Address.
 *
 * Input(s)           : u4ContextId    - Context Identifier
 *                      MacAddr        -  App Address
 *                      pu1AppId       - Application Id
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
MrpAppGetAppIdFromAppAddr (UINT4 u4ContextId, tMacAddr AppAddr, UINT1 *pu1AppId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

    if (MEMCMP (AppAddr, pMrpContextInfo->MvrpAddr, MRP_MAC_ADDR_LEN) == 0)
    {
        *pu1AppId = MRP_MVRP_APP_ID;
    }
    else
    {
        *pu1AppId = MRP_MMRP_APP_ID;
    }

    return;
}

/*****************************************************************************
 * Function Name      : MrpAppDeEnrolMrpApplication
 *
 *
 * Description        : The applications (MVRP or MMRP) deenrol  with Mrp
 *                      using this function. This function is invoked whenever
 *                      Mvrp and/or Mmrp protocols wanted to deregister from
 *                      the MRP.
 *
 * Input(s)           : pMrpContextInfo - Context Information
 *                      u1AppId         - Application Id
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *
 *****************************************************************************/
VOID
MrpAppDeEnrolMrpApplication (tMrpContextInfo * pMrpContextInfo, UINT1 u1AppId)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    UINT2               u2Port = 0;

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, u1AppId);

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pMrpAppEntry, u2Port);

        if (pMrpAppPortEntry == NULL)
        {
            continue;
        }

        MrpAppDelPortFromAppl (pMrpContextInfo, pMrpAppEntry->u1AppId, u2Port);
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpapp.c                       */
/*-----------------------------------------------------------------------*/
