/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: mrprdutl.c,v 1.6 2013/06/25 12:08:38 siva Exp $
 *
 * Description: This file contains MRP High Availability related
 *              routines.
 *********************************************************************/

#include "mrpinc.h"
/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlGetMsgLen 
 *
 *    DESCRIPTION      : This function returns the message length based on 
 *                       the message type
 *
 *    INPUT            : u1MsgType - Message type
 *                       pu2MsgLen - Pointer to message length
 *
 *    OUTPUT           : pu2MsgLen - Length of the synchronization message
 *                                   excluding the size of Message type field
 *                                   and Message Length field
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
MrpRdUtlGetMsgLen (UINT1 u1MsgType, UINT2 *pu2MsgLen)
{
    *pu2MsgLen = MRP_RED_MSG_HDR_SIZE;

    switch (u1MsgType)
    {
        case MRP_RED_PORT_OPER_STATUS_MSG:
            *pu2MsgLen = (UINT2) (*pu2MsgLen + MRP_RED_OPER_STATUS_CHG_MSG_LEN);
            break;

        case MRP_RED_VLAN_DEL_MSG:
            *pu2MsgLen = (UINT2) (*pu2MsgLen + MRP_RED_VLAN_DEL_MSG_LEN);
            break;

        case MRP_RED_MAC_DEL_MSG:
            *pu2MsgLen = (UINT2) (*pu2MsgLen + MRP_RED_MAC_DEL_MSG_LEN);
            break;

        case MRP_RED_SERVICE_REQ_ADD_MSG:
            *pu2MsgLen = (UINT2) (*pu2MsgLen + MRP_RED_SERVICE_REQ_ADD_MSG_LEN);
            break;

        case MRP_RED_SERVICE_REQ_DEL_MSG:
            *pu2MsgLen = (UINT2) (*pu2MsgLen + MRP_RED_SERVICE_REQ_DEL_MSG_LEN);
            break;

        case MRP_RED_LV_ALL_TMR_RESTART_MSG:
            *pu2MsgLen =
                (UINT2) (*pu2MsgLen + MRP_RED_LVALL_TMR_RESTART_MSG_LEN);
            break;

        default:
            MRP_GBL_TRC (MRP_RED_TRC, "MrpRdUtlGetMsgLen: Invalid message "
                         "type\n");
            break;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlFormAttrAddSyncUpMsg 
 *
 *    DESCRIPTION      : This function forms and sends the VLAN/MAC add
 *                       synchronization message.
 *
 *    INPUT            : u1MsgType - Message type
 *                       pMsgInfo  - Pointer to structure containing syncup msg
 *                                   information
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *     
 ****************************************************************************/
INT4
MrpRdUtlFormAttrAddSyncUpMsg (UINT1 u1MsgType, tMrpRedMsgInfo * pMsgInfo)
{
    tRmMsg             *pRmMsg = NULL;
    UINT4              *pu4Offset = NULL;
    UINT4               u4AttrCntOffset = 0;
    UINT1               u1PostMsgToRM = OSIX_FALSE;

    pu4Offset = &(gMrpGlobalInfo.MrpRedInfo.u4Offset);

    /* 
     * ------------------------------------------------------------------
     * | MsgHdr | Context ID | PortNum |  Num Of Attr | Attribute Value |
     * ------------------------------------------------------------------
     * Attribute value can be Vlan Id or MAC Address 
     * In case of MRP_RED_MAC_ADD_MSG, MAP ID needs to be filled
     * after the PortNum field.
     */

    if (OSIX_FALSE == gMrpGlobalInfo.MrpRedInfo.u1IsFirstAttrFilled)
    {
        /* Synchronization message has not been filled with any value. 
         * Hence need to provide space for filling the MRP_RED_MSG_HDR
         */
        *pu4Offset = MRP_RED_MSG_HDR_SIZE;
    }

    pRmMsg = gMrpGlobalInfo.MrpRedInfo.pRedPduSyncMsg;

    if (OSIX_FALSE == pMsgInfo->u1EndOfPdu)
    {
        if (NULL == pRmMsg)
        {
            if (MrpRedAllocMemForRmMsg
                (u1MsgType, (UINT2) MRP_RED_MAX_MSG_SIZE,
                 &(gMrpGlobalInfo.MrpRedInfo.pRedPduSyncMsg)) != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }

            pRmMsg = gMrpGlobalInfo.MrpRedInfo.pRedPduSyncMsg;
            MRP_RED_PUT_4_BYTES (pRmMsg, *pu4Offset, pMsgInfo->u4ContextId);
            MRP_RED_PUT_2_BYTES (pRmMsg, *pu4Offset, pMsgInfo->u2LocalPortId);
        }

        if (MRP_RED_VLAN_ADD_MSG == u1MsgType)
        {
            if (OSIX_FALSE == gMrpGlobalInfo.MrpRedInfo.u1IsFirstAttrFilled)
            {
                /* Number of Attributes field is filled once the entire syncup 
                 * message is formed.
                 */
                MRP_RED_PUT_2_BYTES (pRmMsg, *pu4Offset, 0);
                gMrpGlobalInfo.MrpRedInfo.u1IsFirstAttrFilled = OSIX_TRUE;
            }
            MRP_RED_PUT_2_BYTES (pRmMsg, *pu4Offset, pMsgInfo->VlanId);
        }
        else if (MRP_RED_MAC_ADD_MSG == u1MsgType)
        {
            if (OSIX_FALSE == gMrpGlobalInfo.MrpRedInfo.u1IsFirstAttrFilled)
            {
                /* Fill the MAP ID to which this MAC Address belongs */
                MRP_RED_PUT_2_BYTES (pRmMsg, *pu4Offset, pMsgInfo->VlanId);
                /* Number of Attributes field is filled once the entire syncup 
                 * message is formed.
                 */
                MRP_RED_PUT_2_BYTES (pRmMsg, *pu4Offset, 0);
                gMrpGlobalInfo.MrpRedInfo.u1IsFirstAttrFilled = OSIX_TRUE;
            }
            MRP_RED_PUT_N_BYTES (pRmMsg, pMsgInfo->au1MmrpAttrVal, *pu4Offset,
                                 MAC_ADDR_LEN);
        }

        gMrpGlobalInfo.MrpRedInfo.u2NumOfAttr++;

        if ((MRP_RED_MAX_MSG_SIZE - (*pu4Offset)) <
            MRP_RED_MIN_BUFF_SPACE_REQ (pMsgInfo->u1AppId))
        {
            u1PostMsgToRM = OSIX_TRUE;
        }
    }
    else
    {
        /* Entrie MRPDU has been processed. */

        if (NULL != gMrpGlobalInfo.MrpRedInfo.pRedPduSyncMsg)
        {
            u1PostMsgToRM = OSIX_TRUE;
        }
        else
        {
            *pu4Offset = 0;
        }
    }

    /* Post the message to RM Task */
    if (OSIX_TRUE == u1PostMsgToRM)
    {
        /* Add Message type and length */
        MrpRedPutMsgHdrInRmMsg (pRmMsg, u1MsgType, (UINT2) (*pu4Offset));

        /* Update the number of Attribute values field */
        u4AttrCntOffset = MRP_RED_GET_ATTR_VAL_CNT_OFFSET (u1MsgType);
        MRP_RED_PUT_2_BYTES (pRmMsg, u4AttrCntOffset,
                             gMrpGlobalInfo.MrpRedInfo.u2NumOfAttr);
        gMrpGlobalInfo.MrpRedInfo.u2NumOfAttr = 0;
        gMrpGlobalInfo.MrpRedInfo.u1IsFirstAttrFilled = OSIX_FALSE;
        /* Send update message to RM */
        MrpRedSendMsgToRm (u1MsgType, (UINT2) (*pu4Offset), pRmMsg);
        gMrpGlobalInfo.MrpRedInfo.pRedPduSyncMsg = NULL;
        *pu4Offset = 0;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlProcPortOperChgSyncUp
 *
 *    DESCRIPTION      : This function processes the Oper status synchronization
 *                       message received for a particular port
 *
 *    INPUT            : pRmMsg - Sync up message 
 *                       u2MsgLen - Message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
INT4
MrpRdUtlProcPortOperChgSyncUp (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    tMrpContextInfo    *pContextInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4Offset = 0;
    UINT2               u2PortId = 0;
    UINT1               u1OperStatus = 0;

    MRP_GBL_TRC (MRP_RED_TRC, "MrpRdUtlProcPortOperChgSyncUp: Received port "
                 "oper status change sync up message from RM\r\n");

    /* Message header is already read from RM message buffer.
     * So move the offset as MRP_RED_MSG_HDR_SIZE.
     */
    u4Offset = MRP_RED_MSG_HDR_SIZE;

    /* Validate message length */
    if (u2MsgLen != MRP_RED_OPER_STATUS_CHG_MSG_LEN)
    {
        MRP_GBL_TRC (MRP_RED_TRC, "MrpRdUtlProcPortOperChgSyncUp: Message with "
                     "invalid length\r\n");
        return OSIX_FAILURE;
    }

    MRP_RED_GET_4_BYTES (pRmMsg, u4Offset, u4ContextId);
    MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2PortId);
    MRP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1OperStatus);

    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (NULL == pContextInfo)
    {
        return OSIX_FAILURE;
    }

    if (MrpIfHandlePortOperInd (pContextInfo, u2PortId, u1OperStatus)
        != OSIX_SUCCESS)
    {
        MRP_TRC ((pContextInfo, MRP_RED_TRC, "MrpRdUtlProcPortOperChgSyncUp: "
                  "Processing oper status change failed for port:%d\r\n",
                  u2PortId));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlProcTmrRestartSyncUpMsg
 *
 *    DESCRIPTION      : This function processes the LeaveAll timer restart 
 *                       sync up message for the given application.
 *
 *    INPUT            : pRmMsg - Sync up message 
 *                       u2MsgLen - Message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRdUtlProcTmrRestartSyncUpMsg (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpIfMsg           IfMsg;
    UINT4               u4ContextId = 0;
    UINT4               u4Offset = 0;
    UINT2               u2PortId = 0;
    UINT1               u1AppId = 0;

    MRP_GBL_TRC (MRP_RED_TRC,
                 "MrpRdUtlProcTmrRestartSyncUpMsg: Received LeaveAll"
                 " timer restart sync up message from RM\r\n");

    /* Message header is already read from RM message buffer.
     * So set the offset with MRP_RED_MSG_HDR_SIZE.
     */
    u4Offset = MRP_RED_MSG_HDR_SIZE;

    /* validate message length */
    if (u2MsgLen != MRP_RED_LVALL_TMR_RESTART_MSG_LEN)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpRdUtlProcTmrRestartSyncUpMsg: Message with "
                     "invalid length\r\n");
        return OSIX_FAILURE;
    }

    MRP_RED_GET_4_BYTES (pRmMsg, u4Offset, u4ContextId);
    MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2PortId);
    MRP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1AppId);

    if ((0 == u1AppId) || (u1AppId > MRP_MAX_APPS))
    {
        return OSIX_FAILURE;
    }

    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (NULL == pContextInfo)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&IfMsg, 0, sizeof (tMrpIfMsg));
    IfMsg.u4ContextId = u4ContextId;
    IfMsg.u2Port = u2PortId;
    IfMsg.pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);

    /* LeaveAll Timer is restarted only on receiving MRPDU with LeaveAll event.
     * Hence calling the below function.
     */
    MrpRxHandleLvAllEvent (&IfMsg);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlProcSerReqAddSyncUpMsg 
 *
 *    DESCRIPTION      : This function processes the Attribute Add
 *                       sync up message for Service Requirement.
 *
 *    INPUT            : pRmMsg - Sync up message 
 *                       u2MsgLen - Message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
INT4
MrpRdUtlProcSerReqAddSyncUpMsg (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpAttr            AttrInfo;
    tMrpAppEntry       *pAppEntry = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2PortId = 0;
    UINT2               u2MapId = 0;

    MEMSET (&AttrInfo, 0, sizeof (tMrpAttr));

    /* Message header is already read from RM message buffer.
     * So move the offset as MRP_RED_MSG_HDR_SIZE.
     */
    u4Offset = MRP_RED_MSG_HDR_SIZE;

    /* Validate message length */
    if (u2MsgLen != MRP_RED_SERVICE_REQ_ADD_MSG_LEN)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpRdUtlProcSerReqAddSyncUpMsg: Message with "
                     "invalid length\r\n");
        return OSIX_FAILURE;
    }

    MRP_RED_GET_4_BYTES (pRmMsg, u4Offset, u4ContextId);

    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (NULL == pContextInfo)
    {
        return OSIX_FAILURE;
    }

    MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2PortId);
    MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2MapId);

    pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, MRP_MMRP_APP_ID);
    AttrInfo.u1AttrType = MRP_SERVICE_REQ_ATTR_TYPE;
    AttrInfo.u1AttrLen = MRP_SERVICE_REQ_LEN;
    MRP_RED_GET_1_BYTE (pRmMsg, u4Offset, AttrInfo.au1AttrVal[0]);

    MrpRdUtlApplyAttrRegistration (pAppEntry, u2MapId, u2PortId, &AttrInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlProcAttrAddSyncUpMsg 
 *
 *    DESCRIPTION      : This function processes the Attribute Add sync up 
 *                       message for VLAN and MAC Address. 
 *
 *    INPUT            : pRmMsg    - Sync up message 
 *                       u1MsgType - Message Type
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRdUtlProcAttrAddSyncUpMsg (tRmMsg * pRmMsg, UINT1 u1MsgType)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpAttr            Attr;
    tVlanId             VlanId = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4Offset = 0;
    UINT2               u2NumOfAttr = 0;
    UINT2               u2MapId = 0;
    UINT2               u2PortId = 0;
    UINT1               u1AppId = 0;

    MEMSET (&Attr, 0, sizeof (tMrpAttr));

    /* Message header is already read from RM message buffer.
     * So set the offset with MRP_RED_MSG_HDR_SIZE.
     */
    u4Offset = MRP_RED_MSG_HDR_SIZE;

    MRP_RED_GET_4_BYTES (pRmMsg, u4Offset, u4ContextId);

    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (NULL == pContextInfo)
    {
        return OSIX_FAILURE;
    }

    MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2PortId);

    if (MRP_RED_MAC_ADD_MSG == u1MsgType)
    {
        MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2MapId);
    }

    MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2NumOfAttr);

    while (u2NumOfAttr > 0)
    {
        if (MRP_RED_VLAN_ADD_MSG == u1MsgType)
        {
            MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, VlanId);
            /* Get the MAP ID to which this VLAN belongs */
            u2MapId = MrpPortL2IwfMiGetVlanInstMapping (u4ContextId, VlanId);

            Attr.u1AttrType = MRP_VID_ATTR_TYPE;
            Attr.u1AttrLen = MRP_VLAN_ID_LEN;
            VlanId = OSIX_HTONS (VlanId);
            MEMCPY (Attr.au1AttrVal, &VlanId, MRP_VLAN_ID_LEN);
            u1AppId = MRP_MVRP_APP_ID;
        }
        else if (MRP_RED_MAC_ADD_MSG == u1MsgType)
        {
            Attr.u1AttrType = MRP_MAC_ADDR_ATTR_TYPE;
            Attr.u1AttrLen = MAC_ADDR_LEN;
            MRP_RED_GET_N_BYTES (pRmMsg, Attr.au1AttrVal, u4Offset,
                                 MAC_ADDR_LEN);
            u1AppId = MRP_MMRP_APP_ID;
        }

        pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);
        MrpRdUtlApplyAttrRegistration (pAppEntry, u2MapId, u2PortId, &Attr);
        u2NumOfAttr--;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlProcAttributeSyncUpMsg 
 *
 *    DESCRIPTION      : This function processes the Attribute
 *                       sync up message for the given application.
 *
 *    INPUT            : pRmMsg - Sync up message 
 *                       u2MsgLen - Message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
INT4
MrpRdUtlProcAttributeSyncUpMsg (tRmMsg * pRmMsg, UINT1 u1MsgType,
                                UINT2 u2MsgLen)
{
    tMacAddr            MacAddr;
    tVlanId             VlanId = 0;
    tMrpAttr            AttrInfo;
    UINT4               u4ContextId = 0;
    UINT4               u4Offset = 0;
    UINT2               u2PortId = 0;
    UINT2               u2NumOfAttributes = 0;
    UINT2               u2ValidMsgLen = 0;

    MRP_GBL_TRC (MRP_RED_TRC, "MrpRdUtlProcAttributeSyncUpMsg: Received "
                 "Attribute delete sync up message from RM\r\n");

    MEMSET (MacAddr, 0, MAC_ADDR_LEN);

    /* Message header is already read from RM message buffer.
     * So set the offset with MRP_RED_MSG_HDR_SIZE.
     */
    u4Offset = MRP_RED_MSG_HDR_SIZE;

    MrpRdUtlGetMsgLen (u1MsgType, &u2ValidMsgLen);
    u2ValidMsgLen = (UINT2) (u2ValidMsgLen - u4Offset);

    if (u2MsgLen != u2ValidMsgLen)
    {
        MRP_GBL_TRC (MRP_RED_TRC, "MrpRdUtlProcAttributeSyncUpMsg: Message with"
                     " invalid length\r\n");
        return OSIX_FAILURE;
    }

    MRP_RED_GET_4_BYTES (pRmMsg, u4Offset, u4ContextId);
    MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2PortId);

    switch (u1MsgType)
    {
        case MRP_RED_VLAN_DEL_MSG:
            MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2NumOfAttributes);
            MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, VlanId);
            MrpRdUtlProcAttrDelSyncUpMsg (u4ContextId, u2PortId, VlanId, NULL,
                                          MRP_MVRP_APP_ID);

            break;

        case MRP_RED_MAC_DEL_MSG:
            MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, VlanId);
            MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2NumOfAttributes);
            MEMSET (&AttrInfo, 0, sizeof (tMrpAttr));
            AttrInfo.u1AttrType = MRP_MAC_ADDR_ATTR_TYPE;
            AttrInfo.u1AttrLen = MRP_MAC_ADDR_LEN;
            MRP_RED_GET_N_BYTES (pRmMsg, AttrInfo.au1AttrVal, u4Offset,
                                 MRP_MAC_ADDR_LEN);
            MrpRdUtlProcAttrDelSyncUpMsg (u4ContextId, u2PortId, VlanId,
                                          &AttrInfo, MRP_MMRP_APP_ID);
            break;

        case MRP_RED_SERVICE_REQ_DEL_MSG:
            MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, VlanId);
            MEMSET (&AttrInfo, 0, sizeof (tMrpAttr));
            AttrInfo.u1AttrType = MRP_SERVICE_REQ_ATTR_TYPE;
            AttrInfo.u1AttrLen = MRP_SERVICE_REQ_LEN;
            MRP_RED_GET_1_BYTE (pRmMsg, u4Offset, AttrInfo.au1AttrVal[0]);
            MrpRdUtlProcAttrDelSyncUpMsg (u4ContextId, u2PortId, VlanId,
                                          &AttrInfo, MRP_MMRP_APP_ID);
            break;

        default:
            break;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlProcAttrDelSyncUpMsg 
 *
 *    DESCRIPTION      : This function processes the Attribute Delete
 *                       sync up message for the given application.
 *
 *    INPUT            : pRmMsg - Sync up message 
 *                       u2MsgLen - Message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
INT4
MrpRdUtlProcAttrDelSyncUpMsg (UINT4 u4ContextId, UINT2 u2PortId,
                              tVlanId VlanId, tMrpAttr * pMmrpAttrVal,
                              UINT1 u1AppId)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpIfMsg           IfMsg;
    UINT2               u2AttrIndex = 0;
    UINT2               u2MapId = 0;

    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);
    if (NULL == pContextInfo)
    {
        return OSIX_FAILURE;
    }

    pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);

    if (MRP_MVRP_APP_ID == u1AppId)
    {
        /* Get the MAP ID to which this VLAN belongs */
        for (u2MapId = 0; u2MapId < pAppEntry->u2MapArraySize; u2MapId++)
        {
            pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

            if (NULL == pMapEntry)
            {
                continue;
            }

            pAttrEntry = MRP_GET_ATTR_ENTRY (pMapEntry, VlanId);

            if (NULL != pAttrEntry)
            {
                break;
            }
        }

        if (NULL == pAttrEntry)
        {
            MRP_TRC ((pContextInfo, MRP_RED_TRC, "MrpRdUtlApplyDeleteSyncUpMsg:"
                      " No Attribute Entry for MAP ID %d on port %d for Vlan"
                      " Id %d\n", u2MapId, u2PortId, VlanId));
            return OSIX_FAILURE;
        }
    }
    else if (MRP_MMRP_APP_ID == u1AppId)
    {
        u2MapId = VlanId;
    }

    pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

    if (NULL == pMapEntry)
    {
        MRP_TRC ((pContextInfo, MRP_RED_TRC,
                  "MrpRdUtlApplyDeleteSyncUpMsg: No Map Entry Exists for "
                  "MAP ID %d on port %d\n", u2MapId, u2PortId));
        return OSIX_FAILURE;
    }

    if (MRP_MMRP_APP_ID == u1AppId)
    {
        pAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pMmrpAttrVal, pMapEntry);

        if (NULL == pAttrEntry)
        {
            MRP_TRC ((pContextInfo, MRP_RED_TRC, "MrpRdUtlApplyDeleteSyncUpMsg:"
                      " No MMRP Attribute Entry for MAP ID %d on port %d\n",
                      u2MapId, u2PortId));
            return OSIX_FAILURE;
        }
    }

    pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

    if (NULL == pMapPortEntry)
    {

        MRP_TRC ((pContextInfo, MRP_PDU_TRC,
                  "MrpRdUtlApplyDeleteSyncUpMsg: No Port Entry for MAP ID"
                  " %d for port %d\n", u2MapId, u2PortId));
        return OSIX_FAILURE;
    }

    MEMSET (&IfMsg, 0, sizeof (tMrpIfMsg));
    IfMsg.u4ContextId = u4ContextId;
    IfMsg.u2Port = u2PortId;
    IfMsg.pAppEntry = pAppEntry;
    IfMsg.u2MapId = u2MapId;

    /* Added to avoid Klocwork warning */
    MRP_CHK_NULL_PTR_RET (pAttrEntry, OSIX_SUCCESS);

    /* Get the position of the Attribute value in the AttrInfoList array */
    u2AttrIndex = pAttrEntry->u2AttrIndex;

    MRP_SET_ATTR_REG_SEM_STATE (pMapPortEntry->pu1AttrInfoList[u2AttrIndex],
                                MRP_MT);
    gMrpGlobalInfo.aMrpAppnFn[u1AppId].pAttrIndFn (pMapEntry, pAttrEntry->Attr,
                                                   u2PortId, MRP_HL_IND_LEAVE);
    if (AST_PORT_STATE_FORWARDING == pMapPortEntry->u1PortState)
    {
        MrpMapMadAttributeLeaveInd (&pAttrEntry->Attr, &IfMsg);
    }

    MrpMapDSDelAttrFromMapPortEntry (pMapPortEntry, pAttrEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlProcBulkUpdReq 
 *
 *    DESCRIPTION      : This function handles the Bulk update request from
 *                       the Standby node. It forms the bulk update message
 *                       and sends it to the Standby node through RM. The 
 *                       following are send as part of bulk update sync up
 *                       - List of ports that are operationally UP
 *                       - List of VLANs learnt on each port
 *                       - List of MAC Addresses learnt on each port
 *                       - List of Group Service Requirement learnt on each port
 *                       - Duration for which the LeaveAll timer need to be 
 *                         started in the Standby node
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
MrpRdUtlProcBulkUpdReq (VOID)
{
    tMrpContextInfo    *pContextInfo = NULL;
    UINT4               u4CurrentTime = 0;
    UINT4               u4ContextId = gMrpGlobalInfo.u2FirstContextId;

    if (RM_ACTIVE != MRP_RED_NODE_STATUS ())
    {
        MRP_GBL_TRC (MRP_RED_TRC, "MrpRdUtlProcBulkUpdReq: Rcvd Bulk Req when "
                     "MRP node state is not Active\r\n");
        return;
    }

    MRP_GBL_TRC (MRP_RED_TRC, "MrpRdUtlProcBulkUpdReq: Handling the Bulk Update"
                 " Request\r\n");

    /* Check if MRP is started in any of the contexts. If not sent the Tail
     * message to the Standby node.
     */
    if (OSIX_FALSE == MrpUtilIsMrpStartedInSystem ())
    {
        MRP_GBL_TRC (MRP_RED_TRC, "MrpRdUtlProcBulkUpdReq: MRP is not "
                     " started in any of the contexts. Hence sending tail"
                     " msg \r\n");
        MrpRedSendBulkUpdTailMsg ();
        return;
    }

    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    for (; pContextInfo != NULL;
         (pContextInfo = (MrpCtxtGetContextInfo
                          (pContextInfo->u2NextContextId))))
    {
        if (OSIX_TRUE == MRP_IS_MRP_STARTED (pContextInfo->u4ContextId))
        {
            break;
        }
    }

    /* Update the next relinquish interval */
    u4CurrentTime = OsixGetSysUpTime ();
    gMrpGlobalInfo.MrpRedInfo.u4NextRelinqTime =
        (u4CurrentTime + gMrpGlobalInfo.MrpRedInfo.u4RelinqInterval);
    MrpRBulkSendPortOperStatBulkUpd ();
    MrpRBulkSendLvAllTmrRemTimBlkUpd ();

    /* As Bulk update formation is suspended in between to process other events,
     * the following check is added.
     */
    if (0 != gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt)
    {
        MrpRBulkSendAttrBlkUpd (MRP_RED_VLAN_ADD_MSG);
    }

    if (0 != gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt)
    {
        MrpRBulkSendAttrBlkUpd (MRP_RED_MAC_ADD_MSG);
    }

    if (0 != gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt)
    {
        MrpRBulkSendAttrBlkUpd (MRP_RED_SERVICE_REQ_ADD_MSG);
    }

    if (0 != gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt)
    {
        MrpRedSendBulkUpdTailMsg ();
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlSendLvAllMsg 
 *
 *    DESCRIPTION      : This function send MVRP and MMRP LeaveAll messages 
 *                       through all the ports in which MVRP/MMRP is enabled.
 *
 *    INPUT            : pContextInfo - Pointer to Context Info structure
 *                       u1AppId      - Application identifier 
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : None
 *     
 ****************************************************************************/
VOID
MrpRdUtlSendLvAllMsg (tMrpContextInfo * pContextInfo, UINT1 u1AppId)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    UINT2               u2Port = pContextInfo->u2FirstPortId;

    pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);

    pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);

    for (; pPortEntry != NULL;
         (pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo,
                                           pPortEntry->u2NextPortId)))
    {
        pAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2Port);

        if ((MRP_DISABLED == MRP_PORT_APP_STATUS (pPortEntry, u1AppId)) ||
            (NULL == pAppPortEntry))
        {
            continue;
        }

        if (MrpTmrRestartLvAllTmr (pAppPortEntry) == OSIX_SUCCESS)
        {
            /* Update the Leave ALL SEM state as Active to trigger the sending
             * of LeaveAll PDUs.
             */
            pAppPortEntry->u1LeaveAllSemState = MRP_ACTIVE;
        }
        if (MRP_MVRP_APP_ID == u1AppId)
        {
            MrpMvrpHandleJoinTmrExpiry (pContextInfo->u4ContextId,
                                        pAppPortEntry);
        }
        else
        {
            MrpMmrpHandleJoinTmrExpiry (pContextInfo->u4ContextId,
                                        pAppPortEntry);
        }
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlApplyAttrRegistration 
 *
 *    DESCRIPTION      : This function updates the MRP Database and propagates
 *                       the Join Indication to other ports in the MAP Context.
 *
 *    INPUT            : pAppEntry - Pointer to Application Entry
 *                       u2MapId   - MAP ID
 *                       u2Port    - Local Port Identifier
 *                       pAttr     - Pointer to Attribute info structure
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRdUtlApplyAttrRegistration (tMrpAppEntry * pAppEntry, UINT2 u2MapId,
                               UINT2 u2Port, tMrpAttr * pAttr)
{
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT2               u2AttrIndex = 0;
    UINT1               u1StateChg = 0;

    pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

    if (NULL == pMapEntry)
    {
        /* If it not present then create the MAP and initialise the
         * values */
        pMapEntry = MrpMapDSCreateMrpMapEntry (pAppEntry, u2MapId);

        if (NULL == pMapEntry)
        {
            MRP_TRC ((pAppEntry->pMrpContextInfo, MRP_RED_TRC | ALL_FAILURE_TRC,
                      "MrpRdUtlProcAttrAddSyncUpMsg: No Free Map Entry \n"));
            return OSIX_FAILURE;
        }
    }

    pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

    if (NULL == pMapPortEntry)
    {
        pMapPortEntry = MrpMapDSCreateMrpMapPortEntry (pMapEntry, u2Port);
        if (NULL == pMapPortEntry)
        {
            MrpMapDSDeleteMrpMapEntry (pMapEntry);
            return OSIX_FAILURE;
        }
    }

    pAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMapEntry);

    if (NULL == pAttrEntry)
    {
        pAttrEntry = MrpMapDSCreateMrpMapAttrEntry (pMapEntry, pAttr);
        if (NULL == pAttrEntry)
        {
            MrpMapDSDeleteMrpMapPortEntry (pMapPortEntry);
            MrpMapDSDeleteMrpMapEntry (pMapEntry);
            return OSIX_FAILURE;
        }
    }

    if (MrpMapDSAddAttrToMapPortEntry (pMapPortEntry, pAttrEntry)
        != OSIX_SUCCESS)
    {
        MrpMapDSDelAttrEntryFromMapEntry (pMapEntry, pAttrEntry);
        MrpMapDSDeleteMrpMapPortEntry (pMapPortEntry);
        MrpMapDSDeleteMrpMapEntry (pMapEntry);
        return OSIX_FAILURE;
    }

    u2AttrIndex = pAttrEntry->u2AttrIndex;

    MrpSmRegistrarSem (pMapPortEntry, u2AttrIndex, MRP_REG_RX_JOIN_MT,
                       &u1StateChg);
    MrpSmApplicantSem (pMapPortEntry, u2AttrIndex, MRP_REG_RX_JOIN_MT);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlGetMinBulkUpdMsgLen 
 *
 *    DESCRIPTION      : This function returns the minimum space reqired for
 *                       the given Bulk Update Message Type.
 *
 *    INPUT            : u1MsgType - Bulk Update Message Type 
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : Minimum required buffer space for the given bulk 
 *                       message type.
 *     
 ****************************************************************************/
UINT4
MrpRdUtlGetMinBulkUpdMsgLen (UINT1 u1MsgType)
{
    UINT4               u4Len = 0;

    switch (u1MsgType)
    {
        case MRP_RED_PORT_OPER_STATUS_MSG:
            u4Len = MRP_RED_MIN_PORT_STAT_BLKMSG_LEN;
            break;

        case MRP_RED_MVRP_LV_ALL_TMR_MSG:
        case MRP_RED_MMRP_LV_ALL_TMR_MSG:
            u4Len = MRP_RED_MIN_LVALL_TMR_BLKMSG_LEN;
            break;

        case MRP_RED_VLAN_ADD_MSG:
            u4Len = MRP_RED_MIN_VLAN_BLKMSG_LEN;
            break;

        case MRP_RED_MAC_ADD_MSG:
            u4Len = MRP_RED_MIN_MAC_BLKMSG_LEN;
            break;

        case MRP_RED_SERVICE_REQ_ADD_MSG:
            u4Len = MRP_RED_MIN_SER_REQ_BLKMSG_LEN;
            break;

        default:
            /* Invalid message type */
            break;
    }

    return u4Len;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRdUtlEnableApplications 
 *
 *    DESCRIPTION      : This function enables the Applications based on the
 *                       Admin status.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : None
 *     
 ****************************************************************************/
VOID
MrpRdUtlEnableApplications ()
{
    tMrpContextInfo    *pContextInfo = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4ContextId = gMrpGlobalInfo.u2FirstContextId;

    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    for (; pContextInfo != NULL;
         (pContextInfo = (MrpCtxtGetContextInfo
                          (pContextInfo->u2NextContextId))))
    {
        if (OSIX_FALSE == MRP_IS_MRP_STARTED (pContextInfo->u4ContextId))
        {
            continue;
        }

        if (MRP_ENABLED == pContextInfo->u1MvrpAdminStatus)
        {
            i4RetVal = MrpMvrpEnable (pContextInfo);

            if (i4RetVal == OSIX_FAILURE)
            {
                MRP_TRC ((pContextInfo, MRP_RED_TRC,
                          "MrpRedMakeNodeActiveFromIdle: Failed to enable"
                          " Mvrp.\n"));
            }
        }

        if (MRP_ENABLED == pContextInfo->u1MmrpAdminStatus)
        {
            i4RetVal = MrpMmrpEnable (pContextInfo);

            if (i4RetVal == OSIX_FAILURE)
            {
                MRP_TRC ((pContextInfo, MRP_RED_TRC,
                          "MrpRedMakeNodeActiveFromIdle: Failed to enable"
                          " Mmrp.\n"));
            }
        }
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrprdutl.c                     */
/*-----------------------------------------------------------------------*/
