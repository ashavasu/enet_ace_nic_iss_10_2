/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpbuddy.c,v 1.2 2009/08/31 09:43:07 prabuc Exp $
 *
 * Description: This file contains the routines related to Buddy memory.
 ******************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *   
 *    FUNCTION NAME    : MrpBuddyCalculateBuddySize
 *                                                                          
 *    DESCRIPTION      : This function calculates the size of the buddy that 
 *                       needs to be decreased.
 *
 *    INPUT            : ppInfo       - Pointer to the structure in which 
 *                                      we have to check for the value.
 *                       u2CurrSize   - Current buddy size 
 *                       u2DefBlkSize - default buddy block size 
 *                       u1BuddyId    - Buddy Id                 
 *                                                                          
 *    OUTPUT           : pu2BlkCount   - Num of blocks to be reduced.
 *                       pu2EntryCount - Num of entry present in the above
 *                                       mentioned pointer.
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpBuddyCalculateBuddySize (VOID **ppInfo, UINT2 u2CurrSize, UINT2 u2DefBlkSize,
                            UINT1 u1BuddyId, UINT2 *pu2ReqBuddySize,
                            UINT2 *pu2EntryCount)
{
    UINT2               u2StartIndex = 0;
    UINT2               u2EndIndex = 0;
    UINT2               u2BlkCount = 0;
    UINT2               u2EntryCount = 0;

    /* This function is called when the last Buddy block does not contain any
     * entries and the number of buddy blocks allocated is greater than the
     * default size. So we need to check if entries are present in any of the
     * previous blocks and if so the number of entries present in the new last
     * Buddy block needs to be updated.
     */
    /* Get the end index of the second last Buddy block. */
    u2EndIndex = (UINT2) (u2CurrSize - u2DefBlkSize);

    do
    {
        u2EntryCount = 0;
        u2BlkCount++;
        if (u2EndIndex == u2DefBlkSize)
        {
            /* Only two Buddy blocks were alloted for the given dynamically 
             * growing array.
             */
            u2StartIndex = 1;

            if ((u1BuddyId == (UINT1) gMrpGlobalInfo.MapTableBuddyId) ||
                (u1BuddyId == (UINT1) gMrpGlobalInfo.MvrpPortAttrBuddyId) ||
                (u1BuddyId == (UINT1) gMrpGlobalInfo.LvBitListBuddyId))
            {
                /* For the MAP Table, MVRP Port Attribute List, LvBitList, 
                 * the index of the array starts from 0.
                 * For all other dynamically growing arrays, the index starts 
                 * from 1.
                 */
                u2StartIndex = 0;
            }
        }
        else
        {
            /* Get the start index of the second last Buddy block */
            u2StartIndex = (UINT2) (u2EndIndex - u2DefBlkSize);
        }

        /* Find the number of entries present in the Buddy block starting with
         * u2StartIndex as the first index.
         */
        MrpBuddyGetNoOfEntriesInBuddyBlk (u2StartIndex, u2EndIndex, u1BuddyId,
                                          ppInfo, &u2EntryCount);

        if (u2EntryCount != 0)
        {
            /* Entries are present in the current Buddy block. Hence this buddy 
             * block should be made as the last block of the dynamically 
             * growing array.
             */
            break;
        }
        /* No entries were present in this block. Hence check if entries are
         * present in the previous block. Repeat this until the block with
         * default size is reached. */
        u2EndIndex = u2StartIndex;
    }
    while (u2EndIndex > 1);

    /* Calculate the Required size of the dynamically growing array. u2BlkCount
     * represents the number of blocks that need to be removed and 
     * u2EntryCount represents the number of entries present in the block that
     * is to be made as the last block of the given dynamically growing array. 
     */
    *pu2ReqBuddySize = (UINT2) (u2CurrSize - (u2BlkCount * u2DefBlkSize));
    *pu2EntryCount = u2EntryCount;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpBuddyGetNoOfEntriesInBuddyBlk 
 *                                                                          
 *    DESCRIPTION      : This function returns the number of entries present
 *                       in the Buddy block within the size specified by the
 *                       start and end Index.
 *
 *    INPUT            : u2StartIndex - Start Index
 *                       u2EndIndex   - End Index
 *                       u1BuddyId    - Buddy Block Identifier
 *                       ppInfo       - Pointer to the array which has to be
 *                                      resized
 *                                                                          
 *    OUTPUT           : pu2EntryCount - Number of entries present in the 
 *                                       Buddy block within the size specified
 *                                       by u2StartIndex and u2EndIndex
 *                                                                          
 *    RETURNS          : NONE 
 *                                                                          
 ****************************************************************************/
VOID
MrpBuddyGetNoOfEntriesInBuddyBlk (UINT2 u2StartIndex, UINT2 u2EndIndex,
                                  UINT1 u1BuddyId, VOID **ppInfo,
                                  UINT2 *pu2EntryCount)
{
    UINT2               u2Index = 0;
    UINT1              *pu1Val = NULL;

    for (u2Index = u2StartIndex; u2Index < u2EndIndex; u2Index++)
    {
        if ((u1BuddyId == (UINT1) gMrpGlobalInfo.MapPortAttrBuddyId) ||
            (u1BuddyId == (UINT1) gMrpGlobalInfo.MvrpPortAttrBuddyId) ||
            (u1BuddyId == (UINT1) gMrpGlobalInfo.LvBitListBuddyId))
        {
            pu1Val = *ppInfo;
            if (pu1Val[u2Index] != 0)
            {
                MrpBuddyIncNumOfEntryCnt (u1BuddyId, pu2EntryCount,
                                          pu1Val[u2Index]);
            }
        }
        else
        {
            /* For all other dynamically growing arrays, address is stored
             * at each index. Hence increment the count if it is not NULL.
             */
            if (ppInfo[u2Index] != NULL)
            {
                (*pu2EntryCount)++;
            }
        }
    }

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpBuddyIncNumOfEntryCnt 
 *                                                                          
 *    DESCRIPTION      : This function returns the number of entries present
 *                       in the MapPortAttr or MvrpPortAttr Array
 *
 *    INPUT            : u1BuddyId    - Buddy Block Identifier
 *                       u1Val        - Value present in the current Index 
 *                                      position
 *                                                                          
 *    OUTPUT           : pu2EntryCount - Number of entries present in the 
 *                                       Buddy block
 *                                                                          
 *    RETURNS          : NONE 
 *                                                                          
 ****************************************************************************/
VOID
MrpBuddyIncNumOfEntryCnt (UINT1 u1BuddyId, UINT2 *pu2EntryCount, UINT1 u1Val)
{
    UINT2               u2BitIndex = 0;

    /* In case of MapPortAttr Array, values are stored in each
     * index. Hence increment the count if the value at the 
     * index is not zero. */
    if (u1BuddyId == (UINT1) gMrpGlobalInfo.MapPortAttrBuddyId)
    {
        (*pu2EntryCount)++;
    }
    else
    {
        /* In case of MvrpPortAttr Array, values are stored in
         * each bit. Hence increment the count depending on the
         * number of bits set at the current index. */
        for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE) && (u1Val != 0));
             u2BitIndex++)
        {
            if ((u1Val & MRP_BIT8) != 0)
            {
                (*pu2EntryCount)++;
            }

            u1Val = (UINT1) (u1Val << 1);
        }
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpbuddy.c                     */
/*-----------------------------------------------------------------------*/
