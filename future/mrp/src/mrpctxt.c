/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpctxt.c,v 1.9 2014/01/24 12:23:50 siva Exp $
 *
 * Description: This file contains the Virtual Context related utility routines
 ******************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpCtxtHandleCreateContext
 *
 *    DESCRIPTION      : This function creates given context in MRP
 *
 *    INPUT            : u4ContextId - Context Identifier
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpCtxtHandleCreateContext (UINT4 u4ContextId)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpContextInfo    *pTempContextInfo = NULL;
    UINT4               u4BridgeMode = MRP_INVALID_BRIDGE_MODE;
    UINT2               u2PrevContextId = (UINT2) MRP_INVALID_CONTEXT_ID;

    /* Check if the context is already exsist */
    if (NULL != MRP_CONTEXT_PTR (u4ContextId))
    {
        return OSIX_SUCCESS;
    }

    pContextInfo = (tMrpContextInfo *) MemAllocMemBlk
        (gMrpGlobalInfo.ContextPoolId);

    if (pContextInfo == NULL)
    {
        MRP_GBL_TRC ((ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                     "MrpCtxtHandleCreateContext: Failed to"
                     " Allocate memory for the Context Entry.\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pContextInfo, 0, sizeof (tMrpContextInfo));

    /* store the context pointer in the global context array  */
    MRP_CONTEXT_PTR (u4ContextId) = pContextInfo;

    if (MrpPortL2IwfGetBridgeMode (u4ContextId, &u4BridgeMode) != L2IWF_SUCCESS)
    {
        MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                  "MrpCtxtHandleCreateContext: Failed to get the "
                  "bridge mode for Context %d.\r\n", u4ContextId));
        MemReleaseMemBlock (gMrpGlobalInfo.ContextPoolId,
                            (UINT1 *) pContextInfo);
        MRP_CONTEXT_PTR (u4ContextId) = NULL;

        return OSIX_FAILURE;
    }

    /* Initialise default values for this context info */

    pContextInfo->u4ContextId = u4ContextId;
    pContextInfo->u4BridgeMode = u4BridgeMode;
    pContextInfo->u4TrcOption = MRP_CRITICAL_TRC;
    gMrpGlobalInfo.au1MvrpStatus[u4ContextId] = MRP_DISABLED;
    gMrpGlobalInfo.au1MmrpStatus[u4ContextId] = MRP_DISABLED;
    pContextInfo->u1MvrpTrapEnabled = MRP_DISABLED;
    pContextInfo->u1MmrpTrapEnabled = MRP_DISABLED;
    pContextInfo->u2FirstPortId = MRP_INIT_VAL;

    /* Update the First, Next and Previous contexts. */
    pContextInfo->u2NextContextId = MRP_INIT_VAL;
    pContextInfo->u2PrevContextId = MRP_INIT_VAL;

    if (gMrpGlobalInfo.u2FirstContextId == MRP_INIT_VAL)
    {
        gMrpGlobalInfo.u2FirstContextId = (UINT2) u4ContextId;
    }
    else if (gMrpGlobalInfo.u2FirstContextId > u4ContextId)
    {
        pContextInfo->u2NextContextId = gMrpGlobalInfo.u2FirstContextId;
        pContextInfo->u2PrevContextId = MRP_INIT_VAL;

        pTempContextInfo = MrpCtxtGetContextInfo (gMrpGlobalInfo.
                                                  u2FirstContextId);

        if (pTempContextInfo != NULL)
        {
            pTempContextInfo->u2PrevContextId = (UINT2) u4ContextId;
        }
        gMrpGlobalInfo.u2FirstContextId = (UINT2) u4ContextId;
    }
    else
    {
        for (u2PrevContextId = (UINT2) (u4ContextId - 1);
             u2PrevContextId >= MRP_DEFAULT_CONTEXT_ID; u2PrevContextId--)
        {
            pTempContextInfo = MrpCtxtGetContextInfo (u2PrevContextId);

            if (pTempContextInfo != NULL)
            {
                pContextInfo->u2NextContextId =
                    pTempContextInfo->u2NextContextId;
                pContextInfo->u2PrevContextId = u2PrevContextId;
                pTempContextInfo->u2NextContextId = (UINT2) u4ContextId;
                break;
            }
        }

        if (pContextInfo->u2NextContextId != MRP_INIT_VAL)
        {
            pTempContextInfo =
                MrpCtxtGetContextInfo (pContextInfo->u2NextContextId);

            if (pTempContextInfo != NULL)
            {
                pTempContextInfo->u2PrevContextId = (UINT2) u4ContextId;
            }
        }
    }
    if (MrpPortVcmGetSystemMode (MRP_PROTOCOL_ID) == VCM_MI_MODE)
    {
        /* Get the Context Name */
        MrpPortVcmGetAliasName (u4ContextId, pContextInfo->au1ContextName);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpCtxtHandleDeleteContext
 *
 *    DESCRIPTION      : This function deletes given context in MRP
 *
 *    INPUT            : u4ContextId - Context Identifier
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpCtxtHandleDeleteContext (UINT4 u4ContextId)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpContextInfo    *pPrevContextInfo = NULL;
    tMrpContextInfo    *pNextContextInfo = NULL;

    pContextInfo = MRP_CONTEXT_PTR (u4ContextId);

    /* Disable the MVRP and MMRP Application for this context */
    MrpMvrpDisable (pContextInfo);
    MrpMmrpDisable (pContextInfo);

    if (MrpMainDeInit (pContextInfo) == OSIX_FAILURE)
    {
        MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                  "MrpCtxtHandleDeleteContext: MrpMainDeInit call "
                  "failed for Context %d.\r\n", u4ContextId));
        return OSIX_FAILURE;
    }

    pPrevContextInfo = MrpCtxtGetContextInfo (pContextInfo->u2PrevContextId);
    pNextContextInfo = MrpCtxtGetContextInfo (pContextInfo->u2NextContextId);

    if ((pPrevContextInfo != NULL) && (pNextContextInfo != NULL))
    {
        pPrevContextInfo->u2NextContextId = pContextInfo->u2NextContextId;
        pNextContextInfo->u2PrevContextId = pContextInfo->u2PrevContextId;
    }
    else if (pPrevContextInfo != NULL)
    {
        pPrevContextInfo->u2NextContextId = MRP_INIT_VAL;
    }
    else if (pNextContextInfo != NULL)
    {
        pNextContextInfo->u2PrevContextId = MRP_INIT_VAL;
        gMrpGlobalInfo.u2FirstContextId =
            (UINT2) (pNextContextInfo->u4ContextId);
    }
    else
    {
        gMrpGlobalInfo.u2FirstContextId = MRP_INIT_VAL;
    }
    /* Release the Memory for Context Info for this context */
    MemReleaseMemBlock (gMrpGlobalInfo.ContextPoolId, (UINT1 *) pContextInfo);

    MRP_CONTEXT_PTR (u4ContextId) = NULL;

    gMrpGlobalInfo.au1MrpSystemCtrl[u4ContextId] = MRP_SHUTDOWN;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpCtxtDelPortEntryFromCxtEntry
 *                                                                          
 *    DESCRIPTION      : This function deletes the Port entry from the context 
 *                       structure.
 *
 *    INPUT            : pMrpContextInfo  - Pointer to the Application structure
 *                       pMrpPortEntry - Pointer to the Port structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpCtxtDelPortEntryFromCxtEntry (tMrpContextInfo * pMrpContextInfo,
                                 tMrpPortEntry * pMrpPortEntry)
{
    if (MRP_GET_PORT_ENTRY (pMrpContextInfo,
                            pMrpPortEntry->u2LocalPortId) != NULL)
    {
        pMrpContextInfo->apMrpPortEntry[pMrpPortEntry->u2LocalPortId] = NULL;

    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpCtxtHandleUpdateContextName
 *
 *    DESCRIPTION      : This function updated the name of the context.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpCtxtHandleUpdateContextName (UINT4 u4ContextId)
{
    tMrpContextInfo    *pContextInfo = NULL;

    pContextInfo = MRP_CONTEXT_PTR (u4ContextId);

    if (pContextInfo == NULL)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC,
                     "MrpCtxtHandleUpdateContextName : Getting Context Info"
                     " Failed. \r\n");
        return OSIX_FAILURE;
    }

    MEMSET (pContextInfo->au1ContextName, 0, MRP_CONTEXT_NAME_LEN);

    if (MrpPortVcmGetSystemMode (MRP_PROTOCOL_ID) == VCM_MI_MODE)
    {
        /* Get the Context Name */
        MrpPortVcmGetAliasName (u4ContextId, pContextInfo->au1ContextName);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpCtxtNotifyContextCreateOrDel
 *                                                                          
 *    DESCRIPTION      : This function notifies MRP module about the Context
 *                       Creation or Deletion.
 *
 *    INPUT            : u4ContextId - Virtual Context Identifier 
 *                       u2MsgType   - MRP_CREATE_CONTEXT_MSG / 
 *                                     MRP_DELETE_CONTEXT_MSG
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpCtxtNotifyContextCreateOrDel (UINT4 u4ContextId, UINT2 u2MsgType)
{
    tMrpQMsg           *pQMsg = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    /* Is MRP task initialized */
    if (gMrpGlobalInfo.u1MrpIsInitComplete == OSIX_FALSE)
    {
        /* Return Silently. MRP task is not yet initialized */
        return OSIX_SUCCESS;
    }

    pQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pQMsg)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC, "MrpCtxtNotifyContextCreateOrDel: "
                     "Q Msg memory allocation failed\n");
        return OSIX_FAILURE;
    }

    MEMSET (pQMsg, 0, sizeof (tMrpQMsg));
    pQMsg->u2MsgType = MRP_MSG;
    pQMsg->unMrpMsg.MrpMsg.u2MsgType = u2MsgType;
    pQMsg->u4ContextId = u4ContextId;

    i4RetVal = MrpQueEnqMsg (pQMsg);

    if (i4RetVal == OSIX_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpCtxtGetContextInfo
 *                                                                          
 *    DESCRIPTION      : This function validates the virtual context identifier
 *                       and returns the pointer to the Context Info structure
 *
 *    INPUT            : u4ContextId - Virtual Context Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to the Context Info structure 
 *                                                                          
 ****************************************************************************/
tMrpContextInfo    *
MrpCtxtGetContextInfo (UINT4 u4ContextId)
{
    return (((u4ContextId != 0) && (u4ContextId < MRP_SIZING_CONTEXT_COUNT)
             && (u4ContextId <=
                 SYS_DEF_MAX_NUM_CONTEXTS)) ? gMrpGlobalInfo.
            apContextInfo[u4ContextId] : NULL);
}

/*****************************************************************************
 * FUNCTION NAME      : MrpCtxtGetNextPortInContext                   
 *     
 *                                                                           
 * DESCRIPTION        : This function is used to get the Next Active Port in 
 *                      the Given Context.
 *                                                                           
 * INPUT(s)           : u4ContextId    - Context Identifier   
 *                      u4PortId       - Current Port Id
 *                      pu1NextPortId  - Next Port Id
 *                                       
 * OUTPUT(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : OSIX_SUCCESS / OSIX_FAILURE                            
 *****************************************************************************/
INT4
MrpCtxtGetNextPortInContext (UINT4 u4ContextId, UINT4 u4PortId,
                             UINT4 *pu4NextPortId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

    if (pMrpContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    for (u4PortId = u4PortId + 1;
         u4PortId <= MRP_MAX_PORTS_PER_CONTEXT; u4PortId++)
    {
        pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2) u4PortId);
        if (pMrpPortEntry != NULL)
        {
            *pu4NextPortId = u4PortId;
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************
 * FUNCTION NAME      : MrpCtxtGetValidContext
 *     
 *                                                                           
 * DESCRIPTION        : This function will return the pointer to the context 
 *                      info structure when the context is exist. Else this 
 *                      will return the next nearest context info.
 *                                                                           
 * INPUT(s)           : u4ContextId    - Context Identifier   
 *                                       
 * OUTPUT(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : Pointer to the context info structure
 *****************************************************************************/
tMrpContextInfo    *
MrpCtxtGetValidContext (UINT4 u4ContextId)
{
    tMrpContextInfo    *pContextInfo = NULL;
    UINT4               u4Index = 0;

    if (u4ContextId == 0)
    {
        return (MrpCtxtGetContextInfo (gMrpGlobalInfo.u2FirstContextId));
    }

    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (pContextInfo != NULL)
    {
        return pContextInfo;
    }

    for (u4Index = gMrpGlobalInfo.u2FirstContextId; u4Index != MRP_INIT_VAL;
         u4Index = pContextInfo->u2NextContextId)
    {
        pContextInfo = MrpCtxtGetContextInfo (u4Index);

        if ((pContextInfo == NULL) || (pContextInfo->u4ContextId > u4ContextId))
        {
            return pContextInfo;
        }
    }

    return NULL;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpctxt.c                      */
/*-----------------------------------------------------------------------*/
