/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpsm.c,v 1.9 2013/06/25 12:08:38 siva Exp $
 *
 * Description: This file contains the State Event Machine implimentation 
 *              routines.
 *****************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpSmRegistrarSem
 *                                                                          
 *    DESCRIPTION      : This functions implements the Registar State Machine.
 *
 *    INPUT            : pMapPortEntry - Pointer to the MAP Port Entry.
 *                       u2AttrIndex   - Position of the Attribute Value
 *                                       whose Registrar State Machine is
 *                                       affected.
 *                       u1SemEvent    - Event to be applied to the Registrar
 *                                       State Machine.
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
INT4
MrpSmRegistrarSem (tMrpMapPortEntry * pMapPortEntry, UINT2 u2AttrIndex,
                   UINT1 u1SemEvent, UINT1 *pu1StateChg)
{
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpRegSemEntry    *pRegSemEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpAttr           *pAttr = NULL;
    tMrpIfMsg           IfMsg;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2MapId = 0;
    UINT2               u2Port = 0;
    UINT1              *pu1AttrList = NULL;
    UINT1               u1AppId = 0;
    UINT1               u1CurrRegState = 0;
    UINT1               u1UpdRegState = OSIX_TRUE;

    MEMSET (&IfMsg, 0, sizeof (tMrpIfMsg));
    pContextInfo = pMapPortEntry->pMapEntry->pMrpAppEntry->pMrpContextInfo;
    u2Port = pMapPortEntry->u2Port;
    pu1AttrList = pMapPortEntry->pu1AttrInfoList;
    u2MapId = pMapPortEntry->pMapEntry->u2MapId;
    pAttrEntry = pMapPortEntry->pMapEntry->ppAttrEntryArray[u2AttrIndex];
    u1AppId = pMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId;

    if ((0 == u1AppId) || (u1AppId > MRP_MAX_APPS))
    {
        /* Added to avoid Klocwork warning */
        return OSIX_SUCCESS;
    }

    if (NULL == pAttrEntry)
    {
        return OSIX_SUCCESS;
    }
    pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMapPortEntry,
                                                   pMapPortEntry->u2Port);
    if (pAppPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pPortEntry = pAppPortEntry->pMrpPortEntry;

    if (MRP_APPLICANT_ONLY == pPortEntry->u1ParticipantType)
    {
        /* Registrar state machine should not be running on Applicant-Only
         * ports */
        return OSIX_SUCCESS;
    }

    u1CurrRegState = MRP_GET_ATTR_REG_SEM_STATE (pu1AttrList[u2AttrIndex]);

    pRegSemEntry = &gaMrpRegSem[u1SemEvent][u1CurrRegState];

    if (u1CurrRegState == pRegSemEntry->u4NextState)
    {

        MRP_TRC ((pContextInfo, MRP_REG_SEM_TRC,
                  "MrpSmRegistrarSem: No state change for the following "
                  "attribute on port %d for MAP ID %d\n", u2Port, u2MapId));
        MRP_PRINT_ATTRIBUTE (&pAttrEntry->Attr);

        return OSIX_SUCCESS;
    }

    *pu1StateChg = OSIX_TRUE;

    /* Give indication to Higher Layer Application (VLAN) and also propagate 
     * attribute information to other ports.
     */
    if (MRP_HL_IND_NONE != pRegSemEntry->HlMsgInd)
    {
        /* Get the Attribute value for which the Indication has to be sent */
        pAttr = &pAttrEntry->Attr;
        pAppEntry = pMapPortEntry->pMapEntry->pMrpAppEntry;
        IfMsg.u4ContextId = pContextInfo->u4ContextId;
        IfMsg.pAppEntry = pAppEntry;
        IfMsg.u2MapId = u2MapId;
        IfMsg.u2Port = u2Port;

        i4RetVal = gMrpGlobalInfo.aMrpAppnFn[u1AppId].pAttrIndFn
            (pMapPortEntry->pMapEntry,
             pAttrEntry->Attr, u2Port, (UINT1) pRegSemEntry->HlMsgInd);
        if (OSIX_FAILURE == i4RetVal)
        {
            /* If updation of Vlan Dynamic information fails and Regsem state is
             * not changed to MRP_IN from MRP_MT the attribute should not be
             * registered or propagated
             */
            MrpAttrCheckAndDelAttrEntry (pMapPortEntry, pAttrEntry);

            MRP_TRC ((pContextInfo, MRP_REG_SEM_TRC,
                      "MrpSmRegistrarSem: Higher Layer indication failed"
                      "for the following attribute on port %d for MAP "
                      "ID %d\n", u2Port, u2MapId));
            MRP_PRINT_ATTRIBUTE (&pAttrEntry->Attr);
            *pu1StateChg = OSIX_FALSE;
            return OSIX_FAILURE;
        }
        else
        {
            if (AST_PORT_STATE_FORWARDING == pMapPortEntry->u1PortState)
            {
                /* Propagate to other ports */
                if ((MRP_HL_IND_NEW == pRegSemEntry->HlMsgInd) ||
                    (MRP_HL_IND_JOIN == pRegSemEntry->HlMsgInd))
                {
                    MrpMapMadAttributeJoinInd (pAttr, &IfMsg,
                                               pRegSemEntry->HlMsgInd);
                }
                else
                {
                    MrpMapMadAttributeLeaveInd (pAttr, &IfMsg);
                }
            }
        }
    }

    switch (pRegSemEntry->LeaveTmrInd)
    {
        case MRP_TMR_START:
            MrpTmrStartLeaveTmr (pMapPortEntry, u2AttrIndex);
            break;

        case MRP_TMR_STOP:
            MrpTmrStopLeaveTmr (pMapPortEntry, u2AttrIndex);
            break;
        case MRP_TMR_NONE:
        default:
            /* No operation needs to be done on the Leave Timer */
            break;
    }

    if (MRP_IN == pRegSemEntry->u4NextState)
    {
        /* Increment the number of registrations on this port */
        MRP_INCR_PORT_REG_CNT (pPortEntry, u1AppId);

    }

    if (MRP_MT == pRegSemEntry->u4NextState)
    {
        if (MRP_GET_ATTR_APP_SEM_STATE (pu1AttrList[u2AttrIndex]) == MRP_VO)
        {
            MrpMapDSDelAttrFromMapPortEntry (pMapPortEntry, pAttrEntry);
            MrpMapDsCheckAndDeleteMapEntry (pMapPortEntry->pMapEntry);
            /* As the State is updated by the function 
             * MrpMapDSDelAttrFromMapPortEntry, no need to update it at 
             * the end of the function. */
            u1UpdRegState = OSIX_FALSE;
        }
        else if (MRP_REG_FLUSH == u1SemEvent)
        {
            /* On getting the Flush! event, if the current Registrar state 
             * is IN, then give indication to higher layers to remove the
             * learnt entry and remove the same from the MRP database.
             * The action to be done when the current Registar state is in LV
             * state is already done above.
             */
            if (MRP_IN == u1CurrRegState)
            {
                i4RetVal = gMrpGlobalInfo.aMrpAppnFn[u1AppId].pAttrIndFn
                    (pMapPortEntry->pMapEntry, pAttrEntry->Attr, u2Port,
                     MRP_HL_IND_LEAVE);
            }
            MrpMapDSDelAttrFromMapPortEntry (pMapPortEntry, pAttrEntry);
            MrpMapDsCheckAndDeleteMapEntry (pMapPortEntry->pMapEntry);
            /* As the State is updated by the function 
             * MrpMapDSDelAttrFromMapPortEntry, no need to update it at 
             * the end of the function. */
            u1UpdRegState = OSIX_FALSE;
        }

        /* Decrement the number of registrations on this port */
        MRP_DECR_PORT_REG_CNT (pPortEntry, u1AppId);
    }

    if (u1UpdRegState == OSIX_TRUE)
    {
        MRP_SET_ATTR_REG_SEM_STATE (pu1AttrList[u2AttrIndex],
                                    pRegSemEntry->u4NextState);
    }

    MRP_TRC ((pContextInfo, MRP_REG_SEM_TRC,
              "MrpSmRegistrarSem: State changed to %s for the following "
              "attribute on port %d for MAP ID %d on receiving event"
              " %s \n", gau1RegSEMState[pRegSemEntry->u4NextState],
              u2Port, u2MapId, gau1RegSEMEvtName[u1SemEvent]));
    MRP_PRINT_ATTRIBUTE (&pAttrEntry->Attr);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpSmApplicantSem
 *                                                                          
 *    DESCRIPTION      : This functions implements the Applicant State Machine.
 *
 *    INPUT            : pMapPortEntry - Pointer to the MAP Port Entry.
 *                       u2AttrIndex   - Position of the Attribute Value
 *                                       whose Applicant State Machine is
 *                                       affected.
 *                       u1SemEvent    - Event to be applied to the Applicant
 *                                       State Machine.
 *
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : eMrpSendMsgInd - The type of Message to be sent on 
 *                                        getting the transmit oppurtunity.
 *                                                                          
 ****************************************************************************/

eMrpSendMsgInd
MrpSmApplicantSem (tMrpMapPortEntry * pMapPortEntry, UINT4 u2AttrIndex,
                   UINT1 u1SemEvent)
{
    tMrpAppSemEntry    *pAppSemEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    UINT4               u4RemainingTime = 0;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;
    UINT2               u2Port = 0;
    UINT2               u2MapId = 0;
    UINT1               u1ParticipantType = 0;
    UINT1               u1CurrState = 0;
    UINT1               u1RegState = 0;
    UINT1               u1UpdateFlag = OSIX_FALSE;

    u2Port = pMapPortEntry->u2Port;
    u2MapId = pMapPortEntry->pMapEntry->u2MapId;
    pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMapPortEntry,
                                                   pMapPortEntry->u2Port);
    if (pAppPortEntry == NULL)
    {
        return MRP_SEND_NONE;
    }
    pMrpPortEntry = pAppPortEntry->pMrpPortEntry;

    u1ParticipantType = pMrpPortEntry->u1ParticipantType;

    u1RegState = MRP_GET_ATTR_REG_SEM_STATE (pMapPortEntry->
                                             pu1AttrInfoList[u2AttrIndex]);
    u1CurrState = MRP_GET_ATTR_APP_SEM_STATE (pMapPortEntry->
                                              pu1AttrInfoList[u2AttrIndex]);

    u4ContextId = pMrpPortEntry->pMrpContextInfo->u4ContextId;

    if ((u1CurrState >= MRP_MAX_APP_STATES)
        || (u1SemEvent >= MRP_MAX_APP_EVENTS))
    {
        return MRP_SEND_NONE;
    }

    if ((u1RegState == MRP_MT) && (u1CurrState == MRP_VO))
    {
        if ((u1SemEvent == MRP_APP_RX_LEAVE) ||
            (u1SemEvent == MRP_APP_RX_LEAVE_ALL) ||
            (u1SemEvent == MRP_APP_REDECLARE))
        {
            /* Attribute was neither registered nor declared, so no need 
             * to apply Rx Leave, LeaveAll and redeclare. */
            return MRP_SEND_NONE;
        }
    }

    if (MRP_FULL_PARTICIPANT == u1ParticipantType)
    {
        if (OSIX_TRUE == pMrpPortEntry->bOperP2PStatus)
        {
            pAppSemEntry = gaMrpFullAppP2PSem[u1SemEvent][u1CurrState];
        }
        else
        {
            pAppSemEntry = gaMrpFullAppSem[u1SemEvent][u1CurrState];
        }
    }
    else if (MRP_APPLICANT_ONLY == u1ParticipantType)
    {
        if (OSIX_TRUE == pMrpPortEntry->bOperP2PStatus)
        {
            pAppSemEntry = gaMrpAppOnlyP2PSem[u1SemEvent][u1CurrState];
        }
        else
        {
            pAppSemEntry = gaMrpAppOnlySem[u1SemEvent][u1CurrState];
        }
    }

    if (NULL == pAppSemEntry)
    {
        return MRP_SEND_NONE;
    }

    if ((MRP_AN == u1CurrState) && (MRP_APP_TX == u1SemEvent) &&
        (MRP_IN == u1RegState))
    {
        /* As per the IEEE802.1ak-2007 standard (note 8 of Table 10.3),
         * on getting the tx! event if the Registrar is in IN state and
         * Applicant is in AN state, then the Applicant SEM should move to
         * AA instead of QA.
         */
        pAppSemEntry->u2NextState = MRP_AA;
        pAppSemEntry->u2JoinTmrInd = OSIX_TRUE;
    }

    pAttrEntry = pMapPortEntry->pMapEntry->ppAttrEntryArray[u2AttrIndex];
    MRP_CHK_NULL_PTR_RET (pAttrEntry, MRP_SEND_NONE)
        MRP_PRINT_ATTRIBUTE (&pAttrEntry->Attr);

    if (pAppSemEntry->u2JoinTmrInd == OSIX_TRUE)
    {
        u1UpdateFlag = OSIX_TRUE;
        TmrGetRemainingTime
            (gMrpGlobalInfo.MrpTmrListId,
             &(pAppPortEntry->JoinTmrNode.TmrBlk.TimerNode), &u4RemainingTime);

        if (u4RemainingTime == 0)
        {
            MrpTmrStartJoinTmr (pAppPortEntry);
        }
    }
    else
    {
        if ((u1RegState == MRP_MT) && (pAppSemEntry->u2NextState == MRP_VO))
        {
            MrpMapDSDelAttrFromMapPortEntry (pMapPortEntry, pAttrEntry);
            MrpMapDsCheckAndDeleteMapEntry (pMapPortEntry->pMapEntry);
        }
        else
        {
            u1UpdateFlag = OSIX_TRUE;
        }
    }

    if ((u1UpdateFlag == OSIX_TRUE) &&
        (pAppSemEntry->u2NextState < MRP_NO_CHANGE) &&
        (pAppSemEntry->u2NextState != u1CurrState))
    {
        MRP_SET_ATTR_APP_SEM_STATE (pMapPortEntry->pu1AttrInfoList[u2AttrIndex],
                                    pAppSemEntry->u2NextState);

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  MRP_APP_SEM_TRC,
                  "MrpSmApplicantSem: State changed to %s for the"
                  " following attribute on port %d for MAP ID %d on receiving"
                  " event %s \n",
                  gau1AppSEMState[pAppSemEntry->u2NextState], u2Port,
                  u2MapId, gau1AppSEMEvtName[u1SemEvent]));
    }
    return (pAppSemEntry->SendMsgInd);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpsm.c                        */
/*-----------------------------------------------------------------------*/
