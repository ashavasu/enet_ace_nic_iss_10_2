/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpattr.c,v 1.8 2013/09/28 07:57:37 siva Exp $
 *
 * Description: This file contains the routines related to MRP Attributes.
 *
 ******************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpAttrMapAttrTblCmpFn
 *                                                                          
 *    DESCRIPTION      : This routine will be invoked by the RB Tree library 
 *                       to traverse through the Attribute RB tree present 
 *                       in MAP Entry.
 *
 *    INPUT            : pAttrEntry1 - Pointer to the first attribute entry.
 *                       pAttrEntry2 - Pointer to the second attribute entry.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 1, if pPort1 is greater than pPort2
 *                      -1, if pPort2 is greater than pPort1
 *                       0, if pPort1 and pPort2 are equal.
 *                                                                          
 ****************************************************************************/
INT4
MrpAttrMapAttrTblCmpFn (tRBElem * pElem1, tRBElem * pElem2)
{
    tMrpAttrEntry      *pAttrEntry1 = (tMrpAttrEntry *) pElem1;
    tMrpAttrEntry      *pAttrEntry2 = (tMrpAttrEntry *) pElem2;
    INT4                i4RetVal = 0;

    if (pAttrEntry1->pMapEntry == NULL)
    {
        return MRP_RB_LESS;
    }
    if (pAttrEntry2->pMapEntry == NULL)
    {
        return MRP_RB_GREATER;
    }

    if (pAttrEntry1->pMapEntry->u2MapId > pAttrEntry2->pMapEntry->u2MapId)
    {
        return MRP_RB_GREATER;
    }
    if (pAttrEntry1->pMapEntry->u2MapId < pAttrEntry2->pMapEntry->u2MapId)
    {
        return MRP_RB_LESS;
    }
    if (pAttrEntry1->Attr.u1AttrType > pAttrEntry2->Attr.u1AttrType)
    {
        return MRP_RB_GREATER;
    }
    else if (pAttrEntry1->Attr.u1AttrType < pAttrEntry2->Attr.u1AttrType)
    {
        return MRP_RB_LESS;
    }
    if (pAttrEntry1->Attr.u1AttrLen <= MRP_MAC_ADDR_LEN)
    {
        i4RetVal = MEMCMP (pAttrEntry1->Attr.au1AttrVal,
                           pAttrEntry2->Attr.au1AttrVal,
                           pAttrEntry1->Attr.u1AttrLen);

        if (i4RetVal > 0)
        {
            return MRP_RB_GREATER;
        }
        else if (i4RetVal < 0)
        {
            return MRP_RB_LESS;
        }
    }
    return MRP_RB_EQUAL;

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpAttrFreeMapAttrEntry
 *                                                                          
 *    DESCRIPTION      : Releases the Map attribute Entry to the free pool
 *
 *    INPUT            : pElem - Pointer to the attribut entry to be released
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
INT4
MrpAttrFreeMapAttrEntry (tRBElem * pElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    MemReleaseMemBlock (gMrpGlobalInfo.AttrPoolId, pElem);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpAttrSetAttrIndexToAttribute
 *                                                                          
 *    DESCRIPTION      : This function adds the attribute entry to the MAP
 *                       structure.
 *
 *    INPUT            : pMrpMapEntry  - Pointer to the MAP structure
 *                       pMrpAttrEntry - Pointer to the Attribute structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
VOID
MrpAttrSetAttrIndexToAttribute (tMrpMapEntry * pMrpMapEntry,
                                tMrpAttrEntry * pMrpAttrEntry)
{
    UINT2               u2VlanId = 0;

    if (pMrpMapEntry->pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        MEMCPY (&u2VlanId, pMrpAttrEntry->Attr.au1AttrVal, MRP_VLAN_ID_LEN);
        u2VlanId = OSIX_NTOHS (u2VlanId);

        pMrpAttrEntry->u2AttrIndex = u2VlanId;
    }
    else
    {
        pMrpAttrEntry->u2AttrIndex =
            pMrpMapEntry->pMrpAppEntry->u2NextFreeAttrIndex;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpAttrUpdMapNextFreeAttrIndex
 *                                                                          
 *    DESCRIPTION      : This function updates the Next free attribute Index 
 *                       This function will be called whenever an Attribute 
 *                       is added/deleted to/from the MAP entry.
 *
 *    INPUT            : pMrpMapEntry  - Pointer to the MAP structure
 *                       u1Action      - MRP_ADD/MRP_DELETE
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpAttrUpdMapNextFreeAttrIndex (tMrpMapEntry * pMrpMapEntry, UINT2 u2AttrIndex,
                                UINT1 u1Action)
{
    UINT2               u2Index = 0;

    if (u1Action == MRP_ADD)
    {
        for (u2Index = 1;
             u2Index <= pMrpMapEntry->pMrpAppEntry->u2AttrArraySize; u2Index++)
        {
            if (pMrpMapEntry->pMrpAppEntry->pu1MmrpAttrIndexList[u2Index] == 0)
            {
                pMrpMapEntry->pMrpAppEntry->u2NextFreeAttrIndex = u2Index;
                return;
            }
        }
    }
    else if (u1Action == MRP_DELETE)
    {
        if ((pMrpMapEntry->pMrpAppEntry->u2NextFreeAttrIndex == MRP_INIT_VAL) ||
            (pMrpMapEntry->pMrpAppEntry->u2NextFreeAttrIndex > u2AttrIndex))
        {
            pMrpMapEntry->pMrpAppEntry->u2NextFreeAttrIndex = u2AttrIndex;
        }
    }
}

/***********************************************************************
 * FUNCTION NAME    : MrpAttrCheckAndDelAttrEntry
 *                                                                     
 * DESCRIPTION      : Deletes the Attribute Entry from the Attribute   
 *                    Table if the Applicant is an Observer and the    
 *                    Registrar sem is in MRP_MT state.                
 *                                                                     
 * INPUT(s)         : pPortEntry - Pointer to the Port Entry           
 *                    pAttrEntry - Pointer to the Attribute Entry      
 *                                                                     
 * OUTPUT(s)        : None                                             
 *                                                                     
 * RETURNS          : OSIX_TRUE/ OSIX_FALSE                                             
 ************************************************************************/
INT4
MrpAttrCheckAndDelAttrEntry (tMrpMapPortEntry * pPortEntry,
                             tMrpAttrEntry * pAttrEntry)
{
    UINT2               u2AttrIndex = 0;

    if ((pPortEntry == NULL) || (pAttrEntry == NULL))
    {
        return OSIX_FALSE;
    }

    u2AttrIndex = pAttrEntry->u2AttrIndex;

    if (pPortEntry->pu1AttrInfoList != NULL)
    {
        if (MRP_GET_ATTR_REG_ADMIN_CTRL
            (pPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_REG_NORMAL)
        {

            /*
             * The Attribute entry must be deleted only when the Reg sem state
             * is in MRP_MT state and the App sem is in MRP_VO state
             */

            if ((MRP_GET_ATTR_REG_SEM_STATE
                 (pPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_MT)
                && (MRP_GET_ATTR_APP_SEM_STATE
                    (pPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_VO))
            {
                MrpMapDSDelAttrFromMapPortEntry (pPortEntry, pAttrEntry);
                MrpMapDsCheckAndDeleteMapEntry (pPortEntry->pMapEntry);

                return OSIX_TRUE;
            }
        }
    }
    return OSIX_FALSE;
}

/***********************************************************************
 * FUNCTION NAME    : MrpAttrIsAttrAdminRegCtrlFixed            
 *                                                                      
 * DESCRIPTION      : This function checks whether this attribute is     
 *                    registered in any other port with admin reg. control 
 *                    as MRP_REG_FIXED. This function will be called to 
 *                    decide, if leave message has to be sent for this 
 *                    attribute on all ports.
 *
 * INPUT(s)         : pContextInfo - Pointer to the Context info structure
 *                    pAppEntry    - Pointer to the Application entry
 *                    pMapEntry    - Pointer to the MAP entry
 *                    u2AttrIndex  - Attribute Index
 *                    u2Port       - Port whose STAP state changed
 *                                                                           
 * OUTPUT(s)        : None.                                            
 *                                                                           
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                                      
 ************************************************************************/
INT4
MrpAttrIsAttrAdminRegCtrlFixed (tMrpContextInfo * pContextInfo,
                                tMrpAppEntry * pAppEntry,
                                tMrpMapEntry * pMapEntry,
                                UINT2 u2AttrIndex, UINT2 u2Port)
{
    UINT1              *pMapPortList = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT2               u2Index = 0;
    UINT1               u1Result = OSIX_FALSE;

    pMapPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pMapPortList == NULL)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpAttrIsAttrAdminRegCtrlFixed: Error in allocating memory "
                     "for pMapPortList\r\n");
        return OSIX_FALSE;
    }
    MEMSET (pMapPortList, 0, sizeof (tLocalPortList));

    MrpMapGetPortList (pContextInfo->u4ContextId, pAppEntry->u1AppId,
                       pMapEntry->u2MapId, pMapPortList);

    /* Need to search in all the Ports other than 'u2Port'.
     * Hence clearing the bit corresponding to 'u2Port'. */

    OSIX_BITLIST_RESET_BIT (pMapPortList, u2Port, sizeof (tLocalPortList));

    for (u2Index = 1; u2Index <= MRP_MAX_PORTS_PER_CONTEXT; u2Index++)
    {
        OSIX_BITLIST_IS_BIT_SET (pMapPortList, u2Index, sizeof (tLocalPortList),
                                 u1Result);
        if (u1Result == OSIX_FALSE)
        {
            continue;
        }

        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Index);

        if ((pMapPortEntry == NULL) ||
            (pMapPortEntry->u1PortState == AST_PORT_STATE_DISCARDING))
        {
            continue;
        }

        if (MRP_GET_ATTR_REG_ADMIN_CTRL
            (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_REG_FIXED)
        {
            UtilPlstReleaseLocalPortList (pMapPortList);
            return OSIX_TRUE;
        }
    }

    UtilPlstReleaseLocalPortList (pMapPortList);
    return OSIX_FALSE;
}

/***********************************************************************
 * FUNCTION NAME    : MrpAttrGetRegAdminCtrlFrmAttrVal        
 *                                                                      
 * DESCRIPTION      : This function used to get Registrar Admin Ctrl Value 
 *                    for the Given Attr Value on the Given Port. 
 *
 * INPUT(s)         : u2PortId      - Port Id
 *                    pAttr         -Attr Value , Length,Type Info Structure.
 *                    pMapEntry     - MAP Entry for that Port is belongs to.
 *                    pu2RegAdminCtrl - Reg Admin Ctrl Value.
 *                                                                           
 * OUTPUT(s)        : None.                                            
 *                                                                           
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                      
 ************************************************************************/
INT4
MrpAttrGetRegAdminCtrlFrmAttrVal (UINT2 u2PortId, tMrpAttr * pAttr,
                                  tMrpMapEntry * pMrpMapEntry,
                                  UINT1 *pu1RegAdminCtrl)
{
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    tMrpAttrEntry      *pMrpAttrEntry = NULL;
    UINT2               u2AttrIndex = 0;

    pMrpAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMrpMapEntry);

    if (pMrpAttrEntry == NULL)
    {
        MRP_TRC ((pMrpMapEntry->pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                  "MrpAttrGetRegAdminCtrlFrmAttrVal: "
                  "Getting Attr Entry Failed.\r\n "));
        return OSIX_FAILURE;
    }

    pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, u2PortId);

    if (pMrpMapPortEntry == NULL)
    {
        MRP_TRC ((pMrpMapEntry->pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                  "MrpAttrGetRegAdminCtrlFrmAttrVal: "
                  "Getting MAP Port Entry Failed.\r\n "));
        return OSIX_FAILURE;
    }

    u2AttrIndex = pMrpAttrEntry->u2AttrIndex;

    *pu1RegAdminCtrl = MRP_GET_ATTR_REG_ADMIN_CTRL
        (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex]);

    return OSIX_SUCCESS;

}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpAttrValidateAttrRegistration
 *                                                                          
 *    DESCRIPTION      : This function creates the global port based RB Tree.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                                          
 ****************************************************************************/
INT4
MrpAttrValidateAttrRegistration (tMrpAttr * pAttr, tMrpMapEntry * pMapEntry,
                                 UINT2 u2Port)
{
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT2               u2AttrIndex = 0;

    pAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMapEntry);

    if ((NULL != pAttrEntry) && (0 != pAttrEntry->u2StaticMemPortCnt))
    {
        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

        if (NULL != pMapPortEntry)
        {
            u2AttrIndex = pAttrEntry->u2AttrIndex;

            if (MRP_GET_ATTR_REG_ADMIN_CTRL
                (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_REG_NORMAL)
            {
                return OSIX_TRUE;
            }
        }
    }
    return OSIX_FALSE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpattr.c                      */
/*-----------------------------------------------------------------------*/
