/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpmvrp.c,v 1.18 2013/06/25 12:08:38 siva Exp $
 *
 * Description: This file contains MVRP specific routines.
 ******************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpInit
 *                                                                          
 *    DESCRIPTION      : This function intializes the MVRP application
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMvrpInit (tMrpContextInfo * pMrpContextInfo)
{
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT2               u2Port = 0;

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

        if (pMrpPortEntry != NULL)
        {
            MrpMvrpSetPortMvrpStatus (pMrpPortEntry);
            pMrpPortEntry->u1RestrictedVlanRegCtrl = MRP_DISABLED;
        }
    }

    MrpMvrpEnable (pMrpContextInfo);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpEnable
 *                                                                          
 *    DESCRIPTION      : This function enables the MVRP application
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMvrpEnable (tMrpContextInfo * pMrpContextInfo)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpHwInfo          MrpHwInfo;

    MEMSET (&MrpHwInfo, 0, sizeof (tMrpHwInfo));

    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpEnable: MRP is not started. Failed. \n"));

        return OSIX_FAILURE;
    }

    if (MrpMvrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_TRUE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpEnable: Mvrp Already enabled. \n"));
        return OSIX_SUCCESS;
    }

    pMrpContextInfo->u1MvrpAdminStatus = MRP_ENABLED;

    /* Configurations are restored before GO_ACTIVE/GO_STANDBY event is
     * received by protocol. Hence if MVRP enable is called before GO_ACTIVE/
     * GO_STANDBY event is received, then store that in u1MvrpAdminStatus. In
     * MrpRedMakeNodeActiveFromIdle/MrpRedMakeNodeStandbyFromIdle based on the
     * value of u1MvrpAdminStatus, enable/disable MVRP.
     */

    if (MRP_RED_NODE_STATUS () == RM_INIT)
    {
        return OSIX_SUCCESS;
    }

#ifdef NPAPI_WANTED
    /* For accessing Other module's data struture From MRP module,
     * we should decrement the MRP Context Id by 1.*/

    if (MRP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
    {
        MrpHwInfo.u4ContextId = pMrpContextInfo->u4ContextId - 1;
        MrpHwInfo.u4Status = MRP_ENABLED;

        if (FsMiMrpHwSetMvrpStatus (&MrpHwInfo) != FNP_SUCCESS)
        {
            MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                      "MrpMvrpEnable: NP Programming Failed. \n"));
            return OSIX_FAILURE;
        }
    }
#endif

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MVRP_APP_ID);

    if (MrpAppInitMrpAppEntry (pMrpContextInfo, pMrpAppEntry,
                               MRP_MVRP_APP_ID) == OSIX_FAILURE)
    {
        MrpAppDeInitMrpAppEntry (pMrpAppEntry);
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpEnable: Initalizaing MRP App entry Failed. \n"));
        return OSIX_FAILURE;
    }

    /* Enabling MVRP on the ports present in the context */
    if (MrpAppAddAndEnablePortsInAppl (pMrpContextInfo,
                                       pMrpAppEntry) == OSIX_FAILURE)
    {
        MrpAppDeEnrolMrpApplication (pMrpContextInfo, MRP_MVRP_APP_ID);
        MrpAppDeInitMrpAppEntry (pMrpAppEntry);
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC |
                                    MRP_CRITICAL_TRC),
                  "MrpMvrpEnable: Enabling MVRP on existing ports Failed. \n"));

        return OSIX_FAILURE;
    }

    gMrpGlobalInfo.au1MvrpStatus[pMrpContextInfo->u4ContextId] = MRP_ENABLED;

    /* Static Vlan information might be present in VLAN module, before MVRP 
     * is enabled. So indicate VLAN module to propagate the VLAN information. */
    MrpPortVlanIndMvrpEnable (pMrpContextInfo->u4ContextId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpDisable
 *                                                                          
 *    DESCRIPTION      : This function disables the MVRP application
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMvrpDisable (tMrpContextInfo * pMrpContextInfo)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpHwInfo          MrpHwInfo;
    UINT2               u2Port = 0;
    UINT2               u2MapId = 0;

    MEMSET (&MrpHwInfo, 0, sizeof (tMrpHwInfo));

    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {

        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpDisable: MRP is not started. \n"));

        return OSIX_SUCCESS;
    }

    if (MrpMvrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {

        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpDisable: Mvrp Already disabled. \n"));

        return OSIX_SUCCESS;
    }

#ifdef NPAPI_WANTED

    if (MRP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
    {
        /* For accessing Other module's data struture From MRP module,
         * we should decrement the MRP Context Id by 1.*/
        MrpHwInfo.u4ContextId = pMrpContextInfo->u4ContextId - 1;
        MrpHwInfo.u4Status = MRP_DISABLED;

        if (FsMiMrpHwSetMvrpStatus (&MrpHwInfo) != FNP_SUCCESS)
        {

            MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                      "MrpMvrpDisable: NP Programming Failed. \n"));

            return OSIX_FAILURE;
        }
    }
#endif

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MVRP_APP_ID);

    MrpPortVlanDelAllDynamicVlanInfo (pMrpContextInfo->u4ContextId,
                                      VLAN_INVALID_PORT);

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pMrpAppEntry, u2Port);

        if (pMrpAppPortEntry != NULL)
        {
            MrpAppDelPortFromAppl (pMrpContextInfo, MRP_MVRP_APP_ID, u2Port);
        }
    }

    for (u2MapId = 0; u2MapId < pMrpAppEntry->u2MapArraySize; u2MapId++)
    {
        pMrpMapEntry = pMrpAppEntry->ppMapTable[u2MapId];

        if (pMrpMapEntry == NULL)
        {
            continue;
        }

        MrpMapDSDeleteMrpMapEntry (pMrpMapEntry);
    }

    MrpAppDeInitMrpAppEntry (pMrpAppEntry);

    gMrpGlobalInfo.au1MvrpStatus[pMrpContextInfo->u4ContextId] = MRP_DISABLED;
    MrpPortVlanIndMvrpDisable (pMrpContextInfo->u4ContextId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpAddPort
 *                                                                          
 *    DESCRIPTION      : This function adds a port to the MVRP application
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                       u2Port          - Local Port Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMvrpAddPort (tMrpContextInfo * pMrpContextInfo, UINT2 u2Port)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MrpMvrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpAddPort: " "Mvrp is disabled. \n"));
        return OSIX_SUCCESS;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MVRP_APP_ID);
    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

    if (pMrpPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpAddPort: " "Mrp port entry not found. \n"));
        return OSIX_FAILURE;
    }

    if (MrpAppAddPortToAppl (pMrpAppEntry, u2Port) == OSIX_FAILURE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpAddPort: Adding port %d to Application "
                  "table failed \n", u2Port));
        return OSIX_FAILURE;
    }

    if (pMrpPortEntry->u1PortMvrpStatus == MRP_ENABLED)
    {
        /* Static Vlan information might be present in VLAN module, before MVRP 
         * is enabled on this port. So indicate VLAN module to propagate the 
         * VLAN information. */
        MrpPortVlanIndPortMvrpEnable (pMrpContextInfo->u4ContextId, u2Port);
        if (MrpAppEnableApplOnPort (pMrpContextInfo, pMrpAppEntry,
                                    u2Port) == OSIX_FAILURE)
        {
            MrpAppDelPortFromAppl (pMrpContextInfo,
                                   pMrpAppEntry->u1AppId, u2Port);
            MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                      "MrpMvrpAddPort: Enabling the application on "
                      "port %d failed \n", u2Port));
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpDelPort
 *                                                                          
 *    DESCRIPTION      : This function deletes a port from MVRP application.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                       u2Port          - Local Port Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMvrpDelPort (tMrpContextInfo * pMrpContextInfo, UINT2 u2Port)
{
    if (MrpMvrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpDelPort: Mvrp is disabled. \n"));
        return OSIX_SUCCESS;
    }

    MrpPortVlanDelAllDynamicVlanInfo (pMrpContextInfo->u4ContextId, u2Port);
    MrpAppDelPortFromAppl (pMrpContextInfo, MRP_MVRP_APP_ID, u2Port);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpEnablePort
 *                                                                          
 *    DESCRIPTION      : This function enables MVRP application on the port.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                       u2Port          - Local Port Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMvrpEnablePort (tMrpContextInfo * pMrpContextInfo, UINT2 u2Port)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpEnablePort: MRP is shutdown. \n"));
        return OSIX_FAILURE;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MVRP_APP_ID);
    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

    if (pMrpPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpEnablePort: Port %d not created. \n", u2Port));
        return OSIX_FAILURE;
    }

    if (MrpMvrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        pMrpPortEntry->u1PortMvrpStatus = MRP_ENABLED;

        /* Update VLAN port table with port MVRP status because port type 
         * should not be set as access port on MVRP enabled port. */
        MrpPortSetProtoEnabledStatOnPort
            (pMrpContextInfo->u4ContextId, u2Port, L2_PROTO_MVRP, OSIX_ENABLED);
        return OSIX_SUCCESS;
    }

    if (pMrpPortEntry->u1PortMvrpStatus == MRP_ENABLED)
    {
        return OSIX_SUCCESS;
    }

    if (pMrpPortEntry->u1OperStatus == MRP_OPER_UP)
    {
        /* Static Vlan information might be present in VLAN module, before MVRP 
         * is enabled on this port. So indicate VLAN module to propagate the 
         * VLAN information. */
        MrpPortVlanIndPortMvrpEnable (pMrpContextInfo->u4ContextId, u2Port);
        if (MrpAppEnableApplOnPort (pMrpContextInfo, pMrpAppEntry,
                                    u2Port) == OSIX_FAILURE)
        {
            MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                      "MrpMvrpEnable: Enabling the application on "
                      "port %d failed \n", u2Port));
            return OSIX_FAILURE;
        }
    }

    pMrpPortEntry->u1PortMvrpStatus = MRP_ENABLED;

    MrpPortSetProtoEnabledStatOnPort (pMrpContextInfo->u4ContextId,
                                      u2Port, L2_PROTO_MVRP, OSIX_ENABLED);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpDisablePort
 *                                                                          
 *    DESCRIPTION      : This function disables MVRP application on the port.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                       u2Port          - Local Port Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMvrpDisablePort (tMrpContextInfo * pMrpContextInfo, UINT2 u2Port)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpDisablePort: MRP is shutdown. \n"));
        return OSIX_FAILURE;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MVRP_APP_ID);
    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

    if (pMrpPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                  "MrpMvrpEnablePort: Port %d not created. \n", u2Port));
        return OSIX_FAILURE;
    }

    if (MrpMvrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        pMrpPortEntry->u1PortMvrpStatus = MRP_DISABLED;

        /* Update VLAN port table with port MVRP status because port type 
         * should not be set as access port on MVRP enabled port. */
        MrpPortSetProtoEnabledStatOnPort
            (pMrpContextInfo->u4ContextId, u2Port, L2_PROTO_MVRP,
             OSIX_DISABLED);
        return OSIX_SUCCESS;
    }

    if (pMrpPortEntry->u1PortMvrpStatus == MRP_DISABLED)
    {
        return OSIX_SUCCESS;
    }

    if (pMrpPortEntry->u1OperStatus == MRP_OPER_UP)
    {
        MrpPortVlanDelAllDynamicVlanInfo (pMrpContextInfo->u4ContextId, u2Port);

        if (MrpAppDisableApplOnPort (pMrpContextInfo, pMrpAppEntry,
                                     u2Port) == OSIX_FAILURE)
        {
            MRP_TRC ((pMrpContextInfo, (MRP_MVRP_TRC | INIT_SHUT_TRC),
                      "MrpMvrpEnable: Enabling the application on "
                      "port %d failed \n", u2Port));
            return OSIX_FAILURE;
        }
    }

    pMrpPortEntry->u1PortMvrpStatus = MRP_DISABLED;

    MrpPortSetProtoEnabledStatOnPort (pMrpContextInfo->u4ContextId,
                                      u2Port, L2_PROTO_MVRP, OSIX_DISABLED);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpSetPortMvrpStatus
 *                                                                          
 *    DESCRIPTION      : This function sets the MVRP status of the port by 
 *                       checking the bridge mode and bridge port-type.
 *
 *    INPUT            : pMrpPortEntry - Pointer to the tMrpPortEntry 
 *                                       structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMvrpSetPortMvrpStatus (tMrpPortEntry * pMrpPortEntry)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;
    UINT1               u1TunnelStatus = VLAN_TUNNEL_PROTOCOL_INVALID;
    UINT1               u1PortType = VLAN_DEFAULT_PORT;
    UINT1               u1Status = L2IWF_DISABLED;
    UINT1               u1PortMvrpStatus = 0;

    pMrpContextInfo = pMrpPortEntry->pMrpContextInfo;

    if (MRP_IS_802_1AD_1AH_BRIDGE (pMrpContextInfo) == OSIX_TRUE)
    {
        /* In case of 1ad & 1ah bridge, MVRP cannot be enabled on 
         * Customer ports. */
        if (((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry)) == OSIX_TRUE) ||
            ((MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry)) == OSIX_TRUE))
        {
            pMrpPortEntry->u1PortMvrpStatus = MRP_DISABLED;
        }
        else
        {
            pMrpPortEntry->u1PortMvrpStatus = MRP_ENABLED;
        }
    }
    else
    {
        pMrpPortEntry->u1PortMvrpStatus = MRP_ENABLED;
    }

    /* In case of customer bridge mode, If the Protocol tunnel
     * status is tunnel/discard, MVRP should be disabled on that port */
    if (pMrpContextInfo->u4BridgeMode == MRP_CUSTOMER_BRIDGE_MODE)
    {
        MrpPortGetProtoTunelStatusOnPort (pMrpPortEntry->u4IfIndex,
                                          L2_PROTO_MVRP, &u1TunnelStatus);

        if (u1TunnelStatus != VLAN_TUNNEL_PROTOCOL_PEER)
        {
            pMrpPortEntry->u1PortMvrpStatus = MRP_DISABLED;
        }
    }

    MrpPortL2IwfGetVlanPortType (pMrpPortEntry->u4IfIndex, &u1PortType);

    /* In Access ports, MVRP should not be enabled on the port */
    if (u1PortType == VLAN_ACCESS_PORT)
    {
        pMrpPortEntry->u1PortMvrpStatus = MRP_DISABLED;
    }

    L2IwfGetSispPortCtrlStatus (pMrpPortEntry->u4IfIndex, &u1Status);

    if (u1Status == L2IWF_ENABLED)
    {
        pMrpPortEntry->u1PortMvrpStatus = MRP_DISABLED;
    }

    if (pMrpPortEntry->u1PortMvrpStatus == MRP_ENABLED)
    {
        u1PortMvrpStatus = OSIX_ENABLED;
    }
    else
    {
        u1PortMvrpStatus = OSIX_DISABLED;
    }

    u4ContextId = pMrpPortEntry->pMrpContextInfo->u4ContextId;

    MrpPortSetProtoEnabledStatOnPort (u4ContextId,
                                      pMrpPortEntry->u2LocalPortId,
                                      L2_PROTO_MVRP, u1PortMvrpStatus);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpAttrIndFn
 *                                                                          
 *    DESCRIPTION      : Registered function for Indicating the registration/ 
 *                       de-registration to the Higher layer application.
 *
 *    INPUT            : pMapEntry  - Pointer to the MAP entry
 *                       Attr       - Attribute Information
 *                       u2Port     - Port number of the incoming port
 *                       u1IndType  - MRP_HL_IND_NEW/MRP_HL_IND_JOIN/
 *                                    MRP_HL_IND_LEAVE
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMvrpAttrIndFn (tMrpMapEntry * pMapEntry, tMrpAttr Attr, UINT2 u2Port,
                  UINT1 u1IndType)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpTrapInfo        TrapInfo;
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpRedMsgInfo      SyncUpMsgInfo;
    tVlanId             VlanId = 0;
    INT4                i4RetVal = VLAN_SUCCESS;
    UINT1               u1RedSyncMsgType = 0;

    MEMSET (&TrapInfo, 0, sizeof (tMrpTrapInfo));
    MEMCPY (&VlanId, Attr.au1AttrVal, MRP_VLAN_ID_LEN);
    VlanId = (tVlanId) OSIX_NTOHS (VlanId);

    pContextInfo = pMapEntry->pMrpAppEntry->pMrpContextInfo;

    switch (u1IndType)
    {
        case MRP_HL_IND_JOIN:
            /* Intentional fall through */
        case MRP_HL_IND_NEW:

            MRP_TRC ((pContextInfo, (MRP_REG_SEM_TRC | MRP_PDU_TRC |
                                     MRP_MVRP_TRC), "MrpMvrpAttrIndFn: Received"
                      " Join Or New Indication on Port %d for VLAN ID %d\n",
                      u2Port, VlanId));

            i4RetVal = MrpPortVlanUpdateDynamicVlanInfo
                (pContextInfo->u4ContextId, VlanId, u2Port, VLAN_ADD);

            if (VLAN_SUCCESS == i4RetVal)
            {
                MrpMapHandleMapUpdateMapPorts (pContextInfo->u4ContextId,
                                               u2Port, VlanId, VLAN_ADD);

            }
            else
            {
                /* Send the trap as the VLAN Registration failed */
                TrapInfo.pAttr = &Attr;
                TrapInfo.u4ContextId = pContextInfo->u4ContextId;
                TrapInfo.u2PortId = u2Port;
                TrapInfo.u1ReasonType = MRP_LACK_OF_RESOURCE;
                MrpTrapNotifyVlanOrMacRegFails ((VOID *) &TrapInfo);
                pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);
                MRP_CHK_NULL_PTR_RET (pPortEntry, OSIX_FAILURE);
                pPortEntry->u4MvrpRegFailCnt++;

                MRP_TRC ((pContextInfo, (ALL_FAILURE_TRC | MRP_MVRP_TRC),
                          "MrpMvrpAttrIndFn: Failed to update"
                          " dynamic vlan information for VLAN ID %d and "
                          "Port %d\n", VlanId, u2Port));

                return OSIX_FAILURE;
            }
            u1RedSyncMsgType = MRP_RED_VLAN_ADD_MSG;
            break;

        case MRP_HL_IND_LEAVE:

            MRP_TRC ((pContextInfo, (MRP_REG_SEM_TRC | MRP_MVRP_TRC |
                                     MRP_PDU_TRC), "MrpMvrpAttrIndFn: Received"
                      " Lv Indication on Port %d for VLAN ID %d\n",
                      u2Port, VlanId));

            MrpMapHandleMapUpdateMapPorts (pContextInfo->u4ContextId, u2Port,
                                           VlanId, VLAN_DELETE);
            i4RetVal = MrpPortVlanUpdateDynamicVlanInfo
                (pContextInfo->u4ContextId, VlanId, u2Port, VLAN_DELETE);

            if (VLAN_FAILURE == i4RetVal)
            {

                MRP_TRC ((pContextInfo, (ALL_FAILURE_TRC | MRP_MVRP_TRC),
                          "MrpMvrpAttrIndFn: Failed to remove "
                          "dynamic vlan information for VLAN ID %d and "
                          "Port %d\n", VlanId, u2Port));

                return OSIX_FAILURE;
            }

            u1RedSyncMsgType = MRP_RED_VLAN_DEL_MSG;

            break;

        default:
            break;
    }

    MEMSET (&SyncUpMsgInfo, 0, sizeof (tMrpRedMsgInfo));
    SyncUpMsgInfo.u4ContextId = pContextInfo->u4ContextId;
    SyncUpMsgInfo.u2LocalPortId = u2Port;
    SyncUpMsgInfo.VlanId = VlanId;

    MrpRedSendSyncUpMessages (u1RedSyncMsgType, &SyncUpMsgInfo);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpIsEnabled
 *                                                                          
 *    DESCRIPTION      : This function returns the status of the MVRP
 *                       application.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                                          
 ****************************************************************************/
INT4
MrpMvrpIsEnabled (UINT4 u4ContextId)
{
    if ((u4ContextId != 0) && (u4ContextId < MRP_SIZING_CONTEXT_COUNT))
    {
        return ((gMrpGlobalInfo.au1MvrpStatus[u4ContextId] == MRP_ENABLED)
                ? OSIX_TRUE : OSIX_FALSE);
    }
    return OSIX_FALSE;
}

/****************************************************************************
 *
 *  FUNCTION NAME   : MrpMvrpTypeValidateFn                       
 *
 *    DESCRIPTION      : This function is called by MRP module to check whether
 *                       the attribute type is a valid MVRP attribute type.
 *
 *    INPUT            : u1AttrType - Attribute type
 *
 *    OUTPUT           : None
 *
 *  RETURNS         : OSIX_TRUE / OSIX_FALSE                   
 *
 ****************************************************************************/
INT4
MrpMvrpTypeValidateFn (UINT1 u1AttrType)
{
    if (u1AttrType == MRP_VID_ATTR_TYPE)
    {
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

/****************************************************************************
 *
 *  FUNCTION NAME   : MrpMvrpLenValidateFn                       
 *
 *    DESCRIPTION      : This function is called by MRP module to check whether
 *                       the attribute length is a valid MVRP attribute length.
 *
 *    INPUT            : u1AttrType - Attribute type
*                   u1AttrLen  - Attribute Length
 *
 *    OUTPUT           : None
 *                                                                          
 *  RETURNS         : OSIX_TRUE / OSIX_FALSE                  
 *
 ****************************************************************************/
INT4
MrpMvrpLenValidateFn (UINT1 u1AttrType, UINT1 u1Attrlen)
{
    if ((u1AttrType == MRP_VID_ATTR_TYPE) && (u1Attrlen == MRP_VLAN_ID_LEN))
    {
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

/****************************************************************************
 *
 *  FUNCTION NAME   : MrpMvrpWildCardAttrRegValidateFn          
 *
 *    DESCRIPTION      : This function calls a function in VLAN module and 
 *                       checks if the attribute can be registered.
 *                                                             
 *    INPUT            : u4ContextId - Context Identifier
 *                       Attr        - Structure containing attribute type, 
 *                                     length, value.
 *                       u2Port      - Port Number on which Registration 
 *                                     NORMAL for attribute contained in Attr 
 *                                     to be checked
 *                       u2MapId     - MAP Identifier
 *                                                             
 *    OUTPUT           : None
 *                                                             
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                             
 ****************************************************************************/
INT4
MrpMvrpWildCardAttrRegValidateFn (UINT4 u4ContextId, tMrpAttr Attr,
                                  UINT2 u2Port, UINT2 u2MapId)
{
    /* In case of MVRP this will be a stub function */
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (Attr);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u2MapId);

    return OSIX_TRUE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpHandleJoinTmrExpiry
 *                                                                          
 *    DESCRIPTION      : This function handles the join timer expiry for 
 *                       MVRP application.
 *
 *    INPUT            : pMrpContextInfo  - Pointer to the Context info 
 *                                          structure.
 *                       pMrpAppPortEntry - Pointer to the Application Port 
 *                                          entry.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMvrpHandleJoinTmrExpiry (UINT4 u4ContextId,
                            tMrpAppPortEntry * pMrpAppPortEntry)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pPortEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2Offset = MRP_PDU_HDR_SIZE;
    UINT2               u2EndMark = MRP_END_MARK_VALUE;
    UINT1              *pu1Buf = NULL;
    UINT1               u1Event = MRP_APP_TX;
    UINT1               u1BuffFull = OSIX_FALSE;

    pMrpContextInfo = gMrpGlobalInfo.apContextInfo[u4ContextId];
    u2Port = pMrpAppPortEntry->pMrpPortEntry->u2LocalPortId;
    pPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

    if (pPortEntry == NULL)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MVRP_TRC),
                  "MrpMvrpHandleJoinTmrExpiry: Port Entry "
                  "not found for port %d.\n", u2Port));
        return;
    }

    if (pMrpAppPortEntry->u1LeaveAllSemState == MRP_ACTIVE)
    {
        u1Event = MRP_APP_TX_LA;
        pMrpAppPortEntry->u1LeaveAllSemState = MRP_PASSIVE;
    }

    MEMSET (gMrpGlobalInfo.pu1PduBuf, 0, MRP_MAX_PDU_LEN);

    /* Move the buffer pointer to point to the start of Message structure */
    pu1Buf = gMrpGlobalInfo.pu1PduBuf + u2Offset;

    /* Fill the attribute type */
    MRP_LBUF_PUT_1_BYTE (pu1Buf, (UINT1) MRP_VID_ATTR_TYPE, u2Offset);
    /* Fill the attribute length */
    MRP_LBUF_PUT_1_BYTE (pu1Buf, (UINT1) MRP_VLAN_ID_LEN, u2Offset);

    /* Form attribute list */
    MrpMvrpFormAttrList (&pu1Buf, pPortEntry, u1Event, &u2Offset, &u1BuffFull);

    if (u1BuffFull == MRP_NO_ATTR_FILLED)
    {
        /* No attribute is filled in the PDU by MrpMvrpFormAttrList
         * */
        return;
    }

    /* Add EndMark to denote the end of the MRPDU */
    MRP_LBUF_PUT_2_BYTES (pu1Buf, u2EndMark, u2Offset);

    MrpTxAddPduHdrAndTxPdu (u4ContextId, MRP_MVRP_APP_ID, u2Offset, u2Port, 0);

    if (u1Event == MRP_APP_TX_LA)
    {
        /* Now only we have transmitted a PDU.
         * Increment statistics 
         * */
        MrpUtilIncrTxStats (pPortEntry, MRP_MVRP_APP_ID, MRP_TX_LEAVE_ALL);
    }

    if ((u1BuffFull == OSIX_TRUE) && (u1Event == MRP_APP_TX_LA))
    {
        /* u1BuffFull is set to OSIX_TRUE only when no space is available in 
         * the PDU. Hence apply txLAF! event to the remaining Attribute
         * values
         */
        MrpMvrpApplyTxLAFOnRemainingAttr (pPortEntry,
                                          pPortEntry->pNextMvrpTxNode);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpFormAttrList
 *                                                                          
 *    DESCRIPTION      : This function forms the MVRP Attribute List.    
 *
 *    INPUT            : ppu1Buf     - Pointer to the PDU buffer. Points to the
 *                                     start of the Attribute List
 *                       pPortEntry  - Pointer to the Port entry  
 *                       u1Event     - MRP_APP_TX / MRP_APP_TX_LA
 *                       pu2Offset   - Location of PDU buffer pointer
 *                       pu1BuffFull - Set to OSIX_TRUE if there is no space
 *                                     in the buffer for the remaining 
 *                                     attribute values
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
VOID
MrpMvrpFormAttrList (UINT1 **ppu1AttrList, tMrpPortEntry * pPortEntry,
                     UINT1 u1Event, UINT2 *pu2Offset, UINT1 *pu1BuffFull)
{
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpVectAttrInfo    VectAttrInfo;
    UINT2               u2VectorHdr = 0;
    UINT2               u2NumOfValues = 0;
    UINT2               u2VectorLen = 0;
    UINT2               u2EndMark = MRP_END_MARK_VALUE;
    UINT2               u2AttrIndex = 0;
    UINT2               u2MapId = 0;
    UINT1              *pu1VectAttr = NULL;
    UINT1               u1AddVectHdr = OSIX_FALSE;
    UINT1               u1IsFirstVectAttrFilled = OSIX_FALSE;
    UINT1               u1RegSEMStateChg = OSIX_FALSE;

    /* pu1VectAttr is always points to the start of the Vector Attribute */
    pu1VectAttr = *ppu1AttrList;
    MEMSET (&VectAttrInfo, 0, sizeof (tMrpVectAttrInfo));

    if (u1Event == MRP_APP_TX_LA)
    {
        u2VectorHdr = MRP_PDU_LV_ALL_VALUE;
    }

    pAttrEntry = MrpMvrpGetFirstAttrForTx (pPortEntry);

    if (pAttrEntry == NULL)
    {
        /* If the value of Applicant Admin Ctrl is Non participant means,
         * then transmit LeaveAll Pdu only through that Port.*/

        /* On blocked port, Lv all message needs to be tranmitted */
        if (u1Event == MRP_APP_TX_LA)
        {
            MrpTxAddVectorAttrToPdu (ppu1AttrList, u2VectorHdr,
                                     &pAttrEntry->Attr, NULL,
                                     u2NumOfValues, pu2Offset, OSIX_FALSE);
        }
        return;
    }

    /* Increment the pu1AttrList by MRP_VECTOR_HDR_SIZE as Vector Header is 
     * filled once the end of Vector for the Vector Attribute is reached.
     */

    *ppu1AttrList += MRP_VECTOR_HDR_SIZE;

    *pu1BuffFull = MRP_NO_ATTR_FILLED;

    for (; pAttrEntry != NULL; ((pAttrEntry = MrpMvrpGetNextAttrForTx
                                 (pPortEntry, u2AttrIndex, OSIX_FALSE))))
    {
        u2AttrIndex = pAttrEntry->u2AttrIndex;
        u2MapId = pAttrEntry->pMapEntry->u2MapId;
        u1AddVectHdr = OSIX_FALSE;
        if (MrpTxIsSpaceInMVRPPkt (*pu2Offset, u2NumOfValues,
                                   MRP_VLAN_ID_LEN) == OSIX_FALSE)
        {
            *pu1BuffFull = OSIX_TRUE;
            break;
        }

        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pAttrEntry->pMapEntry,
                                                pPortEntry->u2LocalPortId);
        if (pMapPortEntry == NULL)
        {
            return;
        }

        if ((pPortEntry->au1ApplAdminCtrl[MRP_MVRP_APP_ID]
             [MRP_VID_ATTR_TYPE]) == MRP_NON_PARTICIPANT)
        {
            /* Non-participant: Tx alone can be blocked. This means that
             * rLA event will be given to Registrar SEM.
             * */
            if ((u1Event == MRP_APP_TX_LA) &&
                (MRP_GET_ATTR_REG_ADMIN_CTRL
                 (pMapPortEntry->pu1AttrInfoList[u2AttrIndex])
                 == MRP_REG_NORMAL))
            {
                MrpSmRegistrarSem (pMapPortEntry,
                                   pAttrEntry->u2AttrIndex,
                                   MRP_REG_RX_LEAVE_ALL, &u1RegSEMStateChg);
                /* The Function MrpSmRegistrarSem, sometime deletes the 
                 * MAP entry itself. So Check for the MAP Entry. */

                pMapEntry =
                    MRP_GET_MAP_ENTRY ((&(pPortEntry->pMrpContextInfo->
                                          aMrpAppTable[MRP_MVRP_APP_ID])),
                                       u2MapId);

                if (pMapEntry == NULL)
                {
                    pAttrEntry = NULL;
                    break;
                }
            }

            continue;
        }

        VectAttrInfo.pMapPortEntry = pMapPortEntry;
        VectAttrInfo.pAttr = &(pAttrEntry->Attr);
        VectAttrInfo.u2AttrIndex = pAttrEntry->u2AttrIndex;
        VectAttrInfo.u1Event = u1Event;

        /* Form Vector Attribute */
        MrpMvrpFormVectAttr (ppu1AttrList, &VectAttrInfo, &u1AddVectHdr,
                             &u2NumOfValues);

        if (VectAttrInfo.u1IsAttrFilled == OSIX_FALSE)
        {
            *pu1BuffFull = MRP_NO_ATTR_FILLED;
        }
        else
        {
            *pu1BuffFull = OSIX_FALSE;
        }

        if (u1AddVectHdr == OSIX_TRUE)
        {
            /* End of Vector has reached for current Vector Attribute.
             * Hence add Vector Header */
            u2VectorHdr |= u2NumOfValues;
            MRP_LBUF_PUT_2_BYTES (pu1VectAttr, u2VectorHdr, *pu2Offset);

            u1AddVectHdr = OSIX_FALSE;
            u1IsFirstVectAttrFilled = OSIX_TRUE;
            u2VectorLen = (UINT2) MRP_GET_NUM_OF_VECTORS_FIELD (u2NumOfValues);
            /* Move the pointer to the start of the next Vector Attribute */
            pu1VectAttr += (MRP_VLAN_ID_LEN + u2VectorLen);
            *pu2Offset = (UINT2) (*pu2Offset + (MRP_VLAN_ID_LEN + u2VectorLen));
            if (VectAttrInfo.u1OptEncode == OSIX_FALSE)
            {
                /* Increment the value if first value has been filled for the 
                 * next vector attribute */
                u2NumOfValues = 1;
            }
            else
            {
                /* First value has not been filled for the next vector attribute.
                 * Hence move the pointer to start of the First value field
                 */
                *ppu1AttrList += (u2VectorLen + MRP_VECTOR_HDR_SIZE);
                u2NumOfValues = 0;
            }
            u2VectorHdr = 0;
        }
    }

    /* Check to determine if Vector Header need to be added */
    if ((*ppu1AttrList - pu1VectAttr) ==
        (MRP_VECTOR_HDR_SIZE + MRP_VLAN_ID_LEN))
    {
        if (u1IsFirstVectAttrFilled == OSIX_TRUE)
        {
            /* Leave All event needs to be encoded only in the first Vector
             * Attribute
             */
            u2VectorHdr = 0;
        }

        u2VectorHdr |= u2NumOfValues;
        MRP_LBUF_PUT_2_BYTES (pu1VectAttr, u2VectorHdr, *pu2Offset);
        u2VectorLen = (UINT2) MRP_GET_NUM_OF_VECTORS_FIELD (u2NumOfValues);
        *pu2Offset = (UINT2) (*pu2Offset + (MRP_VLAN_ID_LEN + u2VectorLen));
        pu1VectAttr += (MRP_VLAN_ID_LEN + u2VectorLen);
        /* ppu1AttrList points to the start of the vector field. Hence move the
         * pointer to the end of the vector
         */
        *ppu1AttrList += u2VectorLen;
    }

    /* Add EndMark to denote the end of Attribute List. */
    MRP_LBUF_PUT_2_BYTES (pu1VectAttr, u2EndMark, *pu2Offset);

    /* Update the NextMvrpTxNode, which indicates the next VLAN to be 
     * transmitted in the next opportunity.
     */
    pPortEntry->pNextMvrpTxNode = pAttrEntry;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpFormVectAttr
 *                                                                          
 *    DESCRIPTION      : This function forms the Vector. For the given VLAN ID
 *                       it gets the corresponding Attribute Event and encodes
 *                       it in the Vector.
 *
 *    INPUT            : pVectBuf         - Pointer to start of the Vector field 
 *                       pVectAttrInf     - Pointer to the  VectorAttrInfo
 *                       pu1AddVectHdr    - Set to OSIX_TRUE when end of the 
 *                                          Vector for this Vector Attribute is
 *                                          reached
 *                       pu2NumOfValues   - Number of Attribute values encoded 
 *                                          in this Vector Attribute
 *                       pu1IsFirstValSet - Set to OSIX_TRUE on determining
 *                                          the 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to the first attribute entry
 *                                                                          
 ****************************************************************************/
VOID
MrpMvrpFormVectAttr (UINT1 **ppVectBuf, tMrpVectAttrInfo * pVectAttrInfo,
                     UINT1 *pu1AddVectHdr, UINT2 *pu2NumOfValues)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    UINT2               u2CurrVal = 0;
    UINT2               u2TmpVal = 0;
    UINT2               u2Offset = 0;
    UINT2               u2VectorLen = 0;
    UINT2               u2AttrDiff = 0;
    UINT1               u1OptEncode = OSIX_FALSE;
    UINT1               u1AttrEvent = 0;
    UINT1               u1SetEvntForFirstVal = OSIX_FALSE;

    pMapPortEntry = pVectAttrInfo->pMapPortEntry;

    pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMapPortEntry,
                                                   pMapPortEntry->u2Port);
    if (pAppPortEntry == NULL)
    {
        return;
    }

    pPortEntry = pAppPortEntry->pMrpPortEntry;

    /* Get the Attribute event for this attribute value */
    MrpTxApplyTxEventOnAttr (pMapPortEntry, pVectAttrInfo->pAttr,
                             pVectAttrInfo->u2AttrIndex, pVectAttrInfo->u1Event,
                             &u1AttrEvent, &u1OptEncode);

    MEMCPY (&u2CurrVal, pVectAttrInfo->pAttr->au1AttrVal, MRP_VLAN_ID_LEN);
    u2CurrVal = OSIX_NTOHS (u2CurrVal);
    MrpUtilTransLocalVidFromRelayVid (pPortEntry->u4ContextId,
                                      pPortEntry->u2LocalPortId, &u2CurrVal);

    pVectAttrInfo->u1OptEncode = u1OptEncode;
    if (pVectAttrInfo->u1IsFirstValSet == OSIX_FALSE)
    {
        if (u1OptEncode == OSIX_FALSE)
        {
            /* This Attribute value needs to be stored as the First Value */
            u2TmpVal = u2CurrVal;
            u2TmpVal = (UINT2) OSIX_HTONS (u2TmpVal);

            MRP_LBUF_PUT_N_BYTES (*ppVectBuf, &u2TmpVal, MRP_VLAN_ID_LEN,
                                  u2Offset);

            pVectAttrInfo->u1IsFirstValSet = OSIX_TRUE;
            pVectAttrInfo->u1IsAttrFilled = OSIX_TRUE;

            /* Event for the First value needs to be set in the Vector. */
            u1SetEvntForFirstVal = OSIX_TRUE;
        }
        else
        {
            return;
        }
    }

    u2AttrDiff = (UINT2) (u2CurrVal - pVectAttrInfo->u2LastVlanId);
    /* Store the attribute event in the PDU if the attribute value is within 
     * the optimal encoding interval or if it is the first value. */
    if ((u2AttrDiff < MRP_OPTIMAL_ENCODE_INTERVAL) ||
        (u1SetEvntForFirstVal == OSIX_TRUE))
    {
        if (u1OptEncode == OSIX_TRUE)
        {
            if (pVectAttrInfo->u1Count < MRP_OPTIMAL_ENCODE_INTERVAL)
            {
                pVectAttrInfo->au1AttrEvent[pVectAttrInfo->u1Count++] =
                    u1AttrEvent;
            }
        }
        else
        {
            if (pVectAttrInfo->u1Count != 0)
            {
                MrpTxFillOptEvntsInVector (*ppVectBuf,
                                           pVectAttrInfo->au1AttrEvent,
                                           pVectAttrInfo->u1Count,
                                           pu2NumOfValues, pPortEntry,
                                           MRP_MVRP_APP_ID);
                MEMSET (pVectAttrInfo->au1AttrEvent, 0,
                        MRP_OPTIMAL_ENCODE_INTERVAL);
                pVectAttrInfo->u1Count = 0;
            }
            else if ((u1SetEvntForFirstVal == OSIX_FALSE) && (u2AttrDiff > 1))
            {
                /* Mt event needs to be filled for the intermediate values to 
                 * result in optimal encoding
                 */
                MrpTxFillMtEvntInVector (*ppVectBuf, pPortEntry, pu2NumOfValues,
                                         (UINT1) (u2AttrDiff - 1));
            }

            u1SetEvntForFirstVal = OSIX_FALSE;
            pVectAttrInfo->u2LastVlanId = u2CurrVal;
            (*pu2NumOfValues)++;
            MrpTxEncodeAttrEvent (*ppVectBuf, *pu2NumOfValues, u1AttrEvent);
            MrpUtilIncrTxStats (pPortEntry, MRP_MVRP_APP_ID, u1AttrEvent);
        }
    }
    else
    {
        /* Fill the event for this Attribute Value in the next Vector Attribute. 
         */
        *pu1AddVectHdr = OSIX_TRUE;
        if (u1OptEncode == OSIX_FALSE)
        {
            /* This Attribute value needs to be stored as the First Value in 
             * the next Vector Attribute */
            u2TmpVal = u2CurrVal;
            u2TmpVal = (UINT2) OSIX_HTONS (u2TmpVal);

            /* Length of the current Vector Attribute */
            u2VectorLen =
                (UINT2) MRP_GET_NUM_OF_VECTORS_FIELD (*pu2NumOfValues);

            /* Buffer pointer now points to the start of the Vector field of the
             * current Vector Attribute. Move the buffer pointer to point to the 
             * end of Vector Header of the next Vector Attribute.
             */
            *ppVectBuf += (MRP_VECTOR_HDR_SIZE + u2VectorLen);
            /* Store the first value of the next Vector Attribute */
            MRP_LBUF_PUT_N_BYTES (*ppVectBuf, &u2TmpVal, MRP_VLAN_ID_LEN,
                                  u2Offset);
            pVectAttrInfo->u2LastVlanId = u2CurrVal;
            pVectAttrInfo->u1IsFirstValSet = OSIX_TRUE;
            MrpTxEncodeAttrEvent (*ppVectBuf, 1, u1AttrEvent);
            MrpUtilIncrTxStats (pPortEntry, MRP_MVRP_APP_ID, u1AttrEvent);
        }
        else
        {
            MEMSET (pVectAttrInfo->au1AttrEvent, 0,
                    MRP_OPTIMAL_ENCODE_INTERVAL);
            pVectAttrInfo->u1Count = 0;
            pVectAttrInfo->u1IsFirstValSet = OSIX_FALSE;
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpGetFirstAttrForTx
 *                                                                          
 *    DESCRIPTION      : This function returns the first attribute that  
 *                       needs to be transmitted.
 *
 *    INPUT            : pPortEntry  - Pointer to the Port entry.  
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to the first attribute entry
 *                                                                          
 ****************************************************************************/
tMrpAttrEntry      *
MrpMvrpGetFirstAttrForTx (tMrpPortEntry * pPortEntry)
{

    if (pPortEntry->pNextMvrpTxNode == NULL)
    {
        return (MrpMvrpGetNextAttrForTx (pPortEntry, 0, OSIX_FALSE));
    }
    else
    {
        /* In the previous transmit opportunity, there is no room in the PDU 
         * for filling the entire attributes. So this time we have to start 
         * from the remaining attributes. */
        return (MrpMvrpGetNextAttrForTx
                (pPortEntry,
                 (UINT2) (pPortEntry->pNextMvrpTxNode->u2AttrIndex - 1),
                 OSIX_TRUE));
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpGetNextAttrForTx
 *                                                                          
 *    DESCRIPTION      : This function returns the next attribute that  
 *                       needs to be transmitted.
 *
 *    INPUT            : pPortEntry  - Pointer to the Port entry. 
 *                       u2AttrIndex - Current Attribute Index
 *                       u1Flag      - OSIX_TRUE/OSIX_FALSE
 *                                     If OSIX_TRUE means, the u2AttrIndex is 
 *                                     retrieved from pPortEntry->
 *                                     pNextMvrpTxNode.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to the next attribute entry
 *                                                                          
 ****************************************************************************/
tMrpAttrEntry      *
MrpMvrpGetNextAttrForTx (tMrpPortEntry * pPortEntry, UINT2 u2AttrIndex,
                         UINT1 u1Flag)
{
    tMrpAttrEntry      *pNextAttrEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    UINT2               u2NextAttrIndex = 0;
    UINT2               u2MapId = 0;

    pMrpContextInfo = pPortEntry->pMrpContextInfo;
    pMrpAppEntry = &(pMrpContextInfo->aMrpAppTable[MRP_MVRP_APP_ID]);

    do
    {
        pNextAttrEntry = NULL;

        u2NextAttrIndex = MrpUtilFindNextSetBitInBitList
            (pPortEntry->pu1MvrpAttrList,
             (UINT2) (VLAN_DEV_MAX_VLAN_ID / MRP_PORTS_PER_BYTE), u2AttrIndex);

        if (u2NextAttrIndex == 0)
        {
            /* No entry is present in the BitList. This can happen in two cases:
             * 1. When no VLAN is associated with this port.
             * 2. Traversing the bitlist has been started from middle (when 
             *    there is no space in the MRPDU). In this case when the end
             *    of bit list is reached, we need to start from the beginning.
             */
            if (pPortEntry->pNextMvrpTxNode != NULL)
            {
                /* Handling Case 2 */
                /* Start from the first bit in the BitList */
                u2AttrIndex = 0;
                continue;
            }
            /* Case 1: No VLAN is associated with this port. Hence return NULL
             * */
            break;
        }
        else
        {
            /* In the current transmit oppurtunity, the MRPDU has been formed 
             * starting from the VLAN ID in the pNextMvrpTxNode. Hence return
             * NULL, when GetNext returns the same VLAN as in pNextMvrpTxNode
             */
            if ((u1Flag == OSIX_FALSE) &&
                (pPortEntry->pNextMvrpTxNode != NULL) &&
                (u2NextAttrIndex == pPortEntry->pNextMvrpTxNode->u2AttrIndex))
            {
                break;
            }
        }

        /* Get the MAP ID to which this VLAN belongs */
        for (u2MapId = 0; u2MapId < pMrpAppEntry->u2MapArraySize; u2MapId++)
        {
            pMrpMapEntry = MRP_GET_MAP_ENTRY (pMrpAppEntry, u2MapId);

            if (pMrpMapEntry == NULL)
            {
                continue;
            }

            pNextAttrEntry = MRP_GET_ATTR_ENTRY (pMrpMapEntry, u2NextAttrIndex);

            if (pNextAttrEntry != NULL)
            {
                break;
            }
        }

        if (pMrpMapEntry == NULL)
        {
            u2AttrIndex = u2NextAttrIndex;
            continue;
        }
        /* As u2NextAttrIndex is a non-zero value, when control reaches here
         * pMrpMapEntry is the MST Instance to which this VLAN is associated 
         * with.
         */
        pMrpMapPortEntry =
            MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, pPortEntry->u2LocalPortId);

        /* If the Applicant type is Active, then MVRPDUs need to be send even
         * on ports that are in Blocking state.
         */
        if ((pMrpMapPortEntry == NULL) ||
            ((pPortEntry->au1ApplAdminCtrl[MRP_MVRP_APP_ID][MRP_VID_ATTR_TYPE]
              != MRP_ACTIVE_APPLICANT) &&
             (pMrpMapPortEntry->u1PortState == AST_PORT_STATE_DISCARDING) &&
             (MRP_GET_ATTR_APP_SEM_STATE
              (pMrpMapPortEntry->pu1AttrInfoList[u2NextAttrIndex]) != MRP_LA)))
        {
            /* When the port is in Blocking state need to send Leave messages 
             * for the attributes it has declared */
            u2AttrIndex = u2NextAttrIndex;
            continue;
        }
        break;
    }
    while (1);

    return pNextAttrEntry;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpApplyTxLAFOnRemainingAttr
 *                                                                          
 *    DESCRIPTION      : This function is called to apply txLAF! event for the
 *                       remaining attributes present in the attribute list.
 *
 *    INPUT            : pPortEntry  - Pointer to the Port entry. 
 *                       pAttrEntry  - Pointer to the current attribute entry. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMvrpApplyTxLAFOnRemainingAttr (tMrpPortEntry * pPortEntry,
                                  tMrpAttrEntry * pAttrEntry)
{
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT2               u2AttrIndex = 0;
    UINT1               u1RegSEMStateChg = OSIX_FALSE;

    do
    {
        pMapEntry = pAttrEntry->pMapEntry;
        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry,
                                                pPortEntry->u2LocalPortId);
        if (pMapPortEntry == NULL)
        {
            /* Added to avoid klocwork warning */
            return;
        }

        u2AttrIndex = pAttrEntry->u2AttrIndex;
        /* As per IEEE standard 802.1ak section 10.7.5.20 */
        MrpSmApplicantSem (pMapPortEntry, u2AttrIndex, MRP_APP_TX_LAF);

        if (MRP_GET_ATTR_REG_ADMIN_CTRL
            (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_REG_NORMAL)
        {
            MrpSmRegistrarSem (pMapPortEntry, u2AttrIndex,
                               MRP_REG_RX_LEAVE_ALL, &u1RegSEMStateChg);
        }

    }
    while ((pAttrEntry = MrpMvrpGetNextAttrForTx (pPortEntry, u2AttrIndex,
                                                  OSIX_FALSE)) != NULL);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMvrpRemapUpdateAppSemAndTx
 *                                                                          
 *    DESCRIPTION      : This function handles the VLAN instance remap. 
 *                       It will send LEAVE/MT PDUs.  
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                       pAppEntry - Apllication entry
 *                       pMapEntry     - MAP Entry
 *                       pPortEntry - MrpPort Entry
 *                       pMapPortEntry - Map Port Entry
 *                       u1DynVlan - Indicates whether the VLAN that is remapped
 *                                   is dynamically learnt or not
 *                       u1AttrFlag    - Flag is used to find whether the 
 *                                       remap is with attribute or not
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMvrpRemapUpdateSemAndTx (UINT4 u4ContextId, tMrpAppEntry * pAppEntry,
                            tMrpPortEntry * pPortEntry,
                            tMrpAttrEntry * pAttrEntry,
                            tMrpMapPortEntry * pMapPortEntry, UINT1 u1DynVlan,
                            UINT1 u1AttrFlag)
{
    tVlanId             VlanId = 0;
    UINT2               u2VectorHdr = 0;
    UINT2               u2Offset = MRP_PDU_HDR_SIZE;
    UINT2               u2NumOfValue = 1;
    UINT2               u2TmpVlanId = 0;
    UINT2               u2EndMark = MRP_END_MARK_VALUE;
    UINT1              *pu1Ptr = NULL;
    UINT1               u1AppSemState = 0;
    UINT1               u1RegSemState = 0;
    UINT1               u1AttrEvent = MRP_INVALID_EVENT;

    u1AppSemState = MRP_GET_ATTR_APP_SEM_STATE
        (pMapPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex]);

    u1RegSemState = MRP_GET_ATTR_REG_SEM_STATE
        (pMapPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex]);

    /* if Attribute is not present then continue with next MAP Port */
    if ((u1AppSemState == MRP_VO) && (u1RegSemState == MRP_MT))
    {
        return;
    }

    /* Determine what message to send */
    if ((u1AppSemState == MRP_AN) || (u1AppSemState == MRP_AA) ||
        (u1AppSemState == MRP_QA))
    {
        u1AttrEvent = MRP_LV_ATTR_EVENT;
    }
    else if ((MRP_GET_ATTR_REG_ADMIN_CTRL
              (pMapPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex])
              == MRP_REG_NORMAL) && (u1RegSemState == MRP_IN))
    {
        u1AttrEvent = MRP_MT_ATTR_EVENT;
    }

    /* Update App SEM to VO */
    MRP_SET_ATTR_APP_SEM_STATE
        (pMapPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex], MRP_VO);

    if (u1AttrEvent != MRP_INVALID_EVENT)
    {
        MEMSET (gMrpGlobalInfo.pu1PduBuf, 0, MRP_MAX_PDU_LEN);
        pu1Ptr = gMrpGlobalInfo.pu1PduBuf + u2Offset;

        /* Fill the attribute type and length of the attribute value */
        MRP_LBUF_PUT_1_BYTE (pu1Ptr, (UINT1) MRP_VID_ATTR_TYPE, u2Offset);
        MRP_LBUF_PUT_1_BYTE (pu1Ptr, (UINT1) MRP_VLAN_ID_LEN, u2Offset);

        u2VectorHdr |= u2NumOfValue;

        /* Fill the Vector Header. As the number of Attribute values filled in 
         * this PDU is 1, NumOfValue field is set as 1 */
        MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2VectorHdr, u2Offset);

        MEMCPY (&VlanId, pAttrEntry->Attr.au1AttrVal, MRP_VLAN_ID_LEN);
        VlanId = OSIX_NTOHS (VlanId);
        u2TmpVlanId = VlanId;
        /* Fill the first value field */
        MrpUtilTransLocalVidFromRelayVid (u4ContextId,
                                          pPortEntry->u2LocalPortId,
                                          &u2TmpVlanId);

        MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2TmpVlanId, u2Offset);

        /* Fill the vector field */
        MrpTxEncodeAttrEvent (pu1Ptr, u2NumOfValue, u1AttrEvent);

        /* Number of Vectors in this Attribute List is 1. Hence increment the
         * Offset by 1 */
        u2Offset++;
        /* Move the pointer to point to the end of the Vector field */
        pu1Ptr += 1;

        MrpUtilIncrTxStats (pPortEntry, MRP_MVRP_APP_ID, u1AttrEvent);

        /* Add EndMark to denote the end of Attribute List. */
        MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2EndMark, u2Offset);

        /* Add EndMark to denote the end of the MRPDU */
        MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2EndMark, u2Offset);

        MrpTxAddPduHdrAndTxPdu (u4ContextId, MRP_MVRP_APP_ID, u2Offset,
                                pPortEntry->u2LocalPortId, 0);
    }

    /* Check Registrar SEM, update VLAN DB */
    /* The last parameter is to indicate whether to delete specific MCAST or all
     * MCAST in VLAN */
    if (OSIX_TRUE == u1DynVlan)
    {
        MrpRemapUpdateRegSem (u4ContextId, pAppEntry, pMapPortEntry, pAttrEntry,
                              VlanId, OSIX_FALSE);
    }

    if (u1AttrFlag == MRP_REMAP_WITH_ATTR)
    {
        /* If Port is in Forwarding apply REQ_JOIN */
        if (AST_PORT_STATE_FORWARDING == pMapPortEntry->u1PortState)
        {
            MrpSmApplicantSem (pMapPortEntry, pAttrEntry->u2AttrIndex,
                               MRP_APP_REQ_JOIN);
        }
    }
    else
    {
        /* Delete the attribute from the MapAttrTable once the RegSem is 
         * updated */
        MrpAttrCheckAndDelAttrEntry (pMapPortEntry, pAttrEntry);
    }

}

/************************************************************************
 *
 * FUNCTION NAME    : MrpMvrpHdleRemapEvntForStEntries                
 *                                                                      
 * DESCRIPTION      : Handles Remap when static Registration is present 
 *                                                                      
 * INPUT(s)         : u4ContextId - Context Identifier
 *                    pAppEntry - Pointer to Application Entry          
 *                    u2MapId  - MAP Identifier                         
 *                    pAttr - pointer to attribute                      
 *                    u1RemapFlag - Indicates whether Attribute values are
 *                                  retained in the new MAP ID or not
 *                                                                      
 * OUTPUT(s)        : None                                              
 *                                                                      
 * RETURNS          : None                                              
 *
 ************************************************************************/
VOID
MrpMvrpHdleRemapEvntForStEntries (tMrpAppEntry * pAppEntry, UINT2 u2MapId,
                                  tMrpAttr * pAttr, UINT1 u1RemapFlag)
{
    /* The function is called when the RegCount of the attr in new instance
     * is not zero */
    UINT2               u2PortId = 0;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;

    pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

    if (pMapEntry == NULL)
    {
        return;
    }

    pAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMapEntry);

    for (u2PortId = 1; u2PortId <= MRP_MAX_PORTS_PER_CONTEXT; u2PortId++)
    {

        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

        if (pMapPortEntry == NULL)
        {
            continue;
        }
        pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMapPortEntry,
                                                       pMapPortEntry->u2Port);
        if (pAppPortEntry == NULL)
        {
            return;
        }
        pMrpPortEntry = pAppPortEntry->pMrpPortEntry;

        /* Assumption is if pMapPortEntry is there then pMrpPortEntry 
         * should be present. So no NULL check should be needed here */

        if (NULL != pAttrEntry)
        {
            MrpMvrpRemapUpdateSemAndTx (pMrpPortEntry->u4ContextId, pAppEntry,
                                        pMrpPortEntry, pAttrEntry,
                                        pMapPortEntry, OSIX_FALSE, u1RemapFlag);
        }
    }

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpmvrp.c                        */
/*-----------------------------------------------------------------------*/
