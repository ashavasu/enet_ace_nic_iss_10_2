/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1d1lw.c,v 1.3 2009/08/31 09:43:07 prabuc Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "mrpinc.h"

/* LOW LEVEL Routines for Table : Ieee8021BridgeBaseTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeBaseTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeBaseTable (UINT4
                                                 u4Ieee8021BridgeBaseComponentId)
{
    return (nmhValidateIndexInstanceFsMrpInstanceTable
            (u4Ieee8021BridgeBaseComponentId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeBaseTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeBaseTable (UINT4
                                         *pu4Ieee8021BridgeBaseComponentId)
{
    return (nmhGetFirstIndexFsMrpInstanceTable
            (pu4Ieee8021BridgeBaseComponentId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeBaseTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
                nextIeee8021BridgeBaseComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeBaseTable (UINT4 u4Ieee8021BridgeBaseComponentId,
                                        UINT4
                                        *pu4NextIeee8021BridgeBaseComponentId)
{
    return (nmhGetNextIndexFsMrpInstanceTable
            (u4Ieee8021BridgeBaseComponentId,
             pu4NextIeee8021BridgeBaseComponentId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseBridgeAddress
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseBridgeAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseBridgeAddress (UINT4 u4Ieee8021BridgeBaseComponentId,
                                       tMacAddr *
                                       pRetValIeee8021BridgeBaseBridgeAddress)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pRetValIeee8021BridgeBaseBridgeAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseNumPorts
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseNumPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseNumPorts (UINT4 u4Ieee8021BridgeBaseComponentId,
                                  INT4 *pi4RetValIeee8021BridgeBaseNumPorts)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pi4RetValIeee8021BridgeBaseNumPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseComponentType
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseComponentType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseComponentType (UINT4 u4Ieee8021BridgeBaseComponentId,
                                       INT4
                                       *pi4RetValIeee8021BridgeBaseComponentType)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pi4RetValIeee8021BridgeBaseComponentType);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseDeviceCapabilities
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseDeviceCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseDeviceCapabilities (UINT4
                                            u4Ieee8021BridgeBaseComponentId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValIeee8021BridgeBaseDeviceCapabilities)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pRetValIeee8021BridgeBaseDeviceCapabilities);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseTrafficClassesEnabled
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseTrafficClassesEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseTrafficClassesEnabled (UINT4
                                               u4Ieee8021BridgeBaseComponentId,
                                               INT4
                                               *pi4RetValIeee8021BridgeBaseTrafficClassesEnabled)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pi4RetValIeee8021BridgeBaseTrafficClassesEnabled);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseMmrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseMmrpEnabledStatus (UINT4
                                           u4Ieee8021BridgeBaseComponentId,
                                           INT4
                                           *pi4RetValIeee8021BridgeBaseMmrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo != NULL)
    {
        *pi4RetValIeee8021BridgeBaseMmrpEnabledStatus =
            gMrpGlobalInfo.au1MmrpStatus[u4Ieee8021BridgeBaseComponentId];
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBaseRowStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValIeee8021BridgeBaseRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBaseRowStatus (UINT4 u4Ieee8021BridgeBaseComponentId,
                                   INT4 *pi4RetValIeee8021BridgeBaseRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pi4RetValIeee8021BridgeBaseRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseBridgeAddress
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseBridgeAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseBridgeAddress (UINT4 u4Ieee8021BridgeBaseComponentId,
                                       tMacAddr
                                       SetValIeee8021BridgeBaseBridgeAddress)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (SetValIeee8021BridgeBaseBridgeAddress);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseComponentType
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseComponentType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseComponentType (UINT4 u4Ieee8021BridgeBaseComponentId,
                                       INT4
                                       i4SetValIeee8021BridgeBaseComponentType)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (i4SetValIeee8021BridgeBaseComponentType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseDeviceCapabilities
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseDeviceCapabilities
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseDeviceCapabilities (UINT4
                                            u4Ieee8021BridgeBaseComponentId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValIeee8021BridgeBaseDeviceCapabilities)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pSetValIeee8021BridgeBaseDeviceCapabilities);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseTrafficClassesEnabled
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseTrafficClassesEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseTrafficClassesEnabled (UINT4
                                               u4Ieee8021BridgeBaseComponentId,
                                               INT4
                                               i4SetValIeee8021BridgeBaseTrafficClassesEnabled)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (i4SetValIeee8021BridgeBaseTrafficClassesEnabled);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseMmrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseMmrpEnabledStatus (UINT4
                                           u4Ieee8021BridgeBaseComponentId,
                                           INT4
                                           i4SetValIeee8021BridgeBaseMmrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (gMrpGlobalInfo.au1MmrpStatus[u4Ieee8021BridgeBaseComponentId] ==
        i4SetValIeee8021BridgeBaseMmrpEnabledStatus)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValIeee8021BridgeBaseMmrpEnabledStatus)
    {

        case MRP_ENABLED:

            i4RetVal = MrpMmrpEnable (pMrpContextInfo);

            break;

        case MRP_DISABLED:

            i4RetVal = MrpMmrpDisable (pMrpContextInfo);

            break;

        default:
            break;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBaseRowStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValIeee8021BridgeBaseRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBaseRowStatus (UINT4 u4Ieee8021BridgeBaseComponentId,
                                   INT4 i4SetValIeee8021BridgeBaseRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (i4SetValIeee8021BridgeBaseRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseBridgeAddress
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseBridgeAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseBridgeAddress (UINT4 *pu4ErrorCode,
                                          UINT4 u4Ieee8021BridgeBaseComponentId,
                                          tMacAddr
                                          TestValIeee8021BridgeBaseBridgeAddress)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (TestValIeee8021BridgeBaseBridgeAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseComponentType
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseComponentType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseComponentType (UINT4 *pu4ErrorCode,
                                          UINT4 u4Ieee8021BridgeBaseComponentId,
                                          INT4
                                          i4TestValIeee8021BridgeBaseComponentType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (i4TestValIeee8021BridgeBaseComponentType);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseDeviceCapabilities
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseDeviceCapabilities
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseDeviceCapabilities (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBaseComponentId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValIeee8021BridgeBaseDeviceCapabilities)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pTestValIeee8021BridgeBaseDeviceCapabilities);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseTrafficClassesEnabled
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseTrafficClassesEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseTrafficClassesEnabled (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021BridgeBaseComponentId,
                                                  INT4
                                                  i4TestValIeee8021BridgeBaseTrafficClassesEnabled)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (i4TestValIeee8021BridgeBaseTrafficClassesEnabled);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseMmrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseMmrpEnabledStatus (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBaseComponentId,
                                              INT4
                                              i4TestValIeee8021BridgeBaseMmrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT2               u2VlanIsStarted = 0;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021BridgeBaseMmrpEnabledStatus != MRP_ENABLED) &&
        (i4TestValIeee8021BridgeBaseMmrpEnabledStatus != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    if (i4TestValIeee8021BridgeBaseMmrpEnabledStatus == MRP_ENABLED)
    {
        u2VlanIsStarted = (UINT2) MrpPortVlanGetStartedStatus
            (u4Ieee8021BridgeBaseComponentId);

        /* Before Start the MRP module , VLAN module should
           be started.Otherwise no use of starting MRP. */
        if (u2VlanIsStarted == VLAN_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_NOSHUT_VLAN_ENABLED_ERR);
            return SNMP_FAILURE;
        }

        /* Enable MMRP status only when the IGS and MLDS is disabled */
        if (SNOOP_ENABLED ==
            MrpPortIsIgmpSnoopingEnabled (u4Ieee8021BridgeBaseComponentId))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_IGS_ENABLED_ERR);
            return SNMP_FAILURE;
        }

        if (SNOOP_ENABLED ==
            MrpPortSnoopIsMldSnoopingEnabled (u4Ieee8021BridgeBaseComponentId))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_MLDS_ENABLED_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBaseRowStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValIeee8021BridgeBaseRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBaseRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021BridgeBaseComponentId,
                                      INT4 i4TestValIeee8021BridgeBaseRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (i4TestValIeee8021BridgeBaseRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeBaseTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeBaseTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeBasePortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeBasePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeBasePortTable (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeBasePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeBasePortTable (UINT4
                                             *pu4Ieee8021BridgeBasePortComponentId,
                                             UINT4 *pu4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeBasePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeBasePortTable (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4
                                            *pu4NextIeee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            UINT4
                                            *pu4NextIeee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortIfIndex
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortIfIndex (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     INT4
                                     *pi4RetValIeee8021BridgeBasePortIfIndex)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgeBasePortIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortDelayExceededDiscards
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortDelayExceededDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortDelayExceededDiscards (UINT4
                                                   u4Ieee8021BridgeBasePortComponentId,
                                                   UINT4
                                                   u4Ieee8021BridgeBasePort,
                                                   tSNMP_COUNTER64_TYPE *
                                                   pu8RetValIeee8021BridgeBasePortDelayExceededDiscards)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu8RetValIeee8021BridgeBasePortDelayExceededDiscards);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortMtuExceededDiscards
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortMtuExceededDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortMtuExceededDiscards (UINT4
                                                 u4Ieee8021BridgeBasePortComponentId,
                                                 UINT4 u4Ieee8021BridgeBasePort,
                                                 tSNMP_COUNTER64_TYPE *
                                                 pu8RetValIeee8021BridgeBasePortMtuExceededDiscards)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu8RetValIeee8021BridgeBasePortMtuExceededDiscards);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortCapabilities
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortCapabilities (UINT4
                                          u4Ieee8021BridgeBasePortComponentId,
                                          UINT4 u4Ieee8021BridgeBasePort,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValIeee8021BridgeBasePortCapabilities)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pRetValIeee8021BridgeBasePortCapabilities);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortTypeCapabilities
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortTypeCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortTypeCapabilities (UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValIeee8021BridgeBasePortTypeCapabilities)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pRetValIeee8021BridgeBasePortTypeCapabilities);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortType
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortType (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  INT4 *pi4RetValIeee8021BridgeBasePortType)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgeBasePortType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortExternal
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortExternal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortExternal (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      INT4
                                      *pi4RetValIeee8021BridgeBasePortExternal)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgeBasePortExternal);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortAdminPointToPoint
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortAdminPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortAdminPointToPoint (UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               *pi4RetValIeee8021BridgeBasePortAdminPointToPoint)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgeBasePortAdminPointToPoint);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortOperPointToPoint
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortOperPointToPoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortOperPointToPoint (UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              *pi4RetValIeee8021BridgeBasePortOperPointToPoint)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgeBasePortOperPointToPoint);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeBasePortName
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeBasePortName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeBasePortName (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValIeee8021BridgeBasePortName)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pRetValIeee8021BridgeBasePortName);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBasePortIfIndex
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgeBasePortIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBasePortIfIndex (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     INT4 i4SetValIeee8021BridgeBasePortIfIndex)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgeBasePortIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeBasePortAdminPointToPoint
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgeBasePortAdminPointToPoint
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeBasePortAdminPointToPoint (UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4SetValIeee8021BridgeBasePortAdminPointToPoint)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgeBasePortAdminPointToPoint);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBasePortIfIndex
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgeBasePortIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBasePortIfIndex (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        INT4
                                        i4TestValIeee8021BridgeBasePortIfIndex)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgeBasePortIfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeBasePortAdminPointToPoint
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgeBasePortAdminPointToPoint
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeBasePortAdminPointToPoint (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePort,
                                                  INT4
                                                  i4TestValIeee8021BridgeBasePortAdminPointToPoint)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgeBasePortAdminPointToPoint);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeBasePortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeBasePortTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeTpPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeTpPortTable
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeTpPortTable (UINT4
                                                   u4Ieee8021BridgeTpPortComponentId,
                                                   UINT4 u4Ieee8021BridgeTpPort)
{
    UNUSED_PARAM (u4Ieee8021BridgeTpPortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeTpPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeTpPortTable
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeTpPortTable (UINT4
                                           *pu4Ieee8021BridgeTpPortComponentId,
                                           UINT4 *pu4Ieee8021BridgeTpPort)
{
    UNUSED_PARAM (pu4Ieee8021BridgeTpPortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeTpPort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeTpPortTable
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                nextIeee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort
                nextIeee8021BridgeTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeTpPortTable (UINT4
                                          u4Ieee8021BridgeTpPortComponentId,
                                          UINT4
                                          *pu4NextIeee8021BridgeTpPortComponentId,
                                          UINT4 u4Ieee8021BridgeTpPort,
                                          UINT4 *pu4NextIeee8021BridgeTpPort)
{
    UNUSED_PARAM (u4Ieee8021BridgeTpPortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeTpPortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeTpPort);
    UNUSED_PARAM (pu4NextIeee8021BridgeTpPort);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeTpPortMaxInfo
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort

                The Object 
                retValIeee8021BridgeTpPortMaxInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeTpPortMaxInfo (UINT4 u4Ieee8021BridgeTpPortComponentId,
                                   UINT4 u4Ieee8021BridgeTpPort,
                                   INT4 *pi4RetValIeee8021BridgeTpPortMaxInfo)
{
    UNUSED_PARAM (u4Ieee8021BridgeTpPortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeTpPort);
    UNUSED_PARAM (pi4RetValIeee8021BridgeTpPortMaxInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeTpPortInFrames
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort

                The Object 
                retValIeee8021BridgeTpPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeTpPortInFrames (UINT4 u4Ieee8021BridgeTpPortComponentId,
                                    UINT4 u4Ieee8021BridgeTpPort,
                                    tSNMP_COUNTER64_TYPE *
                                    pu8RetValIeee8021BridgeTpPortInFrames)
{
    UNUSED_PARAM (u4Ieee8021BridgeTpPortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeTpPort);
    UNUSED_PARAM (pu8RetValIeee8021BridgeTpPortInFrames);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeTpPortOutFrames
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort

                The Object 
                retValIeee8021BridgeTpPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeTpPortOutFrames (UINT4 u4Ieee8021BridgeTpPortComponentId,
                                     UINT4 u4Ieee8021BridgeTpPort,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValIeee8021BridgeTpPortOutFrames)
{
    UNUSED_PARAM (u4Ieee8021BridgeTpPortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeTpPort);
    UNUSED_PARAM (pu8RetValIeee8021BridgeTpPortOutFrames);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeTpPortInDiscards
 Input       :  The Indices
                Ieee8021BridgeTpPortComponentId
                Ieee8021BridgeTpPort

                The Object 
                retValIeee8021BridgeTpPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeTpPortInDiscards (UINT4 u4Ieee8021BridgeTpPortComponentId,
                                      UINT4 u4Ieee8021BridgeTpPort,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValIeee8021BridgeTpPortInDiscards)
{
    UNUSED_PARAM (u4Ieee8021BridgeTpPortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeTpPort);
    UNUSED_PARAM (pu8RetValIeee8021BridgeTpPortInDiscards);
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortPriorityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortPriorityTable (UINT4
                                                         u4Ieee8021BridgeBasePortComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortPriorityTable (UINT4
                                                 *pu4Ieee8021BridgeBasePortComponentId,
                                                 UINT4
                                                 *pu4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortPriorityTable (UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4
                                                *pu4NextIeee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                UINT4
                                                *pu4NextIeee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortDefaultUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortDefaultUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortDefaultUserPriority (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             UINT4
                                             *pu4RetValIeee8021BridgePortDefaultUserPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4RetValIeee8021BridgePortDefaultUserPriority);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortNumTrafficClasses
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortNumTrafficClasses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortNumTrafficClasses (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           *pi4RetValIeee8021BridgePortNumTrafficClasses)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortNumTrafficClasses);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortPriorityCodePointSelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortPriorityCodePointSelection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortPriorityCodePointSelection (UINT4
                                                    u4Ieee8021BridgeBasePortComponentId,
                                                    UINT4
                                                    u4Ieee8021BridgeBasePort,
                                                    INT4
                                                    *pi4RetValIeee8021BridgePortPriorityCodePointSelection)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortPriorityCodePointSelection);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortUseDEI
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortUseDEI
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortUseDEI (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                UINT4 u4Ieee8021BridgeBasePort,
                                INT4 *pi4RetValIeee8021BridgePortUseDEI)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortUseDEI);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortRequireDropEncoding
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortRequireDropEncoding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortRequireDropEncoding (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4
                                             *pi4RetValIeee8021BridgePortRequireDropEncoding)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortRequireDropEncoding);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortServiceAccessPrioritySelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortServiceAccessPrioritySelection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortServiceAccessPrioritySelection (UINT4
                                                        u4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePort,
                                                        INT4
                                                        *pi4RetValIeee8021BridgePortServiceAccessPrioritySelection)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortServiceAccessPrioritySelection);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortDefaultUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortDefaultUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortDefaultUserPriority (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             UINT4
                                             u4SetValIeee8021BridgePortDefaultUserPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4SetValIeee8021BridgePortDefaultUserPriority);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortNumTrafficClasses
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortNumTrafficClasses
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortNumTrafficClasses (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4SetValIeee8021BridgePortNumTrafficClasses)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgePortNumTrafficClasses);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortPriorityCodePointSelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortPriorityCodePointSelection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortPriorityCodePointSelection (UINT4
                                                    u4Ieee8021BridgeBasePortComponentId,
                                                    UINT4
                                                    u4Ieee8021BridgeBasePort,
                                                    INT4
                                                    i4SetValIeee8021BridgePortPriorityCodePointSelection)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgePortPriorityCodePointSelection);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortUseDEI
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortUseDEI
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortUseDEI (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                UINT4 u4Ieee8021BridgeBasePort,
                                INT4 i4SetValIeee8021BridgePortUseDEI)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgePortUseDEI);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortRequireDropEncoding
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortRequireDropEncoding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortRequireDropEncoding (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             INT4
                                             i4SetValIeee8021BridgePortRequireDropEncoding)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgePortRequireDropEncoding);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortServiceAccessPrioritySelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortServiceAccessPrioritySelection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortServiceAccessPrioritySelection (UINT4
                                                        u4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePort,
                                                        INT4
                                                        i4SetValIeee8021BridgePortServiceAccessPrioritySelection)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgePortServiceAccessPrioritySelection);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortDefaultUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortDefaultUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortDefaultUserPriority (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                UINT4
                                                u4TestValIeee8021BridgePortDefaultUserPriority)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4TestValIeee8021BridgePortDefaultUserPriority);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortNumTrafficClasses
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortNumTrafficClasses
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortNumTrafficClasses (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4TestValIeee8021BridgePortNumTrafficClasses)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgePortNumTrafficClasses);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortPriorityCodePointSelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortPriorityCodePointSelection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortPriorityCodePointSelection (UINT4 *pu4ErrorCode,
                                                       UINT4
                                                       u4Ieee8021BridgeBasePortComponentId,
                                                       UINT4
                                                       u4Ieee8021BridgeBasePort,
                                                       INT4
                                                       i4TestValIeee8021BridgePortPriorityCodePointSelection)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgePortPriorityCodePointSelection);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortUseDEI
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortUseDEI
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortUseDEI (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   INT4 i4TestValIeee8021BridgePortUseDEI)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgePortUseDEI);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortRequireDropEncoding
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortRequireDropEncoding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortRequireDropEncoding (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                INT4
                                                i4TestValIeee8021BridgePortRequireDropEncoding)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgePortRequireDropEncoding);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortServiceAccessPrioritySelection
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortServiceAccessPrioritySelection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortServiceAccessPrioritySelection (UINT4 *pu4ErrorCode,
                                                           UINT4
                                                           u4Ieee8021BridgeBasePortComponentId,
                                                           UINT4
                                                           u4Ieee8021BridgeBasePort,
                                                           INT4
                                                           i4TestValIeee8021BridgePortServiceAccessPrioritySelection)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgePortServiceAccessPrioritySelection);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgePortPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgePortPriorityTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeUserPriorityRegenTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeUserPriorityRegenTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeUserPriorityRegenTable (UINT4
                                                              u4Ieee8021BridgeBasePortComponentId,
                                                              UINT4
                                                              u4Ieee8021BridgeBasePort,
                                                              UINT4
                                                              u4Ieee8021BridgeUserPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeUserPriority);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeUserPriorityRegenTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeUserPriorityRegenTable (UINT4
                                                      *pu4Ieee8021BridgeBasePortComponentId,
                                                      UINT4
                                                      *pu4Ieee8021BridgeBasePort,
                                                      UINT4
                                                      *pu4Ieee8021BridgeUserPriority)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4Ieee8021BridgeUserPriority);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeUserPriorityRegenTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021BridgeUserPriority
                nextIeee8021BridgeUserPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeUserPriorityRegenTable (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     *pu4NextIeee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     UINT4
                                                     *pu4NextIeee8021BridgeBasePort,
                                                     UINT4
                                                     u4Ieee8021BridgeUserPriority,
                                                     UINT4
                                                     *pu4NextIeee8021BridgeUserPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeUserPriority);
    UNUSED_PARAM (pu4NextIeee8021BridgeUserPriority);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeRegenUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority

                The Object 
                retValIeee8021BridgeRegenUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeRegenUserPriority (UINT4
                                       u4Ieee8021BridgeBasePortComponentId,
                                       UINT4 u4Ieee8021BridgeBasePort,
                                       UINT4 u4Ieee8021BridgeUserPriority,
                                       UINT4
                                       *pu4RetValIeee8021BridgeRegenUserPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeUserPriority);
    UNUSED_PARAM (pu4RetValIeee8021BridgeRegenUserPriority);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeRegenUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority

                The Object 
                setValIeee8021BridgeRegenUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeRegenUserPriority (UINT4
                                       u4Ieee8021BridgeBasePortComponentId,
                                       UINT4 u4Ieee8021BridgeBasePort,
                                       UINT4 u4Ieee8021BridgeUserPriority,
                                       UINT4
                                       u4SetValIeee8021BridgeRegenUserPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeUserPriority);
    UNUSED_PARAM (u4SetValIeee8021BridgeRegenUserPriority);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeRegenUserPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority

                The Object 
                testValIeee8021BridgeRegenUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeRegenUserPriority (UINT4 *pu4ErrorCode,
                                          UINT4
                                          u4Ieee8021BridgeBasePortComponentId,
                                          UINT4 u4Ieee8021BridgeBasePort,
                                          UINT4 u4Ieee8021BridgeUserPriority,
                                          UINT4
                                          u4TestValIeee8021BridgeRegenUserPriority)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeUserPriority);
    UNUSED_PARAM (u4TestValIeee8021BridgeRegenUserPriority);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeUserPriorityRegenTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeUserPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeUserPriorityRegenTable (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeTrafficClassTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeTrafficClassTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeTrafficClassTable (UINT4
                                                         u4Ieee8021BridgeBasePortComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgeBasePort,
                                                         UINT4
                                                         u4Ieee8021BridgeTrafficClassPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeTrafficClassPriority);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeTrafficClassTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeTrafficClassTable (UINT4
                                                 *pu4Ieee8021BridgeBasePortComponentId,
                                                 UINT4
                                                 *pu4Ieee8021BridgeBasePort,
                                                 UINT4
                                                 *pu4Ieee8021BridgeTrafficClassPriority)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4Ieee8021BridgeTrafficClassPriority);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeTrafficClassTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority
                nextIeee8021BridgeTrafficClassPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeTrafficClassTable (UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4
                                                *pu4NextIeee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                UINT4
                                                *pu4NextIeee8021BridgeBasePort,
                                                UINT4
                                                u4Ieee8021BridgeTrafficClassPriority,
                                                UINT4
                                                *pu4NextIeee8021BridgeTrafficClassPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeTrafficClassPriority);
    UNUSED_PARAM (pu4NextIeee8021BridgeTrafficClassPriority);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeTrafficClass
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority

                The Object 
                retValIeee8021BridgeTrafficClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeTrafficClass (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  UINT4 u4Ieee8021BridgeTrafficClassPriority,
                                  INT4 *pi4RetValIeee8021BridgeTrafficClass)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeTrafficClassPriority);
    UNUSED_PARAM (pi4RetValIeee8021BridgeTrafficClass);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeTrafficClass
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority

                The Object 
                setValIeee8021BridgeTrafficClass
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeTrafficClass (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  UINT4 u4Ieee8021BridgeTrafficClassPriority,
                                  INT4 i4SetValIeee8021BridgeTrafficClass)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeTrafficClassPriority);
    UNUSED_PARAM (i4SetValIeee8021BridgeTrafficClass);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeTrafficClass
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority

                The Object 
                testValIeee8021BridgeTrafficClass
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeTrafficClass (UINT4 *pu4ErrorCode,
                                     UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     UINT4 u4Ieee8021BridgeTrafficClassPriority,
                                     INT4 i4TestValIeee8021BridgeTrafficClass)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeTrafficClassPriority);
    UNUSED_PARAM (i4TestValIeee8021BridgeTrafficClass);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeTrafficClassTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeTrafficClassPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeTrafficClassTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortOutboundAccessPriorityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortOutboundAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeRegenUserPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortOutboundAccessPriorityTable (UINT4
                                                                       u4Ieee8021BridgeBasePortComponentId,
                                                                       UINT4
                                                                       u4Ieee8021BridgeBasePort,
                                                                       UINT4
                                                                       u4Ieee8021BridgeRegenUserPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeRegenUserPriority);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortOutboundAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeRegenUserPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortOutboundAccessPriorityTable (UINT4
                                                               *pu4Ieee8021BridgeBasePortComponentId,
                                                               UINT4
                                                               *pu4Ieee8021BridgeBasePort,
                                                               UINT4
                                                               *pu4Ieee8021BridgeRegenUserPriority)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4Ieee8021BridgeRegenUserPriority);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortOutboundAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021BridgeRegenUserPriority
                nextIeee8021BridgeRegenUserPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortOutboundAccessPriorityTable (UINT4
                                                              u4Ieee8021BridgeBasePortComponentId,
                                                              UINT4
                                                              *pu4NextIeee8021BridgeBasePortComponentId,
                                                              UINT4
                                                              u4Ieee8021BridgeBasePort,
                                                              UINT4
                                                              *pu4NextIeee8021BridgeBasePort,
                                                              UINT4
                                                              u4Ieee8021BridgeRegenUserPriority,
                                                              UINT4
                                                              *pu4NextIeee8021BridgeRegenUserPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeRegenUserPriority);
    UNUSED_PARAM (pu4NextIeee8021BridgeRegenUserPriority);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortOutboundAccessPriority
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021BridgeRegenUserPriority

                The Object 
                retValIeee8021BridgePortOutboundAccessPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortOutboundAccessPriority (UINT4
                                                u4Ieee8021BridgeBasePortComponentId,
                                                UINT4 u4Ieee8021BridgeBasePort,
                                                UINT4
                                                u4Ieee8021BridgeRegenUserPriority,
                                                UINT4
                                                *pu4RetValIeee8021BridgePortOutboundAccessPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021BridgeRegenUserPriority);
    UNUSED_PARAM (pu4RetValIeee8021BridgePortOutboundAccessPriority);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortDecodingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortDecodingTable
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortDecodingTable (UINT4
                                                         u4Ieee8021BridgePortDecodingComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgePortDecodingPortNum,
                                                         INT4
                                                         i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                         INT4
                                                         i4Ieee8021BridgePortDecodingPriorityCodePoint)
{
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortDecodingTable
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortDecodingTable (UINT4
                                                 *pu4Ieee8021BridgePortDecodingComponentId,
                                                 UINT4
                                                 *pu4Ieee8021BridgePortDecodingPortNum,
                                                 INT4
                                                 *pi4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                 INT4
                                                 *pi4Ieee8021BridgePortDecodingPriorityCodePoint)
{
    UNUSED_PARAM (pu4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (pi4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (pi4Ieee8021BridgePortDecodingPriorityCodePoint);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortDecodingTable
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                nextIeee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                nextIeee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                nextIeee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint
                nextIeee8021BridgePortDecodingPriorityCodePoint
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortDecodingTable (UINT4
                                                u4Ieee8021BridgePortDecodingComponentId,
                                                UINT4
                                                *pu4NextIeee8021BridgePortDecodingComponentId,
                                                UINT4
                                                u4Ieee8021BridgePortDecodingPortNum,
                                                UINT4
                                                *pu4NextIeee8021BridgePortDecodingPortNum,
                                                INT4
                                                i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                INT4
                                                *pi4NextIeee8021BridgePortDecodingPriorityCodePointRow,
                                                INT4
                                                i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                                INT4
                                                *pi4NextIeee8021BridgePortDecodingPriorityCodePoint)
{
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (pu4NextIeee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (pi4NextIeee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (pi4NextIeee8021BridgePortDecodingPriorityCodePoint);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortDecodingPriority
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                retValIeee8021BridgePortDecodingPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortDecodingPriority (UINT4
                                          u4Ieee8021BridgePortDecodingComponentId,
                                          UINT4
                                          u4Ieee8021BridgePortDecodingPortNum,
                                          INT4
                                          i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                          INT4
                                          i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                          UINT4
                                          *pu4RetValIeee8021BridgePortDecodingPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (pu4RetValIeee8021BridgePortDecodingPriority);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortDecodingDropEligible
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                retValIeee8021BridgePortDecodingDropEligible
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortDecodingDropEligible (UINT4
                                              u4Ieee8021BridgePortDecodingComponentId,
                                              UINT4
                                              u4Ieee8021BridgePortDecodingPortNum,
                                              INT4
                                              i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                              INT4
                                              i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                              INT4
                                              *pi4RetValIeee8021BridgePortDecodingDropEligible)
{
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (pi4RetValIeee8021BridgePortDecodingDropEligible);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortDecodingPriority
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                setValIeee8021BridgePortDecodingPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortDecodingPriority (UINT4
                                          u4Ieee8021BridgePortDecodingComponentId,
                                          UINT4
                                          u4Ieee8021BridgePortDecodingPortNum,
                                          INT4
                                          i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                          INT4
                                          i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                          UINT4
                                          u4SetValIeee8021BridgePortDecodingPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (u4SetValIeee8021BridgePortDecodingPriority);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortDecodingDropEligible
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                setValIeee8021BridgePortDecodingDropEligible
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortDecodingDropEligible (UINT4
                                              u4Ieee8021BridgePortDecodingComponentId,
                                              UINT4
                                              u4Ieee8021BridgePortDecodingPortNum,
                                              INT4
                                              i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                              INT4
                                              i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                              INT4
                                              i4SetValIeee8021BridgePortDecodingDropEligible)
{
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (i4SetValIeee8021BridgePortDecodingDropEligible);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortDecodingPriority
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                testValIeee8021BridgePortDecodingPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortDecodingPriority (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4Ieee8021BridgePortDecodingComponentId,
                                             UINT4
                                             u4Ieee8021BridgePortDecodingPortNum,
                                             INT4
                                             i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                             INT4
                                             i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                             UINT4
                                             u4TestValIeee8021BridgePortDecodingPriority)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (u4TestValIeee8021BridgePortDecodingPriority);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortDecodingDropEligible
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint

                The Object 
                testValIeee8021BridgePortDecodingDropEligible
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortDecodingDropEligible (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021BridgePortDecodingComponentId,
                                                 UINT4
                                                 u4Ieee8021BridgePortDecodingPortNum,
                                                 INT4
                                                 i4Ieee8021BridgePortDecodingPriorityCodePointRow,
                                                 INT4
                                                 i4Ieee8021BridgePortDecodingPriorityCodePoint,
                                                 INT4
                                                 i4TestValIeee8021BridgePortDecodingDropEligible)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortDecodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortDecodingPriorityCodePoint);
    UNUSED_PARAM (i4TestValIeee8021BridgePortDecodingDropEligible);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgePortDecodingTable
 Input       :  The Indices
                Ieee8021BridgePortDecodingComponentId
                Ieee8021BridgePortDecodingPortNum
                Ieee8021BridgePortDecodingPriorityCodePointRow
                Ieee8021BridgePortDecodingPriorityCodePoint
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgePortDecodingTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortEncodingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortEncodingTable
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortEncodingTable (UINT4
                                                         u4Ieee8021BridgePortEncodingComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgePortEncodingPortNum,
                                                         INT4
                                                         i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                                         INT4
                                                         i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                                         INT4
                                                         i4Ieee8021BridgePortEncodingDropEligible)
{
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingDropEligible);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortEncodingTable
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortEncodingTable (UINT4
                                                 *pu4Ieee8021BridgePortEncodingComponentId,
                                                 UINT4
                                                 *pu4Ieee8021BridgePortEncodingPortNum,
                                                 INT4
                                                 *pi4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                                 INT4
                                                 *pi4Ieee8021BridgePortEncodingPriorityCodePoint,
                                                 INT4
                                                 *pi4Ieee8021BridgePortEncodingDropEligible)
{
    UNUSED_PARAM (pu4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (pi4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (pi4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (pi4Ieee8021BridgePortEncodingDropEligible);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortEncodingTable
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                nextIeee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                nextIeee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                nextIeee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                nextIeee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible
                nextIeee8021BridgePortEncodingDropEligible
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortEncodingTable (UINT4
                                                u4Ieee8021BridgePortEncodingComponentId,
                                                UINT4
                                                *pu4NextIeee8021BridgePortEncodingComponentId,
                                                UINT4
                                                u4Ieee8021BridgePortEncodingPortNum,
                                                UINT4
                                                *pu4NextIeee8021BridgePortEncodingPortNum,
                                                INT4
                                                i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                                INT4
                                                *pi4NextIeee8021BridgePortEncodingPriorityCodePointRow,
                                                INT4
                                                i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                                INT4
                                                *pi4NextIeee8021BridgePortEncodingPriorityCodePoint,
                                                INT4
                                                i4Ieee8021BridgePortEncodingDropEligible,
                                                INT4
                                                *pi4NextIeee8021BridgePortEncodingDropEligible)
{
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (pu4NextIeee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (pi4NextIeee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (pi4NextIeee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingDropEligible);
    UNUSED_PARAM (pi4NextIeee8021BridgePortEncodingDropEligible);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortEncodingPriority
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible

                The Object 
                retValIeee8021BridgePortEncodingPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortEncodingPriority (UINT4
                                          u4Ieee8021BridgePortEncodingComponentId,
                                          UINT4
                                          u4Ieee8021BridgePortEncodingPortNum,
                                          INT4
                                          i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                          INT4
                                          i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                          INT4
                                          i4Ieee8021BridgePortEncodingDropEligible,
                                          UINT4
                                          *pu4RetValIeee8021BridgePortEncodingPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingDropEligible);
    UNUSED_PARAM (pu4RetValIeee8021BridgePortEncodingPriority);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortEncodingPriority
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible

                The Object 
                setValIeee8021BridgePortEncodingPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortEncodingPriority (UINT4
                                          u4Ieee8021BridgePortEncodingComponentId,
                                          UINT4
                                          u4Ieee8021BridgePortEncodingPortNum,
                                          INT4
                                          i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                          INT4
                                          i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                          INT4
                                          i4Ieee8021BridgePortEncodingDropEligible,
                                          UINT4
                                          u4SetValIeee8021BridgePortEncodingPriority)
{
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingDropEligible);
    UNUSED_PARAM (u4SetValIeee8021BridgePortEncodingPriority);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortEncodingPriority
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible

                The Object 
                testValIeee8021BridgePortEncodingPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortEncodingPriority (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4Ieee8021BridgePortEncodingComponentId,
                                             UINT4
                                             u4Ieee8021BridgePortEncodingPortNum,
                                             INT4
                                             i4Ieee8021BridgePortEncodingPriorityCodePointRow,
                                             INT4
                                             i4Ieee8021BridgePortEncodingPriorityCodePoint,
                                             INT4
                                             i4Ieee8021BridgePortEncodingDropEligible,
                                             UINT4
                                             u4TestValIeee8021BridgePortEncodingPriority)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingComponentId);
    UNUSED_PARAM (u4Ieee8021BridgePortEncodingPortNum);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePointRow);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingPriorityCodePoint);
    UNUSED_PARAM (i4Ieee8021BridgePortEncodingDropEligible);
    UNUSED_PARAM (u4TestValIeee8021BridgePortEncodingPriority);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgePortEncodingTable
 Input       :  The Indices
                Ieee8021BridgePortEncodingComponentId
                Ieee8021BridgePortEncodingPortNum
                Ieee8021BridgePortEncodingPriorityCodePointRow
                Ieee8021BridgePortEncodingPriorityCodePoint
                Ieee8021BridgePortEncodingDropEligible
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgePortEncodingTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeServiceAccessPriorityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeServiceAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeServiceAccessPriorityTable (UINT4
                                                                  u4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                                  UINT4
                                                                  u4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                                  UINT4
                                                                  u4Ieee8021BridgeServiceAccessPriorityReceived)
{
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityReceived);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeServiceAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeServiceAccessPriorityTable (UINT4
                                                          *pu4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                          UINT4
                                                          *pu4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                          UINT4
                                                          *pu4Ieee8021BridgeServiceAccessPriorityReceived)
{
    UNUSED_PARAM (pu4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (pu4Ieee8021BridgeServiceAccessPriorityReceived);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeServiceAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                nextIeee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                nextIeee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived
                nextIeee8021BridgeServiceAccessPriorityReceived
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeServiceAccessPriorityTable (UINT4
                                                         u4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                         UINT4
                                                         *pu4NextIeee8021BridgeServiceAccessPriorityComponentId,
                                                         UINT4
                                                         u4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                         UINT4
                                                         *pu4NextIeee8021BridgeServiceAccessPriorityPortNum,
                                                         UINT4
                                                         u4Ieee8021BridgeServiceAccessPriorityReceived,
                                                         UINT4
                                                         *pu4NextIeee8021BridgeServiceAccessPriorityReceived)
{
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (pu4NextIeee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityReceived);
    UNUSED_PARAM (pu4NextIeee8021BridgeServiceAccessPriorityReceived);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeServiceAccessPriorityValue
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived

                The Object 
                retValIeee8021BridgeServiceAccessPriorityValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeServiceAccessPriorityValue (UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityReceived,
                                                UINT4
                                                *pu4RetValIeee8021BridgeServiceAccessPriorityValue)
{
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityReceived);
    UNUSED_PARAM (pu4RetValIeee8021BridgeServiceAccessPriorityValue);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeServiceAccessPriorityValue
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived

                The Object 
                setValIeee8021BridgeServiceAccessPriorityValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeServiceAccessPriorityValue (UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                UINT4
                                                u4Ieee8021BridgeServiceAccessPriorityReceived,
                                                UINT4
                                                u4SetValIeee8021BridgeServiceAccessPriorityValue)
{
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityReceived);
    UNUSED_PARAM (u4SetValIeee8021BridgeServiceAccessPriorityValue);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeServiceAccessPriorityValue
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived

                The Object 
                testValIeee8021BridgeServiceAccessPriorityValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeServiceAccessPriorityValue (UINT4 *pu4ErrorCode,
                                                   UINT4
                                                   u4Ieee8021BridgeServiceAccessPriorityComponentId,
                                                   UINT4
                                                   u4Ieee8021BridgeServiceAccessPriorityPortNum,
                                                   UINT4
                                                   u4Ieee8021BridgeServiceAccessPriorityReceived,
                                                   UINT4
                                                   u4TestValIeee8021BridgeServiceAccessPriorityValue)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityPortNum);
    UNUSED_PARAM (u4Ieee8021BridgeServiceAccessPriorityReceived);
    UNUSED_PARAM (u4TestValIeee8021BridgeServiceAccessPriorityValue);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeServiceAccessPriorityTable
 Input       :  The Indices
                Ieee8021BridgeServiceAccessPriorityComponentId
                Ieee8021BridgeServiceAccessPriorityPortNum
                Ieee8021BridgeServiceAccessPriorityReceived
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeServiceAccessPriorityTable (UINT4 *pu4ErrorCode,
                                                  tSnmpIndexList *
                                                  pSnmpIndexList,
                                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortMrpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortMrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortMrpTable (UINT4
                                                    u4Ieee8021BridgeBasePortComponentId,
                                                    UINT4
                                                    u4Ieee8021BridgeBasePort)
{
    return (nmhValidateIndexInstanceFsMrpPortTable
            (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortMrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortMrpTable (UINT4
                                            *pu4Ieee8021BridgeBasePortComponentId,
                                            UINT4 *pu4Ieee8021BridgeBasePort)
{

    return (nmhGetFirstIndexFsMrpPortTable
            (pu4Ieee8021BridgeBasePortComponentId, pu4Ieee8021BridgeBasePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortMrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortMrpTable (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4
                                           *pu4NextIeee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           UINT4 *pu4NextIeee8021BridgeBasePort)
{
    return (nmhGetNextIndexFsMrpPortTable
            (u4Ieee8021BridgeBasePortComponentId,
             pu4NextIeee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort,
             pu4NextIeee8021BridgeBasePort));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMrpJoinTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMrpJoinTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMrpJoinTime (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     INT4
                                     *pi4RetValIeee8021BridgePortMrpJoinTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgePortMrpJoinTime = pMrpPortEntry->u4JoinTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMrpLeaveTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMrpLeaveTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMrpLeaveTime (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      INT4
                                      *pi4RetValIeee8021BridgePortMrpLeaveTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgePortMrpLeaveTime = pMrpPortEntry->u4LeaveTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMrpLeaveAllTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMrpLeaveAllTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMrpLeaveAllTime (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         INT4
                                         *pi4RetValIeee8021BridgePortMrpLeaveAllTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgePortMrpLeaveAllTime = pMrpPortEntry->u4LeaveAllTime;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortMrpJoinTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortMrpJoinTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortMrpJoinTime (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     INT4 i4SetValIeee8021BridgePortMrpJoinTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u4JoinTime = (UINT4) i4SetValIeee8021BridgePortMrpJoinTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortMrpLeaveTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortMrpLeaveTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortMrpLeaveTime (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      INT4
                                      i4SetValIeee8021BridgePortMrpLeaveTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u4LeaveTime = (UINT4) i4SetValIeee8021BridgePortMrpLeaveTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortMrpLeaveAllTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortMrpLeaveAllTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortMrpLeaveAllTime (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         INT4
                                         i4SetValIeee8021BridgePortMrpLeaveAllTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u4LeaveAllTime =
        (UINT4) i4SetValIeee8021BridgePortMrpLeaveAllTime;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortMrpJoinTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortMrpJoinTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortMrpJoinTime (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        INT4
                                        i4TestValIeee8021BridgePortMrpJoinTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        (UINT2) u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if (MRP_IS_JOIN_TIME_VALID ((UINT4) i4TestValIeee8021BridgePortMrpJoinTime,
                                pMrpPortEntry->u4LeaveTime) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_TIMER_VAL_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortMrpLeaveTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortMrpLeaveTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortMrpLeaveTime (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         INT4
                                         i4TestValIeee8021BridgePortMrpLeaveTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if (MRP_IS_LEAVE_TIME_VALID ((UINT4)
                                 i4TestValIeee8021BridgePortMrpLeaveTime,
                                 pMrpPortEntry->u4LeaveAllTime,
                                 pMrpPortEntry->u4JoinTime) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortMrpLeaveAllTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortMrpLeaveAllTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortMrpLeaveAllTime (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4TestValIeee8021BridgePortMrpLeaveAllTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry =
        MRP_GET_PORT_ENTRY (pMrpContextInfo, u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if (MRP_IS_LEAVE_ALL_TIME_VALID ((UINT4)
                                     i4TestValIeee8021BridgePortMrpLeaveAllTime,
                                     pMrpPortEntry->u4LeaveTime) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgePortMrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgePortMrpTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortMmrpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortMmrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortMmrpTable (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort)
{
    return (nmhValidateIndexInstanceFsMrpPortTable
            (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortMmrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortMmrpTable (UINT4
                                             *pu4Ieee8021BridgeBasePortComponentId,
                                             UINT4 *pu4Ieee8021BridgeBasePort)
{
    return (nmhGetFirstIndexFsMrpPortTable
            (pu4Ieee8021BridgeBasePortComponentId, pu4Ieee8021BridgeBasePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortMmrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortMmrpTable (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4
                                            *pu4NextIeee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            UINT4
                                            *pu4NextIeee8021BridgeBasePort)
{
    return (nmhGetNextIndexFsMrpPortTable
            (u4Ieee8021BridgeBasePortComponentId,
             pu4NextIeee8021BridgeBasePortComponentId,
             u4Ieee8021BridgeBasePort, pu4NextIeee8021BridgeBasePort));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMmrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMmrpEnabledStatus (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           *pi4RetValIeee8021BridgePortMmrpEnabledStatus)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgePortMmrpEnabledStatus =
        pMrpPortEntry->u1PortMmrpStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMmrpFailedRegistrations
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMmrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMmrpFailedRegistrations (UINT4
                                                 u4Ieee8021BridgeBasePortComponentId,
                                                 UINT4 u4Ieee8021BridgeBasePort,
                                                 tSNMP_COUNTER64_TYPE *
                                                 pu8RetValIeee8021BridgePortMmrpFailedRegistrations)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValIeee8021BridgePortMmrpFailedRegistrations->lsn =
        pMrpPortEntry->u4MmrpRegFailCnt;
    pu8RetValIeee8021BridgePortMmrpFailedRegistrations->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMmrpLastPduOrigin
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMmrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMmrpLastPduOrigin (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           tMacAddr *
                                           pRetValIeee8021BridgePortMmrpLastPduOrigin)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValIeee8021BridgePortMmrpLastPduOrigin,
            pMrpPortEntry->LastMmrpPduOrigin, MRP_MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortRestrictedGroupRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortRestrictedGroupRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortRestrictedGroupRegistration (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     INT4
                                                     *pi4RetValIeee8021BridgePortRestrictedGroupRegistration)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgePortRestrictedGroupRegistration =
        pMrpPortEntry->u1RestrictedMACRegCtrl;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortMmrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortMmrpEnabledStatus (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4SetValIeee8021BridgePortMmrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pMrpPortEntry->u1PortMmrpStatus ==
        i4SetValIeee8021BridgePortMmrpEnabledStatus)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValIeee8021BridgePortMmrpEnabledStatus)
    {

        case MRP_ENABLED:

            if (MrpMmrpEnablePort (pMrpContextInfo,
                                   (UINT2) u4Ieee8021BridgeBasePort) ==
                OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

        case MRP_DISABLED:

            if (MrpMmrpDisablePort (pMrpContextInfo,
                                    (UINT2) u4Ieee8021BridgeBasePort) ==
                OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }

            break;

        default:
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortRestrictedGroupRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortRestrictedGroupRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortRestrictedGroupRegistration (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     INT4
                                                     i4SetValIeee8021BridgePortRestrictedGroupRegistration)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u1RestrictedMACRegCtrl =
        (UINT1) i4SetValIeee8021BridgePortRestrictedGroupRegistration;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortMmrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortMmrpEnabledStatus (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4TestValIeee8021BridgePortMmrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1TunnelStatus = VLAN_TUNNEL_PROTOCOL_INVALID;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry =
        MRP_GET_PORT_ENTRY (pMrpContextInfo, u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021BridgePortMmrpEnabledStatus != MRP_ENABLED) &&
        (i4TestValIeee8021BridgePortMmrpEnabledStatus != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    if (i4TestValIeee8021BridgePortMmrpEnabledStatus == MRP_ENABLED)
    {
        if (pMrpContextInfo->u4BridgeMode == MRP_PROVIDER_BRIDGE_MODE)
        {
            MrpPortGetPortVlanTunnelStatus (pMrpPortEntry->u4IfIndex,
                                            (BOOL1 *) & u1TunnelStatus);

            if (u1TunnelStatus == OSIX_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_MMRP_PROTO_ENABLED_ERR);
                return SNMP_FAILURE;
            }
        }

        if (pMrpContextInfo->u4BridgeMode == MRP_CUSTOMER_BRIDGE_MODE)
        {
            MrpPortGetProtoTunelStatusOnPort (pMrpPortEntry->u4IfIndex,
                                              L2_PROTO_MMRP, &u1TunnelStatus);

            if ((u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
                (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_MMRP_TUNNEL_ENABLED_ERR);
                return SNMP_FAILURE;
            }
        }

        if ((MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
            (MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))

        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_PORT_MMRP_ENABLED_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortRestrictedGroupRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortRestrictedGroupRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortRestrictedGroupRegistration (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePort,
                                                        INT4
                                                        i4TestValIeee8021BridgePortRestrictedGroupRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021BridgePortRestrictedGroupRegistration !=
         MRP_ENABLED)
        && (i4TestValIeee8021BridgePortRestrictedGroupRegistration !=
            MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgePortMmrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgePortMmrpTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeILanIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeILanIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeILanIfTable (INT4 i4IfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeILanIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeILanIfTable (INT4 *pi4IfIndex)
{
    UNUSED_PARAM (pi4IfIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeILanIfTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeILanIfTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4NextIfIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeILanIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                retValIeee8021BridgeILanIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeILanIfRowStatus (INT4 i4IfIndex,
                                     INT4
                                     *pi4RetValIeee8021BridgeILanIfRowStatus)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pi4RetValIeee8021BridgeILanIfRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeILanIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                setValIeee8021BridgeILanIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeILanIfRowStatus (INT4 i4IfIndex,
                                     INT4 i4SetValIeee8021BridgeILanIfRowStatus)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4SetValIeee8021BridgeILanIfRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeILanIfRowStatus
 Input       :  The Indices
                IfIndex

                The Object 
                testValIeee8021BridgeILanIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeILanIfRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        INT4
                                        i4TestValIeee8021BridgeILanIfRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4TestValIeee8021BridgeILanIfRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeILanIfTable
 Input       :  The Indices
                IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeILanIfTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgeDot1dPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgeDot1dPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgeDot1dPortTable (UINT4
                                                      u4Ieee8021BridgeBasePortComponentId,
                                                      UINT4
                                                      u4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgeDot1dPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgeDot1dPortTable (UINT4
                                              *pu4Ieee8021BridgeBasePortComponentId,
                                              UINT4 *pu4Ieee8021BridgeBasePort)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgeDot1dPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgeDot1dPortTable (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4
                                             *pu4NextIeee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             UINT4
                                             *pu4NextIeee8021BridgeBasePort)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgeDot1dPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgeDot1dPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgeDot1dPortRowStatus (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        INT4
                                        *pi4RetValIeee8021BridgeDot1dPortRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021BridgeDot1dPortRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgeDot1dPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgeDot1dPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgeDot1dPortRowStatus (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        INT4
                                        i4SetValIeee8021BridgeDot1dPortRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021BridgeDot1dPortRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgeDot1dPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgeDot1dPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgeDot1dPortRowStatus (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4TestValIeee8021BridgeDot1dPortRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021BridgeDot1dPortRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgeDot1dPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgeDot1dPortTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
