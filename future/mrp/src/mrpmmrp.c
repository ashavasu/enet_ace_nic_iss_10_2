/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpmmrp.c,v 1.17 2013/06/25 12:08:37 siva Exp $
 *
 * Description: This file contains MMRP specific routines.
 ******************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpInit
 *                                                                          
 *    DESCRIPTION      : This function initializes the MMRP application
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpInit (tMrpContextInfo * pMrpContextInfo)
{
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT2               u2Port = 0;

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

        if (pMrpPortEntry != NULL)
        {
            MrpMmrpSetPortMmrpStatus (pMrpPortEntry);
            pMrpPortEntry->u1RestrictedMACRegCtrl = MRP_DISABLED;
        }
    }

    MrpMmrpEnable (pMrpContextInfo);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpEnable
 *                                                                          
 *    DESCRIPTION      : This function enables the MMRP application
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpEnable (tMrpContextInfo * pMrpContextInfo)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpHwInfo          MrpHwInfo;

    MEMSET (&MrpHwInfo, 0, sizeof (tMrpHwInfo));

    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpEnable: MRP is not started. Failed. \n"));
        return OSIX_FAILURE;
    }

    if (MrpMmrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_TRUE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpEnable: Mmrp Already enabled. \n"));
        return OSIX_SUCCESS;
    }

    pMrpContextInfo->u1MmrpAdminStatus = MRP_ENABLED;

    /* Configurations are restored before GO_ACTIVE/GO_STANDBY event is
     * received by protocol. Hence if MMRP enable is called before GO_ACTIVE/
     * GO_STANDBY event is received, then store that in u1MmrpAdminStatus. In
     * MrpRedMakeNodeActiveFromIdle/MrpRedMakeNodeStandbyFromIdle based on the
     * value of u1MmrpAdminStatus, enable/disable MMRP.
     */
    if (MRP_RED_NODE_STATUS () == RM_INIT)
    {
        return OSIX_SUCCESS;
    }

#ifdef NPAPI_WANTED
    if (MRP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
    {
        /* For accessing Other module's data struture From MRP module,
         ** we should decrement the MRP Context Id by 1.*/
        MrpHwInfo.u4ContextId = pMrpContextInfo->u4ContextId - 1;
        MrpHwInfo.u4Status = MRP_ENABLED;

        if (FsMiMrpHwSetMmrpStatus (&MrpHwInfo) != FNP_SUCCESS)
        {
            MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                      "MrpMmrpEnable: " "NP Programming Failed. \n"));
            return OSIX_FAILURE;
        }
    }
#endif

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MMRP_APP_ID);

    if (MrpAppInitMrpAppEntry (pMrpContextInfo, pMrpAppEntry,
                               MRP_MMRP_APP_ID) == OSIX_FAILURE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpEnable: "
                  "Initalizaing MRP Application entry Failed. \n"));
        return OSIX_FAILURE;
    }

    /* Enabling MMRP on the ports present in the context */
    if (MrpAppAddAndEnablePortsInAppl (pMrpContextInfo,
                                       pMrpAppEntry) == OSIX_FAILURE)
    {
        MrpAppDeEnrolMrpApplication (pMrpContextInfo, MRP_MMRP_APP_ID);
        MrpAppDeInitMrpAppEntry (pMrpAppEntry);
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpEnable: "
                  "Initalizaing MRP Application entry Failed. \n"));
        return OSIX_FAILURE;
    }

    gMrpGlobalInfo.au1MmrpStatus[pMrpContextInfo->u4ContextId] = MRP_ENABLED;
    /* Static MAC information might be present in VLAN module, before MMRP 
     * is enabled. So indicate VLAN module to propagate the MAC information. */
    MrpPortVlanIndMmrpEnable (pMrpContextInfo->u4ContextId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpDisable
 *                                                                          
 *    DESCRIPTION      : This function disables the MMRP application
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpDisable (tMrpContextInfo * pMrpContextInfo)
{
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpHwInfo          MrpHwInfo;
    UINT2               u2Port = 0;
    UINT2               u2MapId = 0;

    MEMSET (&MrpHwInfo, 0, sizeof (tMrpHwInfo));

    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo,
                  (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpDisable: MRP is not started. \n"));
        return OSIX_SUCCESS;
    }

    if (MrpMmrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo,
                  (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpDisable: Mmrp Already disabled. \n"));
        return OSIX_SUCCESS;
    }

#ifdef NPAPI_WANTED
    /* For accessing Other module's data struture From MRP module,
     * we should decrement the MRP Context Id by 1.*/
    if (MRP_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
    {
        MrpHwInfo.u4ContextId = pMrpContextInfo->u4ContextId - 1;
        MrpHwInfo.u4Status = MRP_DISABLED;

        if (FsMiMrpHwSetMmrpStatus (&MrpHwInfo) != FNP_SUCCESS)
        {
            MRP_TRC ((pMrpContextInfo,
                      (MRP_MMRP_TRC | INIT_SHUT_TRC | MRP_CRITICAL_TRC),
                      "MrpMmrpDisable: NP Programming Failed. \n"));
            return OSIX_FAILURE;
        }
    }
#endif

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MMRP_APP_ID);

    MrpPortVlanDelAllDynamicMacInfo (pMrpContextInfo->u4ContextId,
                                     VLAN_INVALID_PORT);
    MrpPortDelAllDynamicDefGroupInfo (pMrpContextInfo->u4ContextId,
                                      VLAN_INVALID_PORT);

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pMrpAppEntry, u2Port);

        if (pMrpAppPortEntry != NULL)
        {
            MrpAppDelPortFromAppl (pMrpContextInfo, MRP_MMRP_APP_ID, u2Port);
        }
    }

    for (u2MapId = 1; u2MapId < pMrpAppEntry->u2MapArraySize; u2MapId++)
    {
        pMrpMapEntry = pMrpAppEntry->ppMapTable[u2MapId];

        if (pMrpMapEntry == NULL)
        {
            continue;
        }

        MrpMapDSDeleteMrpMapEntry (pMrpMapEntry);
    }

    MrpAppDeInitMrpAppEntry (pMrpAppEntry);

    gMrpGlobalInfo.au1MmrpStatus[pMrpContextInfo->u4ContextId] = MRP_DISABLED;

    MrpPortVlanIndMmrpDisable (pMrpContextInfo->u4ContextId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpAddPort
 *                                                                          
 *    DESCRIPTION      : This function adds a port to the MMRP application
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                       u2Port          - Local Port Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpAddPort (tMrpContextInfo * pMrpContextInfo, UINT2 u2Port)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MrpMmrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpAddPort: Mmrp is disabled. \n"));
        return OSIX_SUCCESS;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MMRP_APP_ID);
    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

    if (pMrpPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpAddPort: MRP Port entry not found. \n"));
        return OSIX_FAILURE;

    }

    if (MrpAppAddPortToAppl (pMrpAppEntry, u2Port) == OSIX_FAILURE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpAddPort: Adding port %d to Application "
                  "table failed \n", u2Port));
        return OSIX_FAILURE;
    }

    if (pMrpPortEntry->u1PortMmrpStatus == MRP_ENABLED)
    {
        /* Static MAC information might be present in VLAN module, before MMRP 
         * is enabled. So indicate VLAN module to propagate the 
         * MAC information. */
        MrpPortVlanIndPortMmrpEnable (pMrpContextInfo->u4ContextId, u2Port);
        if (MrpAppEnableApplOnPort (pMrpContextInfo, pMrpAppEntry,
                                    u2Port) == OSIX_FAILURE)
        {
            MrpAppDelPortFromAppl (pMrpContextInfo,
                                   pMrpAppEntry->u1AppId, u2Port);
            MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                      "MrpMmrpAddPort: Enabling the application on "
                      "port %d failed \n", u2Port));
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpDelPort
 *                                                                          
 *    DESCRIPTION      : This function deletes a port from MMRP application.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                       u2Port          - Local Port Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpDelPort (tMrpContextInfo * pMrpContextInfo, UINT2 u2Port)
{
    if (MrpMmrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpDelPort: " "Mmrp is disabled. \n"));
        return OSIX_SUCCESS;
    }

    MrpPortVlanDelAllDynamicMacInfo (pMrpContextInfo->u4ContextId, u2Port);
    MrpPortDelAllDynamicDefGroupInfo (pMrpContextInfo->u4ContextId, u2Port);
    MrpAppDelPortFromAppl (pMrpContextInfo, MRP_MMRP_APP_ID, u2Port);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpEnablePort
 *                                                                          
 *    DESCRIPTION      : This function enables MMRP application on the port.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                       u2Port          - Local Port Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpEnablePort (tMrpContextInfo * pMrpContextInfo, UINT2 u2Port)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpEnablePort: " "MRP is shutdown. \n"));
        return OSIX_FAILURE;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MMRP_APP_ID);
    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

    if (pMrpPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpEnablePort: Port %d not created. \n", u2Port));
        return OSIX_FAILURE;
    }

    if (MrpMmrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        pMrpPortEntry->u1PortMmrpStatus = MRP_ENABLED;

        /* Update VLAN port table with port MMRP status because port type 
         * should not be set as access port on MMRP enabled port. */
        MrpPortSetProtoEnabledStatOnPort
            (pMrpContextInfo->u4ContextId, u2Port, L2_PROTO_MMRP, OSIX_ENABLED);
        return OSIX_SUCCESS;
    }

    if (pMrpPortEntry->u1PortMmrpStatus == MRP_ENABLED)
    {
        return OSIX_SUCCESS;
    }

    if (pMrpPortEntry->u1OperStatus == MRP_OPER_UP)
    {
        /* Static MAC information might be present in VLAN module, before MMRP 
         * is enabled. So indicate VLAN module to propagate 
         * the MAC information. */
        MrpPortVlanIndPortMmrpEnable (pMrpContextInfo->u4ContextId, u2Port);
        if (MrpAppEnableApplOnPort (pMrpContextInfo, pMrpAppEntry,
                                    u2Port) == OSIX_FAILURE)
        {
            MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                      "MrpMmrpEnable: Enabling the application on "
                      "port %d failed \n", u2Port));
            return OSIX_FAILURE;
        }
    }

    pMrpPortEntry->u1PortMmrpStatus = MRP_ENABLED;

    MrpPortSetProtoEnabledStatOnPort (pMrpContextInfo->u4ContextId,
                                      u2Port, L2_PROTO_MMRP, OSIX_ENABLED);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpDisablePort
 *                                                                          
 *    DESCRIPTION      : This function disables MMRP application on the port.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                       u2Port          - Local Port Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpDisablePort (tMrpContextInfo * pMrpContextInfo, UINT2 u2Port)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpDisablePort: " "MRP is shutdown. \n"));
        return OSIX_FAILURE;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MMRP_APP_ID);
    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

    if (pMrpPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                  "MrpMmrpEnablePort: Port %d not created. \n", u2Port));
        return OSIX_FAILURE;
    }

    if (MrpMmrpIsEnabled (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        pMrpPortEntry->u1PortMmrpStatus = MRP_DISABLED;

        /* Update VLAN port table with port MMRP status because port type 
         * should not be set as access port on MMRP enabled port. */
        MrpPortSetProtoEnabledStatOnPort
            (pMrpContextInfo->u4ContextId, u2Port, L2_PROTO_MMRP,
             OSIX_DISABLED);
        return OSIX_SUCCESS;
    }

    if (pMrpPortEntry->u1PortMmrpStatus == MRP_DISABLED)
    {
        return OSIX_SUCCESS;
    }

    if (pMrpPortEntry->u1OperStatus == MRP_OPER_UP)
    {

        MrpPortVlanDelAllDynamicMacInfo (pMrpContextInfo->u4ContextId, u2Port);

        MrpPortDelAllDynamicDefGroupInfo (pMrpContextInfo->u4ContextId, u2Port);
        if (MrpAppDisableApplOnPort (pMrpContextInfo, pMrpAppEntry,
                                     u2Port) == OSIX_FAILURE)
        {
            MRP_TRC ((pMrpContextInfo, (MRP_MMRP_TRC | INIT_SHUT_TRC),
                      "MrpMmrpEnable: Enabling the application"
                      "on port %d failed \n", u2Port));
            return OSIX_FAILURE;
        }
    }

    pMrpPortEntry->u1PortMmrpStatus = MRP_DISABLED;

    MrpPortSetProtoEnabledStatOnPort (pMrpContextInfo->u4ContextId,
                                      u2Port, L2_PROTO_MMRP, OSIX_DISABLED);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpSetPortMmrpStatus
 *                                                                          
 *    DESCRIPTION      : This function sets the MMRP status for the ports 
 *                       by looking at the following things:
 *                        - Bridge Port Type
 *                        - Vlan Port type (ACCESS/TRUNK/HYBRID)
 *                        - Port Tunnel Status
 *                        - Protocol Tunnel Status
 *
 *    INPUT            : pMrpPortEntry - Pointer to the tMrpPortEntry 
 *                                       structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
VOID
MrpMmrpSetPortMmrpStatus (tMrpPortEntry * pMrpPortEntry)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;
    UINT1               u1TunnelStatus = VLAN_TUNNEL_PROTOCOL_INVALID;
    UINT1               u1PortMmrpStatus = 0;

    pMrpContextInfo = pMrpPortEntry->pMrpContextInfo;

    if (MRP_IS_802_1AD_1AH_BRIDGE (pMrpContextInfo) == OSIX_TRUE)
    {
        /* In case of 1ad & 1ah bridge, MMRP cannot be enabled on 
         * Customer ports. */
        if (((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry)) == OSIX_TRUE) ||
            ((MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry)) == OSIX_TRUE))
        {
            pMrpPortEntry->u1PortMmrpStatus = MRP_DISABLED;
        }
        else
        {
            pMrpPortEntry->u1PortMmrpStatus = MRP_ENABLED;
        }
    }
    else
    {
        pMrpPortEntry->u1PortMmrpStatus = MRP_ENABLED;
    }

    /* In case of customer bridge mode, If the Protocol tunnel
     * status is tunnel/discard, MMRP should be disabled on that port */
    if (pMrpContextInfo->u4BridgeMode == MRP_CUSTOMER_BRIDGE_MODE)
    {
        MrpPortGetProtoTunelStatusOnPort (pMrpPortEntry->u4IfIndex,
                                          L2_PROTO_MMRP, &u1TunnelStatus);

        if (u1TunnelStatus != VLAN_TUNNEL_PROTOCOL_PEER)
        {
            pMrpPortEntry->u1PortMmrpStatus = MRP_DISABLED;
        }
    }

    /* In case of provider bridge mode, If the tunnel status of 
     * the port is enabled then MMRP should be disabled on that port */
    if (pMrpContextInfo->u4BridgeMode == MRP_PROVIDER_BRIDGE_MODE)
    {
        MrpPortGetPortVlanTunnelStatus (pMrpPortEntry->u4IfIndex,
                                        (BOOL1 *) & u1TunnelStatus);

        if (u1TunnelStatus == OSIX_TRUE)
        {
            pMrpPortEntry->u1PortMmrpStatus = MRP_DISABLED;
        }
    }

    u4ContextId = pMrpPortEntry->pMrpContextInfo->u4ContextId;

    if (pMrpPortEntry->u1PortMmrpStatus == MRP_ENABLED)
    {
        u1PortMmrpStatus = OSIX_ENABLED;
    }
    else
    {
        u1PortMmrpStatus = OSIX_DISABLED;
    }

    MrpPortSetProtoEnabledStatOnPort (u4ContextId, pMrpPortEntry->u2LocalPortId,
                                      L2_PROTO_MMRP, u1PortMmrpStatus);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpAttrIndFn
 *                                                                          
 *    DESCRIPTION      : Registered function for Indicating the registration/ 
 *                       de-registration to the Higher layer application.
 *
 *    INPUT            : pMapEntry  - Pointer to the MAP entry
 *                       Attr       - Attribute Information
 *                       u2Port     - Port number of the incoming port
 *                       u1IndType  - MRP_HL_IND_NEW/MRP_HL_IND_JOIN/
 *                                    MRP_HL_IND_LEAVE
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpAttrIndFn (tMrpMapEntry * pMapEntry, tMrpAttr Attr, UINT2 u2Port,
                  UINT1 u1IndType)
{
    tMrpContextInfo    *pContextInfo = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    pContextInfo = pMapEntry->pMrpAppEntry->pMrpContextInfo;

    switch (u1IndType)
    {
        case MRP_HL_IND_JOIN:
        case MRP_HL_IND_NEW:
            i4RetVal = MrpMmrpJoinOrNewInd (&Attr, pContextInfo, u2Port,
                                            pMapEntry->u2MapId);
            break;
        case MRP_HL_IND_LEAVE:
            i4RetVal = MrpMmrpLeaveInd (&Attr, pContextInfo, u2Port,
                                        pMapEntry->u2MapId);
            break;
        default:
            i4RetVal = OSIX_FAILURE;
            break;
    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpLeaveInd
 *                                                                          
 *    DESCRIPTION      : Registered function to indicate the de-registration 
 *                       of the Attribute value to the Higher layer application.
 *
 *    INPUT            : pAttr         - Attribute Information
 *                       pContextInfo  - Pointer to Context Info pointer
 *                       u2Port        - Local Port Number
 *                       u2MapId       - MAP Context Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpLeaveInd (tMrpAttr * pAttr, tMrpContextInfo * pContextInfo, UINT2 u2Port,
                 UINT2 u2MapId)
{
    tMacAddr            MacAddr;
    tMrpRedMsgInfo      SyncUpMsgInfo;
    INT4                i4RetVal = VLAN_SUCCESS;
#ifdef  VLAN_EXTENDED_FILTER
    UINT1               u1Type = 0;
#endif /* VLAN_EXTENDED_FILTER */
    UINT1               u1RedMsgType = 0;

    MEMSET (&SyncUpMsgInfo, 0, sizeof (tMrpRedMsgInfo));
    if (MRP_MAC_ADDR_ATTR_TYPE == pAttr->u1AttrType)
    {
        MEMCPY (MacAddr, pAttr->au1AttrVal, MRP_MAC_ADDR_LEN);
        i4RetVal = MrpPortVlanUpdateDynamicMACInfo
            (pContextInfo->u4ContextId, MacAddr, u2MapId,
             MRP_GET_PHY_PORT (pContextInfo->u4ContextId, u2Port), VLAN_DELETE);

        if (VLAN_FAILURE == i4RetVal)
        {

            MRP_TRC ((pContextInfo, (ALL_FAILURE_TRC | MRP_MMRP_TRC),
                      "MrpMmrpAttrIndFn: Failed to update Dynamic"
                      " MAC Information for vlan %d on Port %d\n",
                      u2MapId, u2Port));
            MRP_PRINT_ATTRIBUTE (pAttr);

            return OSIX_FAILURE;
        }
        MEMCPY (SyncUpMsgInfo.au1MmrpAttrVal, pAttr->au1AttrVal,
                MRP_MAC_ADDR_LEN);
        u1RedMsgType = MRP_RED_MAC_DEL_MSG;

        if (MrpMmrpProcessPvlanJoinOrLeave
            (pContextInfo->u4ContextId, MacAddr, u2MapId,
             (UINT2) (MRP_GET_PHY_PORT (pContextInfo->u4ContextId, u2Port)),
             VLAN_DELETE) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
#ifdef VLAN_EXTENDED_FILTER
    else if (MRP_SERVICE_REQ_ATTR_TYPE == pAttr->u1AttrType)
    {
        if (MRP_MMRP_ALL_GROUPS == pAttr->au1AttrVal[0])
        {
            u1Type = MRP_MMRP_ALL_GROUPS;

            MRP_TRC ((pContextInfo,
                      (MRP_REG_SEM_TRC | MRP_PDU_TRC | MRP_MMRP_TRC),
                      "MrpMmrpAttrIndFn: Join or New Ind received for ALL"
                      " Groups on Port %d for VLAN ID %d\n", u2Port, u2MapId));

        }
        else
        {
            u1Type = MRP_MMRP_UNREG_GROUPS;
            MRP_TRC ((pContextInfo, (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
                      "MrpMmrpAttrIndFn: Join Ind received for ALL"
                      " UnReg Groups on Port %d\n",
                      MRP_GET_PHY_PORT (pContextInfo->u4ContextId, u2Port)));
        }

        i4RetVal =
            MrpPortVlanUpdateDynDefGrpInfo (pContextInfo->u4ContextId, u1Type,
                                            u2MapId, u2Port, VLAN_DELETE);
        if (VLAN_FAILURE == i4RetVal)
        {

            MRP_TRC ((pContextInfo, (ALL_FAILURE_TRC | MRP_MMRP_TRC),
                      "MrpMmrpAttrIndFn: Failed to update Dynamic"
                      " Default Group Info for vlan %d on Port %d\n",
                      u2MapId, u2Port));
            MRP_PRINT_ATTRIBUTE (pAttr);
            return OSIX_FAILURE;
        }
        SyncUpMsgInfo.au1MmrpAttrVal[0] = pAttr->au1AttrVal[0];
        u1RedMsgType = MRP_RED_SERVICE_REQ_DEL_MSG;
    }
#endif
    SyncUpMsgInfo.u4ContextId = pContextInfo->u4ContextId;
    SyncUpMsgInfo.u2LocalPortId = u2Port;
    SyncUpMsgInfo.VlanId = u2MapId;

    MrpRedSendSyncUpMessages (u1RedMsgType, &SyncUpMsgInfo);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpJoinOrNewInd
 *                                                                          
 *    DESCRIPTION      : Registered function to indicate Higher Layer about
 *                       the registration of the attribute value.
 *
 *    INPUT            : pAttr         - Attribute Information 
 *                       pContextInfo  - Pointer to Context Info structure
 *                       u2Port        - Local Port Number
 *                       u2MapId       - MAP Context Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpJoinOrNewInd (tMrpAttr * pAttr, tMrpContextInfo * pContextInfo,
                     UINT2 u2Port, UINT2 u2MapId)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpTrapInfo        TrapInfo;
    tMrpRedMsgInfo      SyncUpMsgInfo;
    tMacAddr            MacAddr;
    INT4                i4RetVal = VLAN_SUCCESS;
#ifdef VLAN_EXTENDED_FILTER
    UINT1               u1Type = 0;
#endif
    UINT1               u1RedMsgType = 0;

    MEMSET (&SyncUpMsgInfo, 0, sizeof (tMrpRedMsgInfo));

    if (MRP_MAC_ADDR_ATTR_TYPE == pAttr->u1AttrType)
    {
        MEMCPY (MacAddr, pAttr->au1AttrVal, MRP_MAC_ADDR_LEN);
        i4RetVal = MrpPortVlanUpdateDynamicMACInfo
            (pContextInfo->u4ContextId, MacAddr, u2MapId,
             MRP_GET_PHY_PORT (pContextInfo->u4ContextId, u2Port), VLAN_ADD);

        if (VLAN_FAILURE == i4RetVal)
        {
            TrapInfo.pAttr = pAttr;
            TrapInfo.u4ContextId = pContextInfo->u4ContextId;
            TrapInfo.u2PortId = u2Port;
            TrapInfo.u1ReasonType = MRP_LACK_OF_RESOURCE;
            MrpTrapNotifyVlanOrMacRegFails ((VOID *) &TrapInfo);
            pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);
            MRP_CHK_NULL_PTR_RET (pPortEntry, OSIX_FAILURE);
            pPortEntry->u4MmrpRegFailCnt++;

            MRP_TRC ((pContextInfo, (ALL_FAILURE_TRC | MRP_MMRP_TRC),
                      "MrpMmrpAttrIndFn: Failed to update Dynamic MAC "
                      "Information for vlan %d on Port %d\n", u2MapId, u2Port));

            return OSIX_FAILURE;
        }
        MEMCPY (SyncUpMsgInfo.au1MmrpAttrVal, pAttr->au1AttrVal, MAC_ADDR_LEN);
        u1RedMsgType = MRP_RED_MAC_ADD_MSG;

        if (MrpMmrpProcessPvlanJoinOrLeave
            (pContextInfo->u4ContextId, MacAddr, u2MapId,
             (UINT2) (MRP_GET_PHY_PORT (pContextInfo->u4ContextId, u2Port)),
             VLAN_ADD) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
#ifdef VLAN_EXTENDED_FILTER
    else if (MRP_SERVICE_REQ_ATTR_TYPE == pAttr->u1AttrType)
    {
        if (MRP_MMRP_ALL_GROUPS == pAttr->au1AttrVal[0])
        {
            u1Type = MRP_MMRP_ALL_GROUPS;

            MRP_TRC ((pContextInfo,
                      (MRP_REG_SEM_TRC | MRP_PDU_TRC | MRP_MMRP_TRC),
                      "MrpMmrpAttrIndFn: Join or New Ind received for ALL"
                      " Groups on Port %d for VLAN Id %d\n", u2Port, u2MapId));

        }
        else
        {
            u1Type = MRP_MMRP_UNREG_GROUPS;

            MRP_TRC ((pContextInfo,
                      (MRP_REG_SEM_TRC | MRP_PDU_TRC | MRP_MMRP_TRC),
                      "MrpMmrpAttrIndFn: Join or New Ind received for ALL"
                      " Unreg Groups on Port %d for VLAN Id %d\n", u2Port,
                      u2MapId));

        }

        i4RetVal = MrpPortVlanUpdateDynDefGrpInfo (pContextInfo->u4ContextId,
                                                   u1Type, u2MapId, u2Port,
                                                   VLAN_ADD);
        if (VLAN_FAILURE == i4RetVal)
        {
            MRP_TRC ((pContextInfo, (ALL_FAILURE_TRC | MRP_MMRP_TRC),
                      "MrpMmrpAttrIndFn: Failed to update Dynamic"
                      " Default Group information for Vlan %d on"
                      " Port %d\n", u2MapId,
                      MRP_GET_PHY_PORT (pContextInfo->u4ContextId, u2Port)));
            return OSIX_FAILURE;
        }
        SyncUpMsgInfo.au1MmrpAttrVal[0] = pAttr->au1AttrVal[0];
        u1RedMsgType = MRP_RED_SERVICE_REQ_ADD_MSG;
    }
#endif
    SyncUpMsgInfo.u4ContextId = pContextInfo->u4ContextId;
    SyncUpMsgInfo.u2LocalPortId = u2Port;
    SyncUpMsgInfo.VlanId = u2MapId;

    MrpRedSendSyncUpMessages (u1RedMsgType, &SyncUpMsgInfo);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpIsEnabled
 *                                                                          
 *    DESCRIPTION      : This function returns the status of the MMRP
 *                       application.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpIsEnabled (UINT4 u4ContextId)
{
    if ((u4ContextId != 0) && (u4ContextId < MRP_SIZING_CONTEXT_COUNT))
    {
        return ((gMrpGlobalInfo.au1MmrpStatus[u4ContextId] == MRP_ENABLED)
                ? OSIX_TRUE : OSIX_FALSE);
    }
    return OSIX_FALSE;
}

/****************************************************************************
 * 
 *  FUNCTION NAME   : MrpMmrpTypeValidateFn                    
 *  
 *    DESCRIPTION      : This function is called by MRP module to check whether
 *                       the attribute type is a valid MMRP attribute type.
 *                    
 *    INPUT            : u1AttrType - Attribute type
 *  
 *    OUTPUT           : None
 *  
 *  RETURNS         : OSIX_TRUE / OSIX_FALSE                   
 *  
 ****************************************************************************/
INT4
MrpMmrpTypeValidateFn (UINT1 u1AttrType)
{
    if ((u1AttrType == MRP_MAC_ADDR_ATTR_TYPE) ||
        (u1AttrType == MRP_SERVICE_REQ_ATTR_TYPE))
    {
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

/****************************************************************************
 * 
 *  FUNCTION NAME   : MrpMmrpWildCardAttrRegValidateFn            
 *  
 *    DESCRIPTION      : This function calls a function in VLAN module and 
 *                       checks if the attribute is already registered as 
 *                       wild card entry. Learning will not be allowed for 
 *                       MAC address,if wild card entry is present.
 *                                                             
 *    INPUT            : u4ContextId - Context Identifier
 *                       Attr        - Structure containing attribute type, 
 *                                     length, value.
 *                       u2Port      - Port Number on which Registration 
 *                                     NORMAL for attribute contained in Attr 
 *                                     to be checked
 *                       u2MapId     - MAP Identifier
 *                                                             
 *    OUTPUT           : None
 *                                                             
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                             
 ****************************************************************************/
INT4
MrpMmrpWildCardAttrRegValidateFn (UINT4 u4ContextId, tMrpAttr Attr,
                                  UINT2 u2Port, UINT2 u2MapId)
{
    tMacAddr            MacAddr;
    INT4                i4RetVal = OSIX_TRUE;

    UNUSED_PARAM (u2Port);

    if ((Attr.u1AttrType == MRP_MAC_ADDR_ATTR_TYPE) &&
        (Attr.u1AttrLen == MRP_MAC_ADDR_LEN))
    {
        MEMCPY (MacAddr, Attr.au1AttrVal, MRP_MAC_ADDR_LEN);

        i4RetVal = MrpPortIsMACWildCardEntryPresent (u4ContextId, u2MapId,
                                                     MacAddr);
    }

    return i4RetVal;
}

/****************************************************************************
 *
 *  FUNCTION NAME   : MrpMmrpLenValidateFn                       
 *  
 *    DESCRIPTION      : This function is called by MRP module to check whether
 *                       the attribute length is a valid MMRP attribute length.
 *
 *    INPUT            : u1AttrType - Attribute type
 *                   u1AttrLen  - Attribute Length
 *
 *    OUTPUT           : None
 *
 *  RETURNS         : OSIX_TRUE / OSIX_FALSE                   
 *
 ****************************************************************************/
INT4
MrpMmrpLenValidateFn (UINT1 u1AttrType, UINT1 u1AttrLen)
{
    if (((u1AttrType == MRP_MAC_ADDR_ATTR_TYPE) &&
         (u1AttrLen == MRP_MAC_ADDR_LEN)) ||
        ((u1AttrType == MRP_SERVICE_REQ_ATTR_TYPE) &&
         (u1AttrLen == MRP_SERVICE_REQ_LEN)))
    {
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpHandleJoinTmrExpiry
 *                                                                          
 *    DESCRIPTION      : This function handles the join timer expiry for 
 *                       MMRP application.
 *
 *    INPUT            : pMrpContextInfo  - Pointer to the Context info 
 *                                          structure.
 *                       pMrpAppPortEntry - Pointer to the Application Port 
 *                                          entry.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMmrpHandleJoinTmrExpiry (UINT4 u4ContextId,
                            tMrpAppPortEntry * pMrpAppPortEntry)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT2               u2MapId = 0;
    UINT2               u2Port = 0;

    pMrpContextInfo = gMrpGlobalInfo.apContextInfo[u4ContextId];
    u2Port = pMrpAppPortEntry->pMrpPortEntry->u2LocalPortId;
    pAppEntry = pMrpAppPortEntry->pMrpAppEntry;
    pPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

    if (pPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MMRP_TRC),
                  "MrpMvrpHandleJoinTmrExpiry: Port Entry "
                  "not found for port %d.\n", u2Port));
        return;
    }

    for (u2MapId = 1; u2MapId < pAppEntry->u2MapArraySize; u2MapId++)
    {
        pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

        if (pMapEntry == NULL)
        {
            continue;
        }

        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

        if (pMapPortEntry == NULL)
        {
            continue;
        }

        /* Form and transmit MMRPDU for each MAP ID */
        MrpMmrpFormAndTransmitPdu (pMrpContextInfo, pMrpAppPortEntry,
                                   pPortEntry, pMapPortEntry);
    }

    if (pMrpAppPortEntry->u1LeaveAllSemState == MRP_ACTIVE)
    {
        pMrpAppPortEntry->u1LeaveAllSemState = MRP_PASSIVE;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpFormVectAttr
 *                                                                          
 *    DESCRIPTION      : This function forms the Vector. For the given MMRP   
 *                       attribute value it gets the corresponding Attribute
 *                       Event and encodes it in the Vector.
 *
 *    INPUT            : pVectBuf         - Pointer to start of the Vector field 
 *                       pVectAttrInf     - Pointer to the  VectorAttrInfo
 *                       pu1AddVectHdr    - Set to OSIX_TRUE when end of the 
 *                                          Vector for this Vector Attribute is
 *                                          reached
 *                       pu2NumOfValues   - Number of Attribute values encoded 
 *                                          in this Vector Attribute
 *                       pu1IsFirstValSet - Set to OSIX_TRUE on determining
 *                                          the 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to the first attribute entry
 *                                                                          
 ****************************************************************************/
VOID
MrpMmrpFormVectAttr (UINT1 **ppBuf, tMrpVectAttrInfo * pVectAttrInfo,
                     UINT1 *pu1AddVectHdr, UINT2 *pu2NumOfValues,
                     UINT2 *pu2Offset)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpAttr           *pAttr = NULL;
    INT4                i4AttrDiff = 0;
    UINT2               u2VectorLen = 0;
    UINT1               u1AttrEvent = 0;
    UINT1               u1SetEvntForFirstVal = OSIX_FALSE;
    UINT1               u1OptEncode = OSIX_FALSE;

    pMapPortEntry = pVectAttrInfo->pMapPortEntry;
    pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMapPortEntry,
                                                   pMapPortEntry->u2Port);

    if (pAppPortEntry == NULL)
    {
        /* Check to solve the klockwork warning */
        return;
    }
    pPortEntry = pAppPortEntry->pMrpPortEntry;
    pAttr = pVectAttrInfo->pAttr;

    /* Get the Attribute event for this attribute value */
    MrpTxApplyTxEventOnAttr (pMapPortEntry, pAttr, pVectAttrInfo->u2AttrIndex,
                             pVectAttrInfo->u1Event,
                             &u1AttrEvent, &u1OptEncode);
    pVectAttrInfo->u1OptEncode = u1OptEncode;

    if (pVectAttrInfo->u1IsFirstValSet == OSIX_FALSE)
    {
        if (pVectAttrInfo->u1OptEncode == OSIX_FALSE)
        {
            /* This Attribute value needs to be stored as the First Value of
             * the current Vector Attribute */

            MRP_LBUF_PUT_N_BYTES (*ppBuf, pAttr->au1AttrVal,
                                  MEM_MAX_BYTES (pAttr->u1AttrLen,
                                                 MRP_MAX_ATTR_LEN), *pu2Offset);

            pVectAttrInfo->u1IsFirstValSet = OSIX_TRUE;
            pVectAttrInfo->u1IsAttrFilled = OSIX_TRUE;
            /* Event for the First value needs to be set in the Vector. */
            u1SetEvntForFirstVal = OSIX_TRUE;
        }
        else
        {
            return;
        }
    }
    else
    {
        i4AttrDiff = MrpMmrpDiffAttrValue (pVectAttrInfo->LastMacAddr,
                                           pAttr->au1AttrVal, pAttr->u1AttrLen);
    }

    /* Store the attribute event in the PDU if the attribute value is within 
     * the optimal encoding interval or if it is the first value. */
    if ((i4AttrDiff < MRP_OPTIMAL_ENCODE_INTERVAL) ||
        (u1SetEvntForFirstVal == OSIX_TRUE))
    {
        if (pVectAttrInfo->u1OptEncode == OSIX_TRUE)
        {
            if (pVectAttrInfo->u1Count < MRP_OPTIMAL_ENCODE_INTERVAL)
            {
                pVectAttrInfo->au1AttrEvent[pVectAttrInfo->u1Count++]
                    = u1AttrEvent;
            }
        }
        else
        {
            if (pVectAttrInfo->u1Count != 0)
            {
                MrpTxFillOptEvntsInVector (*ppBuf,
                                           pVectAttrInfo->au1AttrEvent,
                                           pVectAttrInfo->u1Count,
                                           pu2NumOfValues, pPortEntry,
                                           MRP_MMRP_APP_ID);
                MEMSET (pVectAttrInfo->au1AttrEvent, 0,
                        MRP_OPTIMAL_ENCODE_INTERVAL);
                pVectAttrInfo->u1Count = 0;
            }
            else if ((u1SetEvntForFirstVal == OSIX_FALSE) && (i4AttrDiff > 1))
            {
                /* Mt event needs to be filled for the intermediate values to 
                 * result in optimal encoding
                 */
                MrpTxFillMtEvntInVector (*ppBuf, pPortEntry, pu2NumOfValues,
                                         (UINT1) (i4AttrDiff - 1));
            }

            MEMCPY (pVectAttrInfo->LastMacAddr, pAttr->au1AttrVal,
                    MEM_MAX_BYTES (pAttr->u1AttrLen, MRP_MAX_ATTR_LEN));
            (*pu2NumOfValues)++;
            MrpTxEncodeAttrEvent (*ppBuf, *pu2NumOfValues, u1AttrEvent);
            MrpUtilIncrTxStats (pPortEntry, MRP_MMRP_APP_ID, u1AttrEvent);
        }
    }
    else
    {
        *pu1AddVectHdr = OSIX_TRUE;
        /* Fill the event for this Attribute Value in the next Vector Attribute. 
         */

        if (pVectAttrInfo->u1OptEncode == OSIX_FALSE)
        {
            /* This Attribute value needs to be stored as the First Value in 
             * the next Vector Attribute */

            /* Length of the current Vector Attribute */
            u2VectorLen =
                (UINT2) MRP_GET_NUM_OF_VECTORS_FIELD (*pu2NumOfValues);

            /* Buffer pointer now points to the start of the Vector field of the
             * current Vector Attribute. Move the buffer pointer to point to the 
             * end of Vector Header of the next Vector Attribute.
             */
            *ppBuf += (MRP_VECTOR_HDR_SIZE + u2VectorLen);
            /* Store the first value of the next Vector Attribute */
            MRP_LBUF_PUT_N_BYTES (*ppBuf, pAttr->au1AttrVal,
                                  MEM_MAX_BYTES (pAttr->u1AttrLen,
                                                 MRP_MAX_ATTR_LEN), *pu2Offset);
            MEMCPY (pVectAttrInfo->LastMacAddr, pAttr->au1AttrVal,
                    MEM_MAX_BYTES (pAttr->u1AttrLen, MRP_MAX_ATTR_LEN));
            pVectAttrInfo->u1IsFirstValSet = OSIX_TRUE;
            MrpTxEncodeAttrEvent (*ppBuf, 1, u1AttrEvent);
            MrpUtilIncrTxStats (pPortEntry, MRP_MMRP_APP_ID, u1AttrEvent);
        }
        else
        {
            MEMSET (pVectAttrInfo->au1AttrEvent, 0,
                    MRP_OPTIMAL_ENCODE_INTERVAL);
            pVectAttrInfo->u1Count = 0;
            pVectAttrInfo->u1IsFirstValSet = OSIX_FALSE;
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpFormAndTransmitPdu
 *                                                                          
 *    DESCRIPTION      : This function constructs the MMRPDU and transmits 
 *                       the PDU out.
 *
 *    INPUT            : pMrpContextInfo  - Pointer to the Context info 
 *                                          structure.
 *                       pMrpAppPortEntry - Pointer to the Application Port 
 *                                          entry.
 *                       pPortEntry       - Pointer to the Port entry.
 *                       pMapPortEntry    - Pointer to the MAP Port entry.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMmrpFormAndTransmitPdu (tMrpContextInfo * pMrpContextInfo,
                           tMrpAppPortEntry * pMrpAppPortEntry,
                           tMrpPortEntry * pPortEntry,
                           tMrpMapPortEntry * pMapPortEntry)
{
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpVectAttrInfo    VectAttrInfo;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;
    UINT2               u2Port = 0;
    UINT2               u2Offset = MRP_PDU_HDR_SIZE;
    UINT2               u2VectorHdr = 0;
    UINT2               u2VectorLen = 0;
    UINT2               u2EndMark = MRP_END_MARK_VALUE;
    UINT2               u2NumOfValues = 0;
    UINT2               u2MapId = 0;
    UINT1              *pu1Ptr = NULL;
    UINT1              *pu1AttrList = NULL;
    UINT1               u1Event = MRP_APP_TX;
    UINT1               u1Flag = OSIX_TRUE;
    UINT1               u1AddVectHdr = OSIX_FALSE;
    UINT1               u1BuffFull = OSIX_FALSE;
    UINT1               u1AttrType = 0;
    UINT1               u1AttrLen = 0;
    UINT1               u1IsNonParticipantType = OSIX_FALSE;
    UINT1               u1IsMacHdrFilled = OSIX_FALSE;

    u4ContextId = pMrpContextInfo->u4ContextId;
    u2Port = pPortEntry->u2LocalPortId;
    u2MapId = pMapPortEntry->pMapEntry->u2MapId;

    if (pMrpAppPortEntry->u1LeaveAllSemState == MRP_ACTIVE)
    {
        u1Event = MRP_APP_TX_LA;
        u2VectorHdr = MRP_PDU_LV_ALL_VALUE;
        MrpUtilIncrTxStats (pPortEntry, MRP_MMRP_APP_ID, MRP_TX_LEAVE_ALL);
    }

    MEMSET (gMrpGlobalInfo.pu1PduBuf, 0, MRP_MAX_PDU_LEN);
    pu1Ptr = gMrpGlobalInfo.pu1PduBuf + MRP_PDU_HDR_SIZE;

    pAttrEntry = MrpMmrpGetFirstAttrForTx (pMapPortEntry);

    if (pAttrEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MMRP_TRC),
                  "MrpMmrpFormAndTransmitPdu: No Attributes exists for "
                  "Port %d.\n", u2Port));
        return;

    }

    MrpTxAddMmrpMessageHdr (&pu1Ptr, pAttrEntry, &u2Offset);
    pu1AttrList = pu1Ptr;

    /* Increment the pu1AttrList by MRP_VECTOR_HDR_SIZE as Vector Header is 
     * filled once the end of Vector for the Vector Attribute is reached.
     */

    pu1AttrList += MRP_VECTOR_HDR_SIZE;
    MEMSET (&VectAttrInfo, 0, sizeof (tMrpVectAttrInfo));

    do
    {
        VectAttrInfo.pMapPortEntry = pMapPortEntry;
        VectAttrInfo.pAttr = &(pAttrEntry->Attr);
        VectAttrInfo.u2AttrIndex = pAttrEntry->u2AttrIndex;
        VectAttrInfo.u1Event = u1Event;
        u1AttrLen = pAttrEntry->Attr.u1AttrLen;
        u1AttrType = pAttrEntry->Attr.u1AttrType;

        if (u1Event == MRP_APP_TX_LA)
        {
            MrpTxHndLAEventForNonParticipant (pMapPortEntry, pAttrEntry,
                                              u1AttrType,
                                              &u1IsNonParticipantType);
        }
        if (u1IsNonParticipantType == OSIX_FALSE)
        {
            MrpMmrpFormVectAttr (&pu1AttrList, &VectAttrInfo, &u1AddVectHdr,
                                 &u2NumOfValues, &u2Offset);
            if (u1AddVectHdr == OSIX_TRUE)
            {
                /* End of Vector has reached for current Vector Attribute.
                 * Hence add Vector Header */
                u2VectorHdr |= u2NumOfValues;
                MrpTxAddVectHdrAndUpdNoOfValues (&pu1AttrList, &pu1Ptr,
                                                 u2VectorHdr, &u2NumOfValues,
                                                 &u2Offset, u1AttrLen,
                                                 VectAttrInfo.u1OptEncode);
                u2VectorHdr = 0;
                u1AddVectHdr = OSIX_FALSE;
            }
        }

        /* The MAP Port entry might get deleted by the function 
         * MrpMmrpFormVectAttr (when there is no Attribute entry). 
         * So Check whether the MAP entry is present. If MAP entry is 
         * present means the MAP Port entry is also present. */

        pMapEntry = MRP_GET_MAP_ENTRY (pMrpAppPortEntry->pMrpAppEntry, u2MapId);

        if (pMapEntry == NULL)
        {
            break;
        }

        pAttrEntry = MrpMmrpGetNextAttrForTx (pMapPortEntry, pAttrEntry,
                                              &u1Flag, OSIX_TRUE);
        if (pMrpAppPortEntry->pMrpPortEntry->au1ApplAdminCtrl
            [MRP_MMRP_APP_ID][u1AttrType] != MRP_NON_PARTICIPANT)
        {
            if (pAttrEntry != NULL)
            {
                if (MrpTxIsSpaceInMMRPPkt (u2Offset, u2NumOfValues,
                                           pAttrEntry->Attr.u1AttrLen) ==
                    OSIX_FALSE)
                {
                    /* No space in the PDU for this Attribute Value */
                    u1BuffFull = OSIX_TRUE;
                    break;
                }

                /* If the Attribute types are different, then it needs to be filled
                 * as a separate Message
                 */
                if (u1Flag == OSIX_FALSE)
                {
                    if (u2NumOfValues != 0)
                    {
                        u2VectorHdr |= u2NumOfValues;
                        MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2VectorHdr, u2Offset);
                        if (u1Event == MRP_APP_TX_LA)
                        {
                            u2VectorHdr = MRP_PDU_LV_ALL_VALUE;
                        }
                        else
                        {
                            u2VectorHdr = 0;
                        }
                        u2VectorLen =
                            (UINT2)
                            MRP_GET_NUM_OF_VECTORS_FIELD (u2NumOfValues);

                        u2Offset = (UINT2) (u2Offset + u2VectorLen);
                        /* Move the pointer to the end of the Vector */
                        pu1AttrList += u2VectorLen;
                        /* Add EndMark to denote the end of the Service Requirement 
                         * Message */
                        MRP_LBUF_PUT_2_BYTES (pu1AttrList, u2EndMark, u2Offset);
                        u2NumOfValues = 0;
                    }
                    else
                    {
                        /* Service Requirement Value has not filled. */
                        MRP_LBUF_DEC_BY_2_BYTES (pu1AttrList, u2Offset);
                        /*For Decrementing the Vector Header */
                        MRP_LBUF_DEC_BY_2_BYTES (pu1AttrList, u2Offset);
                    }
                    /* For MAC Address */
                    MRP_LBUF_PUT_1_BYTE (pu1AttrList,
                                         (UINT1) MRP_MAC_ADDR_ATTR_TYPE,
                                         u2Offset);
                    MRP_LBUF_PUT_1_BYTE (pu1AttrList, (UINT1) MRP_MAC_ADDR_LEN,
                                         u2Offset);
                    u1IsMacHdrFilled = OSIX_TRUE;
                    /* Set the pointer to the start of the next Vector 
                     * Attribute
                     * */
                    pu1Ptr = pu1AttrList;
                    pu1AttrList += MRP_VECTOR_HDR_SIZE;
                    VectAttrInfo.u1IsFirstValSet = OSIX_FALSE;
                    VectAttrInfo.u1IsAttrFilled = OSIX_FALSE;
                }
            }
        }
        u1Flag = OSIX_TRUE;
    }
    while (pAttrEntry != NULL);
    /* Update the NextMvrpTxNode, which indicates the next VLAN to be 
     * transmitted in the next opportunity.
     */
    if ((pPortEntry->au1ApplAdminCtrl[MRP_MMRP_APP_ID]
         [MRP_SERVICE_REQ_ATTR_TYPE] == MRP_NON_PARTICIPANT) &&
        (pPortEntry->au1ApplAdminCtrl[MRP_MMRP_APP_ID]
         [MRP_MAC_ADDR_ATTR_TYPE] == MRP_NON_PARTICIPANT))
    {
        /* If the Attribute types are different, then it needs to 
         * be filled as a separate Message
         */
        return;
    }

    if (VectAttrInfo.u1IsAttrFilled == OSIX_FALSE)
    {
        if (u1IsMacHdrFilled == OSIX_TRUE)
        {
            /* MAC Address Attribute Type, LEN . */
            MRP_LBUF_DEC_BY_2_BYTES (pu1AttrList, u2Offset);
            /*For Decrementing the Vector Header */
            MRP_LBUF_DEC_BY_2_BYTES (pu1AttrList, u2Offset);
        }
        else
        {
            /* Both Service Req. and MAC not filled, So return. */
            return;
        }
    }
    if (pMapEntry != NULL)
    {
        /* Since there is a chance of MAP Port entry being deleted, 
         * the existance of MAP entry check is required. */
        pMapPortEntry->pNextMmrpTxNode = pAttrEntry;
    }
    u2VectorLen = (UINT2) MRP_GET_NUM_OF_VECTORS_FIELD (u2NumOfValues);
    /* Move the pointer to the end of the Vector */
    pu1AttrList += u2VectorLen;
    u2Offset = (UINT2) (u2Offset + u2VectorLen);
    /* Add the Vector Header for the last filled Vector Attribute */
    u2VectorHdr |= u2NumOfValues;
    MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2VectorHdr, u2Offset);
    /* Add EndMark to denote the end of Attribute List. */
    MRP_LBUF_PUT_2_BYTES (pu1AttrList, u2EndMark, u2Offset);

    /* Add EndMark to denote the end of the MRPDU */
    MRP_LBUF_PUT_2_BYTES (pu1AttrList, u2EndMark, u2Offset);

    MrpTxAddPduHdrAndTxPdu (u4ContextId, MRP_MMRP_APP_ID, u2Offset, u2Port,
                            u2MapId);

    if ((u1BuffFull == OSIX_TRUE) && (u1Event == MRP_APP_TX_LA))
    {
        /* u1BuffFull is set to OSIX_TRUE only when no space is available in the
         * PDU. Hence apply txLAF! event to the remaining Attribute values
         */
        if (pMapEntry != NULL)
        {
            /* If MAP Entry is not there means there is no MAP Port entry, 
             * Attribute entry. So no need to apply TxLAF. */
            MrpMmrpApplyTxLAFOnRemainingAttr (pMapPortEntry, pAttrEntry);
            pMapPortEntry->pNextMmrpTxNode = NULL;
        }
    }
    if (MRP_APP_TX_LA == u1Event)
    {
        /* Now only we have tx. PDU increment the count.
         * */
        MrpUtilIncrTxStats (pPortEntry, MRP_MMRP_APP_ID, MRP_TX_LEAVE_ALL);
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpGetFirstAttrForTx
 *                                                                          
 *    DESCRIPTION      : This function returns the first attribute that  
 *                       needs to be transmitted.
 *
 *    INPUT            : pMapPortEntry  - Pointer to the MAP Port entry.  
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to the first attribute entry
 *                                                                          
 ****************************************************************************/
tMrpAttrEntry      *
MrpMmrpGetFirstAttrForTx (tMrpMapPortEntry * pMapPortEntry)
{
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAttrEntry      *pNextAttrEntry = NULL;

    pMapEntry = pMapPortEntry->pMapEntry;

    if (pMapPortEntry->pNextMmrpTxNode == NULL)
    {
        pNextAttrEntry = (tMrpAttrEntry *)
            RBTreeGetFirst (pMapEntry->pMrpAppEntry->MapAttrTable);

        if (pNextAttrEntry == NULL)
        {
            return NULL;
        }

        do
        {
            pAttrEntry = pNextAttrEntry;

            if (pAttrEntry->pMapEntry->u2MapId == pMapEntry->u2MapId)
            {

                if ((MRP_GET_ATTR_REG_SEM_STATE
                     (pMapPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex])
                     != MRP_MT) ||
                    (MRP_GET_ATTR_APP_SEM_STATE
                     (pMapPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex])
                     != MRP_VO))
                {
                    return pAttrEntry;
                }
            }

        }
        while ((pNextAttrEntry = (tMrpAttrEntry *) RBTreeGetNext
                (pMapEntry->pMrpAppEntry->MapAttrTable,
                 pAttrEntry, NULL)) != NULL);

        return NULL;
    }
    else
    {
        return (pMapPortEntry->pNextMmrpTxNode);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpGetNextAttrForTx
 *                                                                          
 *    DESCRIPTION      : This function returns the next attribute that  
 *                       needs to be transmitted.
 *
 *    INPUT            : pPortEntry  - Pointer to the Port entry. 
 *                       pAttrEntry  - Pointer to the Current Attribute entry.
 *                                                                          
 *    OUTPUT           : pu1Flag     - OSIX_TRUE  - when the type of next 
 *                                     attribute is same as current attribute. 
 *                                     OSIX_FALSE - when the type of next 
 *                                     attribute is not same as current 
 *                                     attribute.
 *                                    
 *                                                                          
 *    RETURNS          : Pointer to the next attribute entry
 *                                                                          
 ****************************************************************************/
tMrpAttrEntry      *
MrpMmrpGetNextAttrForTx (tMrpMapPortEntry * pMapPortEntry,
                         tMrpAttrEntry * pAttrEntry, UINT1 *pu1Flag,
                         UINT1 u1IsFirstAttrNeeded)
{
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpAttrEntry      *pNextAttrEntry = NULL;
    UINT1               u1AttrType = pAttrEntry->Attr.u1AttrType;

    pMapEntry = pMapPortEntry->pMapEntry;
    *pu1Flag = OSIX_TRUE;

    pNextAttrEntry = (tMrpAttrEntry *)
        RBTreeGetNext (pMapEntry->pMrpAppEntry->MapAttrTable, pAttrEntry, NULL);

    if (pNextAttrEntry != NULL)
    {
        do
        {
            pAttrEntry = pNextAttrEntry;

            if (pAttrEntry->pMapEntry->u2MapId == pMapEntry->u2MapId)
            {

                if ((MRP_GET_ATTR_REG_SEM_STATE
                     (pMapPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex])
                     != MRP_MT) ||
                    (MRP_GET_ATTR_APP_SEM_STATE
                     (pMapPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex])
                     != MRP_VO))
                {
                    break;
                }
            }

        }
        while ((pNextAttrEntry = (tMrpAttrEntry *) RBTreeGetNext
                (pMapEntry->pMrpAppEntry->MapAttrTable,
                 pAttrEntry, NULL)) != NULL);
    }

    /* This utility fn. will be used in two places to get the next Attribute
     * 1) To Get the next valid attribute for transmission
     * 2) To Get the next valid attribute for applying txLAF! event
     * In case of (2) this shuld not wrap around to the first entry.
     * Hence preventing the same
     * */
    if ((u1IsFirstAttrNeeded == OSIX_TRUE) && (pNextAttrEntry == NULL) &&
        (pMapPortEntry->pNextMmrpTxNode != NULL))
    {
        pNextAttrEntry =
            (tMrpAttrEntry *)
            RBTreeGetFirst (pMapEntry->pMrpAppEntry->MapAttrTable);

        if (pNextAttrEntry == NULL)
        {
            return NULL;
        }

        do
        {
            pAttrEntry = pNextAttrEntry;
            if (pAttrEntry->pMapEntry->u2MapId == pMapEntry->u2MapId)
            {
                if ((MRP_GET_ATTR_REG_SEM_STATE
                     (pMapPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex])
                     != MRP_MT) ||
                    (MRP_GET_ATTR_APP_SEM_STATE
                     (pMapPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex])
                     != MRP_VO))
                {
                    break;
                }
            }

        }
        while ((pNextAttrEntry = (tMrpAttrEntry *) RBTreeGetNext
                (pMapEntry->pMrpAppEntry->MapAttrTable,
                 pAttrEntry, NULL)) != NULL);

        if (pNextAttrEntry == pMapPortEntry->pNextMmrpTxNode)
        {
            return NULL;
        }
    }

    if (pNextAttrEntry != NULL)
    {
        if (pNextAttrEntry->Attr.u1AttrType != u1AttrType)
        {
            *pu1Flag = OSIX_FALSE;
        }
    }

    return pNextAttrEntry;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpApplyTxLAFOnRemainingAttr
 *                                                                          
 *    DESCRIPTION      : This function will be called to apply txLAF! event 
 *                       for the remaining attributes present in the 
 *                       attribute list.
 *
 *    INPUT            : pMapPortEntry - Pointer to the MAP Port entry. 
 *                       pAttrEntry    - Pointer to the current attr entry. 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMmrpApplyTxLAFOnRemainingAttr (tMrpMapPortEntry * pMapPortEntry,
                                  tMrpAttrEntry * pAttrEntry)
{
    UINT2               u2AttrIndex = 0;
    UINT1               u1Flag = OSIX_FALSE;
    UINT1               u1RegSEMStateChg = OSIX_FALSE;

    do
    {

        u2AttrIndex = pAttrEntry->u2AttrIndex;

        /* As per IEEE standard 802.1ak section 10.7.5.20 */
        MrpSmApplicantSem (pMapPortEntry, u2AttrIndex, MRP_APP_TX_LAF);

        if (MRP_GET_ATTR_REG_ADMIN_CTRL
            (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_REG_NORMAL)
        {
            MrpSmRegistrarSem (pMapPortEntry, u2AttrIndex,
                               MRP_REG_RX_LEAVE_ALL, &u1RegSEMStateChg);
        }

    }
    while ((pAttrEntry = MrpMmrpGetNextAttrForTx (pMapPortEntry, pAttrEntry,
                                                  &u1Flag, OSIX_FALSE))
           != NULL);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpDiffAttrValue
 *                                                                          
 *    DESCRIPTION      : This function is called to get the difference
 *                       between the 2 attribute values. 
 *
 *    INPUT            : pu1CurrVal  - Current Attribute Value
 *                       pu1NextVal  - Next Attribute Value
 *                       u1AttrLen   - Attribute Length
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
INT4
MrpMmrpDiffAttrValue (UINT1 *pu1CurrVal, UINT1 *pu1NextVal, UINT1 u1AttrLen)
{
    UINT4               u4CurrVal1 = 0;
    UINT4               u4CurrVal2 = 0;
    UINT4               u4NextVal1 = 0;
    UINT4               u4NextVal2 = 0;

    if (u1AttrLen == MRP_SERVICE_REQ_LEN)
    {
        return (pu1NextVal[0] - pu1CurrVal[0]);
    }

    u4CurrVal1 =
        (UINT4) ((pu1CurrVal[3] << 16) + (pu1CurrVal[4] << 8) + pu1CurrVal[5]);
    u4NextVal1 =
        (UINT4) ((pu1NextVal[3] << 16) + (pu1NextVal[4] << 8) + pu1NextVal[5]);

    u4CurrVal2 =
        (UINT4) ((pu1CurrVal[0] << 16) + (pu1CurrVal[1] << 8) + pu1CurrVal[2]);
    u4NextVal2 =
        (UINT4) ((pu1NextVal[0] << 16) + (pu1NextVal[1] << 8) + pu1NextVal[2]);

    /* Compare the last three bytes of the MAC Addresses. */
    if (u4NextVal1 > u4CurrVal1)
    {
        /* Check if the first three bytes of the MAC Addresses are the same. If
         * so return the difference of the last three bytes of the MAC
         * Addresses.
         */
        if (u4NextVal2 == u4CurrVal2)
        {
            return (INT4) (u4NextVal1 - u4CurrVal1);
        }
        else
        {
            /* The difference between the two MAC Addresses is definitely 
             * greatrer than the MRP_OPTIMAL_ENCODE_INTERVAL. Hence return
             * MRP_OPTIMAL_ENCODE_INTERVAL + 1.
             */
            return (MRP_OPTIMAL_ENCODE_INTERVAL + 1);
        }
    }
    else
    {
        if ((u4NextVal2 - u4CurrVal2) > 1)
        {
            return (MRP_OPTIMAL_ENCODE_INTERVAL + 1);
        }
        else
        {
            /* The first three bytes of the MAC Addresses are either same
             * or the difference is 1 and the last three bytes of the first
             * MAC Address is greater than the second. Hence to get the
             * difference the following logic is used. Consider the first
             * MAC Address as 00:00:00:ff:ff:fe and the second MAC Address as
             * 00:00:01:00:00:02. In this case, the difference between the two
             * MAC is 4.
             * 
             */
            if (u4NextVal1 != u4CurrVal1)
            {
                return (INT4) ((MRP_MAX_VAL - u4CurrVal1) + (u4NextVal1 + 1));
            }
            else
            {
                /* The first three bytes of the MAC Addresses are either same
                 * or the difference is 1, but the last three bytes are the 
                 * same. Hence the difference between the MAC Addresses will
                 * be huge. Hence return MRP_OPTIMAL_ENCODE_INTERVAL + 1.
                 */
                return (MRP_OPTIMAL_ENCODE_INTERVAL + 1);
            }
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMmrpApplyTcDetectedTmrStatus 
 *                                                                          
 *    DESCRIPTION      : This function processes the status of the tcDetected
 *                       timer.
 *
 *    INPUT            : pAppEntry       - Pointer to the Application Entry
 *                       u2Port          - Local Port Number 
 *                       pu1VlanList     - List of VLANs (MAP ID) 
 *                       u2VlanCntInList - Number of VLANs in pu1VlanList
 *                       u1TmrState      - Timer state (running or expired)
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
VOID
MrpMmrpApplyTcDetectedTmrStatus (tMrpAppEntry * pAppEntry, UINT2 u2Port,
                                 UINT1 *pu1VlanList, UINT2 u2VlanCntInList,
                                 UINT1 u1TmrState)
{
    UINT2               u2ByteInd = 0;
    UINT2               u2NumVlan = 0;
    UINT2               u2VlanFlag = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2VlanId = 0;

    for (u2ByteInd = 0; ((u2ByteInd < MRP_VLAN_LIST_SIZE)
                         && (u2NumVlan < u2VlanCntInList)); u2ByteInd++)
    {
        if (pu1VlanList[u2ByteInd] != 0)
        {
            u2VlanFlag = pu1VlanList[u2ByteInd];
            for (u2BitIndex = 0; ((u2BitIndex < MRP_VLANS_PER_BYTE)
                                  && (u2VlanFlag != 0)
                                  && (u2NumVlan < u2VlanCntInList));
                 u2BitIndex++)
            {
                if ((u2VlanFlag & MRP_BIT8) != 0)
                {
                    u2VlanId =
                        (UINT2) ((u2ByteInd * MRP_VLANS_PER_BYTE) +
                                 u2BitIndex + 1);
                    u2NumVlan++;
                    MrpUtilSetTcDetectedTmrStatus (pAppEntry, u2VlanId,
                                                   u2Port, u1TmrState);
                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }
}

/*************************************************************************
 *
 *  FUNCTION NAME   : MrpTxIsSpaceInMMRPPkt
 *
 *  DESCRIPTION     : This function checks whether Space is present in the 
 *                    PDU or not.
 *
 *  INPUT           : u2CurrOffset - Current Offset in the PDU
 *                    u2NumOfValue - Number Of Values
 *                    u1AttrLen    - Attr Length
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : OSIX_TRUE/OSIX_FALSE
 *
 **************************************************************************/
INT4
MrpTxIsSpaceInMMRPPkt (UINT2 u2CurrOffset, UINT2 u2NumOfValue, UINT1 u1AttrLen)
{
    UINT2               u2AttrListSize = 0;
    UINT2               u2PduSize = 0;

    u2AttrListSize = MRP_VECTOR_HDR_SIZE;    /* LeaveAll + No. Of Values */
    u2AttrListSize = (UINT2) (u2AttrListSize + u1AttrLen);    /* First Value Length */
    u2AttrListSize =
        (UINT2) (u2AttrListSize +
                 ((u2NumOfValue / MRP_MAX_ATTR_EVENTS_PER_VECTOR) + 1));
    /* Vector Length */

    u2PduSize = (UINT2) (u2CurrOffset + u2AttrListSize +
                         MRP_MIN_MMRP_VECT_ATTR_LEN + (2 * MRP_END_MARK_SIZE));

    if (u2PduSize <= MRP_MAX_PDU_LEN)
    {
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

/************************************************************************
 * FUNCTION NAME    : MrpMmrpHandleRemapEvent                
 *                                                                      
 * DESCRIPTION      : Handles Remap when the VLAN which is mapped to a new
 *                    MST instance is statically configured
 *                                                                      
 * INPUT(s)         : pAppEntry   - Pointer to Application Entry          
 *                    u2MapId     - MAP Identifier                         
 *                    u1RemapFlag - Indicates whether Attribute values are
 *                                  retained in the new MAP ID or not
 *                    u1DynVlan   - Indicates whether the VLAN is dynamically
 *                                  learnt or not
 *                                                                      
 * OUTPUT(s)        : None                                              
 *                                                                      
 * RETURNS          : None                                              
 ************************************************************************/
VOID
MrpMmrpHandleRemapEvent (tMrpAppEntry * pAppEntry, UINT2 u2MapId,
                         UINT1 u1RemapFlag, UINT1 u1DynVlan)
{
    UINT1               u1PortState = 0;
    UINT2               u2PortId = 0;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;

    pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

    if (pMapEntry == NULL)
    {
        return;
    }

    for (u2PortId = 1; u2PortId <= MRP_MAX_PORTS_PER_CONTEXT; u2PortId++)
    {
        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

        if (pMapPortEntry == NULL)
        {
            continue;
        }
        pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMapPortEntry,
                                                       pMapPortEntry->u2Port);
        if (pAppPortEntry == NULL)
        {
            continue;
        }
        pMrpPortEntry = pAppPortEntry->pMrpPortEntry;

        u1PortState = L2IwfGetVlanPortState (u2MapId, u2PortId);

        if (u1PortState == AST_PORT_STATE_FORWARDING)
        {
            pMapPortEntry->u1PortState = AST_PORT_STATE_FORWARDING;
        }
        else
        {
            pMapPortEntry->u1PortState = AST_PORT_STATE_DISCARDING;
        }

        /* Assumption is if pMapPortEntry is there then pMrpPortEntry 
         * should be present. So no NULL check should be needed here */

        MrpRemapMmrpUpdateSemAndTx (pAppEntry->pMrpContextInfo,
                                    pAppEntry, pMrpPortEntry, pMapEntry,
                                    pMapPortEntry, u1DynVlan, u1RemapFlag);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : MrpMmrpProcessPvlanJoinOrLeave                   */
/*                                                                           */
/*    Description         : This function updated the Multicast table when   */
/*                          pvlan is configured, if the given vlan is        */
/*                          primary vlan then, scan all the secondary vlans  */
/*                          associated with that vlan and program the        */
/*                          multicast table for those vlans. If the the      */
/*                          given vlan secondary, the get the associated     */
/*                          primary vlan and update the multicast table for  */
/*                          that vlan alone.                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          MacAddr -  MacAddress                            */
/*                          VlanId   - VlanId associated with the multicast  */
/*                                     MacAddress                            */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the port will be added */
/*                                     into the PortList.                    */
/*                                                                           */
/*                                     VLAN_DELETE, then the port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS                                      */
/*                         OSIX_FAILURE                                      */
/*****************************************************************************/
INT4
MrpMmrpProcessPvlanJoinOrLeave (UINT4 u4ContextId, tMacAddr MacAddr,
                                tVlanId GipId, UINT2 u2Port, UINT1 u1Action)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    MEMSET (gau2SecVlanList, 0, (VLAN_MAX_COMMUNITY_VLANS + 1)
            * sizeof (UINT2));

    L2PvlanMappingInfo.InVlanId = GipId;
    L2PvlanMappingInfo.u4ContextId = u4ContextId;
    L2PvlanMappingInfo.pMappedVlans = gau2SecVlanList;
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    MRP_CONVERT_COMP_ID_TO_CTXT_ID (L2PvlanMappingInfo.u4ContextId);

    MrpPortL2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    if (L2PvlanMappingInfo.u1VlanType != L2IWF_NORMAL_VLAN)
    {
        for (u2VlanId = 0; u2VlanId < L2PvlanMappingInfo.u2NumMappedVlans;
             u2VlanId++)
        {
            if (MrpPortVlanUpdateDynamicMACInfo
                (u4ContextId, MacAddr,
                 L2PvlanMappingInfo.pMappedVlans[u2VlanId],
                 u2Port, u1Action) == VLAN_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpmmrp.c                      */
/*-----------------------------------------------------------------------*/
