/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpmapds.c,v 1.12 2014/02/13 12:11:35 siva Exp $
 *
 * Description: This file contains the MAP specific data structure routines.
 ******************************************************************************/
#include "mrpinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpMapDSCreateMrpMapEntry
 *
 *    DESCRIPTION      : This function creates and intializes the MAP entry.
 *
 *    INPUT            : pMrpAppEntry    - Pointer to the Appliction structure
 *                       u2MapId         - MAP Id
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Pointer to the MAP entry
 *
 ****************************************************************************/
tMrpMapEntry       *
MrpMapDSCreateMrpMapEntry (tMrpAppEntry * pMrpAppEntry, UINT2 u2MapId)
{
    tMrpMapEntry       *pMrpMapEntry = NULL;

    pMrpMapEntry = (tMrpMapEntry *) MemAllocMemBlk (gMrpGlobalInfo.MapPoolId);

    if (pMrpMapEntry == NULL)
    {
        MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                  "MrpMapDSCreateMrpMapEntry: Memory allocation for MAP entry "
                  "Failed. \r\n"));
        return NULL;
    }

    MEMSET (pMrpMapEntry, 0, sizeof (tMrpMapEntry));

    if (MrpMapDSInitMrpMapEntry (pMrpAppEntry, pMrpMapEntry, u2MapId)
        == OSIX_FAILURE)
    {
        MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                  "MrpMapDSCreateMrpMapEntry: Initializing the MAP "
                  "entry Failed. \r\n"));
        MemReleaseMemBlock (gMrpGlobalInfo.MapPoolId, (UINT1 *) pMrpMapEntry);
        pMrpMapEntry = NULL;
        return NULL;
    }

    MrpAppAddMapEntryToAppEntry (pMrpAppEntry, pMrpMapEntry);

    return pMrpMapEntry;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpMapDSCreateMrpMapAttrEntry
 *
 *    DESCRIPTION      : This function creates & intializes the MAP Attr entry.
 *
 *    INPUT            : pMrpMapEntry     - Pointer to the MAP structure
 *                       Attr             - Attribute Value
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Pointer to the MAP port structure
 *
 ****************************************************************************/
tMrpAttrEntry      *
MrpMapDSCreateMrpMapAttrEntry (tMrpMapEntry * pMrpMapEntry, tMrpAttr * pAttr)
{
    tMrpAttrEntry      *pMrpAttrEntry = NULL;

    pMrpAttrEntry =
        (tMrpAttrEntry *) MemAllocMemBlk (gMrpGlobalInfo.AttrPoolId);

    if (pMrpAttrEntry == NULL)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC, "MrpMapDSCreateMrpMapAttrEntry:"
                     " Memory allocation for attribute entry Failed. \r\n");
        return NULL;
    }
    MEMSET (pMrpAttrEntry, 0, sizeof (tMrpAttrEntry));

    MEMCPY (&pMrpAttrEntry->Attr, pAttr, sizeof (tMrpAttr));

    pMrpAttrEntry->u2PortCount = MRP_INIT_VAL;
    /* Filling the Back Pointer to the MAP structure */
    pMrpAttrEntry->pMapEntry = pMrpMapEntry;

    if (MrpMapDSAddAttrEntryToMapEntry (pMrpMapEntry, pMrpAttrEntry)
        == OSIX_FAILURE)
    {
        MRP_TRC ((pMrpMapEntry->pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                  "MrpMapDSCreateMrpMapAttrEntry: Adding the Attribute entry to "
                  "MAP table Failed. \r\n"));
        MemReleaseMemBlock (gMrpGlobalInfo.AttrPoolId, (UINT1 *) pMrpAttrEntry);
        pMrpAttrEntry = NULL;
        return NULL;
    }

    return pMrpAttrEntry;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpMapDSDeleteMrpMapEntry
 *
 *    DESCRIPTION      : This function deletes the MAP entry.
 *
 *    INPUT            : pMrpMapEntry  - Pointer to the MAP structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
MrpMapDSDeleteMrpMapEntry (tMrpMapEntry * pMrpMapEntry)
{
    if (pMrpMapEntry->u2AttrCount == 0)
    {
        MrpAppDelMapEntryFromAppEntry (pMrpMapEntry->pMrpAppEntry,
                                       pMrpMapEntry);

        MrpMapDSDeInitMrpMapEntry (pMrpMapEntry);

        MemReleaseMemBlock (gMrpGlobalInfo.MapPoolId, (UINT1 *) pMrpMapEntry);
        pMrpMapEntry = NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSCreateMrpMapPortEntry
 *                                                                          
 *    DESCRIPTION      : This function creates & intializes the MAP port entry.
 *
 *    INPUT            : pMrpMapEntry     - Pointer to the MAP structure
 *                       u2Port           - Local Port Number
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to the MAP port structure
 *                                                                          
 ****************************************************************************/
tMrpMapPortEntry   *
MrpMapDSCreateMrpMapPortEntry (tMrpMapEntry * pMrpMapEntry, UINT2 u2Port)
{
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;

    pMrpMapPortEntry = (tMrpMapPortEntry *) MemAllocMemBlk
        (gMrpGlobalInfo.MapPortPoolId);

    if (pMrpMapPortEntry == NULL)
    {
        MRP_TRC ((pMrpMapEntry->pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                  "MrpMapDSCreateMrpMapPortEntry: Memory allocation for MAP "
                  "port entry Failed. \r\n"));
        return NULL;
    }

    MEMSET (pMrpMapPortEntry, 0, sizeof (tMrpMapPortEntry));

    /* Initialize the members of MapPortEntry */
    if (MrpMapDSInitMrpMapPortEntry (pMrpMapEntry, pMrpMapPortEntry, u2Port)
        == OSIX_FAILURE)
    {
        MRP_TRC ((pMrpMapEntry->pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                  "MrpMapDSCreateMrpMapPortEntry: Adding the MAP port entry to "
                  "MAP table Failed. \r\n"));
        MrpMapDSDeleteMrpMapPortEntry (pMrpMapPortEntry);
        return NULL;
    }

    /* Add the initialized MapPortentry in to the Map Entry */
    MrpMapDSAddPortEntryToMapEntry (pMrpMapEntry, pMrpMapPortEntry);

    return pMrpMapPortEntry;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSInitMrpMapEntry
 *                                                                          
 *    DESCRIPTION      : This function is used initialize the MAP entry 
 *
 *    INPUT            : pMrpAppEntry     - Pointer to the Appliction structure
 *                       pMrpMapEntry     - Pointer to the MAP structure
 *                       u2MapId          - MAP Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapDSInitMrpMapEntry (tMrpAppEntry * pMrpAppEntry,
                         tMrpMapEntry * pMrpMapEntry, UINT2 u2MapId)
{
    /* Allocate memory for Array of pointers that is going 
     * to have address of each attirbute entry node */
    if (pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        pMrpMapEntry->ppAttrEntryArray =
            MemAllocMemBlk (gMrpGlobalInfo.MapMvrpAttrArrayPoolId);

        if (pMrpMapEntry->ppAttrEntryArray == NULL)
        {
            MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                      (INIT_SHUT_TRC | ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                      "MrpMapDSInitMrpMapEntry: MVRP Attr Array MemAlloc "
                      "Failed. \r\n"));
            return OSIX_FAILURE;
        }

        MEMSET (pMrpMapEntry->ppAttrEntryArray, 0,
                (sizeof (tMrpAttrEntry *) * (VLAN_DEV_MAX_VLAN_ID + 1)));
        pMrpMapEntry->u2AttrArraySize = VLAN_DEV_MAX_VLAN_ID;
    }
    else if (pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        pMrpMapEntry->ppAttrEntryArray =
            MemAllocMemBlk (gMrpGlobalInfo.MapMmrpAttrArrayPoolId);

        if (pMrpMapEntry->ppAttrEntryArray == NULL)
        {
            MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                      (INIT_SHUT_TRC | ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                      "MrpMapDSInitMrpMapEntry: MMRP Attr Array MemAlloc "
                      "Failed. \r\n"));
            return OSIX_FAILURE;
        }

        MEMSET (pMrpMapEntry->ppAttrEntryArray, 0,
                (sizeof (tMrpAttrEntry *) * (MRP_MAX_MMRP_ATTR_IN_SYS + 1)));
        pMrpMapEntry->u2AttrArraySize = MRP_MAX_MMRP_ATTR_IN_SYS;

        if (pMrpAppEntry->pu1MmrpAttrIndexList == NULL)
        {
            pMrpAppEntry->pu1MmrpAttrIndexList =
                MemAllocMemBlk (gMrpGlobalInfo.AppMmrpAttrIndxPoolId);

            if (pMrpAppEntry->pu1MmrpAttrIndexList == NULL)
            {
                MRP_TRC ((pMrpAppEntry->pMrpContextInfo,
                          (INIT_SHUT_TRC | ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                          "MrpMapDSInitMrpMapEntry: MMRP AttrIndexList  MemAlloc "
                          "Failed. \r\n"));
                return OSIX_FAILURE;

            }
            MEMSET (pMrpAppEntry->pu1MmrpAttrIndexList, 0,
                    (MRP_MAX_MMRP_ATTR_IN_SYS + 1));
            pMrpAppEntry->u2AttrArraySize = MRP_MAX_MMRP_ATTR_IN_SYS;
        }
    }

    if (pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        pMrpAppEntry->u2NextFreeAttrIndex = 1;
    }
    /* Filling the Back Pointer to the Application structure */
    pMrpMapEntry->pMrpAppEntry = pMrpAppEntry;
    pMrpMapEntry->u2MapId = u2MapId;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSDeInitMrpMapEntry
 *                                                                          
 *    DESCRIPTION      : This function is used de-inits the MAP entry 
 *
 *    INPUT            : pMrpMapEntry     - Pointer to the MAP structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapDSDeInitMrpMapEntry (tMrpMapEntry * pMrpMapEntry)
{
    if (pMrpMapEntry->ppAttrEntryArray != NULL)
    {
        if (pMrpMapEntry->pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
        {
            MemReleaseMemBlock (gMrpGlobalInfo.MapMvrpAttrArrayPoolId,
                                (UINT1 *) pMrpMapEntry->ppAttrEntryArray);
        }
        else if (pMrpMapEntry->pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
        {
            MemReleaseMemBlock (gMrpGlobalInfo.MapMmrpAttrArrayPoolId,
                                (UINT1 *) pMrpMapEntry->ppAttrEntryArray);
        }
        pMrpMapEntry->ppAttrEntryArray = NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSInitMrpMapPortEntry
 *                                                                          
 *    DESCRIPTION      : This function is used for initializing the 
 *                       MAP port entry 
 *
 *    INPUT            : pMrpMapEntry     - Pointer to the MAP structure
 *                       pMrpMapPortEntry - Pointer to the MAP port structure
 *                       u2Port           - Local-port Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapDSInitMrpMapPortEntry (tMrpMapEntry * pMrpMapEntry,
                             tMrpMapPortEntry * pMrpMapPortEntry, UINT2 u2Port)
{
    tMrpAppEntry       *pMrpAppEntry = NULL;
    UINT1               u1Index = 0;
    UINT1               u1PortState = 0;

    pMrpMapPortEntry->pNextMmrpTxNode = NULL;

    /* Filling the Back Pointer to the MAP structure */
    pMrpMapPortEntry->pMapEntry = pMrpMapEntry;
    pMrpAppEntry = pMrpMapEntry->pMrpAppEntry;

    pMrpMapPortEntry->u2Port = u2Port;

    if (pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        /* u2AttrArraySize in MapPortEntry is used to scan the byte list 
         * (AttrInfoList) present in MapPortEntry */
        pMrpMapPortEntry->u2AttrArraySize = VLAN_DEV_MAX_VLAN_ID;

        /* Attribute Info List Memory */
        pMrpMapPortEntry->pu1AttrInfoList =
            (UINT1 *) MemAllocMemBlk (gMrpGlobalInfo.MvrpAttrInfoListPoolId);

        if (pMrpMapPortEntry->pu1AttrInfoList == NULL)
        {
            MRP_TRC ((pMrpMapEntry->pMrpAppEntry->pMrpContextInfo,
                      MRP_CRITICAL_TRC,
                      "MrpMapDSInitMrpMapPortEntry: "
                      "Memory Allocate for MapPortMvrpAttr entry "
                      "Failed. \r\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pMrpMapPortEntry->pu1AttrInfoList, 0,
                (sizeof (UINT1) * (VLAN_DEV_MAX_VLAN_ID + 1)));
    }
    else if (pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        /* u2AttrArraySize in MapPortEntry is used to scan the byte list 
         * (AttrInfoList) present in MapPortEntry */
        pMrpMapPortEntry->u2AttrArraySize = MRP_MAX_MMRP_ATTR_IN_SYS;

        /* Attribute Info List Memory */
        pMrpMapPortEntry->pu1AttrInfoList =
            (UINT1 *) MemAllocMemBlk (gMrpGlobalInfo.MmrpAttrInfoListPoolId);

        if (pMrpMapPortEntry->pu1AttrInfoList == NULL)
        {
            MRP_TRC ((pMrpMapEntry->pMrpAppEntry->pMrpContextInfo,
                      MRP_CRITICAL_TRC,
                      "MrpMapDSInitMrpMapPortEntry: "
                      "Memory Allocate for MapPortMmrpAttr entry "
                      "Failed. \r\n"));
            return OSIX_FAILURE;
        }
        MEMSET (pMrpMapPortEntry->pu1AttrInfoList, 0,
                (sizeof (UINT1) * (MRP_MAX_MMRP_ATTR_IN_SYS + 1)));
    }

    /* LV BitList Memory */
    for (u1Index = 0; u1Index < MRP_LEAVE_SPLIT_INTERVAL; u1Index++)
    {
        if (pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
        {
            pMrpMapPortEntry->apLeaveList[u1Index] =
                (UINT1 *) MemAllocMemBlk (gMrpGlobalInfo.MvrpLvBitListPoolId);

            if (pMrpMapPortEntry->apLeaveList[u1Index] == NULL)
            {
                MRP_TRC ((pMrpMapEntry->pMrpAppEntry->pMrpContextInfo,
                          MRP_CRITICAL_TRC,
                          "MrpMapDSInitMrpMapPortEntry: Memory "
                          "Allocate for  MmrpLvBitList Failed. \r\n"));
                return OSIX_FAILURE;
            }
            MEMSET (pMrpMapPortEntry->apLeaveList[u1Index], 0,
                    MRP_MVRP_LV_BIT_LIST_BLK);
        }
        else if (pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
        {
            pMrpMapPortEntry->apLeaveList[u1Index] =
                (UINT1 *) MemAllocMemBlk (gMrpGlobalInfo.MmrpLvBitListPoolId);

            if (pMrpMapPortEntry->apLeaveList[u1Index] == NULL)
            {
                MRP_TRC ((pMrpMapEntry->pMrpAppEntry->pMrpContextInfo,
                          MRP_CRITICAL_TRC,
                          "MrpMapDSInitMrpMapPortEntry: Memory "
                          "Allocate for  MmrpLvBitList Failed. \r\n"));
                return OSIX_FAILURE;
            }
            MEMSET (pMrpMapPortEntry->apLeaveList[u1Index], 0,
                    MRP_MMRP_LV_BIT_LIST_BLK);
        }
    }

    pMrpMapPortEntry->u2LeavingAttrCnt = 0;
    if (pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        if (u2Port <= MRP_MAX_PORTS_PER_CONTEXT)
        {
            u1PortState = L2IwfGetInstPortState (pMrpMapEntry->u2MapId,
                                                 MRP_GET_PHY_PORT
                                                 (pMrpMapEntry->pMrpAppEntry->
                                                  pMrpContextInfo->u4ContextId,
                                                  u2Port));
        }
    }
    else if (pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        /* For MMRP, mapId represents Vlan Identifier */
        if (u2Port <= MRP_MAX_PORTS_PER_CONTEXT)
        {
            u1PortState = L2IwfGetVlanPortState (pMrpMapEntry->u2MapId,
                                                 MRP_GET_PHY_PORT
                                                 (pMrpMapEntry->pMrpAppEntry->
                                                  pMrpContextInfo->u4ContextId,
                                                  u2Port));
        }
    }
    pMrpMapPortEntry->u1PortState = u1PortState;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSDeInitMrpMapPortEntry
 *                                                                          
 *    DESCRIPTION      : This function is used de-inits the MAP port entry 
 *
 *    INPUT            : pMrpMapPortEntry - Pointer to the MAP port structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapDSDeInitMrpMapPortEntry (tMrpMapPortEntry * pMrpMapPortEntry)
{
    UINT1               u1Index = 0;

    if (pMrpMapPortEntry->pu1AttrInfoList != NULL)
    {
        if (pMrpMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId
            == MRP_MVRP_APP_ID)
        {
            MemReleaseMemBlock (gMrpGlobalInfo.MvrpAttrInfoListPoolId,
                                pMrpMapPortEntry->pu1AttrInfoList);
        }
        else if (pMrpMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId
                 == MRP_MMRP_APP_ID)
        {
            MemReleaseMemBlock (gMrpGlobalInfo.MmrpAttrInfoListPoolId,
                                pMrpMapPortEntry->pu1AttrInfoList);
        }
        pMrpMapPortEntry->pu1AttrInfoList = NULL;
    }

    for (u1Index = 0; u1Index < MRP_LEAVE_SPLIT_INTERVAL; u1Index++)
    {
        if (pMrpMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId
            == MRP_MVRP_APP_ID)
        {
            if (pMrpMapPortEntry->apLeaveList[u1Index] != NULL)
            {
                MemReleaseMemBlock (gMrpGlobalInfo.MvrpLvBitListPoolId,
                                    pMrpMapPortEntry->apLeaveList[u1Index]);
            }
        }
        else if (pMrpMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId
                 == MRP_MMRP_APP_ID)
        {
            if (pMrpMapPortEntry->apLeaveList[u1Index] != NULL)
            {
                MemReleaseMemBlock (gMrpGlobalInfo.MmrpLvBitListPoolId,
                                    pMrpMapPortEntry->apLeaveList[u1Index]);
            }
        }
        pMrpMapPortEntry->apLeaveList[u1Index] = NULL;
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSDeleteMrpMapPortEntry
 *                                                                          
 *    DESCRIPTION      : This function deletes the MAP port entry.
 *
 *    INPUT            : pMrpMapPortEntry - Pointer to the MAP port structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapDSDeleteMrpMapPortEntry (tMrpMapPortEntry * pMrpMapPortEntry)
{
    MrpTmrStop (&(pMrpMapPortEntry->LeaveTmrNode));

    MrpMapDSDelPortEntryFromMapEntry (pMrpMapPortEntry->pMapEntry,
                                      pMrpMapPortEntry);
    MrpMapDSDeInitMrpMapPortEntry (pMrpMapPortEntry);

    MemReleaseMemBlock (gMrpGlobalInfo.MapPortPoolId,
                        (UINT1 *) pMrpMapPortEntry);
    pMrpMapPortEntry = NULL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSAddAttrEntryToMapEntry
 *                                                                          
 *    DESCRIPTION      : This function adds the attribute entry to the MAP
 *                       structure.
 *
 *    INPUT            : pMrpMapEntry  - Pointer to the MAP structure
 *                       pMrpAttrEntry - Pointer to the Attribute structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapDSAddAttrEntryToMapEntry (tMrpMapEntry * pMrpMapEntry,
                                tMrpAttrEntry * pMrpAttrEntry)
{
    if (RBTreeAdd (pMrpMapEntry->pMrpAppEntry->MapAttrTable,
                   (tRBElem *) pMrpAttrEntry) == RB_FAILURE)
    {
        MRP_TRC ((pMrpMapEntry->pMrpAppEntry->pMrpContextInfo,
                  (INIT_SHUT_TRC | ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                  "MrpMapDSAddAttrEntryToMapEntry: Failed to add the "
                  "attribute entry in the RBTree. \r\n"));
        return OSIX_FAILURE;
    }
    MrpAttrSetAttrIndexToAttribute (pMrpMapEntry, pMrpAttrEntry);

    if (pMrpMapEntry->pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        pMrpMapEntry->pMrpAppEntry->pu1MmrpAttrIndexList
            [pMrpAttrEntry->u2AttrIndex] = 1;
    }

    pMrpMapEntry->ppAttrEntryArray[pMrpAttrEntry->u2AttrIndex] = pMrpAttrEntry;
    pMrpMapEntry->u2AttrCount++;

    if (pMrpMapEntry->pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        MrpAttrUpdMapNextFreeAttrIndex (pMrpMapEntry,
                                        pMrpAttrEntry->u2AttrIndex, MRP_ADD);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSAddPortEntryToMapEntry
 *                                                                          
 *    DESCRIPTION      : This function adds the MAP port entry to the MAP
 *                       structure.
 *
 *    INPUT            : pMrpMapEntry     - Pointer to the MAP structure
 *                       pMrpMapPortEntry - Pointer to the MAP port structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapDSAddPortEntryToMapEntry (tMrpMapEntry * pMrpMapEntry,
                                tMrpMapPortEntry * pMrpMapPortEntry)
{
    if (MRP_IS_PORT_VALID (pMrpMapPortEntry->u2Port) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC,
                     "MrpMapDSAddPortEntryToMapEntry: "
                     "Invalid Port.Port NOT created.\n");
        return OSIX_FAILURE;
    }

    if (MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, pMrpMapPortEntry->u2Port) == NULL)
    {
        pMrpMapEntry->apMapPortEntry[pMrpMapPortEntry->u2Port]
            = pMrpMapPortEntry;

    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSAddAttrToMapPortEntry
 *                                                                          
 *    DESCRIPTION      : This function adds the attribute information in the 
 *                       MAP port entry structure.
 *
 *    INPUT            : pMrpMapPortEntry - Pointer to the MAP port structure
 *                       pMrpAttrEntry    - Pointer to the Attribute structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapDSAddAttrToMapPortEntry (tMrpMapPortEntry * pMrpMapPortEntry,
                               tMrpAttrEntry * pMrpAttrEntry)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo =
        pMrpMapPortEntry->pMapEntry->pMrpAppEntry->pMrpContextInfo;
    pMrpPortEntry =
        MRP_GET_PORT_ENTRY (pMrpContextInfo, pMrpMapPortEntry->u2Port);

    /* Check added for to avoid klockwork warnings */
    if (pMrpPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    u1AppId = pMrpMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId;

    if (pMrpMapPortEntry->pu1AttrInfoList[pMrpAttrEntry->u2AttrIndex] != 0)
    {
        /* Attribute lready added */
        return OSIX_SUCCESS;
    }
    pMrpMapPortEntry->pu1AttrInfoList[pMrpAttrEntry->u2AttrIndex] = 0;

    pMrpAttrEntry->u2PortCount++;

    if (u1AppId == MRP_MVRP_APP_ID)
    {
        MrpIfAddAttrToPortMvrpTable (pMrpPortEntry, pMrpAttrEntry);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSDelAttrEntryFromMapEntry
 *                                                                          
 *    DESCRIPTION      : This function deletes the attribute entry from the MAP
 *                       structure.
 *
 *    INPUT            : pMrpMapEntry  - Pointer to the MAP structure
 *                       pMrpAttrEntry - Pointer to the Attribute structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapDSDelAttrEntryFromMapEntry (tMrpMapEntry * pMrpMapEntry,
                                  tMrpAttrEntry * pMrpAttrEntry)
{
    if (MRP_GET_ATTR_ENTRY (pMrpMapEntry, pMrpAttrEntry->u2AttrIndex) != NULL)
    {
        if (pMrpAttrEntry->u2PortCount != 0)
        {
            /* There are ports on which this Attribute is registered. Hence it 
             * cannot be deleted */
            return OSIX_SUCCESS;
        }

        pMrpMapEntry->ppAttrEntryArray[pMrpAttrEntry->u2AttrIndex] = NULL;

        pMrpMapEntry->u2AttrCount--;

        if (pMrpMapEntry->pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
        {
            pMrpMapEntry->pMrpAppEntry->pu1MmrpAttrIndexList
                [pMrpAttrEntry->u2AttrIndex] = 0;
        }

        RBTreeRem (pMrpMapEntry->pMrpAppEntry->MapAttrTable,
                   ((tRBElem *) pMrpAttrEntry));

        if (pMrpMapEntry->pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
        {
            MrpAttrUpdMapNextFreeAttrIndex (pMrpMapEntry,
                                            pMrpAttrEntry->u2AttrIndex,
                                            MRP_DELETE);
        }

        MemReleaseMemBlock (gMrpGlobalInfo.AttrPoolId, (UINT1 *) pMrpAttrEntry);
        pMrpAttrEntry = NULL;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSDelPortEntryFromMapEntry
 *                                                                          
 *    DESCRIPTION      : This function deletes the MAP port entry from the MAP
 *                       structure.
 *
 *    INPUT            : pMrpMapEntry     - Pointer to the MAP structure
 *                       pMrpMapPortEntry - Pointer to the MAP port structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapDSDelPortEntryFromMapEntry (tMrpMapEntry * pMrpMapEntry,
                                  tMrpMapPortEntry * pMrpMapPortEntry)
{
    if (MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, pMrpMapPortEntry->u2Port) != NULL)
    {
        pMrpMapEntry->apMapPortEntry[pMrpMapPortEntry->u2Port] = NULL;

    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapDSDelAttrFromMapPortEntry
 *                                                                          
 *    DESCRIPTION      : This function deletes the attribute information from  
 *                       the MAP port entry structure.
 *
 *    INPUT            : pMrpMapPortEntry - Pointer to the MAP port structure
 *                       pMrpAttrEntry    - Pointer to the Attribute structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapDSDelAttrFromMapPortEntry (tMrpMapPortEntry * pMrpMapPortEntry,
                                 tMrpAttrEntry * pMrpAttrEntry)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    UINT1               u1RegAdminCtrl = 0;

    if (pMrpMapPortEntry->pu1AttrInfoList[pMrpAttrEntry->u2AttrIndex] == 0)
    {
        /* Entry Already Deleted. So returning */
        return OSIX_SUCCESS;
    }

    pAppPortEntry =
        MRP_GET_APP_PORT_ENTRY (pMrpAttrEntry->pMapEntry->pMrpAppEntry,
                                pMrpMapPortEntry->u2Port);

    if (pAppPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    u1RegAdminCtrl = MRP_GET_ATTR_REG_ADMIN_CTRL
        (pMrpMapPortEntry->pu1AttrInfoList[pMrpAttrEntry->u2AttrIndex]);

    pMrpContextInfo =
        pMrpMapPortEntry->pMapEntry->pMrpAppEntry->pMrpContextInfo;
    pMrpMapPortEntry->pu1AttrInfoList[pMrpAttrEntry->u2AttrIndex] = 0;

    if (pAppPortEntry->pPeerMacAddrList == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pAppPortEntry->pPeerMacAddrList[pMrpAttrEntry->u2AttrIndex],
            0, MRP_MAC_ADDR_LEN);

    if (pMrpMapPortEntry->u2LeavingAttrCnt != 0)
    {
        MrpTmrStopLeaveTmr (pMrpMapPortEntry, pMrpAttrEntry->u2AttrIndex);
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        pMrpMapPortEntry->u2Port);

    /* Check Added to avoid klockwork warnings */
    if (pMrpPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pMrpMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        MrpIfDelAttrFromPortMvrpTable (pMrpPortEntry, pMrpAttrEntry);
    }

    pMrpAttrEntry->u2PortCount--;
    if (u1RegAdminCtrl == MRP_REG_FIXED)
    {
        pMrpAttrEntry->u2StaticMemPortCnt--;
    }

    MrpMapDSDelAttrEntryFromMapEntry (pMrpMapPortEntry->pMapEntry,
                                      pMrpAttrEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpMapDSGetAttrEntryFrmMapEntry
 *
 *    DESCRIPTION      : Utility function used to resize the AttrInfoByte 
 *                       array present in the tMrpMapPortEntry.
 *
 *    INPUT            : pMrpMapEntry     - Pointer to the MrpMapEnrty 
 *                                              structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : Pointer to the attribute entry 
 *
 ****************************************************************************/
tMrpAttrEntry      *
MrpMapDSGetAttrEntryFrmMapEntry (tMrpAttr * pAttr, tMrpMapEntry * pMrpMapEntry)
{
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAttrEntry       AttrEntry;

    MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));
    MEMCPY (&(AttrEntry.Attr), pAttr, sizeof (tMrpAttr));
    AttrEntry.pMapEntry = pMrpMapEntry;

    pAttrEntry = RBTreeGet (pMrpMapEntry->pMrpAppEntry->MapAttrTable,
                            (tRBElem *) (&AttrEntry));

    return pAttrEntry;

}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME       : MrpMapDSDelAllAttrInMapOnPort                  
 *                                                                           
 *    DESCRIPTION         : This function will delete all attributes present 
 *                          in the given port and MAP.
 *                                                                           
 *    INPUT(s)            : pMrpMapEntry - MAP Entry.                     
 *                          pMrpMapPortEntry - MAP Port Entry                
 *                                                                           
 *    OUTPUT(s)           : None.                                            
 *                                                                           
 *    RETURNS             : OSIX_SUCCESS / OSIX_FAILURE                      
 *                                                
 *                                                                           
 *****************************************************************************/
VOID
MrpMapDSDelAllAttrInMapOnPort (tMrpMapEntry * pMrpMapEntry,
                               tMrpMapPortEntry * pMrpMapPortEntry)
{
    tMrpAttrEntry      *pMrpAttrEntry = NULL;
    UINT2               u2Index = 0;

    for (u2Index = 1; u2Index <= pMrpMapEntry->u2AttrArraySize; u2Index++)
    {
        pMrpAttrEntry = MRP_GET_ATTR_ENTRY (pMrpMapEntry, u2Index);

        if (pMrpAttrEntry != NULL)
        {
            MrpMapDSDelAttrFromMapPortEntry (pMrpMapPortEntry, pMrpAttrEntry);
        }
    }
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME       : MrpMapDsCheckAndDeleteMapEntry                 
 *                                                                           
 *    DESCRIPTION         : This function checks whether any attributes      
 *                          present in this MAP entry. If there is no 
 *                          attribute, this function deletes the port entries
 *                          present in this MAP entry as well as the MAP
 *                          entry itself.
 *                                                                           
 *    INPUT(s)            : pMrpMapEntry - MAP Entry.                     
 *                                                                           
 *    OUTPUT(s)           : None.                                            
 *                                                                           
 *    RETURNS             : None.
 *                                                
 *                                                                           
 *****************************************************************************/
VOID
MrpMapDsCheckAndDeleteMapEntry (tMrpMapEntry * pMrpMapEntry)
{
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT2               u2Port = 0;

    if (pMrpMapEntry->u2AttrCount == 0)
    {
        /* When there is no attribute, delete the all the Port entries 
         * present in the MAP Entry and also delete the MAP entry. */
        for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
        {
            pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, u2Port);

            if (pMapPortEntry != NULL)
            {
                MrpMapDSDeleteMrpMapPortEntry (pMapPortEntry);
            }
        }

        MrpMapDSDeleteMrpMapEntry (pMrpMapEntry);
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpmapds.c                     */
/*-----------------------------------------------------------------------*/
