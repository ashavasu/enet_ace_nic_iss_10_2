/***********************************************************************
 *  Copyright (C) 2009 Aricent Inc. All Rights Reserved.
 *
 *  $Id: mrpcli.c,v 1.22 2013/06/25 12:08:37 siva Exp $
 *
 *  Description : This file contains CLI SET/GET/TEST and GETNEXT
 *                routines for the MIB objects specified in MRP mibs.
 ************************************************************************/
#ifndef __MRPCLI_C__
#define __MRPCLI_C__

#include "mrpinc.h"
#include "fsmrpcli.h"
#include "std1d1cli.h"
#include "mrpcli.h"

/*************************************************************************
 *
 *  FUNCTION NAME   : cli_process_mrp_cmd
 *
 *  DESCRIPTION     : CLI message handler function
 *
 *  INPUT           : CliHandle - CliContext ID
 *                    u4Command - Command identifier
 *                    ...       - Variable command argument list
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
cli_process_mrp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    INT4                i4RetStatus = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4ContextId = MRP_CLI_INVALID_CONTEXT;
    UINT4               u4TempContextId = MRP_CLI_INVALID_CONTEXT;
    UINT4               u4MrpApp = 0;
    UINT4               u4NotifyType = 0;
    UINT4               u4AttrType = 0;
    UINT2               u2LocalPortId = 0;
    INT1                i1argno = 0;
    UINT4              *apu4args[MRP_MAX_ARGS];
    UINT1              *pu1Inst = NULL;

    CliRegisterLock (CliHandle, MrpLock, MrpUnLock);

    MRP_LOCK ();

    if (MrpCliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId, &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */

    pu1Inst = va_arg (ap, UINT1 *);

    if (pu1Inst != 0)
    {
        u4IfIndex = CLI_PTR_TO_U4 (pu1Inst);
    }

    /* Walk through the rest of the arguments and store in apu4args array. 
     * Store 7 arguements at the max. This is because mrp commands do not
     * take more than Seven inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        apu4args[i1argno++] = va_arg (ap, UINT4 *);

        if (i1argno == MRP_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_MRP_SHUT_MRP:

            i4RetStatus = MrpCliSetSystemCtrl (CliHandle, u4ContextId,
                                               MRP_SHUTDOWN);
            break;

        case CLI_MRP_START_MRP:

            i4RetStatus = MrpCliSetSystemCtrl (CliHandle, u4ContextId,
                                               MRP_START);
            break;

        case CLI_MRP_APPL_STATUS:

            /* apu4args[0] - MRP Applications (MVRP/MMRP) to enable/disable
             * apu4args[1] - ENABLE/DISABLE Value */

            u4MrpApp = CLI_PTR_TO_U4 (apu4args[0]);

            if (u4MrpApp == CLI_MRP_MVRP)
            {
                i4RetStatus = MrpCliSetMvrpStatus (CliHandle, u4ContextId,
                                                   CLI_PTR_TO_U4 (apu4args[1]));
            }

            else
            {
                i4RetStatus = MrpCliSetMmrpStatus (CliHandle, u4ContextId,
                                                   CLI_PTR_TO_U4 (apu4args[1]));
            }

            break;

        case CLI_MRP_PORT_APPL_STATUS:

            /* apu4args[0] - MRP Applications (MVRP/MMRP) to enable/disable
             * apu4args[1] - ENABLE/DISABLE Value*/

            u4MrpApp = CLI_PTR_TO_U4 (apu4args[0]);

            if (MrpPortGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                  &u4TempContextId,
                                                  &u2LocalPortId) !=
                OSIX_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Interface not mapped to any of the "
                           "context.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (u4MrpApp == CLI_MRP_MVRP)
            {
                i4RetStatus = MrpCliSetPortMvrpStatus (CliHandle, u4ContextId,
                                                       CLI_PTR_TO_U4 (apu4args
                                                                      [1]),
                                                       (UINT4) u2LocalPortId);
            }
            else
            {
                i4RetStatus = MrpCliSetPortMmrpStatus (CliHandle, u4ContextId,
                                                       CLI_PTR_TO_U4 (apu4args
                                                                      [1]),
                                                       (UINT4) u2LocalPortId);
            }

            break;

        case CLI_MRP_PERIODIC_TIMER:

            /* apu4args[0] - Periodic Timer Status enable/disable  */

            if (MrpPortGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                  &u4TempContextId,
                                                  &u2LocalPortId) !=
                OSIX_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Interface not mapped to any of the "
                           "context.\r\n ");
                break;
            }

            i4RetStatus = MrpCliSetPeriodicTimerStatus (CliHandle, u4ContextId,
                                                        CLI_PTR_TO_U4 (apu4args
                                                                       [0]),
                                                        (UINT4) u2LocalPortId);
            break;

        case CLI_MRP_PARTICIPANT_TYPE:

            /* apu4args[0] - Participant Type (Full participant/
             * Applicant Only) */

            if (MrpPortGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                  &u4TempContextId,
                                                  &u2LocalPortId) !=
                OSIX_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Interface not mapped to any of the "
                           "context.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = MrpCliSetMrpParticipantType (CliHandle, u4ContextId,
                                                       CLI_PTR_TO_U4 (apu4args
                                                                      [0]),
                                                       (UINT4) u2LocalPortId);
            break;

        case CLI_MRP_APP_ADMIN_CTRL:

            /* apu4args[0] - MRP Application Type (MVRP/MMRP)
               apu4args[1] - MMRP Attribute Type (MAC/SERVICE REQ)
               apu4args[2] - Applicant Admin Control (Normal/Non-participant) */

            u4MrpApp = CLI_PTR_TO_U4 (apu4args[0]);

            if (MrpPortGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                  &u4TempContextId,
                                                  &u2LocalPortId) !=
                OSIX_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Interface not mapped to any of the "
                           "context.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            /* In MVRP Attribute Type is always 1.So Set here. */

            if (u4MrpApp == CLI_MRP_MVRP)
            {
                u4AttrType = MRP_VID_ATTR_TYPE;
                i4RetStatus = MrpCliSetMvrpAppAdminCtrl (CliHandle, u4ContextId,
                                                         (INT4) u4AttrType,
                                                         CLI_PTR_TO_U4 (apu4args
                                                                        [2]),
                                                         (INT4) u2LocalPortId);
            }

            else
            {
                i4RetStatus = MrpCliSetMmrpAppAdminCtrl (CliHandle, u4ContextId,
                                                         CLI_PTR_TO_U4 (apu4args
                                                                        [1]),
                                                         CLI_PTR_TO_U4 (apu4args
                                                                        [2]),
                                                         (UINT4) u2LocalPortId);
            }

            break;

        case CLI_MRP_NOTIFY_REG_FAIL:

            /* apu4args[0] -  To enable/disable Notification 
               apu4args[1] - Notification Type (VLAN/MAC) */

            u4NotifyType = CLI_PTR_TO_U4 (apu4args[1]);

            if (u4NotifyType == CLI_MRP_MVRP)
            {
                i4RetStatus = MrpCliSetNotifyVlanRegFail (CliHandle,
                                                          u4ContextId,
                                                          CLI_PTR_TO_U4
                                                          (apu4args[0]));
            }

            else
            {
                i4RetStatus = MrpCliSetNotifyMacRegFail (CliHandle,
                                                         u4ContextId,
                                                         CLI_PTR_TO_U4 (apu4args
                                                                        [0]));
            }
            break;

        case CLI_MRP_CLEAR_STATISTICS:

            /* apu4args[0] -  MRP Application Type (MVRP/MMRP) or MRP */

            if (u4IfIndex != 0)
            {
                if (MrpPortGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                      &u4TempContextId,
                                                      &u2LocalPortId) !=
                    OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Interface not mapped to any of the "
                               "context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

            }
            else
            {
                u2LocalPortId = 0;
            }

            i4RetStatus = MrpCliClearMrpStatistics (CliHandle, u4ContextId,
                                                    CLI_PTR_TO_U4 (apu4args[0]),
                                                    u2LocalPortId);
            break;

        case CLI_MRP_CLEAR_CONFIG:

            /* apu4args[0] -  MRP Application Type (MVRP/MMRP) or MRP */
            if (u4IfIndex != 0)
            {
                if (MrpPortGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                      &u4TempContextId,
                                                      &u2LocalPortId) !=
                    OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Interface not mapped to any of the "
                               "context.\r\n ");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

            }
            else
            {
                u2LocalPortId = 0;
            }

            i4RetStatus = MrpCliClearMrpConfiguration (CliHandle, u4ContextId,
                                                       CLI_PTR_TO_U4
                                                       (apu4args[0]),
                                                       u2LocalPortId);

            break;

        case CLI_MRP_JOIN_TIMER:
        case CLI_MRP_LEAVE_TIMER:
        case CLI_MRP_LEAVE_ALL_TIMER:

            /* apu4args[0] - Timer value */

            if (MrpPortGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                  &u4TempContextId,
                                                  &u2LocalPortId) !=
                OSIX_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Interface not mapped to any of the "
                           "context.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4RetStatus = MrpCliSetMrpTimers (CliHandle, u4ContextId,
                                              u4Command,
                                              *(apu4args[0]),
                                              (UINT4) u2LocalPortId);
            break;

        case CLI_MRP_RESTRICTED_VLAN:

            /* apu4args[0] - ENABLE/DISABLE Value */

            i4RetStatus = MrpCliSetRestrictedVlanReg (CliHandle, u4ContextId,
                                                      CLI_PTR_TO_U4 (apu4args
                                                                     [0]),
                                                      (UINT4) u2LocalPortId);
            break;

        case CLI_MRP_RESTRICTED_MAC:

            /* apu4args[0] - ENABLE/DISABLE Value */
            i4RetStatus = MrpCliSetRestrictedMacReg (CliHandle, u4ContextId,
                                                     CLI_PTR_TO_U4 (apu4args
                                                                    [0]),
                                                     (UINT4) u2LocalPortId);
            break;

        case CLI_MRP_REGISTRAR_ADMIN_CTRL:

            u4IfIndex = CLI_PTR_TO_U4 (apu4args[1]);
            if (MrpPortGetContextInfoFromIfIndex ((UINT2) u4IfIndex,
                                                  &u4TempContextId,
                                                  &u2LocalPortId) !=
                OSIX_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Interface not mapped to any of the "
                           "context.\r\n ");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            /* apu4args[0] - ENABLE/DISABLE Value */
            i4RetStatus = MrpCliSetRegisterAdminCtrl (CliHandle, u4ContextId,
                                                      (UINT1) (CLI_PTR_TO_U4
                                                               (apu4args[0])),
                                                      u2LocalPortId);
            break;
#ifdef TRACE_WANTED
        case CLI_MRP_DEBUG:

            /* apu4args[0] - Switch Name */
            /* apu4args[1] - Trace Input */

            if (apu4args[0] != NULL)
            {
                if (MrpPortVcmIsSwitchExist
                    ((UINT1 *) apu4args[0], &u4ContextId) != OSIX_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", apu4args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = MRP_DEFAULT_CONTEXT_ID;
            }

            i4RetStatus = MrpCliUpdateTraceInput (CliHandle,
                                                  u4ContextId,
                                                  (UINT1 *) (apu4args[1]));
            break;

        case CLI_MRP_GBL_DEBUG:

            i4RetStatus = MrpCliUpdateGlobalTrace (CliHandle, u4ContextId,
                                                   MRP_ENABLED);
            break;

        case CLI_MRP_NO_GBL_DEBUG:

            i4RetStatus = MrpCliUpdateGlobalTrace (CliHandle, u4ContextId,
                                                   MRP_DISABLED);
            break;

#else
        case CLI_MRP_DEBUG:
        case CLI_MRP_GBL_DEBUG:
            /* fall through */
        case CLI_MRP_NO_GBL_DEBUG:
            CliPrintf (CliHandle, "\r%% Debug not supported \r\n");
            MRP_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
#endif

        default:
            /* Given command does not match with any SET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            MRP_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_MRP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gaMrpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    MRP_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*************************************************************************
 *
 *  FUNCTION NAME   : cli_process_mrp_sh_cmd
 *
 *  DESCRIPTION     : CLI message handler function
 *
 *  INPUT           : CliHandle - CliContext ID
 *                    u4Command - Command identifier
 *                    ...       - Variable command argument list
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

INT4
cli_process_mrp_sh_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMacAddr            MacAddr;
    INT4                i4RetStatus = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4ContextId = MRP_CLI_INVALID_CONTEXT;
    UINT4               u4TempContextId = MRP_CLI_INVALID_CONTEXT;
    UINT4               u4AppId = 0;
    UINT1               u1SerReqVal = MRP_INVALID_SER_REQ_VALUE;
    UINT2               u2LocalPortId = 0;
    UINT2               u2VlanId = 0;
    INT1                i1argno = 0;
    UINT1              *pu1Inst = NULL;
    UINT4              *apu4args[MRP_MAX_ARGS];
    UINT1              *pu1ContextName = NULL;
    UINT1               u1DisplayFlag = 0;
    UINT1              *pu1MacAddr = NULL;

    CliRegisterLock (CliHandle, MrpLock, MrpUnLock);

    MRP_LOCK ();
    va_start (ap, u4Command);

    /* third arguement is always interface name/index */

    pu1Inst = va_arg (ap, UINT1 *);
    if (pu1Inst != 0)
    {
        u4IfIndex = CLI_PTR_TO_U4 (pu1Inst);
    }

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     * After the u4IfIndex. (ie) In all the cli commands we are passing 
     * IfIndex as the first argument in variable argument list. Like that 
     * as the second argument we have to pass context-name*/
    pu1ContextName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguments and store in apu4args array. 
     * Store 7 arguements at the max. This is because mrp commands do not
     * take more than Seven inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */

    while (1)
    {
        apu4args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == MRP_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    if (MrpCliGetContextForShowCmd
        (CliHandle, pu1ContextName, u4IfIndex, u4TempContextId,
         &u4ContextId, &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    if ((u4ContextId == 0) || (u4ContextId >= MRP_SIZING_CONTEXT_COUNT))
    {
        MRP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    /* If Context Name and IfIndex are not given , then we have to 
       display all the Ports and context related Information. */

    if ((pu1ContextName == NULL) && (u4IfIndex == 0))
    {
        u1DisplayFlag = MRP_SHOW_ALL;
    }

    /* If Context Name  is only given means,then we have to display
     * all the Ports inform ation present in that context identified 
     * by the Given Context Name */

    else if (pu1ContextName != NULL)
    {
        u1DisplayFlag = MRP_SHOW_CONTEXT_ONLY;
    }
    else
    {
        u1DisplayFlag = MRP_SHOW_PORT_ONLY;
    }

    if ((u1DisplayFlag == MRP_SHOW_PORT_ONLY) ||
        (u1DisplayFlag == MRP_SHOW_CONTEXT_ONLY))
    {
        /* In this case Context Identifier is obtained from the function
         * MrpCliGetContextForShowCmd. 
         */
        if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
        {
            if (MrpPortVcmGetSystemModeExt (MRP_PROTOCOL_ID) == VCM_MI_MODE)
            {
                pMrpContextInfo = gMrpGlobalInfo.apContextInfo[u4ContextId];

                if (pMrpContextInfo != NULL)
                {
                    CliPrintf (CliHandle, "\n Switch Name : %s \n",
                               pMrpContextInfo->au1ContextName);
                    CliPrintf (CliHandle, " -----------------------\n");
                }
            }
            CliPrintf (CliHandle, "\r%% MRP is shutdown.\r\n");
            MRP_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }
    }

    switch (u4Command)
    {
        case CLI_MRP_SHOW_CONFIG:

            /* apu4args[0] - MRP or MRP Application Type (MVRP/MMRP) */

            u4AppId = CLI_PTR_TO_U4 (apu4args[0]);

            i4RetStatus = MrpCliShowMrpConfig (CliHandle, u4ContextId,
                                               (UINT2) u4AppId, u1DisplayFlag,
                                               u2LocalPortId);
            break;

        case CLI_MRP_SHOW_STATISTICS:

            /* apu4args[0] - MRP or MRP Application Type (MVRP/MMRP) */

            u4AppId = CLI_PTR_TO_U4 (apu4args[0]);

            i4RetStatus = MrpCliShowMrpStatistics (CliHandle, u4ContextId,
                                                   (UINT1) u4AppId,
                                                   u1DisplayFlag,
                                                   u2LocalPortId);
            break;

        case CLI_MRP_SHOW_MVRP_MACHINES:

            if (apu4args[0] != NULL)
            {
                u2VlanId = (tVlanId) (*apu4args[0]);
            }

            /* apu4args[0] - VLAN Id */
            i4RetStatus = MrpCliShowMvrpStateMachines (CliHandle, u4ContextId,
                                                       u2VlanId, u1DisplayFlag,
                                                       u2LocalPortId);
            break;

        case CLI_MRP_SHOW_MMRP_MACHINES:

            /* apu4args[0[ - VLAN Id
               apu4args[1] - Mac address
               apu4args[2] - Service Req Value */

            if (apu4args[0] != NULL)
            {
                u2VlanId = (tVlanId) (*apu4args[0]);

                if (apu4args[1] != NULL)
                {
                    StrToMac ((UINT1 *) apu4args[1], MacAddr);
                    pu1MacAddr = MacAddr;
                }
                else
                {
                    u1SerReqVal = (UINT1) CLI_PTR_TO_U4 (apu4args[2]);
                }
            }

            i4RetStatus = MrpCliShowMmrpStateMachines (CliHandle, u4ContextId,
                                                       u2VlanId, pu1MacAddr,
                                                       u1SerReqVal,
                                                       u1DisplayFlag,
                                                       u2LocalPortId);

            break;

        case CLI_MRP_SHOW_MRP_TIMER:

            i4RetStatus = MrpCliShowMrpTimers (CliHandle, u4ContextId,
                                               u1DisplayFlag, u2LocalPortId);

            break;

        default:
            /* Given command does not match with any GET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");

            MRP_UNLOCK ();

            CliUnRegisterLock (CliHandle);

            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_MRP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gaMrpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    MRP_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : MrpCliSetSystemCtrl                               
 *                                                                          
 *     DESCRIPTION      : This function will start/shut Mrp module          
 *                                                                          
 *     INPUT            : u4ContextId - ConetxtId
 *                        i4Status - Mrp Module status                      
 *                                                                          
 *     OUTPUT           : CliHandle - Contains error messages               
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/
INT4
MrpCliSetSystemCtrl (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMrpInstanceSystemControl
        (&u4ErrorCode, u4ContextId, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpInstanceSystemControl (u4ContextId, i4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetMvrpStatus                                
 *                                                                            
 *     DESCRIPTION      : This function will enable/disable MVRP on the      
 *                        on the device.                                     
 *                                                                            
 *     INPUT            : u4ContextId - Context Id                    
 *                        u4Status  - Enable/Disable Gbl MVRP on the device  
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetMvrpStatus (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMrpInstanceBridgeMvrpEnabledStatus
        (&u4ErrorCode, u4ContextId, (INT4) u4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpInstanceBridgeMvrpEnabledStatus (u4ContextId,
                                                    (INT4) u4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetPortMvrpStatus                            
 *                                                                           
 *     DESCRIPTION      : This function enables/disables MVRP on port        
 *                                                                           
 *     INPUT            : u4ContextId - ContextId 
 *                        u4Status  - Enable/Disable MVRP on the port        
 *                        u4PortId  - Port identifier                        
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetPortMvrpStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4Status, UINT4 u4PortId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMvrpPortMvrpEnabledStatus (&u4ErrorCode,
                                              u4ContextId, u4PortId,
                                              (INT4) u4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMvrpPortMvrpEnabledStatus (u4ContextId,
                                           u4PortId,
                                           (INT4) u4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetMmrpStatus                                
 *                                                                       
 *     DESCRIPTION      : This function will enable/disable MMRP on the      
 *                        on the device.                                     
 *                                                                           
 *      INPUT           : u4ContextId - Virtual Context Id                   
 *                        u4Status  - Enable/Disable Gbl MMRP on the device  
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetMmrpStatus (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMrpInstanceBridgeMmrpEnabledStatus (&u4ErrorCode,
                                                       u4ContextId,
                                                       (INT4) u4Status) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpInstanceBridgeMmrpEnabledStatus (u4ContextId,
                                                    (INT4) u4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetPortMmrpStatus                            
 *                                                                           
 *     DESCRIPTION      : This function enables/disables MMRP on port        
 *                                                                           
 *     INPUT            : u4ContextId - Virtual Context Id                   
 *                        u4Status  - Enable/Disable MMRP on the port        
 *                        u4PortId  - Port identifier                        
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetPortMmrpStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4Status, UINT4 u4PortId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Ieee8021BridgePortMmrpEnabledStatus (&u4ErrorCode, u4ContextId,
                                                      u4PortId,
                                                      (INT4) u4Status) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIeee8021BridgePortMmrpEnabledStatus (u4ContextId, u4PortId,
                                                   (INT4) u4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetPeriodicTimerStatus                       
 *                                                                           
 *     DESCRIPTION      : This function enables/disables Peridioc SEM Status 
 *                        on a  port                                         
 *                                                                           
 *     INPUT            : u4ContextId - Virtual Context Id                   
 *                        u4Action  - Enable/Disable Peridioc SEM Status     
 *                        u4PortId  - Port identifier                        
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetPeriodicTimerStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMrpPortPeriodicSEMStatus (&u4ErrorCode, u4ContextId,
                                             u4PortId,
                                             (INT4) u4Action) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpPortPeriodicSEMStatus (u4ContextId, u4PortId,
                                          (INT4) u4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetMrpParticipantType                        
 *                                                                           
 *     DESCRIPTION      : This function sets Type of MRP Participant on port 
 *                                                                           
 *     INPUT            : u4ContextId - Virtual Context Id                   
 *                        u4Type  - Type of MRP Participant                  
 *                        u4PortId  - Port identifier                        
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetMrpParticipantType (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4Type, UINT4 u4PortId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMrpPortParticipantType (&u4ErrorCode, u4ContextId,
                                           u4PortId,
                                           (INT4) u4Type) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpPortParticipantType (u4ContextId, u4PortId,
                                        (INT4) u4Type) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetMvrpAppAdminCtrl                          
 *                                                                           
 *     DESCRIPTION      : This function sets MVRP Applicant Admin Control    
 *                        on a port.                                         
 *                                                                           
 *     INPUT            : u4ContextId - Context Id  
 *                        i4AttributeType - Attribute Type
 *                        u4AdminCtrl  - Normal/Non-participant Type         
 *                        u4PortId  - Port identifier                        
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetMvrpAppAdminCtrl (tCliHandle CliHandle, UINT4 u4ContextId,
                           INT4 i4AttributeType, UINT4 u4AdminCtrl,
                           UINT4 u4PortId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4ErrorCode = 0;

    pMrpContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (pMrpContextInfo == NULL)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMrpApplicantControlAdminStatus (&u4ErrorCode, u4ContextId,
                                                   u4PortId,
                                                   pMrpContextInfo->MvrpAddr,
                                                   i4AttributeType,
                                                   (INT4) u4AdminCtrl) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpApplicantControlAdminStatus (u4ContextId, u4PortId,
                                                pMrpContextInfo->MvrpAddr,
                                                i4AttributeType,
                                                (INT4) u4AdminCtrl) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetMmrpAppAdminCtrl                          
 *                                                                           
 *     DESCRIPTION      : This function sets MMRP Applicant Admin Control    
 *                        on a port.                                         
 *                                                                           
 *     INPUT            : u4ContextId - Virtual Context Id                    
 *                        u4AttributeType - MAC/SERVICE-REQ                  
 *                        u4AdminCtrl  - Normal/Non-participant Type         
 *                        u4PortId  - Port identifier                        
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetMmrpAppAdminCtrl (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4AttributeType, UINT4 u4AdminCtrl,
                           UINT4 u4PortId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMrpApplicantControlAdminStatus (&u4ErrorCode, u4ContextId,
                                                   u4PortId, gMmrpAddr,
                                                   (INT4) u4AttributeType,
                                                   (INT4) u4AdminCtrl) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpApplicantControlAdminStatus (u4ContextId, u4PortId,
                                                gMmrpAddr,
                                                (INT4) u4AttributeType,
                                                (INT4) u4AdminCtrl) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                        
 *     FUNCTION NAME    : MrpCliSetNotifyVlanRegFail                         
 *                                                                           
 *     DESCRIPTION      : This function enables Notification for notify VLAN 
 *                          Reg Failures.                                    
 *                                                                           
 *     INPUT            : u4ContextId - Virtual Context Id                   
 *                        u4Status  - Enable/Disable Notify Vlan Reg Fail    
 *                                                                           
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetNotifyVlanRegFail (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMrpInstanceNotifyVlanRegFailure (&u4ErrorCode, u4ContextId,
                                                    (INT4) u4Status) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpInstanceNotifyVlanRegFailure (u4ContextId,
                                                 (INT4) u4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetNotifyMacRegFail                          
 *                                                                           
 *     DESCRIPTION      : This function enables Notification for notify MAC  
 *                          Reg Failures.                                    
 *                                                                           
 *     INPUT            : u4ContextId - Virtual Context Id                   
 *                        u4Status  - Enable/Disable Notify MAC Reg Fail     
 *                                                                           
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                          
 *****************************************************************************/
INT4
MrpCliSetNotifyMacRegFail (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMrpInstanceNotifyMacRegFailure (&u4ErrorCode, u4ContextId,
                                                   (INT4) u4Status) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpInstanceNotifyMacRegFailure (u4ContextId,
                                                (INT4) u4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *     FUNCTION NAME    : MrpCliClearMrpConfiguration
 *
 *     DESCRIPTION      : This function clears Context related 
 *                        MRP Configurations.
 *
 *     INPUT            : u4ContextId - Virtual Context Id
 *                        u4Application - MVRP/MMRP
 *                        
 *
 *     OUTPUT           : CliHandle - Contains error messages
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
INT4
MrpCliClearMrpConfiguration (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4Application, UINT2 u2PortId)
{
    UNUSED_PARAM (CliHandle);

    if (u2PortId != 0)
    {
        MrpCliClearConfigurationOnPort (u4ContextId, u2PortId, u4Application);
    }
    else
    {
        MrpCliClearConfiguration (u4ContextId, u4Application);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliClearMrpStatstics                            
 *                                                                           
 *     DESCRIPTION      : This function clear MRP statistics values.  
 *                                                                           
 *     INPUT            : u4ContextId - Virtual Context Id                   
 *                        u4Application - MVRP/MMRP                          
 *                        u4PortId  - Port identifier                        
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliClearMrpStatistics (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4Application, UINT2 u2PortId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMacAddr            MacAddr;
    UINT2               u2Index = u2PortId;
    UINT2               u2AppId = (UINT2) u4Application;
    UINT4               u4ErrCode = MRP_INIT_VAL;

    /*Get the ContextInfo ,If It is NULL means then returns 
     * CLI_FAILURE. */

    pMrpContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (pMrpContextInfo == NULL)
    {
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return CLI_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
    {
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return CLI_FAILURE;
    }

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    /* Get the Port Info  from Context Info  and clear the statstics info 
     * for the corresponding port .Repeated the same for all the ports if 
     * specific port Index is not given. */

    for (; u2Index <= MRP_MAX_PORTS_PER_CONTEXT; u2Index++)
    {
        pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Index);

        if (pMrpPortEntry == NULL)
        {
            /* If the Given port entry is not present , then returns
             * CLI_FAILURE. */

            if (u2PortId != 0)
            {
                return CLI_FAILURE;
            }
            continue;
        }

        if (u4Application == CLI_MRP_MVRP)
        {
            MEMCPY (MacAddr, pMrpContextInfo->MvrpAddr, sizeof (tMacAddr));
        }
        else
        {
            MEMCPY (MacAddr, gMmrpAddr, sizeof (tMacAddr));
        }

        for (; u2AppId <= MRP_MAX_APPS; u2AppId++)
        {
            if (nmhTestv2FsMrpPortStatsClearStatistics (&u4ErrCode,
                                                        u4ContextId,
                                                        (UINT4) u2Index,
                                                        MacAddr,
                                                        MRP_ENABLED) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsMrpPortStatsClearStatistics (u4ContextId,
                                                     (UINT4) u2Index, MacAddr,
                                                     MRP_ENABLED) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (u4Application == 0)
            {
                MEMCPY (MacAddr, pMrpContextInfo->MvrpAddr, sizeof (tMacAddr));
                continue;
            }

            if ((u4Application == CLI_MRP_MVRP) ||
                (u4Application == CLI_MRP_MMRP))
            {
                break;
            }
        }

        u2AppId = (UINT2) u4Application;

        /* If the Given Port Id is same as the u2Index means, then 
         * we should skip from the loop after clearing the statistics entry
         * for that Port. */

        if (u2Index == u2PortId)
        {
            break;
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpSetMrpTimers                                    
 *                                                                           
 *     DESCRIPTION      : This function sets MRP timer values                
 *                                                                           
 *     INPUT            : u4ContextId  - Context Id 
 *                        u4TimerType   - Type of timer to be configured    
 *                        u4TimerValue  - Timer value configured             
 *                        u4PortId      - Port identifier                    
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetMrpTimers (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4TimerType,
                    UINT4 u4TimerValue, UINT4 u4PortId)
{
    UINT4               u4ErrorCode = 0;

    switch (u4TimerType)
    {
        case CLI_MRP_JOIN_TIMER:

            if (nmhTestv2Ieee8021BridgePortMrpJoinTime (&u4ErrorCode,
                                                        u4ContextId, u4PortId,
                                                        (INT4) u4TimerValue) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetIeee8021BridgePortMrpJoinTime (u4ContextId,
                                                     u4PortId,
                                                     (INT4) u4TimerValue) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;

        case CLI_MRP_LEAVE_TIMER:

            if (nmhTestv2Ieee8021BridgePortMrpLeaveTime (&u4ErrorCode,
                                                         u4ContextId, u4PortId,
                                                         (INT4) u4TimerValue) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetIeee8021BridgePortMrpLeaveTime (u4ContextId,
                                                      u4PortId,
                                                      (INT4) u4TimerValue) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;

        case CLI_MRP_LEAVE_ALL_TIMER:

            if (nmhTestv2Ieee8021BridgePortMrpLeaveAllTime (&u4ErrorCode,
                                                            u4ContextId,
                                                            u4PortId,
                                                            (INT4) u4TimerValue)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetIeee8021BridgePortMrpLeaveAllTime (u4ContextId,
                                                         u4PortId,
                                                         (INT4) u4TimerValue) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            break;

        default:
            break;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetRestrictedVlanReg                        
 *                                                                           
 *     DESCRIPTION      : This function enables/disables a port to be part   
 *                        of a restriced VLAN.                               
 *                                                                           
 *     INPUT            : u4ContextId - ContextId
 *                        u4Action   - Action to be performed Set/Re-set     
 *                        u4PortId   - Port identifier                       
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetRestrictedVlanReg (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMrpPortRestrictedVlanRegistration
        (&u4ErrorCode, u4ContextId, u4PortId, (INT4) u4Action) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpPortRestrictedVlanRegistration
        (u4ContextId, u4PortId, (INT4) u4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliSetRestrictedMacReg                          
 *                                                                           
 *     DESCRIPTION      : This function enables/disables restricted MAC      
 *                        registeration on a port                            
 *                                                                           
 *     INPUT            : u4ContextId - ContextId
 *                        u4Action   - Action to be performed Set/Re-set     
 *                        u4PortId   - Port identifier                       
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliSetRestrictedMacReg (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4Action, UINT4 u4PortId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsMrpPortRestrictedGroupRegistration
        (&u4ErrorCode, u4ContextId, u4PortId, (INT4) u4Action) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpPortRestrictedGroupRegistration (u4ContextId,
                                                    u4PortId,
                                                    (INT4) u4Action) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#ifdef TRACE_WANTED

/****************************************************************************
 *
 *     FUNCTION NAME    : MrpCliUpddteTraceInput
 *
 *     DESCRIPTION      : This function updates(sets/resets) the trace input
 *
 *     INPUT            : CliHandle       - CliContext ID
 *                        u4ContextId     - Context Id
 *                        pu1TraceInput   - Pointer to trace input string
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
MrpCliUpdateTraceInput (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT1 *pu1TraceInput)
{
    tSNMP_OCTET_STRING_TYPE TraceInput;
    UINT4               u4ErrorCode = 0;

    MEMSET (&TraceInput, 0, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = (INT4) STRLEN (pu1TraceInput);

    if (nmhTestv2FsMrpInstanceTraceInputString (&u4ErrorCode, u4ContextId,
                                                &TraceInput) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMrpInstanceTraceInputString (u4ContextId, &TraceInput)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliUpdateGlobalTrace                            
 *                                                                           
 *     DESCRIPTION      : This function configures/deconfigures debug level  
 *                                                                           
 *     INPUT            : u4ContextId - Context Id
 *                        u4Action   - Set/Re-set action                     
 *                                                                           
 *     OUTPUT           : CliHandle - Contains error messages                
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 ****************************************************************************/
INT4
MrpCliUpdateGlobalTrace (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4Action)
{
    UINT4               u4ErrorCode = 0;
    UNUSED_PARAM (u4ContextId);

    if (nmhTestv2FsMrpGlobalTraceOption (&u4ErrorCode, (INT4) u4Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMrpGlobalTraceOption ((INT4) u4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}
#endif
/*****************************************************************************
 *                                                                           
 *     FUNCTION NAME    : MrpCliShowMrpTimers                                
 *                                                                           
 *     DESCRIPTION      : This function displays MRP timer values            
 *                                                                           
 *     INPUT            : u4ContextId - Context Id
 *                        u1DisplayFlag - Display Option
 *                        u4PortId - Port identifier                         
 *                                                                           
 *     OUTPUT           : CliHandle  - Contains error messages               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 ****************************************************************************/
INT4
MrpCliShowMrpTimers (tCliHandle CliHandle, UINT4 u4ContextId,
                     UINT1 u1DisplayFlag, UINT2 u2PortId)
{
    INT4                i4JoinTimerVal = 0;
    INT4                i4LeaveTimerVal = 0;
    INT4                i4LeaveAllTimerVal = 0;
    INT4                i4PeriodicSemStatus = MRP_DISABLED;
    UINT4               u4CurrentPortId = 0;
    UINT4               u4NextPortId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4NextContextId = 0;
    UINT4               u4CurrContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT1               au1IntfName[MRP_MAX_PORT_NAME_LENGTH];
    UINT1               u1isShowAll = OSIX_TRUE;

    MEMSET (au1IntfName, 0, MRP_MAX_PORT_NAME_LENGTH);

    if (u1DisplayFlag == MRP_SHOW_PORT_ONLY)
    {
        /*For Getting Next Port Index. */
        u4CurrentPortId = (UINT4) u2PortId - 1;
    }

    if (nmhGetNextIndexIeee8021BridgePortMrpTable
        (u4ContextId, &u4NextContextId, u4CurrentPortId, &u4NextPortId)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (u2PortId != 0)
        {
            u4NextPortId = (UINT4) u2PortId;
            if (nmhValidateIndexInstanceIeee8021BridgePortMrpTable
                (u4NextContextId, u4NextPortId) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }

        u4IfIndex = MRP_GET_PHY_PORT (u4NextContextId, u4NextPortId);

        MrpPortCfaCliGetIfName (u4IfIndex, (INT1 *) au1IntfName);

        if (u4CurrContextId != u4NextContextId)
        {
            MrpCliPrintMrpTimerTblHeader (CliHandle, u4NextContextId);
        }
        /* MRP join time */
        nmhGetIeee8021BridgePortMrpJoinTime (u4NextContextId, u4NextPortId,
                                             &i4JoinTimerVal);

        /* MRP leave time */

        nmhGetIeee8021BridgePortMrpLeaveTime (u4NextContextId, u4NextPortId,
                                              &i4LeaveTimerVal);

        /*MRP leave all time */

        nmhGetIeee8021BridgePortMrpLeaveAllTime (u4NextContextId, u4NextPortId,
                                                 &i4LeaveAllTimerVal);

        /* Periodic Timer Status */
        nmhGetFsMrpPortPeriodicSEMStatus (u4NextContextId, u4NextPortId,
                                          &i4PeriodicSemStatus);

        CliPrintf (CliHandle, "%-11d", i4JoinTimerVal);
        CliPrintf (CliHandle, "%-14d", i4LeaveTimerVal);
        CliPrintf (CliHandle, "%-18d", i4LeaveAllTimerVal);

        if (i4PeriodicSemStatus != MRP_ENABLED)
        {
            CliPrintf (CliHandle, "%-18s", "Disabled");
        }
        else
        {
            CliPrintf (CliHandle, "%-16s", "Enabled");
        }
        CliPrintf (CliHandle, "%-5d", u4NextPortId);
        u4PagingStatus = (UINT4) CliPrintf (CliHandle, "%-6s\n", au1IntfName);
        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User pressed 'q' at more prompt,
             * no more print required, exit
             */
            u1isShowAll = OSIX_FALSE;
        }

        u4CurrContextId = u4NextContextId;
        u4CurrentPortId = u4NextPortId;

        if (nmhGetNextIndexIeee8021BridgePortMrpTable (u4CurrContextId,
                                                       &u4NextContextId,
                                                       u4CurrentPortId,
                                                       &u4NextPortId) ==
            SNMP_FAILURE)
        {
            /*If the Get Next returns  NULL, then set 
               u1isShowAll value as false. */
            u1isShowAll = OSIX_FALSE;
        }
        if ((u1DisplayFlag == MRP_SHOW_CONTEXT_ONLY) &&
            (u4CurrContextId != u4NextContextId))
        {
            /*Set u1isShowAll flag value as false for Next 
               Context id. */
            u1isShowAll = OSIX_FALSE;
        }
        if (u2PortId != 0)
        {
            if (u2PortId != (UINT2) u4NextPortId)
            {
                /*Set u1isShowAll flag value as false for Next
                   Port Id */
                u1isShowAll = OSIX_FALSE;
            }
        }
    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : MrpCliPrintMrpTimerTblHeader
 *
 *     DESCRIPTION      : This function displays MRP Timer values
 *
 *     INPUT            : CliHandle - Handle to the cli context
 *                        u4ContextId - ContextId
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ****************************************************************************/
VOID
MrpCliPrintMrpTimerTblHeader (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (pMrpContextInfo != NULL)
    {
        CliPrintf (CliHandle, "\r Switch Name : %s ",
                   pMrpContextInfo->au1ContextName);
        CliPrintf (CliHandle, "\r\nMRP Port Timer Info (in centi seconds)\r\n");

        CliPrintf (CliHandle, "------------------------------------------\r\n");

        CliPrintf (CliHandle,
                   "Join-time  Leave-time    Leave All-time   PeriodicTimer   Port  IfName\r\n");

        CliPrintf (CliHandle,
                   "--------   ----------   ---------------   --------------  ----- ------\r\n");
    }
}

/****************************************************************************
 *     FUNCTION NAME    : MrpCliShowMrpStatistics                            
 *                                                                           
 *     DESCRIPTION      : This function displays port mrp statistics 
 *                        informations.        
 *                                                                           
 *     INPUT            : CliHandle - CliHandle Context               
 *                        u4ContextId - ContextId                            
 *                        u4Application - Application Type                   
 *                        u4PortId  - Port Id                                
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 ****************************************************************************/
INT4
MrpCliShowMrpStatistics (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT1 u1Application, UINT1 u1DisplayFlag,
                         UINT2 u2PortId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    UINT4               u4CurrContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPortId = 0;
    UINT4               u4CurrentPortId = 0;
    UINT1               u1isShowAll = OSIX_TRUE;
    UINT1               u1IsPrintHeader = OSIX_TRUE;

    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    if (u1DisplayFlag == MRP_SHOW_PORT_ONLY)
    {
        /*For Getting Next Port Index passing this Port Id 
         * as Current Port Id. */
        u4CurrentPortId = (UINT4) u2PortId;
    }

    if (u1Application == CLI_MRP_MVRP)
    {
        /*For getting the Next Mac addr as MVR address */
        MEMCPY (CurrMacAddr, gMmrpAddr, sizeof (tMacAddr));
    }

    CliPrintf (CliHandle, "\r\nMRP Port Statistics \r\n");

    CliPrintf (CliHandle, "------------------------\r\n");

    if (nmhGetNextIndexFsMrpPortStatsTable
        (u4ContextId, &u4NextContextId, u4CurrentPortId, &u4NextPortId,
         CurrMacAddr, &NextMacAddr) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (u1IsPrintHeader == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "\nSwitch Name : %s \n",
                       MRP_CONTEXT_PTR (u4NextContextId)->au1ContextName);
        }

        /* No need to put NULL check here.Because nmhValidate or nmhGetNext 
           routine takes care. */
        pMrpContextInfo = MRP_CONTEXT_PTR (u4NextContextId);

        if (u1Application == CLI_MRP_MVRP)
        {
            if (MEMCMP (NextMacAddr, pMrpContextInfo->MvrpAddr,
                        MRP_MAC_ADDR_LEN) == 0)
            {
                /*Displays Only MVRP Statistics. */
                MrpCliPrintStatistics (CliHandle, u4NextContextId,
                                       NextMacAddr, (UINT2) u4NextPortId);
            }
        }
        else if (u1Application == CLI_MRP_MMRP)
        {
            if (MEMCMP (NextMacAddr, gMmrpAddr, MRP_MAC_ADDR_LEN) == 0)
            {
                /*Displays Only MMRP Statistics. */
                MrpCliPrintStatistics (CliHandle, u4NextContextId,
                                       NextMacAddr, (UINT2) u4NextPortId);
            }
        }
        else
        {
            /*Displays Both MVRP and MMRP statistics. */
            MrpCliPrintStatistics (CliHandle, u4NextContextId,
                                   NextMacAddr, (UINT2) u4NextPortId);
        }

        u4CurrContextId = u4NextContextId;
        u4CurrentPortId = u4NextPortId;
        MEMCPY (CurrMacAddr, NextMacAddr, sizeof (CurrMacAddr));

        if (nmhGetNextIndexFsMrpPortStatsTable (u4CurrContextId,
                                                &u4NextContextId,
                                                u4CurrentPortId,
                                                &u4NextPortId,
                                                CurrMacAddr,
                                                &NextMacAddr) == SNMP_FAILURE)
        {
            /* If the Get Next Port Entry is NULL means,then
               set the Show All flag value as false. */
            u1isShowAll = OSIX_FALSE;

        }
        if (u4CurrContextId == u4NextContextId)
        {
            u1IsPrintHeader = OSIX_FALSE;
        }
        else
        {
            u1IsPrintHeader = OSIX_TRUE;
        }
        if (u2PortId != 0)
        {
            if (u2PortId != (UINT2) u4NextPortId)
            {
                /*If the Port Id is given means , then set the 
                   ShowAll flag value as false for Next Port Index. */
                u1isShowAll = OSIX_FALSE;
            }
        }
        if ((u1DisplayFlag == MRP_SHOW_CONTEXT_ONLY) &&
            (u4CurrContextId != u4NextContextId))
        {
            /*If the Context Id is given means , then set the value of 
               u1isShowAll as false for Next Context. */
            u1isShowAll = OSIX_FALSE;
        }
    }
    while (u1isShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : MrpCliPrintStatistics                             
 *                                                                          
 *     DESCRIPTION      : This function prints port mrp statistics 
 *                        informations .     
 *                                                                          
 *     INPUT            : u4ContextId - ContextId                           
 *                        MacAddr - Application Address                 
 *                        u2PortId  - Port Id                               
 *                                                                          
 *     OUTPUT           : None                                              
 *                                                                          
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           
 *                                                                          
 ****************************************************************************/

INT4
MrpCliPrintStatistics (tCliHandle CliHandle, UINT4 u4ContextId,
                       tMacAddr MacAddr, UINT2 u2PortId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tSNMP_COUNTER64_TYPE NumberOfRegCnt;
    tSNMP_COUNTER64_TYPE RxValidPduCnt;
    tSNMP_COUNTER64_TYPE TxPduCnt;
    tSNMP_COUNTER64_TYPE RxInValidPduCnt;
    tSNMP_COUNTER64_TYPE RxNewMsgCnt;
    tSNMP_COUNTER64_TYPE TxNewMsgCnt;
    tSNMP_COUNTER64_TYPE RxJoinInMsgCnt;
    tSNMP_COUNTER64_TYPE TxJoinInMsgCnt;
    tSNMP_COUNTER64_TYPE RxJoinMtMsgCnt;
    tSNMP_COUNTER64_TYPE TxJoinMtMsgCnt;
    tSNMP_COUNTER64_TYPE RxLeaveMsgCnt;
    tSNMP_COUNTER64_TYPE TxLeaveMsgCnt;
    tSNMP_COUNTER64_TYPE RxMtMsgCnt;
    tSNMP_COUNTER64_TYPE TxMtMsgCnt;
    tSNMP_COUNTER64_TYPE RxInMsgCnt;
    tSNMP_COUNTER64_TYPE TxInMsgCnt;
    tSNMP_COUNTER64_TYPE RxLeaveAllMsgCnt;
    tSNMP_COUNTER64_TYPE TxLeaveAllMsgCnt;
    tMacAddr            MvrpLastMacAddr;
    tMacAddr            MmrpLastMacAddr;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4IfIndex = 0;
    UINT1               au1IntfName[MRP_MAX_PORT_NAME_LENGTH];
    UINT1               au1String[MRP_CLI_MAX_MAC_STRING_SIZE];

    if ((u4ContextId == 0) || (u4ContextId >= MRP_SIZING_CONTEXT_COUNT))
    {
        return CLI_FAILURE;
    }

    MEMSET (au1String, 0, MRP_CLI_MAX_MAC_STRING_SIZE);

    MEMSET (au1IntfName, 0, MRP_MAX_PORT_NAME_LENGTH);

    u4IfIndex = MRP_GET_PHY_PORT (u4ContextId, u2PortId);

    MrpPortCfaCliGetIfName (u4IfIndex, (INT1 *) au1IntfName);

    /* MVRP Failed Registrations */
    nmhGetFsMrpPortStatsNumberOfRegistrations (u4ContextId, u2PortId,
                                               MacAddr, &NumberOfRegCnt);
    /* Rx Valid Pdu Count */
    nmhGetFsMrpPortStatsRxValidPduCount (u4ContextId, u2PortId,
                                         MacAddr, &RxValidPduCnt);
    /* Rx InValid Pdu Count */
    nmhGetFsMrpPortStatsRxInvalidPduCount (u4ContextId, u2PortId,
                                           MacAddr, &RxInValidPduCnt);
    /* Rx New Msg Count */
    nmhGetFsMrpPortStatsRxNewMsgCount (u4ContextId, u2PortId,
                                       MacAddr, &RxNewMsgCnt);
    /* Rx JoinIn msg  Count */
    nmhGetFsMrpPortStatsRxJoinInMsgCount (u4ContextId, u2PortId,
                                          MacAddr, &RxJoinInMsgCnt);
    /* Rx JoinMt msg  Count */
    nmhGetFsMrpPortStatsRxJoinMtMsgCount (u4ContextId, u2PortId,
                                          MacAddr, &RxJoinMtMsgCnt);
    /* Rx Leave msg Count */
    nmhGetFsMrpPortStatsRxLeaveMsgCount (u4ContextId, u2PortId,
                                         MacAddr, &RxLeaveMsgCnt);
    /* Rx Empty msg  Count */
    nmhGetFsMrpPortStatsRxEmptyMsgCount (u4ContextId, u2PortId,
                                         MacAddr, &RxMtMsgCnt);
    /* Rx In msg Count */
    nmhGetFsMrpPortStatsRxInMsgCount (u4ContextId, u2PortId,
                                      MacAddr, &RxInMsgCnt);
    /* Rx LeaveAll msg Count */
    nmhGetFsMrpPortStatsRxLeaveAllMsgCount (u4ContextId, u2PortId,
                                            MacAddr, &RxLeaveAllMsgCnt);
    /* Tx Pdu Count */
    nmhGetFsMrpPortStatsTxPduCount (u4ContextId, u2PortId, MacAddr, &TxPduCnt);
    /* Tx New Msg Count */
    nmhGetFsMrpPortStatsTxNewMsgCount (u4ContextId, u2PortId,
                                       MacAddr, &TxNewMsgCnt);
    /* Tx JoinIn msg  Count */
    nmhGetFsMrpPortStatsTxJoinInMsgCount (u4ContextId, u2PortId,
                                          MacAddr, &TxJoinInMsgCnt);
    /* Tx JoinMt msg  Count */
    nmhGetFsMrpPortStatsTxJoinMtMsgCount (u4ContextId, u2PortId,
                                          MacAddr, &TxJoinMtMsgCnt);
    /* Tx Leave msg Count */
    nmhGetFsMrpPortStatsTxLeaveMsgCount (u4ContextId, u2PortId,
                                         MacAddr, &TxLeaveMsgCnt);
    /* Tx Empty msg  Count */
    nmhGetFsMrpPortStatsTxEmptyMsgCount (u4ContextId, u2PortId,
                                         MacAddr, &TxMtMsgCnt);
    /* Tx In msg Count */
    nmhGetFsMrpPortStatsTxInMsgCount (u4ContextId, u2PortId,
                                      MacAddr, &TxInMsgCnt);
    /* Tx LeaveAll msg Count */
    nmhGetFsMrpPortStatsTxLeaveAllMsgCount (u4ContextId, u2PortId,
                                            MacAddr, &TxLeaveAllMsgCnt);

    nmhGetIeee8021BridgePortMmrpLastPduOrigin (u4ContextId, u2PortId,
                                               &MmrpLastMacAddr);

    nmhGetFsMvrpPortMvrpLastPduOrigin (u4ContextId, u2PortId, &MvrpLastMacAddr);

    pMrpContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (pMrpContextInfo == NULL)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\nLocal Port Id  : %d  IfName : %s \r\n",
               u2PortId, au1IntfName);

    CliPrintf (CliHandle, "------------------------------------\n");

    if (0 == (MEMCMP (MacAddr, pMrpContextInfo->MvrpAddr, MRP_MAC_ADDR_LEN)))
    {
        PrintMacAddress (MvrpLastMacAddr, au1String);

        CliPrintf (CliHandle, "MRP Application             : MVRP \r\n");

        CliPrintf (CliHandle, "%-28s : %-9s\r\n", "Mvrp last pdu origin",
                   au1String);
    }
    else if (0 == (MEMCMP (MacAddr, gMmrpAddr, MRP_MAC_ADDR_LEN)))

    {
        PrintMacAddress (MmrpLastMacAddr, au1String);

        CliPrintf (CliHandle, "MRP Application             : MMRP \r\n");

        CliPrintf (CliHandle, "%-28s : %-9s\r\n", "Mmrp last pdu origin",
                   au1String);

    }

    CliPrintf (CliHandle, "NumberOfRegistrations        : %d\r\n",
               NumberOfRegCnt.lsn);
    CliPrintf (CliHandle, "Valid Packets Received       : %d\r\n",
               RxValidPduCnt.lsn);
    CliPrintf (CliHandle, "Invalid Packets Received     : %d\r\n",
               RxInValidPduCnt.lsn);
    CliPrintf (CliHandle, "New messages Received        : %d\r\n",
               RxNewMsgCnt.lsn);
    CliPrintf (CliHandle, "JoinIn messages Received     : %d\r\n",
               RxJoinInMsgCnt.lsn);
    CliPrintf (CliHandle, "JoinMt messages Received     : %d\r\n",
               RxJoinMtMsgCnt.lsn);
    CliPrintf (CliHandle, "Leave messages Received      : %d\r\n",
               RxLeaveMsgCnt.lsn);
    CliPrintf (CliHandle, "Empty messages Received      : %d\r\n",
               RxMtMsgCnt.lsn);
    CliPrintf (CliHandle, "In messages Received         : %d\r\n",
               RxInMsgCnt.lsn);
    CliPrintf (CliHandle, "LeaveAll messages Received   : %d\r\n",
               RxLeaveAllMsgCnt.lsn);
    CliPrintf (CliHandle, "Packets Transmitted          : %d\r\n",
               TxPduCnt.lsn);
    CliPrintf (CliHandle, "New messages Transmitted     : %d\r\n",
               TxNewMsgCnt.lsn);
    CliPrintf (CliHandle, "JoinIn messages Transmitted  : %d\r\n",
               TxJoinInMsgCnt.lsn);
    CliPrintf (CliHandle, "JoinMt messages Transmitted  : %d\r\n",
               TxJoinMtMsgCnt.lsn);
    CliPrintf (CliHandle, "Leave messages Transmitted   : %d\r\n",
               TxLeaveMsgCnt.lsn);
    CliPrintf (CliHandle, "Empty messages Transmitted   : %d\r\n",
               TxMtMsgCnt.lsn);
    CliPrintf (CliHandle, "In messages Transmitted      : %d\r\n",
               TxInMsgCnt.lsn);
    u4PagingStatus = (UINT4) CliPrintf (CliHandle,
                                        "LeaveAll messages Transmitted: %d\r\n",
                                        TxLeaveAllMsgCnt.lsn);
    if (u4PagingStatus == CLI_FAILURE)
    {
        /* User pressed 'q' at more prompt,
         * no more print required, exit
         */
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 *     FUNCTION NAME    : MrpCliShowMrpConfig                                
 *                                                                           
 *     DESCRIPTION      : This function displays MRP Context and Port related 
 *                        Configurations.          
 *                                                                           
 *     INPUT            : CliHandle - Handle to the cli context              
 *                        u4ContextId - ContextId                            
 *                        u2Application - Application Type    
 *                        u1DisplayFlag  - Display Option
 *                        u2PortId  - Port Id                                
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                           
 *****************************************************************************/
INT4
MrpCliShowMrpConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                     UINT2 u2Application, UINT1 u1DisplayFlag, UINT2 u2PortId)
{
    CliPrintf (CliHandle, "\r\nMRP Configurations \r\n");
    CliPrintf (CliHandle, "--------------------\r\n");

    switch (u1DisplayFlag)
    {
        case MRP_SHOW_CONTEXT_ONLY:
            MrpCliShowMrpConfigOnContext (CliHandle, u4ContextId,
                                          u2Application);
            break;

        case MRP_SHOW_ALL:
            MrpCliShowAllMrpConfig (CliHandle, u2Application);
            break;

        case MRP_SHOW_PORT_ONLY:
            MrpCliShowMrpConfigOnPort (CliHandle, u4ContextId, u2PortId,
                                       u2Application);
            break;

        default:
            break;
    }
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************
 *     FUNCTION NAME    : MrpCliShowMrpConfigOnPort                                
 *                                                                           
 *     DESCRIPTION      : This function displays MRP Context and the given Port
 *                        related Configurations.          
 *                                                                           
 *     INPUT            : CliHandle - Handle to the cli context              
 *                        u4ContextId - ContextId                            
 *                        u2Application - Application Type    
 *                        u2PortId  - Port Id                                
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None 
 *                                                                           
 *****************************************************************************/
VOID
MrpCliShowMrpConfigOnPort (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT2 u2PortId, UINT2 u2Application)
{
    /* Display the context specific information. */
    MrpCliPrintMrpSwitchConfig (CliHandle, u4ContextId, u2Application);

    /* Display port specific information */
    MrpCliPrintMrpConfig (CliHandle, u4ContextId, u2Application, u2PortId);
}

/*****************************************************************************
 *     FUNCTION NAME    : MrpCliShowMrpConfigOnContext                                
 *                                                                           
 *     DESCRIPTION      : This function displays the MRP configurations for the
 *                        specified context and the ports mapped to it.
 *                                                                           
 *     INPUT            : CliHandle - Handle to the cli context              
 *                        u4ContextId - ContextId                            
 *                        u2Application - Application Type    
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None 
 *                                                                           
 *****************************************************************************/
VOID
MrpCliShowMrpConfigOnContext (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT2 u2Application)
{
    UINT4               u4NextPortId = 0;
    UINT4               u4CurrentPortId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4CurrContextId = u4ContextId;

    /* Display the context specific information. */
    MrpCliPrintMrpSwitchConfig (CliHandle, u4CurrContextId, u2Application);

    while ((nmhGetNextIndexFsMrpPortTable (u4CurrContextId, &u4NextContextId,
                                           u4CurrentPortId, &u4NextPortId)
            == SNMP_SUCCESS) && (u4ContextId == u4NextContextId))
    {
        /* Display information for the ports mapped to this context */
        MrpCliPrintMrpConfig (CliHandle, u4NextContextId, u2Application,
                              (UINT2) u4NextPortId);
        u4CurrentPortId = u4NextPortId;
        u4CurrContextId = u4NextContextId;
    }
}

/*****************************************************************************
 *     FUNCTION NAME    : MrpCliShowAllMrpConfig 
 *                                                                           
 *     DESCRIPTION      : This function displays the MRP configurations for the
 *                        all the Contexts created in the system.      
 *                                                                           
 *     INPUT            : CliHandle - Handle to the cli context              
 *                        u2Application - Application Type    
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None 
 *                                                                           
 *****************************************************************************/
VOID
MrpCliShowAllMrpConfig (tCliHandle CliHandle, UINT2 u2Application)
{
    UINT4               u4NextPortId = 0;
    UINT4               u4CurrentPortId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4CurrContextId = 0;

    nmhGetNextIndexFsMrpInstanceTable (u4CurrContextId, &u4NextContextId);

    do
    {
        u4CurrContextId = u4NextContextId;

        /* Display the context specific information. */
        MrpCliPrintMrpSwitchConfig (CliHandle, u4CurrContextId, u2Application);

        /* Display information for the ports of this context */
        while ((nmhGetNextIndexFsMrpPortTable (u4CurrContextId,
                                               &u4NextContextId,
                                               u4CurrentPortId,
                                               &u4NextPortId) == SNMP_SUCCESS)
               && (u4CurrContextId == u4NextContextId))
        {
            /* Display the information only if the port belongs to the same 
             * context */
            MrpCliPrintMrpConfig (CliHandle, u4NextContextId, u2Application,
                                  (UINT2) u4NextPortId);
            u4CurrentPortId = u4NextPortId;
        }
        u4CurrentPortId = 0;
    }
    while (nmhGetNextIndexFsMrpInstanceTable (u4CurrContextId, &u4NextContextId)
           == SNMP_SUCCESS);
}

/*****************************************************************************
 *     FUNCTION NAME    : MrpCliPrintSwitchConfig
 *
 *     DESCRIPTION      : This function displays switch related mrp config
 *
 *      INPUT           : CliHandle - Cli Context Info
 *                        u4ContextId - Context id
 *                        u2Application - Application Type
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
VOID
MrpCliPrintMrpSwitchConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT2 u2Application)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    INT4                i4MvrpTrapStatus = 0;
    INT4                i4MmrpTrapStatus = 0;
    UINT1               u1MvrpStatus = MRP_DISABLED;
    UINT1               u1MmrpStatus = MRP_DISABLED;
    UINT1               u1IsShowBoth = OSIX_FALSE;

    if ((u4ContextId == 0) || (u4ContextId >= MRP_SIZING_CONTEXT_COUNT))
    {
        return;
    }

    if (u2Application == MRP_INIT_VAL)
    {
        /* If u2Application is 0 means, then display both
           MVRP and MMRP details. */
        u1IsShowBoth = OSIX_TRUE;
    }

    pMrpContextInfo = gMrpGlobalInfo.apContextInfo[u4ContextId];

    if (pMrpContextInfo != NULL)
    {
        if (MrpPortVcmGetSystemModeExt (MRP_PROTOCOL_ID) == VCM_MI_MODE)
        {
            CliPrintf (CliHandle, "\r\n Switch Name : %s \r\n",
                       pMrpContextInfo->au1ContextName);

            CliPrintf (CliHandle, " -----------------------\r\n");
        }

        if (MRP_IS_MRP_STARTED (u4ContextId) != OSIX_TRUE)
        {
            CliPrintf (CliHandle, "\r%% MRP is shutdown.\r\n");
            return;
        }

        u1MvrpStatus = gMrpGlobalInfo.au1MvrpStatus[u4ContextId];
        u1MmrpStatus = gMrpGlobalInfo.au1MmrpStatus[u4ContextId];

        if ((u2Application == CLI_MRP_MVRP) || (u1IsShowBoth == OSIX_TRUE))
        {
            if (u1MvrpStatus == MRP_ENABLED)
            {
                CliPrintf (CliHandle, "\r MVRP feature is currently "
                           "Enabled on the Switch.\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r MVRP feature is currently "
                           "Disabled on the Switch.\r\n");
            }
        }

        if ((u2Application == CLI_MRP_MMRP) || (u1IsShowBoth == OSIX_TRUE))
        {
            if (u1MmrpStatus == MRP_ENABLED)
            {
                CliPrintf (CliHandle, "\r MMRP feature is currently "
                           "Enabled on the Switch.\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r MMRP feature is currently "
                           "Disabled on the Switch.\r\n");
            }
        }

        if ((u2Application == CLI_MRP_MVRP) || (u1IsShowBoth == OSIX_TRUE))
        {
            nmhGetFsMrpInstanceNotifyVlanRegFailure (u4ContextId,
                                                     &i4MvrpTrapStatus);
            if (i4MvrpTrapStatus == MRP_ENABLED)
            {
                CliPrintf (CliHandle, "\r VLAN Registration failure "
                           "notification is Enabled.\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r VLAN Registration failure "
                           "notification is Disabled.\r\n");
            }
        }

        if ((u2Application == CLI_MRP_MMRP) || (u1IsShowBoth == OSIX_TRUE))
        {
            nmhGetFsMrpInstanceNotifyMacRegFailure (u4ContextId,
                                                    &i4MmrpTrapStatus);
            if (i4MmrpTrapStatus == MRP_ENABLED)
            {
                CliPrintf (CliHandle, "\r MAC Address Registration failure "
                           "notification is Enabled.\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r MAC Address Registration failure "
                           "notification is Disabled.\r\n");
            }
        }
    }
    CliPrintf (CliHandle, "\r\n");
}

/*****************************************************************************
 *     FUNCTION NAME    : MrpCliPrintMrpConfig                               
 *                                                                           
 *     DESCRIPTION      : This function prints port mrp configurations.         
 *                                                                           
 *     INPUT            : u4ContextId - ContextId                            
 *                        u4Application - Application Type                   
 *                        u2PortId  - Port Id                                
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            
 *                                                                            
 *****************************************************************************/

INT4
MrpCliPrintMrpConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                      UINT2 u2Application, UINT2 u2PortId)
{
    INT4                i4JoinTimerVal = 0;
    INT4                i4LeaveTimerVal = 0;
    INT4                i4LeaveAllTimerVal = 0;
    INT4                i4PeriodicSemStatus = MRP_DISABLED;
    INT4                i4AdminCtrl = MRP_NORMAL_PARTICIPANT;
    INT4                i4AttributeType = 0;
    INT4                i4ModuleEnabledStatus = MRP_DISABLED;
    INT4                i4ResRegCtrl = MRP_DISABLED;
    INT4                i4PortRegAdminControl = 0;
    INT4                i4ParticipantType = 0;
    UINT4               u4IfIndex = 0;
    UINT1               au1IntfName[MRP_MAX_PORT_NAME_LENGTH];
    UINT1               u1IsShowBoth = OSIX_FALSE;

    if ((u4ContextId == 0) || (u4ContextId >= MRP_SIZING_CONTEXT_COUNT))
    {
        return CLI_FAILURE;
    }

    u4IfIndex = MRP_GET_PHY_PORT (u4ContextId, u2PortId);

    MEMSET (au1IntfName, 0, MRP_MAX_PORT_NAME_LENGTH);

    MrpPortCfaCliGetIfName (u4IfIndex, (INT1 *) au1IntfName);

    if (u2Application == MRP_INIT_VAL)
    {
        /* If u2Application is 0 means, then display both 
           MVRP and MMRP details. */
        u1IsShowBoth = OSIX_TRUE;
    }

    CliPrintf (CliHandle, "\n Local Port Id : %d  IfName : %s \r\n",
               u2PortId, au1IntfName);
    CliPrintf (CliHandle, " ---------------------------------\n");

    if ((u2Application == CLI_MRP_MVRP) || (u1IsShowBoth == OSIX_TRUE))
    {
        nmhGetFsMvrpPortMvrpEnabledStatus (u4ContextId,
                                           (UINT4) u2PortId,
                                           &i4ModuleEnabledStatus);
        if (i4ModuleEnabledStatus == MRP_ENABLED)
        {
            CliPrintf (CliHandle, "\rPort MVRP Status         : Enabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\rPort MVRP Status         : Disabled \r\n");
        }
    }
    if ((u2Application == CLI_MRP_MMRP) || (u1IsShowBoth == OSIX_TRUE))
    {
        nmhGetIeee8021BridgePortMmrpEnabledStatus (u4ContextId, u2PortId,
                                                   &i4ModuleEnabledStatus);
        if (i4ModuleEnabledStatus == MRP_ENABLED)
        {
            CliPrintf (CliHandle, "\rPort MMRP Status         : Enabled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\rPort MMRP Status         : Disabled \r\n");
        }
    }

    if ((u2Application == CLI_MRP_MVRP) || (u1IsShowBoth == OSIX_TRUE))
    {
        nmhGetFsMrpPortRestrictedVlanRegistration (u4ContextId,
                                                   (UINT4) u2PortId,
                                                   &i4ResRegCtrl);
        if (i4ResRegCtrl == MRP_ENABLED)
        {
            CliPrintf (CliHandle, "\rRestricted VLAN Reg Ctrl : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\rRestricted VLAN Reg Ctrl : Disabled \r\n");

        }
    }

    if ((u2Application == CLI_MRP_MMRP) || (u1IsShowBoth == OSIX_TRUE))
    {

        nmhGetIeee8021BridgePortRestrictedGroupRegistration (u4ContextId,
                                                             (UINT4) u2PortId,
                                                             &i4ResRegCtrl);
        if (i4ResRegCtrl == MRP_ENABLED)
        {
            CliPrintf (CliHandle, "\rRestricted MAC Reg Ctrl  : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\rRestricted MAC Reg Ctrl  : Disabled \r\n");

        }
    }

    nmhGetFsMrpPortRegAdminControl (u4ContextId, u2PortId,
                                    &i4PortRegAdminControl);

    if (i4PortRegAdminControl < MRP_REG_INVALID)
    {
        CliPrintf (CliHandle, "\rRegistrar Admin Control  : %s\r\n",
                   gau1RegAdminCtrl[i4PortRegAdminControl]);

    }

    /* Participant Type */
    nmhGetFsMrpPortParticipantType (u4ContextId, u2PortId, &i4ParticipantType);

    if (i4ParticipantType == MRP_FULL_PARTICIPANT)
    {
        CliPrintf (CliHandle,
                   "\rParticipant Type         : fullParticipant \r\n");
    }
    else if (i4ParticipantType == MRP_APPLICANT_ONLY)
    {
        CliPrintf (CliHandle,
                   "\rParticipant Type         : applicantOnly \r\n");
    }

    /* MRP join time */
    nmhGetIeee8021BridgePortMrpJoinTime (u4ContextId, u2PortId,
                                         &i4JoinTimerVal);

    /* MRP leave time */

    nmhGetIeee8021BridgePortMrpLeaveTime (u4ContextId, u2PortId,
                                          &i4LeaveTimerVal);

    /*MRP leave all time */

    nmhGetIeee8021BridgePortMrpLeaveAllTime (u4ContextId, u2PortId,
                                             &i4LeaveAllTimerVal);

    /* Periodic Timer Status */
    nmhGetFsMrpPortPeriodicSEMStatus (u4ContextId, u2PortId,
                                      &i4PeriodicSemStatus);

    CliPrintf (CliHandle, "\rJoin Timer Value         : %d \r\n",
               i4JoinTimerVal);
    CliPrintf (CliHandle, "\rLeave Timer Value        : %d \r\n",
               i4LeaveTimerVal);
    CliPrintf (CliHandle, "\rLeaveAll Timer Value     : %d \r\n",
               i4LeaveAllTimerVal);

    if (i4PeriodicSemStatus != MRP_ENABLED)
    {
        CliPrintf (CliHandle, "\rPeriodic SEM Status      : Disabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\rPeriodic SEM Status      : Enabled\r\n");
    }

    CliPrintf (CliHandle, " \n AttributeType       ApplicantType \r\n");
    CliPrintf (CliHandle, " --------------------------------- \r\n");

    if ((u2Application == CLI_MRP_MVRP) || (u1IsShowBoth == OSIX_TRUE))
    {
        nmhGetFsMrpApplicantControlAdminStatus (u4ContextId, (UINT4) u2PortId,
                                                gMvrpAddr, MRP_VID_ATTR_TYPE,
                                                &i4AdminCtrl);
        if (i4AdminCtrl == MRP_NORMAL_PARTICIPANT)
        {
            CliPrintf (CliHandle, "\r%-22s %-16s\r\n", "VLAN", "Normal");
        }
        else if (i4AdminCtrl == MRP_NON_PARTICIPANT)
        {
            CliPrintf (CliHandle, "\r%-22s %-16s\r\n", "VLAN",
                       "Non-Participant");
        }
        else if (i4AdminCtrl == MRP_ACTIVE_APPLICANT)
        {
            CliPrintf (CliHandle, "\r%-22s %-16s\r\n", "VLAN", "Active");
        }
    }

    if ((u2Application == CLI_MRP_MMRP) || (u1IsShowBoth == OSIX_TRUE))
    {
        for (i4AttributeType = 1; i4AttributeType < MRP_MAX_ATTR_TYPES;
             i4AttributeType++)
        {
            nmhGetFsMrpApplicantControlAdminStatus
                (u4ContextId, (UINT4) u2PortId,
                 gMmrpAddr, i4AttributeType, &i4AdminCtrl);

            if (i4AttributeType == MRP_SERVICE_REQ_ATTR_TYPE)
            {
                if (i4AdminCtrl == CLI_MRP_NORMAL_APPLICANT)
                {
                    CliPrintf (CliHandle, "\r%-22s %-16s\r\n", "SER-REQ",
                               "Normal");
                }
                else
                {
                    CliPrintf (CliHandle, "\r%-22s %-16s\r\n", "SER-REQ",
                               "Non-Participant");
                }
            }
            else
            {
                if (i4AdminCtrl == CLI_MRP_NORMAL_APPLICANT)
                {
                    CliPrintf (CliHandle, "\r%-22s %-16s\r\n", "MAC", "Normal");
                }
                else
                {
                    CliPrintf (CliHandle, "\r%-22s %-16s\r\n", "MAC",
                               "Non-Participant");
                }
            }
        }
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/****************************************************************************
 *     FUNCTION NAME    : MrpCliShowMvrpStateMachines                             
 *                                                                           
 *     DESCRIPTION      : This function displays MVRP State machine info.    
 *                                                                           
 *     INPUT            : CliHandle - Handle to the cli context              
 *                        u4ContextId - ContextId                            
 *                        Vlan Id   - Vlan Id                                
 *                        u1DisplayFlag - Display Flag                       
 *                        u2PortId - Port Id                                 
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                                               
 *                                                                           
 *****************************************************************************/

INT4
MrpCliShowMvrpStateMachines (tCliHandle CliHandle, UINT4 u4ContextId,
                             tVlanId VlanId, UINT1 u1DisplayFlag,
                             UINT2 u2PortId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpAttr            Attr;
    tSNMP_OCTET_STRING_TYPE CurrAttrValue;
    tSNMP_OCTET_STRING_TYPE NextAttrValue;
    tMacAddr            NextMacAddr;
    tMacAddr            CurrMacAddr;
    tMacAddr            PeerMacAddr;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4AppState = 0;
    INT4                i4RegState = 0;
    INT4                i4CurrMapId = 0;
    INT4                i4NextMapId = 0;
    INT4                i4AttrType = 0;
    INT4                i4NextAttrType = 0;
    INT4                i4AdminCtrl = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4NextPortId = 0;
    UINT4               u4CurrentPortId = 0;
    UINT4               u4CurrContextId = u4ContextId;
    UINT4               u4NextContextId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2TempVlanId = 0;
    UINT2               u2GivenVlanId = 0;
    UINT1               u1RegAdminCtrl = 0;
    UINT1               u1IsShowAll = OSIX_FALSE;
    UINT1               u1Flag = OSIX_TRUE;
    UINT1               au1IntfName[MRP_MAX_PORT_NAME_LENGTH];
    UINT1               au1CurrAttrVal[MRP_MAX_ATTR_LEN];
    UINT1               au1NextAttrVal[MRP_MAX_ATTR_LEN];
    UINT1               u1IsPrintHeader = OSIX_TRUE;
    UINT1               au1String[MRP_CLI_MAX_MAC_STRING_SIZE];

    /*For  Getting Next App MAC address */
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    /*For Storing  Current MAC address. */
    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));

    MEMSET (PeerMacAddr, 0, sizeof (tMacAddr));

    /*For Storing Current Attr Value. */
    MEMSET (au1CurrAttrVal, 0, MRP_MAX_ATTR_LEN);

    /*For Storing the Next Attr Value. */
    MEMSET (au1NextAttrVal, 0, MRP_MAX_ATTR_LEN);

    MEMSET (au1String, 0, MRP_CLI_MAX_MAC_STRING_SIZE);

    CurrAttrValue.i4_Length = MRP_MAX_ATTR_LEN;
    CurrAttrValue.pu1_OctetList = au1CurrAttrVal;

    NextAttrValue.i4_Length = MRP_MAX_ATTR_LEN;
    NextAttrValue.pu1_OctetList = au1NextAttrVal;

    u4CurrentPortId = u2PortId;

    if (u4ContextId == MRP_INIT_VAL)
    {
        MrpUtilGetNextActiveContext (u4ContextId, &u4CurrContextId);
    }

    /*Get the Context Info for accessing the MVRP address value. */

    pMrpContextInfo = MRP_CONTEXT_PTR (u4CurrContextId);

    if (VlanId != 0)
    {
        /* If VLAN id is given as a one input. */
        /* For Getting the Give Attribute Information Directly,
         * Decrement the VLAN id value by 1 and pass it as a Current
         * Attr Value..*/

        u2TempVlanId = (UINT2) (VlanId - 1);
        u2TempVlanId = OSIX_HTONS (u2TempVlanId);
        CurrAttrValue.i4_Length = MRP_VLAN_ID_LEN;
        MEMCPY (CurrAttrValue.pu1_OctetList, &u2TempVlanId, MRP_VLAN_ID_LEN);

        /* For Comparing the Next Attr Value ,Here this Variable 
         * is used.*/
        u2GivenVlanId = VlanId;
        u2GivenVlanId = OSIX_HTONS (u2GivenVlanId);

    }

    if (u2PortId != 0)
    {
        /*If Port Id is given as one input.For getting the Given Port 
         * Informations directly, Pass this Port Id as Current Port Id.*/

        u4CurrentPortId = (UINT4) u2PortId;
    }

    /*Getting Firxt Index from SEM Table.Pass the Previous attr values as 0. */

    if (nmhGetNextIndexFsMrpSEMTable (u4CurrContextId, &u4NextContextId,
                                      u4CurrentPortId, &u4NextPortId,
                                      pMrpContextInfo->MvrpAddr,
                                      &NextMacAddr, i4CurrMapId, &i4NextMapId,
                                      i4AttrType, &i4NextAttrType,
                                      &CurrAttrValue,
                                      &NextAttrValue) == SNMP_FAILURE)

    {
        return CLI_SUCCESS;
    }
    do
    {
        u1IsShowAll = OSIX_FALSE;

        pMrpContextInfo = MRP_CONTEXT_PTR (u4NextContextId);

        if (u1IsPrintHeader == OSIX_TRUE)
        {
            /*Print the SEM Tabe Header per context basis. */
            MrpCliPrintStateMachineHeader (CliHandle, u4NextContextId);
        }

        if (VlanId != 0)
        {
            if (MEMCMP (&u2GivenVlanId, NextAttrValue.pu1_OctetList,
                        MRP_VLAN_ID_LEN) != 0)
            {
                /*Set the value of u1Flag as false
                   for Next Attribute Value. */

                u1Flag = OSIX_FALSE;
            }
        }

        MEMSET (au1IntfName, 0, MRP_MAX_PORT_NAME_LENGTH);

        u4IfIndex = MRP_GET_PHY_PORT (u4NextContextId, u4NextPortId);

        MrpPortCfaCliGetIfName (u4IfIndex, (INT1 *) au1IntfName);

        /*Compare the MAC address with MVRP application address.
           If It is Ok means, then print these attribute values. */

        /* u1Flag is used to display specific port or vlan informations.
           Example: If the VLAN id is given as one input,then that vlan
           may be learnt on more than one ports.At that time It should 
           display all the values related to that vlan and ignore other 
           attribute information present in Table. */

        if ((MEMCMP (NextMacAddr, pMrpContextInfo->MvrpAddr,
                     MRP_MAC_ADDR_LEN) == 0) && (u1Flag == OSIX_TRUE))
        {

            nmhGetFsMrpSEMApplicantState (u4NextContextId, u4NextPortId,
                                          NextMacAddr, i4NextMapId,
                                          i4NextAttrType, &NextAttrValue,
                                          &i4AppState);
            nmhGetFsMrpSEMRegistrarState (u4NextContextId, u4NextPortId,
                                          NextMacAddr, i4NextMapId,
                                          i4NextAttrType, &NextAttrValue,
                                          &i4RegState);

            nmhGetFsMrpSEMOriginatorAddress (u4NextContextId, u4NextPortId,
                                             NextMacAddr, i4NextMapId,
                                             i4NextAttrType, &NextAttrValue,
                                             &PeerMacAddr);

            nmhGetFsMrpApplicantControlAdminStatus (u4NextContextId,
                                                    (UINT4) u4NextPortId,
                                                    pMrpContextInfo->MvrpAddr,
                                                    i4NextAttrType,
                                                    &i4AdminCtrl);
            MEMCPY (&u2VlanId, NextAttrValue.pu1_OctetList, MRP_VLAN_ID_LEN);

            u2VlanId = OSIX_NTOHS (u2VlanId);

            MEMSET (&Attr, 0, sizeof (tMrpAttr));

            Attr.u1AttrType = (UINT1) i4NextAttrType;

            Attr.u1AttrLen = MRP_VLAN_ID_LEN;

            MEMCPY (Attr.au1AttrVal, NextAttrValue.pu1_OctetList,
                    MRP_VLAN_ID_LEN);

            PrintMacAddress (PeerMacAddr, au1String);

            pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MVRP_APP_ID);

            pMrpMapEntry = MRP_GET_MAP_ENTRY (pMrpAppEntry, i4NextMapId);

            /* Added to avoid klockwork warning */
            MRP_CHK_NULL_PTR_RET (pMrpMapEntry, CLI_SUCCESS);
            MrpAttrGetRegAdminCtrlFrmAttrVal ((UINT2) u4NextPortId,
                                              &Attr, pMrpMapEntry,
                                              &u1RegAdminCtrl);
            if (u1RegAdminCtrl < MRP_REG_INVALID)
            {
                CliPrintf (CliHandle, "%-6d %-7s %-10s %-7d %-9d %-2s&%-10s "
                           "%-4s&%-7s %-6s\n",
                           u4NextPortId, au1IntfName, "VID", i4NextMapId,
                           u2VlanId, gau1AppSEMState[i4AppState],
                           gau1AppAdminStatus[i4AdminCtrl],
                           gau1RegSEMState[i4RegState],
                           gau1RegAdminCtrl[u1RegAdminCtrl], au1String);
            }
        }
        /*Assignin Next Attribute values to Current Attribute for 
           Getting Next Attribute Information.. */
        u4CurrContextId = u4NextContextId;

        u4CurrentPortId = u4NextPortId;

        MEMCPY (CurrMacAddr, NextMacAddr, MRP_MAC_ADDR_LEN);

        i4CurrMapId = i4NextMapId;

        i4AttrType = i4NextAttrType;

        CurrAttrValue.i4_Length = NextAttrValue.i4_Length;

        MEMCPY (CurrAttrValue.pu1_OctetList, NextAttrValue.pu1_OctetList,
                NextAttrValue.i4_Length);

        MEMSET (NextAttrValue.pu1_OctetList, 0, MRP_MAX_ATTR_LEN);

        NextAttrValue.i4_Length = MRP_MAX_ATTR_LEN;

        i4RetVal = nmhGetNextIndexFsMrpSEMTable (u4CurrContextId,
                                                 &u4NextContextId,
                                                 u4CurrentPortId, &u4NextPortId,
                                                 CurrMacAddr, &NextMacAddr,
                                                 i4CurrMapId, &i4NextMapId,
                                                 i4AttrType, &i4NextAttrType,
                                                 &CurrAttrValue,
                                                 &NextAttrValue);

        if (i4RetVal != SNMP_FAILURE)
        {
            u1IsShowAll = OSIX_TRUE;
            u1Flag = OSIX_TRUE;
            if (u4CurrContextId == u4NextContextId)
            {
                u1IsPrintHeader = OSIX_FALSE;
            }
            else
            {
                u1IsPrintHeader = OSIX_TRUE;
            }
        }

        if (u2PortId != 0)
        {
            if (u2PortId != u4NextPortId)
            {
                /* Set the value of u1Flag as false for Next Port Id. */

                u1Flag = OSIX_FALSE;
                u1IsShowAll = OSIX_FALSE;
            }
        }

        if ((u1DisplayFlag == MRP_SHOW_CONTEXT_ONLY) &&
            (u4CurrContextId != u4NextContextId))
        {
            /* Set the value of u1IsShowAll flag as false for Next Context Id.
             */
            u1IsShowAll = FALSE;
        }
    }
    while (u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************
 *     FUNCTION NAME    : MrpCliPrintStateMachineHeader                          
 *                                                                            
 *     DESCRIPTION      : This function Prints State machine Table Headre 
 *                        info.   
 *     INPUT            : CliHandle - Cli Context 
 *                        u4ContextId - Context Id                           
 *                             
 *     OUTPUT           : None                                              
 *                                                                          
 *     RETURNS          : None                                              
 *                                                                          
 *****************************************************************************/
VOID
MrpCliPrintStateMachineHeader (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (pMrpContextInfo != NULL)
    {
        CliPrintf (CliHandle, "\r\nSwitch Name : %s\r\n",
                   pMrpContextInfo->au1ContextName);
        CliPrintf (CliHandle,
                   "\r Port  IfName AttrType  MapId    AttrVal       AppState&Mgmt  RegState&Mgmt  PeerMacAddr\r\n");
        CliPrintf (CliHandle,
                   "\r ---------------------------------------------------------------------------------------\r\n");
    }
}

/*****************************************************************************
 *     FUNCTION NAME    : MrpCliShowMmrpStateMachines                             
 *                                                                           
 *     DESCRIPTION      : This function displays MVRP State machine info.    
 *                                                                           
 *     INPUT            : CliHandle - Handle to the cli context              
 *                        u4ContextId - ContextId                            
 *                        Vlan Id   - Vlan Id                                
 *                        pAttrVal  - MAC Attribute value                   
 *                        u1SerReqVal - SEREQ value                          
 *                        u1DisplayFlag - Display Flag                       
 *                        u2PortId - Port Id                                 
 *                                                                           
 *     OUTPUT           : None                                               
 *                                                                           
 *     RETURNS          : None                                               
 *                                                                           
 ****************************************************************************/

INT4
MrpCliShowMmrpStateMachines (tCliHandle CliHandle, UINT4 u4ContextId,
                             tVlanId VlanId, UINT1 *pAttrVal, UINT1 u1SerReqVal,
                             UINT1 u1DisplayFlag, UINT2 u2PortId)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpAttr            Attr;
    tSNMP_OCTET_STRING_TYPE CurrAttrValue;
    tSNMP_OCTET_STRING_TYPE NextAttrValue;
    tMacAddr            NextMacAddr;
    tMacAddr            CurrMacAddr;
    tMacAddr            MacAddr;
    tMacAddr            ZeroMacAddr;
    tMacAddr            PeerMacAddr;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4AppState = 0;
    INT4                i4RegState = 0;
    INT4                i4CurrMapId = 0;
    INT4                i4NextMapId = 0;
    INT4                i4AttrType = 0;
    INT4                i4NextAttrType = 0;
    INT4                i4AdminCtrl = 0;
    UINT4               u4NextPortId = 0;
    UINT4               u4CurrentPortId = 0;
    UINT4               u4CurrContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1RegAdminCtrl = 0;
    UINT1               u1IsShowAll = OSIX_FALSE;
    UINT1               u1Flag = OSIX_TRUE;
    UINT1               au1CurrAttrVal[MRP_MAX_ATTR_LEN];
    UINT1               au1IntfName[MRP_MAX_PORT_NAME_LENGTH];
    UINT1               au1NextAttrVal[MRP_MAX_ATTR_LEN];
    UINT1               au1AttrVal[MRP_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1String[MRP_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1IsPrintHeader = OSIX_TRUE;

    /*For Getting Next Mac Address */
    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    /*For Getting Current MAC address. */
    MEMSET (CurrMacAddr, 0, sizeof (tMacAddr));

    MEMSET (PeerMacAddr, 0, sizeof (tMacAddr));

    MEMSET (au1String, 0, MRP_CLI_MAX_MAC_STRING_SIZE);

    /*For Getting Current Attr Value. */
    MEMSET (au1CurrAttrVal, 0, MRP_MAC_ADDR_LEN);

    /*For Getting Next Attr Value. */
    MEMSET (au1NextAttrVal, 0, MRP_MAC_ADDR_LEN);

    MEMSET (au1AttrVal, 0, MRP_CLI_MAX_MAC_STRING_SIZE);
    /*For Checking the Mac is a Valid MAC or not. */
    MEMSET (ZeroMacAddr, 0, sizeof (tMacAddr));

    CurrAttrValue.i4_Length = MRP_MAX_ATTR_LEN;
    CurrAttrValue.pu1_OctetList = au1CurrAttrVal;

    NextAttrValue.i4_Length = MRP_MAX_ATTR_LEN;
    NextAttrValue.pu1_OctetList = au1NextAttrVal;

    u4CurrentPortId = u2PortId;

    if (pAttrVal != NULL)
    {
        /*If the MAC value is given as one input. */
        if (MEMCMP (pAttrVal, ZeroMacAddr, MRP_MAC_ADDR_LEN) != 0)
        {
            CurrAttrValue.i4_Length = MRP_MAC_ADDR_LEN;
            MEMCPY (CurrAttrValue.pu1_OctetList, pAttrVal, MRP_MAC_ADDR_LEN);

            /*For getting the Given Attr Value,Pass the Current Attribute value 
             * as like that,If the Given Attr Value is 01:02:02:02:02:02, then 
             * pass the current attr value as (01:02:02:02:02:02 - 1)  
             * for directly get the Given Attr Value as Next Attr Value.*/

            if (CurrAttrValue.pu1_OctetList[5] != 0)
            {
                CurrAttrValue.pu1_OctetList[5] =
                    (UINT1) (CurrAttrValue.pu1_OctetList[5] - 1);
            }
            /* If last byte of the Attr Value is Zero means,
             * Do memset as 0 and pass it as a Current Attr Value.*/
            else
            {
                MEMSET (CurrAttrValue.pu1_OctetList, 0, MRP_MAC_ADDR_LEN);
            }
            i4AttrType = MRP_MAC_ADDR_ATTR_TYPE;
        }
    }
    else if ((u1SerReqVal == MRP_CLI_MMRP_ALL_GROUPS) ||
             (u1SerReqVal == MRP_CLI_MMRP_UNREG_GROUPS) ||
             (u1SerReqVal == MRP_CLI_MMRP_GROUPS))
    {
        /*If Service Requirement Value is given as one input. */

        if (u1SerReqVal == MRP_CLI_MMRP_UNREG_GROUPS)
        {
            CurrAttrValue.pu1_OctetList[0] = (UINT1) (u1SerReqVal - 1);
            i4AttrType = MRP_SERVICE_REQ_ATTR_TYPE;
        }
        else
        {
            CurrAttrValue.pu1_OctetList[0] = u1SerReqVal;
            i4AttrType = MRP_SERVICE_REQ_ATTR_TYPE - 1;
        }
        CurrAttrValue.i4_Length = MRP_SERVICE_REQ_LEN;
    }

    if (u2PortId != 0)
    {
        /* if Port Id is given as one input */
        /* For getting the Given Port Informations ,
         * Pass the Port Id as Current Port Id.No need to decrement 
         * the Port Id here.Because Here the Curr Attribute Value is 
         * Zero,If the Port Id is given.It will take care and returns
         * Correct Port Informations.*/

        u4CurrentPortId = (UINT4) u2PortId;
    }
    if (VlanId != 0)

    {
        /* If the Vlan Id is given , then take that value as 
         * MAP Context Id value. */
        i4CurrMapId = VlanId;
    }

    /*Getting the First Index.Pass Perious Index value as 0. */

    if (nmhGetNextIndexFsMrpSEMTable (u4ContextId, &u4NextContextId,
                                      u4CurrentPortId, &u4NextPortId, gMmrpAddr,
                                      &NextMacAddr, i4CurrMapId, &i4NextMapId,
                                      i4AttrType, &i4NextAttrType,
                                      &CurrAttrValue,
                                      &NextAttrValue) == SNMP_FAILURE)

    {
        return CLI_SUCCESS;
    }

    do
    {
        u1IsShowAll = OSIX_FALSE;

        if (VlanId != 0)
        {
            if (VlanId != i4NextMapId)
            {
                /*Set the value of u1Flag flag as false for Next Port Id. */
                u1Flag = OSIX_FALSE;
            }
        }

        /*For MAC Address */
        if (pAttrVal != NULL)
        {
            if (MEMCMP (pAttrVal, NextAttrValue.pu1_OctetList,
                        MRP_MAC_ADDR_LEN) != 0)
            {
                /*Set the value of u1Flag flag as false for Next Attribute 
                 * Value. */
                u1Flag = OSIX_FALSE;
            }
        }
        /*For Service Req Values */
        else if ((u1SerReqVal == MRP_CLI_MMRP_ALL_GROUPS) ||
                 (u1SerReqVal == MRP_CLI_MMRP_UNREG_GROUPS))
        {
            if ((u1SerReqVal != NextAttrValue.pu1_OctetList[0]) ||
                (i4NextAttrType == MRP_MAC_ADDR_ATTR_TYPE))
            {
                /* Set the value of u1Flag flag as false for Next Attribute 
                 * Value. */
                u1Flag = OSIX_FALSE;
            }
        }
        else if ((u1SerReqVal == MRP_CLI_MMRP_GROUPS) &&
                 (i4NextAttrType == MRP_MAC_ADDR_ATTR_TYPE))
        {
            /* Break the loop if the Next Attribute is MAC Address. */
            break;
        }

        if (u1IsPrintHeader == OSIX_TRUE)
        {
            /*If the Current Context Id and Next Context Id are not same , 
               then print State machine Table Header Informations. */

            MrpCliPrintStateMachineHeader (CliHandle, u4NextContextId);
        }
        /*Compare the MAC address with MMRP application address.
           If It is Ok means, then print these attribute values. */

        /* u1Flag is used to display particular specific informations.
           Example: When the  MAC address is given as one input,That mac 
           address may be learnt more than one ports.At that time I have 
           to diaplay all the values related to that MAC and ignore other 
           attribute information present in Table. */

        MEMSET (au1IntfName, 0, MRP_MAX_PORT_NAME_LENGTH);

        u4IfIndex = MRP_GET_PHY_PORT (u4NextContextId, u4NextPortId);

        MrpPortCfaCliGetIfName (u4IfIndex, (INT1 *) au1IntfName);

        if ((MEMCMP (NextMacAddr, gMmrpAddr,
                     MRP_MAC_ADDR_LEN) == 0) && (u1Flag == OSIX_TRUE))
        {

            nmhGetFsMrpSEMApplicantState (u4NextContextId, u4NextPortId,
                                          NextMacAddr, i4NextMapId,
                                          i4NextAttrType, &NextAttrValue,
                                          &i4AppState);
            nmhGetFsMrpSEMRegistrarState (u4NextContextId, u4NextPortId,
                                          NextMacAddr, i4NextMapId,
                                          i4NextAttrType, &NextAttrValue,
                                          &i4RegState);

            nmhGetFsMrpSEMOriginatorAddress (u4NextContextId, u4NextPortId,
                                             NextMacAddr, i4NextMapId,
                                             i4NextAttrType, &NextAttrValue,
                                             &PeerMacAddr);

            nmhGetFsMrpApplicantControlAdminStatus (u4NextContextId,
                                                    u4NextPortId,
                                                    gMmrpAddr, i4NextAttrType,
                                                    &i4AdminCtrl);

            MEMCPY (au1NextAttrVal, NextAttrValue.pu1_OctetList,
                    NextAttrValue.i4_Length);

            PrintMacAddress (PeerMacAddr, au1String);

            MEMSET (&Attr, 0, sizeof (tMrpAttr));

            Attr.u1AttrType = (UINT1) i4NextAttrType;

            if (i4NextAttrType == MRP_SERVICE_REQ_ATTR_TYPE)
            {
                Attr.u1AttrLen = MRP_SERVICE_REQ_LEN;
            }

            else
            {
                Attr.u1AttrLen = MRP_MAC_ADDR_LEN;
            }
            MEMCPY (Attr.au1AttrVal, au1NextAttrVal, Attr.u1AttrLen);

            pMrpContextInfo = MRP_CONTEXT_PTR (u4NextContextId);

            pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, MRP_MMRP_APP_ID);

            pMrpMapEntry =
                MRP_GET_MAP_ENTRY (pMrpAppEntry, (UINT2) i4NextMapId);
            MRP_CHK_NULL_PTR_RET (pMrpMapEntry, CLI_SUCCESS);
            MrpAttrGetRegAdminCtrlFrmAttrVal ((UINT2) u4NextPortId,
                                              &Attr, pMrpMapEntry,
                                              &u1RegAdminCtrl);

            if (u1RegAdminCtrl < MRP_REG_INVALID)
            {
                /*This is for Service Requirement Type */
                if (i4NextAttrType == MRP_SERVICE_REQ_ATTR_TYPE)
                {
                    CliPrintf (CliHandle, "%-6d %-7s %-10s %-10d %-16d "
                               "%-2s&%-10s %-4s&%-7s %-6s\n",
                               u4NextPortId, au1IntfName, "SER-REQ",
                               i4NextMapId, au1NextAttrVal[0],
                               gau1AppSEMState[i4AppState],
                               gau1AppAdminStatus[i4AdminCtrl],
                               gau1RegSEMState[i4RegState],
                               gau1RegAdminCtrl[u1RegAdminCtrl], au1String);

                }
                /* This is for MAC Attribute Type. */
                else
                {
                    MEMSET (MacAddr, 0, sizeof (tMacAddr));
                    MEMCPY (MacAddr, NextAttrValue.pu1_OctetList,
                            NextAttrValue.i4_Length);
                    PrintMacAddress (MacAddr, au1AttrVal);
                    CliPrintf (CliHandle,
                               "%-6d %-7s %-10s %-10d %-15s %-2s&%-10s "
                               "%-4s&%-7s %-6s\n", u4NextPortId, au1IntfName,
                               "MAC", i4NextMapId, au1AttrVal,
                               gau1AppSEMState[i4AppState],
                               gau1AppAdminStatus[i4AdminCtrl],
                               gau1RegSEMState[i4RegState],
                               gau1RegAdminCtrl[u1RegAdminCtrl], au1String);
                }

            }
        }
        /*Assign the Current Attribute values to Previous Attribute 
           Value for getting the Next Index. */

        u4CurrContextId = u4NextContextId;

        u4CurrentPortId = u4NextPortId;

        MEMCPY (CurrMacAddr, NextMacAddr, MRP_MAC_ADDR_LEN);

        i4CurrMapId = i4NextMapId;

        i4AttrType = i4NextAttrType;

        CurrAttrValue.i4_Length = NextAttrValue.i4_Length;

        MEMCPY (CurrAttrValue.pu1_OctetList, NextAttrValue.pu1_OctetList,
                NextAttrValue.i4_Length);
        MEMSET (NextAttrValue.pu1_OctetList, 0, MRP_MAX_ATTR_LEN);
        NextAttrValue.i4_Length = MRP_MAX_ATTR_LEN;

        i4RetVal = nmhGetNextIndexFsMrpSEMTable (u4CurrContextId,
                                                 &u4NextContextId,
                                                 u4CurrentPortId, &u4NextPortId,
                                                 CurrMacAddr, &NextMacAddr,
                                                 i4CurrMapId, &i4NextMapId,
                                                 i4AttrType, &i4NextAttrType,
                                                 &CurrAttrValue,
                                                 &NextAttrValue);

        if (i4RetVal != SNMP_FAILURE)
        {
            /* If the Gext Next returns some attribute information means,
             * then set the flag values as true. */
            u1IsShowAll = OSIX_TRUE;
            u1Flag = OSIX_TRUE;

            if (u4CurrContextId == u4NextContextId)
            {
                u1IsPrintHeader = OSIX_FALSE;
            }
            else
            {
                u1IsPrintHeader = OSIX_TRUE;
            }
        }

        if (u2PortId != 0)
        {
            if (u2PortId != u4NextPortId)
            {
                /* Set the value of u1Flag as false for Next Port Id. */
                u1Flag = OSIX_FALSE;
                u1IsShowAll = OSIX_FALSE;
            }
        }

        if ((u1DisplayFlag == MRP_SHOW_CONTEXT_ONLY) &&
            (u4CurrContextId != u4NextContextId))
        {
            /* Set the value of u1IsShowAll flag as false
             * for Next Context Id. */
            u1IsShowAll = FALSE;
        }

    }
    while (u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *    Function Name       : MrpCliCliSelectContextOnMode                     
 *                                                                           
 *    Description         : This function is used to check the Mode of       
 *                          the command and also if it a Config Mode         
 *                          command it will do SelectContext for the         
 *                          Context.                                         
 *                                                                           
 *    Input(s)            : u4Cmd - CLI Command.                             
 *                                                                           
 *    Output(s)           : CliHandle - Contains error messages.             
 *                          pu4ContextId - Context Id.                       
 *                          pu2LocalPort - Local Port Number.                
 *                          pu2Flag      - Show command or Not.              
 *                                                                           
 *                                                                           
 *    Returns            : OSIX_SUCCESS/OSIX_FAILURE                           
 *                                                                           
 *                                                                           
 ****************************************************************************/

INT4
MrpCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                           UINT4 *pu4Context, UINT2 *pu2LocalPort)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    INT4                i4PortId = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1IntfCmdFlag = OSIX_FALSE;

    /* This above flag is used in MI case, to know whether the command is 
       an a interface mode command or mrp mode command (for future 
       reference). */

    /* For debug commands the context-name will be present in apu4args[0], so 
     * the select context will be done seperately within the 
     * switch statement*/

    if ((u4Cmd == CLI_MRP_GBL_DEBUG) || (u4Cmd == CLI_MRP_NO_GBL_DEBUG) ||
        (u4Cmd == CLI_MRP_DEBUG))
    {
        return OSIX_SUCCESS;
    }

    *pu4Context = MRP_DEFAULT_CONTEXT_ID;

    /* Get the Context-Id from the pCliContext structure */
    u4ContextId = CLI_GET_CXT_ID ();

    /* Check if the command is a switch mode command */
    if (u4ContextId != MRP_CLI_INVALID_CONTEXT)
    {

        /* Switch-mode Command */
        MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
        *pu4Context = u4ContextId;
    }
    else
    {
        /* This flag (u1IntfCmdFlag) is used only in case of MI.
         * If the command is a mrp mode command (future reference)then the 
         * context-id won't be MRP_CLI_INVALID_CONTEXT, So this is an 
         * interface mode command. Now by refering this flag we have to 
         * get the context-id and local port number from the 
         * IfIndex (CLI_GET_IFINDEX). */

        u1IntfCmdFlag = OSIX_TRUE;
    }

    i4PortId = CLI_GET_IFINDEX ();

    /* In SI both the i4PortId and Localport are same. So no need to call
     *MrpPortGetContextInfoFromIfIndex. */

    *pu2LocalPort = (UINT2) i4PortId;

    if (MrpPortVcmGetSystemMode (MRP_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (u1IntfCmdFlag == OSIX_TRUE)
        {
            /* This is an Interface Mode command
             * Get the context Id from the IfIndex */
            if (i4PortId != -1)
            {
                if (MrpPortGetContextInfoFromIfIndex
                    ((UINT4) i4PortId, pu4Context,
                     pu2LocalPort) != OSIX_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    return OSIX_FAILURE;
                }
                if ((*pu4Context < MRP_SIZING_CONTEXT_COUNT) &&
                    (MRP_IS_MRP_STARTED (*pu4Context) == OSIX_FALSE))
                {
                    CliPrintf (CliHandle, "\r%% MRP Shutdown.\r\n");
                    return OSIX_FAILURE;
                }
            }
        }
    }

    pMrpContextInfo = MrpCtxtGetContextInfo (*pu4Context);

    if (pMrpContextInfo == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Virtual Switch.\r\n ");
        return OSIX_FAILURE;
    }

    if ((MrpPortVlanGetStartedStatus (*pu4Context) == VLAN_FALSE) &&
        (u4Cmd != CLI_MRP_SHUT_MRP))
    {
        CliPrintf (CliHandle, "\r%% VLAN switching is shutdown\r\n");
        return OSIX_FAILURE;
    }

    if (MrpPortGarpGetStartedStatus (*pu4Context) == OSIX_TRUE)
    {
        CliPrintf (CliHandle, "\r%% GARP is Running.\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                          
 *    Function Name       : MrpCliGetContextForShowCmd                      
 *                                                                          
 *    Description         : This function is called only when the show      
 *                          command is given. It handles 3 different        
 *                          types of show commands.                          
 *                            1. If switch name is given, we have to get    
 *                               the context Id from it.                    
 *                            2. If Interface Index is present, from that   
 *                               we have to get the context Id.             
 *                            3. Else we have to go for each and every      
 *                               context.                                   
 *                          For the above all thing we have to check for    
 *                          the Module status of the context.               
 *                                                                          
 *    Input(s)            : u4CurrContext    - Context-Id.                  
 *                          pu1Name          - Switch-Name.                 
 *                          u4IfIndex        - Interface Index.             
 *                                                                          
 *    Output(s)           : CliHandle        - Contains error messages.     
 *                          pu4NextContextId - Context Id.                  
 *                          pu2LocalPort     - Local Port Number.           
 *                                                                          
 *                                                                          
 *    Returns            : OSIX_SUCCESS/OSIX_FAILURE                         
 *                                                                          
 *                                                                          
 ****************************************************************************/
INT4
MrpCliGetContextForShowCmd (tCliHandle CliHandle, UINT1 *pu1Name,
                            UINT4 u4IfIndex, UINT4 u4CurrContext,
                            UINT4 *pu4NextContext, UINT2 *pu2LocalPort)
{
    UINT4               u4ContextId = 0;

    *pu2LocalPort = 0;

    /* If Switch-name is given then get the Context-Id from it */

    if (pu1Name != NULL)
    {
        if (MrpPortVcmIsSwitchExist (pu1Name, &u4ContextId) != OSIX_TRUE)
        {
            CliPrintf (CliHandle, "Switch %s Does not exist.\r\n", pu1Name);
            return OSIX_FAILURE;
        }
    }
    /* If IfIndex is given then get the Context-Id from it */
    else if (u4IfIndex != 0)
    {
        if (MrpPortGetContextInfoFromIfIndex ((UINT2) u4IfIndex, &u4ContextId,
                                              pu2LocalPort) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Interface not mapped to "
                       "any of the context.\r\n ");
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* Case 1: At first entry for this funtion, If Switch-name is not given, 
           then get the first active Context. For this if we give 
           0xffffffff as current context for MrpCliGetNextActiveContext 
           function it will return the First Active context. 
           Case 2: For getting  the Given Context Id directly. */

        if (MrpCliGetNextActiveContext (CliHandle, u4CurrContext, &u4ContextId)
            != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    do
    {
        u4CurrContext = u4ContextId;
        if (MrpPortVlanGetStartedStatus (u4CurrContext) == VLAN_FALSE)
        {
            CliPrintf (CliHandle, "\r\n %% VLAN switching is shutdown\r\n");
            if ((pu1Name != NULL) || (u4IfIndex != 0))
            {
                /* We have to come out of the loop when the switch is 
                 * shutdown, if the command is for specific switch else
                 * continue the Loop. */
                break;
            }
            continue;
        }
        *pu4NextContext = u4ContextId;
        return OSIX_SUCCESS;
    }
    while (MrpUtilGetNextActiveContext (u4CurrContext, &u4ContextId)
           == OSIX_SUCCESS);

    return OSIX_FAILURE;
}

/*************************************************************************
*                                                                          
*    FUNCTION NAME       : MrpCliGetNextActiveContext                       
*                                                                          
*    DESCRIPTION         : This function is used to get the next Active     
*                          context present in the system. Also, in case
*                          MRP is shutdown in a context, it will display
*                          the shutdown status
*                                                                           
*    INPUT(s)            : CliHandle       - Contains error messages.     
*                          u4CurrContextId - Current Context Id.            
*                                                                          
*    OUTPUT(s)           : pu4NextContextId - Next Context Id.              
*                                                                          
*    RETURNS             : OSIX_SUCCESS/OSIX_FAILURE                        
* *************************************************************************/
INT4
MrpCliGetNextActiveContext (tCliHandle CliHandle, UINT4 u4CurrContextId,
                            UINT4 *pu4NextContextId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4ContextId = u4CurrContextId + 1;

    for (; u4ContextId < MRP_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

        if (pMrpContextInfo != NULL)
        {
            if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_TRUE)
            {
                *pu4NextContextId = u4ContextId;
                return OSIX_SUCCESS;
            }
            else
            {
                MrpCliDisplayShutStatus (CliHandle, u4ContextId);
            }
        }
    }
    return OSIX_FAILURE;
}

/*************************************************************************
*                                                                          
*    FUNCTION NAME       : MrpCliChkAndDisplayCtxtStatus                    
*                                                                          
*    DESCRIPTION         : This function verifies whether the set of        
*                          contexts in between the range specified by   
*                          u4CurrContextId - u4NextContextId are present in
*                          MRP and will display if the context has MRP shutdown
*                                                                           
*    INPUT(s)            : CliHandle       - Contains error messages.     
*                          u4CurrContextId - Current Context Id.            
*                          u4NextContextId - Next Context Id.
*                                                                          
*    OUTPUT(s)           : NONE.                                            
*                                                                          
*    RETURNS             : NONE.                                            
* *************************************************************************/
VOID
MrpCliChkAndDisplayCtxtStatus (tCliHandle CliHandle, UINT4 u4CurrContextId,
                               UINT4 u4NextContextId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4ContextId = u4CurrContextId + 1;

    for (; u4ContextId < u4NextContextId; u4ContextId++)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

        if (pMrpContextInfo != NULL)
        {
            if (MRP_IS_MRP_STARTED (u4ContextId) != OSIX_TRUE)
            {
                MrpCliDisplayShutStatus (CliHandle, u4ContextId);
            }
        }
    }
}

/*************************************************************************
*                                                                          
*    FUNCTION NAME       : MrpCliDisplayShutStatus                          
*                                                                          
*    DESCRIPTION         : This function displays the MRP Status for the    
*                          specified context as shut down.              
*                                                                           
*    INPUT(s)            : CliHandle       - Contains error messages.     
*                          u4ContextId     - Context Id.                       
*                                                                          
*    OUTPUT(s)           : NONE.                                            
*                                                                          
*    RETURNS             : NONE.                                            
* *************************************************************************/
VOID
MrpCliDisplayShutStatus (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (pMrpContextInfo != NULL)
    {
        CliPrintf (CliHandle, "\r Switch Name : %s \n",
                   pMrpContextInfo->au1ContextName);
        CliPrintf (CliHandle, " -----------------------\n");
        CliPrintf (CliHandle, "\r%% MRP is shutdown.\r\n");
    }
}

/*************************************************************************
*                                                                          
*    FUNCTION NAME       : MrpCliSetRegisterAdminCtrl                          
*                                                                          
*    DESCRIPTION         : This function configures the port registrar 
*                          admin control.
*                                                                           
*    INPUT(s)            : CliHandle       - Contains error messages      
*                          u4ContextId     - Context Identification
*                          u1AdminRegCtrl  - Registrar admin control            
*                          u2LocalPortId   - Local Port Number                  
*    OUTPUT(s)           : NONE.                                            
*                                                                          
*    RETURNS             : NONE.                                            
* *************************************************************************/
INT4
MrpCliSetRegisterAdminCtrl (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT1 u1AdminRegCtrl, UINT2 u2LocalPortId)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsMrpPortRegAdminControl (&u4ErrorCode, u4ContextId,
                                           u2LocalPortId, u1AdminRegCtrl)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMrpPortRegAdminControl (u4ContextId,
                                        u2LocalPortId, u1AdminRegCtrl)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*************************************************************************
*                                                                          
*    FUNCTION NAME       : MrpCliClearConfiguration                            
*                                                                          
*    DESCRIPTION         : This function clears all the configuration  
*                          present in the context for the application.
*                                                                           
*    INPUT(s)            : u4ContextId     - Context Indentifier          
*                          u4Application   - CLI_MRP_MVRP/CLI_MRP_MMRP          
*    OUTPUT(s)           : NONE.                                            
*                                                                          
*    RETURNS             : NONE.                                            
* *************************************************************************/
VOID
MrpCliClearConfiguration (UINT4 u4ContextId, UINT4 u4Application)
{
    UINT4               u4NextContextId = 0;
    UINT4               u4PortId = 0;
    UINT4               u4NextPortId = 0;

    if (u4Application == CLI_MRP_MVRP)
    {
        nmhSetFsMrpInstanceBridgeMvrpEnabledStatus (u4ContextId, MRP_DISABLED);
        nmhSetFsMrpInstanceNotifyVlanRegFailure (u4ContextId, MRP_DISABLED);
    }
    else
    {
        nmhSetFsMrpInstanceBridgeMmrpEnabledStatus (u4ContextId, MRP_DISABLED);
        nmhSetFsMrpInstanceNotifyMacRegFailure (u4ContextId, MRP_DISABLED);
    }

    if (nmhGetNextIndexFsMrpPortTable (u4ContextId, &u4NextContextId,
                                       u4PortId, &u4NextPortId) == SNMP_FAILURE)
    {
        return;
    }

    do
    {
        if (u4ContextId != u4NextContextId)
        {
            return;
        }

        MrpCliClearConfigurationOnPort (u4NextContextId, u4NextPortId,
                                        u4Application);

        u4ContextId = u4NextContextId;
        u4PortId = u4NextPortId;
    }
    while (nmhGetNextIndexFsMrpPortTable (u4ContextId, &u4NextContextId,
                                          u4PortId, &u4NextPortId)
           == SNMP_SUCCESS);
}

/*************************************************************************
*                                                                          
*    FUNCTION NAME       : MrpCliClearConfigurationOnPort                      
*                                                                          
*    DESCRIPTION         : This function clears all the configuration  
*                          present in the port for the application.
*                                                                           
*    INPUT(s)            : u4ContextId     - Context Indentifier          
*                          u4Port          - Local Port Number                  
*                          u4Application   - CLI_MRP_MVRP/CLI_MRP_MMRP          
*    OUTPUT(s)           : NONE.                                            
*                                                                          
*    RETURNS             : NONE.                                            
* *************************************************************************/
VOID
MrpCliClearConfigurationOnPort (UINT4 u4ContextId, UINT4 u4Port,
                                UINT4 u4Application)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    if (pMrpContextInfo == NULL)
    {
        return;
    }

    if (u4Application == CLI_MRP_MVRP)
    {
        nmhSetFsMvrpPortMvrpEnabledStatus (u4ContextId, u4Port, MRP_ENABLED);
        nmhSetFsMrpPortRestrictedVlanRegistration (u4ContextId,
                                                   u4Port, MRP_DISABLED);
        nmhSetFsMrpApplicantControlAdminStatus (u4ContextId, u4Port,
                                                pMrpContextInfo->MvrpAddr,
                                                MRP_VID_ATTR_TYPE,
                                                MRP_NORMAL_PARTICIPANT);
    }
    else
    {
        nmhSetIeee8021BridgePortMmrpEnabledStatus (u4ContextId, u4Port,
                                                   MRP_ENABLED);
        nmhSetIeee8021BridgePortRestrictedGroupRegistration (u4ContextId,
                                                             u4Port,
                                                             MRP_DISABLED);
        nmhSetFsMrpApplicantControlAdminStatus (u4ContextId, u4Port,
                                                gMmrpAddr,
                                                MRP_SERVICE_REQ_ATTR_TYPE,
                                                MRP_NORMAL_PARTICIPANT);
        nmhSetFsMrpApplicantControlAdminStatus (u4ContextId, u4Port,
                                                gMmrpAddr,
                                                MRP_MAC_ADDR_ATTR_TYPE,
                                                MRP_NORMAL_PARTICIPANT);
    }
}

/*****************************************************************************
 *
 *     FUNCTION NAME    : IssMrpShowDebugging
 *
 *     DESCRIPTION      : This function display debug configured for MRP  
 *
 *     INPUT            : CliHandle - CLI Handler
 *
 *     OUTPUT           : NONE
 *
 *     RETURNS          : NONE 
 *
 *****************************************************************************/
VOID
IssMrpShowDebugging (tCliHandle CliHandle)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4TraceOption = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (MRP_DEFAULT_CONTEXT_ID);

    if (pMrpContextInfo != NULL)
    {
        u4TraceOption = pMrpContextInfo->u4TrcOption;
    }

    if (u4TraceOption != MRP_INVALID_TRC)
    {
        CliPrintf (CliHandle, "\rMRP :");
        if ((u4TraceOption & MRP_ALL_TRC) == MRP_ALL_TRC)
        {
            CliPrintf (CliHandle, "\r\n  MRP all debugging is on\n");
            return;
        }
        if (gMrpGlobalInfo.GlobalTrcOption == MRP_ENABLED)
        {
            CliPrintf (CliHandle, "\r\n  MRP global debugging is on");
        }
        if ((u4TraceOption & MRP_PROTOCOL_TRC) == MRP_PROTOCOL_TRC)
        {
            CliPrintf (CliHandle, "\r\n  MRP protocol related debugging is on");
        }
        if ((u4TraceOption & MRP_MVRP_TRC) == MRP_MVRP_TRC)
        {
            CliPrintf (CliHandle, "\r\n  MRP mvrp related debugging is on");
        }
        if ((u4TraceOption & MRP_MMRP_TRC) == MRP_MMRP_TRC)
        {
            CliPrintf (CliHandle, "\r\n  MRP mmrp related debugging is on");
        }
        if ((u4TraceOption & INIT_SHUT_TRC) == INIT_SHUT_TRC)
        {
            CliPrintf (CliHandle,
                       "\r\n  MRP init and shutdown debugging is on");
        }
        if ((u4TraceOption & MGMT_TRC) == MGMT_TRC)
        {
            CliPrintf (CliHandle, "\r\n  MRP management debugging is on");
        }
        if ((u4TraceOption & DATA_PATH_TRC) == DATA_PATH_TRC)
        {
            CliPrintf (CliHandle, "\r\n  MRP data path debugging is on");
        }
        if ((u4TraceOption & CONTROL_PLANE_TRC) == CONTROL_PLANE_TRC)
        {
            CliPrintf (CliHandle, "\r\n  MRP control plane debugging is on");
        }
        if ((u4TraceOption & DUMP_TRC) == DUMP_TRC)
        {
            CliPrintf (CliHandle, "\r\n  MRP packet dump debugging is on");
        }
        if ((u4TraceOption & OS_RESOURCE_TRC) == OS_RESOURCE_TRC)
        {
            CliPrintf (CliHandle,
                       "\r\n  MRP resources such as memory, data structure debugging is on");
        }
        if ((u4TraceOption & ALL_FAILURE_TRC) == ALL_FAILURE_TRC)
        {
            CliPrintf (CliHandle, "\r\n  MRP all failure debugging is on");
        }
        if ((u4TraceOption & BUFFER_TRC) == BUFFER_TRC)
        {
            CliPrintf (CliHandle,
                       "\r\n  MRP buffer allocation/release debugging is on");
        }
        if ((u4TraceOption & MRP_CRITICAL_TRC) == MRP_CRITICAL_TRC)
        {
            CliPrintf (CliHandle, "\r\n  MRP critical debugging is on");
        }
        if ((u4TraceOption & MRP_APP_SEM_TRC) == MRP_APP_SEM_TRC)
        {
            CliPrintf (CliHandle,
                       "\r\n  MRP applicant state machine related debugging is on");
        }
        if ((u4TraceOption & MRP_REG_SEM_TRC) == MRP_REG_SEM_TRC)
        {
            CliPrintf (CliHandle,
                       "\r\n  MRP registrar state machine related debugging is on");
        }
        if ((u4TraceOption & MRP_LEAVEALL_SEM_TRC) == MRP_LEAVEALL_SEM_TRC)
        {
            CliPrintf (CliHandle,
                       "\r\n  MRP leaveAll state machine related debugging is on");
        }
        if ((u4TraceOption & MRP_PERIODIC_SEM_TRC) == MRP_PERIODIC_SEM_TRC)
        {
            CliPrintf (CliHandle,
                       "\r\n  MRP periodic transmission state machine related debugging is on");
        }
        if ((u4TraceOption & MRP_PDU_TRC) == MRP_PDU_TRC)
        {
            CliPrintf (CliHandle,
                       "\r\n  MRP mrpdu encoding and decoding related debugging is on");
        }
        CliPrintf (CliHandle, "\r\n");
    }
}

#endif /*__MRPCLI_C__*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpcli.c                       */
/*-----------------------------------------------------------------------*/
