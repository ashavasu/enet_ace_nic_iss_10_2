/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpred.c,v 1.4 2014/01/24 12:23:50 siva Exp $
 *
 * Description: This file contains MRP High Availability related
 *              routines.
 *********************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedProcessRmMsg 
 *
 *    DESCRIPTION      : This function processes the received RM events
 *
 *    INPUT            : pRmMsg - Pointer to the structure containing RM
 *                                buffer and the RM event
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
MrpRedProcessRmMsg (tMrpRmCtrlMsg * pRmMsg)
{
    tRmNodeInfo        *pRmNode = NULL;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    /* No need to do NULL check for pRmMsg here , since it is already done 
     * in MrpRedRcvPktFromRm
     */

    switch (pRmMsg->u1Event)
    {
        case GO_ACTIVE:
            MRP_GBL_TRC (MRP_RED_TRC,
                         "MrpRedProcessRmMsg: Received GO_ACTIVE event\r\n");

            if (RM_ACTIVE == MRP_RED_NODE_STATUS ())
            {
                break;
            }

            MrpRedHandleGoActiveEvent ();

            if (OSIX_TRUE == gMrpGlobalInfo.MrpRedInfo.bBulkUpdReqRcvd)
            {
                gMrpGlobalInfo.MrpRedInfo.bBulkUpdReqRcvd = OSIX_FALSE;
                MrpRdUtlProcBulkUpdReq ();
            }
            break;

        case GO_STANDBY:
            MRP_GBL_TRC (MRP_RED_TRC,
                         "MrpRedProcessRmMsg: Received GO_STANDBY event\r\n");
            if ((RM_STANDBY == MRP_RED_NODE_STATUS ()) ||
                (RM_INIT == MRP_RED_NODE_STATUS ()))
            {
                break;
            }
            MrpRedHandleGoStandbyEvent ();
            gMrpGlobalInfo.MrpRedInfo.bBulkUpdReqRcvd = OSIX_FALSE;
            break;

        case RM_INIT_HW_AUDIT:
            /* As MRP does not program the hardware, this event is ignored */
            break;

        case RM_STANDBY_UP:
            MRP_GBL_TRC (MRP_RED_TRC,
                         "MrpRedProcessRmMsg: Received Standby UP event\r\n");
            pRmNode = (tRmNodeInfo *) pRmMsg->pData;
            gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt = pRmNode->u1NumStandby;
            MrpPortRelRmMsgMemory ((UINT1 *) pRmNode);
            MrpRedHandleStandByUpEvent ();
            break;

        case RM_STANDBY_DOWN:
            MRP_GBL_TRC (MRP_RED_TRC,
                         "MrpRedProcessRmMsg: Recevied Standby DOWN event\r\n");
            pRmNode = (tRmNodeInfo *) pRmMsg->pData;
            gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt = pRmNode->u1NumStandby;
            MrpPortRelRmMsgMemory ((UINT1 *) pRmNode);
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            MRP_GBL_TRC (MRP_CRITICAL_TRC,
                         "MrpRedProcessRmMsg: Recevied config "
                         "restore complete event\r\n");
            if (RM_INIT == MRP_RED_NODE_STATUS ())
            {
                if (RM_STANDBY == MrpPortGetRmNodeState ())
                {
                    MrpRedMakeNodeStandbyFromIdle ();
                }
            }
            break;

        case L2_INITIATE_BULK_UPDATES:
            MRP_GBL_TRC (MRP_CRITICAL_TRC, "MrpRedProcessRmMsg: Recevied "
                         "L2_INITIATE_BULK_UPDATES event\r\n");
            MrpRedSendBulkUpdReq ();
            break;

        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pRmMsg->pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pRmMsg->pData, pRmMsg->u2DataLen);

            ProtoAck.u4AppId = RM_MRP_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            MRP_GBL_TRC (MRP_RED_TRC,
                         "MrpRedProcessRmMsg: Received Valid message\r\n");
            MrpRedProcessSyncUpMsg (pRmMsg);
            RM_FREE (pRmMsg->pData);

            /* Sending ACK to RM */
            MrpPortRmApiSendProtoAckToRM (&ProtoAck);
            break;

        default:
            MRP_GBL_TRC (MRP_CRITICAL_TRC,
                         "MrpRedProcessRmMsg: Received invalid event\r\n");
            break;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedHandleGoActiveEvent 
 *
 *    DESCRIPTION      : This function handles the GO_ACTIVE event from RM
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
MrpRedHandleGoActiveEvent (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    MRP_RED_PREV_NODE_STATUS () = MRP_RED_NODE_STATUS ();
    MRP_RED_NODE_STATUS () = RM_ACTIVE;

    /* Update the standby node count */
    gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt = MrpPortGetStandbyNodeCount ();

    if (RM_INIT == MRP_RED_PREV_NODE_STATUS ())
    {
        MrpRedMakeNodeActiveFromIdle ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else if (RM_STANDBY == MRP_RED_PREV_NODE_STATUS ())
    {
        MrpRedMakeNodeActiveFromStandby ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }

    /* Send GO_ACTIVE complete event to RM */
    ProtoEvt.u4AppId = RM_MRP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    if (MrpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
    {
        MRP_GBL_TRC (MRP_RED_TRC, "MrpRedHandleGoActiveEvent: Sending Go Active"
                     " complete event to RM failed\r\n");
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedMakeNodeStandbyFromIdle 
 *
 *    DESCRIPTION      : This function handles the node state change from IDLE
 *                       to STANDBY.
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
MrpRedMakeNodeStandbyFromIdle ()
{

    MRP_RED_PREV_NODE_STATUS () = MRP_RED_NODE_STATUS ();
    MRP_RED_NODE_STATUS () = RM_STANDBY;

    gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt = 0;

    MRP_GBL_TRC (MRP_RED_TRC, "MrpRedMakeNodeStandbyFromIdle: Made node Standby"
                 " from IDLE state\r\n");

    MrpRdUtlEnableApplications ();
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedHandleGoStandbyEvent
 *
 *    DESCRIPTION      : This function handles the GO_STANDBY event from RM
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
MrpRedHandleGoStandbyEvent (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    MRP_RED_PREV_NODE_STATUS () = MRP_RED_NODE_STATUS ();
    MRP_RED_NODE_STATUS () = RM_STANDBY;

    gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt = 0;

    MRP_GBL_TRC (MRP_RED_TRC, "MrpRedHandleGoStandbyEvent: Active to "
                 "Standby transition is successful\r\n");

    /* Intimate RM about the completion of GO_STANDBY process */
    ProtoEvt.u4AppId = RM_MRP_APP_ID;
    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
    ProtoEvt.u4Error = RM_NONE;

    if (MrpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
    {
        MRP_GBL_TRC (MRP_RED_TRC, "MrpRedHandleGoStandbyEvent: Sending Go "
                     "Standby completion event to RM failed\r\n");
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedMakeNodeActiveFromStandby
 *
 *    DESCRIPTION      : This function makes node Active from Standby state
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : None 
 *     
 ****************************************************************************/
VOID
MrpRedMakeNodeActiveFromStandby (VOID)
{
    tMrpContextInfo    *pContextInfo = NULL;
    UINT4               u4ContextId = gMrpGlobalInfo.u2FirstContextId;
    UINT1               u1AppId = 0;

    MRP_GBL_TRC (MRP_RED_TRC, "MrpRedMakeNodeActiveFromStandby: Made node"
                 " Active from Standby state\r\n");

    /* Send LeaveAll PDU through all the ports and also restart the leave all
     * timer */
    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    for (; pContextInfo != NULL;
         (pContextInfo = (MrpCtxtGetContextInfo
                          (pContextInfo->u2NextContextId))))
    {
        if (OSIX_FALSE == MRP_IS_MRP_STARTED (pContextInfo->u4ContextId))
        {
            continue;
        }

        for (u1AppId = 1; u1AppId <= MRP_MAX_APPS; u1AppId++)
        {
            if (OSIX_FALSE == MRP_IS_MRP_APP_ENABLED (pContextInfo->u4ContextId,
                                                      u1AppId))
            {
                continue;
            }

            MrpRdUtlSendLvAllMsg (pContextInfo, u1AppId);
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedMakeNodeActiveFromIdle
 *
 *    DESCRIPTION      : This function makes node Active from Idle state 
 *
 *    INPUT            : NONE 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
MrpRedMakeNodeActiveFromIdle (VOID)
{
    MRP_GBL_TRC (MRP_RED_TRC, "MrpRedMakeNodeActiveFromIdle: Made node Active"
                 " from IDLE state\r\n");
    MrpRdUtlEnableApplications ();
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedHandleStandByUpEvent 
 *
 *    DESCRIPTION      : This function handles the Standby up event. 
 *                       It processes the bulk update requset event if 
 *                       the bulk update request is received before
 *                       receiving Standby up event.
 *                       
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
MrpRedHandleStandByUpEvent (VOID)
{
    if (gMrpGlobalInfo.MrpRedInfo.bBulkUpdReqRcvd == OSIX_TRUE)
    {
#if !defined (CFA_WANTED) && !defined (PNAC_WANTED) && !defined (LA_WANTED) \
        && !defined (RSTP_WANTED) && !defined (MSTP_WANTED) \
            && !defined (VLAN_WANTED)
        gMrpGlobalInfo.MrpRedInfo.bBulkUpdReqRcvd = OSIX_FALSE;

        /* Bulk request msg is recieved before RM_STANDBY_UP event.
         * So we are sending bulk updates now. */

        MrpRdUtlProcBulkUpdReq ();
#endif
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedSendBulkUpdReq 
 *
 *    DESCRIPTION      : This function sends bulk update request message to RM
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
MrpRedSendBulkUpdReq (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pRmMsg = NULL;
    UINT1               u1MsgType = 0;
    UINT2               u2MsgLen = (UINT2) MRP_RED_MSG_HDR_SIZE;
    UINT4               u4Offset = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    if ((RM_ACTIVE == MRP_RED_PREV_NODE_STATUS ()) &&
        (RM_STANDBY == MRP_RED_NODE_STATUS ()))
    {
        /* Send bulk update completion event if previous node state is Active
         * and current node state is Standby
         */
        ProtoEvt.u4AppId = RM_MRP_APP_ID;
        ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
        ProtoEvt.u4Error = RM_NONE;
        if (MrpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
        {
            MRP_GBL_TRC (MRP_CRITICAL_TRC,
                         "MrpRedSendBulkUpdReq: Sending Bulk update "
                         "complete event" " to RM failed\r\n");
        }
    }
    else if ((RM_INIT == MRP_RED_PREV_NODE_STATUS ()) &&
             (RM_STANDBY == MRP_RED_NODE_STATUS ()))
    {
        u1MsgType = (UINT1) MRP_RM_BULK_UPDT_REQ_MSG;

        if (MrpRedAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg)
            != OSIX_SUCCESS)
        {
            MRP_GBL_TRC (MRP_CRITICAL_TRC,
                         "MrpRedSendBulkUpdReq: Memory allocation failed\r\n");

            return;
        }
        /* Form bulk update request message */
        MRP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
        /* Bulk update request message contains only the message type 
         * and length. As there is no other content for this message,
         * the length is filled as 0.
         */
        MRP_RED_PUT_2_BYTES (pRmMsg, u4Offset, 0);
        /* Send bulk update request message to RM */
        MrpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedRegisterWithRM
 *
 *    DESCRIPTION      : Registeres MRP with Redundancy Manager 
 *                       
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;
    INT4                i4RetVal = OSIX_SUCCESS;

    RmRegParams.u4EntId = RM_MRP_APP_ID;
    RmRegParams.pFnRcvPkt = MrpApiRcvPktFromRm;

    i4RetVal = MrpPortRmRegisterProtocols (&RmRegParams);

    if (OSIX_FAILURE == i4RetVal)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC | MRP_RED_TRC,
                     "MrpRedRegisterWithRM: RM Registration failed \r\n");
    }

    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedSendSyncUpMessages 
 *
 *    DESCRIPTION      : This function contstructs and sends the dynamic sync 
 *                       up message to RM which in turn will send the message
 *                       to the MRP module in the other node.
 *
 *    INPUT            : u1MsgType - Message type
 *                       pMsgInfo  - Pointer to information to be filled in 
 *                                   the synchronization message
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *     
 ****************************************************************************/
INT4
MrpRedSendSyncUpMessages (UINT1 u1MsgType, tMrpRedMsgInfo * pMsgInfo)
{
    tRmMsg             *pRmMsg = NULL;
    tRmMsg             *pBasePtr = NULL;
    UINT2               u2MsgLen = 0;
    UINT4               u4Offset = 0;

    /* Message can't be NULL */
    if (NULL == pMsgInfo)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC,
                     "MrpRedSendSyncUpMessages: Message pointer is NULL\n");
        return OSIX_FAILURE;
    }

    if (RM_STANDBY == MRP_RED_NODE_STATUS ())
    {
        /* Only ACTIVE node can send dynamic sync up messages */
        MRP_GBL_TRC (MRP_RED_TRC, "MrpRedSendSyncUpMessages: Sync up message"
                     " can't be sent from STANDBY node\r\n");
        return OSIX_SUCCESS;
    }

    /* Syncup message need not be sent if no standby node is up */
    if (0 == gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt)
    {
        MRP_GBL_TRC (MRP_RED_TRC, "MrpRedSendSyncUpMessages: STANDBY node is"
                     " down. Hence syncup message should not be sent\r\n");
        return OSIX_SUCCESS;
    }

    if ((MRP_RED_VLAN_ADD_MSG == u1MsgType) ||
        (MRP_RED_MAC_ADD_MSG == u1MsgType))
    {
        MrpRdUtlFormAttrAddSyncUpMsg (u1MsgType, pMsgInfo);
        return OSIX_SUCCESS;
    }

    MrpRdUtlGetMsgLen (u1MsgType, &u2MsgLen);

    if (MrpRedAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /*  store the initial value of RM message buffer pointer */
    pBasePtr = pRmMsg;

    u4Offset = MRP_RED_MSG_HDR_SIZE;

    /* Form the message */
    switch (u1MsgType)
    {
        case MRP_RED_PORT_OPER_STATUS_MSG:
            MrpRedFormOperChgSyncUpMsg (pRmMsg, pMsgInfo, &u4Offset);
            break;

        case MRP_RED_VLAN_DEL_MSG:
            MrpRedFormAttributeSyncUpMsg (pRmMsg, pMsgInfo, MRP_MVRP_APP_ID,
                                          MRP_VID_ATTR_TYPE, &u4Offset);
            break;

        case MRP_RED_MAC_DEL_MSG:
            MrpRedFormAttributeSyncUpMsg (pRmMsg, pMsgInfo, MRP_MMRP_APP_ID,
                                          MRP_MAC_ADDR_ATTR_TYPE, &u4Offset);
            break;

        case MRP_RED_SERVICE_REQ_ADD_MSG:
        case MRP_RED_SERVICE_REQ_DEL_MSG:
            MrpRedFormAttributeSyncUpMsg (pRmMsg, pMsgInfo, MRP_MMRP_APP_ID,
                                          MRP_SERVICE_REQ_ATTR_TYPE, &u4Offset);
            break;

        case MRP_RED_LV_ALL_TMR_RESTART_MSG:
            MrpRedFormTmrRestartSyncUpMsg (pRmMsg, pMsgInfo, &u4Offset);
            break;

        default:
            MRP_GBL_TRC (MRP_RED_TRC, "MrpRedSendSyncMessages: Invalid message "
                         "type\r\n");
            break;
    }

    /* Add Message header (Msg type and Msg length) */
    pRmMsg = pBasePtr;
    MrpRedPutMsgHdrInRmMsg (pRmMsg, u1MsgType, u2MsgLen);

    /* Send update message to RM */
    MrpRedSendMsgToRm (u1MsgType, u2MsgLen, pBasePtr);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedPutMsgHdrInRmMsg
 *
 *    DESCRIPTION      : This function adds message header(Message type and
 *                       Message length) in the message
 *
 *    INPUT            : pRmMsgBuf - Pointer to message buffer
 *                       u1MsgType - Message type
 *                       u2MsgLen  - Message length
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
MrpRedPutMsgHdrInRmMsg (tRmMsg * pRmMsgBuf, UINT1 u1MsgType, UINT2 u2MsgLen)
{
    UINT4               u4Offset = 0;

    /* Message Length should not include the length of Message type field and
     * Message Length field.
     */

    u2MsgLen = (UINT2) (u2MsgLen - MRP_RED_MSG_HDR_SIZE);

    MRP_RED_PUT_1_BYTE (pRmMsgBuf, u4Offset, u1MsgType);
    MRP_RED_PUT_2_BYTES (pRmMsgBuf, u4Offset, u2MsgLen);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedFormOperChgSyncUpMsg 
 *
 *    DESCRIPTION      : This function forms the port oper change sync up 
 *                       message to be send to the MRP in the Standby node.
 *
 *    INPUT            : pRmMsgBuf - Pointer to message buffer
 *                       pMsgInfo  - Pointer to structure containing syncup msg
 *                                   information
 *                     : pu4Offset - Pointer to number of bytes added in RM
 *                                   message buffer in calling function
 *
 *    OUTPUT           : pu4Offset - Pointer to number of bytes added in RM
 *                                   message buffer
 *
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
MrpRedFormOperChgSyncUpMsg (tRmMsg * pRmMsgBuf, tMrpRedMsgInfo * pMsgInfo,
                            UINT4 *pu4Offset)
{

    /* Port oper chg sync up message format */
    /* -----------------------------------------------
     * | MsgHdr | Context ID | PortNum |  OperStatus  |
     * ------------------------------------------------*/

    MRP_RED_PUT_4_BYTES (pRmMsgBuf, *pu4Offset, pMsgInfo->u4ContextId);
    MRP_RED_PUT_2_BYTES (pRmMsgBuf, *pu4Offset, pMsgInfo->u2LocalPortId);
    MRP_RED_PUT_1_BYTE (pRmMsgBuf, *pu4Offset, pMsgInfo->u1PortOperStatus);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedFormTmrRestartSyncUpMsg 
 *
 *    DESCRIPTION      : This function forms the Leave All Timer Restart
 *                       sync up message for the given Application
 *
 *    INPUT            : pRmMsgBuf - Pointer to message buffer
 *                       pMsgInfo  - Pointer to structure containing syncup msg
 *                                   info
 *                     : pu4Offset - Pointer to number of bytes added in RM
 *                                   message buffer in calling function
 *
 *    OUTPUT           : pu4Offset - Pointer to number of bytes added in RM
 *                                   message buffer
 *
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
MrpRedFormTmrRestartSyncUpMsg (tRmMsg * pRmMsgBuf, tMrpRedMsgInfo * pMsgInfo,
                               UINT4 *pu4Offset)
{

    /* Leave All Timer Restart Sync up message format */
    /* -----------------------------------------------
     * | MsgHdr | Context ID | PortNum |  AppId       |
     * ------------------------------------------------*/

    MRP_RED_PUT_4_BYTES (pRmMsgBuf, *pu4Offset, pMsgInfo->u4ContextId);
    MRP_RED_PUT_2_BYTES (pRmMsgBuf, *pu4Offset, pMsgInfo->u2LocalPortId);
    MRP_RED_PUT_1_BYTE (pRmMsgBuf, *pu4Offset, pMsgInfo->u1AppId);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedFormAttributeSyncUpMsg 
 *
 *    DESCRIPTION      : This function forms the sync up message for the
 *                       attribute values (VLAN ID/ MAC Address/ Service 
 *                       Requirement).
 *
 *    INPUT            : pRmMsgBuf - Pointer to message buffer
 *                       pMsgInfo  - Pointer to structure containing syncup msg
 *                                   information
 *                       u1AttrType- Attribute type
 *                     : pu4Offset - Pointer to number of bytes added in RM
 *                                   message buffer in calling function
 *
 *    OUTPUT           : pu4Offset - Pointer to number of bytes added in RM
 *                                   message buffer
 *
 *    RETURNS          : NONE 
 *     
 ****************************************************************************/
VOID
MrpRedFormAttributeSyncUpMsg (tRmMsg * pRmMsgBuf, tMrpRedMsgInfo * pMsgInfo,
                              UINT1 u1AppId, UINT1 u1AttrType, UINT4 *pu4Offset)
{
    UINT2               u2NumOfAttributes = 1;    /* Delete Syncup message is sent
                                                 * for each Attribute value
                                                 */

    /* 
     * ------------------------------------------------------------------
     * | MsgHdr | Context ID | PortNum |  Num Of Attr | Attribute Value |
     * ------------------------------------------------------------------
     * Attribute value can be Vlan Id, MAC Address or Service Requirement.
     * Num of Attr field is not present for Service Requirement.
     * In case of MRP_SERVICE_REQ_ATTR_TYPE and MRP_MAC_ADDR_ATTR_TYPE,
     * MAP ID needs to be filled after the PortNum field.
     */

    MRP_RED_PUT_4_BYTES (pRmMsgBuf, *pu4Offset, pMsgInfo->u4ContextId);
    MRP_RED_PUT_2_BYTES (pRmMsgBuf, *pu4Offset, pMsgInfo->u2LocalPortId);

    if (MRP_MVRP_APP_ID == u1AppId)
    {
        MRP_RED_PUT_2_BYTES (pRmMsgBuf, *pu4Offset, u2NumOfAttributes);
        MRP_RED_PUT_2_BYTES (pRmMsgBuf, *pu4Offset, pMsgInfo->VlanId);
    }
    else if (MRP_MMRP_APP_ID == u1AppId)
    {
        if (MRP_SERVICE_REQ_ATTR_TYPE == u1AttrType)
        {
            /* MAP ID to which this Service Requirement attribute value 
             * belongs */
            MRP_RED_PUT_2_BYTES (pRmMsgBuf, *pu4Offset, pMsgInfo->VlanId);
            MRP_RED_PUT_1_BYTE (pRmMsgBuf, *pu4Offset,
                                pMsgInfo->au1MmrpAttrVal[0]);
        }

        if (MRP_MAC_ADDR_ATTR_TYPE == u1AttrType)
        {
            /* MAP ID to which this MAC address belongs */
            MRP_RED_PUT_2_BYTES (pRmMsgBuf, *pu4Offset, pMsgInfo->VlanId);
            MRP_RED_PUT_2_BYTES (pRmMsgBuf, *pu4Offset, u2NumOfAttributes);
            MRP_RED_PUT_N_BYTES (pRmMsgBuf, pMsgInfo->au1MmrpAttrVal,
                                 *pu4Offset, MAC_ADDR_LEN);
        }
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedAllocMemForRmMsg
 *
 *    DESCRIPTION      : This function allocates memory for dynamic sync up/
 *                       bulk update/ tail message
 *
 *    INPUT            : u1MsgType - Message type
 *                       u2MsgLen  - Message length
 *
 *    OUTPUT           : ppRmMsg - pointer to allocate memory 
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRedAllocMemForRmMsg (UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg ** ppRmMsg)
{
    tRmMsg             *pRmMsg = NULL;
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    *ppRmMsg = NULL;

    if ((MRP_RED_INVALID_MSG_LEN == u2MsgLen) ||
        (u2MsgLen > MRP_RED_MAX_MSG_SIZE))
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC, "MrpRedAllocMemForRmMsg: Invalid "
                     "message length \r\n");
        return OSIX_FAILURE;
    }

    if ((pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2MsgLen)) == NULL)
    {
        if ((u1MsgType == MRP_RM_BULK_UPDT_REQ_MSG) ||
            (u1MsgType == MRP_RM_BULK_UPDATE_MSG) ||
            (u1MsgType == MRP_RM_BULK_UPDT_TAIL_MSG))
        {
            /* Send bulk update failure event to RM */
            ProtoEvt.u4AppId = RM_MRP_APP_ID;
            ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
            ProtoEvt.u4Error = RM_MEMALLOC_FAIL;

            if (MrpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
            {
                MRP_GBL_TRC (MRP_RED_TRC, "MrpRedAllocMemForRmMsg: Sending "
                             "failure event to RM failed\r\n");
            }
        }
        MRP_GBL_TRC (MRP_CRITICAL_TRC, "MrpRedAllocMemForRmMsg: Memory "
                     "allocation failed\r\n");
        return OSIX_FAILURE;
    }

    *ppRmMsg = pRmMsg;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedSendBulkUpdTailMsg
 *
 *    DESCRIPTION      : This function sends bulk update tail message to RM
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
MrpRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = (UINT2) MRP_RED_MSG_HDR_SIZE;
    UINT1               u1MsgType = (UINT1) MRP_RM_BULK_UPDT_TAIL_MSG;

    if (MrpRedAllocMemForRmMsg (u1MsgType, u2MsgLen, &pRmMsg) != OSIX_SUCCESS)
    {
        return;
    }

    /* Update bulk update status in RMGR */
    MrpPortSetBulkUpdateStatus ();

    MRP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    /* Tail message contains only the message type and length.
     * As there is no other content for this message,
     * the length is filled as 0.
     */
    MRP_RED_PUT_2_BYTES (pRmMsg, u4Offset, 0);

    MrpRedSendMsgToRm (u1MsgType, u2MsgLen, pRmMsg);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedSendMsgToRm
 *
 *    DESCRIPTION      : This function sends the given protocol(MRP) messages 
 *                       to RM
 *    
 *    INPUT            : u1MsgType - Message type
 *                       u2MsgLen - Totoal Message length (including Message
 *                                  Header)
 *                       pRmMsg - Pointer to message
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
MrpRedSendMsgToRm (UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg * pRmMsg)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    if (MrpPortEnqMsgToRm (pRmMsg, u2MsgLen) != OSIX_SUCCESS)
    {
        if ((MRP_RM_BULK_UPDT_REQ_MSG == u1MsgType) ||
            (MRP_RM_BULK_UPDATE_MSG == u1MsgType) ||
            (MRP_RM_BULK_UPDT_TAIL_MSG == u1MsgType))
        {
            /* Send bulk update failure event to RM */
            ProtoEvt.u4AppId = RM_MRP_APP_ID;
            ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
            ProtoEvt.u4Error = RM_SENDTO_FAIL;

            if (MrpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
            {
                MRP_GBL_TRC (MRP_RED_TRC, "MrpRedSendMsgToRm: Sending bulk "
                             "update failure event to RM failed\r\n");
            }

            MRP_GBL_TRC (MRP_RED_TRC, "MrpRedSendMsgToRm: Sending bulk "
                         "update msg/sync up msg to RM failed\r\n");
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedProcessSyncUpMsg 
 *
 *    DESCRIPTION      : This function is invoked whenever MRP module
 *                       receives a dynamic sync up/bulk update/bulk request
 *                       message.
 *
 *    INPUT            : pRmCtrlMsg - RM control message 
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
MrpRedProcessSyncUpMsg (tMrpRmCtrlMsg * pRmCtrlMsg)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    pRmMsg = pRmCtrlMsg->pData;

    MRP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2MsgLen);

    /* Validate message lentgh */
    if (u2MsgLen != (pRmCtrlMsg->u2DataLen - MRP_RED_MSG_HDR_SIZE))
    {
        /* Message length present in message and data length present in
         * RMCtrl message doesn't match.
         */
        MRP_GBL_TRC (MRP_RED_TRC, "MrpRedProcessSyncUpMsg: Invalid message "
                     "length\r\n");
        return;
    }

    /* Active node can process only BULK_REQ. So, if the node status is
     * Active and message type is not BULK_REQ then return.
     */
    if (RM_ACTIVE == MRP_RED_NODE_STATUS ())
    {
        if (MRP_RM_BULK_UPDT_REQ_MSG != u1MsgType)
        {
            MRP_GBL_TRC (MRP_RED_TRC, "MrpRedProcessSyncUpMsg: Active node "
                         "received sync up message other than "
                         "MRP_RM_BULK_UPDT_REQ_MSG\r\n");
            return;
        }
    }
    else if (RM_INIT == MRP_RED_NODE_STATUS ())
    {
        MRP_GBL_TRC (MRP_RED_TRC, "MrpRedProcessSyncUpMsg: Received sync up "
                     "msg from RM when MRP node is in INIT state\r\n");
        return;
    }

    /* Process the message */
    switch (u1MsgType)
    {
        case MRP_RM_BULK_UPDATE_MSG:
            MrpRBulkProcBulkUpdMsg (pRmMsg, u2MsgLen);
            break;

        case MRP_RM_BULK_UPDT_TAIL_MSG:
            MRP_GBL_TRC (MRP_CRITICAL_TRC, "MrpRedProcessSyncUpMsg: Received "
                         "MRP_RM_BULK_UPDT_TAIL_MSG\n");
            ProtoEvt.u4AppId = RM_MRP_APP_ID;
            ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
            ProtoEvt.u4Error = RM_NONE;

            if (MrpPortSendEventToRm (&ProtoEvt) != OSIX_SUCCESS)
            {
                MRP_GBL_TRC (MRP_CRITICAL_TRC,
                             "MrpRedProcessSyncUpMsg: Sending Bulk update "
                             "complete event to RM failed\r\n");
            }
            break;

        case MRP_RED_PORT_OPER_STATUS_MSG:
            MrpRdUtlProcPortOperChgSyncUp (pRmMsg, u2MsgLen);
            break;

        case MRP_RED_LV_ALL_TMR_RESTART_MSG:
            MrpRdUtlProcTmrRestartSyncUpMsg (pRmMsg, u2MsgLen);
            break;

        case MRP_RED_VLAN_ADD_MSG:
        case MRP_RED_MAC_ADD_MSG:
            MrpRdUtlProcAttrAddSyncUpMsg (pRmMsg, u1MsgType);
            break;

        case MRP_RED_VLAN_DEL_MSG:
        case MRP_RED_MAC_DEL_MSG:
        case MRP_RED_SERVICE_REQ_DEL_MSG:
            MrpRdUtlProcAttributeSyncUpMsg (pRmMsg, u1MsgType, u2MsgLen);
            break;

        case MRP_RED_SERVICE_REQ_ADD_MSG:
            MrpRdUtlProcSerReqAddSyncUpMsg (pRmMsg, u2MsgLen);
            break;

        case MRP_RM_BULK_UPDT_REQ_MSG:
            MRP_GBL_TRC (MRP_CRITICAL_TRC,
                         "MrpRedProcessSyncUpMsg: Received bulk"
                         " update request\r\n");
            if (MRP_INIT_VAL == gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt)
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on getting RM_STANDBY_UP event.
                 */
                gMrpGlobalInfo.MrpRedInfo.bBulkUpdReqRcvd = OSIX_TRUE;
                break;
            }
            gMrpGlobalInfo.MrpRedInfo.bBulkUpdReqRcvd = OSIX_FALSE;
            MrpRdUtlProcBulkUpdReq ();
            break;

        default:
            MRP_GBL_TRC (MRP_RED_TRC, "MrpRedProcessSyncUpMsg: Invalid message"
                         " type\r\n");
            break;
    }

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpred.c                       */
/*-----------------------------------------------------------------------*/
