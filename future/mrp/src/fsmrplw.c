/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmrplw.c,v 1.13 2013/06/25 12:08:37 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include  "lr.h"
# include  "mrpinc.h"
# include  "fssnmp.h"
# include  "mrptdfs.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMrpGlobalTraceOption
 Input       :  The Indices

                The Object 
                retValFsMrpGlobalTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpGlobalTraceOption (INT4 *pi4RetValFsMrpGlobalTraceOption)
{
#ifdef TRACE_WANTED
    *pi4RetValFsMrpGlobalTraceOption = gMrpGlobalInfo.GlobalTrcOption;
#else
    UNUSED_PARAM (*pi4RetValFsMrpGlobalTraceOption);
#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMrpGlobalTraceOption
 Input       :  The Indices

                The Object 
                setValFsMrpGlobalTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpGlobalTraceOption (INT4 i4SetValFsMrpGlobalTraceOption)
{
#ifdef TRACE_WANTED

    if (gMrpGlobalInfo.GlobalTrcOption == i4SetValFsMrpGlobalTraceOption)
    {
        return SNMP_SUCCESS;
    }

    gMrpGlobalInfo.GlobalTrcOption = (UINT1) i4SetValFsMrpGlobalTraceOption;
#else
    UNUSED_PARAM (i4SetValFsMrpGlobalTraceOption);
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMrpGlobalTraceOption
 Input       :  The Indices

                The Object 
                testValFsMrpGlobalTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpGlobalTraceOption (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMrpGlobalTraceOption)
{
#ifdef TRACE_WANTED
    if ((i4TestValFsMrpGlobalTraceOption != MRP_ENABLED) &&
        (i4TestValFsMrpGlobalTraceOption != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (i4TestValFsMrpGlobalTraceOption);
    return SNMP_FAILURE;
#endif

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMrpGlobalTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMrpGlobalTraceOption (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMrpInstanceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMrpInstanceTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsMrpInstanceTable
    (UINT4 u4Ieee8021BridgeBaseComponentId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMrpInstanceTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMrpInstanceTable (UINT4 *pu4Ieee8021BridgeBaseComponentId)
{
    return (nmhGetNextIndexFsMrpInstanceTable
            (0, pu4Ieee8021BridgeBaseComponentId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMrpInstanceTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
                nextIeee8021BridgeBaseComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMrpInstanceTable (UINT4 u4Ieee8021BridgeBaseComponentId,
                                   UINT4 *pu4NextIeee8021BridgeBaseComponentId)
{
    tMrpContextInfo    *pContextInfo = NULL;

    if (u4Ieee8021BridgeBaseComponentId == 0)
    {
        /* Return the First Valid Context */
        pContextInfo = MrpCtxtGetContextInfo (gMrpGlobalInfo.u2FirstContextId);

        if (pContextInfo != NULL)
        {
            *pu4NextIeee8021BridgeBaseComponentId = pContextInfo->u4ContextId;
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }

    pContextInfo = MrpCtxtGetContextInfo (u4Ieee8021BridgeBaseComponentId);

    if ((pContextInfo != NULL) &&
        (pContextInfo->u2NextContextId != MRP_INIT_VAL))
    {
        *pu4NextIeee8021BridgeBaseComponentId = pContextInfo->u2NextContextId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMrpInstanceSystemControl
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValFsMrpInstanceSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpInstanceSystemControl (UINT4 u4Ieee8021BridgeBaseComponentId,
                                  INT4 *pi4RetValFsMrpInstanceSystemControl)
{
    *pi4RetValFsMrpInstanceSystemControl =
        gMrpGlobalInfo.au1MrpSystemCtrl[u4Ieee8021BridgeBaseComponentId];
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpInstanceTraceInputString
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValFsMrpInstanceTraceInputString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpInstanceTraceInputString (UINT4 u4Ieee8021BridgeBaseComponentId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsMrpInstanceTraceInputString)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4TraceOption = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if ((pMrpContextInfo == NULL) ||
        (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE))
    {
        /* default trace option - critical trace */

        u4TraceOption = MRP_CRITICAL_TRC;

        /* get trace input string corresponding to the given
         * trace option */

        pRetValFsMrpInstanceTraceInputString->i4_Length =
            (INT4)
            MrpTrcGetTraceInputValue (pRetValFsMrpInstanceTraceInputString->
                                      pu1_OctetList, u4TraceOption);

        return SNMP_SUCCESS;
    }

    u4TraceOption = pMrpContextInfo->u4TrcOption;

    pRetValFsMrpInstanceTraceInputString->i4_Length =
        (INT4) MrpTrcGetTraceInputValue (pRetValFsMrpInstanceTraceInputString->
                                         pu1_OctetList, u4TraceOption);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpInstanceNotifyVlanRegFailure
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValFsMrpInstanceNotifyVlanRegFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpInstanceNotifyVlanRegFailure (UINT4 u4Ieee8021BridgeBaseComponentId,
                                         INT4
                                         *pi4RetValFsMrpInstanceNotifyVlanRegFailure)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMrpInstanceNotifyVlanRegFailure =
        pMrpContextInfo->u1MvrpTrapEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpInstanceNotifyMacRegFailure
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                retValFsMrpInstanceNotifyMacRegFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpInstanceNotifyMacRegFailure (UINT4 u4Ieee8021BridgeBaseComponentId,
                                        INT4
                                        *pi4RetValFsMrpInstanceNotifyMacRegFailure)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMrpInstanceNotifyMacRegFailure =
        pMrpContextInfo->u1MmrpTrapEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpInstanceBridgeMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object
                retValFsMrpInstanceBridgeMmrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpInstanceBridgeMmrpEnabledStatus (UINT4
                                            u4Ieee8021BridgeBaseComponentId,
                                            INT4
                                            *pi4RetValFsMrpInstanceBridgeMmrpEnabledStatus)
{
    tMmrpStatusInfo     MmrpStatusInfo;
    INT4                i4MmrpEnabledStatus = 0;

    MEMSET (&MmrpStatusInfo, 0, sizeof (tMmrpStatusInfo));

    MmrpStatusInfo.u4ContextId = u4Ieee8021BridgeBaseComponentId;
    MmrpStatusInfo.pi4MmrpEnabledStatus = &i4MmrpEnabledStatus;
    if ((MrpUtilGetMmrpEnabledStatus (&MmrpStatusInfo)) == OSIX_SUCCESS)
    {
        *pi4RetValFsMrpInstanceBridgeMmrpEnabledStatus =
            *(MmrpStatusInfo.pi4MmrpEnabledStatus);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMrpInstanceBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object
                retValFsMrpInstanceBridgeMvrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpInstanceBridgeMvrpEnabledStatus (UINT4
                                            u4Ieee8021BridgeBaseComponentId,
                                            INT4
                                            *pi4RetValFsMrpInstanceBridgeMvrpEnabledStatus)
{
    tMvrpStatusInfo     MvrpStatusInfo;
    INT4                i4MvrpEnabledStatus = 0;

    MEMSET (&MvrpStatusInfo, 0, sizeof (tMvrpStatusInfo));
    MvrpStatusInfo.u4ContextId = u4Ieee8021BridgeBaseComponentId;
    MvrpStatusInfo.pi4MvrpEnabledStatus = &i4MvrpEnabledStatus;
    if ((MrpUtilGetMvrpEnabledStatus (&MvrpStatusInfo)) == OSIX_SUCCESS)
    {
        *pi4RetValFsMrpInstanceBridgeMvrpEnabledStatus =
            *(MvrpStatusInfo.pi4MvrpEnabledStatus);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMrpInstanceSystemControl
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValFsMrpInstanceSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpInstanceSystemControl (UINT4 u4Ieee8021BridgeBaseComponentId,
                                  INT4 i4SetValFsMrpInstanceSystemControl)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBaseComponentId) ==
        i4SetValFsMrpInstanceSystemControl)
    {
        return SNMP_SUCCESS;
    }

    if (i4SetValFsMrpInstanceSystemControl == MRP_SHUTDOWN)
    {
        /* Shutdown the MRP Module. */
        i4RetVal = MrpMainDeInit (pMrpContextInfo);

        if (i4RetVal == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        gMrpGlobalInfo.au1MrpSystemCtrl[u4Ieee8021BridgeBaseComponentId]
            = MRP_START;
        /* Start MRP Module. */
        i4RetVal = MrpMainInit (pMrpContextInfo);

        if (i4RetVal == OSIX_FAILURE)
        {
            /* DeInitialize the Main Module. */
            MrpMainDeInit (pMrpContextInfo);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMrpInstanceTraceInputString
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValFsMrpInstanceTraceInputString
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpInstanceTraceInputString (UINT4 u4Ieee8021BridgeBaseComponentId,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pSetValFsMrpInstanceTraceInputString)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4TrcOption = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    u4TrcOption = MrpTrcGetTraceOptionValue
        (pSetValFsMrpInstanceTraceInputString->pu1_OctetList,
         pSetValFsMrpInstanceTraceInputString->i4_Length);

    if ((STRNCMP (pSetValFsMrpInstanceTraceInputString->pu1_OctetList,
                  "enable", STRLEN ("enable"))) == 0)
    {
        /* Enale Trace */
        pMrpContextInfo->u4TrcOption |= u4TrcOption;
    }
    else
    {
        /*Disable Trace */
        pMrpContextInfo->u4TrcOption &= ~u4TrcOption;
    }

    /* set trace input */
    MEMSET (pMrpContextInfo->au1TrcInput, 0, MRP_TRC_MAX_SIZE);
    MrpTrcGetTraceInputValue (pMrpContextInfo->au1TrcInput,
                              pMrpContextInfo->u4TrcOption);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMrpInstanceNotifyVlanRegFailure
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValFsMrpInstanceNotifyVlanRegFailure
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpInstanceNotifyVlanRegFailure (UINT4 u4Ieee8021BridgeBaseComponentId,
                                         INT4
                                         i4SetValFsMrpInstanceNotifyVlanRegFailure)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo->u1MvrpTrapEnabled =
        (UINT1) i4SetValFsMrpInstanceNotifyVlanRegFailure;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMrpInstanceNotifyMacRegFailure
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                setValFsMrpInstanceNotifyMacRegFailure
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpInstanceNotifyMacRegFailure (UINT4 u4Ieee8021BridgeBaseComponentId,
                                        INT4
                                        i4SetValFsMrpInstanceNotifyMacRegFailure)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo->u1MmrpTrapEnabled =
        (UINT1) i4SetValFsMrpInstanceNotifyMacRegFailure;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMrpInstanceBridgeMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object
                setValFsMrpInstanceBridgeMmrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpInstanceBridgeMmrpEnabledStatus (UINT4
                                            u4Ieee8021BridgeBaseComponentId,
                                            INT4
                                            i4SetValFsMrpInstanceBridgeMmrpEnabledStatus)
{
    tMmrpStatusInfo     MmrpStatusInfo;

    MEMSET (&MmrpStatusInfo, 0, sizeof (tMmrpStatusInfo));
    MmrpStatusInfo.u4ContextId = u4Ieee8021BridgeBaseComponentId;
    MmrpStatusInfo.i4MmrpEnabledStatus =
        i4SetValFsMrpInstanceBridgeMmrpEnabledStatus;
    if ((MrpUtilSetMmrpEnabledStatus (&MmrpStatusInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsMrpInstanceBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object
                setValFsMrpInstanceBridgeMvrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpInstanceBridgeMvrpEnabledStatus (UINT4
                                            u4Ieee8021BridgeBaseComponentId,
                                            INT4
                                            i4SetValFsMrpInstanceBridgeMvrpEnabledStatus)
{
    tMvrpStatusInfo     MvrpStatusInfo;

    MEMSET (&MvrpStatusInfo, 0, sizeof (tMvrpStatusInfo));
    MvrpStatusInfo.u4ContextId = u4Ieee8021BridgeBaseComponentId;
    MvrpStatusInfo.i4MvrpEnabledStatus =
        i4SetValFsMrpInstanceBridgeMvrpEnabledStatus;
    if ((MrpUtilSetMvrpEnabledStatus (&MvrpStatusInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMrpInstanceSystemControl
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValFsMrpInstanceSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpInstanceSystemControl (UINT4 *pu4ErrorCode,
                                     UINT4 u4Ieee8021BridgeBaseComponentId,
                                     INT4 i4TestValFsMrpInstanceSystemControl)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4BridgeMode = MRP_INVALID_BRIDGE_MODE;
    UINT2               u2VlanIsStarted = 0;
    UINT2               u2GarpIsStarted = 0;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMrpInstanceSystemControl != MRP_START) &&
        (i4TestValFsMrpInstanceSystemControl != MRP_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    MrpPortL2IwfGetBridgeMode (pMrpContextInfo->u4ContextId, &u4BridgeMode);

    if (u4BridgeMode == MRP_INVALID_BRIDGE_MODE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_BRIDGE_MODE);
        return SNMP_FAILURE;
    }

    if (i4TestValFsMrpInstanceSystemControl == MRP_START)
    {
        u2VlanIsStarted = (UINT2) MrpPortVlanGetStartedStatus
            (u4Ieee8021BridgeBaseComponentId);
        /* Before Start the MRP module , VLAN module should 
           be started.Otherwise no use of starting MRP. */
        if (u2VlanIsStarted == VLAN_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_NOSHUT_VLAN_ENABLED_ERR);
            return SNMP_FAILURE;
        }

        if (MrpPortVlanGetBaseBridgeMode () == DOT_1D_BRIDGE_MODE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_BASE_BRIDGE_ENABLED_ERR);
            return SNMP_FAILURE;
        }

        u2GarpIsStarted = (UINT2) MrpPortGarpGetStartedStatus
            (u4Ieee8021BridgeBaseComponentId);

        if (u2GarpIsStarted == GARP_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_NOSHUT_GARP_STARTED_ERR);
            return SNMP_FAILURE;
        }

    }

    if (i4TestValFsMrpInstanceSystemControl == MRP_SHUTDOWN)
    {
        if ((gMrpGlobalInfo.au1MvrpStatus[pMrpContextInfo->u4ContextId]
             == MRP_ENABLED) ||
            (gMrpGlobalInfo.au1MmrpStatus[pMrpContextInfo->u4ContextId]
             == MRP_ENABLED))
        {
            /*
             * MRP can be shuts down only when all the MRP
             * applications are diabled
             */
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_SHUT_APP_ENABLED_ERR);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMrpInstanceTraceInputString
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValFsMrpInstanceTraceInputString
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpInstanceTraceInputString (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ieee8021BridgeBaseComponentId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFsMrpInstanceTraceInputString)
{
#ifdef TRACE_WANTED
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4TrcOption = 0;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBaseComponentId) != OSIX_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

#ifdef SNMP_2_WANTED
    if (SNMPCheckForNVTChars
        (pTestValFsMrpInstanceTraceInputString->pu1_OctetList,
         pTestValFsMrpInstanceTraceInputString->i4_Length) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }
#endif

    if ((pTestValFsMrpInstanceTraceInputString->i4_Length > MRP_TRC_MAX_SIZE) ||
        (pTestValFsMrpInstanceTraceInputString->i4_Length < MRP_TRC_MIN_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    u4TrcOption = MrpTrcGetTraceOptionValue
        (pTestValFsMrpInstanceTraceInputString->pu1_OctetList,
         pTestValFsMrpInstanceTraceInputString->i4_Length);

    if (u4TrcOption == MRP_INVALID_TRC)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

#else /*TRACE_WANTED */
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    UNUSED_PARAM (u4Ieee8021BridgeBaseComponentId);
    UNUSED_PARAM (pTestValFsMrpInstanceTraceInputString);
    return SNMP_FAILURE;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMrpInstanceNotifyVlanRegFailure
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValFsMrpInstanceNotifyVlanRegFailure
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpInstanceNotifyVlanRegFailure (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021BridgeBaseComponentId,
                                            INT4
                                            i4TestValFsMrpInstanceNotifyVlanRegFailure)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBaseComponentId) != OSIX_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMrpInstanceNotifyVlanRegFailure != MRP_ENABLED) &&
        (i4TestValFsMrpInstanceNotifyVlanRegFailure != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMrpInstanceNotifyMacRegFailure
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object 
                testValFsMrpInstanceNotifyMacRegFailure
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpInstanceNotifyMacRegFailure (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021BridgeBaseComponentId,
                                           INT4
                                           i4TestValFsMrpInstanceNotifyMacRegFailure)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBaseComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBaseComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMrpInstanceNotifyMacRegFailure != MRP_ENABLED) &&
        (i4TestValFsMrpInstanceNotifyMacRegFailure != MRP_DISABLED))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMrpInstanceBridgeMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object
                testValFsMrpInstanceBridgeMmrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpInstanceBridgeMmrpEnabledStatus (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBaseComponentId,
                                               INT4
                                               i4TestValFsMrpInstanceBridgeMmrpEnabledStatus)
{
    tMmrpStatusInfo     MmrpStatusInfo;
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;

    MEMSET (&MmrpStatusInfo, 0, sizeof (tMmrpStatusInfo));
    MmrpStatusInfo.u4ContextId = u4Ieee8021BridgeBaseComponentId;
    MmrpStatusInfo.i4MmrpEnabledStatus =
        i4TestValFsMrpInstanceBridgeMmrpEnabledStatus;
    MmrpStatusInfo.pu4ErrorCode = &u4ErrCode;
    if ((MrpUtilTestMmrpEnabledStatus (&MmrpStatusInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = *(MmrpStatusInfo.pu4ErrorCode);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMrpInstanceBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId

                The Object
                testValFsMrpInstanceBridgeMvrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpInstanceBridgeMvrpEnabledStatus (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBaseComponentId,
                                               INT4
                                               i4TestValFsMrpInstanceBridgeMvrpEnabledStatus)
{
    tMvrpStatusInfo     MvrpStatusInfo;
    UINT4               u4ErrCode = SNMP_ERR_NO_ERROR;

    MEMSET (&MvrpStatusInfo, 0, sizeof (tMvrpStatusInfo));
    MvrpStatusInfo.u4ContextId = u4Ieee8021BridgeBaseComponentId;
    MvrpStatusInfo.i4MvrpEnabledStatus =
        i4TestValFsMrpInstanceBridgeMvrpEnabledStatus;
    MvrpStatusInfo.pu4ErrorCode = &u4ErrCode;
    if ((MrpUtilTestMvrpEnabledStatus (&MvrpStatusInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = *(MvrpStatusInfo.pu4ErrorCode);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMrpInstanceTable
 Input       :  The Indices
                Ieee8021BridgeBaseComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMrpInstanceTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMrpPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMrpPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMrpPortTable (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MRP_GET_PORT_ENTRY (pMrpContextInfo, u4Ieee8021BridgeBasePort) == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMrpPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMrpPortTable (UINT4 *pu4Ieee8021BridgeBasePortComponentId,
                                UINT4 *pu4Ieee8021BridgeBasePort)
{
    return (nmhGetNextIndexFsMrpPortTable (0,
                                           pu4Ieee8021BridgeBasePortComponentId,
                                           0, pu4Ieee8021BridgeBasePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMrpPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMrpPortTable (UINT4 u4Ieee8021BridgeBasePortComponentId,
                               UINT4 *pu4NextIeee8021BridgeBasePortComponentId,
                               UINT4 u4Ieee8021BridgeBasePort,
                               UINT4 *pu4NextIeee8021BridgeBasePort)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4ContextId = u4Ieee8021BridgeBasePortComponentId;

    for (; u4ContextId < MRP_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

        if (pMrpContextInfo == NULL)
        {
            /* If Context Info NULL means ,Get Next Context Info */
            continue;
        }
        if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
        {
            continue;
        }

        i4RetVal = MrpCtxtGetNextPortInContext (u4ContextId,
                                                u4Ieee8021BridgeBasePort,
                                                pu4NextIeee8021BridgeBasePort);

        if (i4RetVal == OSIX_SUCCESS)
        {
            *pu4NextIeee8021BridgeBasePortComponentId = u4ContextId;
            return SNMP_SUCCESS;
        }

        u4Ieee8021BridgeBasePort = 0;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMrpPortPeriodicSEMStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValFsMrpPortPeriodicSEMStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortPeriodicSEMStatus (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  INT4 *pi4RetValFsMrpPortPeriodicSEMStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMrpPortPeriodicSEMStatus = pMrpPortEntry->u1PeriodicSEMStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortParticipantType
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValFsMrpPortParticipantType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortParticipantType (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                UINT4 u4Ieee8021BridgeBasePort,
                                INT4 *pi4RetValFsMrpPortParticipantType)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMrpPortParticipantType = pMrpPortEntry->u1ParticipantType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortRegAdminControl
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValFsMrpPortRegAdminControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortRegAdminControl (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                UINT4 u4Ieee8021BridgeBasePort,
                                INT4 *pi4RetValFsMrpPortRegAdminControl)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMrpPortRegAdminControl = pMrpPortEntry->u1RegAdminCtrl;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortRestrictedGroupRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValFsMrpPortRestrictedGroupRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortRestrictedGroupRegistration (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            *pi4RetValFsMrpPortRestrictedGroupRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMrpPortRestrictedGroupRegistration =
        pMrpPortEntry->u1RestrictedMACRegCtrl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValFsMrpPortRestrictedVlanRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortRestrictedVlanRegistration (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           *pi4RetValFsMrpPortRestrictedVlanRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMrpPortRestrictedVlanRegistration =
        pMrpPortEntry->u1RestrictedVlanRegCtrl;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMrpPortPeriodicSEMStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValFsMrpPortPeriodicSEMStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpPortPeriodicSEMStatus (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  INT4 i4SetValFsMrpPortPeriodicSEMStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsMrpPortPeriodicSEMStatus == MRP_ENABLED)
    {
        if (pMrpPortEntry->u1PeriodicSEMState != MRP_ACTIVE)
        {
            MrpTmrStartPeriodicTmr (pMrpPortEntry);
            pMrpPortEntry->u1PeriodicSEMState = MRP_ACTIVE;
        }
    }
    else
    {
        if (pMrpPortEntry->u1PeriodicSEMState == MRP_ACTIVE)
        {
            pMrpPortEntry->u1PeriodicSEMState = MRP_PASSIVE;

        }
    }

    pMrpPortEntry->u1PeriodicSEMStatus =
        (UINT1) i4SetValFsMrpPortPeriodicSEMStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMrpPortParticipantType
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValFsMrpPortParticipantType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpPortParticipantType (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                UINT4 u4Ieee8021BridgeBasePort,
                                INT4 i4SetValFsMrpPortParticipantType)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u1ParticipantType = (UINT1) i4SetValFsMrpPortParticipantType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhSetFsMrpPortRegAdminControl
 *  Input       :  The Indices
 *                 Ieee8021BridgeBasePortComponentId
 *                 Ieee8021BridgeBasePort
 *                 The Object 
 *                 setValFsMrpPortRegAdminControl
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMrpPortRegAdminControl (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                UINT4 u4Ieee8021BridgeBasePort,
                                INT4 i4SetValFsMrpPortRegAdminControl)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsMrpPortRegAdminControl == MRP_REG_FORBIDDEN)
    {
        MrpUtilDelAllLearntVlans (pMrpPortEntry);
    }

    pMrpPortEntry->u1RegAdminCtrl = (UINT1) i4SetValFsMrpPortRegAdminControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMrpPortRestrictedGroupRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValFsMrpPortRestrictedGroupRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpPortRestrictedGroupRegistration (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4SetValFsMrpPortRestrictedGroupRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u1RestrictedMACRegCtrl =
        (UINT1) i4SetValFsMrpPortRestrictedGroupRegistration;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMrpPortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValFsMrpPortRestrictedVlanRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpPortRestrictedVlanRegistration (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4SetValFsMrpPortRestrictedVlanRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u1RestrictedVlanRegCtrl =
        (UINT1) i4SetValFsMrpPortRestrictedVlanRegistration;

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMrpPortPeriodicSEMStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValFsMrpPortPeriodicSEMStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpPortPeriodicSEMStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     INT4 i4TestValFsMrpPortPeriodicSEMStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMrpPortPeriodicSEMStatus != MRP_ENABLED) &&
        (i4TestValFsMrpPortPeriodicSEMStatus != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMrpPortParticipantType
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValFsMrpPortParticipantType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpPortParticipantType (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   INT4 i4TestValFsMrpPortParticipantType)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMrpPortParticipantType != MRP_FULL_PARTICIPANT) &&
        (i4TestValFsMrpPortParticipantType != MRP_APPLICANT_ONLY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMrpPortRegAdminControl
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValFsMrpPortRegAdminControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpPortRegAdminControl (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   INT4 i4TestValFsMrpPortRegAdminControl)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if ((i4TestValFsMrpPortRegAdminControl < MRP_REG_NORMAL) ||
        (i4TestValFsMrpPortRegAdminControl > MRP_REG_FORBIDDEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMrpPortRestrictedGroupRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValFsMrpPortRestrictedGroupRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpPortRestrictedGroupRegistration (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4TestValFsMrpPortRestrictedGroupRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMrpPortRestrictedGroupRegistration !=
         MRP_ENABLED)
        && (i4TestValFsMrpPortRestrictedGroupRegistration != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMrpPortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValFsMrpPortRestrictedVlanRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpPortRestrictedVlanRegistration (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4TestValFsMrpPortRestrictedVlanRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        (UINT2) u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMrpPortRestrictedVlanRegistration != MRP_ENABLED)
        && (i4TestValFsMrpPortRestrictedVlanRegistration != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMrpPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMrpPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMvrpPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMvrpPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMvrpPortTable (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort)
{
    tMvrpPortInfo       MvrpPortInfo;

    MEMSET (&MvrpPortInfo, 0, sizeof (tMvrpPortInfo));
    MvrpPortInfo.u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    MvrpPortInfo.u4IfIndex = u4Ieee8021BridgeBasePort;

    if ((MrpUtilValidateMrpTableIndices (&MvrpPortInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMvrpPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMvrpPortTable (UINT4 *pu4Ieee8021BridgeBasePortComponentId,
                                 UINT4 *pu4Ieee8021BridgeBasePort)
{
    return (nmhGetNextIndexFsMvrpPortTable
            (0, pu4Ieee8021BridgeBasePortComponentId, 0,
             pu4Ieee8021BridgeBasePort));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMvrpPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMvrpPortTable (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                UINT4 *pu4NextIeee8021BridgeBasePortComponentId,
                                UINT4 u4Ieee8021BridgeBasePort,
                                UINT4 *pu4NextIeee8021BridgeBasePort)
{
    tMvrpPortInfo       MvrpPortInfo;
    UINT4               u4ContextId = 0;
    UINT4               u4Port = 0;

    MEMSET (&MvrpPortInfo, 0, sizeof (tMvrpPortInfo));
    MvrpPortInfo.u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    MvrpPortInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    MvrpPortInfo.pu4NextContextId = &u4ContextId;
    MvrpPortInfo.pu4NextIfIndex = &u4Port;
    if ((MrpUtilGetNextIndexMrpTable (&MvrpPortInfo)) == OSIX_SUCCESS)
    {
        *pu4NextIeee8021BridgeBasePortComponentId =
            *(MvrpPortInfo.pu4NextContextId);
        *pu4NextIeee8021BridgeBasePort = *(MvrpPortInfo.pu4NextIfIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMvrpPortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValFsMvrpPortMvrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMvrpPortMvrpEnabledStatus (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   INT4 *pi4RetValFsMvrpPortMvrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMvrpPortMvrpEnabledStatus = pMrpPortEntry->u1PortMvrpStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMvrpPortMvrpFailedRegistrations
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValFsMvrpPortMvrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMvrpPortMvrpFailedRegistrations (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValFsMvrpPortMvrpFailedRegistrations)
{
    tMvrpPortInfo       MvrpPortInfo;
    tSNMP_COUNTER64_TYPE u8MvrpFailReg;

    MEMSET (&MvrpPortInfo, 0, sizeof (tMvrpPortInfo));
    MEMSET (&u8MvrpFailReg, 0, sizeof (tSNMP_COUNTER64_TYPE));
    MvrpPortInfo.u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    MvrpPortInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    MvrpPortInfo.pu8PortMvrpFailedRegist = &u8MvrpFailReg;

    if ((MrpUtilGetPortMvrpFailedRegist (&MvrpPortInfo)) == OSIX_SUCCESS)
    {
        *pu8RetValFsMvrpPortMvrpFailedRegistrations =
            *(MvrpPortInfo.pu8PortMvrpFailedRegist);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMvrpPortMvrpLastPduOrigin
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValFsMvrpPortMvrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMvrpPortMvrpLastPduOrigin (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   tMacAddr *
                                   pRetValFsMvrpPortMvrpLastPduOrigin)
{
    tMvrpPortInfo       MvrpPortInfo;
    tMacAddr            MacAddr;

    MEMSET (&MvrpPortInfo, 0, sizeof (tMvrpPortInfo));
    MEMSET (&MacAddr, 0, sizeof (tMacAddr));
    MvrpPortInfo.u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    MvrpPortInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    MvrpPortInfo.pPortMvrpLastPduOrigin = &MacAddr;

    if ((MrpUtilGetPortMvrpLastPduOrigin (&MvrpPortInfo)) == OSIX_SUCCESS)
    {
        MEMCPY (pRetValFsMvrpPortMvrpLastPduOrigin,
                (MvrpPortInfo.pPortMvrpLastPduOrigin), MRP_MAC_ADDR_LEN);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMvrpPortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValFsMvrpPortMvrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMvrpPortMvrpEnabledStatus (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   INT4 i4SetValFsMvrpPortMvrpEnabledStatus)
{

    tMvrpPortInfo       MvrpPortInfo;

    MEMSET (&MvrpPortInfo, 0, sizeof (tMvrpPortInfo));
    MvrpPortInfo.u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    MvrpPortInfo.u4IfIndex = u4Ieee8021BridgeBasePort;
    MvrpPortInfo.i4PortMvrpEnabledStatus = i4SetValFsMvrpPortMvrpEnabledStatus;
    if ((MrpUtilSetPortMvrpEnabledStatus (&MvrpPortInfo)) == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMvrpPortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValFsMvrpPortMvrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMvrpPortMvrpEnabledStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      INT4 i4TestValFsMvrpPortMvrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1PortType = 0;
    UINT1               u1TunnelStatus = 0;
    UINT1               u1Status = 0;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry =
        MRP_GET_PORT_ENTRY (pMrpContextInfo, u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMvrpPortMvrpEnabledStatus != MRP_ENABLED) &&
        (i4TestValFsMvrpPortMvrpEnabledStatus != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    if (i4TestValFsMvrpPortMvrpEnabledStatus == MRP_ENABLED)
    {
        if ((MrpPortL2IwfGetVlanPortType
             (pMrpPortEntry->u4IfIndex, &u1PortType)) == L2IWF_SUCCESS)
        {
            if (u1PortType == VLAN_ACCESS_PORT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_MRP_ACCESS_PORT_ERR);
                return SNMP_FAILURE;
            }
        }

        if (pMrpContextInfo->u4BridgeMode == MRP_CUSTOMER_BRIDGE_MODE)
        {
            MrpPortGetProtoTunelStatusOnPort (pMrpPortEntry->u4IfIndex,
                                              L2_PROTO_MVRP, &u1TunnelStatus);

            if ((u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
                (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_MVRP_TUNNEL_ENABLED_ERR);
                return SNMP_FAILURE;
            }
        }

        if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
            (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_PORT_MVRP_ENABLED_ERR);
            return SNMP_FAILURE;
        }
    }

    if (SISP_IS_LOGICAL_PORT (pMrpPortEntry->u4IfIndex) == VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MVRP_SISP_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    else
    {
        L2IwfGetSispPortCtrlStatus (pMrpPortEntry->u4IfIndex, &u1Status);
        if (u1Status == L2IWF_ENABLED)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MVRP_SISP_CONFIG_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMvrpPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMvrpPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMrpApplicantControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMrpApplicantControlTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpAttributeType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMrpApplicantControlTable (UINT4
                                                    u4Ieee8021BridgeBasePortComponentId,
                                                    UINT4
                                                    u4Ieee8021BridgeBasePort,
                                                    tMacAddr
                                                    FsMrpApplicationAddress,
                                                    INT4 i4FsMrpAttributeType)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MEMCMP (FsMrpApplicationAddress, pMrpContextInfo->MvrpAddr,
                MRP_MAC_ADDR_LEN) == 0)
    {
        if (MRP_IS_VALID_MVRP_ATTR_TYPE (i4FsMrpAttributeType) == OSIX_FALSE)
        {
            return SNMP_FAILURE;
        }

    }
    else if (MEMCMP (FsMrpApplicationAddress, gMmrpAddr, MRP_MAC_ADDR_LEN) == 0)
    {
        if (MRP_IS_VALID_MMRP_ATTR_TYPE (i4FsMrpAttributeType) == OSIX_FALSE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* The given MAC address is a Invalid  Address */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMrpApplicantControlTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpAttributeType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMrpApplicantControlTable (UINT4
                                            *pu4Ieee8021BridgeBasePortComponentId,
                                            UINT4 *pu4Ieee8021BridgeBasePort,
                                            tMacAddr * pFsMrpApplicationAddress,
                                            INT4 *pi4FsMrpAttributeType)
{
    tMacAddr            MacAddr;

    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    return (nmhGetNextIndexFsMrpApplicantControlTable (0,
                                                       pu4Ieee8021BridgeBasePortComponentId,
                                                       0,
                                                       pu4Ieee8021BridgeBasePort,
                                                       MacAddr,
                                                       pFsMrpApplicationAddress,
                                                       0,
                                                       pi4FsMrpAttributeType));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMrpApplicantControlTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                FsMrpApplicationAddress
                nextFsMrpApplicationAddress
                FsMrpAttributeType
                nextFsMrpAttributeType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMrpApplicantControlTable (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4
                                           *pu4NextIeee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           UINT4 *pu4NextIeee8021BridgeBasePort,
                                           tMacAddr FsMrpApplicationAddress,
                                           tMacAddr *
                                           pNextFsMrpApplicationAddress,
                                           INT4 i4FsMrpAttributeType,
                                           INT4 *pi4NextFsMrpAttributeType)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMacAddr            NextMacAddr;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    UINT4               u4PortId = u4Ieee8021BridgeBasePort;

    MEMSET (NextMacAddr, 0, MAC_ADDR_LEN);
    for (; u4ContextId < MRP_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

        if (pMrpContextInfo == NULL)
        {
            /* If the ContextInfo NULL means then  
             * Get next Context Info */
            continue;
        }
        if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
        {
            continue;
        }

        for (; u4PortId <= MRP_MAX_PORTS_PER_CONTEXT; u4PortId++)
        {
            pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u4PortId);

            if (pMrpPortEntry == NULL)
            {
                continue;
            }
            if (MEMCMP (FsMrpApplicationAddress,
                        pMrpContextInfo->MvrpAddr, MAC_ADDR_LEN) == 0)
            {
                if (MRP_IS_VALID_MVRP_ATTR_TYPE (i4FsMrpAttributeType)
                    == OSIX_FALSE)
                {
                    i4FsMrpAttributeType = MRP_INIT_VAL;
                }
                if (i4FsMrpAttributeType > MRP_VID_ATTR_TYPE)
                {
                    i4FsMrpAttributeType = MRP_INVALID_VALUE;
                }
            }
            else if (MEMCMP (FsMrpApplicationAddress, gMmrpAddr,
                             MRP_MAC_ADDR_LEN) == 0)
            {
                if (MRP_IS_VALID_MMRP_ATTR_TYPE (i4FsMrpAttributeType)
                    == OSIX_FALSE)
                {
                    i4FsMrpAttributeType = MRP_INIT_VAL;
                }
                if (i4FsMrpAttributeType > MRP_MAC_ADDR_ATTR_TYPE)
                {
                    i4FsMrpAttributeType = MRP_INVALID_VALUE;
                }
            }

            i4RetVal = MrpAppGetNextAppAddrAndAttrType (u4ContextId,
                                                        FsMrpApplicationAddress,
                                                        NextMacAddr,
                                                        i4FsMrpAttributeType,
                                                        pi4NextFsMrpAttributeType);

            if (i4RetVal == OSIX_SUCCESS)
            {
                *pu4NextIeee8021BridgeBasePortComponentId = u4ContextId;
                *pu4NextIeee8021BridgeBasePort = u4PortId;
                MEMCPY (*pNextFsMrpApplicationAddress, NextMacAddr,
                        MRP_MAC_ADDR_LEN);

                return SNMP_SUCCESS;
            }

            MEMSET (FsMrpApplicationAddress, 0, MRP_MAC_ADDR_LEN);
            i4FsMrpAttributeType = MRP_INIT_VAL;

        }
        u4PortId = 1;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMrpApplicantControlAdminStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpAttributeType

                The Object 
                retValFsMrpApplicantControlAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpApplicantControlAdminStatus (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        tMacAddr FsMrpApplicationAddress,
                                        INT4 i4FsMrpAttributeType,
                                        INT4
                                        *pi4RetValFsMrpApplicantControlAdminStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((0 == u1AppId) || (u1AppId > MRP_MAX_APPS))
    {
        /* Added to avoid Klocwork warning */
        return SNMP_FAILURE;
    }

    *pi4RetValFsMrpApplicantControlAdminStatus =
        pMrpPortEntry->au1ApplAdminCtrl[u1AppId][i4FsMrpAttributeType];

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMrpApplicantControlAdminStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpAttributeType

                The Object 
                setValFsMrpApplicantControlAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpApplicantControlAdminStatus (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        tMacAddr FsMrpApplicationAddress,
                                        INT4 i4FsMrpAttributeType,
                                        INT4
                                        i4SetValFsMrpApplicantControlAdminStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((0 == u1AppId) || (u1AppId > MRP_MAX_APPS))
    {
        /* Added to avoid Klocwork warning */
        return SNMP_FAILURE;
    }

    pMrpPortEntry->au1ApplAdminCtrl[u1AppId][i4FsMrpAttributeType] =
        (UINT1) i4SetValFsMrpApplicantControlAdminStatus;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMrpApplicantControlAdminStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpAttributeType

                The Object 
                testValFsMrpApplicantControlAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpApplicantControlAdminStatus (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           tMacAddr FsMrpApplicationAddress,
                                           INT4 i4FsMrpAttributeType,
                                           INT4
                                           i4TestValFsMrpApplicantControlAdminStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if (MEMCMP (FsMrpApplicationAddress, pMrpContextInfo->MvrpAddr,
                MRP_MAC_ADDR_LEN) == 0)
    {
        if (MRP_IS_VALID_MVRP_ATTR_TYPE (i4FsMrpAttributeType) == OSIX_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_MRP_INVALID_MVRP_ATTR);
            return SNMP_FAILURE;
        }
    }

    else if (MEMCMP (FsMrpApplicationAddress, gMmrpAddr, MRP_MAC_ADDR_LEN) == 0)
    {
        if (MRP_IS_VALID_MMRP_ATTR_TYPE (i4FsMrpAttributeType) == OSIX_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_MRP_INVALID_MMRP_ATTR);
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_MAC_ADDR);
        return SNMP_FAILURE;

    }

    if ((i4TestValFsMrpApplicantControlAdminStatus != MRP_NORMAL_PARTICIPANT) &&
        (i4TestValFsMrpApplicantControlAdminStatus != MRP_NON_PARTICIPANT) &&
        (i4TestValFsMrpApplicantControlAdminStatus != MRP_ACTIVE_APPLICANT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMrpApplicantControlTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpAttributeType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMrpApplicantControlTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMrpPortStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMrpPortStatsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMrpPortStatsTable (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             tMacAddr FsMrpApplicationAddress)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpStatsEntry     *pStatsEntry = NULL;
    UINT1               u1AppId = (UINT1) MRP_MMRP_APP_ID;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MEMCMP (FsMrpApplicationAddress, pMrpContextInfo->MvrpAddr,
                MRP_MAC_ADDR_LEN) != 0)
    {
        if (MEMCMP (FsMrpApplicationAddress, gMmrpAddr, MRP_MAC_ADDR_LEN) != 0)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        u1AppId = (UINT1) MRP_MVRP_APP_ID;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMrpPortStatsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMrpPortStatsTable (UINT4
                                     *pu4Ieee8021BridgeBasePortComponentId,
                                     UINT4 *pu4Ieee8021BridgeBasePort,
                                     tMacAddr * pFsMrpApplicationAddress)
{
    tMacAddr            ZeroMacAddr;

    MEMSET (ZeroMacAddr, 0, sizeof (tMacAddr));

    return (nmhGetNextIndexFsMrpPortStatsTable (0,
                                                pu4Ieee8021BridgeBasePortComponentId,
                                                0, pu4Ieee8021BridgeBasePort,
                                                ZeroMacAddr,
                                                pFsMrpApplicationAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMrpPortStatsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                FsMrpApplicationAddress
                nextFsMrpApplicationAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMrpPortStatsTable (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                    UINT4
                                    *pu4NextIeee8021BridgeBasePortComponentId,
                                    UINT4 u4Ieee8021BridgeBasePort,
                                    UINT4 *pu4NextIeee8021BridgeBasePort,
                                    tMacAddr FsMrpApplicationAddress,
                                    tMacAddr * pNextFsMrpApplicationAddress)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMacAddr            NextMacAddr;
    UINT4               u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    UINT4               u4PortId = u4Ieee8021BridgeBasePort;
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    for (; u4ContextId < MRP_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

        if (pMrpContextInfo == NULL)
        {
            continue;
        }
        if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
        {
            continue;
        }
        for (; u4PortId <= MRP_MAX_PORTS_PER_CONTEXT; u4PortId++)
        {
            pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u4PortId);

            if (pMrpPortEntry == NULL)
            {
                continue;
            }

            i4RetVal = MrpAppGetNextAppAddFrmCurAppAdd (u4ContextId,
                                                        FsMrpApplicationAddress,
                                                        NextMacAddr);

            if (i4RetVal == OSIX_SUCCESS)
            {
                *pu4NextIeee8021BridgeBasePortComponentId = u4ContextId;

                *pu4NextIeee8021BridgeBasePort = u4PortId;

                MEMCPY (pNextFsMrpApplicationAddress, NextMacAddr,
                        MRP_MAC_ADDR_LEN);

                return SNMP_SUCCESS;
            }

            MEMSET (FsMrpApplicationAddress, 0, MRP_MAC_ADDR_LEN);
        }
        u4PortId = 1;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsNumberOfRegistrations
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsNumberOfRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsNumberOfRegistrations (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           tMacAddr FsMrpApplicationAddress,
                                           tSNMP_COUNTER64_TYPE *
                                           pu8RetValFsMrpPortStatsNumberOfRegistrations)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsNumberOfRegistrations->lsn =
        pStatsEntry->u4NoOfRegistration;
    pu8RetValFsMrpPortStatsNumberOfRegistrations->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsRxValidPduCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsRxValidPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsRxValidPduCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     tMacAddr FsMrpApplicationAddress,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsMrpPortStatsRxValidPduCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsRxValidPduCount->lsn = pStatsEntry->u4RxValidPduCnt;
    pu8RetValFsMrpPortStatsRxValidPduCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsRxInvalidPduCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsRxInvalidPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsRxInvalidPduCount (UINT4
                                       u4Ieee8021BridgeBasePortComponentId,
                                       UINT4 u4Ieee8021BridgeBasePort,
                                       tMacAddr FsMrpApplicationAddress,
                                       tSNMP_COUNTER64_TYPE *
                                       pu8RetValFsMrpPortStatsRxInvalidPduCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsRxInvalidPduCount->lsn =
        pStatsEntry->u4RxInvalidPduCnt;
    pu8RetValFsMrpPortStatsRxInvalidPduCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsRxNewMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsRxNewMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsRxNewMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   tMacAddr FsMrpApplicationAddress,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValFsMrpPortStatsRxNewMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);
    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }
    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsRxNewMsgCount->lsn = pStatsEntry->u4RxNewMsgCnt;
    pu8RetValFsMrpPortStatsRxNewMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsRxJoinInMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsRxJoinInMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsRxJoinInMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      tMacAddr FsMrpApplicationAddress,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsMrpPortStatsRxJoinInMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);
    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsRxJoinInMsgCount->lsn =
        pStatsEntry->u4RxJoinInMsgCnt;
    pu8RetValFsMrpPortStatsRxJoinInMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsRxJoinMtMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsRxJoinMtMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsRxJoinMtMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      tMacAddr FsMrpApplicationAddress,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsMrpPortStatsRxJoinMtMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsRxJoinMtMsgCount->lsn =
        pStatsEntry->u4RxJoinMtMsgCnt;
    pu8RetValFsMrpPortStatsRxJoinMtMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsRxLeaveMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsRxLeaveMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsRxLeaveMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     tMacAddr FsMrpApplicationAddress,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsMrpPortStatsRxLeaveMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsRxLeaveMsgCount->lsn = pStatsEntry->u4RxLeaveMsgCnt;
    pu8RetValFsMrpPortStatsRxLeaveMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsRxEmptyMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsRxEmptyMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsRxEmptyMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     tMacAddr FsMrpApplicationAddress,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsMrpPortStatsRxEmptyMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsRxEmptyMsgCount->lsn = pStatsEntry->u4RxEmptyMsgCnt;
    pu8RetValFsMrpPortStatsRxEmptyMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsRxInMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsRxInMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsRxInMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  tMacAddr FsMrpApplicationAddress,
                                  tSNMP_COUNTER64_TYPE *
                                  pu8RetValFsMrpPortStatsRxInMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsRxInMsgCount->lsn = pStatsEntry->u4RxInMsgCnt;
    pu8RetValFsMrpPortStatsRxInMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsRxLeaveAllMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsRxLeaveAllMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsRxLeaveAllMsgCount (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        tMacAddr FsMrpApplicationAddress,
                                        tSNMP_COUNTER64_TYPE *
                                        pu8RetValFsMrpPortStatsRxLeaveAllMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsRxLeaveAllMsgCount->lsn =
        pStatsEntry->u4RxLeaveAllMsgCnt;
    pu8RetValFsMrpPortStatsRxLeaveAllMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsTxPduCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsTxPduCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsTxPduCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                UINT4 u4Ieee8021BridgeBasePort,
                                tMacAddr FsMrpApplicationAddress,
                                tSNMP_COUNTER64_TYPE *
                                pu8RetValFsMrpPortStatsTxPduCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsTxPduCount->lsn = pStatsEntry->u4TxPduCnt;
    pu8RetValFsMrpPortStatsTxPduCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsTxNewMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsTxNewMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsTxNewMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                   UINT4 u4Ieee8021BridgeBasePort,
                                   tMacAddr FsMrpApplicationAddress,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValFsMrpPortStatsTxNewMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsTxNewMsgCount->lsn = pStatsEntry->u4TxNewMsgCnt;
    pu8RetValFsMrpPortStatsTxNewMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsTxJoinInMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsTxJoinInMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsTxJoinInMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      tMacAddr FsMrpApplicationAddress,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsMrpPortStatsTxJoinInMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsTxJoinInMsgCount->lsn =
        pStatsEntry->u4TxJoinInMsgCnt;
    pu8RetValFsMrpPortStatsTxJoinInMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsTxJoinMtMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsTxJoinMtMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsTxJoinMtMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      tMacAddr FsMrpApplicationAddress,
                                      tSNMP_COUNTER64_TYPE *
                                      pu8RetValFsMrpPortStatsTxJoinMtMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsTxJoinMtMsgCount->lsn =
        pStatsEntry->u4TxJoinMtMsgCnt;
    pu8RetValFsMrpPortStatsTxJoinMtMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsTxLeaveMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsTxLeaveMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsTxLeaveMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     tMacAddr FsMrpApplicationAddress,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsMrpPortStatsTxLeaveMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsTxLeaveMsgCount->lsn = pStatsEntry->u4TxLeaveMsgCnt;
    pu8RetValFsMrpPortStatsTxLeaveMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsTxEmptyMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsTxEmptyMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsTxEmptyMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     tMacAddr FsMrpApplicationAddress,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValFsMrpPortStatsTxEmptyMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsTxEmptyMsgCount->lsn = pStatsEntry->u4TxEmptyMsgCnt;
    pu8RetValFsMrpPortStatsTxEmptyMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsTxInMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsTxInMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsTxInMsgCount (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                  UINT4 u4Ieee8021BridgeBasePort,
                                  tMacAddr FsMrpApplicationAddress,
                                  tSNMP_COUNTER64_TYPE *
                                  pu8RetValFsMrpPortStatsTxInMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }
    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsTxInMsgCount->lsn = pStatsEntry->u4TxInMsgCnt;
    pu8RetValFsMrpPortStatsTxInMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsTxLeaveAllMsgCount
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsTxLeaveAllMsgCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsTxLeaveAllMsgCount (UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        tMacAddr FsMrpApplicationAddress,
                                        tSNMP_COUNTER64_TYPE *
                                        pu8RetValFsMrpPortStatsTxLeaveAllMsgCount)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValFsMrpPortStatsTxLeaveAllMsgCount->lsn =
        pStatsEntry->u4TxLeaveAllMsgCnt;
    pu8RetValFsMrpPortStatsTxLeaveAllMsgCount->msn = 0;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpPortStatsClearStatistics
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                retValFsMrpPortStatsClearMrpStatistics
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpPortStatsClearStatistics (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     tMacAddr FsMrpApplicationAddress,
                                     INT4
                                     *pi4RetValFsMrpPortStatsClearStatistics)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMrpPortStatsClearStatistics = MRP_SNMP_FALSE;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMrpPortStatsClearStatistics
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                setValFsMrpPortStatsClearStatistics
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMrpPortStatsClearStatistics (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     tMacAddr FsMrpApplicationAddress,
                                     INT4 i4SetValFsMrpPortStatsClearStatistics)
{
    tMrpStatsEntry     *pStatsEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1AppId = 0;

    UNUSED_PARAM (i4SetValFsMrpPortStatsClearStatistics);

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) != MRP_START)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pStatsEntry = pMrpPortEntry->pStatsEntry[u1AppId];

    if (pStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pStatsEntry, 0, sizeof (tMrpStatsEntry));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMrpPortStatsClearStatistics
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress

                The Object 
                testValFsMrpPortStatsClearStatistics
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMrpPortStatsClearStatistics (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        tMacAddr FsMrpApplicationAddress,
                                        INT4
                                        i4TestValFsMrpPortStatsClearStatistics)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if (MEMCMP (FsMrpApplicationAddress, pMrpContextInfo->MvrpAddr,
                MRP_MAC_ADDR_LEN) != 0)
    {
        if (MEMCMP (FsMrpApplicationAddress, gMmrpAddr, MRP_MAC_ADDR_LEN) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValFsMrpPortStatsClearStatistics != MRP_SNMP_TRUE) &&
        (i4TestValFsMrpPortStatsClearStatistics != MRP_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMrpPortStatsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMrpPortStatsTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMrpSEMTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMrpSEMTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpSEMMapContext
                FsMrpAttributeType
                FsMrpSEMAttributeValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMrpSEMTable (UINT4
                                       u4Ieee8021BridgeBasePortComponentId,
                                       UINT4 u4Ieee8021BridgeBasePort,
                                       tMacAddr FsMrpApplicationAddress,
                                       INT4 i4FsMrpSEMMapContext,
                                       INT4 i4FsMrpAttributeType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMrpSEMAttributeValue)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpAppEntry       *pMrpAppEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpAttrEntry      *pMrpAttrEntry = NULL;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    tMrpAttrEntry       AttrEntry;
    UINT1               u1AppId = 0;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pFsMrpSEMAttributeValue == NULL)
    {
        return SNMP_FAILURE;
    }

    if (MEMCMP (FsMrpApplicationAddress, pMrpContextInfo->MvrpAddr,
                MRP_MAC_ADDR_LEN) == 0)
    {
        if (MRP_IS_VALID_MVRP_ATTR_TYPE (i4FsMrpAttributeType) == OSIX_FALSE)
        {
            return SNMP_FAILURE;
        }

        if (pFsMrpSEMAttributeValue->i4_Length != MRP_VLAN_ID_LEN)
        {
            return SNMP_FAILURE;
        }

    }
    else if (MEMCMP (FsMrpApplicationAddress, gMmrpAddr, MRP_MAC_ADDR_LEN) == 0)
    {

        if (MRP_IS_VALID_MMRP_ATTR_TYPE (i4FsMrpAttributeType) == OSIX_FALSE)
        {
            return SNMP_FAILURE;
        }

        if ((pFsMrpSEMAttributeValue->i4_Length != MRP_MAC_ADDR_LEN) &&
            (pFsMrpSEMAttributeValue->i4_Length != MRP_SERVICE_REQ_LEN))
        {
            return SNMP_FAILURE;
        }

    }
    else
    {
        /* The given Multicast MAC address is a Invalid  Address */
        return SNMP_FAILURE;
    }

    MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));
    AttrEntry.Attr.u1AttrType = (UINT1) i4FsMrpAttributeType;

    AttrEntry.Attr.u1AttrLen = (UINT1) pFsMrpSEMAttributeValue->i4_Length;

    MEMCPY (AttrEntry.Attr.au1AttrVal, pFsMrpSEMAttributeValue->pu1_OctetList,
            pFsMrpSEMAttributeValue->i4_Length);

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_APP_ENABLED (u4Ieee8021BridgeBasePortComponentId,
                                u1AppId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, u1AppId);

    if (i4FsMrpSEMMapContext >= pMrpAppEntry->u2MapArraySize)
    {
        return SNMP_FAILURE;
    }

    pMrpMapEntry = MRP_GET_MAP_ENTRY (pMrpAppEntry,
                                      (UINT2) i4FsMrpSEMMapContext);

    if (pMrpMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    AttrEntry.pMapEntry = pMrpMapEntry;

    pMrpAttrEntry = (tMrpAttrEntry *) RBTreeGet
        (pMrpAppEntry->MapAttrTable, &AttrEntry);

    if (pMrpAttrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry,
                                               (UINT2)
                                               u4Ieee8021BridgeBasePort);
    if (pMrpMapPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMrpSEMTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpSEMMapContext
                FsMrpAttributeType
                FsMrpSEMAttributeValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMrpSEMTable (UINT4 *pu4Ieee8021BridgeBasePortComponentId,
                               UINT4 *pu4Ieee8021BridgeBasePort,
                               tMacAddr * pFsMrpApplicationAddress,
                               INT4 *pi4FsMrpSEMMapContext,
                               INT4 *pi4FsMrpAttributeType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMrpSEMAttributeValue)
{
    tMacAddr            ZeroMacAddr;
    tSNMP_OCTET_STRING_TYPE FirstOctetStringType;
    UINT1               au1Value[MRP_MAX_ATTR_LEN];

    FirstOctetStringType.i4_Length = MRP_MAX_ATTR_LEN;

    FirstOctetStringType.pu1_OctetList = au1Value;
    MEMSET (FirstOctetStringType.pu1_OctetList, 0, MRP_MAX_ATTR_LEN);

    MEMSET (ZeroMacAddr, 0, sizeof (tMacAddr));

    return (nmhGetNextIndexFsMrpSEMTable (0,
                                          pu4Ieee8021BridgeBasePortComponentId,
                                          0, pu4Ieee8021BridgeBasePort,
                                          ZeroMacAddr,
                                          pFsMrpApplicationAddress, 0,
                                          pi4FsMrpSEMMapContext, 0,
                                          pi4FsMrpAttributeType,
                                          &FirstOctetStringType,
                                          pFsMrpSEMAttributeValue));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMrpSEMTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                FsMrpApplicationAddress
                nextFsMrpApplicationAddress
                FsMrpSEMMapContext
                nextFsMrpSEMMapContext
                FsMrpAttributeType
                nextFsMrpAttributeType
                FsMrpSEMAttributeValue
                nextFsMrpSEMAttributeValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMrpSEMTable (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 *pu4NextIeee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              UINT4 *pu4NextIeee8021BridgeBasePort,
                              tMacAddr FsMrpApplicationAddress,
                              tMacAddr * pNextFsMrpApplicationAddress,
                              INT4 i4FsMrpSEMMapContext,
                              INT4 *pi4NextFsMrpSEMMapContext,
                              INT4 i4FsMrpAttributeType,
                              INT4 *pi4NextFsMrpAttributeType,
                              tSNMP_OCTET_STRING_TYPE * pFsMrpSEMAttributeValue,
                              tSNMP_OCTET_STRING_TYPE *
                              pNextFsMrpSEMAttributeValue)
{
    tMrpAttrEntry       AttrEntry;
    tMrpAttrEntry      *pMrpAttrEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAppEntry       *pMrpAppEntry = NULL;
    UINT4               u4ContextId = u4Ieee8021BridgeBasePortComponentId;
    UINT2               u2PortId = 0;
    UINT2               u2PortIdTmp = (UINT2) u4Ieee8021BridgeBasePort;
    UINT2               u2MapIdTmp = (UINT2) i4FsMrpSEMMapContext;
    UINT2               u2MapId = 0;
    UINT1               u1AttrInfo = MRP_INIT_VAL;
    UINT1               u1AppId = 0;
    UINT1               u1AppIdTmp = 0;

    pMrpContextInfo = MrpCtxtGetValidContext (u4ContextId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Clear the attribute Entry */
    MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));

    /* Fill The attribute type and value with the argument passed
     * Assigment is done in the start itself because this assigment 
     * needs to be done only once. If we put this inside the Map 
     * Loop each time the it coninues for some condition  
     * the same assignment will be done multiple times*/
    AttrEntry.Attr.u1AttrType = (UINT1) i4FsMrpAttributeType;

    if (pFsMrpSEMAttributeValue != NULL)
    {
        AttrEntry.Attr.u1AttrLen = (UINT1) pFsMrpSEMAttributeValue->i4_Length;

        if (MEMCMP (FsMrpApplicationAddress,
                    pMrpContextInfo->MvrpAddr, MAC_ADDR_LEN) == 0)
        {
            if (MRP_IS_VALID_MVRP_ATTR_TYPE (i4FsMrpAttributeType)
                == OSIX_FALSE)
            {
                AttrEntry.Attr.u1AttrType = (UINT1) MRP_INIT_VAL;
            }
            if (pFsMrpSEMAttributeValue->i4_Length != MRP_VLAN_ID_LEN)
            {
                AttrEntry.Attr.u1AttrLen = (UINT1) MRP_INIT_VAL;
            }
        }
        else if (MEMCMP (FsMrpApplicationAddress, gMmrpAddr,
                         MRP_MAC_ADDR_LEN) == 0)
        {
            if (MRP_IS_VALID_MMRP_ATTR_TYPE (i4FsMrpAttributeType)
                == OSIX_FALSE)
            {
                AttrEntry.Attr.u1AttrType = (UINT1) MRP_INIT_VAL;
            }

            if ((pFsMrpSEMAttributeValue->i4_Length
                 != MRP_MAC_ADDR_LEN) &&
                (pFsMrpSEMAttributeValue->i4_Length != MRP_SERVICE_REQ_LEN))
            {
                AttrEntry.Attr.u1AttrLen = (UINT1) MRP_INIT_VAL;
            }
        }

        MEMCPY (AttrEntry.Attr.au1AttrVal,
                pFsMrpSEMAttributeValue->pu1_OctetList,
                AttrEntry.Attr.u1AttrLen);

    }

    /*Loop Thro the ContextId - 1st Index of the table */
    for (u4ContextId = pMrpContextInfo->u4ContextId;
         u4ContextId != MRP_INIT_VAL;
         u4ContextId = (UINT4) ((pMrpContextInfo == NULL) ? MRP_INIT_VAL :
                                pMrpContextInfo->u2NextContextId))

    {
        pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

        /*If the context info is null continue with the next Valid Context */
        if (pMrpContextInfo == NULL)
        {
            continue;
        }

        /*If MRP is not started in that context go to next valid context */
        if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
        {
            continue;
        }

        /*If the context Id is not the one passed start with the 1st port */
        if (u4ContextId != u4Ieee8021BridgeBasePortComponentId)
        {
            u2PortIdTmp = 0;
            u2MapIdTmp = 0;
            MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));
        }

        /*Get the port entry of the portId if it is a valid port or return 
         * the next valid port entry*/
        pMrpPortEntry = MrpIfGetValidPort (pMrpContextInfo, u2PortIdTmp);

        /*If context doesn't have a valid port entry go to the next context */
        if (pMrpPortEntry == NULL)
        {
            continue;
        }

        /*Loop thro the ports - 2nd Index of the table */
        for (u2PortId = pMrpPortEntry->u2LocalPortId; u2PortId != MRP_INIT_VAL;
             u2PortId = (UINT2) ((pMrpPortEntry == NULL) ?
                                 MRP_INIT_VAL : pMrpPortEntry->u2NextPortId))
        {

            pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2PortId);

            if (pMrpPortEntry == NULL)
            {
                continue;
            }

            /*If the Port ID is not the one which was passed as argument, 
             * start with the 1st application (MMRP)
             * Reset the Map Id as well as the AttEntry*/

            if (u2PortId != u4Ieee8021BridgeBasePort)
            {
                u1AppIdTmp = MRP_MMRP_APP_ID;
                u2MapIdTmp = 0;
                MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));

            }
            else
            {
                MrpAppGetAppIdFromAppAddr (u4ContextId,
                                           FsMrpApplicationAddress,
                                           &u1AppIdTmp);

            }

            /*Loop thro the application Ids - 3rd Index of the table */
            for (u1AppId = u1AppIdTmp; u1AppId <= MRP_MAX_APPS; u1AppId++)
            {

                pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, u1AppId);

                if (OSIX_FALSE == MRP_IS_MRP_APP_ENABLED (u4ContextId, u1AppId))
                {
                    /* Go to next application and hence reset the map id
                     * and attribute entry. */
                    u2MapIdTmp = 0;
                    MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));
                    continue;
                }
                if (pMrpAppEntry == NULL)
                {
                    /* Go to next application and hence reset the map id
                     * and attribute entry. */
                    u2MapIdTmp = 0;
                    MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));
                    continue;
                }

                /*Loop thro the map Id. The 4th index is Map Id. Map Id is 
                 * the 1st index of RBTree of Attribute Table.*/

                for (u2MapId = u2MapIdTmp;
                     u2MapId < pMrpAppEntry->u2MapArraySize; u2MapId++)
                {

                    /*If MapID is not the same as that is passed
                     * then reset the AttrEntry*/
                    if (u2MapId != (UINT2) i4FsMrpSEMMapContext)
                    {
                        MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));
                    }

                    if (pMrpAppEntry->ppMapTable[u2MapId] == NULL)
                    {
                        /*If MapID does not give a valid mapentry continue
                         * with the next mapId*/
                        continue;
                    }

                    /*Fill up the a MapEntry of the AttrEntry */
                    AttrEntry.pMapEntry = pMrpAppEntry->ppMapTable[u2MapId];

                    /*Do a getNext in the table with whatever value 
                     * filled up for AttrEntry */
                    pMrpAttrEntry = ((tMrpAttrEntry *) RBTreeGetNext
                                     (pMrpAppEntry->MapAttrTable,
                                      &AttrEntry, NULL));

                    if (pMrpAttrEntry == NULL)
                    {
                        /* This means the attribute table for this application
                         * is exhausted. So break and go to next application. 
                         */
                        break;
                    }

                    do
                    {

                        u1AttrInfo = 0;

                        /*Upadte the MapID with the value that
                         * we got it from the RBTree, so that the
                         * mapId continues from the value we got 
                         * it from the AttrTable*/
                        u2MapId = pMrpAttrEntry->pMapEntry->u2MapId;

                        /*Check if the port is a member of the map */
                        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY
                            (pMrpAttrEntry->pMapEntry, u2PortId);

                        if (pMapPortEntry == NULL)
                        {
                            /*If the port is not in the Map,then go
                             * to the next map */
                            break;
                        }
                        else
                        {

                            u1AttrInfo =
                                pMapPortEntry->pu1AttrInfoList
                                [pMrpAttrEntry->u2AttrIndex];

                            /*If the  App SEM state  is VO and Reg SEM 
                             * state in MT skip that attribute*/
                            if ((MRP_GET_ATTR_APP_SEM_STATE (u1AttrInfo)
                                 == MRP_VO) &&
                                (MRP_GET_ATTR_REG_SEM_STATE (u1AttrInfo)
                                 == MRP_MT))
                            {

                                pMrpAttrEntry = (tMrpAttrEntry *)
                                    RBTreeGetNext
                                    (pMrpAppEntry->MapAttrTable,
                                     pMrpAttrEntry, NULL);

                                continue;
                            }

                            /*Fill up the values to be returned */
                            *pu4NextIeee8021BridgeBasePortComponentId
                                = u4ContextId;

                            *pu4NextIeee8021BridgeBasePort = u2PortId;

                            if ((pMrpAttrEntry->Attr.u1AttrLen ==
                                 MRP_SERVICE_REQ_LEN)
                                || (pMrpAttrEntry->Attr.u1AttrLen ==
                                    MRP_MAC_ADDR_LEN))
                            {
                                MEMCPY (*pNextFsMrpApplicationAddress,
                                        gMmrpAddr, MRP_MAC_ADDR_LEN);
                            }
                            else
                            {
                                MEMCPY (*pNextFsMrpApplicationAddress,
                                        pMrpContextInfo->MvrpAddr,
                                        MRP_MAC_ADDR_LEN);
                            }
                            *pi4NextFsMrpSEMMapContext =
                                pMrpAttrEntry->pMapEntry->u2MapId;
                            *pi4NextFsMrpAttributeType =
                                pMrpAttrEntry->Attr.u1AttrType;

                            pNextFsMrpSEMAttributeValue->i4_Length =
                                pMrpAttrEntry->Attr.u1AttrLen;

                            MEMSET
                                (pNextFsMrpSEMAttributeValue->pu1_OctetList,
                                 0,
                                 (size_t) pNextFsMrpSEMAttributeValue->
                                 i4_Length);

                            MEMCPY
                                (pNextFsMrpSEMAttributeValue->pu1_OctetList,
                                 pMrpAttrEntry->Attr.au1AttrVal,
                                 MEM_MAX_BYTES
                                 (pMrpAttrEntry->Attr.u1AttrLen,
                                  MRP_MAX_ATTR_LEN));

                            return SNMP_SUCCESS;

                        }        /* if (pMapPortEntry != NULL) */

                    }
                    while (pMrpAttrEntry);

                }                /*MapId Loop */

                /* Reinitialize the MapId once the map loop is over
                 * Also resetthe attrEntry*/
                u2MapIdTmp = 0;
                MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));

            }                    /* Application Loop */

        }                        /* Port Loop */

    }                            /* Context Loop */

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMrpSEMApplicantState
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpSEMMapContext
                FsMrpAttributeType
                FsMrpSEMAttributeValue

                The Object 
                retValFsMrpSEMApplicantState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpSEMApplicantState (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              tMacAddr FsMrpApplicationAddress,
                              INT4 i4FsMrpSEMMapContext,
                              INT4 i4FsMrpAttributeType,
                              tSNMP_OCTET_STRING_TYPE * pFsMrpSEMAttributeValue,
                              INT4 *pi4RetValFsMrpSEMApplicantState)
{
    tMrpAttrEntry       AttrEntry;
    tMrpAttrEntry      *pMrpAttrEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpAppEntry       *pMrpAppEntry = NULL;
    UINT4               u4PortId = u4Ieee8021BridgeBasePort;
    UINT1               u1AttrInfo = MRP_INIT_VAL;
    UINT1               u1AppId = 0;
    UINT2               u2MapId = (UINT2) i4FsMrpSEMMapContext;

    MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, u1AppId);

    if (i4FsMrpSEMMapContext >= pMrpAppEntry->u2MapArraySize)
    {
        return SNMP_FAILURE;
    }

    pMrpMapEntry = MRP_GET_MAP_ENTRY (pMrpAppEntry, u2MapId);

    if (pMrpMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    AttrEntry.Attr.u1AttrLen = (UINT1) pFsMrpSEMAttributeValue->i4_Length;

    MEMSET (AttrEntry.Attr.au1AttrVal, 0, MRP_MAX_ATTR_LEN);

    MEMCPY (AttrEntry.Attr.au1AttrVal, pFsMrpSEMAttributeValue->pu1_OctetList,
            pFsMrpSEMAttributeValue->i4_Length);

    AttrEntry.Attr.u1AttrType = (UINT1) i4FsMrpAttributeType;

    AttrEntry.pMapEntry = pMrpMapEntry;

    AttrEntry.pMapEntry->pMrpAppEntry = pMrpAppEntry;

    pMrpAttrEntry = (tMrpAttrEntry *) RBTreeGet
        (pMrpAppEntry->MapAttrTable, &AttrEntry);

    if (pMrpAttrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, (UINT2) u4PortId);

    if (pMrpMapPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1AttrInfo = pMrpMapPortEntry->pu1AttrInfoList[pMrpAttrEntry->u2AttrIndex];

    *pi4RetValFsMrpSEMApplicantState = MRP_GET_ATTR_APP_SEM_STATE (u1AttrInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpSEMRegistrarState
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpSEMMapContext
                FsMrpAttributeType
                FsMrpSEMAttributeValue

                The Object 
                retValFsMrpSEMRegistrarState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpSEMRegistrarState (UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              tMacAddr FsMrpApplicationAddress,
                              INT4 i4FsMrpSEMMapContext,
                              INT4 i4FsMrpAttributeType,
                              tSNMP_OCTET_STRING_TYPE * pFsMrpSEMAttributeValue,
                              INT4 *pi4RetValFsMrpSEMRegistrarState)
{
    tMrpAttrEntry       AttrEntry;
    tMrpAttrEntry      *pMrpAttrEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpAppEntry       *pMrpAppEntry = NULL;
    UINT4               u4PortId = u4Ieee8021BridgeBasePort;
    UINT1               u1AttrInfo = MRP_INIT_VAL;
    UINT1               u1AppId = 0;
    UINT2               u2MapId = (UINT2) i4FsMrpSEMMapContext;

    MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);
    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, u1AppId);

    if (i4FsMrpSEMMapContext >= pMrpAppEntry->u2MapArraySize)
    {
        return SNMP_FAILURE;
    }

    pMrpMapEntry = MRP_GET_MAP_ENTRY (pMrpAppEntry, u2MapId);

    if (pMrpMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (AttrEntry.Attr.au1AttrVal, 0, MRP_MAX_ATTR_LEN);

    MEMCPY (AttrEntry.Attr.au1AttrVal, pFsMrpSEMAttributeValue->pu1_OctetList,
            pFsMrpSEMAttributeValue->i4_Length);

    AttrEntry.Attr.u1AttrType = (UINT1) i4FsMrpAttributeType;

    AttrEntry.pMapEntry = pMrpMapEntry;

    AttrEntry.Attr.u1AttrLen = (UINT1) pFsMrpSEMAttributeValue->i4_Length;

    AttrEntry.pMapEntry->pMrpAppEntry = pMrpAppEntry;

    pMrpAttrEntry = (tMrpAttrEntry *) RBTreeGet
        (pMrpAppEntry->MapAttrTable, &AttrEntry);
    if (pMrpAttrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, u4PortId);

    if (pMrpMapPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    u1AttrInfo = pMrpMapPortEntry->pu1AttrInfoList[pMrpAttrEntry->u2AttrIndex];

    *pi4RetValFsMrpSEMRegistrarState = MRP_GET_ATTR_REG_SEM_STATE (u1AttrInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMrpSEMOriginatorAddress
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                FsMrpApplicationAddress
                FsMrpSEMMapContext
                FsMrpAttributeType
                FsMrpSEMAttributeValue

                The Object 
                retValFsMrpSEMOriginatorAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMrpSEMOriginatorAddress (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                 UINT4 u4Ieee8021BridgeBasePort,
                                 tMacAddr FsMrpApplicationAddress,
                                 INT4 i4FsMrpSEMMapContext,
                                 INT4 i4FsMrpAttributeType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMrpSEMAttributeValue,
                                 tMacAddr * pRetValFsMrpSEMOriginatorAddress)
{

    tMrpAttrEntry       AttrEntry;
    tMrpAttrEntry      *pMrpAttrEntry = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpAppEntry       *pMrpAppEntry = NULL;
    UINT4               u4PortId = u4Ieee8021BridgeBasePort;
    UINT1               u1AppId = 0;
    UINT2               u2MapId = (UINT2) i4FsMrpSEMMapContext;

    MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MrpAppGetAppIdFromAppAddr (u4Ieee8021BridgeBasePortComponentId,
                               FsMrpApplicationAddress, &u1AppId);

    if ((u1AppId == 0) || (u1AppId > MRP_MAX_APPS))
    {
        return SNMP_FAILURE;
    }

    pMrpAppEntry = MRP_GET_APP_ENTRY (pMrpContextInfo, u1AppId);

    if (i4FsMrpSEMMapContext >= pMrpAppEntry->u2MapArraySize)
    {
        return SNMP_FAILURE;
    }

    pMrpMapEntry = MRP_GET_MAP_ENTRY (pMrpAppEntry, u2MapId);

    if (pMrpMapEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (AttrEntry.Attr.au1AttrVal, 0, MRP_MAX_ATTR_LEN);

    MEMCPY (AttrEntry.Attr.au1AttrVal, pFsMrpSEMAttributeValue->pu1_OctetList,
            pFsMrpSEMAttributeValue->i4_Length);

    AttrEntry.Attr.u1AttrType = (UINT1) i4FsMrpAttributeType;

    AttrEntry.pMapEntry = pMrpMapEntry;

    AttrEntry.Attr.u1AttrLen = (UINT1) pFsMrpSEMAttributeValue->i4_Length;

    AttrEntry.pMapEntry->pMrpAppEntry = pMrpAppEntry;

    pMrpAttrEntry = (tMrpAttrEntry *) RBTreeGet
        (pMrpAppEntry->MapAttrTable, &AttrEntry);

    if (pMrpAttrEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pMrpAppEntry, u4PortId);

    if (pMrpAppPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMSET (*pRetValFsMrpSEMOriginatorAddress, 0, sizeof (tMacAddr));
    MEMCPY (*pRetValFsMrpSEMOriginatorAddress,
            pMrpAppPortEntry->pPeerMacAddrList[pMrpAttrEntry->u2AttrIndex],
            MRP_MAC_ADDR_LEN);

    return SNMP_SUCCESS;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  fsmrplw.c                      */
/*-----------------------------------------------------------------------*/

/* Low Level GET Routine for All Objects  */
