/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mrptrc.c,v 1.8 2013/11/22 11:45:25 siva Exp $
 *
 * Description: This file contains debugging related functions
 *****************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *                                                                           
 * Function     : MrpTrcPrint                                               
 *                                                                           
 * Description  :  prints the trace - with filename,line no and Context Name   
 *                                                                          
 * Input        : fname   - File name                                        
 *                u4Line  - Line no                                          
 *                s       - string to be printed                             
 *                                                                           
 * Output       : None                                                       
 *                                                                           
 * Returns      : None                                             
 *                                                                           
 *****************************************************************************/
VOID
MrpTrcPrint (const CHR1 * fname, UINT4 u4Line, CHR1 * s)
{

    tOsixSysTime        sysTime;
    const CHR1         *pc1Fname = fname;
    char                ai1LogMsgBuf[MRP_MAX_LOG_STR_LEN];

    if (s == NULL)
    {
        return;
    }
    MEMSET (ai1LogMsgBuf, 0, MRP_MAX_LOG_STR_LEN);

    while (*fname != '\0')
    {
        if (*fname == '/')
        {
            pc1Fname = (fname + 1);
        }
        fname++;
    }
    OsixGetSysTime (&sysTime);
    SNPRINTF (ai1LogMsgBuf, sizeof (ai1LogMsgBuf), "MRP: %s:%d: %s",
              pc1Fname, u4Line, s);
    UtlTrcPrint ((const char *) ai1LogMsgBuf);
}

/****************************************************************************
 *                                                                           
 * Function     : MrpTrc                                                    
 *                                                                           
 * Description  : converts variable argument in to string depending on flag  
 *                                                                           
 * Input        : u4Flags  - Trace flag                                      
 *                fmt  - format strong, variable argument
 *                                                                           
 * Output       : None                                                       
 *                                                                           
 * Returns      : Returns string                                             
 *                                                                           
 *****************************************************************************/
CHR1               *
MrpTrc (tMrpContextInfo * pMrpContextInfo, UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
    static CHR1         ac1buf[MRP_TRC_BUF_SIZE];
    INT4                i4Len = 0;

    if ((u4Flags & pMrpContextInfo->u4TrcOption) == 0)
    {
        return (NULL);
    }

    MEMSET (ac1buf, 0, MRP_TRC_BUF_SIZE);
    SPRINTF ((char *) ac1buf, "%s : ", pMrpContextInfo->au1ContextName);

    i4Len = (INT4) STRLEN (ac1buf);
    va_start (ap, fmt);
    vsprintf (&ac1buf[i4Len++], fmt, ap);
    va_end (ap);

    return (&ac1buf[0]);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTrcGetTraceOptionValue
 *
 *    DESCRIPTION      : This function process given trace input and sets the
 *                       corresponding option bit.
 *
 *    INPUT            : pu1TraceInput - trace string.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : Option Value.
 *
 ****************************************************************************/
PUBLIC UINT4
MrpTrcGetTraceOptionValue (UINT1 *pu1TraceInput, INT4 i4Tracelen)
{
#define MRP_MAX_TRACE_TOKENS      32
#define MRP_MAX_TRACE_TOKEN_SIZE  15
#define MRP_TRACE_TOKEN_DELIMITER ' '    /* space */

    UINT1
         
                    aau1Tokens[MRP_MAX_TRACE_TOKENS][MRP_MAX_TRACE_TOKEN_SIZE];
    UINT1              *apu1Tokens[MRP_MAX_TRACE_TOKENS];
    UINT1               u1Count = 0;
    UINT1               u1TokenCount = 0;
    UINT4               u4TraceOption = MRP_INVALID_TRC;

    MEMSET (aau1Tokens, 0, sizeof (aau1Tokens));

    /* Enable */
    if (STRNCMP (pu1TraceInput, "enable ", STRLEN ("enable ")) == 0)
    {
        pu1TraceInput += STRLEN ("enable ");
        i4Tracelen -= (INT4) STRLEN ("enable ");
    }
    /* Disable */
    else if (STRNCMP (pu1TraceInput, "disable ", STRLEN ("disable ")) == 0)
    {
        pu1TraceInput += STRLEN ("disable ");
        i4Tracelen -= (INT4) STRLEN ("disable ");
    }
    else
    {
        return MRP_INVALID_TRC;
    }

    /* assign memory address for all the pointers in token array(apu1Tokens) */
    for (u1Count = 0; u1Count < MRP_MAX_TRACE_TOKENS; u1Count++)
    {
        apu1Tokens[u1Count] = aau1Tokens[u1Count];
    }

    /* get the tokens from the trace input buffer */
    MrpTrcSplitStrToTokens (pu1TraceInput, i4Tracelen,
                            (UINT1) MRP_TRACE_TOKEN_DELIMITER,
                            (UINT2) MRP_MAX_TRACE_TOKENS, apu1Tokens,
                            &u1TokenCount);
    /* get tokens one by one from the token array and set the
     * trace options based on the tokens */
    for (u1Count = 0; ((u1Count < u1TokenCount) &&
                       (u1Count < MRP_MAX_TRACE_TOKENS)); u1Count++)
    {
        /* set the trace option based on the give token */
        MrpTrcSetTraceOption (apu1Tokens[u1Count], &u4TraceOption);
        /* if invalid trace option is returned by the function
         * MrpUtilSetTraceOption, then dont continue the for loop, just
         * return with invalid trace option */
        if (u4TraceOption == MRP_INVALID_TRC)
        {
            return u4TraceOption;
        }
    }
    return u4TraceOption;
}

/******************************************************************************
 * Function Name      : MrpTrcSetTraceOption
 *
 * Description        : This function sets the trace option based on the given
 *                      trace token
 *
 * Input(s)           : apu1Token      - Poiner to token array
 *                      pu4TraceOption - Pointer to trace option
 *
 * Output(s)          : pu4TraceOption - Pointer to trace option
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
MrpTrcSetTraceOption (UINT1 *pu1Token, UINT4 *pu4TraceOption)
{
    if (STRNCMP (pu1Token, "protocol", STRLEN ("protocol")) == 0)
    {
        *pu4TraceOption |= MRP_PROTOCOL_TRC;
    }
    else if (STRNCMP (pu1Token, "mvrp", STRLEN ("mvrp")) == 0)
    {
        *pu4TraceOption |= MRP_MVRP_TRC;
    }
    else if (STRNCMP (pu1Token, "mmrp", STRLEN ("mmrp")) == 0)
    {
        *pu4TraceOption |= MRP_MMRP_TRC;
    }
    else if (STRNCMP (pu1Token, "init-shut", STRLEN ("init-shut")) == 0)
    {
        *pu4TraceOption |= INIT_SHUT_TRC;
    }
    else if (STRNCMP (pu1Token, "mgmt", STRLEN ("mgmt")) == 0)
    {
        *pu4TraceOption |= MGMT_TRC;
    }
    else if (STRNCMP (pu1Token, "data-path", STRLEN ("data-path")) == 0)
    {
        *pu4TraceOption |= DATA_PATH_TRC;
    }
    else if (STRNCMP (pu1Token, "ctrl", STRLEN ("ctrl")) == 0)
    {
        *pu4TraceOption |= CONTROL_PLANE_TRC;
    }
    else if (STRNCMP (pu1Token, "pkt-dump", STRLEN ("pkt-dump")) == 0)
    {
        *pu4TraceOption |= DUMP_TRC;
    }
    else if (STRNCMP (pu1Token, "resource", STRLEN ("resource")) == 0)
    {
        *pu4TraceOption |= OS_RESOURCE_TRC;
    }
    else if (STRNCMP (pu1Token, "all-fail", STRLEN ("all-fail")) == 0)
    {
        *pu4TraceOption |= ALL_FAILURE_TRC;
    }
    else if (STRNCMP (pu1Token, "all", STRLEN ("all")) == 0)
    {
        *pu4TraceOption |= MRP_ALL_TRC;
    }
    else if (STRNCMP (pu1Token, "buffer", STRLEN ("buffer")) == 0)
    {
        *pu4TraceOption |= BUFFER_TRC;
    }
    else if (STRNCMP (pu1Token, "critical", STRLEN ("critical")) == 0)
    {
        *pu4TraceOption |= MRP_CRITICAL_TRC;
    }
    else if (STRNCMP (pu1Token, "applicant-sem", STRLEN ("applicant-sem")) == 0)
    {
        *pu4TraceOption |= MRP_APP_SEM_TRC;
    }
    else if (STRNCMP (pu1Token, "registrar-sem", STRLEN ("registrar-sem")) == 0)
    {
        *pu4TraceOption |= MRP_REG_SEM_TRC;
    }
    else if (STRNCMP (pu1Token, "leaveall-sem", STRLEN ("leaveall-sem")) == 0)
    {
        *pu4TraceOption |= MRP_LEAVEALL_SEM_TRC;
    }
    else if (STRNCMP (pu1Token, "periodic-sem", STRLEN ("periodic-sem")) == 0)
    {
        *pu4TraceOption |= MRP_PERIODIC_SEM_TRC;
    }
    else if (STRNCMP (pu1Token, "mrpdu", STRLEN ("mrpdu")) == 0)
    {
        *pu4TraceOption |= MRP_PDU_TRC;
    }
    else
    {
        *pu4TraceOption = MRP_INVALID_TRC;
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTrcGetTraceInputValue
 *
 *    DESCRIPTION      : This function gives the trace string depending upon the
 *                       trace option.
 *
 *    INPUT            : pu1TraceInput - trace string.
 *                       u4TraceOption  - trace option.
 *
 *    OUTPUT           : Gives the string format trace option.
 *
 *    RETURNS          : trace string length.
 *
 ****************************************************************************/

PUBLIC UINT4
MrpTrcGetTraceInputValue (UINT1 *pu1TraceInput, UINT4 u4TraceOption)
{

    UINT1               u1BitNumber = 0;
    UINT4               u4TrcLen = 0;
    UINT1               u1IsFirstTrcInputStr = OSIX_TRUE;

    /* trace string */
    UINT1               au1TraceString[MRP_MAX_TRC_OPTIONS][MRP_MAX_TRC_STR_LEN]
        = { " init-shut", " mgmt", " data-path", " ctrl", " pkt-dump",
        " resource", " all-fail", " buffer", " protocol", " mvrp",
        " mmrp", " critical", " applicant-sem", " registrar-sem",
        " leaveall-sem", " periodic-sem", " mrpdu", " all"
    };

    for (u1BitNumber = 0; u1BitNumber < MRP_MAX_TRC_OPTIONS; u1BitNumber++)
    {
        if ((u4TraceOption >> u1BitNumber) & OSIX_TRUE)
        {
            /* if it is the first trace input string being copied to the
             * buffer, then " "(space) is not required in the beginning of the
             * string, so copy from 2nd byte of the string */
            if (u1IsFirstTrcInputStr == OSIX_TRUE)
            {
                STRNCPY (pu1TraceInput, (au1TraceString[u1BitNumber] + 1),
                         (STRLEN (au1TraceString[u1BitNumber]) - 1));
                pu1TraceInput += (STRLEN (au1TraceString[u1BitNumber]) - 1);

                u4TrcLen += (STRLEN (au1TraceString[u1BitNumber]) - 1);
                u1IsFirstTrcInputStr = OSIX_FALSE;
                continue;
            }

            STRNCPY (pu1TraceInput, au1TraceString[u1BitNumber],
                     STRLEN (au1TraceString[u1BitNumber]));
            pu1TraceInput += STRLEN (au1TraceString[u1BitNumber]);
            u4TrcLen += STRLEN (au1TraceString[u1BitNumber]);
        }
    }

    return u4TrcLen;
}

/******************************************************************************
 * Function Name      : MrpTrcSplitStrToTokens
 *
 * Description        : This function splits the given string into tokens
 *                      based on the given token delimiter and stores the
 *                      tokens in token array and returns the token array.
 *
 * Input(s)           : pu1InputStr  - Pointer to input string
 *                      i4Strlen     - String length
 *                      u1Delimiter  - Delimiter by which the token has to be
 *                                     seperated
 *                      u2MaxToken    - Max token count
 *                      apu1Token     - Poiner to token array
 *                      pu1TokenCount - Token count
 *
 * Output(s)          : apu1Token     - Poiner to token array
 *                      pu1TokenCount - Token count
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
MrpTrcSplitStrToTokens (UINT1 *pu1InputStr, INT4 i4Strlen,
                        UINT1 u1Delimiter, UINT2 u2MaxToken,
                        UINT1 *apu1Token[], UINT1 *pu1TokenCount)
{
    UINT1               u1TokenSize = 0;
    UINT1               u1TokenCount = 0;

    while ((i4Strlen > 0) && (u1TokenCount < u2MaxToken))
    {
        /* reset token size to zero */
        u1TokenSize = 0;
        /* scan for delimiter */
        while ((i4Strlen > 0) && (*(pu1InputStr + u1TokenSize) != u1Delimiter))
        {
            i4Strlen--;
            u1TokenSize++;
        }
        /* since the loop breaks whenever delimiter is found the string length
         * and token size shoud be updated once(outside the loop) by the size
         * of delimiter(which is 1byte) */
        i4Strlen--;
        u1TokenSize++;
        /* copy the token excluding delimiter */
        STRNCPY (apu1Token[u1TokenCount], pu1InputStr,
                 (u1TokenSize - sizeof (u1Delimiter)));
        /* move the input string pointer to point to next token */
        pu1InputStr += u1TokenSize;
        /* increment the token count */
        u1TokenCount++;
    }
    /* update the token count */
    *pu1TokenCount = u1TokenCount;
    return;
}

/*************************************************************************
 * Function Name       : MrpTrcGblTrace
 *
 * Description         : This function prints the trace message, the actual
 *                       value of u4Mask will be ignore and u4Mask is set to 1,
 *                       and will be AND with  MRP_GLOBAL_TRACE_OPTION
 *                       to decide to print the buffer or not.
 *
 * Input(s)            : u4Mask - Which path trcaes needs to be enabled
 *                                 , UNUSED
 *                       Fmt - print string
 *
 * Output(s)           : None
 *
 *************************************************************************/
VOID
MrpTrcGblTrace (UINT4 u4Mask, const char *Fmt, ...)
{
    va_list             ap;
    static CHR1         ac1Str[MRP_TRC_BUF_SIZE];
    UINT4               u4Dummy = MRP_ENABLED;

    if (gMrpGlobalInfo.GlobalTrcOption == MRP_DISABLED)
    {
        return;
    }

    va_start (ap, Fmt);
    vsprintf (&ac1Str[0], Fmt, ap);
    va_end (ap);

    /* This DUMP trc for print Attribute  Informations. */

    if (DUMP_TRC == u4Mask)
    {
        UtlTrcLog (gMrpGlobalInfo.GlobalTrcOption, u4Dummy, "", ac1Str);
    }
    else
    {

        UtlTrcLog (gMrpGlobalInfo.GlobalTrcOption, u4Dummy, MRP_NAME, ac1Str);
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME       : MrpTrcPrintPortList
 *
 *    DESCRIPTION         : This function prints the port numbers set in the
 *                          given port list other than the 'u2InPort'.
 *
 *    INPUT(s)            : u4ContextId - ContextId
 *                          u4ModId - Module Id
 *                          PortList - Ports to be printed.
 *                          u2InPort - The Port which must not be printed.
 *
 *    OUTPUT(s)           : None
 *
 *    RETURNS            : None
 *
 *****************************************************************************/
VOID
MrpTrcPrintPortList (UINT4 u4ContextId, UINT4 u4ModId, tLocalPortList PortList,
                     UINT2 u2InPort)
{
    tMrpPortEntry      *pPortEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1Flag = (UINT1) OSIX_FALSE;

    if (((MRP_CONTEXT_PTR (u4ContextId)->u4TrcOption) & u4ModId) == 0)
    {
        return;
    }

    for (u2ByteInd = 0; u2ByteInd < sizeof (tLocalPortList); u2ByteInd++)
    {

        if (PortList[u2ByteInd] != 0)
        {

            u1PortFlag = PortList[u2ByteInd];

            for (u2BitIndex = 0;
                 ((u2BitIndex < VLAN_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {

                if ((u1PortFlag & VLAN_BIT8) != 0)
                {

                    u2Port =
                        (UINT2) ((u2ByteInd * VLAN_PORTS_PER_BYTE) +
                                 u2BitIndex + 1);

                    pPortEntry =
                        MRP_GET_PORT_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                            u2Port);

                    if ((pPortEntry != NULL) && (u2Port != u2InPort))
                    {

                        u1Flag = OSIX_TRUE;
                        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                                  DATA_PATH_TRC, "%d ", pPortEntry->u4IfIndex));
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    if (u1Flag == OSIX_FALSE)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), DATA_PATH_TRC,
                  "NULL Port List\n"));
    }
    else
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), DATA_PATH_TRC, "\n"));
    }
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME       : MrpTrcPrintAttribute                               
 *                                                                           
 *    DESCRIPTION         : This function prints the given Attribute.        
 *                                                                           
 *    INPUT(s)            : pAttr - Pointer to the Attribute structure.      
 *                                                                           
 *    OUTPUT(s)           : None                                             
 *                                                                           
 *    RETURNS            : None                                              
 *                                                                           
 *****************************************************************************/
VOID
MrpTrcPrintAttribute (tMrpAttr * pAttr)
{
    UINT4               u4Index = 0;

    MRP_ATTR_TRC (DUMP_TRC, MRP_NAME, "\t\tAttr Type  = 0x%x\n",
                  pAttr->u1AttrType);

    if (pAttr->u1AttrLen != 0)
    {

        MRP_GBL_TRC (DUMP_TRC, "\t\tAttr Value = ");

        for (u4Index = 0;
             u4Index <
             (UINT4) (MEM_MAX_BYTES (pAttr->u1AttrLen, MRP_MAX_ATTR_LEN));
             u4Index++)
        {
            MRP_ATTR_TRC (DUMP_TRC,
                          MRP_NAME, "0x%x ", pAttr->au1AttrVal[u4Index]);
        }

        MRP_ATTR_TRC (DUMP_TRC, MRP_NAME, "", "\n");
    }
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME       : MrpTrcPrintMacAddr                                 
 *                                                                           
 *    DESCRIPTION         : This function prints the given mac address.      
 *                                                                           
 *    INPUT(s)            : u4ModId - Module identifier.                     
 *                          pMacAddr - Pointer to the mac address to be      
 *                          printed.                                         
 *                                                                           
 *    OUTPUT(s)           : None                                             
 *                                                                           
 *    RETURNS            : None                                              
 *                                                                           
 *****************************************************************************/
VOID
MrpTrcPrintMacAddr (UINT4 u4ContextId, UINT4 u4ModId, tMacAddr MacAddr)
{
    UINT1               u1MacLen = 0;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (MacAddr);
    if (((MRP_CONTEXT_PTR (u4ContextId)->u4TrcOption) & u4ModId) == 0)
    {
        return;
    }

    for (u1MacLen = 0; u1MacLen < MRP_MAC_ADDR_LEN; u1MacLen++)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | DATA_PATH_TRC),
                  "%x", MacAddr[u1MacLen]));

        if (u1MacLen != MRP_MAC_ADDR_LEN - 1)
        {

            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      CONTROL_PLANE_TRC | DATA_PATH_TRC, ":"));
        }
    }

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              CONTROL_PLANE_TRC | DATA_PATH_TRC, "\n"));
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrptrc.c                       */
/*-----------------------------------------------------------------------*/
