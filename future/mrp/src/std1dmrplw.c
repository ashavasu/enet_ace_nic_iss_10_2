/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1dmrplw.c,v 1.2 2013/06/25 12:08:38 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "mrpinc.h"

/* LOW LEVEL Routines for Table : Ieee8021BridgePort */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortMrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
.p***************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortMrpTable (UINT4
                                                    u4Ieee8021BridgeBasePortComponentId,
                                                    UINT4
                                                    u4Ieee8021BridgeBasePort)
{
    return (nmhValidateIndexInstanceFsMrpPortTable
            (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortMrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortMrpTable (UINT4
                                            *pu4Ieee8021BridgeBasePortComponentId,
                                            UINT4 *pu4Ieee8021BridgeBasePort)
{

    return (nmhGetFirstIndexFsMrpPortTable
            (pu4Ieee8021BridgeBasePortComponentId, pu4Ieee8021BridgeBasePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortMrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortMrpTable (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4
                                           *pu4NextIeee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           UINT4 *pu4NextIeee8021BridgeBasePort)
{
    return (nmhGetNextIndexFsMrpPortTable
            (u4Ieee8021BridgeBasePortComponentId,
             pu4NextIeee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort,
             pu4NextIeee8021BridgeBasePort));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMrpJoinTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMrpJoinTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMrpJoinTime (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     INT4
                                     *pi4RetValIeee8021BridgePortMrpJoinTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgePortMrpJoinTime = (INT4) pMrpPortEntry->u4JoinTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMrpLeaveTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMrpLeaveTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMrpLeaveTime (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      INT4
                                      *pi4RetValIeee8021BridgePortMrpLeaveTime)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgePortMrpLeaveTime =
        (INT4) pMrpPortEntry->u4LeaveTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMrpLeaveAllTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMrpLeaveAllTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMrpLeaveAllTime (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         INT4
                                         *pi4RetValIeee8021BridgePortMrpLeaveAllTime)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgePortMrpLeaveAllTime =
        (INT4) pMrpPortEntry->u4LeaveAllTime;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortMrpJoinTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortMrpJoinTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortMrpJoinTime (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                     UINT4 u4Ieee8021BridgeBasePort,
                                     INT4 i4SetValIeee8021BridgePortMrpJoinTime)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u4JoinTime = (UINT4) i4SetValIeee8021BridgePortMrpJoinTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortMrpLeaveTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortMrpLeaveTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortMrpLeaveTime (UINT4 u4Ieee8021BridgeBasePortComponentId,
                                      UINT4 u4Ieee8021BridgeBasePort,
                                      INT4
                                      i4SetValIeee8021BridgePortMrpLeaveTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u4LeaveTime = (UINT4) i4SetValIeee8021BridgePortMrpLeaveTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortMrpLeaveAllTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortMrpLeaveAllTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortMrpLeaveAllTime (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         INT4
                                         i4SetValIeee8021BridgePortMrpLeaveAllTime)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u4LeaveAllTime =
        (UINT4) i4SetValIeee8021BridgePortMrpLeaveAllTime;

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortMrpJoinTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortMrpJoinTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortMrpJoinTime (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4Ieee8021BridgeBasePortComponentId,
                                        UINT4 u4Ieee8021BridgeBasePort,
                                        INT4
                                        i4TestValIeee8021BridgePortMrpJoinTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        (UINT2) u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if (MRP_IS_JOIN_TIME_VALID ((UINT4) i4TestValIeee8021BridgePortMrpJoinTime,
                                pMrpPortEntry->u4LeaveTime) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_TIMER_VAL_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortMrpLeaveTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortMrpLeaveTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortMrpLeaveTime (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         INT4
                                         i4TestValIeee8021BridgePortMrpLeaveTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if (MRP_IS_LEAVE_TIME_VALID ((UINT4)
                                 i4TestValIeee8021BridgePortMrpLeaveTime,
                                 pMrpPortEntry->u4LeaveAllTime,
                                 pMrpPortEntry->u4JoinTime) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortMrpLeaveAllTime
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortMrpLeaveAllTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortMrpLeaveAllTime (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4TestValIeee8021BridgePortMrpLeaveAllTime)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry =
        MRP_GET_PORT_ENTRY (pMrpContextInfo, u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
        (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_CONFIG_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if (MRP_IS_LEAVE_ALL_TIME_VALID ((UINT4)
                                     i4TestValIeee8021BridgePortMrpLeaveAllTime,
                                     pMrpPortEntry->u4LeaveTime) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgePortMrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgePortMrpTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021BridgePortMmrpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021BridgePortMmrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021BridgePortMmrpTable (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort)
{
    return (nmhValidateIndexInstanceFsMrpPortTable
            (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021BridgePortMmrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021BridgePortMmrpTable (UINT4
                                             *pu4Ieee8021BridgeBasePortComponentId,
                                             UINT4 *pu4Ieee8021BridgeBasePort)
{
    return (nmhGetFirstIndexFsMrpPortTable
            (pu4Ieee8021BridgeBasePortComponentId, pu4Ieee8021BridgeBasePort));

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021BridgePortMmrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021BridgePortMmrpTable (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4
                                            *pu4NextIeee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            UINT4
                                            *pu4NextIeee8021BridgeBasePort)
{
    return (nmhGetNextIndexFsMrpPortTable
            (u4Ieee8021BridgeBasePortComponentId,
             pu4NextIeee8021BridgeBasePortComponentId,
             u4Ieee8021BridgeBasePort, pu4NextIeee8021BridgeBasePort));

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMmrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMmrpEnabledStatus (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           *pi4RetValIeee8021BridgePortMmrpEnabledStatus)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgePortMmrpEnabledStatus =
        pMrpPortEntry->u1PortMmrpStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMmrpFailedRegistrations
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMmrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMmrpFailedRegistrations (UINT4
                                                 u4Ieee8021BridgeBasePortComponentId,
                                                 UINT4 u4Ieee8021BridgeBasePort,
                                                 tSNMP_COUNTER64_TYPE *
                                                 pu8RetValIeee8021BridgePortMmrpFailedRegistrations)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValIeee8021BridgePortMmrpFailedRegistrations->lsn =
        pMrpPortEntry->u4MmrpRegFailCnt;
    pu8RetValIeee8021BridgePortMmrpFailedRegistrations->msn = 0;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortMmrpLastPduOrigin
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortMmrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortMmrpLastPduOrigin (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           tMacAddr *
                                           pRetValIeee8021BridgePortMmrpLastPduOrigin)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValIeee8021BridgePortMmrpLastPduOrigin,
            pMrpPortEntry->LastMmrpPduOrigin, MRP_MAC_ADDR_LEN);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021BridgePortRestrictedGroupRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021BridgePortRestrictedGroupRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021BridgePortRestrictedGroupRegistration (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     INT4
                                                     *pi4RetValIeee8021BridgePortRestrictedGroupRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021BridgePortRestrictedGroupRegistration =
        pMrpPortEntry->u1RestrictedMACRegCtrl;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortMmrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortMmrpEnabledStatus (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4SetValIeee8021BridgePortMmrpEnabledStatus)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pMrpPortEntry->u1PortMmrpStatus ==
        i4SetValIeee8021BridgePortMmrpEnabledStatus)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValIeee8021BridgePortMmrpEnabledStatus)
    {

        case MRP_ENABLED:

            if (MrpMmrpEnablePort (pMrpContextInfo,
                                   (UINT2) u4Ieee8021BridgeBasePort) ==
                OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            break;

        case MRP_DISABLED:

            if (MrpMmrpDisablePort (pMrpContextInfo,
                                    (UINT2) u4Ieee8021BridgeBasePort) ==
                OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }

            break;

        default:
            break;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIeee8021BridgePortRestrictedGroupRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021BridgePortRestrictedGroupRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021BridgePortRestrictedGroupRegistration (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     INT4
                                                     i4SetValIeee8021BridgePortRestrictedGroupRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u1RestrictedMACRegCtrl =
        (UINT1) i4SetValIeee8021BridgePortRestrictedGroupRegistration;
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortMmrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortMmrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortMmrpEnabledStatus (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4TestValIeee8021BridgePortMmrpEnabledStatus)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1TunnelStatus = VLAN_TUNNEL_PROTOCOL_INVALID;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry =
        MRP_GET_PORT_ENTRY (pMrpContextInfo, u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021BridgePortMmrpEnabledStatus != MRP_ENABLED) &&
        (i4TestValIeee8021BridgePortMmrpEnabledStatus != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    if (i4TestValIeee8021BridgePortMmrpEnabledStatus == MRP_ENABLED)
    {
        if (pMrpContextInfo->u4BridgeMode == MRP_PROVIDER_BRIDGE_MODE)
        {
            MrpPortGetPortVlanTunnelStatus (pMrpPortEntry->u4IfIndex,
                                            (BOOL1 *) & u1TunnelStatus);

            if (u1TunnelStatus == OSIX_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_MMRP_PROTO_ENABLED_ERR);
                return SNMP_FAILURE;
            }
        }

        if (pMrpContextInfo->u4BridgeMode == MRP_CUSTOMER_BRIDGE_MODE)
        {
            MrpPortGetProtoTunelStatusOnPort (pMrpPortEntry->u4IfIndex,
                                              L2_PROTO_MMRP, &u1TunnelStatus);

            if ((u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
                (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_MMRP_TUNNEL_ENABLED_ERR);
                return SNMP_FAILURE;
            }
        }

        if ((MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
            (MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))

        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_PORT_MMRP_ENABLED_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021BridgePortRestrictedGroupRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021BridgePortRestrictedGroupRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021BridgePortRestrictedGroupRegistration (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePort,
                                                        INT4
                                                        i4TestValIeee8021BridgePortRestrictedGroupRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021BridgePortRestrictedGroupRegistration !=
         MRP_ENABLED)
        && (i4TestValIeee8021BridgePortRestrictedGroupRegistration !=
            MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021BridgePortMmrpTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021BridgePortMmrpTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
