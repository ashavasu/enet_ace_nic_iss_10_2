/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpvlan.c,v 1.2 2009/08/31 09:43:07 prabuc Exp $
 *
 * DESCRIPTION: This file contains the routines for handling the events posted 
 *               by VLAN module
 ******************************************************************************/

#include "mrpinc.h"

/***************************************************************************
 *
 *  FUNCTION NAME   : MrpVlanHandlePropagateVlanInfo                        
 *
 *  DESCRIPTION     : This function is to propagate the static VLAN 
 *                    configured to all other ports in the MAP context.
 *
 *  INPUT(s)        : u4ContextId - Context Identifier
 *                    VlanId      - VLAN
 *                    u2Port      - Port Added, equal to Zero in case 
 *                                  port list is filled.
 *                    AddPortList - Ports to be added                        
 *                    DelPortList - ports to be deleted                      
 *
 *  OUTPUT(s)       :  None                                                 
 *
 *  RETURNS         : None                                                  
 *
 ***************************************************************************/
VOID
MrpVlanHandlePropagateVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                                UINT2 u2Port,
                                tLocalPortList AddPortList,
                                tLocalPortList DelPortList)
{
    tMrpAttr            Attr;
    tMrpAppEntry       *pAppEntry = NULL;
    UINT2               u2MapId = 0;

    if (MrpMvrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MVRP_TRC),
                  "MrpVlanHandlePropagateVlanInfo: MVRP is NOT enabled. "
                  "Cannot Propagate the Vlan Id %d.\n", VlanId));
        return;
    }

    MEMSET (&Attr, 0, sizeof (tMrpAttr));
    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), (CONTROL_PLANE_TRC | MRP_MVRP_TRC),
              "MrpVlanHandlePropagateVlanInfo: Rcvd Propagation req for "
              "Vlan Id %d.\n", VlanId));

    pAppEntry = MRP_GET_APP_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                   MRP_MVRP_APP_ID);

    /* The MST instance to which the VLAN is mapped is
     * the MAP Id of the VLAN.*/
    u2MapId = MrpPortL2IwfMiGetVlanInstMapping (u4ContextId, VlanId);

    Attr.u1AttrType = MRP_VID_ATTR_TYPE;
    Attr.u1AttrLen = MRP_VLAN_ID_LEN;

    VlanId = (tVlanId) OSIX_HTONS (VlanId);

    MEMCPY (Attr.au1AttrVal, &VlanId, MRP_VLAN_ID_LEN);

    /* If u2Port is non zero, then the attribute has been added over single
     * port only. Hence process accordingly
     * */
    if (u2Port != 0)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | MRP_MVRP_TRC),
                  "MrpMvrpHandlePropVlanInfoPerPort: \tAdded Port = %d\n",
                  u2Port));

        MrpMapAppAttributeReqJoinOnPort (pAppEntry, &Attr, u2MapId, u2Port);

        return;
    }

    /* u2Port value is zero. Hence start processing the port list
     * */

    if (MEMCMP (AddPortList, gMrpNullPortList, sizeof (tLocalPortList)) != 0)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | MRP_MVRP_TRC),
                  "MrpVlanHandlePropagateVlanInfo: \tAdded Ports = "));

        MRP_PRINT_PORT_LIST (u4ContextId, MVRP_MOD_TRC, AddPortList);

        MrpMapAppAttributeReqJoin (pAppEntry, &Attr, u2MapId, AddPortList);
    }

    if (MEMCMP (DelPortList, gMrpNullPortList, sizeof (tLocalPortList)) != 0)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | MRP_MVRP_TRC),
                  "MrpVlanHandlePropagateVlanInfo: \tDeleted Ports = "));

        MRP_PRINT_PORT_LIST (u4ContextId, MVRP_MOD_TRC, DelPortList);

        MrpMapAppAttributeReqLeave (pAppEntry, &Attr, u2MapId, DelPortList);

    }
}

/****************************************************************************
 *
 *  FUNCTION NAME   : MrpVlanHandleSetVlanForbiddPorts                        
 *
 *  DESCRIPTION     : This function sets forbidden ports for a specific      
 *                    VlanId                                                
 *
 *  INPUT(s)        : VlanId - VlanId for which Forbidden ports to be updated
 *                    u2Port - Port to be added. Will be Zero if port list is
 *                             valid.
 *                   AddPortList - Ports to be added                        
 *                   DelPortList - ports to be deleted                      
 *
 *  OUTPUT(s)       : None                                                 
 *
 *  RETURNS         : None                                                 
 *
 ****************************************************************************/
VOID
MrpVlanHandleSetVlanForbiddPorts (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT2 u2Port, tLocalPortList AddPortList,
                                  tLocalPortList DelPortList)
{
    tMrpAttr            Attr;
    tMrpAppEntry       *pAppEntry = NULL;
    UINT2               u2MapId = 0;

    if (MrpMvrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MVRP_TRC),
                  "MrpVlanHandleSetVlanForbiddPorts: MVRP is NOT enabled. "
                  "Cannot Set Forbidden Ports for the Vlan Id %d.\n", VlanId));
        return;
    }

    MEMSET (&Attr, 0, sizeof (tMrpAttr));
    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | MRP_MVRP_TRC),
              "MrpVlanHandleSetVlanForbiddPorts: Rcvd Forbidden Ports Set "
              "Req for Vlan Id %d.\n", VlanId));

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | MRP_MVRP_TRC),
              "MrpVlanHandleSetVlanForbiddPorts: \tAdded Ports = "));
    MRP_PRINT_PORT_LIST (u4ContextId, MVRP_MOD_TRC, AddPortList);

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | MRP_MVRP_TRC),
              "MrpVlanHandleSetVlanForbiddPorts: \tDeleted Ports = "));
    MRP_PRINT_PORT_LIST (u4ContextId, MVRP_MOD_TRC, DelPortList);

    pAppEntry = MRP_GET_APP_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                   MRP_MVRP_APP_ID);
    /* The MST instance to which the VLAN is mapped is
     * the MAP Id of the VLAN.*/
    u2MapId = MrpPortL2IwfMiGetVlanInstMapping (u4ContextId, VlanId);

    Attr.u1AttrType = MRP_VID_ATTR_TYPE;
    Attr.u1AttrLen = MRP_VLAN_ID_LEN;

    VlanId = (tVlanId) OSIX_HTONS (VlanId);

    MEMCPY (Attr.au1AttrVal, &VlanId, MRP_VLAN_ID_LEN);

    if (u2Port != 0)
    {
        MrpMapAppSetForbiddenPort (pAppEntry, &Attr, u2MapId, u2Port);
    }
    else
    {
        MrpMapAppSetForbiddenPorts (pAppEntry, &Attr, u2MapId,
                                    AddPortList, DelPortList);
    }
}

/****************************************************************************
 *
 *  FUNCTION NAME   : MrpVlanHandlePropagateMacInfo            
 *
 *  DESCRIPTION     : This function is to propagate the static MAC 
 *                    configured to all other ports in the MAP context.
 *
 *  INPUT           : u4ContextId - Context Identifier
 *                    MacAddr     - Mac Address
 *                    VlanId      - MAP Context
 *                    u2Port      - Port to be added. Will be Zero
 *                                  if valid port list is passed.
 *                    AddPortList - Ports to be added
 *                    DelPortList - ports to be deleted
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : None                                      
 *
 ****************************************************************************/
VOID
MrpVlanHandlePropagateMacInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                               tVlanId VlanId, UINT2 u2Port,
                               tLocalPortList AddPortList,
                               tLocalPortList DelPortList)
{
    tMrpAttr            Attr;
    tMrpAppEntry       *pAppEntry = NULL;

    if (MrpMmrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHandlePropagateMacInfo: MMRP is NOT enabled. "
                  "Cannot Propagate for the Vlan Id %d, the Group Addr ",
                  VlanId));

        MRP_PRINT_MAC_ADDR (u4ContextId, MMRP_MOD_TRC, MacAddr);

        return;
    }

    MEMSET (&Attr, 0, sizeof (tMrpAttr));
    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
              "MrpVlanHandlePropagateMacInfo: Rcvd Propagation Req: "
              "Vlan Id = %d, Group Addr = ", VlanId));

    MRP_PRINT_MAC_ADDR (u4ContextId, MMRP_MOD_TRC, MacAddr);

    pAppEntry = MRP_GET_APP_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                   MRP_MMRP_APP_ID);

    Attr.u1AttrType = MRP_MAC_ADDR_ATTR_TYPE;
    Attr.u1AttrLen = MRP_MAC_ADDR_LEN;

    MEMCPY (Attr.au1AttrVal, MacAddr, MRP_MAC_ADDR_LEN);

    if (u2Port != 0)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHandlePropagateMacInfo: Added Port = %d\n", u2Port));

        MrpMapAppAttributeReqJoinOnPort (pAppEntry, &Attr, VlanId, u2Port);

        return;
    }

    if (MEMCMP (AddPortList, gMrpNullPortList, sizeof (tLocalPortList)) != 0)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHandlePropagateMacInfo: \tAdded Ports = "));

        MRP_PRINT_PORT_LIST (u4ContextId, MMRP_MOD_TRC, AddPortList);

        MrpMapAppAttributeReqJoin (pAppEntry, &Attr, VlanId, AddPortList);
    }

    if (MEMCMP (DelPortList, gMrpNullPortList, sizeof (tLocalPortList)) != 0)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHandlePropagateMacInfo: \tDeleted Ports = "));

        MRP_PRINT_PORT_LIST (u4ContextId, MMRP_MOD_TRC, DelPortList);

        MrpMapAppAttributeReqLeave (pAppEntry, &Attr, VlanId, DelPortList);
    }
}

/****************************************************************************
 * 
 *  FUNCTION NAME   :MrpVlanHandleSetMcastForbidPorts  
 *       
 *  DESCRIPTION     :This function set the forbidden ports for a
 *                       given MAC entry
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                       MacAddr     - Mac Address
 *                       VlanId      - MAP Context
 *                       u2Port      - Port
 *                       AddPortList - Ports to be added
 *                       DelPortList - ports to be deleted
 *                   
 *    OUTPUT           : None
 *  
 *  RETURNS         :None                                       
 *
 ****************************************************************************/
VOID
MrpVlanHandleSetMcastForbidPorts (UINT4 u4ContextId, tMacAddr MacAddr,
                                  tVlanId VlanId, UINT2 u2Port,
                                  tLocalPortList AddPortList,
                                  tLocalPortList DelPortList)
{
    tMrpAttr            Attr;
    tMrpAppEntry       *pAppEntry = NULL;

    if (MrpMmrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHandleSetMcastForbidPorts: MMRP is NOT enabled."
                  " Cannot Set Forbidden Ports for the Vlan Id %d and "
                  "the Mac Addr ", VlanId));

        MRP_PRINT_MAC_ADDR (u4ContextId, MMRP_MOD_TRC, MacAddr);

        return;
    }

    MEMSET (&Attr, 0, sizeof (tMrpAttr));
    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MMRP_TRC),
              "MrpVlanHandleSetMcastForbidPorts: Received Forbidden Ports "
              "Set Request for the Vlan Id %d and the Mac Addr ", VlanId));
    MRP_PRINT_MAC_ADDR (u4ContextId, MMRP_MOD_TRC, MacAddr);

    pAppEntry = MRP_GET_APP_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                   MRP_MMRP_APP_ID);

    if (u2Port != 0)
    {
        MrpMapAppSetForbiddenPort (pAppEntry, &Attr, VlanId, u2Port);

        return;
    }

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), CONTROL_PLANE_TRC,
              "MrpVlanHandleSetMcastForbidPorts: \tAdded Ports = "));
    MRP_PRINT_PORT_LIST (u4ContextId, MMRP_MOD_TRC, AddPortList);

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), CONTROL_PLANE_TRC,
              "MrpVlanHandleSetMcastForbidPorts: \tDeleted Ports = "));
    MRP_PRINT_PORT_LIST (u4ContextId, MMRP_MOD_TRC, DelPortList);

    Attr.u1AttrType = MRP_MAC_ADDR_ATTR_TYPE;
    Attr.u1AttrLen = MRP_MAC_ADDR_LEN;

    MEMCPY (Attr.au1AttrVal, MacAddr, MRP_MAC_ADDR_LEN);

    MrpMapAppSetForbiddenPorts (pAppEntry, &Attr, VlanId,
                                AddPortList, DelPortList);
}

/****************************************************************************
 * 
 *  FUNCTION NAME    : MrpVlanHandlePropagateDefGrpInfo           
 *  
 *  DESCRIPTION      : This function is to propagate the default group 
 *                     info configured to all other ports in the MAP context.
 *                 
 *  INPUT            : u4ContextId - Context Identifier
 *                     u1Type      - MRP_MMRP_ALL_GROUPS/MRP_MMRP_UNREG_GROUPS
 *                     VlanId      - MAP Context
 *                     AddPortList - Ports to be added
 *                     DelPortList - ports to be deleted
 *                 
 *  OUTPUT           : None
 *  
 *  RETURNS          : None                                       
 *
 ****************************************************************************/
VOID
MrpVlanHandlePropagateDefGrpInfo (UINT4 u4ContextId, UINT1 u1Type,
                                  tVlanId VlanId, UINT2 u2Port,
                                  tLocalPortList AddPortList,
                                  tLocalPortList DelPortList)
{
#ifdef VLAN_EXTENDED_FILTER
    tMrpAttr            Attr;
    tMrpAppEntry       *pAppEntry = NULL;

    if (MrpMmrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHandlePropagateDefGrpInfo: MMRP is NOT enabled. "
                  "Cannot Propagate Default Groups (Value = %d) behaviour "
                  "for the Vlan Id %d\n", u1Type, VlanId));
        return;
    }
    MEMSET (&Attr, 0, sizeof (tMrpAttr));
    pAppEntry = MRP_GET_APP_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                   MRP_MMRP_APP_ID);

    Attr.u1AttrType = MRP_SERVICE_REQ_ATTR_TYPE;
    Attr.u1AttrLen = MRP_SERVICE_REQ_LEN;
    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
              "MrpVlanHandlePropagateDefGrpInfo: Rcvd"
              "Propagation Req for Service Req. Attribute Type %d "
              "for Vlan Id = %d\n", u1Type, VlanId));
    Attr.au1AttrVal[0] = u1Type;

    if (u2Port != 0)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHandlePropagateDefGrpInfo:Added Port = %d\n",
                  u2Port));

        MrpMapAppAttributeReqJoinOnPort (pAppEntry, &Attr, VlanId, u2Port);

        return;
    }

    if (MEMCMP (AddPortList, gMrpNullPortList, sizeof (tLocalPortList)) != 0)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHandlePropagateDefGrpInfo: Added Ports = "));

        MRP_PRINT_PORT_LIST (u4ContextId, MMRP_MOD_TRC, AddPortList);

        MrpMapAppAttributeReqJoin (pAppEntry, &Attr, VlanId, AddPortList);
    }

    if (MEMCMP (DelPortList, gMrpNullPortList, sizeof (tLocalPortList)) != 0)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHandlePropagateDefGrpInfo: Deleted Ports = "));
        MRP_PRINT_PORT_LIST (u4ContextId, MMRP_MOD_TRC, DelPortList);

        MrpMapAppAttributeReqLeave (pAppEntry, &Attr, VlanId, DelPortList);
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
#endif /* VLAN_EXTENDED_FILTER */
}

/****************************************************************************
 * 
 *  FUNCTION NAME   : MrpVlanHndleSetDefGrpForbidPorts        
 *  
 *  DESCRIPTION     : This function set the forbidden ports for a  
 *                    given service requirement
 *                 
 *  INPUT           : u4ContextId - Context Identifier
 *                    u1Type      - MRP_MMRP_ALL_GROUPS/MRP_MMRP_UNREG_GROUPS
 *                    VlanId      - MAP Context
 *                    AddPortList - Ports to be added
 *                    DelPortList - ports to be deleted
 *                 
 *  OUTPUT          : None
 *  
 *  RETURNS         : None                                      
 *
 ****************************************************************************/
VOID
MrpVlanHndleSetDefGrpForbidPorts (UINT4 u4ContextId, UINT1 u1Type,
                                  tVlanId VlanId, UINT2 u2Port,
                                  tLocalPortList AddPortList,
                                  tLocalPortList DelPortList)
{
#ifdef VLAN_EXTENDED_FILTER
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpAttr            Attr;

    if (MrpMmrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHndleSetDefGrpForbidPorts: MMRP is NOT enabled."
                  " Cannot Set Default Groups (Value = %d) Forbidden Ports"
                  " for the Vlan Id %d\n", u1Type, VlanId));
        return;
    }

    MEMSET (&Attr, 0, sizeof (tMrpAttr));
    pAppEntry = MRP_GET_APP_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                   MRP_MMRP_APP_ID);

    Attr.u1AttrType = MRP_SERVICE_REQ_ATTR_TYPE;
    Attr.u1AttrLen = MRP_SERVICE_REQ_LEN;
    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
              "MrpVlanHndleSetDefGrpForbidPorts : Rcvd"
              " Forbidden Ports Set Req for Service Req. Attribute Type %d "
              "for Vlan Id = %d\n", u1Type, VlanId));
    Attr.au1AttrVal[0] = u1Type;

    if (u2Port != 0)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
                  "MrpVlanHndleSetDefGrpForbidPorts :Added Ports = %d\n",
                  u2Port));

        MrpMapAppSetForbiddenPort (pAppEntry, &Attr, VlanId, u2Port);

        return;
    }

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
              "MrpVlanHndleSetDefGrpForbidPorts : \tAdded Ports = "));
    MRP_PRINT_PORT_LIST (u4ContextId, MMRP_MOD_TRC, AddPortList);

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | MRP_MMRP_TRC),
              "MrpVlanHndleSetDefGrpForbidPorts : \tDeleted Ports = "));
    MRP_PRINT_PORT_LIST (u4ContextId, MMRP_MOD_TRC, DelPortList);

    MrpMapAppSetForbiddenPorts (pAppEntry, &Attr, VlanId,
                                AddPortList, DelPortList);
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
#endif /* VLAN_EXTENDED_FILTER */
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpvlan.c                      */
/*-----------------------------------------------------------------------*/
