/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: mrprbulk.c,v 1.8 2014/02/13 12:11:35 siva Exp $
 *
 * Description: This file contains routines related to bulk update 
 *              request handling.
 *********************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRBulkSendPortOperStatBulkUpd 
 *
 *    DESCRIPTION      : This funtion constructs the bulk update message
 *                       containing the list of ports that are operationally
 *                       UP.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRBulkSendPortOperStatBulkUpd ()
{
    tRmMsg             *pRmMsg = NULL;
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpPortEntry      *pPortEntry = NULL;
    UINT4               u4ContextId = gMrpGlobalInfo.u2FirstContextId;
    UINT4               u4Offset = 0;
    UINT4               u4PortCntFieldOffset = 0;
    UINT4               u4CurrentTime = 0;
    UINT2               u2NextContextId = 0;
    UINT2               u2NextPortId = 0;
    UINT2               u2Port = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2NumOfPorts = 0;
    UINT1               u1MsgType = MRP_RM_BULK_UPDATE_MSG;
    UINT1               u1BulkUpdtType = MRP_RED_PORT_OPER_STATUS_MSG;
    UINT1               u1IsCtxtIdFilled = OSIX_FALSE;

    /* Port Oper Status Bulk Update Message format */
    /* --------------------------------------------------------------
     * | MsgHdr | BulkMsgType | Context ID | Num of Ports | Port Id  |
     * ----------------------------------------------------------------
     * This message contains the list of ports that are operationally UP.
     * Here Message Len starts from BulkMsgType field.
     */

    for (; u4ContextId != MRP_INIT_VAL; u4ContextId = u2NextContextId)
    {
        pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

        if ((NULL == pContextInfo) ||
            (OSIX_FALSE == MRP_IS_MRP_STARTED (pContextInfo->u4ContextId)))
        {
            continue;
        }

        u2NextContextId = pContextInfo->u2NextContextId;
        u2Port = pContextInfo->u2FirstPortId;

        for (; u2Port != MRP_INVALID_PORT; u2Port = u2NextPortId)
        {
            pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);

            if ((NULL == pPortEntry) ||
                (MRP_OPER_DOWN == pPortEntry->u1OperStatus))
            {
                continue;
            }

            u2NextPortId = pPortEntry->u2NextPortId;

            /* Check whether bulk update request handling needs to be
             * relinquished to process the other events in the MRP Queue.
             */

            u4CurrentTime = OsixGetSysUpTime ();

            if (u4CurrentTime >= gMrpGlobalInfo.MrpRedInfo.u4NextRelinqTime)
            {
                if (NULL != pRmMsg)
                {
                    MRP_RED_PUT_2_BYTES (pRmMsg, u4PortCntFieldOffset,
                                         u2NumOfPorts);
                    u2NumOfPorts = 0;
                    MrpRedPutMsgHdrInRmMsg (pRmMsg, u1MsgType,
                                            (UINT2) u4Offset);
                    MrpRedSendMsgToRm (u1MsgType, (UINT2) u4Offset, pRmMsg);
                    pRmMsg = NULL;
                    u2BufSize = 0;
                    u1IsCtxtIdFilled = OSIX_FALSE;
                    u4Offset = 0;
                }

                MrpRBulkCPURelinInBulkReqHandlng ();

                if (gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt == 0)
                {
                    return OSIX_SUCCESS;
                }
            }

            if ((u2BufSize - u4Offset) <
                MrpRdUtlGetMinBulkUpdMsgLen (u1BulkUpdtType))
            {
                if (NULL != pRmMsg)
                {
                    MRP_RED_PUT_2_BYTES (pRmMsg, u4PortCntFieldOffset,
                                         u2NumOfPorts);
                    u2NumOfPorts = 0;
                    MrpRedPutMsgHdrInRmMsg (pRmMsg, u1MsgType,
                                            (UINT2) u4Offset);
                    MrpRedSendMsgToRm (u1MsgType, (UINT2) u4Offset, pRmMsg);
                    u1IsCtxtIdFilled = OSIX_FALSE;
                    u4Offset = MRP_RED_MSG_HDR_SIZE;
                }

                u2BufSize = MRP_RED_MAX_MSG_SIZE;
                u4Offset = MRP_RED_MSG_HDR_SIZE;

                if (MrpRedAllocMemForRmMsg (u1MsgType, u2BufSize, &pRmMsg)
                    != OSIX_SUCCESS)
                {
                    return OSIX_FAILURE;
                }

                MRP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdtType);
            }

            /* Added to avoid Klockwork warning. When control reaches here.
             * memory will be allocated for pRmMsg. Hence it can never be NULL.
             */
            MRP_CHK_NULL_PTR_RET (pRmMsg, OSIX_FAILURE);
            if (OSIX_FALSE == u1IsCtxtIdFilled)
            {
                MRP_RED_PUT_4_BYTES (pRmMsg, u4Offset, u4ContextId);
                u1IsCtxtIdFilled = OSIX_TRUE;
                u4PortCntFieldOffset = u4Offset;
                /* This filed is updated after filling the message with the
                 * all the ports that are operationally up.
                 */
                MRP_RED_PUT_2_BYTES (pRmMsg, u4Offset, 0);
            }

            MRP_RED_PUT_2_BYTES (pRmMsg, u4Offset, u2Port);
            u2NumOfPorts++;

        }
        u1IsCtxtIdFilled = OSIX_FALSE;
    }

    /* Send the last message. */
    if (pRmMsg != NULL)
    {
        MRP_RED_PUT_2_BYTES (pRmMsg, u4PortCntFieldOffset, u2NumOfPorts);
        MrpRedPutMsgHdrInRmMsg (pRmMsg, u1MsgType, (UINT2) u4Offset);
        MrpRedSendMsgToRm (u1MsgType, (UINT2) u4Offset, pRmMsg);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRBulkFormLvAllTmrRemTimBlkUpd 
 *
 *    DESCRIPTION      : This funtion constructs the bulk update message
 *                       containing the remaining the duration for which the
 *                       LeaveAll timer needs to be started in the standby node.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRBulkFormLvAllTmrRemTimBlkUpd (UINT1 u1AppId, UINT1 u1BulkUpdtType)
{
    tRmMsg             *pRmMsg = NULL;
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    UINT4               u4ContextId = gMrpGlobalInfo.u2FirstContextId;
    UINT4               u4RemainingTime = 0;
    UINT4               u4Offset = 0;
    UINT4               u4PortCntFieldOffset = 0;
    UINT4               u4CurrentTime = 0;
    UINT2               u2NextContextId = 0;
    UINT2               u2NextPortId = 0;
    UINT2               u2Port = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2NumOfPorts = 0;
    UINT1               u1IsCtxtIdFilled = OSIX_FALSE;
    UINT1               u1MsgType = MRP_RM_BULK_UPDATE_MSG;

    /* Leave All Timer Duration Bulk Update Message format */
    /* ----------------------------------------------------------------------
     * | MsgHdr | Context ID | Num of Ports | Port Id  | Rem Timer Duration |
     * ----------------------------------------------------------------------*/

    for (; u4ContextId != MRP_INIT_VAL; u4ContextId = u2NextContextId)
    {
        pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

        if ((NULL == pContextInfo) ||
            (OSIX_FALSE == MRP_IS_MRP_APP_ENABLED (u4ContextId, u1AppId)))
        {
            continue;
        }

        u2NextContextId = pContextInfo->u2NextContextId;

        pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);

        u2Port = pContextInfo->u2FirstPortId;
        for (; u2Port != MRP_INVALID_PORT; u2Port = u2NextPortId)
        {
            pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);

            if (NULL == pPortEntry)
            {
                continue;
            }

            u2NextPortId = pPortEntry->u2NextPortId;

            pAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2Port);
            if ((MRP_DISABLED == MRP_PORT_APP_STATUS (pPortEntry,
                                                      u1AppId)) ||
                (NULL == pAppPortEntry))
            {
                continue;
            }

            u4CurrentTime = OsixGetSysUpTime ();
            if (u4CurrentTime >= gMrpGlobalInfo.MrpRedInfo.u4NextRelinqTime)
            {
                if (NULL != pRmMsg)
                {
                    MRP_RED_PUT_2_BYTES (pRmMsg, u4PortCntFieldOffset,
                                         u2NumOfPorts);
                    u2NumOfPorts = 0;
                    MrpRedPutMsgHdrInRmMsg (pRmMsg, u1MsgType,
                                            (UINT2) u4Offset);
                    MrpRedSendMsgToRm (u1MsgType, (UINT2) u4Offset, pRmMsg);
                    pRmMsg = 0;
                    u1IsCtxtIdFilled = OSIX_FALSE;
                    u2BufSize = 0;
                    u4Offset = 0;
                }

                MrpRBulkCPURelinInBulkReqHandlng ();

                if (gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt == 0)
                {
                    return OSIX_SUCCESS;
                }
            }

            TmrGetRemainingTime
                (gMrpGlobalInfo.MrpTmrListId,
                 &(pAppPortEntry->LeaveAllTmrNode.TmrBlk.TimerNode),
                 &u4RemainingTime);

            if (0 == u4RemainingTime)
            {
                continue;
            }

            if ((u2BufSize - u4Offset) <
                MrpRdUtlGetMinBulkUpdMsgLen (u1BulkUpdtType))
            {
                if (NULL != pRmMsg)
                {
                    MRP_RED_PUT_2_BYTES (pRmMsg, u4PortCntFieldOffset,
                                         u2NumOfPorts);
                    u2NumOfPorts = 0;
                    MrpRedPutMsgHdrInRmMsg (pRmMsg, u1MsgType,
                                            (UINT2) u4Offset);
                    MrpRedSendMsgToRm (u1MsgType, (UINT2) u4Offset, pRmMsg);
                    u1IsCtxtIdFilled = OSIX_FALSE;
                    u4Offset = MRP_RED_MSG_HDR_SIZE;
                }

                u2BufSize = MRP_RED_MAX_MSG_SIZE;
                u4Offset = MRP_RED_MSG_HDR_SIZE;

                if (MrpRedAllocMemForRmMsg (u1MsgType, u2BufSize, &pRmMsg)
                    != OSIX_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
                MRP_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1BulkUpdtType);
            }

            /* Added to avoid Klockwork warning. When control reaches here.
             * memory will be allocated for pRmMsg. Hence it can never be NULL.
             */
            MRP_CHK_NULL_PTR_RET (pRmMsg, OSIX_FAILURE);

            if (OSIX_FALSE == u1IsCtxtIdFilled)
            {
                MRP_RED_PUT_4_BYTES (pRmMsg, u4Offset, u4ContextId);
                u1IsCtxtIdFilled = OSIX_TRUE;
                u4PortCntFieldOffset = u4Offset;
                /* This filed is updated after filling the message with the
                 * all the ports that are operationally up.
                 */
                MRP_RED_PUT_2_BYTES (pRmMsg, u4Offset, 0);
            }

            MRP_RED_PUT_2_BYTES (pRmMsg, u4Offset, u2Port);
            u2NumOfPorts++;
            MRP_RED_PUT_4_BYTES (pRmMsg, u4Offset, u4RemainingTime);
        }

        u1IsCtxtIdFilled = OSIX_FALSE;
    }

    /* Send the last message. */
    if (pRmMsg != NULL)
    {
        MRP_RED_PUT_2_BYTES (pRmMsg, u4PortCntFieldOffset, u2NumOfPorts);
        MrpRedPutMsgHdrInRmMsg (pRmMsg, u1MsgType, (UINT2) u4Offset);
        MrpRedSendMsgToRm (u1MsgType, (UINT2) u4Offset, pRmMsg);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRBulkSendLvAllTmrRemTimBlkUpd 
 *
 *    DESCRIPTION      : This funtion constructs the bulk update message
 *                       containing the remaining the duration for which the
 *                       LeaveAll timer needs to be started in the standby node.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : None 
 *     
 ****************************************************************************/
VOID
MrpRBulkSendLvAllTmrRemTimBlkUpd ()
{
    /* As Bulk update formation is suspended in between to process other events,
     * the following check is added.
     */
    if (0 != gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt)
    {
        MrpRBulkFormLvAllTmrRemTimBlkUpd (MRP_MVRP_APP_ID,
                                          MRP_RED_MVRP_LV_ALL_TMR_MSG);
    }

    if (0 != gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt)
    {
        MrpRBulkFormLvAllTmrRemTimBlkUpd (MRP_MMRP_APP_ID,
                                          MRP_RED_MMRP_LV_ALL_TMR_MSG);
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRBulkSendAttrBlkUpd 
 *
 *    DESCRIPTION      : This funtion constructs the bulk update message
 *                       containing the list of Attributes that are assocaited
 *                       with each port in the Context.
 *
 *    INPUT            : u1BulkMsgType - Type of Bulk Update Message  
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRBulkSendAttrBlkUpd (UINT1 u1BulkMsgType)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpAttrEntry       AttrEntry;
    tMrpRedBulkMsgInfo  MrpBulkMsgInfo;
    UINT4               u4ContextId = gMrpGlobalInfo.u2FirstContextId;
    UINT2               u2NextContextId = 0;
    UINT2               u2NextPortId = 0;
    UINT2               u2Port = 0;
    UINT1               u1AppId = MRP_MVRP_APP_ID;

    if ((MRP_RED_MAC_ADD_MSG == u1BulkMsgType) ||
        (MRP_RED_SERVICE_REQ_ADD_MSG == u1BulkMsgType))
    {
        u1AppId = MRP_MMRP_APP_ID;
    }

    MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));
    MEMSET (&MrpBulkMsgInfo, 0, sizeof (tMrpRedBulkMsgInfo));

    /* Attribute Bulk Update Message format */
    /* -------------------------------------------------------------------------
     * | MsgHdr | Context ID | Num of Ports | Port Id  | Num of Attrs |Attr Val|
     * -------------------------------------------------------------------------
     * In case MAC and Service Requiement, MAP ID is filled before the Num of 
     * Attrs field.
     */

    for (; u4ContextId != MRP_INIT_VAL; u4ContextId = u2NextContextId)
    {
        pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

        if ((NULL == pContextInfo) ||
            (OSIX_FALSE == MRP_IS_MRP_APP_ENABLED (u4ContextId, u1AppId)))
        {
            continue;
        }

        u2NextContextId = pContextInfo->u2NextContextId;
        MrpBulkMsgInfo.u4ContextId = u4ContextId;

        pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);

        u2Port = pContextInfo->u2FirstPortId;

        for (; u2Port != MRP_INVALID_PORT; u2Port = u2NextPortId)
        {
            pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);

            if (NULL == pPortEntry)
            {
                continue;
            }

            u2NextPortId = pPortEntry->u2NextPortId;

            /* Attributes are learnt on the port only if the port is oper up
             * and the application is enabled.
             */
            if ((MRP_DISABLED == MRP_PORT_APP_STATUS (pPortEntry, u1AppId)) ||
                (MRP_OPER_DOWN == pPortEntry->u1OperStatus))
            {
                continue;
            }

            if (0 == gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt)
            {
                return OSIX_SUCCESS;
            }
            /* Scan all the MAP and fill the attributes that are learnt on this
             * port.
             */
            MrpRBulkFormAttrBulkMsg (&MrpBulkMsgInfo, u1BulkMsgType, pAppEntry,
                                     u2Port);
        }

        if (NULL != MrpBulkMsgInfo.pRmMsg)
        {
            /* Attributes learnt on all the ports in this Context have been 
             * filled in the sync-up message. Hence update the port count in
             * the message.
             */
            MRP_RED_PUT_2_BYTES (MrpBulkMsgInfo.pRmMsg,
                                 MrpBulkMsgInfo.u4PortCntFieldOffset,
                                 MrpBulkMsgInfo.u2NumOfPorts);
            MrpBulkMsgInfo.u2NumOfPorts = 0;
            MrpBulkMsgInfo.u2NumOfAttrs = 0;
            MrpBulkMsgInfo.u1IsPortIdFilled = OSIX_FALSE;
            MrpBulkMsgInfo.u1IsCtxtIdFilled = OSIX_FALSE;
            MrpBulkMsgInfo.u1IsPortCntFilled = OSIX_TRUE;
        }
    }

    if (NULL != MrpBulkMsgInfo.pRmMsg)
    {
        MrpRBulkUpdAndSendAttrBulkMsg (&MrpBulkMsgInfo);
    }

    return OSIX_SUCCESS;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRBulkProcBulkUpdMsg 
 *
 *    DESCRIPTION      : This funtion processes the bulk update message.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRBulkProcBulkUpdMsg (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    UINT4               u4Offset = MRP_RED_MSG_HDR_SIZE;
    UINT1               u1BulkUpdType = 0;

    MRP_RED_GET_1_BYTE (pRmMsg, u4Offset, u1BulkUpdType);

    CRU_BUF_Move_ValidOffset (pRmMsg, u4Offset);
    MRP_GBL_TRC (MRP_RED_TRC, "Processing the bulk update message\n");

    /* u2MsgLen contains sizeof (u1BulkUpdType). Hence decrement the value. */
    u2MsgLen--;

    switch (u1BulkUpdType)
    {
        case MRP_RED_PORT_OPER_STATUS_MSG:
            MrpRBulkProcPortOperStatusMsg (pRmMsg, u2MsgLen);
            break;

        case MRP_RED_VLAN_ADD_MSG:
            MrpRBulkProcAttrBulkMsg (pRmMsg, MRP_RED_VLAN_ADD_MSG, u2MsgLen);
            break;

        case MRP_RED_MAC_ADD_MSG:
            MrpRBulkProcAttrBulkMsg (pRmMsg, MRP_RED_MAC_ADD_MSG, u2MsgLen);
            break;

        case MRP_RED_SERVICE_REQ_ADD_MSG:
            MrpRBulkProcAttrBulkMsg (pRmMsg, MRP_RED_SERVICE_REQ_ADD_MSG,
                                     u2MsgLen);
            break;

        case MRP_RED_MVRP_LV_ALL_TMR_MSG:
        case MRP_RED_MMRP_LV_ALL_TMR_MSG:
            MrpRBulkProcLvAllTmrBulkMsg (pRmMsg,
                                         (UINT1) MRP_RED_MVRP_LV_ALL_TMR_MSG,
                                         u2MsgLen);
            break;
        default:
            break;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRBulkProcPortOperStatusMsg 
 *
 *    DESCRIPTION      : This funtion processes the Oper Up bulk update message.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRBulkProcPortOperStatusMsg (tRmMsg * pRmMsg, UINT2 u2MsgLen)
{
    tMrpContextInfo    *pContextInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4Offset = 0;
    UINT2               u2PortId = 0;
    UINT2               u2PortCount = 0;

    while (u2MsgLen != 0)
    {
        MRP_RED_GET_4_BYTES (pRmMsg, u4Offset, u4ContextId);

        pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

        if (NULL == pContextInfo)
        {
            return OSIX_FAILURE;
        }

        MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2PortCount);

        while (u2PortCount > 0)
        {
            MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2PortId);

            if (MrpIfHandlePortOperInd (pContextInfo, u2PortId, CFA_IF_UP)
                != OSIX_SUCCESS)
            {
                MRP_TRC ((pContextInfo, MRP_RED_TRC,
                          "MrpRBulkProcPortOperStatusMsg: Processing oper up msg"
                          " failed for port:%d\r\n", u2PortId));
                return OSIX_FAILURE;

            }
            u2PortCount--;
        }
        u2MsgLen = (UINT2) (u2MsgLen - u4Offset);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRBulkProcAttrBulkMsg 
 *
 *    DESCRIPTION      : This funtion processes the VLAN/MAC/SERVICE REQUIREMENT
 *                       bulk update message.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRBulkProcAttrBulkMsg (tRmMsg * pRmMsg, UINT1 u1MsgType, UINT2 u2MsgLen)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpAttr            Attr;
    UINT4               u4ContextId = 0;
    UINT4               u4Offset = 0;
    UINT2               u2NumOfMapID = 0;
    UINT2               u2NumOfPorts = 0;
    UINT2               u2NumOfAttr = 0;
    UINT2               u2PortId = 0;
    UINT2               u2MapId = 0;
    UINT1               u1AppId = MRP_MVRP_APP_ID;

    if ((MRP_RED_MAC_ADD_MSG == u1MsgType) ||
        (MRP_RED_SERVICE_REQ_ADD_MSG == u1MsgType))
    {
        u1AppId = MRP_MMRP_APP_ID;
    }

    MEMSET (&Attr, 0, sizeof (tMrpAttr));

    while (u2MsgLen != 0)
    {
        MRP_RED_GET_4_BYTES (pRmMsg, u4Offset, u4ContextId);

        pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

        if (NULL == pContextInfo)
        {
            return OSIX_FAILURE;
        }

        MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2NumOfPorts);

        while (u2NumOfPorts > 0)
        {
            MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2PortId);

            MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2NumOfMapID);

            while (u2NumOfMapID > 0)
            {
                MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2MapId);

                MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2NumOfAttr);

                while (u2NumOfAttr > 0)
                {
                    if (MRP_RED_VLAN_ADD_MSG == u1MsgType)
                    {
                        Attr.u1AttrType = MRP_VID_ATTR_TYPE;
                        Attr.u1AttrLen = MRP_VLAN_ID_LEN;
                        MRP_RED_GET_N_BYTES (pRmMsg, Attr.au1AttrVal, u4Offset,
                                             MRP_VLAN_ID_LEN);
                    }
                    else if (MRP_RED_MAC_ADD_MSG == u1MsgType)
                    {
                        Attr.u1AttrType = MRP_MAC_ADDR_ATTR_TYPE;
                        Attr.u1AttrLen = MAC_ADDR_LEN;
                        MRP_RED_GET_N_BYTES (pRmMsg, Attr.au1AttrVal, u4Offset,
                                             MAC_ADDR_LEN);
                    }
                    else if (MRP_RED_SERVICE_REQ_ADD_MSG == u1MsgType)
                    {
                        Attr.u1AttrType = MRP_SERVICE_REQ_ATTR_TYPE;
                        Attr.u1AttrLen = MRP_SERVICE_REQ_LEN;
                        MRP_RED_GET_1_BYTE (pRmMsg, u4Offset,
                                            Attr.au1AttrVal[0]);
                    }

                    pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);
                    MrpRdUtlApplyAttrRegistration (pAppEntry, u2MapId, u2PortId,
                                                   &Attr);

                    u2NumOfAttr--;
                }
                u2NumOfMapID--;
            }
            u2NumOfPorts--;
        }
        u2MsgLen = (UINT2) (u2MsgLen - u4Offset);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *   FUNCTION NAME    : MrpRBulkProcLvAllTmrBulkMsg 
 *
 *   DESCRIPTION      : This funtion processes the bulk message containing the
 *                      remaining time for which the leave timer needs to be
 *                      started in the Standby node.
 *
 *   INPUT            : pRmMsg    - Pointer to Message
 *                      u1MsgType - Bulk update message type
 *                      u2MsgLen  - Length of the message
 *
 *   OUTPUT           : None
 *
 *   RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRBulkProcLvAllTmrBulkMsg (tRmMsg * pRmMsg, UINT1 u1MsgType, UINT2 u2MsgLen)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4TmrDuration = 0;
    UINT4               u4Offset = 0;
    UINT2               u2PortId = 0;
    UINT2               u2PortCount = 0;
    UINT1               u1AppId = MRP_MVRP_APP_ID;

    if (MRP_RED_MMRP_LV_ALL_TMR_MSG == u1MsgType)
    {
        u1AppId = MRP_MMRP_APP_ID;
    }

    while (u2MsgLen != 0)
    {
        MRP_RED_GET_4_BYTES (pRmMsg, u4Offset, u4ContextId);

        pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

        if (NULL == pContextInfo)
        {
            return OSIX_FAILURE;
        }

        pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);
        MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2PortCount);

        while (u2PortCount > 0)
        {
            MRP_RED_GET_2_BYTES (pRmMsg, u4Offset, u2PortId);

            pAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2PortId);

            if (NULL == pAppPortEntry)
            {
                return OSIX_FAILURE;
            }

            MRP_RED_GET_4_BYTES (pRmMsg, u4Offset, u4TmrDuration);
            /* Start the timer */
            if (MrpTmrStart (MRP_LEAVE_ALL_TMR, &pAppPortEntry->LeaveAllTmrNode,
                             u4TmrDuration) != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }

            u2PortCount--;
        }
        u2MsgLen = (UINT2) (u2MsgLen - u4Offset);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *   FUNCTION NAME    : MrpRBulkCPURelinInBulkReqHandlng 
 *
 *   DESCRIPTION      : This funtion stops the bulk update request      
 *                      handling event and processes the other events posted 
 *                      to MRP Task.
 *
 *   INPUT            : None 
 *
 *   OUTPUT           : None
 *
 *   RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRBulkCPURelinInBulkReqHandlng ()
{
    UINT4               u4CurrentTime = 0;

    MRP_UNLOCK ();
    /* Call the event processing function */
    MrpRBulkProcessPendingEvents ();

    MRP_LOCK ();
    /* Update the next relinquish interval */
    u4CurrentTime = OsixGetSysUpTime ();

    gMrpGlobalInfo.MrpRedInfo.u4NextRelinqTime =
        (u4CurrentTime + gMrpGlobalInfo.MrpRedInfo.u4RelinqInterval);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *   FUNCTION NAME    : MrpRBulkProcessPendingEvents
 *
 *   DESCRIPTION      : This funtion processes all the pending events during
 *                      CPU relinquish.
 *
 *   INPUT            : None 
 *
 *   OUTPUT           : None
 *
 *   RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
VOID
MrpRBulkProcessPendingEvents ()
{
    UINT4               u4Events = 0;

    if (OsixEvtRecv (gMrpGlobalInfo.TaskId, MRP_ALL_EVENTS, OSIX_NO_WAIT,
                     &u4Events) == OSIX_SUCCESS)
    {
        MrpQueProcessEvents (u4Events);
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRBulkFormAttrBulkMsg 
 *
 *    DESCRIPTION      : This funtion constructs the bulk update message
 *                       containing the list of Attributes that are assocaited
 *                       with each port in the Context.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRBulkFormAttrBulkMsg (tMrpRedBulkMsgInfo * pMrpBulkMsgInfo,
                         UINT1 u1BulkMsgType, tMrpAppEntry * pAppEntry,
                         UINT2 u2Port)
{
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    UINT4               u4CurrentTime = 0;
    UINT2               u2MapId = 0;

    for (u2MapId = 0; u2MapId < pAppEntry->u2MapArraySize; u2MapId++)
    {
        pMapEntry = pAppEntry->ppMapTable[u2MapId];

        if (NULL == pMapEntry)
        {
            continue;
        }

        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

        if (NULL == pMapPortEntry)
        {
            continue;
        }

        pAttrEntry = (tMrpAttrEntry *) RBTreeGetFirst
            (pMapEntry->pMrpAppEntry->MapAttrTable);

        for (; NULL != pAttrEntry; pAttrEntry = ((tMrpAttrEntry *) RBTreeGetNext
                                                 (pMapEntry->pMrpAppEntry->
                                                  MapAttrTable, pAttrEntry,
                                                  NULL)))
        {
            if ((MRP_RED_SERVICE_REQ_ADD_MSG == u1BulkMsgType) &&
                (MAC_ADDR_LEN == pAttrEntry->Attr.u1AttrLen))
            {
                break;
            }

            if ((MRP_RED_MAC_ADD_MSG == u1BulkMsgType) &&
                (MRP_SERVICE_REQ_LEN == pAttrEntry->Attr.u1AttrLen))
            {
                continue;
            }

            /* Check if bulk update request handling needs to be suspended 
             * to process other events in the MRP queue
             */
            u4CurrentTime = OsixGetSysUpTime ();
            if (u4CurrentTime >= gMrpGlobalInfo.MrpRedInfo.u4NextRelinqTime)
            {
                /* Send the formed sync-up message to RM and then process
                 * the pending events
                 */

                if (NULL != pMrpBulkMsgInfo->pRmMsg)
                {
                    MrpRBulkUpdAndSendAttrBulkMsg (pMrpBulkMsgInfo);
                }

                MrpRBulkCPURelinInBulkReqHandlng ();
                if (0 == gMrpGlobalInfo.MrpRedInfo.u1StandbyNodeCnt)
                {
                    return OSIX_SUCCESS;
                }
            }

            /* Fill this Attribute in the sync-up message */
            if (NULL != pMrpBulkMsgInfo->pRmMsg)
            {
                MrpRBulkFillAttrBulkMsg (u1BulkMsgType, pMapPortEntry,
                                         pAttrEntry, pMrpBulkMsgInfo);
            }
        }

        if (0 != pMrpBulkMsgInfo->u2NumOfAttrs)
        {
            /* All the Attributes for the given port for this MAP ID has been
             * filled in the message. Hence update the number of attributes.
             */
            if (NULL != pMrpBulkMsgInfo->pRmMsg)
            {
                MRP_RED_PUT_2_BYTES (pMrpBulkMsgInfo->pRmMsg,
                                     pMrpBulkMsgInfo->u4AttrCntFieldOffset,
                                     pMrpBulkMsgInfo->u2NumOfAttrs);
            }
            pMrpBulkMsgInfo->u1IsAttrCntFilled = OSIX_TRUE;
            pMrpBulkMsgInfo->u2NumOfAttrs = 0;
            /* Attributes belonging to this MAP has been filled in the message.
             * Hence increment the Number of MAP IDs.
             */
            pMrpBulkMsgInfo->u2NumOfMapIds++;
        }
    }

    if (0 != pMrpBulkMsgInfo->u2NumOfMapIds)
    {
        /* Attributes learnt on the port that belong to all the MAP Context have
         * been filled in the sync-up message. Hence update the MAPID count in
         * the message.
         */

        if (NULL != pMrpBulkMsgInfo->pRmMsg)
        {
            MRP_RED_PUT_2_BYTES (pMrpBulkMsgInfo->pRmMsg,
                                 pMrpBulkMsgInfo->u4MapCntFieldOffset,
                                 pMrpBulkMsgInfo->u2NumOfMapIds);
        }
        pMrpBulkMsgInfo->u2NumOfMapIds = 0;
        pMrpBulkMsgInfo->u1IsMapCntFilled = OSIX_TRUE;
    }
    pMrpBulkMsgInfo->u2NumOfAttrs = 0;
    pMrpBulkMsgInfo->u1IsPortIdFilled = OSIX_FALSE;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRBulkUpdAndSendAttrBulkMsg 
 *
 *    DESCRIPTION      : This funtion sends the bulk update message to RM
 *
 *    INPUT            : pMrpBulkMsgInfo - Pointer to BulkMsgInfo structure
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
VOID
MrpRBulkUpdAndSendAttrBulkMsg (tMrpRedBulkMsgInfo * pMrpBulkMsgInfo)
{
    if (OSIX_FALSE == pMrpBulkMsgInfo->u1IsPortCntFilled)
    {
        MRP_RED_PUT_2_BYTES (pMrpBulkMsgInfo->pRmMsg,
                             pMrpBulkMsgInfo->u4PortCntFieldOffset,
                             pMrpBulkMsgInfo->u2NumOfPorts);
        pMrpBulkMsgInfo->u2NumOfPorts = 0;
    }

    if (OSIX_FALSE == pMrpBulkMsgInfo->u1IsAttrCntFilled)
    {
        MRP_RED_PUT_2_BYTES (pMrpBulkMsgInfo->pRmMsg,
                             pMrpBulkMsgInfo->u4AttrCntFieldOffset,
                             pMrpBulkMsgInfo->u2NumOfAttrs);
        if (0 != pMrpBulkMsgInfo->u2NumOfAttrs)
        {
            /* The buffer space may be exhaused after filling some of the
             * attributes belonging to the MAP ID. Hence increment the
             * u2NumOfMapIds.
             */
            pMrpBulkMsgInfo->u2NumOfMapIds++;
        }

        pMrpBulkMsgInfo->u2NumOfAttrs = 0;
    }

    if (OSIX_FALSE == pMrpBulkMsgInfo->u1IsMapCntFilled)
    {
        MRP_RED_PUT_2_BYTES (pMrpBulkMsgInfo->pRmMsg,
                             pMrpBulkMsgInfo->u4MapCntFieldOffset,
                             pMrpBulkMsgInfo->u2NumOfMapIds);
        pMrpBulkMsgInfo->u2NumOfMapIds = 0;
    }

    MrpRedPutMsgHdrInRmMsg (pMrpBulkMsgInfo->pRmMsg,
                            (UINT1) MRP_RM_BULK_UPDATE_MSG,
                            (UINT2) (pMrpBulkMsgInfo->u4MsgOffset));
    MrpRedSendMsgToRm ((UINT1) MRP_RM_BULK_UPDATE_MSG,
                       (UINT2) (pMrpBulkMsgInfo->u4MsgOffset),
                       pMrpBulkMsgInfo->pRmMsg);
    pMrpBulkMsgInfo->u1IsCtxtIdFilled = OSIX_FALSE;
    pMrpBulkMsgInfo->u1IsPortIdFilled = OSIX_FALSE;
    pMrpBulkMsgInfo->u1IsMapIdFilled = OSIX_FALSE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRBulkFillAttrBulkMsg 
 *
 *    DESCRIPTION      : This funtion fills the Attribute values in the bulk
 *                       sync-up message 
 *
 *    INPUT            : u1BulkMsgType - Bulk update Message Type 
 *                       pMapPortEntry - Pointer to Map Port Entry
 *                       u2AttrIndex   - Attribute Index
 *                       pMrpBulkMsgInfo - Pointer to BulkMsgInfo structure
 *
 *    OUTPUT           : None
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
VOID
MrpRBulkFillAttrBulkMsg (UINT1 u1BulkMsgType, tMrpMapPortEntry * pMapPortEntry,
                         tMrpAttrEntry * pAttrEntry,
                         tMrpRedBulkMsgInfo * pMrpBulkMsgInfo)
{
    UINT2               u2AttrIndex = pAttrEntry->u2AttrIndex;
    UINT2              *pu2BufSize = &(pMrpBulkMsgInfo->u2BufSize);

    if ((MRP_MT != MRP_GET_ATTR_REG_SEM_STATE
         (pMapPortEntry->pu1AttrInfoList[u2AttrIndex])) &&
        (MRP_REG_NORMAL == MRP_GET_ATTR_REG_ADMIN_CTRL
         (pMapPortEntry->pu1AttrInfoList[u2AttrIndex])))
    {
        if ((*pu2BufSize - pMrpBulkMsgInfo->u4MsgOffset) <
            MrpRdUtlGetMinBulkUpdMsgLen (u1BulkMsgType))
        {
            if (NULL != pMrpBulkMsgInfo->pRmMsg)
            {
                MrpRBulkUpdAndSendAttrBulkMsg (pMrpBulkMsgInfo);
            }

            *pu2BufSize = MRP_RED_MAX_MSG_SIZE;
            pMrpBulkMsgInfo->u4MsgOffset = MRP_RED_MSG_HDR_SIZE;

            if (MrpRedAllocMemForRmMsg ((UINT1) MRP_RM_BULK_UPDATE_MSG,
                                        *pu2BufSize,
                                        &(pMrpBulkMsgInfo->pRmMsg))
                != OSIX_SUCCESS)
            {
                return;
            }

            MRP_RED_PUT_1_BYTE (pMrpBulkMsgInfo->pRmMsg,
                                pMrpBulkMsgInfo->u4MsgOffset, u1BulkMsgType);
        }

        if (OSIX_FALSE == pMrpBulkMsgInfo->u1IsCtxtIdFilled)
        {
            MRP_RED_PUT_4_BYTES (pMrpBulkMsgInfo->pRmMsg,
                                 pMrpBulkMsgInfo->u4MsgOffset,
                                 pMrpBulkMsgInfo->u4ContextId);
            pMrpBulkMsgInfo->u1IsCtxtIdFilled = OSIX_TRUE;
            pMrpBulkMsgInfo->u4PortCntFieldOffset =
                pMrpBulkMsgInfo->u4MsgOffset;

            /* Port Count filed is updated once all the Attributes learnt on the
             * port is filled in the message.
             */
            MRP_RED_PUT_2_BYTES (pMrpBulkMsgInfo->pRmMsg,
                                 pMrpBulkMsgInfo->u4MsgOffset, 0);
        }

        if (OSIX_FALSE == pMrpBulkMsgInfo->u1IsPortIdFilled)
        {
            MRP_RED_PUT_2_BYTES (pMrpBulkMsgInfo->pRmMsg,
                                 pMrpBulkMsgInfo->u4MsgOffset,
                                 pMapPortEntry->u2Port);
            pMrpBulkMsgInfo->u1IsPortIdFilled = OSIX_TRUE;
            pMrpBulkMsgInfo->u2NumOfPorts++;
            pMrpBulkMsgInfo->u4MapCntFieldOffset = pMrpBulkMsgInfo->u4MsgOffset;

            /* MAP Count filed is updated once all the Attributes learnt on the
             * port is filled in the message.
             */
            MRP_RED_PUT_2_BYTES (pMrpBulkMsgInfo->pRmMsg,
                                 pMrpBulkMsgInfo->u4MsgOffset, 0);
        }

        if (OSIX_FALSE == pMrpBulkMsgInfo->u1IsMapIdFilled)
        {
            MRP_RED_PUT_2_BYTES (pMrpBulkMsgInfo->pRmMsg,
                                 pMrpBulkMsgInfo->u4MsgOffset,
                                 pMapPortEntry->pMapEntry->u2MapId);
            pMrpBulkMsgInfo->u1IsMapIdFilled = OSIX_TRUE;
            pMrpBulkMsgInfo->u4AttrCntFieldOffset =
                pMrpBulkMsgInfo->u4MsgOffset;
            /* This filed is updated after filling the message with the all the
             * attributes that are learnt on this port.
             */
            MRP_RED_PUT_2_BYTES (pMrpBulkMsgInfo->pRmMsg,
                                 pMrpBulkMsgInfo->u4MsgOffset, 0);

        }

        MRP_RED_PUT_N_BYTES (pMrpBulkMsgInfo->pRmMsg,
                             pAttrEntry->Attr.au1AttrVal,
                             pMrpBulkMsgInfo->u4MsgOffset,
                             pAttrEntry->Attr.u1AttrLen);
        pMrpBulkMsgInfo->u2NumOfAttrs++;

    }

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrprbulk.c                     */
/*-----------------------------------------------------------------------*/
