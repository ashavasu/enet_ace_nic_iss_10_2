/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpsz.c,v 1.3 2013/11/29 11:04:14 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _MRPSZ_C
#include "mrpinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
MrpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MRP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsMRPSizingParams[i4SizingId].u4StructSize,
                                     FsMRPSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(MRPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            MrpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
MrpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsMRPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, MRPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
MrpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MRP_MAX_SIZING_ID; i4SizingId++)
    {
        if (MRPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (MRPMemPoolIds[i4SizingId]);
            MRPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
