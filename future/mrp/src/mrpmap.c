/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpmap.c,v 1.14 2013/03/08 13:33:45 siva Exp $
 *
 * Description: This file contains the MAP related routines.
 ******************************************************************************/

#include "mrpinc.h"
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapAppAttributeReqJoin
 *                                                                          
 *    DESCRIPTION      : This function is called when the higher-layer 
 *                       application (VLAN module) send a Join.Request for 
 *                       an attribute.
 *
 *    INPUT            : pAppEntry - Pointer to the application entry.
 *                       pAttr     - pointer to Mrp Attribute structure
 *                       u2MapId   - MAP Identifier
 *                       AddPorts  - Ports that needs to be added.
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapAppAttributeReqJoin (tMrpAppEntry * pAppEntry, tMrpAttr * pAttr,
                           UINT2 u2MapId, tLocalPortList AddPorts)
{
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    tMrpIfMsg           IfMsg;
    UINT2               u2PortId = 0;
    UINT1               u1SendNewInd = OSIX_FALSE;
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;
    UINT1               u1Propagate = OSIX_FALSE;
    UINT1               u1IsEntryPresent = OSIX_FALSE;
    eMrpSendMsgInd      MsgType = MRP_SEND_JOIN;
    BOOL1               bResult = OSIX_FALSE;

    /* Scan all the ports in the context and check the ports are 
     * present in added portlist. If present then add the attribute else 
     * create port entry and add the attribute */

    for (u2PortId = 1; u2PortId <= MRP_MAX_PORTS_PER_CONTEXT; u2PortId++)
    {

        pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2PortId);

        if (pMrpAppPortEntry == NULL)
        {
            continue;
        }
        OSIX_BITLIST_IS_BIT_SET (AddPorts, u2PortId, sizeof (tLocalPortList),
                                 bResult);

        if (bResult == OSIX_FALSE)
        {
            continue;
        }

        if (u1IsEntryPresent == OSIX_FALSE)
        {
            if (MrpMapCreateAppAttrReqJoin (pAppEntry, pAttr, &pAttrEntry,
                                            u2MapId,
                                            &u1SendNewInd) != OSIX_SUCCESS)
            {
                return;
            }
            u1IsEntryPresent = OSIX_TRUE;
        }

        /* Get the MrpMapEntry using the MAP Id */
        if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
        {
            /* Can never happen as the above fn. only creates this
             * entry
             * */
            MRP_TRC ((pAppEntry->pMrpContextInfo,
                      (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                      "MrpMapAppAttributeReqJoinOnPort: Map entry not created "
                      "for %d in appl. \n", u2MapId));
            return;
        }

        /* Add a attribute entry if the port is a static port (i4Result == TRUE)
         * irrespective of whether the port is in Forwarding/Blocking state. */
        if (bResult == OSIX_TRUE)
        {
            if (MrpMapAttrReqJoinOnPort (pAppEntry, pMapEntry, pAttrEntry,
                                         u2PortId, &u1PortState)
                != OSIX_SUCCESS)
            {

                MRP_TRC ((pAppEntry->pMrpContextInfo,
                          (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                          "MrpMapAppAttributeReqJoin: MrpMapAttrReqJoinOnPort"
                          "returned failure for port %d in appl. \n",
                          u2PortId));

                continue;
            }

            if (u1PortState == AST_PORT_STATE_FORWARDING)
            {
                /*
                 * Atleast one of the added ports must be in Forwarding state
                 * to propagate the attribute.
                 *
                 * Otherwise, if all the static ports are not in Forwarding
                 * state, propagating the attribute is useless, since frames
                 * received for that attribute from other bridges will never
                 * be forwarded on these static ports.
                 *
                 * When these static ports move to Forwarding state, the
                 * attribute will be propagated on all active ports.
                 */

                u1Propagate = OSIX_TRUE;
            }
        }
    }

    if (u1Propagate == OSIX_TRUE)
    {
        /* Some ports in the static port list is in Forwarding state.
         * Hence, propagate this attribute. */

        IfMsg.pAppEntry = pAppEntry;
        IfMsg.u2MapId = u2MapId;
        IfMsg.u4ContextId = pAppEntry->pMrpContextInfo->u4ContextId;
        /* Set the port to invalid port for propagating the attribute on
         * all ports.*/
        IfMsg.u2Port = MRP_INVALID_PORT;

        if (u1SendNewInd == OSIX_TRUE)
        {
            MsgType = MRP_SEND_NEW;
        }

        MrpMapPropagateOnAllPorts (pAttr, &IfMsg, pMapEntry, MsgType);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapAppAttributeReqJoinOnPort
 *                                                                          
 *    DESCRIPTION      : This function is called when the higher-layer 
 *                       application (VLAN module) send a Join.Request for 
 *                       an attribute on a port.
 *
 *    INPUT            : pAppEntry - Pointer to the application entry.
 *                       pAttr     - pointer to Mrp Attribute structure
 *                       u2MapId   - MAP Identifier
 *                       u2Port    - Port that needs to be added.
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapAppAttributeReqJoinOnPort (tMrpAppEntry * pAppEntry, tMrpAttr * pAttr,
                                 UINT2 u2MapId, UINT2 u2Port)
{
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpIfMsg           IfMsg;
    UINT1               u1SendNewInd = OSIX_FALSE;
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;
    eMrpSendMsgInd      MsgType = MRP_SEND_JOIN;

    /* Since, we expect this information to be posted from VLAN module, VLAN
     * may trigger this event before the port is created or the port is made
     * oper up in MRP. Avoid processing in such scenarios. The following 
     * portion will be handled when Oper Up comes to MRP.
     */
    pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2Port);

    if (pMrpAppPortEntry == NULL)
    {
        return;
    }

    if (MrpMapCreateAppAttrReqJoin (pAppEntry, pAttr, &pAttrEntry, u2MapId,
                                    &u1SendNewInd) != OSIX_SUCCESS)
    {
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapAppAttributeReqJoinOnPort: MrpMapCreateAppAttrReqJoin"
                  "returned FAILURE \n"));
        return;
    }

    /* The value of pAttrEntry should be updated by the above function.
     * This can be NULL only when the above function returns OSIX_FAILURE
     * */

    /* Get the MrpMapEntry using the MAP Id */
    if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
    {
        /* Can never happen as the above fn. only creates this
         * entry
         * */
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapAppAttributeReqJoinOnPort: Map entry not created "
                  "for %d in appl. \n", u2MapId));
        return;
    }

    if (MrpMapAttrReqJoinOnPort (pAppEntry, pMapEntry, pAttrEntry,
                                 u2Port, &u1PortState) != OSIX_SUCCESS)
    {

        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapAppAttributeReqJoinOnPort: MrpMapAttrReqJoinOnPort"
                  "returned FAILURE \n"));
        return;
    }

    if (u1PortState == AST_PORT_STATE_FORWARDING)
    {
        IfMsg.pAppEntry = pAppEntry;
        IfMsg.u2MapId = u2MapId;
        IfMsg.u4ContextId = pAppEntry->pMrpContextInfo->u4ContextId;
        /* Set the port to invalid port for propagating the attribute on
         * all ports.*/
        IfMsg.u2Port = MRP_INVALID_PORT;

        if (u1SendNewInd == OSIX_TRUE)
        {
            MsgType = MRP_SEND_NEW;
        }

        MrpMapPropagateOnAllPorts (pAttr, &IfMsg, pMapEntry, MsgType);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapAppAttributeReqLeave
 *                                                                          
 *    DESCRIPTION      : This function is called when the higher-layer 
 *                       application (VLAN module) send a Leave.Request for 
 *                       an attribute.
 *
 *    INPUT            : pAppEntry - Pointer to the application entry.
 *                       pAttr     - pointer to Mrp Attribute structure
 *                       u2MapId   - MAP Identifier
 *                       DelPorts  - Ports that needs to be deleted.
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapAppAttributeReqLeave (tMrpAppEntry * pAppEntry, tMrpAttr * pAttr,
                            UINT2 u2MapId, tLocalPortList DelPorts)
{
    tMrpMapPortEntry   *pPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    UINT1               u1Result = OSIX_FALSE;
    UINT1               u1SendLeaveFlag = OSIX_TRUE;
    UINT2               u2PortId = 0;
    UINT4               u4DelPortCount = 0;
    UINT4               u4DelPortBlockCount = 0;
    UINT2               u2AttrIndex = 0;

    if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
    {
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "MrpMapAppAttributeReqLeave: " "Map Entry not present\n"));
        return;
    }

    pAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMapEntry);

    if (pAttrEntry == NULL)
    {
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "MrpMapAppAttributeReqLeave: "
                  "No Attribute Entry registered in this MAP context\n"));
        return;
    }

    u2AttrIndex = pAttrEntry->u2AttrIndex;

    for (u2PortId = 1; u2PortId <= MRP_MAX_PORTS_PER_CONTEXT; u2PortId++)
    {
        pPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

        if (pPortEntry == NULL)
        {
            continue;
        }

        /* Propagate the attribute only on ports that belong
         * to the given Map Context. */

        OSIX_BITLIST_IS_BIT_SET (DelPorts, pPortEntry->u2Port,
                                 sizeof (tLocalPortList), u1Result);

        if (u1Result == OSIX_TRUE)
        {
            if (MRP_GET_ATTR_REG_ADMIN_CTRL
                (pPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_REG_FIXED)
            {
                pAttrEntry->u2StaticMemPortCnt--;
            }

            /* If the port is in deleted ports then reset the RegAdminCtrl 
             * and RegSemState to the following  */
            if (MRP_GET_ATTR_REG_ADMIN_CTRL
                (pPortEntry->pu1AttrInfoList[u2AttrIndex]) != MRP_REG_NORMAL)
            {
                MRP_SET_ATTR_REG_ADMIN_CTRL
                    (pPortEntry->pu1AttrInfoList[u2AttrIndex], MRP_REG_NORMAL);
                MRP_SET_ATTR_REG_SEM_STATE
                    (pPortEntry->pu1AttrInfoList[u2AttrIndex], MRP_MT);
            }

            u4DelPortCount++;

            if (pPortEntry->u1PortState == AST_PORT_STATE_DISCARDING)
            {
                u4DelPortBlockCount++;
            }
        }

        /*
         * We need to check, if this attribute is associated statically
         * with any other port.
         */
        if ((MRP_GET_ATTR_REG_ADMIN_CTRL
             (pPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_REG_FIXED) &&
            (pPortEntry->u1PortState == AST_PORT_STATE_FORWARDING))
        {

            u1SendLeaveFlag = OSIX_FALSE;
        }
    }

    /*
     * If all the deleted ports are in Blocking state, we dont need
     * to send Leave message on other ports. This is because, we would
     * not have propagated the messages, if all the deleted ports
     * are in Blocking state.
     */
    if ((u4DelPortBlockCount != u4DelPortCount) &&
        (u1SendLeaveFlag == OSIX_TRUE))
    {
        for (u2PortId = 1; u2PortId <= MRP_MAX_PORTS_PER_CONTEXT; u2PortId++)
        {
            pPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

            if (pPortEntry == NULL)
            {
                continue;
            }

            MrpSmApplicantSem (pPortEntry, u2AttrIndex, MRP_APP_REQ_LEAVE);

            /* The above function will delete the MAP entry once all the 
             * ports are deleted. So get the MAP entry Freshly */
            if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
            {
                break;
            }

            MrpAttrCheckAndDelAttrEntry (pPortEntry, pAttrEntry);
        }                        /* For all the ports */
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapPropagateOnAllPorts
 *                                                                          
 *    DESCRIPTION      : This function will propagates the attribute   
 *                       information on all the ports. 
 *
 *    INPUT            : pAttr      - pointer to Mrp Attribute structure
 *                       pIfMsg     - pointer to the MrpIfMsg structure
 *                       pMapEntry  - pointer to the MAP structure
 *                       SendMsgInd - Message to be Propagated
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapPropagateOnAllPorts (tMrpAttr * pAttr, tMrpIfMsg * pIfMsg,
                           tMrpMapEntry * pMapEntry, eMrpSendMsgInd SendMsgInd)
{
    UINT1              *pMapPortList = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT2               u2PortId = 0;
    UINT1               u1Result = OSIX_FALSE;

    pMapPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pMapPortList == NULL)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpMapPropagateOnAllPorts: Error in allocating memory "
                     "for pMapPortList\r\n");
        return;
    }
    MEMSET (pMapPortList, 0, sizeof (tLocalPortList));

    MrpMapGetPortList (pIfMsg->u4ContextId, pIfMsg->pAppEntry->u1AppId,
                       pIfMsg->u2MapId, pMapPortList);

    for (u2PortId = 1; u2PortId <= MRP_MAX_PORTS_PER_CONTEXT; u2PortId++)
    {
        pAppPortEntry = MRP_GET_APP_PORT_ENTRY (pIfMsg->pAppEntry, u2PortId);

        if ((pAppPortEntry == NULL) ||
            (pAppPortEntry->pMrpPortEntry->u2LocalPortId == pIfMsg->u2Port))
        {
            continue;
        }

        /* Propagate the attribute only on ports that belong
         * to the given Map Context. */

        OSIX_BITLIST_IS_BIT_SET (pMapPortList,
                                 pAppPortEntry->pMrpPortEntry->u2LocalPortId,
                                 sizeof (tLocalPortList), u1Result);

        if (u1Result == OSIX_TRUE)
        {
            pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

            if (pMapPortEntry == NULL)
            {
                pMapPortEntry = MrpMapDSCreateMrpMapPortEntry (pMapEntry,
                                                               u2PortId);
                if (pMapPortEntry == NULL)
                {
                    MRP_TRC ((pAppPortEntry->pMrpAppEntry->pMrpContextInfo,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "MrpMapPropagateOnAllPorts: No Free MAP Port "
                              " Entry for port %d. Failed \n", u2PortId));
                    continue;
                }
            }

            if (SendMsgInd == MRP_SEND_JOIN)
            {

                MrpMadMapAttributeJoinReq (pAttr, pMapPortEntry, pIfMsg,
                                           pMapEntry, MRP_SEND_JOIN);
            }
            else if (SendMsgInd == MRP_SEND_NEW)
            {
                MrpMadMapAttributeJoinReq (pAttr, pMapPortEntry, pIfMsg,
                                           pMapEntry, MRP_SEND_NEW);
            }
            else
            {
                MrpMadMapAttributeLeaveReq (pAttr, pMapPortEntry, pIfMsg,
                                            pMapEntry);
            }
        }
    }
    UtilPlstReleaseLocalPortList (pMapPortList);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapAppSetForbiddenPorts
 *                                                                          
 *    DESCRIPTION      : This function is called when the higher-layer 
 *                       application (VLAN module) sends the registrar state 
 *                       as forbidden for the set of ports.
 *
 *    INPUT            : pAppEntry - Pointer to the application entry.
 *                       pAttr     - pointer to Mrp Attribute structure
 *                       u2MapId   - MAP Identifier
 *                       AddPorts  - Ports that needs to be added.
 *                       DelPorts  - Ports that needs to be deleted.
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapAppSetForbiddenPorts (tMrpAppEntry * pAppEntry, tMrpAttr * pAttr,
                            UINT2 u2MapId, tLocalPortList AddPorts,
                            tLocalPortList DelPorts)
{
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    UINT2               u2AttrIndex = 0;
    UINT2               u2PortId = 0;
    UINT1               u1IsEntryPresent = OSIX_FALSE;
    BOOL1               bResult1 = OSIX_FALSE;
    BOOL1               bResult2 = OSIX_FALSE;
    UINT1               u1SendNewInd = OSIX_FALSE;    /*Dummy vble in this fn */

    for (u2PortId = 1; u2PortId <= MRP_MAX_PORTS_PER_CONTEXT; u2PortId++)
    {
        pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2PortId);

        if (pMrpAppPortEntry == NULL)
        {
            continue;
        }

        OSIX_BITLIST_IS_BIT_SET (AddPorts, u2PortId, sizeof (tLocalPortList),
                                 bResult1);
        OSIX_BITLIST_IS_BIT_SET (DelPorts, u2PortId, sizeof (tLocalPortList),
                                 bResult2);

        if ((bResult1 == OSIX_FALSE) && (bResult2 == OSIX_FALSE))
        {
            continue;
        }

        if (u1IsEntryPresent == OSIX_FALSE)
        {
            if (MrpMapCreateAppAttrReqJoin (pAppEntry, pAttr, &pAttrEntry,
                                            u2MapId,
                                            &u1SendNewInd) != OSIX_SUCCESS)
            {
                return;
            }
            u1IsEntryPresent = OSIX_TRUE;
        }

        if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
        {
            /* Can never happen as the above fn. only creates this
             * entry
             * */
            MRP_TRC ((pAppEntry->pMrpContextInfo,
                      (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                      "MrpMapAppAttributeReqJoinOnPort: Map entry not created "
                      "for %d in appl. \n", u2MapId));
            return;
        }

        u2AttrIndex = pAttrEntry->u2AttrIndex;

        if (bResult1 == OSIX_TRUE)
        {
            if (MrpMapAttrSetForbidenPort (pAppEntry, pMapEntry, pAttrEntry,
                                           pAttr, u2PortId) != OSIX_SUCCESS)
            {
                MRP_TRC ((pAppEntry->pMrpContextInfo,
                          (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                          "MrpMapAppSetForbiddenPorts: "
                          "MrpMapAttrSetForbidenPort returned Failure\n"));
            }
        }
        else
        {
            pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

            if (pMrpMapPortEntry != NULL)
            {
                MRP_SET_ATTR_REG_ADMIN_CTRL
                    (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex],
                     MRP_REG_NORMAL);

                MrpAttrCheckAndDelAttrEntry (pMrpMapPortEntry, pAttrEntry);
            }
            else
            {
                MRP_TRC ((pAppEntry->pMrpContextInfo,
                          (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                          "MrpMapAppSetForbiddenPorts: "
                          "MapPortEntry not found \n"));
            }
        }
    }                            /* For all interfaces */
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapAppSetForbiddenPort
 *                                                                          
 *    DESCRIPTION      : This function is called when the higher-layer 
 *                       application (VLAN module) sends the registrar state 
 *                       as forbidden for a port.
 *
 *    INPUT            : pAppEntry - Pointer to the application entry.
 *                       pAttr     - pointer to Mrp Attribute structure
 *                       u2MapId   - MAP Identifier
 *                       u2Port    - Port that needs to be added.
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapAppSetForbiddenPort (tMrpAppEntry * pAppEntry, tMrpAttr * pAttr,
                           UINT2 u2MapId, UINT2 u2Port)
{
    tMrpAppPortEntry   *pMrpAppPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    UINT1               u1SendNewInd = OSIX_FALSE;    /*Dummy vble in this fn */

    if (MrpMapCreateAppAttrReqJoin (pAppEntry, pAttr, &pAttrEntry, u2MapId,
                                    &u1SendNewInd) != OSIX_SUCCESS)
    {
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapAppSetForbiddenPort: MrpMapCreateAppAttrReqJoin"
                  "returned FAILURE \n"));
        return;
    }

    /* The value of pAttrEntry should be updated by the above function.
     * This can be NULL only when the above function returns OSIX_FAILURE
     * */

    if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
    {
        /* Can never happen as the above fn. only creates this
         * entry
         * */
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapAppSetForbiddenPort: Map entry not created "
                  "for %d in appl. \n", u2MapId));
        return;
    }

    pMrpAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2Port);

    if (pMrpAppPortEntry == NULL)
    {
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapAppSetForbiddenPort: APP entry not created "
                  "for %d in appl. \n", u2MapId));
        return;
    }

    if (MrpMapAttrSetForbidenPort (pAppEntry, pMapEntry, pAttrEntry, pAttr,
                                   u2Port) != OSIX_SUCCESS)
    {
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapAppSetForbiddenPort: "
                  "MrpMapAttrSetForbidenPort returned Failure\n"));
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapMadAttributeJoinInd
 *                                                                          
 *    DESCRIPTION      : This function indicates the Join message 
 *                       to the ports other than the source port. 
 *
 *    INPUT            : pAttr      - Pointer to Mrp Attribute structure
 *                       pIfMsg     - Pointer to tMrpIfMsg structure
 *                       SendMsgInd - Message to be Propagated
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapMadAttributeJoinInd (tMrpAttr * pAttr, tMrpIfMsg * pIfMsg,
                           eMrpHlMsgInd SendMsgInd)
{
    UINT1              *pPortList = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT2               u2MapId = 0;
    UINT2               u2PortId = 0;
    UINT1               u1Result = OSIX_FAILURE;

    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList == NULL)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpMapMadAttributeJoinInd : Error in allocating memory "
                     "for pPortList\r\n");
        return;
    }
    MEMSET (pPortList, 0, sizeof (tLocalPortList));
    pAppEntry = pIfMsg->pAppEntry;
    u2MapId = pIfMsg->u2MapId;

    if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
    {
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapMadAttributeJoinInd: " "No Map Entry present\n"));
        UtilPlstReleaseLocalPortList (pPortList);
        return;
    }

    pContextInfo = pAppEntry->pMrpContextInfo;

    MrpMapGetPortList (pIfMsg->u4ContextId, pIfMsg->pAppEntry->u1AppId,
                       u2MapId, pPortList);

    /* Propagate this Join on all other belonging to the given Map. */

    for (u2PortId = 1; u2PortId <= MRP_MAX_PORTS_PER_CONTEXT; u2PortId++)
    {
        pAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2PortId);

        if (pAppPortEntry == NULL)
        {
            continue;
        }

        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

        if (pAppEntry->u1AppId == MRP_MVRP_APP_ID)
        {
            if (pMapPortEntry == NULL)
            {
                pMapPortEntry = MrpMapDSCreateMrpMapPortEntry (pMapEntry,
                                                               u2PortId);
                if (NULL == pMapPortEntry)
                {
                    MRP_TRC ((pContextInfo,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "MrpMapMadAttributeJoinInd:"
                              "No Free MAPPort Entry \n"));
                    continue;
                }
            }

            if (u2PortId != pIfMsg->u2Port)
            {
                if (SendMsgInd == MRP_HL_IND_NEW)
                {
                    MrpMadMapAttributeJoinReq (pAttr, pMapPortEntry, pIfMsg,
                                               pMapEntry, MRP_SEND_NEW);
                }
                else
                {
                    MrpMadMapAttributeJoinReq (pAttr, pMapPortEntry, pIfMsg,
                                               pMapEntry, MRP_SEND_JOIN);
                }
            }
        }
        else
        {
            /* This is Vlan Context Map */
            if (u2PortId != pIfMsg->u2Port)
            {
                OSIX_BITLIST_IS_BIT_SET (pPortList,
                                         u2PortId,
                                         sizeof (tLocalPortList), u1Result);

                if (u1Result == OSIX_TRUE)
                {
                    if (pMapPortEntry == NULL)
                    {
                        pMapPortEntry =
                            MrpMapDSCreateMrpMapPortEntry (pMapEntry, u2PortId);

                        if (NULL == pMapPortEntry)
                        {
                            MRP_TRC ((pContextInfo,
                                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                                      "MrpMapMadAttributeJoinInd:"
                                      "No Free MAP Port Entry \n"));
                            continue;
                        }
                    }

                    if (SendMsgInd == MRP_HL_IND_NEW)
                    {
                        MrpMadMapAttributeJoinReq (pAttr, pMapPortEntry, pIfMsg,
                                                   pMapEntry, MRP_SEND_NEW);
                    }
                    else
                    {
                        MrpMadMapAttributeJoinReq (pAttr, pMapPortEntry, pIfMsg,
                                                   pMapEntry, MRP_SEND_JOIN);
                    }
                }
            }
        }
    }
    UtilPlstReleaseLocalPortList (pPortList);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapHandleMapUpdateMapPorts
 *                                                                          
 *    DESCRIPTION      : This function transmits Join messages on the given
 *                       port, for all the Attributes registered for the 
 *                       given MapId.
 *                       This function is called by MVRP whenever a port
 *                       is learnt for a Vlan.
 *                       Updation of Map Port list affects only MMRP.
 *
 *    INPUT            : u4ContextId - Context identifier
 *                       u2Port      - Local-Port Identifier
 *                       u2MapId     - MAP Identifier
 *                       u1Action    - MRP_ADD/MRP_DELETE
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapHandleMapUpdateMapPorts (UINT4 u4ContextId, UINT2 u2Port, UINT2 u2MapId,
                               UINT1 u1Action)
{
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpMapPortEntry   *pPortEntry = NULL;
    tMrpMapPortEntry   *pNewPortEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpIfMsg           IfMsg;
    UINT4               u4Index = 0;
    UINT2               u2PortId = 0;

    if (MrpMmrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {
        return;
    }
    pAppEntry = MRP_GET_APP_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                   MRP_MMRP_APP_ID);

    /* Since, we expect this information to be posted from VLAN module, VLAN
     * may trigger this event before the port is created or the port is made
     * oper up in MRP. Avoid processing in such scenarios. The following 
     * portion will be handled when Oper Up comes to MRP
     * */
    pMrpPortEntry = MRP_GET_PORT_ENTRY (pAppEntry->pMrpContextInfo, u2Port);

    if (pMrpPortEntry == NULL)
    {
        return;
    }

    if (pMrpPortEntry->u1OperStatus != MRP_OPER_UP)
    {
        return;
    }

    if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
    {
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapHandleMapUpdateMapPorts: " "No Map Entry present\n"));
        return;
    }

    pNewPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

    if (u1Action == MRP_ADD)
    {
        for (u2PortId = 1; u2PortId <= MRP_MAX_PORTS_PER_CONTEXT; u2PortId++)
        {
            pPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

            if (pPortEntry == NULL)
            {
                continue;
            }

            if ((pPortEntry->u2Port == u2Port) ||
                (pPortEntry->u1PortState == AST_PORT_STATE_DISCARDING))
            {
                continue;
            }

            for (u4Index = 1; u4Index <= pPortEntry->u2AttrArraySize; u4Index++)
            {
                if ((pMapEntry->ppAttrEntryArray[u4Index] == NULL) ||
                    (MRP_GET_ATTR_REG_SEM_STATE
                     (pPortEntry->pu1AttrInfoList[u4Index]) != MRP_IN))
                {
                    continue;
                }
                IfMsg.pAppEntry = pAppEntry;
                IfMsg.u2MapId = pMapEntry->u2MapId;
                IfMsg.u2Port = pPortEntry->u2Port;

                if (pNewPortEntry == NULL)
                {
                    pNewPortEntry = MrpMapDSCreateMrpMapPortEntry (pMapEntry,
                                                                   u2Port);

                    if (pNewPortEntry == NULL)
                    {
                        MRP_TRC ((pAppEntry->pMrpContextInfo,
                                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                                  "MrpMapHandleMapUpdateMapPorts: Unable to "
                                  "add this port %d in Map %d\n", u2Port,
                                  u2MapId));
                        return;
                    }
                }

                MrpMadMapAttributeJoinReq
                    (&pMapEntry->ppAttrEntryArray[u4Index]->Attr,
                     pNewPortEntry, &IfMsg, pMapEntry, MRP_SEND_JOIN);
            }
        }
    }
    else if (u1Action == MRP_DELETE)
    {
        /* First do the steps that is done when a port moves
         * to blocking state. */

        if (pNewPortEntry == NULL)
        {
            MRP_TRC ((pAppEntry->pMrpContextInfo,
                      (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                      "MrpMapHandleMapUpdateMapPorts: "
                      "No such port Entry present\n"));
            return;
        }

        MrpUtilPortStateChgedToBlocking (MRP_CONTEXT_PTR (u4ContextId),
                                         pAppEntry, pMapEntry, pNewPortEntry);

        /* Remove all the attribute present in the port for that MAP */
        MrpMapDSDelAllAttrInMapOnPort (pMapEntry, pNewPortEntry);
        MrpMapDsCheckAndDeleteMapEntry (pMapEntry);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapMadAttributeLeaveInd
 *                                                                          
 *    DESCRIPTION      : This function indicates the Leave message to the 
 *                       ports other than the source port.
 *
 *    INPUT            : pAttr  - pointer to the Mrp Attrribute
 *                       pIfMsg - Pointer to IfMsg Structure
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapMadAttributeLeaveInd (tMrpAttr * pAttr, tMrpIfMsg * pIfMsg)
{
    UINT1              *pPortList = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAttrEntry      *pPrevAttrEntry = NULL;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    tMrpMapPortEntry   *pPrevPortEntry = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    UINT2               u2AttrIndex = 0;
    UINT2               u2MapId = 0;
    UINT2               u2Port = 0;
    UINT1               u1Result = OSIX_FALSE;
    UINT1               u1RegCount = MRP_NO_REGS_FOR_LEAVEMSG;

    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList == NULL)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpMapMadAttributeLeaveInd : Error in allocating memory "
                     "for pPortList\r\n");
        return;
    }
    MEMSET (pPortList, 0, sizeof (tLocalPortList));

    /* We will propagate this Leave only if one other port other than the
     * source port has seen this registration. In this case we will send a
     * LEAVE message on the other port alone. This is done to change the 
     * state of the participant to Observer. In case of no other port having
     * seen the registration we propagate LEAVE_EMPTY to all other ports. */
    pAppEntry = pIfMsg->pAppEntry;
    u2MapId = pIfMsg->u2MapId;
    MrpMapGetPortList (pIfMsg->u4ContextId, pIfMsg->pAppEntry->u1AppId,
                       u2MapId, pPortList);

    if ((pMrpMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
    {
        UtilPlstReleaseLocalPortList (pPortList);
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapMadAttributeLeaveInd: " "No Map Entry present\n"));
        return;
    }

    pAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMrpMapEntry);

    if (pAttrEntry == NULL)
    {
        UtilPlstReleaseLocalPortList (pPortList);
        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpMapMadAttributeLeaveInd: "
                  "Attribute entry not present\n"));
        return;
    }

    u2AttrIndex = pAttrEntry->u2AttrIndex;

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, u2Port);

        if (pMrpMapPortEntry == NULL)
        {
            continue;
        }

        if (u2Port != pIfMsg->u2Port)
        {
            OSIX_BITLIST_IS_BIT_SET (pPortList, u2Port, sizeof (tLocalPortList),
                                     u1Result);

            if (u1Result == OSIX_TRUE)
            {
                /* Checking out for the registration for the attributes
                 * in other ports and the counter is incremented accordingly */

                if ((MRP_GET_ATTR_REG_SEM_STATE
                     (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex])
                     == MRP_IN) &&
                    (pMrpMapPortEntry->u1PortState ==
                     AST_PORT_STATE_FORWARDING))
                {
                    /* Increment the no of registrations for that 
                       attribute */
                    u1RegCount++;

                    /* Store the port entry which had registration for 
                     * the same attribute. This will be used if 
                     * u1RegCount == 1 */
                    pPrevPortEntry = pMrpMapPortEntry;
                    pPrevAttrEntry = pAttrEntry;
                }

                if (u1RegCount > MRP_MIN_REGS_FOR_LEAVEMSG)
                {
                    /* we are not bothered...we are going to ignore this 
                     * LEAVE */
                    break;
                }

            }
        }

    }

    /*
     * if u1RegCount >= 2 - No need to propagate LEAVE
     * if u1RegCount == 1 - Propagate on other port which had seen that
     *                      registration
     * if u1RegCount == 0 - Propagate LEAVE on all other ports
     */
    if (u1RegCount == MRP_NO_REGS_FOR_LEAVEMSG)
    {
        /*
         * This attribute is not registered on any other ports...
         * So propagate LEAVE message to all other ports to make them
         *  Observers from Members
         */
        for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
        {
            pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, u2Port);

            if (pMrpMapPortEntry == NULL)
            {
                continue;
            }

            if (pMrpMapPortEntry->u2Port != pIfMsg->u2Port)
            {
                OSIX_BITLIST_IS_BIT_SET (pPortList, u2Port,
                                         sizeof (tLocalPortList), u1Result);
                if (u1Result == OSIX_TRUE)
                {
                    MrpMadMapAttributeLeaveReq (pAttr, pMrpMapPortEntry,
                                                pIfMsg, pMrpMapEntry);
                }
            }
        }
    }
    else if (u1RegCount == MRP_MIN_REGS_FOR_LEAVEMSG)
    {
        /*
         * This attribute is registered on one port other than the port which
         * received the LEAVE message. So propagate the LEAVE message on this
         * port if the admin reg control equals NORMAL. This is becos we will
         * be a member (sending join ins for attribute reg on the same port)
         * on statically configured ports.
         */
        if (MRP_GET_ATTR_REG_ADMIN_CTRL
            (pPrevPortEntry->pu1AttrInfoList[pPrevAttrEntry->u2AttrIndex])
            == MRP_REG_NORMAL)
        {
            MrpMadMapAttributeLeaveReq (pAttr, pPrevPortEntry, pIfMsg,
                                        pMrpMapEntry);
        }
    }
    UtilPlstReleaseLocalPortList (pPortList);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapGetPortList
 *                                                                          
 *    DESCRIPTION      : Fills the list of ports present in this MAP Context
 *
 *    INPUT            : u4ContextId  - Context Identifier
 *                       u1AppId      - Application Index
 *                       u2MapId      - MAP Context identifier
 *                                                                          
 *    OUTPUT           : MapPortList  - Portlist containing the list of ports
 *                                      that are member of this MAP.
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapGetPortList (UINT4 u4ContextId, UINT1 u1AppId, UINT2 u2MapId,
                   tLocalPortList MapPortList)
{
    if (u1AppId == MRP_MVRP_APP_ID)
    {
        MEMSET (MapPortList, MRP_INIT_WITH_ONE, sizeof (tLocalPortList));
    }
    else
    {
        MEMSET (MapPortList, 0, sizeof (tLocalPortList));
        MrpPortGetVlanLocalEgressPorts (u4ContextId, u2MapId, MapPortList);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapSyncAttributeTableForMap
 *                                                                          
 *    DESCRIPTION      : Synchronises the attibute table for a given port 
 *                       for a given MAP. This function will be called 
 *                       whenever a port moves to forwarding state for a
 *                       particular MAP. This function transmits join 
 *                       messages on the given port for the given MAP for 
 *                       all the attributes registered on other ports.
 *
 *    INPUT            : pMrpContextInfo  - Pointer to the Context Information
 *                       pMrpMapEntry     - Pointer to the MAP structure
 *                       pIfMsg           - Pointer to the MrpIfMsg Structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMapSyncAttributeTableForMap (tMrpContextInfo * pMrpContextInfo,
                                tMrpMapEntry * pMrpMapEntry, tMrpIfMsg * pIfMsg)
{
    UINT1              *pMapPortList = NULL;
    tMrpAppEntry       *pMrpAppEntry = pIfMsg->pAppEntry;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    tMrpMapPortEntry   *pMrpTmpMapPortEntry = NULL;
    UINT1              *pu1AttrPtr = NULL;
    UINT2               u2Port = 0;
    UINT2               u2Index = 0;
    UINT1               u1Result = OSIX_TRUE;

    pMapPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pMapPortList == NULL)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpMapSyncAttributeTableForMap : Error in allocating memory "
                     "for pMapPortList\r\n");
        return;
    }
    MEMSET (pMapPortList, 0, sizeof (tLocalPortList));

    MrpMapGetPortList (pMrpContextInfo->u4ContextId, pMrpAppEntry->u1AppId,
                       pIfMsg->u2MapId, pMapPortList);

    OSIX_BITLIST_IS_BIT_SET (pMapPortList, pIfMsg->u2Port,
                             sizeof (tLocalPortList), u1Result);
    UtilPlstReleaseLocalPortList (pMapPortList);

    if (u1Result == OSIX_FALSE)
    {
        return;
    }

    pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMrpMapEntry, pIfMsg->u2Port);

    if (pMrpMapPortEntry == NULL)
    {
        pMrpMapPortEntry = MrpMapDSCreateMrpMapPortEntry (pMrpMapEntry,
                                                          pIfMsg->u2Port);
        if (pMrpMapPortEntry == NULL)
        {
            return;
        }
    }

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pMrpTmpMapPortEntry = pMrpMapEntry->apMapPortEntry[u2Port];

        if ((pMrpTmpMapPortEntry == NULL) ||
            (pMrpTmpMapPortEntry->u1PortState == AST_PORT_STATE_DISCARDING) ||
            (pMrpTmpMapPortEntry->u2Port == pIfMsg->u2Port))
        {
            continue;
        }

        pu1AttrPtr = pMrpTmpMapPortEntry->pu1AttrInfoList;

        for (u2Index = 1; u2Index <= pMrpTmpMapPortEntry->u2AttrArraySize;
             u2Index++)
        {
            if ((pMrpMapEntry->ppAttrEntryArray[u2Index] == NULL) ||
                (MRP_GET_ATTR_REG_SEM_STATE (pu1AttrPtr[u2Index]) != MRP_IN))
            {
                continue;
            }

            MrpMadMapAttributeJoinReq
                (&(pMrpMapEntry->ppAttrEntryArray[u2Index]->Attr),
                 pMrpMapPortEntry, pIfMsg, pMrpMapEntry, MRP_SEND_NEW);
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapCreateAppAttrReqJoin   
 *                                                                          
 *    DESCRIPTION      : This function validates the entry whenever MRP
 *                       module processes the Join.Request for an attribute
 *                       It creates the MAP entry and AttrEntry in case, these
 *                       are not already created. And if these are created 
 *                       newly, then it updates the value of pu1SendNewInd as
 *                       OSIX_TRUE
 *
 *    INPUT            : pAppEntry - Pointer to the application entry.
 *                       pAttr     - pointer to Mrp Attribute structure
 *                       u2MapId   - MAP Identifier
 *                       u2Port    - Port that needs to be added.
 *
 *    OUTPUT           : *pAttrEntry - Attribute Entry.
 *                       pu1SendNewInd - Indicates whether new msg needs to
 *                                       be sent.
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapCreateAppAttrReqJoin (tMrpAppEntry * pAppEntry, tMrpAttr * pAttr,
                            tMrpAttrEntry ** ppAttrEntryArray, UINT2 u2MapId,
                            UINT1 *pu1SendNewInd)
{
    tMrpMapEntry       *pMapEntry = NULL;

    /* Get the MrpMapEntry using the MAP Id */
    if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
    {
        /* if it not present then create the MAP and initialise the 
         * values */
        pMapEntry = MrpMapDSCreateMrpMapEntry (pAppEntry, u2MapId);

        if (pMapEntry == NULL)
        {
            MRP_TRC ((pAppEntry->pMrpContextInfo,
                      (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                      "MrpMapCreateAppAttrReqJoin: No Free Map Entry \n"));

            return OSIX_FAILURE;
        }
    }

    *ppAttrEntryArray = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMapEntry);

    if (*ppAttrEntryArray == NULL)
    {
        /* Attribute entry not present, so creating new attribute entry */
        *ppAttrEntryArray = MrpMapDSCreateMrpMapAttrEntry (pMapEntry, pAttr);

        if (*ppAttrEntryArray == NULL)
        {
            MRP_TRC ((pAppEntry->pMrpContextInfo,
                      (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                      "MrpMapCreateAppAttrReqJoin:"
                      "No Free Attribute Entry \n"));
            MrpMapDSDeleteMrpMapEntry (pMapEntry);

            return OSIX_FAILURE;
        }

        /* This flag is used to indicate new declaration of an attribute. 
         * This flag is TRUE then we need to send NEW indication to all
         * other ports */

        *pu1SendNewInd = OSIX_TRUE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapAttrReqJoinOnPort          
 *                                                                          
 *    DESCRIPTION      : This function updates the attribue in the MAP 
 *                       Port entry and triggers the SEM for this port.    
 *
 *    INPUT            : pAppEntry - Pointer to the application entry.
 *                       pMapEntry - Pointer to Map entry
 *                       pAttrEntry- pointer to Mrp Attribute structure
 *                       u2Port    - Port that needs to be added.
 *
 *    OUTPUT           : pu1PortState - Port State for this Map info
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapAttrReqJoinOnPort (tMrpAppEntry * pAppEntry, tMrpMapEntry * pMapEntry,
                         tMrpAttrEntry * pAttrEntry, UINT2 u2Port,
                         UINT1 *pu1PortState)
{
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    UINT2               u2AttrIndex = 0;

    u2AttrIndex = pAttrEntry->u2AttrIndex;
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pAppEntry);

    /* Add a attribute entry if the port is a static port (i4Result == TRUE)
     * irrespective of whether the port is in Forwarding/Blocking state. */
    pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

    if (pMrpMapPortEntry == NULL)
    {
        pMrpMapPortEntry = MrpMapDSCreateMrpMapPortEntry (pMapEntry, u2Port);

        if (pMrpMapPortEntry == NULL)
        {
            MRP_TRC ((pAppEntry->pMrpContextInfo,
                      (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                      "MrpMapAttrReqJoinOnPort:" "No Free MAP Port Entry \n"));
            return OSIX_FAILURE;
        }
    }

    if (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex] == 0)
    {
        /* If the value of info list is 0 means, the attribute was not 
         * added in the MapPortEntry. So need to add. */
        if (MrpMapDSAddAttrToMapPortEntry (pMrpMapPortEntry, pAttrEntry)
            != OSIX_SUCCESS)
        {
            MRP_TRC ((pAppEntry->pMrpContextInfo,
                      (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                      "MrpMapAttrReqJoinOnPort:"
                      "Adding attribute to MrpMapPortEntry Failed \n"));

            MrpMapDSDeleteMrpMapPortEntry (pMrpMapPortEntry);
            return OSIX_FAILURE;
        }
    }
    else                        /* Attribute entry already present */
    {
        if (MRP_GET_ATTR_REG_SEM_STATE
            (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_LV)
        {
            MrpTmrStopLeaveTmr (pMrpMapPortEntry, (UINT4) u2AttrIndex);
        }
    }

    MRP_SET_ATTR_REG_SEM_STATE
        (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex], MRP_IN);

    if (MRP_GET_ATTR_REG_ADMIN_CTRL
        (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex]) != MRP_REG_FIXED)
    {
        MRP_SET_ATTR_REG_ADMIN_CTRL
            (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex], MRP_REG_FIXED);
        pAttrEntry->u2StaticMemPortCnt++;
    }

    *pu1PortState = pMrpMapPortEntry->u1PortState;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMapAttrReqJoinOnPort          
 *                                                                          
 *    DESCRIPTION      : This function updates the attribue in the MAP 
 *                       Port entry and triggers the SEM for this port.    
 *
 *    INPUT            : pAppEntry - Pointer to the application entry.
 *                       pMapEntry - Pointer to Map entry
 *                       pAttr     - pointer to Mrp Attribute structure
 *                       pAttrEntry- pointer to Mrp Attribute structure
 *                       u2Port    - Port that needs to be added.
 *
 *    OUTPUT           : NONE 
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMapAttrSetForbidenPort (tMrpAppEntry * pAppEntry, tMrpMapEntry * pMapEntry,
                           tMrpAttrEntry * pAttrEntry, tMrpAttr * pAttr,
                           UINT2 u2Port)
{
    UINT1              *pTmpDelPorts = NULL;
    tMrpMapPortEntry   *pMrpMapPortEntry = NULL;
    UINT2               u2AttrIndex = pAttrEntry->u2AttrIndex;

    if ((0 == pAppEntry->u1AppId) || (pAppEntry->u1AppId > MRP_MAX_APPS))
    {
        /* Added to avoid Klocwork warning */
        return OSIX_FAILURE;
    }

    /* Validation for pMrpAppPortEntry should be taken care of
     * invoking function
     * */

    /* Port present in added portlist */
    pMrpMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

    if (pMrpMapPortEntry == NULL)
    {
        pMrpMapPortEntry = MrpMapDSCreateMrpMapPortEntry (pMapEntry, u2Port);

        if (pMrpMapPortEntry == NULL)
        {
            MrpMapDSDelAttrEntryFromMapEntry (pMapEntry, pAttrEntry);

            MRP_TRC ((pAppEntry->pMrpContextInfo,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "MrpMapAppSetForbiddenPorts:"
                      "No Free MAP Port Entry \n"));

            return OSIX_FAILURE;
        }
    }

    if (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex] == 0)
    {
        /* If the value of info list is 0 means, the attribute was not
         * added in the MapPortEntry. So need to add. */
        if (MrpMapDSAddAttrToMapPortEntry (pMrpMapPortEntry, pAttrEntry)
            != OSIX_SUCCESS)
        {
            MRP_TRC ((pAppEntry->pMrpContextInfo,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "MrpMapAppSetForbiddenPorts: "
                      "Adding attribute to MrpMapPortEntry Failed \n"));

            MrpMapDSDeleteMrpMapPortEntry (pMrpMapPortEntry);
            MrpMapDSDelAttrEntryFromMapEntry (pMapEntry, pAttrEntry);
            return OSIX_FAILURE;
        }
    }

    if (MRP_GET_ATTR_REG_SEM_STATE
        (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex]) != MRP_MT)
    {
        if (MRP_GET_ATTR_REG_SEM_STATE
            (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_LV)
        {
            MrpTmrStopLeaveTmr (pMrpMapPortEntry, (UINT4) u2AttrIndex);
        }

        /* If the Attribute was registered on this port,
         * then give the Leave Indication to the Application. */
        gMrpGlobalInfo.aMrpAppnFn[pAppEntry->u1AppId].pAttrIndFn
            (pMapEntry, *pAttr, u2Port, (UINT1) MRP_HL_IND_LEAVE);

        if (pMrpMapPortEntry->u1PortState == AST_PORT_STATE_FORWARDING)
        {
            /*
             * If the Attribute was registered previously,
             * then other ports would have been made Members. Since
             * When the Port is made a Forbidden Port, we must
             * indicate this to other ports, to make then Observers.
             * MrpMapAppAttributeReqLeave (), will make other ports
             * Observers, if there is no other port, that is statically
             * configured.
             */

            pTmpDelPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

            if (pTmpDelPorts == NULL)
            {
                MRP_GBL_TRC (MRP_RED_TRC,
                             "MrpMapAttrSetForbidenPort: Error in allocating memory "
                             "for pTmpDelPorts\r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pTmpDelPorts, 0, sizeof (tLocalPortList));

            OSIX_BITLIST_SET_BIT (pTmpDelPorts, pMrpMapPortEntry->u2Port,
                                  sizeof (tLocalPortList));

            MrpMapAppAttributeReqLeave (pAppEntry,
                                        &pAttrEntry->Attr,
                                        pMapEntry->u2MapId, pTmpDelPorts);
            UtilPlstReleaseLocalPortList (pTmpDelPorts);
        }
    }

    MRP_SET_ATTR_REG_ADMIN_CTRL
        (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex], MRP_REG_FORBIDDEN);
    MRP_SET_ATTR_REG_SEM_STATE
        (pMrpMapPortEntry->pu1AttrInfoList[u2AttrIndex], MRP_MT);

    return OSIX_SUCCESS;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpmap.c                        */
/*-----------------------------------------------------------------------*/
