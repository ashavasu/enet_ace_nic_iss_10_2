/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: mrprdstb.c,v 1.3 2009/12/23 16:33:15 prabuc Exp $
 *
 * Description: This file contains MRP High Availability related
 *              stub routines.
 *********************************************************************/
#ifndef L2RED_WANTED
#include "mrpinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedSendSyncUpMessages 
 *
 *    DESCRIPTION      : This function contstructs and sends the dynamic sync 
 *                       up message to RM which in turn will send the message 
 *                       to the MRP module in the other node.
 *
 *    INPUT            : u1MsgType - Message type
 *                       pMsgInfo  - Pointer to information to be filled in 
 *                                   the synchronization message
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *     
 ****************************************************************************/
INT4
MrpRedSendSyncUpMessages (UINT1 u1MsgType, tMrpRedMsgInfo * pMsgInfo)
{
    UNUSED_PARAM (u1MsgType);
    UNUSED_PARAM (pMsgInfo);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * 
 * FUNCTION NAME      : MrpPortDeRegisterWithRM 
 *                                                                           
 * DESCRIPTION        : This function DeRegisters/UnRegisters MRP from RM                 
 *                                                                           
 * INPUT              : NONE 
 *                                                                           
 * OUTPUT             : NONE                                                 
 *                                                                           
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE                            
 *                                                                           
 ****************************************************************************/
INT4
MrpPortDeRegisterWithRM (VOID)
{
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedRegisterWithRM
 *
 *    DESCRIPTION      : Registeres MRP with Redundancy Manager
 *                       
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpRedRegisterWithRM (VOID)
{
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpRedProcessRmMsg 
 *
 *    DESCRIPTION      : This function processes the received RM events
 *
 *    INPUT            : pRmMsg - Pointer to the structure containing RM
 *                                buffer and the RM event
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : NONE
 *     
 ****************************************************************************/
VOID
MrpRedProcessRmMsg (tMrpRmCtrlMsg * pRmMsg)
{
    UNUSED_PARAM (pRmMsg);
}

/*****************************************************************************
 *
 * FUNCTION NAME      : MrpPortRelRmMsgMemory 
 * 
 * DESCRIPTION        : This function calls the RM API to release the memory
 *                      allocated for RM message.
 *
 * INPUT              : pu1Block - Memory block 
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE                            
 *                                                                           
 ****************************************************************************/
INT4
MrpPortRelRmMsgMemory (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
}

#endif
