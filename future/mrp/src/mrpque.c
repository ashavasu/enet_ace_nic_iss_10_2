/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpque.c,v 1.12 2013/09/28 07:57:37 siva Exp $
 *
 * Description: This file contains procedures related to 
 *              - Processing QMsgs
 *              - Enqing QMsg from external modules to MRP task 
 *********************************************************************/

#include "mrpinc.h"

/* Private functions for this file */
PRIVATE VOID MrpQueProcessVlanMapCfgEvent PROTO ((tMrpQMsg *));
PRIVATE VOID MrpQueProcessBulkCfgEvent PROTO ((tMrpQMsg *));
PRIVATE INT4        MrpQueHandleTcDetectedTmrInd
PROTO ((tMrpContextInfo * pContextInfo, UINT2 u2Port, UINT2 u2MapId,
        UINT1 u1TmrState));

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpQuePostCfgMessage
 *
 *    DESCRIPTION      : Posts the message to MRP Message Queue
 *
 *    INPUT            : u1MsgType - Message Type
 *                       u2LocalPortId - Local Port Index
 *                       u4ContextId - Context Id
 *                       u4IfIndex - Physical Interface Index.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpQuePostCfgMessage (UINT1 u1MsgType, UINT2 u2LocalPortId,
                      UINT4 u4ContextId, UINT4 u4IfIndex)
{
    tMrpQMsg           *pMrpQMsg = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                     "MrpQuePostCfgMessage: MRP is Shutdown on this "
                     "context\r\n");

        return i4RetVal;
    }

    /* Allocate the Cfg Queue message buffer. This buffer will be released by 
     * the mrpque.c file after processing the dequeued messages */
    pMrpQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pMrpQMsg)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC,
                     "MrpQuePostCfgMessage: Queue Message Memory Allocation "
                     "failed\r\n");

        return i4RetVal;
    }

    MEMSET (pMrpQMsg, 0, sizeof (tMrpQMsg));
    pMrpQMsg->u2MsgType = MRP_MSG;

    pMrpQMsg->unMrpMsg.MrpMsg.u2MsgType = u1MsgType;
    pMrpQMsg->unMrpMsg.MrpMsg.u2Port = u2LocalPortId;
    pMrpQMsg->u4IfIndex = u4IfIndex;
    pMrpQMsg->u4ContextId = u4ContextId;

    i4RetVal = MrpQueEnqMsg (pMrpQMsg);

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpQueMsgHandler 
 *                                                                          
 *    DESCRIPTION      : This function dequeues the queue messages 
 *                       received by the MRP task. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
MrpQueMsgHandler (VOID)
{
    tMrpQMsg           *pMrpQMsg = NULL;

    while (OsixQueRecv (gMrpGlobalInfo.MsgQId, (UINT1 *) &pMrpQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (NULL == pMrpQMsg)
        {
            /* Add to avoid kloc-work warning */
            continue;
        }

        MRP_LOCK ();

        switch (pMrpQMsg->u2MsgType)
        {
            case MRP_VLAN_INST_MAP_MSG:
                /* Intentional fallthrough */
            case MRP_VLAN_LIST_INST_MAP_MSG:

                MrpQueProcessVlanMapCfgEvent (pMrpQMsg);
                MemReleaseMemBlock (gMrpGlobalInfo.VlanInstMapPoolId,
                                    (UINT1 *) pMrpQMsg);

                break;

            case MRP_BULK_MSG:

                MrpQueProcessBulkCfgEvent (pMrpQMsg);
                MemReleaseMemBlock (gMrpGlobalInfo.QBulkMsgPoolId,
                                    (UINT1 *) pMrpQMsg);

                break;

            case MRP_RM_MSG:
                MrpRedProcessRmMsg (&(pMrpQMsg->unMrpMsg.MrpRmMsg));
                MemReleaseMemBlock (gMrpGlobalInfo.QMsgPoolId,
                                    (UINT1 *) pMrpQMsg);
                break;

            case MRP_MSG:
                /* Intentional fallthrough */
            default:

                MrpQueProcessMsg (pMrpQMsg);
                MemReleaseMemBlock (gMrpGlobalInfo.QMsgPoolId,
                                    (UINT1 *) pMrpQMsg);

                break;

        }                        /* Switch u2MsgType */

        /* relinquish lock here */
        MRP_UNLOCK ();
    }

}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpQueProcessMsg
 *
 *    DESCRIPTION      : This function processes the received qeueue messages
 *
 *    INPUT            : pMrpQMsg - Pointer to the queue message
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
MrpQueProcessMsg (tMrpQMsg * pMrpQMsg)
{
    tMrpContextInfo    *pContextInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT2               u2Port = 0;
    UINT2               u2MsgType = 0;
    BOOLEAN             bResult = OSIX_FALSE;

    u2MsgType = pMrpQMsg->unMrpMsg.MrpMsg.u2MsgType;

    if (MRP_IS_VC_VALID (pMrpQMsg->u4ContextId) == OSIX_FALSE)
    {
        L2MI_SYNC_GIVE_SEM ();
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpQueProcessMsg: Invalid Context Id.\n");
        return;
    }

    if (u2MsgType != MRP_CREATE_CONTEXT_MSG)
    {
        pContextInfo = MRP_CONTEXT_PTR (pMrpQMsg->u4ContextId);

        if (NULL == pContextInfo)
        {
            MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                         "MrpQueProcessMsg: Context Not initialized.\n");
            return;
        }
    }

    u2Port = pMrpQMsg->unMrpMsg.MrpMsg.u2Port;

    switch (u2MsgType)
    {
        case MRP_PORT_MAP_MSG:
            /* MAP port will be considered as Creation of a Port */
        case MRP_PORT_CREATE_MSG:
            i4RetVal = MrpIfHandleCreatePort (pContextInfo,
                                              pMrpQMsg->u4IfIndex, u2Port);

            if (i4RetVal == OSIX_FAILURE)
            {
                MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                          "MrpQueProcessMsg: MRP Port Creation failed "
                          "for Port %d \n", pMrpQMsg->u4IfIndex));

            }

            if (u2MsgType == MRP_PORT_MAP_MSG)
            {
                L2MI_SYNC_GIVE_SEM ();
            }
            else
            {
                L2_SYNC_GIVE_SEM ();
            }

            break;

        case MRP_PORT_UNMAP_MSG:
            /* UNMAP port will be considered as Deletion of a Port */
        case MRP_PORT_DELETE_MSG:
            i4RetVal = MrpIfHandleDeletePort (pContextInfo, u2Port);

            if (i4RetVal == OSIX_FAILURE)
            {
                MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                          "MrpQueProcessMsg : MRP Port Deletion failed for "
                          "Port %d \n", pMrpQMsg->u4IfIndex));

            }

            if (u2MsgType == MRP_PORT_UNMAP_MSG)
            {
                L2MI_SYNC_GIVE_SEM ();
            }

            break;

        case MRP_PORT_OPER_UP_MSG:

            i4RetVal =
                MrpIfHandlePortOperInd (pContextInfo, u2Port, MRP_OPER_UP);

            if (i4RetVal == OSIX_FAILURE)
            {

                MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                          "MrpQueProcessMsg : MRP Port Oper UP Ind failed "
                          "for Port %d \n", pMrpQMsg->u4IfIndex));
            }
            break;

        case MRP_PORT_OPER_DOWN_MSG:

            i4RetVal =
                MrpIfHandlePortOperInd (pContextInfo, u2Port, MRP_OPER_DOWN);

            if (i4RetVal == OSIX_FAILURE)
            {

                MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                          "MrpQueProcessMsg :MRP Port Oper DOWN Ind failed"
                          " for Port %d \n", pMrpQMsg->u4IfIndex));
            }
            break;

        case MRP_PORT_OPER_P2P_MSG:

            MRP_TRC ((pContextInfo, CONTROL_PLANE_TRC,
                      "MrpQueProcessMsg :Received MRP_PORT_OPER_P2P_MSG "
                      "for Port %d \n", pMrpQMsg->u4IfIndex));

            MrpIfHandlePortOperP2PStatusChg (pContextInfo, u2Port,
                                             pMrpQMsg->u1MsgInfo);
            break;

        case MRP_PORT_ROLE_CHG_MSG:

            MRP_TRC ((pContextInfo, CONTROL_PLANE_TRC,
                      "MrpQueProcessMsg : Received MRP_PORT_ROLE_CHG_MSG "
                      "for Port %d \n", pMrpQMsg->u4IfIndex));

            MrpIfHandlePortRoleChgEvent (pContextInfo, u2Port,
                                         pMrpQMsg->unMrpMsg.MrpMsg.u2MapId,
                                         pMrpQMsg->u1MsgInfo);
            break;

        case MRP_TCDETECTED_TMR_STATUS:

            MRP_TRC ((pContextInfo, CONTROL_PLANE_TRC,
                      "MrpQueProcessMsg:Received MRP_TCDETECTED_TMR_STATUS"
                      " for Port %d \n", pMrpQMsg->u4IfIndex));

            MrpQueHandleTcDetectedTmrInd (pContextInfo, u2Port,
                                          pMrpQMsg->unMrpMsg.MrpMsg.u2MapId,
                                          pMrpQMsg->u1MsgInfo);
            break;

        case MRP_STAP_PORT_FWD_MSG:

            MrpIfHandleStapPortChange (pContextInfo, u2Port,
                                       AST_PORT_STATE_FORWARDING,
                                       pMrpQMsg->unMrpMsg.MrpMsg.u2MapId);
            break;

        case MRP_STAP_PORT_BLK_MSG:

            MrpIfHandleStapPortChange (pContextInfo, u2Port,
                                       AST_PORT_STATE_DISCARDING,
                                       pMrpQMsg->unMrpMsg.MrpMsg.u2MapId);
            break;

        case MRP_PROP_VLAN_INFO_MSG:

            MrpVlanHandlePropagateVlanInfo (pMrpQMsg->u4ContextId,
                                            (tVlanId)
                                            pMrpQMsg->unMrpMsg.MrpMsg.u2MapId,
                                            u2Port,
                                            pMrpQMsg->unMrpMsg.MrpMsg.
                                            AddedPorts,
                                            pMrpQMsg->unMrpMsg.MrpMsg.
                                            DeletedPorts);
            break;

        case MRP_PROP_MAC_INFO_MSG:

            MrpVlanHandlePropagateMacInfo (pMrpQMsg->u4ContextId,
                                           pMrpQMsg->unMrpMsg.MrpMsg.MacAddr,
                                           pMrpQMsg->unMrpMsg.MrpMsg.u2MapId,
                                           u2Port,
                                           pMrpQMsg->unMrpMsg.MrpMsg.
                                           AddedPorts,
                                           pMrpQMsg->unMrpMsg.MrpMsg.
                                           DeletedPorts);

            break;

        case MRP_PROP_FWDALL_INFO_MSG:
            MrpVlanHandlePropagateDefGrpInfo (pMrpQMsg->u4ContextId,
                                              MRP_MMRP_ALL_GROUPS,
                                              pMrpQMsg->unMrpMsg.MrpMsg.u2MapId,
                                              u2Port,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              AddedPorts,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              DeletedPorts);
            break;

        case MRP_PROP_FWDUNREG_INFO_MSG:
            MrpVlanHandlePropagateDefGrpInfo (pMrpQMsg->u4ContextId,
                                              MRP_MMRP_UNREG_GROUPS,
                                              pMrpQMsg->unMrpMsg.MrpMsg.u2MapId,
                                              u2Port,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              AddedPorts,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              DeletedPorts);
            break;

        case MRP_SET_VLAN_FORBID_MSG:
            MrpVlanHandleSetVlanForbiddPorts (pMrpQMsg->u4ContextId,
                                              pMrpQMsg->unMrpMsg.MrpMsg.u2MapId,
                                              u2Port,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              AddedPorts,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              DeletedPorts);
            break;

        case MRP_SET_MCAST_FORBID_MSG:
            MrpVlanHandleSetMcastForbidPorts (pMrpQMsg->u4ContextId,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              MacAddr,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              u2MapId,
                                              u2Port,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              AddedPorts,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              DeletedPorts);

            break;

        case MRP_SET_FWDALL_FORBID_MSG:
            MrpVlanHndleSetDefGrpForbidPorts (pMrpQMsg->u4ContextId,
                                              MRP_MMRP_ALL_GROUPS,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              u2MapId,
                                              u2Port,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              AddedPorts,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              DeletedPorts);

            break;

        case MRP_SET_FWDUNREG_FORBID_MSG:
            MrpVlanHndleSetDefGrpForbidPorts (pMrpQMsg->u4ContextId,
                                              MRP_MMRP_UNREG_GROUPS,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              u2MapId,
                                              u2Port,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              AddedPorts,
                                              pMrpQMsg->unMrpMsg.MrpMsg.
                                              DeletedPorts);

            break;

        case MRP_UPDATE_MAP_PORTS_MSG:
            for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
            {
                OSIX_BITLIST_IS_BIT_SET (pMrpQMsg->unMrpMsg.MrpMsg.
                                         AddedPorts, u2Port,
                                         sizeof (tLocalPortList), bResult);
                if (bResult == OSIX_TRUE)
                {
                    MrpMapHandleMapUpdateMapPorts (pMrpQMsg->u4ContextId,
                                                   u2Port,
                                                   pMrpQMsg->unMrpMsg.MrpMsg.
                                                   u2MapId, MRP_ADD);

                }

                OSIX_BITLIST_IS_BIT_SET (pMrpQMsg->unMrpMsg.MrpMsg.
                                         DeletedPorts, u2Port,
                                         sizeof (tLocalPortList), bResult);
                if (bResult == OSIX_TRUE)
                {
                    MrpMapHandleMapUpdateMapPorts (pMrpQMsg->u4ContextId,
                                                   u2Port,
                                                   pMrpQMsg->unMrpMsg.MrpMsg.
                                                   u2MapId, MRP_DELETE);
                }
            }
            break;

        case MRP_CREATE_CONTEXT_MSG:

            i4RetVal = MrpCtxtHandleCreateContext (pMrpQMsg->u4ContextId);

            if (i4RetVal == OSIX_FAILURE)
            {
                MRP_GBL_TRC (ALL_FAILURE_TRC, "MRP context Creation failed.");

            }

            L2MI_SYNC_GIVE_SEM ();

            break;

        case MRP_DELETE_CONTEXT_MSG:

            i4RetVal = MrpCtxtHandleDeleteContext (pMrpQMsg->u4ContextId);

            if (i4RetVal == OSIX_FAILURE)
            {
                MRP_GBL_TRC (ALL_FAILURE_TRC, "MRP context Deletion failed.");
            }

            L2MI_SYNC_GIVE_SEM ();

            break;

        case MRP_UPDATE_CONTEXT_NAME:

            i4RetVal = MrpCtxtHandleUpdateContextName (pMrpQMsg->u4ContextId);

            if (i4RetVal == OSIX_FAILURE)
            {

                MRP_GBL_TRC (ALL_FAILURE_TRC,
                             "MRP context name updation failed.");
            }
            break;
        default:
            break;
    }

}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpQueProcessVlanMapCfgEvent
 *
 *    DESCRIPTION      : This Function Process the received VLAN Map
 *                       configuration event
 *
 *    INPUT            : pMrpQMsg - Pointer to the Queue Message
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpQueProcessVlanMapCfgEvent (tMrpQMsg * pMrpQMsg)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpVidMapMsg      *pMrpVidMapMsg = NULL;
    tVlanMapInfo       *pVlanMapInfo = NULL;
    INT4                i4MsgIndex = 0;
    INT4                i4EventIndex = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2NewInstance = 0;

    u4ContextId = pMrpQMsg->u4ContextId;

    if (MRP_IS_VC_VALID (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpQueProcessVlanMapCfgEvent: Invalid Context Id.\n");
        return;
    }
    pContextInfo = MRP_CONTEXT_PTR (u4ContextId);

    if (NULL == pContextInfo)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpQueProcessVlanMapCfgEvent: Context Not initialized\n");
        return;
    }

    if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                     "MrpQueProcessVlanMapCfgEvent: MRP is Shutdown on this "
                     "context\r\n");
        return;
    }

    /* Currently the loop executes only once */
    for (i4MsgIndex = 0; i4MsgIndex < pMrpQMsg->i4Count; i4MsgIndex++)
    {
        pMrpVidMapMsg = &(pMrpQMsg->unMrpMsg.MrpVidMapMsg[i4MsgIndex]);

        u2NewInstance = pMrpVidMapMsg->u2NewMapId;

        switch (pMrpVidMapMsg->u2MsgEvent)
        {
            case MRP_MAP_INST_VLAN_MSG:
                /* Fall Through */
            case MRP_UNMAP_INST_VLAN_MSG:

                for (i4EventIndex = 0;
                     i4EventIndex < pMrpVidMapMsg->i4EventCount; i4EventIndex++)
                {
                    pVlanMapInfo = &(pMrpVidMapMsg->VlanMapInfo[i4EventIndex]);

                    MrpRemapHandleVlanInstanceMap (u4ContextId,
                                                   pVlanMapInfo->VlanId,
                                                   pVlanMapInfo->u2MapId,
                                                   u2NewInstance);
                }

                break;

            case MRP_MAP_INST_VLAN_LIST_MSG:
                /* Fall Through */
            case MRP_UNMAP_INST_VLAN_LIST_MSG:

                for (i4EventIndex = 0;
                     i4EventIndex < pMrpVidMapMsg->i4EventCount; i4EventIndex++)
                {
                    pVlanMapInfo = &(pMrpVidMapMsg->VlanMapInfo[i4EventIndex]);

                    /* Take care to validate the presence of MVRP/MMRP before
                     * actually handing the respective application changes */

                    MrpRemapHandleVlanInstanceMap (u4ContextId,
                                                   pVlanMapInfo->VlanId,
                                                   pVlanMapInfo->u2MapId,
                                                   u2NewInstance);
                }

                break;

            default:
                break;
        }
    }

    return;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpQueProcessBulkCfgEvent
 *
 *    DESCRIPTION      : This Function Process the received bulk 
 *                       configuration event
 *
 *    INPUT            : pMrpQMsg - Pointer to the Queue Message
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpQueProcessBulkCfgEvent (tMrpQMsg * pMrpQMsg)
{
    tMrpBulkMsg        *pMrpBulkMsg = NULL;
    INT4                i4MsgIndex = 0;

    if (MRP_IS_MRP_STARTED (pMrpQMsg->u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC ((OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                     "MrpQueProcessBulkCfgEvent: MRP is Shutdown on this "
                     "context\r\n");
        return;
    }

    for (i4MsgIndex = 0; i4MsgIndex < pMrpQMsg->i4Count; i4MsgIndex++)
    {
        pMrpBulkMsg = &(pMrpQMsg->unMrpMsg.MrpBulkMsg[i4MsgIndex]);

        switch (pMrpBulkMsg->u2MsgType)
        {
            case MRP_PROP_VLAN_INFO_MSG:

                MrpVlanHandlePropagateVlanInfo (pMrpQMsg->u4ContextId,
                                                pMrpBulkMsg->VlanId,
                                                pMrpBulkMsg->u2Port,
                                                pMrpBulkMsg->Ports,
                                                gMrpNullPortList);
                break;

            case MRP_PROP_MAC_INFO_MSG:

                MrpVlanHandlePropagateMacInfo (pMrpQMsg->u4ContextId,
                                               pMrpBulkMsg->MacAddr,
                                               pMrpBulkMsg->VlanId,
                                               pMrpBulkMsg->u2Port,
                                               pMrpBulkMsg->Ports,
                                               gMrpNullPortList);
                break;

            case MRP_PROP_FWDALL_INFO_MSG:

                MrpVlanHandlePropagateDefGrpInfo (pMrpQMsg->u4ContextId,
                                                  MRP_MMRP_ALL_GROUPS,
                                                  pMrpBulkMsg->VlanId,
                                                  pMrpBulkMsg->u2Port,
                                                  pMrpBulkMsg->Ports,
                                                  gMrpNullPortList);
                break;

            case MRP_PROP_FWDUNREG_INFO_MSG:

                MrpVlanHandlePropagateDefGrpInfo (pMrpQMsg->u4ContextId,
                                                  MRP_MMRP_UNREG_GROUPS,
                                                  pMrpBulkMsg->VlanId,
                                                  pMrpBulkMsg->u2Port,
                                                  pMrpBulkMsg->Ports,
                                                  gMrpNullPortList);
                break;

            case MRP_SET_VLAN_FORBID_MSG:

                MrpVlanHandleSetVlanForbiddPorts (pMrpQMsg->u4ContextId,
                                                  pMrpBulkMsg->VlanId,
                                                  pMrpBulkMsg->u2Port,
                                                  pMrpBulkMsg->Ports,
                                                  gMrpNullPortList);
                break;

            case MRP_SET_MCAST_FORBID_MSG:

                MrpVlanHandleSetMcastForbidPorts (pMrpQMsg->u4ContextId,
                                                  pMrpBulkMsg->MacAddr,
                                                  pMrpBulkMsg->VlanId,
                                                  pMrpBulkMsg->u2Port,
                                                  pMrpBulkMsg->Ports,
                                                  gMrpNullPortList);
                break;

            case MRP_SET_FWDALL_FORBID_MSG:

                MrpVlanHndleSetDefGrpForbidPorts (pMrpQMsg->u4ContextId,
                                                  MRP_MMRP_ALL_GROUPS,
                                                  pMrpBulkMsg->VlanId,
                                                  pMrpBulkMsg->u2Port,
                                                  pMrpBulkMsg->Ports,
                                                  gMrpNullPortList);
                break;

            case MRP_SET_FWDUNREG_FORBID_MSG:

                MrpVlanHndleSetDefGrpForbidPorts (pMrpQMsg->u4ContextId,
                                                  MRP_MMRP_UNREG_GROUPS,
                                                  pMrpBulkMsg->VlanId,
                                                  pMrpBulkMsg->u2Port,
                                                  pMrpBulkMsg->Ports,
                                                  gMrpNullPortList);
                break;

            default:
                break;
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpQueRxPduHandler 
 *                                                                          
 *    DESCRIPTION      : This function dequeues the MRPDU received by MRP task. 
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
MrpQueRxPduHandler (VOID)
{
    tMrpRxPduQMsg      *pRxPduQMsg = NULL;
    tMrpContextInfo    *pContextInfo = NULL;
    UINT4               u4ContextId = 0;

    /* Event received, dequeue MRPDU for processing */
    while (OsixQueRecv (gMrpGlobalInfo.RxPktQId, (UINT1 *) &pRxPduQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (NULL == pRxPduQMsg)
        {
            /* Added to avoid klockwork warning */
            continue;
        }

        MRP_LOCK ();

        u4ContextId = pRxPduQMsg->u4ContextId;

        pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

        if (NULL == pContextInfo)
        {
            MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                         "MrpQueRxPduHandler: Pkt received on "
                         "invalid context\r\n");

            CRU_BUF_Release_MsgBufChain (pRxPduQMsg->pMrpPdu, OSIX_FALSE);
            /* Releasing the RxPduQMsg Memory to pool */
            MemReleaseMemBlock (gMrpGlobalInfo.PduQMsgPoolId,
                                (UINT1 *) pRxPduQMsg);

            continue;
        }

        if (OSIX_TRUE == MRP_IS_MRP_STARTED (u4ContextId))
        {
            MrpQueHandleIncomingFrame (pRxPduQMsg->pMrpPdu, pContextInfo,
                                       pRxPduQMsg->u2Port, pRxPduQMsg->u2MapId);
        }
        else
        {
            MRP_TRC ((pContextInfo, CONTROL_PLANE_TRC,
                      "MrpQueRxPduHandler: MRP Not Started \n"));
        }

        CRU_BUF_Release_MsgBufChain (pRxPduQMsg->pMrpPdu, OSIX_FALSE);
        /* Releasing the RxPduQMsg Memory to pool */
        MemReleaseMemBlock (gMrpGlobalInfo.PduQMsgPoolId, (UINT1 *) pRxPduQMsg);

        /* relinquish the lock here */
        MRP_UNLOCK ();
    }

    return;
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpQueHandleIncomingFrame
 *
 *    DESCRIPTION      : This function processes the received MRP Frame.
 *
 *   INPUT               pMrpPdu     - Pointer to the received MRP PDU
 *                                     buffer (CRU Buffer)
 *                       pContextInfo- Pointer to Context Info structure
 *                       u2Port      - Local Port Number
 *                       u2MapId     - MAP Id
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
VOID
MrpQueHandleIncomingFrame (tCRU_BUF_CHAIN_HEADER * pMrpPdu,
                           tMrpContextInfo * pContextInfo,
                           UINT2 u2Port, UINT2 u2MapId)
{
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMacAddr            DestAddr;
    tMrpIfMsg           IfMsg;
    UINT4               u4FrameSize = 0;
    UINT4               u4Offset = 0;
    UINT1              *pu1TmpBuff = NULL;

    if (MRP_IS_PORT_VALID (u2Port) == OSIX_FALSE)
    {

        MRP_TRC ((pContextInfo, MRP_PDU_TRC,
                  "MrpQueHandleIncomingFrame: Invalid Port number.\n"));

        return;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);

    if (NULL == pMrpPortEntry)
    {

        MRP_TRC ((pContextInfo, MRP_PDU_TRC | ALL_FAILURE_TRC,
                  "MrpQueHandleIncomingFrame:"
                  " Port %d unknown. MRPDDU dropped.\n", u2Port));

        return;
    }

    if ((MRP_OPER_DOWN == pMrpPortEntry->u1OperStatus) ||
        (MRP_REG_NORMAL != pMrpPortEntry->u1RegAdminCtrl))
    {
        MRP_TRC ((pContextInfo, MRP_PDU_TRC |
                  ALL_FAILURE_TRC, "MrpQueHandleIncomingFrame:"
                  "Port %d Oper Status = DOWN or Registrar Admin is not Normal."
                  " MRPDU dropped.\n", u2Port));
        return;
    }

    MEMSET (&IfMsg, 0, sizeof (tMrpIfMsg));

    u4FrameSize = CRU_BUF_Get_ChainValidByteCount (pMrpPdu);

    if ((u4FrameSize < MRP_MIN_FRAME_SIZE) ||
        (u4FrameSize > MRP_MAX_FRAME_SIZE))
    {

        MRP_TRC ((pContextInfo, MRP_PDU_TRC | ALL_FAILURE_TRC,
                  "MrpQueHandleIncomingFrame:"
                  " Rcvd Pkt of invalid PDU length %d \n", u4FrameSize));

        return;
    }

    MEMSET (gMrpGlobalInfo.pu1PduBuf, 0, MRP_MAX_FRAME_SIZE);

    if (CRU_BUF_Copy_FromBufChain (pMrpPdu, gMrpGlobalInfo.pu1PduBuf,
                                   u4Offset, u4FrameSize) == CRU_FAILURE)
    {

        MRP_TRC ((pContextInfo, MRP_PDU_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                  "MrpQueHandleIncomingFrame: Received MrpPdu Copy from CRU"
                  " Buffer FAILED!\n"));

        return;
    }

    MEMCPY (DestAddr, gMrpGlobalInfo.pu1PduBuf, ETHERNET_ADDR_SIZE);

    if (OSIX_TRUE == MRP_IS_802_1AD_BRIDGE (pContextInfo))
    {
        /* Check whether this packet can be processed on this port. In case of
         * MVRP packet received on a PPNP, the following function changes the
         * Destination MacAddr to Provider MVRP Address.
         */
        if (MrpUtilIsPortOkForMRPDU (pMrpPortEntry, DestAddr) == OSIX_FALSE)
        {
            return;
        }
    }

    pAppEntry =
        MrpAppGetAppEntryFrmAppAddr (pContextInfo->u4ContextId, DestAddr);

    if (NULL == pAppEntry)
    {

        MRP_TRC ((pContextInfo, MRP_PDU_TRC |
                  ALL_FAILURE_TRC, "MrpQueHandleIncomingFrame:"
                  "Application not enrolled !\n"));

        return;
    }

    if (MRP_DISABLED == MRP_PORT_APP_STATUS (pMrpPortEntry, pAppEntry->u1AppId))
    {

        MRP_TRC ((pContextInfo, MRP_PDU_TRC | ALL_FAILURE_TRC,
                  "MrpQueHandleIncomingFrame:"
                  "Application disabled on port %d!\n", u2Port));

        return;
    }

    IfMsg.pAppEntry = pAppEntry;
    IfMsg.u4ContextId = pContextInfo->u4ContextId;

    pu1TmpBuff = gMrpGlobalInfo.pu1PduBuf;
    pu1TmpBuff = pu1TmpBuff + MRP_SRC_ADDR_OFFSET;
    MEMCPY (IfMsg.SrcMacAddr, pu1TmpBuff, ETHERNET_ADDR_SIZE);

    IfMsg.u2MapId = u2MapId;
    IfMsg.u2Port = u2Port;
    IfMsg.u2PktLen = (UINT2) (u4FrameSize - MRP_PDU_HDR_SIZE);

    /* Move the buffer pointer to the start of the first Message in MRPDU */
    pu1TmpBuff = gMrpGlobalInfo.pu1PduBuf + MRP_PDU_HDR_SIZE;

    MRP_TRC ((pContextInfo, MRP_PDU_TRC, "MrpQueHandleIncomingFrame: %d ,%s ",
              u4FrameSize, gMrpGlobalInfo.pu1PduBuf));

    if (MrpRxValidatePDU (pu1TmpBuff, &IfMsg) == OSIX_FAILURE)
    {

        MRP_TRC ((pContextInfo, MRP_PDU_TRC | ALL_FAILURE_TRC,
                  "MrpQueHandleIncomingFrame: "
                  "Invalid Frame received on port %d\n", u2Port));

        MRP_INCR_RX_INVALID_PDU_CNT (pMrpPortEntry, pAppEntry->u1AppId);
        return;
    }

    MRP_INCR_RX_VALID_PDU_CNT (pMrpPortEntry, pAppEntry->u1AppId);
    MRP_UPDT_LAST_PDU_ORIGIN (pMrpPortEntry, pAppEntry->u1AppId,
                              IfMsg.SrcMacAddr);
    MrpRxApplyPDU (pu1TmpBuff, &IfMsg);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpQueHandleTcDetectedTmrInd 
 *                                                                          
 *    DESCRIPTION      : This function processes the status of the tcDetected
 *                       timer.
 *
 *    INPUT            : pContextInfo - Pointer to the Context Info structure
 *                       u2Port       - Local Port Number
 *                       u2MapId      - MST Instance to which the port is 
 *                                      mapped
 *                       u1TmrState   - Timer state (running or expired)
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
MrpQueHandleTcDetectedTmrInd (tMrpContextInfo * pContextInfo, UINT2 u2Port,
                              UINT2 u2MapId, UINT1 u1TmrState)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    UINT2               u2VlanCntInList = 0;
    UINT1               u1AppId = 0;
    UINT1              *pu1VlanList = NULL;

    if (MRP_IS_MRP_STARTED (pContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpQueHandleTcDetectedTmrInd: MRP is Shutdown in this"
                     " context.\n");
        return OSIX_FAILURE;
    }

    if (MRP_IS_PORT_VALID (u2Port) == OSIX_FALSE)
    {
        MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                  "MrpQueHandleTcDetectedTmrInd: Invalid Port number.\n"));
        return OSIX_FAILURE;
    }

    pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);
    if (pPortEntry == NULL)
    {

        MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                  "MrpQueHandleTcDetectedTmrInd: Local Port %d in Context"
                  "Id %d not created in MRP Module. \n", u2Port,
                  pContextInfo->u4ContextId));

        return OSIX_FAILURE;
    }

    pu1VlanList = UtlShMemAllocVlanList ();
    if (pu1VlanList == NULL)
    {
        return OSIX_FAILURE;
    }

    for (u1AppId = 1; u1AppId <= MRP_MAX_APPS; u1AppId++)
    {
        if (OSIX_FALSE == MRP_IS_MRP_APP_ENABLED (pContextInfo->u4ContextId,
                                                  u1AppId))
        {
            continue;
        }

        if (MRP_DISABLED == MRP_PORT_APP_STATUS (pPortEntry, u1AppId))
        {
            continue;
        }

        pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);

        if (MRP_MVRP_APP_ID == u1AppId)
        {
            MrpUtilSetTcDetectedTmrStatus (pAppEntry, u2MapId, u2Port,
                                           u1TmrState);
        }
        else                    /* MMRP */
        {
            MEMSET (pu1VlanList, 0, MRP_VLAN_LIST_SIZE);
            u2VlanCntInList = MrpPortL2IwfGetVlanListInInst
                (pContextInfo->u4ContextId, u2MapId, pu1VlanList);
            MrpMmrpApplyTcDetectedTmrStatus (pAppEntry, u2Port, pu1VlanList,
                                             u2VlanCntInList, u1TmrState);
        }
    }
    UtlShMemFreeVlanList (pu1VlanList);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpQueEnqMsg
 *
 *    DESCRIPTION      : Function to post queue messages to MRP task.
 *
 *    INPUT            : pMsg - Pointer to the message to be posted
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpQueEnqMsg (tMrpQMsg * pQMsg)
{

    if (OsixQueSend (gMrpGlobalInfo.MsgQId, (UINT1 *) &pQMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gMrpGlobalInfo.QMsgPoolId, (UINT1 *) pQMsg);
        MRP_GBL_TRC (OS_RESOURCE_TRC,
                     "MrpQueEnqMsg: Osix Queue send Failed!!!\r\n");
        return OSIX_FAILURE;
    }

    OsixEvtSend (gMrpGlobalInfo.TaskId, MRP_MSG_ENQ_EVENT);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpQueFlushContextsQueue
 *
 *    DESCRIPTION      : Function to flush the contents of the queue.
 *
 *    INPUT            : None 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          :  None
 *
 ****************************************************************************/
VOID
MrpQueFlushContextsQueue ()
{
    tMrpRxPduQMsg      *pRxPduQMsg = NULL;
    tMrpQMsg           *pMrpQMsg = NULL;
    UINT1               u1Event = 0;

    /* Flush All Q Messages */
    while (OsixQueRecv (gMrpGlobalInfo.RxPktQId, (UINT1 *) &pRxPduQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pRxPduQMsg->pMrpPdu, OSIX_FALSE);
        MemReleaseMemBlock (gMrpGlobalInfo.PduQMsgPoolId, (UINT1 *) pRxPduQMsg);
    }

    while (OsixQueRecv (gMrpGlobalInfo.MsgQId, (UINT1 *) &pMrpQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if ((MRP_VLAN_INST_MAP_MSG == pMrpQMsg->u2MsgType) ||
            (MRP_VLAN_LIST_INST_MAP_MSG == pMrpQMsg->u2MsgType))
        {
            MemReleaseMemBlock (gMrpGlobalInfo.VlanInstMapPoolId,
                                (UINT1 *) pMrpQMsg);
        }

        if (MRP_BULK_MSG == pMrpQMsg->u2MsgType)
        {
            MemReleaseMemBlock (gMrpGlobalInfo.QBulkMsgPoolId,
                                (UINT1 *) pMrpQMsg);
        }

        if ((MRP_MSG == pMrpQMsg->u2MsgType) ||
            (MRP_RM_MSG == pMrpQMsg->u2MsgType))
        {
            if (MRP_RM_MSG == pMrpQMsg->u2MsgType)
            {
                u1Event = pMrpQMsg->unMrpMsg.MrpRmMsg.u1Event;
                if ((RM_STANDBY_UP == u1Event) || (RM_STANDBY_DOWN == u1Event))
                {
                    MrpPortRelRmMsgMemory
                        ((UINT1 *) pMrpQMsg->unMrpMsg.MrpRmMsg.pData);
                }
                else if (RM_MESSAGE == u1Event)
                {
                    RM_FREE (pMrpQMsg->unMrpMsg.MrpRmMsg.pData);
                }

            }
            MemReleaseMemBlock (gMrpGlobalInfo.QMsgPoolId, (UINT1 *) pMrpQMsg);
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpQueProcessEvents 
 *
 *    DESCRIPTION      : Function to process the events that has been posted 
 *                       to the MRP Task.
 *
 *    INPUT            : u4Events - Event 
 *
 *    OUTPUT           : None
 *
 *    RETURNS          :  None
 *
 ****************************************************************************/
VOID
MrpQueProcessEvents (UINT4 u4Events)
{
    if (u4Events & MRP_PDU_ENQ_EVENT)
    {
        MRP_GBL_TRC (CONTROL_PLANE_TRC, "MrpQueProcessEvents: Recvd PDU "
                     "Event \r\n");
        MrpQueRxPduHandler ();
    }

    if (u4Events & MRP_TIMER_EXP_EVENT)
    {
        MRP_GBL_TRC (CONTROL_PLANE_TRC,
                     "MrpQueProcessEvents: Recvd Tmr Exp Event \r\n");
        MrpTmrExpHandler ();
    }

    if (u4Events & MRP_MSG_ENQ_EVENT)
    {
        MRP_GBL_TRC (CONTROL_PLANE_TRC,
                     "MrpQueProcessEvents: Recvd Cfg Msg Event \r\n");
        MrpQueMsgHandler ();
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpque.c                        */
/*-----------------------------------------------------------------------*/
