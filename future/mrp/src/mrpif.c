/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpif.c,v 1.11 2014/01/24 12:23:50 siva Exp $
 *
 * Description: This file contains procedures for 
 *              - creating/deleting interface structures 
 *              - handling interface up/down
 *              - handling port role, port state and point-to-point status
 *                change
 ******************************************************************************/
#include "mrpinc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfCreatePorts
 *                                                                          
 *    DESCRIPTION      : This function creates the ports that are currently 
 *                       present in the system (by refering L2IWF module).
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpIfCreatePorts (tMrpContextInfo * pMrpContextInfo)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2PrevLocalPort = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1OperStatus = 0;
    INT4                i4Status = OSIX_FAILURE;

    while (MrpPortGetNxtValidPortForContext (pMrpContextInfo->u4ContextId,
                                             u2PrevLocalPort, &u2LocalPort,
                                             &u4IfIndex) == L2IWF_SUCCESS)
    {
        u2PrevLocalPort = u2LocalPort;
        /* Need to move for the next port, when the port is in port-channel. 
         * All the layer 2 modules will not be knowing about the ports present 
         * in the port-channel. */
        if (MrpPortL2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
        {
            continue;
        }

        if (MrpIfHandleCreatePort (pMrpContextInfo, u4IfIndex,
                                   u2LocalPort) == OSIX_FAILURE)
        {
            MRP_TRC ((pMrpContextInfo,
                      (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                      "MrpIfCreatePorts: Mrp Port Creation failed for"
                      " Port %d", u4IfIndex));
            continue;
        }

        MrpPortL2IwfGetPortOperStatus (MRP_MODULE, u4IfIndex, &u1OperStatus);

        if (u1OperStatus == CFA_IF_UP)
        {
            /* No need to indicate MRP about the oper-down */
            i4Status =
                MrpIfHandlePortOperInd (pMrpContextInfo, u2LocalPort,
                                        MRP_OPER_UP);
        }
    }
    UNUSED_PARAM (i4Status);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfDeletePorts
 *                                                                          
 *    DESCRIPTION      : This function deletes the ports that are currently 
 *                       present in the system.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpIfDeletePorts (tMrpContextInfo * pMrpContextInfo)
{
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT2               u2Port = 0;

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

        if (pMrpPortEntry != NULL)
        {
            /* Set MVRP and MMRP disbaled for this port in l2iwf. */
            MrpPortSetProtoEnabledStatOnPort
                (pMrpContextInfo->u4ContextId, u2Port, L2_PROTO_MVRP,
                 OSIX_DISABLED);
            MrpPortSetProtoEnabledStatOnPort
                (pMrpContextInfo->u4ContextId, u2Port, L2_PROTO_MMRP,
                 OSIX_DISABLED);

            /* DeInit Port Entry. Remove from the Global Port Table and
             * Context Info Table */
            MrpIfDeInitAndDelPortEntry (pMrpContextInfo, pMrpPortEntry);

            /* Release the memory for the Port Entry */
            MemReleaseMemBlock (gMrpGlobalInfo.PortPoolId,
                                (UINT1 *) pMrpPortEntry);
            pMrpPortEntry = NULL;
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfHandleCreatePort
 *                                                                          
 *    DESCRIPTION      : This function creates the port in MRP module
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                       u4IfIndex       - Interface Index
 *                       u2LocalPort     - Local-Port Index.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpIfHandleCreatePort (tMrpContextInfo * pMrpContextInfo, UINT4 u4IfIndex,
                       UINT2 u2LocalPort)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpPortEntry      *pTempPortEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT2               u2PrevPortId = 0;

    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpIfHandleCreatePort: MRP is Shutdown in this "
                     "context\n");
        return OSIX_FAILURE;
    }

    if (MRP_IS_PORT_VALID (u2LocalPort) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC,
                     "MrpIfHandleCreatePort: "
                     "Invalid Port.Port NOT created.\n");
        return OSIX_FAILURE;
    }

    pPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2LocalPort);

    if (pPortEntry != NULL)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpIfHandleCreatePort: "
                  "Port %d is already created.", u2LocalPort));

        return OSIX_SUCCESS;
    }

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (MrpPortGetCfaIfInfo (u4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpIfHandleCreatePort: "
                  "Failed to get Cfa Interface Parameters. \n"));

        return OSIX_FAILURE;
    }

    /* Since PIPs are not visible to Layer 2 modules, MRP will not be 
     * creating the PIPs. */
    if (CfaIfInfo.u1BrgPortType == MRP_PROVIDER_INSTANCE_PORT)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpIfHandleCreatePort: MRP can't be enabled on PIP \r\n"));

        return OSIX_SUCCESS;
    }

    /* Allocate memory for the port entry structure. */
    pPortEntry = (tMrpPortEntry *) MemAllocMemBlk (gMrpGlobalInfo.PortPoolId);

    if (pPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo, MRP_CRITICAL_TRC,
                  "MrpIfHandleCreatePort: "
                  "Memory allocation failed for Port creation\n"));
        return OSIX_FAILURE;
    }

    /* Initialize the Port entry and Add to Global Port Table
     * and update the Context Info Table */
    if (MrpIfInitAndAddPortEntry (pMrpContextInfo, pPortEntry, u2LocalPort,
                                  u4IfIndex) != OSIX_SUCCESS)
    {
        MRP_TRC ((pMrpContextInfo, (MRP_CRITICAL_TRC | ALL_FAILURE_TRC),
                  "MrpIfHandleCreatePort: "
                  "Failed for Initialize and Add the Port\n"));
        MemReleaseMemBlock (gMrpGlobalInfo.PortPoolId, (UINT1 *) pPortEntry);
        pPortEntry = NULL;
        return OSIX_FAILURE;
    }

    /* Set the port status for MVRP */
    MrpMvrpSetPortMvrpStatus (pPortEntry);

    /* Set the port status for MMRP */
    MrpMmrpSetPortMmrpStatus (pPortEntry);

    /* Update the First, Next and Previous ports. */
    pPortEntry->u2NextPortId = MRP_INIT_VAL;
    pPortEntry->u2PrevPortId = MRP_INIT_VAL;

    if (pMrpContextInfo->u2FirstPortId == MRP_INIT_VAL)
    {
        pMrpContextInfo->u2FirstPortId = u2LocalPort;
    }
    else if (pMrpContextInfo->u2FirstPortId > u2LocalPort)
    {
        pPortEntry->u2NextPortId = pMrpContextInfo->u2FirstPortId;
        pPortEntry->u2PrevPortId = MRP_INIT_VAL;

        pTempPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                             pMrpContextInfo->u2FirstPortId);

        if (pTempPortEntry != NULL)
        {
            pTempPortEntry->u2PrevPortId = u2LocalPort;
        }
        pMrpContextInfo->u2FirstPortId = u2LocalPort;
    }
    else
    {
        for (u2PrevPortId = (UINT2) (u2LocalPort - 1);
             u2PrevPortId > MRP_INIT_VAL; u2PrevPortId--)
        {
            pTempPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2PrevPortId);

            if (pTempPortEntry != NULL)
            {
                pPortEntry->u2NextPortId = pTempPortEntry->u2NextPortId;
                pPortEntry->u2PrevPortId = u2PrevPortId;
                pTempPortEntry->u2NextPortId = u2LocalPort;
                break;
            }
        }

        if (pPortEntry->u2NextPortId != MRP_INIT_VAL)
        {
            pTempPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                                 pPortEntry->u2NextPortId);

            if (pTempPortEntry != NULL)
            {
                pTempPortEntry->u2PrevPortId = u2LocalPort;
            }
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfHandleDeletePort
 *                                                                          
 *    DESCRIPTION      : This function is for deleting a port.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                       u2LocalPort     - Local-Port Index.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpIfHandleDeletePort (tMrpContextInfo * pMrpContextInfo, UINT2 u2LocalPort)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpPortEntry      *pPrevPortEntry = NULL;
    tMrpPortEntry      *pNextPortEntry = NULL;

    if (MRP_IS_PORT_VALID (u2LocalPort) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpIfHandleDeletePort: "
                     "Invalid Port number.\n");
        return OSIX_FAILURE;
    }

    pPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2LocalPort);

    if (pPortEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpIfHandleDeletePort: "
                  "Port Entry is not present for port %d\r\n", u2LocalPort));
        return OSIX_FAILURE;
    }

    /* Delete this port from MVRP */
    MrpMvrpDelPort (pMrpContextInfo, u2LocalPort);

    /* Delete this port from MMRP */
    MrpMmrpDelPort (pMrpContextInfo, u2LocalPort);

    /* DeInit Port Entry. Remove from the Global Port Table and
     * Context Info Table */
    MrpIfDeInitAndDelPortEntry (pMrpContextInfo, pPortEntry);

    pPrevPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                         pPortEntry->u2PrevPortId);
    pNextPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                         pPortEntry->u2NextPortId);

    if ((pPrevPortEntry != NULL) && (pNextPortEntry != NULL))
    {
        pPrevPortEntry->u2NextPortId = pPortEntry->u2NextPortId;
        pNextPortEntry->u2PrevPortId = pPortEntry->u2PrevPortId;
    }
    else if (pPrevPortEntry != NULL)
    {
        pPrevPortEntry->u2NextPortId = MRP_INIT_VAL;
    }
    else if (pNextPortEntry != NULL)
    {
        pNextPortEntry->u2PrevPortId = MRP_INIT_VAL;
        pMrpContextInfo->u2FirstPortId = pNextPortEntry->u2LocalPortId;
    }
    else
    {
        pMrpContextInfo->u2FirstPortId = MRP_INIT_VAL;
    }
    /* Release the memory for the Port Entry */
    MemReleaseMemBlock (gMrpGlobalInfo.PortPoolId, (UINT1 *) pPortEntry);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfHandlePortOperInd
 *                                                                          
 *    DESCRIPTION      : This function handles the interface Oper up 
 *                       or Down indication
 *
 *    INPUT            : pMrpContextInfo - pointer to the Context info 
 *                                         structure.
 *                       u2LocalPort     - Local-Port Index
 *                       u1OperStatus    - MRP_OPER_UP/MRP_OPER_DOWN
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpIfHandlePortOperInd (tMrpContextInfo * pMrpContextInfo, UINT2 u2LocalPort,
                        UINT1 u1OperStatus)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpRedMsgInfo      SyncUpMsgInfo;

    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpIfHandlePortOperInd: MRP is Shutdown in this "
                     "context.\n");
        return OSIX_FAILURE;
    }

    if (MRP_IS_PORT_VALID (u2LocalPort) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpIfHandlePortOperInd: "
                     "Invalid Port number");
        return OSIX_FAILURE;
    }

    pPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2LocalPort);

    if (pPortEntry == NULL)
    {
        /* Port not created in MRP */
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpIfHandlePortOperInd: "
                  "Port Entry not found in this MRP Context\n"));

        return OSIX_FAILURE;
    }

    if (pPortEntry->u1OperStatus == u1OperStatus)
    {
        /* no change in Oper Status */
        return OSIX_SUCCESS;
    }

    switch (u1OperStatus)
    {
        case MRP_OPER_UP:

            pPortEntry->u1OperStatus = MRP_OPER_UP;

            /* Give Port create indications to MVRP application */
            if (MrpMvrpAddPort (pMrpContextInfo, u2LocalPort) != OSIX_SUCCESS)
            {
                MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                          "MrpIfHandlePortOperInd: "
                          "Port registration in MVRP Module failed.\n"));
            }

            /* Give Port create indications to MMRP application */
            if (MrpMmrpAddPort (pMrpContextInfo, u2LocalPort) != OSIX_SUCCESS)
            {
                MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                          "MrpIfHandlePortOperInd: "
                          "Port registration in MMRP Module failed.\n"));
            }
            /* MRP module will now propagate the attribute entries */
            break;

        case MRP_OPER_DOWN:

            pPortEntry->u1OperStatus = MRP_OPER_DOWN;

            /* Give port delete indications to MVRP application */
            MrpMvrpDelPort (pMrpContextInfo, u2LocalPort);
            /* Give port delete indications to MMRP application */
            MrpMmrpDelPort (pMrpContextInfo, u2LocalPort);

            break;

        default:
            return OSIX_FAILURE;
    }

    /* Indicate Standby node about the oper status change */
    MEMSET (&SyncUpMsgInfo, 0, sizeof (tMrpRedMsgInfo));
    SyncUpMsgInfo.u4ContextId = pMrpContextInfo->u4ContextId;
    SyncUpMsgInfo.u2LocalPortId = u2LocalPort;
    SyncUpMsgInfo.u1PortOperStatus = pPortEntry->u1OperStatus;

    MrpRedSendSyncUpMessages (MRP_RED_PORT_OPER_STATUS_MSG, &SyncUpMsgInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfInitAndAddPortEntry
 *                                                                          
 *    DESCRIPTION      : Initialize and Add the port entry to the global
 *                       port entry Table. Add to the Context info table
 *                       also by calling MrpIfInitMrpPortEntry.
 *
 *    INPUT            : pMrpContextInfo- Pointer to the Context Information
 *                       pPortEntry - Pointer to the Port Entry Structure
 *                       u2LocalPort - Local Port Index
 *                       u4IfIndex   - Interface IfIndex
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpIfInitAndAddPortEntry (tMrpContextInfo * pMrpContextInfo,
                          tMrpPortEntry * pPortEntry, UINT2 u2LocalPort,
                          UINT4 u4IfIndex)
{
    if (MrpIfInitMrpPortEntry (pMrpContextInfo, pPortEntry,
                               u2LocalPort, u4IfIndex) == OSIX_FAILURE)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpIfInitAndAddPortEntry: "
                  "Unable to add port entry to global port RBTree\n"));
        MrpIfDeInitMrpPortEntry (pMrpContextInfo, pPortEntry);
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (gMrpGlobalInfo.MrpGlobalPortTable,
                   (tRBElem *) pPortEntry) == RB_FAILURE)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpIfInitAndAddPortEntry: "
                  "Unable to add port entry to global port RBTree\n"));
        MrpIfDeInitMrpPortEntry (pMrpContextInfo, pPortEntry);

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfDeInitAndDelPortEntry
 *                                                                          
 *    DESCRIPTION      : De-Initialize and Delete the port entry to the
 *                       global port entry Table. Also delete from the context 
 *                       info table by calling MrpIfDeInitMrpPortEntry
 *
 *    INPUT            : pContextInfo - Pointer to the Context information
 *                       pPortEntry - Pointer to the Port Entry Structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpIfDeInitAndDelPortEntry (tMrpContextInfo * pContextInfo,
                            tMrpPortEntry * pPortEntry)
{
    RBTreeRem (gMrpGlobalInfo.MrpGlobalPortTable, (tRBElem *) pPortEntry);
    /* DeInit and Remove from Context info table */
    MrpIfDeInitMrpPortEntry (pContextInfo, pPortEntry);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfHandlePortRoleChgEvent
 *                                                                          
 *    DESCRIPTION      : This function processes the port role change.
 *
 *    INPUT            : pContextInfo - Pointer to the Context Info structure
 *                       u2Port       - Local Port Number
 *                       u2MapID        - CIST or MST Instance Identifier of 
 *                                        the port whose port role has changed
 *                       u1PortRoleEvent- Flush/Redeclare, depends on the old
 *                                        and the new port role.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/

INT4
MrpIfHandlePortRoleChgEvent (tMrpContextInfo * pContextInfo, UINT2 u2Port,
                             UINT2 u2MapId, UINT1 u1PortRoleEvent)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tVlanId             VlanId = 0;
    UINT1               u1AppId = 0;

    if (MRP_IS_MRP_STARTED (pContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpIfHandlePortRoleChgEvent: MRP is Shutdown in this "
                     "context.\n");
        return OSIX_FAILURE;
    }

    if (MRP_IS_PORT_VALID (u2Port) == OSIX_FALSE)
    {
        MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                  "MrpIfHandlePortRoleChgEvent: Invalid Port number.\n"));
        return OSIX_FAILURE;
    }

    pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);
    if (pPortEntry == NULL)
    {
        MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                  "MrpIfHandlePortRoleChgEvent: Local Port %d in Context Id"
                  " %d not created in MRP Module. \n", u2Port,
                  pContextInfo->u4ContextId));

        return OSIX_FAILURE;
    }

    /* Apply the port role change to the state machines in both the Applications
     */
    for (u1AppId = 1; u1AppId <= MRP_MAX_APPS; u1AppId++)
    {
        if (OSIX_FALSE == MRP_IS_MRP_APP_ENABLED (pContextInfo->u4ContextId,
                                                  u1AppId))
        {
            continue;
        }
        if (MRP_DISABLED == MRP_PORT_APP_STATUS (pPortEntry, u1AppId))
        {
            continue;
        }

        pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);

        if (MRP_MVRP_APP_ID == u1AppId)
        {
            MrpUtilHandlePortRoleChg (pAppEntry, u2MapId, u2Port,
                                      u1PortRoleEvent);
        }
        else                    /* MMRP */
        {
            for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
            {
                if (MrpPortL2IwfMiGetVlanInstMapping
                    (pContextInfo->u4ContextId, VlanId) != u2MapId)
                {
                    continue;
                }

                MrpUtilHandlePortRoleChg (pAppEntry, VlanId, u2Port,
                                          u1PortRoleEvent);
            }
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfHandlePortOperP2PStatusChg
 *                                                                          
 *    DESCRIPTION      : This function handles the Oper Point-To-Point status
 *                       change of the port.
 *
 *    INPUT            : pContextInfo - Pointer to the Context Info structure
 *                       u2Port          - Local Port Number whose P2P status 
 *                                         has changed
 *                       u1OperP2PStatus - Oper Point-To-Point status    
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
MrpIfHandlePortOperP2PStatusChg (tMrpContextInfo * pContextInfo, UINT2 u2Port,
                                 UINT1 u1OperP2PStatus)
{
    tMrpPortEntry      *pPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (pContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpIfHandlePortOperP2PStatusChg: MRP is Shutdown in this"
                     " context.\n");
        return OSIX_FAILURE;
    }

    if (MRP_IS_PORT_VALID (u2Port) == OSIX_FALSE)
    {

        MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpIfHandlePortOperP2PStatusChg:"
                     "Invalid Port number.\n");

        return OSIX_FAILURE;
    }

    pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);

    if (pPortEntry == NULL)
    {

        MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                  "MrpIfHandlePortOperP2PStatusChg: Local Port %d in "
                  "Context Id %d not created in MRP Module. \n", u2Port,
                  pContextInfo->u4ContextId));

        return OSIX_FAILURE;
    }

    if (pPortEntry->bOperP2PStatus == u1OperP2PStatus)
    {
        return OSIX_SUCCESS;
    }

    if (OSIX_FALSE == u1OperP2PStatus)
    {
        pPortEntry->bOperP2PStatus = OSIX_FALSE;
    }
    else
    {
        /* The port has changed from shared media to Point-to-Point. In P2P 
         * state, the Applicant states like AO, QO, AP and QP are not 
         * applicable. Hence if the current Applicant state is either AO or 
         * QO, change the state explicitly to VO and if it is either AP or QP
         * change it to VP.
         */

        pPortEntry->bOperP2PStatus = OSIX_TRUE;
        MrpUtilHandleSharedtoP2PMediaChg (pContextInfo, pPortEntry);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfHandleStapPortChange
 *                                                                          
 *    DESCRIPTION      : This function handles the port state change.
 *
 *    INPUT            : pContextInfo - Pointer to the Context Info structure
 *                       u2Port       - Local Port Number whose port status 
 *                                      has changed
 *                       u1State      - Port State Change
 *                       u2MapId      - MSTI or CIST Identifier to which the 
 *                                      port belongs
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
VOID
MrpIfHandleStapPortChange (tMrpContextInfo * pContextInfo, UINT2 u2Port,
                           UINT1 u1State, UINT2 u2MapId)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tVlanId             VlanId = 0;
    UINT1               u1AppId = 0;
    UINT1               u1AppStatus = (UINT1) MRP_DISABLED;

    if (MRP_IS_MRP_STARTED (pContextInfo->u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpIfHandleStapPortChange: MRP is Shutdown in this "
                     "context.\n");
        return;
    }

    if (MRP_IS_PORT_VALID (u2Port) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpIfHandleStapPortChange: "
                     "Invalid Port number.\n");
        return;
    }

    pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);
    if (pPortEntry == NULL)
    {

        MRP_TRC ((pContextInfo, ALL_FAILURE_TRC,
                  "MrpIfHandleStapPortChange: Local Port %d in Context Id"
                  " %d not created in MRP Module. \n", u2Port,
                  pContextInfo->u4ContextId));
        return;
    }

    /* Apply the port state change to both the Applications */
    for (u1AppId = 1; u1AppId <= MRP_MAX_APPS; u1AppId++)
    {
        if (OSIX_FALSE == MRP_IS_MRP_APP_ENABLED (pContextInfo->u4ContextId,
                                                  u1AppId))
        {
            continue;
        }

        pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);
        u1AppStatus = (UINT1) MRP_PORT_APP_STATUS (pPortEntry, u1AppId);

        if (MRP_MVRP_APP_ID == u1AppId)
        {
            pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);
            if (pMapEntry == NULL)
            {
                continue;
            }
            pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);
            if (pMapPortEntry == NULL)
            {
                continue;
            }

            if (u1State == AST_PORT_STATE_FORWARDING)
            {
                MrpUtilPortStateChgToForwarding (pContextInfo, pAppEntry,
                                                 pMapEntry, pMapPortEntry,
                                                 u1AppStatus);
            }
            else
            {
                MrpUtilPortStateChgedToBlocking (pContextInfo, pAppEntry,
                                                 pMapEntry, pMapPortEntry);
            }
        }
        else                    /* MMRP */
        {
            for (VlanId = 1; VlanId <= VLAN_MAX_VLAN_ID; VlanId++)
            {
                if (MrpPortL2IwfMiGetVlanInstMapping
                    (pContextInfo->u4ContextId, VlanId) != u2MapId)
                {
                    continue;
                }

                pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, VlanId);
                if (pMapEntry == NULL)
                {
                    continue;
                }
                pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);
                if (pMapPortEntry == NULL)
                {
                    continue;
                }

                if (u1State == AST_PORT_STATE_FORWARDING)
                {
                    MrpUtilPortStateChgToForwarding (pContextInfo, pAppEntry,
                                                     pMapEntry, pMapPortEntry,
                                                     u1AppStatus);
                }
                else
                {
                    MrpUtilPortStateChgedToBlocking (pContextInfo, pAppEntry,
                                                     pMapEntry, pMapPortEntry);
                }
            }
        }
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpIfCrtGblPortRbTree
 *
 *    DESCRIPTION      : This function creates the global port based RB Tree.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpIfCrtGblPortRbTree (VOID)
{
    gMrpGlobalInfo.MrpGlobalPortTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMrpPortEntry, GlobalPortRBNode),
                              (tRBCompareFn) MrpIfMrpPortTblCmpFn);

    if (gMrpGlobalInfo.MrpGlobalPortTable == NULL)
    {
        MRP_GBL_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
                     "MrpIfCrtGblPortRbTree: RBTree creation Failed\r\n");
        return OSIX_FAILURE;
    }
    /* No Semaphore creation is needed inside this RBTree. */
    RBTreeDisableMutualExclusion (gMrpGlobalInfo.MrpGlobalPortTable);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpIfDelGblPortRbTree
 *
 *    DESCRIPTION      : This function deletes the global port based RB Tree.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
MrpIfDelGblPortRbTree (VOID)
{
    if (gMrpGlobalInfo.MrpGlobalPortTable != NULL)
    {
        RBTreeDestroy (gMrpGlobalInfo.MrpGlobalPortTable,
                       (tRBKeyFreeFn) MrpIfFreeGlobPortEntry, 0);
        gMrpGlobalInfo.MrpGlobalPortTable = NULL;
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpIfMrpPortTblCmpFn
 *
 *    DESCRIPTION      : This routine will be invoked by the RB Tree library
 *                       to traverse through the global port based RB tree.
 *
 *    INPUT            : pPort1 - Pointer to the first port entry.
 *                       pPort2 - Pointer to the second port entry.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : MRP_RB_GREATER, if pPort1 is greater than pPort2
 *                       MRP_RB_LESS, if pPort2 is greater than pPort1
 *                       MRP_RB_EQUAL, if pPort1 and pPort2 are equal.
 *
 ****************************************************************************/
INT4
MrpIfMrpPortTblCmpFn (tRBElem * pPort1, tRBElem * pPort2)
{
    tMrpPortEntry      *pMrpPortEntry1 = (tMrpPortEntry *) pPort1;
    tMrpPortEntry      *pMrpPortEntry2 = (tMrpPortEntry *) pPort2;

    if (pMrpPortEntry1->u4IfIndex > pMrpPortEntry2->u4IfIndex)
    {
        return MRP_RB_GREATER;
    }
    else if (pMrpPortEntry1->u4IfIndex < pMrpPortEntry2->u4IfIndex)
    {
        return MRP_RB_LESS;
    }
    return MRP_RB_EQUAL;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpIfFreeGlobPortEntry
 *
 *    DESCRIPTION      : Releases the Port Entry to the free pool
 *
 *    INPUT            : pElem - Pointer to the Port entry to be released
 *
 *    OUTPUT           : None
 *
 *    RETURNS          :  OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpIfFreeGlobPortEntry (tRBElem * pElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    MemReleaseMemBlock (gMrpGlobalInfo.PortPoolId, pElem);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfAddAttrToPortMvrpTable
 *                                                                          
 *    DESCRIPTION      : This function adds the attribute information in the 
 *                       Port MVRP table present in Port structure.
 *
 *    INPUT            : pMrpPortEntry    - Pointer to the port structure
 *                       pMrpAttrEntry    - Pointer to the Attribute structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpIfAddAttrToPortMvrpTable (tMrpPortEntry * pMrpPortEntry,
                             tMrpAttrEntry * pMrpAttrEntry)
{
    UINT1               u1Result = OSIX_FALSE;

    OSIX_BITLIST_IS_BIT_SET
        (pMrpPortEntry->pu1MvrpAttrList, pMrpAttrEntry->u2AttrIndex,
         (VLAN_MAX_VLAN_ID / MRP_PORTS_PER_BYTE), u1Result);

    if (u1Result == OSIX_TRUE)
    {
        /* Entry Already Deleted. So returning */
        return OSIX_SUCCESS;
    }

    OSIX_BITLIST_SET_BIT (pMrpPortEntry->pu1MvrpAttrList,
                          pMrpAttrEntry->u2AttrIndex,
                          VLAN_MAX_VLAN_ID / MRP_PORTS_PER_BYTE);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfDelAttrFromPortMvrpTable
 *                                                                          
 *    DESCRIPTION      : This function deletes the attribute information from  
 *                       the MVRP table present in Port structure.
 *
 *    INPUT            : pMrpPortEntry    - Pointer to the port structure
 *                       pMrpAttrEntry    - Pointer to the Attribute structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpIfDelAttrFromPortMvrpTable (tMrpPortEntry * pMrpPortEntry,
                               tMrpAttrEntry * pMrpAttrEntry)
{
    UINT1               u1Result = OSIX_FALSE;

    OSIX_BITLIST_IS_BIT_SET
        (pMrpPortEntry->pu1MvrpAttrList, pMrpAttrEntry->u2AttrIndex,
         (VLAN_MAX_VLAN_ID / MRP_PORTS_PER_BYTE), u1Result);

    if (u1Result == OSIX_TRUE)
    {
        OSIX_BITLIST_RESET_BIT
            (pMrpPortEntry->pu1MvrpAttrList, pMrpAttrEntry->u2AttrIndex,
             (VLAN_MAX_VLAN_ID / MRP_PORTS_PER_BYTE));

        if (pMrpPortEntry->pNextMvrpTxNode == pMrpAttrEntry)
        {
            pMrpPortEntry->pNextMvrpTxNode =
                MrpMvrpGetNextAttrForTx (pMrpPortEntry,
                                         pMrpAttrEntry->u2AttrIndex,
                                         OSIX_FALSE);
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfInitMrpPortEntry
 *                                                                          
 *    DESCRIPTION      : Initialize the port entry and creates the required 
 *                       RBTree.
 *
 *    INPUT            : pMrpContextInfo- Pointer to the Context Information
 *                       pPortEntry - Pointer to the Port Entry Structure
 *                       u2LocalPort - Local Port Index
 *                       u4IfIndex   - Interface IfIndex
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpIfInitMrpPortEntry (tMrpContextInfo * pMrpContextInfo,
                       tMrpPortEntry * pPortEntry,
                       UINT2 u2LocalPort, UINT4 u4IfIndex)
{
    tMrpStatsEntry     *pMrpMvrpStatsEntry = NULL;
    tMrpStatsEntry     *pMrpMmrpStatsEntry = NULL;
    UINT1               u1PortType = (UINT1) MRP_INVALID_BRIDGE_PORT;

    MEMSET (pPortEntry, 0, sizeof (tMrpPortEntry));

    pPortEntry->u4ContextId = pMrpContextInfo->u4ContextId;
    pPortEntry->u4IfIndex = u4IfIndex;
    pPortEntry->u2LocalPortId = u2LocalPort;

    /* Allocate Memory for the mvrpAttributeList in the MrpPortEntry */
    pPortEntry->pu1MvrpAttrList =
        (UINT1 *) MemAllocMemBlk (gMrpGlobalInfo.MvrpPortAttrListPoolId);

    if (MRP_IS_PORT_VALID (u2LocalPort) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC,
                     "MrpIfInitMrpPortEntry: "
                     "Invalid Port.Port NOT created.\n");
        return OSIX_FAILURE;
    }

    if (pPortEntry->pu1MvrpAttrList == NULL)
    {
        MRP_TRC ((pMrpContextInfo,
                  (ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                  "MrpIfInitMrpPortEntry: Memory allocation failed "
                  "for MVRP Attributes\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pPortEntry->pu1MvrpAttrList, 0,
            (VLAN_MAX_VLAN_ID / MRP_PORTS_PER_BYTE));

    /* Allocate Memory for the MvrpStatsEntry in the MrpPortEntry */
    pMrpMvrpStatsEntry =
        (tMrpStatsEntry *) MemAllocMemBlk (gMrpGlobalInfo.StatisticsPoolId);

    if (pMrpMvrpStatsEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo,
                  (ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                  "MrpIfInitMrpPortEntry : Memory allocation failed for "
                  "Mvrp statistics entry creation\n"));
        return OSIX_FAILURE;
    }
    pPortEntry->pStatsEntry[MRP_MVRP_APP_ID] = pMrpMvrpStatsEntry;

    /* Allocate Memory for the MmrpStatsEntry */
    pMrpMmrpStatsEntry =
        (tMrpStatsEntry *) MemAllocMemBlk (gMrpGlobalInfo.StatisticsPoolId);

    if (pMrpMmrpStatsEntry == NULL)
    {
        MRP_TRC ((pMrpContextInfo,
                  (ALL_FAILURE_TRC | MRP_CRITICAL_TRC),
                  "MrpIfInitMrpPortEntry : Memory allocation failed for "
                  "Mmrp statistics entry creation\n"));
        return OSIX_FAILURE;
    }
    pPortEntry->pStatsEntry[MRP_MMRP_APP_ID] = pMrpMmrpStatsEntry;

    MEMSET (pMrpMvrpStatsEntry, 0, sizeof (tMrpStatsEntry));
    MEMSET (pMrpMmrpStatsEntry, 0, sizeof (tMrpStatsEntry));

    pPortEntry->u1OperStatus = MRP_OPER_DOWN;
    MrpPortL2IwfGetPortOperP2PStatus (u4IfIndex, &(pPortEntry->bOperP2PStatus));
    pPortEntry->u4JoinTime = MRP_DEF_JOIN_TIME;
    pPortEntry->u4LeaveTime = MRP_DEF_LEAVE_TIME;
    pPortEntry->u4LeaveAllTime = MRP_DEF_LEAVE_ALL_TIME;
    pPortEntry->u1RestrictedVlanRegCtrl = MRP_DISABLED;
    pPortEntry->u1RestrictedMACRegCtrl = MRP_DISABLED;
    pPortEntry->u1ParticipantType = MRP_FULL_PARTICIPANT;
    pPortEntry->u1PeriodicSEMStatus = MRP_DISABLED;
    pPortEntry->u1PeriodicSEMState = MRP_PASSIVE;

    pPortEntry->au1ApplAdminCtrl[MRP_MMRP_APP_ID]
        [MRP_SERVICE_REQ_ATTR_TYPE] = MRP_NORMAL_PARTICIPANT;
    pPortEntry->au1ApplAdminCtrl[MRP_MMRP_APP_ID]
        [MRP_MAC_ADDR_ATTR_TYPE] = MRP_NORMAL_PARTICIPANT;
    pPortEntry->au1ApplAdminCtrl[MRP_MVRP_APP_ID]
        [MRP_VID_ATTR_TYPE] = MRP_NORMAL_PARTICIPANT;

    if (MRP_GET_PORT_ENTRY (pMrpContextInfo, pPortEntry->u2LocalPortId) != NULL)
    {
        /*Entry is already present */
        return OSIX_FAILURE;
    }
    pMrpContextInfo->apMrpPortEntry[pPortEntry->u2LocalPortId] = pPortEntry;

    if (MRP_IS_802_1AD_1AH_BRIDGE (pMrpContextInfo) == OSIX_TRUE)
    {
        if (MrpPortL2IwfGetPbPortType (u4IfIndex, &u1PortType) == L2IWF_FAILURE)
        {
            pPortEntry->u2BridgePortType = MRP_PROVIDER_NETWORK_PORT;
        }
        pPortEntry->u2BridgePortType = u1PortType;
    }
    else
    {
        pPortEntry->u2BridgePortType = MRP_CUSTOMER_BRIDGE_PORT;
    }

    /* Filling Back pointer */
    pPortEntry->pMrpContextInfo = pMrpContextInfo;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpIfDeInitMrpPortEntry
 *                                                                          
 *    DESCRIPTION      : De-Initialize the port entry and deletes the required 
 *                       RBTree.
 *
 *    INPUT            : pMrpContextInfo- Pointer to the Context Information
 *                       pPortEntry - Pointer to the Port Entry Structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
VOID
MrpIfDeInitMrpPortEntry (tMrpContextInfo * pMrpContextInfo,
                         tMrpPortEntry * pPortEntry)
{
    TmrStop (gMrpGlobalInfo.MrpTmrListId, &(pPortEntry->PeriodicTmr));

    if (pPortEntry->pu1MvrpAttrList != NULL)
    {
        MemReleaseMemBlock (gMrpGlobalInfo.MvrpPortAttrListPoolId,
                            (UINT1 *) pPortEntry->pu1MvrpAttrList);
        pPortEntry->pu1MvrpAttrList = NULL;
    }

    if (pPortEntry->pStatsEntry[MRP_MVRP_APP_ID] != NULL)
    {
        MemReleaseMemBlock (gMrpGlobalInfo.StatisticsPoolId, (UINT1 *)
                            pPortEntry->pStatsEntry[MRP_MVRP_APP_ID]);
        pPortEntry->pStatsEntry[MRP_MVRP_APP_ID] = NULL;
    }

    if (pPortEntry->pStatsEntry[MRP_MMRP_APP_ID] != NULL)
    {
        MemReleaseMemBlock (gMrpGlobalInfo.StatisticsPoolId, (UINT1 *)
                            pPortEntry->pStatsEntry[MRP_MMRP_APP_ID]);
        pPortEntry->pStatsEntry[MRP_MMRP_APP_ID] = NULL;
    }

    MrpCtxtDelPortEntryFromCxtEntry (pMrpContextInfo, pPortEntry);
}

/****************************************************************************
 *                                                                           
 *    Function Name       : MrpIfGetContextInfoFromIfIndex                   
 *                                                                           
 *    Description         : This function retives the context and Local port 
 *                          Id from the given IfIndex.                       
 *                                                                           
 *    Input(s)            : u4IfIndex                                        
 *                                                                           
 *    Output(s)           : pu4ContextId                                     
 *                          pu2LocalPort                                     
 *                                                                           
 *    Use of Recursion    : None.                                            
 *                                                                           
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE .                      
 *                                                                           
 ****************************************************************************/
INT4
MrpIfGetContextInfoFromIfIndex (UINT4 u4Port, UINT4 *pu4ContextId,
                                UINT2 *pu2LocalPort)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpPortEntry       DummyEntry;

    MEMSET (&DummyEntry, 0, sizeof (tMrpPortEntry));

    /* Get Global ifindex based RBTree and get the context ID and
       local port from the port entry else return failure */
    DummyEntry.u4IfIndex = u4Port;

    pPortEntry = (tMrpPortEntry *) RBTreeGet (gMrpGlobalInfo.MrpGlobalPortTable,
                                              (tRBElem *) & DummyEntry);
    if (pPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    *pu4ContextId = pPortEntry->u4ContextId;
    *pu2LocalPort = pPortEntry->u2LocalPortId;

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME      : MrpIfGetValidPort
 *     
 *                                                                           
 * DESCRIPTION        : This function will return the pointer to the port 
 *                      info structure when the port is exist. Else this 
 *                      will return the next nearest port information.
 *                                                                           
 * INPUT(s)           : pContextInfo   - Pointer to the context info
 *                      u2Port         - Local Port Number    
 *                                       
 * OUTPUT(s)          : None                                                 
 *                                                                           
 * Return Value(s)    : Pointer to the port info structure
 *****************************************************************************/
tMrpPortEntry      *
MrpIfGetValidPort (tMrpContextInfo * pContextInfo, UINT2 u2Port)
{
    tMrpPortEntry      *pPortEntry = NULL;
    UINT2               u2Index = 0;

    if (u2Port == 0)
    {
        return (MRP_GET_PORT_ENTRY (pContextInfo, pContextInfo->u2FirstPortId));
    }

    pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);

    if (pPortEntry != NULL)
    {
        return pPortEntry;
    }

    for (u2Index = pContextInfo->u2FirstPortId; u2Index != MRP_INIT_VAL;
         u2Index = pPortEntry->u2NextPortId)
    {
        pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Index);

        if ((pPortEntry == NULL) || (pPortEntry->u2LocalPortId > u2Index))
        {
            return pPortEntry;
        }
    }

    return NULL;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpif.c                        */
/*-----------------------------------------------------------------------*/
