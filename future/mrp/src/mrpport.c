/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpport.c,v 1.14 2012/04/16 14:12:12 siva Exp $
 *
 * Description: This file contains the wrapper routines of the external 
 *              routines which is required for MRP operations.
 *********************************************************************/

#include "mrpinc.h"
#include "snmputil.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortL2IwfGetBridgeMode
 *                                                                          
 *    DESCRIPTION      : This function calls the L2IWF module to get the
 *                       bridge mode.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                                                                          
 *    OUTPUT           : pu4BridgeMode - bridge mode
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    return (L2IwfGetBridgeMode
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), pu4BridgeMode));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortVlanGetBaseBridgeMode
 *                                                                          
 *    DESCRIPTION      : This function calls the VLAN module to get the
 *                       the Base Bridge Mode when the user tries to start 
 *                       MRP module.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Base Bridge Mode or 0
 *                                                                          
 ****************************************************************************/
INT4
MrpPortVlanGetBaseBridgeMode ()
{
    return (VlanGetBaseBridgeMode ());
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortL2IwfGetPbPortType
 *                                                                          
 *    DESCRIPTION      : This function calls the L2IWF module to get the
 *                       bridge port type.
 *
 *    INPUT            : u4IfIndex - Interface Index
 *                                                                          
 *    OUTPUT           : pu1PortType - bridge port type
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortL2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType)
{
    return (L2IwfGetPbPortType (u4IfIndex, pu1PortType));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortGetNxtValidPortForContext
 *                                                                          
 *    DESCRIPTION      : This routine returns the next active port in the
 *                       context as ordered by the HL Port ID. This is used 
 *                       by the L2 modules to know the active ports in the
 *                       system when they are started from the shutdown state
 *
 *    INPUT            : u4ContextId      - Context identifier
 *                       u2LocalPortId    - HL Port Index whose next port is 
 *                                          to be determined
 *                                                                          
 *    OUTPUT           : pu2NextLocalPort - The next active HL Port ID
 *                       pu4NextIfIndex   - IfIndex of the next HL Port
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortGetNxtValidPortForContext (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                  UINT2 *pu2NextLocalPort,
                                  UINT4 *pu4NextIfIndex)
{
    tL2NextPortInfo     L2NextPortInfo;

    L2NextPortInfo.u4ContextId = MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId);
    L2NextPortInfo.u2LocalPortId = u2LocalPortId;
    L2NextPortInfo.pu2NextLocalPort = pu2NextLocalPort;
    L2NextPortInfo.pu4NextIfIndex = pu4NextIfIndex;
    L2NextPortInfo.u4ModuleId = L2IWF_PROTOCOL_ID_MRP;

    return (L2IwfGetNextValidPortForProtoCxt (&L2NextPortInfo));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortGetVlanLocalEgressPorts
 *                                                                          
 *    DESCRIPTION      : This routine returns the Egress ports for the given
 *                       VlanId as Local port IDs.
 *
 *    INPUT            : u4ContextId      - Context Identifier
 *                       VlanId           - Vlan whose Egress ports are to be 
 *                                          obtained
 *                                                                          
 *    OUTPUT           : EgressPorts      - Egress Port List for the given Vlan
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortGetVlanLocalEgressPorts (UINT4 u4ContextId, tVlanId VlanId,
                                tLocalPortList EgressPorts)
{
    return (L2IwfMiGetVlanLocalEgressPorts
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId),
             VlanId, EgressPorts));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortL2IwfIsPortInPortChannel
 *                                                                          
 *    DESCRIPTION      : This function calls the L2IWF module to check 
 *                       whether the port is in port-channel.
 *
 *    INPUT            : u4IfIndex        - Interface Index
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortL2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortL2IwfGetPortOperP2PStatus
 *                                                                          
 *    DESCRIPTION      : This function calls L2IWF module to get the port's 
 *                       oper point to point status.
 *
 *    INPUT            : u4IfIndex  - IfIndex of the port whose oper point to
 *                                    point status is to be determined
 *                                                                          
 *    OUTPUT           : pbOperPointToPoint - Oper point to point status
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortL2IwfGetPortOperP2PStatus (UINT4 u4IfIndex, BOOL1 * pbOperPointToPoint)
{
    INT4                i4RetVal = OSIX_FAILURE;

    *pbOperPointToPoint = OSIX_FALSE;

    if (L2IwfGetPortOperPointToPointStatus (u4IfIndex, pbOperPointToPoint)
        == L2IWF_SUCCESS)
    {
        if (RST_TRUE == *pbOperPointToPoint)
        {
            *pbOperPointToPoint = OSIX_TRUE;
        }
        i4RetVal = OSIX_SUCCESS;
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortL2IwfGetPortOperStatus
 *                                                                          
 *    DESCRIPTION      : This routine determines the operational status of a
 *                       port indicated to a given module by it's lower layer
 *                       modules (determines the lower layer port oper status)
 *
 *    INPUT            : i4ModuleId - Module for which the lower layer
 *                                    oper status is to be determined
 *                       u4IfIndex  - Global IfIndex of the port whose lowe
 *                                    layer oper status is to be determined
 *                                                                          
 *    OUTPUT           : pu1OperStatus - Lower layer port oper status for a
 *                                       given module
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT4 u4IfIndex,
                               UINT1 *pu1OperStatus)
{
    return (L2IwfGetPortOperStatus (i4ModuleId, u4IfIndex, pu1OperStatus));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortL2IwfGetVlanPortType
 *                                                                          
 *    DESCRIPTION      : This routine returns the port type for the given 
 *                       port. 
 *
 *    INPUT            : u4IfIndex     - Interface index for which the 
 *                                       port-type needs to be obtained.
 *                                                                          
 *    OUTPUT           : pu1PortType   - VLAN_ACCESS_PORT/
 *                                       VLAN_HYBRID_PORT/
 *                                       VLAN_TRUNK_PORT
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortL2IwfGetVlanPortType (UINT4 u4IfIndex, UINT1 *pu1PortType)
{
    return (L2IwfGetVlanPortType (u4IfIndex, pu1PortType));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortGetPortVlanTunnelStatus
 *                                                                          
 *    DESCRIPTION      : This routine returns the tunnel status for the given 
 *                       port. 
 *
 *    INPUT            : u2IfIndex     - Interface Index for which the 
 *                                       tunnel status needs to be obtained.
 *                                                                          
 *    OUTPUT           : pu1PortType   - OSIX_TRUE/OSIX_FALSE
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortGetPortVlanTunnelStatus (UINT4 u4IfIndex, BOOL1 * pbTunnelStatus)
{
    return (L2IwfGetPortVlanTunnelStatus (u4IfIndex, pbTunnelStatus));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortGetProtoTunelStatusOnPort
 *                                                                          
 *    DESCRIPTION      : This routine returns the protocol tunnel status for 
 *                       the given port. 
 *
 *    INPUT            : u4IfIndex     - Interface Index for which the 
 *                                       port-type needs to be obtained.
 *                       u2Protocol    - L2 Protocol
 *                                                                          
 *    OUTPUT           : pu1PortType   - OSIX_TRUE/OSIX_FALSE
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpPortGetProtoTunelStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                  UINT1 *pu1TunnelStatus)
{
    L2IwfGetProtocolTunnelStatusOnPort (u4IfIndex, u2Protocol, pu1TunnelStatus);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortSetProtoEnabledStatOnPort
 *                                                                          
 *    DESCRIPTION      : This routine will be called by protocol routines to 
 *                       update the corresponding protocol's enabled status
 *                       on a port.
 *
 *    INPUT            : u4ContextId   - Context Identified
 *                       u2LocalPortId - Local port Index of the port whose
 *                                       protocols enabled status is to be
 *                                       updated.
 *                       u2Protocol    - L2_PROTO_MVRP/L2_PROTO_MMRP
 *                       u1Status      - Protocol status (enabled/disable)
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortSetProtoEnabledStatOnPort (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                  UINT2 u2Protocol, UINT1 u1Status)
{
    return (L2IwfSetProtocolEnabledStatusOnPort
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), u2LocalPortId,
             u2Protocol, u1Status));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortVcmGetSystemMode
 *                                                                          
 *    DESCRIPTION      : This function calls the VCM Module to get the mode  
 *                       of the system (SI / MI).
 *
 *    INPUT            : u2ProtocolId - Protocol Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : VCM_SI_MODE/VCM_MI_MODE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortVcmGetSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}


/****************************************************************************
 *  
 *       FUNCTION NAME    : MrpPortVcmGetAliasName
 *    
 *       DESCRIPTION      : This function calls the VCM Module to get the mode
 *                          of the system (SI / MI).
 *       
 *       INPUT            : u2ProtocolId - Protocol Identifier
 *         
 *       OUTPUT           : None
 *           
 *       RETURNS          : VCM_SI_MODE/VCM_MI_MODE
 *             
 ****************************************************************************/

INT4
MrpPortVcmGetSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}




/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortVcmGetAliasName
 *                                                                          
 *    DESCRIPTION      : This function calls the VCM Module to get the 
 *                       Alias name of the given context.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                                                                          
 *    OUTPUT           : pu1Alias    - Alias Name
 *                                                                          
 *    RETURNS          : VCM_SUCCESS/VCM_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    return (VcmGetAliasName
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), pu1Alias));
}

/*****************************************************************************
 * FUNCTION NAME       : MrpPortHandleOutgoingPktOnPort
 *
 * DESCRIPTION        : This function Sends out the MRP packet from MRP
 *                      module.
 *
 * INPUT(s)           : pBuf - pointer to the outgoing packet (CRU Buffer)
 *                      u4ContextId   - Context Id
 *                      u4PktSize        - Packet size
 *                      u2Protocol       - Protocol type
 *                      u1Encap          - Encapsulation Type
 *
 * OUTPUT(s)          : None
 *
 * RETURN VALUE(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
INT4
MrpPortHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf,
                                UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT4 u4PktSize, UINT2 u2Protocol,
                                UINT1 u1Encap)
{
    tPbbTag             PbbTag;
    tVlanTag            VlanTag;
    tCfaBackPlaneParams BackPlaneParams;

    if (MRP_IS_VIP_INTERFACE (u4IfIndex) != OSIX_FALSE)
    {
        /* VIP Interface */
        MEMSET (&PbbTag, 0, sizeof (tPbbTag));
        MEMSET (&VlanTag, 0, sizeof (VlanTag));

        PbbTag.InnerIsidTag.u1Priority = VLAN_DEF_TUNNEL_BPDU_PRIORITY;

        if (L2IwfTransmitFrameOnVip (MRP_CONVERT_COMP_ID_TO_CTXT_ID
                                     (u4ContextId),
                                     u4IfIndex, pBuf, &VlanTag,
                                     &PbbTag, L2_MMRP_PKT) == L2IWF_SUCCESS)
        {
            return OSIX_SUCCESS;
        }
    }
    else if (CfaApiGetBackPlaneStatusForIface (u4IfIndex) == CFA_BACKPLANE_INTF)
    {
        MEMSET (&BackPlaneParams, 0, sizeof (tCfaBackPlaneParams));

        BackPlaneParams.u4ContextId =
            MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId);

        /* Backplane interface */
        if (L2IwfHandleOutgoinPktOnBackPlane (pBuf, &BackPlaneParams,
                                              u4IfIndex, u4PktSize,
                                              u2Protocol, u1Encap)
            == L2IWF_SUCCESS)
        {
            return OSIX_SUCCESS;
        }
    }
    else
    {
        if (L2IwfHandleOutgoingPktOnPort (pBuf, u4IfIndex, u4PktSize,
                                          u2Protocol, u1Encap) == L2IWF_SUCCESS)
        {
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************
 * FUNCTION NAME       : MrpPortGetContextInfoFromIfIndex
 *
 * DESCRIPTION        : This function calls the VCM Module to get the        
 *                      Context-Id and the Localport number.                 
 *                                                                           
 * INPUT(s)           : u4IfIndex - Interface Identifier.                    
 *                                                                           
 * OUTPUT(s)          : pu4ContextId - Context Identifier.                   
 *                      pu2LocalPortId - Local port number.                  
 *                                                                           
 * RETURN VALUE(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                           
 *****************************************************************************/
INT4
MrpPortGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                  UINT2 *pu2LocalPortId)
{
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                      pu2LocalPortId) == VCM_SUCCESS)
    {
        MRP_CONVERT_CTXT_ID_TO_COMP_ID (*pu4ContextId);
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 * FUNCTION NAME       : MrpPortGetCfaIfInfo
 *
 * DESCRIPTION        : This function calls the CFA Module to get the
 *                      interface information from the given interface index.
 *                                                                           
 * INPUT(s)           : u4IfIndex - Interface Identifier.                    
 *                                                                           
 * OUTPUT(s)          : pIfInfo   - Interface information.
 *                                                                           
 * RETURN VALUE(s)    : NONE
 *                                                                           
 *****************************************************************************/
INT4
MrpPortGetCfaIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaGetIfInfo (u4IfIndex, pIfInfo));
}

/*****************************************************************************
 * FUNCTION NAME       : MrpPortCfaCliGetIfName
 *
 * DESCRIPTION        : This function calls the CFA Module to get the
 *                      interface name for the given interface index.
 *
 * INPUT(s)           : u4IfIndex - Interface Identifier.
 *
 * OUTPUT(s)          : pi1IfName - Interface Name.
 *
 * RETURN VALUE(s)    : NONE
 *
 *****************************************************************************/
INT4
MrpPortCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    return (CfaCliGetIfName (u4IfIndex, pi1IfName));
}

/*****************************************************************************
 * FUNCTION NAME      : MrpPortVlanGetStartedStatus
 *
 * DESCRIPTION        : This function returns whether VLAN is started or 
 *                      shutdown.
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier.                    
 *                                                                           
 * OUTPUT(s)          : None
 *                                                                           
 * RETURN VALUE(s)    : VLAN_TRUE/VLAN_FALSE
 *                                                                           
 *****************************************************************************/
INT4
MrpPortVlanGetStartedStatus (UINT4 u4ContextId)
{
    return (VlanGetStartedStatus
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId)));
}

/****************************************************************************
 * FUNCTION NAME      : MrpPortVlanIndMvrpEnable
 *
 * DESCRIPTION        : This function indicates VLAN, that MVRP is enabled. 
 *                      If there are any Static Vlan entry configured 
 *                      then VLAN module should indicate MVRP.
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier.                    
 *                                                                           
 * OUTPUT(s)          : None
 *                                                                           
 * RETURN VALUE(s)    : None
 *                                                                           
 *****************************************************************************/
VOID
MrpPortVlanIndMvrpEnable (UINT4 u4ContextId)
{
    VlanMvrpEnableInd (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId));
}

/*****************************************************************************
 * FUNCTION NAME       : MrpPortVlanIndMvrpDisable
 *
 * DESCRIPTION        : This function indicates VLAN, that MVRP is Disabled. 
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier.                    
 *                                                                           
 * OUTPUT(s)          : None
 *                                                                           
 * RETURN VALUE(s)    : None
 *                                                                           
 *****************************************************************************/
VOID
MrpPortVlanIndMvrpDisable (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
}

/*****************************************************************************
 * FUNCTION NAME       : MrpPortVlanIndMmrpEnable
 *
 * DESCRIPTION        : This function indicates VLAN, that MMRP is enabled. 
 *                      If there are any Static MAC entry configured then 
 *                      VLAN module should indicate MMRP.
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier.                    
 *                                                                           
 * OUTPUT(s)          : None
 *                                                                           
 * RETURN VALUE(s)    : None
 *                                                                           
 *****************************************************************************/
VOID
MrpPortVlanIndMmrpEnable (UINT4 u4ContextId)
{
    VlanMmrpEnableInd (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId));
}

/*****************************************************************************
 * FUNCTION NAME       : MrpPortVlanIndMmrpDisable
 *
 * DESCRIPTION        : This function indicates VLAN, that MMRP is Disabled. 
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier.                    
 *                                                                           
 * OUTPUT(s)          : None
 *                                                                           
 * RETURN VALUE(s)    : None
 *                                                                           
 *****************************************************************************/
VOID
MrpPortVlanIndMmrpDisable (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
}

/****************************************************************************
 * FUNCTION NAME      : MrpPortVlanIndPortMvrpEnable
 *
 * DESCRIPTION        : This function indicates VLAN, that MVRP is enabled on 
 *                      the specified port. If there are any Static Vlan entry 
 *                      configured then VLAN module should indicate MVRP.
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier.                    
 *                      u2Port      - LocalPort Identifier                   
 *                                                                           
 * OUTPUT(s)          : None
 *                                                                           
 * RETURN VALUE(s)    : None
 *                                                                           
 *****************************************************************************/
VOID
MrpPortVlanIndPortMvrpEnable (UINT4 u4ContextId, UINT2 u2Port)
{
    VlanMvrpPortEnableInd
        (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), u2Port);
}

/****************************************************************************
 * FUNCTION NAME      : MrpPortVlanIndPortMmrpEnable
 *
 * DESCRIPTION        : This function indicates VLAN, that MVRP is enabled on 
 *                      the specified port. If there are any Static MAC entry 
 *                      configured then VLAN module should indicate MMRP.
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier.                    
 *                      u2Port      - LocalPort Identifier                   
 *                                                                           
 * OUTPUT(s)          : None
 *                                                                           
 * RETURN VALUE(s)    : None
 *                                                                           
 *****************************************************************************/
VOID
MrpPortVlanIndPortMmrpEnable (UINT4 u4ContextId, UINT2 u2Port)
{
    VlanMmrpPortEnableInd
        (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), u2Port);
}

/*****************************************************************************
 * FUNCTION NAME       : MrpPortVlanDelAllDynamicVlanInfo
 *
 * DESCRIPTION        : This function indicates VLAN, to remove all the     
 *                      dynamically learnt VLAN information.
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier.                    
 *                      u2Port      - Local Port Identifier.                    
 *                                                                           
 * OUTPUT(s)          : None
 *                                                                           
 * RETURN VALUE(s)    : None
 *                                                                           
 *****************************************************************************/
VOID
MrpPortVlanDelAllDynamicVlanInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    VlanDeleteAllDynamicVlanInfo (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId),
                                  u2Port);
}

/*****************************************************************************
 * FUNCTION NAME       : MrpPortVlanDelAllDynamicMacInfo
 *
 * DESCRIPTION        : This function indicates VLAN, to remove all the     
 *                      dynamically learnt MAC information.
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier.                    
 *                      u2Port      - Local Port Identifier.                    
 *                                                                           
 * OUTPUT(s)          : None
 *                                                                           
 * RETURN VALUE(s)    : None
 *                                                                           
 *****************************************************************************/
VOID
MrpPortVlanDelAllDynamicMacInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    UINT4               u4IfIndex = VLAN_INVALID_PORT;

    if ((u2Port > VLAN_INVALID_PORT) && (u2Port <= MRP_MAX_PORTS_PER_CONTEXT))
    {
        u4IfIndex = MRP_GET_PHY_PORT (u4ContextId, u2Port);
    }

    VlanMiDeleteAllDynamicMcastInfo
        (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), u4IfIndex,
         VLAN_ADDR_FMLY_ALL);
}

/*****************************************************************************
 * FUNCTION NAME      : MrpPortTagOutgoingMmrpFrame
 *
 * DESCRIPTION        : This function is used to tag the MMRP PDU.          
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier
 *                      pBuf     - Pointer to the incoming packet            
 *                      VlanId   - VlanId
 *                      u2Port   - Local Port on which the GARP PDU is to be
 *                                 transmitted.
 *                                                                           
 * OUTPUT(s)          : pBuf - Modified Pbuf.
 *                                                                           
 * RETURN VALUE(s)    : None
 *                                                                           
 *****************************************************************************/
INT4
MrpPortTagOutgoingMmrpFrame (UINT4 u4ContextId, tCRU_BUF_CHAIN_DESC * pBuf,
                             tVlanId VlanId, UINT2 u2Port)
{
    return (VlanCheckAndTagOutgoingGmrpFrame
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), pBuf,
             VlanId, u2Port));
}

/*****************************************************************************
 * FUNCTION NAME      : MrpPortGarpGetStartedStatus
 *
 * DESCRIPTION        : This function returns whether GARP is started or 
 *                      shutdown.
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier.                    
 *                                                                           
 * OUTPUT(s)          : None
 *                                                                           
 * RETURN VALUE(s)    : VLAN_TRUE/VLAN_FALSE
 *                                                                           
 *****************************************************************************/
INT4
MrpPortGarpGetStartedStatus (UINT4 u4ContextId)
{
    return (GarpIsGarpEnabledInContext
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId)));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortL2IwfGetVlanListInInst
 *                                                                          
 *    DESCRIPTION      : This function calls the L2IWF module to get the
 *                       list of vlans mapped to the given Instance. 
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                       u2MstInst   - InstanceId for which the list of vlans
 *                                     mapped needs to be obtained
 *                       pu1VlanList - VlanList to be returned 
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : Number of vlans mapped to the given Instance. 
 *                                                                          
 ****************************************************************************/
UINT2
MrpPortL2IwfGetVlanListInInst (UINT4 u4ContextId, UINT2 u2MstInst,
                               UINT1 *pu1VlanList)
{
    return (L2IwfMiGetVlanListInInstance (u4ContextId, u2MstInst, pu1VlanList));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortL2IwfMiGetVlanInstMapping
 *                                                                          
 *    DESCRIPTION      : This function calls the L2IWF module to get the
 *                       MST Instance to which the given VLAN ID is mapped.
 *                       In case of RSTP, this function returns 0 as the MSTID.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                       VlanId      - VLAN Identifier
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : Identifier of the Instance to which the given VLAN ID
 *                       is mapped.
 *                                                                          
 ****************************************************************************/
UINT2
MrpPortL2IwfMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfMiGetVlanInstMapping
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), VlanId));
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpPortNotifyFaults
 *
 *    DESCRIPTION      : This function Sends the trap message to the Fault
 *                       Manager.
 *
 *    INPUT            : tSNMP_VAR_BIND * - pointer the trapmessage.
 *                       pu1Msg  *        - Pointer to the log message.
 *                       u4ModuleId       - ModuleId
 *
 *    OUTPUT           : Calls the FmApiNotifyFaults to send the
 *                       Trap message to FaultManager.
 *
 *    RETURNS          : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpPortNotifyFaults (tSNMP_VAR_BIND * pTrapMsg, UINT1 *pu1Msg, UINT4 u4ModuleId)
{
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));
    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pSyslogMsg = pu1Msg;
    FmFaultMsg.u4ModuleId = u4ModuleId;
#ifdef FM_WANTED
    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        SNMP_AGT_FreeVarBindList (pTrapMsg);
        MRP_GBL_TRC (ALL_FAILURE_TRC,
                     "MrpPortNotifyFaults: Sending "
                     "Notfication To FM is Failed");
        return OSIX_FAILURE;
    }
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME      : MrpPortIsIgmpSnoopingEnabled                    
 *                                                                           
 * DESCRIPTION        : This function checks if IGMP snooping feature is     
 *                      enabled in the system or not.                        
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier                    
 *                                                                           
 * OUTPUT(s)          : None                                                 
 *                                                                           
 * RETURN VALUE(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        
 *****************************************************************************/
INT4
MrpPortIsIgmpSnoopingEnabled (UINT4 u4ContextId)
{
    return (SnoopIsIgmpSnoopingEnabled
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId)));
}

/*****************************************************************************
 * FUNCTION NAME      : MrpPortSnoopIsMldSnoopingEnabled                     
 *                                                                           
 * DESCRIPTION        : This function checks if MLD snooping feature is      
 *                      enabled in the system or not.                        
 *                                                                           
 * INPUT(s)           : u4ContextId - Context Identifier                     
 *                                                                           
 * OUTPUT(s)          : None                                                 
 *                                                                           
 * RETURN VALUE(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        
 *****************************************************************************/
INT4
MrpPortSnoopIsMldSnoopingEnabled (UINT4 u4ContextId)
{
    return (SnoopIsMldSnoopingEnabled
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId)));
}

/*****************************************************************************
 * FUNCTION NAME      : MrpPortVcmIsSwitchExist                              
 *                                                                           
 * DESCRIPTION        : This function calls the VCM Module to check whether  
 *                      the context exist or not.                            
 *                                                                           
 * INPUT(s)           : pu1Alias - Alias Name                                
 *                                                                           
 * OUTPUT(s)          : pu4VcNum - Context Identifier                        
 *                                                                           
 * RETURN VALUE(s)    : OSIX_TRUE /OSIX_FALSE                                      *                                                                           
 *****************************************************************************/
INT4
MrpPortVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    if (VcmIsSwitchExist (pu1Alias, pu4VcNum) == VCM_TRUE)
    {
        MRP_CONVERT_CTXT_ID_TO_COMP_ID (*pu4VcNum);
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortIsMACWildCardEntryPresent
 *                                                                          
 *    DESCRIPTION      : This function calls the VLAN module to check 
 *                       if the MAC Address is registered as wild card entry 
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                       u2VlanId      - Vlan Identifier
 *                       MacAddr     - MAC Address
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : OSIX_TRUE / OSIX_FALSE 
 *                                                                          
 ****************************************************************************/

INT4
MrpPortIsMACWildCardEntryPresent (UINT4 u4ContextId, UINT2 u2VlanId,
                                  tMacAddr MacAddr)
{
    INT4                i4RetVal = OSIX_FALSE;

    if (MRP_IS_MCASTADDR (MacAddr) == OSIX_TRUE)
    {
        i4RetVal = VlanIsMcastWildCardEntryPresent
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), u2VlanId, MacAddr);
    }
    else
    {
        i4RetVal = VlanIsUcastWildCardEntryPresent
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), u2VlanId, MacAddr);
    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortVlanUpdateDynamicVlanInfo
 *                                                                          
 *    DESCRIPTION      : This function calls the VLAN module to update 
 *                       the Current VLAN table
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                       VlanId      - VlanId which is learnt by MVRP
 *                       u2Port      - Local Port Number 
 *                       u1Action    - VLAN_ADD / VLAN_DELETE
 *                                                                          
 *    OUTPUT           : pu4BridgeMode - bridge mode
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/

INT4
MrpPortVlanUpdateDynamicVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT2 u2Port, UINT1 u1Action)
{
    return (VlanUpdateDynamicVlanInfo
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId),
             VlanId, u2Port, u1Action));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortVlanUpdateDynDefGrpInfo
 *                                                                          
 *    DESCRIPTION      : This function calls the VLAN module to update the
 *                       dynamic entries in the Default GroupInfo table.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                       
 *                                                                          
 *    OUTPUT           : pu4BridgeMode - bridge mode
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortVlanUpdateDynDefGrpInfo (UINT4 u4ContextId, UINT1 u1Type,
                                tVlanId VlanId, UINT2 u2Port, UINT1 u1Action)
{

    if (u1Type == MRP_MMRP_ALL_GROUPS)
    {
        u1Type = VLAN_ALL_GROUPS;
    }
    else
    {
        u1Type = VLAN_UNREG_GROUPS;
    }

    return (VlanUpdateDynamicDefGroupInfo
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), u1Type, VlanId,
             u2Port, u1Action));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortPbGetRelayVidFrmLocalVid 
 *                                                                          
 *    DESCRIPTION      : This function calls the L2IWF module to get the
 *                       relay VID for the corresponding local VID.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                       u2LocalPort - Local Port Number
 *                       LocalVid    - Local VLAN Identifier
 *                                                                          
 *    OUTPUT           : *pRelayVid  - Relay VID  for the given local VID 
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/

INT4
MrpPortPbGetRelayVidFrmLocalVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                 tVlanId LocalVid, tVlanId * pRelayVid)
{
    return (L2IwfPbGetRelayVidFromLocalVid
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), u2LocalPort,
             LocalVid, pRelayVid));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortPbGetLocalVidFrmRelayVid 
 *                                                                          
 *    DESCRIPTION      : This function calls the L2IWF module to get the
 *                       local VID for the corresponding relay VID.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                       u2LocalPort - Local Port Number
 *                       RelayVid    - Local VLAN Identifier
 *                                                                          
 *    OUTPUT           : *pLocalVid  - Local VID  for the given Relay VID 
 *                                                                          
 *    RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPortPbGetLocalVidFrmRelayVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                 tVlanId RelayVid, tVlanId * pLocalVid)
{
    return (L2IwfPbGetLocalVidFromRelayVid
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), u2LocalPort,
             RelayVid, pLocalVid));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortL2IwfGetVlanFdbId 
 *                                                                          
 *    DESCRIPTION      : This function calls the L2IWF module to get the
 *                       FdbId corresponding to the given VlanId.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                       VlanId      - Vlan whose Fdb Id is to obtained
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : FdbId value corresponding to the VlanId 
 *                                                                          
 ****************************************************************************/
UINT4
MrpPortL2IwfGetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfMiGetVlanFdbId (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId),
                                 VlanId));
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPortVlanFlushFdbEntries 
 *                                                                          
 *    DESCRIPTION      : This function calls the VLAN module to flush the
 *                       FDB entries. 
 *
 *    INPUT            : u4IfIndex - Interface Index
 *                       u4FdbId   - FDB Identifier
 *                                                                          
 *    OUTPUT           : None 
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
INT4
MrpPortVlanFlushFdbEntries (UINT4 u4IfIndex, UINT4 u4FdbId)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    i4RetVal = VlanFlushFdbEntriesOnPort
        (u4IfIndex, u4FdbId, (UINT1) MRP_MODULE, VLAN_OPTIMIZE);
    return i4RetVal;
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME        : MrpPortVlanIsVlanDynamic                        
 *                                                                           
 *    DESCRIPTION         : This function checks if the VLAN is dynamically  
 *    INPUT(s)            : VlanId - Vlan ID                                 
 *                                                                           
 *    OUTPUT(s)           : None                                             
 *                                                                           
 *    RETURN            : VLAN_TRUE/VLAN_FALSE                              
 *                                                                           
 *****************************************************************************/
INT4
MrpPortVlanIsVlanDynamic (UINT4 u4ContextId, UINT2 VlanId)
{
    return (VlanIsVlanDynamic (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId),
                               VlanId));
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME        : MrpPortVlanUpdateDynamicMACInfo             
 *                                                                           
 *    DESCRIPTION         : This function updated the Multicast table.       
 *                                                                           
 *    INPUT(s)            : u4ContextId - Context Identifier                 
 *                          MacAddr -  MacAddress                            
 *                          VlanId   - VlanId associated with the multicast  
 *                                     MacAddress                            
 *                          u2Port   - The Port Number                       
 *                          u1Action - VLAN_ADD, then the port will be added 
 *                                     into the PortList.                    
 *                                                                           
 *                                     VLAN_DELETE, then the port will be    
 *                                     deleted from the PortList.            
 *                                                                           
 *    OUTPUT(s)           : None                                             
 *                                                                           
 *    RETURN            : VLAN_SUCCESS                                      
 *                         VLAN_FAILURE                                      
 *****************************************************************************/
INT4
MrpPortVlanUpdateDynamicMACInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                                 tVlanId VlanId, UINT4 u4IfIndex,
                                 UINT1 u1Action)
{
    if (MRP_IS_MCASTADDR (MacAddr) == L2IWF_TRUE)
    {
        return (VlanMiUpdateDynamicMcastInfo
                (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), MacAddr, VlanId,
                 u4IfIndex, u1Action));
    }
    else
    {
        return (VlanMiUpdateDynamicUcastInfo
                (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), MacAddr, VlanId,
                 u4IfIndex, u1Action));
    }
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME        : MrpPortDelDynamicMacInfoForVlan           
 *                                                                           
 *    DESCRIPTION         : This function removes all the dynamic multicast / 
 *                          unicast information from the table for the 
 *                          specified Vlan for the given context                
 *                                                                           
 *    OUTPUT(s)           : None                                             
 *                                                                           
 *    RETURN            : None                                              
 *****************************************************************************/
VOID
MrpPortDelDynamicMacInfoForVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    /* This function takes care of deleting both unicast and multicast
     * information.
     */
    VlanMiDelDynamicMcastInfoForVlan
        (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), VlanId);
}

#ifdef VLAN_EXTENDED_FILTER
/****************************************************************************
 *                                                                           
 *    FUNCTION NAME        : MrpPortDelAllDynamicDefGroupInfo    
 *                                                                           
 *    DESCRIPTION         : This function removes all the dynamic service 
 *                          req information from the table.                
 *                                                                           
 *    INPUT(s)            : u2Port - This is currently called from MRP, so  
 *                          the port is local port                           
 *                                                                           
 *    OUTPUT(s)           : None                                             
 *                                                                           
 *    RETURN            : None                                              
 *                                                                           
 *****************************************************************************/
VOID
MrpPortDelAllDynamicDefGroupInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    VlanDeleteAllDynamicDefGroupInfo
        (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), u2Port);
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME        : MrpPortDelDynDefGroupInfoForVlan       
 *                                                                           
 *    DESCRIPTION         : This function removes all the dynamic default    
 *                          group information from the table for the         
 *                          specified Vlan                                   
 *                                                                           
 *                                                                           
 *    OUTPUT(s)           : None                                             
 *                                                                           
 *    RETURN            : None                                              
 *****************************************************************************/
VOID
MrpPortDelDynDefGroupInfoForVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    VlanMiDelDynamicDefGroupInfoForVlan
        (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), VlanId);
}

#endif
/****************************************************************************
 * FUNCTION NAME       : MrpPortL2IwfGetInstPortState                            
 *                                                                          
 * DESCRIPTION        : This function calls the L2IWF module to get the     
 *                      instance port state on a port.                      
 *                                                                          
 * INPUT(s)           : u2MstInst  - InstanceId for which the port state is 
 *                      to be obtained.                                     
 *                      u4IfIndex  - Global IfIndex of the port whose port  
 *                      state is to be obtained.                            
 *                                                                          
 * OUTPUT(s)          : None.                                               
 *                                                                          
 * RETURN VALUE(s)    : Port State                                          
 *                                                                          
 *****************************************************************************/
UINT1
MrpPortL2IwfGetInstPortState (UINT2 u2MstInst, UINT4 u4IfIndex)
{
    return (L2IwfGetInstPortState (u2MstInst, u4IfIndex));
}

/****************************************************************************
 * FUNCTION NAME      : MrpPortL2IwfGetVlanPortState                            
 *                                                                           
 * DESCRIPTION        : This routine returns the port state given the        
 *                      Vlan Id and the Port Index. It accesses the L2Iwf    
 *                      common database to find the Mst Instance to which    
 *                      the Vlan is mapped and then gets the ports state for 
 *                      that instance.                                       
 *                                                                           
 * INPUT(s)           : VlanId - VlanId for which the port state is to be    
 *                               obtained.                                   
 *                    : u2IfIndex - Global IfIndex of the port whose state   
 *                                    is to be obtained.                     
 *                                                                           
 * OUTPUT(s)          : None                                                 
 *                                                                           
 * RETURN VALUE(s)    : Port State                                           
 *****************************************************************************/
UINT1
MrpPortL2IwfGetVlanPortState (tVlanId VlanId, UINT4 u4IfIndex)
{
    return (L2IwfGetVlanPortState (VlanId, u4IfIndex));
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME       : MrpPortIsPvrstStartedInContext               
 *                                                                           
 *    DESCRIPTION         : This function checks if the PVRST  is 
 *                          started in the context or not .                  
 *                                                                           
 *    INPUT(s)            : ContextId - Context Identifier                           
 *    OUTPUT(s)           : None                                             
 *                                                                           
 *    RETURNS            : VLAN_TRUE/VLAN_FALSE                              
 *****************************************************************************/
INT4
MrpPortIsPvrstStartedInContext (UINT4 u4ContextId)
{
    return (AstIsPvrstStartedInContext
            (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId)));
}

/**************************************************************
 *  Function Name   : MrpPortPbbTeVidIsEspVid                     
 *  Description     : It returns the given VLAN is ESP VLAN    
 *                    or not.                                  
 *  Input(s)        : u4ContextId - Context Identifier 
 *                    VlanId      - Vlan Identifier                             
 *  Output(s)       :                                          
 *  Returns         : OSIX_TRUE/OSIX_FALSE                        
 ***************************************************************/
INT4
MrpPortPbbTeVidIsEspVid (UINT4 u4ContextId, tVlanId VlanId)
{
    if (PbbTeVidIsEspVid
        (MRP_CONVERT_COMP_ID_TO_CTXT_ID (u4ContextId), VlanId) == OSIX_TRUE)
    {
        return OSIX_TRUE;
    }

    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : MrpPortCfaCliConfGetIfName                           */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1IfName - Interface name of the given IfIndex.     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
MrpPortCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    if (CfaCliConfGetIfName (u4IfIndex, pi1IfName) != CFA_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MrpPortIsJoinTmrReducedFrBackPln                     */
/*                                                                           */
/* Description        : MRP Module invokes this API for determining the value*/
/*                      of join timer for back plane interfaces. Some of the */
/*                      hardware can have back plane connection that induces */
/*                      a factor of time delay. For such a kind of platform, */
/*                      this API should be modified to return OSIX_TRUE.     */
/*                      By default, it returns OSIX_FALSE. If it returns     */
/*                      OSIX_TRUE, then the default value of join timer will */
/*                      be reduced by 5 centi seconds to accommodate the time*/
/*                      delay of the back plane connection.                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
MrpPortIsJoinTmrReducedFrBackPln (VOID)
{
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : MrpPortL2IwfGetPVlanMappingInfo                      */
/*                      This function gives the mapping between primary and  */
/*                      secondary vlan. This function will be invoked by     */
/*                      Spanning tree, GMRP, MMRP, IGS to know the type of   */
/*                      vlan and to get the primary to seconday vlan mapping */
/*                      and vice versa.                                      */
/*                                                                           */
/* Input(s)           : pL2PvlanMappingInfo - Pointer to the structure       */
/*                                            containing Private vlan mapping*/
/*                                            information. The following are */
/*                                            the fields in this structure   */
/*                                            that are input to this API.    */
/*                      u1RequestType - Type of request. It can take the     */
/*                                      following values:                    */
/*                                        1.L2IWF_VLAN_TYPE                  */
/*                                        2.L2IWF_MAPPED_VLANS               */
/*                                        3.L2IWF_NUM_MAPPED_VLANS           */
/*                                                                           */
/*                                      If L2IWF_VLAN_TYPE is passed, then   */
/*                                      pMappedVlans will not be filled, but */
/*                                      only value for pVlanType is filled.  */
/*                                                                           */
/*                                      If L2IWF_MAPPED_VLANS is passed,     */
/*                                      then both pVlanMapped Vlans and      */
/*                                      pVlanType are filled.                */
/*                                                                           */
/*                                      If L2IWF_NUM_MAPPED_VLANS is passed, */
/*                                      the the number of asscoiated vlans   */
/*                                      for the given vlan will be filled.   */
/*                      InVlanId - Id of the given Vlan.                     */
/*                                                                           */
/* Output(s)          : pL2PvlanMappingInfo - pointer to the structure       */
/*                                            containing Private vlan mapping*/
/*                                            information. The following are */
/*                                            the fields in this structure   */
/*                                            that are output to this API.   */
/*                                                                           */
/*                      pMappedVlans - If the InVlanId is a primary vlan,    */
/*                                     then this pointer points to an array  */
/*                                     containing the list of secondary vlans*/
/*                                     associated with that primary vlan.    */
/*                                                                           */
/*                                     If the InVlanId is a secondary vlan,  */
/*                                     this *pMappedVlans will contain the ID*/
/*                                     of primary vlan associated with the   */
/*                                     given secondary vlan (InVlanId).      */
/*                                                                           */
/*                                     If u1RequestTyps is                   */
/*                                     L2IWF_MAPPED_VLANS, then only this    */
/*                                     array will be filled. Otherwise this  */
/*                                     will not filled.                      */
/*                                                                           */
/*                      pu1VlanType - This gives the type of vlan of the     */
/*                                    given input vlan (InVlanId). Possible  */
/*                                    values for this parameter are:         */
/*                                      1.L2IWF_VLAN_TYPE_NORMAL,            */
/*                                      2.L2IWF_VLAN_TYPE_PRIMARY,           */
/*                                      3.L2IWF_VLAN_TYPE_ISOLATED,          */
/*                                      4.L2IWF_VLAN_TYPE_COMMUNITY          */
/*                                                                           */
/*                      pu2NumMappedVlans - Number of vlans present in the   */
/*                                          array pointed by pMappedVlans.   */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
MrpPortL2IwfGetPVlanMappingInfo (tL2PvlanMappingInfo * pL2PvlanMappingInfo)
{
    L2IwfGetPVlanMappingInfo (pL2PvlanMappingInfo);
}

#ifdef L2RED_WANTED
/*****************************************************************************
 *
 * FUNCTION NAME      : MrpPortRelRmMsgMemory 
 * 
 * DESCRIPTION        : This function calls the RM API to release the memory
 *                      allocated for RM message.
 *
 * INPUT              : pu1Block - Memory block 
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE                            
 *                                                                           
 ****************************************************************************/
INT4
MrpPortRelRmMsgMemory (UINT1 *pu1Block)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (RmReleaseMemoryForMsg (pu1Block) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************
 *
 * FUNCTION NAME      : MrpPortEnqMsgToRm 
 * 
 * DESCRIPTION        : This function calls the RM Module to enqueue the
 *                      message from applications to RM task.
 *
 * INPUT              : pRmMsg - msg from application(MRP)
 *                      u2DataLen - Len of msg
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpPortEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, RM_MRP_APP_ID, RM_MRP_APP_ID)
        != RM_SUCCESS)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC, "MrpPortEnqMsgToRm: Enqueuing sync up "
                     "message to RM failed\r\n");
        /* memory allocated for pRmMsg is freed here, only in failure case. 
         * In success case RM will free the memory */
        RM_FREE (pRmMsg);
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/*****************************************************************************
 *
 * FUNCTION NAME      : MrpPortGetRmNodeState
 * 
 * DESCRIPTION        : This function gets the node state from RM
 *
 * INPUT              : NONE 
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : UINT1 - Node status 
 *                                                                           
 ****************************************************************************/
UINT1
MrpPortGetRmNodeState (VOID)
{
    return (UINT1) (RmGetNodeState ());
}

/*****************************************************************************
 *
 * FUNCTION NAME      : MrpPortGetStandbyNodeCount 
 * 
 * DESCRIPTION        : This function gets the standby node count from RM
 *
 * INPUT              : NONE 
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : UINT1 - Standby node count 
 *                                                                           
 ****************************************************************************/
UINT1
MrpPortGetStandbyNodeCount (VOID)
{
    return (RmGetStandbyNodeCount ());
}

/*****************************************************************************
 *
 * FUNCTION NAME      : MrpPortSetBulkUpdateStatus 
 * 
 * DESCRIPTION        : This function sets MRP bulk update status in RM
 *                      
 * INPUT              : NONE
 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : NONE                            
 *                                                                           
 ****************************************************************************/
PUBLIC VOID
MrpPortSetBulkUpdateStatus (VOID)
{
    RmSetBulkUpdatesStatus (RM_MRP_APP_ID);

    return;
}

/*****************************************************************************
 *
 * FUNCTION NAME      : MrpPortSendEventToRm 
 * 
 * DESCRIPTION        : This function posts an event to RM
 *                      
 * INPUT              : pEvt - pointer to protocol event structure
 *                      u4Event - Event to be sent to RM. And the event can be
 *                      any of the following,
 *                      RM_PROTOCOL_BULK_UPDT_COMPLETION
 *                      RM_BULK_UPDT_ABORT
 *                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED
 *                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED
 *                      RM_STANDBY_EVT_PROCESSED
 *
 *                      u4Error - In case the event is RM_BULK_UPDATE_ABORT, 
 *                      the reason for the failure is send in the error. And 
 *                      the error can be any of the following,
 *                      RM_MEMALLOC_FAIL
 *                      RM_SENTO_FAIL
 *                      RM_PROCESS_FAIL
 *                      
 * OUTPUT             : None 
 *
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                           
 ****************************************************************************/
INT4
MrpPortSendEventToRm (tRmProtoEvt * pEvt)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (RmApiHandleProtocolEvent (pEvt) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/*****************************************************************************
 * 
 * FUNCTION NAME      : MrpPortRmApiSendProtoAckToRM 
 * 
 * DESCRIPTION        : This function is used to send acknowledgement to RM
 *                      after processing the sync-up message.
 *
 * INPUT              : tRmProtoAck contains 
 *                      u4AppId  - Protocol Identifier
 *                      u4SeqNum - Sequence number of the RM message for
 *                                 which this ACK is generated.

 * 
 * OUTPUT             : NONE
 *
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE 
 *
 *****************************************************************************/
INT4
MrpPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (RmApiSendProtoAckToRM (pProtoAck) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/*****************************************************************************
 * 
 * FUNCTION NAME      : MrpPortDeRegisterWithRM 
 *                                                                           
 * DESCRIPTION        : This function DeRegisters/UnRegisters MRP from RM                 
 *                                                                           
 * INPUT              : RM_MRP_APP_ID - MRP application ID   
 *                                                                           
 * OUTPUT             : NONE                                                 
 *                                                                           
 * RETURNS            : OSIX_SUCCESS/OSIX_FAILURE                            
 *                                                                           
 ****************************************************************************/
INT4
MrpPortDeRegisterWithRM (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (RmDeRegisterProtocols (RM_MRP_APP_ID) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpPortRmRegisterProtocols 
 *
 *    DESCRIPTION      : Registeres MRP with Redundancy Manager 
 *                       
 *    INPUT            : pRmRegParams - Reg. params to be provided by MRP
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpPortRmRegisterProtocols (tRmRegParams * pRmRegParams)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (RmRegisterProtocols (pRmRegParams) != RM_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    return i4RetVal;
}

#endif /* L2RED_WANTED */
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpport.c                       */
/*-----------------------------------------------------------------------*/
