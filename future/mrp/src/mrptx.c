/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrptx.c,v 1.7 2013/06/25 12:08:38 siva Exp $
 *
 * Description: This file contains all the MRP transmit related procedures. 
 *
 *****************************************************************************/

#include "mrpinc.h"

/*************************************************************************
 *
 *  FUNCTION NAME   : MrpTxApplyTxEventOnAttr
 *
 *  DESCRIPTION     : This function applys the Tx Event on that Attribute.
 *
 *  INPUT           : pMapPortEntry - MAP Port Entry
 *                    pAttrEntry    - Attribute Entry
 *                    pu1AttrEvent  - Attribute Event 
 *                    pu1OptEncode  - Optimal Encoding Flag
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : None
 *
 **************************************************************************/
VOID
MrpTxApplyTxEventOnAttr (tMrpMapPortEntry * pMapPortEntry,
                         tMrpAttr * pAttr, UINT2 u2AttrIndex, UINT1 u1Event,
                         UINT1 *pu1AttrEvent, UINT1 *pu1OptEncode)
{
    tMrpAppEntry       *pAppEntry = NULL;
    UINT2               u2MapId = 0;
    UINT2               u2Port = 0;
    UINT1               u1SendMsgInd = MRP_SEND_NONE;
    UINT1               u1RegSEMStateChg = OSIX_FALSE;
    UINT1               u1RegState = 0;
    UINT1               u1RegAdmCtrl = 0;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pAttr);

    pAppEntry = pMapPortEntry->pMapEntry->pMrpAppEntry;
    u2MapId = pMapPortEntry->pMapEntry->u2MapId;
    u2Port = pMapPortEntry->u2Port;
    u1RegState = MRP_GET_ATTR_REG_SEM_STATE (pMapPortEntry->
                                             pu1AttrInfoList[u2AttrIndex]);
    u1RegAdmCtrl = MRP_GET_ATTR_REG_ADMIN_CTRL (pMapPortEntry->
                                                pu1AttrInfoList[u2AttrIndex]);

    u1SendMsgInd = (UINT1) MrpSmApplicantSem (pMapPortEntry,
                                              u2AttrIndex, u1Event);

    switch (u1SendMsgInd)
    {
        case MRP_SEND_NEW:
            MRP_TRC ((pAppEntry->pMrpContextInfo, CONTROL_PLANE_TRC,
                      "MrpTxApplyTxEventOnAttr: Sending NEW on the port %d"
                      " for MAP %d\n", u2Port, u2MapId));
            *pu1AttrEvent = MRP_NEW_ATTR_EVENT;
            MRP_PRINT_ATTRIBUTE (pAttr);
            break;

        case MRP_SEND_JOIN:
        case MRP_SEND_JOIN_OPTIONAL:
            if (u1RegState == MRP_IN)
            {
                MRP_TRC ((pAppEntry->pMrpContextInfo, CONTROL_PLANE_TRC,
                          "MrpTxApplyTxEventOnAttr: Sending JOIN IN on the"
                          " port %d for MAP %d\n", u2Port, u2MapId));
                *pu1AttrEvent = MRP_JOININ_ATTR_EVENT;
            }
            else

            {
                MRP_TRC ((pAppEntry->pMrpContextInfo, CONTROL_PLANE_TRC,
                          "MrpTxApplyTxEventOnAttr: Sending JOIN MT on the"
                          " port %d for MAP %d\n", u2Port, u2MapId));
                *pu1AttrEvent = MRP_JOINMT_ATTR_EVENT;
            }

            if (u1SendMsgInd == MRP_SEND_JOIN_OPTIONAL)
            {
                *pu1OptEncode = OSIX_TRUE;
            }
            MRP_PRINT_ATTRIBUTE (pAttr);
            break;

        case MRP_SEND_LEAVE:
            MRP_TRC ((pAppEntry->pMrpContextInfo, CONTROL_PLANE_TRC,
                      "MrpTxApplyTxEventOnAttr: Sending LEAVE on the port"
                      " %d for MAP %d\n", u2Port, u2MapId));
            *pu1AttrEvent = MRP_LV_ATTR_EVENT;
            MRP_PRINT_ATTRIBUTE (pAttr);
            break;

        case MRP_SEND_IN_OR_MT:
        case MRP_SEND_IN_OR_MT_OPTIONAL:
            if (u1RegState == MRP_IN)
            {
                MRP_TRC ((pAppEntry->pMrpContextInfo, CONTROL_PLANE_TRC,
                          "MrpTxApplyTxEventOnAttr: Sending IN on the port"
                          "%d for MAP " "%d\n", u2Port, u2MapId));
                *pu1AttrEvent = MRP_IN_ATTR_EVENT;
            }
            else
            {
                MRP_TRC ((pAppEntry->pMrpContextInfo, CONTROL_PLANE_TRC,
                          "MrpTxApplyTxEventOnAttr: Sending MT on the port"
                          "%d for MAP " "%d\n", u2Port, u2MapId));
                *pu1AttrEvent = MRP_MT_ATTR_EVENT;
            }

            if (u1SendMsgInd == MRP_SEND_IN_OR_MT_OPTIONAL)
            {
                *pu1OptEncode = OSIX_TRUE;
            }

            MRP_PRINT_ATTRIBUTE (pAttr);
            break;
        default:
            break;
    }

    /* Apply rLA! on Reg SEM */
    if (u1Event == MRP_APP_TX_LA)
    {
        /* The MAP Port entry will not be deleted by the MrpSmApplicantSem 
         * Function. So the pMapPortEntry will never be NULL at this point. */

        if (u1RegAdmCtrl == MRP_REG_NORMAL)
        {
            /* As per IEEE standard 802.1ak section 10.7.5.20 */
            MrpSmRegistrarSem (pMapPortEntry, u2AttrIndex,
                               MRP_REG_RX_LEAVE_ALL, &u1RegSEMStateChg);
        }
        MrpSmApplicantSem (pMapPortEntry, u2AttrIndex, MRP_APP_RX_LEAVE_ALL);
    }

}

/*************************************************************************
 *
 *  FUNCTION NAME   : MrpTxEncodeAttrEvent
 *
 *  DESCRIPTION     : This function encodes  the Attribute Event values 
 *                    for transmission.
 *
 *  INPUT           : pu1Ptr       - to store encoded Attribute event values
 *                    u2NoOfValues - Number Of Values
 *                    u1AttrEvent  - Attribute Event
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : None
 *
 **************************************************************************/

VOID
MrpTxEncodeAttrEvent (UINT1 *pu1Ptr, UINT2 u2NoOfValues, UINT1 u1AttrEvent)
{
    UINT2               u2BytePos = 0;
    UINT2               u2BitPos = 0;

    u2BytePos = (UINT2) (u2NoOfValues / MRP_MAX_ATTR_EVENTS_PER_VECTOR);
    u2BitPos = (UINT2) ((u2NoOfValues % MRP_MAX_ATTR_EVENTS_PER_VECTOR));

    if (u2BitPos == 1)
    {
        pu1Ptr[u2BytePos] = (UINT1) (u1AttrEvent * 6 * 6);
    }
    else if (u2BitPos == 2)
    {
        pu1Ptr[u2BytePos] = (UINT1) (pu1Ptr[u2BytePos] + (u1AttrEvent * 6));
    }
    else
    {
        pu1Ptr[u2BytePos - 1] = (UINT1) (pu1Ptr[u2BytePos - 1] + u1AttrEvent);
    }
}

/*************************************************************************
 *
 *  FUNCTION NAME   : MrpTxIsSpaceInPkt
 *
 *  DESCRIPTION     : This function checks whether Space is present in the 
 *                    PDU or not.
 *
 *  INPUT           : u2CurrOffset - Current Offset in the PDU
 *                    u2NumOfValue - Number Of Values
 *                    u1AttrLen    - Attr Length
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : OSIX_TRUE/OSIX_FALSE
 *
 **************************************************************************/

INT4
MrpTxIsSpaceInPkt (UINT2 u2CurrOffset, UINT2 u2NumOfValue, UINT1 u1AttrLen)
{
    UINT2               u2AttrListSize = 0;
    UINT2               u2PduSize = 0;

    u2AttrListSize = MRP_VECTOR_HDR_SIZE;    /* LeaveAll + No. Of Values */
    u2AttrListSize = (UINT2) (u2AttrListSize + u1AttrLen);    /* First Value Length */
    u2AttrListSize =
        (UINT2) (u2AttrListSize +
                 ((u2NumOfValue / MRP_MAX_ATTR_EVENTS_PER_VECTOR) + 1));
    /* Vector Length */

    u2PduSize = (UINT2) (u2CurrOffset + u2AttrListSize +
                         (2 * MRP_END_MARK_SIZE));

    if (u2PduSize <= MRP_MAX_PDU_LEN)
    {
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

/*************************************************************************
 *
 *  FUNCTION NAME   : MrpTxIsSpaceInMVRPPkt
 *
 *  DESCRIPTION     : This function checks whether Space is present in the 
 *                    PDU or not.
 *
 *  INPUT           : u2CurrOffset - Current Offset in the PDU
 *                    u2NumOfValue - Number Of Values
 *                    u1AttrLen    - Attr Length
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : OSIX_TRUE/OSIX_FALSE
 *
 **************************************************************************/

INT4
MrpTxIsSpaceInMVRPPkt (UINT2 u2CurrOffset, UINT2 u2NumOfValue, UINT1 u1AttrLen)
{
    UINT2               u2AttrListSize = 0;
    UINT2               u2PduSize = 0;

    u2AttrListSize = MRP_VECTOR_HDR_SIZE;    /* LeaveAll + No. Of Values */
    u2AttrListSize = (UINT2) (u2AttrListSize + u1AttrLen);    /* First Value Length */
    u2AttrListSize =
        (UINT2) (u2AttrListSize +
                 ((u2NumOfValue / MRP_MAX_ATTR_EVENTS_PER_VECTOR) + 1));
    /* Vector Length */

    u2PduSize = (UINT2) (u2CurrOffset + u2AttrListSize +
                         MRP_MIN_MVRP_VECT_ATTR_LEN + (2 * MRP_END_MARK_SIZE));

    if (u2PduSize <= MRP_MAX_PDU_LEN)
    {
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

/*************************************************************************
 *
 *  FUNCTION NAME   : MrpTxAddVectorAttrToPdu
 *
 *  DESCRIPTION     : This function adds Vector Attribute to PDU for Tx.
 *
 *  INPUT           : ppu1Ptr       -  Pointer to the PDU buffer
 *                    u2VectorHdr   -  Vector Header
 *                    pAttr         -  Pointer to Attribute Structure
 *                    pu1Vector     - pointer to Vector
 *                    u2NumOfValue  - Number Of  Values
 *                    pu2Offset     - Pdu Offset
 *                    u1Flag        - Flag Value(Filling the  First value ot not)
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : None
 *
 **************************************************************************/

VOID
MrpTxAddVectorAttrToPdu (UINT1 **ppu1Ptr, UINT2 u2VectorHdr, tMrpAttr * pAttr,
                         UINT1 *pu1Vector, UINT2 u2NumOfValue, UINT2 *pu2Offset,
                         UINT1 u1Flag)
{
    UINT2               u2VectorLen = 0;

    /* Filling the Vector Header */
    if (u2VectorHdr != 0)
    {
        MRP_LBUF_PUT_2_BYTES (*ppu1Ptr, u2VectorHdr, *pu2Offset);

        if (u1Flag == OSIX_TRUE)
        {
            /* Filling the First Value */
            MRP_LBUF_PUT_N_BYTES (*ppu1Ptr, pAttr->au1AttrVal,
                                  MEM_MAX_BYTES (pAttr->u1AttrLen,
                                                 MRP_MAX_ATTR_LEN), *pu2Offset);
            /* Filling the VECTOR */
            if (u2NumOfValue != 0)
            {
                u2VectorLen = (UINT2) ((u2NumOfValue /
                                        MRP_MAX_ATTR_EVENTS_PER_VECTOR) + 1);
            }
            MRP_LBUF_PUT_N_BYTES (*ppu1Ptr, pu1Vector, u2VectorLen, *pu2Offset);
        }
    }
}

/*************************************************************************
 *
 *  FUNCTION NAME   : MrpTxAddEndMarkAndTxPdu
 *
 *  DESCRIPTION     : This function adds End mark and transmits the PDU.
 *
 *  INPUT           : u4ContextId   - Context Id
 *                    u1AppId       - Application Id
 *                    DestAddr      - Destination Address
 *                    u2BufLen      - Buffer Length
 *                    u2Port        - Port Id
 *                    u2MapId       - MapId
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : None
 *
 **************************************************************************/

VOID
MrpTxAddEndMarkAndTxPdu (UINT4 u4ContextId, UINT1 u1AppId,
                         tMacAddr DestAddr, UINT2 u2BufLen,
                         UINT2 u2Port, UINT2 u2MapId)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = 0;
    UINT1              *pu1StartBuf = NULL;
    UINT2               u2EtherType = 0;
    UINT2               u2Len = 0;
    UINT2               u2Offset = 0;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    pBuf = CRU_BUF_Allocate_MsgBufChain (u2BufLen, 0);

    if (pBuf == NULL)
    {
        return;
    }

    u4IfIndex = MRP_GET_PHY_PORT (u4ContextId, u2Port);

    if (MrpPortGetCfaIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return;
    }

    if (u1AppId == MRP_MVRP_APP_ID)
    {
        u2EtherType = (UINT2) MRP_MVRP_ETHER_TYPE;
    }
    else
    {
        u2EtherType = (UINT2) MRP_MMRP_ETHER_TYPE;
    }

    pu1StartBuf = gMrpGlobalInfo.pu1PduBuf;

    MRP_LBUF_PUT_MAC_ADDRESS (pu1StartBuf, DestAddr, u2Offset);

    MRP_LBUF_PUT_MAC_ADDRESS (pu1StartBuf, IfInfo.au1MacAddr, u2Offset);

    MRP_LBUF_PUT_2_BYTES (pu1StartBuf, u2EtherType, u2Offset);

    MRP_LBUF_PUT_1_BYTE (pu1StartBuf, MRP_PROTOCOL_VERSION, u2Offset);

    CRU_BUF_Copy_OverBufChain (pBuf, gMrpGlobalInfo.pu1PduBuf, 0, u2BufLen);

    if (u1AppId == MRP_MMRP_APP_ID)
    {
        i4RetVal = MrpPortTagOutgoingMmrpFrame (u4ContextId, pBuf,
                                                u2MapId, u2Port);

        if (i4RetVal == VLAN_NO_FORWARD)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, 0);
            return;
        }
    }

    u2Len = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);

    MrpPortHandleOutgoingPktOnPort (pBuf, u4ContextId, u4IfIndex, u2Len, 0,
                                    CFA_ENCAP_NONE);

    pMrpPortEntry = MRP_GET_PORT_ENTRY (MRP_CONTEXT_PTR (u4ContextId), u2Port);
    if (pMrpPortEntry == NULL)
    {
        /* Added to avoid Klockwork warning */
        return;
    }

    MrpUtilIncrTxStats (pMrpPortEntry, u1AppId, MRP_TX_PDU_CNT);
}

/*************************************************************************
 *
 *  FUNCTION NAME   : MrpTxAddPduHdrAndTxPdu
 *
 *  DESCRIPTION     : This function adds the MRPDU Header (Destination Addr,
 *                    Source Addr, Ethertype and Protocol Version) in the 
 *                    buffer and transmits the PDU.
 *
 *  INPUT           : u4ContextId   - Context Id
 *                    u1AppId       - Application Id
 *                    u2BufLen      - Buffer Length
 *                    u2Port        - Port Id
 *                    u2MapId       - MapId
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : None
 *
 **************************************************************************/

VOID
MrpTxAddPduHdrAndTxPdu (UINT4 u4ContextId, UINT1 u1AppId, UINT2 u2BufLen,
                        UINT2 u2Port, UINT2 u2MapId)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpPortEntry      *pPortEntry = NULL;
    tCfaIfInfo          IfInfo;
    tMacAddr            DestAddr;
    UINT4               u4IfIndex = 0;
    INT4                i4RetVal = 0;
    UINT1              *pu1StartBuf = NULL;
    UINT2               u2EtherType = 0;
    UINT2               u2Len = 0;
    UINT2               u2Offset = 0;

    /* Standby node should not transmit the PDUs */
    if (RM_STANDBY == MRP_RED_NODE_STATUS ())
    {
        return;
    }

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    pContextInfo = gMrpGlobalInfo.apContextInfo[u4ContextId];

    pPortEntry = MRP_GET_PORT_ENTRY (pContextInfo, u2Port);

    if (pPortEntry == NULL)
    {
        /* Added to avoid Klockwork warning */
        return;
    }

    pBuf = CRU_BUF_Allocate_MsgBufChain (u2BufLen, 0);

    if (pBuf == NULL)
    {
        return;
    }

    u4IfIndex = MRP_GET_PHY_PORT (u4ContextId, u2Port);

    if (MrpPortGetCfaIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return;
    }

    if (u1AppId == MRP_MVRP_APP_ID)
    {
        if (pPortEntry->u2BridgePortType == MRP_PROP_PROVIDER_NETWORK_PORT)
        {
            MEMCPY (DestAddr, gCustomerMvrpAddr, MRP_MAC_ADDR_LEN);
        }
        else
        {
            MEMCPY (DestAddr, pContextInfo->MvrpAddr, MRP_MAC_ADDR_LEN);
        }

        u2EtherType = (UINT2) MRP_MVRP_ETHER_TYPE;
    }
    else
    {
        MEMCPY (DestAddr, gMmrpAddr, MRP_MAC_ADDR_LEN);
        u2EtherType = (UINT2) MRP_MMRP_ETHER_TYPE;
    }

    pu1StartBuf = gMrpGlobalInfo.pu1PduBuf;

    MRP_LBUF_PUT_MAC_ADDRESS (pu1StartBuf, DestAddr, u2Offset);

    MRP_LBUF_PUT_MAC_ADDRESS (pu1StartBuf, IfInfo.au1MacAddr, u2Offset);

    MRP_LBUF_PUT_2_BYTES (pu1StartBuf, u2EtherType, u2Offset);

    MRP_LBUF_PUT_1_BYTE (pu1StartBuf, MRP_PROTOCOL_VERSION, u2Offset);

    CRU_BUF_Copy_OverBufChain (pBuf, gMrpGlobalInfo.pu1PduBuf, 0, u2BufLen);

    if (u1AppId == MRP_MMRP_APP_ID)
    {
        i4RetVal = MrpPortTagOutgoingMmrpFrame (u4ContextId, pBuf, u2MapId,
                                                u2Port);

        if (i4RetVal == VLAN_NO_FORWARD)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, 0);
            return;
        }
    }

    u2Len = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);

    MrpPortHandleOutgoingPktOnPort (pBuf, u4ContextId, u4IfIndex, u2Len, 0,
                                    CFA_ENCAP_NONE);

    MrpUtilIncrTxStats (pPortEntry, u1AppId, MRP_TX_PDU_CNT);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpTxFillOptEvntsInVector 
 *                                                                          
 *    DESCRIPTION      : This function returns the first attribute that  
 *                       needs to be transmitted.
 *
 *    INPUT            : pVectBuf    - Pointer to the Vector
 *                       pu1AttrEvent - List of events to be encoded
 *                       u1Count       - Count of events to be encoded
 *                       pu2NumOfValue - Number of values encoded in this
 *                                       Vector Attribute
 *                       pPortEntry  - Pointer to the Port entry
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to the first attribute entry
 *                                                                          
 ****************************************************************************/
VOID
MrpTxFillOptEvntsInVector (UINT1 *pVectBuf, UINT1 *pu1AttrEvent,
                           UINT1 u1Count, UINT2 *pu2NumOfValue,
                           tMrpPortEntry * pPortEntry, UINT1 u1AppId)
{
    UINT1               u1Index = 0;

    for (; u1Index < u1Count; u1Index++)
    {
        (*pu2NumOfValue)++;
        MrpTxEncodeAttrEvent (pVectBuf, *pu2NumOfValue, pu1AttrEvent[u1Index]);
        MrpUtilIncrTxStats (pPortEntry, u1AppId, pu1AttrEvent[u1Index]);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpTxFillMtEvntInVector 
 *                                                                          
 *    DESCRIPTION      : This function fills MT Attribute Event in the MRPDU
 *                       for optimal encoding.
 *
 *    INPUT            : pVectBuf    - Pointer to the Vector
 *                       pPortEntry  - Pointer to the Port entry
 *                       pu2NumOfValue - Number of values encoded in this 
 *                                       Vector Attribute
 *                       u1Count       - Count of MT events to be encoded
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Pointer to the first attribute entry
 *                                                                          
 ****************************************************************************/
VOID
MrpTxFillMtEvntInVector (UINT1 *pVectBuf, tMrpPortEntry * pPortEntry,
                         UINT2 *pu2NumOfValue, UINT1 u1Count)
{
    UINT1               u1Index = 0;

    for (; u1Index < u1Count; u1Index++)
    {
        (*pu2NumOfValue)++;
        MrpTxEncodeAttrEvent (pVectBuf, *pu2NumOfValue, MRP_MT_ATTR_EVENT);
        MrpUtilIncrTxStats (pPortEntry, MRP_MVRP_APP_ID, MRP_MT_ATTR_EVENT);
    }

}

/************************************************************************
 * FUNCTION NAME    : MrpTxAddMmrpMessageHdr 
 *                                                                      
 * DESCRIPTION      : This function adds Attribute type and length to the
 *                    MMRPDU
 *                                                                      
 * INPUT(s)         : ppu1MsgHdr - Pointer to the start of the MMRP Message
 *                    pAttrEntry - Pointer to Attribute Entry
 *                    *pu2Offset - Offset
 *                                                                      
 * OUTPUT(s)        : None                                              
 *                                                                      
 * RETURNS          : None                                              
 ************************************************************************/
VOID
MrpTxAddMmrpMessageHdr (UINT1 **ppu1MsgHdr, tMrpAttrEntry * pAttrEntry,
                        UINT2 *pu2Offset)
{
    if (pAttrEntry->Attr.u1AttrType == MRP_SERVICE_REQ_ATTR_TYPE)
    {
        MRP_LBUF_PUT_1_BYTE (*ppu1MsgHdr, (UINT1) MRP_SERVICE_REQ_ATTR_TYPE,
                             *pu2Offset);
        MRP_LBUF_PUT_1_BYTE (*ppu1MsgHdr, (UINT1) MRP_SERVICE_REQ_LEN,
                             *pu2Offset);
    }
    else
    {
        MRP_LBUF_PUT_1_BYTE (*ppu1MsgHdr, (UINT1) MRP_MAC_ADDR_ATTR_TYPE,
                             *pu2Offset);
        MRP_LBUF_PUT_1_BYTE (*ppu1MsgHdr, (UINT1) MRP_MAC_ADDR_LEN, *pu2Offset);
    }

}

/************************************************************************
 *
 * FUNCTION NAME    : MrpTxAddVectHdrAndUpdNoOfValues 
 *                                                                      
 * DESCRIPTION      : This function is called to add the Vector Header in
 *                    the Vector Attribute when the end of the Vector field
 *                    has been reached. If FirsValue field has been filled
 *                    in the next Vector Attribute then NumberOfValues is
 *                    set to 1, else it is set to 0 and the current buffer
 *                    location is moved to the end of the current Vector
 *                    Attribute.
 *                                                                      
 * INPUT(s)         : ppu1Ptr      - Pointer to the cuurent Buffer location  
 *                    ppu1AttrList - Pointer to the the location where the
 *                                   Vector Header needs to be filled
 *                    u2VectorHdr  - Vector Header
 *                    u1FirstValFilled - Indicates whether first value has been
 *                                       in the next Vector Attribute
 *                                                                      
 * OUTPUT(s)        : *pu2NumOfValues - Number of values filled in the current
 *                                      Vector Attribute
 *                    *pu2Offset      - Position of Buffer pointer
 *                                                                      
 * RETURNS          : None                                              
 *
 ************************************************************************/

VOID
MrpTxAddVectHdrAndUpdNoOfValues (UINT1 **ppu1Ptr, UINT1 **ppu1AttrList,
                                 UINT2 u2VectorHdr, UINT2 *pu2NumOfValues,
                                 UINT2 *pu2Offset, UINT1 u1AttrLen,
                                 UINT1 u1FirstValFilled)
{
    UINT2               u2VectorLen = 0;

    MRP_LBUF_PUT_2_BYTES (*ppu1AttrList, u2VectorHdr, *pu2Offset);

    u2VectorLen = (UINT2) MRP_GET_NUM_OF_VECTORS_FIELD (*pu2NumOfValues);
    *pu2Offset = (UINT2) (*pu2Offset + u2VectorLen);

    /* Move the pointer to the start of the Vector Header of the next 
     * Vector Attribute */
    *ppu1AttrList += (u1AttrLen + u2VectorLen);

    if (u1FirstValFilled == OSIX_FALSE)
    {
        /* Increment the value if first value has been filled for the 
         * next vector attribute */
        *pu2NumOfValues = 1;
    }
    else
    {
        /* First value has not been filled for the next vector attribute.
         * Hence move the pointer to start of the First value field
         */
        *ppu1Ptr += (u2VectorLen + MRP_VECTOR_HDR_SIZE);
        *pu2NumOfValues = 0;
    }

}

/************************************************************************
 *
 * FUNCTION NAME    : MrpTxHndLAEventForNonParticipant
 *
 * DESCRIPTION      : This function checks the Participant Type  and applies 
 *                    TX_LA event to Registrar Sem if the Participant type is 
 *                    Non Participant.
 *
 * INPUT(s)         : pMapPortEntry  - MAP Port Entry
 *                    pAttrEntry  - Attr Entry
 *                    u1AttrType  - Attribute Type 
 *
 * OUTPUT(s)        : pu1IsNonParticipant - Participant Type
 *
 * RETURNS          : None
 *
 ************************************************************************/
INT4
MrpTxHndLAEventForNonParticipant (tMrpMapPortEntry * pMapPortEntry,
                                  tMrpAttrEntry * pAttrEntry,
                                  UINT1 u1AttrType, UINT1 *pu1IsNonParticipant)
{
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    UINT1               u1RegSEMStateChg = 0;
    pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMapPortEntry,
                                                   pMapPortEntry->u2Port);

    if (pAppPortEntry == NULL)
    {
        return OSIX_FALSE;
    }
    pMrpPortEntry = pAppPortEntry->pMrpPortEntry;

    if (u1AttrType >= MRP_MAX_ATTR_TYPES)
    {
        return OSIX_FALSE;
    }

    if ((pMrpPortEntry->au1ApplAdminCtrl[MRP_MMRP_APP_ID][u1AttrType]
         == MRP_NON_PARTICIPANT))
    {
        /* Non-participant: Tx alone can be blocked. This means that
         * rLA event will be given to Registrar SEM.
         * */
        MrpSmRegistrarSem (pMapPortEntry, pAttrEntry->u2AttrIndex,
                           MRP_REG_RX_LEAVE_ALL, &u1RegSEMStateChg);
        MRP_TRC ((pMrpPortEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MMRP_TRC),
                  "MrpMmrpFormAndTransmitPdu: Port %u is Non-"
                  "Participant exiting.....\n", pMrpPortEntry->u2LocalPortId));
        *pu1IsNonParticipant = OSIX_TRUE;
        return OSIX_TRUE;

    }
    return OSIX_FALSE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrptx.c                        */
/*-----------------------------------------------------------------------*/
