/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpremap.c,v 1.11 2013/06/25 12:08:38 siva Exp $
 *
 * Description: This file contains the MRP Remap feature related functions.
 ******************************************************************************/

#include "mrpinc.h"

/**********************************************************************
 *
 * FUNCTION NAME    : MrpRemapHandleVlanInstanceMap                      
 *                                                                      
 * DESCRIPTION      : Handles the VLAN mapping to a new MST instance.           
 *                                                                      
 * INPUT(s)         : u4ContextId - Context Identifier
 *                    VlanId - VLAN Identifier                          
 *                    u2OldMapId       - Old MST Instance Identifier                 
 *                    u2NewInstance - New MST Instance Identifier           
 *                                                                      
 * OUTPUT(s)        : None                                              
 *                                                                      
 * RETURNS          : VOID                                              
 *
 ************************************************************************/

VOID
MrpRemapHandleVlanInstanceMap (UINT4 u4ContextId, tVlanId VlanId,
                               UINT2 u2OldMapId, UINT2 u2NewInstance)
{
    tMrpAppEntry       *pMvrpAppEntry = NULL;
    tMrpAppEntry       *pMmrpAppEntry = NULL;
    tMrpAttr            MrpAttr;
    INT2                i2RegCount = MRP_INVALID_REGCOUNT;
    UINT1               u1RemapFlag = MRP_REMAP_WITH_ATTR;
    UINT1               u1DynVlan = OSIX_TRUE;

    if ((u4ContextId == 0) || (u4ContextId >= MRP_SIZING_CONTEXT_COUNT))
    {
        return;
    }

    /* MRP Applications need to be present for handling remap */
    pMvrpAppEntry = MRP_GET_APP_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                       MRP_MVRP_APP_ID);

    pMmrpAppEntry = MRP_GET_APP_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                       MRP_MMRP_APP_ID);

    /* Here Instance 4094 is also checked because when a ESP VLAN is 
     * configured it is mapped to 4094 instance. ESP VLANs should not 
     * be propagated and should delete the propagated information also */
    if ((VLAN_TRUE == MrpPortVlanIsVlanDynamic (u4ContextId, VlanId)) ||
        (u2NewInstance == AST_TE_MSTID))
    {
        if (MrpMmrpIsEnabled (u4ContextId) == OSIX_TRUE)
        {
            MrpRemapWithoutAttrInNewInst (u4ContextId, pMmrpAppEntry,
                                          VlanId, u2OldMapId, u1DynVlan);
        }
        if (MrpMvrpIsEnabled (u4ContextId) == OSIX_TRUE)
        {
            MrpRemapWithoutAttrInNewInst (u4ContextId, pMvrpAppEntry,
                                          VlanId, u2OldMapId, u1DynVlan);
        }
    }
    else
    {
        u1DynVlan = OSIX_FALSE;
        /* First loop the existing information to obtain the RegCount.
         * Then loop again to act according to the count */
        if (MrpMvrpIsEnabled (u4ContextId) == OSIX_TRUE)
        {
            i2RegCount = MrpRemapUpdateMvrpRegCount (pMvrpAppEntry, VlanId,
                                                     u2OldMapId, u2NewInstance);
        }
        else if (MrpMmrpIsEnabled (u4ContextId) == OSIX_TRUE)
        {
            i2RegCount = MrpRemapUpdateVlanRegCount (pMmrpAppEntry,
                                                     VlanId, u2NewInstance);
        }

        if (i2RegCount < 0)
        {
            return;
        }

        if (0 == i2RegCount)
        {
            /* This means static reg present but ports are in BLOCKING state
             * in the new Instance. So handle like Dynamic thread */
            u1RemapFlag = MRP_REMAP_WITHOUT_ATTR;
        }

        if (MrpMmrpIsEnabled (u4ContextId) == OSIX_TRUE)
        {
            MrpMmrpHandleRemapEvent (pMmrpAppEntry, VlanId, u1RemapFlag,
                                     u1DynVlan);
        }
        if (MrpMvrpIsEnabled (u4ContextId) == OSIX_TRUE)
        {
            MrpAttr.u1AttrType = MRP_VID_ATTR_TYPE;

            MrpAttr.u1AttrLen = MRP_VLAN_ID_LEN;

            VlanId = (tVlanId) OSIX_HTONS (VlanId);
            MEMCPY (MrpAttr.au1AttrVal, &VlanId, MRP_VLAN_ID_LEN);

            MrpMvrpHdleRemapEvntForStEntries (pMvrpAppEntry, u2NewInstance,
                                              &MrpAttr, u1RemapFlag);
        }
    }
    return;
}

/***********************************************************************
 *
 * FUNCTION NAME    : MrpRemapWithoutAttrInNewInst              
 *                                                                      
 * DESCRIPTION      : Handles the Remap when the VLAN which is mapped to
 *                    new MST instance is dynamically learnt. It just sends
 *                    leave message through the port which was in forwarding
 *                    state in the previous MST Instance.
 *                                                                      
 * INPUT(s)         : u4ContextId - Context Identifier
 *                    pAppEntry   - Pointer to Application Entry          
 *                    u2VlanId    - VLAN Identifier                       
 *                    u2Instance  - Old MST Instance Identifier                  
 *                    u1DynVlan   - Indicates whether the VLAN which had been
 *                                  remapped to a new instance is dynamically
 *                                  learnt or not
 *                                                                      
 * OUTPUT(s)        : None                                              
 *                                                                      
 * RETURNS          : None                                              
 ************************************************************************/
VOID
MrpRemapWithoutAttrInNewInst (UINT4 u4ContextId, tMrpAppEntry * pAppEntry,
                              tVlanId VlanId, UINT2 u2Instance, UINT1 u1DynVlan)
{
    /* The function is called when the RegCount of the attr in new instance
     * is zero */
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    UINT2               u2MapId = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortState = 0;

    /* Retrieve MAP and Attribute Indices to work for each Application */
    if (pAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        u2MapId = u2Instance;

    }
    else if (pAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        u2MapId = VlanId;

    }
    else
    {
        return;
    }

    /* Retrieve the MapEntry */

    pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

    if (pMapEntry == NULL)
    {
        return;
    }

    if (pAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        /* Incase of MVRP, the Attribute Entry corresponding to this VLAN
         * needs to be removed */
        pAttrEntry = MRP_GET_ATTR_ENTRY (pMapEntry, VlanId);

        if (pAttrEntry == NULL)
        {
            return;
        }
    }

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

        if (pMapPortEntry == NULL)
        {
            continue;
        }
        pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMapPortEntry,
                                                       pMapPortEntry->u2Port);
        if (pAppPortEntry == NULL)
        {
            continue;
        }
        pMrpPortEntry = pAppPortEntry->pMrpPortEntry;

        if (pAppEntry->u1AppId == MRP_MMRP_APP_ID)
        {
            u1PortState = L2IwfGetVlanPortState (VlanId, u2Port);

            if (u1PortState == AST_PORT_STATE_FORWARDING)
            {
                pMapPortEntry->u1PortState = AST_PORT_STATE_FORWARDING;
            }
            else
            {
                pMapPortEntry->u1PortState = AST_PORT_STATE_DISCARDING;
            }
        }

        if (pAppEntry->u1AppId == MRP_MVRP_APP_ID)
        {
            /* Update Applicant SEM, Tx LEAVE/EMPTY */
            MrpMvrpRemapUpdateSemAndTx (u4ContextId, pAppEntry,
                                        pMrpPortEntry, pAttrEntry,
                                        pMapPortEntry, u1DynVlan,
                                        MRP_REMAP_WITHOUT_ATTR);
        }
        else if (pAppEntry->u1AppId == MRP_MMRP_APP_ID)
        {

            MrpRemapMmrpUpdateSemAndTx (MRP_CONTEXT_PTR (u4ContextId),
                                        pAppEntry, pMrpPortEntry, pMapEntry,
                                        pMapPortEntry, u1DynVlan,
                                        MRP_REMAP_WITHOUT_ATTR);
        }
    }

    return;
}

/***********************************************************************
 * FUNCTION NAME    : MrpRemapUpdateRegSem                          
 *                                                                      
 * DESCRIPTION      : Updates Registration SEM and VLAN Database        
 *                                                                      
 * INPUT(s)         : u4ContextId - Context Identifier
 *                    pAppEntry - Pointer to Application Entry          
 *                    pMapEntry - pointer to MAP Entry                  
 *                    pAttrEntry - pointer to attribute                 
 *                    u2VlanId - VLAN Identifier                        
 *                    u1VlanIpdate - To update VLAN Database            
 *                                                                      
 * OUTPUT(s)        : None                                              
 *                                                                      
 * RETURNS          : None                                              
 ************************************************************************/
VOID
MrpRemapUpdateRegSem (UINT4 u4ContextId, tMrpAppEntry * pAppEntry,
                      tMrpMapPortEntry * pMapPortEntry,
                      tMrpAttrEntry * pAttrEntry, tVlanId VlanId,
                      UINT1 u1VlanUpdate)
{
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    UINT2               u2AttrIndex = 0;
    UINT1               u1ServiceAttrType = 0;
    INT4                i4RetVal = VLAN_FAILURE;

    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);
    if (pContextInfo == NULL)
    {
        return;
    }
    u2AttrIndex = pAttrEntry->u2AttrIndex;
    pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMapPortEntry,
                                                   pMapPortEntry->u2Port);

    if (pAppPortEntry == NULL)
    {
        return;
    }

    if ((MRP_GET_ATTR_REG_ADMIN_CTRL
         (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_REG_NORMAL) &&
        (MRP_GET_ATTR_REG_SEM_STATE
         (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]) != MRP_MT))
    {
        if (MRP_GET_ATTR_REG_SEM_STATE
            (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_LV)
        {
            MrpTmrStopLeaveTmr (pMapPortEntry, (UINT4) u2AttrIndex);
        }

        MRP_SET_ATTR_REG_SEM_STATE
            (pMapPortEntry->pu1AttrInfoList[u2AttrIndex], MRP_MT);

        if (pAppEntry->u1AppId == MRP_MVRP_APP_ID)
        {
            i4RetVal =
                MrpPortVlanUpdateDynamicVlanInfo (u4ContextId, VlanId,
                                                  pMapPortEntry->u2Port,
                                                  VLAN_DELETE);

            if (i4RetVal == VLAN_FAILURE)
            {
                MRP_TRC ((pContextInfo, (ALL_FAILURE_TRC | MRP_PROTOCOL_TRC),
                          "MrpRemapUpdateRegSem: Failed to update "
                          "Dynamic Vlan Information for Vlan %d on Port "
                          "%d\n", VlanId, pAppPortEntry->pMrpPortEntry));
            }

        }
        else if (pAppEntry->u1AppId == MRP_MMRP_APP_ID)
        {
            if (OSIX_TRUE == u1VlanUpdate)
            {
                if (MRP_MAC_ADDR_ATTR_TYPE == pAttrEntry->Attr.u1AttrType)
                {

                    i4RetVal = MrpPortVlanUpdateDynamicMACInfo
                        (u4ContextId, pAttrEntry->Attr.au1AttrVal, VlanId,
                         pMapPortEntry->u2Port, VLAN_DELETE);

                    if (i4RetVal == VLAN_FAILURE)
                    {
                        MRP_TRC ((pContextInfo,
                                  (ALL_FAILURE_TRC | MRP_PROTOCOL_TRC),
                                  "MrpRemapUpdateRegSem : Failed to "
                                  "Update Dynamic Multicast "
                                  "Information for Vlan %d on Port %d\n",
                                  VlanId, pAppPortEntry->pMrpPortEntry));
                    }

                }
                else
                {
                    u1ServiceAttrType = pAttrEntry->Attr.au1AttrVal[0];
#ifdef VLAN_EXTENDED_FILTER

                    i4RetVal = MrpPortVlanUpdateDynDefGrpInfo
                        (u4ContextId, u1ServiceAttrType, VlanId,
                         pMapPortEntry->u2Port, VLAN_DELETE);

                    if (i4RetVal == VLAN_FAILURE)
                    {
                        MRP_TRC ((pContextInfo,
                                  (ALL_FAILURE_TRC | MRP_PROTOCOL_TRC),
                                  "MrpRemapUpdateRegSem : Failed to "
                                  "Update Dynamic Default group"
                                  "Information for Vlan %d on Port %d\n",
                                  VlanId, pAppPortEntry->pMrpPortEntry));
                    }
#endif /* VLAN_EXTENDED_FILTER */
                }
            }
            else
            {
                if (MRP_MAC_ADDR_ATTR_TYPE == pAttrEntry->Attr.u1AttrType)
                {
                    MrpPortDelDynamicMacInfoForVlan (u4ContextId, VlanId);
                }
                else
                {
                    u1ServiceAttrType = pAttrEntry->Attr.au1AttrVal[0];
#ifdef VLAN_EXTENDED_FILTER
                    MrpPortDelDynDefGroupInfoForVlan (u4ContextId, VlanId);
#endif
                }
            }
        }
    }

    return;
}

/**********************************************************************
 * FUNCTION NAME    : MrpRemapUpdateMvrpRegCount                       
 *                                                                     
 * DESCRIPTION      : Calculate MVRP Registration count for VlanId     
 *                    specified and moves the attribute to New MAP     
 *                                                                     
 * INPUT(s)         : pAppEntry - Pointer to Application Entry         
 *                    VlanId - VLAN Identifier                         
 *                    u2OldMapId - Old MAP Identifier                     
 *                    u2NewMapId - New MAP Identifier                  
 *                                                                     
 * OUTPUT(s)        : None                                             
 *                                                                     
 * RETURNS          : Registration count                               
 ************************************************************************/
INT2
MrpRemapUpdateMvrpRegCount (tMrpAppEntry * pAppEntry, tVlanId VlanId,
                            UINT2 u2OldMapId, UINT2 u2NewMapId)
{
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAttrEntry       AttrEntry;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    UINT2               u2AttrIndex = 0;
    UINT2               u2PortId = 0;
    UINT1               u1PortAppRegSemState = 0;
    INT2                i2RegCount = MRP_INVALID_REGCOUNT;

    /* Form the attribute of the given VLAN to be retrieved */
    MEMSET (&AttrEntry, 0, sizeof (tMrpAttrEntry));

    AttrEntry.Attr.u1AttrType = MRP_VID_ATTR_TYPE;
    AttrEntry.Attr.u1AttrLen = MRP_VLAN_ID_LEN;

    VlanId = (tVlanId) OSIX_HTONS (VlanId);
    MEMCPY (AttrEntry.Attr.au1AttrVal, &VlanId, AttrEntry.Attr.u1AttrLen);

    if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2OldMapId)) != NULL)
    {
        AttrEntry.pMapEntry = pMapEntry;

        pAttrEntry = RBTreeGet (pMapEntry->pMrpAppEntry->MapAttrTable,
                                (tRBElem *) & AttrEntry);
        if (pAttrEntry == NULL)
        {
            return i2RegCount;
        }
        u2AttrIndex = pAttrEntry->u2AttrIndex;

        for (u2PortId = 1; u2PortId <= MRP_MAX_PORTS_PER_CONTEXT; u2PortId++)
        {
            pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

            if (pMapPortEntry == NULL)
            {
                continue;
            }
            /* Before removing the attribute entry from the old 
             * instance should copy the Applicant, Registrar SEM state.
             * Because it should be copied to the new instance also */
            u1PortAppRegSemState = pMapPortEntry->pu1AttrInfoList[u2AttrIndex];

            if (MrpMapDSDelAttrFromMapPortEntry (pMapPortEntry, pAttrEntry)
                == OSIX_SUCCESS)
            {
                if (u1PortAppRegSemState != 0)
                {
                    i2RegCount =
                        MrpRemapAddAttrEntryInNewMap (pAppEntry, u2PortId,
                                                      u2NewMapId,
                                                      &(AttrEntry.Attr),
                                                      u1PortAppRegSemState);
                }
            }
        }
        MrpMapDsCheckAndDeleteMapEntry (pMapEntry);
    }
    return i2RegCount;
}

/***********************************************************************
 * FUNCTION NAME    : MrpRemapUpdateVlanRegCount                    
 *                                                                      
 * DESCRIPTION      : Calculates VLAN Registration count in New MAP     
 *                    in the absence of MVRP                            
 *                                                                      
 * INPUT(s)         : pMmrpAppEntry - Pointer to MMRP Application Entry 
 *                    VlanId - VLAN Identifier                          
 *                    u2NewMapId - New MAP Identifier                   
 *                                                                      
 * OUTPUT(s)        : None                                              
 *                                                                      
 * RETURNS          : i2RegCount - Registration count                     
 ************************************************************************/
INT2
MrpRemapUpdateVlanRegCount (tMrpAppEntry * pMmrpAppEntry, tVlanId VlanId,
                            UINT2 u2NewMapId)
{
    UINT1              *pPortList = NULL;
    UINT4               u4ContextId = 0;
    INT2                i2RegCount = MRP_INVALID_REGCOUNT;
    UINT2               au2ConfPorts[MRP_MAX_PORTS_PER_CONTEXT + 1];
    UINT2               u2NumPorts = 0;
    UINT2               u2TempPort = 0;
    UINT1               u1PortState = 0;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;

    u4ContextId = pMmrpAppEntry->pMrpContextInfo->u4ContextId;

    pMapEntry = MRP_GET_MAP_ENTRY (pMmrpAppEntry, VlanId);

    if (pMapEntry == NULL)
    {
        return i2RegCount;
    }

    /* When MVRP is not there in the system, Egress Ports can be only
       static ports */
    pPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList == NULL)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpRemapUpdateVlanRegCount : Error in allocating memory "
                     "for pPortList\r\n");
        return i2RegCount;
    }
    MEMSET (pPortList, 0, sizeof (tLocalPortList));
    MrpPortGetVlanLocalEgressPorts (u4ContextId, VlanId, pPortList);

    UtilGetPortArrayFromPortList (pPortList, sizeof (tLocalPortList),
                                  MRP_MAX_PORTS_PER_CONTEXT, au2ConfPorts,
                                  &u2NumPorts);

    for (u2TempPort = 1; ((u2TempPort <= MRP_MAX_PORTS_PER_CONTEXT)
                          && (u2TempPort <= u2NumPorts)); u2TempPort++)
    {
        pMrpPortEntry = MRP_GET_PORT_ENTRY (MRP_CONTEXT_PTR (u4ContextId),
                                            au2ConfPorts[u2TempPort]);

        if (pMrpPortEntry == NULL)
        {
            continue;
        }

        u1PortState = MrpUtilGetInstPortState (u2NewMapId, pMrpPortEntry);

        if (u1PortState == AST_PORT_STATE_FORWARDING)
        {
            i2RegCount++;
        }
    }

    UtilPlstReleaseLocalPortList (pPortList);
    return i2RegCount;
}

/***********************************************************************
 * FUNCTION NAME    : MrpRemapAddAttrEntryInNewMap                        
 *                                                                      
 * DESCRIPTION      : Adds the Attribute Entry in the MAP Context       
 *                                                                      
 * INPUT(s)         : pAppEntry  - Pointer to App Entry
 *                    u2PortId   - Local Port Identifier
 *                    u2NewMapId - New MAP Identifier
 *                    pAttr      - Pointer to Attribute Information
 *                    u1PortAppRegSemState - Contains the states of Applicant
 *                                           and Registrar SEM in the previous
 *                                           MAP Context
 *                                                                      
 * OUTPUT(s)        : None                                              
 *                                                                      
 * RETURNS          : i2RegCount - Number of ports that are in forwarding
 *                                 state in the new MAP Context
 ************************************************************************/
INT2
MrpRemapAddAttrEntryInNewMap (tMrpAppEntry * pAppEntry, UINT2 u2PortId,
                              UINT2 u2NewMapId, tMrpAttr * pAttr,
                              UINT1 u1PortAppRegSemState)
{
    tMrpAttrEntry      *pAttrEntryInNewMap = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    INT2                i2RegCount = 0;

    if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2NewMapId)) == NULL)
    {
        pMapEntry = MrpMapDSCreateMrpMapEntry (pAppEntry, u2NewMapId);

        if (pMapEntry == NULL)
        {
            return MRP_INVALID_REGCOUNT;
        }
    }

    pAttrEntryInNewMap = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMapEntry);

    if (pAttrEntryInNewMap == NULL)
    {
        pAttrEntryInNewMap = MrpMapDSCreateMrpMapAttrEntry (pMapEntry, pAttr);

        if (pAttrEntryInNewMap == NULL)
        {
            return MRP_INVALID_REGCOUNT;
        }
    }

    pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2PortId);

    if (pMapPortEntry == NULL)
    {
        pMapPortEntry = MrpMapDSCreateMrpMapPortEntry (pMapEntry, u2PortId);

        if (pMapPortEntry == NULL)
        {
            MRP_TRC ((pAppEntry->pMrpContextInfo,
                      (CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC | MRP_PROTOCOL_TRC),
                      "MrpRemapUpdateMvrpRegCount:"
                      "No Free MAP Port Entry \n"));
            return MRP_INVALID_REGCOUNT;
        }
    }

    if (MrpMapDSAddAttrToMapPortEntry (pMapPortEntry, pAttrEntryInNewMap)
        != OSIX_SUCCESS)
    {

        MRP_TRC ((pAppEntry->pMrpContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                   MRP_PROTOCOL_TRC),
                  "MrpRemapUpdateMvrpRegCount:"
                  "Adding attribute to " "MrpMapPortEntry Failed \n"));
        return MRP_INVALID_REGCOUNT;
    }

    /* Copy the Applicant, Registrar SEM sate that 
     * copied previously to the new Map Port entry */
    pMapPortEntry->pu1AttrInfoList[pAttrEntryInNewMap->u2AttrIndex]
        = u1PortAppRegSemState;

    /* Update the number of ports that are statically confugured for this
     * Attribute value
     */
    if (MRP_GET_ATTR_REG_ADMIN_CTRL
        (pMapPortEntry->pu1AttrInfoList[pAttrEntryInNewMap->u2AttrIndex])
        == MRP_REG_FIXED)
    {
        pAttrEntryInNewMap->u2StaticMemPortCnt++;
        /* Check Stap Status to update count */
        if (AST_PORT_STATE_FORWARDING == pMapPortEntry->u1PortState)
        {
            i2RegCount++;
        }
    }
    return i2RegCount;
}

/***********************************************************************
 *
 * FUNCTION NAME    : MrpRemapFormMMRPDU 
 *                                                                      
 * DESCRIPTION      : This function forms the MMRPDU which needs to be 
 *                    transmitted out on the event of Remap.
 *                                                                      
 * INPUT(s)         : ppBuf           - Buffer Pointer 
 *                    pVectAttrInfo   - Pointer to VectAttrInfo structure
 *                    u1AttrEvent     - Attribute Event to be encoded in the 
 *                                      Vector                              
 *                                                                      
 * OUTPUT(s)        : *pu2Offset      - Current position of the Buffer Pointer 
 *                    *pu2NumOfValues - Number of Attribute Values filled in
 *                                      the Vector Attribute
 *                    *pu1AddVectHdr  - Indicates whether Vector Header for the
 *                                      current Vector Attribute needs to be
 *                                      added or not
 *                                                                      
 * RETURNS          : None 
 *
 ************************************************************************/
VOID
MrpRemapFormMMRPDU (UINT1 **ppBuf, tMrpVectAttrInfo * pVectAttrInfo,
                    UINT2 *pu2Offset, UINT2 *pu2NumOfValues, UINT1 u1AttrEvent,
                    UINT1 *pu1AddVectHdr)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpAttr           *pAttr = NULL;
    INT4                i4AttrDiff = 0;
    UINT2               u2VectorLen = 0;
    UINT1               u1SetEvntForFirstVal = OSIX_FALSE;

    pAttr = pVectAttrInfo->pAttr;
    pAppPortEntry =
        MRP_GET_APP_PORT_FRM_MAP_PORT (pVectAttrInfo->pMapPortEntry,
                                       pVectAttrInfo->pMapPortEntry->u2Port);

    if (pAppPortEntry == NULL)
    {
        return;
    }

    pPortEntry = pAppPortEntry->pMrpPortEntry;

    if (pVectAttrInfo->u1IsFirstValSet == OSIX_FALSE)
    {
        /* This Attribute value needs to be stored as the First Value of
         * the current Vector Attribute */

        MRP_LBUF_PUT_N_BYTES (*ppBuf, pAttr->au1AttrVal,
                              MEM_MAX_BYTES (pAttr->u1AttrLen,
                                             MRP_MAX_ATTR_LEN), *pu2Offset);

        pVectAttrInfo->u1IsFirstValSet = OSIX_TRUE;
        /* Event for the First value needs to be set in the Vector. */
        u1SetEvntForFirstVal = OSIX_TRUE;
    }
    else
    {
        i4AttrDiff = MrpMmrpDiffAttrValue (pVectAttrInfo->LastMacAddr,
                                           pAttr->au1AttrVal, pAttr->u1AttrLen);
    }

    /* Store the attribute event in the PDU if the attribute value is within 
     * the optimal encoding interval or if it is the first value. */
    if ((i4AttrDiff < MRP_OPTIMAL_ENCODE_INTERVAL) ||
        (u1SetEvntForFirstVal == OSIX_TRUE))
    {
        if ((u1SetEvntForFirstVal == OSIX_FALSE) && (i4AttrDiff > 1))
        {
            /* Mt event needs to be filled for the intermediate values to 
             * result in optimal encoding
             */
            MrpTxFillMtEvntInVector (*ppBuf, pPortEntry, pu2NumOfValues,
                                     (UINT1) (i4AttrDiff - 1));
        }

        MEMCPY (pVectAttrInfo->LastMacAddr, pAttr->au1AttrVal,
                MEM_MAX_BYTES (pAttr->u1AttrLen, MRP_MAX_ATTR_LEN));
        (*pu2NumOfValues)++;
        MrpTxEncodeAttrEvent (*ppBuf, *pu2NumOfValues, u1AttrEvent);
        MrpUtilIncrTxStats (pPortEntry, MRP_MMRP_APP_ID, u1AttrEvent);
    }
    else
    {
        *pu1AddVectHdr = OSIX_TRUE;
        /* This Attribute value needs to be stored as the First Value in 
         * the next Vector Attribute */

        /* Length of the current Vector Attribute */
        u2VectorLen = (UINT2) MRP_GET_NUM_OF_VECTORS_FIELD (*pu2NumOfValues);

        /* Buffer pointer now points to the start of the Vector field of the
         * current Vector Attribute. Move the buffer pointer to point to the 
         * end of Vector Header of the next Vector Attribute.
         */
        *ppBuf += (MRP_VECTOR_HDR_SIZE + u2VectorLen);
        /* Store the first value of the next Vector Attribute */
        MRP_LBUF_PUT_N_BYTES (*ppBuf, pAttr->au1AttrVal,
                              MEM_MAX_BYTES (pAttr->u1AttrLen,
                                             MRP_MAX_ATTR_LEN), *pu2Offset);
        MEMCPY (pVectAttrInfo->LastMacAddr, pAttr->au1AttrVal,
                MEM_MAX_BYTES (pAttr->u1AttrLen, MRP_MAX_ATTR_LEN));
        pVectAttrInfo->u1IsFirstValSet = OSIX_TRUE;
        MrpTxEncodeAttrEvent (*ppBuf, 1, u1AttrEvent);
        MrpUtilIncrTxStats (pPortEntry, MRP_MMRP_APP_ID, u1AttrEvent);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpRemapMmrpUpdateSemAndTx
 *                                                                          
 *    DESCRIPTION      : This function constructs the MMRPDU and transmits 
 *                       the PDU out while handling the remap event.
 *
 *    INPUT            : pContextInfo - Context Info Pointer 
 *                       pAppEntry - Application entry
 *                       pPortEntry - MrpPort Entry
 *                       pMapEntry  - Map Entry
 *                       pMapPortEntry - Map Port Entry
 *                       u1DynVlan   - Indicates whether the VLAN is dynamically
 *                                     learnt or not
 *                       u1AttrFlag    - Flag is used to find whether the 
 *                                       remap is with attribute or not
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpRemapMmrpUpdateSemAndTx (tMrpContextInfo * pContextInfo,
                            tMrpAppEntry * pAppEntry,
                            tMrpPortEntry * pPortEntry,
                            tMrpMapEntry * pMapEntry,
                            tMrpMapPortEntry * pMapPortEntry, UINT1 u1DynVlan,
                            UINT1 u1AttrFlag)
{
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAttrEntry      *pNextAttrEntry = NULL;
    tMrpVectAttrInfo    VectAttrInfo;
    UINT2               u2Port = 0;
    UINT2               u2Offset = MRP_PDU_HDR_SIZE;
    UINT2               u2VectorHdr = 0;
    UINT2               u2NumOfValues = 0;
    UINT2               u2EndMark = MRP_END_MARK_VALUE;
    UINT2               u2AttrIndex = 0;
    UINT2               u2VectorLen = 0;
    UINT1               u1AddVectHdr = 0;
    UINT1              *pu1Ptr = NULL;
    UINT1              *pu1AttrList = NULL;
    UINT1               u1AppSemState = 0;
    UINT1               u1RegSemState = 0;
    UINT1               u1AttrEvent = MRP_INVALID_EVENT;
    UINT1               u1Flag = 0;
    UINT1               u1IsMsgHdrFilled = OSIX_FALSE;

    u2Port = pPortEntry->u2LocalPortId;

    MEMSET (&VectAttrInfo, 0, sizeof (tMrpVectAttrInfo));
    MEMSET (gMrpGlobalInfo.pu1PduBuf, 0, MRP_MAX_PDU_LEN);
    pu1Ptr = gMrpGlobalInfo.pu1PduBuf + MRP_PDU_HDR_SIZE;

    pAttrEntry = MrpMmrpGetFirstAttrForTx (pMapPortEntry);

    if (pAttrEntry == NULL)
    {
        MRP_TRC ((pContextInfo,
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC | MRP_MMRP_TRC),
                  "MrpRemapMmrpUpdateSemAndTx :"
                  "No Attributes exists for Port %d.\n", u2Port));
        return;
    }

    while (pAttrEntry != NULL)
    {
        /* Get the attribute index for accessing the attrInfoList from 
         * MapPortEntry */
        u2AttrIndex = pAttrEntry->u2AttrIndex;
        u1AppSemState = MRP_GET_ATTR_APP_SEM_STATE
            (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]);
        u1RegSemState = MRP_GET_ATTR_REG_SEM_STATE
            (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]);

        /* if attribute is not present then continue to get the next 
         * attribute */
        if ((u1AppSemState == MRP_VO) && (u1RegSemState == MRP_MT))
        {
            pNextAttrEntry = MrpMmrpGetNextAttrForTx
                (pMapPortEntry, pAttrEntry, &u1Flag, OSIX_TRUE);
            pAttrEntry = pNextAttrEntry;
            continue;
        }

        u1AttrEvent = MRP_INVALID_EVENT;
        /* Determine what message to send */
        if ((u1AppSemState == MRP_AN) || (u1AppSemState == MRP_AA) ||
            (u1AppSemState == MRP_QA))
        {
            u1AttrEvent = MRP_LV_ATTR_EVENT;
        }
        else if ((MRP_GET_ATTR_REG_ADMIN_CTRL
                  (pMapPortEntry->pu1AttrInfoList[u2AttrIndex])
                  == MRP_REG_NORMAL) && (u1RegSemState == MRP_IN))
        {
            u1AttrEvent = MRP_MT_ATTR_EVENT;
        }

        if (u1AttrEvent != MRP_INVALID_EVENT)
        {
            if (u1IsMsgHdrFilled == OSIX_FALSE)
            {
                MrpTxAddMmrpMessageHdr (&pu1Ptr, pAttrEntry, &u2Offset);
                /* Set the pointer to point to the start of Vector Header */
                pu1AttrList = pu1Ptr;
                pu1Ptr += MRP_VECTOR_HDR_SIZE;
                u1IsMsgHdrFilled = OSIX_TRUE;
            }

            /* Update App SEM to VO */
            MRP_SET_ATTR_APP_SEM_STATE
                (pMapPortEntry->pu1AttrInfoList[u2AttrIndex], MRP_VO);
            VectAttrInfo.pAttr = &(pAttrEntry->Attr);
            VectAttrInfo.u2AttrIndex = pAttrEntry->u2AttrIndex;
            VectAttrInfo.pMapPortEntry = pMapPortEntry;

            MrpRemapFormMMRPDU (&pu1Ptr, &VectAttrInfo, &u2Offset,
                                &u2NumOfValues, u1AttrEvent, &u1AddVectHdr);
            if (u1AddVectHdr == OSIX_TRUE)
            {
                /* End of Vector has reached for current Vector Attribute.
                 * Hence add Vector Header */
                u2VectorHdr |= u2NumOfValues;
                MrpTxAddVectHdrAndUpdNoOfValues (&pu1Ptr, &pu1AttrList,
                                                 u2VectorHdr,
                                                 &u2NumOfValues, &u2Offset,
                                                 pAttrEntry->Attr.u1AttrLen,
                                                 VectAttrInfo.u1OptEncode);
                u2VectorHdr = 0;
            }
        }
        pNextAttrEntry =
            MrpMmrpGetNextAttrForTx (pMapPortEntry, pAttrEntry, &u1Flag,
                                     OSIX_TRUE);

        if ((pNextAttrEntry != NULL) && (u1AttrEvent != MRP_INVALID_EVENT))
        {
            if ((u1Flag == OSIX_FALSE) && (pu1AttrList != NULL))
            {
                /* Attribute types are different. Hence fill this attribute
                 * in the next Message
                 */
                u2VectorHdr = u2VectorHdr | u2NumOfValues;
                /* Fill the Vector Header for the current Vector Attribute */
                MRP_LBUF_PUT_2_BYTES (pu1AttrList, u2VectorHdr, u2Offset);
                /* Add EndMark to denote the End of the Message */
                MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2EndMark, u2Offset);
                /* For MAC Address */
                MrpTxAddMmrpMessageHdr (&pu1Ptr, pNextAttrEntry, &u2Offset);
                pu1AttrList = pu1Ptr;
                u2VectorHdr = 0;
                u2NumOfValues = 0;
            }

            if ((MrpTxIsSpaceInPkt (u2Offset, u2NumOfValues,
                                    pNextAttrEntry->Attr.u1AttrLen) ==
                 OSIX_FALSE) && (pu1AttrList != NULL))
            {
                u2VectorHdr = u2VectorHdr | u2NumOfValues;
                MRP_LBUF_PUT_2_BYTES (pu1AttrList, u2VectorHdr, u2Offset);
                u2VectorLen =
                    (UINT2) MRP_GET_NUM_OF_VECTORS_FIELD (u2NumOfValues);
                u2Offset = (UINT2) (u2Offset + u2VectorLen);
                pu1Ptr += u2VectorLen;
                /* Add EndMark to denote the end of Attribute List. */
                MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2EndMark, u2Offset);
                /* Add EndMark to denote the end of the MRPDU */
                MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2EndMark, u2Offset);

                MrpTxAddPduHdrAndTxPdu (pContextInfo->u4ContextId,
                                        MRP_MMRP_APP_ID, u2Offset, u2Port,
                                        pMapPortEntry->pMapEntry->u2MapId);

            }
        }
        else if ((pNextAttrEntry == NULL) && (u1AttrEvent != MRP_INVALID_EVENT)
                 && (pu1AttrList != NULL))
        {
            u2VectorHdr = u2VectorHdr | u2NumOfValues;
            MRP_LBUF_PUT_2_BYTES (pu1AttrList, u2VectorHdr, u2Offset);
            u2VectorLen = (UINT2) MRP_GET_NUM_OF_VECTORS_FIELD (u2NumOfValues);
            u2Offset = (UINT2) (u2Offset + u2VectorLen);
            pu1Ptr += u2VectorLen;
            /* Add EndMark to denote the end of Attribute List. */
            MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2EndMark, u2Offset);
            /* Add EndMark to denote the end of the MRPDU */
            MRP_LBUF_PUT_2_BYTES (pu1Ptr, u2EndMark, u2Offset);

            MrpTxAddPduHdrAndTxPdu (pContextInfo->u4ContextId, MRP_MMRP_APP_ID,
                                    u2Offset, u2Port,
                                    pMapPortEntry->pMapEntry->u2MapId);

            /* Add the Vector Hdr and EndMark */
            pMapPortEntry->pNextMmrpTxNode = NULL;
        }
        /* Check Registrar SEM, update VLAN DB */
        /* The last parameter is to indicate whether to delete
         * specific MCAST or all MCAST in VLAN */
        if (u1DynVlan == OSIX_TRUE)
        {
            /* Dynamically learnt VLAN. Hence give indication to VLAN module
             * to delete all the MAC learnt on this VLAN 
             */
            MrpRemapUpdateRegSem (pContextInfo->u4ContextId, pAppEntry,
                                  pMapPortEntry, pAttrEntry, pMapEntry->u2MapId,
                                  OSIX_FALSE);
        }
        else
        {
            /* VLAN (MAP ID) is statically configured */
            MrpRemapUpdateRegSem (pContextInfo->u4ContextId, pAppEntry,
                                  pMapPortEntry, pAttrEntry, pMapEntry->u2MapId,
                                  OSIX_TRUE);
        }

        if (u1AttrFlag == MRP_REMAP_WITH_ATTR)
        {
            /* If Port is in Forwarding apply REQ_JOIN */
            if (AST_PORT_STATE_FORWARDING == pMapPortEntry->u1PortState)
            {
                MrpSmApplicantSem (pMapPortEntry, pAttrEntry->u2AttrIndex,
                                   MRP_APP_REQ_JOIN);
            }

        }

        /* Delete the attribute from the MapAttrTable once the RegSem is 
         * updated */
        MrpAttrCheckAndDelAttrEntry (pMapPortEntry, pAttrEntry);
        pAttrEntry = pNextAttrEntry;
    }

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpremap.c                     */
/*-----------------------------------------------------------------------*/
