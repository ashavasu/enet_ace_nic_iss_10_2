/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1q1lw.c,v 1.4 2009/10/08 07:00:21 prabuc Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "mrpinc.h"

/* LOW LEVEL Routines for Table : Ieee8021QBridgeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeTable (UINT4
                                              u4Ieee8021QBridgeComponentId)
{
    return (nmhValidateIndexInstanceFsMrpInstanceTable
            (u4Ieee8021QBridgeComponentId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeTable (UINT4 *pu4Ieee8021QBridgeComponentId)
{
    return (nmhGetFirstIndexFsMrpInstanceTable (pu4Ieee8021QBridgeComponentId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
                nextIeee8021QBridgeComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeTable (UINT4 u4Ieee8021QBridgeComponentId,
                                     UINT4 *pu4NextIeee8021QBridgeComponentId)
{
    return (nmhGetNextIndexFsMrpInstanceTable
            (u4Ieee8021QBridgeComponentId, pu4NextIeee8021QBridgeComponentId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanVersionNumber
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeVlanVersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanVersionNumber (UINT4 u4Ieee8021QBridgeComponentId,
                                        INT4
                                        *pi4RetValIeee8021QBridgeVlanVersionNumber)
{
    UNUSED_PARAM (u4Ieee8021QBridgeComponentId);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeVlanVersionNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeMaxVlanId
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeMaxVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeMaxVlanId (UINT4 u4Ieee8021QBridgeComponentId,
                                INT4 *pi4RetValIeee8021QBridgeMaxVlanId)
{
    UNUSED_PARAM (u4Ieee8021QBridgeComponentId);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeMaxVlanId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeMaxSupportedVlans
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeMaxSupportedVlans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeMaxSupportedVlans (UINT4 u4Ieee8021QBridgeComponentId,
                                        UINT4
                                        *pu4RetValIeee8021QBridgeMaxSupportedVlans)
{
    UNUSED_PARAM (u4Ieee8021QBridgeComponentId);
    UNUSED_PARAM (pu4RetValIeee8021QBridgeMaxSupportedVlans);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeNumVlans
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeNumVlans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeNumVlans (UINT4 u4Ieee8021QBridgeComponentId,
                               UINT4 *pu4RetValIeee8021QBridgeNumVlans)
{
    UNUSED_PARAM (u4Ieee8021QBridgeComponentId);
    UNUSED_PARAM (pu4RetValIeee8021QBridgeNumVlans);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                retValIeee8021QBridgeMvrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeMvrpEnabledStatus (UINT4 u4Ieee8021QBridgeComponentId,
                                        INT4
                                        *pi4RetValIeee8021QBridgeMvrpEnabledStatus)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021QBridgeComponentId);

    if (pMrpContextInfo != NULL)
    {
        *pi4RetValIeee8021QBridgeMvrpEnabledStatus =
            gMrpGlobalInfo.au1MvrpStatus[u4Ieee8021QBridgeComponentId];
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                setValIeee8021QBridgeMvrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeMvrpEnabledStatus (UINT4 u4Ieee8021QBridgeComponentId,
                                        INT4
                                        i4SetValIeee8021QBridgeMvrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if (MRP_IS_MRP_STARTED (u4Ieee8021QBridgeComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021QBridgeComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (gMrpGlobalInfo.au1MvrpStatus[u4Ieee8021QBridgeComponentId] ==
        i4SetValIeee8021QBridgeMvrpEnabledStatus)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValIeee8021QBridgeMvrpEnabledStatus)
    {
        case MRP_ENABLED:

            i4RetVal = MrpMvrpEnable (pMrpContextInfo);

            break;

        case MRP_DISABLED:
            i4RetVal = MrpMvrpDisable (pMrpContextInfo);

            break;

        default:
            break;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021QBridgeComponentId

                The Object 
                testValIeee8021QBridgeMvrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeMvrpEnabledStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Ieee8021QBridgeComponentId,
                                           INT4
                                           i4TestValIeee8021QBridgeMvrpEnabledStatus)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT2               u2VlanIsStarted = 0;

    if (MRP_IS_VC_VALID (u4Ieee8021QBridgeComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021QBridgeComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021QBridgeComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (i4TestValIeee8021QBridgeMvrpEnabledStatus == MRP_ENABLED)
    {
        u2VlanIsStarted = (UINT2) MrpPortVlanGetStartedStatus
            (u4Ieee8021QBridgeComponentId);
        /* Before Start the MRP module , VLAN module should
           be started.Otherwise no use of starting MRP. */
        if (u2VlanIsStarted == VLAN_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_NOSHUT_VLAN_ENABLED_ERR);
            return SNMP_FAILURE;
        }

        if (MrpPortIsPvrstStartedInContext (u4Ieee8021QBridgeComponentId) ==
            PVRST_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_MRP_PVRST_STARTED_ERR);
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValIeee8021QBridgeMvrpEnabledStatus != MRP_ENABLED) &&
        (i4TestValIeee8021QBridgeMvrpEnabledStatus != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeTable
 Input       :  The Indices
                Ieee8021QBridgeComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeCVlanPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeCVlanPortTable (UINT4
                                                       u4Ieee8021QBridgeCVlanPortComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeCVlanPortNumber)
{
    UNUSED_PARAM (u4Ieee8021QBridgeCVlanPortComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeCVlanPortNumber);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeCVlanPortTable (UINT4
                                               *pu4Ieee8021QBridgeCVlanPortComponentId,
                                               UINT4
                                               *pu4Ieee8021QBridgeCVlanPortNumber)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeCVlanPortComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeCVlanPortNumber);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                nextIeee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
                nextIeee8021QBridgeCVlanPortNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeCVlanPortTable (UINT4
                                              u4Ieee8021QBridgeCVlanPortComponentId,
                                              UINT4
                                              *pu4NextIeee8021QBridgeCVlanPortComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeCVlanPortNumber,
                                              UINT4
                                              *pu4NextIeee8021QBridgeCVlanPortNumber)
{
    UNUSED_PARAM (u4Ieee8021QBridgeCVlanPortComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeCVlanPortComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeCVlanPortNumber);
    UNUSED_PARAM (pu4NextIeee8021QBridgeCVlanPortNumber);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeCVlanPortRowStatus
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber

                The Object 
                retValIeee8021QBridgeCVlanPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeCVlanPortRowStatus (UINT4
                                         u4Ieee8021QBridgeCVlanPortComponentId,
                                         UINT4 u4Ieee8021QBridgeCVlanPortNumber,
                                         INT4
                                         *pi4RetValIeee8021QBridgeCVlanPortRowStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeCVlanPortComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeCVlanPortNumber);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeCVlanPortRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeCVlanPortRowStatus
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber

                The Object 
                setValIeee8021QBridgeCVlanPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeCVlanPortRowStatus (UINT4
                                         u4Ieee8021QBridgeCVlanPortComponentId,
                                         UINT4 u4Ieee8021QBridgeCVlanPortNumber,
                                         INT4
                                         i4SetValIeee8021QBridgeCVlanPortRowStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeCVlanPortComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeCVlanPortNumber);
    UNUSED_PARAM (i4SetValIeee8021QBridgeCVlanPortRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeCVlanPortRowStatus
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber

                The Object 
                testValIeee8021QBridgeCVlanPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeCVlanPortRowStatus (UINT4 *pu4ErrorCode,
                                            UINT4
                                            u4Ieee8021QBridgeCVlanPortComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeCVlanPortNumber,
                                            INT4
                                            i4TestValIeee8021QBridgeCVlanPortRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeCVlanPortComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeCVlanPortNumber);
    UNUSED_PARAM (i4TestValIeee8021QBridgeCVlanPortRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeCVlanPortTable
 Input       :  The Indices
                Ieee8021QBridgeCVlanPortComponentId
                Ieee8021QBridgeCVlanPortNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeCVlanPortTable (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeFdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeFdbTable (UINT4
                                                 u4Ieee8021QBridgeFdbComponentId,
                                                 UINT4 u4Ieee8021QBridgeFdbId)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeFdbTable (UINT4
                                         *pu4Ieee8021QBridgeFdbComponentId,
                                         UINT4 *pu4Ieee8021QBridgeFdbId)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeFdbId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                nextIeee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                nextIeee8021QBridgeFdbId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeFdbTable (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                        UINT4
                                        *pu4NextIeee8021QBridgeFdbComponentId,
                                        UINT4 u4Ieee8021QBridgeFdbId,
                                        UINT4 *pu4NextIeee8021QBridgeFdbId)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeFdbId);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeFdbDynamicCount
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                retValIeee8021QBridgeFdbDynamicCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeFdbDynamicCount (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                      UINT4 u4Ieee8021QBridgeFdbId,
                                      UINT4
                                      *pu4RetValIeee8021QBridgeFdbDynamicCount)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (pu4RetValIeee8021QBridgeFdbDynamicCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeFdbLearnedEntryDiscards
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                retValIeee8021QBridgeFdbLearnedEntryDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeFdbLearnedEntryDiscards (UINT4
                                              u4Ieee8021QBridgeFdbComponentId,
                                              UINT4 u4Ieee8021QBridgeFdbId,
                                              tSNMP_COUNTER64_TYPE *
                                              pu8RetValIeee8021QBridgeFdbLearnedEntryDiscards)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (pu8RetValIeee8021QBridgeFdbLearnedEntryDiscards);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeFdbAgingTime
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                retValIeee8021QBridgeFdbAgingTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeFdbAgingTime (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                   UINT4 u4Ieee8021QBridgeFdbId,
                                   INT4 *pi4RetValIeee8021QBridgeFdbAgingTime)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeFdbAgingTime);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeFdbAgingTime
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                setValIeee8021QBridgeFdbAgingTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeFdbAgingTime (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                   UINT4 u4Ieee8021QBridgeFdbId,
                                   INT4 i4SetValIeee8021QBridgeFdbAgingTime)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (i4SetValIeee8021QBridgeFdbAgingTime);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeFdbAgingTime
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId

                The Object 
                testValIeee8021QBridgeFdbAgingTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeFdbAgingTime (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021QBridgeFdbComponentId,
                                      UINT4 u4Ieee8021QBridgeFdbId,
                                      INT4 i4TestValIeee8021QBridgeFdbAgingTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (i4TestValIeee8021QBridgeFdbAgingTime);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeFdbTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeTpFdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeTpFdbTable (UINT4
                                                   u4Ieee8021QBridgeFdbComponentId,
                                                   UINT4 u4Ieee8021QBridgeFdbId,
                                                   tMacAddr
                                                   Ieee8021QBridgeTpFdbAddress)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (Ieee8021QBridgeTpFdbAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeTpFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeTpFdbTable (UINT4
                                           *pu4Ieee8021QBridgeFdbComponentId,
                                           UINT4 *pu4Ieee8021QBridgeFdbId,
                                           tMacAddr *
                                           pIeee8021QBridgeTpFdbAddress)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (pIeee8021QBridgeTpFdbAddress);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeTpFdbTable
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                nextIeee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                nextIeee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress
                nextIeee8021QBridgeTpFdbAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeTpFdbTable (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                          UINT4
                                          *pu4NextIeee8021QBridgeFdbComponentId,
                                          UINT4 u4Ieee8021QBridgeFdbId,
                                          UINT4 *pu4NextIeee8021QBridgeFdbId,
                                          tMacAddr Ieee8021QBridgeTpFdbAddress,
                                          tMacAddr *
                                          pNextIeee8021QBridgeTpFdbAddress)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeFdbId);
    UNUSED_PARAM (Ieee8021QBridgeTpFdbAddress);
    UNUSED_PARAM (pNextIeee8021QBridgeTpFdbAddress);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpFdbPort
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress

                The Object 
                retValIeee8021QBridgeTpFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpFdbPort (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                UINT4 u4Ieee8021QBridgeFdbId,
                                tMacAddr Ieee8021QBridgeTpFdbAddress,
                                UINT4 *pu4RetValIeee8021QBridgeTpFdbPort)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (Ieee8021QBridgeTpFdbAddress);
    UNUSED_PARAM (pu4RetValIeee8021QBridgeTpFdbPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpFdbStatus
 Input       :  The Indices
                Ieee8021QBridgeFdbComponentId
                Ieee8021QBridgeFdbId
                Ieee8021QBridgeTpFdbAddress

                The Object 
                retValIeee8021QBridgeTpFdbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpFdbStatus (UINT4 u4Ieee8021QBridgeFdbComponentId,
                                  UINT4 u4Ieee8021QBridgeFdbId,
                                  tMacAddr Ieee8021QBridgeTpFdbAddress,
                                  INT4 *pi4RetValIeee8021QBridgeTpFdbStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeFdbComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeFdbId);
    UNUSED_PARAM (Ieee8021QBridgeTpFdbAddress);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeTpFdbStatus);
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeTpGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeTpGroupTable (UINT4
                                                     u4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeVlanIndex,
                                                     tMacAddr
                                                     Ieee8021QBridgeTpGroupAddress)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeTpGroupAddress);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeTpGroupTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeTpGroupTable (UINT4
                                             *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                             UINT4 *pu4Ieee8021QBridgeVlanIndex,
                                             tMacAddr *
                                             pIeee8021QBridgeTpGroupAddress)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pIeee8021QBridgeTpGroupAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeTpGroupTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress
                nextIeee8021QBridgeTpGroupAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeTpGroupTable (UINT4
                                            u4Ieee8021QBridgeVlanCurrentComponentId,
                                            UINT4
                                            *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                            UINT4 u4Ieee8021QBridgeVlanIndex,
                                            UINT4
                                            *pu4NextIeee8021QBridgeVlanIndex,
                                            tMacAddr
                                            Ieee8021QBridgeTpGroupAddress,
                                            tMacAddr *
                                            pNextIeee8021QBridgeTpGroupAddress)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeTpGroupAddress);
    UNUSED_PARAM (pNextIeee8021QBridgeTpGroupAddress);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpGroupEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress

                The Object 
                retValIeee8021QBridgeTpGroupEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpGroupEgressPorts (UINT4
                                         u4Ieee8021QBridgeVlanCurrentComponentId,
                                         UINT4 u4Ieee8021QBridgeVlanIndex,
                                         tMacAddr Ieee8021QBridgeTpGroupAddress,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValIeee8021QBridgeTpGroupEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeTpGroupAddress);
    UNUSED_PARAM (pRetValIeee8021QBridgeTpGroupEgressPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpGroupLearnt
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeTpGroupAddress

                The Object 
                retValIeee8021QBridgeTpGroupLearnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpGroupLearnt (UINT4
                                    u4Ieee8021QBridgeVlanCurrentComponentId,
                                    UINT4 u4Ieee8021QBridgeVlanIndex,
                                    tMacAddr Ieee8021QBridgeTpGroupAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValIeee8021QBridgeTpGroupLearnt)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeTpGroupAddress);
    UNUSED_PARAM (pRetValIeee8021QBridgeTpGroupLearnt);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeForwardAllTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeForwardAllTable (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardAllVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardAllVlanIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeForwardAllTable (UINT4
                                                *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                UINT4
                                                *pu4Ieee8021QBridgeForwardAllVlanIndex)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeForwardAllVlanIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
                nextIeee8021QBridgeForwardAllVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeForwardAllTable (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               UINT4
                                               *pu4NextIeee8021QBridgeForwardAllVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardAllVlanIndex);
    UNUSED_PARAM (pu4NextIeee8021QBridgeForwardAllVlanIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardAllPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                retValIeee8021QBridgeForwardAllPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardAllPorts (UINT4
                                      u4Ieee8021QBridgeVlanCurrentComponentId,
                                      UINT4
                                      u4Ieee8021QBridgeForwardAllVlanIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValIeee8021QBridgeForwardAllPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardAllVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeForwardAllPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardAllStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                retValIeee8021QBridgeForwardAllStaticPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardAllStaticPorts (UINT4
                                            u4Ieee8021QBridgeVlanCurrentComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeForwardAllVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValIeee8021QBridgeForwardAllStaticPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardAllVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeForwardAllStaticPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardAllForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                retValIeee8021QBridgeForwardAllForbiddenPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardAllForbiddenPorts (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeForwardAllForbiddenPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardAllVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeForwardAllForbiddenPorts);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeForwardAllStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                setValIeee8021QBridgeForwardAllStaticPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeForwardAllStaticPorts (UINT4
                                            u4Ieee8021QBridgeVlanCurrentComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeForwardAllVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValIeee8021QBridgeForwardAllStaticPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardAllVlanIndex);
    UNUSED_PARAM (pSetValIeee8021QBridgeForwardAllStaticPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeForwardAllForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                setValIeee8021QBridgeForwardAllForbiddenPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeForwardAllForbiddenPorts (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pSetValIeee8021QBridgeForwardAllForbiddenPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardAllVlanIndex);
    UNUSED_PARAM (pSetValIeee8021QBridgeForwardAllForbiddenPorts);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardAllStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                testValIeee8021QBridgeForwardAllStaticPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardAllStaticPorts (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardAllVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValIeee8021QBridgeForwardAllStaticPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardAllVlanIndex);
    UNUSED_PARAM (pTestValIeee8021QBridgeForwardAllStaticPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardAllForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex

                The Object 
                testValIeee8021QBridgeForwardAllForbiddenPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardAllForbiddenPorts (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanCurrentComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeForwardAllVlanIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pTestValIeee8021QBridgeForwardAllForbiddenPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardAllVlanIndex);
    UNUSED_PARAM (pTestValIeee8021QBridgeForwardAllForbiddenPorts);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeForwardAllTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardAllVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeForwardAllTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeForwardUnregisteredTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeForwardUnregisteredTable (UINT4
                                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                                 UINT4
                                                                 u4Ieee8021QBridgeForwardUnregisteredVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeForwardUnregisteredTable (UINT4
                                                         *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                         UINT4
                                                         *pu4Ieee8021QBridgeForwardUnregisteredVlanIndex)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
                nextIeee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeForwardUnregisteredTable (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeForwardUnregisteredVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    UNUSED_PARAM (pu4NextIeee8021QBridgeForwardUnregisteredVlanIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardUnregisteredPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                retValIeee8021QBridgeForwardUnregisteredPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardUnregisteredPorts (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeForwardUnregisteredPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeForwardUnregisteredPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardUnregisteredStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                retValIeee8021QBridgeForwardUnregisteredStaticPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardUnregisteredStaticPorts (UINT4
                                                     u4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pRetValIeee8021QBridgeForwardUnregisteredStaticPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeForwardUnregisteredStaticPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                retValIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeForwardUnregisteredForbiddenPorts (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pRetValIeee8021QBridgeForwardUnregisteredForbiddenPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeForwardUnregisteredForbiddenPorts);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeForwardUnregisteredStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                setValIeee8021QBridgeForwardUnregisteredStaticPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeForwardUnregisteredStaticPorts (UINT4
                                                     u4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pSetValIeee8021QBridgeForwardUnregisteredStaticPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    UNUSED_PARAM (pSetValIeee8021QBridgeForwardUnregisteredStaticPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                setValIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeForwardUnregisteredForbiddenPorts (UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    UNUSED_PARAM (pSetValIeee8021QBridgeForwardUnregisteredForbiddenPorts);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardUnregisteredStaticPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                testValIeee8021QBridgeForwardUnregisteredStaticPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardUnregisteredStaticPorts (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021QBridgeVlanCurrentComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pTestValIeee8021QBridgeForwardUnregisteredStaticPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    UNUSED_PARAM (pTestValIeee8021QBridgeForwardUnregisteredStaticPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeForwardUnregisteredForbiddenPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex

                The Object 
                testValIeee8021QBridgeForwardUnregisteredForbiddenPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeForwardUnregisteredForbiddenPorts (UINT4 *pu4ErrorCode,
                                                           UINT4
                                                           u4Ieee8021QBridgeVlanCurrentComponentId,
                                                           UINT4
                                                           u4Ieee8021QBridgeForwardUnregisteredVlanIndex,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pTestValIeee8021QBridgeForwardUnregisteredForbiddenPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeForwardUnregisteredVlanIndex);
    UNUSED_PARAM (pTestValIeee8021QBridgeForwardUnregisteredForbiddenPorts);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeForwardUnregisteredTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeForwardUnregisteredVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeForwardUnregisteredTable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeStaticUnicastTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeStaticUnicastTable (UINT4
                                                           u4Ieee8021QBridgeStaticUnicastComponentId,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                           tMacAddr
                                                           Ieee8021QBridgeStaticUnicastAddress,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastReceivePort)
{
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeStaticUnicastTable (UINT4
                                                   *pu4Ieee8021QBridgeStaticUnicastComponentId,
                                                   UINT4
                                                   *pu4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                   tMacAddr *
                                                   pIeee8021QBridgeStaticUnicastAddress,
                                                   UINT4
                                                   *pu4Ieee8021QBridgeStaticUnicastReceivePort)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (pIeee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (pu4Ieee8021QBridgeStaticUnicastReceivePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                nextIeee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                nextIeee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                nextIeee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
                nextIeee8021QBridgeStaticUnicastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeStaticUnicastTable (UINT4
                                                  u4Ieee8021QBridgeStaticUnicastComponentId,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeStaticUnicastComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeStaticUnicastVlanIndex,
                                                  tMacAddr
                                                  Ieee8021QBridgeStaticUnicastAddress,
                                                  tMacAddr *
                                                  pNextIeee8021QBridgeStaticUnicastAddress,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeStaticUnicastReceivePort)
{
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (pu4NextIeee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (pNextIeee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (pu4NextIeee8021QBridgeStaticUnicastReceivePort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastStaticEgressPorts (UINT4
                                                     u4Ieee8021QBridgeStaticUnicastComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                     tMacAddr
                                                     Ieee8021QBridgeStaticUnicastAddress,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pRetValIeee8021QBridgeStaticUnicastStaticEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (pRetValIeee8021QBridgeStaticUnicastStaticEgressPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastForbiddenEgressPorts (UINT4
                                                        u4Ieee8021QBridgeStaticUnicastComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                        tMacAddr
                                                        Ieee8021QBridgeStaticUnicastAddress,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pRetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (pRetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastStorageType
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastStorageType (UINT4
                                               u4Ieee8021QBridgeStaticUnicastComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticUnicastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastReceivePort,
                                               INT4
                                               *pi4RetValIeee8021QBridgeStaticUnicastStorageType)
{
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeStaticUnicastStorageType);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticUnicastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                retValIeee8021QBridgeStaticUnicastRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticUnicastRowStatus (UINT4
                                             u4Ieee8021QBridgeStaticUnicastComponentId,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                             tMacAddr
                                             Ieee8021QBridgeStaticUnicastAddress,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastReceivePort,
                                             INT4
                                             *pi4RetValIeee8021QBridgeStaticUnicastRowStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeStaticUnicastRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticUnicastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastStaticEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticUnicastStaticEgressPorts (UINT4
                                                     u4Ieee8021QBridgeStaticUnicastComponentId,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                     tMacAddr
                                                     Ieee8021QBridgeStaticUnicastAddress,
                                                     UINT4
                                                     u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (pSetValIeee8021QBridgeStaticUnicastStaticEgressPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticUnicastForbiddenEgressPorts (UINT4
                                                        u4Ieee8021QBridgeStaticUnicastComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                        tMacAddr
                                                        Ieee8021QBridgeStaticUnicastAddress,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (pSetValIeee8021QBridgeStaticUnicastForbiddenEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticUnicastStorageType
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticUnicastStorageType (UINT4
                                               u4Ieee8021QBridgeStaticUnicastComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticUnicastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticUnicastReceivePort,
                                               INT4
                                               i4SetValIeee8021QBridgeStaticUnicastStorageType)
{
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (i4SetValIeee8021QBridgeStaticUnicastStorageType);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticUnicastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                setValIeee8021QBridgeStaticUnicastRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticUnicastRowStatus (UINT4
                                             u4Ieee8021QBridgeStaticUnicastComponentId,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                             tMacAddr
                                             Ieee8021QBridgeStaticUnicastAddress,
                                             UINT4
                                             u4Ieee8021QBridgeStaticUnicastReceivePort,
                                             INT4
                                             i4SetValIeee8021QBridgeStaticUnicastRowStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (i4SetValIeee8021QBridgeStaticUnicastRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastStaticEgressPorts (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                        tMacAddr
                                                        Ieee8021QBridgeStaticUnicastAddress,
                                                        UINT4
                                                        u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                        tSNMP_OCTET_STRING_TYPE
                                                        *
                                                        pTestValIeee8021QBridgeStaticUnicastStaticEgressPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (pTestValIeee8021QBridgeStaticUnicastStaticEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastForbiddenEgressPorts (UINT4 *pu4ErrorCode,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastComponentId,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                           tMacAddr
                                                           Ieee8021QBridgeStaticUnicastAddress,
                                                           UINT4
                                                           u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pTestValIeee8021QBridgeStaticUnicastForbiddenEgressPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (pTestValIeee8021QBridgeStaticUnicastForbiddenEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastStorageType
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastStorageType (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                  tMacAddr
                                                  Ieee8021QBridgeStaticUnicastAddress,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                  INT4
                                                  i4TestValIeee8021QBridgeStaticUnicastStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (i4TestValIeee8021QBridgeStaticUnicastStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticUnicastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort

                The Object 
                testValIeee8021QBridgeStaticUnicastRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticUnicastRowStatus (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021QBridgeStaticUnicastComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeStaticUnicastVlanIndex,
                                                tMacAddr
                                                Ieee8021QBridgeStaticUnicastAddress,
                                                UINT4
                                                u4Ieee8021QBridgeStaticUnicastReceivePort,
                                                INT4
                                                i4TestValIeee8021QBridgeStaticUnicastRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticUnicastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticUnicastReceivePort);
    UNUSED_PARAM (i4TestValIeee8021QBridgeStaticUnicastRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeStaticUnicastTable
 Input       :  The Indices
                Ieee8021QBridgeStaticUnicastComponentId
                Ieee8021QBridgeStaticUnicastVlanIndex
                Ieee8021QBridgeStaticUnicastAddress
                Ieee8021QBridgeStaticUnicastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeStaticUnicastTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeStaticMulticastTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeStaticMulticastTable (UINT4
                                                             u4Ieee8021QBridgeVlanCurrentComponentId,
                                                             UINT4
                                                             u4Ieee8021QBridgeVlanIndex,
                                                             tMacAddr
                                                             Ieee8021QBridgeStaticMulticastAddress,
                                                             UINT4
                                                             u4Ieee8021QBridgeStaticMulticastReceivePort)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeStaticMulticastTable (UINT4
                                                     *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                     UINT4
                                                     *pu4Ieee8021QBridgeVlanIndex,
                                                     tMacAddr *
                                                     pIeee8021QBridgeStaticMulticastAddress,
                                                     UINT4
                                                     *pu4Ieee8021QBridgeStaticMulticastReceivePort)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pIeee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (pu4Ieee8021QBridgeStaticMulticastReceivePort);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                nextIeee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
                nextIeee8021QBridgeStaticMulticastReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeStaticMulticastTable (UINT4
                                                    u4Ieee8021QBridgeVlanCurrentComponentId,
                                                    UINT4
                                                    *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                    UINT4
                                                    u4Ieee8021QBridgeVlanIndex,
                                                    UINT4
                                                    *pu4NextIeee8021QBridgeVlanIndex,
                                                    tMacAddr
                                                    Ieee8021QBridgeStaticMulticastAddress,
                                                    tMacAddr *
                                                    pNextIeee8021QBridgeStaticMulticastAddress,
                                                    UINT4
                                                    u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                    UINT4
                                                    *pu4NextIeee8021QBridgeStaticMulticastReceivePort)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (pNextIeee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (pu4NextIeee8021QBridgeStaticMulticastReceivePort);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastStaticEgressPorts (UINT4
                                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       tMacAddr
                                                       Ieee8021QBridgeStaticMulticastAddress,
                                                       UINT4
                                                       u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pRetValIeee8021QBridgeStaticMulticastStaticEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (pRetValIeee8021QBridgeStaticMulticastStaticEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastForbiddenEgressPorts (UINT4
                                                          u4Ieee8021QBridgeVlanCurrentComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanIndex,
                                                          tMacAddr
                                                          Ieee8021QBridgeStaticMulticastAddress,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pRetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (pRetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastStorageType
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastStorageType (UINT4
                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanIndex,
                                                 tMacAddr
                                                 Ieee8021QBridgeStaticMulticastAddress,
                                                 UINT4
                                                 u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                 INT4
                                                 *pi4RetValIeee8021QBridgeStaticMulticastStorageType)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeStaticMulticastStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeStaticMulticastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                retValIeee8021QBridgeStaticMulticastRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeStaticMulticastRowStatus (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4 u4Ieee8021QBridgeVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticMulticastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticMulticastReceivePort,
                                               INT4
                                               *pi4RetValIeee8021QBridgeStaticMulticastRowStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeStaticMulticastRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastStaticEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticMulticastStaticEgressPorts (UINT4
                                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                                       UINT4
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       tMacAddr
                                                       Ieee8021QBridgeStaticMulticastAddress,
                                                       UINT4
                                                       u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (pSetValIeee8021QBridgeStaticMulticastStaticEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticMulticastForbiddenEgressPorts (UINT4
                                                          u4Ieee8021QBridgeVlanCurrentComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanIndex,
                                                          tMacAddr
                                                          Ieee8021QBridgeStaticMulticastAddress,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (pSetValIeee8021QBridgeStaticMulticastForbiddenEgressPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticMulticastStorageType
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticMulticastStorageType (UINT4
                                                 u4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanIndex,
                                                 tMacAddr
                                                 Ieee8021QBridgeStaticMulticastAddress,
                                                 UINT4
                                                 u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                 INT4
                                                 i4SetValIeee8021QBridgeStaticMulticastStorageType)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (i4SetValIeee8021QBridgeStaticMulticastStorageType);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeStaticMulticastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                setValIeee8021QBridgeStaticMulticastRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeStaticMulticastRowStatus (UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4 u4Ieee8021QBridgeVlanIndex,
                                               tMacAddr
                                               Ieee8021QBridgeStaticMulticastAddress,
                                               UINT4
                                               u4Ieee8021QBridgeStaticMulticastReceivePort,
                                               INT4
                                               i4SetValIeee8021QBridgeStaticMulticastRowStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (i4SetValIeee8021QBridgeStaticMulticastRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastStaticEgressPorts (UINT4 *pu4ErrorCode,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanCurrentComponentId,
                                                          UINT4
                                                          u4Ieee8021QBridgeVlanIndex,
                                                          tMacAddr
                                                          Ieee8021QBridgeStaticMulticastAddress,
                                                          UINT4
                                                          u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pTestValIeee8021QBridgeStaticMulticastStaticEgressPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (pTestValIeee8021QBridgeStaticMulticastStaticEgressPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastForbiddenEgressPorts (UINT4
                                                             *pu4ErrorCode,
                                                             UINT4
                                                             u4Ieee8021QBridgeVlanCurrentComponentId,
                                                             UINT4
                                                             u4Ieee8021QBridgeVlanIndex,
                                                             tMacAddr
                                                             Ieee8021QBridgeStaticMulticastAddress,
                                                             UINT4
                                                             u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                             tSNMP_OCTET_STRING_TYPE
                                                             *
                                                             pTestValIeee8021QBridgeStaticMulticastForbiddenEgressPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (pTestValIeee8021QBridgeStaticMulticastForbiddenEgressPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastStorageType
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastStorageType (UINT4 *pu4ErrorCode,
                                                    UINT4
                                                    u4Ieee8021QBridgeVlanCurrentComponentId,
                                                    UINT4
                                                    u4Ieee8021QBridgeVlanIndex,
                                                    tMacAddr
                                                    Ieee8021QBridgeStaticMulticastAddress,
                                                    UINT4
                                                    u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                    INT4
                                                    i4TestValIeee8021QBridgeStaticMulticastStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (i4TestValIeee8021QBridgeStaticMulticastStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeStaticMulticastRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort

                The Object 
                testValIeee8021QBridgeStaticMulticastRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeStaticMulticastRowStatus (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanCurrentComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanIndex,
                                                  tMacAddr
                                                  Ieee8021QBridgeStaticMulticastAddress,
                                                  UINT4
                                                  u4Ieee8021QBridgeStaticMulticastReceivePort,
                                                  INT4
                                                  i4TestValIeee8021QBridgeStaticMulticastRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (Ieee8021QBridgeStaticMulticastAddress);
    UNUSED_PARAM (u4Ieee8021QBridgeStaticMulticastReceivePort);
    UNUSED_PARAM (i4TestValIeee8021QBridgeStaticMulticastRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeStaticMulticastTable
 Input       :  The Indices
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                Ieee8021QBridgeStaticMulticastAddress
                Ieee8021QBridgeStaticMulticastReceivePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeStaticMulticastTable (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanNumDeletes
 Input       :  The Indices

                The Object 
                retValIeee8021QBridgeVlanNumDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanNumDeletes (tSNMP_COUNTER64_TYPE *
                                     pu8RetValIeee8021QBridgeVlanNumDeletes)
{
    UNUSED_PARAM (pu8RetValIeee8021QBridgeVlanNumDeletes);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeVlanCurrentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeVlanCurrentTable (UINT4
                                                         u4Ieee8021QBridgeVlanTimeMark,
                                                         UINT4
                                                         u4Ieee8021QBridgeVlanCurrentComponentId,
                                                         UINT4
                                                         u4Ieee8021QBridgeVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanTimeMark);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeVlanCurrentTable
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeVlanCurrentTable (UINT4
                                                 *pu4Ieee8021QBridgeVlanTimeMark,
                                                 UINT4
                                                 *pu4Ieee8021QBridgeVlanCurrentComponentId,
                                                 UINT4
                                                 *pu4Ieee8021QBridgeVlanIndex)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanTimeMark);
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeVlanCurrentTable
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                nextIeee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                nextIeee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeVlanCurrentTable (UINT4
                                                u4Ieee8021QBridgeVlanTimeMark,
                                                UINT4
                                                *pu4NextIeee8021QBridgeVlanTimeMark,
                                                UINT4
                                                u4Ieee8021QBridgeVlanCurrentComponentId,
                                                UINT4
                                                *pu4NextIeee8021QBridgeVlanCurrentComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeVlanIndex,
                                                UINT4
                                                *pu4NextIeee8021QBridgeVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanTimeMark);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanTimeMark);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanIndex);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanFdbId
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanFdbId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanFdbId (UINT4 u4Ieee8021QBridgeVlanTimeMark,
                                UINT4 u4Ieee8021QBridgeVlanCurrentComponentId,
                                UINT4 u4Ieee8021QBridgeVlanIndex,
                                UINT4 *pu4RetValIeee8021QBridgeVlanFdbId)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanTimeMark);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pu4RetValIeee8021QBridgeVlanFdbId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanCurrentEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanCurrentEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanCurrentEgressPorts (UINT4
                                             u4Ieee8021QBridgeVlanTimeMark,
                                             UINT4
                                             u4Ieee8021QBridgeVlanCurrentComponentId,
                                             UINT4 u4Ieee8021QBridgeVlanIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pRetValIeee8021QBridgeVlanCurrentEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanTimeMark);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeVlanCurrentEgressPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanCurrentUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanCurrentUntaggedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanCurrentUntaggedPorts (UINT4
                                               u4Ieee8021QBridgeVlanTimeMark,
                                               UINT4
                                               u4Ieee8021QBridgeVlanCurrentComponentId,
                                               UINT4 u4Ieee8021QBridgeVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeVlanCurrentUntaggedPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanTimeMark);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeVlanCurrentUntaggedPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStatus (UINT4 u4Ieee8021QBridgeVlanTimeMark,
                                 UINT4 u4Ieee8021QBridgeVlanCurrentComponentId,
                                 UINT4 u4Ieee8021QBridgeVlanIndex,
                                 INT4 *pi4RetValIeee8021QBridgeVlanStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanTimeMark);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeVlanStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanCreationTime
 Input       :  The Indices
                Ieee8021QBridgeVlanTimeMark
                Ieee8021QBridgeVlanCurrentComponentId
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeVlanCreationTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanCreationTime (UINT4 u4Ieee8021QBridgeVlanTimeMark,
                                       UINT4
                                       u4Ieee8021QBridgeVlanCurrentComponentId,
                                       UINT4 u4Ieee8021QBridgeVlanIndex,
                                       UINT4
                                       *pu4RetValIeee8021QBridgeVlanCreationTime)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanTimeMark);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanCurrentComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pu4RetValIeee8021QBridgeVlanCreationTime);
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeVlanStaticTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeVlanStaticTable (UINT4
                                                        u4Ieee8021QBridgeVlanStaticComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeVlanStaticVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeVlanStaticTable (UINT4
                                                *pu4Ieee8021QBridgeVlanStaticComponentId,
                                                UINT4
                                                *pu4Ieee8021QBridgeVlanStaticVlanIndex)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanStaticVlanIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                nextIeee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
                nextIeee8021QBridgeVlanStaticVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeVlanStaticTable (UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               *pu4NextIeee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               UINT4
                                               *pu4NextIeee8021QBridgeVlanStaticVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanStaticVlanIndex);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticName
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticName (UINT4
                                     u4Ieee8021QBridgeVlanStaticComponentId,
                                     UINT4 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValIeee8021QBridgeVlanStaticName)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeVlanStaticName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticEgressPorts (UINT4
                                            u4Ieee8021QBridgeVlanStaticComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeVlanStaticVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValIeee8021QBridgeVlanStaticEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeVlanStaticEgressPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanForbiddenEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanForbiddenEgressPorts (UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pRetValIeee8021QBridgeVlanForbiddenEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeVlanForbiddenEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticUntaggedPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticUntaggedPorts (UINT4
                                              u4Ieee8021QBridgeVlanStaticComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeVlanStaticVlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pRetValIeee8021QBridgeVlanStaticUntaggedPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pRetValIeee8021QBridgeVlanStaticUntaggedPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeVlanStaticRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                retValIeee8021QBridgeVlanStaticRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeVlanStaticRowStatus (UINT4
                                          u4Ieee8021QBridgeVlanStaticComponentId,
                                          UINT4
                                          u4Ieee8021QBridgeVlanStaticVlanIndex,
                                          INT4
                                          *pi4RetValIeee8021QBridgeVlanStaticRowStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeVlanStaticRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeVlanStaticName
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeVlanStaticName (UINT4
                                     u4Ieee8021QBridgeVlanStaticComponentId,
                                     UINT4 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValIeee8021QBridgeVlanStaticName)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pSetValIeee8021QBridgeVlanStaticName);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeVlanStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeVlanStaticEgressPorts (UINT4
                                            u4Ieee8021QBridgeVlanStaticComponentId,
                                            UINT4
                                            u4Ieee8021QBridgeVlanStaticVlanIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValIeee8021QBridgeVlanStaticEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pSetValIeee8021QBridgeVlanStaticEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeVlanForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanForbiddenEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeVlanForbiddenEgressPorts (UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pSetValIeee8021QBridgeVlanForbiddenEgressPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pSetValIeee8021QBridgeVlanForbiddenEgressPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeVlanStaticUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticUntaggedPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeVlanStaticUntaggedPorts (UINT4
                                              u4Ieee8021QBridgeVlanStaticComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeVlanStaticVlanIndex,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pSetValIeee8021QBridgeVlanStaticUntaggedPorts)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pSetValIeee8021QBridgeVlanStaticUntaggedPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeVlanStaticRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                setValIeee8021QBridgeVlanStaticRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeVlanStaticRowStatus (UINT4
                                          u4Ieee8021QBridgeVlanStaticComponentId,
                                          UINT4
                                          u4Ieee8021QBridgeVlanStaticVlanIndex,
                                          INT4
                                          i4SetValIeee8021QBridgeVlanStaticRowStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (i4SetValIeee8021QBridgeVlanStaticRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticName
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticName (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4Ieee8021QBridgeVlanStaticComponentId,
                                        UINT4
                                        u4Ieee8021QBridgeVlanStaticVlanIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValIeee8021QBridgeVlanStaticName)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pTestValIeee8021QBridgeVlanStaticName);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticEgressPorts (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticComponentId,
                                               UINT4
                                               u4Ieee8021QBridgeVlanStaticVlanIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValIeee8021QBridgeVlanStaticEgressPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pTestValIeee8021QBridgeVlanStaticEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanForbiddenEgressPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanForbiddenEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanForbiddenEgressPorts (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanStaticComponentId,
                                                  UINT4
                                                  u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pTestValIeee8021QBridgeVlanForbiddenEgressPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pTestValIeee8021QBridgeVlanForbiddenEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticUntaggedPorts
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticUntaggedPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticUntaggedPorts (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanStaticComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeVlanStaticVlanIndex,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pTestValIeee8021QBridgeVlanStaticUntaggedPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (pTestValIeee8021QBridgeVlanStaticUntaggedPorts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeVlanStaticRowStatus
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex

                The Object 
                testValIeee8021QBridgeVlanStaticRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeVlanStaticRowStatus (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4Ieee8021QBridgeVlanStaticComponentId,
                                             UINT4
                                             u4Ieee8021QBridgeVlanStaticVlanIndex,
                                             INT4
                                             i4TestValIeee8021QBridgeVlanStaticRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanStaticVlanIndex);
    UNUSED_PARAM (i4TestValIeee8021QBridgeVlanStaticRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeVlanStaticTable
 Input       :  The Indices
                Ieee8021QBridgeVlanStaticComponentId
                Ieee8021QBridgeVlanStaticVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeVlanStaticTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeNextFreeLocalVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeNextFreeLocalVlanTable
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeNextFreeLocalVlanTable (UINT4
                                                               u4Ieee8021QBridgeNextFreeLocalVlanComponentId)
{
    UNUSED_PARAM (u4Ieee8021QBridgeNextFreeLocalVlanComponentId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeNextFreeLocalVlanTable
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeNextFreeLocalVlanTable (UINT4
                                                       *pu4Ieee8021QBridgeNextFreeLocalVlanComponentId)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeNextFreeLocalVlanComponentId);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeNextFreeLocalVlanTable
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId
                nextIeee8021QBridgeNextFreeLocalVlanComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeNextFreeLocalVlanTable (UINT4
                                                      u4Ieee8021QBridgeNextFreeLocalVlanComponentId,
                                                      UINT4
                                                      *pu4NextIeee8021QBridgeNextFreeLocalVlanComponentId)
{
    UNUSED_PARAM (u4Ieee8021QBridgeNextFreeLocalVlanComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeNextFreeLocalVlanComponentId);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeNextFreeLocalVlanIndex
 Input       :  The Indices
                Ieee8021QBridgeNextFreeLocalVlanComponentId

                The Object 
                retValIeee8021QBridgeNextFreeLocalVlanIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeNextFreeLocalVlanIndex (UINT4
                                             u4Ieee8021QBridgeNextFreeLocalVlanComponentId,
                                             UINT4
                                             *pu4RetValIeee8021QBridgeNextFreeLocalVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021QBridgeNextFreeLocalVlanComponentId);
    UNUSED_PARAM (pu4RetValIeee8021QBridgeNextFreeLocalVlanIndex);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgePortVlanTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgePortVlanTable (UINT4
                                                      u4Ieee8021BridgeBasePortComponentId,
                                                      UINT4
                                                      u4Ieee8021BridgeBasePort)
{
    return (nmhValidateIndexInstanceFsMrpPortTable
            (u4Ieee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgePortVlanTable (UINT4
                                              *pu4Ieee8021BridgeBasePortComponentId,
                                              UINT4 *pu4Ieee8021BridgeBasePort)
{
    return (nmhGetFirstIndexFsMrpPortTable
            (pu4Ieee8021BridgeBasePortComponentId, pu4Ieee8021BridgeBasePort));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgePortVlanTable (UINT4
                                             u4Ieee8021BridgeBasePortComponentId,
                                             UINT4
                                             *pu4NextIeee8021BridgeBasePortComponentId,
                                             UINT4 u4Ieee8021BridgeBasePort,
                                             UINT4
                                             *pu4NextIeee8021BridgeBasePort)
{
    return (nmhGetNextIndexFsMrpPortTable
            (u4Ieee8021BridgeBasePortComponentId,
             pu4NextIeee8021BridgeBasePortComponentId, u4Ieee8021BridgeBasePort,
             pu4NextIeee8021BridgeBasePort));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePvid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePvid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePvid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                           UINT4 u4Ieee8021BridgeBasePort,
                           UINT4 *pu4RetValIeee8021QBridgePvid)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4RetValIeee8021QBridgePvid);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortAcceptableFrameTypes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortAcceptableFrameTypes (UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               *pi4RetValIeee8021QBridgePortAcceptableFrameTypes)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021QBridgePortAcceptableFrameTypes);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortIngressFiltering
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortIngressFiltering (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           *pi4RetValIeee8021QBridgePortIngressFiltering)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4RetValIeee8021QBridgePortIngressFiltering);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortMvrpEnabledStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortMvrpEnabledStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            *pi4RetValIeee8021QBridgePortMvrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021QBridgePortMvrpEnabledStatus =
        pMrpPortEntry->u1PortMvrpStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortMvrpFailedRegistrations
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortMvrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortMvrpFailedRegistrations (UINT4
                                                  u4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePort,
                                                  tSNMP_COUNTER64_TYPE *
                                                  pu8RetValIeee8021QBridgePortMvrpFailedRegistrations)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pu8RetValIeee8021QBridgePortMvrpFailedRegistrations->lsn =
        pMrpPortEntry->u4MvrpRegFailCnt;
    pu8RetValIeee8021QBridgePortMvrpFailedRegistrations->msn = 0;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortMvrpLastPduOrigin
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortMvrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortMvrpLastPduOrigin (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            tMacAddr *
                                            pRetValIeee8021QBridgePortMvrpLastPduOrigin)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);
    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValIeee8021QBridgePortMvrpLastPduOrigin,
            pMrpPortEntry->LastMvrpPduOrigin, MRP_MAC_ADDR_LEN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgePortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                retValIeee8021QBridgePortRestrictedVlanRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgePortRestrictedVlanRegistration (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     INT4
                                                     *pi4RetValIeee8021QBridgePortRestrictedVlanRegistration)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIeee8021QBridgePortRestrictedVlanRegistration =
        pMrpPortEntry->u1RestrictedVlanRegCtrl;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgePvid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePvid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgePvid (UINT4 u4Ieee8021BridgeBasePortComponentId,
                           UINT4 u4Ieee8021BridgeBasePort,
                           UINT4 u4SetValIeee8021QBridgePvid)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4SetValIeee8021QBridgePvid);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortAcceptableFrameTypes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgePortAcceptableFrameTypes (UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4SetValIeee8021QBridgePortAcceptableFrameTypes)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021QBridgePortAcceptableFrameTypes);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgePortIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortIngressFiltering
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgePortIngressFiltering (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4SetValIeee8021QBridgePortIngressFiltering)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4SetValIeee8021QBridgePortIngressFiltering);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgePortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortMvrpEnabledStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgePortMvrpEnabledStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4SetValIeee8021QBridgePortMvrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pMrpPortEntry->u1PortMvrpStatus ==
        i4SetValIeee8021QBridgePortMvrpEnabledStatus)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValIeee8021QBridgePortMvrpEnabledStatus)
    {
        case MRP_ENABLED:

            if (MrpMvrpEnablePort (pMrpContextInfo,
                                   (UINT2) u4Ieee8021BridgeBasePort) ==
                OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }

            break;

        case MRP_DISABLED:

            if (MrpMvrpDisablePort (pMrpContextInfo,
                                    (UINT2) u4Ieee8021BridgeBasePort) ==
                OSIX_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
        default:

            break;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgePortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                setValIeee8021QBridgePortRestrictedVlanRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgePortRestrictedVlanRegistration (UINT4
                                                     u4Ieee8021BridgeBasePortComponentId,
                                                     UINT4
                                                     u4Ieee8021BridgeBasePort,
                                                     INT4
                                                     i4SetValIeee8021QBridgePortRestrictedVlanRegistration)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                        u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pMrpPortEntry->u1RestrictedVlanRegCtrl =
        (UINT1) i4SetValIeee8021QBridgePortRestrictedVlanRegistration;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePvid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePvid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePvid (UINT4 *pu4ErrorCode,
                              UINT4 u4Ieee8021BridgeBasePortComponentId,
                              UINT4 u4Ieee8021BridgeBasePort,
                              UINT4 u4TestValIeee8021QBridgePvid)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4TestValIeee8021QBridgePvid);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortAcceptableFrameTypes
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortAcceptableFrameTypes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortAcceptableFrameTypes (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  u4Ieee8021BridgeBasePort,
                                                  INT4
                                                  i4TestValIeee8021QBridgePortAcceptableFrameTypes)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021QBridgePortAcceptableFrameTypes);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortIngressFiltering
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortIngressFiltering
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortIngressFiltering (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4TestValIeee8021QBridgePortIngressFiltering)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4TestValIeee8021QBridgePortIngressFiltering);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortMvrpEnabledStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortMvrpEnabledStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortMvrpEnabledStatus (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4TestValIeee8021QBridgePortMvrpEnabledStatus)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1PortType = 0;
    UINT1               u1TunnelStatus = 0;
    UINT1               u1Status = 0;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry =
        MRP_GET_PORT_ENTRY (pMrpContextInfo, u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021QBridgePortMvrpEnabledStatus != MRP_ENABLED) &&
        (i4TestValIeee8021QBridgePortMvrpEnabledStatus != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    if (i4TestValIeee8021QBridgePortMvrpEnabledStatus == MRP_ENABLED)
    {
        if ((MrpPortL2IwfGetVlanPortType
             (pMrpPortEntry->u4IfIndex, &u1PortType)) == L2IWF_SUCCESS)
        {
            if (u1PortType == VLAN_ACCESS_PORT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_MRP_ACCESS_PORT_ERR);
                return SNMP_FAILURE;
            }
        }

        if (pMrpContextInfo->u4BridgeMode == MRP_CUSTOMER_BRIDGE_MODE)
        {
            MrpPortGetProtoTunelStatusOnPort (pMrpPortEntry->u4IfIndex,
                                              L2_PROTO_MVRP, &u1TunnelStatus);

            if ((u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
                (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_MVRP_TUNNEL_ENABLED_ERR);
                return SNMP_FAILURE;
            }
        }

        if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
            (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MRP_PORT_MVRP_ENABLED_ERR);
            return SNMP_FAILURE;
        }
    }

    if (SISP_IS_LOGICAL_PORT (pMrpPortEntry->u4IfIndex) == VCM_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MVRP_SISP_CONFIG_ERR);
        return SNMP_FAILURE;
    }
    else
    {
        L2IwfGetSispPortCtrlStatus (pMrpPortEntry->u4IfIndex, &u1Status);
        if (u1Status == L2IWF_ENABLED)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_MVRP_SISP_CONFIG_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgePortRestrictedVlanRegistration
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort

                The Object 
                testValIeee8021QBridgePortRestrictedVlanRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgePortRestrictedVlanRegistration (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        u4Ieee8021BridgeBasePort,
                                                        INT4
                                                        i4TestValIeee8021QBridgePortRestrictedVlanRegistration)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_VC_VALID (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (u4Ieee8021BridgeBasePortComponentId);

    if (pMrpContextInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_MRP_CONTEXT);
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (u4Ieee8021BridgeBasePortComponentId) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_MRP_DISABLED_ERR);
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) u4Ieee8021BridgeBasePort) == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                        (UINT2) u4Ieee8021BridgeBasePort);

    if (pMrpPortEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_MRP_INVALID_PORT_STATUS_ERR);
        return SNMP_FAILURE;
    }

    if ((i4TestValIeee8021QBridgePortRestrictedVlanRegistration != MRP_ENABLED)
        && (i4TestValIeee8021QBridgePortRestrictedVlanRegistration
            != MRP_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_MRP_INVALID_VAL);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgePortVlanTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgePortVlanTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgePortVlanStatisticsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgePortVlanStatisticsTable (UINT4
                                                                u4Ieee8021BridgeBasePortComponentId,
                                                                UINT4
                                                                u4Ieee8021BridgeBasePort,
                                                                UINT4
                                                                u4Ieee8021QBridgeVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgePortVlanStatisticsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgePortVlanStatisticsTable (UINT4
                                                        *pu4Ieee8021BridgeBasePortComponentId,
                                                        UINT4
                                                        *pu4Ieee8021BridgeBasePort,
                                                        UINT4
                                                        *pu4Ieee8021QBridgeVlanIndex)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4Ieee8021QBridgeVlanIndex);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgePortVlanStatisticsTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex
                nextIeee8021QBridgeVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgePortVlanStatisticsTable (UINT4
                                                       u4Ieee8021BridgeBasePortComponentId,
                                                       UINT4
                                                       *pu4NextIeee8021BridgeBasePortComponentId,
                                                       UINT4
                                                       u4Ieee8021BridgeBasePort,
                                                       UINT4
                                                       *pu4NextIeee8021BridgeBasePort,
                                                       UINT4
                                                       u4Ieee8021QBridgeVlanIndex,
                                                       UINT4
                                                       *pu4NextIeee8021QBridgeVlanIndex)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pu4NextIeee8021QBridgeVlanIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpVlanPortInFrames
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeTpVlanPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpVlanPortInFrames (UINT4
                                         u4Ieee8021BridgeBasePortComponentId,
                                         UINT4 u4Ieee8021BridgeBasePort,
                                         UINT4 u4Ieee8021QBridgeVlanIndex,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValIeee8021QBridgeTpVlanPortInFrames)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pu8RetValIeee8021QBridgeTpVlanPortInFrames);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpVlanPortOutFrames
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeTpVlanPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpVlanPortOutFrames (UINT4
                                          u4Ieee8021BridgeBasePortComponentId,
                                          UINT4 u4Ieee8021BridgeBasePort,
                                          UINT4 u4Ieee8021QBridgeVlanIndex,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValIeee8021QBridgeTpVlanPortOutFrames)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pu8RetValIeee8021QBridgeTpVlanPortOutFrames);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeTpVlanPortInDiscards
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeVlanIndex

                The Object 
                retValIeee8021QBridgeTpVlanPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeTpVlanPortInDiscards (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           UINT4 u4Ieee8021QBridgeVlanIndex,
                                           tSNMP_COUNTER64_TYPE *
                                           pu8RetValIeee8021QBridgeTpVlanPortInDiscards)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (u4Ieee8021QBridgeVlanIndex);
    UNUSED_PARAM (pu8RetValIeee8021QBridgeTpVlanPortInDiscards);
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeLearningConstraintsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintsTable (UINT4
                                                                 u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                                 UINT4
                                                                 u4Ieee8021QBridgeLearningConstraintsVlan,
                                                                 INT4
                                                                 i4Ieee8021QBridgeLearningConstraintsSet)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsVlan);
    UNUSED_PARAM (i4Ieee8021QBridgeLearningConstraintsSet);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeLearningConstraintsTable (UINT4
                                                         *pu4Ieee8021QBridgeLearningConstraintsComponentId,
                                                         UINT4
                                                         *pu4Ieee8021QBridgeLearningConstraintsVlan,
                                                         INT4
                                                         *pi4Ieee8021QBridgeLearningConstraintsSet)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeLearningConstraintsComponentId);
    UNUSED_PARAM (pu4Ieee8021QBridgeLearningConstraintsVlan);
    UNUSED_PARAM (pi4Ieee8021QBridgeLearningConstraintsSet);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                nextIeee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                nextIeee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
                nextIeee8021QBridgeLearningConstraintsSet
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeLearningConstraintsTable (UINT4
                                                        u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeLearningConstraintsComponentId,
                                                        UINT4
                                                        u4Ieee8021QBridgeLearningConstraintsVlan,
                                                        UINT4
                                                        *pu4NextIeee8021QBridgeLearningConstraintsVlan,
                                                        INT4
                                                        i4Ieee8021QBridgeLearningConstraintsSet,
                                                        INT4
                                                        *pi4NextIeee8021QBridgeLearningConstraintsSet)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeLearningConstraintsComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsVlan);
    UNUSED_PARAM (pu4NextIeee8021QBridgeLearningConstraintsVlan);
    UNUSED_PARAM (i4Ieee8021QBridgeLearningConstraintsSet);
    UNUSED_PARAM (pi4NextIeee8021QBridgeLearningConstraintsSet);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                retValIeee8021QBridgeLearningConstraintsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintsType (UINT4
                                              u4Ieee8021QBridgeLearningConstraintsComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeLearningConstraintsVlan,
                                              INT4
                                              i4Ieee8021QBridgeLearningConstraintsSet,
                                              INT4
                                              *pi4RetValIeee8021QBridgeLearningConstraintsType)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsVlan);
    UNUSED_PARAM (i4Ieee8021QBridgeLearningConstraintsSet);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeLearningConstraintsType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintsStatus
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                retValIeee8021QBridgeLearningConstraintsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintsStatus (UINT4
                                                u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeLearningConstraintsVlan,
                                                INT4
                                                i4Ieee8021QBridgeLearningConstraintsSet,
                                                INT4
                                                *pi4RetValIeee8021QBridgeLearningConstraintsStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsVlan);
    UNUSED_PARAM (i4Ieee8021QBridgeLearningConstraintsSet);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeLearningConstraintsStatus);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeLearningConstraintsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                setValIeee8021QBridgeLearningConstraintsType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeLearningConstraintsType (UINT4
                                              u4Ieee8021QBridgeLearningConstraintsComponentId,
                                              UINT4
                                              u4Ieee8021QBridgeLearningConstraintsVlan,
                                              INT4
                                              i4Ieee8021QBridgeLearningConstraintsSet,
                                              INT4
                                              i4SetValIeee8021QBridgeLearningConstraintsType)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsVlan);
    UNUSED_PARAM (i4Ieee8021QBridgeLearningConstraintsSet);
    UNUSED_PARAM (i4SetValIeee8021QBridgeLearningConstraintsType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeLearningConstraintsStatus
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                setValIeee8021QBridgeLearningConstraintsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeLearningConstraintsStatus (UINT4
                                                u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                UINT4
                                                u4Ieee8021QBridgeLearningConstraintsVlan,
                                                INT4
                                                i4Ieee8021QBridgeLearningConstraintsSet,
                                                INT4
                                                i4SetValIeee8021QBridgeLearningConstraintsStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsVlan);
    UNUSED_PARAM (i4Ieee8021QBridgeLearningConstraintsSet);
    UNUSED_PARAM (i4SetValIeee8021QBridgeLearningConstraintsStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                testValIeee8021QBridgeLearningConstraintsType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintsType (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                 UINT4
                                                 u4Ieee8021QBridgeLearningConstraintsVlan,
                                                 INT4
                                                 i4Ieee8021QBridgeLearningConstraintsSet,
                                                 INT4
                                                 i4TestValIeee8021QBridgeLearningConstraintsType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsVlan);
    UNUSED_PARAM (i4Ieee8021QBridgeLearningConstraintsSet);
    UNUSED_PARAM (i4TestValIeee8021QBridgeLearningConstraintsType);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintsStatus
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet

                The Object 
                testValIeee8021QBridgeLearningConstraintsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintsStatus (UINT4 *pu4ErrorCode,
                                                   UINT4
                                                   u4Ieee8021QBridgeLearningConstraintsComponentId,
                                                   UINT4
                                                   u4Ieee8021QBridgeLearningConstraintsVlan,
                                                   INT4
                                                   i4Ieee8021QBridgeLearningConstraintsSet,
                                                   INT4
                                                   i4TestValIeee8021QBridgeLearningConstraintsStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsComponentId);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintsVlan);
    UNUSED_PARAM (i4Ieee8021QBridgeLearningConstraintsSet);
    UNUSED_PARAM (i4TestValIeee8021QBridgeLearningConstraintsStatus);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeLearningConstraintsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintsComponentId
                Ieee8021QBridgeLearningConstraintsVlan
                Ieee8021QBridgeLearningConstraintsSet
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeLearningConstraintsTable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeLearningConstraintDefaultsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeLearningConstraintDefaultsTable (UINT4
                                                                        u4Ieee8021QBridgeLearningConstraintDefaultsComponentId)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeLearningConstraintDefaultsTable (UINT4
                                                                *pu4Ieee8021QBridgeLearningConstraintDefaultsComponentId)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
                nextIeee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeLearningConstraintDefaultsTable (UINT4
                                                               u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                               UINT4
                                                               *pu4NextIeee8021QBridgeLearningConstraintDefaultsComponentId)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeLearningConstraintDefaultsComponentId);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintDefaultsSet
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                retValIeee8021QBridgeLearningConstraintDefaultsSet
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintDefaultsSet (UINT4
                                                    u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                    INT4
                                                    *pi4RetValIeee8021QBridgeLearningConstraintDefaultsSet)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeLearningConstraintDefaultsSet);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeLearningConstraintDefaultsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                retValIeee8021QBridgeLearningConstraintDefaultsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeLearningConstraintDefaultsType (UINT4
                                                     u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                     INT4
                                                     *pi4RetValIeee8021QBridgeLearningConstraintDefaultsType)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeLearningConstraintDefaultsType);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeLearningConstraintDefaultsSet
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                setValIeee8021QBridgeLearningConstraintDefaultsSet
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeLearningConstraintDefaultsSet (UINT4
                                                    u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                    INT4
                                                    i4SetValIeee8021QBridgeLearningConstraintDefaultsSet)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    UNUSED_PARAM (i4SetValIeee8021QBridgeLearningConstraintDefaultsSet);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeLearningConstraintDefaultsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                setValIeee8021QBridgeLearningConstraintDefaultsType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeLearningConstraintDefaultsType (UINT4
                                                     u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                     INT4
                                                     i4SetValIeee8021QBridgeLearningConstraintDefaultsType)
{
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    UNUSED_PARAM (i4SetValIeee8021QBridgeLearningConstraintDefaultsType);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsSet
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                testValIeee8021QBridgeLearningConstraintDefaultsSet
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsSet (UINT4 *pu4ErrorCode,
                                                       UINT4
                                                       u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                       INT4
                                                       i4TestValIeee8021QBridgeLearningConstraintDefaultsSet)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    UNUSED_PARAM (i4TestValIeee8021QBridgeLearningConstraintDefaultsSet);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsType
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId

                The Object 
                testValIeee8021QBridgeLearningConstraintDefaultsType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeLearningConstraintDefaultsType (UINT4 *pu4ErrorCode,
                                                        UINT4
                                                        u4Ieee8021QBridgeLearningConstraintDefaultsComponentId,
                                                        INT4
                                                        i4TestValIeee8021QBridgeLearningConstraintDefaultsType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeLearningConstraintDefaultsComponentId);
    UNUSED_PARAM (i4TestValIeee8021QBridgeLearningConstraintDefaultsType);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeLearningConstraintDefaultsTable
 Input       :  The Indices
                Ieee8021QBridgeLearningConstraintDefaultsComponentId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeLearningConstraintDefaultsTable (UINT4 *pu4ErrorCode,
                                                        tSnmpIndexList *
                                                        pSnmpIndexList,
                                                        tSNMP_VAR_BIND *
                                                        pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeProtocolGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeProtocolGroupTable (UINT4
                                                           u4Ieee8021QBridgeProtocolGroupComponentId,
                                                           INT4
                                                           i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                           tSNMP_OCTET_STRING_TYPE
                                                           *
                                                           pIeee8021QBridgeProtocolTemplateProtocolValue)
{
    UNUSED_PARAM (u4Ieee8021QBridgeProtocolGroupComponentId);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolTemplateFrameType);
    UNUSED_PARAM (pIeee8021QBridgeProtocolTemplateProtocolValue);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeProtocolGroupTable (UINT4
                                                   *pu4Ieee8021QBridgeProtocolGroupComponentId,
                                                   INT4
                                                   *pi4Ieee8021QBridgeProtocolTemplateFrameType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pIeee8021QBridgeProtocolTemplateProtocolValue)
{
    UNUSED_PARAM (pu4Ieee8021QBridgeProtocolGroupComponentId);
    UNUSED_PARAM (pi4Ieee8021QBridgeProtocolTemplateFrameType);
    UNUSED_PARAM (pIeee8021QBridgeProtocolTemplateProtocolValue);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                nextIeee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                nextIeee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
                nextIeee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeProtocolGroupTable (UINT4
                                                  u4Ieee8021QBridgeProtocolGroupComponentId,
                                                  UINT4
                                                  *pu4NextIeee8021QBridgeProtocolGroupComponentId,
                                                  INT4
                                                  i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                  INT4
                                                  *pi4NextIeee8021QBridgeProtocolTemplateFrameType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pNextIeee8021QBridgeProtocolTemplateProtocolValue)
{
    UNUSED_PARAM (u4Ieee8021QBridgeProtocolGroupComponentId);
    UNUSED_PARAM (pu4NextIeee8021QBridgeProtocolGroupComponentId);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolTemplateFrameType);
    UNUSED_PARAM (pi4NextIeee8021QBridgeProtocolTemplateFrameType);
    UNUSED_PARAM (pIeee8021QBridgeProtocolTemplateProtocolValue);
    UNUSED_PARAM (pNextIeee8021QBridgeProtocolTemplateProtocolValue);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolGroupId
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                retValIeee8021QBridgeProtocolGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolGroupId (UINT4
                                      u4Ieee8021QBridgeProtocolGroupComponentId,
                                      INT4
                                      i4Ieee8021QBridgeProtocolTemplateFrameType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pIeee8021QBridgeProtocolTemplateProtocolValue,
                                      INT4
                                      *pi4RetValIeee8021QBridgeProtocolGroupId)
{
    UNUSED_PARAM (u4Ieee8021QBridgeProtocolGroupComponentId);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolTemplateFrameType);
    UNUSED_PARAM (pIeee8021QBridgeProtocolTemplateProtocolValue);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeProtocolGroupId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolGroupRowStatus
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                retValIeee8021QBridgeProtocolGroupRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolGroupRowStatus (UINT4
                                             u4Ieee8021QBridgeProtocolGroupComponentId,
                                             INT4
                                             i4Ieee8021QBridgeProtocolTemplateFrameType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIeee8021QBridgeProtocolTemplateProtocolValue,
                                             INT4
                                             *pi4RetValIeee8021QBridgeProtocolGroupRowStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeProtocolGroupComponentId);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolTemplateFrameType);
    UNUSED_PARAM (pIeee8021QBridgeProtocolTemplateProtocolValue);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeProtocolGroupRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeProtocolGroupId
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                setValIeee8021QBridgeProtocolGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeProtocolGroupId (UINT4
                                      u4Ieee8021QBridgeProtocolGroupComponentId,
                                      INT4
                                      i4Ieee8021QBridgeProtocolTemplateFrameType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pIeee8021QBridgeProtocolTemplateProtocolValue,
                                      INT4
                                      i4SetValIeee8021QBridgeProtocolGroupId)
{
    UNUSED_PARAM (u4Ieee8021QBridgeProtocolGroupComponentId);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolTemplateFrameType);
    UNUSED_PARAM (pIeee8021QBridgeProtocolTemplateProtocolValue);
    UNUSED_PARAM (i4SetValIeee8021QBridgeProtocolGroupId);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeProtocolGroupRowStatus
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                setValIeee8021QBridgeProtocolGroupRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeProtocolGroupRowStatus (UINT4
                                             u4Ieee8021QBridgeProtocolGroupComponentId,
                                             INT4
                                             i4Ieee8021QBridgeProtocolTemplateFrameType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pIeee8021QBridgeProtocolTemplateProtocolValue,
                                             INT4
                                             i4SetValIeee8021QBridgeProtocolGroupRowStatus)
{
    UNUSED_PARAM (u4Ieee8021QBridgeProtocolGroupComponentId);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolTemplateFrameType);
    UNUSED_PARAM (pIeee8021QBridgeProtocolTemplateProtocolValue);
    UNUSED_PARAM (i4SetValIeee8021QBridgeProtocolGroupRowStatus);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolGroupId
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                testValIeee8021QBridgeProtocolGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolGroupId (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4Ieee8021QBridgeProtocolGroupComponentId,
                                         INT4
                                         i4Ieee8021QBridgeProtocolTemplateFrameType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pIeee8021QBridgeProtocolTemplateProtocolValue,
                                         INT4
                                         i4TestValIeee8021QBridgeProtocolGroupId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeProtocolGroupComponentId);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolTemplateFrameType);
    UNUSED_PARAM (pIeee8021QBridgeProtocolTemplateProtocolValue);
    UNUSED_PARAM (i4TestValIeee8021QBridgeProtocolGroupId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolGroupRowStatus
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue

                The Object 
                testValIeee8021QBridgeProtocolGroupRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolGroupRowStatus (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4Ieee8021QBridgeProtocolGroupComponentId,
                                                INT4
                                                i4Ieee8021QBridgeProtocolTemplateFrameType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pIeee8021QBridgeProtocolTemplateProtocolValue,
                                                INT4
                                                i4TestValIeee8021QBridgeProtocolGroupRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021QBridgeProtocolGroupComponentId);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolTemplateFrameType);
    UNUSED_PARAM (pIeee8021QBridgeProtocolTemplateProtocolValue);
    UNUSED_PARAM (i4TestValIeee8021QBridgeProtocolGroupRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeProtocolGroupTable
 Input       :  The Indices
                Ieee8021QBridgeProtocolGroupComponentId
                Ieee8021QBridgeProtocolTemplateFrameType
                Ieee8021QBridgeProtocolTemplateProtocolValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeProtocolGroupTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021QBridgeProtocolPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021QBridgeProtocolPortTable (UINT4
                                                          u4Ieee8021BridgeBasePortComponentId,
                                                          UINT4
                                                          u4Ieee8021BridgeBasePort,
                                                          INT4
                                                          i4Ieee8021QBridgeProtocolPortGroupId)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolPortGroupId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021QBridgeProtocolPortTable (UINT4
                                                  *pu4Ieee8021BridgeBasePortComponentId,
                                                  UINT4
                                                  *pu4Ieee8021BridgeBasePort,
                                                  INT4
                                                  *pi4Ieee8021QBridgeProtocolPortGroupId)
{
    UNUSED_PARAM (pu4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pi4Ieee8021QBridgeProtocolPortGroupId);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                nextIeee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                nextIeee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
                nextIeee8021QBridgeProtocolPortGroupId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021QBridgeProtocolPortTable (UINT4
                                                 u4Ieee8021BridgeBasePortComponentId,
                                                 UINT4
                                                 *pu4NextIeee8021BridgeBasePortComponentId,
                                                 UINT4 u4Ieee8021BridgeBasePort,
                                                 UINT4
                                                 *pu4NextIeee8021BridgeBasePort,
                                                 INT4
                                                 i4Ieee8021QBridgeProtocolPortGroupId,
                                                 INT4
                                                 *pi4NextIeee8021QBridgeProtocolPortGroupId)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (pu4NextIeee8021BridgeBasePort);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolPortGroupId);
    UNUSED_PARAM (pi4NextIeee8021QBridgeProtocolPortGroupId);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolPortGroupVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                retValIeee8021QBridgeProtocolPortGroupVid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolPortGroupVid (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4Ieee8021QBridgeProtocolPortGroupId,
                                           INT4
                                           *pi4RetValIeee8021QBridgeProtocolPortGroupVid)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolPortGroupId);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeProtocolPortGroupVid);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021QBridgeProtocolPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                retValIeee8021QBridgeProtocolPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021QBridgeProtocolPortRowStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4Ieee8021QBridgeProtocolPortGroupId,
                                            INT4
                                            *pi4RetValIeee8021QBridgeProtocolPortRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolPortGroupId);
    UNUSED_PARAM (pi4RetValIeee8021QBridgeProtocolPortRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeProtocolPortGroupVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                setValIeee8021QBridgeProtocolPortGroupVid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeProtocolPortGroupVid (UINT4
                                           u4Ieee8021BridgeBasePortComponentId,
                                           UINT4 u4Ieee8021BridgeBasePort,
                                           INT4
                                           i4Ieee8021QBridgeProtocolPortGroupId,
                                           INT4
                                           i4SetValIeee8021QBridgeProtocolPortGroupVid)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolPortGroupId);
    UNUSED_PARAM (i4SetValIeee8021QBridgeProtocolPortGroupVid);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIeee8021QBridgeProtocolPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                setValIeee8021QBridgeProtocolPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021QBridgeProtocolPortRowStatus (UINT4
                                            u4Ieee8021BridgeBasePortComponentId,
                                            UINT4 u4Ieee8021BridgeBasePort,
                                            INT4
                                            i4Ieee8021QBridgeProtocolPortGroupId,
                                            INT4
                                            i4SetValIeee8021QBridgeProtocolPortRowStatus)
{
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolPortGroupId);
    UNUSED_PARAM (i4SetValIeee8021QBridgeProtocolPortRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolPortGroupVid
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                testValIeee8021QBridgeProtocolPortGroupVid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolPortGroupVid (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4Ieee8021BridgeBasePortComponentId,
                                              UINT4 u4Ieee8021BridgeBasePort,
                                              INT4
                                              i4Ieee8021QBridgeProtocolPortGroupId,
                                              INT4
                                              i4TestValIeee8021QBridgeProtocolPortGroupVid)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolPortGroupId);
    UNUSED_PARAM (i4TestValIeee8021QBridgeProtocolPortGroupVid);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021QBridgeProtocolPortRowStatus
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId

                The Object 
                testValIeee8021QBridgeProtocolPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021QBridgeProtocolPortRowStatus (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4Ieee8021BridgeBasePortComponentId,
                                               UINT4 u4Ieee8021BridgeBasePort,
                                               INT4
                                               i4Ieee8021QBridgeProtocolPortGroupId,
                                               INT4
                                               i4TestValIeee8021QBridgeProtocolPortRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021BridgeBasePortComponentId);
    UNUSED_PARAM (u4Ieee8021BridgeBasePort);
    UNUSED_PARAM (i4Ieee8021QBridgeProtocolPortGroupId);
    UNUSED_PARAM (i4TestValIeee8021QBridgeProtocolPortRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021QBridgeProtocolPortTable
 Input       :  The Indices
                Ieee8021BridgeBasePortComponentId
                Ieee8021BridgeBasePort
                Ieee8021QBridgeProtocolPortGroupId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021QBridgeProtocolPortTable (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
