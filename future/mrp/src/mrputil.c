/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrputil.c,v 1.13 2013/05/06 11:58:46 siva Exp $
 *
 * Description: This file contains the MRP data structure related utility 
 * routines.
 ******************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilHandleBridgeMode
 *                                                                          
 *    DESCRIPTION      : This function fills the corresponding MVRP MAC 
 *                       address of the bridge.
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpUtilHandleBridgeMode (tMrpContextInfo * pMrpContextInfo)
{
    if (MrpPortL2IwfGetBridgeMode (pMrpContextInfo->u4ContextId,
                                   &(pMrpContextInfo->u4BridgeMode))
        == L2IWF_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (pMrpContextInfo->u4BridgeMode == MRP_INVALID_BRIDGE_MODE)
    {
        return OSIX_FAILURE;
    }

    switch (pMrpContextInfo->u4BridgeMode)
    {
        case MRP_PROVIDER_BRIDGE_MODE:
            /* Fall through */
        case MRP_CUSTOMER_BRIDGE_MODE:
            MEMCPY (pMrpContextInfo->MvrpAddr,
                    gCustomerMvrpAddr, sizeof (tMacAddr));
            break;
        case MRP_PBB_ICOMPONENT_BRIDGE_MODE:
            /* Fall through */
        case MRP_PBB_BCOMPONENT_BRIDGE_MODE:
            /* Fall through */
        case MRP_PROVIDER_CORE_BRIDGE_MODE:
            /* Fall through */
        case MRP_PROVIDER_EDGE_BRIDGE_MODE:
            MEMCPY (pMrpContextInfo->MvrpAddr,
                    gProviderMvrpAddr, sizeof (tMacAddr));
            break;
        default:
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilIsMrpStarted
 *                                                                          
 *    DESCRIPTION      : This function returns OSIX_TRUE if MRP is started 
 *                       in the given Virtual Context else it returns 
 *                       OSIX_FALSE.
 *
 *    INPUT            : u4ContextId - Context Identifier
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                                          
 ****************************************************************************/
INT4
MrpUtilIsMrpStarted (UINT4 u4ContextId)
{
    INT4                i4RetVal = OSIX_FALSE;

    if ((u4ContextId != 0) && (u4ContextId < MRP_SIZING_CONTEXT_COUNT))
    {
        if ((gMrpGlobalInfo.u1MrpIsInitComplete == OSIX_TRUE) &&
            (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_TRUE))
        {
            i4RetVal = OSIX_TRUE;
        }
    }
    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilSyncAttributeTable
 *                                                                          
 *    DESCRIPTION      : Transmits Join messages on the given port for all
 *                       the Attributes registered on other ports.
 *
 *    INPUT            : pMrpAppEntry     - Pointer to the Appliction structure
 *                       pMrpAppPortEntry - Pointer to the App port structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpUtilSyncAttributeTable (tMrpAppEntry * pMrpAppEntry,
                           tMrpAppPortEntry * pMrpAppPortEntry)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpMapEntry       *pMrpMapEntry = NULL;
    tMrpIfMsg           IfMsg;
    UINT2               u2MapId = 0;

    pMrpContextInfo = pMrpAppEntry->pMrpContextInfo;

    IfMsg.pAppEntry = pMrpAppEntry;
    IfMsg.u2Port = pMrpAppPortEntry->pMrpPortEntry->u2LocalPortId;

    for (u2MapId = 0; u2MapId < pMrpAppEntry->u2MapArraySize; u2MapId++)
    {
        pMrpMapEntry = MRP_GET_MAP_ENTRY (pMrpAppEntry, u2MapId);

        if (pMrpMapEntry == NULL)
        {
            continue;
        }

        IfMsg.u2MapId = u2MapId;
        pMrpMapEntry->pMrpAppEntry = pMrpAppEntry;

        MrpMapSyncAttributeTableForMap (pMrpContextInfo, pMrpMapEntry, &IfMsg);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilPortStateChgToForwarding
 *                                                                          
 *    DESCRIPTION      : Invoked whenever port state is changed from      
 *                       Blocked state to Forwarding state.
 *
 *    INPUT            : pContextInfo  - Pointer to the Context Information
 *                       pAppEntry     - Pointer to the Appliction structure
 *                       pMapEntry     - Pointer to the MAP structure
 *                       pMapPortEntry - Pointer to the MAP port structure
 *                       u1AppStatus   - MRP_ENABLED/MRP_DISABLED
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpUtilPortStateChgToForwarding (tMrpContextInfo * pContextInfo,
                                 tMrpAppEntry * pAppEntry,
                                 tMrpMapEntry * pMapEntry,
                                 tMrpMapPortEntry * pMapPortEntry,
                                 UINT1 u1AppStatus)
{
    tMrpAttrEntry      *pMrpAttrEntry = NULL;
    tMrpIfMsg           IfMsg;
    UINT2               u2Index = 0;
    UINT1              *pu1AttrList = NULL;

    if (pMapPortEntry->u1PortState == AST_PORT_STATE_FORWARDING)
    {
        /* Old STP status equal to the new STP status. */
        return;
    }

    IfMsg.u4ContextId = pContextInfo->u4ContextId;
    IfMsg.pAppEntry = pAppEntry;
    IfMsg.u2MapId = pMapEntry->u2MapId;
    IfMsg.u2Port = pMapPortEntry->u2Port;

    pMapPortEntry->u1PortState = AST_PORT_STATE_FORWARDING;

    /* Propagate the entries registered on this port on all other ports */
    pu1AttrList = pMapPortEntry->pu1AttrInfoList;

    for (u2Index = 1; u2Index <= pMapPortEntry->u2AttrArraySize; u2Index++)
    {
        pMrpAttrEntry = MRP_GET_ATTR_ENTRY (pMapEntry, u2Index);

        if (pMrpAttrEntry == NULL)
        {
            continue;
        }

        if (MRP_GET_ATTR_REG_SEM_STATE (pu1AttrList[u2Index]) == MRP_IN)
        {
            /* When the port state moving to Forwarding ,It should always 
             * sends NEW indication. */

            MrpMapPropagateOnAllPorts (&(pMrpAttrEntry->Attr), &IfMsg,
                                       pMapEntry, MRP_SEND_NEW);

            /* If the Admin Reg control is Fixed, then we must make the 
             * Applicant, a member. This is because, when the port moves to 
             * Blocking state, all Member Applicants are made Observer 
             * Applicants. */

            if ((MRP_GET_ATTR_REG_ADMIN_CTRL (pu1AttrList[u2Index]) ==
                 MRP_REG_FIXED) && (u1AppStatus == MRP_ENABLED))
            {
                MrpSmApplicantSem (pMapPortEntry, u2Index, MRP_APP_REQ_JOIN);
            }
        }
    }

    if (u1AppStatus == MRP_ENABLED)
    {
        /* Declare on this port all the attributes registered on other 
         * ports */
        MrpMapSyncAttributeTableForMap (pContextInfo, pMapEntry, &IfMsg);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilPortStateChgedToBlocking
 *                                                                          
 *    DESCRIPTION      : Invoked whenever port state is changed from      
 *                       Forwarding state to Blocked state.
 *
 *    INPUT            : pContextInfo  - Pointer to the Context Information
 *                       pAppEntry     - Pointer to the Appliction structure
 *                       pMapEntry     - Pointer to the MAP structure
 *                       pMapPortEntry - Pointer to the MAP port structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpUtilPortStateChgedToBlocking (tMrpContextInfo * pContextInfo,
                                 tMrpAppEntry * pAppEntry,
                                 tMrpMapEntry * pMapEntry,
                                 tMrpMapPortEntry * pMapPortEntry)
{
    tMrpAttrEntry      *pMrpAttrEntry = NULL;
    UINT1              *pu1AttrPtr = NULL;
    tMrpIfMsg           IfMsg;
    INT4                i4RetVal = OSIX_FALSE;
    UINT2               u2Index = 0;

    if (pMapPortEntry->u1PortState == AST_PORT_STATE_DISCARDING)
    {
        /* Old STAP status equal to new STAP status. */
        return;
    }

    IfMsg.u4ContextId = pContextInfo->u4ContextId;
    IfMsg.pAppEntry = pAppEntry;
    IfMsg.u2MapId = pMapEntry->u2MapId;
    IfMsg.u2Port = pMapPortEntry->u2Port;

    pMapPortEntry->u1PortState = AST_PORT_STATE_DISCARDING;

    pu1AttrPtr = pMapPortEntry->pu1AttrInfoList;
    for (u2Index = 1; u2Index <= pMapPortEntry->u2AttrArraySize; u2Index++)
    {
        pMrpAttrEntry = MRP_GET_ATTR_ENTRY (pMapEntry, u2Index);

        if (pMrpAttrEntry == NULL)
        {
            continue;
        }

        if (MRP_GET_ATTR_REG_SEM_STATE (pu1AttrPtr[u2Index]) == MRP_IN)
        {
            /* Previously when the STAP status of a port goes from FORWARDING 
             * to BLOCKING ,leave is propogated on to all ports only if no port
             * has registration FIXED for that attribute.But as per IEEE802.1u
             * implementation LEAVE should be propogated on all ports only if 
             * no port has its Registrar State Machine to be IN state for that 
             * attribute. ie.,even if that attribute has been learnt 
             * dynamically on any one of the ports LEAVE should not be 
             * propogated */

            i4RetVal = MrpAttrIsAttrAdminRegCtrlFixed
                (pContextInfo, pAppEntry, pMapEntry,
                 pMrpAttrEntry->u2AttrIndex, pMapPortEntry->u2Port);

            if (i4RetVal == OSIX_FALSE)
            {
                MrpMapPropagateOnAllPorts (&(pMrpAttrEntry->Attr), &IfMsg,
                                           pMapEntry, MRP_SEND_LEAVE);
            }
        }

        MrpSmApplicantSem (pMapPortEntry, u2Index, MRP_APP_REQ_LEAVE);
        MrpAttrCheckAndDelAttrEntry (pMapPortEntry, pMrpAttrEntry);
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilHandleSharedtoP2PMediaChg
 *                                                                          
 *    DESCRIPTION      : This function updates the Applicant SEM states 
 *                       that are specific to shared media 
 *
 *    INPUT            : pContextInfo    - Pointer to the Context Info structure
 *                       pPortEntry      - Pointer to the MrpPortEntry
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
VOID
MrpUtilHandleSharedtoP2PMediaChg (tMrpContextInfo * pContextInfo,
                                  tMrpPortEntry * pPortEntry)
{
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT2               u2MapId = 0;
    UINT2               u2Port = 0;
    UINT2               u2Index = 0;
    UINT1              *pu1Attrptr = NULL;
    UINT1               u1AppSemState = 0;
    UINT1               u1AppId = 0;

    u2Port = pPortEntry->u2LocalPortId;

    for (u1AppId = 1; u1AppId <= MRP_MAX_APPS; u1AppId++)
    {
        if (OSIX_FALSE == MRP_IS_MRP_APP_ENABLED (pContextInfo->u4ContextId,
                                                  u1AppId))
        {
            continue;
        }

        if (MRP_DISABLED == MRP_PORT_APP_STATUS (pPortEntry, u1AppId))
        {
            continue;
        }

        pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);

        for (u2MapId = 0; u2MapId < pAppEntry->u2MapArraySize; u2MapId++)
        {
            pMapEntry = pAppEntry->ppMapTable[u2MapId];

            if (NULL == pMapEntry)
            {
                continue;
            }

            pMapPortEntry = pMapEntry->apMapPortEntry[u2Port];

            if (NULL == pMapPortEntry)
            {
                continue;
            }

            pu1Attrptr = pMapPortEntry->pu1AttrInfoList;

            for (u2Index = 1; u2Index <= pMapPortEntry->u2AttrArraySize;
                 u2Index++)
            {
                u1AppSemState =
                    MRP_GET_ATTR_APP_SEM_STATE (pu1Attrptr[u2Index]);
                /* Point-to-point subset participants exclue AO, QO, AP and QP.
                 */
                switch (u1AppSemState)
                {
                    case MRP_AO:
                        MRP_SET_ATTR_APP_SEM_STATE (pu1Attrptr[u2Index],
                                                    MRP_VO);
                        break;
                    case MRP_QO:
                        MRP_SET_ATTR_APP_SEM_STATE (pu1Attrptr[u2Index],
                                                    MRP_VO);
                        break;
                    case MRP_AP:
                        MRP_SET_ATTR_APP_SEM_STATE (pu1Attrptr[u2Index],
                                                    MRP_VP);
                        break;
                    case MRP_QP:
                        MRP_SET_ATTR_APP_SEM_STATE (pu1Attrptr[u2Index],
                                                    MRP_VP);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilHandlePortRoleChg
 *                                                                          
 *    DESCRIPTION      : This function processes the port role change.
 *
 *    INPUT            : pAppEntry      - Pointer to the Application Entry
 *                       u2MapID        - CIST or MST Instance Identifier of 
 *                                        the port whose port role has changed
 *                       u2Port         - Local Port Number
 *                       u1PortRoleEvent- Flush/Redeclare, depends on the old
 *                                        and the new port role.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
VOID
MrpUtilHandlePortRoleChg (tMrpAppEntry * pAppEntry, UINT2 u2MapId, UINT2 u2Port,
                          UINT1 u1PortRoleEvent)
{
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    UINT2               u2Index = 0;
    UINT2               u2AttributeSize = 0;
    UINT1               u1RegSEMStateChg = OSIX_FALSE;

    pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

    if (NULL == pMapEntry)
    {
        /* MAP Entry not created */

        MRP_TRC ((pAppEntry->pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpUtilHandlePortRoleChg: Map Entry for %d not"
                  " created in MRP Application  %d \n", u2MapId,
                  pAppEntry->u1AppId));

        return;
    }

    pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);
    if (NULL == pMapPortEntry)
    {
        /* MAP Port Entry not created */
        MRP_TRC ((pAppEntry->pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpUtilHandlePortRoleChg: Map Port Entry for"
                  " port %d not created in MRP Module. \n", u2Port));

        return;
    }

    u2AttributeSize = pMapPortEntry->u2AttrArraySize;

    for (u2Index = 1; u2Index <= u2AttributeSize; u2Index++)
    {
        /* if the map entry is deleted in the first iteration itself.
         * then return here */
        if ((pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId)) == NULL)
        {
            return;
        }

        pAttrEntry = MRP_GET_ATTR_ENTRY (pMapEntry, u2Index);

        if (pAttrEntry == NULL)
        {
            continue;
        }

        if ((MRP_GET_ATTR_REG_SEM_STATE
             (pMapPortEntry->pu1AttrInfoList[u2Index]) == MRP_MT) &&
            (MRP_GET_ATTR_APP_SEM_STATE
             (pMapPortEntry->pu1AttrInfoList[u2Index]) == MRP_VO))
        {
            continue;
        }

        if ((MRP_FLUSH_IND == u1PortRoleEvent) &&
            (MRP_GET_ATTR_REG_ADMIN_CTRL
             (pMapPortEntry->pu1AttrInfoList[u2Index]) == MRP_REG_NORMAL))
        {
            MrpSmRegistrarSem (pMapPortEntry, u2Index, MRP_REG_FLUSH,
                               &u1RegSEMStateChg);
        }
        else if (MRP_REDECLARE_IND == u1PortRoleEvent)
        {
            if (MRP_REG_NORMAL == MRP_GET_ATTR_REG_ADMIN_CTRL
                (pMapPortEntry->pu1AttrInfoList[u2Index]))
            {
                MrpSmRegistrarSem (pMapPortEntry, u2Index, MRP_REG_REDECLARE,
                                   &u1RegSEMStateChg);
            }
            MrpSmApplicantSem (pMapPortEntry, u2Index, MRP_APP_REDECLARE);
        }
    }

    if (MRP_FLUSH_IND == u1PortRoleEvent)
    {
        /* Applying  LeaveAll timer expiry */
        pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMapPortEntry,
                                                       pMapPortEntry->u2Port);
        if (pAppPortEntry == NULL)
        {
            return;
        }
        pAppPortEntry->u1LeaveAllSemState = MRP_ACTIVE;
        MrpTmrRestartLvAllTmr (pAppPortEntry);
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilCheckIfMACIsValid 
 *                                                                          
 *    DESCRIPTION      : This function checks if the MAC Address is VALID    
 *
 *    INPUT            : MacAddr  - MAC Address which needs to be validated
 *                                                                          
 *    OUTPUT           : None 
 *
 *    RETURNS          : OSIX_TRUE / OSIX_FALSE
 *                                                                          
 ****************************************************************************/
INT4
MrpUtilCheckIfMACIsValid (tMacAddr MacAddr)
{
    if (MRP_IS_NULL_MAC_ADDR (MacAddr) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }

    if (MRP_VLAN_IS_BCASTADDR (MacAddr) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }

    /* Check for Vlan Reserved Address */
    if (MRP_VLAN_IS_RESERVED_ADDR (MacAddr) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }

    /* Check for MRP Reserved Address */
    if (MRP_IS_RESERVED_MRP_APP_ADDR (MacAddr) == OSIX_TRUE)
    {
        return OSIX_FALSE;
    }

    return OSIX_TRUE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilGetNextMAC
 *                                                                          
 *    DESCRIPTION      : This function returns the next MAC Address by adding
 *                       one to the value of the current MAC Address.          
 *
 *    INPUT            : MacAddr    - MAC whose next MAC is to retured
 *                                                                          
 *    OUTPUT           : MacAddr    - Next MAC Address
 *
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/

VOID
MrpUtilGetNextMAC (tMacAddr MacAddr)
{
    INT1                i1MacPos = 0;

    for (i1MacPos = (MAC_ADDR_LEN - 1); i1MacPos >= 0; i1MacPos--)
    {
        if (i1MacPos == (MAC_ADDR_LEN - 1))
        {
            if (((UINT1) (MacAddr[i1MacPos] + 1)) < (MacAddr[i1MacPos]))
            {
                continue;
            }
            else
            {
                MacAddr[i1MacPos] = (UINT1) (MacAddr[i1MacPos] + 1);
                break;
            }
        }
        else if (i1MacPos != (MAC_ADDR_LEN - 1))
        {
            if (MacAddr[i1MacPos] == MRP_MAX_VAL_IN_BYTE)
            {
                continue;
            }
            else
            {
                MacAddr[i1MacPos] = (UINT1) (MacAddr[i1MacPos] + 1);
                break;
            }
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilTranslateVID
 *                                                                          
 *    DESCRIPTION      : This function translates the local VID to relay VID 
 *                       and fills the pAttr accordingly.
 *
 *    INPUT            : u4ContextId - Context Identider
 *                       u2Port      - Local port on which the PDU was received
 *                       pVid        - Vlan ID which needs to be translated
 *                                                                          
 *    OUTPUT           : pAttr       - Pointer to the MrpAttribute containg the 
 *                                     translated VID
 *                       pVid        - Translated VID
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpUtilTranslateVID (UINT4 u4ContextId, UINT2 u2Port, tMrpAttr * pAttr,
                     tVlanId * pVid)
{

    INT4                i4RetVal = OSIX_SUCCESS;
    tVlanId             RelayVid = 0;

    if (OSIX_FALSE == MRP_IS_802_1AD_BRIDGE (MRP_CONTEXT_PTR (u4ContextId)))
    {
        return OSIX_SUCCESS;
    }

    /* Get the translated value for given vid. */
    i4RetVal = MrpPortPbGetRelayVidFrmLocalVid (u4ContextId, u2Port, *pVid,
                                                &RelayVid);

    if (L2IWF_SUCCESS == i4RetVal)
    {
        /* Change the given value to relay vid. */
        *pVid = RelayVid;

        RelayVid = (tVlanId) OSIX_HTONS (RelayVid);

        MEMCPY (pAttr->au1AttrVal, &RelayVid, MRP_VLAN_ID_LEN);
    }
    else
    {
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilTransLocalVidFromRelayVid
 *                                                                          
 *    DESCRIPTION      : This function translates the Relay VID to Local VID. 
 *
 *    INPUT            : u4ContextId - Context Identider
 *                       u2Port      - Local port on which the PDU was received
 *                       pVid        - Vlan ID which needs to be translated
 *                                                                          
 *    OUTPUT           : pVid        - Translated VID
 *
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpUtilTransLocalVidFromRelayVid (UINT4 u4ContextId, UINT2 u2Port,
                                  tVlanId * pVid)
{
    INT4                i4RetVal = L2IWF_FAILURE;
    tVlanId             LocalVid = 0;

    if (OSIX_FALSE == MRP_IS_802_1AD_BRIDGE (MRP_CONTEXT_PTR (u4ContextId)))
    {
        return;
    }

    /* Get the translated value for given vid. */
    i4RetVal = MrpPortPbGetLocalVidFrmRelayVid (u4ContextId, u2Port, *pVid,
                                                &LocalVid);
    if (L2IWF_SUCCESS == i4RetVal)
    {
        /* Change the given value to Local vid. */
        *pVid = LocalVid;
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilIsPortOkForMRPDU
 *                                                                          
 *    DESCRIPTION      : This function checks if the MRPDU received on the 
 *                       given port can be accepted or not in case of 802.1 AD
 *                       bridges. MVRPDUs received cannot be accepted on ports 
 *                       other than PNP and PPNPs. When a customer MVRPDU is 
 *                       received on a PPNP, its destination address needs to be
 *                       changed. 
 *
 *    INPUT            : pMrpPortEntry - Pointer to MRP port entry 
 *                       AppAddress    - Destination Address of the received 
 *                                       MRPDU
 *                                                                          
 *    OUTPUT           : AppAddress    - Updated value to be used for further
 *                                       processing.   
 *
 *    RETURNS          : OSIX_TRUE if the MRPDU can be accepted else OSIX_FALSE
 *                                                                          
 ****************************************************************************/
INT4
MrpUtilIsPortOkForMRPDU (tMrpPortEntry * pMrpPortEntry, tMacAddr AppAddress)
{
    tMrpContextInfo    *pContextInfo = NULL;
    INT4                i4RetVal = OSIX_TRUE;

    if ((pMrpPortEntry->u2BridgePortType == MRP_PROP_PROVIDER_NETWORK_PORT)
        && (MEMCMP (AppAddress, gCustomerMvrpAddr, sizeof (tMacAddr)) == 0))
    {
        /* As propritary customer network ports are connected to Q-in-Q 
         * bridge, we have to process the MVRP packets received on this 
         * port with destination address as customer MVRP address in the 
         * S-VLAN component. */

        /* This we achieve by changing the AppAddress to gProviderMvrpAddr,
         * so the remaining thread thinks that a Mvrp packet is received 
         * with provider MVRP address on this port. */
        pContextInfo = pMrpPortEntry->pMrpContextInfo;

        if (pContextInfo != NULL)
        {
            MEMCPY (AppAddress, pContextInfo->MvrpAddr, ETHERNET_ADDR_SIZE);
        }
        else
        {
            i4RetVal = OSIX_FALSE;
        }
    }

    return i4RetVal;
}

/*************************************************************************
*                                                                          
*    FUNCTION NAME       : MrpUtilGetNextActiveContext                       
*                                                                          
*    DESCRIPTION         : This function is used to get the next Active     
*                          context present in the system.                   
*                                                                           
*    INPUT(s)            : u4CurrContextId - Current Context Id.            
*                                                                          
*    OUTPUT(s)           : pu4NextContextId - Next Context Id.              
*                                                                          
*    RETURNS            : OSIX_SUCCESS/OSIX_FAILURE                        
* *************************************************************************/
INT4
MrpUtilGetNextActiveContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4ContextId = u4CurrContextId + 1;

    for (; u4ContextId < MRP_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

        if (pMrpContextInfo != NULL)
        {
            if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_TRUE)
            {
                *pu4NextContextId = u4ContextId;
                return OSIX_SUCCESS;
            }
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME       : MrpUtilGetInstPortState                             
 *                                                                           
 *    DESCRIPTION         : This function will get the per instance port     
 *                          state from the L2IWF module. In case of CEP it   
 *                          returns forwarding                               
 *                                                                           
 *    INPUT(s)            : u2MapId- Instance Id.                            
 *                          pMrpPortEntry- Global Port Entry                
 *                                                                           
 *    OUTPUT(s)           : None.                                            
 *                                                                           
 *    RETURNS             : AST_PORT_STATE_FORWARDING/                       
 *                          AST_PORT_STATE_DISCARDING                        
 *                                                                           
 *****************************************************************************/

UINT1
MrpUtilGetInstPortState (UINT2 u2MapId, tMrpPortEntry * pMrpPortEntry)
{
    /*Since CEP is a part of CVLAN component, MRP Protocol assumes CEP will
     * be always forwarding */
    if (pMrpPortEntry->u2BridgePortType == MRP_CUSTOMER_EDGE_PORT)
    {
        return (UINT1) AST_PORT_STATE_FORWARDING;
    }

    return (MrpPortL2IwfGetInstPortState (u2MapId, pMrpPortEntry->u4IfIndex));
}

/****************************************************************************
 *                                                                           
 *    FUNCTION NAME       : MrpUtilGetVlanPortState                             
 *                                                                           
 *    DESCRIPTION         : This function will get the per vlan port         
 *                          state from the L2IWF module. In case of CEP it   
 *                          returns forwarding                               
 *                                                                           
 *    INPUT(s)            : u2MapId- Instance Id.                            
 *                          pGlobPortEntry- Global Port Entry                
 *                                                                           
 *    OUTPUT(s)           : None.                                            
 *                                                                           
 *    RETURNS             : AST_PORT_STATE_FORWARDING/                       
 *                          AST_PORT_STATE_DISCARDING                        
 *                                                                           
 *****************************************************************************/

UINT1
MrpUtilGetVlanPortState (tMrpMapPortEntry * pMrpMapPortEntry)
{
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    /*Since CEP is a part of CVLAN component, MRP Protocol assumes CEP will
     * be always forwarding */
    pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pMrpMapPortEntry,
                                                   pMrpMapPortEntry->u2Port);

    if (pAppPortEntry != NULL)
    {
        if (pAppPortEntry->pMrpPortEntry->u2BridgePortType
            == MRP_CUSTOMER_EDGE_PORT)
        {
            return (UINT1) AST_PORT_STATE_FORWARDING;
        }
    }

    return (pMrpMapPortEntry->u1PortState);
}

/***********************************************************************
 * FUNCTION NAME    : MrpUtilHandlePeriodicEvntOnPort                        
 *                                                                      
 * DESCRIPTION      : This function handle the Periodic event (generated
 *                    by the periodic timer expiry) on a particular 
 *                    port. This will send a periodic event to all the
 *                    applicant state event machines, running on this 
 *                    port.
 *
 * INPUT(s)         : pPortEntry - Pointer to the port entry
 *                                                                           
 * OUTPUT(s)        : None.                                            
 *                                                                           
 * RETURNS          : None.
 *                                                                      
 ************************************************************************/
VOID
MrpUtilHandlePeriodicEvntOnPort (tMrpPortEntry * pPortEntry)
{
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpContextInfo    *pContextInfo = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAttrEntry      *pNextAttrEntry = NULL;
    UINT2               u2Port = 0;
    UINT2               u2MapId = 0;
    UINT1               u1AppId = 0;

    u2Port = pPortEntry->u2LocalPortId;
    pContextInfo = pPortEntry->pMrpContextInfo;

    if (pContextInfo == NULL)
    {
        return;
    }

    /* Scan the Application Table */
    for (u1AppId = 1; u1AppId <= MRP_MAX_APPS; u1AppId++)
    {
        pAppEntry = MRP_GET_APP_ENTRY (pContextInfo, u1AppId);

        /* Scan each MAP entry associated with this APP Entry */
        for (u2MapId = 0; u2MapId < pAppEntry->u2MapArraySize; u2MapId++)
        {
            pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);
            if (pMapEntry == NULL)
            {
                MRP_TRC ((pContextInfo,
                          (ALL_FAILURE_TRC | MRP_PERIODIC_SEM_TRC),
                          "MrpUtilHandlePeriodicEvntOnPort: MapEntry not found."
                          " Map Id : %d \r\n", u2MapId));
                continue;
            }

            /* Get the Map Port Entry */
            pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

            if (pMapPortEntry == NULL)
            {
                MRP_TRC ((pContextInfo,
                          (ALL_FAILURE_TRC | MRP_PERIODIC_SEM_TRC),
                          "MrpUtilHandlePeriodicEvntOnPort: MapPortEntry not"
                          " found. Port Id : %d \r\n", u2Port));
                continue;
            }

            /* u2Port belongs to this MAP */
            /* Scan the Attribute list associated with this MAP port */

            pNextAttrEntry = (tMrpAttrEntry *)
                RBTreeGetFirst (pAppEntry->MapAttrTable);

            while (pNextAttrEntry != NULL)
            {
                pAttrEntry = pNextAttrEntry;

                /* Post periodic! event to the current applicant SEM */
                MrpSmApplicantSem (pMapPortEntry, pAttrEntry->u2AttrIndex,
                                   MRP_APP_PERIODIC);

                /* Get the next attribute entry for this port */
                pNextAttrEntry = (tMrpAttrEntry *)
                    RBTreeGetNext (pAppEntry->MapAttrTable,
                                   (tRBElem *) pAttrEntry, NULL);
            }
        }
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpLock
 *
 *    DESCRIPTION      : Function to take the mutual exclusion protocol
 *                       semaphore.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpLock (VOID)
{
    if (OsixSemTake (gMrpGlobalInfo.SemId) == OSIX_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);

}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUnLock
 *
 *    DESCRIPTION      : Function to release the mutual exclusion protocol
 *                       semaphore.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpUnLock (VOID)
{
    OsixSemGive (gMrpGlobalInfo.SemId);
    return (SNMP_SUCCESS);
}

/***********************************************************************
 * FUNCTION NAME    : MrpUtilIncrTxStats                        
 *                                                                      
 * DESCRIPTION      : This function increments the Transmission counters 
 *                    specified in the u1AttrEvent.
 *
 * INPUT(s)         : pPortEntry  - Pointer to the port entry
 *                    u1AppId     - Application ID (MVRP/MMRP)
 *                    u1AttrEvent - Attribute Event
 *                                                                           
 * OUTPUT(s)        : None.                                            
 *                                                                           
 * RETURNS          : None.
 *                                                                      
 ************************************************************************/
VOID
MrpUtilIncrTxStats (tMrpPortEntry * pMrpPortEntry, UINT1 u1AppId,
                    UINT1 u1AttrEvent)
{
    if (u1AttrEvent == MRP_TX_LEAVE_ALL)
    {
        pMrpPortEntry->pStatsEntry[u1AppId]->u4TxLeaveAllMsgCnt++;
    }
    else if (u1AttrEvent == MRP_TX_PDU_CNT)
    {
        pMrpPortEntry->pStatsEntry[u1AppId]->u4TxPduCnt++;
    }
    else
    {
        switch (u1AttrEvent)
        {
            case MRP_NEW_ATTR_EVENT:
                pMrpPortEntry->pStatsEntry[u1AppId]->u4TxNewMsgCnt++;
                break;

            case MRP_JOININ_ATTR_EVENT:
                pMrpPortEntry->pStatsEntry[u1AppId]->u4TxJoinInMsgCnt++;
                break;

            case MRP_JOINMT_ATTR_EVENT:
                pMrpPortEntry->pStatsEntry[u1AppId]->u4TxJoinMtMsgCnt++;
                break;

            case MRP_IN_ATTR_EVENT:
                pMrpPortEntry->pStatsEntry[u1AppId]->u4TxInMsgCnt++;
                break;

            case MRP_MT_ATTR_EVENT:
                pMrpPortEntry->pStatsEntry[u1AppId]->u4TxEmptyMsgCnt++;
                break;

            case MRP_LV_ATTR_EVENT:
                pMrpPortEntry->pStatsEntry[u1AppId]->u4TxLeaveMsgCnt++;
                break;

            default:
                break;
        }
    }
}

/***********************************************************************
 * FUNCTION NAME    : MrpUtilFindFirstSetBitInBitList           
 *                                                                      
 * DESCRIPTION      : This function finds the bit position (left to right) 
 *                    of the first set bit in the bitlist pointed  
 *                    by 'pu1Data'.
 *
 * INPUT(s)         : pu1Data       - Pointer to the bitlist
 *                    u2BitListSize - BitList size
 *                                                                           
 * OUTPUT(s)        : None.                                            
 *                                                                           
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                                      
 ************************************************************************/
UINT2
MrpUtilFindFirstSetBitInBitList (UINT1 *pu1Data, UINT2 u2BitListSize)
{
    UINT2               u2BytePos = 0;
    UINT2               u2BitPos = 0;
    UINT2               u2RetValue = 0;
    UINT1               u1Value = 0;
    UINT1               u1Temp = MRP_BIT8;

    for (u2BytePos = 0; u2BytePos < u2BitListSize; u2BytePos++)
    {
        if (pu1Data[u2BytePos] != 0)
        {
            u1Value = pu1Data[u2BytePos];

            for (u2BitPos = 0; ((u2BitPos < MRP_PORTS_PER_BYTE) &&
                                (u1Value != 0)); u2BitPos++)
            {
                if ((u1Value & u1Temp) != 0)
                {
                    u2RetValue = (UINT2) ((u2BytePos * MRP_PORTS_PER_BYTE) +
                                          (u2BitPos + 1));
                    return u2RetValue;
                }
            }
        }
    }
    return u2RetValue;
}

/***********************************************************************
 * FUNCTION NAME    : MrpUtilFindNextSetBitInBitList           
 *                                                                      
 * DESCRIPTION      : This function finds the bit position (left to right) 
 *                    of the next set bit in the bitlist pointed  
 *                    by 'pu1Data'.
 *
 * INPUT(s)         : pu1Data       - Pointer to the bitlist
 *                    u2BitListSize - BitList size
 *                    u2CurrentBit  - Current Bit in the Bitlist
 *                                                                           
 * OUTPUT(s)        : None.                                            
 *                                                                           
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                                      
 ************************************************************************/
UINT2
MrpUtilFindNextSetBitInBitList (UINT1 *pu1Data, UINT2 u2BitListSize,
                                UINT2 u2CurrentBit)
{
    UINT2               u2BytePos = 0;
    UINT2               u2BitPos = 0;
    UINT2               u2CurrBytePos = 0;
    UINT2               u2CurrBitPos = 0;
    UINT2               u2RetValue = 0;
    UINT1               u1Value = 0;
    UINT1               u1Temp = MRP_BIT8;

    u2CurrBytePos = (UINT2) (u2CurrentBit / MRP_PORTS_PER_BYTE);
    u2CurrBitPos = (UINT2) (u2CurrentBit % MRP_PORTS_PER_BYTE);

    for (u2BytePos = u2CurrBytePos; u2BytePos < u2BitListSize; u2BytePos++)
    {
        if (pu1Data[u2BytePos] != 0)
        {
            u1Value = pu1Data[u2BytePos];

            if (u2CurrBitPos != 0)
            {
                u1Value = (UINT1) (u1Value << u2CurrBitPos);
            }

            for (u2BitPos = u2CurrBitPos; ((u2BitPos < MRP_PORTS_PER_BYTE) &&
                                           (u1Value != 0)); u2BitPos++)
            {
                if ((u1Value & u1Temp) != 0)
                {
                    u2RetValue = (UINT2) ((u2BytePos * MRP_PORTS_PER_BYTE) +
                                          (u2BitPos + 1));
                    return u2RetValue;
                }
                u1Value = (UINT1) (u1Value << 1);
            }
        }
        u2CurrBitPos = 0;
    }
    return u2RetValue;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilSetTcDetectedTmrStatus
 *
 *    DESCRIPTION      : This function sets the TcDetected Timer status
 *
 *    INPUT            : pAppEntry   - Pointer to the Application Entry
 *                       u2MapId     - MAP Context ID
 *                       u2Port      - Local Port Number
 *                       u1TmrState  - Timer state (running or expired)
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpUtilSetTcDetectedTmrStatus (tMrpAppEntry * pAppEntry, UINT2 u2MapId,
                               UINT2 u2Port, UINT1 u1TmrState)
{
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;

    pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

    if (NULL == pMapEntry)
    {
        /* MAP Entry not created */

        MRP_TRC ((pAppEntry->pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpUtilSetTcDetectedTmrStatus: Map Entry for"
                  " %d not created in MRP Application  %d \n", u2MapId,
                  pAppEntry->u1AppId));

        return OSIX_FAILURE;
    }

    pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);
    if (NULL == pMapPortEntry)
    {
        /* MAP Port Entry not created */
        MRP_TRC ((pAppEntry->pMrpContextInfo, ALL_FAILURE_TRC,
                  "MrpUtilSetTcDetectedTmrStatus: Map Port Entry"
                  " for port %d not created in MRP Module. \n", u2Port));

        return OSIX_FAILURE;
    }

    pMapPortEntry->u1TcDetectedFlag = u1TmrState;
    return OSIX_SUCCESS;
}

/****************************************************************
 *
 *  FUNCTION NAME   : MrpUtilMmrpPropOrSetDefGrpInfo 
 *
 *  DESCRIPTION     : This function indicates the MRP task about
 *                    either propagating default group info or setting of
 *                    forbidden ports in default group.
 *
 *  INPUT           : u4ContextId - Context Identifier          
 *                    u2MsgType - Message type. It can take any one of
 *                                the following values.
 *                                VLAN_SET_FWDALL_FORBID_MSG
 *                                VLAN_SET_FWDUNREG_FORBID_MSG
 *                                VLAN_PROP_FWDALL_INFO_MSG
 *                                VLAN_PROP_FWDUNREG_INFO_MSG
 *                    AddPortList - Port list to be added
 *                    DelPortList - Port list to be deleted
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : None
 *
 ****************************************************************/
INT4
MrpUtilMmrpPropOrSetDefGrpInfo (UINT4 u4ContextId, UINT2 u2MsgType,
                                tVlanId VlanId, tLocalPortList AddPortList,
                                tLocalPortList DelPortList)
{
#ifdef VLAN_EXTENDED_FILTER
    tMrpQMsg           *pMrpQMsg = NULL;

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpUtilMmrpPropOrSetDefGrpInfo: MRP is not started in "
                     "current context\r\n");
        return OSIX_SUCCESS;
    }

    if (MrpMmrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpApiMmrpSetMcastForbiddPorts: MMRP is NOT enabled. "
                  "Cannot Propagate Default Groups (Value = %d) behaviour "
                  "for the Vlan Id %d\n", u2MsgType, VlanId));

        return OSIX_SUCCESS;
    }

    pMrpQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pMrpQMsg)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_CRITICAL_TRC,
                  "MrpUtilMmrpPropOrSetDefGrpInfo:"
                  "Q Msg memory allocation failed\n"));

        return OSIX_FAILURE;
    }

    MEMSET (pMrpQMsg, 0, sizeof (tMrpQMsg));

    pMrpQMsg->u2MsgType = MRP_MSG;

    pMrpQMsg->unMrpMsg.MrpMsg.u2MsgType = u2MsgType;

    pMrpQMsg->unMrpMsg.MrpMsg.u2MapId = VlanId;
    pMrpQMsg->u4ContextId = u4ContextId;

    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    MrpQueEnqMsg (pMrpQMsg);

    return OSIX_SUCCESS;

#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2MsgType);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);

    return OSIX_SUCCESS;
#endif /* VLAN_EXTENDED_FILTER */
}

/****************************************************************
 *
 *  FUNCTION NAME   : MrpUtilPostRcvdPduToTask 
 *
 *  DESCRIPTION     : This function enqueues the received frame to MRP PDU
 *                    Queue for processing. 
 *
 * INPUT            : pFrame - pointer to the received  MVRP/MMRP 
 *                                frame buffer (CRU buff)
 *                    u4ContextId - Component Id in which it should be
 *                                  processed
 *                    u2LocalPortId - Local Port identifier            
 *                    u2MapId - MAP id. value will be as follows:
 *                              0 in case of MVRP pkt.
 *                              VlanId in case of MMRP pkt.
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************/
INT4
MrpUtilPostRcvdPduToTask (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4ContextId,
                          UINT2 u2LocalPortId, UINT2 u2MapId)
{
    tMrpRxPduQMsg      *pRxPduQMsg = NULL;
    UINT2               u2EtherType = 0;

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpUtilPostRcvdPduToTask: MRP is not started in "
                     "current context\r\n");
        CRU_BUF_Release_MsgBufChain (pFrame, FALSE);

        return OSIX_FAILURE;
    }

    /* Get the EtherType Field in the Pkt */
    if (CRU_BUF_Copy_FromBufChain (pFrame, (UINT1 *) &u2EtherType,
                                   MRP_ETHER_TYPE_OFFSET,
                                   CFA_ENET_TYPE_OR_LEN) == CRU_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpUtilPostRcvdPduToTask: Failed to get ether type "
                     "from the packet\r\n");
        CRU_BUF_Release_MsgBufChain (pFrame, FALSE);

        return OSIX_FAILURE;
    }

    u2EtherType = OSIX_NTOHS (u2EtherType);

    /* Validate the ethertype field. */
    if ((u2EtherType != MRP_MMRP_ETHER_TYPE) &&
        (u2EtherType != MRP_MVRP_ETHER_TYPE))
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpUtilPostRcvdPduToTask: Invalid EtherType\r\n");
        CRU_BUF_Release_MsgBufChain (pFrame, FALSE);

        return OSIX_FAILURE;
    }

    /* Allocate memory for the PDU queue message */
    pRxPduQMsg = (tMrpRxPduQMsg *) MemAllocMemBlk
        (gMrpGlobalInfo.PduQMsgPoolId);

    if (pRxPduQMsg == NULL)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC,
                     "MrpUtilPostRcvdPduToTask: Failed to allocate the PDU "
                     "Queue Message buffer\r\n");

        CRU_BUF_Release_MsgBufChain (pFrame, FALSE);
        return OSIX_FAILURE;
    }

    /* Fill the PDU Queue message structure */
    pRxPduQMsg->pMrpPdu = pFrame;
    pRxPduQMsg->u4ContextId = u4ContextId;
    pRxPduQMsg->u2Port = u2LocalPortId;
    pRxPduQMsg->u2MapId = u2MapId;

    /* Post the message to MRP PDU Queue */
    if (OsixQueSend (gMrpGlobalInfo.RxPktQId, (UINT1 *) &pRxPduQMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpUtilPostRcvdPduToTask: Send To Q failed\r\n");
        CRU_BUF_Release_MsgBufChain (pFrame, FALSE);
        MemReleaseMemBlock (gMrpGlobalInfo.PduQMsgPoolId, (UINT1 *) pRxPduQMsg);

        return OSIX_FAILURE;
    }

    /* Send Event to MRP Task */
    OsixEvtSend (gMrpGlobalInfo.TaskId, MRP_PDU_ENQ_EVENT);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilIsMrpStartedInSystem
 *                                                                          
 *    DESCRIPTION      : This function returns OSIX_TRUE if MRP is started 
 *                       in any of the Virtual Contexts in the system else it 
 *                       returns OSIX_FALSE.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                                          
 ****************************************************************************/
INT4
MrpUtilIsMrpStartedInSystem ()
{
    tMrpContextInfo    *pContextInfo = NULL;
    INT4                i4RetVal = OSIX_FALSE;

    pContextInfo = MrpCtxtGetContextInfo (gMrpGlobalInfo.u2FirstContextId);

    for (; pContextInfo != NULL;
         (pContextInfo = (MrpCtxtGetContextInfo
                          (pContextInfo->u2NextContextId))))
    {
        if (OSIX_TRUE == MRP_IS_MRP_STARTED (pContextInfo->u4ContextId))
        {
            i4RetVal = OSIX_TRUE;
        }
    }

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpUtilDelAllLearntVlans 
 *                                                                          
 *    DESCRIPTION      : This function is called when the Registrar Admin  
 *                       Control parameter of the port is set as Forbidden.   
 *                       This function takes care of removing all the learnt
 *                       VLANs immediately.
 *
 *    INPUT            : pPortEntry - Pointer to MrpPort Entry 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpUtilDelAllLearntVlans (tMrpPortEntry * pPortEntry)
{
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT4               u4Index = 0;
    UINT2               u2MapId = 0;

    pAppEntry = &(pPortEntry->pMrpContextInfo->aMrpAppTable[MRP_MVRP_APP_ID]);

    for (u2MapId = 0; u2MapId < pAppEntry->u2MapArraySize; u2MapId++)
    {
        pMapEntry = pAppEntry->ppMapTable[u2MapId];

        if (pMapEntry == NULL)
        {
            continue;
        }

        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry,
                                                pPortEntry->u2LocalPortId);

        if (pMapPortEntry == NULL)
        {
            continue;
        }

        /* Delete all the VLANs that are learnt dynamically. */
        for (u4Index = 1; u4Index <= pMapPortEntry->u2AttrArraySize; u4Index++)
        {
            if ((pMapEntry->ppAttrEntryArray[u4Index] == NULL) ||
                (MRP_GET_ATTR_REG_ADMIN_CTRL
                 (pMapPortEntry->pu1AttrInfoList[u4Index]) != MRP_REG_NORMAL))
            {
                continue;
            }

            MrpPortVlanUpdateDynamicVlanInfo (pPortEntry->u4ContextId,
                                              (UINT2) u4Index,
                                              pPortEntry->u2LocalPortId,
                                              VLAN_DELETE);
            MrpMapDSDelAttrFromMapPortEntry (pMapPortEntry,
                                             pMapEntry->
                                             ppAttrEntryArray[u4Index]);
            MrpMapDsCheckAndDeleteMapEntry (pMapEntry);
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilGetMmrpEnabledStatus
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the MmrpEnabledStatus.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : pi4MmrpEnabledStatus -MmrpEnabledStatus 
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpUtilGetMmrpEnabledStatus (tMmrpStatusInfo * pMmrpStatusInfo)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (pMmrpStatusInfo->u4ContextId);
    if (pMrpContextInfo != NULL)
    {
        *(pMmrpStatusInfo->pi4MmrpEnabledStatus) =
            gMrpGlobalInfo.au1MmrpStatus[pMmrpStatusInfo->u4ContextId];
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilSetMmrpEnabledStatus
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to Set the MmrpEnabledStatus.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : i4MmrpEnabledStatus -MmrpEnabledStatus
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpUtilSetMmrpEnabledStatus (tMmrpStatusInfo * pMmrpStatusInfo)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if (MRP_IS_MRP_STARTED (pMmrpStatusInfo->u4ContextId) == OSIX_TRUE)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (pMmrpStatusInfo->u4ContextId);
        if (pMrpContextInfo != NULL)
        {
            if (gMrpGlobalInfo.au1MmrpStatus[pMmrpStatusInfo->u4ContextId] ==
                pMmrpStatusInfo->i4MmrpEnabledStatus)
            {
                return OSIX_SUCCESS;
            }
            switch (pMmrpStatusInfo->i4MmrpEnabledStatus)
            {
                case MRP_ENABLED:
                    i4RetVal = MrpMmrpEnable (pMrpContextInfo);
                    break;

                case MRP_DISABLED:
                    i4RetVal = MrpMmrpDisable (pMrpContextInfo);
                    break;

                default:
                    break;
            }

        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilTestMmrpEnabledStatus
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to Test the MmrpEnabledStatus.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : i4MmrpEnabledStatus -MmrpEnabledStatus
 *                     : pu4ErrorCode -Error Value
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpUtilTestMmrpEnabledStatus (tMmrpStatusInfo * pMmrpStatusInfo)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT2               u2VlanIsStarted = 0;

    if (MRP_IS_VC_VALID (pMmrpStatusInfo->u4ContextId) == OSIX_FALSE)
    {
        *pMmrpStatusInfo->pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (pMmrpStatusInfo->u4ContextId);

    if (pMrpContextInfo == NULL)
    {
        *pMmrpStatusInfo->pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (pMmrpStatusInfo->u4ContextId) == OSIX_FALSE)
    {
        *pMmrpStatusInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if ((pMmrpStatusInfo->i4MmrpEnabledStatus != MRP_ENABLED) &&
        (pMmrpStatusInfo->i4MmrpEnabledStatus != MRP_DISABLED))
    {
        *pMmrpStatusInfo->pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (pMmrpStatusInfo->i4MmrpEnabledStatus == MRP_ENABLED)
    {
        u2VlanIsStarted = (UINT2) MrpPortVlanGetStartedStatus
            (pMmrpStatusInfo->u4ContextId);

        /* Before Start the MRP module , VLAN module should
           be started.Otherwise no use of starting MRP. */
        if (u2VlanIsStarted == VLAN_FALSE)
        {
            *pMmrpStatusInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        /* Enable MMRP status only when the IGS and MLDS is disabled */
        if (SNOOP_ENABLED ==
            MrpPortIsIgmpSnoopingEnabled (pMmrpStatusInfo->u4ContextId))
        {
            *pMmrpStatusInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (SNOOP_ENABLED ==
            MrpPortSnoopIsMldSnoopingEnabled (pMmrpStatusInfo->u4ContextId))
        {
            *pMmrpStatusInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilGetMvrpEnabledStatus
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the MvrpEnabledStatus.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : pi4MvrpEnabledStatus -MvrpEnabledStatus 
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpUtilGetMvrpEnabledStatus (tMvrpStatusInfo * pMvrpStatusInfo)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (pMvrpStatusInfo->u4ContextId);
    if (pMrpContextInfo != NULL)
    {
        *pMvrpStatusInfo->pi4MvrpEnabledStatus =
            gMrpGlobalInfo.au1MvrpStatus[pMvrpStatusInfo->u4ContextId];
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilSetMvrpEnabledStatus
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to Set the MvrpEnabledStatus.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : i4MvrpEnabledStatus -MvrpEnabledStatus
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpUtilSetMvrpEnabledStatus (tMvrpStatusInfo * pMvrpStatusInfo)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if (MRP_IS_MRP_STARTED (pMvrpStatusInfo->u4ContextId) == OSIX_TRUE)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (pMvrpStatusInfo->u4ContextId);
        if (pMrpContextInfo != NULL)
        {
            if (gMrpGlobalInfo.au1MvrpStatus[pMvrpStatusInfo->u4ContextId] ==
                pMvrpStatusInfo->i4MvrpEnabledStatus)
            {
                return OSIX_SUCCESS;
            }
            switch (pMvrpStatusInfo->i4MvrpEnabledStatus)
            {
                case MRP_ENABLED:
                    i4RetVal = MrpMvrpEnable (pMrpContextInfo);
                    break;

                case MRP_DISABLED:
                    i4RetVal = MrpMvrpDisable (pMrpContextInfo);
                    break;

                default:
                    break;
            }

        }
    }
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilTestMvrpEnabledStatus
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to Test the MvrpEnabledStatus.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : i4MvrpEnabledStatus -MvrpEnabledStatus
 *                     : pu4ErrorCode -Error Value
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpUtilTestMvrpEnabledStatus (tMvrpStatusInfo * pMvrpStatusInfo)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT2               u2VlanIsStarted = 0;

    if (MRP_IS_VC_VALID (pMvrpStatusInfo->u4ContextId) == OSIX_FALSE)
    {
        *pMvrpStatusInfo->pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (pMvrpStatusInfo->u4ContextId);

    if (pMrpContextInfo == NULL)
    {
        *pMvrpStatusInfo->pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return OSIX_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (pMvrpStatusInfo->u4ContextId) == OSIX_FALSE)
    {
        *pMvrpStatusInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if (pMvrpStatusInfo->i4MvrpEnabledStatus == MRP_ENABLED)
    {
        u2VlanIsStarted = (UINT2) MrpPortVlanGetStartedStatus
            (pMvrpStatusInfo->u4ContextId);
        /* Before Start the MRP module , VLAN module should
           be started.Otherwise no use of starting MRP. */
        if (u2VlanIsStarted == VLAN_FALSE)
        {
            *pMvrpStatusInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (MrpPortIsPvrstStartedInContext (pMvrpStatusInfo->u4ContextId) ==
            PVRST_TRUE)
        {
            *pMvrpStatusInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return OSIX_FAILURE;
        }
    }

    if ((pMvrpStatusInfo->i4MvrpEnabledStatus != MRP_ENABLED) &&
        (pMvrpStatusInfo->i4MvrpEnabledStatus != MRP_DISABLED))
    {
        *pMvrpStatusInfo->pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilValidateMrpTableIndices
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to Validate the MRP Table Indices.
 *
 *    INPUT            : u4ContextId - Context ID.
 *                     : u4IfIndex   -Interface Index
 *    OUTPUT           : None 
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpUtilValidateMrpTableIndices (tMvrpPortInfo * pMvrpPortInfo)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    if (MRP_IS_VC_VALID (pMvrpPortInfo->u4ContextId) == OSIX_TRUE)
    {
        if (MRP_IS_MRP_STARTED (pMvrpPortInfo->u4ContextId) == OSIX_TRUE)
        {
            if (MRP_IS_PORT_VALID ((UINT2) (pMvrpPortInfo->u4IfIndex))
                == OSIX_TRUE)
            {
                pMrpContextInfo = MRP_CONTEXT_PTR (pMvrpPortInfo->u4ContextId);
                if (pMrpContextInfo != NULL)
                {
                    if (MRP_GET_PORT_ENTRY (pMrpContextInfo,
                                            pMvrpPortInfo->u4IfIndex) != NULL)
                    {
                        return OSIX_SUCCESS;
                    }
                }
            }
        }
    }
    return OSIX_FAILURE;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilGetNextIndexMrpTable
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the next index of MRP Table.
 *
 *    INPUT            : u4ContextId - Context ID.
 *                     : u4IfIndex   -Interface Index
 *    OUTPUT           : pu4NextContextId -Next Component ID
 *                     : pu4NextIfIndex  -Next Interface Index
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpUtilGetNextIndexMrpTable (tMvrpPortInfo * pMvrpPortInfo)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4ContextId = pMvrpPortInfo->u4ContextId;

    for (; u4ContextId < MRP_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

        if (pMrpContextInfo == NULL)
        {
            /* If Context Info NULL means ,Get Next Context Info */
            continue;
        }
        if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
        {
            continue;
        }

        i4RetVal = MrpCtxtGetNextPortInContext (u4ContextId,
                                                pMvrpPortInfo->u4IfIndex,
                                                pMvrpPortInfo->pu4NextIfIndex);
        if (i4RetVal == OSIX_SUCCESS)
        {
            *(pMvrpPortInfo->pu4NextContextId) = u4ContextId;
            return OSIX_SUCCESS;
        }
        pMvrpPortInfo->u4IfIndex = 0;
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilGetPortMvrpEnabledStatus
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the Port MvrpEnabledStatus.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : pi4PortMvrpEnabledStatus -Port MvrpEnabledStatus
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpUtilGetPortMvrpEnabledStatus (tMvrpPortInfo * pMvrpPortInfo)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (pMvrpPortInfo->u4ContextId);
    if (pMrpContextInfo != NULL)
    {
        pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                            (pMvrpPortInfo->u4IfIndex));
        if (pMrpPortEntry != NULL)
        {
            *(pMvrpPortInfo->pi4PortMvrpEnabledStatus) =
                pMrpPortEntry->u1PortMvrpStatus;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilGetPortMvrpFailedRegist
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the Port MvrpFailedRegistrations.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : pi4PortMvrpFailedRegist -Port 
 *                                    MvrpFailedRegistrations
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpUtilGetPortMvrpFailedRegist (tMvrpPortInfo * pMvrpPortInfo)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (pMvrpPortInfo->u4ContextId);
    if (pMrpContextInfo != NULL)
    {
        pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                            (pMvrpPortInfo->u4IfIndex));
        if (pMrpPortEntry != NULL)
        {
            pMvrpPortInfo->pu8PortMvrpFailedRegist->lsn
                = pMrpPortEntry->u4MvrpRegFailCnt;
            pMvrpPortInfo->pu8PortMvrpFailedRegist->msn = 0;
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilGetPortMvrpLastPduOrigin
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the Port MvrpLastPduOrigin.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : pi4PortMvrpLastPduOrigin -Port
 *                                    MvrpLastPduOrigin
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpUtilGetPortMvrpLastPduOrigin (tMvrpPortInfo * pMvrpPortInfo)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pMrpContextInfo = MRP_CONTEXT_PTR (pMvrpPortInfo->u4ContextId);
    if (pMrpContextInfo != NULL)
    {
        pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                            (pMvrpPortInfo->u4IfIndex));
        if (pMrpPortEntry != NULL)
        {
            MEMCPY (pMvrpPortInfo->pPortMvrpLastPduOrigin,
                    pMrpPortEntry->LastMvrpPduOrigin, MRP_MAC_ADDR_LEN);
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilSetPortMvrpEnabledStatus
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to set the Port MvrpEnabledStatus.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : i4PortMvrpEnabledStatus -Port MvrpEnabledStatus
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpUtilSetPortMvrpEnabledStatus (tMvrpPortInfo * pMvrpPortInfo)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    if (MRP_IS_MRP_STARTED (pMvrpPortInfo->u4ContextId) == OSIX_TRUE)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (pMvrpPortInfo->u4ContextId);
        if (pMrpContextInfo != NULL)
        {
            pMrpPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, (UINT2)
                                                (pMvrpPortInfo->u4IfIndex));
            if (pMrpPortEntry != NULL)
            {
                if (pMrpPortEntry->u1PortMvrpStatus ==
                    pMvrpPortInfo->i4PortMvrpEnabledStatus)
                {
                    return OSIX_SUCCESS;
                }
                switch (pMvrpPortInfo->i4PortMvrpEnabledStatus)
                {
                    case MRP_ENABLED:
                        if (MrpMvrpEnablePort (pMrpContextInfo,
                                               (UINT2) (pMvrpPortInfo->
                                                        u4IfIndex)) ==
                            OSIX_SUCCESS)
                        {
                            return OSIX_SUCCESS;
                        }
                        break;

                    case MRP_DISABLED:
                        if (MrpMvrpDisablePort (pMrpContextInfo,
                                                (UINT2) (pMvrpPortInfo->
                                                         u4IfIndex)) ==
                            OSIX_SUCCESS)
                        {
                            return OSIX_SUCCESS;
                        }

                        break;

                    default:
                        break;
                }
            }
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpUtilTestPortMvrpEnabledStatus
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to test the Port MvrpEnabledStatus.
 *
 *    INPUT            : u4ContextId - Context ID.
 *    OUTPUT           : i4PortMvrpEnabledStatus -Port MvrpEnabledStatus
 *                     : pu4ErrorCode - Error Code
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpUtilTestPortMvrpEnabledStatus (tMvrpPortInfo * pMvrpPortInfo)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    UINT1               u1PortType = 0;
    UINT1               u1TunnelStatus = 0;
    UINT1               u1Status = 0;

    if (MRP_IS_VC_VALID (pMvrpPortInfo->u4ContextId) == OSIX_FALSE)
    {
        *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (pMvrpPortInfo->u4ContextId);

    if (pMrpContextInfo == NULL)
    {
        *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (MRP_IS_MRP_STARTED (pMvrpPortInfo->u4ContextId) == OSIX_FALSE)
    {
        *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MRP_IS_PORT_VALID ((UINT2) (pMvrpPortInfo->u4IfIndex)) == OSIX_FALSE)
    {
        *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pMrpPortEntry =
        MRP_GET_PORT_ENTRY (pMrpContextInfo, pMvrpPortInfo->u4IfIndex);

    if (pMrpPortEntry == NULL)
    {
        *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pMvrpPortInfo->i4PortMvrpEnabledStatus != MRP_ENABLED) &&
        (pMvrpPortInfo->i4PortMvrpEnabledStatus != MRP_DISABLED))
    {
        *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pMvrpPortInfo->i4PortMvrpEnabledStatus == MRP_ENABLED)
    {
        if ((MrpPortL2IwfGetVlanPortType
             (pMrpPortEntry->u4IfIndex, &u1PortType)) == L2IWF_SUCCESS)
        {
            if (u1PortType == VLAN_ACCESS_PORT)
            {
                *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        if (pMrpContextInfo->u4BridgeMode == MRP_CUSTOMER_BRIDGE_MODE)
        {
            MrpPortGetProtoTunelStatusOnPort (pMrpPortEntry->u4IfIndex,
                                              L2_PROTO_MVRP, &u1TunnelStatus);

            if ((u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
                (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }

        if ((MRP_IS_802_1AD_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE) ||
            (MRP_IS_802_1AH_CUSTOMER_PORTS (pMrpPortEntry) == OSIX_TRUE))
        {
            *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (SISP_IS_LOGICAL_PORT (pMrpPortEntry->u4IfIndex) == VCM_TRUE)
    {
        *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        L2IwfGetSispPortCtrlStatus (pMrpPortEntry->u4IfIndex, &u1Status);
        if (u1Status == L2IWF_ENABLED)
        {
            *pMvrpPortInfo->pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrputil.c                      */
/*-----------------------------------------------------------------------*/
