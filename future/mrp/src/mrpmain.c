/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpmain.c,v 1.14 2013/05/06 11:58:46 siva Exp $
 *
 * Description: This file contains MRP task main loop and initialization
 *              routines.
 *********************************************************************/
#ifndef _MRPMAIN_C_
#define _MRPMAIN_C_

#include "mrpinc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMainTask
 *                                                                          
 *    DESCRIPTION      : This is the initialization function for the MRP 
 *                       module, called at bootup time. 
 *
 *    INPUT            : pArg - Pointer to the arguments 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None 
 *                                                                          
 ****************************************************************************/
VOID
MrpMainTask (INT1 *pArg)
{
    UINT4               u4Events = 0;

    UNUSED_PARAM (pArg);

    if (MrpMainTaskInit () == OSIX_FAILURE)
    {
        MrpMainTaskDeInit ();
        /* Indicate the status of initialization to the main routine */
        lrInitComplete (OSIX_FAILURE);
    }

    /* Registering the MIBS with SNMP Agent */
    RegisterFSMRP ();
    RegisterSTD1DMRP ();

    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gMrpGlobalInfo.TaskId, MRP_ALL_EVENTS, OSIX_WAIT,
                         &u4Events) == OSIX_SUCCESS)
        {
            MrpQueProcessEvents (u4Events);
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMainTaskInit
 *                                                                          
 *    DESCRIPTION      : This function creates the task queue, allocates 
 *                       MemPools for task queue messages and creates protocol
 *                       semaphore.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMainTaskInit (VOID)
{
    if (OsixCreateSem (MRP_SEM_NAME, 1, OSIX_DEFAULT_SEM_MODE,
                       &(gMrpGlobalInfo.SemId)) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (OS_RESOURCE_TRC, "MrpMainTaskInit: Semaphore creation "
                     "Failed \r\n");
        return OSIX_FAILURE;
    }

    if (OsixQueCrt (MRP_PDU_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    MAX_MRP_PDU_Q_DEPTH,
                    &(gMrpGlobalInfo.RxPktQId)) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (OS_RESOURCE_TRC, "MrpMainTaskInit: Rx PDU Queue creation "
                     "Failed \r\n");
        return OSIX_FAILURE;
    }

    if (OsixQueCrt (MRP_MSG_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    MAX_MRP_MSG_Q_DEPTH,
                    &(gMrpGlobalInfo.MsgQId)) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (OS_RESOURCE_TRC, "MrpMainTaskInit: Rx CFG Queue creation "
                     "Failed \r\n");
        return OSIX_FAILURE;
    }

    if (MrpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        MRP_GBL_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "MrpMainTaskInit: MRP creation of Mempools Failed \r\n");

        return OSIX_FAILURE;
    }

    MrpAssignMempoolIds ();

    if (MrpMainModuleStart () == OSIX_FAILURE)
    {
        MRP_GBL_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "MrpMainTaskInit: MRP module start Failed \r\n");

        return OSIX_FAILURE;
    }

    if (OsixGetTaskId (SELF, MRP_TASK, &(gMrpGlobalInfo.TaskId))
        == OSIX_FAILURE)
    {
        MRP_GBL_TRC (OS_RESOURCE_TRC, "MrpMainTaskInit: Obtaining Task Id "
                     "Failed \r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMainTaskDeInit
 *                                                                          
 *    DESCRIPTION      : This function deletes the task queue, allocates 
 *                       MemPools for task queue messages and creates protocol
 *                       semaphore.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMainTaskDeInit (VOID)
{

    MrpMainHandleModuleStartFailure ();

    MrpSizingMemDeleteMemPools ();

    OsixQueDel (gMrpGlobalInfo.RxPktQId);
    OsixQueDel (gMrpGlobalInfo.MsgQId);
    OsixSemDel (gMrpGlobalInfo.SemId);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMainHandleModuleStartFailure
 *                                                                          
 *    DESCRIPTION      : This function  deletes the following informations,
 *                       when the Main Module Start Failure happens.
 *                       1) The timer list (TmrDeInit) 
 *                       2) The global port based RB Tree(DelGblPortRbTree) 
 *                       3) All mem pools(MemPoolDeInit).
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpMainHandleModuleStartFailure (VOID)
{
    MrpTmrDeInit ();
    MrpIfDelGblPortRbTree ();
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMainModuleStart
 *                                                                          
 *    DESCRIPTION      : This function allocates memory pools for all tables
 *                       in MRP module. It also initalizes the global
 *                       structure.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMainModuleStart (VOID)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;

    for (u4ContextId = 0; u4ContextId < MRP_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        gMrpGlobalInfo.au1MrpSystemCtrl[u4ContextId] = MRP_SHUTDOWN;
    }

    gMrpGlobalInfo.GlobalTrcOption = MRP_DISABLED;

    /* Update the interval at which CPU relinquish needs to be done. The value
     * should be kept as twice the join time to avoid the leave timer expiry
     * of the peer node.
     */

    gMrpGlobalInfo.MrpRedInfo.u4RelinqInterval = MRP_RELINQUISH_INTERVAL;

    /* Creating the Global RB tree for storing the tMrpPortEntry. */
    if (MrpIfCrtGblPortRbTree () == OSIX_FAILURE)
    {
        MRP_GBL_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "MrpMainModuleStart: Global Port RBTree Failed \r\n");
        return OSIX_FAILURE;
    }

    /* Enrol the application (MVRP/MMRP) specific functions in MRP framework. */
    MrpAppEnrolApplFunctions ();

    gMrpGlobalInfo.pu1PduBuf = MemAllocMemBlk (gMrpGlobalInfo.MrpPduPoolId);

    if (gMrpGlobalInfo.pu1PduBuf == NULL)
    {
        MRP_GBL_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "MrpMainModuleStart: Memory Allocation Failed "
                     "for PDU buffer\r\n");
        return OSIX_FAILURE;
    }

    MEMSET (gMrpGlobalInfo.pu1PduBuf, 0, MRP_MAX_PDU_LEN);

    if (MrpMainInitDefaultContext () == OSIX_FAILURE)
    {
        MRP_GBL_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC, "MrpMainModuleStart: "
                     "Default context initialization Failed \r\n");
        MemReleaseMemBlock (gMrpGlobalInfo.MrpPduPoolId,
                            gMrpGlobalInfo.pu1PduBuf);
        return OSIX_FAILURE;
    }

    if (MrpTmrInit () == OSIX_FAILURE)
    {
        MRP_GBL_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "MrpMainModuleStart: Timer initialization Failed \r\n");
        MemReleaseMemBlock (gMrpGlobalInfo.MrpPduPoolId,
                            gMrpGlobalInfo.pu1PduBuf);
        return OSIX_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (MRP_DEFAULT_CONTEXT_ID);

    if (pMrpContextInfo == NULL)
    {
        MRP_GBL_TRC (INIT_SHUT_TRC | ALL_FAILURE_TRC,
                     "MrpMainModuleStart: Context selection Failed \r\n");
        MemReleaseMemBlock (gMrpGlobalInfo.MrpPduPoolId,
                            gMrpGlobalInfo.pu1PduBuf);
        return OSIX_FAILURE;
    }

    if (MrpMainInit (pMrpContextInfo) == OSIX_FAILURE)
    {
        MRP_TRC ((pMrpContextInfo, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                  "MrpMainModuleStart: MrpMainInit Failed \r\n"));
        MemReleaseMemBlock (gMrpGlobalInfo.MrpPduPoolId,
                            gMrpGlobalInfo.pu1PduBuf);
        return OSIX_FAILURE;
    }

    if (pMrpContextInfo->u4BridgeMode == MRP_INVALID_BRIDGE_MODE)
    {
        MRP_TRC ((pMrpContextInfo, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                  "MrpMainModuleStart: Invalid bridge mode. \r\n"));
        MemReleaseMemBlock (gMrpGlobalInfo.MrpPduPoolId,
                            gMrpGlobalInfo.pu1PduBuf);
        return OSIX_FAILURE;
    }

    /* Register MRP Task with Redundancy Manager RM, to receive messages from
     * peer node.
     */

    MrpRedRegisterWithRM ();
    gMrpGlobalInfo.u1MrpIsInitComplete = OSIX_TRUE;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMainInitDefaultContext
 *                                                                          
 *    DESCRIPTION      : This function initializes the necessary elements 
 *                       of default context.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
INT4
MrpMainInitDefaultContext (VOID)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;

    /* Allocating memory for default Context from Context mempool */
    pMrpContextInfo = (tMrpContextInfo *) MemAllocMemBlk
        (gMrpGlobalInfo.ContextPoolId);

    if (pMrpContextInfo == NULL)
    {
        MRP_GBL_TRC (OS_RESOURCE_TRC, "MrpMainInitDefaultContext: Unable to "
                     "allocate memory for context info. Failed\r\n");
        return OSIX_FAILURE;
    }

    /* storing the context pointer in the global context pointer array */
    MRP_CONTEXT_PTR (MRP_DEFAULT_CONTEXT_ID) = pMrpContextInfo;

    /* Initializing the default Context structure with default values */
    MEMSET (pMrpContextInfo, 0, sizeof (tMrpContextInfo));

    gMrpGlobalInfo.au1MvrpStatus[MRP_DEFAULT_CONTEXT_ID] = MRP_DISABLED;
    gMrpGlobalInfo.au1MmrpStatus[MRP_DEFAULT_CONTEXT_ID] = MRP_DISABLED;
    pMrpContextInfo->u1MvrpTrapEnabled = MRP_DISABLED;
    pMrpContextInfo->u1MmrpTrapEnabled = MRP_DISABLED;
    pMrpContextInfo->u4ContextId = MRP_DEFAULT_CONTEXT_ID;
    gMrpGlobalInfo.u2FirstContextId = MRP_DEFAULT_CONTEXT_ID;
    pMrpContextInfo->u2NextContextId = MRP_INIT_VAL;
    pMrpContextInfo->u2PrevContextId = MRP_INIT_VAL;

    if (MrpPortVcmGetSystemMode (MRP_PROTOCOL_ID) == VCM_MI_MODE)
    {
        MrpPortVcmGetAliasName (MRP_DEFAULT_CONTEXT_ID,
                                pMrpContextInfo->au1ContextName);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMainInit
 *                                                                          
 *    DESCRIPTION      : Starts the MRP Module on a Context  
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMainInit (tMrpContextInfo * pMrpContextInfo)
{
    if (MrpUtilHandleBridgeMode (pMrpContextInfo) != OSIX_SUCCESS)
    {
        MRP_TRC ((pMrpContextInfo, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                  "MrpMainInit: Failed to handle bridge mode updation. \r\n"));
        return OSIX_FAILURE;
    }

    pMrpContextInfo->u4TrcOption = MRP_CRITICAL_TRC;

    MrpTrcGetTraceInputValue (pMrpContextInfo->au1TrcInput,
                              pMrpContextInfo->u4TrcOption);

    MrpIfCreatePorts (pMrpContextInfo);

    /* Enable MVRP */
    if (MrpPortIsPvrstStartedInContext (pMrpContextInfo->u4ContextId) ==
        AST_FALSE)
    {
        /* MVRP should be enable only when PVRST is not started. 
         */

        MrpMvrpInit (pMrpContextInfo);
    }

    /* Enable MMRP */
    MrpMmrpInit (pMrpContextInfo);

    MRP_TRC ((pMrpContextInfo, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
              "MrpMainInit: MRP Module Init is successful.\n"));

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpMainDeInit
 *                                                                          
 *    DESCRIPTION      : Shuts down the MRP Module on a context 
 *
 *    INPUT            : pMrpContextInfo - Pointer to the Context info 
 *                                         structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpMainDeInit (tMrpContextInfo * pMrpContextInfo)
{
    if (MRP_IS_MRP_STARTED (pMrpContextInfo->u4ContextId) == OSIX_FALSE)
    {
        /* MRP is already shutdown. */
        return OSIX_SUCCESS;
    }

    if ((gMrpGlobalInfo.au1MvrpStatus[pMrpContextInfo->u4ContextId]
         == MRP_ENABLED)
        || (gMrpGlobalInfo.au1MmrpStatus[pMrpContextInfo->u4ContextId]
            == MRP_ENABLED))
    {
        /* MRP can be disabled only when all the MRP applications 
         * are disabled */
        return OSIX_FAILURE;
    }

    gMrpGlobalInfo.au1MrpSystemCtrl[pMrpContextInfo->u4ContextId] =
        MRP_SHUTDOWN;

    MrpIfDeletePorts (pMrpContextInfo);

    pMrpContextInfo->u4BridgeMode = MRP_INVALID_BRIDGE_MODE;

    MRP_TRC ((pMrpContextInfo, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
              "MrpMainDeInit: MRP Module De-Init is successful.\n"));

    return OSIX_SUCCESS;
}

VOID
MrpAssignMempoolIds (VOID)
{

    /*tMrpQMsg */
    gMrpGlobalInfo.QMsgPoolId = MRPMemPoolIds[MAX_MRP_MSG_Q_DEPTH_SIZING_ID];

    /*tMrpBulkQMsg */
    gMrpGlobalInfo.QBulkMsgPoolId = MRPMemPoolIds[MAX_MRP_BULK_MSG_SIZING_ID];

    /*tMrpRxPduQMsg */
    gMrpGlobalInfo.PduQMsgPoolId = MRPMemPoolIds[MAX_MRP_PDU_Q_DEPTH_SIZING_ID];

    /*tMrpContextInfo */
    gMrpGlobalInfo.ContextPoolId = MRPMemPoolIds[MAX_MRP_CONTEXTS_SIZING_ID];

    /*tMrpPortEntry */
    gMrpGlobalInfo.PortPoolId
        = MRPMemPoolIds[MAX_MRP_PORT_ENTRIES_IN_SYSTEM_SIZING_ID];

    /*tMrpAppPortEntry */
    gMrpGlobalInfo.AppPortPoolId
        = MRPMemPoolIds[MAX_MRP_MEM_APP_PORT_ENTRIES_IN_SYSTEM_SIZING_ID];

    /*tMrpStatsEntry */
    gMrpGlobalInfo.StatisticsPoolId
        = MRPMemPoolIds[MAX_MRP_STATS_ENTRIES_IN_SYSTEM_SIZING_ID];

    /*tMrpMapEntry */
    gMrpGlobalInfo.MapPoolId
        = MRPMemPoolIds[MAX_MRP_MAP_ENTRIES_IN_SYS_SIZING_ID];

    /*tMrpAttrEntry */
    gMrpGlobalInfo.AttrPoolId
        = MRPMemPoolIds[MAX_MRP_ATTR_ENTRIES_IN_SYS_SIZING_ID];

    /*tMrpMapPortEntry */
    gMrpGlobalInfo.MapPortPoolId
        = MRPMemPoolIds[MAX_MRP_MAP_PORT_ENTRIES_SIZING_ID];

    /*UINT1[MRP_MAX_PDU_LEN] */
    gMrpGlobalInfo.MrpPduPoolId
        = MRPMemPoolIds[MAX_MRP_PDU_BUFFER_COUNT_SIZING_ID];

    /*tMrpVidMapQMsg */
    gMrpGlobalInfo.VlanInstMapPoolId
        = MRPMemPoolIds[MAX_MRP_BULK_VID_MAP_QMSG_SIZING_ID];

    /*tMrpMvrpMapTable */
    gMrpGlobalInfo.MrpMvrpMapPoolId
        = MRPMemPoolIds[MAX_MRP_MVRP_MAPS_BLKS_IN_SYS_SIZING_ID];

    /*tMrpMmrpMapTable */
    gMrpGlobalInfo.MrpMmrpMapPoolId
        = MRPMemPoolIds[MAX_MRP_MMRP_MAPS_BLKS_IN_SYS_SIZING_ID];

    /*tMrpMvrpAttrArrayEntry */
    gMrpGlobalInfo.MapMvrpAttrArrayPoolId
        = MRPMemPoolIds[MAX_MRP_MVRP_MAPS_IN_SYS_SIZING_ID];

    /*tMrpMmrpAttrArrayEntry */
    gMrpGlobalInfo.MapMmrpAttrArrayPoolId
        = MRPMemPoolIds[MAX_MRP_MMRP_MAPS_IN_SYS_SIZING_ID];

    /*tMrpMvrpPortAttrInfoList */
    gMrpGlobalInfo.MvrpAttrInfoListPoolId
        = MRPMemPoolIds[MAX_MRP_MVRP_MAP_PORTS_IN_SYS_SIZING_ID];

    /*tMrpMmrpPortAttrInfoList */
    gMrpGlobalInfo.MmrpAttrInfoListPoolId
        = MRPMemPoolIds[MAX_MRP_MMRP_MAP_PORTS_IN_SYS_SIZING_ID];

    /*tMrpPortMvrpPeerMacTable */
    gMrpGlobalInfo.MvrpAppPortPeerMacPoolId
        = MRPMemPoolIds[MAX_MRP_MVRP_PEER_MAC_TABLE_SIZING_ID];

    /*tMrpPortMmrpPeerMacTable */
    gMrpGlobalInfo.MmrpAppPortPeerMacPoolId
        = MRPMemPoolIds[MAX_MRP_MMRP_PEER_MAC_TABLE_SIZING_ID];

    /*tMrpMvrpLvBitListTbl */
    gMrpGlobalInfo.MvrpLvBitListPoolId
        = MRPMemPoolIds[MAX_MRP_MVRP_LV_BIT_LIST_COUNT_SIZING_ID];

    /*tMrpMmrpLvBitListTbl */
    gMrpGlobalInfo.MmrpLvBitListPoolId
        = MRPMemPoolIds[MAX_MRP_MMRP_LV_BIT_LIST_COUNT_SIZING_ID];

    /*tMrpPortMvrpAttrList */
    gMrpGlobalInfo.MvrpPortAttrListPoolId
        = MRPMemPoolIds[MAX_MRP_PORT_ATTR_TABLE_SIZING_ID];

    /*tMrpMmrpAttrIndexList */
    gMrpGlobalInfo.AppMmrpAttrIndxPoolId
        = MRPMemPoolIds[MAX_MRP_MMPR_APP_ATTR_ARRAY_BLKS_SIZING_ID];

    return;
}

#endif/*_MRPMAIN_C_*/

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpmain.c                      */
/*-----------------------------------------------------------------------*/
