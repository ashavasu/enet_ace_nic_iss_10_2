/*****************************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: mrpmad.c,v 1.9 2013/06/25 12:08:37 siva Exp $
*
* Description: This file contains the functions called by the MAD to give 
*              indications to other ports in the MAP context.
******************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *
 *  FUNCTION NAME   : MrpMadMapAttributeJoinReq               
 *
 *    DESCRIPTIO      : This function is called by MAD to indicate the 
 *                      Join request to other ports in the MAP context.
 *
 *    INPUT            : pAttr      - pointer to Mrp Attribute structure
 *                       pPortEntry - Pointer to the MAP Port entry
 *                       pIfMsg     - Pointer to tMrpIfMsg structure
 *                       pMapEntry  - Pointer to the MAP structure
 *                       SendMsgInd - MRP_SEND_NEW/MRP_SEND_JOIN
 *
 *    OUTPUT           : None
 *
 *  RETURNS         : None                                    
 *
 ****************************************************************************/
VOID
MrpMadMapAttributeJoinReq (tMrpAttr * pAttr,
                           tMrpMapPortEntry * pPortEntry, tMrpIfMsg * pIfMsg,
                           tMrpMapEntry * pMapEntry, eMrpSendMsgInd SendMsgInd)
{
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tVlanId             VlanId = 0;
    UINT4               u4FdbId = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;
    UINT1               u1RegAdmCtrl = 0;

    pAppEntry = pIfMsg->pAppEntry;

    pAppPortEntry = MRP_GET_APP_PORT_FRM_MAP_PORT (pPortEntry,
                                                   pPortEntry->u2Port);
    if (pAppPortEntry == NULL)
    {
        MRP_GBL_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "MrpMadMapAttributeJoinReq:" "App entry not present \n");
        return;
    }
    pMrpPortEntry = pAppPortEntry->pMrpPortEntry;

    if (pMrpPortEntry == NULL)
    {
        MRP_GBL_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "MrpMadMapAttributeJoinReq:" "Port entry not present \n");
        return;
    }

    if (pAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        if (pMrpPortEntry->u1PortMvrpStatus == MRP_DISABLED)
        {
            /* Application is disabled on this port. Ignore the JOIN now. */
            return;
        }
    }
    else
    {
        if (pMrpPortEntry->u1PortMmrpStatus == MRP_DISABLED)
        {
            /* Application is disabled on this port. Ignore the JOIN now. */
            return;
        }
    }

    if (pAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        u1PortState = MrpUtilGetVlanPortState (pPortEntry);
    }
    else
    {
        u1PortState = MrpUtilGetVlanPortState (pPortEntry);
    }

    if (pAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        if ((pMrpPortEntry->au1ApplAdminCtrl[MRP_MVRP_APP_ID][MRP_VID_ATTR_TYPE]
             != MRP_ACTIVE_APPLICANT)
            && (u1PortState != AST_PORT_STATE_FORWARDING))
        {
            /* When the Applicant Admin Control is set as Active, MRPDUs need to
             * be sent on blocked ports. This feature is applicable only for
             * MVRP.
             */
            return;
        }
    }
    else
    {
        if (u1PortState != AST_PORT_STATE_FORWARDING)
        {
            return;
        }
    }
    pAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMapEntry);

    if (pAttrEntry == NULL)
    {
        pAttrEntry = MrpMapDSCreateMrpMapAttrEntry (pMapEntry, pAttr);

        if (pAttrEntry == NULL)
        {
            MRP_TRC ((pMrpPortEntry->pMrpContextInfo,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "MrpMadMapAttributeJoinReq: "
                      "No Free Attribute Entry \n"));
            return;

        }
    }

    if (MrpMapDSAddAttrToMapPortEntry (pPortEntry, pAttrEntry) != OSIX_SUCCESS)
    {
        MRP_TRC ((pMrpPortEntry->pMrpContextInfo,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "MrpMadMapAttributeJoinReq:"
                  "Adding attribute to MrpPortEntry Failed \n"));
        MrpMapDSDelAttrEntryFromMapEntry (pMapEntry, pAttrEntry);
        return;
    }

    if (SendMsgInd == MRP_SEND_NEW)
    {
        /* Only in case of MVRP, VLAN needs to be informed about FDB 
         * flushing on receiving "new".
         */
        u1RegAdmCtrl = MRP_GET_ATTR_REG_ADMIN_CTRL
            (pPortEntry->pu1AttrInfoList[pAttrEntry->u2AttrIndex]);

        if ((MRP_REG_NORMAL == u1RegAdmCtrl) &&
            (MRP_MVRP_APP_ID == pAppEntry->u1AppId))
        {
            u4ContextId = pAppEntry->pMrpContextInfo->u4ContextId;
            MEMCPY (&VlanId, pAttr->au1AttrVal, MRP_VLAN_ID_LEN);
            VlanId = OSIX_NTOHS (VlanId);
            u4FdbId = MrpPortL2IwfGetVlanFdbId (u4ContextId, VlanId);
            MrpPortVlanFlushFdbEntries (pMrpPortEntry->u4IfIndex, u4FdbId);
        }

        MrpSmApplicantSem (pPortEntry, pAttrEntry->u2AttrIndex,
                           MRP_APP_REQ_NEW);
    }
    else
    {
        MrpSmApplicantSem (pPortEntry, pAttrEntry->u2AttrIndex,
                           MRP_APP_REQ_JOIN);
    }
}

/****************************************************************************
 *
 *  FUNCTION NAME   : MrpMadMapAttributeLeaveReq              
 *
 *  DESCRIPTION     : This function is called by MAD to indicate the 
 *                    Leave request to other ports in the MAP context.
 *
 *    INPUT            : pAttr      - pointer to Mrp Attribute structure
 *                       pPortEntry - Pointer to the MAP Port entry
 *                       pIfMsg     - Pointer to tMrpIfMsg structure
 *                       pMapEntry  - Pointer to the MAP structure
 *
 *    OUTPUT           : None
 *
 *  RETURNS         : None                                    
 *
 ****************************************************************************/
VOID
MrpMadMapAttributeLeaveReq (tMrpAttr * pAttr,
                            tMrpMapPortEntry * pPortEntry, tMrpIfMsg * pIfMsg,
                            tMrpMapEntry * pMapEntry)
{
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpPortEntry      *pMrpPortEntry = NULL;

    pAppEntry = pIfMsg->pAppEntry;

    pAppPortEntry =
        MRP_GET_APP_PORT_FRM_MAP_PORT (pPortEntry, pPortEntry->u2Port);

    if (pAppPortEntry == NULL)
    {
        MRP_GBL_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "MrpMadMapAttributeLeaveReq:" "Port entry not present \n");
        return;
    }

    pMrpPortEntry = pAppPortEntry->pMrpPortEntry;

    if (pMrpPortEntry == NULL)
    {
        MRP_GBL_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                     "MrpMadMapAttributeLeaveReq:" "Port entry not present \n");
        return;
    }

    if (pAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        if (pMrpPortEntry->u1PortMvrpStatus == MRP_DISABLED)
        {
            /* MRP Application disabled on this port...Ignore LEAVE now... */
            return;
        }
    }
    else
    {
        if (pMrpPortEntry->u1PortMmrpStatus == MRP_DISABLED)
        {
            /* MRP Application disabled on this port...Ignore LEAVE now... */
            return;
        }
    }

    if (pPortEntry->u1PortState == AST_PORT_STATE_FORWARDING)
    {
        pAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMapEntry);

        if (pAttrEntry == NULL)
        {
            return;
        }

        MrpSmApplicantSem (pPortEntry, pAttrEntry->u2AttrIndex,
                           MRP_APP_REQ_LEAVE);
        MrpAttrCheckAndDelAttrEntry (pPortEntry, pAttrEntry);
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpmad.c                       */
/*-----------------------------------------------------------------------*/
