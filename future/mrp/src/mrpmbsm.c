/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpmbsm.c,v 1.7 2013/05/06 11:58:46 siva Exp $
 *
 * Description: This file contains the functions related to MBSM.
 ******************************************************************************/

#include "mrpinc.h"

/*****************************************************************************
*                                                                           
*    Function Name       : MrpMbsmProcessUpdate                           
*                                                                           
*    Description         : This function is an API for mbsm module to       
*                          update the MRP module a new LC                 
*                          is inserted or removed                           
*                                                                           
*    Input(s)            : pPortInfo - Inserted port list                   
*                          pSlotInfo - Information about inserted slot      
*                          i4Event   - Event type.                          
*                                                                           
*    Output(s)           : None                                             
*                                                                           
*                                                                           
*    Returns            : MBSM_SUCCESS or MBSM_FAILURE                     
*
*****************************************************************************/
INT4
MrpMbsmProcessUpdate (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo,
                      INT4 i4Event)
{
    INT4                i4RetVal = MBSM_FAILURE;

    switch (i4Event)
    {
        case MBSM_MSG_CARD_INSERT:

            i4RetVal = MrpMbsmUpdateOnCardInsertion (pPortInfo, pSlotInfo);
            break;

        case MBSM_MSG_CARD_REMOVE:

            i4RetVal = MBSM_SUCCESS;
            break;

        default:
            i4RetVal = MBSM_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************
*                                                                           
*    Function Name       : MrpMbsmUpdateOnCardInsertion                     
*                                                                           
*    Description         : This function checks the status of MVRP and      
*                            MMRP and update the status in Hw               
*                                                                           
*    Input(s)            : pPortInfo - Inserted port list                   
*                          pSlotInfo - Information about inserted slot      
*                                                                           
*    Output(s)           : None                                             
*                                                                           
*                                                                           
*    Returns            : MBSM_SUCCESS on success                           
*                         MBSM_FAILURE on failure                           
*                                                                           
*****************************************************************************/
INT4
MrpMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
                              tMbsmSlotInfo * pSlotInfo)
{

    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpHwInfo          MrpHwInfo;
    UINT4               u4ContextId = MRP_INIT_VAL;

    MEMSET (&MrpHwInfo, 0, sizeof (tMrpHwInfo));
    MrpHwInfo.u4Status = MRP_ENABLED;
    MrpHwInfo.pSlotInfo = pSlotInfo;

    UNUSED_PARAM (pPortInfo);

    for (u4ContextId = 1; u4ContextId < MRP_SIZING_CONTEXT_COUNT; u4ContextId++)
    {
        pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);

        if (pMrpContextInfo == NULL)
        {
            continue;
        }

        if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
        {
            MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_MRP,
                           "Card Insertion: MRP is not started"
                           " in current context id %d \n", u4ContextId);
            continue;
        }

        MrpHwInfo.u4ContextId = u4ContextId - 1;

        if (MrpMvrpIsEnabled (u4ContextId) == OSIX_TRUE)
        {
            if (0 == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
            {
                if (FsMiMrpHwSetMvrpStatus (&MrpHwInfo) == FNP_FAILURE)
                {
                    MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_MRP,
                                   "Card Insertion:  Enabling MVRP Failed for"
                                   " context id %d \n", u4ContextId);
                    return MBSM_FAILURE;
                }
            }
        }

        if (MrpMmrpIsEnabled (u4ContextId) == OSIX_TRUE)
        {
            if (0 == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
            {
                if (FsMiMrpHwSetMmrpStatus (&MrpHwInfo) == FNP_FAILURE)
                {
                    MBSM_DBG_ARG1 (MBSM_PROTO_TRC, MBSM_MRP,
                                   "Card Insertion: Enabling MMRP Failed for"
                                   " context id %d \n", u4ContextId);
                    return MBSM_FAILURE;
                }
            }
        }
    }

    return MBSM_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpmbsm.c                      */
/*-----------------------------------------------------------------------*/
