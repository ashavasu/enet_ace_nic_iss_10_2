/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpapi.c,v 1.17 2013/09/28 07:57:37 siva Exp $
 *
 * DESCRIPTION: This file contains the APIs exported by MRP module.
 ******************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiEnqueueRxPkt 
 *
 *    DESCRIPTION      : This function receives the incoming MRP PDU  from
 *                       the CFA Module and enqueues it to MRP PDU Queue for 
 *                       processing. 
 *
 *    INPUT            : pFrame - pointer to the received  MVRP/MMRP 
 *                                frame buffer (CRU buff)
 *                       u4IfIndex - Src port on which the frame is 
 *                                   received.
 *                       u2MapId - MAP id. value will be as follows:
 *                                 0 in case of MVRP pkt.
 *                                 VlanId in case of MMRP pkt.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
MrpApiEnqueueRxPkt (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4IfIndex,
                    UINT2 u2MapId)
{
    UINT2               u2LocalPortId = 0;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;

    /* Get the Context Id and the Local Port Id by the IfIndex */
    if (MrpPortGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiEnqueueRxPkt: Failed to Get Context Id\r\n");
        CRU_BUF_Release_MsgBufChain (pFrame, FALSE);

        return OSIX_FAILURE;
    }

    if (MrpUtilPostRcvdPduToTask (pFrame, u4ContextId, u2LocalPortId,
                                  u2MapId) != OSIX_SUCCESS)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiEnqueueRxPkt: MrpUtilPostRcvdPduToTask "
                     "returned Failure\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiNotifyPortOperP2PChg
 *                                                                          
 *    DESCRIPTION      : This function is called by L2IWF to post a message to 
 *                       MRP task's message queue in order to notify the change
 *                       in the operational point- to-point status of the port.
 *
 *    INPUT            : u4IfIndex      - Interface Index
 *                       bOperP2PStatus - Boolean value indicating whether
 *                                        the port is a Point To Point Port
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
MrpApiNotifyPortOperP2PChg (UINT4 u4IfIndex, BOOL1 bOperP2PStatus)
{
    tMrpQMsg           *pQMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    /* Get the Context Id and the Local Port Id from the IfIndex */
    if (MrpPortGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyPortOperP2PChg: Failed to Get Context Id\n");
        return OSIX_FAILURE;
    }

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyPortOperP2PChg: MRP is not started in "
                     "current context\r\n");
        return OSIX_FAILURE;
    }

    pQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pQMsg)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_CRITICAL_TRC,
                  "MrpApiNotifyPortOperP2PChg: Q Msg memory allocation "
                  "failed\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pQMsg, 0, sizeof (tMrpQMsg));

    pQMsg->u2MsgType = MRP_MSG;
    pQMsg->unMrpMsg.MrpMsg.u2MsgType = MRP_PORT_OPER_P2P_MSG;
    pQMsg->unMrpMsg.MrpMsg.u2Port = u2LocalPortId;
    pQMsg->u4IfIndex = u4IfIndex;
    pQMsg->u4ContextId = u4ContextId;

    if (RST_TRUE == bOperP2PStatus)
    {
        pQMsg->u1MsgInfo = OSIX_TRUE;
    }
    else
    {
        pQMsg->u1MsgInfo = OSIX_FALSE;
    }

    i4RetVal = MrpQueEnqMsg (pQMsg);

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiNotifyPortRoleChange
 *                                                                          
 *    DESCRIPTION      : This function is called by STP to post a message
 *                       to MRP task's message queue to notify the change in
 *                       the port role.        
 *
 *    INPUT            : u4IfIndex - Interface Index        
 *                       u2MapID   - CIST or MST Instance Identifier of the port
 *                                   whose port role has changed
 *                       u1OldRole - Old Port Role                         
 *                       u1NewRole - New Port Role                        
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
MrpApiNotifyPortRoleChange (UINT4 u4IfIndex, UINT2 u2MapID, UINT1 u1OldRole,
                            UINT1 u1NewRole)
{
    tMrpQMsg           *pQMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    UINT1               u1IndType = 0;

    /* Get the Context Id and the Local Port Id from the IfIndex */
    if (MrpPortGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyPortRoleChange: Failed to Get Context Id\n");
        return OSIX_FAILURE;
    }

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyPortRoleChange: MRP is not started in "
                     "current context\r\n");

        return OSIX_FAILURE;
    }

    if (((AST_PORT_ROLE_ALTERNATE == u1OldRole) ||
         (AST_PORT_ROLE_ROOT == u1OldRole)) &&
        (AST_PORT_ROLE_DESIGNATED == u1NewRole))
    {
        u1IndType = MRP_FLUSH_IND;
    }
    else if ((AST_PORT_ROLE_DESIGNATED == u1OldRole) &&
             ((AST_PORT_ROLE_ALTERNATE == u1NewRole) ||
              (AST_PORT_ROLE_ROOT == u1NewRole)))
    {
        u1IndType = MRP_REDECLARE_IND;
    }
    else
    {
        return OSIX_SUCCESS;
    }

    pQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pQMsg)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_CRITICAL_TRC,
                  "MrpApiNotifyPortRoleChange: Q Msg memory allocation "
                  "failed\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pQMsg, 0, sizeof (tMrpQMsg));

    pQMsg->u2MsgType = MRP_MSG;
    pQMsg->unMrpMsg.MrpMsg.u2MsgType = MRP_PORT_ROLE_CHG_MSG;
    pQMsg->unMrpMsg.MrpMsg.u2Port = u2LocalPortId;
    pQMsg->unMrpMsg.MrpMsg.u2MapId = u2MapID;
    pQMsg->u4IfIndex = u4IfIndex;
    pQMsg->u4ContextId = u4ContextId;

    pQMsg->u1MsgInfo = u1IndType;

    i4RetVal = MrpQueEnqMsg (pQMsg);

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiNotifyTcDetectedTmrState
 *                                                                          
 *    DESCRIPTION      : This function is called by STP module to post a 
 *                       message to MRP task's message queue 
 *                       whenever the tcDetected Timer is started/
 *                       stopped/expired.
 *
 *    INPUT            : u4IfIndex  - Interface Index of the port on which
 *                                    tcDetected Timer is running
 *                       u2MapID    - MST Instance ID to which this port 
 *                                    belongs (will 0 in case of RSTP)
 *                       u1TmrState - Timer status (OSIX_TRUE - Timer running
 *                                                  OSIX_FALSE - Timer not
 *                                                  running)
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
MrpApiNotifyTcDetectedTmrState (UINT4 u4IfIndex, UINT2 u2MapID,
                                UINT1 u1TmrState)
{
    tMrpQMsg           *pQMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    /* Get the Context Id and the Local Port Id from the IfIndex */
    if (MrpPortGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyTcDetectedTmrState: Failed to Get Context"
                     " Id\n");
        return OSIX_FAILURE;
    }

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyTcDetectedTmrState: MRP is not started in "
                     "current context\r\n");

        return OSIX_FAILURE;
    }

    pQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pQMsg)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_CRITICAL_TRC,
                  "MrpApiNotifyTcDetectedTmrState: Q Msg memory allocation "
                  "failed\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pQMsg, 0, sizeof (tMrpQMsg));
    pQMsg->u2MsgType = MRP_MSG;
    pQMsg->unMrpMsg.MrpMsg.u2MsgType = MRP_TCDETECTED_TMR_STATUS;
    pQMsg->unMrpMsg.MrpMsg.u2Port = u2LocalPortId;
    pQMsg->unMrpMsg.MrpMsg.u2MapId = u2MapID;
    pQMsg->u4IfIndex = u4IfIndex;
    pQMsg->u4ContextId = u4ContextId;
    pQMsg->u1MsgInfo = u1TmrState;

    i4RetVal = MrpQueEnqMsg (pQMsg);

    return i4RetVal;
}

/****************************************************************************
 *
 * FUNCTION NAME      : MrpApiFillBulkMessage                                
 *                                                                           
 * DESCRIPTION        : This function is used to fill the Vlan / Mcast /     
 *                      Ucast / Def Group Info in the Message. This funtion  
 *                      is called whenever a Mvrp and Mmrp is enabled.       
 *                      This function also post the message if the max no    
 *                      of message is filled.                                
 *                                                                           
 *    INPUT              : ppMrpQMsg - Double Pointer to store MrpQMsg          
 *                      u1MsgType  - Type of Message - Vlan / Mcast /        
 *                                   Ucast / Def Grp                         
 *                      VlanId     - VlanId                                  
 *                      u2Port     - LocalPort Identifier                    
 *                      Ports      - Added Portlist                          
 *                      pu1MacAddr - MacAddres                               
 *                                                                           
 *    OUTPUT             : ppMrpQMsg - pointer to Allocated Message             
 *                      pi4Count   - No of Message Filled                    
 *                                                                           
 *    RETURNS            : OSIX_SUCCESS / OSIX_FAILURE                           
 *                                                                           
 ****************************************************************************/
INT4
MrpApiFillBulkMessage (UINT4 u4ContextId, tMrpQMsg ** ppMrpQMsg,
                       UINT1 u1MsgType, tVlanId VlanId, UINT2 u2Port,
                       tLocalPortList Ports, UINT1 *pu1MacAddr)
{
    tMrpBulkMsg        *pMrpBulkMsg = NULL;
    INT4                i4MsgIndex = 0;
    BOOL1               bInvalidMsgType = OSIX_FALSE;

    /*  This allocation is done only once for each message post to MRP. 
     *  As the function is called in loop the check here is essential */

    if (*ppMrpQMsg == NULL)
    {
        if ((*ppMrpQMsg =
             (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QBulkMsgPoolId)) ==
            NULL)
        {
            MRP_GBL_TRC (MRP_CRITICAL_TRC, "MrpApiFillBulkMessage:"
                         "Q Msg memory allocation failed\n");

            return OSIX_FAILURE;

        }

        MEMSET (*ppMrpQMsg, 0, sizeof (tMrpBulkQMsg));
    }

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
    i4MsgIndex = (*ppMrpQMsg)->i4Count;
    (*ppMrpQMsg)->u4ContextId = u4ContextId;

    pMrpBulkMsg = &((*ppMrpQMsg)->unMrpMsg.MrpBulkMsg[i4MsgIndex]);

    pMrpBulkMsg->VlanId = VlanId;

    switch (u1MsgType)
    {
        case VLAN_PROP_MAC_INFO_MSG:
            pMrpBulkMsg->u2MsgType = (UINT2) MRP_PROP_MAC_INFO_MSG;
            break;

        case VLAN_SET_MCAST_FORBID_MSG:
            pMrpBulkMsg->u2MsgType = (UINT2) MRP_SET_MCAST_FORBID_MSG;
            break;

        case VLAN_PROP_FWDALL_INFO_MSG:
            pMrpBulkMsg->u2MsgType = (UINT2) MRP_PROP_FWDALL_INFO_MSG;
            break;

        case VLAN_SET_FWDALL_FORBID_MSG:
            pMrpBulkMsg->u2MsgType = (UINT2) MRP_SET_FWDALL_FORBID_MSG;
            break;

        case VLAN_PROP_FWDUNREG_INFO_MSG:
            pMrpBulkMsg->u2MsgType = (UINT2) MRP_PROP_FWDUNREG_INFO_MSG;
            break;

        case VLAN_SET_FWDUNREG_FORBID_MSG:
            pMrpBulkMsg->u2MsgType = (UINT2) MRP_SET_FWDUNREG_FORBID_MSG;
            break;

        case VLAN_PROP_VLAN_INFO_MSG:
            pMrpBulkMsg->u2MsgType = (UINT2) MRP_PROP_VLAN_INFO_MSG;
            break;

        case VLAN_SET_VLAN_FORBID_MSG:
            pMrpBulkMsg->u2MsgType = (UINT2) MRP_SET_VLAN_FORBID_MSG;
            break;

        default:
            bInvalidMsgType = OSIX_TRUE;
            break;
    }

    if (bInvalidMsgType == OSIX_TRUE)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC, "MrpApiFillBulkMessage: Received "
                     "Invalid Message Type\n");
        MemReleaseMemBlock (gMrpGlobalInfo.QBulkMsgPoolId,
                            (UINT1 *) *ppMrpQMsg);
        return OSIX_FAILURE;
    }

    pMrpBulkMsg->u2Port = u2Port;

    MEMCPY (pMrpBulkMsg->Ports, Ports, sizeof (tLocalPortList));

    if (pu1MacAddr != NULL)
    {
        MEMCPY (pMrpBulkMsg->MacAddr, pu1MacAddr, ETHERNET_ADDR_SIZE);
    }

    (*ppMrpQMsg)->i4Count++;

    if (((*ppMrpQMsg)->i4Count) == VLAN_NO_OF_MSG_PER_POST)
    {
        MrpApiPostBulkCfgMessage (*ppMrpQMsg);
        *ppMrpQMsg = NULL;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           
 *    FUNCTION NAME       : MrpApiPostBulkCfgMessage                        
 *                                                                           
 *    DESCRIPTION         : Posts the message to Mrp Config Q.             
 *                                                                           
 *    INPUT               : pMrpQMsg -  Pointer to MrpQMsg                
 *                                                                           
 *    OUTPUT              : None                                             
 *                                                                           
 *    RETURNS            : OSIX_SUCCESS / OSIX_FAILURE                     
 *                                                                           
 *****************************************************************************/
INT4
MrpApiPostBulkCfgMessage (tMrpQMsg * pMrpQMsg)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (MrpUtilIsMrpStarted (pMrpQMsg->u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiPostBulkCfgMessage: MRP is not started in "
                     "current context\r\n");

        MemReleaseMemBlock (gMrpGlobalInfo.QBulkMsgPoolId, (UINT1 *) pMrpQMsg);

        return i4RetVal;
    }

    pMrpQMsg->u2MsgType = MRP_BULK_MSG;

    i4RetVal = MrpQueEnqMsg (pMrpQMsg);
    return i4RetVal;
}

/*****************************************************************************
 * 
 * FUNCTION NAME       : MrpApiGetMvrpAddr
 *   
 * DESCRIPTION         : This function is used to obtain the Mvrp address
 *                       of the given context. This function is invoked only
 *                       after taking MRP lock, hence no lock is taken here.
 * INPUT               : u4ContextId
 *        
 * OUTPUT              : MacAddr - MVRP address.
 *          
 * RETURNS            : OSIX_SUCCESS / OSIX_FAILURE
 *            
 ******************************************************************************/
INT4
MrpApiGetMvrpAddr (UINT4 u4ContextId, tMacAddr * MacAddr)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    pMrpContextInfo = MrpCtxtGetContextInfo (u4ContextId);
    if (pMrpContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (MacAddr, pMrpContextInfo->MvrpAddr, sizeof (tMacAddr));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * FUNCTION NAME       : MrpApiGetMmrpAddr
 *    
 * DESCRIPTION         : This function is used to obtain the global Mmrp
 *                       address. This function is invoked only
 *                       after taking MRP lock, hence no lock is taken here.
 * INPUT               : MacAddr
 *        
 * OUTPUT              : None
 *          
 * RETURNS            : OSIX_SUCCESS / OSIX_FAILURE
 *            
 ******************************************************************************/
INT4
MrpApiGetMmrpAddr (tMacAddr * MacAddr)
{

    if (MacAddr == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (MacAddr, gMmrpAddr, sizeof (tMacAddr));
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *  FUNCTION NAME   : MrpApiIsMrpStarted
 *
 *   DESCRIPTION     : RETURNS the MRP Module system Status           
 *
 *   INPUT           : u4ContextId - Virtual Context Identifier
 *
 *   OUTPUT          : None 
 *
 *  RETURNS         : OSIX_TRUE /OSIX_FALSE                     
 *
 ****************************************************************************/
INT4
MrpApiIsMrpStarted (UINT4 u4ContextId)
{
    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
    return ((gMrpGlobalInfo.au1MrpSystemCtrl[u4ContextId] == MRP_START) ?
            OSIX_TRUE : OSIX_FALSE);
}

/*****************************************************************************
 *
 *  FUNCTION NAME   : MrpApiIsMvrpEnabled        
 *
 *   DESCRIPTION     : RETURNS the MVRP Module Status Application
 *
 *   INPUT           : u4ContextId - Virtual Context Identifier
 *
 *   OUTPUT          : None
 *
 *  RETURNS         : OSIX_TRUE /OSIX_FALSE                   
 *
 ******************************************************************************/

INT4
MrpApiIsMvrpEnabled (UINT4 u4ContextId)
{
    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
    return (MrpMvrpIsEnabled (u4ContextId));
}

/*****************************************************************************
 *
 *  FUNCTION NAME   : MrpApiNotifyVlanInfo        
 *
 *   DESCRIPTION    : This function is called from VLAN module in the following
 *                    scenarios:
 *                    - whenever a static VLAN entry is added or deleted
 *                    - whenever forbidden ports are set for the specified VLAN 
 *                      Identifier 
 *                    - whenever a static MAC entry is added or deleted
 *                    - whenever the forbidden ports are set for the given 
 *                      multicast entry 
 *                    - to propagate the default Group Service Requirement 
 *                      Information to all ports during initialization
 *                    - whenever forbidden ports are set for the given Group
 *                      Service Requiremenent
 *                    - whenever static member ports are added/ deleted for the
 *                      given VLAN identifier.
 *
 *   INPUT          : pVlanInfo - Pointer to MrpVLanInfo structure 
 *
 *   OUTPUT         : None
 *
 *  RETURNS         : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ******************************************************************************/
INT4
MrpApiNotifyVlanInfo (tMrpVlanInfo * pVlanInfo)
{
    UINT1              *pAddedPorts = NULL;
    UINT1              *pDeletedPorts = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    pAddedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pAddedPorts == NULL)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpApiNotifyVlanInfo : Error in allocating memory "
                     "for pAddedPorts\r\n");
        return OSIX_FAILURE;
    }
    pDeletedPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pDeletedPorts == NULL)
    {
        UtilPlstReleaseLocalPortList (pAddedPorts);
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpApiNotifyVlanInfo : Error in allocating memory "
                     "for pDeletedPorts\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pAddedPorts, 0, sizeof (tLocalPortList));
    MEMSET (pDeletedPorts, 0, sizeof (tLocalPortList));

    /* Convert the physical port list to local port list */
    if (pVlanInfo->pAddedPorts != NULL)
    {
        L2IwfConvToLocalPortList (*(pVlanInfo->pAddedPorts), pAddedPorts);
    }

    if (pVlanInfo->pDeletedPorts != NULL)
    {
        L2IwfConvToLocalPortList (*(pVlanInfo->pDeletedPorts), pDeletedPorts);
    }

    switch (pVlanInfo->u1MsgType)
    {
        case VLAN_PROP_VLAN_INFO_MSG:
            MRP_MVRP_PROP_VLAN_INFO (pVlanInfo->u4ContextId,
                                     pVlanInfo->VlanId,
                                     pAddedPorts, pDeletedPorts);
            break;

        case VLAN_SET_VLAN_FORBID_MSG:
            MRP_MVRP_SET_VLAN_FORBID_PORTS (pVlanInfo->u4ContextId,
                                            pVlanInfo->VlanId,
                                            pAddedPorts, pDeletedPorts);
            break;

        case VLAN_PROP_MAC_INFO_MSG:
            MRP_MMRP_PROP_MAC_INFO (pVlanInfo->u4ContextId,
                                    pVlanInfo->MacAddr,
                                    pVlanInfo->VlanId,
                                    pAddedPorts, pDeletedPorts);
            break;

        case VLAN_SET_MCAST_FORBID_MSG:
            MRP_MMRP_SER_MCAST_FORBID_PORTS (pVlanInfo->u4ContextId,
                                             pVlanInfo->MacAddr,
                                             pVlanInfo->VlanId,
                                             pAddedPorts, pDeletedPorts);
            break;

        case VLAN_PROP_FWDALL_INFO_MSG:
        case VLAN_PROP_FWDUNREG_INFO_MSG:
        case VLAN_SET_FWDALL_FORBID_MSG:
        case VLAN_SET_FWDUNREG_FORBID_MSG:

            /* In case of MRP, the Context Identifier starts from 1 whereas in
             * other modules the Context Identifier starts from 0. Hence, the
             * value of Context Identifier passed from other modules needs to
             * be incremented by 1 before using in the MRP module. This 
             * conversion is done inside the functions exported to other
             * modules.
             */
            MRP_CONVERT_CTXT_ID_TO_COMP_ID (pVlanInfo->u4ContextId);

            i4RetVal = MrpUtilMmrpPropOrSetDefGrpInfo (pVlanInfo->u4ContextId,
                                                       pVlanInfo->u1MsgType,
                                                       pVlanInfo->VlanId,
                                                       pAddedPorts,
                                                       pDeletedPorts);
            break;

        case VLAN_UPDATE_MAP_PORTS_MSG:
            MRP_MAP_UPD_MAP_PORTS (pVlanInfo->u4ContextId,
                                   pAddedPorts, pDeletedPorts,
                                   pVlanInfo->VlanId);
            break;

        case VLAN_UPDT_OR_POST_MSG:

            /* This message can be posted either for a single port or for
             * portlist. In case of a single port get the corresponding 
             * Context Identifier and local port from VCM.
             */
            if (pVlanInfo->u4IfIndex != 0)
            {
                if (MrpPortGetContextInfoFromIfIndex (pVlanInfo->u4IfIndex,
                                                      &u4ContextId,
                                                      &u2LocalPortId)
                    == OSIX_FAILURE)
                {
                    UtilPlstReleaseLocalPortList (pAddedPorts);
                    UtilPlstReleaseLocalPortList (pDeletedPorts);
                    MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                 "MrpApiNotifyVlanInfo: Failed to Get Context Id"
                                 " and local port number \r\n");
                    return OSIX_FAILURE;
                }
            }

            i4RetVal = MRP_FILL_BULK_MSG (pVlanInfo->u4ContextId,
                                          pVlanInfo->ppMrpQMsg,
                                          pVlanInfo->u1SubMsgType,
                                          pVlanInfo->VlanId,
                                          u2LocalPortId, pAddedPorts,
                                          pVlanInfo->MacAddr);
        default:
            break;
    }
    UtilPlstReleaseLocalPortList (pAddedPorts);
    UtilPlstReleaseLocalPortList (pDeletedPorts);
    return i4RetVal;
}

/**************************************************************************
 *
 *  FUNCTION NAME   :MrpApiMvrpPropagateVlanInfo                                
 *  
 *   DESCRIPTION     : Depending on u1action we will add a static entry or
 *                     delete it.
 *  
 *   INPUT           : u1AttrType -The attribute type received                
 *                   AddPortList - Ports to be added                        
 *                   DelPortList - ports to be deleted                      
 *                   u1Action - depending on u1Action we will add a static  
 *                              vlan  entry or delete it                    
 *  
 *   OUTPUT          :  None     
 *  
 *  RETURNS         : None                                                  
 *
 ***************************************************************************/
VOID
MrpApiMvrpPropagateVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                             tLocalPortList AddPortList,
                             tLocalPortList DelPortList)
{
    tMrpQMsg           *pMrpQMsg = NULL;

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);

    if ((u4ContextId == 0) || (u4ContextId >= MRP_SIZING_CONTEXT_COUNT))
    {
        return;
    }

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiMvrpPropagateVlanInfo: MRP is not started in "
                     "current context\r\n");
        return;
    }

    if (MrpMvrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MVRP is NOT enabled. Cannot Propagate "
                  "the Vlan Id %d.\n", VlanId));

        return;
    }

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              CONTROL_PLANE_TRC,
              "Rcvd Propagation req for Vlan Id %d.\n", VlanId));

    pMrpQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pMrpQMsg)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  MRP_CRITICAL_TRC, "MrpApiMvrpPropagateVlanInfo:"
                  "Q Msg memory allocation failed\n"));

        return;
    }

    MEMSET (pMrpQMsg, 0, sizeof (tMrpQMsg));

    pMrpQMsg->u2MsgType = MRP_MSG;

    pMrpQMsg->unMrpMsg.MrpMsg.u2MsgType = VLAN_PROP_VLAN_INFO_MSG;
    pMrpQMsg->unMrpMsg.MrpMsg.u2MapId = VlanId;
    pMrpQMsg->u4ContextId = u4ContextId;
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    MrpQueEnqMsg (pMrpQMsg);

    return;
}

/***************************************************************************
 *  FUNCTION NAME   :MrpApiMvrpSetVlanForbiddPorts                              
 *
 *  DESCRIPTION     :This function sets forbidden ports for a specific      
 *                    VlanId                                                
 *
 *  INPUT           : VlanId - VlanId for which Forbidden ports to be 
 *                            updated
 *                   AddPortList - Ports to be added                        
 *                   DelPortList - ports to be deleted                      
 *
 *  OUTPUT          :  None                                                 
 *
 *  RETURNS         : None                                                  
 *
 ***************************************************************************/
VOID
MrpApiMvrpSetVlanForbiddPorts (UINT4 u4ContextId, tVlanId VlanId,
                               tLocalPortList AddPortList,
                               tLocalPortList DelPortList)
{
    tMrpQMsg           *pMrpQMsg = NULL;

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiMvrpSetVlanForbiddPorts: MRP is not started in "
                     "current context\r\n");
        return;
    }

    if (MrpMvrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpApiMvrpSetVlanForbiddPorts: MVRP is NOT enabled."
                  "Cannot Set Forbidden Ports for the Vlan Id %d.\n", VlanId));

        return;
    }

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), CONTROL_PLANE_TRC,
              "MrpApiMvrpSetVlanForbiddPorts: Rcvd Forbidden Ports Set "
              "Req for Vlan Id %d.\n", VlanId));

    pMrpQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pMrpQMsg)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_CRITICAL_TRC,
                  "MrpApiMvrpSetVlanForbiddPorts:"
                  "Q Msg memory allocation failed\n"));
        return;
    }

    MEMSET (pMrpQMsg, 0, sizeof (tMrpQMsg));

    pMrpQMsg->u2MsgType = MRP_MSG;

    pMrpQMsg->unMrpMsg.MrpMsg.u2MsgType = MRP_SET_VLAN_FORBID_MSG;
    pMrpQMsg->unMrpMsg.MrpMsg.u2MapId = VlanId;
    pMrpQMsg->u4ContextId = u4ContextId;
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    MrpQueEnqMsg (pMrpQMsg);

    return;
}

/***************************************************************
 *
 *  FUNCTION NAME   : MrpApiIsMmrpEnabled
 *
 *  DESCRIPTION     : RETURNS the MMRP Application Module Status           
 *
 *  INPUT           : u4ContextId - Virtual Context Identifier                                      
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : OSIX_TRUE / OSIX_FALSE              
 *
 **************************************************************/
INT4
MrpApiIsMmrpEnabled (UINT4 u4ContextId)
{
    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
    return (MrpMmrpIsEnabled (u4ContextId));
}

/***************************************************************
 *
 *  FUNCTION NAME   :MrpApi1MmrpPropagateMcastInfo                     
 *
 *  DESCRIPTION     :depending on u1action we will add a static 
 *                   Multicast entry or delete an entry         
 *  INPUT           : pMacAddr - Pointer to mac address          
 *                   VlanId - VlanId                            
 *                   AddPortList - Port list to be added        
 *                   DelPortList - Port list to be deleted      
 *
 *  OUTPUT          : None                                       
 *
 *  RETURNS         :None                                       
 *
 ***************************************************************/
VOID
MrpApiMmrpPropagateMacInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                            tVlanId VlanId,
                            tLocalPortList AddPortList,
                            tLocalPortList DelPortList)
{

    tMrpQMsg           *pMrpQMsg = NULL;

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiMmrpPropagateMacInfo: MRP is not started in "
                     "current context\r\n");
        return;
    }

    if (MrpMmrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpApiMmrpPropagateMacInfo: MMRP is NOT enabled. Cannot"
                  " Propagate for the Vlan Id %d, the Group Addr", VlanId));
        MRP_PRINT_MAC_ADDR (u4ContextId, MMRP_MOD_TRC, MacAddr);
        return;
    }

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
              "MrpApiMmrpPropagateMacInfo: Received Forbidden Ports Set "
              "Request for the Vlan Id %d and the Group Addr", VlanId));
    MRP_PRINT_MAC_ADDR (u4ContextId, MMRP_MOD_TRC, MacAddr);

    pMrpQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pMrpQMsg)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_CRITICAL_TRC,
                  "MrpApiMmrpPropagateMacInfo:"
                  " Q Msg memory allocation failed\n"));

        return;
    }

    MEMSET (pMrpQMsg, 0, sizeof (tMrpQMsg));

    pMrpQMsg->u2MsgType = MRP_MSG;

    pMrpQMsg->unMrpMsg.MrpMsg.u2MsgType = MRP_PROP_MAC_INFO_MSG;
    pMrpQMsg->unMrpMsg.MrpMsg.u2MapId = VlanId;
    pMrpQMsg->u4ContextId = u4ContextId;
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.MacAddr, MacAddr, ETHERNET_ADDR_SIZE);
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    MrpQueEnqMsg (pMrpQMsg);

    return;

}

/****************************************************************
 *
 *  FUNCTION NAME   :MrpApiMmrpSetMcastForbiddPorts             
 *
 *  DESCRIPTION     :This function set the forbidden ports for a
 *                   given multicast entry                      
 *
 *  INPUT           : pMacAddr - Pointer to multicast mac address
 *                   VlanId - VlanId                            
 *                   AddPortList - Port list to be added        
 *                   DelPortList - Port list to be deleted      
 *
 *  OUTPUT          : None                                       
 *
 *  RETURNS         :None                                       
 *
 ****************************************************************/
VOID
MrpApiMmrpSetMcastForbiddPorts (UINT4 u4ContextId, tMacAddr MacAddr,
                                tVlanId VlanId,
                                tLocalPortList AddPortList,
                                tLocalPortList DelPortList)
{
    tMrpQMsg           *pMrpQMsg = NULL;

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiMmrpSetMcastForbiddPorts: MRP is not started in "
                     "current context\r\n");
        return;
    }

    if (MrpMmrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpApiMmrpSetMcastForbiddPorts: MMRP is NOT enabled. "
                  "Cannot Set Forbidden  Ports for the Vlan Id %d and the "
                  "Group Addr", VlanId));
        MRP_PRINT_MAC_ADDR (u4ContextId, MMRP_MOD_TRC, MacAddr);

        return;
    }

    MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
              (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
              "MrpApiMmrpSetMcastForbiddPorts:Received Forbidden Ports "
              "Set Request for the Vlan Id %d and the Group Addr", VlanId));
    MRP_PRINT_MAC_ADDR (u4ContextId, MMRP_MOD_TRC, MacAddr);

    pMrpQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pMrpQMsg)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_CRITICAL_TRC,
                  "MrpApiMmrpSetMcastForbiddPorts:"
                  "Q Msg memory allocation failed\n"));

        return;
    }

    MEMSET (pMrpQMsg, 0, sizeof (tMrpQMsg));

    pMrpQMsg->u2MsgType = MRP_MSG;

    pMrpQMsg->unMrpMsg.MrpMsg.u2MsgType = MRP_SET_MCAST_FORBID_MSG;
    pMrpQMsg->unMrpMsg.MrpMsg.u2MapId = VlanId;
    pMrpQMsg->u4ContextId = u4ContextId;
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.MacAddr, MacAddr, ETHERNET_ADDR_SIZE);
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));
    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    MrpQueEnqMsg (pMrpQMsg);

    return;
}

/****************************************************************
 *
 *  FUNCTION NAME   :MrpApiMmrpPropagateDefGroupInfo                  
 *
 *  DESCRIPTION     :This function propagates the default Group 
 *                   info to all the ports during initialisation
 *
 *  INPUT           : u1Type - Whether it is VLAN_ALL_GROUPS or  
 *                   VLAN_UNREG_GROUPS                          
 *                   VlanId - VlanId                            
 *                   AddPortList - Port list to be added        
 *                   DelPortList - Port list to be deleted      
 *
 *  OUTPUT          : None                                       
 *
 *  RETURNS         :None                                       
 *
 ****************************************************************/
VOID
MrpApiMmrpPropagateDefGroupInfo (UINT4 u4ContextId, UINT1 u1Type,
                                 tVlanId VlanId,
                                 tLocalPortList AddPortList,
                                 tLocalPortList DelPortList)
{
    UINT2               u2MsgType = 0;

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);

    if (u1Type == VLAN_ALL_GROUPS)
    {
        u2MsgType = MRP_PROP_FWDALL_INFO_MSG;
    }
    else
    {
        u2MsgType = MRP_PROP_FWDUNREG_INFO_MSG;
    }

    if (MrpUtilMmrpPropOrSetDefGrpInfo (u4ContextId, u2MsgType, VlanId,
                                        AddPortList, DelPortList)
        != OSIX_SUCCESS)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiMmrpPropagateDefGroupInfo: "
                     "MrpUtilMmrpPropOrSetDefGrpInfo returned failure\r\n");
    }

    return;
}

/****************************************************************
 *
 *  FUNCTION NAME   :MrpApiMmrpSetDefGrpForbiddPorts              
 *
 *  DESCRIPTION     :depending on u1action we will add a static 
 *                   entry or delete it                         
 *
 *  INPUT           : pMacAddr - Pointer to Mac address          
 *                   VlanId - VlanId                            
 *                   AddPortList - Port list to be added        
 *                   DelPortList - Port list to be deleted      
 *
 *  OUTPUT          : None                                       
 *
 *  RETURNS         :None                                       
 *
 ****************************************************************/
VOID
MrpApiMmrpSetDefGrpForbiddPorts (UINT4 u4ContextId, UINT1 u1Type,
                                 tVlanId VlanId,
                                 tLocalPortList AddPortList,
                                 tLocalPortList DelPortList)
{
    UINT2               u2MsgType = 0;

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);

    if (u1Type == VLAN_ALL_GROUPS)
    {
        u2MsgType = MRP_SET_FWDALL_FORBID_MSG;
    }
    else
    {
        u2MsgType = MRP_SET_FWDUNREG_FORBID_MSG;
    }

    if (MrpUtilMmrpPropOrSetDefGrpInfo (u4ContextId, u2MsgType, VlanId,
                                        AddPortList, DelPortList)
        != OSIX_SUCCESS)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiMmrpPropagateDefGroupInfo: "
                     "MrpUtilMmrpPropOrSetDefGrpInfo returned failure\r\n");
    }

    return;
}

/**************************************************************************
 *
 *  FUNCTION NAME    : MrpApiMrpMapUpdateMapPorts 
 *
 *  DESCRIPTION      : This function transmits Join messages on the given
 *                     port, for all the Attributes registered for the   
 *                     given MapId.                                      
 *                     This function is called by VLAN whenever static   
 *                     member ports are added.                           
 *                     Updation of Map Port list affects only MMRP.      
 *
 *  INPUT            : AddPortList - Ports that is added to the          
 *                              given MAP context.                       
 *                     u2Map  - Id of the MAP context for which          
 *                              new port is added.                       
 *  OUTPUT           : None                                              
 *
 *  RETURNS          : None                                              
 *
 **************************************************************************/
VOID
MrpApiMrpMapUpdateMapPorts (UINT4 u4ContextId, tLocalPortList AddPortList,
                            tLocalPortList DelPortList, UINT2 u2MapId)
{
    tMrpQMsg           *pMrpQMsg = NULL;

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiMrpMapUpdateMapPorts: MRP is not started in "
                     "current context\r\n");

        return;
    }

    if (MrpMmrpIsEnabled (u4ContextId) == OSIX_FALSE)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                  "MrpApiMrpMapUpdateMapPorts: MMRP is NOT enabled. "
                  "Cannot Update Map Ports for MapId %d \r\n", u2MapId));

        return;
    }

    pMrpQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pMrpQMsg)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_CRITICAL_TRC,
                  "MrpApiMrpMapUpdateMapPorts:"
                  "Q Msg memory allocation failed\n"));
        return;
    }

    MEMSET (pMrpQMsg, 0, sizeof (tMrpQMsg));

    pMrpQMsg->u2MsgType = MRP_MSG;

    pMrpQMsg->unMrpMsg.MrpMsg.u2MsgType = MRP_UPDATE_MAP_PORTS_MSG;
    pMrpQMsg->unMrpMsg.MrpMsg.u2MapId = u2MapId;
    pMrpQMsg->u4ContextId = u4ContextId;

    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.AddedPorts, AddPortList,
            sizeof (tLocalPortList));

    MEMCPY (pMrpQMsg->unMrpMsg.MrpMsg.DeletedPorts, DelPortList,
            sizeof (tLocalPortList));

    MrpQueEnqMsg (pMrpQMsg);

    return;
}

/*****************************************************************************
*                                                                           
*    FUNCTION NAME       : MrpApiCreateContext                              
*                                                                           
*    DESCRIPTION         : This function creates given context              
*                                                                           
*    INPUT               : u4ContextId - Context Identifier                 
*                                                                           
*    OUTPUT              : None.                                            
*                                                                           
*    RETURNS             : OSIX_SUCCESS / OSIX_FAILURE.                        
*                                                                           
*****************************************************************************/
INT4
MrpApiCreateContext (UINT4 u4ContextId)
{
    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
    return (MrpCtxtNotifyContextCreateOrDel (u4ContextId,
                                             MRP_CREATE_CONTEXT_MSG));
}

/*****************************************************************************
*                                                                           
*    FUNCTION NAME       : MrpApiDeleteContext                              
*                                                                           
*    DESCRIPTION         : This function deleted  given context             
*                                                                           
*    INPUT               : u4ContextId - Context Identifier                 
*                                                                           
*    OUTPUT              : None.                                            
*                                                                           
*    RETURNS            : OSIX_SUCCESS / OSIX_FAILURE.                        
*                                                                           
*****************************************************************************/
INT4
MrpApiDeleteContext (UINT4 u4ContextId)
{
    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
    return (MrpCtxtNotifyContextCreateOrDel (u4ContextId,
                                             MRP_DELETE_CONTEXT_MSG));
}

/******************************************************************************
*                                                                           
*    FUNCTION NAME       : MrpApiPortMapIndication                          
*                                                                           
*    DESCRIPTION         : This function is for mapping a port to a         
*                          context.                                        
*                                                                           
*    INPUT               : u4ContextId - Context Identifier                 
*                          u4IfIndex  - Interface index                     
*                          u2LocalPortId - local Port Id                    
*                                                                           
*    OUTPUT              : None.                                            
*                                                                          
*    RETURNS            : OSIX_SUCCESS / OSIX_FAILURE.                        
*                                                                           
*****************************************************************************/
INT4
MrpApiPortMapIndication (UINT4 u4ContextId, UINT4 u4IfIndex,
                         UINT2 u2LocalPortId)
{
    INT4                i4RetVal = OSIX_FAILURE;

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
    i4RetVal = MrpQuePostCfgMessage (MRP_PORT_MAP_MSG, u2LocalPortId,
                                     u4ContextId, u4IfIndex);

    if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
    {

        MrpPortSetProtoEnabledStatOnPort (u4ContextId, u2LocalPortId,
                                          L2_PROTO_MVRP, OSIX_DISABLED);
        MrpPortSetProtoEnabledStatOnPort (u4ContextId, u2LocalPortId,
                                          L2_PROTO_MMRP, OSIX_DISABLED);

    }
    if (i4RetVal == OSIX_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/*****************************************************************************
*                                                                           
*    FUNCTION NAME       : MrpApiPortUnmapIndication                        
*                                                                           
*    DESCRIPTION         : This function is for unmapping a port from a         
*                          context.                                         
*                                                                           
*    INPUT               : u4IfIndex - Interface Index                     
*                                                                           
*    OUTPUT              : None.                                            
*                                                                           
*    RETURNS            : OSIX_SUCCESS / OSIX_FAILURE.                        
*                                                                           
*****************************************************************************/
INT4
MrpApiPortUnmapIndication (UINT4 u4IfIndex)
{
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;
    UINT2               u2LocalPortId = 0;

    if (MrpPortGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiPortUnmapIndication: Failed to Get Context Id\r\n");
        return OSIX_FAILURE;
    }

    i4RetVal = MrpQuePostCfgMessage (MRP_PORT_UNMAP_MSG, u2LocalPortId,
                                     u4ContextId, u4IfIndex);
    if (i4RetVal == OSIX_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/*****************************************************************************
*                                                                           
*    FUNCTION NAME       : MrpApiUpdateContextName                          
*                                                                           
*    DESCRIPTION         : This function updates the name of the context    
*                                                                           
*    INPUT               : u4ContextId - Context Identifier                 
*                                                                          
*    OUTPUT              : None.                                            
*                                                                           
*                                                                           
*    RETURNS            : OSIX_SUCCESS / OSIX_FAILURE.                        
*                                                                           
*****************************************************************************/
INT4
MrpApiUpdateContextName (UINT4 u4ContextId)
{
    tMrpQMsg           *pQMsg = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    /* Is MRP task initialized */
    if (gMrpGlobalInfo.u1MrpIsInitComplete == OSIX_FALSE)
    {
        /* Return Silently. MRP task is not yet initialized */
        return OSIX_SUCCESS;
    }

    pQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pQMsg)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC, "MrpApiUpdateContextName:"
                     "Q Msg memory allocation failed\n");
        return OSIX_FAILURE;
    }

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
    MEMSET (pQMsg, 0, sizeof (tMrpQMsg));
    pQMsg->u2MsgType = MRP_MSG;
    pQMsg->unMrpMsg.MrpMsg.u2MsgType = MRP_UPDATE_CONTEXT_NAME;
    pQMsg->u4ContextId = u4ContextId;

    i4RetVal = MrpQueEnqMsg (pQMsg);

    return i4RetVal;
}

/******************************************************************************
*
*   FUNCTION NAME   : MrpApiCreatePort                                      
*
*   DESCRIPTION     : This function is for adding a port.                     
*
*   INPUT           : u4ContextId   - Context Identifier
*                     u4IfIndex     - Interface Index
*                     u2LocalPortId - Local Port Number
*
*   OUTPUT          : None                                                    
*
*   RETURNS         : OSIX_SUCCESS /OSIX_FAILURE                               
*                                                                            
******************************************************************************/
INT4
MrpApiCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    INT4                i4RetVal = OSIX_FAILURE;

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
    i4RetVal = MrpQuePostCfgMessage (MRP_PORT_CREATE_MSG, u2LocalPortId,
                                     u4ContextId, u4IfIndex);

    if (i4RetVal == OSIX_SUCCESS)
    {
        L2_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/******************************************************************************
* 
*   FUNCTION NAME   : MrpApiDeletePort                                        
* 
*   DESCRIPTION     : This function is for deleting a port.                  
* 
*   INPUT           : u4IfIndex - Port to be deleted                            
* 
*   OUTPUT          : None                                                    
* 
*   RETURNS         : OSIX_SUCCESS / OSIX_FAILURE
*                                                                            
******************************************************************************/
INT4
MrpApiDeletePort (UINT4 u4IfIndex)
{
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;
    UINT2               u2LocalPortId = 0;

    if (MrpPortGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiDeletePort: Failed to Get Context Id\r\n");
        return OSIX_FAILURE;
    }
    i4RetVal = MrpQuePostCfgMessage (MRP_PORT_DELETE_MSG, u2LocalPortId,
                                     u4ContextId, u4IfIndex);
    return i4RetVal;
}

/************************************************************************
*
*   FUNCTION NAME    : MrpApiPortOperInd                                
*
*   DESCRIPTION      : Invoked by VLAN whenever Port Oper Status is     
*                     changed. If Link Aggregation is enabled, then    
*                     Oper Down indication is given for each port      
*                     initially. When the Link Aggregation Group is    
*                     formed, then Oper Up indication is given.       
*                     In the Oper Down state, frames will neither      
*                     be transmitted nor be received.                  
*
*   INPUT            : u4IfIndex - Port for which Oper Status is changed.  
*                     u1OperStatus - MRP_OPER_UP/MRP_OPER_DOWN         
*
*   OUTPUT           : None                                             
*
*   RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                        
*
************************************************************************/
INT4
MrpApiPortOperInd (UINT4 u4IfIndex, UINT1 u1OperStatus)
{

    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;
    UINT2               u2LocalPortId = 0;

    if (MrpPortGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiPortOperInd: Failed to Get Context Id\r\n");
        return OSIX_FAILURE;
    }
    if (u1OperStatus == MRP_OPER_UP)
    {
        i4RetVal = MrpQuePostCfgMessage (MRP_PORT_OPER_UP_MSG, u2LocalPortId,
                                         u4ContextId, u4IfIndex);
    }
    else
    {
        i4RetVal = MrpQuePostCfgMessage (MRP_PORT_OPER_DOWN_MSG, u2LocalPortId,
                                         u4ContextId, u4IfIndex);
    }
    return i4RetVal;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiNotifyPortStateChange 
 *
 *    DESCRIPTION      : This function is called by STP to post a message
 *                       to MRP task's message queue to notify the change in
 *                       the port state.       
 *
 *    INPUT            : u4IfIndex - Interface Index        
 *                       u2MapID   - CIST or MST Instance Identifier of the port
 *                                   whose state (FORWARDING to DISCARDING or
 *                                   vice versa) has changed
 *                       u1State   - Port State                        
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
MrpApiNotifyPortStateChange (UINT4 u4IfIndex, UINT2 u2MapId, UINT1 u1State)
{
    tMrpQMsg           *pQMsg = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;
    UINT2               u2LocalPortId = 0;

    /* Get the Context Id and the Local Port Id from the IfIndex */
    if (MrpPortGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyPortStateChange: "
                     "Failed to Get Context Id\r\n");
        return OSIX_FAILURE;
    }

    if (MrpUtilIsMrpStarted (u4ContextId) == OSIX_FALSE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyPortStateChange: MRP is not started in"
                     " current context\r\n");
        return OSIX_FAILURE;
    }

    if ((u1State != AST_PORT_STATE_FORWARDING) &&
        (u1State != AST_PORT_STATE_DISCARDING))
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyPortStateChange: MRP handles only discarding"
                     " and learning.\r\n");
        return OSIX_FAILURE;
    }

    pQMsg = (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

    if (NULL == pQMsg)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_CRITICAL_TRC,
                  "MrpApiNotifyPortStateChange: Q Msg memory allocation "
                  "failed\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pQMsg, 0, sizeof (tMrpQMsg));

    /*KNOWN ISSUE: 
       In a Provider Edge Bridge,
       When a CEP is configured as only member port of an S-VLAN and 
       there exists a Provider Edge Port  (CEP,  S-VLAN). 
       then MVRP propagation in Provider network happens and this 
       results in learning of SVLANs in the provider network 
       irrespective of the port state of C-VLAN component ports. 

       When CEP becomes an alternate port in the C-VLAN component, 
       the data path between the customer and provider breaks. 
       But the S-VLANs propagation will continue in the provider 
       network (as the internal CNPs are in forwarding state). 

       This results in unnecessary traffic load on the above S-VLAN 
       path connecting the above CEP and other customer points of 
       attachments. */

    pQMsg->u2MsgType = MRP_MSG;
    if (u1State == AST_PORT_STATE_FORWARDING)
    {
        pQMsg->unMrpMsg.MrpMsg.u2MsgType = MRP_STAP_PORT_FWD_MSG;
    }
    else
    {
        pQMsg->unMrpMsg.MrpMsg.u2MsgType = MRP_STAP_PORT_BLK_MSG;
    }
    pQMsg->unMrpMsg.MrpMsg.u2Port = u2LocalPortId;
    pQMsg->unMrpMsg.MrpMsg.u2MapId = u2MapId;
    pQMsg->u4IfIndex = u4IfIndex;
    pQMsg->u4ContextId = u4ContextId;

    i4RetVal = MrpQueEnqMsg (pQMsg);

    return i4RetVal;
}

/*************************************************************************
 *  FUNCTION NAME   : MrpApiUpdateInstVlanMap                  
 *                                                             
 *   DESCRIPTION     : This function updates MRP of Instance to VLAN map
 *                     Updation                     
 *                                                             
 *   INPUT           : u1Action    - Update / Post Message         
 *                     u4ContextId - Context Identifier        
 *                     u2NewMapId  - New Map Identifier          
 *                     u2OldMapId  - Old Map Identifier          
 *                     VlanId      - VLAN Identifier of mapped VLAN  
 *                                    mapped to the instance                   
 *                     u2MapEvent  - Indicates whether it is a   
 *                                   single VLAN to MSTI or list of VLAN      
 *                                   map/umap                                 
 *                     pu1Result   - Output to indicate message   
 *                                   post or updation                         
 *                                                              
 *   OUTPUT          : Buffer to be posted to MRP              
 *                                                             
 *  RETURNs         : None                                     
 ****************************************************************/

VOID
MrpApiUpdateInstVlanMap (UINT4 u4ContextId, UINT1 u1Action, UINT2 u2NewMapId,
                         UINT2 u2OldMapId, tVlanId VlanId,
                         UINT2 u2MapEvent, UINT1 *pu1Result)
{
    tMrpVidMapMsg      *pMrpVidMapMsg = NULL;
    tVlanMapInfo       *pVlanMapInfo = NULL;
    tMrpQMsg           *pMrpQMsg = NULL;
    INT4                i4EventIndex = 0;
    UINT4               u4Size = 0;
    UINT2               u2MsgType = 0;
    UINT1               u1BufAlloc = OSIX_FALSE;

    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);
    pMrpQMsg = gMrpGlobalInfo.apMrpRemapQMsg[u4ContextId];

    /* Should Post the message if max array is reached and re-initialise 
     * buffer pointer to NULL on posting */

    if (MRP_IS_MRP_STARTED (u4ContextId) == OSIX_FALSE)
    {
        if (pMrpQMsg != NULL)
        {
            if ((MRP_MAP_INST_VLAN_LIST_MSG == u2MapEvent) ||
                (MRP_UNMAP_INST_VLAN_LIST_MSG == u2MapEvent))
            {

                MemReleaseMemBlock (gMrpGlobalInfo.VlanInstMapPoolId,
                                    (UINT1 *) pMrpQMsg);
            }
            else
            {
                MemReleaseMemBlock (gMrpGlobalInfo.QMsgPoolId,
                                    (UINT1 *) pMrpQMsg);
            }
        }
        gMrpGlobalInfo.apMrpRemapQMsg[u4ContextId] = NULL;
        return;
    }

    if (MRP_POST_MAP_MSG != u1Action)
    {
        if (NULL == pMrpQMsg)
        {
            if ((MRP_MAP_INST_VLAN_LIST_MSG == u2MapEvent) ||
                (MRP_UNMAP_INST_VLAN_LIST_MSG == u2MapEvent))
            {
                u4Size = (sizeof (tMrpQMsg) +
                          (sizeof (tVlanMapInfo) *
                           (VLAN_MAP_INFO_PER_POST - 1)));

                pMrpQMsg = (tMrpQMsg *)
                    MemAllocMemBlk (gMrpGlobalInfo.VlanInstMapPoolId);

                u2MsgType = MRP_VLAN_LIST_INST_MAP_MSG;
            }
            else
            {
                u4Size = sizeof (tMrpQMsg);

                pMrpQMsg =
                    (tMrpQMsg *) MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId);

                u2MsgType = MRP_VLAN_INST_MAP_MSG;
            }

            if (NULL == pMrpQMsg)
            {

                MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                          (OS_RESOURCE_TRC | ALL_FAILURE_TRC |
                           MRP_CRITICAL_TRC),
                          " MrpApiUpdateInstVlanMap : Message Allocation failed"
                          " for VLAN Map  Config Message\r\n"));
                return;
            }

            u1BufAlloc = OSIX_TRUE;

            MEMSET (pMrpQMsg, 0, u4Size);

            gMrpGlobalInfo.apMrpRemapQMsg[u4ContextId] = pMrpQMsg;

            (pMrpQMsg)->i4Count++;
            (pMrpQMsg)->u2MsgType = u2MsgType;
            (pMrpQMsg)->u4ContextId = u4ContextId;
        }

        /* The below part of updating the message to be sent to MRP needs to 
         * be modified when MST-VLAN map is to be operated upon COMMIT. 
         * Take care of change the ProcessMsg thread also. 
         * Operate similar to MrpFillBulkMsg, post and process like BulkMsg */

        /* As of now MrpVidMapMsg is indexed with 0 as only one msg is present 
         */
        pMrpVidMapMsg = &((pMrpQMsg)->unMrpMsg.MrpVidMapMsg[0]);

        i4EventIndex = pMrpVidMapMsg->i4EventCount;

        pVlanMapInfo = &(pMrpVidMapMsg->VlanMapInfo[i4EventIndex]);

        /* Fill the below only when the buffer is freshly allocated */
        if (OSIX_TRUE == u1BufAlloc)
        {
            pMrpVidMapMsg->u2NewMapId = u2NewMapId;
            pMrpVidMapMsg->u2MsgEvent = u2MapEvent;
        }

        pVlanMapInfo->u2MapId = u2OldMapId;
        pVlanMapInfo->VlanId = VlanId;

        pMrpVidMapMsg->i4EventCount++;

        *pu1Result = MRP_MAP_MSG_UPDATED;
    }                            /* MRP_POST_MAP_MSG */
    else
    {
        /* When u1Action is post map message, pMrpQMsg cannot be NULL */
        pMrpVidMapMsg = &((pMrpQMsg)->unMrpMsg.MrpVidMapMsg[0]);
    }

    if ((MRP_POST_MAP_MSG == u1Action) || (MRP_UPDNPOST_MAP_MSG == u1Action) ||
        (((pMrpVidMapMsg)->i4EventCount) == VLAN_MAP_INFO_PER_POST))
    {
        MrpQueEnqMsg (pMrpQMsg);
        gMrpGlobalInfo.apMrpRemapQMsg[u4ContextId] = NULL;
        *pu1Result = MRP_MAP_MSG_POSTED;
    }

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiEnqueueRxPktOnBackPlane
 *
 *    DESCRIPTION      : This function receives the incoming MRP PDU  from
 *                       the CFA Module for Backplane interface and enqueues it
 *                       to MRP PDU Queue for processing.In backplane interface
 *                       we insert the context id that triggered the PDU TX into
 *                       the Pkt. That would have be retrieved from the CPU 
 *                       itself. That context id alone should be used for 
 *                       processing this PDU.
 *
 *    INPUT            : pFrame - pointer to the received  MVRP/MMRP 
 *                                frame buffer (CRU buff)
 *                       pBackPlaneParams - pointer to the structure containing
 *                                          values specific for PDU processing
 *                                          in backplane intf.
 *                       u4IfIndex - Src port on which the frame is 
 *                                   received.
 *                       u2MapId - MAP id. value will be as follows:
 *                                 0 in case of MVRP pkt.
 *                                 VlanId in case of MMRP pkt.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
MrpApiEnqueueRxPktOnBackPlane (tCRU_BUF_CHAIN_DESC * pFrame,
                               tCfaBackPlaneParams * pBackPlaneParams,
                               UINT4 u4IfIndex, UINT2 u2MapId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpPortEntry      *pPortEntry = NULL;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;
    UINT2               u2Port = MRP_INVALID_PORT;

    u4ContextId =
        MRP_CONVERT_CTXT_ID_TO_COMP_ID (pBackPlaneParams->u4ContextId);

    /* We have the corresponding context Id and IfIndex. Get the local port
     * identifier from MRP Data strucutures, since we expect backplane
     * interface to be part of all the contexts created in the system
     * */
    MRP_LOCK ();
    pMrpContextInfo = MRP_CONTEXT_PTR (u4ContextId);
    if (pMrpContextInfo == NULL)
    {
        MRP_UNLOCK ();
        return OSIX_FAILURE;
    }

    for (u2Port = 1; u2Port <= MRP_MAX_PORTS_PER_CONTEXT; u2Port++)
    {
        pPortEntry = MRP_GET_PORT_ENTRY (pMrpContextInfo, u2Port);

        if (pPortEntry != NULL)
        {
            if (u4IfIndex == pPortEntry->u4IfIndex)
            {
                break;
            }
        }                        /* Invalid Port */
    }                            /* Maximum ports */
    MRP_UNLOCK ();

    if (MrpUtilPostRcvdPduToTask (pFrame, u4ContextId, u2Port, u2MapId)
        != OSIX_SUCCESS)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiEnqueueRxPkt: MrpUtilPostRcvdPduToTask "
                     "returned Failure\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiPropVlanDelFromEsp     
 *
 *    DESCRIPTION      : This API will be invoked by PBB-TE module to indicate
 *                       that a VLAN has been removed from ESP list. If the vlan
 *                       was part of ESP while creating, then that VLAN will not
 *                       be propagated by MRP. Hence, now this indication needs 
 *                       to be given to MRP module to trigger vlan propagation.
 *
 *    INPUT            : u4ContextId - Denotes in which context, the config 
 *                                     has to be applied
 *                       VlanId      - Identifies the VLAN
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 
 ****************************************************************************/
INT4
MrpApiPropVlanDelFromEsp (UINT4 u4ContextId, tVlanId VlanId)
{
    UINT1              *pAddPorts = NULL;
    UINT1              *pDelPortList = NULL;
    UINT4               u4CompId = u4ContextId;

    pAddPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pAddPorts == NULL)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpApiPropVlanDelFromEsp : Error in allocating memory "
                     "for pAddPorts\r\n");
        return OSIX_FAILURE;
    }
    pDelPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pDelPortList == NULL)
    {
        UtilPlstReleaseLocalPortList (pAddPorts);
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpApiPropVlanDelFromEsp : Error in allocating memory "
                     "for pDelPortList\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pAddPorts, 0, sizeof (tLocalPortList));
    MEMSET (pDelPortList, 0, sizeof (tLocalPortList));

    u4CompId = MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);

    MrpPortGetVlanLocalEgressPorts (u4CompId, VlanId, pAddPorts);

    /* Conversion of ctxt to comp id, MRP status and MVRP status will be
     * taken care by this API itself. 
     * */
    MrpApiMvrpPropagateVlanInfo (u4ContextId, VlanId, pAddPorts, pDelPortList);

    UtilPlstReleaseLocalPortList (pAddPorts);
    UtilPlstReleaseLocalPortList (pDelPortList);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiRemVlanAddedInEsp      
 *
 *    DESCRIPTION      : This API will be invoked by PBB-TE module to indicate
 *                       that a VLAN has been added to ESP list. Since, the VLAN
 *                       is part of ESP now, this VLAN needs to be removed from
 *                       MRP.
 *
 *    INPUT            : u4ContextId - Denotes in which context, the config 
 *                                     has to be applied
 *                       VlanId      - Identifies the VLAN
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
MrpApiRemVlanAddedInEsp (UINT4 u4ContextId, tVlanId VlanId)
{
    UINT1              *pAddPorts = NULL;
    UINT1              *pDelPortList = NULL;

    pAddPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pAddPorts == NULL)
    {
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpApiRemVlanAddedInEsp : Error in allocating memory "
                     "for pAddPorts\r\n");
        return OSIX_FAILURE;
    }
    pDelPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pDelPortList == NULL)
    {
        UtilPlstReleaseLocalPortList (pAddPorts);
        MRP_GBL_TRC (MRP_RED_TRC,
                     "MrpApiRemVlanAddedInEsp : Error in allocating memory "
                     "for pDelPortList\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pAddPorts, 0, sizeof (tLocalPortList));
    MEMSET (pDelPortList, 0, sizeof (tLocalPortList));

    MrpPortGetVlanLocalEgressPorts (u4ContextId, VlanId, pDelPortList);

    /* Conversion of ctxt to comp id, MRP status and MVRP status will be
     * taken care by this API itself. 
     * */
    MrpApiMvrpPropagateVlanInfo (u4ContextId, VlanId, pAddPorts, pDelPortList);

    UtilPlstReleaseLocalPortList (pAddPorts);
    UtilPlstReleaseLocalPortList (pDelPortList);
    return OSIX_SUCCESS;
}

#ifdef L2RED_WANTED
/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiRcvPktFromRm 
 *
 *    DESCRIPTION      : This API constructs a message containing the
 *                       given RM event and RM message and posts it to the
 *                       MRP queue
 *
 *    INPUT            : u1Event   - Event type given by RM module 
 *                       pData     - RM Message to enqueue
 *                       u2DataLen - Length of the message
 *
 *    OUTPUT           : NONE
 *    
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *     
 ****************************************************************************/
INT4
MrpApiRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tMrpQMsg           *pQMsg = NULL;

    /* pData (Message pointer is valid only if the event is RM_MESSAGE/
     * RM_STANDBY_UP/RM_STANDBY_DOWN.
     */

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP)
         || (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        MRP_GBL_TRC (MRP_RED_TRC, "MrpApiRcvPktFromRm: Received message "
                     "with message pointer as NULL\r\n");
        return OSIX_FAILURE;
    }

    if (u1Event == RM_MESSAGE)
    {

        if (u2DataLen < MRP_RED_MSG_HDR_SIZE)
        {
            RM_FREE (pData);
            MRP_GBL_TRC (MRP_RED_TRC,
                         "MrpApiRcvPktFromRm: Received invalid message\r\n");
            return OSIX_FAILURE;
        }
    }
    else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
    {
        if (u2DataLen != RM_NODE_COUNT_MSG_SIZE)
        {
            MrpPortRelRmMsgMemory ((UINT1 *) pData);
            MRP_GBL_TRC (MRP_RED_TRC,
                         "MrpApiRcvPktFromRm: Received invalid message\r\n");
            return OSIX_FAILURE;
        }
    }

    if ((pQMsg = (tMrpQMsg *) (MemAllocMemBlk (gMrpGlobalInfo.QMsgPoolId)))
        == NULL)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC | MRP_RED_TRC,
                     "MrpApiRcvPktFromRm: Memory allocation failed for RM "
                     "message\r\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            MrpPortRelRmMsgMemory ((UINT1 *) pData);
        }

        return OSIX_FAILURE;
    }

    MEMSET (pQMsg, 0, sizeof (tMrpQMsg));

    pQMsg->u2MsgType = MRP_RM_MSG;

    pQMsg->unMrpMsg.MrpRmMsg.u1Event = u1Event;
    pQMsg->unMrpMsg.MrpRmMsg.pData = pData;
    pQMsg->unMrpMsg.MrpRmMsg.u2DataLen = u2DataLen;

    if (MrpQueEnqMsg (pQMsg) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (MRP_CRITICAL_TRC | MRP_RED_TRC,
                     "MrpApiRcvPktFromRm: Enqueuing RmMessage to MRP message "
                     "queue failed\r\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            MrpPortRelRmMsgMemory ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#endif /* L2RED_WANTED */

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiShutdownModule
 *                                                                          
 *    DESCRIPTION      : This function is shutdowns MRP in all the contexts
 *                       in the System.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpApiShutdownModule ()
{
    tMrpContextInfo    *pContextInfo = NULL;
    UINT4               u4ContextId = gMrpGlobalInfo.u2FirstContextId;

    MRP_LOCK ();
    gMrpGlobalInfo.u1MrpIsInitComplete = OSIX_FALSE;
    MrpQueFlushContextsQueue ();
    pContextInfo = MrpCtxtGetContextInfo (u4ContextId);

    for (; pContextInfo != NULL;
         (pContextInfo = (MrpCtxtGetContextInfo
                          (pContextInfo->u2NextContextId))))

    {
        if (MrpCtxtHandleDeleteContext (pContextInfo->u4ContextId)
            == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    MrpSizingMemDeleteMemPools ();
    MrpPortDeRegisterWithRM ();
    MrpTmrDeInit ();
    MRP_UNLOCK ();
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiStartModule
 *                                                                          
 *    DESCRIPTION      : This function is starts the MRP module.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpApiStartModule ()
{
    INT4                i4RetVal = OSIX_FAILURE;

    MRP_LOCK ();
    i4RetVal = MrpMainModuleStart ();
    MRP_UNLOCK ();

    return i4RetVal;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiNotifyStpInfo
 *                                                                          
 *    DESCRIPTION      : This function is called from STP/L2IWF modules 
 *                       to indicate the following.
 *                       - MRP_PORT_ROLE_CHG_MSG
 *                       - MRP_PORT_OPER_P2P_MSG
 *                       - MRP_TCDETECTED_TMR_STATUS
 *
 *    INPUT            : pMrpInfo - Pointer to the structure which 
 *                                  contains the MRP info from the above 
 *                                  modules. 
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
MrpApiNotifyStpInfo (tMrpInfo * pMrpInfo)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (pMrpInfo == NULL)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyStpInfo: MrpInfo Passed in NULL\n");
        return OSIX_FAILURE;
    }
    /* Get the Context Id and the Local Port Id from the IfIndex */
    if (MrpPortGetContextInfoFromIfIndex (pMrpInfo->u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                     "MrpApiNotifyStpInfo: Failed to Get Context Id\n");
        return OSIX_FAILURE;
    }

    switch (pMrpInfo->u1Flag)
    {
        case MSG_PORT_ROLE_CHANGE:
            MRP_NOTIFY_PORT_ROLE_CHANGE (pMrpInfo->u4IfIndex,
                                         pMrpInfo->u2MapId, pMrpInfo->u1OldRole,
                                         pMrpInfo->u1NewRole);
            break;

        case MSG_PORT_STATE_CHANGE:
            MRP_NOTIFY_PORT_STATE_CHANGE (pMrpInfo->u4IfIndex,
                                          pMrpInfo->u2MapId,
                                          pMrpInfo->u1PortState);

            break;

        case MSG_PORT_OPER_P2P:
            MRP_NOTIFY_PORT_P2P (pMrpInfo->u4IfIndex, pMrpInfo->b1TruthVal);
            break;

        case MSG_TC_DETECTED_TMR_STATUS:
            MRP_NOTIFY_TC_DETECTED_TMR_STATE (pMrpInfo->u4IfIndex,
                                              pMrpInfo->u2MapId,
                                              (UINT1) pMrpInfo->b1TruthVal);
            break;

        default:
            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), ALL_FAILURE_TRC,
                      "MrpApiNotifyStpInfo: Invalid Message type received "
                      "failed\n"));
            i4RetVal = OSIX_FAILURE;
            break;

    }

    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiMrpConfigInfo 
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to do the following.
                            MMRP_ENABLED_STATUS_GET 
                            MMRP_ENABLED_STATUS_SET
                            MMRP_ENABLED_STATUS_TEST
                            MVRP_ENABLED_STATUS_GET
                            MVRP_ENABLED_STATUS_SET
                            MVRP_ENABLED_STATUS_TEST
                            MRP_VALIDATE_MRP_TABLE
                            MRP_GET_NEXT_INDEX_MRP_TABLE
                            MVRP_PORT_ENABLED_STATUS_GET
                            MVRP_PORT_FAILED_REGIST_GET
                            MVRP_PORT_PDU_ORIGIN_GET
                            MVRP_PORT_ENABLED_STATUS_SET
                            MVRP_PORT_ENABLED_STATUS_TEST
 *
 *    INPUT            : pMrpConfigInfo - Pointer to the structure which
 *                                  contains the MRP info from the above
 *                                  modules.
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpApiMrpConfigInfo (tMrpConfigInfo * pMrpConfigInfo)
{
    INT4                i4RetVal = OSIX_FAILURE;

    MRP_LOCK ();

    switch (pMrpConfigInfo->u4InfoType)
    {
        case MMRP_ENABLED_STATUS_GET:
            i4RetVal = MrpUtilGetMmrpEnabledStatus
                (&(pMrpConfigInfo->unMrpInfo.MmrpStatusInfo));
            MRP_UNLOCK ();
            return i4RetVal;

        case MMRP_ENABLED_STATUS_SET:
            i4RetVal = MrpUtilSetMmrpEnabledStatus
                (&(pMrpConfigInfo->unMrpInfo.MmrpStatusInfo));
            MRP_UNLOCK ();
            return i4RetVal;

        case MMRP_ENABLED_STATUS_TEST:
            i4RetVal = MrpUtilTestMmrpEnabledStatus
                (&(pMrpConfigInfo->unMrpInfo.MmrpStatusInfo));
            MRP_UNLOCK ();
            return i4RetVal;

        case MVRP_ENABLED_STATUS_GET:
            i4RetVal = MrpUtilGetMvrpEnabledStatus
                (&(pMrpConfigInfo->unMrpInfo.MvrpStatusInfo));
            MRP_UNLOCK ();
            return i4RetVal;

        case MVRP_ENABLED_STATUS_SET:
            i4RetVal = MrpUtilSetMvrpEnabledStatus
                (&(pMrpConfigInfo->unMrpInfo.MvrpStatusInfo));
            MRP_UNLOCK ();
            return i4RetVal;

        case MVRP_ENABLED_STATUS_TEST:
            i4RetVal = MrpUtilTestMvrpEnabledStatus
                (&(pMrpConfigInfo->unMrpInfo.MvrpStatusInfo));
            MRP_UNLOCK ();
            return i4RetVal;

        case MVRP_PORT_ENABLED_STATUS_GET:
            i4RetVal = MrpUtilGetPortMvrpEnabledStatus
                (&(pMrpConfigInfo->unMrpInfo.MvrpPortInfo));
            MRP_UNLOCK ();
            return i4RetVal;

        case MVRP_PORT_FAILED_REGIST_GET:
            i4RetVal = MrpUtilGetPortMvrpFailedRegist
                (&(pMrpConfigInfo->unMrpInfo.MvrpPortInfo));
            MRP_UNLOCK ();
            return i4RetVal;

        case MVRP_PORT_PDU_ORIGIN_GET:
            i4RetVal = MrpUtilGetPortMvrpLastPduOrigin
                (&(pMrpConfigInfo->unMrpInfo.MvrpPortInfo));
            MRP_UNLOCK ();
            return i4RetVal;

        case MVRP_PORT_ENABLED_STATUS_SET:
            i4RetVal = MrpUtilSetPortMvrpEnabledStatus
                (&(pMrpConfigInfo->unMrpInfo.MvrpPortInfo));
            MRP_UNLOCK ();
            return i4RetVal;

        case MVRP_PORT_ENABLED_STATUS_TEST:
            i4RetVal = MrpUtilTestPortMvrpEnabledStatus
                (&(pMrpConfigInfo->unMrpInfo.MvrpPortInfo));
            MRP_UNLOCK ();
            return i4RetVal;
        default:
            break;
    }
    MRP_UNLOCK ();
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpapi.c                       */
/*-----------------------------------------------------------------------*/
