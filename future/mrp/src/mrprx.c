/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrprx.c,v 1.8 2013/06/25 12:08:38 siva Exp $
 *
 * Description: This file contains all the MRPPDU processing procedures. 
 *
 *****************************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxValidatePDU
 *
 *    DESCRIPTION      : This function validates the MRPDU and discards the
 *                       MRPDU if it is malformed.
 *
 *    INPUT            : pMrpPduBuff  - Pointer to the first Message in the 
 *                                      received MRPDU.
 *                       pIfMsg       - Pointer to IfMsg Structure
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
MrpRxValidatePDU (UINT1 *pMrpPduBuff, tMrpIfMsg * pIfMsg)
{
    tMrpContextInfo    *pContextInfo = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2Offset = 0;
    UINT2               u2VectAttrLen = 0;
    UINT2               u2Value = 0;
    UINT2               u2RemPduSize = 0;
    UINT1               u1AppId = 0;
    UINT1               u1DiscardPDU = OSIX_FALSE;
    UINT1               u1AttrType = 0;
    UINT1               u1AttrLen = 0;
    UINT1              *pMrpPdu = NULL;
    UINT1               u1EndofPDU = OSIX_FALSE;

    pContextInfo = MRP_CONTEXT_PTR (pIfMsg->u4ContextId);
    pMrpPdu = pMrpPduBuff;
    u1AppId = pIfMsg->pAppEntry->u1AppId;
    u2RemPduSize = pIfMsg->u2PktLen;    /* Total PDU length - MRP HDR length */

    if ((0 == u1AppId) || (u1AppId > MRP_MAX_APPS))
    {
        /* Added to avoid Klocwork warning */
        return OSIX_FAILURE;
    }

    MRP_TRC ((pContextInfo, MRP_PDU_TRC,
              "MrpRxValidatePDU: Validating the reiceived MRPDU"
              "for Application Id %d\n", u1AppId));

    while (u2RemPduSize)
    {
        MRP_LBUF_GET_1_BYTE (pMrpPdu, u2Offset, u1AttrType);

        /* Section 10.8.2.2 of IEEE 802.1ak-2007 says that attribute type 0
         * shall not be used by any Application.*/

        if (MRP_INVAID_ATTR_TYPE == u1AttrType)
        {

            MRP_TRC ((pContextInfo, MRP_PDU_TRC,
                      "MrpRxValidatePDU: Received invalid Attribute Type"
                      " for Application Id %d\n", u1AppId));

            return OSIX_FAILURE;
        }

        MRP_LBUF_GET_1_BYTE (pMrpPdu, u2Offset, u1AttrLen);

        i4RetVal =
            gMrpGlobalInfo.aMrpAppnFn[u1AppId].pAttrTypeValidateFn (u1AttrType);

        if (OSIX_TRUE == i4RetVal)
        {
            /* The Attribute Length needs to be validated only for known 
             * Attribute types.
             */
            i4RetVal =
                gMrpGlobalInfo.aMrpAppnFn[u1AppId].
                pAttrLenValidateFn (u1AttrType, u1AttrLen);
            if (OSIX_FALSE == i4RetVal)
            {

                MRP_TRC ((pContextInfo, MRP_PDU_TRC,
                          "MrpRxValidatePDU: Received invalid Attribute "
                          "Length for Application Id %d\n", u1AppId));

                return OSIX_FAILURE;
            }

        }

        u2RemPduSize = (UINT2) (u2RemPduSize - u2Offset);
        /* Get the Vector Header */
        MRP_LBUF_GET_2_BYTES (pMrpPdu, u2Offset, u2Value);

        while (u2Value != MRP_END_MARK_VALUE)
        {
            MrpRxGetVectorAttrLen (u2Value, u1AttrLen, &u2VectAttrLen);
            if (u2RemPduSize < u2VectAttrLen)
            {
                u1DiscardPDU = OSIX_TRUE;
                break;
            }

            /* Move the PDU buffer pointer to the end of the Vector Attribute.
             * PDU buffer is pointing to the First Value field. Hence 
             * MRP_VECTOR_HDR_SIZE needs to be decreased from the
             * u2VectAttrLen.
             */
            pMrpPdu = pMrpPdu + (u2VectAttrLen - MRP_VECTOR_HDR_SIZE);

            u2RemPduSize = (UINT2) (u2RemPduSize - u2VectAttrLen);

            if (0 == u2RemPduSize)
            {
                u1EndofPDU = OSIX_TRUE;
                break;
            }
            MRP_LBUF_GET_2_BYTES (pMrpPdu, u2Offset, u2Value);
            u2VectAttrLen = 0;
        }

        if ((OSIX_FALSE == u1DiscardPDU) && (OSIX_FALSE == u1EndofPDU))
        {
            /* End of a Message */
            u2RemPduSize = (UINT2) (u2RemPduSize - MRP_END_MARK_SIZE);
            /* Get next two bytes to check if End Of the PDU has reached */
            MRP_LBUF_GET_2_BYTES (pMrpPdu, u2Offset, u2Value);
            if (u2Value == MRP_END_MARK_VALUE)
            {
                /* End of PDU has reached */
                break;
            }
            else if (u2RemPduSize < MRP_MIN_MSG_LEN)
            {
                /* Only if the last Vector Attribute is incomplete, the PDU needs
                 * to discarded. In this case, the previous messages can be 
                 * processed.
                 */
                u1DiscardPDU = OSIX_TRUE;
                break;
            }
            else
            {
                MRP_LBUF_DEC_BY_2_BYTES (pMrpPdu, u2Offset);
            }
        }
        u2Offset = 0;
    }

    if (OSIX_TRUE == u1DiscardPDU)
    {

        MRP_TRC ((pContextInfo, MRP_PDU_TRC,
                  "MrpRxValidatePDU: Last Vector Attribute is "
                  "incomplete in the MRPDU for Application Id %d. Hence"
                  " discarding the PDU \n", u1AppId));

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxGetVectorAttrLen
 *
 *    DESCRIPTION      : This function returns the length of the Vector 
 *                       Attribute.
 *
 *    INPUT            : u2VectorHdr     - Vector Header value
 *                       u1FirstValLen   - Length of First Value Field
 *
 *    OUTPUT           : *pu2VectAttrLen - Length of the Vector Attribute
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
VOID
MrpRxGetVectorAttrLen (UINT2 u2VectorHdr, UINT1 u1FirstValLen,
                       UINT2 *pu2VectAttrLen)
{
    UINT2               u2AttrNum = 0;
    UINT2               u2NoOfVectors = 0;

    /* Get the number of Attributes which are encoded in this Vector Attribute
     */
    MRP_GET_NUM_OF_VAL_FIELD (u2VectorHdr, u2AttrNum);
    u2NoOfVectors = (UINT2) (MRP_GET_NUM_OF_VECTORS_FIELD (u2AttrNum));
    *pu2VectAttrLen = (UINT2) (MRP_VECTOR_HDR_SIZE + u1FirstValLen +
                               u2NoOfVectors);
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxApplyPDU 
 *
 *    DESCRIPTION      : This function decodes the Attribute Value and the 
 *                       corresponding Attribute Events. Updates the State
 *                       Machine States and gives indication to the higher 
 *                       layers if required.
 *
 *   INPUT             : pMrpPdu     - Pointer to first Message in the MRPDU 
 *                       pIfMsg      - Pointer to IfMsg structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
MrpRxApplyPDU (UINT1 *pMrpPdu, tMrpIfMsg * pIfMsg)
{
    tMrpRedMsgInfo      SyncUpMsgInfo;
    INT4                i4RetVal = OSIX_TRUE;
    UINT2               u2Offset = 0;
    UINT2               u2RemPduSize = 0;
    UINT2               u2Value = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1AttrType = 0;
    UINT1               u1AttrLen = 0;
    UINT1               u1AppId = 0;
    UINT1               u1IsLvAllEvntHandled = OSIX_FALSE;
    UINT1               u1RedMsgType = MRP_RED_VLAN_ADD_MSG;

    u2RemPduSize = pIfMsg->u2PktLen;
    u1AppId = pIfMsg->pAppEntry->u1AppId;

    if ((0 == u1AppId) || (u1AppId > MRP_MAX_APPS))
    {
        /* Added to avoid Klocwork warning */
        return OSIX_FAILURE;
    }

    while (u2RemPduSize)
    {
        MRP_LBUF_GET_1_BYTE (pMrpPdu, u2Offset, u1AttrType);

        MRP_LBUF_GET_1_BYTE (pMrpPdu, u2Offset, u1AttrLen);

        u2RemPduSize = (UINT2) (u2RemPduSize - u2Offset);

        i4RetVal =
            gMrpGlobalInfo.aMrpAppnFn[u1AppId].pAttrTypeValidateFn (u1AttrType);

        while (OSIX_FALSE == i4RetVal)
        {
            MRP_TRC ((MRP_CONTEXT_PTR (pIfMsg->u4ContextId),
                      MRP_PDU_TRC, "MrpRxApplyPDU: Unrecognized "
                      "Attribute type received for AppId %d on port %d\n",
                      u1AppId, pIfMsg->u2Port));
            /* Skip this message as the Attribute Type is Unrecognized */
            MrpRxSkipMessage (pMrpPdu, u1AttrLen, &u2RemPduSize, &u2MsgLen);

            if (0 == u2RemPduSize)
            {
                /* End Mark to denote the end of the PDU is not present. Hence 
                 * return. 
                 */
                return OSIX_SUCCESS;
            }
            else
            {
                pMrpPdu += u2MsgLen;
                /* Check whether this was the last message in the MRPDU */
                u2Offset = 0;
                MRP_LBUF_GET_2_BYTES (pMrpPdu, u2Offset, u2Value);

                if (u2Value == MRP_END_MARK_VALUE)
                {
                    /* End of the PDU has reached. Hence return. */
                    return OSIX_SUCCESS;
                }
                else
                {
                    /* There are other messages to be processed. */
                    MRP_LBUF_DEC_BY_2_BYTES (pMrpPdu, u2Offset);
                }
            }
            /* Get the Attribute Type of the next Message */
            u2Offset = 0;
            MRP_LBUF_GET_1_BYTE (pMrpPdu, u2Offset, u1AttrType);

            MRP_LBUF_GET_1_BYTE (pMrpPdu, u2Offset, u1AttrLen);

            u2RemPduSize = (UINT2) (u2RemPduSize - u2Offset);
            i4RetVal =
                gMrpGlobalInfo.aMrpAppnFn[u1AppId].
                pAttrTypeValidateFn (u1AttrType);
        }

        u2Offset = 0;
        MRP_LBUF_GET_2_BYTES (pMrpPdu, u2Offset, u2Value);

        while (u2Value != MRP_END_MARK_VALUE)
        {
            MrpRxDecodeVectorAttribute (pMrpPdu, u2Value, pIfMsg, u1AttrType,
                                        u1AttrLen, &u2Offset,
                                        &u1IsLvAllEvntHandled);

            /* Move the PDU buffer pointer to the end of the Vector Attribute.
             * PDU buffer is pointing to the First Value field. Hence 
             * MRP_VECTOR_HDR_SIZE needs to be decreased from the
             * u2VectAttrLen.
             */
            pMrpPdu = pMrpPdu + (u2Offset - MRP_VECTOR_HDR_SIZE);

            u2RemPduSize = (UINT2) (u2RemPduSize - u2Offset);
            if (0 == u2RemPduSize)
            {
                /* End of PDU has reached. Hence return. */
                return OSIX_SUCCESS;
            }

            u2Offset = 0;
            MRP_LBUF_GET_2_BYTES (pMrpPdu, u2Offset, u2Value);
        }
        u2RemPduSize = (UINT2) (u2RemPduSize - u2Offset);
        MRP_LBUF_GET_2_BYTES (pMrpPdu, u2Offset, u2Value);
        if (u2Value == MRP_END_MARK_VALUE)
        {
            break;
        }
        else
        {
            MRP_LBUF_DEC_BY_2_BYTES (pMrpPdu, u2Offset);
        }
        u2Offset = 0;
    }

    /* Form SyncUp message for sending the registered Attribute values to
     * the standby node */
    MEMSET (&SyncUpMsgInfo, 0, sizeof (tMrpRedMsgInfo));
    SyncUpMsgInfo.u1EndOfPdu = OSIX_TRUE;

    /* Only VLAN and MAC Address information will be accumulated and send.
     * Service requirement will be sent as and when it is processed. */
    if (MRP_MMRP_APP_ID == u1AppId)
    {
        u1RedMsgType = MRP_RED_MAC_ADD_MSG;
    }

    MrpRedSendSyncUpMessages (u1RedMsgType, &SyncUpMsgInfo);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxSkipMessage
 *
 *    DESCRIPTION      : This function is used for skipping the Message as the
 *                       Attribute Type is unrecognized in this Protocol 
 *                       Version.
 *
 *    INPUT            : pVectAttr       - Pointer to the start of Vector
 *                                         Attribute
 *                       u1FirstValLen   - Length of First Value Field
 *                       *pu2PduLen      - MRPDU Length
 *
 *    OUTPUT           : *pu2PduLen      - Remaining PDU Length
 *                       *pu2MsgLen      - Message Length
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
VOID
MrpRxSkipMessage (UINT1 *pVectAttr, UINT1 u1FirstValLen, UINT2 *pu2PduLen,
                  UINT2 *pu2MsgLen)
{
    UINT2               u2Offset = 0;
    UINT2               u2VectAttrLen = 0;
    UINT2               u2Value = 0;

    MRP_LBUF_GET_2_BYTES (pVectAttr, u2Offset, u2Value);

    while (u2Value != MRP_END_MARK_VALUE)
    {
        MrpRxGetVectorAttrLen (u2Value, u1FirstValLen, &u2VectAttrLen);

        *pu2PduLen = (UINT2) (*pu2PduLen - u2VectAttrLen);

        if (*pu2PduLen == 0)
        {
            break;
        }

        /* pVectAttr buffer is pointing to the First Value field. Hence 
         * MRP_VECTOR_HDR_SIZE needs to be decreased from the u2VectAttrLen
         * and move pVectAttr to point to the end of this Vector Atribute.
         */

        pVectAttr = pVectAttr + (u2VectAttrLen - MRP_VECTOR_HDR_SIZE);
        MRP_LBUF_GET_2_BYTES (pVectAttr, u2Offset, u2Value);
    }
    *pu2PduLen = (UINT2) (*pu2PduLen - MRP_END_MARK_SIZE);
    *pu2MsgLen = (UINT2) (u2VectAttrLen + MRP_END_MARK_SIZE);
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxDecodeVectorAttribute
 *
 *    DESCRIPTION      : This function decodes the Vector Attribute and calls 
 *                       the State Machines.
 *
 *    INPUT            : pVectAttr       - Pointer to the First Value Field in
 *                                         the Vector Attribute
 *                       u2VectHdr       - Vector Header
 *                       pIfMsg          - IfMsg Structure pointer
 *                       u1AttrType      - Attribute type
 *                       u1AttrLen       - Attribute length
 *
 *    OUTPUT           : pMrpPdu         - Points to the end of this Vector 
 *                                         Attribute
 *                       *pu2VectAttrLen - Length of the PDU that has been 
 *                                         decoded
 *                       *pu1IsLvAllEvntHandled - Indicates whether LvAll event 
 *                                                had been handled while 
 *                                                processing the MRPDU.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
MrpRxDecodeVectorAttribute (UINT1 *pMrpPdu, UINT2 u2VectHdr, tMrpIfMsg * pIfMsg,
                            UINT1 u1AttrType, UINT1 u1AttrLen,
                            UINT2 *pu2VectAttrLen, UINT1 *pu1IsLvAllEvntHandled)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpAttr            Attr;
    tMrpTrapInfo        TrapInfo;
    INT4                i4RetVal = OSIX_TRUE;
    UINT4               u4ContextId = 0;
    UINT2               u2Offset = 0;
    UINT2               u2AttrNum = 0;
    tVlanId             VlanId = 0;
    UINT2               u2Port = 0;
    UINT2               u2NoOfVectors = 0;
    UINT1               u1LvAllEvent = 0;
    UINT1               u1AppId = 0;

    MEMSET (&Attr, 0, sizeof (Attr));
    MEMSET (&TrapInfo, 0, sizeof (tMrpTrapInfo));
    u4ContextId = pIfMsg->u4ContextId;
    u1AppId = pIfMsg->pAppEntry->u1AppId;
    u2Port = pIfMsg->u2Port;
    pPortEntry = MRP_GET_PORT_ENTRY (MRP_CONTEXT_PTR (u4ContextId), u2Port);
    MRP_CHK_NULL_PTR_RET (pPortEntry, OSIX_FAILURE);
    MRP_DECODE_LV_ALL_EVENT (u2VectHdr, u1LvAllEvent);

    if (MRP_PDU_LV_ALL_EVENT == u1LvAllEvent)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_PDU_TRC,
                  "MrpRxDecodeVectorAttribute: LEAVE ALL Pdu received on "
                  "port %d for MAP %d for App Id %d\n", u2Port,
                  pIfMsg->u2MapId, u1AppId));
        /* Leave All event should be handled only once though it may be encoded
         * multiple times in the MRPDU 
         */
        if (OSIX_FALSE == *pu1IsLvAllEvntHandled)
        {
            MrpRxHandleLvAllEvent (pIfMsg);
            *pu1IsLvAllEvntHandled = OSIX_TRUE;
        }
        MRP_INCR_RX_LV_ALL_MSG_CNT (pPortEntry, u1AppId);
    }
    else if (u1LvAllEvent > MRP_PDU_LV_ALL_EVENT)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  MRP_PDU_TRC, "MrpRxDecodeVectorAttribute: Unrecognized "
                  "Leave All Event value received for AppId %d on port"
                  " %d. Hence ignoring the event \n", u1AppId, u2Port));

    }

    /* Get the number of attribute values that are encoded in this Vector 
     * Attribute
     */
    MRP_GET_NUM_OF_VAL_FIELD (u2VectHdr, u2AttrNum);
    u2NoOfVectors = (UINT2) (MRP_GET_NUM_OF_VECTORS_FIELD (u2AttrNum));

    Attr.u1AttrType = u1AttrType;
    Attr.u1AttrLen = u1AttrLen;

    /* Get the First Value Field */
    if (u1AttrLen <= MRP_MAX_ATTR_LEN)
    {
        MRP_LBUF_GET_N_BYTES (pMrpPdu, Attr.au1AttrVal, u1AttrLen, u2Offset);
    }
    if (u1AppId == MRP_MVRP_APP_ID)
    {
        /* if the attribute value in MVRPDU is mapped to ESP 
         * VLAN list in this context then should not register that attribute */
        MEMCPY (&VlanId, Attr.au1AttrVal, MRP_VLAN_ID_LEN);
        VlanId = (tVlanId) OSIX_NTOHS (VlanId);

        if (MrpPortPbbTeVidIsEspVid (u4ContextId, VlanId) == OSIX_TRUE)
        {
            return OSIX_FAILURE;
        }
        /* If the MVRPDU is received in a Virtual Instance Port of I-BEB 
         * component in a PBBN then do not allow register that MVRP 
         * attribute */
        if (pPortEntry->u2BridgePortType == MRP_VIRTUAL_INSTANCE_PORT)
        {
            return OSIX_FAILURE;
        }
    }
    /* For MVRP, update the MapId after validating the Vlan ID. */
    i4RetVal = MrpRxIsAttributeValid (&Attr, pIfMsg);

    if ((OSIX_FALSE == i4RetVal) && (u2AttrNum == 1))
    {
        if (((u1AppId == MRP_MVRP_APP_ID) &&
             (Attr.u1AttrType == MRP_VID_ATTR_TYPE)) ||
            ((u1AppId == MRP_MMRP_APP_ID) &&
             (Attr.u1AttrType == MRP_MAC_ADDR_ATTR_TYPE)))
        {
            /* Send the trap as the Attribute Value is not valid */
            TrapInfo.pAttr = &Attr;
            TrapInfo.u4ContextId = u4ContextId;
            TrapInfo.u2PortId = u2Port;
            TrapInfo.u1ReasonType = MRP_UNSUPPORTED_ATTR_VAL;
            MrpTrapNotifyVlanOrMacRegFails ((VOID *) &TrapInfo);

        }
        /* Move the pointer to the next Vector Attribute in the Attribute
         * List.
         */
        pMrpPdu = pMrpPdu + u2NoOfVectors;
        u2Offset = (UINT2) (u2Offset + u2NoOfVectors);
        *pu2VectAttrLen = (UINT2) (*pu2VectAttrLen + u2Offset);
        return OSIX_SUCCESS;
    }
    MrpRxDecodeAttrEvents (pMrpPdu, &Attr, u2AttrNum, u2NoOfVectors,
                           pIfMsg, (UINT1) i4RetVal);
    u2Offset = (UINT2) (u2Offset + u2NoOfVectors);
    *pu2VectAttrLen = (UINT2) (*pu2VectAttrLen + u2Offset);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxIsAttributeValid
 *
 *    DESCRIPTION      : This function validates attributes in MVRPDUs and
 *                       MMRPDUs.
 *
 *    INPUT            : pAttr       - Pointer to the Attribute strcuture
 *                                     containing the First value encoded
 *                                     in the Vector Attribute
 *                       pIfMsg      - Pointer to the IfMsg Structure
 *
 *    OUTPUT           : None 
 *                       
 *
 *    RETURNS          : OSIX_TRUE / OSIX_FALSE 
 *
 ****************************************************************************/
INT4
MrpRxIsAttributeValid (tMrpAttr * pAttr, tMrpIfMsg * pIfMsg)
{
    INT4                i4RetVal = OSIX_TRUE;
    UINT1               u1AppId = 0;

    u1AppId = pIfMsg->pAppEntry->u1AppId;

    if (MRP_MVRP_APP_ID == u1AppId)
    {
        i4RetVal = MrpRxIsMvrpAttrValueValid (pAttr, pIfMsg);
    }
    else if (MRP_MMRP_APP_ID == u1AppId)
    {
        i4RetVal = MrpRxIsMmrpAttrValueValid (pAttr);
    }

    return i4RetVal;
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxDecodeAttrEvents
 *
 *    DESCRIPTION      : This function decodes the Attribute Events for the 
 *                       corresponding Attribute Values and updates the 
 *                       State Machines and higher layers.
 *
 *    INPUT            : pVector         - Pointer to the start of the
 *                                         Vector Field in the Vector Attribute
 *                       pAttr           - Contains information about the 
 *                                         Attribute in the First Value Field
 *                       u2AttrNum       - Number of Attribute Values whose 
 *                                         events are encoded in this Vector
 *                                         Attribute
 *                       u2NoOfVectors   - Number of Vector Fields in this 
 *                                         Vector Attribute
 *                       pIfMsg          - Pointer to the MrpIfMsg Structure
 *                       u1ValidStatus   - OSIX_TRUE(If First Value field is
 *                                         valid else OSIX_FALSE)
 *
 *    OUTPUT           : pVector         - Points to the end of the Vector
 *                                         Attribute
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

VOID
MrpRxDecodeAttrEvents (UINT1 *pVector, tMrpAttr * pAttr, UINT2 u2AttrNum,
                       UINT2 u2NoOfVectors, tMrpIfMsg * pIfMsg,
                       UINT1 u1ValidStatus)
{
    tMrpTrapInfo        TrapInfo;
    UINT2               u2Offset = 0;
    UINT2               u2VectorCnt = 0;
    UINT1               u1Vector = 0;
    UINT1               u1AppId = 0;
    INT1                i1NumOfEvents = MRP_MAX_ATTR_EVENTS_PER_VECTOR - 1;
    UINT1               au1AttrEvents[MRP_MAX_ATTR_EVENTS_PER_VECTOR];

    u1AppId = pIfMsg->pAppEntry->u1AppId;

    MEMSET (&TrapInfo, 0, sizeof (tMrpTrapInfo));
    MEMSET (au1AttrEvents, 0, MRP_MAX_ATTR_EVENTS_PER_VECTOR);

    for (u2VectorCnt = 0; u2VectorCnt < u2NoOfVectors; u2VectorCnt++)
    {
        /* Get the Vector field */
        MRP_LBUF_GET_1_BYTE (pVector, u2Offset, u1Vector);
        MRP_GET_ATTR_EVENTS (u1Vector, au1AttrEvents);
        while (i1NumOfEvents >= 0)
        {
            /* Apply the attribute event to the first attribute value only if 
             * it is a valid value. Else send trap and decode the remaining
             * attributes values. If valid apply the corresponding attribute
             * event else send trap saying it as Unsupported type.
             */
            if (OSIX_TRUE == u1ValidStatus)
            {
                MrpRxApplyRcdEvent (pAttr, au1AttrEvents[i1NumOfEvents],
                                    pIfMsg);
            }
            else if (((u1AppId == MRP_MVRP_APP_ID) &&
                      (pAttr->u1AttrType == MRP_VID_ATTR_TYPE)) ||
                     ((u1AppId == MRP_MMRP_APP_ID) &&
                      (pAttr->u1AttrType == MRP_MAC_ADDR_ATTR_TYPE)))
            {
                /* Send the trap as the attribute value is not valid */
                TrapInfo.pAttr = pAttr;
                TrapInfo.u4ContextId = pIfMsg->u4ContextId;
                TrapInfo.u2PortId = pIfMsg->u2Port;
                TrapInfo.u1ReasonType = MRP_UNSUPPORTED_ATTR_VAL;
                MrpTrapNotifyVlanOrMacRegFails ((VOID *) &TrapInfo);
            }
            i1NumOfEvents--;
            u2AttrNum--;

            if (u2AttrNum == 0)
            {
                return;
            }
            /* Get the next attribute value and check if it is a VALID value. */
            u1ValidStatus = (UINT1) MrpRxGetNextAttr (pAttr, pIfMsg);
        }
        i1NumOfEvents = MRP_MAX_ATTR_EVENTS_PER_VECTOR - 1;
    }
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxApplyRcdEvent
 *
 *    DESCRIPTION      : This function updates the State Machines and 
 *                       higher layers for the given Attribute Value and Event.
 *
 *    INPUT            : pAttr           - Contains information about the 
 *                                         Attribute 
 *                       u1AttrEvent     - Attribute Event (New / JoinIn / In /
 *                                         JoinMt / Mt / Lv)
 *                       pIfMsg          - Pointer to the MrpIfMsg Structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
MrpRxApplyRcdEvent (tMrpAttr * pAttr, UINT1 u1AttrEvent, tMrpIfMsg * pIfMsg)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpTrapInfo        TrapInfo;
    INT4                i4RetVal = OSIX_TRUE;
    UINT4               u4ContextId = 0;
    UINT2               u2Port = 0;
    UINT2               u2MapId = 0;
    UINT1               u1AppId = 0;
    UINT1               u1RestrictedRegControl = OSIX_FALSE;

    MEMSET (&TrapInfo, 0, sizeof (tMrpTrapInfo));
    u4ContextId = pIfMsg->u4ContextId;
    u2Port = pIfMsg->u2Port;
    u2MapId = pIfMsg->u2MapId;
    pPortEntry = MRP_GET_PORT_ENTRY (MRP_CONTEXT_PTR (u4ContextId), u2Port);
    MRP_CHK_NULL_PTR_RET (pPortEntry, OSIX_FAILURE);
    u1AppId = pIfMsg->pAppEntry->u1AppId;

    if ((0 == u1AppId) || (u1AppId > MRP_MAX_APPS))
    {
        /* Added to avoid Klocwork warning */
        return OSIX_FAILURE;
    }

    switch (u1AttrEvent)
    {
        case MRP_NEW_ATTR_EVENT:
            /* Intentional Fall through */
        case MRP_JOININ_ATTR_EVENT:
            /* Intentional Fall through */
        case MRP_JOINMT_ATTR_EVENT:
            i4RetVal =
                gMrpGlobalInfo.aMrpAppnFn[u1AppId].
                pAttrWildCardValidateFn (u4ContextId, *pAttr, u2Port, u2MapId);
            if (OSIX_TRUE == i4RetVal)
            {
                if (((u1AppId == MRP_MMRP_APP_ID) &&
                     (pAttr->u1AttrType == MRP_MAC_ADDR_ATTR_TYPE)) ||
                    ((u1AppId == MRP_MVRP_APP_ID) &&
                     (pAttr->u1AttrType == MRP_VID_ATTR_TYPE)))
                {
                    /* Restricted Registration is applicable only for
                     * MAC Address and VLAN */
                    u1RestrictedRegControl =
                        (UINT1) MRP_GET_RESTRICTED_REG_CTRL (u1AppId,
                                                             pPortEntry);
                }

                if (OSIX_TRUE == u1RestrictedRegControl)
                {

                    pMapEntry = MRP_GET_MAP_ENTRY (pIfMsg->pAppEntry, u2MapId);
                    if ((NULL != pMapEntry) &&
                        (MrpAttrValidateAttrRegistration (pAttr, pMapEntry,
                                                          u2Port)) == OSIX_TRUE)
                    {
                        MrpRxHandleJoinOrNewEvent (pIfMsg, pAttr, u1AttrEvent);
                    }
                    else
                    {
                        /* Send the trap as Restricted Attribute 
                         * Registration parameter is set to TRUE.
                         */
                        TrapInfo.pAttr = pAttr;
                        TrapInfo.u4ContextId = u4ContextId;
                        TrapInfo.u2PortId = u2Port;
                        TrapInfo.u1ReasonType = MRP_RESTRICTED_REGISTRATION;
                        MrpTrapNotifyVlanOrMacRegFails ((VOID *) &TrapInfo);
                    }
                }
                else
                {
                    MrpRxHandleJoinOrNewEvent (pIfMsg, pAttr, u1AttrEvent);
                }
            }

            break;

        case MRP_IN_ATTR_EVENT:
            /* Intentional Fall through */
        case MRP_MT_ATTR_EVENT:
            /* Intentional Fall through */
        case MRP_LV_ATTR_EVENT:
            MrpRxHandleEvents (pAttr, pIfMsg, u1AttrEvent);
            break;

        default:
            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      MRP_PDU_TRC, "MrpRxApplyRcdEvent: Invalid"
                      " Attr Event received on port %d\n", u2Port));
            break;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxHandleEvents 
 *
 *    DESCRIPTION      : This function handles the IN, MT and LV events in the
 *                       received MRPDU.
 *
 *    INPUT            : pAttr           - Contains information about the 
 *                                         Attribute 
 *                       pIfMsg          - Pointer to the MrpIfMsg Structure
 *                       u1AttrEvent     - Attribute Event (In / Mt / Lv)
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
VOID
MrpRxHandleEvents (tMrpAttr * pAttr, tMrpIfMsg * pIfMsg, UINT1 u1AttrEvent)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2AttrIndex = 0;
    UINT2               u2MapId = 0;
    UINT2               u2Port = 0;
    UINT1               u1AppId = 0;
    UINT1               u1RegSEMStateChg = OSIX_FALSE;

    pAppEntry = pIfMsg->pAppEntry;
    u2MapId = pIfMsg->u2MapId;
    u2Port = pIfMsg->u2Port;
    u4ContextId = pIfMsg->u4ContextId;
    pPortEntry = MRP_GET_PORT_ENTRY (MRP_CONTEXT_PTR (u4ContextId), u2Port);

    if (NULL == pPortEntry)
    {
        /* Added to avoid Klocwork warning */
        return;
    }
    u1AppId = pAppEntry->u1AppId;

    pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

    if (NULL == pMapEntry)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_PDU_TRC,
                  "MrpRxHandleEvents: No Map Entry Exists for "
                  "MAP ID %d on port %d\n", u2MapId, u2Port));
        return;
    }

    pAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMapEntry);

    if (NULL == pAttrEntry)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_PDU_TRC,
                  "MrpRxHandleEvents: No Attribute Entry for MAP"
                  " ID %d on port %d\n", u2MapId, u2Port));
        MRP_PRINT_ATTRIBUTE (pAttr);
        return;
    }

    pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

    if (NULL == pMapPortEntry)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_PDU_TRC,
                  "MrpRxHandleEvents: No Port Entry for MAP ID"
                  " %d for port %d\n", u2MapId, u2Port));
        return;
    }

    pAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2Port);

    if (NULL == pAppPortEntry)
    {

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId), MRP_PDU_TRC,
                  "MrpRxHandleEvents: No AppPort Entry for"
                  " port %d\n", u2Port));
        return;
    }

    /* Get the position of the Attribute value in the AttrInfoList array */
    u2AttrIndex = pAttrEntry->u2AttrIndex;

    switch (u1AttrEvent)
    {
        case MRP_IN_ATTR_EVENT:
            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      MRP_PDU_TRC, "MrpRxHandleEvents: "
                      "Received IN Pdu on port %d for MAP %d\n",
                      u2Port, u2MapId));
            MrpSmApplicantSem (pMapPortEntry, u2AttrIndex, MRP_APP_RX_IN);
            MRP_INCR_RX_IN_MSG_CNT (pPortEntry, u1AppId);
            break;

        case MRP_MT_ATTR_EVENT:

            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      MRP_PDU_TRC, "MrpRxHandleEvents: "
                      "Received MT Pdu on port %d for MAP %d\n",
                      u2Port, u2MapId));
            MrpSmApplicantSem (pMapPortEntry, u2AttrIndex, MRP_APP_RX_MT);
            MRP_INCR_RX_EMPTY_MSG_CNT (pPortEntry, u1AppId);
            break;

        case MRP_LV_ATTR_EVENT:

            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      MRP_PDU_TRC, "MrpRxHandleEvents: "
                      "Received LV Pdu on port %d for MAP %d\n",
                      u2Port, u2MapId));

            if (MRP_GET_ATTR_REG_ADMIN_CTRL
                (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_REG_NORMAL)
            {
                MrpSmRegistrarSem (pMapPortEntry, u2AttrIndex,
                                   MRP_REG_RX_LEAVE, &u1RegSEMStateChg);
                if (u1RegSEMStateChg == OSIX_TRUE)
                {
                    /* Store the MRPDU originator's MAC address that had 
                     * resulted in the state change of Registrar State Machine
                     */
                    /* Store the index associated with the Peer MAC 
                     * Address */
                    MEMCPY (pAppPortEntry->pPeerMacAddrList[u2AttrIndex],
                            pIfMsg->SrcMacAddr, MRP_MAC_ADDR_LEN);
                }

            }
            MrpSmApplicantSem (pMapPortEntry, u2AttrIndex, MRP_APP_RX_LEAVE);
            MRP_INCR_RX_LEAVE_MSG_CNT (pPortEntry, u1AppId);
            break;

        default:
            break;
    }
    return;
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxHandleLvAllEvent
 *
 *    DESCRIPTION      : This function handles the Leave All Event in the 
 *                       received MRPDU.
 *
 *    INPUT            : pIfMsg   - Pointer to the MrpIfMsg Structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
VOID
MrpRxHandleLvAllEvent (tMrpIfMsg * pIfMsg)
{
    tMrpRedMsgInfo      SyncUpMsgInfo;
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2Index = 0;
    UINT2               u2Port = 0;
    UINT2               u2MapId = 0;
    UINT1              *pu1Attrptr = NULL;
    UINT1               u1RegSEMStateChg = OSIX_FALSE;

    u4ContextId = pIfMsg->u4ContextId;
    pAppEntry = pIfMsg->pAppEntry;
    u2Port = pIfMsg->u2Port;

    /* Apply the leave all event to the Applicant and Registrar SEM of all the 
     * attribute values of the Application for the port on which the PDU was
     * received. 
     */
    for (u2MapId = 0; u2MapId < pAppEntry->u2MapArraySize; u2MapId++)
    {
        pMapEntry = pAppEntry->ppMapTable[u2MapId];

        if (NULL == pMapEntry)
        {
            continue;
        }

        pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

        if (NULL == pMapPortEntry)
        {
            continue;
        }

        pAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2Port);

        if (NULL == pAppPortEntry)
        {
            continue;
        }

        pu1Attrptr = pMapPortEntry->pu1AttrInfoList;

        for (u2Index = 1; u2Index <= pMapPortEntry->u2AttrArraySize; u2Index++)
        {
            pAttrEntry = MRP_GET_ATTR_ENTRY (pMapEntry, u2Index);

            if (pAttrEntry == NULL)
            {
                continue;
            }
            /* Registrar SEM must be updated only if the Registrar Admin Control
             * is MRP_REG_NORMAL.
             * Applicant SEM must be updated irrespective of
             * the Registrar Admin Control value. 
             */
            if (MRP_REG_NORMAL ==
                MRP_GET_ATTR_REG_ADMIN_CTRL (pu1Attrptr[u2Index]))
            {
                MrpSmRegistrarSem (pMapPortEntry, u2Index,
                                   MRP_REG_RX_LEAVE_ALL, &u1RegSEMStateChg);
            }

            if (u1RegSEMStateChg == OSIX_TRUE)
            {
                /* Store the MRPDU originator's MAC address that had resulted in
                 * the state change of Registrar State Machine
                 */
                /* Store the index associated with the Peer MAC Address */
                MEMCPY (pAppPortEntry->pPeerMacAddrList[u2Index],
                        pIfMsg->SrcMacAddr, MRP_MAC_ADDR_LEN);
                u1RegSEMStateChg = OSIX_FALSE;
            }

            MrpSmApplicantSem (pMapPortEntry, u2Index, MRP_APP_RX_LEAVE_ALL);
        }
    }

    pAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2Port);

    if (NULL == pAppPortEntry)
    {
        return;
    }

    /* Restart the Leave All Timer Again */
    if (MrpTmrRestartLvAllTmr (pAppPortEntry) == OSIX_SUCCESS)
    {
        /* Update the Leave ALL SEM state */
        pAppPortEntry->u1LeaveAllSemState = MRP_PASSIVE;

        /* Send indication to Standby node to restart the leave all timer */
        MEMSET (&SyncUpMsgInfo, 0, sizeof (tMrpRedMsgInfo));
        SyncUpMsgInfo.u4ContextId = u4ContextId;
        SyncUpMsgInfo.u2LocalPortId = u2Port;
        SyncUpMsgInfo.u1AppId = pAppEntry->u1AppId;
        MrpRedSendSyncUpMessages (MRP_RED_LV_ALL_TMR_RESTART_MSG,
                                  &SyncUpMsgInfo);
    }

    return;
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxHandleJoinOrNewEvent
 *
 *    DESCRIPTION      : This function handles the received JoinIN or JoinMT 
 *                       or New event in the received MRPDU.
 *
 *    INPUT            : pIfMsg          - Pointer to the MrpIfMsg Structure
 *                       pAttr           - Contains information about the 
 *                                         Attribute 
 *                       u1AttrEventType - Attribute Event (New / JoinIn /
 *                                         JoinMt)
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None 
 *
 ****************************************************************************/
VOID
MrpRxHandleJoinOrNewEvent (tMrpIfMsg * pIfMsg, tMrpAttr * pAttr,
                           UINT1 u1AttrEventType)
{
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpTrapInfo        TrapInfo;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpAttrEntry      *pAttrEntry = NULL;
    tMrpAppEntry       *pAppEntry = NULL;
    tVlanId             VlanId = 0;
    UINT4               u4FdbId = 0;
    UINT4               u4ContextId = MRP_INVALID_CONTEXT_ID;
    UINT2               u2AttrIndex = 0;
    UINT2               u2MapId = 0;
    UINT2               u2Port = 0;
    UINT1               u1AppId = 0;
    UINT1               u1RegSemEvent = 0;
    UINT1               u1AppSemEvent = 0;
    UINT1               u1RegAdmCtrl = 0;
    UINT1               u1RegSEMStateChg = OSIX_FALSE;

    MEMSET (&TrapInfo, 0, sizeof (tMrpTrapInfo));
    pAppEntry = pIfMsg->pAppEntry;
    u4ContextId = pIfMsg->u4ContextId;
    u1AppId = pIfMsg->pAppEntry->u1AppId;
    u2MapId = pIfMsg->u2MapId;
    u2Port = pIfMsg->u2Port;
    pPortEntry = MRP_GET_PORT_ENTRY (MRP_CONTEXT_PTR (u4ContextId), u2Port);

    if (NULL == pPortEntry)
    {
        /* Added to avoid Klocwork warning */
        return;
    }

    /* Get the MrpMapEntry using the MAP Id */
    pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);

    if (NULL == pMapEntry)
    {
        /* if it not present then create the MAP and initialise the 
         * values */
        pMapEntry = MrpMapDSCreateMrpMapEntry (pAppEntry, u2MapId);

        if (NULL == pMapEntry)
        {
            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "MrpRxHandleJoinOrNewEvent: No Free Map Entry \n"));
            return;
        }
    }

    pAttrEntry = MrpMapDSGetAttrEntryFrmMapEntry (pAttr, pMapEntry);
    pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);

    if (NULL == pMapPortEntry)
    {
        pMapPortEntry = MrpMapDSCreateMrpMapPortEntry (pMapEntry, u2Port);

        if (NULL == pMapPortEntry)
        {
            MrpMapDSDeleteMrpMapEntry (pMapEntry);
            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "MrpRxHandleJoinOrNewEvent:" "No Free Port Entry \n"));
            return;
        }
    }

    pAppPortEntry = MRP_GET_APP_PORT_ENTRY (pAppEntry, u2Port);

    if (pAppPortEntry == NULL)
    {
        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "MrpRxHandleJoinOrNewEvent:" "No App Port Entry \n"));
        return;
    }

    if (NULL == pAttrEntry)
    {
        pAttrEntry = MrpMapDSCreateMrpMapAttrEntry (pMapEntry, pAttr);

        if (NULL == pAttrEntry)
        {
            TrapInfo.pAttr = pAttr;
            TrapInfo.u4ContextId = u4ContextId;
            TrapInfo.u2PortId = u2Port;
            TrapInfo.u1ReasonType = MRP_LACK_OF_RESOURCE;
            MrpTrapNotifyVlanOrMacRegFails ((VOID *) &TrapInfo);

            if (MRP_MVRP_APP_ID == pAppEntry->u1AppId)
            {
                pPortEntry->u4MvrpRegFailCnt++;
            }
            else
            {
                pPortEntry->u4MmrpRegFailCnt++;
            }
            MrpMapDSDeleteMrpMapPortEntry (pMapPortEntry);
            MrpMapDSDeleteMrpMapEntry (pMapEntry);
            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "MrpRxHandleJoinOrNewEvent:"
                      "No Free Attribute Entry \n"));
            return;

        }
    }

    if (MrpMapDSAddAttrToMapPortEntry (pMapPortEntry, pAttrEntry)
        != OSIX_SUCCESS)
    {
        MrpMapDSDelAttrEntryFromMapEntry (pMapEntry, pAttrEntry);
        MrpMapDSDeleteMrpMapPortEntry (pMapPortEntry);
        MrpMapDSDeleteMrpMapEntry (pMapEntry);

        MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "MrpRxHandleJoinOrNewEvent:"
                  "Adding attribute to MrpPortEntry Failed \n"));
        return;
    }

    /* Get the position of the attribute value in the Attribute array. */
    u2AttrIndex = pAttrEntry->u2AttrIndex;

    switch (u1AttrEventType)
    {
        case MRP_JOININ_ATTR_EVENT:

            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      MRP_PDU_TRC, "MrpRxHandleJoinOrNewEvent: JOIN IN "
                      "Pdu received on port %d for MAP %d\n", u2Port, u2MapId));

            u1RegSemEvent = MRP_REG_RX_JOIN_IN;
            u1AppSemEvent = MRP_APP_RX_JOIN_IN;
            MRP_INCR_RX_JOIN_IN_MSG_CNT (pPortEntry, u1AppId);
            break;

        case MRP_JOINMT_ATTR_EVENT:

            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      MRP_PDU_TRC, "MrpRxHandleJoinOrNewEvent: JOIN MT Pdu"
                      " received on port %d for MAP %d\n", u2Port, u2MapId));

            u1RegSemEvent = MRP_REG_RX_JOIN_MT;
            u1AppSemEvent = MRP_APP_RX_JOIN_MT;
            MRP_INCR_RX_JOIN_MT_MSG_CNT (pPortEntry, u1AppId);
            break;

        case MRP_NEW_ATTR_EVENT:

            MRP_TRC ((MRP_CONTEXT_PTR (u4ContextId),
                      MRP_PDU_TRC, "MrpRxHandleJoinOrNewEvent: NEW Pdu "
                      "received on port %d for MAP %d\n", u2Port, u2MapId));

            u1RegSemEvent = MRP_REG_RX_NEW;
            u1AppSemEvent = MRP_APP_RX_NEW;
            MRP_INCR_RX_NEW_MSG_CNT (pPortEntry, u1AppId);
            break;

        default:
            return;
    }

    u1RegAdmCtrl = MRP_GET_ATTR_REG_ADMIN_CTRL (pMapPortEntry->
                                                pu1AttrInfoList[u2AttrIndex]);
    if (MRP_REG_NORMAL == u1RegAdmCtrl)
    {
        if (MrpSmRegistrarSem (pMapPortEntry, u2AttrIndex, u1RegSemEvent,
                               &u1RegSEMStateChg) == OSIX_FAILURE)
        {
            return;
        }
        if (u1RegSEMStateChg == OSIX_TRUE)
        {
            /* Store the MRPDU originator's MAC address that had resulted in the
             * state change of Registrar State Machine
             */
            /* Store the index associated with the Peer MAC Address */
            MEMCPY (pAppPortEntry->pPeerMacAddrList[u2AttrIndex],
                    pIfMsg->SrcMacAddr, MRP_MAC_ADDR_LEN);
        }
    }
    MrpSmApplicantSem (pMapPortEntry, u2AttrIndex, u1AppSemEvent);

    /* Only MVRP makes use of the "new" declaration capability */
    if ((u1RegAdmCtrl == MRP_REG_NORMAL) &&
        (MRP_NEW_ATTR_EVENT == u1AttrEventType) &&
        (MRP_MVRP_APP_ID == pAppEntry->u1AppId))
    {
        MEMCPY (&VlanId, pAttr->au1AttrVal, MRP_VLAN_ID_LEN);
        VlanId = (tVlanId) OSIX_NTOHS (VlanId);
        u4FdbId = MrpPortL2IwfGetVlanFdbId (u4ContextId, VlanId);
        MrpPortVlanFlushFdbEntries (MRP_GET_PHY_PORT (u4ContextId, u2Port),
                                    u4FdbId);
    }
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpRxGetNextAttr 
 *
 *    DESCRIPTION      : This function returns the next Valid Attribute
 *
 *    INPUT            : pAttr           - Contains information about the 
 *                                         Attribute 
 *                       pIfMsg          - Pointer to the MrpIfMsg Structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_TRUE - If the next Attribute value is Valid
 *                       OSIX_FALSE - If the next Attribute value is Invalid
 *
 ****************************************************************************/
INT4
MrpRxGetNextAttr (tMrpAttr * pAttr, tMrpIfMsg * pIfMsg)
{
    tVlanId             VlanId = 0;
    tVlanId             TmpVlanId = 0;
    INT4                i4RetVal = OSIX_TRUE;

    if (MRP_MVRP_APP_ID == pIfMsg->pAppEntry->u1AppId)
    {
        MEMCPY (&VlanId, pAttr->au1AttrVal, MRP_VLAN_ID_LEN);
        VlanId = (tVlanId) OSIX_NTOHS (VlanId);
        VlanId++;
        TmpVlanId = (tVlanId) OSIX_HTONS (VlanId);
        MEMCPY (pAttr->au1AttrVal, &TmpVlanId, MRP_VLAN_ID_LEN);

        if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_FALSE)
        {
            MRP_TRC ((MRP_CONTEXT_PTR (pIfMsg->u4ContextId),
                      MRP_PDU_TRC,
                      "MrpRxGetNextAttr: \r\n!!Got Invalid VlanId on port %d\n",
                      pIfMsg->u2Port));

            i4RetVal = OSIX_FALSE;
        }
        else
        {
            /* If Vid translation is enabled, the get thre relay
             * vid and work on that relay vid. The following
             * function will change the vid in pAttr as well as
             * VlanId if the vid translation is enabled on that 
             * port.*/
            MrpUtilTranslateVID (pIfMsg->u4ContextId, pIfMsg->u2Port, pAttr,
                                 &VlanId);

            /* Update the MVRP attr's MapID from MSTI-VID mapping */
            pIfMsg->u2MapId =
                MrpPortL2IwfMiGetVlanInstMapping (pIfMsg->u4ContextId, VlanId);
        }
    }
    else
    {
        if (MRP_MAC_ADDR_ATTR_TYPE == pAttr->u1AttrType)
        {
            MrpUtilGetNextMAC (pAttr->au1AttrVal);
            if (MrpUtilCheckIfMACIsValid (pAttr->au1AttrVal) == OSIX_FALSE)
            {
                i4RetVal = OSIX_FALSE;
            }
        }
        else if (MRP_SERVICE_REQ_ATTR_TYPE == pAttr->u1AttrType)
        {
            pAttr->au1AttrVal[0] = (UINT1) (pAttr->au1AttrVal[0] + 1);
            if (pAttr->au1AttrVal[0] > MRP_MMRP_UNREG_GROUPS)
            {

                MRP_TRC ((MRP_CONTEXT_PTR (pIfMsg->u4ContextId),
                          MRP_PDU_TRC, "MrpRxGetNextAttr: "
                          "\r\n!! Got Invalid Service Requirement "
                          "Attributeon port %d\n", pIfMsg->u2Port));

                i4RetVal = OSIX_FALSE;
            }
        }
    }
    return i4RetVal;
}

/*************************************************************************
 *
 *  FUNCTION NAME   : MrpRxIsMvrpAttrValueValid 
 *
 *  DESCRIPTION     : This function checks whether the MVRP Attribute Value
 *                    is valid or not. If valid and if VID translation is 
 *                    enabled, then relay VID is obtained. Also this function
 *                    gets the MAP ID corresponding to this VLAN ID.
 *
 *  INPUT           : pAttr  - Pointer to Attribute Info structure  
 *                    pIfMsg - Pointer to IfMsg structure
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : OSIX_TRUE/OSIX_FALSE
 *
 **************************************************************************/
INT4
MrpRxIsMvrpAttrValueValid (tMrpAttr * pAttr, tMrpIfMsg * pIfMsg)
{
    tVlanId             VlanId = 0;
    INT4                i4RetVal = OSIX_FALSE;

    if (MRP_VID_ATTR_TYPE == pAttr->u1AttrType)
    {
        MEMCPY (&VlanId, pAttr->au1AttrVal, MRP_VLAN_ID_LEN);
        VlanId = (tVlanId) OSIX_NTOHS (VlanId);

        if (VLAN_IS_VLAN_ID_VALID (VlanId) == VLAN_TRUE)
        {
            /* If VID translation is enabled, then get the relay
             * VID and work on that relay VID. The following
             * function will change the VID in pAttr as well as
             * VlanId if the vid translation is enabled on that 
             * port.
             */
            MrpUtilTranslateVID (pIfMsg->u4ContextId, pIfMsg->u2Port, pAttr,
                                 &VlanId);
            /* Update the MVRP attr's MapID from MSTI-VID mapping */
            pIfMsg->u2MapId =
                MrpPortL2IwfMiGetVlanInstMapping (pIfMsg->u4ContextId, VlanId);
            i4RetVal = OSIX_TRUE;
        }
    }
    return i4RetVal;
}

/*************************************************************************
 *
 *  FUNCTION NAME   : MrpRxIsMmrpAttrValueValid 
 *
 *  DESCRIPTION     : This function checks whether the MMRP Attribute Value
 *                    is valid or not.
 *
 *  INPUT           : pAttr - Pointer to Attribute Info structure  
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : OSIX_TRUE/OSIX_FALSE
 *
 **************************************************************************/
INT4
MrpRxIsMmrpAttrValueValid (tMrpAttr * pAttr)
{
    INT4                i4RetVal = OSIX_TRUE;

    if (MRP_MAC_ADDR_ATTR_TYPE == pAttr->u1AttrType)
    {
        i4RetVal = MrpUtilCheckIfMACIsValid (pAttr->au1AttrVal);
    }
    else if (MRP_SERVICE_REQ_ATTR_TYPE == pAttr->u1AttrType)
    {
        if (pAttr->au1AttrVal[0] > MRP_MMRP_UNREG_GROUPS)
        {
            i4RetVal = OSIX_FALSE;
        }
    }
    return i4RetVal;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrprx.c                        */
/*-----------------------------------------------------------------------*/
