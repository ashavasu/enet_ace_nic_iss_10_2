/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpsrc.c,v 1.11 2016/03/17 12:37:24 siva Exp $
 *
 * Description: This file contains procedures for
 *              - Implementing Show Running configuration feature for MRP
 *********************************************************************/

#include "mrpinc.h"

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpSrcShowRunningConfigIfaceDetails
 *                                                                          
 *    DESCRIPTION      : This function displays the set of configurations that
 *                       have been provisioned over the given interface within
 *                       MRP context.
 *
 *    INPUT            : CliHandle - Handle to the cli context         
 *                       i4IfIndex - Specified interface index
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpSrcShowRunningConfigIfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{
    INT4                i4RestVlanReg = MRP_INVALID_VALUE;
    INT4                i4RestGpReg = MRP_INVALID_VALUE;
    UINT4               u4CompId = MRP_INVALID_CONTEXT_ID;
    UINT2               u2Port = MRP_INVALID_PORT;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    CliRegisterLock (CliHandle, MrpLock, MrpUnLock);
    MRP_LOCK ();

    if (MrpIfGetContextInfoFromIfIndex ((UINT4) i4IfIndex, &u4CompId, &u2Port)
        != OSIX_SUCCESS)
    {
        /* Invalid Port Identifier */
        MRP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }
    if (nmhValidateIndexInstanceFsMvrpPortTable (u4CompId,
                                                 (UINT4)
                                                 i4IfIndex) == SNMP_SUCCESS)
    {
        nmhGetFsMrpPortRestrictedVlanRegistration (u4CompId,
                                                   (UINT4)
                                                   i4IfIndex, &i4RestVlanReg);
        CfaCliConfGetIfName ((UINT4) i4IfIndex, piIfName);
        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
        if (i4RestVlanReg != MRP_DEF_RESTRICTED_VLAN_REG)
        {
            CliPrintf (CliHandle, "mrp vlan restricted enable\r\n");
        }

        nmhGetIeee8021BridgePortRestrictedGroupRegistration (u4CompId,
                                                             (UINT4)
                                                             i4IfIndex,
                                                             &i4RestGpReg);
        if (i4RestGpReg != MRP_DEF_GROUP_RESTRICTED_REG)
        {
            CliPrintf (CliHandle, "mrp mac-address restricted enable\r\n");
        }
    }

    MRP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpSrcShowRunningConfig
 *                                                                          
 *    DESCRIPTION      : This function displays the set of configurations that
 *                       have been provisioned over the system within
 *                       MRP context.
 *
 *    INPUT            : CliHandle - Handle to the cli context         
 *                       u4ContextId - Context Identifier     
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpSrcShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId)
{
    CliRegisterLock (CliHandle, GarpLock, GarpUnLock);
    MRP_LOCK ();
    /* Convert the incoming context identifier to component identifier */
    MRP_CONVERT_CTXT_ID_TO_COMP_ID (u4ContextId);

    MrpSrcShowRunningConfigTables (CliHandle, u4ContextId);

    MRP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpSrcShowRunningConfigTables
 *                                                                          
 *    DESCRIPTION      : This function displays the set of configurations that
 *                       have been provisioned over the system within
 *                       MRP context.
 *
 *    INPUT            : CliHandle - Handle to the cli context         
 *                       u4CompId  - Component Identifier     
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
VOID
MrpSrcShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4CompId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMacAddr            MacAddr;
    INT4                i4RetVal = MRP_INVALID_VALUE;
    INT4                i4SystemControl = MRP_INVALID_VALUE;
    INT4                i4MvrpStatus = MRP_INVALID_VALUE;
    INT4                i4MmrpStatus = MRP_INVALID_VALUE;
    INT4                i4VlanRegFailure = MRP_INVALID_VALUE;
    INT4                i4MacRegFailure = MRP_INVALID_VALUE;
    INT4                i4CurrAppId = MRP_INVALID_VALUE;
    INT4                i4NextAppId = MRP_INVALID_VALUE;
    UINT4               u4IfIndex = MRP_INVALID_PORT;
    UINT4               u4PrevLocalPort = MRP_INVALID_PORT;
    UINT4               u4LocalPort = MRP_INVALID_PORT;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    INT1                i1ToContinue = OSIX_TRUE;
    UINT4               u4SysMode = VCM_SI_MODE;
    INT1                i1Flag = OSIX_FALSE;

    MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (MacAddr, 0, sizeof (tMacAddr));

    /* Assumption: 
     * 1) u4ContextId represents the component identifier and not the virtual
     *    context identifier  and is validated already.
     * 2) MRP Lock has been taken
     * */

    pMrpContextInfo = MrpCtxtGetContextInfo (u4CompId);

    if (pMrpContextInfo == NULL)
    {
        return;
    }

    if (nmhValidateIndexInstanceFsMrpInstanceTable (u4CompId) != SNMP_FAILURE)
    {
        nmhGetFsMrpInstanceSystemControl (u4CompId, &i4SystemControl);
        nmhGetFsMrpInstanceBridgeMvrpEnabledStatus (u4CompId, &i4MvrpStatus);
	nmhGetFsMrpInstanceBridgeMmrpEnabledStatus (u4CompId, &i4MmrpStatus);
	nmhGetFsMrpInstanceNotifyVlanRegFailure (u4CompId, &i4VlanRegFailure);
	nmhGetFsMrpInstanceNotifyMacRegFailure (u4CompId, &i4MacRegFailure);
	/* Convert the incoming context identifier to component identifier */

    	u4SysMode = (UINT4) MrpPortVcmGetSystemMode (MRP_PROTOCOL_ID);

    	if (u4SysMode == VCM_MI_MODE)
    	{
		if ((i4SystemControl != MRP_DEF_SYSTEM_CTRL) 
		|| (i4MvrpStatus != MRP_DEF_MVRP_STATUS)
		|| (i4MmrpStatus != MRP_DEF_MMRP_STATUS) 
		|| (i4VlanRegFailure != MRP_DEF_MAC_REG_FAIL)
		|| (i4MacRegFailure != MRP_DEF_MAC_REG_FAIL))
		{
        		pMrpContextInfo = MRP_CONTEXT_PTR (u4CompId);
        		if (pMrpContextInfo == NULL)
        		{
            			return;
        		}
        		if (STRLEN(pMrpContextInfo->au1ContextName) != 0)
        		{
        			CliPrintf (CliHandle, "\rswitch  %s \r\n",
                		pMrpContextInfo->au1ContextName);
                                i1Flag = OSIX_TRUE;
    			}
    		}
	}

	 if (i4SystemControl != MRP_DEF_SYSTEM_CTRL)
        {
            CliPrintf (CliHandle, "no shutdown mrp\r\n");
        }
        else
        {
            return;
        }

        /* Validation for Ieee8021Q Table is ||| to that of FsMrpInstanceTable
         * */
        if (i4MvrpStatus != MRP_DEF_MVRP_STATUS)
        {
            CliPrintf (CliHandle, "set mvrp enable\r\n");
        }

        if (i4MmrpStatus != MRP_DEF_MMRP_STATUS)
        {
            CliPrintf (CliHandle, "set mmrp enable\r\n");
        }

        if (i4VlanRegFailure != MRP_DEF_VLAN_REG_FAIL)
        {
            CliPrintf (CliHandle,
                       "set vlan notify failed-registration enable\r\n");
        }

        if (i4MacRegFailure != MRP_DEF_MAC_REG_FAIL)
        {
            CliPrintf (CliHandle,
                       "set mac notify failed-registration enable\r\n");
        }
    }

    while (MrpCtxtGetNextPortInContext (u4CompId, u4PrevLocalPort,
                                        &u4LocalPort) == OSIX_SUCCESS)
    {
        u4IfIndex = MRP_GET_PHY_PORT (u4CompId, u4LocalPort);

        MrpPortCfaCliConfGetIfName (u4IfIndex, (INT1 *) au1NameStr);

        if (nmhValidateIndexInstanceFsMvrpPortTable (u4CompId,
                                                     u4LocalPort)
            != SNMP_FAILURE)
        {
            nmhGetFsMvrpPortMvrpEnabledStatus (u4CompId, u4LocalPort,
                                               &i4RetVal);
            if (i4RetVal != MRP_DEF_PORT_MVRP_STATUS)
            {
                CliPrintf (CliHandle,
                           "set port mvrp %s disable\r\n", au1NameStr);
            }
            i4RetVal = MRP_INVALID_VALUE;
        }

        if (nmhValidateIndexInstanceIeee8021BridgePortMmrpTable (u4CompId,
                                                                 u4LocalPort)
            != SNMP_FAILURE)
        {
            nmhGetIeee8021BridgePortMmrpEnabledStatus (u4CompId, u4LocalPort,
                                                       &i4RetVal);
            if (i4RetVal != MRP_DEF_PORT_MMRP_STATUS)
            {
                CliPrintf (CliHandle,
                           "set port mmrp %s disable\r\n", au1NameStr);
            }
            i4RetVal = MRP_INVALID_VALUE;
        }

        if (nmhValidateIndexInstanceFsMrpPortTable (u4CompId, u4LocalPort)
            != SNMP_FAILURE)
        {
            nmhGetFsMrpPortPeriodicSEMStatus (u4CompId, u4LocalPort, &i4RetVal);
            if (i4RetVal != MRP_DEF_PERIODIC_SEM)
            {
                CliPrintf (CliHandle,
                           "set port mrp %s periodictimer enable\r\n",
                           au1NameStr);
            }
            i4RetVal = MRP_INVALID_VALUE;

            nmhGetFsMrpPortParticipantType (u4CompId, u4LocalPort, &i4RetVal);

            if (i4RetVal != MRP_DEF_PARTICPANT_TYPE)
            {
                CliPrintf (CliHandle,
                           "set port mrp %s participant applicant-only\r\n",
                           au1NameStr);
            }
            i4RetVal = MRP_INVALID_VALUE;
            nmhGetFsMrpPortRegAdminControl (u4CompId, u4LocalPort, &i4RetVal);

            if (i4RetVal == MRP_REG_FIXED)
            {
                CliPrintf (CliHandle,
                           "set port mvrp %s registration fixed\r\n",
                           au1NameStr);
            }
            else if (i4RetVal == MRP_REG_FORBIDDEN)
            {
                CliPrintf (CliHandle,
                           "set port mvrp %s registration forbidden\r\n",
                           au1NameStr);
            }
        }

        do
        {
            if (MrpSrcGetNextAppIdForDisplay (i4CurrAppId, MacAddr, u4CompId,
                                              &i4NextAppId) == OSIX_FAILURE)
            {
                /* Reinitialise for next port in the loop
                 * */
                i1ToContinue = OSIX_FALSE;
                i4NextAppId = MRP_INVALID_VALUE;
                i4CurrAppId = MRP_INVALID_VALUE;
                MEMSET (MacAddr, 0, sizeof (tMacAddr));
                break;
            }

            if (nmhValidateIndexInstanceFsMrpApplicantControlTable (u4CompId,
                                                                    u4LocalPort,
                                                                    MacAddr,
                                                                    i4NextAppId)
                != SNMP_SUCCESS)
            {
                if ((MEMCMP (MacAddr, gMmrpAddr, MRP_MAC_ADDR_LEN) == 0) &&
                    (i4NextAppId == MRP_SERVICE_REQ_ATTR_TYPE))
                {
                    /* GetNextApp returs the following order; MVRP, Serv and 
                     * then MAC. Hence, while processing MAC, set the value of
                     * nextApp as Zero
                     * */
                    i4NextAppId = 0;
                    break;
                }

                continue;
            }
            nmhGetFsMrpApplicantControlAdminStatus (u4CompId, u4LocalPort,
                                                    MacAddr, i4NextAppId,
                                                    &i4RetVal);
            switch (i4NextAppId)
            {
                case MRP_VID_ATTR_TYPE:
                    /* This case represents two values
                     * 1) MRP_VID_ATTR_TYPE + MacAddr == MVRP MAC Address
                     * 2) MRP_SERVICE_REQ_ATTR_TYPE + 
                     * MacAddr == MMRP MAC Address
                     * Need to handle appropriately
                     * */
                    if ((MEMCMP (MacAddr, gMmrpAddr, MRP_MAC_ADDR_LEN) == 0)
                        && (i4RetVal != MRP_DEF_MMRP_SERVICE_REQ_APP))
                    {
                        CliPrintf (CliHandle,
                                   "set port mmrp service-requirement "
                                   "%s applicant non-participant\r\n",
                                   au1NameStr);
                    }
                    else if ((MEMCMP (MacAddr, pMrpContextInfo->MvrpAddr,
                                      MRP_MAC_ADDR_LEN) == 0))
                    {
                        if (i4RetVal == MRP_NON_PARTICIPANT)
                        {
                            CliPrintf (CliHandle,
                                       "set port mvrp %s applicant "
                                       "non-participant\r\n", au1NameStr);
                        }
                        else if (i4RetVal == MRP_ACTIVE_APPLICANT)
                        {
                            CliPrintf (CliHandle,
                                       "set port mvrp %s applicant "
                                       "active\r\n", au1NameStr);
                        }
                        /* This represents the last parameter that
                         * should be displayed. Make NextAppId as Zero
                         */
                        i4NextAppId = 0;
                    }
                    break;

                case MRP_MAC_ADDR_ATTR_TYPE:

                    if (i4RetVal != MRP_DEF_MMRP_MACP_APP)
                    {
                        CliPrintf (CliHandle,
                                   "set port mmrp mac %s applicant "
                                   "non-participant\r\n", au1NameStr);
                    }

                    break;

                default:
                    /* Impossible case */
                    i4NextAppId = 0;
                    break;
            }                    /* switch Application id */
            i4CurrAppId = i4NextAppId;
            i4RetVal = MRP_INVALID_VALUE;

        }
        while (i1ToContinue == OSIX_TRUE);

        if (nmhGetIeee8021BridgePortMrpJoinTime (u4CompId, u4LocalPort,
                                                 &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal != MRP_DEF_JOIN_TIME)
            {
                CliPrintf (CliHandle, "set port mrp %s timer join %d\r\n",
                           au1NameStr, i4RetVal);
            }
        }
        i4RetVal = MRP_INVALID_VALUE;

        if (nmhGetIeee8021BridgePortMrpLeaveTime (u4CompId, u4LocalPort,
                                                  &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal != MRP_DEF_LEAVE_TIME)
            {
                CliPrintf (CliHandle, "set port mrp %s timer leave %d\r\n",
                           au1NameStr, i4RetVal);
            }
        }
        i4RetVal = MRP_INVALID_VALUE;

        if (nmhGetIeee8021BridgePortMrpLeaveAllTime (u4CompId, u4LocalPort,
                                                     &i4RetVal) == SNMP_SUCCESS)
        {
            if (i4RetVal != MRP_DEF_LEAVE_ALL_TIME)
            {
                CliPrintf (CliHandle, "set port mrp %s timer leaveall %d\r\n",
                           au1NameStr, i4RetVal);
            }
        }
        i4RetVal = MRP_INVALID_VALUE;
        i1ToContinue = OSIX_TRUE;

        u4PrevLocalPort = u4LocalPort;
    }                            /* For all the ports in the context */
    if ((u4SysMode == VCM_MI_MODE) && (i1Flag == OSIX_TRUE))
    {
        CliPrintf (CliHandle, "! \r\n");
    }
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpSrcGetNextAppIdForDisplay 
 *                                                                          
 *    DESCRIPTION      : This function fetches the next attribute that can be 
 *                       used to scan through the FsMrpApplicantControlTable 
 *
 *    INPUT            : i4CurrAppId - Current Application Identifier. 
 *                       MacAddr - MacAddress that will be updated based on
 *                                 the application.
 *                                                                          
 *    OUTPUT           : *pi4NextAppId - Next Application Identifier
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpSrcGetNextAppIdForDisplay (INT4 i4CurrAppId, tMacAddr MacAddr,
                              UINT4 u4CompId, INT4 *pi4NextAppId)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    /* The function provides the next application identifier to be used
     * for traversing FsMrpApplicantControlTable. If invalid AppId is passed,
     * then this function will return MVRP APPlication id. Or else, it returns
     * in the following sequence
     * 1) MRP_VID_ATTR_TYPE 
     * 2) MRP_SERVICE_REQ_ATTR_TYPE 
     * 3) MRP_MAC_ADDR_ATTR_TYPE 
     * */
    switch (i4CurrAppId)
    {
        case MRP_INVALID_VALUE:

            *pi4NextAppId = MRP_MAC_ADDR_ATTR_TYPE;
            MEMCPY (MacAddr, gMmrpAddr, sizeof (tMacAddr));
            break;

        case MRP_SERVICE_REQ_ATTR_TYPE:

            *pi4NextAppId = MRP_VID_ATTR_TYPE;
            if (MEMCMP (MacAddr, gMmrpAddr, sizeof (tMacAddr)) == 0)
            {
                pMrpContextInfo = MrpCtxtGetContextInfo (u4CompId);

                if (pMrpContextInfo != NULL)
                {
                    MEMCPY (MacAddr, pMrpContextInfo->MvrpAddr,
                            sizeof (tMacAddr));
                }
            }
            else
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;

        case MRP_MAC_ADDR_ATTR_TYPE:

            *pi4NextAppId = MRP_SERVICE_REQ_ATTR_TYPE;
            MEMCPY (MacAddr, gMmrpAddr, sizeof (tMacAddr));
            break;

        default:
            /* Impossible case */
            *pi4NextAppId = MRP_INVALID_VALUE;
            i4RetVal = OSIX_FAILURE;
            break;
    }
    return i4RetVal;
}
