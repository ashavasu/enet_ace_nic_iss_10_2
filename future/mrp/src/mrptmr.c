/*****************************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: mrptmr.c,v 1.7 2010/04/23 06:59:32 prabuc Exp $
*
* Description: This file contains the APIs exported by MRP module.
******************************************************************************/

#include "mrpinc.h"

/* Proto types of the functions private to this file only */
PRIVATE VOID MrpTmrInitTmrDesc PROTO ((VOID));
PRIVATE VOID MrpTmrDeInitTmrDesc PROTO ((VOID));
PRIVATE VOID MrpTmrJoinTmrExp PROTO ((VOID *pArg));
PRIVATE VOID MrpTmrLeaveTmrExp PROTO ((VOID *pArg));
PRIVATE VOID MrpTmrLeaveAllTmrExp PROTO ((VOID *pArg));
PRIVATE VOID MrpTmrPeriodicTmrExp PROTO ((VOID *pArg));
PRIVATE VOID        MrpTmrApplyLvTmrExpToAttributes
PROTO ((tMrpMapPortEntry * pMapPortEntry, UINT1 *pu1ExpiredAttribList));

PRIVATE VOID MrpTmrLeaveTmrExpForAttrib PROTO ((tMrpMapPortEntry *, UINT2));
PRIVATE VOID MrpTmrUpdateLeaveTmrSplitLists PROTO ((tMrpMapPortEntry *));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpTmrInit
 *                                                                          
 *    DESCRIPTION      : This function creates a timer list for all the timers
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
MrpTmrInit (VOID)
{
    if (TmrCreateTimerList (MRP_TASK, MRP_TIMER_EXP_EVENT, NULL,
                            &(gMrpGlobalInfo.MrpTmrListId)) == TMR_FAILURE)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC | INIT_SHUT_TRC,
                     "MrpTmrInit: Timer list creation Failed \r\n");
        return OSIX_FAILURE;
    }

    MrpTmrInitTmrDesc ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpTmrDeInit
 *                                                                          
 *    DESCRIPTION      : This function deletes the timer list.
 *                       Call this function after stopping all the 
 *                       running timers.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
MrpTmrDeInit (VOID)
{
    if (gMrpGlobalInfo.MrpTmrListId != MRP_INIT_VAL)
    {
        if (TmrDeleteTimerList (gMrpGlobalInfo.MrpTmrListId) == TMR_FAILURE)
        {
            MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC | INIT_SHUT_TRC,
                         "MrpTmrDeInit: Timer list deletion FAILED\r\n");
            return OSIX_FAILURE;
        }
        gMrpGlobalInfo.MrpTmrListId = MRP_INIT_VAL;

        MrpTmrDeInitTmrDesc ();
    }

    MRP_GBL_TRC (OS_RESOURCE_TRC | INIT_SHUT_TRC,
                 "MrpTmrDeInit: Timer list deletion successful\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrInitTmrDesc
 *
 *    DESCRIPTION      : This function intializes the timer desc for all
 *                       the timers in MRP module.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpTmrInitTmrDesc (VOID)
{
    /* 1. Join Timer */
    gMrpGlobalInfo.aTmrDesc[MRP_JOIN_TMR].i2Offset =
        (INT2) MRP_OFFSET (tMrpAppPortEntry, JoinTmr);
    gMrpGlobalInfo.aTmrDesc[MRP_JOIN_TMR].TmrExpFn = MrpTmrJoinTmrExp;

    /* 2. Leave Timer */
    gMrpGlobalInfo.aTmrDesc[MRP_LEAVE_TMR].i2Offset =
        (INT2) MRP_OFFSET (tMrpMapPortEntry, LeaveTmr);
    gMrpGlobalInfo.aTmrDesc[MRP_LEAVE_TMR].TmrExpFn = MrpTmrLeaveTmrExp;

    /* 3. Leave All Timer */
    gMrpGlobalInfo.aTmrDesc[MRP_LEAVE_ALL_TMR].i2Offset =
        (INT2) MRP_OFFSET (tMrpAppPortEntry, LeaveAllTmr);
    gMrpGlobalInfo.aTmrDesc[MRP_LEAVE_ALL_TMR].TmrExpFn = MrpTmrLeaveAllTmrExp;

    /* 4. Periodic Timer */
    gMrpGlobalInfo.aTmrDesc[MRP_PERIODIC_TMR].i2Offset =
        (INT2) MRP_OFFSET (tMrpPortEntry, PeriodicTmr);
    gMrpGlobalInfo.aTmrDesc[MRP_PERIODIC_TMR].TmrExpFn = MrpTmrPeriodicTmrExp;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrDeInitTmrDesc
 *
 *    DESCRIPTION      : This function de-intializes the timer desc for all
 *                       the timers in MRP module.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpTmrDeInitTmrDesc (VOID)
{
    MEMSET (gMrpGlobalInfo.aTmrDesc, 0, MRP_MAX_TMR_TYPES * sizeof (tTmrDesc));
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrExpHandler
 *
 *    DESCRIPTION      : This function is called whenever a timer expiry
 *                       event is received by MRP task. Different timer
 *                       expiry handlers are called based on the timer type.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
MrpTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = MRP_INVALID_VALUE;

    while ((pExpiredTimers = TmrGetNextExpiredTimer
            (gMrpGlobalInfo.MrpTmrListId)) != NULL)
    {
        MRP_LOCK ();
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        if (u1TimerId < MRP_MAX_TMR_TYPES)
        {
            i2Offset = gMrpGlobalInfo.aTmrDesc[u1TimerId].i2Offset;

            /* Call the registered expired handler function */
            (*(gMrpGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
        MRP_UNLOCK ();
    }                            /* End of while */
}

/****************************************************************************
 *                  Timer Expiry Handler Functions 
 ***************************************************************************/
/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrJoinTmrExp
 *
 *    DESCRIPTION      : Handle the Join Timer Expiry
 *
 *    INPUT            : pArg - pointer to the AppPort struct 
 *                       (tMrpAppPortEntry) whose timer has expired.
 *                       tMrpAppPortEntry
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpTmrJoinTmrExp (VOID *pArg)
{
    tMrpContextInfo    *pMrpContextInfo = NULL;
    tMrpAppPortEntry   *pAppPortEntry = (tMrpAppPortEntry *) pArg;

    /* For fixing Kloc-work warning , the below NULL check was
     * added.
     * */

    if (pAppPortEntry == NULL)
    {
        return;
    }

    pMrpContextInfo = pAppPortEntry->pMrpAppEntry->pMrpContextInfo;

    if (pMrpContextInfo == NULL)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC, "MrpTmrJoinTmrExp: "
                     "Context Info is NULL\r\n");
        return;
    }

    if (pAppPortEntry->pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        MrpMvrpHandleJoinTmrExpiry (pMrpContextInfo->u4ContextId,
                                    pAppPortEntry);
    }
    else
    {
        MrpMmrpHandleJoinTmrExpiry (pMrpContextInfo->u4ContextId,
                                    pAppPortEntry);
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrLeaveTmrExp
 *
 *    DESCRIPTION      : Handle the Leave Timer Expiry
 *
 *    INPUT            : pArg - pointer to the MapPort specific struct
 *                       (tMrpMapPortEntry) whose timer has expired.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpTmrLeaveTmrExp (VOID *pArg)
{
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    tMrpPortEntry      *pPortEntry = NULL;
    tMrpMapPortEntry   *pMapPortEntry = NULL;
    UINT1              *pu1ExpiredAttribList = NULL;
    UINT2               u2MapId = 0;
    UINT2               u2Port = 0;

    pMapPortEntry = (tMrpMapPortEntry *) pArg;

    if (pMapPortEntry == NULL)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpTmrLeaveTmrExp: Map port entry is NULL\r\n");
        return;
    }

    pAppEntry = pMapPortEntry->pMapEntry->pMrpAppEntry;

    u2MapId = pMapPortEntry->pMapEntry->u2MapId;
    u2Port = pMapPortEntry->u2Port;

    /* Swap the leave timer split lists */
    MrpTmrUpdateLeaveTmrSplitLists (pMapPortEntry);

    /* MrpTmrUpdateLeaveTmrSplitLists() place the list of expired attributes
     * in the splist list[0] */
    pu1ExpiredAttribList = pMapPortEntry->apLeaveList[0];

    /* Get the Attributes for which the leave tmr has expired and
     * handle the expiry */
    MrpTmrApplyLvTmrExpToAttributes (pMapPortEntry, pu1ExpiredAttribList);

    /* When there is no attribute present in this MAP the MAP Port entry 
     * will get deleted by function MrpTmrApplyLvTmrExpToAttributes. So 
     * obtain the pMapPortEntry freshly */
    pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);
    if (pMapEntry == NULL)
    {
        return;
    }

    pMapPortEntry = MRP_GET_MAP_PORT_ENTRY (pMapEntry, u2Port);
    if (pMapPortEntry == NULL)
    {
        return;
    }

    /* Start the Leave Timer if the u2LeavingAttrCnt != 0 */
    if (pMapPortEntry->u2LeavingAttrCnt != 0)
    {
        pPortEntry = MRP_GET_PORT_ENTRY (pAppEntry->pMrpContextInfo, u2Port);
        if (((pPortEntry != NULL) &&
             (MrpTmrStart (MRP_LEAVE_TMR, &(pMapPortEntry->LeaveTmrNode),
                           MRP_GET_LV_TMR_INTERVAL (pPortEntry)))) !=
            OSIX_SUCCESS)
        {
            MRP_TRC ((pPortEntry->pMrpContextInfo,
                      (ALL_FAILURE_TRC | OS_RESOURCE_TRC),
                      "MrpTmrLeaveTmrExp: Failed to start the Leave Tmr\r\n"));
            return;
        }
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrLeaveAllTmrExp
 *
 *    DESCRIPTION      : Handle the Leave All Timer Expiry
 *
 *    INPUT            : pArg - pointer to the AppPort specific struct
 *                      (tMrpAppPortEntry) whose timer has expired.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpTmrLeaveAllTmrExp (VOID *pArg)
{
    tMrpAppPortEntry   *pAppPortEntry = NULL;
    tMrpPortEntry      *pPortEntry = NULL;
    UINT4               u4RemainingTime = 0;

    pAppPortEntry = (tMrpAppPortEntry *) pArg;

    if (pAppPortEntry == NULL)
    {
        return;
    }

    pPortEntry = pAppPortEntry->pMrpPortEntry;

    if (pPortEntry == NULL)
    {
        MRP_TRC ((pAppPortEntry->pMrpAppEntry->pMrpContextInfo,
                  (ALL_FAILURE_TRC | OS_RESOURCE_TRC | MRP_LEAVEALL_SEM_TRC),
                  "MrpTmrLeaveAllTmrExp: port entry is NULL\r\n"));
        return;
    }

    /* Start the Leave All Timer Again */
    if (MrpTmrStartLeaveAllTmr (pAppPortEntry) != OSIX_SUCCESS)
    {
        MRP_TRC ((pPortEntry->pMrpContextInfo,
                  (ALL_FAILURE_TRC | OS_RESOURCE_TRC | MRP_LEAVEALL_SEM_TRC),
                  "MrpTmrLeaveAllTmrExp: "
                  "Failed to start the Leave All timer \r\n"));
        return;
    }

    /* Update the Leave ALL SEM state */
    pAppPortEntry->u1LeaveAllSemState = MRP_ACTIVE;

    /* If the Join Timer is not running on this port start it */
    TmrGetRemainingTime (gMrpGlobalInfo.MrpTmrListId,
                         &(pAppPortEntry->JoinTmrNode.TmrBlk.TimerNode),
                         &u4RemainingTime);

    if (u4RemainingTime == 0)
    {
        if (MrpTmrStartJoinTmr (pAppPortEntry) != OSIX_SUCCESS)
        {
            MRP_TRC ((pPortEntry->pMrpContextInfo,
                      (ALL_FAILURE_TRC | OS_RESOURCE_TRC |
                       MRP_LEAVEALL_SEM_TRC),
                      "MrpTmrLeaveAllTmrExp: "
                      "Failed to start the Join tmr\r\n"));
            return;
        }
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrPeriodicTmrExp
 *
 *    DESCRIPTION      : Handle the Periodic Timer Expiry
 *
 *    INPUT            : pArg - pointer to the port specific struct 
 *                       (tMrpPortEntry) whose timer has expired.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpTmrPeriodicTmrExp (VOID *pArg)
{
    tMrpPortEntry      *pPortEntry = NULL;

    pPortEntry = (tMrpPortEntry *) pArg;

    if (pPortEntry == NULL)
    {
        return;
    }

    if (pPortEntry->u1PeriodicSEMState == MRP_PASSIVE)
    {
        /* SEM is disabled */
        return;
    }

    /* Start the Periodic Timer Again */
    if (MrpTmrStartPeriodicTmr (pPortEntry) != OSIX_SUCCESS)
    {
        MRP_TRC ((pPortEntry->pMrpContextInfo,
                  (ALL_FAILURE_TRC | OS_RESOURCE_TRC),
                  "MrpTmrPeriodicTmrExp: "
                  "Failed to start the periodic timer\r\n"));
        return;
    }

    /* Send Periodic Event to all the Applicant SEM associated with this port */
    MrpUtilHandlePeriodicEvntOnPort (pPortEntry);

    /* Update the Periodic SEM state */
    pPortEntry->u1PeriodicSEMState = MRP_ACTIVE;

}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrStart
 *
 *    DESCRIPTION      : Starts a timer of the given duration
 *
 *    INPUT            : u1TimerType - Timer Type
 *                       pTmrNode    - Pointer to the Timer Node structure
 *                       u4Duration  - The duration of timer in
 *                                     centi-second
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpTmrStart (UINT1 u1TmrType, tMrpTimer * pTmrNode, UINT4 u4Duration)
{
    UINT4               u4MiliSec = 0;

    if (u4Duration <= 0)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpTmrStart: " "Invalid Timer Duration\r\n");
        return OSIX_FAILURE;
    }

    u4MiliSec = (u4Duration * MRP_MILLI_SEC_CONVERTER);

    if (TmrStart (gMrpGlobalInfo.MrpTmrListId, &(pTmrNode->TmrBlk), u1TmrType,
                  0, u4MiliSec) != TMR_SUCCESS)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpTmrStart: " "Unable to Start Timer\r\n");

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrRestartLvAllTmr
 *
 *    DESCRIPTION      : Restarts the Leave All Timer
 *
 *    INPUT            : pAppPortEntry - Pointer to the App port entry
 *                                       for which the timer needs to be started
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpTmrRestartLvAllTmr (tMrpAppPortEntry * pAppPortEntry)
{
    UINT4               u4LeaveAllTime = 0;
    UINT4               u4RandLeaveAllTime = 0;
    UINT4               u4MiliSec = 0;

    u4LeaveAllTime = pAppPortEntry->pMrpPortEntry->u4LeaveAllTime;

    MRP_GET_RANDOM_LVALL_TIME (u4LeaveAllTime, u4RandLeaveAllTime);
    u4MiliSec = (u4RandLeaveAllTime * MRP_MILLI_SEC_CONVERTER);

    if (TmrRestart (gMrpGlobalInfo.MrpTmrListId,
                    &(pAppPortEntry->LeaveAllTmrNode.TmrBlk),
                    MRP_LEAVE_ALL_TMR, 0, u4MiliSec) != TMR_SUCCESS)
    {
        MRP_GBL_TRC ((ALL_FAILURE_TRC | OS_RESOURCE_TRC),
                     "MrpTmrRestartLvAllTmr: " "Failed to restart timer\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrStop
 *
 *    DESCRIPTION      : Stops a timer
 *
 *    INPUT            : pTmrNode    - Pointer to the Timer Node structure
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpTmrStop (tMrpTimer * pTmrNode)
{
    if (TmrStop (gMrpGlobalInfo.MrpTmrListId, &(pTmrNode->TmrBlk)) !=
        TMR_SUCCESS)

    {
        MRP_GBL_TRC ((ALL_FAILURE_TRC | OS_RESOURCE_TRC | MRP_CRITICAL_TRC),
                     "MrpTmrStop: " "Failed to stop timer\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrLeaveTmrExpForAttrib
 *
 *    DESCRIPTION      : Handle the Leave Timer Expiry for a particular
 *                       Attribute Entry.
 *
 *    INPUT            : pMapPortEntry - Pointer to the MAP port entry
 *                       u2AttrIndex - Index of the Attribute entry for
 *                                     which the Leave timer is expired.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpTmrLeaveTmrExpForAttrib (tMrpMapPortEntry * pMapPortEntry, UINT2 u2AttrIndex)
{
    UINT1               u1RegSEMStateChg = OSIX_FALSE;

    if (pMapPortEntry->pu1AttrInfoList == NULL)
    {
        return;
    }
    /* Send LV event to the registerer state machine for this port */
    if (MRP_GET_ATTR_REG_ADMIN_CTRL
        (pMapPortEntry->pu1AttrInfoList[u2AttrIndex]) == MRP_REG_NORMAL)
    {
        MrpSmRegistrarSem (pMapPortEntry, u2AttrIndex, MRP_REG_LEAVE_TMR_EXPIRY,
                           &u1RegSEMStateChg);
    }
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrUpdateLeaveTmrSplitLists
 *
 *    DESCRIPTION      : Swap the leave timer split lists.
 *
 *    INPUT            : pMapPortEntry - Pointer to the MAP port entry
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpTmrUpdateLeaveTmrSplitLists (tMrpMapPortEntry * pMapPortEntry)
{
    UINT4               u4ListId = 0;
    UINT1              *pPrevPtr = NULL;    /* For list[0] previous pointer will
                                             * be NULL */
    UINT1              *pCurrPtr = NULL;

    for (u4ListId = 0; u4ListId < MRP_LEAVE_SPLIT_INTERVAL; u4ListId++)
    {
        /* Swap the pointer towards downwards */
        pCurrPtr = pMapPortEntry->apLeaveList[u4ListId];

        pMapPortEntry->apLeaveList[u4ListId] = pPrevPtr;

        pPrevPtr = pCurrPtr;    /* pCurrPtr will be used as pPrevPtr in the 
                                   next itteration */
    }

    /* NOTE: list[last] is now present in pPrevPtr.
     * and list[0] is NULL */

    pMapPortEntry->apLeaveList[0] = pPrevPtr;
    /* Now List[0] containing the list of attributes for which
     * the leave timer is expired. take necessary action on them
     * in the calling function */
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrStopLeaveTmr
 *
 *    DESCRIPTION      : This function Stops the Leave Timer for a particular
 *                       Attribute. But the Leave timer may run for a set of 
 *                       attributes, so before stopping the timer it is 
 *                       verified that if any other attribute is associated 
 *                       with this timer. If this is the only attribute
 *                       for which the leave timer is running then 
 *                       stop the timer. Otherwise just remove the Attribute 
 *                       for the leave timer split list, without stopping the 
 *                       timer.
 *                       NOTE: Don't update the u2LeavingAttrCnt count
 *                       outside mrptmr.c
 *
 *    INPUT            : pMapPortEntry - Pointer to Map Port Entry on which
 *                          the leave timer is running.
 *                       u4AttrIndex - Index of the Attribute for which
 *                                     the leave timer needs to be stopped.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpTmrStopLeaveTmr (tMrpMapPortEntry * pMapPortEntry, UINT4 u4AttrIndex)
{
    UINT4               u4Count = 0;
    BOOLEAN             bResult = OSIX_FALSE;

    if ((pMapPortEntry == NULL) || (u4AttrIndex == 0))
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpTmrStopLeaveTmr: "
                     "Can't stop the leave timer. Invalid Argument.\r\n");
        return OSIX_FAILURE;
    }

    /* Search the Attribute in all the leave timer split lists */
    for (u4Count = 0; u4Count < MRP_LEAVE_SPLIT_INTERVAL; u4Count++)
    {
        if (pMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
        {
            OSIX_BITLIST_IS_BIT_SET (pMapPortEntry->apLeaveList[u4Count],
                                     u4AttrIndex, MRP_MVRP_LV_BIT_LIST_BLK,
                                     bResult);
        }
        else if (pMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId ==
                 MRP_MMRP_APP_ID)
        {
            OSIX_BITLIST_IS_BIT_SET (pMapPortEntry->apLeaveList[u4Count],
                                     u4AttrIndex, MRP_MMRP_LV_BIT_LIST_BLK,
                                     bResult);
        }
        if (OSIX_TRUE == bResult)
        {
            break;
        }
    }

    if (OSIX_TRUE == bResult)
    {
        if (pMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
        {
            OSIX_BITLIST_RESET_BIT (pMapPortEntry->apLeaveList[u4Count],
                                    u4AttrIndex, MRP_MVRP_LV_BIT_LIST_BLK);
        }
        else if (pMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId ==
                 MRP_MMRP_APP_ID)
        {
            OSIX_BITLIST_RESET_BIT (pMapPortEntry->apLeaveList[u4Count],
                                    u4AttrIndex, MRP_MMRP_LV_BIT_LIST_BLK);
        }

        /* Decrement the Leaving Attribute Count */
        pMapPortEntry->u2LeavingAttrCnt--;

        /* If this is the last attribute for which the leave timer is 
         * running on this port then stop the timer */
        if (pMapPortEntry->u2LeavingAttrCnt == 0)
        {
            if (MrpTmrStop (&pMapPortEntry->LeaveTmrNode) != OSIX_SUCCESS)
            {
                MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                             "MrpTmrStopLeaveTmr: "
                             "Failed to stop the Leave Tiimer for Port\r\n");
                return OSIX_FAILURE;
            }

        }
    }

    else
    {
        MRP_TRC ((pMapPortEntry->pMapEntry->pMrpAppEntry->pMrpContextInfo,
                  (ALL_FAILURE_TRC | OS_RESOURCE_TRC),
                  "MrpTmrStopLeaveTmr: "
                  "Leave timer is not running for the attrib index %d\r\n",
                  u4AttrIndex));
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrStartLeaveTmr
 *
 *    DESCRIPTION      : Start the leave timer for an attribute. as a single
 *                       leave timer may run for multiple attributes at a
 *                       time, so before start the leave timer check if 
 *                       this timer is running for any attribute or not.
 *                       if it is already running then, only add the
 *                       attribute to the split list 0.
 *                       Otherwise allocate the split list for the leave
 *                       timer and start the timer also.
 *
 *                       NOTE: Don't update the u2LeavingAttrCnt count
 *                       outside mrptmr.c
 *
 *    INPUT            : pMapPortEntry - Pointer to the Map port entry
 *                       u4AttrIndex - Index of the attribute for which
 *                          the timer needs to be started.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpTmrStartLeaveTmr (tMrpMapPortEntry * pMapPortEntry, UINT4 u4AttrIndex)
{
    tMrpPortEntry      *pPortEntry = NULL;

    if ((pMapPortEntry == NULL) || (u4AttrIndex == 0))
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                     "MrpTmrStartLeaveTmr: "
                     "Can't start the leave timer. Invalid Argument.\r\n");
        return OSIX_FAILURE;
    }

    /* Add the u4AttrIndex to the first split list */
    if (pMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        OSIX_BITLIST_SET_BIT (pMapPortEntry->apLeaveList[0],
                              u4AttrIndex, MRP_MVRP_LV_BIT_LIST_BLK);
    }
    else if (pMapPortEntry->pMapEntry->pMrpAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        OSIX_BITLIST_SET_BIT (pMapPortEntry->apLeaveList[0],
                              u4AttrIndex, MRP_MMRP_LV_BIT_LIST_BLK);
    }

    if (pMapPortEntry->u2LeavingAttrCnt == 0)
    {
        /* This is the first attribute in this port for which the leave tmr 
         * is going to be started */

        pPortEntry = MRP_GET_PORT_ENTRY
            (pMapPortEntry->pMapEntry->pMrpAppEntry->pMrpContextInfo,
             pMapPortEntry->u2Port);
        MRP_CHK_NULL_PTR_RET (pPortEntry, OSIX_FAILURE);
        if (MrpTmrStart (MRP_LEAVE_TMR, &(pMapPortEntry->LeaveTmrNode),
                         MRP_GET_LV_TMR_INTERVAL (pPortEntry)) != OSIX_SUCCESS)
        {

            MRP_TRC ((pPortEntry->pMrpContextInfo,
                      (ALL_FAILURE_TRC | OS_RESOURCE_TRC),
                      "MrpTmrStartLeaveTmr: "
                      "Failed to start the Leave Tmr for port %d\r\n",
                      pMapPortEntry->u2Port));

            return OSIX_FAILURE;
        }
    }
    /* Increment the Leaving Attribute Count */
    pMapPortEntry->u2LeavingAttrCnt++;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrStartJoinTmr
 *
 *    DESCRIPTION      : Start the Join Timer
 *
 *    INPUT            : pAppPortEntry - pointer to the app port entry
 *                          for which the timer needs to be started.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpTmrStartJoinTmr (tMrpAppPortEntry * pAppPortEntry)
{
    UINT4               u4JoinTime = 0;
    UINT4               u4RandJoinTime = 0;
    tMrpPortEntry      *pPortEntry = NULL;

    if (pAppPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    pPortEntry = pAppPortEntry->pMrpPortEntry;

    u4JoinTime = pPortEntry->u4JoinTime;

    MRP_RANDOM_TIMEOUT (u4JoinTime, u4RandJoinTime);

    /* For P2P case Join timer value should be 1.5 time */
    if (pPortEntry->bOperP2PStatus == OSIX_TRUE)
    {
        u4RandJoinTime = (UINT4) (MRP_JOIN_TMR_MULTIPLIER * u4RandJoinTime);
    }

    /* Start the timer */
    if (MrpTmrStart (MRP_JOIN_TMR, &pAppPortEntry->JoinTmrNode, u4RandJoinTime)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrStartLeaveAllTmr
 *
 *    DESCRIPTION      : Start the Leave All timer
 *
 *    INPUT            : pAppPortEntry - pointer to the app port entry
 *                          for which the timer needs to be started.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpTmrStartLeaveAllTmr (tMrpAppPortEntry * pAppPortEntry)
{
    UINT4               u4LeaveAllTime = 0;
    UINT4               u4RandLeaveAllTime = 0;
    tMrpPortEntry      *pPortEntry = NULL;

    if (pAppPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    pPortEntry = pAppPortEntry->pMrpPortEntry;

    if (pPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    u4LeaveAllTime = pPortEntry->u4LeaveAllTime;

    MRP_GET_RANDOM_LVALL_TIME (u4LeaveAllTime, u4RandLeaveAllTime);

    /* Start the timer */
    if (MrpTmrStart (MRP_LEAVE_ALL_TMR, &pAppPortEntry->LeaveAllTmrNode,
                     u4RandLeaveAllTime) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrStartPeriodicTmr
 *
 *    DESCRIPTION      : Start the periodic timer
 *
 *    INPUT            : pPortEntry - pointer to the port entry for which
 *                                  timer needs to be started.
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
MrpTmrStartPeriodicTmr (tMrpPortEntry * pPortEntry)
{
    if (pPortEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Start the timer */
    if (MrpTmrStart (MRP_PERIODIC_TMR, &(pPortEntry->PeriodicTmrNode),
                     MRP_DEF_PERIODIC_TIME) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpTmrApplyLvTmrExpToAttributes
 *
 *    DESCRIPTION      : Scans the Attribute List and applies Leave Timer
 *                       expiry to each Attribute.
 *
 *    INPUT            : pMapPortEntry         - Pointer to the MAP port entry
 *                       pu1ExpiredAttribList  - List of Atributes for which the
 *                                               Leave timer is expired
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
MrpTmrApplyLvTmrExpToAttributes (tMrpMapPortEntry * pMapPortEntry,
                                 UINT1 *pu1ExpiredAttribList)
{
    tMrpAppEntry       *pAppEntry = NULL;
    tMrpMapEntry       *pMapEntry = NULL;
    UINT2               u2MapId = 0;
    UINT2               u2ByteInd = 0;
    UINT2               u2LvSplitListSize = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2AttrIndex = 0;
    UINT1               u1ByteVal = 0;

    pAppEntry = pMapPortEntry->pMapEntry->pMrpAppEntry;

    u2MapId = pMapPortEntry->pMapEntry->u2MapId;
    if (pAppEntry->u1AppId == MRP_MVRP_APP_ID)
    {
        u2LvSplitListSize = MRP_MVRP_LV_BIT_LIST_BLK;
    }
    else if (pAppEntry->u1AppId == MRP_MMRP_APP_ID)
    {
        u2LvSplitListSize = MRP_MMRP_LV_BIT_LIST_BLK;
    }

    /* Scan Byte-by-Byte */
    for (u2ByteInd = 0; u2ByteInd < u2LvSplitListSize; u2ByteInd++)
    {
        /* Get the integer value for the current byte */
        u1ByteVal = pu1ExpiredAttribList[u2ByteInd];

        if (u1ByteVal != 0)
            /* i.e. Some bits are set within this byte */
        {
            /* Scan each Bit within this byte now */
            for (u2BitIndex = 0; ((u2BitIndex < BITS_PER_BYTE) &&
                                  (u1ByteVal != 0)); u2BitIndex++)
            {
                if ((u1ByteVal & MRP_BIT8) != 0)
                    /* Current BIT is set */
                {
                    /* Get the Attribute index for this bit position */
                    u2AttrIndex =
                        (UINT2) ((u2ByteInd * BITS_PER_BYTE) + u2BitIndex + 1);

                    /* Reset the BIT for this attribute */
                    OSIX_BITLIST_RESET_BIT (pu1ExpiredAttribList, u2AttrIndex,
                                            u2LvSplitListSize);

                    /* Decrement the Leaving Attribute Count */
                    pMapPortEntry->u2LeavingAttrCnt--;

                    if (MRP_RED_NODE_STATUS () == RM_ACTIVE)
                    {
                        /* Handle the Leave Timer Expiry for this Attribute */
                        MrpTmrLeaveTmrExpForAttrib (pMapPortEntry, u2AttrIndex);
                        /* When there is no attribute present in this MAP the
                         * MAP Port entry will get deleted by function
                         * MrpTmrLeaveTmrExpForAttrib. So obtain the pMapEntry freshly */
                        pMapEntry = MRP_GET_MAP_ENTRY (pAppEntry, u2MapId);
                        if (pMapEntry == NULL)
                        {
                            return;
                        }

                    }
                    else if (MRP_RED_NODE_STATUS () == RM_STANDBY)
                    {
                        /* In Standby node, Attributes should be deleted only on 
                         * receiving the Delete syncup message from Active node.
                         * Because the standby node will not be aware for which
                         * Attributes, Join event is received.
                         *
                         * In the Active node, when Join event
                         * is received, the Leave Timer will be stopped.
                         * But as that Attribute had already been registered,
                         * no indication will be given to Standby node. Hence
                         * in the Standby node when leave timer expiry, 
                         * the Registrar state machine for these Attribute
                         * will continue to remain in LV state (as deletion 
                         * will be done only on receiving the delete sync-up)
                         * which will further prevent the starting of Lv
                         * Timer when LeaveAll PDU is sent during the transition
                         * from Standby to Active. As a result, stale entries
                         * will not get aged out. So when the Lv Timer expires
                         * for an Attribute, set the Registrar state as IN.
                         * This will not impact the functionality
                         * because the Active node will send delete indication
                         * if no Join event is received for the Attribute value.
                         */
                        MRP_SET_ATTR_REG_SEM_STATE
                            (pMapPortEntry->pu1AttrInfoList[u2AttrIndex],
                             MRP_IN);
                    }

                }
                u1ByteVal = (UINT1) (u1ByteVal << 1);
            }
        }
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrptmr.c                        */
/*-----------------------------------------------------------------------*/
