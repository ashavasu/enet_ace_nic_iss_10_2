/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrptrap.c,v 1.14 2014/02/18 10:36:52 siva Exp $
 *
 * Description: This file contains the functions to send the trap message.
 ******************************************************************************/
#include "fssnmp.h"
# include "mrpinc.h"
# include "snmputil.h"
#ifdef SNMP_3_WANTED
# include "fsmrp.h"
#endif
# include "fsmrp.h"

CHR1               *gac1FailureReasons[] = {
    NULL,
    "Lack Of Resources.",
    "Restricted Registration Enabled.",
    "Unsupported Attribute Value."
};

CHR1               *gac1MrpSysLogMsg[] = {
    NULL,
    "MVRP : VLAN  Registration Failed.",
    "MMRP : MAC Registration Failed."
};

static INT1         gai1TempBuffer[SNMP_MAX_OID_LENGTH + 1];

/*****************************************************************************
* Function Name      : MrpTrapMakeObjIdFromDotNew                           
*                                                                           
* Description        : This function will return the OID from name of the   
*                      object                                               
*                                                                           
* Input(s)           : pi1TextStr - Object Name                             
*                                                                           
* Output(s)          : None                                                 
*                                                                           
* Return Value(s)    : pOidPtr  or NULL                                     
*****************************************************************************/
tSNMP_OID_TYPE     *
MrpTrapMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    /* TRAP_ADDITON */
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount = 0;
    INT1               *pi1TempPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT1              *pu1TempPtr = NULL;

    /* see if there is an alpha descriptor at begining */

    if (isalpha (*pi1TextStr) != 0)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0;
             ((pi1TempPtr < pi1DotPtr) && (u2Index < SNMP_MAX_OID_LENGTH));
             u2Index++)
        {
            gai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        gai1TempBuffer[u2Index] = '\0';

        for (u2Index = 0; ((u2Index < MRP_MIB_OID_TABLE_SIZE) &&
                           (orig_mib_oid_table[u2Index].pName != NULL));
             u2Index++)
        {
            if ((STRCMP
                 (orig_mib_oid_table[u2Index].pName,
                  (INT1 *) gai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) gai1TempBuffer) ==
                    STRLEN (orig_mib_oid_table[u2Index].pName)))
            {
                STRNCPY ((INT1 *) gai1TempBuffer,
                         orig_mib_oid_table[u2Index].pNumber,
                         MEM_MAX_BYTES (STRLEN
                                        (orig_mib_oid_table[u2Index].pNumber),
                                        sizeof (gai1TempBuffer)));
                gai1TempBuffer[sizeof (gai1TempBuffer) - 1] = '\0';
                break;
            }
        }

        if (u2Index < MRP_MIB_OID_TABLE_SIZE)
        {
            if (orig_mib_oid_table[u2Index].pName == NULL)
            {
                MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpTrapMakeObjIdFromDotNew : "
                             " OID not found \r\n");
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) gai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN ((INT1 *) pi1DotPtr));
    }
    else
    {
        /* is not alpha, so just copy into gai1TempBuffer */
        STRCPY ((INT1 *) gai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;

    for (u2Index = 0; ((u2Index <= SNMP_MAX_OID_LENGTH) &&
                       (gai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (gai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }

    /* The object specified may be either Tabular or Scalar
     * Object. Tabular Objects needs index to be added at the
     * end. So, allocating for Maximum OID Length
     * */

    if ((pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH)) == NULL)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpTrapMakeObjIdFromDotNew : "
                     "OID memory allocation failed \r\n");
        return (NULL);
    }

    pOidPtr->u4_Length = (UINT4) u2DotCount + 1;

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) gai1TempBuffer;
    for (u2Index = 0; ((u2Index < u2DotCount + 1)
                       && (u2Index <= SNMP_MAX_OID_LENGTH)); u2Index++)
    {
        if (MrpTrapParseSubIdNew
            ((&(pu1TempPtr)), &(pOidPtr->pu4_OidList[u2Index])) == OSIX_FAILURE)
        {
            MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpTrapMakeObjIdFromDotNew : "
                         "Parse SubId Failed. \r\n");
            SNMP_FreeOid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {

            SNMP_FreeOid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */
    return (pOidPtr);
}

/*****************************************************************************
* Function Name      : MrpTrapParseSubIdNew                                 
*                                                                           
* Description        : This function Parse the OID and returns the Sub Oid  
*                      witout Dots.                                         
*                                                                           
* Input(s)           : ppu1TempPtr                                          
*                                                                           
* Output(s)          : Sub Oid Value                                            *                                                                        
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                              
*****************************************************************************/
INT4
MrpTrapParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1              *pu1Tmp = NULL;
    UINT4               u4Value = 0;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

/*****************************************************************************
* Function Name      : MrpTrapNotifyVlanorMacRegFails                       
*                                                                           
* Description        : This function will send  trap to the administrator   
*                      when the port fails to register a VLAN or MAC.       
*                                                                           
* Input(s)           : pTrapInfo        - Trap Info                          
*                                              
*                                                                           
* Output(s)          : None                                                 
*                                                                           
* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                              
*****************************************************************************/
INT4
MrpTrapNotifyVlanOrMacRegFails (VOID *pTrapInfo)
{

    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tMacAddr            MacAddr;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tMrpTrapInfo       *pMrpTrapInfo = NULL;
    tMrpContextInfo    *pMrpContextInfo = NULL;
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT2               u2VlanId = 0;
    INT1                ai1MrpTrapOid[] = "1.3.6.1.4.1.29601.2.200.7";
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH + 1];
    UINT1              *pu1ContextName = NULL;
    UINT1              *pu1FailureReason = NULL;
    UINT1               au1TempContextName[MRP_CONTEXT_NAME_LEN];
    UINT1              *pu1TempMsg = NULL;
    UINT1              *pTrapSysLogMsg = NULL;
    UINT1               au1TempMsg[MRP_SYS_LOG_MSG_LEN];
    UINT1               au1MacAddr[MRP_MAX_MAC_STRING_SIZE];

    MEMSET (au1TempMsg, 0, MRP_SYS_LOG_MSG_LEN);
    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH + 1);
    MEMSET (au1TempContextName, 0, MRP_CONTEXT_NAME_LEN);
    pu1TempMsg = au1TempMsg;

    pTrapSysLogMsg = pu1TempMsg;

    MEMSET (au1MacAddr, 0, MRP_MAX_MAC_STRING_SIZE);

    pMrpTrapInfo = (tMrpTrapInfo *) pTrapInfo;

    if (pMrpTrapInfo == NULL)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpTrapNotifyVlanOrMacRegFails :"
                     "pMrpTrapInfo is NULL.\r\n");
        return OSIX_FAILURE;
    }

    if (pMrpTrapInfo->u1ReasonType >= MRP_MAX_FAILURE_REASONS)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpTrapNotifyVlanOrMacRegFails :"
                     "Invalid Reason .\r\n");
        return OSIX_FAILURE;
    }

    pMrpContextInfo = MRP_CONTEXT_PTR (pMrpTrapInfo->u4ContextId);

    if (pMrpContextInfo == NULL)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpTrapNotifyVlanOrMacRegFails :"
                     "Context Info Getting Failed.\r\n");
        return OSIX_FAILURE;
    }

    if (pMrpTrapInfo->pAttr->u1AttrType == MRP_VID_ATTR_TYPE)
    {
        if (pMrpContextInfo->u1MvrpTrapEnabled == MRP_DISABLED)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (pMrpContextInfo->u1MmrpTrapEnabled == MRP_DISABLED)
        {
            return OSIX_FAILURE;
        }
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    pEnterpriseOid = alloc_oid (MRP_SNMPV2_TRAP_OID_LEN);

    if (pEnterpriseOid == NULL)
    {
        MRP_GBL_TRC (ALL_FAILURE_TRC, "MrpTrapNotifyVlanOrMacRegFails :"
                     "OID Memory Allocation Failed\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, ai1MrpTrapOid, sizeof (ai1MrpTrapOid));

    pSnmpTrapOid = alloc_oid (MRP_SNMPV2_TRAP_OID_LEN);

    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        MRP_GBL_TRC (ALL_FAILURE_TRC,
                     "MrpTrapNotifyVlanOrMacRegFails: OID Memory Allocation"
                     " Failed\r\n");
        return OSIX_FAILURE;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid,
            MRP_SNMPV2_TRAP_OID_LEN * sizeof (UINT4));

    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - 1;

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        MRP_GBL_TRC (ALL_FAILURE_TRC,
                     "MrpTrapNotifyVlanOrMacRegFails: Variable Binding "
                     "Failed\r\n");
        return OSIX_FAILURE;
    }

    pStartVb = pVbList;

    SPRINTF ((char *) au1Buf, "fsMrpTrapContextName");

    pOid = MrpTrapMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  " MrpTrapNotifyVlanOrMacRegFails : OID Not Found \r\n"));
        SNMP_AGT_FreeVarBindList (pStartVb);
        return OSIX_FAILURE;
    }

    if (MrpPortVcmGetAliasName (pMrpTrapInfo->u4ContextId,
                                au1TempContextName) == VCM_FAILURE)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  " MrpTrapNotifyVlanOrMacRegFails : Context Name "
                  "Getting Failed. \r\n"));
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return OSIX_FAILURE;
    }
    pu1ContextName = au1TempContextName;

    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) pu1ContextName,
                                  (INT4) STRLEN (pu1ContextName));
    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                              0, pOstring, NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  " MrpTrapNotifyVlanOrMacRegFails : Variable Binding "
                  "Failed. \r\n"));
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return OSIX_FAILURE;
    }

    SPRINTF ((char *) pu1TempMsg, "Context Name : %s ", au1TempContextName);
    pu1TempMsg += STRLEN (pu1TempMsg);

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((char *) au1Buf, "fsMrpTrapBridgeBasePort");
    pOid = MrpTrapMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  " MrpTrapNotifyVlanOrMacRegFails : OID not found. \r\n"));
        SNMP_AGT_FreeVarBindList (pStartVb);
        return OSIX_FAILURE;
    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              pMrpTrapInfo->u2PortId, NULL, NULL,
                              SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  " MrpTrapNotifyVlanOrMacRegFails : "
                  "Variable Binding Failed. \r\n"));
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return OSIX_FAILURE;
    }
    SPRINTF ((char *) pu1TempMsg, "Port Id : %d ", pMrpTrapInfo->u2PortId);
    pu1TempMsg += STRLEN (pu1TempMsg);

    pVbList = pVbList->pNextVarBind;

    if (pMrpTrapInfo->pAttr->u1AttrType == MRP_VID_ATTR_TYPE)
    {
        SPRINTF ((char *) au1Buf, "fsMrpTrapMvrpAttributeValue");
        pOid = MrpTrapMakeObjIdFromDotNew ((INT1 *) au1Buf);

        if (pOid == NULL)
        {
            MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                      " MrpTrapNotifyVlanOrMacRegFails : "
                      " OID not found. \r\n"));
            SNMP_AGT_FreeVarBindList (pStartVb);
            return OSIX_FAILURE;
        }

        pOstring =
            SNMP_AGT_FormOctetString ((UINT1 *)
                                      &pMrpTrapInfo->pAttr->au1AttrVal,
                                      MRP_VLAN_ID_LEN);
        if (pOstring == NULL)
        {
            MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                      " MrpTrapNotifyVlanOrMacRegFails : Copy the Input String"
                      "to Octet String Failed. \r\n"));
            SNMP_FreeOid (pOid);
            SNMP_AGT_FreeVarBindList (pStartVb);
            return OSIX_FAILURE;
        }
        pVbList->pNextVarBind =
            SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                  0, pOstring, NULL, SnmpCnt64Type);

        MEMCPY (&u2VlanId, pMrpTrapInfo->pAttr->au1AttrVal, MRP_VLAN_ID_LEN);

        u2VlanId = OSIX_NTOHS (u2VlanId);

        SPRINTF ((char *) pu1TempMsg, "Attr Value : %d ", u2VlanId);
    }

    else
    {
        SPRINTF ((char *) au1Buf, "fsMrpTrapMmrpAttributeValue");
        pOid = MrpTrapMakeObjIdFromDotNew ((INT1 *) au1Buf);

        if (pOid == NULL)
        {
            MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                      " MrpTrapNotifyVlanOrMacRegFails : OID not found. \r\n"));
            SNMP_AGT_FreeVarBindList (pStartVb);
            return OSIX_FAILURE;
        }

        pOstring =
            SNMP_AGT_FormOctetString ((UINT1 *)
                                      &pMrpTrapInfo->pAttr->au1AttrVal,
                                      MRP_MAC_ADDR_LEN);
        if (pOstring == NULL)
        {
            MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                      " MrpTrapNotifyVlanOrMacRegFails : Copy the Input String"
                      "to Octet String Failed. \r\n"));
            SNMP_FreeOid (pOid);
            SNMP_AGT_FreeVarBindList (pStartVb);
            return OSIX_FAILURE;
        }
        pVbList->pNextVarBind =
            SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                  0, pOstring, NULL, SnmpCnt64Type);
        MEMCPY (MacAddr, pMrpTrapInfo->pAttr->au1AttrVal, MRP_MAC_ADDR_LEN);
        PrintMacAddress (MacAddr, au1MacAddr);

        SPRINTF ((char *) pu1TempMsg, "Attr Value : %s ", au1MacAddr);

    }

    if (pVbList->pNextVarBind == NULL)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  " MrpTrapNotifyVlanOrMacRegFails : "
                  " Variable Binding Failed. \r\n"));
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_AGT_FreeOctetString (pOstring);
        return OSIX_FAILURE;
    }

    pu1TempMsg += STRLEN (pu1TempMsg);

    pVbList = pVbList->pNextVarBind;

    SPRINTF ((char *) au1Buf, "fsMrpTrapAttrRegFailureReason");
    pOid = MrpTrapMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  " MrpTrapNotifyVlanOrMacRegFails : OID not found. \r\n"));
        SNMP_AGT_FreeVarBindList (pStartVb);
        return OSIX_FAILURE;
    }

    pu1FailureReason = (UINT1 *) gac1FailureReasons[pMrpTrapInfo->u1ReasonType];

    pOstring =
        SNMP_AGT_FormOctetString (pu1FailureReason,
                                  (INT4) STRLEN (pu1FailureReason));

    if (pOstring == NULL)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  " MrpTrapNotifyVlanOrMacRegFails : Copy the Input String"
                  "to Octet String Failed. \r\n"));
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return OSIX_FAILURE;
    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                              pOstring, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        MRP_TRC ((pMrpContextInfo, ALL_FAILURE_TRC,
                  " MrpTrapNotifyVlanOrMacRegFails : Variable Binding"
                  " Failed. \r\n"));
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return OSIX_FAILURE;
    }

    SPRINTF ((char *) pu1TempMsg, "Failure Reason : %s ", pu1FailureReason);
    pu1TempMsg += STRLEN (pu1TempMsg);

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    MrpPortNotifyFaults (pStartVb, pTrapSysLogMsg, FM_NOTIFY_MOD_ID_MRP);

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrptrap.c                      */
/*-----------------------------------------------------------------------*/
