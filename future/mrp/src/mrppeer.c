/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrppeer.c,v 1.4 2010/06/14 13:33:23 prabuc Exp $
 *
 * Description: This file contains the routines for handling Peer tracking
 *                         
 ******************************************************************************/

#include "mrpinc.h"
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPeerCrtPeerMacAddrIndxTbl
 *                                                                          
 *    DESCRIPTION      : This function creates the Peer MAC Address Index Table
 *                       RBTree.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPeerCrtPeerMacAddrIndxTbl (tMrpContextInfo * pContextInfo)
{
    pContextInfo->PeerMacAddrIndxTbl =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMrpPeerMACInfo,
                                             PeerMacAddrIndxNode),
                              (tRBCompareFn) MrpPeerMacAddrIndxTblCmpFn);

    if (pContextInfo->PeerMacAddrIndxTbl == NULL)
    {
        MRP_GBL_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
                     "MrpPeerCrtPeerMacAddrIndxTbl: RBTree creation Failed\n");
        return OSIX_FAILURE;
    }

    /* No Semaphore creation is needed inside this RBTree. */
    RBTreeDisableMutualExclusion (pContextInfo->PeerMacAddrIndxTbl);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPeerDelPeerMacAddrTbl
 *                                                                          
 *    DESCRIPTION      : This function deletes the Peer MAC Address Table RB
 *                       Tree.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
VOID
MrpPeerDelPeerMacAddrTbl (tMrpContextInfo * pContextInfo)
{
    if (pContextInfo->PeerMacAddrTbl != NULL)
    {
        RBTreeDestroy (pContextInfo->PeerMacAddrTbl,
                       (tRBKeyFreeFn) MrpPeerFreePeerMACEntry, 0);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPeerDelPeerMacAddrIndxTbl
 *                                                                          
 *    DESCRIPTION      : This function deletes the Peer MAC Address Index Table
 *                       RBTree.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
VOID
MrpPeerDelPeerMacAddrIndxTbl (tMrpContextInfo * pContextInfo)
{
    if (pContextInfo->PeerMacAddrIndxTbl != NULL)
    {
        RBTreeDestroy (pContextInfo->PeerMacAddrIndxTbl, NULL, 0);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPeerMacAddrTblCmpFn
 *                                                                          
 *    DESCRIPTION      : This routine will be invoked by the RB Tree library 
 *                       to traverse through the PeerMacAddrTbl  RB tree.
 *
 *    INPUT            : pPeerInfo1 - Pointer to the first Peer MAC Info Entry
 *                       pPeerInfo2 - Pointer to the second Peer MAC Info Entry
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : 1, if MAC Address in pPeerInfo1 is greater than
 *                          pPeerInfo2
 *                      -1, if MAC Address in pPeerInfo1 is less than 
 *                          pPeerInfo2
 *                       0, both are equal.
 *                                                                          
 ****************************************************************************/
INT4
MrpPeerMacAddrTblCmpFn (tRBElem * pPeerInfo1, tRBElem * pPeerInfo2)
{
    tMrpPeerMACInfo    *pPeerMACInfo1 = (tMrpPeerMACInfo *) pPeerInfo1;
    tMrpPeerMACInfo    *pPeerMACInfo2 = (tMrpPeerMACInfo *) pPeerInfo2;
    INT4                i4RetVal = 0;

    i4RetVal = MEMCMP (pPeerMACInfo1->PeerMacAddr, pPeerMACInfo2->PeerMacAddr,
                       MAC_ADDR_LEN);

    if (i4RetVal > 0)
    {
        return MRP_RB_GREATER;
    }
    else if (i4RetVal < 0)
    {
        return MRP_RB_LESS;
    }
    return MRP_RB_EQUAL;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpPeerFreePeerMACEntry
 *                                                                          
 *    DESCRIPTION      : Releases the Peer MAC Entry to the free pool
 *
 *    INPUT            : pElem - Pointer to the Peer MAC Address entry to be 
 *                               released
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
INT4
MrpPeerFreePeerMACEntry (tRBElem * pElem, UINT4 u4Arg)
{
    tMrpPeerMACInfo    *pPeerMACInfo = (tMrpPeerMACInfo *) pElem;
    UNUSED_PARAM (u4Arg);

    MemReleaseMemBlock (gMrpGlobalInfo.PeerMACAddrPoolId,
                        (UINT1 *) pPeerMACInfo);
    pPeerMACInfo = NULL;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpPeerAddSrcMACToPeerMACTbl 
 *
 *    DESCRIPTION      : This function adds the source MAC Address of the MRPDU
 *                       that had resulted in the state change of Registrar 
 *                       State Machine to the PeerMACTbl.
 *
 *    INPUT            : pContextInfo - Pointer to Context Info structure 
 *                       SrcMacAddr - Source MAC Address of the MRPDU that had
 *                                    resulted in the state change of Registrar
 *                                    State Machine  
 *
 *    OUTPUT           : *pu2MACIndex - Index associated with the MAC address 
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpPeerAddSrcMACToPeerMACTbl (tMrpContextInfo * pContextInfo,
                              tMacAddr SrcMacAddr, UINT2 *pu2MACIndex)
{
    tMrpPeerMACInfo     PeerMACInfo;
    tMrpPeerMACInfo    *pPeerMACInfo = NULL;

    MEMCPY (PeerMACInfo.PeerMacAddr, SrcMacAddr, MAC_ADDR_LEN);
    if ((pPeerMACInfo = RBTreeGet (pContextInfo->PeerMacAddrTbl,
                                   (tRBElem *) (&PeerMACInfo))) != NULL)
    {
        *pu2MACIndex = pPeerMACInfo->u2PeerMacIndex;
        pPeerMACInfo->u4AttrCnt++;
        return OSIX_SUCCESS;
    }

    pPeerMACInfo = (tMrpPeerMACInfo *)
        MemAllocMemBlk (gMrpGlobalInfo.PeerMACAddrPoolId);

    if (pPeerMACInfo == NULL)
    {
        MRP_TRC ((pContextInfo, MRP_CRITICAL_TRC,
                  "MrpPeerAddSrcMACToPeerMACTbl: Memory Allocation failed!\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pPeerMACInfo, 0, sizeof (tMrpPeerMACInfo));

    MEMCPY (&(pPeerMACInfo->PeerMacAddr), SrcMacAddr, MAC_ADDR_LEN);
    pPeerMACInfo->u2PeerMacIndex = pContextInfo->u2NextFreePeerMACIndex;
    pPeerMACInfo->u4AttrCnt++;

    if (RBTreeAdd (pContextInfo->PeerMacAddrTbl, (tRBElem *) pPeerMACInfo)
        != RB_SUCCESS)
    {
        MemReleaseMemBlock (gMrpGlobalInfo.PeerMACAddrPoolId,
                            (UINT1 *) pPeerMACInfo);

        pPeerMACInfo = NULL;
        return OSIX_FAILURE;
    }

    if (RBTreeAdd (pContextInfo->PeerMacAddrIndxTbl, (tRBElem *) pPeerMACInfo)
        != RB_SUCCESS)
    {
        RBTreeRem (pContextInfo->PeerMacAddrTbl, (tRBElem *) pPeerMACInfo);
        MemReleaseMemBlock (gMrpGlobalInfo.PeerMACAddrPoolId,
                            (UINT1 *) pPeerMACInfo);

        pPeerMACInfo = NULL;
        return OSIX_FAILURE;
    }

    *pu2MACIndex = pPeerMACInfo->u2PeerMacIndex;

    return OSIX_SUCCESS;
}

/****************************************************************************
 *  
 *    FUNCTION NAME    : MrpPeerGetPeerMACIndex 
 *
 *    DESCRIPTION      : This function returns the Index associated with the
 *                       MAC address
 *
 *    INPUT            : pContextInfo - Pointer to Context Info structure 
 *                       SrcMacAddr - Source MAC Address of the MRPDU that had
 *                                    resulted in the state change of Registrar
 *                                    State Machine  
 *
 *    OUTPUT           : *pu2MACIndex - Index associated with the MAC address 
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
MrpPeerGetPeerMACIndex (tMrpContextInfo * pContextInfo, tMacAddr SrcMacAddr,
                        UINT2 *pu2MACIndex)
{
    tMrpPeerMACInfo     PeerMACInfo;
    tMrpPeerMACInfo    *pPeerMACInfo = NULL;

    MEMCPY (PeerMACInfo.PeerMacAddr, SrcMacAddr, MAC_ADDR_LEN);
    if ((pPeerMACInfo = RBTreeGet (pContextInfo->PeerMacAddrTbl,
                                   (tRBElem *) (&PeerMACInfo))) != NULL)
    {
        *pu2MACIndex = pPeerMACInfo->u2PeerMacIndex;
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrppeer.c                      */
/*-----------------------------------------------------------------------*/
