/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mrptrc.h,v 1.4 2010/12/14 10:59:32 siva Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/
#ifndef _MRPTRC_H_
#define _MRPTRC_H_

#define  MRP_NAME      "MRP:" 

#define MRP_MOD_TRC   0x00000100
#define MMRP_MOD_TRC  0x00000200
#define MVRP_MOD_TRC  0x00000400

#define MRP_MIN_TRC_VAL       INIT_SHUT_TRC
#define MRP_MAX_TRC_VAL       MRP_ALL_TRC

#ifdef TRACE_WANTED

#define  MRP_GBL_TRC      MrpTrcGblTrace
#define  MRP_TRC(x)       MrpTrcPrint( __FILE__, __LINE__, MrpTrc x)
#define  MRP_ATTR_TRC      MrpTrcGblTrace

#else /* TRACE_WANTED */

#define  MRP_GBL_TRC(x, y)
#define  MRP_TRC(x) 
#define MRP_ATTR_TRC(Mask, ModuleName,Str, Arg1)

#endif /* TRACE_WANTED */

#define MRP_PROTOCOL_TRC            0x00000100
#define MRP_MVRP_TRC                0x00000200
#define MRP_MMRP_TRC                0x00000400
#define MRP_CRITICAL_TRC            0x00000800
#define MRP_APP_SEM_TRC             0x00001000
#define MRP_REG_SEM_TRC             0x00002000
#define MRP_LEAVEALL_SEM_TRC        0x00004000
#define MRP_PERIODIC_SEM_TRC        0x00008000
#define MRP_PDU_TRC                 0x00010000
#define MRP_RED_TRC                 0x00020000
#define MRP_INVALID_TRC             0x00000000

#define MRP_ALL_TRC           (MRP_PROTOCOL_TRC     |\
                               MRP_MVRP_TRC         |\
                               MRP_MMRP_TRC         |\
                               INIT_SHUT_TRC        |\
                               MGMT_TRC             |\
                               DATA_PATH_TRC        |\
                               CONTROL_PLANE_TRC    |\
                               DUMP_TRC             |\
                               OS_RESOURCE_TRC      |\
                               ALL_FAILURE_TRC      |\
                               BUFFER_TRC           |\
                               MRP_CRITICAL_TRC     |\
                               MRP_APP_SEM_TRC      |\
                               MRP_REG_SEM_TRC      |\
                               MRP_LEAVEALL_SEM_TRC |\
                               MRP_PERIODIC_SEM_TRC |\
                               MRP_PDU_TRC          |\
                               MRP_RED_TRC)

#ifdef TRACE_WANTED
#define MRP_PRINT_ATTRIBUTE(pAttr)     MrpTrcPrintAttribute ((pAttr))
#define MRP_PRINT_MAC_ADDR(u4CtxtId, u4ModId, pMacAddr)   \
        MrpTrcPrintMacAddr (u4CtxtId, u4ModId, (pMacAddr))
#define MRP_PRINT_PORT_LIST(u4ContextId, u4ModId, PortList)  \
        MrpTrcPrintPortList (u4ContextId, u4ModId, (PortList), MRP_INVALID_PORT)
#else
#define MRP_PRINT_ATTRIBUTE(pAttr)
#define MRP_PRINT_MAC_ADDR(u4CtxtId, u4ModId, pMacAddr)
#define MRP_PRINT_PORT_LIST(u4CtxtId, u4ModId, PortList)
#endif

#endif /* _MRPTRC_H */

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrptrc.h                        */
/*-----------------------------------------------------------------------*/
