/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpmacs.h,v 1.13 2013/06/25 12:08:30 siva Exp $
 *
 * Description: This file contains macro definitions used in MRP module
 *
 *****************************************************************************/
#ifndef _MRP_MACS_H
#define _MRP_MACS_H

#define MRP_DEFAULT_CONTEXT_ID          L2IWF_DEFAULT_CONTEXT + 1
#define MRP_CONTEXT_NAME_LEN            L2IWF_CONTEXT_ALIAS_LEN + 1

#define MRP_CUSTOMER_BRIDGE_MODE        VLAN_CUSTOMER_BRIDGE_MODE
#define MRP_PROVIDER_BRIDGE_MODE        VLAN_PROVIDER_BRIDGE_MODE
#define MRP_PROVIDER_EDGE_BRIDGE_MODE   VLAN_PROVIDER_EDGE_BRIDGE_MODE
#define MRP_PROVIDER_CORE_BRIDGE_MODE   VLAN_PROVIDER_CORE_BRIDGE_MODE
#define MRP_PBB_ICOMPONENT_BRIDGE_MODE  VLAN_PBB_ICOMPONENT_BRIDGE_MODE
#define MRP_PBB_BCOMPONENT_BRIDGE_MODE  VLAN_PBB_BCOMPONENT_BRIDGE_MODE
#define MRP_INVALID_BRIDGE_MODE         VLAN_INVALID_BRIDGE_MODE

#define MRP_CUSTOMER_BRIDGE_PORT        CFA_CUSTOMER_BRIDGE_PORT
#define MRP_CUSTOMER_EDGE_PORT          CFA_CUSTOMER_EDGE_PORT
#define MRP_CNP_PORTBASED_PORT          CFA_CNP_PORTBASED_PORT
#define MRP_CNP_STAGGED_PORT            CFA_CNP_STAGGED_PORT
#define MRP_PROVIDER_NETWORK_PORT       CFA_PROVIDER_NETWORK_PORT
#define MRP_PROP_CUSTOMER_EDGE_PORT     CFA_PROP_CUSTOMER_EDGE_PORT
#define MRP_PROP_CUSTOMER_NETWORK_PORT  CFA_PROP_CUSTOMER_NETWORK_PORT
#define MRP_PROP_PROVIDER_NETWORK_PORT  CFA_PROP_PROVIDER_NETWORK_PORT
#define MRP_VIRTUAL_INSTANCE_PORT       CFA_VIRTUAL_INSTANCE_PORT
#define MRP_PROVIDER_INSTANCE_PORT      CFA_PROVIDER_INSTANCE_PORT
#define MRP_CUSTOMER_BACKBONE_PORT      CFA_CUSTOMER_BACKBONE_PORT
#define MRP_CNP_CTAGGED_PORT            CFA_CNP_CTAGGED_PORT
#define MRP_INVALID_BRIDGE_PORT       CFA_INVALID_BRIDGE_PORT

#define MRP_SIZING_CONTEXT_COUNT  (FsMRPSizingParams[MAX_MRP_CONTEXTS_SIZING_ID].u4PreAllocatedUnits + 1)

#define MRP_IS_VC_VALID(u4ContextId) \
    (((u4ContextId == 0) || (u4ContextId >= MRP_SIZING_CONTEXT_COUNT)) ? \
     OSIX_FALSE : OSIX_TRUE)

/* This macro is used to convert the context ID given from MRP 
 * module to external modules*/
#define MRP_CONVERT_COMP_ID_TO_CTXT_ID(u4ContextId) \
    u4ContextId = (u4ContextId - 1)

/* This macro is used to convert the context ID given from 
 * external modules to MRP module */
#define MRP_CONVERT_CTXT_ID_TO_COMP_ID(u4ContextId) \
    u4ContextId = (u4ContextId + 1)


#define MRP_IS_MRP_APP_ENABLED(u4ContextId, u1AppId) \
    ((u1AppId == MRP_MVRP_APP_ID) ? \
        MrpMvrpIsEnabled (u4ContextId) : MrpMmrpIsEnabled (u4ContextId))

#define MRP_OFFSET(x, y)                FSAP_OFFSETOF(x,y)


#define MRP_CONTEXT_PTR(u4CtxtId)       gMrpGlobalInfo.apContextInfo[u4CtxtId]

#define MRP_IS_MRP_STARTED(u4ContextId) \
    ((gMrpGlobalInfo.au1MrpSystemCtrl[u4ContextId] == MRP_START) ? OSIX_TRUE : OSIX_FALSE)


#define MRP_GET_PORT_ENTRY(pContextInfo, u2Port) \
    ((u2Port > 0 && u2Port <= MRP_MAX_PORTS_PER_CONTEXT) ? \
     pContextInfo->apMrpPortEntry[u2Port]: NULL)
#define MRP_GET_APP_ENTRY(pContextInfo, u1AppId) \
    &(pContextInfo->aMrpAppTable[u1AppId])

#define MRP_GET_MAP_ENTRY(pAppEntry, u2MapId) \
    ((u2MapId < pAppEntry->u2MapArraySize)? \
     pAppEntry->ppMapTable[u2MapId]: NULL)

#define MRP_GET_APP_PORT_ENTRY(pAppEntry, u2Port) \
    ((u2Port <= MRP_MAX_PORTS_PER_CONTEXT)? \
    pAppEntry->apAppPortEntry[u2Port]: NULL)

#define MRP_GET_ATTR_ENTRY(pMapEntry, u2AttrIndex) \
    ((u2AttrIndex <= pMapEntry->u2AttrArraySize)? \
    pMapEntry->ppAttrEntryArray[u2AttrIndex]:NULL)
#define MRP_GET_PEER_MAC_ADDR(pContextInfo, u2PeerMACIndex) \
    ((u2PeerMACIndex < pContextInfo->u2PeerMACArraysize)? \
      pContextInfo->ppPeerMacAddr[u2PeerMACIndex]:NULL)
#define MRP_GET_MAP_PORT_ENTRY(pMapEntry, u2Port) \
    ((u2Port <= MRP_MAX_PORTS_PER_CONTEXT) ?\
    pMapEntry->apMapPortEntry[u2Port]: NULL)

#define MRP_GET_APP_PORT_FRM_MAP_PORT(pMapPortEntry, u2Port) \
    ((u2Port <= MRP_MAX_PORTS_PER_CONTEXT) ? \
pMapPortEntry->pMapEntry->pMrpAppEntry->apAppPortEntry[u2Port]:NULL)

   
#define MRP_IS_PORT_VALID(u2Port) \
   (((u2Port > MRP_MAX_PORTS_PER_CONTEXT) || (u2Port == 0)) ? \
     OSIX_FALSE : OSIX_TRUE)

#define MRP_IS_802_1AD_BRIDGE(pContextInfo) \
    (((pContextInfo->u4BridgeMode  == MRP_PROVIDER_EDGE_BRIDGE_MODE) || \
      (pContextInfo->u4BridgeMode  == MRP_PROVIDER_CORE_BRIDGE_MODE)) ? \
     OSIX_TRUE : OSIX_FALSE)

#define MRP_IS_802_1AD_1AH_BRIDGE(pContextInfo) \
    (((pContextInfo->u4BridgeMode  == MRP_PROVIDER_EDGE_BRIDGE_MODE) || \
      (pContextInfo->u4BridgeMode  == MRP_PROVIDER_CORE_BRIDGE_MODE) || \
      (pContextInfo->u4BridgeMode  == MRP_PBB_ICOMPONENT_BRIDGE_MODE) || \
      (pContextInfo->u4BridgeMode == MRP_PBB_BCOMPONENT_BRIDGE_MODE)) ? \
     OSIX_TRUE : OSIX_FALSE)

#define MRP_IS_802_1AD_CUSTOMER_PORTS(pPortEntry) \
    (((pPortEntry->u2BridgePortType  == MRP_CUSTOMER_EDGE_PORT) || \
      (pPortEntry->u2BridgePortType  == MRP_CNP_PORTBASED_PORT) || \
      (pPortEntry->u2BridgePortType  == MRP_PROP_CUSTOMER_EDGE_PORT) || \
      (pPortEntry->u2BridgePortType  == MRP_PROP_CUSTOMER_NETWORK_PORT)) \
     ? OSIX_TRUE : OSIX_FALSE)

#define MRP_IS_802_1AH_CUSTOMER_PORTS(pPortEntry) \
    ((pPortEntry->u2BridgePortType == MRP_CUSTOMER_BACKBONE_PORT) \
     ? OSIX_TRUE : OSIX_FALSE)


#define MRP_GET_ATTR_REG_ADMIN_CTRL(u1AttrInfo)\
    (UINT1)((u1AttrInfo & MRP_REG_ADMIN_CTRL_BITMASK) >> 6)

#define MRP_GET_ATTR_REG_SEM_STATE(u1AttrInfo)\
    (UINT1)((u1AttrInfo & MRP_REG_SEM_STATE_BITMASK) >> 4)

#define MRP_GET_ATTR_APP_SEM_STATE(u1AttrInfo)\
    (u1AttrInfo & MRP_APP_SEM_STATE_BITMASK)

#define MRP_SET_ATTR_REG_ADMIN_CTRL(u1AttrInfo, u1RegCtrl)\
    u1AttrInfo = (UINT1)((u1AttrInfo & 0x3F) | (u1RegCtrl << 6))

#define MRP_SET_ATTR_REG_SEM_STATE(u1AttrInfo, u1RegState)\
    u1AttrInfo = (UINT1)((u1AttrInfo & 0xCF) | (u1RegState << 4))

#define MRP_SET_ATTR_APP_SEM_STATE(u1AttrInfo, u1AppState)\
    u1AttrInfo = (UINT1) ((u1AttrInfo & 0xF0) | u1AppState)

#define MRP_IS_MCASTADDR(MacAddr) \
    ((FS_UTIL_IS_MCAST_MAC(MacAddr) == OSIX_TRUE) ? OSIX_TRUE : OSIX_FALSE)



#define MRP_GET_RANDOM_LVALL_TIME(u4TimeVal, u4OutVal) \
    OsixGetSysTime((tOsixSysTime *)&(u4OutVal)); \
    (u4OutVal) = (u4OutVal) % ((u4TimeVal) / 2);\
    if (u4OutVal == 0) \
    {\
        (u4OutVal)++; \
    }\
    (u4OutVal) = (u4TimeVal) + (u4OutVal)

#define MRP_RANDOM_TIMEOUT(u4TimeOut, u4RandVal) \
{\
    OsixGetSysTime((tOsixSysTime *)&u4RandVal); \
    (u4RandVal) = (u4RandVal) % (u4TimeOut) + 1;\
}

#define MRP_GET_LV_TMR_INTERVAL(pPortEntry) \
    (pPortEntry->u4LeaveTime / MRP_LEAVE_SPLIT_INTERVAL)

#define MRP_IS_VALID_MVRP_ATTR_TYPE(i4AttrType) \
    ((i4AttrType  != MRP_VID_ATTR_TYPE) ? OSIX_FALSE : OSIX_TRUE)

#define MRP_IS_VALID_MMRP_ATTR_TYPE(i4AttrType) \
    (((i4AttrType == MRP_SERVICE_REQ_ATTR_TYPE) || \
      (i4AttrType == MRP_MAC_ADDR_ATTR_TYPE)) ? OSIX_TRUE : OSIX_FALSE)

#define MRP_GET_PHY_PORT(u4CtxtId, u2Port)\
    (gMrpGlobalInfo.apContextInfo[u4CtxtId]->apMrpPortEntry[u2Port])->u4IfIndex

#define MRP_LBUF_DEC_BY_2_BYTES(pBuf, u2Offset) \
{\
    pBuf -= 2;\
    u2Offset = (UINT2) (u2Offset - 2);\
}

#define MRP_LBUF_GET_1_BYTE(pBuf, u2Offset, u1Value) \
{\
    u1Value = pBuf[0]; \
    pBuf += 1;\
    u2Offset = (UINT2) (u2Offset + 1);\
}
#define MRP_LBUF_GET_2_BYTES(pBuf, u2Offset, u2Value) \
{\
    UINT2 u2TemVal = 0; \
    MEMCPY (&u2TemVal, pBuf, 2);\
    u2Value = (UINT2) (OSIX_NTOHS(u2TemVal));\
    pBuf += 2;\
    u2Offset = (UINT2) (u2Offset + 2);\
}

#define MRP_LBUF_PUT_1_BYTE(pBuf, u1Value, u2Offset) \
{\
    *pBuf = u1Value; \
    pBuf += 1;\
    u2Offset = (UINT2) (u2Offset + 1);\
}

#define MRP_LBUF_PUT_2_BYTES(pBuf, u2Value, u2Offset) \
{\
    u2Value = (UINT2) OSIX_HTONS (u2Value);\
    MEMCPY (pBuf, &u2Value, 2);\
    pBuf += 2;\
    u2Offset = (UINT2) (u2Offset + 2);\
}

#define MRP_LBUF_PUT_MAC_ADDRESS(pBuf, pValue, u2Offset) \
{\
    MEMCPY (pBuf, pValue, 6);\
    pBuf += 6;\
    u2Offset = (UINT2) (u2Offset + 6);\
}

#define MRP_LBUF_PUT_N_BYTES(pBuf, pValue, u2Len, u2Offset) \
{\
    MEMCPY (pBuf, pValue, u2Len);\
    pBuf += u2Len;\
    u2Offset = (UINT2) (u2Offset + u2Len);\
}

#define MRP_IS_LEAVE_TIME_VALID(LeaveTime, LeaveAllTime, JoinTime) \
        (((LeaveTime < LeaveAllTime) && (LeaveTime > (2 * JoinTime))) ?\
           OSIX_TRUE : OSIX_FALSE)

#define MRP_IS_JOIN_TIME_VALID(JoinTime, LeaveTime) \
    (((JoinTime > 0 ) &&((2 * JoinTime) < LeaveTime)) ? OSIX_TRUE : OSIX_FALSE)

#define MRP_IS_LEAVE_ALL_TIME_VALID(LeaveAllTime, LeaveTime) \
  (((LeaveAllTime != 0) && (LeaveAllTime > LeaveTime)) ? \
        OSIX_TRUE : OSIX_FALSE)

#define MRP_LBUF_GET_N_BYTES(pBuf, pu1Dest, u2Len, u2Offset) \
{\
    MEMCPY (pu1Dest, pBuf, u2Len);\
    pBuf += (UINT1)u2Len;\
    u2Offset = (UINT2) (u2Offset + u2Len);\
}

#define MRP_GET_NUM_OF_VAL_FIELD(u2VectHdr, u2AttrNum)\
    u2AttrNum = (UINT2)(u2VectHdr & MRP_NUM_OF_VAL_FIELD_BITMASK)

#define MRP_DECODE_LV_ALL_EVENT(u2VectHdr, u1LvAllEvent)\
    u1LvAllEvent = (UINT1)((u2VectHdr & MRP_LV_ALL_EVENT_BITMASK) >> 13)

#define MRP_GET_NUM_OF_VECTORS_FIELD(u2Value)\
    (((u2Value % 3) == 0) ? (u2Value / 3) : ((u2Value / 3) + 1))
#define MRP_IS_RESERVED_MRP_APP_ADDR(pAddr) \
         (((MEMCMP(pAddr, gMrpReservedAddr,(ETHERNET_ADDR_SIZE-1)) == 0) \
          &&((pAddr[5] >= gMmrpAddr[5])&& (pAddr[5] <= gMrpReservedAddr[5])))? \
          OSIX_TRUE : OSIX_FALSE)
#define MRP_IS_NULL_MAC_ADDR(pAddr) \
    ((MEMCMP(pAddr, gMrpNullMacAddress, ETHERNET_ADDR_SIZE) == 0)? OSIX_TRUE :\
      OSIX_FALSE)

#define MRP_VLAN_IS_BCASTADDR(pMacAddr) \
        ((MEMCMP(pMacAddr,gVlanBcastAddress,ETHERNET_ADDR_SIZE) == 0) ? \
          OSIX_TRUE : OSIX_FALSE)

#define MRP_VLAN_IS_RESERVED_ADDR(pAddr) \
         (((MEMCMP(pAddr,gVlanReservedAddress,(ETHERNET_ADDR_SIZE-1)) == 0) \
          &&(pAddr[5] <= gVlanReservedAddress[5])) ? \
          OSIX_TRUE : OSIX_FALSE)


#define MRP_GET_ATTR_EVENTS(u1Vector, pu1AttrEvents)\
{\
    pu1AttrEvents[0] = (UINT1) (u1Vector % 6);\
    pu1AttrEvents[1] = (UINT1) ((u1Vector / 6) % 6);\
    pu1AttrEvents[2] = (UINT1) ((u1Vector / 6) / 6);\
}

#define MRP_GET_RESTRICTED_REG_CTRL(u1AppId, pPortEntry)\
    ((u1AppId == MRP_MVRP_APP_ID) ? pPortEntry->u1RestrictedVlanRegCtrl : \
     pPortEntry->u1RestrictedMACRegCtrl)
#define MRP_PORT_APP_STATUS(pMrpPortEntry,u1AppId)\
    ((u1AppId == MRP_MVRP_APP_ID) ? pMrpPortEntry->u1PortMvrpStatus : \
     pMrpPortEntry->u1PortMmrpStatus)

#define MRP_TX_LEAVE_ALL          (MRP_INVALID_EVENT + 1) 
#define MRP_TX_PDU_CNT            (MRP_INVALID_EVENT + 2) 

#define MRP_INCR_RX_INVALID_PDU_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4RxInvalidPduCnt++;\
    }\
}
#define MRP_INCR_RX_VALID_PDU_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4RxValidPduCnt++;\
    }\
}
#define MRP_INCR_RX_NEW_MSG_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4RxNewMsgCnt++;\
    }\
}
#define MRP_INCR_RX_JOIN_IN_MSG_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4RxJoinInMsgCnt++;\
    }\
}
#define MRP_INCR_RX_JOIN_MT_MSG_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4RxJoinMtMsgCnt++;\
    }\
}
#define MRP_INCR_RX_LEAVE_MSG_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4RxLeaveMsgCnt++;\
    }\
}
#define MRP_INCR_RX_IN_MSG_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4RxInMsgCnt++;\
    }\
}
#define MRP_INCR_RX_EMPTY_MSG_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4RxEmptyMsgCnt++;\
    }\
}
#define MRP_INCR_RX_LV_ALL_MSG_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4RxLeaveAllMsgCnt++;\
    }\
}

#define MRP_INCR_PORT_REG_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4NoOfRegistration++;\
    }\
}
#define MRP_DECR_PORT_REG_CNT(pMrpPortEntry, u1AppId)\
{\
    if ((u1AppId == MRP_MVRP_APP_ID) || (u1AppId == MRP_MMRP_APP_ID))\
    {\
        pMrpPortEntry->pStatsEntry[u1AppId]->u4NoOfRegistration--;\
    }\
}
#define MRP_UPDT_LAST_PDU_ORIGIN(pMrpPortEntry, u1AppId, SrcMacAddr)\
    ((u1AppId == MRP_MVRP_APP_ID) ? MEMCPY (&pMrpPortEntry->LastMvrpPduOrigin, &SrcMacAddr, ETHERNET_ADDR_SIZE)\
     : MEMCPY (&pMrpPortEntry->LastMmrpPduOrigin, &SrcMacAddr, ETHERNET_ADDR_SIZE))

#define MRP_IS_VIP_INTERFACE(u4IfIndex) \
    (((u4IfIndex >= CFA_MIN_VIP_IF_INDEX) && \
      (u4IfIndex <= CFA_MAX_VIP_IF_INDEX))?OSIX_TRUE:OSIX_FALSE)

/* Following macro is introduced to avoid klockwork warnings */
#define MRP_CHK_NULL_PTR_RET(ptr, RetVal)\
{\
    if(ptr == NULL) \
    {\
        return RetVal;\
    }\
}

/* The following macro is used to find the actual memory to be
 * allocated using buddy pool
 * u2HdrSize - temporary variable used to calculate the memory
 * u4MinBlk  - Min block size
 * u4MaxBlk  - Maximum block size
 * sizingId  - Global array index to be updated
 */
#define MRP_CALCULATE_BUDDY_MEMORY(u2HdrSize,u4MinBlk,u4MaxBlk,sizingId) \
    u2HdrSize = (UINT2) \
                (4 * (((((u4MaxBlk / u4MinBlk) * 2) - 1) / 32) \
                      + 1)); \
    gFsMrpSizingParams[sizingId].u4PreAllocatedUnits = \
        ((u4MaxBlk  / u4MinBlk) * sizeof (VOID *)) + \
        ((u2HdrSize + u4MaxBlk ) * 1);

#define MRP_NOTIFY_PORT_P2P(u4IfIndex, bOperP2PStatus)  \
       MrpApiNotifyPortOperP2PChg (u4IfIndex, bOperP2PStatus);

#define MRP_NOTIFY_PORT_ROLE_CHANGE(u4IfIndex, u2MapID, u1OldRole, u1NewRole) \
 MrpApiNotifyPortRoleChange (u4IfIndex, u2MapID, u1OldRole, u1NewRole);

#define MRP_NOTIFY_TC_DETECTED_TMR_STATE(u4IfIndex, u2MapID, u1TmrState) \
 MrpApiNotifyTcDetectedTmrState (u4IfIndex, u2MapID, u1TmrState);

#define MRP_NOTIFY_PORT_STATE_CHANGE(u4IfIndex, u2MapId, u1State) \
 MrpApiNotifyPortStateChange (u4IfIndex, u2MapId, u1State);

#define MRP_MVRP_PROP_VLAN_INFO(u4ContextId, VlanId, AddPortList, DelPortList) \
 MrpApiMvrpPropagateVlanInfo (u4ContextId, VlanId, AddPortList, \
   DelPortList);

#define MRP_MVRP_SET_VLAN_FORBID_PORTS(u4ContextId, VlanId, AddPortList, \
  DelPortList) \
MrpApiMvrpSetVlanForbiddPorts (u4ContextId, VlanId, AddPortList, DelPortList);

#define MRP_MMRP_PROP_MAC_INFO(u4ContextId, MacAddr, VlanId, AddPortList, \
  DelPortList) \
MrpApiMmrpPropagateMacInfo (u4ContextId, MacAddr, VlanId, AddPortList, \
  DelPortList);

#define MRP_MMRP_SER_MCAST_FORBID_PORTS(u4ContextId, MacAddr, VlanId, \
  AddPortList, DelPortList) \
MrpApiMmrpSetMcastForbiddPorts (u4ContextId, MacAddr, VlanId, AddPortList, \
  DelPortList);

#define MRP_MAP_UPD_MAP_PORTS(u4ContextId, AddPortList, DelPortList, u2MapId) \
 MrpApiMrpMapUpdateMapPorts (u4ContextId, AddPortList, DelPortList, u2MapId);

#define MRP_FILL_BULK_MSG(u4ContextId, ppMrpQMsg, u1MsgType, VlanId, u2Port, \
  Ports, pu1MacAddr) \
MrpApiFillBulkMessage (u4ContextId, ppMrpQMsg, u1MsgType, VlanId, u2Port, \
  Ports, pu1MacAddr);


#endif /*_MRP_MACS_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpmacs.h                      */
/*-----------------------------------------------------------------------*/
