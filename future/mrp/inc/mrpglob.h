/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpglob.h,v 1.14 2011/04/27 11:59:34 siva Exp $
 *
 * Description: This file contains the global structure used in MRP module. 
 *
 *****************************************************************************/
#ifndef _MRPGLOB_H
#define _MRPGLOB_H

/*-------------- GLOBAL DECLARATIONS -------------------------------*/


tMacAddr            gVlanReservedAddress = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x0F };
tMacAddr            gVlanBcastAddress = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

tMrpGlobalInfo          gMrpGlobalInfo;
UINT1                   gau1MrpBitMaskMap [MRP_PORTS_PER_BYTE] = { 0x01, 0x80,
                                   0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };
tMacAddr                gMvrpAddr = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x21 };
tMacAddr                gMmrpAddr = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x20 };
tMacAddr                gProviderMvrpAddr = { 0x01, 0x80, 0xC2, 
                                              0x00, 0x00, 0x0D };
tMacAddr                gCustomerMvrpAddr = { 0x01, 0x80, 0xC2, 
                                              0x00, 0x00, 0x21 };
tMacAddr                gMrpReservedAddr = { 0x01, 0x80, 0xC2, 
                                              0x00, 0x00, 0x2F };
tMacAddr                gMrpNullMacAddress = { 0x00, 0x00, 0x00, 0x00, 0x00,
                                               0x00 };
tLocalPortList          gMrpNullPortList;

UINT2                   gau2SecVlanList[VLAN_MAX_COMMUNITY_VLANS + 1];

CHR1               *gau1RegSEMState[] = {
    "MT",
    "IN",
    "LV"
};

CHR1               *gau1AppSEMState[] = {
    "VO",
    "VP",
    "VN",
    "AN",
    "AA",
    "QA",
    "LA",
    "AO",
    "QO",
    "AP",
    "QP",
    "LO"
};

CHR1               *gau1AppAdminStatus[] = {
    NULL,
    "Normal",
    "Non-Participant",
    "Active"
};

CHR1               *gau1RegAdminCtrl[] = {
    "Normal",
    "Fixed",
    "Forbidden"
};

#ifdef TRACE_WANTED
CHR1               *gau1RegSEMEvtName[] = {
    "RX_NEW",
    "RX_JOIN_IN",
    "RX_JOIN_MT",
    "RX_LEAVE",
    "RX_LEAVE_ALL",
    "TX_LEAVE_ALL",
    "REDECLARE",
    "FLUSH",
    "LEAVE_TMR_EXPIRY"
};

CHR1               *gau1AppSEMEvtName[] = {
    "REQ_NEW",
    "REQ_JOIN",
    "REQ_LEAVE",
    "RX_NEW",
    "RX_JOIN_IN",
    "RX_IN",
    "RX_JOIN_MT",
    "RX_MT",
    "RX_LEAVE",
    "RX_LEAVE_ALL",
    "REDECLARE",
    "PERIODIC",
    "TX",
    "TX_LA",
    "TX_LAF"
};
#endif

/*-------------- REGISTRAR STATE EVENT MACHINE ---------------------*/

tMrpRegSemEntry         gaMrpRegSem [MRP_MAX_REG_EVENTS][MRP_MAX_REG_STATES] = 
{
    { /* MRP_REG_RX_NEW */
        /* MRP_MT */ {MRP_HL_IND_NEW,   MRP_TMR_NONE, MRP_IN},
        /* MRP_IN */ {MRP_HL_IND_NEW,   MRP_TMR_NONE, MRP_IN}, 
        /* MRP_LV */ {MRP_HL_IND_NEW,   MRP_TMR_STOP, MRP_IN} 
    },
    { /* MRP_REG_RX_JOIN_IN */
        /* MRP_MT */ {MRP_HL_IND_JOIN,  MRP_TMR_NONE, MRP_IN}, 
        /* MRP_IN */ {MRP_HL_IND_NONE,  MRP_TMR_NONE, MRP_IN}, 
        /* MRP_LV */ {MRP_HL_IND_NONE,  MRP_TMR_STOP, MRP_IN} 
    },
    { /* MRP_REG_RX_JOIN_MT */
        /* MRP_MT */ {MRP_HL_IND_JOIN,  MRP_TMR_NONE, MRP_IN}, 
        /* MRP_IN */ {MRP_HL_IND_NONE,  MRP_TMR_NONE, MRP_IN}, 
        /* MRP_LV */ {MRP_HL_IND_NONE,  MRP_TMR_STOP, MRP_IN} 
    },
    { /* MRP_REG_RX_LEAVE */
        /* MRP_MT */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_MT},
        /* MRP_IN */ {MRP_HL_IND_NONE,  MRP_TMR_START, MRP_LV}, 
        /* MRP_LV */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_LV} 
    },
    { /* MRP_REG_RX_LEAVE_ALL */
        /* MRP_MT */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_MT},
        /* MRP_IN */ {MRP_HL_IND_NONE,  MRP_TMR_START, MRP_LV}, 
        /* MRP_LV */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_LV} 
    },
    { /* MRP_REG_TX_LEAVE_ALL */
        /* MRP_MT */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_MT},
        /* MRP_IN */ {MRP_HL_IND_NONE,  MRP_TMR_START, MRP_LV}, 
        /* MRP_LV */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_LV} 
    },
    { /* MRP_REG_REDECLARE */
        /* MRP_MT */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_MT},
        /* MRP_IN */ {MRP_HL_IND_NONE,  MRP_TMR_START, MRP_LV}, 
        /* MRP_LV */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_LV} 
    },
    { /* MRP_REG_FLUSH */
        /* MRP_MT */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_MT},
        /* MRP_IN */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_MT}, 
        /* MRP_LV */ {MRP_HL_IND_LEAVE, MRP_TMR_NONE,  MRP_MT} 
    },
    { /* MRP_REG_LEAVE_TMR_EXPIRY */
        /* MRP_MT */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_MT},
        /* MRP_IN */ {MRP_HL_IND_NONE,  MRP_TMR_NONE,  MRP_IN}, 
        /* MRP_LV */ {MRP_HL_IND_LEAVE, MRP_TMR_NONE,  MRP_MT} 
    }
};

/*-------------- APPLICANT STATE EVENT MACHINE ---------------------*/

tMrpAppSemEntry  gMrpAppAction1 = {MRP_SEND_NONE, MRP_NO_CHANGE, OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction2 = {MRP_SEND_NONE, MRP_VN, OSIX_TRUE}; 
tMrpAppSemEntry  gMrpAppAction3 = {MRP_SEND_NONE, MRP_VP, OSIX_TRUE}; 
tMrpAppSemEntry  gMrpAppAction4 = {MRP_SEND_NONE, MRP_AA, OSIX_TRUE}; 
tMrpAppSemEntry  gMrpAppAction5 = {MRP_SEND_NONE, MRP_AP, OSIX_TRUE}; 
tMrpAppSemEntry  gMrpAppAction6 = {MRP_SEND_NONE, MRP_QP, OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction7 = {MRP_SEND_NONE, MRP_VO, OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction8 = {MRP_SEND_NONE, MRP_LA, OSIX_TRUE}; 
tMrpAppSemEntry  gMrpAppAction9 = {MRP_SEND_NONE, MRP_AO, OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction10 = {MRP_SEND_NONE, MRP_QO, OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction11 = {MRP_SEND_NONE, MRP_QA, OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction12 = {MRP_SEND_NONE, MRP_LO, OSIX_TRUE}; 
tMrpAppSemEntry  gMrpAppAction13 = {MRP_SEND_IN_OR_MT_OPTIONAL, MRP_NO_CHANGE, 
                                    OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction14 = {MRP_SEND_JOIN, MRP_AA, OSIX_TRUE}; 
tMrpAppSemEntry  gMrpAppAction15 = {MRP_SEND_NEW, MRP_AN, OSIX_TRUE}; 
tMrpAppSemEntry  gMrpAppAction16 = {MRP_SEND_NEW, MRP_QA, OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction17 = {MRP_SEND_JOIN, MRP_QA, OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction18 = {MRP_SEND_JOIN_OPTIONAL, MRP_NO_CHANGE, 
                                    OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction19 = {MRP_SEND_LEAVE, MRP_VO, OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction20 = {MRP_SEND_IN_OR_MT, MRP_VO, OSIX_FALSE}; 
tMrpAppSemEntry  gMrpAppAction21 = {MRP_SEND_IN_OR_MT_OPTIONAL, MRP_LO, 
                                    OSIX_TRUE}; 
tMrpAppSemEntry  gMrpAppAction22 = {MRP_SEND_IN_OR_MT, MRP_AA, OSIX_TRUE}; 
tMrpAppSemEntry  gMrpAppAction23 = {MRP_SEND_JOIN, MRP_NO_CHANGE, OSIX_FALSE}; 

tMrpAppSemEntry        *gaMrpFullAppSem [MRP_MAX_APP_EVENTS][MRP_MAX_APP_STATES] = 
{
    { /* MRP_APP_REQ_NEW */
        /* MRP_VO */ &gMrpAppAction2, 
        /* MRP_VP */ &gMrpAppAction2, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction2, 
        /* MRP_QA */ &gMrpAppAction2, 
        /* MRP_LA */ &gMrpAppAction2, 
        /* MRP_AO */ &gMrpAppAction2, 
        /* MRP_QO */ &gMrpAppAction2, 
        /* MRP_AP */ &gMrpAppAction2, 
        /* MRP_QP */ &gMrpAppAction2, 
        /* MRP_LO */ &gMrpAppAction2 
    },
    { /* MRP_APP_REQ_JOIN */
        /* MRP_VO */ &gMrpAppAction3, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction4, 
        /* MRP_AO */ &gMrpAppAction5, 
        /* MRP_QO */ &gMrpAppAction6, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction3 
    },
    { /* MRP_APP_REQ_LEAVE */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction7, 
        /* MRP_VN */ &gMrpAppAction8, 
        /* MRP_AN */ &gMrpAppAction8, 
        /* MRP_AA */ &gMrpAppAction8, 
        /* MRP_QA */ &gMrpAppAction8, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction9, 
        /* MRP_QP */ &gMrpAppAction10, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_NEW */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_JOIN_IN */
        /* MRP_VO */ &gMrpAppAction9, 
        /* MRP_VP */ &gMrpAppAction5, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction11, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction10, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction6, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_IN */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_JOIN_MT */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction9, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction5, 
        /* MRP_LO */ &gMrpAppAction7 
    },
    { /* MRP_APP_RX_MT */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction9, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction5, 
        /* MRP_LO */ &gMrpAppAction7 
    },
    { /* MRP_APP_RX_LEAVE */
        /* MRP_VO */ &gMrpAppAction12, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction12, 
        /* MRP_QO */ &gMrpAppAction12, 
        /* MRP_AP */ &gMrpAppAction3, 
        /* MRP_QP */ &gMrpAppAction3, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_LEAVE_ALL */
        /* MRP_VO */ &gMrpAppAction12, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction12, 
        /* MRP_QO */ &gMrpAppAction12, 
        /* MRP_AP */ &gMrpAppAction3, 
        /* MRP_QP */ &gMrpAppAction3, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_REDECLARE */
        /* MRP_VO */ &gMrpAppAction12, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction12, 
        /* MRP_QO */ &gMrpAppAction12, 
        /* MRP_AP */ &gMrpAppAction3, 
        /* MRP_QP */ &gMrpAppAction3, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_PERIODIC */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction5, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_TX */
        /* MRP_VO */ &gMrpAppAction13, 
        /* MRP_VP */ &gMrpAppAction14, 
        /* MRP_VN */ &gMrpAppAction15, 
        /* MRP_AN */ &gMrpAppAction16, 
        /* MRP_AA */ &gMrpAppAction17, 
        /* MRP_QA */ &gMrpAppAction18, 
        /* MRP_LA */ &gMrpAppAction19, 
        /* MRP_AO */ &gMrpAppAction13, 
        /* MRP_QO */ &gMrpAppAction13, 
        /* MRP_AP */ &gMrpAppAction17, 
        /* MRP_QP */ &gMrpAppAction13, 
        /* MRP_LO */ &gMrpAppAction20 
    },
    { /* MRP_APP_TX_LA */
        /* MRP_VO */ &gMrpAppAction21, 
        /* MRP_VP */ &gMrpAppAction22, 
        /* MRP_VN */ &gMrpAppAction15, 
        /* MRP_AN */ &gMrpAppAction16, 
        /* MRP_AA */ &gMrpAppAction17, 
        /* MRP_QA */ &gMrpAppAction23, 
        /* MRP_LA */ &gMrpAppAction21, 
        /* MRP_AO */ &gMrpAppAction21, 
        /* MRP_QO */ &gMrpAppAction21, 
        /* MRP_AP */ &gMrpAppAction17, 
        /* MRP_QP */ &gMrpAppAction17, 
        /* MRP_LO */ &gMrpAppAction13 
    },
    { /* MRP_APP_TX_LAF */
        /* MRP_VO */ &gMrpAppAction12, 
        /* MRP_VP */ &gMrpAppAction3, 
        /* MRP_VN */ &gMrpAppAction2, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction12, 
        /* MRP_AO */ &gMrpAppAction12, 
        /* MRP_QO */ &gMrpAppAction12, 
        /* MRP_AP */ &gMrpAppAction3, 
        /* MRP_QP */ &gMrpAppAction3, 
        /* MRP_LO */ &gMrpAppAction1 
    }
};

tMrpAppSemEntry        *gaMrpFullAppP2PSem [MRP_MAX_APP_EVENTS][MRP_MAX_APP_STATES] = 
{
    { /* MRP_APP_REQ_NEW */
        /* MRP_VO */ &gMrpAppAction2, 
        /* MRP_VP */ &gMrpAppAction2, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction2, 
        /* MRP_QA */ &gMrpAppAction2, 
        /* MRP_LA */ &gMrpAppAction2, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction2 
    },
    { /* MRP_APP_REQ_JOIN */
        /* MRP_VO */ &gMrpAppAction3, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction4, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction3 
    },
    { /* MRP_APP_REQ_LEAVE */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction7, 
        /* MRP_VN */ &gMrpAppAction8, 
        /* MRP_AN */ &gMrpAppAction8, 
        /* MRP_AA */ &gMrpAppAction8, 
        /* MRP_QA */ &gMrpAppAction8, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_NEW */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_JOIN_IN */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction11, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_IN */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction11, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_JOIN_MT */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction7 
    },
    { /* MRP_APP_RX_MT */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction7 
    },
    { /* MRP_APP_RX_LEAVE */
        /* MRP_VO */ &gMrpAppAction12, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_LEAVE_ALL */
        /* MRP_VO */ &gMrpAppAction12, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_REDECLARE */
        /* MRP_VO */ &gMrpAppAction12, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_PERIODIC */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_TX */
        /* MRP_VO */ &gMrpAppAction13, 
        /* MRP_VP */ &gMrpAppAction14, 
        /* MRP_VN */ &gMrpAppAction15, 
        /* MRP_AN */ &gMrpAppAction16, 
        /* MRP_AA */ &gMrpAppAction17, 
        /* MRP_QA */ &gMrpAppAction18, 
        /* MRP_LA */ &gMrpAppAction19, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction20 
    },
    { /* MRP_APP_TX_LA */
        /* MRP_VO */ &gMrpAppAction21, 
        /* MRP_VP */ &gMrpAppAction22, 
        /* MRP_VN */ &gMrpAppAction15, 
        /* MRP_AN */ &gMrpAppAction16, 
        /* MRP_AA */ &gMrpAppAction17, 
        /* MRP_QA */ &gMrpAppAction23, 
        /* MRP_LA */ &gMrpAppAction21, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction13 
    },
    { /* MRP_APP_TX_LAF */
        /* MRP_VO */ &gMrpAppAction12, 
        /* MRP_VP */ &gMrpAppAction3, 
        /* MRP_VN */ &gMrpAppAction2, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction12, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    }
};

tMrpAppSemEntry        *gaMrpAppOnlySem [MRP_MAX_APP_EVENTS][MRP_MAX_APP_STATES] = 
{
    { /* MRP_APP_REQ_NEW */
        /* MRP_VO */ &gMrpAppAction2, 
        /* MRP_VP */ &gMrpAppAction2, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction2, 
        /* MRP_QA */ &gMrpAppAction2, 
        /* MRP_LA */ &gMrpAppAction2, 
        /* MRP_AO */ &gMrpAppAction2, 
        /* MRP_QO */ &gMrpAppAction2, 
        /* MRP_AP */ &gMrpAppAction2, 
        /* MRP_QP */ &gMrpAppAction2,
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_REQ_JOIN */
        /* MRP_VO */ &gMrpAppAction3, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction4, 
        /* MRP_AO */ &gMrpAppAction5, 
        /* MRP_QO */ &gMrpAppAction6, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_REQ_LEAVE */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction7, 
        /* MRP_VN */ &gMrpAppAction8, 
        /* MRP_AN */ &gMrpAppAction8, 
        /* MRP_AA */ &gMrpAppAction8, 
        /* MRP_QA */ &gMrpAppAction8, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction9, 
        /* MRP_QP */ &gMrpAppAction10, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_NEW */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_JOIN_IN */
        /* MRP_VO */ &gMrpAppAction9, 
        /* MRP_VP */ &gMrpAppAction5, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction11, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction10, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction6, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_IN */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_JOIN_MT */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction9, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction5, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_MT */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction9, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction5, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_LEAVE */
        /* MRP_VO */ &gMrpAppAction7, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction7, 
        /* MRP_QO */ &gMrpAppAction7, 
        /* MRP_AP */ &gMrpAppAction3, 
        /* MRP_QP */ &gMrpAppAction3, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_LEAVE_ALL */
        /* MRP_VO */ &gMrpAppAction7, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction7, 
        /* MRP_QO */ &gMrpAppAction7, 
        /* MRP_AP */ &gMrpAppAction3, 
        /* MRP_QP */ &gMrpAppAction3, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_REDECLARE */
        /* MRP_VO */ &gMrpAppAction7, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction7, 
        /* MRP_QO */ &gMrpAppAction7, 
        /* MRP_AP */ &gMrpAppAction3, 
        /* MRP_QP */ &gMrpAppAction3, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_PERIODIC */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction5, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_TX */
        /* MRP_VO */ &gMrpAppAction13, 
        /* MRP_VP */ &gMrpAppAction14, 
        /* MRP_VN */ &gMrpAppAction15, 
        /* MRP_AN */ &gMrpAppAction16, 
        /* MRP_AA */ &gMrpAppAction17, 
        /* MRP_QA */ &gMrpAppAction18, 
        /* MRP_LA */ &gMrpAppAction19, 
        /* MRP_AO */ &gMrpAppAction13, 
        /* MRP_QO */ &gMrpAppAction13, 
        /* MRP_AP */ &gMrpAppAction17, 
        /* MRP_QP */ &gMrpAppAction13, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_TX_LA */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_TX_LAF */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    }
};

tMrpAppSemEntry        *gaMrpAppOnlyP2PSem [MRP_MAX_APP_EVENTS][MRP_MAX_APP_STATES] = 
{
    { /* MRP_APP_REQ_NEW */
        /* MRP_VO */ &gMrpAppAction2, 
        /* MRP_VP */ &gMrpAppAction2, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction2, 
        /* MRP_QA */ &gMrpAppAction2, 
        /* MRP_LA */ &gMrpAppAction2, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_REQ_JOIN */
        /* MRP_VO */ &gMrpAppAction3, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction4, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_REQ_LEAVE */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction7, 
        /* MRP_VN */ &gMrpAppAction8, 
        /* MRP_AN */ &gMrpAppAction8, 
        /* MRP_AA */ &gMrpAppAction8, 
        /* MRP_QA */ &gMrpAppAction8, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_NEW */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_JOIN_IN */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction11, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_IN */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction11, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_JOIN_MT */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_MT */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_LEAVE */
        /* MRP_VO */ &gMrpAppAction7, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_RX_LEAVE_ALL */
        /* MRP_VO */ &gMrpAppAction7, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_REDECLARE */
        /* MRP_VO */ &gMrpAppAction7, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction2, 
        /* MRP_AA */ &gMrpAppAction3, 
        /* MRP_QA */ &gMrpAppAction3, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_PERIODIC */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction4, 
        /* MRP_LA */ &gMrpAppAction1,
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_TX */
        /* MRP_VO */ &gMrpAppAction13, 
        /* MRP_VP */ &gMrpAppAction14, 
        /* MRP_VN */ &gMrpAppAction15, 
        /* MRP_AN */ &gMrpAppAction16, 
        /* MRP_AA */ &gMrpAppAction17, 
        /* MRP_QA */ &gMrpAppAction18, 
        /* MRP_LA */ &gMrpAppAction19, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_TX_LA */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    },
    { /* MRP_APP_TX_LAF */
        /* MRP_VO */ &gMrpAppAction1, 
        /* MRP_VP */ &gMrpAppAction1, 
        /* MRP_VN */ &gMrpAppAction1, 
        /* MRP_AN */ &gMrpAppAction1, 
        /* MRP_AA */ &gMrpAppAction1, 
        /* MRP_QA */ &gMrpAppAction1, 
        /* MRP_LA */ &gMrpAppAction1, 
        /* MRP_AO */ &gMrpAppAction1, 
        /* MRP_QO */ &gMrpAppAction1, 
        /* MRP_AP */ &gMrpAppAction1, 
        /* MRP_QP */ &gMrpAppAction1, 
        /* MRP_LO */ &gMrpAppAction1 
    }
};


#endif /*_MRPGLOB_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpglob.h                       */
/*-----------------------------------------------------------------------*/
