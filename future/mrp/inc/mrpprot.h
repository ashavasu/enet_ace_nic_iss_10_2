/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpprot.h,v 1.18 2012/04/16 14:12:12 siva Exp $
 *
 * Description:  This file contains prototypes for functions defined in MRP.
 *****************************************************************************/
#ifndef _MRP_PROT_H
#define _MRP_PROT_H

/*--------------------------------------------------------------------------*/
/*                       mrpmain.c                                          */
/*--------------------------------------------------------------------------*/

INT4 MrpMainTaskInit           PROTO ((VOID));
VOID MrpMainTaskDeInit         PROTO ((VOID));
INT4 MrpMainModuleStart        PROTO ((VOID));
INT4 MrpMainMemPoolInit        PROTO ((VOID));
VOID MrpMainMemPoolDeInit      PROTO ((VOID));
INT4 MrpMainCreateMemPool PROTO ((VOID));
INT4 MrpMainDeleteMemPool PROTO ((VOID));
INT4 MrpMainInitDefaultContext PROTO ((VOID));
INT4 MrpMainInit               PROTO ((tMrpContextInfo *pMrpContextInfo));
INT4 MrpMainDeInit             PROTO ((tMrpContextInfo *pMrpContextInfo));
VOID MrpMainHandleModuleStartFailure PROTO ((VOID));
VOID MrpAssignMempoolIds       PROTO ((VOID));

/*--------------------------------------------------------------------------*/
/*                       mrptmr.c                                           */
/*--------------------------------------------------------------------------*/

INT4 MrpTmrInit             PROTO ((VOID));
INT4 MrpTmrDeInit           PROTO ((VOID));
VOID MrpTmrExpHandler       PROTO ((VOID));
INT4 MrpTmrStop             PROTO ((tMrpTimer *));
INT4 MrpTmrStart            PROTO ((UINT1, tMrpTimer *, UINT4));
INT4 MrpTmrStartJoinTmr PROTO ((tMrpAppPortEntry *));
INT4 MrpTmrStartLeaveAllTmr PROTO ((tMrpAppPortEntry *));
INT4 MrpTmrStartLeaveTmr PROTO ((tMrpMapPortEntry *, UINT4));
INT4 MrpTmrStartPeriodicTmr PROTO ((tMrpPortEntry *));
INT4 MrpTmrStopLeaveTmr PROTO ((tMrpMapPortEntry *, UINT4));
INT4 MrpTmrRestartLvAllTmr PROTO ((tMrpAppPortEntry *));

/*--------------------------------------------------------------------------*/
/*                       mrpif.c                                            */
/*--------------------------------------------------------------------------*/

VOID MrpIfCreatePorts            PROTO ((tMrpContextInfo *));
VOID MrpIfDeletePorts            PROTO ((tMrpContextInfo *));
INT4 MrpIfHandleCreatePort       PROTO ((tMrpContextInfo *, UINT4, UINT2));
INT4 MrpIfHandleDeletePort       PROTO ((tMrpContextInfo *, UINT2));
INT4 MrpIfHandlePortOperInd      PROTO ((tMrpContextInfo *, UINT2, UINT1));
INT4 MrpIfInitAndAddPortEntry    PROTO ((tMrpContextInfo *, tMrpPortEntry *, 
                                         UINT2, UINT4));
VOID MrpIfDeInitAndDelPortEntry  PROTO ((tMrpContextInfo *pContextInfo, 
                                         tMrpPortEntry * pPortEntry));
INT4 MrpIfHandlePortRoleChgEvent PROTO ((tMrpContextInfo *pContextInfo,
                                         UINT2 u2Port, UINT2 u2MapId,
                                         UINT1 u1PortRoleEvent));
INT4 MrpIfHandlePortOperP2PStatusChg PROTO ((tMrpContextInfo *pContextInfo,
                                             UINT2 u2Port,
                                             UINT1 u1OperP2PStatus));
VOID MrpIfHandleStapPortChange PROTO ((tMrpContextInfo *pContextInfo, 
                                       UINT2 u2Port, UINT1 u1State, 
                                       UINT2 u2MapId));
INT4 MrpIfCrtGblPortRbTree   PROTO ((VOID));
VOID MrpIfDelGblPortRbTree   PROTO ((VOID));
INT4 MrpIfMrpPortTblCmpFn    PROTO ((tRBElem *, tRBElem *));
INT4 MrpIfFreeGlobPortEntry  PROTO ((tRBElem *, UINT4));
INT4 MrpIfAddAttrToPortMvrpTable PROTO ((tMrpPortEntry *pMrpPortEntry, 
                                         tMrpAttrEntry *pMrpAttrEntry));
INT4
MrpIfDelAttrFromPortMvrpTable PROTO ((tMrpPortEntry *pMrpPortEntry, 
                                      tMrpAttrEntry *pMrpAttrEntry));
INT4
MrpIfIncAttrListSizeInPortEntry PROTO ((tMrpPortEntry *pMrpPortEntry, 
                                        UINT2 u2AttrIndex));
INT4 MrpIfInitMrpPortEntry   PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                     tMrpPortEntry *pPortEntry, 
                                     UINT2 u2LocalPort, UINT4 u4IfIndex));
VOID MrpIfDeInitMrpPortEntry PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                     tMrpPortEntry *pPortEntry));
INT4
MrpIfDecAttrListSizeInPortEntry PROTO ((tMrpPortEntry *pMrpPortEntry));

INT4
MrpIfGetContextInfoFromIfIndex PROTO ((UINT4 u4Port, UINT4 *pu4ContextId,
                                       UINT2 *pu2LocalPort));

tMrpPortEntry *
MrpIfGetValidPort PROTO ((tMrpContextInfo *pContextInfo, UINT2 u2Port));

/*--------------------------------------------------------------------------*/
/*                       mrpmvrp.c                                          */
/*--------------------------------------------------------------------------*/

INT4 MrpMvrpInit               PROTO ((tMrpContextInfo *pMrpContextInfo));
INT4 MrpMvrpEnable             PROTO ((tMrpContextInfo *pMrpContextInfo));
INT4 MrpMvrpDisable            PROTO ((tMrpContextInfo *pMrpContextInfo));
INT4 MrpMvrpAddPort            PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       UINT2 u2Port));
INT4 MrpMvrpDelPort            PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       UINT2 u2Port));
INT4 MrpMvrpEnablePort         PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       UINT2 u2Port));
INT4 MrpMvrpDisablePort        PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       UINT2 u2Port));
VOID MrpMvrpSetPortMvrpStatus  PROTO ((tMrpPortEntry *pMrpPortEntry));
INT4 MrpMvrpAttrIndFn          PROTO ((tMrpMapEntry *pMapEntry, tMrpAttr Attr, 
                                       UINT2 u2Port, UINT1 u1IndType));
INT4 MrpMvrpTypeValidateFn     PROTO ((UINT1 u1AttrType));
INT4 MrpMvrpIsEnabled          PROTO ((UINT4 u4ContextId));
INT4
MrpMvrpWildCardAttrRegValidateFn PROTO ((UINT4 u4ContextId, tMrpAttr Attr, 
                                         UINT2 u2Port, UINT2 u2MapId));
INT4
MrpMvrpLenValidateFn PROTO ((UINT1 u1AttrType, UINT1 u1Attrlen));

VOID MrpMvrpHandleJoinTmrExpiry PROTO ((UINT4 u4ContextId,
                                        tMrpAppPortEntry *pMrpAppPortEntry));

tMrpAttrEntry * MrpMvrpGetFirstAttrForTx PROTO ((tMrpPortEntry *pPortEntry));
tMrpAttrEntry * 
MrpMvrpGetNextAttrForTx PROTO ((tMrpPortEntry *pPortEntry, 
                                UINT2 u2AttrIndex, UINT1 u1Flag));
VOID
MrpMvrpApplyTxLAFOnRemainingAttr PROTO ((tMrpPortEntry *pPortEntry, 
                                                tMrpAttrEntry *pAttrEntry));

VOID
MrpMvrpRemapUpdateSemAndTx PROTO ((UINT4 u4ContextId, tMrpAppEntry *pAppEntry, 
                                   tMrpPortEntry * pPortEntry,
                                   tMrpAttrEntry * pAttrEntry,
                                   tMrpMapPortEntry *pMapPortEntry, 
                                   UINT1 u1DynVlan, UINT1 u1AttrFlag));

VOID
MrpMvrpFormAttrList PROTO ((UINT1 **ppu1AttrList, tMrpPortEntry *pPortEntry,
                            UINT1 u1Event, UINT2 *pu2Offset,
                            UINT1 *pu1BuffFull));
VOID
MrpMvrpFormVectAttr PROTO ((UINT1 **ppVectBuf, tMrpVectAttrInfo *pVectAttrInfo, 
                            UINT1 *pu1AddVectHdr, UINT2 *pu2NumOfValues));


/*--------------------------------------------------------------------------*/
/*                       mrpmmrp.c                                          */
/*--------------------------------------------------------------------------*/

INT4 MrpMmrpInit               PROTO ((tMrpContextInfo *));
INT4 MrpMmrpEnable             PROTO ((tMrpContextInfo *));
INT4 MrpMmrpDisable            PROTO ((tMrpContextInfo *pMrpContextInfo));
INT4 MrpMmrpAddPort            PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       UINT2 u2Port));
INT4 MrpMmrpDelPort            PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       UINT2 u2Port));
INT4 MrpMmrpEnablePort         PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       UINT2 u2Port));
INT4 MrpMmrpDisablePort        PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       UINT2 u2Port));
VOID MrpMmrpSetPortMmrpStatus  PROTO ((tMrpPortEntry *));
INT4 MrpMmrpAttrIndFn          PROTO ((tMrpMapEntry *pMapEntry, tMrpAttr Attr, 
                                       UINT2 u2Port, UINT1 u1IndType));
INT4 MrpMmrpTypeValidateFn     PROTO ((UINT1 u1AttrType));
INT4 MrpMmrpWildCardAttrRegValidateFn PROTO ((UINT4 u4ContextId, 
                                              tMrpAttr Attr, UINT2 u2Port, 
                                              UINT2 u2MapId));
INT4
MrpMmrpLenValidateFn PROTO ((UINT1 u1AttrType, UINT1 u1Attrlen));

INT4 MrpMmrpIsEnabled          PROTO ((UINT4 u4ContextId));
VOID MrpMmrpHandleJoinTmrExpiry PROTO ((UINT4 u4ContextId,
                                        tMrpAppPortEntry *pMrpAppPortEntry));
VOID
MrpMmrpFormAndTransmitPdu PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                  tMrpAppPortEntry *pMrpAppPortEntry, 
                                  tMrpPortEntry *pPortEntry, 
                                  tMrpMapPortEntry *pMapPortEntry));
INT4 MrpMmrpLeaveInd           PROTO ((tMrpAttr *pAttr, 
                                       tMrpContextInfo *pContextInfo, 
                                       UINT2 u2Port, UINT2 u2MapId));
INT4 MrpMmrpJoinOrNewInd       PROTO ((tMrpAttr *pAttr, 
                                       tMrpContextInfo *pContextInfo,
                                       UINT2 u2Port, UINT2 u2MapId));
tMrpAttrEntry *
MrpMmrpGetFirstAttrForTx PROTO ((tMrpMapPortEntry *pMapPortEntry));
tMrpAttrEntry *
MrpMmrpGetNextAttrForTx PROTO ((tMrpMapPortEntry *pMapPortEntry, 
                                tMrpAttrEntry *pAttrEntry, UINT1 *pu1Flag,
    UINT1 u1IsFirstAttrNeeded));
VOID
MrpMmrpApplyTxLAFOnRemainingAttr PROTO ((tMrpMapPortEntry *pMapPortEntry, 
                                         tMrpAttrEntry *pAttrEntry));
INT4
MrpMmrpDiffAttrValue PROTO ((UINT1 *pu1CurrVal, UINT1 *pu1NextVal, 
                             UINT1 u1AttrLen));
VOID
MrpMmrpApplyTcDetectedTmrStatus PROTO ((tMrpAppEntry *pAppEntry, UINT2 u2Port, 
                                        UINT1 *pu1VlanList,
                                        UINT2 u2VlanCntInList, 
                                        UINT1 u1TmrState));
VOID
MrpMmrpFormVectAttr PROTO ((UINT1 **ppBuf, tMrpVectAttrInfo * pVectAttrInfo,
                            UINT1 *pu1AddVectHdr, UINT2 *pu2NumOfValues, 
                            UINT2 *pu2Offset));
VOID
MrpMmrpHandleRemapEvent PROTO ((tMrpAppEntry * pAppEntry, UINT2 u2MapId,
                                UINT1 u1RemapFlag, UINT1 u1DynVlan));
INT4
MrpMmrpProcessPvlanJoinOrLeave (UINT4 u4ContextId, tMacAddr MacAddr,
                                tVlanId GipId, UINT2 u2Port, UINT1 u1Action);


/*--------------------------------------------------------------------------*/
/*                       mrputil.c                                          */
/*--------------------------------------------------------------------------*/
INT4 MrpUtilHandleBridgeMode   PROTO ((tMrpContextInfo *));
INT4 MrpUtilIsMrpStarted       PROTO ((UINT4));

VOID MrpUtilSyncAttributeTable PROTO ((tMrpAppEntry *pMrpAppEntry, 
                                       tMrpAppPortEntry *pMrpAppPortEntry));

VOID
MrpUtilPortStateChgToForwarding PROTO ((tMrpContextInfo *pContextInfo, 
                                        tMrpAppEntry *pAppEntry, 
                                        tMrpMapEntry *pMapEntry, 
                                        tMrpMapPortEntry *pMapPortEntry, 
                                        UINT1 u1AppStatus));
VOID 
MrpUtilPortStateChgedToBlocking PROTO ((tMrpContextInfo *pContextInfo, 
                                        tMrpAppEntry *pAppEntry, 
                                        tMrpMapEntry *pMapEntry, 
                                        tMrpMapPortEntry *pMapPortEntry));
VOID
MrpUtilHandleSharedtoP2PMediaChg PROTO ((tMrpContextInfo * pContextInfo,
                                         tMrpPortEntry *pPortEntry));

PUBLIC VOID MrpUtilHandlePortRoleChg  PROTO ((tMrpAppEntry *pAppEntry, 
                                              UINT2 u2MapId, UINT2 u2Port,
                                              UINT1 u1PortRoleEvent));


PUBLIC INT4 MrpUtilCheckIfMACIsValid PROTO ((tMacAddr MacAddr));
PUBLIC VOID MrpUtilGetNextMAC PROTO ((tMacAddr MacAddr));
PUBLIC INT4 MrpUtilIsPortOkForMRPDU PROTO ((tMrpPortEntry *pMrpPortEntry, 
                                            tMacAddr AppAddress));
INT4 MrpUtilTranslateVID PROTO ((UINT4 u4ContextId, UINT2 u2Port, 
                                 tMrpAttr * pAttr, tVlanId *pVid));
VOID
MrpUtilTransLocalVidFromRelayVid PROTO ((UINT4 u4ContextId, UINT2 u2Port, 
                                         tVlanId *pVid));
INT4 MrpUtilSetTcDetectedTmrStatus PROTO ((tMrpAppEntry *pAppEntry,
                                           UINT2 u2MapId,   
                                           UINT2 u2Port,
                                           UINT1 u1TmrState));

INT4
MrpUtilGetNextActiveContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId);
UINT1
MrpUtilGetVlanPortState PROTO ((tMrpMapPortEntry * pMrpMapPortEntry)); 

UINT1
MrpUtilGetInstPortState PROTO ((UINT2 u2MapId, 
                                tMrpPortEntry * pMrpPortEntry));
VOID
MrpUtilHandlePeriodicEvntOnPort PROTO ((tMrpPortEntry *));

VOID MrpUtilIncrTxStats PROTO ((tMrpPortEntry *pMrpPortEntry, UINT1 u1AppId, 
                                UINT1 u1AttrEvent));
UINT2
MrpUtilFindFirstSetBitInBitList PROTO ((UINT1 *pu1Data, UINT2 u2BitListSize));
UINT2
MrpUtilFindNextSetBitInBitList PROTO ((UINT1 *pu1Data, UINT2 u2BitListSize, 
                                       UINT2 u2CurrentBit));
INT4
MrpUtilMmrpPropOrSetDefGrpInfo PROTO ((UINT4 u4ContextId, UINT2 u2MsgType, 
                                       tVlanId VlanId,
                                       tLocalPortList AddPortList, 
                                       tLocalPortList DelPortList));
INT4
MrpUtilIsMrpStartedInSystem PROTO ((VOID));

INT4 
MrpUtilPostRcvdPduToTask PROTO ((tCRU_BUF_CHAIN_DESC * pFrame, 
     UINT4 u4ContextId, UINT2 u2LocalPortId,
     UINT2 u2MapId));
INT4
MrpUtilDelAllLearntVlans PROTO ((tMrpPortEntry *pPortEntry));

INT4 MrpUtilGetMmrpEnabledStatus PROTO ((tMmrpStatusInfo *pMmrpStatusInfo));
INT4 MrpUtilSetMmrpEnabledStatus PROTO ((tMmrpStatusInfo *pMmrpStatusInfo ));
INT4 MrpUtilTestMmrpEnabledStatus PROTO ((tMmrpStatusInfo *pMmrpStatusInfo));
INT4 MrpUtilGetMvrpEnabledStatus PROTO ((tMvrpStatusInfo *pMvpStatusInfo));
INT4 MrpUtilSetMvrpEnabledStatus PROTO ((tMvrpStatusInfo *pMvpStatusInfo));
INT4 MrpUtilTestMvrpEnabledStatus PROTO ((tMvrpStatusInfo *pMvpStatusInfo));
INT4 MrpUtilValidateMrpTableIndices PROTO ((tMvrpPortInfo *pMvrpPortInfo));
INT4 MrpUtilGetNextIndexMrpTable PROTO ((tMvrpPortInfo *pMvrpPortInfo));
INT4 MrpUtilGetPortMvrpEnabledStatus PROTO ((tMvrpPortInfo *pMvrpPortInfo));
INT4 MrpUtilGetPortMvrpFailedRegist PROTO ((tMvrpPortInfo *pMvrpPortInfo));
INT4 MrpUtilGetPortMvrpLastPduOrigin PROTO ((tMvrpPortInfo *pMvrpPortInfo));
INT4 MrpUtilSetPortMvrpEnabledStatus PROTO ((tMvrpPortInfo *pMvrpPortInfo));
INT4 MrpUtilTestPortMvrpEnabledStatus PROTO ((tMvrpPortInfo *pMvrpPortInfo));



/*--------------------------------------------------------------------------*/
/*                       mrpport.c                                          */
/*--------------------------------------------------------------------------*/

INT4
MrpPortCfaCliGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));
INT4 MrpPortL2IwfGetBridgeMode PROTO ((UINT4, UINT4 *));
INT4 MrpPortL2IwfGetPbPortType PROTO ((UINT4 u4IfIndex, UINT1 *pu1PortType));
INT4 MrpPortGetNxtValidPortForContext PROTO ((UINT4, UINT2, UINT2 *, UINT4 *));
INT4 MrpPortGetVlanLocalEgressPorts PROTO ((UINT4 u4ContextId, 
                                            tVlanId VlanId, 
                                            tLocalPortList EgressPorts));
INT4 MrpPortL2IwfIsPortInPortChannel PROTO ((UINT4));
INT4 MrpPortL2IwfGetPortOperStatus PROTO ((INT4, UINT4, UINT1 *));
INT4 MrpPortL2IwfGetVlanPortType PROTO ((UINT4, UINT1 *));
INT4 MrpPortGetPortVlanTunnelStatus PROTO ((UINT4, BOOL1 *));
VOID MrpPortGetProtoTunelStatusOnPort PROTO ((UINT4, UINT2, UINT1 *));
INT4 MrpPortVcmGetSystemMode PROTO ((UINT2));
INT4 MrpPortVcmGetSystemModeExt PROTO ((UINT2));
INT4 MrpPortVcmGetAliasName PROTO ((UINT4, UINT1 *));
INT4 MrpPortHandleOutgoingPktOnPort PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
         UINT4 u4ContextId, UINT4 u4IfIndex, 
         UINT4 u4PktSize, UINT2 u2Protocol,
         UINT1 u1Encap));
INT4 MrpPortGetContextInfoFromIfIndex PROTO ((UINT4, UINT4 *, UINT2 *));
INT4 MrpPortGetCfaIfInfo PROTO ((UINT4, tCfaIfInfo *));
VOID MrpPortVlanIndMvrpEnable PROTO ((UINT4 u4ContextId));
VOID MrpPortVlanIndMvrpDisable PROTO ((UINT4 u4ContextId));
VOID MrpPortVlanIndMmrpEnable PROTO ((UINT4 u4ContextId));
VOID MrpPortVlanIndPortMvrpEnable PROTO ((UINT4 u4ContextId, UINT2 u2Port));
VOID MrpPortVlanIndPortMmrpEnable PROTO ((UINT4 u4ContextId, UINT2 u2Port));
VOID MrpPortVlanIndMmrpDisable PROTO ((UINT4 u4ContextId));
VOID MrpPortVlanDelAllDynamicVlanInfo PROTO ((UINT4 u4ContextId, 
                                              UINT2 u2Port));
VOID MrpPortVlanDelAllDynamicMacInfo  PROTO ((UINT4 u4ContextId, 
                                              UINT2 u2Port));
INT4 MrpPortSetProtoEnabledStatOnPort PROTO ((UINT4 u4ContextId, 
                                              UINT2 u2LocalPortId,
                                              UINT2 u2Protocol, 
                                              UINT1 u1Status));
INT4 MrpPortVlanGetStartedStatus PROTO ((UINT4 u4ContextId));
UINT2 MrpPortL2IwfGetVlanListInInst PROTO ((UINT4 u4ContextId, UINT2 u2MstInst,
                                            UINT1 *pu1VlanList));
PUBLIC INT4 MrpPortNotifyFaults     PROTO ((tSNMP_VAR_BIND *pTrapMsg,
                                                 UINT1 *pu1Msg,
                                                 UINT4 u4ModuleId));
INT4
MrpPortIsIgmpSnoopingEnabled PROTO ((UINT4 u4ContextId));

INT4
MrpPortSnoopIsMldSnoopingEnabled  PROTO ((UINT4 u4ContextId));

INT4
MrpPortVcmIsSwitchExist PROTO ((UINT1 *pu1Alias, UINT4 *pu4VcNum));

INT4 MrpPortGarpGetStartedStatus PROTO ((UINT4 u4ContextId));
INT4
MrpPortTagOutgoingMmrpFrame PROTO ((UINT4 u4ContextId, 
                                    tCRU_BUF_CHAIN_DESC *pBuf, 
                                    tVlanId VlanId, UINT2 u2Port));
UINT4 
MrpPortL2IwfGetVlanFdbId PROTO ((UINT4 u4ContextId, tVlanId VlanId));
INT4
MrpPortVlanFlushFdbEntries PROTO ((UINT4 u4IfIndex, UINT4 u4FdbId));
INT4 MrpPortVlanUpdateDynDefGrpInfo PROTO ((UINT4 u4ContextId, UINT1 u1Type,
                                            tVlanId VlanId, UINT2 u2Port, 
                                            UINT1 u1Action));
INT4 MrpPortPbGetRelayVidFrmLocalVid PROTO ((UINT4 u4ContextId, 
                                             UINT2 u2LocalPort,
                                             tVlanId LocalVid,
                                             tVlanId * pRelayVid));
INT4 
MrpPortPbGetLocalVidFrmRelayVid PROTO ((UINT4 u4ContextId, UINT2 u2LocalPort,
                                        tVlanId RelayVid, tVlanId * pLocalVid));

UINT2
MrpPortL2IwfMiGetVlanInstMapping PROTO ((UINT4 u4ContextId, 
                                         tVlanId VlanId));
INT4
MrpPortVlanIsVlanDynamic PROTO ((UINT4 u4ContextId, UINT2 VlanId));
INT4
MrpPortVlanUpdateDynamicMACInfo PROTO ((UINT4 u4ContextId, 
                                          tMacAddr MacAddr,
                                          tVlanId VlanId, 
                                          UINT4 u4IfIndex, 
                                          UINT1 u1Action));
INT4
MrpPortVlanUpdateDynamicVlanInfo PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                         UINT2 u2Port, UINT1 u1Action));
VOID
MrpPortDelDynamicMacInfoForVlan PROTO ((UINT4 u4ContextId, tVlanId VlanId));
UINT1
MrpPortL2IwfGetVlanPortState PROTO ((tVlanId VlanId, UINT4 u4IfIndex));
UINT1
MrpPortL2IwfGetInstPortState PROTO ((UINT2 u2MstInst, UINT4 u4IfIndex));
VOID
MrpPortDelDynDefGroupInfoForVlan PROTO ((UINT4 u4ContextId, tVlanId VlanId));
VOID
MrpPortDelAllDynamicDefGroupInfo PROTO ((UINT4 u4ContextId, UINT2 u2Port));
INT4
MrpPortIsPvrstStartedInContext   PROTO ((UINT4 u4ContextId));

INT4
MrpPortPbbTeVidIsEspVid PROTO ((UINT4 u4ContextId, tVlanId VlanId));

INT4
MrpPortIsMACWildCardEntryPresent PROTO ((UINT4 u4ContextId, UINT2 u2VlanId,
                                         tMacAddr MacAddr));
INT4
MrpPortL2IwfGetPortOperP2PStatus PROTO ((UINT4 u4IfIndex,
                                         BOOL1 *pbOperPointToPoint));

INT4
MrpPortCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName);

UINT1 MrpPortIsJoinTmrReducedFrBackPln (VOID);
INT4
MrpPortRelRmMsgMemory PROTO ((UINT1 *pu1Block));
UINT1
MrpPortGetRmNodeState PROTO ((VOID));
VOID
MrpPortSetBulkUpdateStatus PROTO ((VOID));
INT4
MrpPortEnqMsgToRm PROTO ((tRmMsg * pRmMsg, UINT2 u2DataLen));
UINT1
MrpPortGetStandbyNodeCount PROTO ((VOID));
INT4
MrpPortSendEventToRm PROTO ((tRmProtoEvt * pEvt));
INT4
MrpPortRmRegisterProtocols PROTO ((tRmRegParams *pRmRegParams));
INT4
MrpPortDeRegisterWithRM PROTO ((VOID));
INT4
MrpPortRmApiSendProtoAckToRM PROTO ((tRmProtoAck * pProtoAck));
INT4
MrpPortVlanGetBaseBridgeMode PROTO ((VOID));
VOID
MrpPortL2IwfGetPVlanMappingInfo (tL2PvlanMappingInfo *pL2PvlanMappingInfo);

/*--------------------------------------------------------------------------*/
/*                       mrpque.c                                           */
/*--------------------------------------------------------------------------*/
VOID MrpQueRxPduHandler PROTO ((VOID));
VOID
MrpQueHandleIncomingFrame PROTO ((tCRU_BUF_CHAIN_HEADER * pMrpPdu, 
                                  tMrpContextInfo *pContextInfo,
                                  UINT2 u2Port, UINT2 u2MapId));

VOID MrpQueMsgHandler PROTO ((VOID));
VOID MrpQueProcessMsg PROTO ((tMrpQMsg *pMrpQMsg));
INT4 MrpQuePostCfgMessage PROTO ((UINT1, UINT2, UINT4, UINT4));
INT4 MrpQueEnqMsg PROTO ((tMrpQMsg *pQMsg));
VOID
MrpQueFlushContextsQueue PROTO ((VOID));
VOID
MrpQueProcessEvents PROTO ((UINT4 u4Events));

/*--------------------------------------------------------------------------*/
/*                       mrpsm.c                                            */
/*--------------------------------------------------------------------------*/

INT4 MrpSmRegistrarSem PROTO ((tMrpMapPortEntry *pMapPortEntry, 
                               UINT2 u2AttrIndex, UINT1 u1SemEvent, 
                               UINT1 *pu1StateChg));
eMrpSendMsgInd MrpSmApplicantSem PROTO ((tMrpMapPortEntry *pMapPortEntry, 
                                         UINT4 u4AttrIndex, UINT1 u1SemEvent));

/*--------------------------------------------------------------------------*/
/*                       mrpmap.c                                           */
/*--------------------------------------------------------------------------*/

INT4
MrpMapDecAttrArraySizeInMapEntry PROTO ((tMrpMapEntry *pMrpMapEntry));
INT4
MrpMapDecPortArraySizeInMapEntry PROTO ((tMrpMapEntry *pMrpMapEntry));
INT4
MrpMapDecArraySizeInMapPortEntry PROTO ((tMrpMapPortEntry *
                                         pMrpMapPortEntry));

VOID MrpMapGetPortList PROTO ((UINT4 u4ContextId, UINT1 u1AppId, UINT2 u2MapId, 
                               tLocalPortList MapPortList));
VOID
MrpMapAppAttributeReqJoin PROTO ((tMrpAppEntry *pAppEntry, tMrpAttr * pAttr,
                                  UINT2 u2MapId, tLocalPortList AddPorts));
VOID
MrpMapAppAttributeReqJoinOnPort PROTO ((tMrpAppEntry *pAppEntry, 
                                        tMrpAttr * pAttr, 
                                        UINT2 u2MapId, UINT2 u2Port));
VOID
MrpMapAppAttributeReqLeave PROTO ((tMrpAppEntry *pAppEntry, tMrpAttr * pAttr,
                                   UINT2 u2MapId, tLocalPortList DelPorts));
VOID
MrpMapPropagateOnAllPorts PROTO ((tMrpAttr * pAttr,
                                  tMrpIfMsg * pIfMsg, tMrpMapEntry *pMapEntry, 
                                  eMrpSendMsgInd SendMsgInd));
VOID
MrpMapAppSetForbiddenPorts PROTO ((tMrpAppEntry *pAppEntry,
                                   tMrpAttr * pAttr,
                                   UINT2 u2MapId,
                                   tLocalPortList AddPorts, 
                                   tLocalPortList DelPorts));
VOID
MrpMapAppSetForbiddenPort PROTO ((tMrpAppEntry *pAppEntry, tMrpAttr * pAttr,
                                  UINT2 u2MapId, UINT2 u2Port));
VOID
MrpMapMadAttributeJoinInd PROTO ((tMrpAttr * pAttr, tMrpIfMsg * pIfMsg,
                                  eMrpHlMsgInd SendMSg));

VOID
MrpMapHandleMapUpdateMapPorts PROTO ((UINT4 u4ContextId, UINT2 u2Port, 
                                      UINT2 u2MapId, 
                                      UINT1 u1Action));
VOID
MrpMapMadAttributeLeaveInd PROTO ((tMrpAttr * pAttr, tMrpIfMsg * pIfMsg));
INT4
MrpMapIncAttrArraySizeInMapEntry PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                          UINT2 u2AttrIndex));
INT4
MrpMapIncPortArraySizeInMapEntry PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                         UINT2 u2Port));
INT4
MrpMapIncArraySizeInMapPortEntry PROTO ((tMrpMapPortEntry *pMrpPortEntry, 
                                         UINT2 u2AttrIndex));

VOID MrpMapSyncAttributeTableForMap PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                             tMrpMapEntry *pMrpMapEntry, 
                                             tMrpIfMsg *pIfMsg));
INT4
MrpMapCreateAppAttrReqJoin (tMrpAppEntry * pAppEntry, tMrpAttr * pAttr,
       tMrpAttrEntry ** ppAttrEntryArray, UINT2 u2MapId,
       UINT1 * pu1SendNewInd);

INT4
MrpMapAttrReqJoinOnPort (tMrpAppEntry * pAppEntry, tMrpMapEntry * pMapEntry,
    tMrpAttrEntry * pAttrEntry, UINT2 u2Port, 
    UINT1 * pu1PortState);

INT4
MrpMapAttrSetForbidenPort (tMrpAppEntry * pAppEntry, tMrpMapEntry * pMapEntry,
      tMrpAttrEntry * pAttrEntry, tMrpAttr * pAttr,
      UINT2 u2Port);

/*--------------------------------------------------------------------------*/
/*                       mrpmapds.c                                         */
/*--------------------------------------------------------------------------*/

tMrpMapEntry *
MrpMapDSCreateMrpMapEntry PROTO ((tMrpAppEntry *pMrpAppEntry, UINT2 u2MapId));
tMrpAttrEntry *
MrpMapDSCreateMrpMapAttrEntry PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                      tMrpAttr *pAttr));
VOID 
MrpMapDSDeleteMrpMapEntry PROTO ((tMrpMapEntry *pMrpMapEntry));
tMrpMapPortEntry *
MrpMapDSCreateMrpMapPortEntry PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                     UINT2 u2Port));
INT4 
MrpMapDSInitMrpMapEntry    PROTO ((tMrpAppEntry *pMrpAppEntry, 
                                   tMrpMapEntry *pMrpMapEntry, 
                                   UINT2 u2MapId));
VOID
MrpMapDSDeInitMrpMapEntry  PROTO ((tMrpMapEntry *pMrpMapEntry));
INT4
MrpMapDSInitMrpMapPortEntry PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                    tMrpMapPortEntry *pMrpMapPortEntry, 
                                    UINT2 u2Port));
VOID
MrpMapDSDeInitMrpMapPortEntry PROTO ((tMrpMapPortEntry *pMrpMapPortEntry));
VOID
MrpMapDSDeleteMrpMapPortEntry PROTO ((tMrpMapPortEntry *pMrpMapPortEntry));
INT4
MrpMapDSAddAttrEntryToMapEntry PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                      tMrpAttrEntry *pMrpAttrEntry));
INT4
MrpMapDSAddPortEntryToMapEntry PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                       tMrpMapPortEntry *pMrpMapPortEntry));
INT4
MrpMapDSAddAttrToMapPortEntry PROTO ((tMrpMapPortEntry *pMrpMapPortEntry, 
                                     tMrpAttrEntry *pMrpAttrEntry));
INT4
MrpMapDSDelAttrEntryFromMapEntry PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                        tMrpAttrEntry *pMrpAttrEntry));
tMrpAttrEntry *
MrpMapDSGetAttrEntryFrmMapEntry PROTO ((tMrpAttr * pAttr, 
                                        tMrpMapEntry *pMrpMapEntry));
VOID
MrpMapDSDelAllAttrInMapOnPort PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                     tMrpMapPortEntry *pMrpMapPortEntry));
INT4
MrpMapDSDelPortEntryFromMapEntry PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                       tMrpMapPortEntry *pMrpMapPortEntry));
INT4
MrpMapDSDelAttrFromMapPortEntry PROTO ((tMrpMapPortEntry *pMrpMapPortEntry, 
                                       tMrpAttrEntry *pMrpAttrEntry));
VOID
MrpMapDsCheckAndDeleteMapEntry PROTO ((tMrpMapEntry * pMrpMapEntry));

/*--------------------------------------------------------------------------*/
/*                       mrpmad.c                                           */
/*--------------------------------------------------------------------------*/

VOID
MrpMadMapAttributeJoinReq PROTO ((tMrpAttr * pAttr,
                           tMrpMapPortEntry * pPortEntry, tMrpIfMsg * pIfMsg,
                           tMrpMapEntry *pMapEntry, 
                           eMrpSendMsgInd SendMsgInd));
VOID
MrpMadMapAttributeLeaveReq PROTO ((tMrpAttr * pAttr,
                                   tMrpMapPortEntry * pPortEntry, 
                                   tMrpIfMsg * pIfMsg,
                                   tMrpMapEntry *pMapEntry));


/*--------------------------------------------------------------------------*/
/*                       mrprx.c                                            */
/*--------------------------------------------------------------------------*/
INT4 MrpRxValidatePDU PROTO ((UINT1 *pMrpPduBuff, tMrpIfMsg *pIfMsg));
VOID MrpRxGetVectorAttrLen PROTO ((UINT2 u2VectorHdr, UINT1 u1FirstValLen, 
                                   UINT2 *pu2VectAttrLen));
INT4 MrpRxApplyPDU PROTO ((UINT1 *pu1TmpBuff, tMrpIfMsg *pIfMsg));
INT4 MrpRxDecodeVectorAttribute PROTO ((UINT1 *pMrpPdu, UINT2 u2VectHdr, 
                                        tMrpIfMsg *pIfMsg, UINT1 u1AttrType,
                                        UINT1 u1AttrLen, UINT2 *pu2VectAttrLen,
                                        UINT1 *pu1IsLvAllEvntHandled));
VOID MrpRxSkipMessage PROTO ((UINT1 *pVectAttr, UINT1 u1FirstValLen, 
                             UINT2 *pu2PduLen, UINT2 *pu2MsgLen));
INT4 MrpRxIsAttributeValid PROTO ((tMrpAttr *pAttr, tMrpIfMsg *pIfMsg));
VOID MrpRxDecodeAttrEvents PROTO ((UINT1 *pVector, tMrpAttr *pAttr,
                                   UINT2 u2AttrNum, UINT2 u2NoOfVectors, 
                                   tMrpIfMsg *pIfMsg, UINT1 u1ValidStatus));
INT4 MrpRxApplyRcdEvent PROTO ((tMrpAttr *pAttr, UINT1 u1AttrEvent,
                                tMrpIfMsg *pIfMsg));
VOID MrpRxHandleLvAllEvent PROTO ((tMrpIfMsg *pIfMsg));
VOID MrpRxHandleJoinOrNewEvent PROTO ((tMrpIfMsg *pIfMsg, tMrpAttr *pAttr, 
                                       UINT1 u1AttrEventType));
VOID MrpRxHandleEvents PROTO ((tMrpAttr *pAttr, tMrpIfMsg *pIfMsg,
                               UINT1 u1AttrEvent));
INT4 MrpRxGetNextAttr PROTO ((tMrpAttr *pAttr, tMrpIfMsg *pIfMsg));
INT4 MrpRxIsMvrpAttrValueValid PROTO ((tMrpAttr *pAttr, tMrpIfMsg *pIfMsg));
INT4 MrpRxIsMmrpAttrValueValid PROTO ((tMrpAttr *pAttr));


/*--------------------------------------------------------------------------*/
/*                       mrptx.c                                            */
/*--------------------------------------------------------------------------*/
INT4
MrpTxHndLAEventForNonParticipant PROTO ((tMrpMapPortEntry *pMapPortEntry, 
         tMrpAttrEntry  *pAttrEntry,
        UINT1 u1AttrType,
     UINT1  *pu1IsNonParticipant));

VOID MrpTxApplyTxEventOnAttr PROTO ((tMrpMapPortEntry *pMapPortEntry, 
                                     tMrpAttr *pAttr, UINT2 u2AttrIndex, 
                                     UINT1 u1Event, UINT1 *pu1AttrEvent, 
                                     UINT1 *pu1OptEncode));
VOID MrpTxEncodeAttrEvent PROTO ((UINT1 *pu1Ptr, UINT2 u2NoOfValues, 
                                  UINT1 u1AttrEvent));
INT4 MrpTxIsSpaceInPkt PROTO ((UINT2 u2CurrOffset, UINT2 u2NumOfValue, 
                               UINT1 u1AttrLen));
INT4
MrpTxIsSpaceInMVRPPkt PROTO ((UINT2 u2CurrOffset, UINT2 u2NumOfValue, 
                              UINT1 u1AttrLen));

VOID
MrpTxAddVectorAttrToPdu PROTO ((UINT1 **ppu1Ptr, UINT2 u2VectorHdr,
                                tMrpAttr *pAttr, UINT1 *pu1Vector,
                                UINT2 u2NumOfValue, UINT2 *pu2Offset,
                                UINT1 u1Flag));

VOID MrpTxAddEndMarkAndTxPdu PROTO ((UINT4 u4ContextId, UINT1 u1AppId, 
                                     tMacAddr DestAddr, UINT2 u2BufLen, 
                                     UINT2 u2Port, UINT2 u2MapId));

VOID MrpTxAddPduHdrAndTxPdu PROTO ((UINT4 u4ContextId, UINT1 u1AppId,
                                    UINT2 u2BufLen, UINT2 u2Port,
                                    UINT2 u2MapId));
VOID
MrpTxFillMtEvntInVector PROTO ((UINT1 *pVectBuf, tMrpPortEntry *pPortEntry,
                                  UINT2 *pu2NumOfValue, UINT1 u1Count));
VOID
MrpTxFillOptEvntsInVector PROTO ((UINT1 *pVectBuf, UINT1 *pu1AttrEvent, 
                                    UINT1 u1Count, UINT2 *pu2NumOfValue, 
                                  tMrpPortEntry *pPortEntry, UINT1 u1AppId));
INT4
MrpTxIsSpaceInMMRPPkt PROTO ((UINT2 u2CurrOffset, UINT2 u2NumOfValue,
                              UINT1 u1AttrLen));
VOID
MrpTxAddMmrpMessageHdr PROTO ((UINT1 **ppu1MsgHdr, tMrpAttrEntry *pAttrEntry,
                             UINT2 *pu2Offset));
VOID
MrpTxAddVectHdrAndUpdNoOfValues PROTO ((UINT1 **ppu1Ptr, UINT1 **ppu1AttrList, 
                                        UINT2 u2VectorHdr, 
                                        UINT2 *pu2NumOfValues, UINT2 *pu2Offset,
                                        UINT1 u1AttrLen, 
                                        UINT1 u1FirstValFilled));


/*--------------------------------------------------------------------------*/
/*                     mrptrap.c                                            */
/*--------------------------------------------------------------------------*/

tSNMP_OID_TYPE     *
MrpTrapMakeObjIdFromDotNew PROTO ((INT1 *pi1TextStr));

INT4 
MrpTrapParseSubIdNew PROTO ((UINT1 **tempPtr, UINT4 *pu4Value));

INT4
MrpTrapNotifyVlanOrMacRegFails PROTO ((VOID *pTrapInfo));


/*--------------------------------------------------------------------------*/
/*                     mrptrc.c                                            */
/*--------------------------------------------------------------------------*/

CHR1 *MrpTrc PROTO ((tMrpContextInfo *pContextInfo, UINT4 u4Trc,  
                     const char *fmt, ...));
VOID MrpTrcPrint PROTO ((const char *fname, UINT4 u4Line, CHR1 *S));

PUBLIC UINT4 MrpTrcGetTraceOptionValue PROTO ((UINT1 *pu1TraceInput,
                                                      INT4 i4Tracelen));
PUBLIC UINT4 MrpTrcGetTraceInputValue  PROTO ((UINT1 *pu1TraceInput,
                                                 UINT4 u4TracVal));
PUBLIC VOID MrpTrcSetTraceOption       PROTO ((UINT1 *pu1Token,
                                                 UINT4 *pu4TraceOption));
PUBLIC VOID MrpTrcSplitStrToTokens     PROTO ((UINT1 *pu1InputStr,
                                                 INT4 i4Strlen,
                                                 UINT1 u1Delimiter,
                                                 UINT2 u2MaxToken,
                                                 UINT1 *apu1Token[],
                                                 UINT1 *pu1TokenCount));
VOID
MrpTrcGblTrace  PROTO ((UINT4 u4Mask, const char *fmt, ...));
VOID
MrpTrcPrintPortList PROTO ((UINT4 u4ContextId, UINT4 u4ModId, 
                            tLocalPortList PortList, UINT2 u2InPort));
VOID
MrpTrcPrintAttribute PROTO ((tMrpAttr * pAttr));

VOID
MrpTrcPrintMacAddr PROTO ((UINT4 u4ContextId, UINT4 u4ModId, 
                            tMacAddr MacAddr));

/*--------------------------------------------------------------------------*/
/*                     mrpapp.c                                            */
/*--------------------------------------------------------------------------*/
VOID
MrpAppDeEnrolMrpApplication PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                    UINT1 u1AppId));

VOID MrpAppEnrolApplFunctions PROTO ((VOID));
VOID MrpAppModuleEnable    PROTO ((tMrpContextInfo *));
INT4 MrpAppInitMrpAppEntry    PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       tMrpAppEntry *pMrpAppEntry, 
                                       UINT1 u1AppId));
VOID MrpAppDeInitMrpAppEntry  PROTO ((tMrpAppEntry *pMrpAppEntry));
tMrpAppPortEntry *
MrpAppCreateMrpAppPortEntry PROTO ((tMrpAppEntry *pMrpAppEntry, 
                                     UINT2 u2Port));
INT4
MrpAppAddAppPortEntryToAppEntry PROTO ((tMrpAppEntry *pMrpAppEntry, 
                                         tMrpAppPortEntry *pMrpAppPortEntry));
INT4
MrpAppAddMapEntryToAppEntry PROTO ((tMrpAppEntry *pMrpAppEntry, 
                                     tMrpMapEntry *pMrpMapEntry));
INT4
MrpAppDelAppPortEntryFrmAppEntry PROTO ((tMrpAppEntry *pMrpAppEntry, 
                                         tMrpAppPortEntry *pMrpAppPortEntry));
INT4
MrpAppDelMapEntryFromAppEntry PROTO ((tMrpAppEntry *pMrpAppEntry, 
                                       tMrpMapEntry *pMrpMapEntry));
INT4
MrpAppIncPortArraySizeInAppEntry PROTO ((tMrpAppEntry *pMrpAppEntry,
                                         UINT2 u2Port));
INT4
MrpAppIncMapArraySizeInAppEntry PROTO ((tMrpAppEntry *pMrpAppEntry, 
                                         UINT2 u2MapId));
INT4
MrpAppDecPortArraySizeInAppEntry PROTO ((tMrpAppEntry *pMrpAppEntry));

INT4
MrpAppDecMapArraySizeInAppEntry PROTO ((tMrpAppEntry *pMrpAppEntry));

INT4 MrpAppAddAndEnablePortsInAppl PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                            tMrpAppEntry *pMrpAppEntry));
INT4 MrpAppAddPortToAppl      PROTO ((tMrpAppEntry *pMrpAppEntry, 
                                       UINT2 u2Port));
INT4 MrpAppDelPortFromAppl    PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       UINT1 u1AppId, UINT2 u2Port));
INT4 MrpAppEnableApplOnPort   PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       tMrpAppEntry *pAppEntry, UINT2 u2Port));

INT4 MrpAppDisableApplOnPort  PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                       tMrpAppEntry *pAppEntry, UINT2 u2Port));

INT4 MrpAppInitMrpAppPortEntry PROTO ((tMrpAppEntry *pMrpAppEntry, 
                                        tMrpAppPortEntry *pMrpAppPortEntry, 
                                       UINT2 u2Port));
PUBLIC 
tMrpAppEntry * MrpAppGetAppEntryFrmAppAddr PROTO ((UINT4 u4ContextId, 
                                                           tMacAddr AppAddr));
INT4
MrpAppGetNextAppAddFrmCurAppAdd  PROTO ((UINT4  u4ContextId,
                                        tMacAddr CurrAppAddr ,
                                        UINT1 *pu1NextAppAddr));
INT4
MrpAppGetNextAppAddrAndAttrType PROTO ((UINT4  u4ContextId,
                                          tMacAddr CurrAppAddr ,
                                          UINT1 *pu1NextAppAddr,
                                          INT4  i4CurrAttrType,
                                          INT4 *pi4NextAttrType));
VOID
MrpAppGetAppIdFromAppAddr  PROTO ((UINT4 u4ContextId, 
                                    tMacAddr AppAddr, 
                                    UINT1 *u1AppId));
INT4
MrpAppDeInitMrpAppPortEntry PROTO ((tMrpAppEntry * pMrpAppEntry,
                                    tMrpAppPortEntry * pMrpAppPortEntry));
/*--------------------------------------------------------------------------*/
/*                     mrpbuddy.c                                          */
/*--------------------------------------------------------------------------*/
VOID 
MrpBuddyCalculateBuddySize PROTO ((VOID **pInfo, UINT2 u2CurrSize, 
                                   UINT2 u2DefBlkSize, UINT1 u1BuddyId, 
                                   UINT2 *pu2ReqBuddySize, 
                                   UINT2 *pu2EntryCount));
VOID
MrpBuddyIncNumOfEntryCnt PROTO ((UINT1 u1BuddyId, UINT2 *pu2EntryCount, 
                                 UINT1 u1Val));
VOID
MrpBuddyGetNoOfEntriesInBuddyBlk PROTO ((UINT2 u2StartIndex, UINT2 u2EndIndex, 
                                         UINT1 u1BuddyId, VOID **ppInfo, 
                                         UINT2 *pu2EntryCount));

/*--------------------------------------------------------------------------*/
/*                     mrpremap.c                                          */
/*--------------------------------------------------------------------------*/
VOID
MrpRemapHandleVlanInstanceMap PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
                                      UINT2 u2MapId, UINT2 u2NewInstance));

VOID
MrpRemapWithoutAttrInNewInst PROTO ((UINT4 u4ContextId,
                                     tMrpAppEntry * pAppEntry, tVlanId VlanId,
                                     UINT2 u2Instance, UINT1 u1DynVlan));
VOID
MrpRemapUpdateRegSem PROTO ((UINT4 u4ContextId, tMrpAppEntry * pAppEntry, 
                             tMrpMapPortEntry * pMapPortEntry,
                             tMrpAttrEntry * pAttrEntry, tVlanId VlanId,
                             UINT1 u1VlanUpdate));
INT2
MrpRemapUpdateMvrpRegCount PROTO ((tMrpAppEntry * pAppEntry, 
                                   tVlanId VlanId,
                                   UINT2 u2MapId, UINT2 u2NewMapId));

INT2
MrpRemapUpdateVlanRegCount PROTO ((tMrpAppEntry *pMmrpAppEntry, tVlanId VlanId, 
                                   UINT2 u2NewMapId));

VOID
MrpMvrpHdleRemapEvntForStEntries PROTO ((tMrpAppEntry * pAppEntry,
                                         UINT2 u2MapId, tMrpAttr * pAttr,
                                         UINT1 u1RemapFlag));

INT2
MrpRemapAddAttrEntryInNewMap PROTO ((tMrpAppEntry *pAppEntry, UINT2 u2PortId, 
                                     UINT2 u2NewMapId, tMrpAttr *pAttr,
                                     UINT1 u1PortAppRegSemState));

VOID
MrpRemapFormMMRPDU PROTO ((UINT1 **ppBuf, tMrpVectAttrInfo *pVectAttrInfo, 
                           UINT2 *pu2Offset, UINT2 *pu2NumOfValues, 
                           UINT1 u1AttrEvent, UINT1 *pu1AddVectHdr));
VOID
MrpRemapMmrpUpdateSemAndTx PROTO ((tMrpContextInfo *pContextInfo,
                                   tMrpAppEntry *pAppEntry,
                                   tMrpPortEntry * pPortEntry, 
                                   tMrpMapEntry *pMapEntry,
                                   tMrpMapPortEntry * pMapPortEntry,
                                   UINT1 u1DynVlan, 
                                   UINT1 u1AttrFlag));

/*--------------------------------------------------------------------------*/
/*                       mrpctxt.c                                          */
/*--------------------------------------------------------------------------*/

INT4
MrpCtxtHandleCreateContext PROTO ((UINT4));
INT4
MrpCtxtHandleDeleteContext PROTO ((UINT4));
tMrpContextInfo *
MrpCtxtGetContextInfo PROTO ((UINT4 u4ContextId));
INT4
MrpCtxtNotifyContextCreateOrDel PROTO ((UINT4 u4ContextId, UINT2 u2MsgType));
INT4
MrpCtxtAddPortToContextEntry PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                     tMrpPortEntry *pMrpPortEntry));
INT4
MrpCtxtDelPortEntryFromCxtEntry PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                        tMrpPortEntry *pMrpPortEntry));
INT4
MrpCtxtIncPortAraySizeInCxtEntry PROTO ((tMrpContextInfo *pMrpContextInfo, 
                                         UINT2 u2Port));
INT4
MrpCtxtDecPortAraySizeInCxtEntry PROTO ((tMrpContextInfo *pMrpContextInfo));

INT4
MrpCtxtHandleUpdateContextName PROTO ((UINT4));
INT4
MrpCtxtGetNextPortInContext PROTO ((UINT4 u4ContextId, 
                                    UINT4 u4PortId, 
                                    UINT4 *pu4NextPortId));

tMrpContextInfo *
MrpCtxtGetValidContext PROTO ((UINT4 u4ContextId));

/*--------------------------------------------------------------------------*/
/*                       mrpattr.c                                          */
/*--------------------------------------------------------------------------*/

INT4
MrpAttrMapAttrTblCmpFn PROTO ((tRBElem *pAttEntry1, tRBElem *pAttEntry2));
INT4
MrpAttrFreeMapAttrEntry PROTO ((tRBElem *pElem, UINT4 u4Arg));
VOID
MrpAttrSetAttrIndexToAttribute PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                       tMrpAttrEntry *pMrpAttrEntry));
VOID
MrpAttrUpdMapNextFreeAttrIndex PROTO ((tMrpMapEntry *pMrpMapEntry, 
                                       UINT2 u2AttrIndex, UINT1 u1Action));
INT4
MrpAttrCheckAndDelAttrEntry PROTO ((tMrpMapPortEntry * pPortEntry,
                                    tMrpAttrEntry * pAttrEntry));
INT4
MrpAttrIsAttrAdminRegCtrlFixed PROTO ((tMrpContextInfo *pContextInfo, 
                                       tMrpAppEntry *pAppEntry, 
                                       tMrpMapEntry *pMapEntry, 
                                       UINT2 u2AttrIndex, UINT2 u2Port));
INT4
MrpAttrGetRegAdminCtrlFrmAttrVal PROTO ((UINT2 u2PortId, tMrpAttr *pAttr,
                                         tMrpMapEntry  *pMrpMapEntry,
                                         UINT1 *pu1RegAdminCtrl));
INT4
MrpAttrValidateAttrRegistration PROTO ((tMrpAttr *pAttr, 
                                        tMrpMapEntry *pMapEntry, 
                                        UINT2 u2Port));
/*--------------------------------------------------------------------------*/
/*                       mrppeer.c                                          */
/*--------------------------------------------------------------------------*/
INT4
MrpPeerCrtPeerMacAddrTbl PROTO ((tMrpContextInfo *pContextInfo));
INT4
MrpPeerCrtPeerMacAddrIndxTbl PROTO ((tMrpContextInfo *pContextInfo));
VOID
MrpPeerDelPeerMacAddrTbl PROTO ((tMrpContextInfo *pContextInfo));
VOID
MrpPeerDelPeerMacAddrIndxTbl PROTO ((tMrpContextInfo *pContextInfo));
INT4
MrpPeerFreePeerMACEntry PROTO ((tRBElem * pElem, UINT4 u4Arg));
INT4
MrpPeerAddSrcMACToPeerMACTbl PROTO ((tMrpContextInfo * pContextInfo,
                                     tMacAddr SrcMacAddr, UINT2 *pu2MACIndex));
INT4
MrpPeerGetPeerMACIndex PROTO ((tMrpContextInfo *pContextInfo,
                               tMacAddr SrcMacAddr, UINT2 *pu2MACIndex));


/*--------------------------------------------------------------------------*/
/*                       mrpvlan.c                                          */
/*--------------------------------------------------------------------------*/
VOID
MrpVlanHandlePropagateVlanInfo PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                                       UINT2 u2Port, tLocalPortList AddPortList,
                                       tLocalPortList DelPortList));
VOID
MrpVlanHandleSetVlanForbiddPorts PROTO ((UINT4 u4ContextId, tVlanId VlanId,
                                         UINT2 u2Port,
                                         tLocalPortList AddPortList,
                                         tLocalPortList DelPortList));
VOID
MrpVlanHandlePropagateMacInfo PROTO ((UINT4 u4ContextId, tMacAddr MacAddr, 
                                      tVlanId VlanId, UINT2 u2Port,
                                      tLocalPortList AddPortList,
                                      tLocalPortList DelPortList));
VOID
MrpVlanHandleSetMcastForbidPorts PROTO ((UINT4 u4ContextId, tMacAddr MacAddr, 
                                         tVlanId VlanId, UINT2 u2Port,
                                         tLocalPortList AddPortList,
                                         tLocalPortList DelPortList));
VOID
MrpVlanHandlePropagateDefGrpInfo PROTO ((UINT4 u4ContextId, UINT1 u1Type, 
                                         tVlanId VlanId, UINT2 u2Port,
                                         tLocalPortList AddPortList,
                                         tLocalPortList DelPortList));
VOID
MrpVlanHndleSetDefGrpForbidPorts PROTO ((UINT4 u4ContextId, UINT1 u1Type, 
                                          tVlanId VlanId,
                                          UINT2 u2Port,
                                          tLocalPortList AddPortList,
                                          tLocalPortList DelPortList));

/*--------------------------------------------------------------------------*/
/*                       mrpred.c                                           */
/*--------------------------------------------------------------------------*/
#ifdef L2RED_WANTED
VOID
MrpRedMakeNodeActiveFromStandby PROTO ((VOID));
VOID
MrpRedMakeNodeStandbyFromIdle PROTO ((VOID));
VOID
MrpRedHandleStandByUpEvent PROTO ((VOID));
VOID
MrpRedSendMsgToRm PROTO ((UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg * pRmMsg));
VOID
MrpRedHandleGoActiveEvent PROTO ((VOID));
VOID
MrpRedHandleGoStandbyEvent PROTO ((VOID));
VOID
MrpRedMakeNodeActiveFromIdle PROTO ((VOID));
VOID
MrpRedSendBulkUpdReq PROTO ((VOID));
VOID
MrpRedFormOperChgSyncUpMsg PROTO ((tRmMsg * pRmMsgBuf, tMrpRedMsgInfo *pMsgInfo,
                                   UINT4 *pu4Offset));
VOID
MrpRedFormTmrRestartSyncUpMsg PROTO ((tRmMsg * pRmMsgBuf, 
                                      tMrpRedMsgInfo *pMsgInfo, 
                                      UINT4 *pu4Offset));
VOID
MrpRedFormAttributeSyncUpMsg PROTO ((tRmMsg * pRmMsgBuf, 
                                     tMrpRedMsgInfo * pMsgInfo,
                                     UINT1 u1AppId, UINT1 u1AttrType, 
                                     UINT4 *pu4Offset));
INT4
MrpRedAllocMemForRmMsg PROTO ((UINT1 u1MsgType, UINT2 u2MsgLen,
                               tRmMsg ** ppRmMsg));
VOID
MrpRedProcessSyncUpMsg PROTO ((tMrpRmCtrlMsg * pRmCtrlMsg));
VOID
MrpRedSendBulkUpdTailMsg PROTO ((VOID));
VOID
MrpRedPutMsgHdrInRmMsg PROTO ((tRmMsg * pRmMsgBuf, UINT1 u1MsgType,
                               UINT2 u2MsgLen));
#endif /* L2RED_WANTED */
VOID
MrpRedProcessRmMsg PROTO ((tMrpRmCtrlMsg *pRmMsg));
INT4
MrpRedSendSyncUpMessages PROTO ((UINT1 u1MsgType, tMrpRedMsgInfo *pMsgInfo));
INT4
MrpRedRegisterWithRM PROTO ((VOID));

/*--------------------------------------------------------------------------*/
/*                       mrprdutl.c                                         */
/*--------------------------------------------------------------------------*/
#ifdef L2RED_WANTED
VOID
MrpRdUtlGetMsgLen PROTO ((UINT1 u1MsgType, UINT2 *pu2MsgLen));
INT4
MrpRdUtlFormAttrAddSyncUpMsg PROTO ((UINT1 u1MsgType,
                                     tMrpRedMsgInfo *pMsgInfo));
INT4
MrpRdUtlProcPortOperChgSyncUp PROTO ((tRmMsg * pRmMsg, UINT2 u2MsgLen));
INT4
MrpRdUtlProcTmrRestartSyncUpMsg PROTO ((tRmMsg *pRmMsg, UINT2 u2MsgLen));
INT4
MrpRdUtlProcSerReqAddSyncUpMsg PROTO ((tRmMsg *pRmMsg, UINT2 u2MsgLen));
INT4
MrpRdUtlProcAttrAddSyncUpMsg PROTO ((tRmMsg *pRmMsg, UINT1 u1MsgType));
INT4
MrpRdUtlProcAttributeSyncUpMsg PROTO ((tRmMsg *pRmMsg, UINT1 u1MsgType,
                                       UINT2 u2MsgLen));
INT4
MrpRdUtlProcAttrDelSyncUpMsg PROTO ((UINT4 u4ContextId, UINT2 u2PortId,
                                     tVlanId VlanId, tMrpAttr *pMmrpAttrVal, 
                                     UINT1 u1AppId));

VOID
MrpRdUtlProcBulkUpdReq PROTO ((VOID));
VOID
MrpRdUtlSendLvAllMsg PROTO ((tMrpContextInfo *pContextInfo, UINT1 u1AppId));
INT4
MrpRdUtlApplyAttrRegistration PROTO ((tMrpAppEntry *pAppEntry, UINT2 u2MapId, 
                                      UINT2 u2Port, tMrpAttr *pAttr));
UINT4
MrpRdUtlGetMinBulkUpdMsgLen PROTO ((UINT1 u1MsgType));
VOID
MrpRdUtlEnableApplications PROTO ((VOID));
#endif /* L2RED_WANTED */
/*--------------------------------------------------------------------------*/
/*                       mrprbulk.c                                         */
/*--------------------------------------------------------------------------*/
#ifdef L2RED_WANTED
INT4
MrpRBulkSendPortOperStatBulkUpd PROTO ((VOID));
VOID
MrpRBulkSendLvAllTmrRemTimBlkUpd PROTO ((VOID));
INT4
MrpRBulkFormLvAllTmrRemTimBlkUpd PROTO ((UINT1 u1AppId, UINT1 u1MsgType));
INT4
MrpRBulkProcLvAllTmrBulkMsg PROTO ((tRmMsg *pRmMsg, UINT1 u1MsgType,
                                    UINT2 u2MsgLen));
INT4
MrpRBulkProcAttrBulkMsg PROTO ((tRmMsg *pRmMsg, UINT1 u1MsgType, 
                                UINT2 u2MsgLen));
INT4
MrpRBulkProcPortOperStatusMsg PROTO ((tRmMsg *pRmMsg, UINT2 u2MsgLen));
INT4 
MrpRBulkCPURelinInBulkReqHandlng PROTO ((VOID));
VOID 
MrpRBulkProcessPendingEvents PROTO ((VOID));
INT4
MrpRBulkProcBulkUpdMsg PROTO ((tRmMsg *pRmMsg, UINT2 u2MsgLen));
INT4
MrpRBulkSendAttrBlkUpd PROTO ((UINT1 u1BulkMsgType));
VOID 
MrpRBulkFillAttrBulkMsg PROTO ((UINT1 u1BulkMsgType,
                                tMrpMapPortEntry *pMapPortEntry,
                                tMrpAttrEntry *pAttrEntry, 
                                tMrpRedBulkMsgInfo *pMrpBulkMsgInfo));
VOID
MrpRBulkUpdAndSendAttrBulkMsg PROTO ((tMrpRedBulkMsgInfo *pMrpBulkMsgInfo));
INT4
MrpRBulkFormAttrBulkMsg PROTO ((tMrpRedBulkMsgInfo *pMrpBulkMsgInfo, 
                                UINT1 u1BulkMsgType, tMrpAppEntry *pAppEntry,
                                UINT2 u2Port));
#endif /* L2RED_WANTED */
#endif/*_MRP_PROT_H*/
/*--------------------------------------------------------------------------*/
/*                     End of the file mrpprot.h                           */
/*--------------------------------------------------------------------------*/
