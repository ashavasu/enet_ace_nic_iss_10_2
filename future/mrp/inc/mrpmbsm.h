/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpmbsm.h,v 1.3 2009/08/31 09:43:05 prabuc Exp $
 *
 * Description: This file contains MRP MBSM Function Prototypes . 
 *
 *****************************************************************************/
#ifndef _MRP_MBSM_H
#define _MRP_MBSM_H

#ifdef MBSM_WANTED

#define MBSM_MRP    "MRPMbsm"

INT4
MrpMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
                tMbsmSlotInfo * pSlotInfo);
#endif /*_MRP_MBSM_H*/
#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpmbsm.h                      */
/*-----------------------------------------------------------------------*/
