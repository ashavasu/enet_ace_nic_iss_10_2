/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mrprdstb.h,v 1.2 2009/08/31 09:43:05 prabuc Exp $
 *
 * Description: This file contains the definitions, data structures
 *              and prototypes to support High Availability for MRP module.   
 *
 ************************************************************************/
/* Message types */
enum {
    /* in rmgr.h,
     *  RM_BULK_UPDT_REQ_MSG(1)
     *  RM_BULK_UPDATE_MSG(2)
     *  RM_BULK_UPDT_TAIL_MSG(3) message
     *  types are defined with values 1,2 and 3 respectively. So, start MRP
     *  specific msgtypes with value 4 */
    MRP_RED_VLAN_ADD_MSG = 4,
    MRP_RED_VLAN_DEL_MSG = 5,
    MRP_RED_MAC_ADD_MSG = 6,
    MRP_RED_MAC_DEL_MSG = 7,
    MRP_RED_SERVICE_REQ_ADD_MSG = 8,
    MRP_RED_SERVICE_REQ_DEL_MSG = 9,
    MRP_RED_PORT_OPER_STATUS_MSG = 10,
    MRP_RED_LV_ALL_TMR_RESTART_MSG = 11,
    MRP_RED_MVRP_LV_ALL_TMR_MSG = 12,
    MRP_RED_MMRP_LV_ALL_TMR_MSG = 13,
    MRP_RED_INVALID_MSG = 12
};

#define MRP_RED_NODE_STATUS() RM_ACTIVE

#define MRP_IS_NP_PROGRAMMING_ALLOWED()  OSIX_TRUE 
