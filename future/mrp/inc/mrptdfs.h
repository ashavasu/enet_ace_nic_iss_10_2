/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mrptdfs.h,v 1.12 2013/05/06 11:58:48 siva Exp $
 *
 * Description: This file contains data structures defined for
 *              MRP module.
 *********************************************************************/
#ifndef _MRP_TDFS_H
#define _MRP_TDFS_H

typedef struct _tMrpMapEntry tMrpMapEntry;
typedef struct _tMrpAppEntry tMrpAppEntry;
typedef struct _tMrpContextInfo tMrpContextInfo;

/* As per IEEE 802.1ak 2007 section 10.8.2.3 */
/* MRPDU Attribute Events */
enum {
    MRP_NEW_ATTR_EVENT = 0,
    MRP_JOININ_ATTR_EVENT = 1,
    MRP_IN_ATTR_EVENT = 2,
    MRP_JOINMT_ATTR_EVENT = 3,
    MRP_MT_ATTR_EVENT = 4,
    MRP_LV_ATTR_EVENT = 5, 
    MRP_INVALID_EVENT = 6
};

/* MRP Attribute Types */
enum {
    MRP_VID_ATTR_TYPE = 1,
    MRP_SERVICE_REQ_ATTR_TYPE = 1,
    MRP_MAC_ADDR_ATTR_TYPE = 2,
    MRP_MAX_ATTR_TYPES = 3
};

/* MRP Timer Types */
enum {
    MRP_JOIN_TMR = 0,      /* Join Timer */
    MRP_LEAVE_TMR = 1,     /* Leave Timer */
    MRP_LEAVE_ALL_TMR = 2, /* LeaveAll Timer */
    MRP_PERIODIC_TMR = 3,  /* Periodic Timer */
    MRP_MAX_TMR_TYPES = 4
};

/* Used for seding Traps */
enum {
    MRP_LACK_OF_RESOURCE = 1,
    MRP_RESTRICTED_REGISTRATION = 2,
    MRP_UNSUPPORTED_ATTR_VAL = 3,
    MRP_MAX_FAILURE_REASONS = 4
};

/* Registrar Admin Control */
enum {
    MRP_REG_NORMAL = 0,
    MRP_REG_FIXED = 1,
    MRP_REG_FORBIDDEN = 2,
    MRP_REG_INVALID = 3
};

/* Applicant SEM states */
enum {
    MRP_VO = 0,
    MRP_VP = 1,
    MRP_VN = 2,
    MRP_AN = 3,
    MRP_AA = 4,
    MRP_QA = 5,
    MRP_LA = 6,
    MRP_AO = 7,
    MRP_QO = 8,
    MRP_AP = 9,
    MRP_QP = 10,
    MRP_LO = 11,
    MRP_NO_CHANGE = 12
};

/* Applicant SEM events */
enum {
    MRP_APP_REQ_NEW = 0,
    MRP_APP_REQ_JOIN = 1,
    MRP_APP_REQ_LEAVE = 2,
    MRP_APP_RX_NEW = 3,
    MRP_APP_RX_JOIN_IN = 4,
    MRP_APP_RX_IN = 5,
    MRP_APP_RX_JOIN_MT = 6,
    MRP_APP_RX_MT = 7,
    MRP_APP_RX_LEAVE = 8,
    MRP_APP_RX_LEAVE_ALL = 9,
    MRP_APP_REDECLARE = 10,
    MRP_APP_PERIODIC = 11,
    MRP_APP_TX = 12,
    MRP_APP_TX_LA = 13,
    MRP_APP_TX_LAF = 14
};

/* Type of message to be sent on getting the Tx opportunity */
typedef enum {
    MRP_SEND_NEW = 0,
    MRP_SEND_JOIN = 1,
    MRP_SEND_LEAVE = 2,
    MRP_SEND_IN_OR_MT = 3,
    MRP_SEND_JOIN_OPTIONAL = 4,
    MRP_SEND_IN_OR_MT_OPTIONAL = 5,
    MRP_SEND_NONE = 6
} eMrpSendMsgInd;


/* Registrar SEM states */
enum {
    MRP_MT = 0,
    MRP_IN = 1,
    MRP_LV = 2
};

/* Registrar SEM events */
enum {
    MRP_REG_RX_NEW = 0,
    MRP_REG_RX_JOIN_IN = 1,
    MRP_REG_RX_JOIN_MT = 2,
    MRP_REG_RX_LEAVE = 3,
    MRP_REG_RX_LEAVE_ALL = 4,
    MRP_REG_TX_LEAVE_ALL = 5,
    MRP_REG_REDECLARE = 6,
    MRP_REG_FLUSH = 7,
    MRP_REG_LEAVE_TMR_EXPIRY = 8
};

/* Type of indications to be given from MAD to MAP and higher layer 
 * protocol (VLAN)
 */
typedef enum {
    MRP_HL_IND_NEW = 0, 
    MRP_HL_IND_JOIN = 1, 
    MRP_HL_IND_LEAVE = 2, 
    MRP_HL_IND_NONE = 3
} eMrpHlMsgInd;

typedef enum {
    MRP_TMR_START = 0,
    MRP_TMR_STOP = 1,
    MRP_TMR_NONE = 2
} eMrpLeaveTmrInd;

/* Used while forming the bulk update synchronization message */
typedef struct MrpRedBulkMsgInfo {
    tRmMsg     *pRmMsg;
    UINT4       u4ContextId;
    UINT4       u4MsgOffset;
    UINT4       u4AttrCntFieldOffset;
    UINT4       u4PortCntFieldOffset;
    UINT4       u4MapCntFieldOffset;
    UINT2       u2BufSize;
    UINT2       u2NumOfPorts;
    UINT2       u2NumOfAttrs;
    UINT2       u2NumOfMapIds;
    UINT1       u1IsCtxtIdFilled;
    UINT1       u1IsPortIdFilled;
    UINT1       u1IsMapIdFilled;
    UINT1       u1IsPortCntFilled;
    UINT1       u1IsAttrCntFilled;
    UINT1       u1IsMapCntFilled;
    UINT1       au1Reserved[2];
}tMrpRedBulkMsgInfo;

/* Contains information to be filled in the synchronization message */
typedef struct MrpRedMsgInfo {
    UINT4             u4ContextId;
    tVlanId           VlanId;
    UINT2             u2LocalPortId;
    UINT1             au1MmrpAttrVal [MRP_MAX_ATTR_LEN];
                       /* Used for filling the MAC address and the Service
                        * Requirement.
                        * au1MmrpAttrVal [0] - Contains Service requirement
                        */
    UINT1             u1PortOperStatus;
    UINT1             u1AppId;
    UINT1             u1EndOfPdu; /* Set to OSIX_TRUE when MRPDU processing
                                   * is completed. Else it is set to OSIX_FALSE
                                   */
    UINT1             au1Reserved[3];
}tMrpRedMsgInfo;

/*  Structure defined for the usage of Registrar State Machine */

typedef struct MrpRegSemEntry {
    eMrpHlMsgInd        HlMsgInd; /* Indicates whether propagation as well as
                                   * higher layer needs to be given for the 
                                   * next Registrar SEM state
                                   */
    eMrpLeaveTmrInd     LeaveTmrInd; /* Indicates whether the Leave timer needs
                                      * to be started or stopped for the next
                                      * Registrar SEM state
                                      */
    UINT4               u4NextState; /* Next Registrar SEM state */
} tMrpRegSemEntry;

/*  Structure defined for the usage of Applicant State Machine */
typedef struct MrpAppSemEntry {
    eMrpSendMsgInd      SendMsgInd; /* Indicates the type of event to be encoded
                                     * for the current Applicant SEM state
                                     */
    UINT2               u2NextState; /* Next Applicant SEM state */
    UINT2               u2JoinTmrInd; /* Indicates whether timer need to be 
                                       * started for the next Applicant SEM
                                       * state
                                       */
} tMrpAppSemEntry;


/* MRP Timer Structure */
typedef struct MrpTimer {
    tTmrBlk             TmrBlk;
} tMrpTimer;

/* Attribute Information Structure */
typedef struct MrpAttr {
    UINT1               au1AttrVal [MRP_MAX_ATTR_LEN];
    UINT1               u1AttrType;
    UINT1               u1AttrLen;
} tMrpAttr;

/* Used For sending Trap */
typedef struct MrpTraps{
    tMrpAttr *pAttr;       /* Attribute Information (Attr type, 
                            * Attr Length,Attr Value).*/
    UINT4    u4ContextId;  
    UINT2    u2PortId;
    UINT1    u1ReasonType; /* Reason for Registration failures.*/
    UINT1    u1Reserved;
}tMrpTrapInfo;

/* Structure for MRP Statistics Counters */
typedef struct MrpStatsEntry {
    UINT4               u4RxValidPduCnt;
    UINT4               u4RxInvalidPduCnt;
    UINT4               u4RxNewMsgCnt;
    UINT4               u4RxJoinInMsgCnt;
    UINT4               u4RxJoinMtMsgCnt;
    UINT4               u4RxLeaveMsgCnt;
    UINT4               u4RxInMsgCnt;
    UINT4               u4RxEmptyMsgCnt;
    UINT4               u4RxLeaveAllMsgCnt;
    UINT4               u4TxPduCnt;
    UINT4               u4TxNewMsgCnt;
    UINT4               u4TxJoinInMsgCnt;
    UINT4               u4TxJoinMtMsgCnt;
    UINT4               u4TxLeaveMsgCnt;
    UINT4               u4TxInMsgCnt;
    UINT4               u4TxEmptyMsgCnt;
    UINT4               u4TxLeaveAllMsgCnt;
    UINT4               u4NoOfRegistration;
} tMrpStatsEntry;

typedef struct MrpAttrEntry {
   tRBNodeEmbd         MapAttrEntryNode; /* RBTree Node to be added to the 
                                           * MapAttrTable RBTree.*/
    tMrpMapEntry       *pMapEntry;        /* Back Pointer to the MAP entry.*/
    tMrpAttr            Attr;
    UINT2               u2AttrIndex;      /* Indicates the position of the 
                                           * Attribute value in the
                                           * AttrInfoList and LvBitList.*/
    UINT2               u2StaticMemPortCnt;
                                         /* Number of ports that are statically
                                          * configured for this Attribute value.
                                          * Used on receiving a MRPDU on a port
                                          * whose Restricted VLAN/MAC parameter
                                          * is set to TRUE.
                                          */
    UINT2               u2PortCount;      /* No: of ports on which state 
                                           * machine is maintained for this 
                                           * attribute value.*/
    UINT1               u1Reserved[2];
} tMrpAttrEntry;



/* Attention: The Following structures are used for Memory Calculation Only */
/* START- MEMORY CALCULATION BY USING STRUCTURES */

typedef struct MrpVidMapBulkMsg {
    UINT2           u2MsgEvent;/* Can be map/unmap VLAN List or single VLAN */
    UINT2           u2NewMapId;  /* Can be Instance Id (or Fid) */
    INT4            i4EventCount; /* No of mapping posted - VlanMapInfo index */
    tVlanMapInfo    VlanMapInfo[VLAN_MAP_INFO_PER_POST]; /* When List of VLAN is mapped */
} tMrpVidMapBulkMsg;



typedef struct MrpBulkQMsg {
   INT4                  i4Count;    /* Count of msgs in union - 
                                        No. of Bulk/VidMap Messages */
   UINT4                 u4ContextId; /*Context  Identifier*/
   UINT4                 u4IfIndex;   /*Interface Index*/
   UINT2                 u2MsgType;
   UINT1                 u1MsgInfo; /* TcDetected Timer Status / OperP2P /
                                     * Flush or Redeclare event 
                                     */
   UINT1                 au1Reserved[1];
   union
   {
       tMrpRmCtrlMsg  MrpRmMsg; /* Control Msg from RM Module */
       tMrpBulkMsg    MrpBulkMsg [VLAN_NO_OF_MSG_PER_POST]; /* Bulk Array of messages from VLAN at 
                                        MVRP/MMRP init time */
       tMrpMsg        MrpMsg;
       tMrpVidMapMsg  MrpVidMapMsg[1];
   }
   unMrpMsg;
    /* !!!!!!!!!!!!!!!!!!!!!!!!CAUTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * DONT DECLARE ANY VARIABLES BELOW THIS UNION. IF DECLARED, THEN THOSE
     * VARIABLES MAY GET CORRUPTED BECAUSE THERE ARE DYNAMICALLY GROWING 
     * ARRAYS INSIDE THIS UNION. 
     * REFER: MrpFillBulkMessage &  MrpUpdateInstVlanMap */
}tMrpBulkQMsg;

typedef struct MrpVidMapQMsg {
   INT4                  i4Count;    /* Count of msgs in union - 
                                        No. of Bulk/VidMap Messages */
   UINT4                 u4ContextId; /*Context  Identifier*/
   UINT4                 u4IfIndex;   /*Interface Index*/
   UINT2                 u2MsgType;
   UINT1                 u1MsgInfo; /* TcDetected Timer Status / OperP2P /
                                     * Flush or Redeclare event 
                                     */
   UINT1                 au1Reserved[1];
   union
   {
       tMrpRmCtrlMsg  MrpRmMsg; /* Control Msg from RM Module */
       tMrpBulkMsg    MrpBulkMsg [1]; /* Bulk Array of messages from VLAN at 
                                        MVRP/MMRP init time */
       tMrpMsg        MrpMsg;
       tMrpVidMapBulkMsg MrpVidMapMsg[1];
   }
   unMrpMsg;
    /* !!!!!!!!!!!!!!!!!!!!!!!!CAUTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     * DONT DECLARE ANY VARIABLES BELOW THIS UNION. IF DECLARED, THEN THOSE
     * VARIABLES MAY GET CORRUPTED BECAUSE THERE ARE DYNAMICALLY GROWING 
     * ARRAYS INSIDE THIS UNION. 
     * REFER: MrpFillBulkMessage &  MrpUpdateInstVlanMap */
}tMrpVidMapQMsg;


/* Array of MvrpAttrEntry pointers of size VLAN_DEV_MAX_VLAN_ID+1 */ 
typedef struct MrpMvrpAttrArrayEntry {
    tMrpAttrEntry   *apMrpAttrEntry[MRP_MAX_MEM_MVRP_ATTR_IN_SYS];
}tMrpMvrpAttrArrayEntry;

/* Array of MmrpAttrEntry pointers of size MRP_MAX_MMRP_ATTR_IN_SYS+1 */ 
typedef struct MrpMmrpAttrArrayEntry {
    tMrpAttrEntry   *apMrpAttrEntry[MRP_MAX_MEM_MMRP_ATTR_IN_SYS];
}tMrpMmrpAttrArrayEntry;

/* Array of UINT1 of size VLAN_DEV_MAX_VLAN_ID+1 */ 
typedef struct MrpMvrpPortAttrList {
    UINT1     aAttrList[MRP_MAX_MEM_MVRP_ATTR_IN_SYS];
}tMrpMvrpPortAttrInfoList;

/* Array of UINT1 of size MRP_MAX_MMRP_ATTR_IN_SYS+1 */ 
typedef struct MrpMmrpPortAttrList {
    UINT1     aAttrList[MRP_MAX_MEM_MMRP_ATTR_IN_SYS];
}tMrpMmrpPortAttrInfoList;

/* Array of tMacAddr of size VLAN_DEV_MAX_VLAN_ID+1 */ 
typedef struct MrpPortMvrpPeerMacTable{
    tMacAddr  PeerMacAddr[MRP_MAX_MEM_MVRP_ATTR_IN_SYS];
}tMrpPortMvrpPeerMacTable;

/* Array of tMacAddr of size MRP_MAX_MMRP_ATTR_IN_SYS+1 */ 
typedef struct MrpPortMmrpPeerMacTable{
    tMacAddr  PeerMacAddr[MRP_MAX_MEM_MMRP_ATTR_IN_SYS];
}tMrpPortMmrpPeerMacTable;

/* Array of UINT1 of size ((VLAN_MAX_VLAN_ID+31)/32*4) */ 
typedef struct MrpMvrpBitListTable {
    UINT1   au1BitList[((VLAN_MAX_VLAN_ID + 31)/32 * 4)];
}tMrpMvrpLvBitListTbl;

/* Array of UINT1 of size ((MRP_MAX_MMRP_ATTR_IN_SYS+31)/32*4) */ 
typedef struct MrpMmrpBitListTable {
    UINT1   au1BitList[((MRP_MAX_MMRP_ATTR_IN_SYS + 31)/32 * 4)];
}tMrpMmrpLvBitListTbl;

/* Array of UINT1 of size ((VLAN_MAX_VLAN_ID/MRP_PORTS_PER_BYTE) */ 
typedef struct MrpPortAttrTable {
    UINT1  au1PortAttr[(VLAN_MAX_VLAN_ID / MRP_PORTS_PER_BYTE)];
}tMrpPortMvrpAttrList;

/* Array of tMrpMapEntry pointers of size AST_MAX_MST_INSTANCES*/
typedef struct MrpMvrpMapTable {
    tMrpMapEntry *apMapEntry[AST_MAX_MST_INSTANCES];
}tMrpMvrpMapTable;

/* Array of tMrpMapEntry pointers of size (VLAN_DEV_MAX_VLAN_ID+1)*/
typedef struct MrpMmrpMapTable {
    tMrpMapEntry *apMapEntry[MRP_MAX_MEM_MVRP_ATTR_IN_SYS];
}tMrpMmrpMapTable;

/* Array of UINT1 pointers of size (MRP_MAX_MMRP_ATTR_IN_SYS+1) */
typedef struct MrpMmrpAttrIndexList{
    UINT1 *apu1MmrpAttrIndexList[MRP_MAX_MEM_MMRP_ATTR_IN_SYS];
}tMrpMmrpAttrIndexList;
/* END - MEMORY CALCULATION BY USING STRUCTURES */


typedef INT4 (*tMrpAppIndFn) (tMrpMapEntry *, tMrpAttr, UINT2, UINT1);
typedef INT4 (*tMrpAppValidateFn) (UINT1);
typedef INT4 (*tMrpAttrLenValidateFn) (UINT1, UINT1);
typedef INT4 (*tMrpAppWildCardValidateFn) (UINT4, tMrpAttr, UINT2, UINT2);

/* Application specific Functions */
typedef struct MrpAppnFn {
    tMrpAppIndFn              pAttrIndFn;
    tMrpAppValidateFn         pAttrTypeValidateFn;
    tMrpAttrLenValidateFn     pAttrLenValidateFn;
    tMrpAppWildCardValidateFn pAttrWildCardValidateFn;
} tMrpAppnFn;

typedef struct _tMrpPortEntry {
    tRBNodeEmbd         GlobalPortRBNode; 
                                   /* RBTree node to be added to the 
                                    * Global Port RBTree.
                                    */
    tMrpContextInfo    *pMrpContextInfo;
                                   /* Back Pointer to the context 
                                    * structure.
                                    */
    tMrpAttrEntry      *pNextMvrpTxNode;
                                   /* Pointer to the next attribute 
                                    * (VLAN ID) value to be transmitted 
                                    * when the Join Timer expires. If
                                    * this is NULL, then the tranmission
                                    * should start from the first VLAN
                                    * ID in the MvrpTxTable.
                                    */
    tMrpStatsEntry     *pStatsEntry [MRP_MAX_APPS + 1];
                                   /* Statistics Info
                                    */
    tMrpTimer           PeriodicTmrNode; 
                                   /* Periodic Transmission Timer Node
                                    */
    UINT4               u4IfIndex; 
                                   /* INDEX of Global Port RBTree:
                                    * Interface Index
                                    */
    UINT4               u4ContextId; 
                                   /* Context Id to which the physical port
                                    * is mapped
                                    */
    UINT4               u4JoinTime;
    UINT4               u4LeaveTime;
    UINT4               u4LeaveAllTime;
    UINT4               u4MvrpRegFailCnt;
                                   /* No: of failed MVRP registrations */
    UINT4               u4MmrpRegFailCnt;
                                   /* No: of failed MMRP registrations
                                    */
    UINT1              *pu1MvrpAttrList;
                                   /* Bitlist consisting list of MVRP
                                    * attributes mapped to this port. 
                                    * This is will be used in the TX 
                                    * thread for sending packet in 
                                    * optimized encoding logic.
                                    */
    tMacAddr            LastMvrpPduOrigin;
                                   /* Source MAC Addr. of the last
                                    * MVRPDU receieved on this port
                                    */
    tMacAddr            LastMmrpPduOrigin;
                                   /* Source MAC Addr. of the last
                                    * MMRPDU receieved on this port
                                    */
    UINT2              u2NextPortId;
    UINT2              u2PrevPortId;
    UINT2               u2BridgePortType; 
                                   /* Customer Port/CEP/CNP/PNP/..
                                    */
    UINT2               u2LocalPortId;
                                   /* Local Port Number
                                    */
    UINT1               au1ApplAdminCtrl [MRP_MAX_APPS + 1][MRP_MAX_ATTR_TYPES];
                                   /* Normal or Non-Participant or
                                    * Active
                                    */
    UINT1               u1ParticipantType;
                                   /* Full Participant/Applicant Only
                                    */
    UINT1               u1RegAdminCtrl;
                                   /* Reg admin control
                                    */
    UINT1               u1PeriodicSEMStatus;
                                   /* Enabled /Disabled
                                    */
    UINT1               u1PeriodicSEMState;
                                   /* Active /Passive
                                    */
    UINT1               u1OperStatus;
                                   /* Operational Status of the Port
                                    */
    UINT1               u1PortMvrpStatus;
                                   /* Enabled /Disabled
                                    */
    UINT1               u1PortMmrpStatus;
                                   /* Enabled /Disabled
                                    */
    UINT1               u1RestrictedVlanRegCtrl;
                                   /* TRUE /FALSE
                                    */
    UINT1               u1RestrictedMACRegCtrl;
                                   /* TRUE/ FALSE
                                    */
    BOOL1               bOperP2PStatus;
                                   /* TRUE/ FALSE
                                    */
    UINT1               au1Reserved[1];
#define PeriodicTmr     PeriodicTmrNode.TmrBlk     
}tMrpPortEntry;

typedef struct MrpAppPortEntry {
    tMrpTimer           JoinTmrNode;  
    tMrpTimer           LeaveAllTmrNode; 
    tMrpPortEntry      *pMrpPortEntry;   
                                         /* Pointer to the MrpPortEntry */
    tMrpAppEntry       *pMrpAppEntry;     
                                         /* Back Pointer to the MrpAppEntry*/
    tMacAddr           (*pPeerMacAddrList); 
                                         /* This is a pointer to an array of 
                                          * 6 bytes. This array is indexed 
                                          * by attribute index. An entry in
                                          * this array contains the source 
                                          * Mac Adddress of the peer that 
                                          * caused a change in the register
                                          * state machine for the attribute
                                          * this entry is pointing to. */
    UINT1               u1LeaveAllSemState; 
                                         /* ACTIVE or PASSIVE */
#define JoinTmr         JoinTmrNode.TmrBlk     
#define LeaveAllTmr     LeaveAllTmrNode.TmrBlk
    UINT1               au1Reserved [3];
} tMrpAppPortEntry;

typedef struct MrpMapPortEntry {
    tMrpTimer           LeaveTmrNode;  
    tMrpAttrEntry      *pNextMmrpTxNode;  /* Pointer to the next MMRP attribute
                                           * to be transmitted for this MAP 
                                           * Context on Join Timer expiry. 
                                           * If this is NULL, then the 
                                           * tranmission should start from the 
                                           * first attribute in the 
                                           * MapAttrTable RBTree. */
    tMrpMapEntry       *pMapEntry;        /* Back Pointer to MAP Entry */ 
    UINT1              *pu1AttrInfoList; 
                                     /* For each Attribute Value, the Registrar
                                      * SEM state, the Applicant SEM state and 
                                      * the Registrar Admin Control value are 
                                      * represented by using a single byte.
                                      * The value in the leftmost two bits are 
                                      * used to represent the Registrar Admin 
                                      * Control, value in the next two bits are 
                                      * used to represent the Registrar SEM 
                                      * state and the value in the remaining 
                                      * four bits represent the Applicant SEM
                                      * state.
                                      */
    UINT1             *apLeaveList[MRP_LEAVE_SPLIT_INTERVAL]; 
                                     /* Each Index contains the list of 
                                      * Attribute values for which the leave 
                                      * timer is running for a period of
                                      * (LEAVE Duration /
                                      * MRP_LEAVE_SPLIT_INTERVAL) for that
                                      * instance. If for a particular instance
                                      * say for T0, leave timer is not running
                                      * for any attribute value, then 
                                      * pBitListPtr [0] will be NULL.
                                      */
    UINT2               u2LeavingAttrCnt; /* Number of attributes in LV State. 
                                           * If this variable is not zero, then
                                           * leave timer needs to be started. */
    UINT2               u2Port; /* Local Port Number*/
    UINT2               u2AttrArraySize;
    UINT1               u1PortState; /* BLOCKING/FORWARDING */
    UINT1               u1TcDetectedFlag; /* TRUE/ FALSE */
#define LeaveTmr        LeaveTmrNode.TmrBlk  
} tMrpMapPortEntry;


struct _tMrpMapEntry {
    tMrpMapPortEntry  *apMapPortEntry[MRP_MAX_PORTS_PER_CONTEXT + 1]; 
                                       /* Array of pointers to the Ports 
                                        * belonging to this MAP Context.
                                        */
    tMrpAttrEntry     **ppAttrEntryArray;    
                                       /* Each index points to the attribute 
                                        * value. Used to get the RBTree Node
                                        * containing the attribute value for
                                        * which the Leave Timer has expired. */
    tMrpAppEntry       *pMrpAppEntry;  /* back Pointer to the MrpAppEntry */
    UINT2               u2NextFreeAttrIndex;
    UINT2               u2MapId; /* MAP Context Identifier */
    UINT2               u2AttrArraySize; 
    UINT2               u2AttrCount; /* This Variable is used to store
                                            * the number of attributes present 
                                            * in the last block of the array.*/
};

struct _tMrpAppEntry {
    tRBTree             MapAttrTable;  
                                       /* RBTree Head to tMrpAttrEntry. This 
                                        * RBtree contains all the attribute
                                        * values for a particular MAP Context.
                                        * Attribute Type and Value are the 
                                        * indices of this RBTree.
                                        */
    tMrpMapEntry      **ppMapTable;
                                        /* Array of pointers to MAP Entries for
                                         * this application. */
    tMrpAppPortEntry  *apAppPortEntry[MRP_MAX_PORTS_PER_CONTEXT + 1]; 
                                         /* Contains per Application per port
                                          * specific Information. */
    tMrpContextInfo    *pMrpContextInfo; 
                                         /* Back Pointer to Context structure */
    UINT1              *pu1MmrpAttrIndexList;
                                         /* Pointer to a byteList of Max MMRP 
                                          * attributes in system used to store
                                          * the attribute index of MMRP
                                          * attributes alone. */
    UINT2               u2NextFreeAttrIndex;
    UINT2               u2AttrArraySize;
    UINT2               u2MapArraySize;   
    UINT1               u1AppId;         
                                         /* Application Id */
    UINT1               u1Reserved;
};


struct _tMrpContextInfo { 
    tMrpPortEntry     *apMrpPortEntry [MRP_MAX_PORTS_PER_CONTEXT + 1]; 
                                      /* Array of pointers indexed by 
                                       * Local-port number. This will be 
                                       * used in the core code for instant  
                                       * access of tMrpPortEntry structure. */
    tMrpAppEntry        aMrpAppTable [MRP_MAX_APPS + 1]; 
                                      /* Application
                                       * specific information
                                       */
    UINT4               u4ContextId;  
    UINT4               u4BridgeMode;
    UINT4               u4TrcOption; 
    UINT2               u2NextFreePeerMACIndex;
    UINT2               u2NextContextId;
    UINT2               u2PrevContextId;
    UINT2               u2FirstPortId;
                                      /* Trace Option */
    UINT1               au1TrcInput [MRP_TRC_MAX_SIZE];
    tMacAddr            MvrpAddr;     
                                      /* This will be filled in the basis of
                                       * bridge mode. In case of customer
                                       * bridges it will be 01:80:C2:00:00:21.
                                       * For 802.1ad & 802.1ah bridges it will
                                       * be 01:80:C2:00:00:0D.
                                       */
    UINT1               u1MvrpTrapEnabled; 
                                      /* TRUE/FALSE */
    UINT1               u1MmrpTrapEnabled;
                                      /* TRUE/FALSE */
    UINT1               au1ContextName [MRP_CONTEXT_NAME_LEN];
    UINT1               u1MvrpAdminStatus;
                                      /* MRP_ENABLED/ MRP_DISABLED */
    UINT1               u1MmrpAdminStatus;
                                      /* MRP_ENABLED/ MRP_DISABLED */
    UINT1               au1Pad[1];
 };

/* Structure used for passing the information across the functions */
typedef struct MrpIfMsg {
    tMrpAppEntry       *pAppEntry;
    UINT4               u4ContextId;
    tMacAddr            SrcMacAddr; /* Source MAC Address of the MRPDU */
    UINT2               u2MapId;
    UINT2               u2Port;
    UINT2               u2PktLen; /* MRPDU Length */
} tMrpIfMsg;

typedef struct {
    tCRU_BUF_CHAIN_HEADER  *pMrpPdu; /* Received MRPDU */
    UINT4                   u4ContextId; /* Virtual Context Identifier */
    UINT2                   u2Port; /* Local Port Number */
    UINT2                   u2MapId; /* MAP Context Identifier */
} tMrpRxPduQMsg;

typedef struct {
    tMrpMapPortEntry      *pMapPortEntry;
    tMrpAttr              *pAttr;
    UINT2                  u2LastVlanId;/* Last VLAN ID whose Attribute event 
                                         * was filled in the current Vector
                                         * Attribute
                                         */
    UINT2                  u2AttrIndex; /* Index of the attribute */
    tMacAddr               LastMacAddr; /* Last MAC Address whose Attribute 
                                         * event was filled in the current 
                                         * Vector Attribute
                                         */
    UINT1                  u1Count; /* Indicates the number of Attribute events
                                      * that has to be encoded for optimal
                                      * encoding
                                      */
    UINT1                  u1Event; /* tx! or txLA!*/
    UINT1                  au1AttrEvent[MRP_OPTIMAL_ENCODE_INTERVAL];
                                          /* List of Attribute events that has
                                           * to be encoded in the current Vector 
                                           * Attribute for optimal encoding
                                           */
    UINT1                  u1IsFirstValSet; /* Indicates whether the First 
                                             * Value has been set for the
                                             * current Vector Attribute
                                             */
    UINT1                  u1IsAttrFilled;  /* Indicates whether any attribute
          * is filled in the PDU.
          */
    UINT1                  u1OptEncode; /* Indicates whether the current 
                                         * attribute value has to be encoded
                                         * for optimal encoding or not.
                                         */
    UINT1                  au1Reserved[2];
} tMrpVectAttrInfo;


typedef struct MrpRedInfo{
    tRmMsg       *pRedPduSyncMsg; /* Used for synchronizing the VLAN and MAC
                                   * information that have been learnt from
                                   * MRPDU.
                                   */
    UINT4        u4Offset; /* Current location of the RM Message buffer */
    UINT4        u4RelinqInterval; /* The interval at which bulk update
                                    * req. handling needs to be suspended
                                    */
    UINT4        u4NextRelinqTime; /* Time at which bulk update
                                           * req. handling needs to be
                                           * suspended and other pending
                                           * events need to be processed
                                           */

    UINT2        u2NumOfAttr;/* Number of Attribute values filled in the RM
                              * message.
                              */
    UINT1         u1StandbyNodeCnt; /* Indicates number of standby nodes that 
                                     * are up.
                                     */
    UINT1         u1NodeState;     /* Current Node state(RM_INIT/ RM_ACTIVE/ 
                                    * RM_STANDBY) */
    UINT1         u1PrevNodeState; /* Previous Node state */
    BOOL1         bBulkUpdReqRcvd; /* This flag will be set to OSIX_TRUE if bulk
                                    * request is received before getting the RM
                                    * STANDBY_UP event.
                                    */
    UINT1         u1IsFirstAttrFilled; /* Set to OSIX_TRUE after thre first 
                                        * Attribute in MRPDU is filled in the
                                        * synchronization message.
                                        */
    UINT1         u1Reserved;
}tMrpRedInfo;

/* ------------------------------------------------------------------
 *                Global Information 
 * This structure contains all the Global Data required for MRP 
 * Operation .
 * ----------------------------------------------------------------- */

typedef struct MrpGlobalInfo {
    tOsixTaskId         TaskId; /* MRP Task Id */
    tOsixQId            RxPktQId; /* Queue used for packet reception */
    tOsixQId            MsgQId; /* Queue used for messages handling */
    tOsixSemId          SemId;  /* MRP Semaphore Id */
    tMemPoolId          QMsgPoolId;
    tMemPoolId          QBulkMsgPoolId; 
    tMemPoolId          ContextPoolId; /* Memory Pool Id for ContextInfo */
    tMemPoolId          PortPoolId;
    tMemPoolId          PeerMACAddrPoolId;
    tMemPoolId          AppPortPoolId;
    tMemPoolId          StatisticsPoolId;
    tMemPoolId          MapPoolId;
    tMemPoolId          AttrPoolId;
                                      /* Identifier for the memory pool
                                       * that is allocated for the
                                       * attr RB tree inside the Map Entry */
    tMemPoolId          MapPortPoolId;
    tMemPoolId          MrpPduPoolId; 
    tMemPoolId          VlanInstMapPoolId; 
    tMemPoolId          PduQMsgPoolId; 
    tMemPoolId          MapMvrpAttrArrayPoolId; 
                                          /* Identifier of the memory 
                                          * for storing the address of each
                                          * attribute value in the AttrTable.*/
    tMemPoolId          MapMmrpAttrArrayPoolId; 
                                          /* Identifier of the  memory 
                                           * for storing the address of each
                                           * attribute value in the AttrTable.*/
    tMemPoolId          MvrpAttrInfoListPoolId; 
                                          /* Identifier of the memory 
                                           * for MVRP AttrInfoList. */
    tMemPoolId          MmrpAttrInfoListPoolId; 
                                          /* Identifier of the memory 
                                           * for MVRP AttrInfoList. */
    tMemPoolId          MvrpAppPortPeerMacPoolId;
                                          /* Identifier of the memory
                                              * for MVRP PeerMacAddrList */
    tMemPoolId          MmrpAppPortPeerMacPoolId;
                                          /* Identifier of the memory
                                              * for MMRP PeerMacAddrList */
    tMemPoolId          MvrpLvBitListPoolId;  
                                          /* Identifier of the  memory for
                                           * MVRP LvBitList. */
    tMemPoolId          MmrpLvBitListPoolId;  
                                          /* Identifier of the  memory for
                                           * MMRP LvBitList. */

    tMemPoolId          MvrpPortAttrListPoolId;
                                          /* Identifier of the memory
                                              * for AttrBitList present in 
                                              * the MrpPortEntry. */
    tMemPoolId          MrpMvrpMapPoolId; 
    tMemPoolId          MrpMmrpMapPoolId; 
    tMemPoolId          AppMmrpAttrIndxPoolId;
                                         /* Identifier of the memory for MMRP
                                          * Attribute index list in AppEntry */
    tRBTree             MrpGlobalPortTable;
                                          /* IfIndex based RB Tree Head for 
                                             * tMrpPortEntry. This RB Tree 
                                             * is maintained accross 
                                             * contexts which is used 
                                             * in the MRP APIs for getting 
                                             * the ContextId & Local-Port 
                                             * number for the given IfIndex.
                                             */
    tMrpContextInfo    *apContextInfo [SYS_DEF_MAX_NUM_CONTEXTS + 1]; 
                                            /* This array contains per virtual 
                                             * context specific information. 
                                             */
    tMrpAppnFn          aMrpAppnFn [MRP_MAX_APPS + 1];
                                            /* Appln. specific 
                                             * function pointers */
    tTmrDesc            aTmrDesc[MRP_MAX_TMR_TYPES];
                                     /* Timer data structure that contains 
                                      * function ptrs for timer handling and
                                      * offsets to identify the data structure
                                      * containing timer block.Timer ID is the
                                      * index to this data structure.
                                      */
    tTimerListId        MrpTmrListId; /* Timer List Id */
    tMrpRedInfo         MrpRedInfo;
    tMrpQMsg           *apMrpRemapQMsg [SYS_DEF_MAX_NUM_CONTEXTS + 1];
                                        /* This array contains RemapQMsg
                                         * related informations. */

    UINT1               *pu1PduBuf;
                                    /* Buffer used for forming the PDU
                                     * when REMAP got triggered as well
                                     * as on Tx Opportunity*/
    UINT2               u2FirstContextId;
    UINT1               u1MrpIsInitComplete;
    UINT1               GlobalTrcOption;
    UINT1               au1MrpSystemCtrl [SYS_DEF_MAX_NUM_CONTEXTS + 1];
    UINT1               au1MvrpStatus [SYS_DEF_MAX_NUM_CONTEXTS + 1]; /*Enable/Disable*/
    UINT1               au1MmrpStatus [SYS_DEF_MAX_NUM_CONTEXTS + 1]; /*Enable/Disable*/ 
    UINT1               au1Pad[(4 - (((SYS_DEF_MAX_NUM_CONTEXTS + 1) * 3) % 4))];
} tMrpGlobalInfo;


#endif /* _MRP_TDFS_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrptdfs.h                      */
/*-----------------------------------------------------------------------*/

