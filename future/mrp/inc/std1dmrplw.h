/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1dmrplw.h,v 1.1 2011/11/28 12:07:22 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/


/* Proto Validate Index Instance for Ieee8021BridgePortMrpTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgePortMrpTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgePortMrpTable  */

INT1
nmhGetFirstIndexIeee8021BridgePortMrpTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgePortMrpTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgePortMrpJoinTime ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgePortMrpLeaveTime ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgePortMrpLeaveAllTime ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgePortMrpJoinTime ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgePortMrpLeaveTime ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgePortMrpLeaveAllTime ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgePortMrpJoinTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgePortMrpLeaveTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgePortMrpLeaveAllTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgePortMrpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021BridgePortMmrpTable. */
INT1
nmhValidateIndexInstanceIeee8021BridgePortMmrpTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021BridgePortMmrpTable  */

INT1
nmhGetFirstIndexIeee8021BridgePortMmrpTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021BridgePortMmrpTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021BridgePortMmrpEnabledStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021BridgePortMmrpFailedRegistrations ARG_LIST((UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetIeee8021BridgePortMmrpLastPduOrigin ARG_LIST((UINT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetIeee8021BridgePortRestrictedGroupRegistration ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021BridgePortMmrpEnabledStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021BridgePortRestrictedGroupRegistration ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021BridgePortMmrpEnabledStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021BridgePortRestrictedGroupRegistration ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021BridgePortMmrpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
