/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpred.h,v 1.2 2009/08/31 09:43:05 prabuc Exp $
 *
 * Description: This file contains the definitions, data structures
 *              and prototypes to support High Availability for MRP module.   
 *
 ************************************************************************/

/* Message types */
enum {
    MRP_RM_BULK_UPDT_REQ_MSG = RM_BULK_UPDT_REQ_MSG, /* 1 */
    MRP_RM_BULK_UPDATE_MSG = RM_BULK_UPDATE_MSG, /* 2 */
    MRP_RM_BULK_UPDT_TAIL_MSG = RM_BULK_UPDT_TAIL_MSG, /* 3 */ 
    MRP_RED_VLAN_ADD_MSG = 4,
    MRP_RED_VLAN_DEL_MSG = 5,
    MRP_RED_MAC_ADD_MSG = 6,
    MRP_RED_MAC_DEL_MSG = 7,
    MRP_RED_SERVICE_REQ_ADD_MSG = 8,
    MRP_RED_SERVICE_REQ_DEL_MSG = 9,
    MRP_RED_PORT_OPER_STATUS_MSG = 10,
    MRP_RED_LV_ALL_TMR_RESTART_MSG = 11,
    MRP_RED_MVRP_LV_ALL_TMR_MSG = 12,
    MRP_RED_MMRP_LV_ALL_TMR_MSG = 13,
    MRP_RED_INVALID_MSG = 12
};

#define MRP_RED_INVALID_MSG_LEN 0
#define MRP_RED_NODE_STATUS() gMrpGlobalInfo.MrpRedInfo.u1NodeState
#define MRP_RED_PREV_NODE_STATUS() gMrpGlobalInfo.MrpRedInfo.u1PrevNodeState

/* Message Length */
#define MRP_RED_BULK_UPD_REQ_MSG_LEN       MRP_RED_MSG_HDR_SIZE

/* sizeof (u4ContextId) + sizeof(u2LocalPortId) + sizeof (u1OperStatus) */ 
#define MRP_RED_OPER_STATUS_CHG_MSG_LEN       7

/* sizeof (u4ContextId) + sizeof(u2LocalPortId) + sizeof (u2NumOfVlans) + 
 * sizeof (VlanId)
 */ 
#define MRP_RED_VLAN_DEL_MSG_LEN 10

/* sizeof (u4ContextId) + sizeof(u2LocalPortId) + sizeof (u2MapId) + 
 * sizeof (u2NumOfMACAddress) + sizeof (MACAddress)
 */ 
#define MRP_RED_MAC_DEL_MSG_LEN 16

/* sizeof (u4ContextId) + sizeof(u2LocalPortId) + sizeof (u2MapId) + 
 * sizeof (u1ServiceReqAttrValue)
 */ 
#define MRP_RED_SERVICE_REQ_ADD_MSG_LEN 9

/* sizeof (u4ContextId) + sizeof(u2LocalPortId) + sizeof (u2MapId) + 
 * sizeof (u1ServiceReqAttrValue)
 */ 
#define MRP_RED_SERVICE_REQ_DEL_MSG_LEN 9

/* sizeof (u4ContextId) + sizeof(u2LocalPortId) + sizeof (u1AppId)
 */ 
#define MRP_RED_LVALL_TMR_RESTART_MSG_LEN 7

/* sizeof (u4ContextId) + sizeof(u2PortCnt) + sizeof(u2LocalPortId) */
#define MRP_RED_MIN_PORT_STAT_BLKMSG_LEN 8
/* sizeof (u4ContextId) + sizeof(u2PortCnt) + sizeof(u2LocalPortId) +
 * sizeof (u4RemTimerDuration) */
#define MRP_RED_MIN_LVALL_TMR_BLKMSG_LEN 12
/* sizeof (u4ContextId) + sizeof(u2PortCnt) + sizeof(u2LocalPortId) +
 * sizeof (u2VlanCnt) + sizeof(vlanid) */
#define MRP_RED_MIN_VLAN_BLKMSG_LEN 12
/* sizeof (u4ContextId) + sizeof(u2PortCnt) + sizeof(u2LocalPortId) +
 * sizeof (u2MacCnt) + sizeof (macadd) */
#define MRP_RED_MIN_MAC_BLKMSG_LEN 18
/* sizeof (u4ContextId) + sizeof(u2PortCnt) + sizeof(u2LocalPortId) +
 * sizeof (u2SerReqCnt) + sizeof (u1SerReqVal) */
#define MRP_RED_MIN_SER_REQ_BLKMSG_LEN 10

#define MRP_RED_MVRP_ATTR_VAL_CNT_OFFSET 9
#define MRP_RED_MMRP_ATTR_VAL_CNT_OFFSET 11
#define MRP_RED_GET_ATTR_VAL_CNT_OFFSET(u1MsgType)\
    ((u1MsgType == MRP_RED_VLAN_ADD_MSG) ? MRP_RED_MVRP_ATTR_VAL_CNT_OFFSET :\
     MRP_RED_MMRP_ATTR_VAL_CNT_OFFSET)

#define MRP_BULK_UPD_NOT_STARTED 0
#define MRP_BULK_UPD_COMPLETED   1
#define MRP_BULK_UPD_IN_PROGRESS 2

#define MRP_RED_MSG_TYPE_SIZE 1
#define MRP_RED_MSG_LEN_SIZE  2
#define MRP_RED_BULK_UPDT_MSG_TYPE_SIZE 1


#define MRP_RED_MSG_HDR_SIZE       (MRP_RED_MSG_TYPE_SIZE +\
                                    MRP_RED_MSG_LEN_SIZE)

#define MRP_RED_MIN_BUFF_SPACE_REQ(u1AppId) \
    ((u1AppId == MRP_MVRP_APP_ID) ? MRP_VLAN_ID_LEN : MRP_MAC_ADDR_LEN)

#define MRP_RED_MAX_MSG_SIZE 1500
#define MRP_RED_BULK_UPD_MSG_HDR_SIZE     (MRP_RED_MSG_HDR_SIZE + \
                                            MRP_RED_BULK_UPDT_MSG_TYPE_SIZE)

#define MRP_RED_PUT_1_BYTE(pMsg, u4Offset, u1MesgType) \
{\
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1MesgType); \
    u4Offset += 1; \
}

#define MRP_RED_PUT_2_BYTES(pMsg, u4Offset, u2MesgType) \
{\
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2MesgType); \
    u4Offset += 2; \
}

#define MRP_RED_PUT_4_BYTES(pMsg, u4Offset, u4MesgType) \
{\
    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, u4MesgType); \
    u4Offset += 4; \
}

#define MRP_RED_PUT_N_BYTES(pdest, psrc, u4Offset, u4Size) \
{\
    RM_COPY_TO_OFFSET(pdest, psrc, u4Offset, u4Size); \
    u4Offset +=u4Size; \
}

#define MRP_RED_GET_1_BYTE(pMsg, u4Offset, u1MesgType) \
{\
    RM_GET_DATA_1_BYTE (pMsg, u4Offset, u1MesgType); \
    u4Offset += 1; \
}

#define MRP_RED_GET_2_BYTES(pMsg, u4Offset, u2MesgType) \
{\
    RM_GET_DATA_2_BYTE (pMsg, u4Offset, u2MesgType); \
    u4Offset += 2; \
}

#define MRP_RED_GET_4_BYTES(pMsg, u4Offset, u4MesgType) \
{\
    RM_GET_DATA_4_BYTE (pMsg, u4Offset, u4MesgType); \
    u4Offset += 4; \
}

#define MRP_RED_GET_N_BYTES(psrc, pdest, u4Offset, u4Size) \
{\
    RM_GET_DATA_N_BYTE (psrc, pdest, u4Offset, u4Size); \
    u4Offset +=u4Size; \
}

#define MRP_CRU_BUF_MOVE_VALID_OFFSET(pBuf, Len) \
{ \
    CRU_BUF_Move_ValidOffset (pBuf, Len);\
}

#ifdef RM_WANTED
#define  MRP_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? OSIX_TRUE:\
         OSIX_FALSE)
#else
#define  MRP_IS_NP_PROGRAMMING_ALLOWED() OSIX_TRUE
#endif

/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpred.h                       */
/*-----------------------------------------------------------------------*/

