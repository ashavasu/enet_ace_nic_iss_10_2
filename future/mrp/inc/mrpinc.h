/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpinc.h,v 1.7 2012/03/21 13:05:18 siva Exp $
 *
 * Description: This file contains header files included in MRP module. 
 *
 *****************************************************************************/
#ifndef _MRP_INC_H
#define _MRP_INC_H

#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "bridge.h"
#include "l2iwf.h"
#include "fsutil.h"
#include "fsvlan.h"
#include "la.h"
#include "snp.h"
#include "garp.h"

#include "mstp.h"
#include "rstp.h"
#include "pbbte.h"
#include "vcm.h"
#include "fm.h"
#include "iss.h"
#include "mrpconst.h"
#include "mrpmacs.h"
#include "mrp.h"
#include "mrptdfs.h"
#ifdef _MRPMAIN_C_
#include "mrpglob.h"
#else
#include "mrpextn.h"
#endif
#include "mrpprot.h"
#include "mrptrc.h"
#include "fsbuddy.h"
#include "rmgr.h"
#ifdef L2RED_WANTED
#include "mrpred.h"
#else
#include "mrprdstb.h"
#endif

#include "mrpsz.h"
#include "fsmrplw.h"
#include "std1dmrplw.h"
#include "fsmrpwr.h"
#include "std1dmrpwr.h"
#include "mrpcli.h"
#include "utlbit.h"
#include "pvrst.h"
#ifdef NPAPI_WANTED
#include "npapi.h"
#include "mrpnp.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#include "mrpmbsm.h"
#endif
#endif
#endif /*_MRP_INC_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpinc.h                        */
/*-----------------------------------------------------------------------*/
