/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: fsmrpwr.h,v 1.5 2011/11/28 12:06:00 siva Exp $
 *
 *******************************************************************/

#ifndef _FSMRPWR_H
#define _FSMRPWR_H

VOID RegisterFSMRP(VOID);

VOID UnRegisterFSMRP(VOID);
INT4 FsMrpGlobalTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpGlobalTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpGlobalTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpGlobalTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMrpInstanceTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMrpInstanceSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceTraceInputStringGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceNotifyVlanRegFailureGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceNotifyMacRegFailureGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceBridgeMmrpEnabledStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceBridgeMvrpEnabledStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceTraceInputStringSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceNotifyVlanRegFailureSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceNotifyMacRegFailureSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceBridgeMmrpEnabledStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceBridgeMvrpEnabledStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceTraceInputStringTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceNotifyVlanRegFailureTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceNotifyMacRegFailureTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceBridgeMmrpEnabledStatusTest
                      (UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceBridgeMvrpEnabledStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpInstanceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMrpPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMrpPortPeriodicSEMStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortParticipantTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortRegAdminControlGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortRestrictedGroupRegistrationGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortRestrictedVlanRegistrationGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortPeriodicSEMStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortParticipantTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortRegAdminControlSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortRestrictedGroupRegistrationSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortRestrictedVlanRegistrationSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortPeriodicSEMStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpPortParticipantTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpPortRegAdminControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpPortRestrictedGroupRegistrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpPortRestrictedVlanRegistrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMvrpPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMvrpPortMvrpEnabledStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMvrpPortMvrpFailedRegistrationsGet(tSnmpIndex *, tRetVal *);
INT4 FsMvrpPortMvrpLastPduOriginGet(tSnmpIndex *, tRetVal *);
INT4 FsMvrpPortMvrpEnabledStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMvrpPortMvrpEnabledStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMvrpPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMrpApplicantControlTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMrpApplicantControlAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpApplicantControlAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpApplicantControlAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpApplicantControlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMrpPortStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMrpPortStatsClearStatisticsGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsNumberOfRegistrationsGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsRxValidPduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsRxInvalidPduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsRxNewMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsRxJoinInMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsRxJoinMtMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsRxLeaveMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsRxEmptyMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsRxInMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsRxLeaveAllMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsTxPduCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsTxNewMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsTxJoinInMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsTxJoinMtMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsTxLeaveMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsTxEmptyMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsTxInMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsTxLeaveAllMsgCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsClearStatisticsSet(tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsClearStatisticsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMrpPortStatsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMrpSEMTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMrpSEMApplicantStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpSEMRegistrarStateGet(tSnmpIndex *, tRetVal *);
INT4 FsMrpSEMOriginatorAddressGet(tSnmpIndex *, tRetVal *);
#endif /* _FSMRPWR_H */
