/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: std1dmrpwr.h,v 1.1 2011/11/28 12:07:24 siva Exp $
 *
 *******************************************************************/

#ifndef _STD1D1WR_H
#define _STD1D1WR_H

VOID RegisterSTD1DMRP(VOID);

VOID UnRegisterSTD1DMRP(VOID);
INT4 GetNextIndexIeee8021BridgePortMrpTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgePortMrpJoinTimeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveTimeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveAllTimeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpJoinTimeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveTimeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveAllTimeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpJoinTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveAllTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgePortMmrpTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgePortMmrpEnabledStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMmrpFailedRegistrationsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMmrpLastPduOriginGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortRestrictedGroupRegistrationGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMmrpEnabledStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortRestrictedGroupRegistrationSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMmrpEnabledStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortRestrictedGroupRegistrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMmrpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STD1D1WR_H */
