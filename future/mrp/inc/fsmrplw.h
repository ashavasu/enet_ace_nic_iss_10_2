/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmrplw.h,v 1.5 2011/11/28 12:06:00 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMrpGlobalTraceOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMrpGlobalTraceOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMrpGlobalTraceOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMrpGlobalTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMrpInstanceTable. */
INT1
nmhValidateIndexInstanceFsMrpInstanceTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMrpInstanceTable  */

INT1
nmhGetFirstIndexFsMrpInstanceTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMrpInstanceTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMrpInstanceSystemControl ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMrpInstanceTraceInputString ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMrpInstanceNotifyVlanRegFailure ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMrpInstanceNotifyMacRegFailure ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMrpInstanceBridgeMmrpEnabledStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMrpInstanceBridgeMvrpEnabledStatus ARG_LIST((UINT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMrpInstanceSystemControl ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMrpInstanceTraceInputString ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMrpInstanceNotifyVlanRegFailure ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMrpInstanceNotifyMacRegFailure ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMrpInstanceBridgeMmrpEnabledStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMrpInstanceBridgeMvrpEnabledStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMrpInstanceSystemControl ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMrpInstanceTraceInputString ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMrpInstanceNotifyVlanRegFailure ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMrpInstanceNotifyMacRegFailure ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMrpInstanceBridgeMmrpEnabledStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsMrpInstanceBridgeMvrpEnabledStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMrpInstanceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMrpPortTable. */
INT1
nmhValidateIndexInstanceFsMrpPortTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMrpPortTable  */

INT1
nmhGetFirstIndexFsMrpPortTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMrpPortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMrpPortPeriodicSEMStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMrpPortParticipantType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMrpPortRegAdminControl ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMrpPortRestrictedGroupRegistration ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMrpPortRestrictedVlanRegistration ARG_LIST((UINT4  , UINT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMrpPortPeriodicSEMStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMrpPortParticipantType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMrpPortRegAdminControl ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMrpPortRestrictedGroupRegistration ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMrpPortRestrictedVlanRegistration ARG_LIST((UINT4  , UINT4  ,INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMrpPortPeriodicSEMStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMrpPortParticipantType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMrpPortRegAdminControl ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMrpPortRestrictedGroupRegistration ARG_LIST((UINT4 *  ,UINT4  , 
              UINT4,INT4 ));
INT1
nmhTestv2FsMrpPortRestrictedVlanRegistration ARG_LIST((UINT4 *  ,UINT4  , UINT4
 ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMrpPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FsMvrpPortTable. */
INT1
nmhValidateIndexInstanceFsMvrpPortTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMvrpPortTable  */

INT1
nmhGetFirstIndexFsMvrpPortTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMvrpPortTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMvrpPortMvrpEnabledStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMvrpPortMvrpFailedRegistrations ARG_LIST((UINT4  , UINT4 ,
                                          tSNMP_COUNTER64_TYPE *));
INT1
nmhGetFsMvrpPortMvrpLastPduOrigin ARG_LIST((UINT4  , UINT4 ,tMacAddr * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMvrpPortMvrpEnabledStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMvrpPortMvrpEnabledStatus ARG_LIST
        ((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMvrpPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMrpApplicantControlTable. */
INT1
nmhValidateIndexInstanceFsMrpApplicantControlTable ARG_LIST((UINT4  , UINT4  , tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMrpApplicantControlTable  */

INT1
nmhGetFirstIndexFsMrpApplicantControlTable ARG_LIST((UINT4 * , UINT4 * , tMacAddr *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMrpApplicantControlTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMrpApplicantControlAdminStatus ARG_LIST((UINT4  , UINT4  , tMacAddr  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMrpApplicantControlAdminStatus ARG_LIST((UINT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMrpApplicantControlAdminStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMrpApplicantControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMrpPortStatsTable. */
INT1
nmhValidateIndexInstanceFsMrpPortStatsTable ARG_LIST((UINT4  , UINT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsMrpPortStatsTable  */

INT1
nmhGetFirstIndexFsMrpPortStatsTable ARG_LIST((UINT4 * , UINT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMrpPortStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMrpPortStatsClearStatistics ARG_LIST((UINT4  , UINT4  , tMacAddr ,INT4 *));

INT1
nmhGetFsMrpPortStatsNumberOfRegistrations ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsRxValidPduCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsRxInvalidPduCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsRxNewMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsRxJoinInMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsRxJoinMtMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsRxLeaveMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsRxEmptyMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsRxInMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsRxLeaveAllMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsTxPduCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsTxNewMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsTxJoinInMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsTxJoinMtMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsTxLeaveMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsTxEmptyMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsTxInMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsMrpPortStatsTxLeaveAllMsgCount ARG_LIST((UINT4  , UINT4  , tMacAddr ,tSNMP_COUNTER64_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMrpPortStatsClearStatistics ARG_LIST((UINT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMrpPortStatsClearStatistics ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMrpPortStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMrpSEMTable. */
INT1
nmhValidateIndexInstanceFsMrpSEMTable ARG_LIST((UINT4  , UINT4  , tMacAddr  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMrpSEMTable  */

INT1
nmhGetFirstIndexFsMrpSEMTable ARG_LIST((UINT4 * , UINT4 * , tMacAddr *  , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMrpSEMTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMrpSEMApplicantState ARG_LIST((UINT4  , UINT4  , tMacAddr  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMrpSEMRegistrarState ARG_LIST((UINT4  , UINT4  , tMacAddr  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMrpSEMOriginatorAddress ARG_LIST((UINT4  , UINT4  , tMacAddr  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */
