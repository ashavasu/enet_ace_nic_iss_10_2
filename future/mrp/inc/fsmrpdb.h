/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmrpdb.h,v 1.7 2011/11/28 12:06:00 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMRPDB_H
#define _FSMRPDB_H

UINT1 FsMrpInstanceTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMrpPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMvrpPortTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,
    SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMrpApplicantControlTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMrpPortStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 FsMrpSEMTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsmrp [] ={1,3,6,1,4,1,29601,2,27};
tSNMP_OID_TYPE fsmrpOID = {9, fsmrp};


UINT4 FsMrpGlobalTraceOption [ ] ={1,3,6,1,4,1,29601,2,27,1,1};
UINT4 FsMrpInstanceSystemControl [ ] ={1,3,6,1,4,1,29601,2,27,2,1,1,1};
UINT4 FsMrpInstanceTraceInputString [ ] ={1,3,6,1,4,1,29601,2,27,2,1,1,2};
UINT4 FsMrpInstanceNotifyVlanRegFailure [ ] ={1,3,6,1,4,1,29601,2,27,2,1,1,3};
UINT4 FsMrpInstanceNotifyMacRegFailure [ ] ={1,3,6,1,4,1,29601,2,27,2,1,1,4};
UINT4 FsMrpInstanceBridgeMmrpEnabledStatus [ ] =
                               {1,3,6,1,4,1,29601,2,27,2,1,1,5};
UINT4 FsMrpInstanceBridgeMvrpEnabledStatus [ ] =
                               {1,3,6,1,4,1,29601,2,27,2,1,1,6};
UINT4 FsMrpPortPeriodicSEMStatus [ ] ={1,3,6,1,4,1,29601,2,27,3,1,1,1};
UINT4 FsMrpPortParticipantType [ ] ={1,3,6,1,4,1,29601,2,27,3,1,1,2};
UINT4 FsMrpPortRegAdminControl [ ] ={1,3,6,1,4,1,29601,2,27,3,1,1,3};
UINT4 FsMrpPortRestrictedGroupRegistration [ ] =
        {1,3,6,1,4,1,29601,2,27,3,1,1,4};
UINT4 FsMrpPortRestrictedVlanRegistration [ ] ={1,3,6,1,4,1,29601,2,27,3,1,1,5};
UINT4 FsMvrpPortMvrpEnabledStatus [ ] ={1,3,6,1,4,1,29601,2,27,3,2,1,1};
UINT4 FsMvrpPortMvrpFailedRegistrations [ ] ={1,3,6,1,4,1,29601,2,27,3,2,1,2};
UINT4 FsMvrpPortMvrpLastPduOrigin [ ] ={1,3,6,1,4,1,29601,2,27,3,2,1,3};
UINT4 FsMrpApplicationAddress [ ] ={1,3,6,1,4,1,29601,2,27,5,1,1,1};
UINT4 FsMrpAttributeType [ ] ={1,3,6,1,4,1,29601,2,27,5,1,1,2};
UINT4 FsMrpApplicantControlAdminStatus [ ] ={1,3,6,1,4,1,29601,2,27,5,1,1,3};
UINT4 FsMrpPortStatsClearStatistics [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,1};
UINT4 FsMrpPortStatsNumberOfRegistrations [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,2};
UINT4 FsMrpPortStatsRxValidPduCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,3};
UINT4 FsMrpPortStatsRxInvalidPduCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,4};
UINT4 FsMrpPortStatsRxNewMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,5};
UINT4 FsMrpPortStatsRxJoinInMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,6};
UINT4 FsMrpPortStatsRxJoinMtMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,7};
UINT4 FsMrpPortStatsRxLeaveMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,8};
UINT4 FsMrpPortStatsRxEmptyMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,9};
UINT4 FsMrpPortStatsRxInMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,10};
UINT4 FsMrpPortStatsRxLeaveAllMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,11};
UINT4 FsMrpPortStatsTxPduCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,12};
UINT4 FsMrpPortStatsTxNewMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,13};
UINT4 FsMrpPortStatsTxJoinInMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,14};
UINT4 FsMrpPortStatsTxJoinMtMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,15};
UINT4 FsMrpPortStatsTxLeaveMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,16};
UINT4 FsMrpPortStatsTxEmptyMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,17};
UINT4 FsMrpPortStatsTxInMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,18};
UINT4 FsMrpPortStatsTxLeaveAllMsgCount [ ] ={1,3,6,1,4,1,29601,2,27,4,1,1,19};
UINT4 FsMrpSEMMapContext [ ] ={1,3,6,1,4,1,29601,2,27,6,1,1,1};
UINT4 FsMrpSEMAttributeValue [ ] ={1,3,6,1,4,1,29601,2,27,6,1,1,2};
UINT4 FsMrpSEMApplicantState [ ] ={1,3,6,1,4,1,29601,2,27,6,1,1,3};
UINT4 FsMrpSEMRegistrarState [ ] ={1,3,6,1,4,1,29601,2,27,6,1,1,4};
UINT4 FsMrpSEMOriginatorAddress [ ] ={1,3,6,1,4,1,29601,2,27,6,1,1,5};
UINT4 FsMrpTrapContextName [ ] ={1,3,6,1,4,1,29601,2,27,7,1};
UINT4 FsMrpTrapBridgeBasePort [ ] ={1,3,6,1,4,1,29601,2,27,7,2};
UINT4 FsMrpTrapMvrpAttributeValue [ ] ={1,3,6,1,4,1,29601,2,27,7,3};
UINT4 FsMrpTrapMmrpAttributeValue [ ] ={1,3,6,1,4,1,29601,2,27,7,4};
UINT4 FsMrpTrapAttrRegFailureReason [ ] ={1,3,6,1,4,1,29601,2,27,7,5};




tMbDbEntry fsmrpMibEntry[]= {

{{11,FsMrpGlobalTraceOption}, NULL, FsMrpGlobalTraceOptionGet, FsMrpGlobalTraceOptionSet, FsMrpGlobalTraceOptionTest, FsMrpGlobalTraceOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsMrpInstanceSystemControl}, GetNextIndexFsMrpInstanceTable, FsMrpInstanceSystemControlGet, FsMrpInstanceSystemControlSet, FsMrpInstanceSystemControlTest, FsMrpInstanceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpInstanceTableINDEX, 1, 0, 0, "2"},

{{13,FsMrpInstanceTraceInputString}, GetNextIndexFsMrpInstanceTable, FsMrpInstanceTraceInputStringGet, FsMrpInstanceTraceInputStringSet, FsMrpInstanceTraceInputStringTest, FsMrpInstanceTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMrpInstanceTableINDEX, 1, 0, 0, NULL},

{{13,FsMrpInstanceNotifyVlanRegFailure}, GetNextIndexFsMrpInstanceTable, FsMrpInstanceNotifyVlanRegFailureGet, FsMrpInstanceNotifyVlanRegFailureSet, FsMrpInstanceNotifyVlanRegFailureTest, FsMrpInstanceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpInstanceTableINDEX, 1, 0, 0, "2"},

{{13,FsMrpInstanceNotifyMacRegFailure}, GetNextIndexFsMrpInstanceTable, FsMrpInstanceNotifyMacRegFailureGet, FsMrpInstanceNotifyMacRegFailureSet, FsMrpInstanceNotifyMacRegFailureTest, FsMrpInstanceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpInstanceTableINDEX, 1, 0, 0, "2"},

{{13,FsMrpInstanceBridgeMmrpEnabledStatus}, GetNextIndexFsMrpInstanceTable, FsMrpInstanceBridgeMmrpEnabledStatusGet, FsMrpInstanceBridgeMmrpEnabledStatusSet, FsMrpInstanceBridgeMmrpEnabledStatusTest, FsMrpInstanceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpInstanceTableINDEX, 1, 0, 0, "1"},

{{13,FsMrpInstanceBridgeMvrpEnabledStatus}, GetNextIndexFsMrpInstanceTable, FsMrpInstanceBridgeMvrpEnabledStatusGet, FsMrpInstanceBridgeMvrpEnabledStatusSet, FsMrpInstanceBridgeMvrpEnabledStatusTest, FsMrpInstanceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpInstanceTableINDEX, 1, 0, 0, "1"},

{{13,FsMrpPortPeriodicSEMStatus}, GetNextIndexFsMrpPortTable, FsMrpPortPeriodicSEMStatusGet, FsMrpPortPeriodicSEMStatusSet, FsMrpPortPeriodicSEMStatusTest, FsMrpPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpPortTableINDEX, 2, 0, 0, "2"},

{{13,FsMrpPortParticipantType}, GetNextIndexFsMrpPortTable, FsMrpPortParticipantTypeGet, FsMrpPortParticipantTypeSet, FsMrpPortParticipantTypeTest, FsMrpPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpPortTableINDEX, 2, 0, 0, "1"},

{{13,FsMrpPortRegAdminControl}, GetNextIndexFsMrpPortTable, FsMrpPortRegAdminControlGet, FsMrpPortRegAdminControlSet, FsMrpPortRegAdminControlTest, FsMrpPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpPortTableINDEX, 2, 0, 0, "0"},

{{13,FsMrpPortRestrictedGroupRegistration}, GetNextIndexFsMrpPortTable, FsMrpPortRestrictedGroupRegistrationGet, FsMrpPortRestrictedGroupRegistrationSet, FsMrpPortRestrictedGroupRegistrationTest, FsMrpPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpPortTableINDEX, 2, 0, 0, "2"},

{{13,FsMrpPortRestrictedVlanRegistration}, GetNextIndexFsMrpPortTable, FsMrpPortRestrictedVlanRegistrationGet, FsMrpPortRestrictedVlanRegistrationSet, FsMrpPortRestrictedVlanRegistrationTest, FsMrpPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpPortTableINDEX, 2, 0, 0, "2"},


{{13,FsMvrpPortMvrpEnabledStatus}, GetNextIndexFsMvrpPortTable, FsMvrpPortMvrpEnabledStatusGet, FsMvrpPortMvrpEnabledStatusSet, FsMvrpPortMvrpEnabledStatusTest, FsMvrpPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMvrpPortTableINDEX, 2, 0, 0, "1"},

{{13,FsMvrpPortMvrpFailedRegistrations}, GetNextIndexFsMvrpPortTable, FsMvrpPortMvrpFailedRegistrationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMvrpPortTableINDEX, 2, 0, 0, NULL},

{{13,FsMvrpPortMvrpLastPduOrigin}, GetNextIndexFsMvrpPortTable, FsMvrpPortMvrpLastPduOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMvrpPortTableINDEX, 2, 0, 0, NULL},

{{13,FsMrpPortStatsClearStatistics}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsClearStatisticsGet, FsMrpPortStatsClearStatisticsSet, FsMrpPortStatsClearStatisticsTest, FsMrpPortStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpPortStatsTableINDEX, 3, 0, 0, "2"},

{{13,FsMrpPortStatsNumberOfRegistrations}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsNumberOfRegistrationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsRxValidPduCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsRxValidPduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsRxInvalidPduCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsRxInvalidPduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsRxNewMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsRxNewMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsRxJoinInMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsRxJoinInMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsRxJoinMtMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsRxJoinMtMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsRxLeaveMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsRxLeaveMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsRxEmptyMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsRxEmptyMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsRxInMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsRxInMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsRxLeaveAllMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsRxLeaveAllMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsTxPduCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsTxPduCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsTxNewMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsTxNewMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsTxJoinInMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsTxJoinInMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsTxJoinMtMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsTxJoinMtMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsTxLeaveMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsTxLeaveMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsTxEmptyMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsTxEmptyMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsTxInMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsTxInMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpPortStatsTxLeaveAllMsgCount}, GetNextIndexFsMrpPortStatsTable, FsMrpPortStatsTxLeaveAllMsgCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsMrpPortStatsTableINDEX, 3, 0, 0, NULL},

{{13,FsMrpApplicationAddress}, GetNextIndexFsMrpApplicantControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, FsMrpApplicantControlTableINDEX, 4, 0, 0, NULL},

{{13,FsMrpAttributeType}, GetNextIndexFsMrpApplicantControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMrpApplicantControlTableINDEX, 4, 0, 0, NULL},

{{13,FsMrpApplicantControlAdminStatus}, GetNextIndexFsMrpApplicantControlTable, FsMrpApplicantControlAdminStatusGet, FsMrpApplicantControlAdminStatusSet, FsMrpApplicantControlAdminStatusTest, FsMrpApplicantControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMrpApplicantControlTableINDEX, 4, 0, 0, "1"},

{{13,FsMrpSEMMapContext}, GetNextIndexFsMrpSEMTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMrpSEMTableINDEX, 6, 0, 0, NULL},

{{13,FsMrpSEMAttributeValue}, GetNextIndexFsMrpSEMTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMrpSEMTableINDEX, 6, 0, 0, NULL},

{{13,FsMrpSEMApplicantState}, GetNextIndexFsMrpSEMTable, FsMrpSEMApplicantStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMrpSEMTableINDEX, 6, 0, 0, NULL},

{{13,FsMrpSEMRegistrarState}, GetNextIndexFsMrpSEMTable, FsMrpSEMRegistrarStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMrpSEMTableINDEX, 6, 0, 0, NULL},

{{13,FsMrpSEMOriginatorAddress}, GetNextIndexFsMrpSEMTable, FsMrpSEMOriginatorAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, FsMrpSEMTableINDEX, 6, 0, 0, NULL},

{{11,FsMrpTrapContextName}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsMrpTrapBridgeBasePort}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsMrpTrapMvrpAttributeValue}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsMrpTrapMmrpAttributeValue}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsMrpTrapAttrRegFailureReason}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fsmrpEntry = { 47, fsmrpMibEntry };

#endif /* _FSMRPDB_H */

