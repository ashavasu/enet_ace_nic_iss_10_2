/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpconst.h,v 1.15 2011/05/02 08:45:17 siva Exp $
 *
 * Description: This file contains constant definitions used in MRP Module.
 *
 *****************************************************************************/
#ifndef _MRP_CONST_H
#define _MRP_CONST_H


#define MRP_TASK                   ((UINT1 *)"MRP")
#define MRP_PDU_QUEUE_NAME         ((UINT1 *)"MRPQ")
#define MRP_MSG_QUEUE_NAME         ((UINT1 *)"MRMQ")
#define MRP_SEM_NAME               ((const UINT1 *)"MRPS")

#define MRP_TIMER_EXP_EVENT        0x0001
#define MRP_PDU_ENQ_EVENT          0x0002
#define MRP_MSG_ENQ_EVENT          0x0004
#define MRP_ALL_EVENTS             (MRP_TIMER_EXP_EVENT | MRP_PDU_ENQ_EVENT \
                                    | MRP_MSG_ENQ_EVENT)


#define MRP_MMRP_APP_ID             1
#define MRP_MVRP_APP_ID             2

#define MRP_INVALID_REGCOUNT        -1

#define MRP_NO_REGS_FOR_LEAVEMSG        0
#define MRP_MIN_REGS_FOR_LEAVEMSG       1

#define MRP_MMRP_ALL_GROUPS             0
#define MRP_MMRP_UNREG_GROUPS           1

#define MRP_SNMP_TRUE               1
#define MRP_SNMP_FALSE              2

#define MRP_START                   1
#define MRP_SHUTDOWN                2

#define MRP_PASSIVE                 1
#define MRP_ACTIVE                  2

#define MRP_SERVICE_REQ_LEN         1
#define MRP_VLAN_ID_LEN             2
#define MRP_MAC_ADDR_LEN            6

#define MRP_INIT_VAL                0
#define MRP_PORTS_PER_BYTE          8
#define MRP_VLANS_PER_BYTE          8

#define MRP_MAX_APPS                2
#define MRP_MAX_ATTR_LEN            6

#define MRP_MAX_REG_EVENTS          9
#define MRP_MAX_REG_STATES          3

#define MRP_MAX_APP_EVENTS          15
#define MRP_MAX_APP_STATES          12


#define MRP_OPTIMAL_ENCODE_INTERVAL 3
#define MRP_LEAVE_SPLIT_INTERVAL    3

#define MRP_TRC_MIN_SIZE            1
#define MRP_MAX_TRC_OPTIONS         19
#define MRP_MAX_TRC_STR_LEN         15

#define MRP_BIT8                    0x80
#define MRP_MAX_VAL                 0xffffff
#define MRP_INIT_WITH_ONE           0xFF
#define MRP_MAX_VAL_IN_BYTE         0xFF
#define MRP_MILLI_SEC_CONVERTER     10

/* As per IEEE 802.1ak 2007 section 10.7.11 */
#define MRP_DEF_JOIN_TIME           20 /* Centi Seconds */
#define MRP_DEF_LEAVE_TIME          60 /* Centi Seconds */
#define MRP_DEF_LEAVE_ALL_TIME      1000 /* Centi Seconds */
#define MRP_DEF_PERIODIC_TIME       100 /* Centi Seconds (1 Second) */

#define MRP_RELINQUISH_INTERVAL (2 * MRP_DEF_JOIN_TIME)
#define MRP_JOIN_TMR_MULTIPLIER     1.5


#define MRP_MAX_MSG_PER_PORT        4  /* One for MVRP ,two for service req  
                                        * attr, one for multicast MAC */
#define MRP_MAX_PORT_FOR_BULK_MSG   1 /* This value can be changed as needed */


#define MRP_MAX_MMRP_MAPS_PER_PORT \
    ((MRP_VLAN_DEV_MAX_NUM_VLAN > VLAN_DEV_MAX_VLAN_ID)? \
     VLAN_DEV_MAX_VLAN_ID: MRP_VLAN_DEV_MAX_NUM_VLAN)

#define MRP_MAX_MEM_MVRP_ATTR_IN_SYS       (VLAN_DEV_MAX_VLAN_ID+1)

#define MRP_MAX_MEM_MMRP_ATTR_IN_SYS       (MRP_MAX_MMRP_ATTR_IN_SYS+1)

#define MRP_MVRP_LV_BIT_LIST_BLK              \
                  ((VLAN_MAX_VLAN_ID + 31)/32 * 4)

#define MRP_MMRP_LV_BIT_LIST_BLK              \
                   ((MRP_MAX_MMRP_ATTR_IN_SYS + 31)/32 * 4)

#define MRP_MVRP_LV_BIT_LIST_BLK_COUNT (MRP_MAX_MVRP_MAP_PORTS_IN_SYS*3)

#define MRP_MMRP_LV_BIT_LIST_BLK_COUNT (MRP_MAX_MMRP_MAP_PORTS_IN_SYS*3)

#define MRP_MVRP_PORT_ATTR_SIZE  (VLAN_MAX_VLAN_ID / MRP_PORTS_PER_BYTE)

#define MRP_MAX_PDU_LEN             1496 /* MRPDUs can have tag information, so 
                                          * reduced the size of tag (4 Bytes) 
                                          * from the ethernet MTU (1500 Bytes)*/

#define MRP_MAX_REMAP_ATTR_ENTRIES  MRP_MAX_MMRP_ATTRIBUTES


#define MRP_MAX_APP_PORT_ENTRIES   (MRP_MAX_APPS * \
                                    MRP_MAX_PORT_ENTRIES_IN_SYSTEM)
#define MRP_MAX_APP_PORT_ENTRIES_IN_SYSTEM \
                                   (MRP_MAX_APPS * \
                                    MRP_MAX_PORT_ENTRIES_IN_SYSTEM)

#define MRP_MAX_PORT_STATS_ENTRIES  MRP_MAX_APP_PORT_ENTRIES
#define MRP_MAX_PORT_STATS_ENTRIES_IN_SYSTEM \
                                    MRP_MAX_APP_PORT_ENTRIES_IN_SYSTEM



#define MRP_MAX_MAP_PORT_ENTRIES_IN_SYS (MRP_MAX_MVRP_MAP_PORTS_IN_SYS + \
                                         MRP_MAX_MMRP_MAP_PORTS_IN_SYS)


#define MRP_PORT_MVRP_ATTR_MAX_BLK_SIZE  ((VLAN_MAX_VLAN_ID / \
                                           MRP_PORTS_PER_BYTE) * \
                                          MRP_MAX_PORT_ENTRIES_IN_SYSTEM)

#define MRP_VLAN_LIST_SIZE           ((VLAN_MAX_VLAN_ID + 31)/32 * 4)

#define MRP_INVALID_VALUE           -1
#define MRP_INVALID_PORT             0

/* MRP Applicant Admin Control Values */
#define MRP_NORMAL_PARTICIPANT      1
#define MRP_NON_PARTICIPANT         2
#define MRP_ACTIVE_APPLICANT        3

/* MRP Participant Types*/
#define MRP_FULL_PARTICIPANT 1
#define MRP_APPLICANT_ONLY 2

#define MRP_PORT_CREATE_MSG         1
#define MRP_PORT_DELETE_MSG         2
#define MRP_PORT_OPER_UP_MSG        3
#define MRP_PORT_OPER_DOWN_MSG      4
#define MRP_STAP_PORT_FWD_MSG       5
#define MRP_STAP_PORT_BLK_MSG       6
#define MRP_PROP_VLAN_INFO_MSG      7
#define MRP_PROP_MAC_INFO_MSG       8
#define MRP_PROP_FWDALL_INFO_MSG    9
#define MRP_PROP_FWDUNREG_INFO_MSG  10
#define MRP_SET_VLAN_FORBID_MSG     11
#define MRP_SET_MCAST_FORBID_MSG    12
#define MRP_SET_FWDALL_FORBID_MSG   13
#define MRP_SET_FWDUNREG_FORBID_MSG 14
#define MRP_UPDT_OR_POST_MSG        15
#define MRP_UPDATE_MAP_PORTS_MSG    16
#define MRP_CREATE_CONTEXT_MSG      17
#define MRP_DELETE_CONTEXT_MSG      18
#define MRP_PORT_MAP_MSG            19
#define MRP_PORT_UNMAP_MSG          20
#define MRP_UPDATE_CONTEXT_NAME     21
#define MRP_PORT_OPER_P2P_MSG       22
#define MRP_PORT_ROLE_CHG_MSG       23
#define MRP_TCDETECTED_TMR_STATUS   24

#define MRP_BULK_MSG                1
#define MRP_MSG                     2
#define MRP_VLAN_INST_MAP_MSG       3
#define MRP_VLAN_LIST_INST_MAP_MSG  4
#define MRP_RM_MSG                  5


/*   MRP RB Tree related constants  */

#define MRP_RB_GREATER              1
#define MRP_RB_EQUAL                0
#define MRP_RB_LESS                -1


/* List of messages to be processed inside MRP_VLAN_MAP_MSG */
#define MRP_MAP_INST_VLAN_MSG        MST_MAP_INST_VLAN_MSG
#define MRP_UNMAP_INST_VLAN_MSG      MST_UNMAP_INST_VLAN_MSG
#define MRP_MAP_INST_VLAN_LIST_MSG   MST_MAP_INST_VLAN_LIST_MSG
#define MRP_UNMAP_INST_VLAN_LIST_MSG MST_UNMAP_INST_VLAN_LIST_MSG

#define MRP_UPDATE_MAP_MSG          1
#define MRP_POST_MAP_MSG            2
#define MRP_UPDNPOST_MAP_MSG        3

#define MRP_MAP_MSG_UPDATED         MST_UPDATE_INST_VLAN_MAP_MSG
#define MRP_MAP_MSG_POSTED          MST_POST_INST_VLAN_MAP_MSG

#define MRP_REG_ADMIN_CTRL_BITMASK  0xC0
#define MRP_REG_SEM_STATE_BITMASK   0x30
#define MRP_APP_SEM_STATE_BITMASK   0x0F

#define MRP_ADD                     1
#define MRP_DELETE                  2

/* MRPDU related constants */
#define MRP_PROTOCOL_VERSION        0
#define MRP_MIN_FRAME_SIZE (MRP_PDU_HDR_SIZE + MRP_MIN_MSG_LEN + \
                            MRP_END_MARK_SIZE)
#define MRP_MAX_FRAME_SIZE 1500

#define MRP_DST_ADDR_OFFSET 0
#define MRP_SRC_ADDR_OFFSET 6
#define MRP_ETHER_TYPE_LEN   2
#define MRP_ETHER_TYPE_OFFSET 12
#define MRP_PROTOCOL_VERSION_OFFSET 14

#define MRP_PDU_LV_ALL_VALUE        8192
#define MRP_PDU_HDR_SIZE 15 /* DA + SA + ETHERTYPE + PROTOCOL VERSION */
#define MRP_ATTR_TYPE_SIZE 1
#define MRP_ATTR_LEN_SIZE 1
#define MRP_VECTOR_HDR_SIZE 2
#define MRP_VECTOR_SIZE 1
#define MRP_END_MARK_SIZE 2
#define MRP_INVAID_ATTR_TYPE 0 /* 0 is reserved and shall not be used by any 
                                * Application.
                                */
#define MRP_END_MARK_VALUE 0  
#define MRP_MIN_MSG_LEN 8 /* Length of Service Requirement Message */
#define MRP_MIN_MVRP_VECT_ATTR_LEN 5 
#define MRP_MIN_MMRP_VECT_ATTR_LEN 9 
#define MRP_NUM_OF_VAL_FIELD_BITMASK 0x1FFF
#define MRP_LV_ALL_EVENT_BITMASK 0xE000

#define MRP_PDU_LV_ALL_EVENT 1
#define MRP_PDU_NULL_LV_ALL_EVENT 0

#define MRP_MAX_ATTR_EVENTS_PER_VECTOR 3


#define MRP_FLUSH_IND 1
#define MRP_REDECLARE_IND 2

#define MRP_INVALID_CONTEXT_ID   0xFFFFFFFF

#define MRP_MIB_OID_TABLE_SIZE   89

#define MRP_SYS_LOG_MSG_LEN     256

#define MRP_MAX_MAC_STRING_SIZE   21

#define MRP_INVALID_SER_REQ_VALUE  255

#define MRP_SNMPV2_TRAP_OID_LEN     11

#define MRP_REMAP_WITH_ATTR       1
#define MRP_REMAP_WITHOUT_ATTR    2
#define MRP_MAX_LOG_STR_LEN       256
#define MRP_TRC_BUF_SIZE          2000

#define MRP_MAX_PORT_NAME_LENGTH   CFA_MAX_PORT_NAME_LENGTH

#define MRP_NO_ATTR_FILLED         0xF

#define MRP_DEFAULT_POINTER_SIZE       4

/* SRC Related const. Need to updated if default value changes */
#define MRP_DEF_RESTRICTED_VLAN_REG   MRP_DISABLED
#define MRP_DEF_GROUP_RESTRICTED_REG  MRP_DISABLED
#define MRP_DEF_SYSTEM_CTRL           MRP_SHUTDOWN
#define MRP_DEF_MVRP_STATUS           MRP_DISABLED
#define MRP_DEF_MMRP_STATUS           MRP_DISABLED
#define MRP_DEF_VLAN_REG_FAIL         MRP_DISABLED
#define MRP_DEF_MAC_REG_FAIL          MRP_DISABLED
#define MRP_DEF_PORT_MVRP_STATUS      MRP_ENABLED
#define MRP_DEF_PORT_MMRP_STATUS      MRP_ENABLED
#define MRP_DEF_PERIODIC_SEM          MRP_DISABLED
#define MRP_DEF_PARTICPANT_TYPE       MRP_FULL_PARTICIPANT
#define MRP_DEF_MMRP_SERVICE_REQ_APP  MRP_NORMAL_PARTICIPANT
#define MRP_DEF_MVRP_APP              MRP_NORMAL_PARTICIPANT
#define MRP_DEF_MMRP_MACP_APP         MRP_NORMAL_PARTICIPANT

/*MRP Sizing Related Macros*/
#define   MRP_MAX_MMRP_PORTS    64
                                         /* Maximum number of interfaces
                                          * available in the system for MMRP. This
                                          * affects the memory pools in the
                                          * MRP module
                                          */

#define   MRP_MAX_MVRP_PORTS    64
                                         /* Maximum number of interfaces
                                          * available in the system for MVRP. This
                                          * affects the memory pools in the
                                          * MRP module
                                          */
#endif /*_MRP_CONST_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpconst.h                     */
/*-----------------------------------------------------------------------*/
