/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: mrpextn.h,v 1.7 2011/04/27 11:59:34 siva Exp $
 *
 * Description: This file contains extern variables declaration
 *              used in MRP module. 
 *
 *****************************************************************************/
#ifndef _MRPEXTN_H
#define _MRPEXTN_H

extern tMrpGlobalInfo   gMrpGlobalInfo;
extern UINT1            gau1MrpBitMaskMap [];
extern tMacAddr         gMvrpAddr;
extern tMacAddr         gMmrpAddr;
extern tMacAddr         gProviderMvrpAddr;
extern tMacAddr         gCustomerMvrpAddr;
extern tMacAddr         gMrpReservedAddr;
extern tMacAddr         gMrpNullMacAddress;
extern tMacAddr         gVlanReservedAddress;
extern tMacAddr         gVlanBcastAddress;
extern tLocalPortList   gMrpNullPortList;
extern CHR1             *gau1RegSEMState[];
extern CHR1             *gau1AppSEMState[];
extern CHR1             *gau1AppAdminStatus[];
extern CHR1             *gau1RegAdminCtrl[];
extern  UINT2           gau2SecVlanList[];

#ifdef TRACE_WANTED
extern CHR1             *gau1RegSEMEvtName[];
extern CHR1             *gau1AppSEMEvtName[];
#endif

extern tMrpRegSemEntry  gaMrpRegSem [MRP_MAX_REG_EVENTS][MRP_MAX_REG_STATES];
extern tMrpAppSemEntry *gaMrpFullAppSem [MRP_MAX_APP_EVENTS][MRP_MAX_APP_STATES];
extern tMrpAppSemEntry *gaMrpFullAppP2PSem [MRP_MAX_APP_EVENTS][MRP_MAX_APP_STATES];
extern tMrpAppSemEntry *gaMrpAppOnlySem [MRP_MAX_APP_EVENTS][MRP_MAX_APP_STATES];
extern tMrpAppSemEntry *gaMrpAppOnlyP2PSem [MRP_MAX_APP_EVENTS][MRP_MAX_APP_STATES];

extern tMrpAppSemEntry  gMrpAppAction1;
extern tMrpAppSemEntry  gMrpAppAction2;
extern tMrpAppSemEntry  gMrpAppAction3;
extern tMrpAppSemEntry  gMrpAppAction4;
extern tMrpAppSemEntry  gMrpAppAction5;
extern tMrpAppSemEntry  gMrpAppAction6;
extern tMrpAppSemEntry  gMrpAppAction7;
extern tMrpAppSemEntry  gMrpAppAction8;
extern tMrpAppSemEntry  gMrpAppAction9;
extern tMrpAppSemEntry  gMrpAppAction10;
extern tMrpAppSemEntry  gMrpAppAction11;
extern tMrpAppSemEntry  gMrpAppAction12;
extern tMrpAppSemEntry  gMrpAppAction13;
extern tMrpAppSemEntry  gMrpAppAction14;
extern tMrpAppSemEntry  gMrpAppAction15;
extern tMrpAppSemEntry  gMrpAppAction16;
extern tMrpAppSemEntry  gMrpAppAction17;
extern tMrpAppSemEntry  gMrpAppAction18;
extern tMrpAppSemEntry  gMrpAppAction19;
extern tMrpAppSemEntry  gMrpAppAction20;
extern tMrpAppSemEntry  gMrpAppAction21;
extern tMrpAppSemEntry  gMrpAppAction22;
extern tMrpAppSemEntry  gMrpAppAction23;

extern tFsModSizingParams gFsMrpSizingParams [];
extern tFsModSizingInfo gFsMrpSizingInfo;

#endif /*_MRP_EXTN_H*/
/*-----------------------------------------------------------------------*/
/*                       End of the file  mrpextn.h                      */
/*-----------------------------------------------------------------------*/
