#ifndef _STD1D1WR_H
#define _STD1D1WR_H
INT4 GetNextIndexIeee8021BridgeBaseTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTD1D1(VOID);

VOID UnRegisterSTD1D1(VOID);
INT4 Ieee8021BridgeBaseBridgeAddressGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseNumPortsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseComponentTypeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseDeviceCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseTrafficClassesEnabledGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseMmrpEnabledStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseBridgeAddressSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseComponentTypeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseDeviceCapabilitiesSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseTrafficClassesEnabledSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseMmrpEnabledStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseBridgeAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseComponentTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseDeviceCapabilitiesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseTrafficClassesEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseMmrpEnabledStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBaseTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeBasePortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeBasePortIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortDelayExceededDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortMtuExceededDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortTypeCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortTypeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortExternalGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortAdminPointToPointGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortOperPointToPointGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortNameGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortAdminPointToPointSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortAdminPointToPointTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeBasePortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeTpPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeTpPortMaxInfoGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeTpPortInFramesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeTpPortOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeTpPortInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021BridgePortPriorityTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgePortDefaultUserPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortNumTrafficClassesGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortPriorityCodePointSelectionGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortUseDEIGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortRequireDropEncodingGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortServiceAccessPrioritySelectionGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortDefaultUserPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortNumTrafficClassesSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortPriorityCodePointSelectionSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortUseDEISet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortRequireDropEncodingSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortServiceAccessPrioritySelectionSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortDefaultUserPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortNumTrafficClassesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortPriorityCodePointSelectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortUseDEITest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortRequireDropEncodingTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortServiceAccessPrioritySelectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortPriorityTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeUserPriorityRegenTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeRegenUserPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeRegenUserPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeRegenUserPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeUserPriorityRegenTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeTrafficClassTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeTrafficClassGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeTrafficClassSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeTrafficClassTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeTrafficClassTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgePortOutboundAccessPriorityTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgePortOutboundAccessPriorityGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexIeee8021BridgePortDecodingTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgePortDecodingPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortDecodingDropEligibleGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortDecodingPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortDecodingDropEligibleSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortDecodingPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortDecodingDropEligibleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortDecodingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgePortEncodingTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgePortEncodingPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortEncodingPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortEncodingPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortEncodingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeServiceAccessPriorityTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeServiceAccessPriorityValueGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeServiceAccessPriorityValueSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeServiceAccessPriorityValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeServiceAccessPriorityTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgePortMrpTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgePortMrpJoinTimeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveTimeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveAllTimeGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpJoinTimeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveTimeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveAllTimeSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpJoinTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpLeaveAllTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMrpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgePortMmrpTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgePortMmrpEnabledStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMmrpFailedRegistrationsGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMmrpLastPduOriginGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortRestrictedGroupRegistrationGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMmrpEnabledStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortRestrictedGroupRegistrationSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMmrpEnabledStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortRestrictedGroupRegistrationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgePortMmrpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeILanIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeILanIfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeILanIfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeILanIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeILanIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexIeee8021BridgeDot1dPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Ieee8021BridgeDot1dPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeDot1dPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeDot1dPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Ieee8021BridgeDot1dPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STD1D1WR_H */
