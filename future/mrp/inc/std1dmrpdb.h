/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: std1dmrpdb.h,v 1.1 2011/11/28 12:07:20 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STD1D1DB_H
#define _STD1D1DB_H

UINT1 Ieee8021BridgePortMrpTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021BridgePortMmrpTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 std1dmrp [] ={1,3,111,2,802,1,1,2};
tSNMP_OID_TYPE std1dmrpOID = {8, std1dmrp};


/* Generated OID's for tables */

UINT4 Ieee8021BridgePortMrpTable [] ={1,3,111,2,802,1,1,2,1,4,1};
tSNMP_OID_TYPE Ieee8021BridgePortMrpTableOID = {11, Ieee8021BridgePortMrpTable};


UINT4 Ieee8021BridgePortMmrpTable [] ={1,3,111,2,802,1,1,2,1,5,1};
tSNMP_OID_TYPE Ieee8021BridgePortMmrpTableOID = {11, Ieee8021BridgePortMmrpTable};


UINT4 Ieee8021BridgePortMrpJoinTime [ ] ={1,3,111,2,802,1,1,2,1,4,1,1,1};
UINT4 Ieee8021BridgePortMrpLeaveTime [ ] ={1,3,111,2,802,1,1,2,1,4,1,1,2};
UINT4 Ieee8021BridgePortMrpLeaveAllTime [ ] ={1,3,111,2,802,1,1,2,1,4,1,1,3};
UINT4 Ieee8021BridgePortMmrpEnabledStatus [ ] ={1,3,111,2,802,1,1,2,1,5,1,1,1};
UINT4 Ieee8021BridgePortMmrpFailedRegistrations [ ] ={1,3,111,2,802,1,1,2,1,5,1,1,2};
UINT4 Ieee8021BridgePortMmrpLastPduOrigin [ ] ={1,3,111,2,802,1,1,2,1,5,1,1,3};
UINT4 Ieee8021BridgePortRestrictedGroupRegistration [ ] ={1,3,111,2,802,1,1,2,1,5,1,1,4};


tMbDbEntry Ieee8021BridgePortMrpTableMibEntry[]= {

{{13,Ieee8021BridgePortMrpJoinTime}, GetNextIndexIeee8021BridgePortMrpTable, Ieee8021BridgePortMrpJoinTimeGet, Ieee8021BridgePortMrpJoinTimeSet, Ieee8021BridgePortMrpJoinTimeTest, Ieee8021BridgePortMrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgePortMrpTableINDEX, 2, 0, 0, "20"},

{{13,Ieee8021BridgePortMrpLeaveTime}, GetNextIndexIeee8021BridgePortMrpTable, Ieee8021BridgePortMrpLeaveTimeGet, Ieee8021BridgePortMrpLeaveTimeSet, Ieee8021BridgePortMrpLeaveTimeTest, Ieee8021BridgePortMrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgePortMrpTableINDEX, 2, 0, 0, "60"},

{{13,Ieee8021BridgePortMrpLeaveAllTime}, GetNextIndexIeee8021BridgePortMrpTable, Ieee8021BridgePortMrpLeaveAllTimeGet, Ieee8021BridgePortMrpLeaveAllTimeSet, Ieee8021BridgePortMrpLeaveAllTimeTest, Ieee8021BridgePortMrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgePortMrpTableINDEX, 2, 0, 0, "1000"},
};
tMibData Ieee8021BridgePortMrpTableEntry = { 3, Ieee8021BridgePortMrpTableMibEntry };

tMbDbEntry Ieee8021BridgePortMmrpTableMibEntry[]= {

{{13,Ieee8021BridgePortMmrpEnabledStatus}, GetNextIndexIeee8021BridgePortMmrpTable, Ieee8021BridgePortMmrpEnabledStatusGet, Ieee8021BridgePortMmrpEnabledStatusSet, Ieee8021BridgePortMmrpEnabledStatusTest, Ieee8021BridgePortMmrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgePortMmrpTableINDEX, 2, 0, 0, "1"},

{{13,Ieee8021BridgePortMmrpFailedRegistrations}, GetNextIndexIeee8021BridgePortMmrpTable, Ieee8021BridgePortMmrpFailedRegistrationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Ieee8021BridgePortMmrpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgePortMmrpLastPduOrigin}, GetNextIndexIeee8021BridgePortMmrpTable, Ieee8021BridgePortMmrpLastPduOriginGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Ieee8021BridgePortMmrpTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021BridgePortRestrictedGroupRegistration}, GetNextIndexIeee8021BridgePortMmrpTable, Ieee8021BridgePortRestrictedGroupRegistrationGet, Ieee8021BridgePortRestrictedGroupRegistrationSet, Ieee8021BridgePortRestrictedGroupRegistrationTest, Ieee8021BridgePortMmrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021BridgePortMmrpTableINDEX, 2, 0, 0, "2"},
};
tMibData Ieee8021BridgePortMmrpTableEntry = { 4, Ieee8021BridgePortMmrpTableMibEntry };

#endif /* _STD1D1DB_H */

