#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 07 March 2002                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


L2IWF_SWITCHES = -UDEBUG_WANTED 

TOTAL_OPNS = ${L2IWF_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

L2IWF_BASE_DIR = ${BASE_DIR}/l2iwf
L2IWF_SRC_DIR  = ${L2IWF_BASE_DIR}/src
L2IWF_INC_DIR  = ${L2IWF_BASE_DIR}/inc
L2IWF_OBJ_DIR  = ${L2IWF_BASE_DIR}/obj
COMMON_INC_DIR  = ${BASE_DIR}/inc
VLANGARP_INC_DIR  = ${COMMON_INC_DIR}/vlangarp

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${L2IWF_INC_DIR} -I${COMMON_INC_DIR} 
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
