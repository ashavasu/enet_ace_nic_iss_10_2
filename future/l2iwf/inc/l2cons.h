/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: l2cons.h,v 1.17 2011/08/18 09:35:53 siva Exp $
 *
 * Description: This file contains MACROS for L2IWF Module.
 *
 *******************************************************************/

#ifndef _L2CONS_H_
#define _L2CONS_H_

#define L2IWF_SEMAPHORE    ((const UINT1 *)"L2SM")
#define L2_DB_SEM          ((const UINT1 *)"L2DB")
#define L2_SYNC_SEM    ((const UINT1 *)"L2SY")
#define L2_MI_SYNC_SEM    ((const UINT1 *)"L2MS")

#define L2_SNOOP_ENQ_CONSUMED 1
#define L2_SNOOP_ENQ_CONTINUE 2
#define L2_SNOOP_ENQ_FAIL 3


#define L2IWF_PORTORVLAN_PER_BYTE           8
#define L2IWF_IS_VALID_VLANID(Vid) \
                (((Vid < VLAN_DEV_MIN_VLAN_ID) || (Vid > VLAN_MAX_VLAN_ID))? L2IWF_FALSE:L2IWF_TRUE)

#define    L2IWF_MAX_MST_INSTANCES (L2IWF_MAX_CONTEXTS * \
          AST_MAX_MST_INSTANCES)
#endif /* _L2_CONS_H_ */
