
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: l2glob.h,v 1.21 2016/03/09 11:37:35 siva Exp $
 *
 * Description: This file contains all the macros used by L2IWF module
 *
 *******************************************************************/
#ifndef _L2GLOB_H_
#define _L2GLOB_H_


#ifdef _L2INIT_C_
tL2GlobalInfo    gL2GlobalInfo;
tL2ContextInfo  *gpL2Context = NULL;
UINT2       gu2PoIntfPhysicalMAC = L2IWF_FALSE;
UINT1               gau1VlanPortBitMaskMap[VLAN_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02};


tFsModSizingParams gFsL2iwfSizingParams [] = 
{
    {"tL2PortInfo", "L2IWF_MAX_PORTS_PER_CONTEXT_EXT", sizeof (tL2PortInfo),
     L2IWF_MAX_PORTS_PER_CONTEXT_EXT, L2IWF_MAX_PORTS_PER_CONTEXT_EXT, 0},

    {"tL2VlanInfo", "VLAN_DEV_MAX_NUM_VLAN", sizeof (tL2VlanInfo),
     VLAN_DEV_MAX_NUM_VLAN, VLAN_DEV_MAX_NUM_VLAN, 0},

    {"tL2PortChannelInfo", "LA_DEV_MAX_TRUNK_GROUP", 
     (sizeof (tL2PortChannelInfo) * LA_DEV_MAX_TRUNK_GROUP), 1, 1, 0},

    {"tL2VlanFidMstInstInfo", "L2IWF_MAX_CONTEXTS", 
     (sizeof (tL2VlanFidMstInstInfo) * VLAN_DEV_MAX_VLAN_ID), 
     L2IWF_MAX_CONTEXTS, L2IWF_MAX_CONTEXTS, 0},
  
    {"tL2ContextInfo", "L2IWF_MAX_CONTEXTS", sizeof (tL2ContextInfo),
     L2IWF_MAX_CONTEXTS, L2IWF_MAX_CONTEXTS, 0},

    {"tL2PnacMacInfo", "PNAC_MAX_AUTHSESS", sizeof (tL2PnacMacInfo),
     PNAC_MAX_AUTHSESS, PNAC_MAX_AUTHSESS, 0},

#ifdef PB_WANTED
    {"tL2VidTransEntry", "VLAN_MAX_VID_TRANSLATION_ENTRIES", 
     sizeof (tL2VidTransEntry), VLAN_MAX_VID_TRANSLATION_ENTRIES, 
     VLAN_MAX_VID_TRANSLATION_ENTRIES, 0},

    {"tL2CVlanPortEntry", "L2IWF_MAX_NUM_PEP", sizeof (tL2CVlanPortEntry),
     L2IWF_MAX_NUM_PEP, L2IWF_MAX_NUM_PEP, 0},
#endif

    {"tL2PortVlanInfo", "L2IWF_PORT_VLAN_MEMBLK_COUNT", sizeof (tL2PortVlanInfo),
     L2IWF_PORT_VLAN_MEMBLK_COUNT, L2IWF_PORT_VLAN_MEMBLK_COUNT, 0},

    {"tIfTypeProtoDenyInfo", "L2IWF_MAX_IFTYPE_PROT_ENTRIES", sizeof (tIfTypeProtoDenyInfo),
     L2IWF_MAX_IFTYPE_PROT_ENTRIES, L2IWF_MAX_IFTYPE_PROT_ENTRIES, 0},

    {"tL2LldpAppInfo", "LLDP_MAX_APPL", 
        (sizeof ("tL2LldpAppInfo") * LLDP_MAX_APPL), 1, 1, 0},

    {"tL2PvlanInfo", "L2IWF_MAX_PVLAN_ENTRIES", 
        sizeof (tL2PvlanInfo), L2IWF_MAX_PVLAN_ENTRIES, 
        L2IWF_MAX_PVLAN_ENTRIES, 0},
    
    {"tL2SecondaryVlanInfo",
        "VLAN_MAX_SECONDARY_VLANS", 
        sizeof (tL2SecondaryVlanInfo), VLAN_MAX_SECONDARY_VLANS, 
        VLAN_MAX_SECONDARY_VLANS, 0},

 {"tL2MstInstInfo", "L2IWF_MAX_MST_INSTANCES", sizeof(tL2MstInstInfo), 
  L2IWF_MAX_MST_INSTANCES, L2IWF_MAX_MST_INSTANCES, 0},

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsL2iwfSizingInfo;
UINT2 gu2MbsmNpBulkSyncStatus = ZERO;

#else
extern tL2GlobalInfo  gL2GlobalInfo;
extern tL2ContextInfo  *gpL2Context;
extern UINT2 gL2DefaultVlanId;
extern UINT2 gu2PoIntfPhysicalMAC;
extern UINT1               gau1VlanPortBitMaskMap[VLAN_PORTS_PER_BYTE];

extern tFsModSizingParams gFsL2iwfSizingParams [];
extern tFsModSizingInfo gFsL2iwfSizingInfo;
extern UINT2 gu2MbsmNpBulkSyncStatus;
#endif
extern VOID  AstIndicateSTPStatus (UINT4 u4PortNum, BOOL1 bSTPStatus);
#endif
