/* $Id: l2inc.h,v 1.40 2015/09/13 09:21:20 siva Exp $ */
#ifndef _L2INC_H
#define _L2INC_H

#include "lr.h"
#include "mux.h"
#include "cfa.h"
#include "ip.h"
#include "size.h"
#include "utilipvx.h"

#include "eoam.h"
#include "bridge.h"
#include "fsvlan.h"

#include "pbbte.h"
#include "secmod.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif
#ifdef PTP_WANTED
#include "ptp.h"
#endif
#include "srmbuf.h"
#include "elm.h" 
#include "rstp.h" 
#include "l2iwf.h"
#include "mstp.h"
#include "pnac.h"
#include "la.h"
#include "lldp.h"
#include "snp.h"
#include "qosxtd.h"
#ifdef CN_WANTED
#include "cn.h"
#endif
#ifdef POE_WANTED
#include "poe.h"
#endif
#ifdef DCBX_WANTED
#include "dcbx.h"
#endif
#ifdef FSB_WANTED
#include "fsb.h"
#endif
#ifdef RBRG_WANTED
#include "rbridge.h"
#endif
#include "iss.h"
#include "vcm.h"
#include "garp.h"
#include "mrp.h"
#include "pvrst.h"
#include "ecfm.h"
#include "dhcp.h"
#ifdef PBB_WANTED
#include "pbb.h"
#endif

#include "elps.h"

#include "l2cons.h"
#include "l2tdfs.h"
#include "l2macro.h"
#include "l2proto.h"
#include "erps.h"
#ifdef PB_WANTED
#include "l2pb.h"
#endif
#include "l2glob.h"
#include "ipdb.h"
#include "l2ds.h"
#ifdef MEF_WANTED
#include "fsmef.h"
#endif
#ifdef ESAT_WANTED
#include "esat.h"
#endif

#ifdef MBSM_WANTED
#include "mbsm.h"
#endif

#include "rmon.h"
#include "isspi.h"
#ifdef ICCH_WANTED
#include "icch.h"
#endif

#include "issu.h"

#ifdef SYNCE_WANTED
#include "synce.h"
#endif
#endif
