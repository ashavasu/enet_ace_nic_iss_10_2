/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: l2tdfs.h,v 1.69 2018/01/11 11:19:19 siva Exp $
 *
 * Description: This file contains all the typedefs used by the L2IWF 
 *              and module.
 *
 *******************************************************************/


#ifndef _L2TDFS_H_
#define _L2TDFS_H_

typedef struct L2PortChannelInfo
{
    UINT2       au2ConfiguredPorts [(SYS_DEF_MAX_PHYSICAL_INTERFACES +1)];
    UINT2       au2ActivePorts [LA_MAX_PORTS_PER_AGG];
    UINT2       u2NumConfPorts;
    UINT2       u2NumActivePorts;
    BOOL1       bValid;
    BOOL1       bConfigured;
    BOOL1       bAddLinkInProgress;
    UINT1       au1Reserved [1];

} tL2PortChannelInfo;

typedef struct L2ProtStatus {
    /* Protocol Status can be Tunnel/Peer/Discard. For each protocol, the
     * corresponding protocol status tells what to do on this port. */
    UINT1       u1Dot1xProtocolStatus;
    UINT1       u1LacpProtocolStatus;
    UINT1       u1StpProtocolStatus;
    UINT1       u1GvrpProtocolStatus;
    UINT1       u1GmrpProtocolStatus;
    UINT1       u1MvrpProtocolStatus;
    UINT1       u1MmrpProtocolStatus;
    UINT1       u1IgmpProtocolStatus;
    UINT1       u1ElmiProtocolStatus;
    UINT1       u1LldpProtocolStatus;
    UINT1       u1EcfmProtocolStatus;
    UINT1       u1EoamProtocolStatus;
    UINT1       u1PtpProtocolStatus;
    UINT1       u1OtherProtocolStatus[VLAN_MAX_OTHER_PROTOCOL_SUPPORT];
    UINT1       au1Pad[1];
}tL2ProtStatus;


typedef struct L2PortInfo
{
    tRBNodeEmbd L2GlobalIfIndexNode; /* RB tree node to be added to 
                                      global IfIndex based RB Tree */
    tL2ProtStatus ProtocolStatus;/* Specifies how protocol ctrl pkts to
                                  * be handled on this port. */
    UINT1       *pInstPortState; /* Pointer to array of Spanning 
                                    * Tree Instance Port state information */     
    UINT4       u4ContextId;   /* Context to which this port belongs */
    UINT4       u4IfIndex;     /* Global IfIndex of the port */
    UINT4       u4AggIndex;    /* Global Port Channel Index of which this port
                                  is a member */
    UINT2       u2LocalPortId;   /* Local per-context port number */
    UINT2       u2OperControlDir; /* PNAC Port Control Direction */
    UINT2       u2AuthControlPortControl; /* PNAC Port Auth Control */
    UINT2       u2AuthControlPortStatus; /* PNAC Port Auth Status */
    UINT2       u2PortAuthMode;          /* Authentication mode whether 
                                          Port-Based or Mac-Based*/
    tVlanId     Pvid; /* PVID */
    UINT2       u2PbPortType;
    UINT1       u1BridgePortOperStatus;
    UINT1       u1TunnelStatus; /* Port tunnelling status */
    BOOL1       bOperEdge; /* Port Oper Edge status */
    UINT1       u1PortType; /* Access port,Trunk Port, Hybrid port,
                               promicous host.*/
    UINT1       u1PortAccpFrmType; /* Admit all ,Admit only tagged or Admit only
                                      untagged and priority tagged frames. */
    UINT1       u1GvrpStatus; /* Gvrp enabled/disabled status on this port.*/
    UINT1       u1GmrpStatus; /* Gmrp enabled/disabled status on this port.*/
    UINT1       u1MvrpStatus;
    UINT1       u1MmrpStatus;
    UINT1       u1StpStatus;  /* Spanning tree enabled/disabled status on 
                               * this port.*/
    UINT1       u1PortPaeStatus; /* PNAC enabled/disabled status of the port */
    UINT1       u1VidTransStatus;/* Indicates whether VID translation is
                                  * enabled on this port or not. */
    UINT1       u1VipOperStatusFlag; /* This Flag is set by PBB if the all the 
                                        coditions to set the VIP operstatus are
                                        satisfied */
    UINT1       u1IsSispEnabled;
 UINT1       u1LaConfigAllowed; /* Configuration allowed based on
                                    * Mirroring enabled/disabled status on
                                    * this port */
    UINT1       u1PortOwner;  /* Protocol that controls the port state of 
                                 this port */            
    BOOL1       bOperPointToPoint; /* Port Oper Point to Point status */
    UINT1       au1AllVlanList[VLAN_LIST_SIZE];
    UINT1       au1UnTaggedVlanList[VLAN_LIST_SIZE];
    UINT1       u1ServiceType;      /* Service type (Port Based/Vlan Based) */ 
    UINT1       u1OverrideOption;
    UINT1       au1Reserved[3]; /**/
} tL2PortInfo;

typedef struct L2PnacMacInfo
{
    tRBNodeEmbd SrcMacNode; /*Node to be added to the Mac sess table RB Tree*/ 
    tMacAddr    SrcMacAddr; /*Source Mac address in the received frame */
    UINT1       au1Reserved[2];
} tL2PnacMacInfo;

/* Vlan Flag bits */
#define    L2IWF_VLAN_FLOOD_DISABLED 0x1

typedef struct L2VlanInfo
{
    tVlanId         VlanId;
    tLocalPortList  EgressPorts;  /* Static Egress ports + Learnt
                                * ports + Trunk Ports */
    tLocalPortList  UntagPorts;  /* Static Untag ports */
    tLocalPortList  ForwardPorts;  /*List of ports that are in forwarding state.*/
    UINT2           u2FwdPortCount;/*Number of ports that are in forwarding state*/
    tL2ProtStatus   ProtocolStatus;/* Specifies Tunnel/Peer/Discard status per protocol for this vlan. */
    UINT1           u1TunnelStatus; /* Vlan tunnelling status (Enabled/Disabled) */
    UINT1           u1ServiceType; /* Service type for this VLAN(ELINE/ELAN)*/
    UINT1           VlanFlags;    /* Flag bits for the Vlan entry
         - Flooding disabled */
    UINT1           u1FCoEVlanType; /* This field specifies the type of FCoE VLAN */
} tL2VlanInfo;

typedef struct L2VlanFidMstInstInfo
{
    UINT2            u2Fid;
    UINT2            u2MstInst;

} tL2VlanFidMstInstInfo;

typedef struct L2MstInstInfo
{
 UINT1 au1VlanList[VLAN_LIST_SIZE];
 UINT2 u2FdbCount;
} tL2MstInstInfo;

typedef UINT1 tComponentType;
typedef UINT1 tInterfaceType;

typedef struct L2ContextInfo
{
   tL2PortInfo          *apL2PortInfo [L2IWF_MAX_PORTS_PER_CONTEXT_EXT]; 
                                       /* array of 
                                          Port entry pointers */
   tL2VlanFidMstInstInfo *paL2VidFidInstMap;  /* Pointer to array of 
                                             Vlan-Spanning tree, FID mapping
                                             indexed by VlanId. */
   tL2VlanInfo          *apL2VlanInfo[VLAN_DEV_MAX_VLAN_ID];/* Array of 
                                                               Pointers to 
                                                               Vlan
                                                               structure */
   tL2MstInstInfo       *apL2MstInstInfo[AST_MAX_MST_INSTANCES]; /* Array of 
                 pointers 
                 to MST 
                 Instance 
                 structure */
   UINT4                u4VIDDigest;              
   UINT4                u4BridgeMode;
   UINT4                u4CnpCount;      /* Count of number of CNPs in 
                                            * the context */
   UINT4                 u4AllToOneBundlingStatus; /* Status - TRUE - If any PISID
                                                      Configured */
   UINT4                  u4InternalCount;    /* Count of number of internal ports in 
                                            * the context */
   UINT4                  u4PeerCounter;
   UINT4                  u4DiscardCounter;
   UINT4                  u4TunnelCounter;
   UINT4                  u4DenyProtocolListCount;
   tRBTree              CVlanPortTable;
   tRBTree                IngVidTransTable; /* VID translation table. Will be
                                             * useful only in case when the 
                                             * bridge is a proivder bridge */
   tRBTree                EgrVidTransTable; /* VID translation table. Will be
                                             * useful only in case when the 
                                             * bridge is a proivder bridge */
   tRBTree               IfTypeProtoDenyTable;  /* IfType Protocol Deny Table */
   UINT1                 *pInstanceActive; /* Pointer to array of Spanning 
                                            * Tree Instance Port state 
           * information */
   UINT2                  u2NumVidTransEntries;
   UINT1                  u1AdminIntfTypeFlag;
   tInterfaceType         InterfaceType; /* The Service Type (S or C)  */
   BOOL1                  b1EnhancedFilterStatus;
   BOOL1                  b1PerfDataStatus;
   UINT1                  u1VlanLearningType;
   UINT1                 au1Reserved[1]; /* Reserved bytes for alignment */
} tL2ContextInfo;

typedef struct L2GlobalInfo
{
   tOsixSemId           SemId;
   tOsixSemId           DBSemId;
   tOsixSemId           SyncSemId;
   tOsixSemId           MiSyncSemId;
   tOsixSemId           BrgSyncSemId;
   tMemPoolId           VlanInfoPoolId;
   tMemPoolId           PortInfoPoolId;
   tMemPoolId           ContextInfoPoolId;
#ifdef PB_WANTED
   tMemPoolId           PepInfoPoolId;
   tMemPoolId           VidTransMemPoolId;
#endif 
   tMemPoolId           MacSessPoolId;
   tMemPoolId           PortVlanInfoPoolId; /* Pool Id for the structure tL2PortVlanInfo*/ 
   tMemPoolId           IfTypeProtoDenyPoolId; /* Pool Id for IfType Protocol Deny Table */

   tMemPoolId           LldpApplPoolId;    /* Pool Id for tL2LldpAppInfo */
   tMemPoolId           PvlanTablePoolId; /* Pool Id for the tL2PvlanInfo. */ 
   tMemPoolId           PvlanSecondaryVlanPoolId; /* Pool Id for the 
                                                     tL2SecondaryVlanInfo. */
   tMemPoolId           MstInfoPoolId;  /* MST Info Pool Id */
   tMemPoolId           PortChannelInfoPoolId; /* L2 Port Channel Info Pool Id */
   tMemPoolId           VlanFidMstInstPoolId; /* L2VlanFidMstInst Pool Id */
   tL2PortChannelInfo   *paL2PortChannelInfo; /* Pointer to array of 
                                                 Port Channel structure */
   tL2ContextInfo       *apL2ContextInfo [L2IWF_MAX_CONTEXTS];
   tL2LldpAppInfo       *paL2LldpAppInfo; /* Pointer to array of 
                                              LLDP application structure */
   tRBTree              GlobalIfIndexTable; /* Global ifIndex based port
                                                table which is common for 
                                                all contexts */
   tRBTree              SessMacTable; /* Pnac Mac session table*/
   tRBTree              PortVlanTable;    /* Port vlan information table.*/
   tRBTree              PvlanTable;       /* Private vlan information table.*/

   UINT1                 u1PortStateBuddyId;   /* Buddy Id for storing Variable
                                           response data in MIB response
                                           table */
   UINT1                 u1InstStateBuddyId;   /* Buddy Id for storing Variable
                                           response data in MIB response
                                           table */
   UINT1                u1PbbShutdownStatus; /* PBB ShutDown Status */
   UINT1                au1Reserved[3]; /* Reserved bytes for alignment */
   UINT2                u2Dummy; /* Padding */
} tL2GlobalInfo;

/* Content of each node in RBTree */
typedef struct PortVlanInfo
{
 tRBNodeEmbd         NextPortVlanInfo; /* Points to the next RBNode */
 UINT4               u4IfIndex; /* INDEX : Port Number */
 UINT1               au1VlanName[VLAN_STATIC_MAX_NAME_LEN + 1]; /*Vlan Name*/
 UINT1               u1VlanNameTxEnable; /* Transmission enable or disabled*/
 UINT2               u2VlanId; /* INDEX : Vlan ID */
        BOOL1               b1LckStatus; /* Defines if this port has to block data for
                                      * this vlan or not */
 UINT1               au1Pad[3];
}tL2PortVlanInfo;

typedef struct IfTypeProtoDenyInfo
{
 tRBNodeEmbd         NextIfTypeProtoDenyInfo; /* Points to the next RBNode */
    UINT1               u1IfType; /* Second Index - Interface type */
    UINT1               u1BrgPortType; /* Third Index - Bridge Port type */
    UINT1               u1Protocol; /* Fourth Index - Protocol Identifier */
    UINT1               au1Reserved[1]; /* Reserved byte for structure 
                                           alignment */
}tIfTypeProtoDenyInfo;

typedef struct PvlanInfo {
    tRBNodeEmbd         NextPvlanInfo; 
                              /* Points to the next pvlan node */
    tTMO_SLL            SecondaryVlanList;
                              /* Head to the secondary vlan SLL.
                               * If the indexed vlan id is secondary
                               * vlan then this head will be points to NULL.
                               */
    UINT4               u4ContextId;  
                              /* Index: Context Identifier */
    tVlanId             VlanId; 
                              /*Index: Vlan Identifier */
    tVlanId               PrimaryVlanId;
                              /* If the indexed vlan is secondary vlan the
                               * associated primary vlan
                               */
    UINT1               u1VlanType; 
                              /* Type of the vlan 
                                 - Normal
                                 - Primary 
                                 - Isolated 
                                 - Community
                               */
    UINT1               au1Reserved[3];
                              /* Used for padding */
} tL2PvlanInfo; 

typedef struct SecondaryVlanInfo {
    tTMO_SLL_NODE     NextNode;
                        /* Pointer to the next secondary vlan node in 
                         * the list.
                         */
    tVlanId           SecondaryVlanId;
                        /* Secondary vlan Id. */
    UINT2             u2Reserved;
                        /* Used for padding. */
}tL2SecondaryVlanInfo;


/* Sizing Params enum */
enum
{
    L2IWF_L2PORT_INFO_SIZING_ID = 0,
    L2IWF_L2VLAN_INFO_SIZING_ID,
    L2IWF_L2PORT_CH_INFO_SIZING_ID,
    L2IWF_L2VLN_FID_MST_INST_SIZING_ID,
    L2IWF_L2CONTEXT_INFO_SIZING_ID,
    L2IWF_L2PNAC_MAC_INFO_SIZING_ID,
#ifdef PB_WANTED
    L2IWF_L2VID_TRANS_SIZING_ID,
    L2IWF_L2CVLN_PORT_SIZING_ID,
#endif
    L2IWF_L2PORT_VLN_INFO_SIZING_ID,
    L2IWF_IFTYPE_PROT_INFO_SIZING_ID,
    L2IWF_L2LLDP_APP_INFO_SIZING_ID,
    L2IWF_PVLAN_TABLE,
    L2IWF_PVLAN_SECONDARY_VLAN_TABLE,
 L2IWF_L2MST_INFO_SIZING_ID,
    VLAN_NO_SIZING_ID
};
#endif
