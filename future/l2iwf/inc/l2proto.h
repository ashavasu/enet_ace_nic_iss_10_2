/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: l2proto.h,v 1.55 2017/08/01 13:49:26 siva Exp $
 *
 * Description: This file contains all the function prototypes
 *              used by the L2IWF Module.
 *
 *******************************************************************/


#ifndef _L2PROTO_H_
#define _L2PROTO_H_

#include "cli.h"

/***********        Internal routines       ***************/
INT4 L2IwfHandleMemInit (VOID);
INT4 L2IwfCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId,
                      tCfaIfInfo *pCfaIfInfo);
INT4
L2IwfClassifyFrame (UINT4 u4ContextId, tCRU_BUF_CHAIN_DESC * pFrame,
                    tMacAddr DestAddr, tL2ProtStatus * pL2ProtStatus,
                    UINT2 u2PbPortType, UINT4 u4BridgeMode, UINT4 u4ModuleId);
INT4 L2IwfIsEcfmControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, 
                               UINT1 *pu1PktType);
INT4 L2IwfIsErpsPacket (tCRU_BUF_CHAIN_HEADER * pBuf); 



INT4 L2IwfCreateContext (UINT4 u4ContextId);
INT4 L2IwfDeleteContext (UINT4 u4ContextId);
INT4 L2IwfSelectContext (UINT4 u4ContextId);
INT4 L2IwfReleaseContext (VOID);
INT4 L2IwfPnacSessCrtMemPoolAndRBTree (VOID);
INT4 L2IwfPortVlanCrtMemPoolAndRBTree (VOID);
INT4 L2IwfPnacSessDelMemPoolAndRBTree (VOID);
INT4 L2IwfPortVlanDelMemPoolAndRBTree (VOID);
INT4 L2IwfAddPortVlanEntries (UINT2 u2VlanId, tPortList IfAddPortList,
                              UINT1 *pu1VlanName);
INT4 L2IwfDeletePortVlanEntries (UINT2 u2VlanId, tPortList IfDelPortList);
INT4 L2IwfRBFreePortVlanInfo (tRBElem *pRBElem, UINT4 u4Arg);
INT4 L2IwfUpdatePortVlanEntry (UINT4 u4IfIndex, tVlanId VlanId,
                               UINT1 *pu1VlanName, UINT1 u1Action);
VOID L2IwfScanAndDeletePortVlanEntry (UINT4 u4IfIndex, UINT1 *pu1TxEnabled);
VOID L2IwfUpdateVlanFwdCount (tVlanId VlanId, UINT2 u2LocalPort,
                              BOOL1 bSetFlag);


INT4 
L2IwfProcessIgmpCtrlPkt(tCRU_BUF_CHAIN_HEADER * pBuf,UINT4 u4ContextId,
          UINT4 u4LogicalIfIndex, UINT4 u4IfIndex,
          tEnetV2Header *pEthHdr,tL2PortInfo *pL2PortEntry,
   UINT1 u1IgmpProtocolStatus);
INT4
L2IwfPvlanCrtMemPoolAndRBTree (VOID);
INT4
L2IwfPvlanDelMemPoolAndRBTree (VOID);
INT4
L2IwfRemSecVlansFromPrimaryVlan (tL2PvlanInfo *pPvlanInfo,
                                 tVlanId SecondaryVlanId);
INT4
L2IwfDeletePvlanEntry (tL2PvlanInfo *pPvlanInfo);

/* Routines to maintain/ access RBTree data */
tL2PortInfo * L2IwfGetIfIndexEntry (UINT4 u4IfIndex);
tL2PortInfo * L2IwfGetFirstIfIndexEntry (VOID);
tL2PortInfo * L2IwfGetNextIfIndexEntry (tL2PortInfo *);
INT4 L2IwfAddIfIndexEntry (tL2PortInfo *pL2PortEntry);
INT4 L2IwfRemoveIfIndexEntry (tL2PortInfo *pL2PortEntry);
INT4 L2IwfIfIndexTblCmpFn (tRBElem *pKey1, tRBElem *pKey2);
INT4 L2IwfFreePortEntry (tRBElem *pElem, UINT4 u4Arg);
INT4 L2IwfMacCmpEntry (tRBElem * pNodeA, tRBElem * pNodeB);
INT4 L2IwfWalkFnIsTunnelPort (tRBElem * pElem, eRBVisit visit,
                              UINT4 u4Level, void *pArg, void *pOut);

INT4 L2IwfSendPortCreateInd (UINT4 u4ContextId, UINT4 u4IfIndex, 
                                 UINT2 u2LocalPortId, tCfaIfInfo *pIfInfo);

VOID L2IwfSendPortDeleteInd (UINT4 u4IfIndex, tCfaIfInfo *pIfInfo);

INT4 L2IwfSendIntfTypeChangeInd (UINT4 u4ContextId, UINT1 u1IntfType);

INT4 L2IwfGetSispPortInfo (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, 
      UINT4 *pu4SharedPort);
INT4
L2IwfHigherLayerModulesIndication (UINT4 u4IfIndex, UINT1 u1Status,
                                   tL2IwfContextInfo * pL2IwfContextInfo);

INT4 L2IwfInitContextInfo (VOID);

VOID L2IwfConvToGlblIfIndexList (tLocalPortList LocalEgressPorts, 
                                 tPortList EgressPorts);
INT4 L2IwfUpdatePortVlanRBTree (UINT4 u4IfIndex, UINT2 u2VlanId,
                                UINT1 *pu1VlanName, UINT1 u1Action, 
                                UINT1 *pu1TxEnabled);
tL2PortVlanInfo *L2IwfGetPortVlanEntry (UINT4, UINT2 );

INT4 L2IwfGetPortVlanMembers (UINT4 u4IfIndex, UINT1 *pu1VlanList);

VOID L2IwfDelPortVlanEntriesForPorts (tPortList IfDelPortList);
VOID L2IwfResetAllVlanNameIfTxEnabled (VOID);
INT4 L2IwfPortVlanInfoCmpEntry (tRBElem *pNodeA, tRBElem *pNodeB);


/*************PROVIDER BRIDGE RELATED FUNCTION PROTOTYPEs****************/

INT4 L2IwfPbInitMemPools (VOID);
INT4 L2IwfPbDeInitMemPools (VOID);
INT4 L2IwfPBCmpPepEntry (tRBElem * pNodeA, tRBElem * pNodeB);
VOID L2IwfCreateProviderEdgePort (UINT2 u2Port, tVlanId SVlanId);
VOID L2IwfDeleteProviderEdgePort (UINT2 u2Port, tVlanId SVlanId);
VOID L2IwfPepOperStatusIndication(UINT2 u2Port, tVlanId SVlanId, UINT1 u1OperStatus);
INT4 L2IwfValidateBridgeMode (UINT4 u4BridgeMode);
INT4 L2IwfPbCmpPepEntry (tRBElem * pNodeA, tRBElem * pNodeB);
INT4 L2IwfPbUtilDeletePEPEntry (UINT2 u2LocalPortId, tVlanId SVlanId);

INT4 L2IwfHandleIfPnacFrame (tCRU_BUF_CHAIN_HEADER * pBuf, tPktHandleInfo * pPktHandleInfo, 
                             UINT1 u1FrameType, UINT1 u1ProtTunnelStatus);

INT4 L2IwfHandleIfLaFrame (tCRU_BUF_CHAIN_HEADER * pBuf, tPktHandleInfo * pPktHandleInfo,
                           UINT4 *pu4LogicalIfIndex, UINT1 u1ProtTunnelStatus,
      UINT1 u1FrameType);
INT4
L2IwfMiGetFdbListInInstance (UINT4 u4ContextId, UINT2 u2Instance, 
                             tVlanFlushInfo *pVlanFlushInfo);
         
INT4
L2IwfTransmitFrameFromCbp (UINT4 u4ContextId, UINT2 u2LocalPort,
                           tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag, 
                           tPbbTag * pPbbTag, UINT1 u1FrameType);
        
tCRU_BUF_CHAIN_HEADER * L2iwfDuplicateCruBuf (tCRU_BUF_CHAIN_HEADER *
                                                 pSrcCruBuf);

VOID
l2iwfPktFree (tCRU_BUF_CHAIN_HEADER * pBuf);


       
VOID L2iwfAppendIsidTagToFrame PROTO ((tCRU_BUF_CHAIN_DESC *, tPbbTag*));
VOID L2iwfUpdateIsidTag PROTO ((tCRU_BUF_CHAIN_DESC *, tPbbTag*));

VOID
IsidFormTag (tPbbTag *pPbbTag, UINT4 *pu4Tag);

VOID
L2iwfFormBTag (tPbbTag PbbTag, UINT2 *pu2Tag);

VOID
L2iwfCheckAndRemoveItagFromFrame (tCRU_BUF_CHAIN_DESC * pFrame);

INT4
L2IwfPbbGetCbpFwdPortListForIsid ( UINT4 u4ContextId,
                          UINT2 u2Port, UINT4 u4Isid ,
         tLocalPortList IfFwdPortList); 
        
INT4
L2iwfAstHandleInFrame (UINT4 u4ContextId, UINT2 u2LocalPort,
    tCRU_BUF_CHAIN_DESC * pBuf);        


/****************Functions used in l2pkt.c *************************/

#if defined IGS_WANTED || MLDS_WANTED
INT4
L2IwfSnoopEnqueuePkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                       UINT4 u4LogicalIfIndex, UINT4 u4IfIndex,
                       tEnetV2Header * pEthHdr, UINT2 u2PbPortType); 
#endif
INT4
L2IwfGetTagInfo (UINT4 u4ContextId, UINT2 u2LocalPort, tCRU_BUF_CHAIN_DESC * pBuf,
                 tVlanTag * pVlanTag, tPbbTag * pPbbTag,
                 UINT1 *pu1IngressAction, UINT4 *pu4InOffset,
                 UINT4 *pu4TagOffSet, UINT1 u1FrameType, UINT1 u1BrgPortType,
                 UINT1 *pu1IsSend);
INT1 L2iwfPbbGetPipVipWithIsid PROTO ((UINT4, UINT2, UINT4, UINT2 *, UINT2 *));

INT4
L2IwfHlUpdateSispStatusOnPort (UINT4 u4IfIndex, UINT4 u4ContextId, 
          UINT1 u1Status);

/* Prototypes for IfType protocol deny table */
tIfTypeProtoDenyInfo *
L2IwfIfTypeProtoDenyGetNode (INT4 i4ContextId, INT4 i4IfType,
                             INT4 i4BrgPortType, INT4 i4Protocol);

tIfTypeProtoDenyInfo *
L2IwfIfTypeProtoDenyCreateNode (VOID);

tIfTypeProtoDenyInfo *
L2IwfIfTypeProtoDenyGetNextNode (INT4 i4ContextId, INT4 i4IfType,
                                 INT4 i4BrgPortType, INT4 i4Protocol);

INT4
L2IwfIfTypeProtoDenyAddNode (INT4 i4ContextId,
                             tIfTypeProtoDenyInfo *pIfTypeProtoDenyInfoNode);

INT4
L2IwfIfTypeProtoDenyFreeNode (tIfTypeProtoDenyInfo *pIfTypeProtoDenyInfoNode);

INT4
L2IwfIfTypeProtoDenyEntryCmp (tRBElem * pKey1, tRBElem * pKey2);

INT4
L2IwfIfTypeProtoDenyDelNode (INT4 i4ContextId,
                             tIfTypeProtoDenyInfo *pIfTypeProtoDenyInfoNode);

INT4
L2IwfGetIfTypeDenyProtoForPort (UINT4 u4IfIndex, tCfaIfInfo *pCfaIfInfo,
                                  UINT1 *pu1DenyProtocols);
INT4
L2IwfGetFdbInfo (tL2IwfFdbInfo *pL2IwfFdbInfo);

/* HITLESS RESTART */
INT4
L2IwfIsApsSteadyStatePacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *u4ApsFlag);
INT4
L2IwfSendApsSteadyStatePkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
        UINT4 u4PktSize, UINT4 u4ApsFlag);
UINT1
L2IwfRedGetHRFlag (VOID);

INT4
L2IwfHandleIfLldpFrame (tCRU_BUF_CHAIN_HEADER * pBuf, 
                        tPktHandleInfo * pPktHandleInfo, UINT1 u1FrameType,
                        UINT1 u1ProtTunnelStatus);

VOID
L2IwfGetOuterVlanInfoFromFrame (tCRU_BUF_CHAIN_DESC * pBuf, UINT2 *pu2OuterVlanId);

INT4
L2IwfGetACIfIndexFromFrame (tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4IfIndex, UINT4 *pu4AcIfIndex);

INT4
L2IwfCalculateVidDigest (UINT4 u4ContextId);

INT4 
L2IwfSendVidDigestToLLDP (UINT4 u4ContextId, UINT4 u4CRC32);

extern INT4 CfaIvrSetIpAddress (tCliHandle, UINT4, UINT4,  UINT4);
extern INT4 CfaUpdtIvrInterface PROTO ((UINT4 u4IfIndex, UINT1 u1Status));
extern INT4 CfaIpIfCreateIpInterface PROTO ((UINT4   u4CfaIfIndex));
extern INT4 CfaGetIfName (UINT4 u4IfIndex, UINT1 *au1IfName);
extern INT4 CfaGetIfOperMauType PROTO((UINT4 u4IfIndex, INT4 i4MauIndex,
                                       UINT4 *pu4RetOperMauType));

#ifdef ICCH_WANTED
INT4 L2IwfLaUpdtPortIsolationEntry (UINT4 u4IfIndex, UINT2 u2AggId);
#endif

#endif
