/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: l2pb.h,v 1.3 2007/02/01 14:55:29 iss Exp $
 *
 * Description: This file contains all the typedefs used by the L2IWF 
 *              and Provider Bridging modules.
 *
 *******************************************************************/
#ifndef _L2PB_H
#define _L2PB_H_

typedef struct L2CVlanPortEntry {
   tRBNodeEmbd    CVlanPortRbNode;
   UINT2          u2Port;        /* HlPortId of CEP attached with 
                                  * the C-VLAN component.           */
   tVlanId        SVid;          /* SVID associated with this port. */
   UINT1          u1PortState;   /* Spanning Tree Port State.       */
   UINT1          u1OperStatus;  /* Oper Status of this port.       */
   BOOL1          bOperEdge;     /* Port Oper Edge status.          */
   UINT1          au1Reserved[1];
}tL2CVlanPortEntry;

typedef struct _VidtransEntry {
    tRBNodeEmbd  IngVidTransNode;  /* Ingress RB tree node related 
                                    * information for this node.           */
    tRBNodeEmbd  EgrVidTransNode;  /* Egress RB tree node related 
                                    * information for this node.           */
    UINT2        u2Port;           /* Port on which vid translation to   
                                    * be done.                             */
    tVlanId      LocalVid;         /* Local VlanId                         */
    tVlanId      RelayVid;         /* Relay VlanId                         */
    UINT1        u1RowStatus;      /* SNMP row status for this entry.      */
    UINT1        au1Pad[1];
}tL2VidTransEntry;


/* Proto-types for provider bridge related functions. */
/* Vid translation table related prototypes. */
INT4 L2IwfPbIngVidTransTableCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);
INT4 L2IwfPbEgrVidTransTableCmp (tRBElem * pRBElem1, tRBElem * pRBElem2);

INT4 L2IwfPbFreeVidTransEntry (tRBElem * pElem, UINT4 u4Arg);

INT4 L2IwfPbCreateVidTransEntry (UINT2 u2LocalPort, tVlanId LocalVid);
INT4 L2IwfPbDelVidTransEntry (UINT2 u2LocalPort, tVlanId LocalVid);

INT4 L2IwfPbSetRelayVidForVidTransEntry (UINT2 u2LocalPort, tVlanId LocalVid, 
                                       tVlanId RelayVid);

tL2VidTransEntry * L2IwfPbGetVidTransEntryInCtxt (UINT2 u2LocalPort, 
		tVlanId SearchVid, UINT1 u1IsLocalVid);

INT4 L2IwfPbGetRelayVidFromLocalVidInCtxt (UINT2 u2LocalPort, tVlanId LocalVid, 
                                         tVlanId *pRelayVid);

INT4 L2IwfPbGetLocalVidFromRelayVidInCtxt (UINT2 u2LocalPort, tVlanId RelayVid, 
                                         tVlanId *pLocalVid);

tL2CVlanPortEntry * L2IwfPbGetPepEntry (UINT2 u2Port, tVlanId SVlanId);

#endif






