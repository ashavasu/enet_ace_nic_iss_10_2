/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: l2macro.h,v 1.81 2017/10/10 11:57:21 siva Exp $
 *
 * Description: This file contains all the macros used by L2IWF module
 *
 *******************************************************************/


#ifndef _L2MACRO_H_
#define _L2MACRO_H_


/* *****  FSAP Macros  ***** */
#define L2IWF_CREATE_SEMAPHORE(pu1Sem, u4Count, u4Flags, pSemId) \
        OsixCreateSem ((pu1Sem), (u4Count), (u4Flags), (pSemId))

#define L2IWF_CREATE_MEMPOOL(BlockSize, NumBlocks, MemoryType, pPoolId) \
        MemCreateMemPool (BlockSize, NumBlocks, MemoryType, pPoolId)

#define L2IWF_DELETE_MEMPOOL(PoolId) \
        MemDeleteMemPool (PoolId)

#define L2IWF_DELETE_SEMAPHORE(SemId)   OsixSemDel(SemId)

#define L2IWF_ALLOCATE_MEMBLOCK(PoolId) \
        MemAllocMemBlk (PoolId)

#define L2IWF_RELEASE_MEMBLOCK(PoolId, pBlock) \
        MemReleaseMemBlock (PoolId, (UINT1 *) pBlock)

#define L2IWF_PBB_SHUTDOWN_STATUS()\
        (gL2GlobalInfo.u1PbbShutdownStatus)

#define L2IWF_CVLAN_PORT_TBL() \
        (gpL2Context->CVlanPortTable)

#define L2IWF_PNAC_MAC_TBL() \
        (gL2GlobalInfo.SessMacTable)

#define L2IWF_BRIDGE_MODE() \
        (gpL2Context->u4BridgeMode)

#define L2IWF_INTERFACE_TYPE() \
        (gpL2Context->InterfaceType)

#define L2IWF_CNP_COUNT() \
        (gpL2Context->u4CnpCount)

#define L2IWF_INTERNAL_COUNT() \
        (gpL2Context->u4InternalCount)

#define L2IWF_ALL_TO_ONE_BUNDLING_STATUS() \
        (gpL2Context->u4AllToOneBundlingStatus)

#define L2IWF_ADMIN_INTERFACE_TYPE_FLAG() \
        (gpL2Context->u1AdminIntfTypeFlag)

#define L2IWF_GET_BRG_MODE_FROM_CTXTID(u4ContextId) \
         (L2IWF_CONTEXT_INFO(u4ContextId)->u4BridgeMode)

#define L2IWF_MAX_NUM_PEP   AST_MAX_SERVICES_PER_CUSTOMER * L2IWF_MAX_CVLAN_CONTEXT

#define L2IWF_TAKE_SEM  OsixSemTake
#define L2IWF_GIVE_SEM  OsixSemGive
  
/* *****  Data structure access Macros  ***** */
    
/* 
 * The following macros can be used ONLY AFTER a Context 
 * has been selected. Any port number parameters in these
 * macros should be local port numbers.
 */
    
#define L2IWF_PORT_INFO(u2PortIndex) \
        ((gpL2Context->apL2PortInfo [(u2PortIndex) - 1]))
    
#define L2IWF_VLAN_INFO(VlanId) \
        (gpL2Context->apL2VlanInfo[(VlanId)-1])

#define L2IWF_MST_INFO(InstId) \
        (gpL2Context->apL2MstInstInfo[(InstId)])

#define L2IWF_INSTANCE_ACTIVE_PTR \
        (gpL2Context->pInstanceActive)

#define L2IWF_INSTANCE_ACTIVE(u2InstanceId) \
        (gpL2Context->pInstanceActive [(u2InstanceId)])

#define L2IWF_VID_INST_MAP_ARRAY() \
        (gpL2Context->paL2VidFidInstMap)
    
#define L2IWF_ING_VID_TRANS_TBL() \
        (gpL2Context->IngVidTransTable)

#define L2IWF_EGR_VID_TRANS_TBL() \
        (gpL2Context->EgrVidTransTable)

#define L2IWF_VLAN_ENH_FILTERING_STATUS() \
        (gpL2Context->b1EnhancedFilterStatus)

#define L2IWF_CFA_DENY_PROTOCOL_LISTCOUNT() \
        (gpL2Context->u4DenyProtocolListCount)

#define L2IWF_STP_PERFORMANCE_DATA_STATUS() \
        (gpL2Context->b1PerfDataStatus)

#define L2IWF_VLAN_LEARNING_MODE() \
        (gpL2Context->u1VlanLearningType)

#define INCR_L2IWF_CONTEXT_DISCARD_TUNNEL_INFO(ContextId) \
        (gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4DiscardCounter)++

#define DECR_L2IWF_CONTEXT_DISCARD_TUNNEL_INFO(ContextId) \
    if(gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4DiscardCounter > 0) \
    { (gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4DiscardCounter)--; }
  

#define INCR_L2IWF_CONTEXT_PEER_TUNNEL_INFO(ContextId) \
        (gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4PeerCounter)++

#define DECR_L2IWF_CONTEXT_PEER_TUNNEL_INFO(ContextId) \
    if(gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4PeerCounter>0)\
    {(gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4PeerCounter)-- ; }

#define INCR_L2IWF_CONTEXT_TUNNEL_TUNNEL_INFO(ContextId) \
        (gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4TunnelCounter)++

#define DECR_L2IWF_CONTEXT_TUNNEL_TUNNEL_INFO(ContextId) \
    if(gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4TunnelCounter > 0) \
    { (gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4TunnelCounter)--; }

#define L2IWF_CONTEXT_TUNNEL_TUNNEL_INFO(ContextId) \
        (gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4TunnelCounter)

#define L2IWF_CONTEXT_PEER_TUNNEL_INFO(ContextId) \
        (gL2GlobalInfo.apL2ContextInfo[(ContextId)]->u4PeerCounter)

#define L2IWF_VID_INST_MAP(VlanId) \
        ((tL2VlanFidMstInstInfo*) \
         (gpL2Context->paL2VidFidInstMap + (VlanId-1)))->u2MstInst

#define L2IWF_MST_VLAN_LIST(InstId) \
        (gpL2Context->apL2MstInstInfo[(InstId)]->au1VlanList) 
#define L2IWF_MST_FDB_COUNT(InstId) \
        (gpL2Context->apL2MstInstInfo[(InstId)]->u2FdbCount)

#define L2IWF_VID_FID_MAP(VlanId) \
        ((tL2VlanFidMstInstInfo*) \
         (gpL2Context->paL2VidFidInstMap + (VlanId-1)))->u2Fid

#define L2IWF_INST_PORT_STATE_PTR(u2Port) \
        ((L2IWF_PORT_INFO ((u2Port)))->pInstPortState)

#define L2IWF_INST_PORT_STATE(u2MstInst, u2Port) \
        ((L2IWF_PORT_INFO ((u2Port)))->pInstPortState[(u2MstInst)])

#define L2IWF_PORT_ACTIVE(u2PortIndex) \
    (((L2IWF_PORT_INFO (u2PortIndex)) != NULL)? OSIX_TRUE : OSIX_FALSE)
    
#define L2IWF_PORT_IFINDEX(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->u4IfIndex)
    
#define L2IWF_PORT_TYPE(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->u1PortType)
    
#define L2IWF_PORT_ACCP_FRAME_TYPE(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->u1PortAccpFrmType)
    
#define L2IWF_PORT_PVID(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->Pvid)

#define L2IWF_PORT_ALL_VLAN(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->au1AllVlanList)

#define L2IWF_PORT_UNTAGGED_VLAN(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->au1UnTaggedVlanList)

#define L2IWF_PORT_GVRP_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->u1GvrpStatus)

#define L2IWF_PORT_GMRP_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->u1GmrpStatus)

#define L2IWF_PORT_MVRP_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->u1MvrpStatus)

#define L2IWF_PORT_MMRP_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->u1MmrpStatus)

#define L2IWF_PORT_STP_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->u1StpStatus)

#define L2IWF_PORT_OPER_CONTROL_DIR(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->u2OperControlDir)

#define L2IWF_PORT_AUTH_CONTROL_PORT_CONTROL(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->u2AuthControlPortControl)

#define L2IWF_PORT_AUTH_CONTROL_PORT_STATUS(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->u2AuthControlPortStatus)

#define L2IWF_AGG_INDEX_FOR_PORT(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->u4AggIndex)

#define L2IWF_PORT_TUNNEL_STATUS(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->u1TunnelStatus)

#define L2IWF_PORT_OPER_EDGE(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->bOperEdge)

#define L2IWF_PORT_BRIDGE_OPER_STATUS(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->u1BridgePortOperStatus)
    
#define L2IWF_PORT_VID_TRANS_STATUS(u2PortIndex) \
        ((L2IWF_PORT_INFO ((u2PortIndex)))->u1VidTransStatus)

#define L2IWF_PB_PORT_TYPE(pL2PortInfo) \
        ((pL2PortInfo)->u2PbPortType)

#ifdef VLAN_WANTED    
#define L2IWF_VLAN_ACTIVE(VlanId) \
        (((L2IWF_VLAN_INFO ((VlanId))) == NULL)? OSIX_FALSE:OSIX_TRUE)
#else
#define L2IWF_VLAN_ACTIVE(VlanId)  L2IwfIsVlanActive(VlanId)
#endif

#define L2IWF_VLAN_TUNNEL_STATUS(VlanId) \
        ((pL2VlanInfo)->u1TunnelStatus)

#define L2IWF_VLAN_VLANID(VlanId) \
        ((L2IWF_VLAN_INFO ((VlanId)))->VlanId)

#define L2IWF_VLAN_EGRESS_PORTS(VlanId) \
        ((L2IWF_VLAN_INFO ((VlanId)))->EgressPorts)
    
#define L2IWF_VLAN_UNTAG_PORTS(VlanId) \
        ((L2IWF_VLAN_INFO ((VlanId)))->UntagPorts)
    
#define L2IWF_VLAN_FORWARD_PORTS(VlanId) \
        ((L2IWF_VLAN_INFO ((VlanId)))->ForwardPorts)

#define L2IWF_VLAN_SERVICE_TYPE(VlanId) \
        ((L2IWF_VLAN_INFO ((VlanId)))->u1ServiceType)

#define L2IWF_FCOE_VLAN_TYPE(VlanId) \
 ((L2IWF_VLAN_INFO ((VlanId)))->u1FCoEVlanType)

#define L2IWF_VLAN_FLAGS(VlanId) \
        ((L2IWF_VLAN_INFO ((VlanId)))->VlanFlags)

#define L2IWF_GET_PNAC_HL_PORT_OPER_STATUS(u2PortIndex, u1OperStatus)      \
                                                                           \
        u1OperStatus = CFA_IF_DOWN;                                        \
                                                                           \
        if (L2IWF_PORT_AUTH_CONTROL_PORT_STATUS (u2PortIndex) ==           \
            PNAC_PORTSTATUS_AUTHORIZED)                                    \
        {                                                                  \
            /* Authorized */                                               \
            u1OperStatus = CFA_IF_UP;                                      \
        }                                                                  \
        else                                                               \
        {                                                                  \
            /* Unauthorized */                                             \
            if (L2IWF_PORT_AUTH_CONTROL_PORT_CONTROL (u2PortIndex) ==      \
                PNAC_PORTCNTRL_AUTO)                                       \
            {                                                              \
                /* UnAuthorized Auto */                                    \
                if (L2IWF_PORT_OPER_CONTROL_DIR (u2PortIndex) ==           \
                    PNAC_CNTRLD_DIR_IN)                                    \
                {                                                          \
                    /* UnAuthorized Auto In */                             \
                    u1OperStatus = CFA_IF_UP;                              \
                }                                                          \
            }                                                              \
        }                                                                 


/* 
 * The following macros can be used anywhere
 * (even before a Context has been selected).
 */
    
#define L2IWF_CONTEXTINFO_POOLID() \
        (gL2GlobalInfo.ContextInfoPoolId)
    
#define L2IWF_VLANINFO_POOLID() \
        (gL2GlobalInfo.VlanInfoPoolId)
    
#define L2IWF_PVLANINFO_POOLID() \
        (gL2GlobalInfo.PvlanTablePoolId)

#define L2IWF_MSTINFO_POOLID() \
        (gL2GlobalInfo.MstInfoPoolId)
    
#define L2IWF_PVLAN_SECONDARY_VLAN_POOLID() \
        (gL2GlobalInfo.PvlanSecondaryVlanPoolId)

#define L2IWF_PORTINFO_POOLID() \
        (gL2GlobalInfo.PortInfoPoolId)


#define L2IWF_PEPINFO_POOLID() \
        (gL2GlobalInfo.PepInfoPoolId)

#define L2IWF_PNAC_MAC_SESS_POOLID() \
        (gL2GlobalInfo.MacSessPoolId)

#define L2IWF_IFTYPE_PROTO_DENY_POOLID() \
        (gL2GlobalInfo.IfTypeProtoDenyPoolId)

#define L2IWF_LLDP_APPL_POOLID() \
        (gL2GlobalInfo.LldpApplPoolId)

#define L2IWF_PORT_CHANNEL_INFO_POOLID() \
        (gL2GlobalInfo.PortChannelInfoPoolId)

#define L2IWF_VLAN_FID_MST_INST_POOLID() \
        (gL2GlobalInfo.VlanFidMstInstPoolId)

#define L2IWF_DOT1X_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1Dot1xProtocolStatus)

#define L2IWF_LA_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1LacpProtocolStatus)

#define L2IWF_STP_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1StpProtocolStatus)

#define L2IWF_GVRP_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1GvrpProtocolStatus)

#define L2IWF_GMRP_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1GmrpProtocolStatus)

#define L2IWF_IGMP_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1IgmpProtocolStatus)

#define L2IWF_MVRP_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1MvrpProtocolStatus)

#define L2IWF_MMRP_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1MmrpProtocolStatus)

#define L2IWF_ELMI_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1ElmiProtocolStatus)

#define L2IWF_LLDP_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1LldpProtocolStatus)

#define L2IWF_ECFM_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1EcfmProtocolStatus)

#define L2IWF_EOAM_TUNNEL_STATUS(pPortInfo) \
   ((pPortInfo)->ProtocolStatus.u1EoamProtocolStatus)

#define L2IWF_OVERRIDE_OPTION_STATUS(pPortInfo) \
   ((pPortInfo)->u1OverrideOption)

/* VLAN Macros */
#define L2IWF_DOT1X_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1Dot1xProtocolStatus)

#define L2IWF_LA_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1LacpProtocolStatus)

#define L2IWF_STP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1StpProtocolStatus)

#define L2IWF_GVRP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1GvrpProtocolStatus)

#define L2IWF_GMRP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1GmrpProtocolStatus)

#define L2IWF_IGMP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1IgmpProtocolStatus)

#define L2IWF_MVRP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1MvrpProtocolStatus)

#define L2IWF_MMRP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1MmrpProtocolStatus)

#define L2IWF_ELMI_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1ElmiProtocolStatus)

#define L2IWF_LLDP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1LldpProtocolStatus)

#define L2IWF_ECFM_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1EcfmProtocolStatus)

#define L2IWF_EOAM_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1EoamProtocolStatus)

#define L2IWF_PTP_TUNNEL_STATUS_PER_VLAN(pVlanInfo) \
   ((pVlanInfo)->ProtocolStatus.u1PtpProtocolStatus)

#define L2IWF_OTHER_TUNNEL_STATUS_PER_VLAN(pVlanInfo, index) \
   ((pVlanInfo)->ProtocolStatus.u1OtherProtocolStatus[index])
/* VLAN Macros End */

#define L2IWF_VID_TRANS_ENTRY_POOLID() \
        (gL2GlobalInfo.VidTransMemPoolId)

#define L2IWF_CONTEXT_INFO(ContextId) \
        (gL2GlobalInfo.apL2ContextInfo[(ContextId)])

#define L2IWF_PORT_CHANNEL_INFO(u2AggIndex) \
        ((tL2PortChannelInfo *)             \
         ((gL2GlobalInfo.paL2PortChannelInfo) + \
          ((u2AggIndex) - BRG_MAX_PHY_PORTS - 1)))
    
#define L2IWF_PORT_CHANNEL_VALID(u2PortIndex) \
        ((L2IWF_PORT_CHANNEL_INFO ((u2PortIndex)))->bValid)

#define L2IWF_PORT_CHANNEL_CONFIGURED(u2PortIndex) \
        ((L2IWF_PORT_CHANNEL_INFO ((u2PortIndex)))->bConfigured)

#define L2IWF_PORT_CHANNEL_CONFIGURED(u2PortIndex) \
        ((L2IWF_PORT_CHANNEL_INFO ((u2PortIndex)))->bConfigured)

#define L2IWF_PORT_CHANNEL_CONFIGURED_PORT(u2AggIndex, u4Index) \
        ((L2IWF_PORT_CHANNEL_INFO ((u2AggIndex)))->au2ConfiguredPorts[(u4Index)])
    
#define L2IWF_PORT_CHANNEL_NUM_CONF_PORTS(u2AggIndex) \
        ((L2IWF_PORT_CHANNEL_INFO ((u2AggIndex)))->u2NumConfPorts)

#define L2IWF_PORT_CHANNEL_ACTIVE_PORT(u2AggIndex, u4Index) \
        ((L2IWF_PORT_CHANNEL_INFO ((u2AggIndex)))->au2ActivePorts[(u4Index)])
    
#define L2IWF_PORT_CHANNEL_NUM_ACTIVE_PORTS(u2AggIndex) \
        ((L2IWF_PORT_CHANNEL_INFO ((u2AggIndex)))->u2NumActivePorts)

#define L2IWF_PORT_CHANNEL_NEED_REPROGRAM(u2AggIndex) \
        ((L2IWF_PORT_CHANNEL_INFO ((u2AggIndex)))->bAddLinkInProgress)

#define L2IWF_GLOBAL_IFINDEX_TREE() \
        (gL2GlobalInfo.GlobalIfIndexTable)

#define L2IWF_IFTYPE_PROTO_DENY_TREE() \
        (gpL2Context->IfTypeProtoDenyTable)

#define L2IWF_IFENTRY_TUNNEL_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->u1TunnelStatus)
    
#define L2IWF_IFENTRY_TYPE(pL2PortInfo) \
        ((pL2PortInfo)->u1PortType)
    
#define L2IWF_IFENTRY_ACCP_FRAME_TYPE(pL2PortInfo) \
        ((pL2PortInfo)->u1PortAccpFrmType)
    
#define L2IWF_IFENTRY_PVID(pL2PortInfo) \
        ((pL2PortInfo)->Pvid)

#define L2IWF_IFENTRY_GVRP_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->u1GvrpStatus)

#define L2IWF_IFENTRY_MVRP_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->u1MvrpStatus)

#define L2IWF_IFENTRY_LOCAL_PORT(pL2PortInfo) \
        ((pL2PortInfo)->u2LocalPortId)
    
#define L2IWF_IFENTRY_CONTEXT_ID(pL2PortInfo) \
        ((pL2PortInfo)->u4ContextId)
    
#define L2IWF_IFENTRY_BRIDGE_OPER_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->u1BridgePortOperStatus)
    
#define L2IWF_IFENTRY_PORT_STATE(pL2PortInfo, u2MstInst) \
        ((pL2PortInfo)->pInstPortState[(u2MstInst)])
    
#define L2IWF_IFENTRY_OPEREDGE_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->bOperEdge)
    
#define L2IWF_IFENTRY_AGG_INDEX(pL2PortInfo) \
        ((pL2PortInfo)->u4AggIndex)
    
#define L2IWF_IFENTRY_AUTH_CONTROL_PORT_CONTROL(pL2PortInfo) \
        ((pL2PortInfo)->u2AuthControlPortControl)

#define L2IWF_IFENTRY_OPER_CONTROL_DIR(pL2PortInfo) \
        ((pL2PortInfo)->u2OperControlDir)

#define L2IWF_IFENTRY_AUTH_CONTROL_PORT_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->u2AuthControlPortStatus)
    
#define L2IWF_DEFAULT_VLAN_ID() gL2DefaultVlanId

   /* Macros for accessing the port bit array */

   /* 
    * This macro is used to find whether the specified port u2Port is
    * a member of the specified port list.
    */
   /* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.*/
#define L2IWF_VLAN_IS_MEMBER_PORT(au1PortArray, u2Port, u1Result) \
        {\
           UINT2 u2PortBytePos;\
           UINT2 u2PortBitPos;\
           u2PortBytePos = (UINT2)(u2Port / VLAN_PORTS_PER_BYTE);\
           u2PortBitPos  = (UINT2)(u2Port % VLAN_PORTS_PER_BYTE);\
    if (u2PortBitPos  == 0) {u2PortBytePos = (UINT2) (u2PortBytePos - 1);} \
           \
           if ((u2PortBytePos < CONTEXT_PORT_LIST_SIZE) && (au1PortArray[u2PortBytePos] \
                & gau1VlanPortBitMaskMap[u2PortBitPos]) != 0) {\
           \
              u1Result = VLAN_TRUE;\
           }\
           else {\
           \
              u1Result = VLAN_FALSE; \
           } \
        }

#define L2IWF_FWD_PORT_COUNT(VlanId) \
        L2IWF_VLAN_INFO(VlanId)->u2FwdPortCount

#define L2IWF_IFENTRY_OPER_P2P_STATUS(pL2PortInfo) \
        ((pL2PortInfo)->bOperPointToPoint)

#define L2IWF_PORT_VLAN_MEMBLK_COUNT VLAN_DEV_MAX_NUM_VLAN
#define L2IWF_ADD      1
#define L2IWF_DELETE   2
#define L2IWF_UPDATE   3

#define L2IWF_MAX_PVLAN_ENTRIES VLAN_MAX_PRIMARY_VLANS + \
                                VLAN_MAX_ISOLATED_VLANS + \
                                VLAN_MAX_COMMUNITY_VLANS


#define L2IWF_BUF_GET_OPCODE(pBuf) \
    (UINT1) (pBuf->ModuleData.u4Reserved3 & 0x000000FF)

#define L2IWF_BUF_SET_OPCODE(pBuf,u1OpCode)\
do \
   {\
    UINT4 u4Byte; \
    u4Byte = pBuf->ModuleData.u4Reserved3; \
    u4Byte = u4Byte&0xffffff00; \
    u4Byte = u4Byte|u1OpCode; \
    pBuf->ModuleData.u4Reserved3 = u4Byte;\
    }\
while(0)

#define L2IWF_INTERNAL_PORT (u4IfIndex) \
   CfaIsInternalInterface(u4IfIndex) 

#define L2IWF_IS_INTERFACE_RANGE_VALID(u4IfIndex) \
   (((((u4IfIndex > L2IWF_BRG_MAX_PHY_PLUS_LOG_PORTS) &&\
      (u4IfIndex < CFA_MIN_INTERNAL_IF_INDEX)) &&\
      ((u4IfIndex > CFA_MAX_PSW_IF_INDEX) ||\
      (u4IfIndex < CFA_MIN_PSW_IF_INDEX) ||\
      (u4IfIndex > CFA_MAX_NVE_IF_INDEX) ||\
      (u4IfIndex < CFA_MIN_NVE_IF_INDEX) ||\
      (u4IfIndex > CFA_MAX_TAP_IF_INDEX) ||\
      (u4IfIndex < CFA_MIN_TAP_IF_INDEX))) ||\
      (u4IfIndex > BRG_MAX_PHY_PLUS_LAG_INT_PORTS)) ? L2IWF_FALSE : L2IWF_TRUE)

#define L2IWF_IS_MULTICAST_ADDR(MacAddr)\
    ((MacAddr[0]&0x01)?(L2IWF_TRUE):(L2IWF_FALSE))

/* HITLESS RESTART */
#define L2IWF_APS_PDU_HDR_LEN        19 /* DEST_MAC(6) + SRC_MAC(6) +
                                           Ether Type(2) + VLAN Identifier(2) +
                                           OAM PDU Type(2) + MEL/Version(1) */
#define L2IWF_APS_OPCODE_OFFSET      L2IWF_APS_PDU_HDR_LEN
#define L2IWF_APS_REQ_STATE_OFFSET   L2IWF_APS_OPCODE_OFFSET + 3
                                  /* Offset(1)  + TLV Offset(1) + Flags(1) */

#define L2IWF_APS_STATUS_OFFSET      L2IWF_APS_REQ_STATE_OFFSET + 1

#define APS_INVALID  0
#define APS_SSP      1
#define RAPS_SSP     2

#define L2IWF_OPCODE_APS             39
#define L2IWF_OPCODE_RAPS            40

#define L2IWF_ELPS_SSP_REQ_STATE     0x0f

#define L2IWF_ERPS_SSP_REQ_STATE     0x00
#define L2IWF_ERPS_SSP_STATUS        0xC0

#define L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED 1
#define L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED 2

#define L2IWF_OVERRIDE_OPTION_ENABLE  1
#define L2IWF_OVERRIDE_OPTION_DISABLE 2
/* ELPS Steady state packet is got when the opcode is
 *  *  *  * 39 and the State is set to No Request */
#define IS_STEADY_STATE_PKT_FOR_ELPS(u4Opcode, \
        u1RequestOrState)\
        ((u1Opcode == L2IWF_OPCODE_APS \
          && u1RequestOrState == L2IWF_ELPS_SSP_REQ_STATE))

        /* ERPS Steady state packet is got when the opcode is
         *  *  *  * 40 and the State is set to No Request */
#define IS_STEADY_STATE_PKT_FOR_ERPS(u4Opcode, \
                u1RequestOrState, u1Status)\
                ((u1Opcode == L2IWF_OPCODE_RAPS \
                  && u1RequestOrState == L2IWF_ERPS_SSP_REQ_STATE\
                    && u1Status == L2IWF_ERPS_SSP_STATUS))

#define L2IWF_HR_STATUS()               L2IwfRedGetHRFlag()
#define L2IWF_HR_STATUS_DISABLE         RM_HR_STATUS_DISABLE
#define L2IWF_MBSM_NP_BULK_SYNC_STATUS() gu2MbsmNpBulkSyncStatus

#endif /* _L2MACRO_H_ */


             
