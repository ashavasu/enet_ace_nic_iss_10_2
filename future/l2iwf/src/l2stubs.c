/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: l2stubs.c,v 1.223 2018/02/09 09:41:16 siva Exp $
 *
 * Description: This file contains all the stub functions
 *
 *******************************************************************/
#ifndef _LRSTUB_C
#define _LRSTUB_C
#include "l2inc.h"
#include "lr.h"
#include "cfa.h"
#include "fsvlan.h"
#include "pbb.h"
#include "bridge.h"
#include "l2iwf.h"
#include "snp.h"
#include "rstp.h"
#include "mstp.h"
#include "pvrst.h"
#include "lldp.h"
#include "la.h"
#include "vcm.h"
#include "vcmcli.h"
#include "pnac.h"
#include "radius.h"
#include "diffsrv.h"
#include "iss.h"
#include "garp.h"
#include "mrp.h"
#include "cli.h"
#include "npapi.h"
#include "ip.h"
#include "elm.h"
#include "evcpro.h"
#include "lcm.h"
#include "uni.h"
#include "tac.h"
#include "qosxtd.h"
#include "srmbuf.h"
#include "ecfm.h"
#include "elps.h"
#include "isspi.h"
#include "msr.h"
#ifndef CLI_WANTED
#include "srmmemi.h"
#include "sshfs.h"
#endif
#ifndef GARP_WANTED
extern INT4
 
 
 
 
GarpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                                       INT4 i4LocalPort);
#endif
#if !defined(ISS_WANTED) && !defined(WSS_WANTED)
INT4                gi4SnmpMemInit = SNMP_FAILURE;
tMemPoolId          gSnmpOidListPoolId;
tMemPoolId          gSnmpOidTypePoolId;
tMemPoolId          gOctetStrPoolId;
tMemPoolId          gSnmpOctetListPoolId;
UINT4               AllocBlocks = 0;

tSNMP_OCTET_STRING_TYPE *allocmem_octetstring PROTO ((INT4));

VOID free_octetstring PROTO ((tSNMP_OCTET_STRING_TYPE *));

VOID SNMP_ReverseOctetString PROTO ((tSNMP_OCTET_STRING_TYPE * pInOctetStr,
                                     tSNMP_OCTET_STRING_TYPE * pOutOctetStr));
tSNMP_OID_TYPE     *alloc_oid PROTO ((INT4));

VOID free_oid       PROTO ((tSNMP_OID_TYPE *));

PUBLIC INT4         SnmpUtilDecodeOid (UINT1 *pu1InOidStr,
                                       tSNMP_OID_TYPE * pOutOidStr);
PUBLIC INT4         SnmpUtilEncodeOid (tSNMP_OID_TYPE * pInOidStr,
                                       UINT1 *pu1OutOidStr, UINT1 *u1ErrorCode);

#endif /* ISS_WANTED & WSS_WANTED */

#ifndef ECFM_WANTED
#define ECFM_TRUE                       TRUE
#define ECFM_FALSE                      FALSE
#endif

#ifndef CLI_WANTED
/* prototypes for stub entries  */
PUBLIC VOID        *FsCustMalloc (size_t);
PUBLIC VOID        *FsCustCalloc (size_t, size_t);
PUBLIC VOID         FsCustFree (void *);
PUBLIC VOID        *FsCustRealloc (void *, size_t);
#endif

#if !defined(ISS_WANTED) && !defined(WSS_WANTED)
UINT4               gu4AclOverLa = 1;
#endif /*ISS_WANTED & WSS_WANTED */
#ifndef CFA_WANTED
INT4 CfaIfmConfigNetworkInterface PROTO ((UINT1 u1OperCode, UINT4 u4IfIndex,
                                          UINT4 u4Flag,
                                          tIpConfigInfo * pIpConfigInfo));
UINT1               CfaFsCfaHwClearStats (UINT4 u4Port);
UINT1               CfaFsCfaHwSetMtu (UINT4 u4IfIndex, UINT4 u4MtuSize);
UINT1
 
            CfaFsHwGetStat (UINT4 u4IfIndex, INT1 i1StatType, UINT4 *pu4Value);

#endif /* CFA_WANTED */
#ifndef LA_WANTED
INT4
 
 
 
 
 
 
 
 LaVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                 UINT2 *pu2LocalPortId);
#endif
#ifndef VLAN_WANTED
INT4                VlanPortCfaDeleteSChannelInterface (UINT4 u4SChIfIndex);
INT4
 
 
 
 
 
 
   VlanPortCfaGetFreeSChIndexForUap (UINT4 u4UapIfIndex, UINT4 *pu4SChIfIndex);
#endif

#if !defined (CFA_WANTED) || !defined(ISS_WANTED)
INT4                gi4MibResStatus = MIB_RESTORE_INVALID_VALUE;
#endif /* CFA_WANTED || ISS_WANTED */

#ifndef CFA_WANTED
/*****************************************************************************
 *
 *    Function Name        : CfaSetIfInfo
 *
 *    Description          : This function updates the interface related params
 *                           assigned to this interface by the external modules.
 *
 *    Input(s)             : i4CfaIfParam, u4IfIndex, *pIfInfo 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred :None 
 *
 *    Global Variables Modified :None 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid 
 *                        CFA_FAILURE if u4IfIndex is Invalid 
 *****************************************************************************/
INT4
CfaSetIfInfo (INT4 i4CfaIfParam, UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    UNUSED_PARAM (i4CfaIfParam);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pIfInfo);
    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetIndexFromType     
 *
 *    Description         : This function is used to get the Interface index 
 *                          of an interface, given the interface type 
 *                          and port number.
 *
 *    Input(s)            : u4IfType   - interface type
 *                          pi1IfNum   - port number
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          port number
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/

INT4
CfaCliGetIndexFromType (UINT4 u4IfType, INT1 *pi1IfNum, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (u4IfType);
    UNUSED_PARAM (pi1IfNum);
    UNUSED_PARAM (pu4IfIndex);
    return CLI_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCliGetIndexFromSubType  
 *
 *    Description         : This function is used to get the Interface index 
 *                          of an interface, given the interface type, subtype 
 *                          and port number.
 *
 *    Input(s)            : u4IfType   - interface type
 *                          u4SubType  - interface sub type
 *                          pi1IfNum   - port number
 *
 *    Output(s)           : pu4IfIndex - IfIndex value of the given 
 *                          port number
 *
 *    Returns            : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/
INT4
CfaCliGetIndexFromSubType (UINT4 u4IfType, UINT4 u4IfSubType, INT1 *pi1IfNum,
                           UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (u4IfType);
    UNUSED_PARAM (u4IfSubType);
    UNUSED_PARAM (pi1IfNum);
    UNUSED_PARAM (pu4IfIndex);
    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsPhysicalInterface
 *
 *    Description         : This function validates the interface type
 *                          for the given interface index is Physical
 *                          or virtual.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : return CFA_TRUE if it's a Physical Interface
 *                         otherwise CFA_FALSE
 *
 *****************************************************************************/
UINT1               CfaIsPhysicalInterface
PROTO ((UINT4 u4IfIndex))
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_TRUE;
}

/*****************************************************************************
 *
 *    Function Name       :CfaIsLaggInterface 
 *
 *    Description         : This function checks whether  the interface 
 *                          is PortChannel interface or not.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_TRUE-If interface is a PortChannel interface
 *****************************************************************************/

UINT1
CfaIsLaggInterface (UINT4 u4IfIndex)
{

    UNUSED_PARAM (u4IfIndex);
    return CFA_FALSE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetIfInfo
 *
 *    Description          : This function returns the interface related params
 *                           assigned to this interface to the external modules.
 *
 *    Input(s)             : u4IfIndex, *pIfInfo 
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred :None 
 *
 *    Global Variables Modified :None 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid 
 *                        CFA_FAILURE if u4IfIndex is Invalid 
 *****************************************************************************/
INT4
CfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pIfInfo);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetInterfaceNameFromIndex
 *
 *    Description          : This function retrieves the interface name
 *                           for the given interface index.          
 *
 *    Input(s)             : u4Index - Interface Index
 *                           pu1Alias - Pointer to interface alias name
 *
 *    Output(s)            : puAlias - Pointer to interface alias name
 *
 *    Global Variables Referred :gapIfTable (Interface table) structure.
 *
 *    Global Variables Modified :None
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            :OSIX_SUCCESS if interface is valid
 *                        otherwise OSIX_FAILURE. 
 *****************************************************************************/
UINT4
CfaGetInterfaceNameFromIndex (UINT4 u4Index, UINT1 *pu1Alias)
{
    UNUSED_PARAM (u4Index);
    UNUSED_PARAM (pu1Alias);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfOperStatus                                   */
/*                                                                           */
/* Description        : This function gets the oper-status of the interface  */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1OperStatus:Pointer to the oper status             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS -if u4IfIndex is valid                   */
/*                      CFA_FAILURE -if u4IfIndex is Invalid                 */
/*****************************************************************************/
INT4
CfaGetIfOperStatus (UINT4 u4IfIndex, UINT1 *pu1OperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    *pu1OperStatus = CFA_IF_UP;
    return CFA_SUCCESS;
}

/*****************************************************************************
*
*    Function Name       : CfaCliConfGetIfName
*
*    Description         : This function returns Interface name for specified
*                          interface
*
*    Input(s)            : u4IfIndex - Interface Index
*
*    Output(s)           : pi1IfName - Pointer to buffer
*
*
*    Returns            : CLI_SUCCESS if name assigned for pi1IfName
*                         CLI_FAILURE if name is not assign pi1IfName
*                         CFA_FAILURE if u4IfIndex is not valid interface
*
*****************************************************************************/
INT4
CfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetLinkTrapEnabledStatus
 *
 *    Description          : This function returns the snmp trap link
 *                           status to the external modules.
 *
 *    Input(s)             : u4IfIndex, *pu1TrapStatus
 *
 *    Output(s)            : None
 *
 *    Global Variables Referred :None
 *
 *    Global Variables Modified :None
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid
 *                        CFA_FAILURE if u4IfIndex is Invalid
 *****************************************************************************/
INT1
CfaGetLinkTrapEnabledStatus (UINT4 u4IfIndex, UINT1 *pu1TrapStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1TrapStatus);
    return CFA_SUCCESS;
}

#ifndef CLI_WANTED
/*****************************************************************************
 *
 *    Function Name       : CfaCliGetIfName
 *
 *    Description         : This function is used to get the Interface name 
 *                          for given interface index.
 *                          This function should return a meaningful name for
 *                          the given interface index.
 *
 *    Input(s)            : Interface index
 *
 *
 *    Output(s)           : pi1IfName - Interface name of the given 
 *                          interface index.
 *
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion    : None.
 *
 *    Returns             : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/
INT4
CfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return CLI_SUCCESS;
}
#endif

/*****************************************************************************
 *
 *    Function Name       : CfaIvrNotifyVlanIfOperStatusInCxt 
 *
 *    Description         : This function updates the oper status of L3 VLAN 
 *                          interface when there is a change in oper status
 *                          indication from VLAN.
 *
 *    Input(s)            : L2ContextId, VlanId and u1InputOperStatus
 *
 *    Output(s)           : None. 
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :    CFA_SUCCESS when the oper status updation is 
 *                            successful. Else CFA_FAILURE.
 *        
 *****************************************************************************/

INT1
CfaIvrNotifyVlanIfOperStatusInCxt (UINT4 u4L2ContextId,
                                   tVlanIfaceVlanId VlanId, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4L2ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1OperStatus);
    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name       : CfaIvrNotifyVlanIfOperStatusInCxt
 *
 *    Description         : This function updates the oper status of L3 VLAN
 *                          when Mclag membership changes for L2 VLAN
 *
 *    Input(s)            : L2ContextId, VlanId and u1InputOperStatus
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :    CFA_SUCCESS when the oper status updation is
 *                            successful. Else CFA_FAILURE.
 *
 *****************************************************************************/

INT1
CfaIvrNotifyMclagIfOperStatusInCxt (UINT4 u4L2ContextId,
                                    tVlanIfaceVlanId VlanId, UINT1 u1OperStatus)
{

    UNUSED_PARAM (u4L2ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1OperStatus);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetIlanPorts                                    */
/*                                                                           */
/*     DESCRIPTION      : This function is called from PBB module to get the */
/*                        logically connected port of the given port         */
/*                                                                           */
/*     INPUT            : u4InIfIndex - IfIndex of the port specified        */
/*                                                                           */
/*     OUTPUT           : **pOutInfo - Pointer to the link list of structure */
/*                         containing associated IfIndex info                */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaGetIlanPorts (UINT4 u4InIfIndex, tTMO_SLL * pOutInfo)
{
    UNUSED_PARAM (u4InIfIndex);
    UNUSED_PARAM (pOutInfo);
    return CFA_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : CfaFreeILanListNodes                                 */
/*                                                                           */
/* Description        : This function is to free the ILan List nodes         */
/*                      to gCfaILanPortPoolId.                               */
/*                                                                           */
/* Input(s)           : pList - Pointer to the ILan Port List.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaFreeILanListNodes (tTMO_SLL * pList)
{
    UNUSED_PARAM (pList);
    return;
}

/*****************************************************************************
 *
 *    Function Name       :CfaIsVirtualInterface 
 *
 *    Description         : This function checks whether  the interface 
 *                          is virtual/internal interface or not.
 *
 *    Input(s)            : u4Index - interface index
 *
 *    Output(s)           : NONE
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_TRUE-If interface is a virtual interface
 *****************************************************************************/

UINT1
CfaIsVirtualInterface (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_TRUE;

}

/*****************************************************************************
 *
 *    Function Name       : CfaGetVlanInterfaceIndex
 *
 *    Description         : This function returns the VLAN interface index 
 *
 *    Input(s)            : VLAN ID. 
 *
 *    Output(s)           : VLAN Interface Index.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  VLAN interface index corresponding to the 
 *                          VLAN ID,if success. Else, returns the invalid 
 *                          interface index.
 *****************************************************************************/

UINT4
CfaGetVlanInterfaceIndex (tVlanIfaceVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return CFA_INVALID_INDEX;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetVlanInterfaceIndexInCxt
 *
 *    Description         : This function returns the VLAN interface index 
 *
 *    Input(s)            : L2 Context Id, VLAN ID. 
 *
 *    Output(s)           : VLAN Interface Index.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  VLAN interface index corresponding to the 
 *                          VLAN ID,if success. Else, returns the invalid 
 *                          interface index.
 *****************************************************************************/

UINT4
CfaGetVlanInterfaceIndexInCxt (UINT4 u4L2ContextId, tVlanIfaceVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4L2ContextId);
    return CFA_INVALID_INDEX;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfHandleOutgoingPktOnPort               */
/*                                                                           */
/*    Description               : This API is used by L2 modules to transmit */
/*                                packets on a set of port through CFA.      */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                u4IfIndex - ifIndex of the driver port.    */
/*                                u4PktSize - Size of the data buffer        */
/*                                u2Protocol - Protocol type                 */
/*                                u1EncapType - Encapsulation Type           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                              UINT4 u4PktSize, UINT2 u2Protocol,
                              UINT1 u1EncapType)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktSize);
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (u1EncapType);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfHandleOutgoingPktOnPortList           */
/*                                                                           */
/*    Description               : This API is used by L2 modules to transmit */
/*                                packets on a port through CFA.             */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                PortList  - Bitlist of ports on which      */
/*                                            frame is to be transmitted.    */
/*                                u4PktSize - Size of the data buffer        */
/*                                u2Protocol - Protocol type                 */
/*                                u1EncapType - Encapsulation Type           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfHandleOutgoingPktOnPortList (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  tPortList PortList,
                                  UINT4 u4PktSize, UINT2 u2Protocol,
                                  UINT1 u1EncapType)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (u4PktSize);
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (u1EncapType);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfHandleOutgoingPktOnPort               */
/*                                                                           */
/*    Description               : This API is used by L2 modules to transmit */
/*                                packets on backplane interfaces through CFA*/
/*                                The structure tCfaBackPlaneParams specifies   */
/*                                the set of parameters that needs to be     */
/*                                added into this PDU in CFA.                */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                pBackPlaneParams - Pointer to values to be */
/*                                                   pushed into the PDU.    */
/*                                u4IfIndex - ifIndex of the driver port.    */
/*                                u4PktSize - Size of the data buffer        */
/*                                u2Protocol - Protocol type                 */
/*                                u1EncapType - Encapsulation Type           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfHandleOutgoinPktOnBackPlane (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  tCfaBackPlaneParams * pBackPlaneParams,
                                  UINT4 u4IfIndex, UINT4 u4PktSize,
                                  UINT2 u2Protocol, UINT1 u1EncapType)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pBackPlaneParams);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktSize);
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (u1EncapType);

    return L2IWF_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetSysMacAddress
 *
 *    Description         : This function returns the Switch's Base Mac address
 *                          read from NVRAM
 *    Input(s)            : None. 
 *
 *    Output(s)           : pSwitchMac - the Switch Mac address.
 *
 *    Global Variables Referred : gIssSysGroupInfo.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : None 
 *                         
 *
 *****************************************************************************/

VOID
CfaGetSysMacAddress (tMacAddr SwitchMac)
{
    MEMSET (SwitchMac, 0, sizeof (tMacAddr));
    return;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetVlanId
 *
 *    Description         : This function returns the VLAN ID for a particular
 *                          VLAN interface.
 *
 *    Input(s)            : Interface index. 
 *
 *    Output(s)           : VLAN ID.
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  VLAN ID correspoding to the VLAN interface index.
 *    
 *****************************************************************************/

INT1
CfaGetVlanId (UINT4 u4IfIndex, tVlanIfaceVlanId * pVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pVlanId);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetIvrStatus
 *
 *    Description         : This function returns the IVR status 
 *
 *    Input(s)            : None. 
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : gu4IsIvrEnabled.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            :  CFA_ENABLED - If IVR is enabled.
 *                          CFA_DISABLED - If IVR is disabled.
 *****************************************************************************/

UINT1
CfaGetIvrStatus ()
{
    return CFA_DISABLED;
}

/*****************************************************************************/
/* Function Name      : CfaIsThisInterfaceVlan                               */
/*                                                                           */
/* Description        : This function checks whether the given Vlan is an    */
/*                      interface Vlan or not.                               */
/*                                                                           */
/* Input(s)           : VlanId                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_TRUE/ CFA_FALSE                                  */
/*****************************************************************************/

BOOL1
CfaIsThisInterfaceVlan (UINT2 VlanId)
{

    UNUSED_PARAM (VlanId);
    return CFA_TRUE;
}

/*****************************************************************************
 *
 *    Function Name        : CfaDeletePortChannelIndication
 *
 *    Description        : This function is for Posting a message 
 *                         for deleting the PortChannel
 *                         Interface. 
 *
 *    Input(s)            : UINT4 u4IfIndex,
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if deletion succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaDeletePortChannelIndication (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfCounters                                     */
/*                                                                           */
/* Description        : This function Gets the interface counters            */
/*                                                                           */
/* Input(s)           : u4IfIndex -Interface Index                           */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pIfCounter-Pointer to the interface counter structure*/
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaGetIfCounters (UINT4 u4IfIndex, tIfCountersStruct * pIfCounter)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pIfCounter);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGddConfigPort                                     */
/*                                                                           */
/* Description        :                                                      */
/*                                                                           */
/* Input(s)           : u4IfIndex -Interface Index                           */
/*                                                                           */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/

INT4
CfaGddConfigPort (UINT4 u4IfIndex, UINT1 u1ConfigOption, VOID *pConfigParam)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pConfigParam);
    UNUSED_PARAM (u1ConfigOption);

    return (CFA_SUCCESS);
}

/*****************************************************************************
 *
 *    Function Name        : CfaUpdateVipOperStatus
 *
 *    Description          : This function is used to trigger CFA for 
 *                           updating the OperStatus of the VIP
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns          : CFA_SUCCESS / CFA_FAILURE.
 *****************************************************************************/
INT4
CfaUpdateVipOperStatus (UINT4 u4VipIfIndex, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4VipIfIndex);
    UNUSED_PARAM (u1OperStatus);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetEthernetType                                   */
/*                                                                           */
/* Description        : This function gets the ethernet type                 */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1EthernetType:Pointer to the ethernet type.        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetEthernetType (UINT4 u4IfIndex, UINT1 *pu1EthernetType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1EthernetType);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIfSetInDiscard                                    */
/*                                                                           */
/* Description        : This function increments the number of               */
/*                      incoming packets discarded                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaIfSetInDiscard (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CfaMuxUnRegisterProtocolCallBack
 *
 *    DESCRIPTION      : This function is called by the protocols for
 *                       unregistering packet reception usually when the module
 *                       is shutdown
 *
 *    INPUT            : i1ProtocolId - The ID of the registering/deregistering
 *                                       protocol
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE
 *
 ****************************************************************************/
INT4
CfaMuxUnRegisterProtocolCallBack (INT1 i1ProtocolId)
{
    UNUSED_PARAM (i1ProtocolId);
    return CFA_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CfaMuxRegisterProtocolCallBack
 *
 *    DESCRIPTION      : This function is called by the protocols for
 *                       registering packet reception. The protocols register
 *                       with a set of qualifiers and a call back function.
 *                       This function registers with the MUX and when the MUX
 *                       sends packet, it sends the packet to the corresponding
 *                       module.
 *
 *    INPUT            : i1ProtocolId - The ID of the registering/deregistering
 *                                       protocol
 *                       pProtocolInfo - Structure containing the qualifiers for
 *                       the protocol
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : CFA_SUCCESS/CFA_FAILURE
 *
 ****************************************************************************/

INT4
CfaMuxRegisterProtocolCallBack (INT1 i1ProtocolId,
                                tProtocolInfo * pProtocolInfo)
{
    UNUSED_PARAM (i1ProtocolId);
    UNUSED_PARAM (pProtocolInfo);
    return CFA_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CfaGetInterfaceBrgPortType
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the port type.
 *
 *    INPUT            : u4IfIndex   - Interface Index
 *    OUTPUT           : pi4BrgPortType  -PortType
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

INT4
CfaGetInterfaceBrgPortType (UINT4 u4IfIndex, INT4 *pi4BrgPortType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi4BrgPortType);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaUfdIsLastDownlinkInGroup                          */
/*                                                                           */
/* Description        : This function checks whether the given port is the   */
/*                      last downlink in the UFD group                       */
/*                                                                           */
/* Input(s)           : u4IfIndex                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
INT4
CfaUfdIsLastDownlinkInGroup (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_FALSE;

}

/*****************************************************************************
 *
 *    Function Name        : CfaApiPortChannelUpdateInfo
 *
 *    Description        : This function is for Posting a message from LA to 
 *                         CFA for the following information.
 *                         1. Adding a port to a portChannel.
 *                         2. Port channel oper status indication.
 *
 *    Input(s)            : pCfaLaInfoMsg  - LA information to be notified to
 *                                           CFA.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure,
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE,
 *
 *****************************************************************************/
INT4
CfaApiPortChannelUpdateInfo (tCfaLaInfoMsg * pCfaLaInfoMsg)
{
    UNUSED_PARAM (pCfaLaInfoMsg);
    return CFA_SUCCESS;
}

/*****************************************************************************
 **
 **    Function Name        : CfaInterfaceHLPortOperIndication
 **
 **    Description          : This function validates the ifIndex and calls
 **                           CfaInterfaceStatusChangeIndication to upate the
 **                           interface status.
 **    Input(s)             : u4IfIndex - Interface index
 **                           u1OperStatus -
 **
 **    Output(s)            : None.
 **
 **    Returns              : None.
 ******************************************************************************/
VOID
CfaInterfaceHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1OperStatus);
    return;
}

/*****************************************************************************
 *
 *    Function Name        : CfaInterfaceLAPortCreateIndication
 *
 *    Description          : This function validates the ifIndex and calls
 *                           post event to CFA for processing  the physical port
 *                           creation from LA module, with the Port channel is
 *                           deleted.
 *
 *    Input(s)             : u4IfIndex - Interface index
 *                           u1OperStatus - Oper Status
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.
 *****************************************************************************/
VOID
CfaInterfaceLAPortCreateIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1OperStatus);
    return;
}
#endif

#ifndef BRIDGE_WANTED
/*****************************************************************************/
/* Function Name      : BrgDeleteFwdEntryForMac                              */
/*                                                                           */
/* Description        : This function deletes the forwarding table enties    */
/*                      matching the given MAC Address & port number         */
/*                                                                           */
/* Input(s)           : pDestAddr - mac address                              */
/*                      u2Port    - Port  number                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
BrgDeleteFwdEntryForMac (tMacAddr * pDestAddr, UINT2 u2Port)
{
    UNUSED_PARAM (pDestAddr);
    UNUSED_PARAM (u2Port);
    return;
}

/*****************************************************************************/
/* Function Name      : BrgGetAgeoutTime                                     */
/*                                                                           */
/* Description        : This function returns the AgeoutTime to be used      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : tpInfo                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : AgeoutTime                                           */
/*****************************************************************************/
UINT4
BrgGetAgeoutTime (VOID)
{
    return NORMAL_AGEOUT_TIME;

}

/*****************************************************************************/
/* Function Name      : BrgIncrFilterInDiscards                              */
/*                                                                           */
/* Description        : This function increments the Filter Indiscard        */
/*                                                                           */
/* Input(s)           : u2Port - port number                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : base , tpInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
BrgIncrFilterInDiscards (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return;
}

#endif

#ifndef ISS_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRestoreTypeFromNvRam                       */
/*                                                                          */
/*    Description        : This function is used to get the Restore         */
/*                         Type from the NVRAM                              */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore Type                                     */
/****************************************************************************/
INT4
IssGetRestoreTypeFromNvRam (VOID)
{
    return 0;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCPUControlledLearning                         */
/*                                                                          */
/*    Description        : This function is invoked to get the status for   */
/*                         CPU controlled learning for a particular port.   */
/*                                                                          */
/*    Input(s)           : port number                                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssCPUControlledLearning (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAsyncMode                                  */
/*                                                                          */
/*    Description        : This function is invoked to get Restore Flag.    */
/*                                                                          */
/*    Input(s)           : i4ProtoId - Protocol Identifier                  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore Flag                                     */
/****************************************************************************/
INT4
IssGetAsyncMode (INT4 i4ProtoId)
{
    UNUSED_PARAM (i4ProtoId);
    return ((INT4) ISS_NPAPI_MODE_SYNCHRONOUS);
}

/*****************************************************************************
 *
 *    Function Name        : IssGetStackingModel
 *
 *    Description          : This function is called to get the Stacking Model
 *
 *
 *    Input(s)             : None
 *
 *
 *    Output(s)            : None.
 *
 *    Returns              : ISS_CTRL_PLANE_STACKING_MODEL /
 *                           ISS_DATA_PLANE_STACKING_MODEL /
 *                           ISS_DISS_STACKING_MODEL
 *
 *****************************************************************************/
PUBLIC INT4
IssGetStackingModel (VOID)
{
    return 0;

}

/*****************************************************************************/
/* Function Name      : IssGetPortCtrlMode                                   */
/*                                                                           */
/* Description        : Get the port control mode for the given port.        */
/*                                                                           */
/* Input(s)           : i4IfIndex - IfIndex of the port.                     */
/*                                                                           */
/* Output(s)          : pi4PortCtrlMode - Control Mode of the port           */
/*                                        (enableRx, enableTx, enableRxTx,   */
/*                                         disabled).                        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
IssGetPortCtrlMode (INT4 i4IfIndex, INT4 *pi4PortCtrlMode)
{
    UNUSED_PARAM (i4IfIndex);
    *pi4PortCtrlMode = 0;
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAutoPortCreateFlag                         */
/*                                                                          */
/*    Description        : This function is invoked to get the automatic    */
/*                         port create flag                                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_ENABLE                                       */
/****************************************************************************/
INT4
IssGetAutoPortCreateFlag (VOID)
{
    return ISS_ENABLE;
}

/*****************************************************************************/
/* Function Name      : IssApiUpdtPortIsolationEntry                         */
/*                                                                           */
/* Description        : This function is called to update the port isolation */
/*                      table.                                               */
/*                                                                           */
/* Input(s)           : pPortIsolationEntry - Pointer to the port isolation  */
/*                      information.                                         */
/*                                                                           */
/* Output(s)          : NONE.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS.                                         */
/*                                                                           */
/*****************************************************************************/
INT4
IssApiUpdtPortIsolationEntry (tIssUpdtPortIsolation * pPortIsolationEntry)
{
    UNUSED_PARAM (pPortIsolationEntry);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssFreeAggregatorMac                             */
/*                                                                          */
/*    Description        : This function is invoked to free the Aggregator  */
/*                         MAC address.                                     */
/*                                                                          */
/*    Input(s)           : u2AggIndex - Aggregator Index.                   */
/*                                                                          */
/*    Output(s)          : AggrMac.                                         */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssFreeAggregatorMac (UINT4 u4AggIndex)
{
    UNUSED_PARAM (u4AggIndex);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetDefaultVlanIdFromNvRam                     */
/*                                                                          */
/*    Description        : This function is used to get the Default Vlan Id */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Default Vlan Id                                  */
/****************************************************************************/
UINT2
IssGetDefaultVlanIdFromNvRam ()
{
    return ISS_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntApiGetInventoryInfo                               *
 *                                                                           *
 * Description        : This function is called to get physical entity device*
 *                      information that has to be used in LLDP-MED Inventory*
 *                      TLV of the local device                              *
 *                                                                           *
 * Input(s)           : NONE                                                 *
 *                                                                           *
 * Output(s)          : pInventoryInfo - Inventory Info of the entity        *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
PUBLIC INT1
EntApiGetInventoryInfo (tLldpMedLocInventoryInfo * pInventoryInfo)
{
    UNUSED_PARAM (pInventoryInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSwitchid                                   */
/*                                                                          */
/*    Description        : This function is invoked to get the switchid     */
/*                         which is configured in nodeid file               */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Switchid                                         */
/****************************************************************************/

INT4
IssGetSwitchid (VOID)
{
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssMirrorStatusCheck                             */
/*                                                                          */
/*    Description        : This function checks if mirroring can be enabled */
/*                         on a given port.                                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS if mirroring can be enabled         */
/*                         ISS_FAILURE if mirroring cannot be enabled      */
/****************************************************************************/
INT4
IssMirrorStatusCheck (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIncrementalSaveStatus                      */
/*                                                                          */
/*    Description        : This function is invoked to get the incremental  */
/*                         save status used for the current session         */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Incremental save status.                         */
/****************************************************************************/

INT4
IssGetIncrementalSaveStatus (VOID)
{
    /*In case if incremental save status is enabled, this function return ISS_TRUE */
    return ISS_FALSE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetHwCapabilities                             */
/*                                                                          */
/*    Description        : This function is invoked to get the Hardware     */
/*                         supported informations                           */
/*                                                                          */
/*    Input(s)           : u4HwSupport - Opcode for which the information   */
/*                         on hardware support is required                  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Status of Hardware Support                       */
/****************************************************************************/

UINT1
IssGetHwCapabilities (UINT1 u1HwSupport)
{
    UNUSED_PARAM (u1HwSupport);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAggregatorMac                              */
/*                                                                          */
/*    Description        : This function is invoked to get the Aggregator   */
/*                         MAC address from the ISS Group Information.      */
/*                                                                          */
/*    Input(s)           : u2AggIndex - Aggregator Index.                   */
/*                                                                          */
/*    Output(s)          : AggrMac.                                         */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssGetAggregatorMac (UINT2 u2AggIndex, tMacAddr AggrMac)
{
    UNUSED_PARAM (u2AggIndex);
    UNUSED_PARAM (AggrMac);
    return ISS_SUCCESS;
}

#ifndef IP_WANTED
/*-------------------------------------------------------------------+
 *
 * Function           : IpUpdateInterfaceStatus
 *
 * Input(s)           : u2Port, u1OperStatus
 *
 * Output(s)          : None
 *
 * Returns            : IP_SUCCESS, IP_FAILURE
 *
 * Action             : Invoked by CFA
 *                      Enqueues to IP task regarding change of interface.
 *
 *
+-------------------------------------------------------------------*/
INT4
IpUpdateInterfaceStatus (UINT2 u2Port, UINT1 *pu1IfName, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (u1OperStatus);
    return IP_SUCCESS;
}

#endif
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclValidateMemberPort                            */
/*                                                                           */
/*     DESCRIPTION      : This function checks wheather the filter is        */
/*                   attached with the physical port                    */
/*                                                                           */
/*     INPUT            : u4IfIndex - Physical port number                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
AclValidateMemberPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssACLApiModifyFilterEntry                           */
/*                                                                           */
/* Description        : This function is called from the DCBx Application    */
/*                      priority module to configure the priority for        */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLApiModifyFilterEntry (tIssAclHwFilterInfo * pAppPriAclInfo,
                            UINT1 u1FilterType, UINT1 u1Status)
{
    INT4                i = ISS_SUCCESS;
    UNUSED_PARAM (pAppPriAclInfo);
    UNUSED_PARAM (u1FilterType);
    UNUSED_PARAM (u1Status);
    return i;
}

/*****************************************************************************/
/*  Function Name   : IssApiGetIsolationFwdStatus                            */
/*  Description     : This function does the following                       */
/*                      If the u1Action is set as ISS_PI_GET_FWD_STATUS then */
/*                      it checks whether the egress port is an allowed      */
/*                      egress port for the given ingress port, invlanid     */
/*                      pair.                                                */
/*                        (i) Fetches RB Node corresponding to this ingress  */
/*                            index.                                         */
/*                        (ii) If entry is not found, return ISS_SUCCESS.    */
/*                        (iii)If an entry is found and egress port also     */
/*                             matches then return ISS_SUCCESS.              */
/*                        (iv) If an entry is found and egress port match is */
/*                             not found, then return ISS_FAILURE            */
/*                                                                           */
/*  Input(s)        : u4IngressIfIndex - Ingress Port                        */
/*                    u4EgressIfIndex  - Egress Port                         */
/*                    InVlanId       - Vlan Id                               */
/*                    u1Action       - Action supported by this API.         */
/*                      (i) ISS_PI_GET_FWD_STATUS- Get the Port Isolation    */
/*                        forwarding status.                                 */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS,if the given egress port is an*/
/*                                 allowed forward port for the given        */
/*                                 (ingress port, vlanid) pair.              */
/*                                 ISS_FAILURE, otherwise                    */
/*****************************************************************************/
INT1
IssApiGetIsolationFwdStatus (tIssPortIsoInfo * pIssPortIsoInfo)
{
    UNUSED_PARAM (pIssPortIsoInfo);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRestoreFlagFromNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to get Restore Flag.    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore Flag                                     */
/****************************************************************************/
INT1
IssGetRestoreFlagFromNvRam ()
{
    return 0;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrGetRestorationStatus                          */
/*                                                                          */
/*    Description        : This function returns the whether the restoration*/
/*                          process is ongoing or completed.                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE / ISS_FALSE                             */
/****************************************************************************/
tIssBool
MsrGetRestorationStatus ()
{
    return ISS_TRUE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrGetClearConfigStatus                          */
/*                                                                          */
/*    Description        : This function returns the whether the clear      */
/*                          config status is ongoing or completed            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE / ISS_FALSE                             */
/****************************************************************************/

tIssBool
MsrGetClearConfigStatus ()
{
    return ISS_TRUE;
}

/*****************************************************************************/
/* Function Name      : IssACLApiGetFilterEntry                              */
/*                                                                           */
/* Description        : This function is called from the DCBx Application    */
/*                      priority module to get the FilterId for              */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLApiGetFilterEntry (tIssAclHwFilterInfo * pAppPriAclInfo,
                         UINT1 u1FilterType, UINT1 u1Status)
{
    UNUSED_PARAM (pAppPriAclInfo);
    UNUSED_PARAM (u1FilterType);
    UNUSED_PARAM (u1Status);
    return ISS_SUCCESS;
}

#endif

#ifndef RSTP_WANTED

/*****************************************************************************/
/* Function Name      : AstHandleInFrame                                     */
/*                                                                           */
/* Description        : This function is called from the CFA Module to       */
/*                      handover the BPDUs to ASTP Task by posting the BDPUs */
/*                      to the AST Queue and sending an event to ASTP Task   */
/*                                                                           */
/* Input(s)           : pCruBuf - Pointer to the BPDU CRU Buffer             */
/*                      u4IfIndex - Interface Index of the Port              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE / RST_DATA                 */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfHandleIncomingLayer2Pkt                         */
/*****************************************************************************/
INT4
AstHandleInFrame (tCRU_BUF_CHAIN_HEADER * pCruBuf, UINT4 u4IfIndex)
{
    UNUSED_PARAM (pCruBuf);
    UNUSED_PARAM (u4IfIndex);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIsContextStpCompatible                            */
/*                                                                           */
/* Description        : Called by other modules to know if STP compatibility */
/*                      is enabled in the given virtual context.             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes, Context is STP compatible.                  */
/*                      0 - No, Context is NOT STP compatible.               */
/*****************************************************************************/
INT4
AstIsContextStpCompatible (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIsRstStartedInContext                             */
/*                                                                           */
/* Description        : Called by other modules to know if RSTP is started   */
/*                      or shutdown in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Rstp is started in the given context         */
/*                      0 - No, Rstp is NOT started in the given context     */
/*****************************************************************************/
INT4
AstIsRstStartedInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return 0;
}

/*****************************************************************************/
/* Function Name      : RstGetEnabledStatus                                  */
/*                                                                           */
/* Description        : Gets the RSTP module status. Called by other modules */
/*                      to know if RSTP is enabled or disabled.              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_ENABLED (or) RST_DISABLED                        */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/* Calling Function   : InSetLogicalPortOperStatus                           */
/*                      BridgeAggOperStatusIndication                        */
/*                      BridgeUpdatePortStatus                               */
/*****************************************************************************/
INT4
RstGetEnabledStatus (VOID)
{
    return RST_DISABLED;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortOperEdgeStatus                           */
/*                                                                           */
/* Description        : This routine returns the port's oper edge status     */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2PortIndex - Index of the port whose state          */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : bOperEdge - Boolean value indicating whether the     */
/*                                  port is an Edge Port                     */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortOperEdgeStatus (UINT4 u4PortIndex, BOOL1 * pbOperEdge)
{
    UNUSED_PARAM (u4PortIndex);
    *pbOperEdge = OSIX_FALSE;
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstUpdatePortStatus                                  */
/*                                                                           */
/* Description        : This function is called from the Bridge Module to    */
/*                      indicate the operational status of the physical port */
/*                      status.                                              */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to which     */
/*                                  this indication belongs                  */
/*                      u1Status - The status of the Port (up or down)       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/*                                                                           */
/* Calling Function   : nmhSetDot1dBasePortAdminStatus                       */
/*****************************************************************************/
INT4
AstUpdatePortStatus (UINT4 u4PortNum, UINT1 u1Status)
{
    UNUSED_PARAM (u4PortNum);
    UNUSED_PARAM (u1Status);
    return RST_SUCCESS;
}

/******************************************************************************/
/* Function Name      : AstPortSpeedChgIndication                            */
/*                                                                           */
/* Description        : This function is a stub when STP is not used. It will*/
/*                      called from the LA Module to indicate change in      */
/*                      active ports of channel group.                       */
/*                      This function needs to be called whenever port path  */
/*                      cost for a port needs to be re-calculated based on   */
/*                      the latest speed of the port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - IfIndex of the port whose port path cost */
/*                                  needs to be re-calculted based on latest */
/*                                  speed of that port                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfAddActivePortToPortChannel                      */
/*                      L2IwfRemoveActivePortToPortChannel                   */
/*****************************************************************************/
INT4
AstPortSpeedChgIndication (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortOperPointToPointStatus                   */
/*                                                                           */
/* Description        : This routine returns the port's oper point to point  */
/*                      status of given the Port Index.                      */
/*                      It accesses the L2Iwf common database.               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose oper    */
/*                                  point to point status is to be obtained. */
/*                                                                           */
/* Output(s)          : bOperPointToPoint - Boolean value indicating whether */
/*                      the port is Point To Point                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortOperPointToPointStatus (UINT4 u4IfIndex, BOOL1 * pbOperPointToPoint)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pbOperPointToPoint);
    return L2IWF_SUCCESS;
}

#ifndef ERPS_WANTED

/*****************************************************************************/
/* Function Name      : L2IwfGetInstPortState                                */
/*                                                                           */
/* Description        : This routine returns the port state given the        */
/*                      Instance Index and the Port Index. It accesses the   */
/*                      L2Iwf common database.                               */
/*                                                                           */
/* Input(s)           : u2MstInst - InstanceId for which the port state is   */
/*                                  to be obtained.                          */
/*                    : u2PortIndex - Index of the port whose port state     */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Port State                                           */
/*****************************************************************************/
UINT1
L2IwfGetInstPortState (UINT2 u2MstInst, UINT4 u4PortIndex)
{
    UNUSED_PARAM (u2MstInst);
    UNUSED_PARAM (u4PortIndex);
    return AST_PORT_STATE_FORWARDING;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanPortState                                */
/*                                                                           */
/* Description        : This routine returns the port state given the        */
/*                      Vlan Id and the Port Index. It accesses the L2Iwf    */
/*                      common database to find the Mst Instance to which    */
/*                      the Vlan is mapped and then gets the ports state for */
/*                      that instance.                                       */
/*                                                                           */
/* Input(s)           : VlanId - VlanId for which the port state is to be    */
/*                               obtained.                                   */
/*                    : u2PortIndex - Index of the port whose state          */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Port State                                           */
/*****************************************************************************/
UINT1
L2IwfGetVlanPortState (tVlanId VlanId, UINT4 u4PortIndex)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4PortIndex);
    return AST_PORT_STATE_FORWARDING;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortStateCtrlOwner                           */
/*                                                                           */
/* Description        : This routine is called to get the Owner protocol of  */
/*                      the given port to avoid race condition between       */
/*                      protocol, while updating same port info.             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose owner   */
/*                                  protocol to be returned.                 */
/*                                                                           */
/* Output(s)          : pu1OwnerProtId  - Owner Protocol Id.                 */
/*                                                                           */
/* Return Value(s)    : Owner Protocol Id.                                   */
/*****************************************************************************/

INT4
L2IwfGetPortStateCtrlOwner (UINT4 u4IfIndex, UINT1 *pu1OwnerProtId,
                            BOOL1 bLockReq)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1OwnerProtId);
    UNUSED_PARAM (bLockReq);

    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsRapsVlanPresent                               */
/*                                                                           */
/* Description        : This routine validates whether RAPS_VLAN ID is       */
/*                      mapped to the instance.                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2MstInst   - InstanceId to which the list of vlan   */
/*                                    mapped are obtained.                   */
/*                      u2RapsVlanId  - RAPS VlanId which is mappped to the  */
/*                                    Instance.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfIsRapsVlanPresent (UINT4 u4ContextId, UINT2 u2RapsVlanId, UINT2 u2MstInst)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2RapsVlanId);
    UNUSED_PARAM (u2MstInst);
    return L2IWF_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : L2IwfGetInstPortStateForPort                         */
/*                                                                           */
/* Description        : This routine is called from NPAPI to get the given   */
/*                      port's state in all active instances.                */
/*                                                                           */
/* Input(s)           : u4PortNum    - Index of the port                     */
/*                                                                           */
/* Output(s)          : InstPortStateInfo - Array of instance, port state    */
/*                                          pairs.                           */
/*                      u2Count           - Number of such pairs filled.     */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfGetInstPortStateForPort (UINT4 u4PortNum,
                              tMstInstPortStateInfo * pInstPortStateInfo,
                              UINT2 *pu2Count)
{
    UNUSED_PARAM (u4PortNum);
    pInstPortStateInfo->u2Inst = RST_DEFAULT_INSTANCE;
    pInstPortStateInfo->u1PortState = AST_PORT_STATE_FORWARDING;
    UNUSED_PARAM (pu2Count);
    return L2IWF_SUCCESS;
}

#endif /*NPAPI_WANTED */

#endif /*ERPS_WANTED */

#endif /*RSTP_WANTED */

#ifndef MSTP_WANTED
/*****************************************************************************/
/* Function Name      : MstGetEnabledStatus                                  */
/*                                                                           */
/* Description        : Get the MSTP module status. Called by other modules  */
/*                      to know if MSTP is enabled or disabled.              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_ENABLED (or) MST_DISABLED                        */
/*                                                                           */
/* Called By          : Bridge Module                                        */
/* Calling Functions  : BrgGetPortStpState                                   */
/*                      BrgGetPortStpEnableStatus                            */
/*                      TpIsPortOkForForwarding                              */
/*****************************************************************************/
INT4
MstGetEnabledStatus (VOID)
{
    return MST_DISABLED;
}

/*****************************************************************************/
/* Function Name      : MstpMapVidToMstId                                    */
/*                                                                           */
/* Description        : This API is called to map a Vlan ID to a MST-ID.     */
/*                      The MST-ID can also be the CIST in which case this   */
/*                      routine removes the Vlan from the previously mapped  */
/*                      MSTI. This routine performs the same operation as    */
/*                      that when the instance to Vlan mapping is configured */
/*                      from management.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : MST_FAILURE (or) MST_SUCCESS                         */
/*****************************************************************************/
INT4
MstpMapVidToMstId (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2MstId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2MstId);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstUpdateVlanPortList                                */
/*                                                                           */
/* Description        : This MSTP API will be invoked from VLAN, whenever    */
/*                      interfaces are either added to VLAN or deleted from  */
/*                      the VLAN. This API will post a message to MSTP, which*/
/*                      will add the SISP enabled ports to corresponding     */
/*                      instances or delete the SISP enabled ports from      */
/*                      instances depending upon the configuration.          */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId      - Vlan Identifier                        */
/*                      AddedPorts  - Port list bit map for ports that are   */
/*                                    added to the VLAN.                     */
/*                      DeletedPorts- Port list bit map for ports that are   */
/*                                    deleted from the VLAN.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_FAILURE (or) MST_SUCCESS                         */
/*                                                                           */
/* Called By          :                                                      */
/* Calling Functions  :                                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
MstUpdateVlanPortList (UINT4 u4ContextId, tVlanId VlanId,
                       tLocalPortList AddedPorts, tLocalPortList DeletedPorts)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddedPorts);
    UNUSED_PARAM (DeletedPorts);

    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstSispValidateInstRestriction                       */
/*                                                                           */
/* Description        : This MSTP API will be invoked from L2IWF,whenever    */
/*                      interfaces are added to VLAN. This API will check    */
/*                      against the restriction that the same port cannot be */
/*                      present in same instance in two different contexts.  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u4PortNum   - Port number                            */
/*                      VlanId      - Vlan Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_FAILURE (or) MST_SUCCESS                         */
/*                                                                           */
/* Calling Functions  : MstSispIsPerStPortCreationValid                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
MstSispValidateInstRestriction (UINT4 u4ContextId, UINT4 u4PortNum,
                                tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4PortNum);
    UNUSED_PARAM (u4ContextId);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MstUpdateSispStatusOnPort                            */
/*                                                                           */
/* Description        : This MSTP API will be invoked by SISP, whenever SISP */
/*                      status is updated on the interface. This API will    */
/*                      post a message to MSTP, which will handle this event.*/
/*                                                                           */
/* Input(s)           : u4IfIndex   - Interface Index.                       */
/*                      u4ContextId - Context Id of Interface Index.         */
/*                      bStatus     - SISP Status,can be either,             */
/*                                        1) SISP_ENABLE                     */
/*                                        1) SISP_DISABLE                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MST_FAILURE (or) MST_SUCCESS                         */
/*                                                                           */
/* Called By          :                                                      */
/* Calling Functions  :                                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
MstUpdateSispStatusOnPort (UINT4 u4IfIndex, UINT4 u4ContextId,
                           tAstBoolean bStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (bStatus);

    return MST_SUCCESS;
}
#endif

#ifndef IGS_WANTED
/*****************************************************************************/
/* Function Name      : SnoopIsIgmpSnoopingEnabled                             */
/*                                                                           */
/* Description        : This function checks if IGMP snooping feature is     */
/*                      enabled in the system or not.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopIsIgmpSnoopingEnabled (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return SNOOP_DISABLED;
}

#endif

#ifndef MLDS_WANTED
/*****************************************************************************/
/* Function Name      : SnoopIsMldSnoopingEnabled                             */
/*                                                                           */
/* Description        : This function checks if MLD snooping feature is     */
/*                      enabled in the system or not.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopIsMldSnoopingEnabled (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return SNOOP_DISABLED;
}
#endif

#if (!defined(IGS_WANTED) && !defined(MLDS_WANTED))
/*****************************************************************************/
/* Function Name      : SnoopUpdateVlanStatus                                */
/*                                                                           */
/* Description        : This function is called from the VLAN Module         */
/*                      to post a message to IGS in order to do the foll:    */
/*                      ->flush the informations learned via bridge when     */
/*                        Vlan is Disabled in the system                     */
/*                      ->flush the informations learned via Vlan   when     */
/*                        Vlan is enabled in the system                      */
/*                                                                           */
/* Input(s)           : b1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : VLAN Module                                          */
/*****************************************************************************/
INT4
SnoopUpdateVlanStatus (BOOL1 b1Status)
{
    UNUSED_PARAM (b1Status);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopPortOperIndication                              */
/*                                                                           */
/* Description        : This function is called from the CFA Module          */
/*                      to indicate the port status indication               */
/*                                                                           */
/* Input(s)           : u4Port - Port number                                 */
/*                      u1Status - Link Status (Up or Down)                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopPortOperIndication (UINT4 u4Port, UINT1 u1Status)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Status);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopIndicateTopoChange                              */
/*                                                                           */
/* Description        : This function is called from the SpanningTree        */
/*                      Module to indicate the topology Change               */
/*                                                                           */
/* Input(s)           : u4Port - Port number                                 */
/*                      VlanList -List Of Vlans                              */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : STP  Module                                          */
/*****************************************************************************/
INT4
SnoopIndicateTopoChange (UINT4 u4Port, tSnoopVlanList VlanList)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (VlanList);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopDeleteFwdAndGroupTableDynamicInfo               */
/*                                                                           */
/* Description        : This function deletes the multicast                  */
/*                      entries for a MAC address which is configured        */
/*                      as a Wild card                                       */
/*                                                                           */
/* Input(s)           : u4ContextId      - context Id                        */
/*                      MacAddr          - Mac address                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SnoopDeleteFwdAndGroupTableDynamicInfo (UINT4 u4ContextId, tMacAddr MacAddr)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    return;
}
#endif

#if ((!defined(ERPS_WANTED)) && (!defined(RSTP_WANTED)))

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanListInInstance                         */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      the customer's MI-unaware protocol stacks.           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2MstInst   - InstanceId for which the list of mapped*/
/*                                    needs to be obtained.                  */
/*                      pu1VlanList - VlanList to be returned                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Vlans mapped to the instance               */
/*****************************************************************************/
UINT2
L2IwfMiGetVlanListInInstance (UINT4 u4ContextId, UINT2 u2MstInst,
                              UINT1 *pu1VlanList)
{
    UINT2               u2NumVlans = 0;
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2MstInst);
    UNUSED_PARAM (pu1VlanList);
    return u2NumVlans;
}

/*****************************************************************************/
/* Function Name      : L2IwfFlushFdbForGivenInst                            */
/*                                                                           */
/*                                                                           */
/* Description        : This function constructs the fdblist from the        */
/*                      Instance Id and Interface Index and then             */
/*                      calls the VLAN module to flush the                   */
/*                      FDB list .                                           */
/*                                                                           */
/* Input(s)           : u4Port - Port number                                 */
/*                      pu1FdbList - List of FDB Identifiers                 */
/*                      i2OptimizeFlag - Indicates whether this call can be  */
/*                      grouped with the flush call for other ports as a     */
/*                      as a single flush call.                              */
/*                      u2Flag  -  Flag that indicates whether the entire    */
/*                                 list will be flushed or a single fdbid    */
/*                                 will be flushed.                          */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfFlushFdbForGivenInst (UINT4 u4ContextId, UINT4 u4IfIndex,
                           UINT2 u2InstanceId, INT2 i2OptimizeFlag,
                           UINT1 u1Module, UINT2 u2FlushFlag)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i2OptimizeFlag);
    UNUSED_PARAM (u2InstanceId);
    UNUSED_PARAM (u1Module);
    UNUSED_PARAM (u2FlushFlag);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanInstMapping                              */
/*                                                                           */
/* Description        : This routine returns the Instance to which the given */
/*                      Vlan Id is mapped. It accesses the L2Iwf common      */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Mstp Instance                                        */
/*****************************************************************************/
UINT2
L2IwfGetVlanInstMapping (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return RST_DEFAULT_INSTANCE;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetInstPortState                                */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Port's state for that instance in the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2MstInst - InstanceId for which the port state is   */
/*                                  to be updated.                           */
/*                    : u4IfIndex - Global IfIndex of the port whose state   */
/*                                  is to be updated.                        */
/*                      u1PortState - Port state to be set                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetInstPortState (UINT2 u2MstInst, UINT4 u4IfIndex, UINT1 u1PortState)
{
    UNUSED_PARAM (u2MstInst);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1PortState);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetVlanInstMapping                              */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Vlan to Instance mapping for the given VLAN in the   */
/*                      L2Iwf common database or the given context.          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                      u2MstInst - InstanceId to be set for this Vlan       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2MstInst)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2MstInst);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfCreateSpanningTreeInstance                      */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to indicate    */
/*                      that the given instance has been created and is      */
/*                      active in the given context.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2InstanceId - Index of the instance                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfCreateSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2InstanceId);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeleteSpanningTreeInstance                      */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to indicate    */
/*                      that the given instance has been deleted and is not  */
/*                      active in the given context.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2InstanceId - Index of the instance                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfDeleteSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2InstanceId);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortStateCtrlOwner                           */
/*                                                                           */
/* Description        : This routine is called to set the Owner protocol for */
/*                      the given port to avoid race condition between       */
/*                      protocol, while updating same port info.             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose owner   */
/*                                  protocol to be set.                      */
/*                      u1ProtId  - Owner Protocol Id.                       */
/*                      u1OwnerStatus - L2IWF_SET - to set the given Owner Id*/
/*                                      L2IWF_RESET - to reset Owner Id.     */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Owner Protocol Id.                                   */
/*****************************************************************************/
INT4
L2IwfSetPortStateCtrlOwner (UINT4 u4IfIndex, UINT1 u1ProtId,
                            UINT1 u1OwnerStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ProtId);
    UNUSED_PARAM (u1OwnerStatus);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanInstMapping                            */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      the customer's MI-unaware protocol stacks.           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Mstp Instance                                        */
/*****************************************************************************/
UINT2
L2IwfMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return RST_DEFAULT_INSTANCE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanListInInstance                           */
/*                                                                           */
/* Description        : This routine returns the list of vlans mapped to an  */
/*                      MSTP instance.                                       */
/*                                                                           */
/* Input(s)           : u2MstInst   - InstanceId for which the list of mapped*/
/*                                    needs to be obtained.                  */
/*                      pu1VlanList - VlanList to be returned                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of vlans mapped to the instance               */
/*****************************************************************************/
UINT2
L2IwfGetVlanListInInstance (UINT2 u2MstInst, UINT1 *pu1VlanList)
{
    UNUSED_PARAM (u2MstInst);
    UNUSED_PARAM (pu1VlanList);
    return 0;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetInstanceInfo                                 */
/*                                                                           */
/* Description        : This routine returns the instance info of the given  */
/*                      Spanning tree instance.                              */
/*                                                                           */
/* Input(s)           : u2Instance - Spanning tree Instance id, for which    */
/*                                   instance info to be returned.           */
/*                                                                           */
/* Output(s)          : pStpInstInfo - Pointer to Instance info.             */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetInstanceInfo (UINT4 u4ContextId, UINT2 u2Instance,
                      tStpInstanceInfo * pStpInstInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Instance);
    UNUSED_PARAM (pStpInstInfo);

    return L2IWF_FAILURE;
}
#endif

#ifndef LA_WANTED
/*****************************************************************************/
/* Function Name      : LaCallBackRegister                                   */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS/LA_FAILURE                                */
/*****************************************************************************/
INT4
LaCallBackRegister (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{

    UNUSED_PARAM (u4Event);
    UNUSED_PARAM (pFsCbInfo);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaIsLaStarted.                                       */
/*                                                                           */
/* Description        : This function is called by either the CFA/ PNAC or   */
/*                      Bridge module to check if the LA Module is started or*/
/*                      not.                                                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gLaGlobalInfo.LaSystemControl.                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_TRUE - if the module is started.                  */
/*                      LA_FALSE - if the module is shutdown.                */
/*****************************************************************************/
tLaBoolean
LaIsLaStarted (VOID)
{
    return LA_FALSE;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPortInPortChannel                             */
/*                                                                           */
/* Description        : This routine checks whether the port is a member of  */
/*                      any Port Channel and if so returns SUCCESS.          */
/*                                                                           */
/* Input(s)           : u4PortIndex - Index of the port                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfIsPortInPortChannel (UINT4 u4PortIndex)
{
    UNUSED_PARAM (u4PortIndex);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeleteAndAddPortToPortChannel                   */
/*                                                                           */
/* Description        : This routine is called from LA to add the given      */
/*                      port to the given PortChannel's configured portlist  */
/*                      in the L2Iwf common database                         */
/*                                                                           */
/* Input(s)           : u2AggId     - Index of the port channel to which the */
/*                                    port is to be added.                   */
/*                      u4IfIndex - Global Index of the port to be added to  */
/*                                  the Configured port list                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfDeleteAndAddPortToPortChannel (UINT4 u4IfIndex, UINT2 u2AggId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2AggId);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetConfiguredPortsForPortChannel                */
/*                                                                           */
/* Description        : This routine returns the list of ports configured as */
/*                      members of the given PortChannel. It accesses the    */
/*                      L2Iwf common database                                */
/*                                                                           */
/* Input(s)           : AggId - Index of the aggregator                      */
/*                                                                           */
/* Output(s)          : ConfiguredPorts - Configured port list for the given */
/*                                        aggregator                         */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetConfiguredPortsForPortChannel (UINT2 u2AggId,
                                       UINT2 au2ConfPorts[], UINT2 *pu2NumPorts)
{
    UNUSED_PARAM (u2AggId);
    UNUSED_PARAM (au2ConfPorts);
    UNUSED_PARAM (pu2NumPorts);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPotentialTxPortForAgg                        */
/*                                                                           */
/* Description        : This routine is invoked from CFA to obtain one       */
/*                      physical interface corresponding to the given        */
/*                      Port channel for frame transmission.                 */
/*                                                                           */
/* Input(s)           : u2AggId - Port channel index                         */
/*                                                                           */
/* Output(s)          : u2PhyPort - Index of the Physical port corresponding */
/*                                  to the port channel                      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetPotentialTxPortForAgg (UINT2 u2AggId, UINT2 *pu2PhyPort)
{

    UNUSED_PARAM (u2AggId);
    UNUSED_PARAM (pu2PhyPort);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortChannelForPort                           */
/*                                                                           */
/* Description        : This routine returns the Port Channel of which the   */
/*                      given port is a member. It accesses the L2Iwf common */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2PortIndex - Index of the port whose PortChannel is */
/*                                    to be obtained.                        */
/*                                                                           */
/* Output(s)          : u2AggId     - Port Channel Index                     */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortChannelForPort (UINT4 u4PortIndex, UINT2 *pu2AggId)
{

    UNUSED_PARAM (u4PortIndex);
    UNUSED_PARAM (pu2AggId);

    return L2IWF_FAILURE;
}

 /*****************************************************************************/
/* Function Name        : LaGetPortChannelIndex                              */
/*                                                                           */
/* Description          : This function returns the portchannel IfIndex of   */
/*                        the port which is configured.                      */
/*                                                                           */
/* Input(s)             : u2PortIndex- PortNumber                            */
/*                                                                           */
/* Output(s)            : pu2AggIndex- IfIndex of the portchannel for which  */
/*                        port is configured                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred             : None                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified             : None                                               */
/*                                                                           */
/* Return Value(s)      : LA_SUCCESS - On Port is aggregatable               */
/*                        LA_FAILURE - On port is not aggregatable           */
/*****************************************************************************/

INT4
LaGetPortChannelIndex (UINT2 u2PortIndex, UINT2 *pu2AggIndex)
{
    UNUSED_PARAM (u2PortIndex);
    UNUSED_PARAM (pu2AggIndex);
    return LA_FAILURE;
}

/*****************************************************************************/
/* Function Name      : LaUpdatePortChannelAdminStatus.                      */
/*                                                                           */
/* Description        : This function is called by CFA when the Port Channel */
/*                      admin status changes.                                */
/*                      in the status of a physical port.                    */
/*                                                                           */
/* Input(s)           : Port Channel Index, Admin Status.                    */
/*                                                                           */
/* Output(s)          : Returns the Operational Status since LA controls     */
/*                      it.                                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : LA_SUCCESS - On success.                             */
/*                      LA_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
LaUpdatePortChannelAdminStatus (UINT2 u2IfIndex, UINT1 u1AdminStatus,
                                UINT1 *pu1OperStatus)
{
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (u1AdminStatus);
    UNUSED_PARAM (pu1OperStatus);
    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPortActiveInPortChannel                       */
/*                                                                           */
/* Description        : This routine is called by LLDP to check if the given */
/*                      port is an active member port of any port-channel    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port number                              */
/*                      u2AggId   - Aggregator Index                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE / L2IWF_FALSE                             */
/*****************************************************************************/

INT4
L2IwfIsPortActiveInPortChannel (UINT4 u4IfIndex, UINT2 u2AggId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2AggId);
    return L2IWF_FALSE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetActivePortsForPortChannel                    */
/*                                                                           */
/* Description        : This routine returns the list of active member ports */
/*                      of the given PortChannel. It accesses the L2IWF      */
/*                      common database                                      */
/*                                                                           */
/* Input(s)           : AggId    - Index of the aggregator                   */
/*                                                                           */
/* Output(s)          : ActivePorts - Active port list for the given         */
/*                                    aggregator                             */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetActivePortsForPortChannel (UINT2 u2AggId, UINT2 au2ActivePorts[],
                                   UINT2 *pu2NumPorts)
{
    UNUSED_PARAM (u2AggId);
    UNUSED_PARAM (au2ActivePorts);
    UNUSED_PARAM (pu2NumPorts);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPortChannelConfigAllowed                      */
/*                                                                           */
/* Description        : This function is called whenever lacp is enabled on  */
/*                      the port. It checks whether Portchannel configuration*/
/*                      allowed in the port.                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Number which is created in system   */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfIsPortChannelConfigAllowed (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function Name      : L2IwfBlockPortChannelConfig                          */
/*                                                                           */
/* Description        : This utilitity function used to update mirror status  */
/*                      in L2PortEntry.                                      */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - mirroring status (enabled/disabled)       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfBlockPortChannelConfig (UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function name      : L2IwfCreatePortIndicationFromLA                      */
/*                                                                           */
/* Description        : This function is invoked by LA, for creating the     */
/*                      physical ports that have come out of an aggregation, */
/*                      in other L2 modules. This function also updates the  */
/*                      Port's oper status as maintained in LA to Bridge and */
/*                      it's higher layers.                                  */
/*                                                                           */
/* Input(s)           : u4IfIndex - Index of the port to be created.         */
/*                      u1PortOperStatus - Port Oper status from LA          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfCreatePortIndicationFromLA (UINT4 u4IfIndex, UINT1 u1PortOperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1PortOperStatus);
}

/*****************************************************************************/
/* Function Name      : LaUpdatePauseAdminMode                               */
/* Description        : When the pause admin mode of a physical port is      */
/*                      changed ,this routine is called to update the        */
/*                      previously configured pasue admin mode for the port. */
/*                                                                           */
/* Input(s)           : u2IfIndex           - Index of the physical port     */
/*                      i4IfPauseAdminMode  - Pause Admin Mode of the port   */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
LaUpdatePauseAdminMode (UINT2 u2IfIndex, UINT4 i4IfPauseAdminMode)
{
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (i4IfPauseAdminMode);
    return;
}

/*****************************************************************************/
/* Function Name      : LaUpdateAggPauseAdminMode                            */
/* Description        : When the pause admin mode of a port-channel is       */
/*                      changed ,this routine is called to update the        */
/*                      previously configured pasue admin mode for the       */
/*                      port-channel.                                        */
/*                                                                           */
/* Input(s)           : u2IfIndex           - Index of the physical port     */
/*                      i4AggPauseAdminMode - Pause Admin Mode of the port   */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*                                                                           */
/*****************************************************************************/
VOID
LaUpdateAggPauseAdminMode (UINT2 u2IfIndex, UINT4 i4AggPauseAdminMode)
{
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (i4AggPauseAdminMode);
    return;
}

/*****************************************************************************/
/* Function Name      : LaSetDefaultPropForAggregator                        */
/*                                                                           */
/* Description        : This routine is called to set default properties for */
/*                      an aggregator                                        */
/*                                                                           */
/* Input(s)           : u2AggId - Aggregator ID                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/ VLAN_FAILURE                           */
/*****************************************************************************/
INT4
LaSetDefaultPropForAggregator (UINT2 u2AggId)
{
    UNUSED_PARAM (u2AggId);

    return LA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LaVcmGetContextInfoFromIfIndex                       */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Context-Id and the Localport number.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier.                    */
/*                                                                           */
/* Output(s)          : pu4ContextId - Context Identifier.                   */
/*                      pu2LocalPortId - Local port number.                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
LaVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                UINT2 *pu2LocalPortId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4ContextId);
    UNUSED_PARAM (pu2LocalPortId);
    return LA_SUCCESS;
}

#endif

#ifndef VLAN_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPvid                                      */
/*                                                                           */
/*    Description         : This Api Gets the pvid of the given port.        */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port IfIndex                         */
/*                                                                           */
/*    Output(s)           : *pu4RetValFsDot1qPvid  - pointe of port PVid     */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanApiGetPvid (UINT4 u4IfIndex, UINT4 *pu4RetValFsDot1qPvid)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4RetValFsDot1qPvid);
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetVlanTagInfo                               */
/*                                                                           */
/*    Description         : This function used to get the VlanId in the      */
/*                          ingress packet.                                  */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          u2InPort - Incoming interface Port Number        */
/*                                                                           */
/*    Output(s)           : pVlanId  - Vlan Associated with the packet.      */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetVlanTagInfo (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4InPort,
                    tVlanId * pVlanId)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4InPort);
    UNUSED_PARAM (pVlanId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPortOperPointToPointUpdate                   */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port's Oper P2P   */
/*                          status get changed.                              */
/*                                                                           */
/*    Input(s)            : u2IfIndex - Port for which P2P status changed. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPortOperPointToPointUpdate (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPortStpStateUpdate                           */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port changes to   */
/*                          discarding or Forwarding                         */
/*                                                                           */
/*    Input(s)            : u2IfIndex - Port for which STP state changed.    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPortStpStateUpdate (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetBaseBridgeMode ()                         */
/*                                                                           */
/*    Description         : This function gets the Base Bridge Mode          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gpVlanContextInfo->i4BaseBridgeMode        */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : i4BaseBridgeMode                                  */
/*****************************************************************************/
INT4
VlanGetBaseBridgeMode ()
{
    return VLAN_CUSTOMER_BRIDGE_MODE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortVlanListFromIfIndex                      */
/*                                                                           */
/* Description        : This routine is called to get the vlan list for the  */
/*                      given IfIndex.                                       */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id                           */
/*                      u4IfIndex     - IfIndex                              */
/*                      u1VlanPortType - Port Vlan Type                      */
/*                      u1VlanPortType values may be                         */
/*                                   1. UnTagged member port -0              */
/*                                   2. tagged member port - 1               */
/*                                   3. member port (tagged or untagged)-2   */
/*                                                                           */
/* Output(s)          : pVlanIdList  -Vlan List                              */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetPortVlanListFromIfIndex (UINT4 u4ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pVlanIdList,
                                 UINT4 u4IfIndex, UINT1 u1VlanPortType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pVlanIdList);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1VlanPortType);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : VlanVplsLearn                                              */
/*                                                                           */
/* Description  : Learns the VC-Mac-Vlan association from Vlan MacMap table  */
/*                Here physical port number is ZERO since recv port can be a */
/*                L3 port too and PW is operating on it.                     */
/*                                                                           */
/* Input        : MacAddr - mac address to be learnt                         */
/*                ValnId - source vlan id                                    */
/*                u4PwVcIndex - VC id on which packet was received           */
/*                u4ContextId - Virtual switch instance number               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
VlanVplsLearn (UINT4 u4ContextId, tMacAddr MacAddr, tVlanId VlanId,
               UINT4 u4PwVcIndex)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4PwVcIndex);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanValidateVlanId                               */
/*                                                                           */
/*    Description         : This function Validates the VlanId (STUB)        */
/*                                                                           */
/*    Input(s)            : u4VlanId - Vlan Idto be validated                */
/*                          : u4ContextId - Context Id                       */
/*                                                                           */
/*    Output(s)           :                                                  */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_SUCCESS -                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanValidateVlanId (UINT4 u4ContextId, UINT4 u4VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnFwdMplsPktOnPorts                       */
/*                                                                           */
/*    Description         : This function is called by MPLS Module to post a */
/*                          Packet received on a MPLS Interface to be fwd to */
/*                          Ethernet Ports                                   */
/*                                                                           */
/*    Input(s)            : pBuf - Packet received on MPLS Interface         */
/*                          u4ContextId - Context in which the Packet should */
/*                          be processed.                                    */
/*                          VlanId - VlanId for which the packet is          */
/*                          classified                                       */
/*                          u1Priority - priority of the pkt recvd from MPLS */
/*                          u1PwMode - Raw / Tagged Mode                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2VpnFwdMplsPktOnPorts (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                            tVlanId VlanId, UINT1 u1Priority, UINT1 u1PwMode,
                            UINT4 u4OutPort)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Priority);
    UNUSED_PARAM (u1PwMode);
    UNUSED_PARAM (u4OutPort);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnAddL2VpnInfo                            */
/*                                                                           */
/*    Description         : This function is called by MPLS Module to create */
/*                          a L2Vpn Map Entry in the VLAN Module             */
/*                                                                           */
/*    Input(s)            : pVplsInfo - Structure containing Information     */
/*                          about L2Vpn Map Entry                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2VpnAddL2VpnInfo (tVplsInfo * pVplsInfo)
{
    UNUSED_PARAM (pVplsInfo);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanL2VpnDelL2VpnInfo                            */
/*                                                                           */
/*    Description         : This function is called by MPLS Module to delete */
/*                          a L2Vpn Map Entry in the VLAN Module             */
/*                                                                           */
/*    Input(s)            : pVplsInfo - Structure containing Information     */
/*                          about L2Vpn Map Entry                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanL2VpnDelL2VpnInfo (tVplsInfo * pVplsInfo)
{
    UNUSED_PARAM (pVplsInfo);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeletePortAndCopyProperties                  */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port is added to  */
/*                          port-channel in LA module                        */
/*                                                                           */
/*    Input(s)            : u4IfIndex- The number of the port that is deleted*/
/*                          u4PoIndex- The portchannel index                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDeletePortAndCopyProperties (UINT4 u4IfIndex, UINT4 u4PoIndex)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PoIndex);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : VlanSetShortAgeoutTime                               */
/*                                                                           */
/*    Description     : This function applies a short ageout time for aging  */
/*                      out dynamically learnt mac address on this port.     */
/*                       This function will be called                        */
/*                         By RSTP when operating in StpCompatible mode      */
/*                         during topology change in the port                */
/*                                                                           */
/*    Input(s)        : u2Port - port number                                 */
/*                      i4Agingtime - The short age out time to be applied   */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSetShortAgeoutTime (UINT4 u4PortNum, INT4 i4AgingTime)
{
    UNUSED_PARAM (u4PortNum);
    UNUSED_PARAM (i4AgingTime);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanPortOperInd()                                */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever Port Oper Status is    */
/*                          changed. If Link Aggregation is enabled, then    */
/*                          Oper Down indication is given for each port      */
/*                          initially. When the Link Aggregation Group is    */
/*                          formed, then Oper Up indication is given.        */
/*                          In the Oper Down state, frames will neither      */
/*                          be transmitted nor be received.                  */
/*                                                                           */
/*    Input(s)            : u2Port    - The Number of the Port               */
/*                          u1OperStatus - VLAN_OPER_UP / VLAN_OPER_DOWN     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPortOperInd (UINT4 u2Port, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1OperStatus);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanFdbId                                    */
/*                                                                           */
/* Description        : This routine returns the FdbId corresponding to the  */
/*                      given VlanId. It accesses the L2Iwf common database  */
/*                                                                           */
/* Input(s)           : VlanId      - Vlan whose Fdb Id is to obtained       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FdbId value corresponding to the VlanId              */
/*****************************************************************************/
UINT4
L2IwfGetVlanFdbId (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : VlanDeleteFdbEntries                                 */
/*                                                                           */
/*    Description     : This function flushes the FDB entries in FDB  Tbl    */
/*                      learned on this port.                                */
/*                       This function will be called                        */
/*                      1) By RSTP during topology change in the port        */
/*                      2) By PNAC module during port down indication        */
/*                      3) By VLAN module during port deletion               */
/*                                                                           */
/*    Input(s)        : u2Port - port number                                 */
/*                      i4OptimizeFlag - Indicates whether this call can be  */
/*                                       grouped with the flush call for     */
/*                                       other ports as a single flush call  */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDeleteFdbEntries (UINT4 u4Port, INT4 i4OptimizeFlag)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (i4OptimizeFlag);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsVlanActive                                    */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      Vlan is active.                                      */
/*                                                                           */
/* Input(s)           : VlanId                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
L2IwfIsVlanActive (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetNextActiveVlan                               */
/*                                                                           */
/* Description        : This routine returns the next active vlan in the     */
/*                      system. This is used by the AST module  to know the  */
/*                      active vlans in the system.                          */
/*                                                                           */
/* Input(s)           : u2VlanId    - Vlan Index whose next vlan is to be    */
/*                                    determined                             */
/*                                                                           */
/* Output(s)          : *pu2NextVlanId -  pointer to the next active VlanId  */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfGetNextActiveVlan (UINT2 u2VlanId, UINT2 *pu2NextVlanId)
{
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (pu2NextVlanId);
    return L2IWF_FAILURE;

}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanEgressPorts                              */
/*                                                                           */
/* Description        : This routine returns the Egress ports for the given  */
/*                      VlanId. It accesses the L2Iwf common database        */
/*                                                                           */
/* Input(s)           : VlanId      - Vlan whose Egress ports are to be      */
/*                                    obtained                               */
/*                                                                           */
/* Output(s)          : EgressPorts - Egress Port List for the given Vlan    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVlanEgressPorts (tVlanId VlanId, tPortList EgressPorts)
{
    UNUSED_PARAM (VlanId);
    MEMSET (EgressPorts, 0, sizeof (tPortList));
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanLocalEgressPorts                         */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to get the      */
/*                      VLAN egress porlist for the given vlan, instance     */
/*                      L2Iwf returns local port list for the given VLAN     */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                    : VlanId - VLAN Identifier                             */
/*                                                                           */
/* Output(s)          : EgressPortBitmap - egress port list, local port list */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfMiGetVlanLocalEgressPorts (UINT4 u4Instance, tVlanId VlanId,
                                tSnoopPortBmp EgressPortBitmap)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
    MEMSET (EgressPortBitmap, 0, sizeof (tSnoopPortBmp));
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiUpdateDynamicMcastInfo                     */
/*                                                                           */
/*    Description         : This function updated the Multicast table.       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          MacAddr -  MacAddress                            */
/*                          VlanId   - VlanId associated with the multicast  */
/*                                     MacAddress                            */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the port will be added */
/*                                     into the PortList.                    */
/*                                                                           */
/*                                     VLAN_DELETE, then the port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMiUpdateDynamicMcastInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                              tVlanId VlanId, UINT4 u4Port, UINT1 u1Action)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Action);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiDelDynamicMcastInfoForVlan                 */
/*                                                                           */
/*    Description         : This function removes all the dynamic multicast  */
/*                          information from the table for the specified     */
/*                          Vlan for the given context                       */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanMiDelDynamicMcastInfoForVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteAllDynamicDefGroupInfo                 */
/*                                                                           */
/*    Description         : This function removes all the dynamic service req*/
/*                          information from the table.                      */
/*                                                                           */
/*    Input(s)            : u2Port - This is currently called from GARP, so  */
/*                          the port is local port                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteAllDynamicDefGroupInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUpdateDynamicDefGroupInfo                    */
/*                                                                           */
/*    Description         : This function updates the dynamic entries in the */
/*                          Default GroupInfo table.                         */
/*                                                                           */
/*    Input(s)            : u1Type   - The Type of group has to be updated   */
/*                          VlanId   - The VlanId                            */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the port will be added */
/*                                     into the PortList.                    */
/*                                     VLAN_DELETE, then the port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUpdateDynamicDefGroupInfo (UINT4 u4ContextId, UINT1 u1Type, tVlanId VlanId,
                               UINT2 u2Port, UINT1 u1Action)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1Action);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : VlanMiSnoopGetTxPortList                         */
/*                                                                           */
/*    Description         : This function is invoked by IGS to get the       */
/*                          list of ports for Transmission of packet for a   */
/*                          given instance.                                  */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : u4Instance - Instance Id                         */
/*                        : DestAddr - Destination mac address of the frame  */
/*                          u2InPort - Port on which the packet is received  */
/*                          VlanId   - Vlan ID associated with the packet    */
/*                          u1Action - Flag  to indicate whether the pkt     */
/*                                     should be Forwarded on all ports or   */
/*                                     on the specific ports given by IGS    */
/*                          IgsPortList - a specific Port list in which      */
/*                                        the packet s to be transmitted.    */
/*                                                                           */
/*    Output(s)           : TagPortList   - Tag Port list for Tx             */
/*                        : UntagPortList - Untag Port List for Tx           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns          : VLAN_FORWARD / VLAN_NO_FORWARD                      */
/*****************************************************************************/
INT4
VlanMiSnoopGetTxPortList (UINT4 u4ContextId, tMacAddr DestAddr, UINT4 u4InPort,
                          tVlanId VlanId, UINT1 u1Action,
                          tPortList IgsPortList, tPortList TagPortList,
                          tPortList UntagPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (DestAddr);
    UNUSED_PARAM (u4InPort);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Action);
    UNUSED_PARAM (IgsPortList);
    UNUSED_PARAM (TagPortList);
    UNUSED_PARAM (UntagPortList);

    return VLAN_NO_FORWARD;
}

/*****************************************************************************/
/*    Function Name       : VlanApiForwardOnPorts                            */
/*                                                                           */
/*    Description         : This function is invoked by IGS for forwarding   */
/*                          the mcast packets received on a port to a list of*/
/*                          ports for a given instance.                      */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pBuf - Packet to be forwarded.                   */
/*                          pVlanFwdInfo- Forwarding information needed by   */
/*                                        VLAN to transmit the packet.       */
/*                                                                           */
/*    Output(s)           :  None                                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns          : VLAN_FORWARD / VLAN_NO_FORWARD                      */
/*****************************************************************************/
INT4
VlanApiForwardOnPorts (tCRU_BUF_CHAIN_DESC * pBuf, tVlanFwdInfo * pVlanFwdInfo)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pVlanFwdInfo);

    return VLAN_NO_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiDeleteAllDynamicMcastInfo                  */
/*                                                                           */
/*    Description         : This function removes all the dynamic multicast  */
/*                          information from the table.                      */
/*                                                                           */
/*    Input(s)            : u4ContextID - Context Id                         */
/*                          u2Port - Port Number of the input port           */
/*                          u1MacAddrType - Mac Address type of the dynamic  */
/*                          mcast entries to be deleted (IPV4 or IPV6 mac    */
/*                          address type).Value 1 indicates ipv4 mac address */
/*                          type and value 2 indicates ipv6 mac address type */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanMiDeleteAllDynamicMcastInfo (UINT4 u4ContextId, UINT4 u4Port,
                                 UINT1 u1MacAddrType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1MacAddrType);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanAddOuterTagInFrame                           */
/*                                                                           */
/*    Description         : This function takes care of adding the VLAN tag  */
/*                          to the given frame since the ethertype will be   */
/*                          based on the port type                           */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          VlanId - VlanID to be tagged                     */
/*                          u1Priority - Priority to be tagged.              */
/*                          u4IfIndex  - Interface Index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD/VLAN_NO_FORWARD                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanAddOuterTagInFrame (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId,
                        UINT1 u1Priority, UINT4 u4IfIndex)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Priority);
    UNUSED_PARAM (u4IfIndex);
    return VLAN_NO_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlanIdList                               */
/*                                                                           */
/*    Description         : This function returns the Customer VLAN list     */
/*                          for the given Port, Service VLAN in the CVID     */
/*                          Registration table.                              */
/*                                                                           */
/*    Input(s)            : u4IfIndex    - Interface Index                   */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : CVlanList    - Customer VLAN List                */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetCVlanIdList (UINT4 u4IfIndex, tVlanId SVlanId, tVlanList CVlanList)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (CVlanList);
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     Function Name    : VlanPbClearEvcL2TunnelCounters                     */
/*                                                                           */
/*     Description      : This function Clears the L2Protocol Tunnel Counters*/
/*                        ,which is invoked from MEF.                        */
/*                                                                           */
/*     Input (s)        : tCliHandle  CliHandle - Context in which the CLI   */
/*                                                command is processed       */
/*                        u4ContextId           - Context ID                 */
/*                        tVlanId VlanId        - Vlan ID                    */
/*                                                                           */
/*     Output (s)       : None                                               */
/*                                                                           */
/*     Global Variables Referred  : gpVlanContextInfo                        */
/*                                                                           */
/*     Global Variables Modified  : None                                     */
/*                                                                           */
/*     RETURNS          : VLAN_SUCCESS/VLAN_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VlanPbClearEvcL2TunnelCounters (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetCVlanInfoOnCustomerPort                   */
/*                                                                           */
/*    Description         : This function returns the tagged Customer VLAN   */
/*                          list and untagged customer VLAN list for the     */
/*                          given Port, Service VLAN in the CVID             */
/*                          Registration table.                              */
/*                                                                           */
/*    Input(s)            : u4IfIndex    - Interface Index                   */
/*                          SVlanId      - Service VLAN ID                   */
/*                                                                           */
/*    Output(s)           : pCVlanInfo - Customer VLAN Information           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetCVlanInfoOnCustomerPort (UINT4 u4IfIndex, tVlanId SVlanId,
                                tCVlanInfo * pCVlanInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (pCVlanInfo);
    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiUpdateStaticVlanInfo                      */
/*                                                                           */
/*    Description         : This function updates the Static VLAN table.     */
/*                          with the input interface being added as  a tagged*/
/*                          member port.                                     */
/*                                                                           */
/*    Input(s)            : VlanId   - The VlanId which is to be statically  */
/*                                     created.                              */
/*                          u4IfIndex - u4IfIndex  Number                    */
/*                          u4Action  - vlan create or delete                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FAILURE/VLAN_SUCCESS                         */
/*                                                                           */
/*****************************************************************************/
INT4
VlanApiUpdateStaticVlanInfo (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex,
                             UINT4 u4Action)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Action);
    return VLAN_FAILURE;
}

#ifdef L2RED_WANTED
/******************************************************************************/
/*  Function Name   : VlanRedStartMcastAudit                                  */
/*                                                                            */
/*  Description     : This function will be invoked by IGS module when the    */
/*                    node becomes active from the standby state. The IGS     */
/*                    module will invoke this function after the completion   */
/*                    of relearning timer expiry and when the forwarding table*/
/*                    mode is mac based.                                      */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
VlanRedStartMcastAudit (VOID)
{
    return;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFlushFdbEntries                              */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires in Fdb     */
/*                          table based on port and FdbId                    */
/*                                                                           */
/*    Input(s)            : u2Port - Port number                             */
/*                          u4FdbId - FdbId                                  */
/*                          i4OptimizeFlag - Indicates whether this call can */
/*                                           be grouped with  the flush call */
/*                                           for other ports as a single     */
/*                                           bridge  flush                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanFlushFdbEntries (UINT4 u4Port, UINT4 u4FdbId, INT4 i4OptimizeFlag)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (i4OptimizeFlag);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortVlanTunnelStatus                         */
/*                                                                           */
/* Description        : This routine returns the port's tunnel status        */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2PortIndex - Index of the port whose tunnel status  */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : bTunnelPort - Boolean value indicating whether       */
/*                                    Tunnelling is enabled on the port      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetPortVlanTunnelStatus (UINT4 u4PortIndex, BOOL1 * pbTunnelPort)
{
    UNUSED_PARAM (u4PortIndex);
    *pbTunnelPort = OSIX_FALSE;
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsDot1xConfigAllowed                            */
/*                                                                           */
/* Description        : This routine checks whether dot1x can be enabled on  */
/*                      this port.                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE/ L2IWF_FALSE                              */
/*****************************************************************************/
INT4
L2IwfIsDot1xConfigAllowed (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return L2IWF_TRUE;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsTunnelPortPresent                             */
/*                                                                           */
/* Description        : This routine scans the L2Iwf common database and     */
/*                      returns TRUE if any port has tunnelling enabled.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
L2IwfIsTunnelPortPresent (VOID)
{
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name   : VlanResetShortAgeoutTime                             */
/*                                                                           */
/*    Description     : This function reverts back to the long ageout time   */
/*                      for aging out out dynamically learnt mac address on  */
/*                      this port.                                           */
/*                       This function will be called                        */
/*                         By RSTP when operating in StpCompatible mode      */
/*                                                                           */
/*    Input(s)        : u2Port - port number                                 */
/*                                                                           */
/*    Output(s)       : None                                                 */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanResetShortAgeoutTime (UINT4 u4PortNum)
{

    UNUSED_PARAM (u4PortNum);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetEnabledStatus                             */
/*                                                                           */
/*    Description         : This function returns whether VLAN is enabled or */
/*                          not in the system                                */
/*                                                                           */
/*    Input(s)            : None                          .                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gu1VlanStatus                              */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE or VLAN_FALSE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetEnabledStatus ()
{
    return VLAN_FALSE;
}

/*****************************************************************************/
/*    Function Name       : VlanSnoopGetTxPortList                             */
/*                                                                           */
/*    Description         : This function is invoked by IGS to get the       */
/*                          list of ports for Transmission of packet.        */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : DestAddr - Destination mac address of the frame  */
/*                          u2InPort - Port on which the packet is received  */
/*                          VlanId   - Vlan ID associated with the packet    */
/*                          u1Action - Flag  to indicate whether the pkt     */
/*                                     should be Forwarded on all ports or   */
/*                                     on the specific ports given by IGS    */
/*                          IgsPortList - a specific Port list in which      */
/*                                        the packet s to be transmitted.    */
/*                                                                           */
/*    Output(s)           : TagPortList   - Tag Port list for Tx             */
/*                        : UntagPortList - Untag Port List for Tx           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns          : VLAN_FORWARD / VLAN_NO_FORWARD                      */
/*****************************************************************************/
INT4
VlanSnoopGetTxPortList (tMacAddr DestAddr, UINT4 u4InPort,
                        tVlanId VlanId, UINT1 u1Action,
                        tPortList IgsPortList, tPortList TagPortList,
                        tPortList UntagPortList)
{
    MEMSET (UntagPortList, 0, sizeof (tPortList));
    MEMSET (TagPortList, 0, sizeof (tPortList));
    UNUSED_PARAM (DestAddr);
    UNUSED_PARAM (u4InPort);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Action);
    UNUSED_PARAM (IgsPortList);

    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanTagFrame                                     */
/*                                                                           */
/*    Description         : This function takes care of adding the VLAN tag  */
/*                          to the given frame.                              */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          VlanId - VlanID to be tagged                     */
/*                          u1Priority - Priority to be tagged.              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanTagFrame (tCRU_BUF_CHAIN_DESC * pFrame, tVlanId VlanId, UINT1 u1Priority)
{

    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Priority);
    return;
}

/**************************************************************************/
/* Function Name       : VlanGetDefPortType                               */
/*                                                                        */
/* Description         : This function finds out the default port type to */
/*                       be kept for a port.                              */
/*                                                                        */
/* Input(s)            : u4IfIndex - The port in which the default        */
/*                                   port-type needs to be determined.    */
/*                                                                        */
/* Output(s)           : None.                                            */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : Bridge port type.                                */
/*                                                                        */
/**************************************************************************/
UINT1
VlanGetDefPortType (UINT4 u4IfIndex)
{
    UINT1               u1BridgePortType = VLAN_CUSTOMER_BRIDGE_PORT;

    UNUSED_PARAM (u4IfIndex);
    return u1BridgePortType;
}

#ifdef NPAPI_WANTED
/***************************************************************************/
/* Function Name    : VlanCopyPortPropertiesToHw ()                        */
/*                                                                         */
/* Description      : This function programs the hardware with all         */
/*                    the Port properties for the port "u2DstPort".        */
/*                    The properties to be copied to "u2DstPort" is        */
/*                    obtained from "u2Port".                              */
/*                    "u2SrcPort" must have been created in Vlan. This     */
/*                    function does not program the Dynamic Vlan and Mcast */
/*                    membership and hence must not be called for ports    */
/*                    that are in OPER UP state. Dynamic Learning is NOT   */
/*                    done when the port is in OPER DOWN state.            */
/*                                                                         */
/* Input(s)         : u4DstPort - The Port whose properties have to be     */
/*                                programmed in the Hardware.              */
/*                                                                         */
/*                  : u4SrcPort - The Port from where the port properties  */
/*                                have to be obtained.                     */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanCopyPortPropertiesToHw (UINT4 u4DstPort, UINT4 u4SrcPort, UINT4 u4ContextId)
{
    UNUSED_PARAM (u4DstPort);
    UNUSED_PARAM (u4SrcPort);
    UNUSED_PARAM (u4ContextId);
    return VLAN_SUCCESS;
}

/***************************************************************************/
/* Function Name    : VlanRemovePortPropertiesFromHw ()                    */
/*                                                                         */
/* Description      : This function removes from the Hardware all the      */
/*                    port properties of "u4DstPort". The exact properties */
/*                    which need to be removed is obtained from the        */
/*                    port "u4SrcPort". This function is typically called  */
/*                    when a physical port is removed from a Port Channel. */
/*                    When the physical port is removed from a Port        */
/*                    Channel, the Port Channel properties must be removed */
/*                    from the physical port. This function will be        */
/*                    invoked with "u4DstPort" as the Physical Port Id and */
/*                    "u4SrcPort" as the Port Channel Id.                  */
/*                    "u4SrcPort" must have been created in VLAN.          */
/*                                                                         */
/*                    This function will also be invoked whenever a Port   */
/*                    is deleted in VLAN. In which, u4SrcPort and          */
/*                    u4DstPort will be same.                              */
/*                                                                         */
/* Input(s)         : u4DstPort - Port whose properties have to be removed */
/*                                from the Hardware.                       */
/*                                                                         */
/*                    u4SrcPort - Port from where the properties to be     */
/*                                removed, have to be obtained.            */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : VLAN_SUCCESS on success,                             */
/*                    VLAN_FAILURE otherwise.                              */
/***************************************************************************/
INT4
VlanRemovePortPropertiesFromHw (UINT4 u4DstPort, UINT4 u4SrcPort)
{
    UNUSED_PARAM (u4DstPort);
    UNUSED_PARAM (u4SrcPort);
    return VLAN_SUCCESS;
}

#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIdentifyVlanIdAndUntagFrame                  */
/*                                                                           */
/*    Description         : This function is called by GARP/IGS/IVR to get   */
/*                          VlanId                                           */
/*                          Assumption is that buffer will be released by    */
/*                          the calling function                             */
/*                                                                           */
/*    Input(s)            : i4ModId  - Module for which the packet is to be  */
/*                                     given.                                */
/*                        : pFrame   - Pointer to the incoming packet        */
/*                          u2InPort - Incoming interface Port Number        */
/*                                                                           */
/*    Output(s)           : pFrame   - Pointer to the updated packet         */
/*                          pVlanId  - Gip Associated with the packet.       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD / VLAN_NO_FORWARD                    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIdentifyVlanIdAndUntagFrame (INT4 i4ModId, tCRU_BUF_CHAIN_DESC * pFrame,
                                 UINT4 u4InPort, tVlanId * pVlanId)
{
    UNUSED_PARAM (i4ModId);
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4InPort);
    UNUSED_PARAM (pVlanId);
    return VLAN_NO_FORWARD;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetVlanInfoAndUntagFrame                     */
/*                                                                           */
/*    Description         : This function is called by L2Iwf to get VLAN     */
/*                          Information and strip of the outer-vlan from the */
/*                          packet before posting the packet to Snoop.       */
/*                          Assumption is that buffer will be released by    */
/*                          the calling function                             */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          u2InPort - Incoming interface Port Number        */
/*                                                                           */
/*    Output(s)           : pFrame   - Pointer to the updated packet         */
/*                          pVlantag  - Pointer to TAG Info of the packet    */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD / VLAN_NO_FORWARD                    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetVlanInfoAndUntagFrame (tCRU_BUF_CHAIN_DESC * pFrame,
                              UINT4 u4InPort, tVlanTag * pVlanTag)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4InPort);
    UNUSED_PARAM (pVlanTag);
    return VLAN_NO_FORWARD;
}

/*****************************************************************************/
/*    Function Name       : VlanIvrGetTxPortOrPortListInCxt                  */
/*                                                                           */
/*    Description         : This function is called when ever IP wants to Tx */
/*                          packet on an logical vlan interface. This fuction*/
/*                          returns the physical ports for trransmission.    */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          VlanId   - Vlan ID                               */
/*                          u4L2ContextId - L2 context ID associated with    */
/*                                          Vlan ID                          */
/*                          u1BcastFlag - Flag to indicate whether the pkt   */
/*                                        should be broadcast or not         */
/*                                                                           */
/*    Output(s)           : pu2OutPort    - Out Port for Tx if known Ucast.  */
/*                        : pbIsTag       - Pkt should be tagged / Untagged  */
/*                        : TagPortList   - Tag Port list for Tx             */
/*                        : UntagPortList - Untag Port List for Tx           */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns          : VLAN_FORWARD / VLAN_NO_FORWARD                      */
/*****************************************************************************/
INT4
VlanIvrGetTxPortOrPortListInCxt (UINT4 u4L2ContextId,
                                 tMacAddr DestAddr, tVlanId VlanId,
                                 UINT1 u1BcastFlag, UINT4 *pu4OutPort,
                                 BOOL1 * pbIsTag, tPortList TagPortList,
                                 tPortList UntagPortList)
{
    UNUSED_PARAM (u4L2ContextId);
    UNUSED_PARAM (DestAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1BcastFlag);
    UNUSED_PARAM (pu4OutPort);
    UNUSED_PARAM (pbIsTag);
    UNUSED_PARAM (TagPortList);
    UNUSED_PARAM (UntagPortList);
    return VLAN_NO_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIvrGetVlanIfOperStatus                       */
/*                                                                           */
/*    Description         :This function is for returning the oper status    */
/*                          of VLAN interface                                */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Interface Index                      */
/*                                                                           */
/*    Output(s)           : pu1OperStatus - Oper status of the VLAN interface*/
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
VlanIvrGetVlanIfOperStatus (UINT4 u4IfIndex, UINT1 *pu1OperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    *pu1OperStatus = CFA_IF_DOWN;
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetFdbEntryDetails                           */
/*                                                                           */
/*    Description         : This function returns Fdb Entry Information.     */
/*                                                                           */
/*    Input(s)            : u4FdbId - Fdb Id.                                */
/*                          MacAddr - Mac Address.                           */
/*                                                                           */
/*    Output(s)           : pu2Port - Port on which the Mac Addr is learnt.  */
/*                        : pu1Status - Flag for Learnt or not.              */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetFdbEntryDetails (UINT4 u4FdbId, tMacAddr MacAddr,
                        UINT2 *pu2Port, UINT1 *pu1Status)
{
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (pu2Port);
    UNUSED_PARAM (pu1Status);

    return VLAN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetProtocolTunnelStatusOnPort               */
/*                                                                           */
/*    Description         : This function returns the tunnel status (Tunnel/ */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) on a port        */
/*                                                                           */
/*    Input(s)            : u2Port - Port number                             */
/*                          u2Protocol     - L2 Protocol                     */
/*                                                                           */
/*    Output(s)           : u1TunnelStatus - Protocol Tunnel status          */
/*                                                                           */
/*    Returns             : None.                                            */
/*                                                                           */
/*****************************************************************************/
VOID
L2IwfGetProtocolTunnelStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                    UINT1 *pu1TunnelStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (pu1TunnelStatus);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetProtocolTunnelStatusPerVlan              */
/*                                                                           */
/*    Description         : This function returns the tunnel status (Tunnel/ */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) on a vlan        */
/*                                                                           */
/*    Input(s)            : u4ContextId - contextId                          */
/*                          u4VlanId    - VlanId                             */
/*                          u2Protocol  - L2 Protocol                        */
/*                                                                           */
/*    Output(s)           : pu1TunnelStatus - Protocol Tunnel status          */
/*                                                                           */
/*    Returns             : None.                                            */
/*                                                                           */
/*****************************************************************************/
VOID
L2IwfGetProtocolTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                     UINT2 u2Protocol, UINT1 *pu1TunnelStatus)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (pu1TunnelStatus);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetL2ProtocolTunnelStatus                   */
/*                                                                           */
/*    Description         : This function returns the tunnel status (Tunnel/ */
/*                          Peer/Discard) for protocols based on Per Port/   */
/*                          Per Vlan Service type                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - contextId                          */
/*                          u4IfIndex   - PortId                             */
/*                          VlanId      - VlanId                             */
/*                          u4ProtocolId - L2 Protocol                       */
/*                                                                           */
/*    Output(s)           : pu1TunnelStatus - Protocol Tunnel status         */
/*                          pu1L2CPProtType - Protocol Type(Port/VLan) based */
/*                                                                           */
/*    Returns             : L2IWF_FAILURE/L2IWF_SUCCESS.                     */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfGetL2ProtocolTunnelStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                tVlanId VlanId, UINT4 u4ProtocolId,
                                UINT1 *pu1TunnelStatus, UINT1 *pu1L2CPProtType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4ProtocolId);
    UNUSED_PARAM (pu1TunnelStatus);
    UNUSED_PARAM (pu1L2CPProtType);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetL2CPProtcolType                              */
/*                                                                           */
/* Description        : This routine is called to get the L2CP               */
/*                      protocol type(Port/Vlan) based.                      */
/*                                                                           */
/* Input(s)           : u4ProtocolId      - Portocol ID                      */
/*                                                                           */
/* Output(s)          : pu1L2CPProtType - Protocol Type                      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetL2CPProtcolType (UINT4 u4ProtocolId, UINT1 *pu1L2CPProtType)
{
    UNUSED_PARAM (u4ProtocolId);
    UNUSED_PARAM (pu1L2CPProtType);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetOverrideOption                               */
/*                                                                           */
/* Description        : This routine is called to set the override           */
/*                      option.                                              */
/*                                                                           */
/* Input(s)           : u2Port      - Port ID                                */
/*                      u1OverrideOption - OverrideOption                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetOverrideOption (UINT4 u4Port, UINT1 u1OverrideOption)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1OverrideOption);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetOverrideOption                               */
/*                                                                           */
/* Description        : This routine is called to get the override           */
/*                      option status.                                       */
/*                                                                           */
/* Input(s)           : u2Port      - Port ID                                */
/*                      u1OverrideOption - OverrideOption                    */
/*                                                                           */
/* Output(s)          : u1OverrideOption - OverrideOption                    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
VOID
L2IwfGetOverrideOption (UINT4 u4Port, UINT1 *pu1OverrideOption)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pu1OverrideOption);

    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetBridgeMode                                   */
/*                                                                           */
/* Description        : This function returns the Bridge mode configured     */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : pu4BridgeMode - Bridge mode configured in L2IWF      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    UNUSED_PARAM (u4ContextId);
    *pu4BridgeMode = L2IWF_CUSTOMER_BRIDGE_MODE;
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanIsDeiBitSet                                     */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      Dei Bit ise set TRUE or FALSE on the Port.           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Actual Port Number                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
VlanIsDeiBitSet (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function Name      : VlanTagOutFrame                                      */
/*                                                                           */
/* Description        : This routine is called to add/delete/update the      */
/*                      VLAN TAG in the frame as per the current VLAN rules. */
/*                      Dei Bit ise set TRUE or FALSE on the Port.           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context identifier                     */
/*                      u2PortNum - local port number                        */
/*                      pBuf - Pointer to the CRU buffer.                    */
/*                      pVlanTagInfo - VLAN tag information of the frame     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/

INT4
VlanTagOutFrame (UINT4 u4ContextId, UINT2 u2PortNum,
                 tCRU_BUF_CHAIN_HEADER * pBuf, tVlanTagInfo * pVlanTagInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pVlanTagInfo);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortVlanList                                 */
/*                                                                           */
/* Description        : This routine is called to get the vlan list.if       */
/*                      argument u1VlanPortType is Tagged then this function */
/*                      will return the List of vlans for which this port is */
/*                      untagged Member.If the u1VlanPortType is untagged or */
/*                      mamber port then this functiion will return the list */
/*                      of vlans for which port is untagged memberor both    */
/*                      tagged & untagged List                               */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id                           */
/*                      u2LocalPortId- Local Port Number                     */
/*                      u1VlanPortType - Port Vlan Type                      */
/*                      u1VlanPortType values may be                         */
/*                                   1. UnTagged member port -0              */
/*                                   2. tagged member port - 1               */
/*                                   3. member port (tagged or untagged)-2   */
/*                                                                           */
/* Output(s)          : pVlanIdList  -Vlan List                              */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetPortVlanList (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pVlanIdList,
                      UINT2 u2LocalPortId, UINT1 u1VlanPortType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pVlanIdList);
    UNUSED_PARAM (u2LocalPortId);
    UNUSED_PARAM (u1VlanPortType);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : L2IwfGetPortVlanMemberList                         */
/*                                                                           */
/* Description          : Returns the List of VLANs for which the particular */
/*                        port is member of.                                 */
/*                                                                           */
/* Input (s)            : u2VlanId - VLAN ID value.                          */
/*                        pu1VlanList - VLAN List                            */
/*                                                                           */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE.                     */
/*****************************************************************************/
INT4
L2IwfGetPortVlanMemberList (UINT4 u4IfIndex, UINT1 *pu1VlanList)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1VlanList);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfSetProtocolTunnelStatusOnPort               */
/*                                                                           */
/*    Description         : This function sets the tunnel status (Tunnel/    */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) on a port        */
/*                                                                           */
/*    Input(s)            : u2Port         - Port number                     */
/*                          u2Protocol     - L2 Protocol                     */
/*                          u1TunnelStatus - Protocol tunnel status          */
/*                                           on a port.                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : L2IWF_SUCCESS / L2IWF_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                    UINT2 u2Protocol, UINT1 u1TunnelStatus)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (u1TunnelStatus);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfSetProtocolTunnelStatusPerVlan               */
/*                                                                           */
/*    Description         : This function sets the tunnel status (Tunnel/    */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) per vlan         */
/*                                                                           */
/*    Input(s)            : u4VlanId       - Vlan ID                         */
/*                          u4ContextId    - Context ID                      */
/*                          u2Protocol     - L2 Protocol                     */
/*                          u1TunnelStatus - Protocol tunnel status          */
/*                                           on particular vlan.             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : L2IWF_SUCCESS / L2IWF_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfSetProtocolTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                     UINT2 u2Protocol, UINT1 u1TunnelStatus)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (u2Protocol);
    UNUSED_PARAM (u1TunnelStatus);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetTunnelStatusPerVlan                          */
/*                                                                           */
/* Description        : This routine is called to set the                    */
/*                      tunnel status.                                       */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context ID                            */
/*                      u4VlanId     - Vlan Id                               */
/*                      u1TunnelStatus - TunnelStatus(ENABLE/DISABLE)        */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                             UINT1 u1TunnelStatus)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (u1TunnelStatus);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetAllToOneBndlStatus                           */
/*                                                                           */
/* Description        : This routine is called from VLAN to get the          */
/*                      All To One Bundling Status from L2IWF COntext Info   */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                                                                           */
/* Output(s)          : pu1Status - All to one bundling status               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetAllToOneBndlStatus (UINT4 u4ContextId, UINT1 *pu1Status)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu1Status);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsMvrpFrameTagged                            */
/*                                                                           */
/*    Description         : This function checks whether the given the MVRP  */
/*                          frame has any Vlan Tag or Not.                   */
/*                          MVRP packet should not contain any VLAN tag. If  */
/*                          it is present in that case the frame needs to    */
/*                          be discarded by the caller.                      */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                          u4Port - Port on which the mvrp frame was        */
/*                          received                                         */
/*                          *pi4FrameType - VLAN_TAGGED/VLAN_UNTAGGED        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns                 : VLAN_SUCCESS                                 */
/*                              VLAN_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsMvrpFrameTagged (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4Port,
                       INT4 *pi4FrameType)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pi4FrameType);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMvrpEnableInd ()                             */
/*                                                                           */
/*    Description         : Mvrp indicates Vlan, that Mvrp is enabled.       */
/*                          If there are any Static Vlan entry configured    */
/*                          then, they must be propagated.                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanMvrpEnableInd (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMmrpEnableInd ()                             */
/*                                                                           */
/*    Description         : Mmrp indicates Vlan, that Mmrp is enabled.       */
/*                          If there are any Static Mcast / Ucast entry      */
/*                          configured then, they must be propagated.        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanMmrpEnableInd (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMvrpPortEnableInd                            */
/*                                                                           */
/*    Description         : Mvrp indicates Vlan, that Mvrp is enabled in the */
/*                          port. If there are any Static Vlan entry         */
/*                          configured then, they must be propagated.        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2Port      - LocalPort Identifier               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanMvrpPortEnableInd (UINT4 u4ContextId, UINT2 u2Port)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMmrpPortEnableInd ()                         */
/*                                                                           */
/*    Description         : Mmrp indicates Vlan, that Mmrp is enabled on port*/
/*                          If there are any Static Mcast / Ucast entry      */
/*                          configured then, they must be propagated.        */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          u2Port      - LocalPort Identifier               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*****************************************************************************/
VOID
VlanMmrpPortEnableInd (UINT4 u4ContextId, UINT2 u2Port)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeleteAllDynamicVlanInfo                     */
/*                                                                           */
/*    Description         : Removes all the dynamic vlan information         */
/*                          from the Current table.                          */
/*                                                                           */
/*    Input(s)            : u2Port - Port Number of the input port.          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanDeleteAllDynamicVlanInfo (UINT4 u4ContextId, UINT2 u2Port)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCheckAndTagOutgoingGmrpFrame                 */
/*                                                                           */
/*    Description         : This function is called by GARP/MRP module to    */
/*                          transmit GARP/MRP PDUs.                          */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanId   - VlanId                                */
/*                          u2Port   - Port on which the GARP PDU is to be   */
/*                                     transmitted.                          */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_FORWARD/                                     */
/*                         VLAN_NO_FORWARD                                   */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCheckAndTagOutgoingGmrpFrame (UINT4 u4ContextId, tCRU_BUF_CHAIN_DESC * pBuf,
                                  tVlanId VlanId, UINT2 u2Port)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
    return VLAN_FORWARD;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsMcastWildCardEntryPresent                  */
/*                                                                           */
/*    Description         : This function checks if the Multicast MacAddress */
/*                          is registered as wild card entry                 */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          MacAddr -  MAC address                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsMcastWildCardEntryPresent (UINT4 u4ContextId, UINT2 u2VlanId,
                                 tMacAddr MacAddr)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (MacAddr);
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsUcastWildCardEntryPresent                  */
/*                                                                           */
/*    Description         : This function checks if the Unicast MacAddress   */
/*                          is registered as wild card entry                 */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2VlanId    - Vlan Id                            */
/*                          MacAddr     -  MAC address                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_TRUE/OSIX_FALSE                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsUcastWildCardEntryPresent (UINT4 u4ContextId, UINT2 u2VlanId,
                                 tMacAddr MacAddr)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (MacAddr);
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUpdateDynamicVlanInfo                        */
/*                                                                           */
/*    Description         : This function updates the Current vlan table.    */
/*                                                                           */
/*    Input(s)            : VlanId   - The VlanId which is learnt by GVRP    */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the Port will be added */
/*                                     into the PortList.                    */
/*                                                                           */
/*                                     VLAN_DELETE, then the Port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS on success                           */
/*                         VLAN_FAILURE on failure                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUpdateDynamicVlanInfo (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2Port,
                           UINT1 u1Action)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1Action);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFlushFdbEntriesOnPort                        */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires in Fdb     */
/*                          table based on port and FdbId                    */
/*                                                                           */
/*    Input(s)            : u2Port   - Port number                           */
/*                          u4FdbId  - FdbId                                 */
/*                          i4ModId  - STP_MODULE/MRP_MODULE                 */
/*                          i4OptimizeFlag - Indicates whether this call can */
/*                                           be grouped with  the flush call */
/*                                           for other ports as a single     */
/*                                           bridge  flush                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanFlushFdbEntriesOnPort (UINT4 u4Port, UINT4 u4FdbId, INT4 i4ModId,
                           INT4 i4OptimizeFlag)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4FdbId);
    UNUSED_PARAM (i4ModId);
    UNUSED_PARAM (i4OptimizeFlag);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsVlanDynamic                                */
/*                                                                           */
/*    Description         : This function checks if the VLAN is dynamically  */
/*    Input(s)            : VlanId - Vlan ID                                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE/VLAN_FALSE                              */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsVlanDynamic (UINT4 u4ContextId, UINT2 VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiUpdateDynamicUcastInfo                     */
/*                                                                           */
/*    Description         : This function updated the Group table.           */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                          MacAddr -  MacAddress                            */
/*                          VlanId   - VlanId associated with the unicast    */
/*                                     MacAddress                            */
/*                          u2Port   - The Port Number                       */
/*                          u1Action - VLAN_ADD, then the port will be added */
/*                                     into the PortList.                    */
/*                                                                           */
/*                                     VLAN_DELETE, then the port will be    */
/*                                     deleted from the PortList.            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMiUpdateDynamicUcastInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                              tVlanId VlanId, UINT4 u4Port, UINT1 u1Action)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Action);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiDelDynamicDefGroupInfoForVlan              */
/*                                                                           */
/*    Description         : This function removes all the dynamic default    */
/*                          group information from the table for the         */
/*                          specified Vlan                                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanMiDelDynamicDefGroupInfoForVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanPortType                                 */
/*                                                                           */
/* Description        : This routine returns the port tunnel type            */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global Port Index of the port whose port */
/*                               type is to be obtained.                     */
/*                                                                           */
/* Output(s)          : pPortType   - Port Type VLAN_ACCESS_PORT/            */
/*                                              VLAN_HYBRID_PORT/            */
/*                                              VLAN_TRUNK_PORT              */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVlanPortType (UINT4 u4IfIndex, UINT1 *pPortType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pPortType);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIdentifyVlanId                               */
/*                                                                           */
/*    Description         : This function is called by IVR to get the vlanId */
/*                                                                           */
/*    Input(s)            : pFrame   - Pointer to the incoming packet        */
/*                          u2InPort - Incoming interface Port Number        */
/*                                                                           */
/*    Output(s)           : pVlanId  - Gip Associated with the packet.       */
/*                                                                           */
/*    Returns            : VLAN_FORWARD / VLAN_NO_FORWARD                    */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIdentifyVlanId (tCRU_BUF_CHAIN_DESC * pFrame,
                    UINT4 u4InPort, tVlanId * pVlanId)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4InPort);
    UNUSED_PARAM (pVlanId);
    return VLAN_NO_FORWARD;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanPortPvid                                 */
/*                                                                           */
/* Description        : This routine returns the port VLAN Id for the given  */
/*                      given Port Index. It accesses the L2Iwf common       */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global Port Index of the port whose port */
/*                               type is to be obtained.                     */
/*                                                                           */
/* Output(s)          : pPortPvid   - Port VLAN Id.                          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVlanPortPvid (UINT4 u4IfIndex, tVlanId * pVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pVlanId);
    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCheckWildCardEntry                           */
/*                                                                           */
/*    Description         : This function checks for WildCard entry for      */
/*                          given mac                                        */
/*                                                                           */
/*    Input(s)            : WildCardMacAddr - Mac address of wild card entry */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VLAN_TRUE/VLAN_FALSE                             */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCheckWildCardEntry (tMacAddr WildCardMacAddr)
{
    UNUSED_PARAM (WildCardMacAddr);

    return VLAN_TRUE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanLearningType                             */
/*                                                                           */
/* Description        : This routine is called to get the vlan learning      */
/*                      type.                                                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Learning Type                                        */
/*****************************************************************************/
UINT1
L2IwfGetVlanLearningType (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);

    return VLAN_INDEP_LEARNING;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetVlanLearningType                             */
/*                                                                           */
/* Description        : This routine is called to set the vlan learning      */
/*                      type.                                                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u1LearnMode - Learning type                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVlanLearningType (UINT4 u4ContextId, UINT1 u1LearnMode)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1LearnMode);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanApiEvbGetSystemStatus                            */
/*                                                                           */
/* Description        : This function returns the EVB system status on this  */
/*                      context Id.                                          */
/*                                                                           */
/* Input(s)           : u4ContextID - Context Id.                            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VLAN_TRUE/VLAN_FALSE                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
VlanApiEvbGetSystemStatus (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiEvbGetSbpPortsOnUap                       */
/*                                                                           */
/*    Description         : This function retrieves the SBP ports present on */
/*                          the UAP.                                         */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                                                                           */
/*    Output(s)           : pSbpArray    - SBP ports.                        */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/
VOID
VlanApiEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex, tVlanEvbSbpArray * pSbpArray)
{
    UNUSED_PARAM (u4UapIfIndex);
    UNUSED_PARAM (pSbpArray);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiGetSChIfIndex                             */
/*                                                                           */
/*    Description         : This function provides the S-Channel IfIndex for */
/*                          the given SVID and UAP IfIndex.                  */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                          u2SVID       - u2SVID                            */
/*                          u4ContextId  - Context Identifier                */
/*                                                                           */
/*    Output(s)           : pu4SChIfIndex- S-Channel IfIndex                 */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanApiGetSChIfIndex (UINT4 u4ContextId, UINT4 u4UapIfIndex,
                      UINT2 u2SVID, UINT4 *pu4SChIfIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4UapIfIndex);
    UNUSED_PARAM (u2SVID);
    UNUSED_PARAM (pu4SChIfIndex);
    return VLAN_FAILURE;
}

/*****************************************************************************/
/* Function Name      : VlanCheckPortType                                    */
/*                                                                           */
/* Description        : This function will check if the port is a tagged     */
/*                      member of the Vlan                                   */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u2VlanId - Vlan Id                                   */
/*                      u2Port - Port Number                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_TRUE/VLAN_FALSE                                 */
/*****************************************************************************/
INT4
VlanCheckPortType (UINT2 u2VlanId, UINT2 u2Port)
{
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u2Port);
    return VLAN_TRUE;
}

/*****************************************************************************/
/* Function Name      : VlanHandleSTPStatusChange                            */
/*                                                                           */
/* Description        : This routine handles STP status change from STP      */
/*                      for setting the corresponding tunnel status          */
/*                      in the required port                                 */
/*                                                                           */
/* Input(s)           : tAstSTPTunnelInfo structure from STP                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
VlanHandleSTPStatusChange (tAstSTPTunnelInfo STPTunnelInfo)
{
    UNUSED_PARAM (STPTunnelInfo);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanConfigDot1qPvidForPvrstStart                 */
/*                                                                           */
/*    Description         : This function is called by PVRST module when     */
/*                          the module is started.                  */
/*                          When PVID is already configured                  */
/*                          on the port, the port will be set to untagged    */
/*                          member of that PVID                              */
/*                                      */
/*    Input(s)            : u2Port - Port Number                             */
/*                          VlanId - VLAN ID (PVID)                          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanConfigDot1qPvidForPvrstStart (UINT2 u2Port, tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanPbPortEncapTypeIsDot1q                           */
/*                                                                           */
/* Description        : This function will check the encaptype of the port as*/
/*                      DOT1q or not.                                        */
/*                                                                           */
/* Input(s)           : u4FsPbPort                                           */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPbPortEncapTypeIsDot1q (UINT4 u4FsPbPort)
{
    UNUSED_PARAM (u4FsPbPort);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDelFdbEntryByMacAddr                         */
/*                                                                           */
/*    Description         : This function deletes Fdb Entry Information based*/
/*                          on MAC addr input                                */
/*                                                                           */
/*    Input(s)            : MacAddr - Mac Address.                           */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanDelFdbEntryByMacAddr (UINT1 *pu1MacAddr)
{
    UNUSED_PARAM (pu1MacAddr);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiGetSChInfoFromSChIfIndex                  */
/*                                                                           */
/*    Description         : This function provides the UAP IfIndex and SVID  */
/*                          for the given SCh IfIndex.                       */
/*                                                                           */
/*    Input(s)            : u4SChIfIndex - S-Channel IfIndex.                */
/*                          u4ContextId  - Context Identifier                */
/*                                                                           */
/*    Output(s)           : pu4UapIfIndex- UAP IfIndex                       */
/*                          pu2SVID      - SVID                              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanApiGetSChInfoFromSChIfIndex (UINT4 u4SChIfIndex, UINT4 *pu4UapIfIndex,
                                 UINT2 *pu2SVID)
{
    *pu4UapIfIndex = u4SChIfIndex;
    UNUSED_PARAM (pu2SVID);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanPortCfaDeleteSChannelInterface                   */
/*                                                                           */
/* Description        : Deletes the S-Channel interface in CFA and thereby   */
/*                      indicating to L2IWF to create the S-Channel interface*/
/*                      to VLAN, IGMP Snooping and FIP Snooping.             */
/*                                                                           */
/* Input(s)           : u4SChIfIndex - S-Channel interface index.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/
INT4
VlanPortCfaDeleteSChannelInterface (UINT4 u4SChIfIndex)
{
    UNUSED_PARAM (u4SChIfIndex);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanPortCfaGetFreeSChIndexForUap                     */
/*                                                                           */
/* Description        :This function provides the unique interface index for */
/*                     the  S-Channel interface for the given UAP.           */
/*                     Using this interface index, SBP port is               */
/*                     created in CFA and in the respective layer 2 modules  */
/*                                                                           */
/* Input(s)           : u4UapIfIndex - UAP interface index                   */
/*                                                                           */
/* Output(s)          : u4IfIndex - Unique interface index if available      */
/*                      zero - otherwise                                     */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
INT4
VlanPortCfaGetFreeSChIndexForUap (UINT4 u4UapIfIndex, UINT4 *pu4SChIfIndex)
{
    UNUSED_PARAM (u4UapIfIndex);
    UNUSED_PARAM (pu4SChIfIndex);
    return VLAN_SUCCESS;
}
#endif

#ifndef GARP_WANTED
/************************************************************************/
/*  Function Name    : GarpPortOperInd ()                               */
/*  Description      : Invoked by VLAN whenever Port Oper Status is     */
/*                     changed. If Link Aggregation is enabled, then    */
/*                     Oper Down indication is given for each port      */
/*                     initially. When the Link Aggregation Group is    */
/*                     formed, then Oper Up indication is given.        */
/*                     In the Oper Down state, frames will neither      */
/*                     be transmitted nor be received.                  */
/*  Input(s)         : u2Port - Port for which Oper Status is changed.  */
/*                     u1OperStatus - GAARP_OPER_UP/GARP_OPER_DOWN      */
/*  Output(s)        : None                                             */
/*  Global Variables                                                    */
/*  Referred         : None                                             */
/*  Global Variables                                                    */
/*  Modified         : None                                             */
/*  Exceptions or OS                                                    */
/*  Error Handling   : None                                             */
/*  Use of Recursion : None                                             */
/*  Returns          : GARP_SUCCESS / GARP_FAILURE                      */
/************************************************************************/
INT4
GarpPortOperInd (UINT2 u2Port, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1OperStatus);
    return GARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : GarpRedSendMsg                                       */
/*                                                                           */
/* Description        : This function sends GO_ACTIVE and GO_STANDBY msgs    */
/*                      to GARP module. This fn. will be invoked by VLAN.    */
/*                                                                           */
/* Input(s)           : u4MessageType                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
GarpRedSendMsg (UINT4 u4MessageType)
{
    UNUSED_PARAM (u4MessageType);
    return;
}
#endif

#if (! defined (VLAN_WANTED)) && (! defined (LLDP_WANTED) && (! defined ECFM_WANTED))
/*****************************************************************************/
/* Function Name      : L2IwfPortVlanCrtMemPoolAndRBTree                     */
/*                                                                           */
/* Description        : This routine creates the mempool required for        */
/*                      storing Port Vlan Mapping and creates RBTree in L2Iwf*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPortVlanCrtMemPoolAndRBTree (VOID)
{
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPortVlanDelMemPoolAndRBTree                     */
/*                                                                           */
/* Description        : This routine deletes the mempool and RBTree created  */
/*                      for maintaing Port Vlan Mapping                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPortVlanDelMemPoolAndRBTree (VOID)
{
    return L2IWF_SUCCESS;
}
#endif

#ifndef PNAC_WANTED
/*****************************************************************************/
/* Function Name      : L2IwfSetPortPnacAuthStatus                           */
/*                                                                           */
/* Description        : This routine is called from PNAC to update the       */
/*                      Port's Auth Status  in the L2Iwf common database.    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Index of the port whose Auth Status      */
/*                                    is to be updated.                      */
/*                      u2PortAuthControl - Auth Status  value to be updated */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetPortPnacAuthStatus (UINT4 u4IfIndex, UINT2 u2PortAuthStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2PortAuthStatus);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortPnacPaeStatus                            */
/*                                                                           */
/* Description        : This routine is called from PNAC to update the       */
/*                      Port's Pae Status in the L2Iwf common database.      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Mode  status is to be updated.           */
/*                      u1PortAuthStatus - PNAC enabled/disabled status      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetPortPnacPaeStatus (UINT4 u4IfIndex, UINT1 u1PortPaeStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1PortPaeStatus);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsSrcMacAuthorized                              */
/*                                                                           */
/* Description        : This routine checks whether the supplicant mac       */
/*                      is authorized for mac based authentication mode      */
/*                                                                           */
/* Input(s)           : SrcMacAddr -Source Mac Address in the received frame */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfIsSrcMacAuthorized (UINT1 *pSrcMacAddr)
{
    UNUSED_PARAM (pSrcMacAddr);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortPnacAuthMode                             */
/*                                                                           */
/* Description        : This routine is called from CFA to get the           */
/*                      Port's Auth Mode in the L2Iwf common database.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Mode  is to be retrieved.                */
/*                                                                           */
/* Output(s)          : pu2PortAuthMode - Auth Mode value to be retrieved    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortPnacAuthMode (UINT4 u4IfIndex, UINT2 *pu2PortAuthMode)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu2PortAuthMode);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortPnacPaeStatus                            */
/*                                                                           */
/* Description        : This routine is called from CFA to get the           */
/*                      Port's Pae status in the L2Iwf common database.      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Pae status is to be retrieved.           */
/*                                                                           */
/* Output(s)          : pu1PortPaeStatus - Port pnac enable/disable          */
/*                                         status value to be retrieved      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortPnacPaeStatus (UINT4 u4IfIndex, UINT1 *pu1PortPaeStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1PortPaeStatus);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortPnacAuthControl                          */
/*                                                                           */
/* Description        : This routine returns the port's Auth Control Status  */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2PortIndex - Index of the port whose Auth Control   */
/*                                    status is to be obtained.              */
/*                                                                           */
/* Output(s)          : u2PortAuthControl - Port's Auth Control status       */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortPnacAuthControl (UINT4 u4PortIndex, UINT2 *pu2PortAuthControl)
{
    UNUSED_PARAM (u4PortIndex);
    *pu2PortAuthControl = PNAC_PORTCNTRL_FORCEAUTHORIZED;
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PnacGetStartedStatus                             */
/*                                                                           */
/*    Description         : This function returns whether Pnac is started or */
/*                          shutdown                                         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gPnacSystemInfo.u2SystemControl            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/* Return Value(s)    : PNAC_TRUE or PNAC_FALSE                              */
/*****************************************************************************/
INT4
PnacGetStartedStatus ()
{
    return PNAC_TRUE;
}

/*****************************************************************************/
/* Function Name      : PnacHandleEvent                                      */
/*                                                                           */
/* Description        : This function can be called by external modules to   */
/*                      pass any event to PNAC module.                       */
/*                      Currently this fucntion is called by Bridge Detection*/
/*                      State Machine(RSTP/MSTP), whenever edge port         */
/*                      parameter changes value.                             */
/*                                                                           */
/* Input(s)           : u2PortNum - Port Number.                             */
/*                      u4Event   - Event Passed                             */
/*                      1) PNAC_BRIDGE_DETECTED - Edge Port is FALSE         */
/*                      2) PNAC_BRIDGE_NOT_DETECTED - Edge Port is TRUE      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS / PNAC_FAILURE                          */
/*****************************************************************************/

INT4
PnacHandleEvent (UINT2 u2PortNum, UINT4 u4Event)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (u4Event);
    return PNAC_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PnacGetPnacEnableStatus                              */
/*                                                                           */
/* Description        : This function gets the PNAC Module Status - i.e      */
/*                      Whether it is enabled or disabled                    */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gPnacSystemInfo.u2SystemAuthControl                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PNAC_SUCCESS - if enabled                            */
/*                      PNAC_FAILURE - if disabled                           */
/*****************************************************************************/
INT4
PnacGetPnacEnableStatus (VOID)
{
    return PNAC_FAILURE;
}                                /* PnacGetPnacEnableStatus */

/*****************************************************************************/
/* Function Name      : L2IwfIsPortAuthorized                                */
/*                                                                           */
/* Description        : This routine checks whether the port is a authorized */
/*                      port or not.                                         */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfIsPortAuthorized (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return L2IWF_SUCCESS;
}
#endif

#ifndef LLDP_WANTED

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfAlias
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the change in interface alias
 *
 *    INPUT            : u4IfIndex - Interface index
 *                       pu1IfAlias - pointer to interface alias
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
INT4
LldpApiNotifyIfAlias (UINT4 u4IfIndex, UINT1 *pu1IfAlias)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1IfAlias);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiIsLldpCtrlFrame
 *
 *    DESCRIPTION      : This function checks whether the given frame is
 *                       an LLDP control frame. 
 *
 *    INPUT            : pBuf      - Frame to be verified
 *                       u4IfIndex - Index of the port on which the frame was 
 *                                   received
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_TRUE / OSIX_FALSE 
 *
 ****************************************************************************/
INT4
LldpApiIsLldpCtrlFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    return OSIX_FALSE;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfCreate
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the creation of physical port
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyIfCreate (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfMap
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the mapping of physical port
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiNotifyIfMap (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfDelete
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the deletion of physical port
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyIfDelete (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfUnMap
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the unmapping of physical port
 *
 *    INPUT            : u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiNotifyIfUnMap (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIfOperStatusChg
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message
 *                       queue to indicate the oper status changeof physical 
 *                       port
 *
 *    INPUT            : u4IfIndex    - Interface index
 *                       u1OperStatus - Operational link status of the port
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyIfOperStatusChg (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1OperStatus);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiEnqIncomingFrame
 *
 *    DESCRIPTION      : This function receives the incoming LLDP frame from
 *                       the CFA Module and enqueues it to LLDP Control task.
 *
 *    INPUT            : pBuf      - Pointer to the received buffer
 *                       u4IfIndex - Interface index
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiEnqIncomingFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyPortVlanId 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in Port Vlan Id
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       u2VlanId  - Port VLAN Id
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiNotifyPortVlanId (UINT4 u4IfIndex, UINT2 u2VlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2VlanId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyProtoVlanStatus 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in the status of Protocol
 *                       based Vlan Classification on the port. If the status 
 *                       is changed globally then this function will be called
 *                       with u4IfIndex as zero.
 *
 *    INPUT            : u4IfIndex - Interface index 
 *                       u1ProtVlanStatus  - Status of protocol based vlan 
 *                       classification (Enabled / Disabled) 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyProtoVlanStatus (UINT4 u4IfIndex, UINT1 u1ProtVlanStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ProtVlanStatus);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyProtoVlanId 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in the Protocol Vlan Id
 *                       mapped to the port.
 *
 *    INPUT            : u4IfIndex    - Interface index 
 *                       u2VlanId     - Protocol Vlan Id which need to be added
 *                                      or deleted to/from LLDP data structure.
 *                       u1ActionFlag - Operation to be performed (Add / Delete 
 *                                      / Updation)
 *                       u2OldVlanId  - Non-zero only if u1ActionFlag = Updation
 *                                      i.e if for the same protocol
 *                                      group the protocol vlan id is replaced 
 *                                      with another protocol vlan id. In this 
 *                                      case the node containing the u2OldVlanId 
 *                                      will be deleted and a new node with 
 *                                      u2VlanId will be added.  
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiNotifyProtoVlanId (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 u1ActionFlag,
                          UINT2 u2OldVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u1ActionFlag);
    UNUSED_PARAM (u2OldVlanId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyPortAggCapability 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in the Port Aggregation 
 *                       Capability.
 *
 *    INPUT            : u4IfIndex    - Interface index 
 *                       u1AggCap     - Aggregation Capability 
 *                                     (TRUE indicates capable of being 
 *                                     aggregated and FALSE indicates  not
 *                                     capable of being aggregated) 
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyPortAggCapability (UINT4 u4IfIndex, UINT1 u1AggCap)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1AggCap);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyResetAggCapability 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue to notify  the change in the Port Aggregation 
 *                       Capability when the port-channel is deleted.
 *
 *    INPUT            : AggConfPorts - List of ports which are not capable of 
 *                                      aggregation
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyResetAggCapability (tPortList AggConfPorts)
{
    UNUSED_PARAM (AggConfPorts);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyAggStatus 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's queue 
 *                       whenever there is a change in the active port-channel
 *                       membership of a port.
 *
 *    INPUT            : u4IfIndex   - Index of the port which has become an
 *                                     active member of a port-channel or
 *                                     removed from the active member list.
 *                       u1AggStatus - Aggregation Status 
 *                                     (LLDP_AGG_ACTIVE_PORT /
 *                                     LLDP_AGG_NOT_ACTIVE_PORT)
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiNotifyAggStatus (UINT4 u4IfIndex, UINT1 u1AggStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1AggStatus);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifySysName
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's queue
 *                       to notify the change in the System Name. 
 *
 *    INPUT            : pu1SysName  - System Name string is passed through 
 *                                     this pointer.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 *    CALLED BY        : nmhSetSysName
 *
 *    CALLS            : LldpQueEnqAppMsg
 *
 ****************************************************************************/
INT4
LldpApiNotifySysName (UINT1 *pu1SysName)
{
    UNUSED_PARAM (pu1SysName);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIpv4IfStatusChange
 *
 *    DESCRIPTION      : This Callback function is registered with ipv4 
 *                       module(using NetIpv4RegisterHigherLayerProtocol() API)
 *                       to get the notification about the If status change.
 *                       Whenever status of any ip interface changed this
 *                       callback routine is invoked. 
 *                       After getting this 
 *                       notification LLDP module poll the if tables in in 
 *                       Ipv4 module to get the IP addresses of all active 
 *                       interfaces, and update its local Management Address 
 *                       database. These Ip addresses are used by LLDP module 
 *                       to construct the Management Address TLV's.
 *
 *    INPUT            : pNetIpIfInfo - If Config Record Structure
 *                       u4Mask - Unused Param
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 *    CALLED BY        : nmhSetSysName
 *
 *    CALLS            : LldpQueEnqAppMsg
 *
 ****************************************************************************/
PUBLIC VOID
LldpApiNotifyIpv4IfStatusChange (tNetIpv4IfInfo * pNetIpIfInfo, UINT4 u4Mask)
{
    UNUSED_PARAM (pNetIpIfInfo);
    UNUSED_PARAM (u4Mask);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiNotifyIpv6IfStatusChange
 *
 *    DESCRIPTION      : This Callback function is registered with ipv6 
 *                       module(using NetIpv6RegisterHigherLayerProtocol() API)
 *                       to get the notification about the If status change.
 *                       Whenever status of any ip interface changed this
 *                       callback routine is invoked. 
 *                       After getting this notification.
 *                       notification LLDP module poll the if tables in 
 *                       Ipv6 module to get the IP addresses of all active 
 *                       interfaces, and update its local Management Address 
 *                       database. These Ip addresses are used by LLDP module 
 *                       to construct the Management Address TLV's.
 *                       Registered with NETIPV6_ADDRESS_CHANGE.
 *
 *    INPUT            : pNetIpIfInfo - If Config Record Structure
 *                       u4Mask - Unused Param
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 *    CALLED BY        : nmhSetSysName
 *
 *    CALLS            : LldpQueEnqAppMsg
 *
 ****************************************************************************/
PUBLIC VOID
LldpApiNotifyIpv6IfStatusChange (tNetIpv6HliParams * pNetIpv6HlParams)
{
    UNUSED_PARAM (pNetIpv6HlParams);
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbNotifyVlnInfoChgForPort 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue.
 *                       This function will be called only when a port is
 *                       removed from the vlan membership and when the 
 *                       transmission of vlan name tlv for that vlan is enabled 
 *                       on this port.
 *
 *    INPUT            : u4IfIndex   - Port number
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpVlndbNotifyVlnInfoChgForPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpVlndbNotifyVlanInfoChg 
 *
 *    DESCRIPTION      : This function posts a message to LLDP task's message 
 *                       queue.
 *                       This function will be called in the following cases 
 *                       only when the transmission of vlan name tlv is enabled
 *                       on the ports in the given portlist.
 *                       1. Whenever the vlan name is updated.
 *                       2. When vlan is deleted.
 *                       3. When vlan member ports changes.
 *
 *    INPUT            : VlanPortList - List of ports mapped to the vlan
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 *
 ****************************************************************************/
PUBLIC INT4
LldpVlndbNotifyVlanInfoChg (tPortList VlanPortList)
{
    UNUSED_PARAM (VlanPortList);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiModuleStart
 *
 *    DESCRIPTION      : Other modules invoke this API to start
 *                       LLDP module. In current implementation, Redundancy
 *                       Manager invokes this API start LLDP module in
 *                       cable pull-out scenario.
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiModuleStart (VOID)
{
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiModuleShutDown
 *
 *    DESCRIPTION      : Other modules invoke this API to shutdown
 *                       LLDP module. In current implementation, Redundancy
 *                       Manager invokes this API shutdown LLDP module in
 *                       cable pull-out scenario.
 *
 *    INPUT            : NONE
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
PUBLIC VOID
LldpApiModuleShutDown (VOID)
{
    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiHandleApplPortRequest
 *
 *    DESCRIPTION      : L2IWF uses this API to register the application data
 *                       with LLDP.
 *
 *    INPUT            : u4IfIndex - Port on which the application is to 
 *                                   be registered
 *                       pLldpAppPortMsg - Message to be stored in LLDP
 *                       u1ApplPortRequest
 *                            L2IWF_LLDP_APPL_PORT_REGISTER - To register
 *                            L2IWF_LLDP_APPL_PORT_UPDATE - To update 
 *                            L2IWF_LLDP_APPL_PORT_DEREGISTER - To de-register
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiHandleApplPortRequest (UINT4 u4IfIndex,
                              tLldpAppPortMsg * pLldpAppPortMsg,
                              UINT1 u1ApplPortRequest)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pLldpAppPortMsg);
    UNUSED_PARAM (u1ApplPortRequest);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiGetInstanceId
 *
 *    DESCRIPTION      : Function that returns the Agent Id given a destination
 *                       Mac address
 *
 *    INPUT            : pu1Mac - Destination mac address index to be used in
 *                       LLDP PDU that transmits the appilication TLV.
 *    OUTPUT           : pu4InstanceId - Destination mac address index 
 *                       corresponding to the mac address.
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/
PUBLIC INT4
LldpApiGetInstanceId (UINT1 *pu1MacAddr, UINT4 *pu4InstanceId)
{

    UNUSED_PARAM (pu1MacAddr);
    UNUSED_PARAM (pu4InstanceId);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : LldpApiSetDstMac
 *
 *    DESCRIPTION      : API to set the destination mac address
 *
 *    INPUT            : pu1MacAddr - Destination mac address index to be 
 *                       set for the LLDP agent
 *                       i4IfIndex - Interface Index on which the LLDP agent
 *                       should be set
 *                       u1RowStatus - specifies if the agent is created/deleted
 *
 *    OUTPUT           : sets the LLDP agent with i4IfIndex and pu1MacAddr
 *
 *    RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 ****************************************************************************/

PUBLIC INT4
LldpApiSetDstMac (INT4 i4UapIfIndex, UINT1 *pu1MacAddr, UINT1 u1RowStatus)
{

    UNUSED_PARAM (i4UapIfIndex);
    UNUSED_PARAM (pu1MacAddr);
    UNUSED_PARAM (u1RowStatus);

    return OSIX_SUCCESS;
}
#endif

#ifndef DIFFSRV_WANTED
/*****************************************************************************/
/* Function Name      : DsGetStartedStatus                                   */
/*                                                                           */
/* Description        : This function gets the system status whether its     */
/*                      started or shutdown.                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_TRUE     - On success                             */
/*                      DS_FALSE    - On Failure                             */
/*****************************************************************************/
INT4
DsGetStartedStatus ()
{
    return DS_FALSE;
}

/*****************************************************************************/
/* Function Name      : DsCheckiffservPort                                   */
/*                                                                           */
/* Description        : This function checks whether the given port is a     */
/*                      Diffsrv port                                         */
/*                                                                           */
/* Input(s)           : i4TrunkPort - Trunk Port                             */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : DS_SUCCESS     - On success                          */
/*                      DS_FAILURE - On Failure                              */
/*****************************************************************************/
INT4
DsCheckDiffservPort (INT4 i4TrunkPort)
{
    UNUSED_PARAM (i4TrunkPort);
    return DS_FAILURE;
}
#endif

#ifndef RADIUS_WANTED

/*-----------------------------------------------------------------------------
Procedure    : radiusAuthentication()
Description    : This module frames packet for authentication request, and
              transmits the packet to the server. Then the user request is
Procedure    : radiusAuthentication()
Description    : This module frames packet for authentication request, and
              transmits the packet to the server. Then the user request is
              entered in the User queue. It first validates the radius
              input given to it. If any error is found the packet framing
              is abandoned. The user password is hidden using Message
              digest algorithm. If the protocol used for authentication of
              the user is CHAP, then the password is not hidden. The CHAP
              response and the CHAP Identifier are taken as the Password.
              Then all the attributes are filled. After all attributes
              filled, the packet is transmitted.
Input(s)    : p_RadiusInputAuth
              p_RadiusServerAuth
OutPut(s)    : None
Returns        : The Identifier of the packet if packet is transmitted
              successfully
              -1, if there is failure in transmitting the packet.
Called By    : The User
Calling        : radiusValidateInputAuth()
              radiusFramePrePacketAuth()
              radiusHidePassword()
              radiusFillAttributesAuth()
              radiusTransmitPacket()
              radiusEnterUserQAuth()
Author        : G.Vedavinayagam
Date        : 11/11/97
odificaions    :
-----------------------------------------------------------------------------*/
INT4
radiusAuthentication (tRADIUS_INPUT_AUTH * p_RadiusInputAuth,
                      tRADIUS_SERVER * p_RadiusServerAuth,
                      VOID (*CallBackfPtr) (VOID *),
                      UINT4 ifIndex, UINT1 u1ReqId)
{

    UNUSED_PARAM (p_RadiusServerAuth);
    UNUSED_PARAM (p_RadiusInputAuth);
    UNUSED_PARAM (ifIndex);
    UNUSED_PARAM (u1ReqId);
    UNUSED_PARAM (CallBackfPtr);
    return NOT_OK;
}

/*****************************************************************************/
/* Function Name      : RadApiHandleRadAuthCB                                */
/*                                                                           */
/* Description        : This function is invoked for wss pnac module         */
/*                      Interaction                                          */
/*                                                                           */
/* Input(s)           : pRadiusInputAuth ,pRadiusServerAuth,u4IfIndex,       */
/*                      u1ReqId                                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : Call Back Function Pointer                           */
/*****************************************************************************/

tRadHandleRadAuthCB
RadApiHandleRadAuthCB (tRADIUS_INPUT_AUTH * pRadiusInputAuth,
                       tRADIUS_SERVER * pRadiusServerAuth,
                       UINT4 u4IfIndex, UINT1 u1ReqId, INT4 *pi4Ret)
{
    tRadHandleRadAuthCB pHandleRadAuthCB = NULL;
    UNUSED_PARAM (pRadiusServerAuth);
    UNUSED_PARAM (pRadiusInputAuth);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ReqId);
    UNUSED_PARAM (pi4Ret);
    return pHandleRadAuthCB;
}

/*****************************************************************************/
/* Function Name      : RadApiRegisterHandleRadAuthResCB                     */
/*                                                                           */
/* Description        : This function is invoked to register the wss-pnac    */
/*                      Interaction function                                 */
/*                                                                           */
/* Input(s)           : RadAuthCB, RadAuthCB                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
RadApiRegisterHandleRadAuthResCB (tRadHandleRadAuthCB RadAuthCB,
                                  tRadHandleRadResCB RadResCB)
{
    UNUSED_PARAM (RadAuthCB);
    UNUSED_PARAM (RadResCB);
    return;
}

#endif

#ifndef GARP_WANTED

/***************************************************************/
/*  Function Name   : GarpCreatePort                           */
/*  Description     : This function is for adding a port.      */
/*  Input(s)        : u2Port - Port to be added.               */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : gaGarpGlobalPortTable          */
/*  Global variables Modified : gaGarpGlobalPortTable          */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful addition of   */
/*                    port. GARP_FAILURE on failure            */
/***************************************************************/
INT4
GarpCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalPortId);
    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GarpDeletePort                           */
/*  Description     : This function is for deleting a port.    */
/*  Input(s)        : u2Port - Port to be deleted              */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_SUCCESS on successful deletion  of  */
/*                    port. GARP_FAILURE on failure            */
/***************************************************************/
INT4
GarpDeletePort (UINT2 u2Port)
{
    UNUSED_PARAM (u2Port);
    return GARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : GarpLock                                             */
/*                                                                           */
/* Description        : This function is used to take the GARP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS or GARP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
GarpLock (VOID)
{

    return GARP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : GarpUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the GARP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS or GARP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
GarpUnLock (VOID)
{
    return GARP_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : GarpShowRunningConfigInterfaceDetails              */
/*                                                                           */
/*     DESCRIPTION      : This function displays current garp configuration  */
/*                        for Interface                                      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - context id
 *                        i4LocalPort - Local port of a particular context   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

INT4
GarpShowRunningConfigInterfaceDetails (tCliHandle CliHandle, UINT4 u4ContextId,
                                       INT4 i4LocalPort)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4LocalPort);
    return CLI_SUCCESS;
}

/****************************************************************/
/*  Function Name   :GmrpPropagateDefGroupInfo                  */
/*  Description     :This function propagates the default Group */
/*                   info to all the ports during initialisation*/
/*  Input(s)        :u1Type - Whether it is VLAN_ALL_GROUPS or  */
/*                   VLAN_UNREG_GROUPS                          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpPropagateDefGroupInfo (UINT4 u4ContextId, UINT1 u1Type,
                           tVlanId VlanId,
                           tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/****************************************************************/
/*  Function Name   :GmrpSetDefGroupForbiddenPorts              */
/*  Description     :depending on u1action we will add a static */
/*                   entry or delete it                         */
/*  Input(s)        :pMacAddr - Pointer to Mac address          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpSetDefGroupForbiddenPorts (UINT4 u4ContextId, UINT1 u1Type,
                               tVlanId VlanId,
                               tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/****************************************************************/
/*  Function Name   :GmrpPropagateMcastInfo                     */
/*  Description     :depending on u1action we will add a static */
/*                   Multicast entry or delete an entry         */
/*  Input(s)        :pMacAddr - Pointer to mac address          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpPropagateMcastInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                        tVlanId VlanId,
                        tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/****************************************************************/
/*  Function Name   :GmrpSetMcastForbiddenPorts                 */
/*  Description     :This function set the forbidden ports for a*/
/*                   given multicast entry                      */
/*  Input(s)        :pMacAddr - Pointer to multicast mac address*/
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
GmrpSetMcastForbiddenPorts (UINT4 u4ContextId, tMacAddr MacAddr,
                            tVlanId VlanId,
                            tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/****************************************************************************/
/*  Function Name   :GvrpPropagateVlanInfo                                  */
/*  Description     :depending on u1action we will add a static             */
/*                   entry or delete it                                     */
/*  Input(s)        :u1AttrType -The attribute type received                */
/*                   AddPortList - Ports to be added                        */
/*                   DelPortList - ports to be deleted                      */
/*                   u1Action - depending on u1Action we will add a static  */
/*                              vlan  entry or delete it                    */
/*  Output(s)       :  None                                                 */
/*  Global Variables Referred : None                                        */
/*  Global variables Modified : None                                        */
/*  Returns         : None                                                  */
/****************************************************************************/
void
GvrpPropagateVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                       tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/**************************************************************************/
/*  Function Name    : GarpGipUpdateGipPorts ()                           */
/*  Description      : This function transmits Join messages on the given */
/*                     port, for all the Attributes registered for the    */
/*                     given GipId.                                       */
/*                     This function is called by VLAN whenever static    */
/*                     member ports are added.                            */
/*                     Updation of Gip Port list affects only GMRP.       */
/*  Input(s)         : AddPortList - Ports that is added to the           */
/*                              given GIP context.                        */
/*                     u2Gip  - Id of the GIP context for which           */
/*                              new port is added.                        */
/*  Output(s)        : None                                               */
/*  Global Variables                                                      */
/*  Referred         : None                                               */
/*  Global variables                                                      */
/*  Modified         : None                                               */
/*  Exceptions or OS                                                      */
/*  Error handling   : None                                               */
/*  Use of Recursion : None                                               */
/*  Returns          : None                                               */
/**************************************************************************/
VOID
GarpGipUpdateGipPorts (UINT4 u4ContextId, tPortList AddPortList,
                       tPortList DelPortList, UINT2 u2GipId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    UNUSED_PARAM (u2GipId);
    return;
}

/****************************************************************************/
/*  Function Name   :GvrpSetVlanForbiddenPorts                              */
/*  Description     :This function sets forbidden ports for a specific      */
/*                    VlanId                                                */
/*  Input(s)        :VlanId - VlanId for which Forbidden ports to be updated*/
/*                   AddPortList - Ports to be added                        */
/*                   DelPortList - ports to be deleted                      */
/*  Output(s)       :  None                                                 */
/*  Global Variables Referred : None                                        */
/*  Global variables Modified : None                                        */
/*  Returns         : None                                                  */
/****************************************************************************/
void
GvrpSetVlanForbiddenPorts (UINT4 u4ContextId, tVlanId VlanId,
                           tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/*****************************************************************************/
/* Function Name      : GarpFillBulkMessage                                  */
/*                                                                           */
/* Description        : This function is used to fill the Vlan / Mcast /     */
/*                      Def Group Info in the Message. This funtion is called*/
/*                      whenever a Gvrp and Gmrp is enabled. This function   */
/*                      also post the message if the max no of message is    */
/*                      filled.                                              */
/*                                                                           */
/* Input(s)           : ppGarpQMsg - Double Pointer to store GarpQMsg        */
/*                      u1MsgType  - Type of Message - Vlan / Mcast / Def Grp*/
/*                      VlanId     - VlanId                                  */
/*                      Ports      - Added Portlist                          */
/*                      pu1MacAddr - MacAddres                               */
/*                                                                           */
/* Output(s)          : ppGarpQMsg - pointer to Allocated Message            */
/*                      pi4Count   - No of Message Filled                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : GARP_SUCCESS or GARP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
GarpFillBulkMessage (UINT4 u4ContextId, tGarpQMsg ** ppGarpQMsg,
                     UINT1 u1MsgType, tVlanId VlanId, tPortList Ports,
                     UINT1 *pu1MacAddr)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (ppGarpQMsg);
    UNUSED_PARAM (u1MsgType);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (Ports);
    UNUSED_PARAM (pu1MacAddr);

    return GARP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : GarpPostBulkCfgMessage                           */
/*                                                                           */
/*    Description         : Posts the message to Garp Config Q.              */
/*                                                                           */
/*    Input(s)            : pGarpQMsg -  Pointer to GarpQMsg                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : GARP_SUCCESS / GARP_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
GarpPostBulkCfgMessage (tGarpQMsg * pGarpQMsg)
{
    UNUSED_PARAM (pGarpQMsg);
    return GARP_SUCCESS;
}

/***************************************************************/
/*  Function Name   : GvrpIsGvrpEnabledInContext               */
/*  Description     : Returns the GVRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Global Variables Referred : gpGarpContextInfo->u1GvrpStatus*/
/*  Global variables Modified :                                */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GVRP_ENABLED /GVRP_DISABLED              */
/***************************************************************/
INT4
GvrpIsGvrpEnabledInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return GVRP_DISABLED;
}

/***************************************************************/
/*  Function Name   : GmrpIsGmrpEnabledInContext               */
/*  Description     : Returns the GMRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Global Variables Referred : gpGarpContextInfo->u1GmrpStatus                  */
/*  Global variables Modified :                                */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GMRP_ENABLED /GMRP_DISABLED              */
/***************************************************************/
INT4
GmrpIsGmrpEnabledInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return GMRP_DISABLED;
}

/***************************************************************/
/*  Function Name   : GarpMiUpdateInstVlanMap                  */
/*                                                             */
/*  Description     : This function updates GARP of Instance   */
/*                    to VLAN map Updation                     */
/*                                                             */
/*  Input(s)        : u1Action - Update / Post Message         */
/*                    u4ContextId - Context Identifier          */
/*                    u2NewMapId - New Map Identifier          */
/*                    u2OldMapId - Old Map Identifier          */
/*                                                             */
/*                    VlanId - VLAN Identifier of mapped VLAN  */
/*                    mapped to the instance                   */
/*                                                             */
/*                    u2MapEvent - Indicates whether it is a   */
/*                    single VLAN to MSTI or list of VLAN      */
/*                    map/umap                                 */
/*                                                             */
/*                    pu1Result - output to indicate message   */
/*                    post or updation                         */
/*                                                             */
/*  Output(s)       : Buffer to be posted to GARP              */
/*                                                             */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
VOID
GarpMiUpdateInstVlanMap (UINT4 u4ContextId, UINT1 u1Action, UINT2 u2NewMapId,
                         UINT2 u2OldMapId, tVlanId VlanId,
                         UINT2 u2MapEvent, UINT1 *pu1Result)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Action);
    UNUSED_PARAM (u2NewMapId);
    UNUSED_PARAM (u2OldMapId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2MapEvent);
    UNUSED_PARAM (pu1Result);
    return;
}

/***************************************************************/
/*  Function Name   : GarpIsGarpEnabledInContext               */
/*  Description     : Returns the GARP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Global Variables Referred : gau1GarpShutDownStatus         */
/*  Global variables Modified :                                */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : GARP_ENABLED /GARP_DISABLED              */
/***************************************************************/
INT4
GarpIsGarpEnabledInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return GARP_TRUE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureGarpShutdownStatus
 Input       :  The Indices

                The Object 
                retValDot1qFutureGarpShutdownStatus
 Output      :  The Get Low Lev Routine Take the Indices & store the Value
                requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureGarpShutdownStatus (INT4
                                     *pi4RetValDot1qFutureGarpShutdownStatus)
{
    UNUSED_PARAM (pi4RetValDot1qFutureGarpShutdownStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1qFutureGarpShutdownStatus
 Input       :  The Indices

                The Object
                setValDot1qFutureGarpShutdownStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qFutureGarpShutdownStatus (INT4 i4SetValDot1qFutureGarpShutdownStatus)
{
    UNUSED_PARAM (i4SetValDot1qFutureGarpShutdownStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qFutureGarpShutdownStatus
 Input       :  The Indices

                The Object
                testValDot1qFutureGarpShutdownStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qFutureGarpShutdownStatus (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValDot1qFutureGarpShutdownStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValDot1qFutureGarpShutdownStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qGvrpStatus
 Input       :  The Indices

                testValDot1qGvrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qGvrpStatus (UINT4 *pu4ErrorCode, INT4 i4TestValDot1qGvrpStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValDot1qGvrpStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1qGvrpStatus
 Input       :  The Indices

                The Object
                setValDot1qGvrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qGvrpStatus (INT4 i4SetValDot1qGvrpStatus)
{
    UNUSED_PARAM (i4SetValDot1qGvrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qGvrpStatus
 Input       :  The Indices
 
                The Object
                retValDot1qGvrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qGvrpStatus (INT4 *pi4RetValDot1qGvrpStatus)
{
    UNUSED_PARAM (pi4RetValDot1qGvrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qPortGvrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object
                testValDot1qPortGvrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qPortGvrpStatus (UINT4 *pu4ErrorCode,
                              INT4 i4Dot1dBasePort,
                              INT4 i4TestValDot1qPortGvrpStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4TestValDot1qPortGvrpStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1qPortGvrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object
                setValDot1qPortGvrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qPortGvrpStatus (INT4 i4Dot1dBasePort,
                           INT4 i4SetValDot1qPortGvrpStatus)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4SetValDot1qPortGvrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qPortGvrpFailedRegistrations
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retValDot1qPortGvrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPortGvrpFailedRegistrations (INT4 i4Dot1dBasePort,
                                        UINT4 *pu4GvrpNumFailedReg)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pu4GvrpNumFailedReg);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qPortGvrpLastPduOrigin
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retValDot1qPortGvrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPortGvrpLastPduOrigin (INT4 i4Dot1dBasePort,
                                  tMacAddr * pLastPduOrigin)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pLastPduOrigin);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1qPortRestrictedVlanRegistration
 Input       :  The Indices
                Dot1dBasePort

                The Object
                testValDot1qPortRestrictedVlanRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1qPortRestrictedVlanRegistration (UINT4 *pu4ErrorCode,
                                              INT4 i4Dot1dBasePort,
                                              INT4
                                              i4TestValDot1qPortRestrictedVlanRegistration)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4TestValDot1qPortRestrictedVlanRegistration);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function   :  nmhSetDot1qPortRestrictedVlanRegistration
 Input      :  The Indices
               Dot1dBasePort

               The Object
               setValDot1qPortRestrictedVlanRegistration
 Output     :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly.
 Returns    :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1qPortRestrictedVlanRegistration (INT4 i4Dot1dBasePort,
                                           INT4
                                           i4SetValDot1qPortRestrictedVlanRegistration)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4SetValDot1qPortRestrictedVlanRegistration);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qPortRestrictedVlanRegistration
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retValDot1qPortRestrictedVlanRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPortRestrictedVlanRegistration (INT4 i4Dot1dBasePort,
                                           INT4
                                           *pi4RetValDot1qPortRestrictedVlanRegistration)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pi4RetValDot1qPortRestrictedVlanRegistration);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dGmrpStatus
 Input       :  The Indices

                The Object
                testValDot1dGmrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dGmrpStatus (UINT4 *pu4ErrorCode, INT4 i4TestValDot1dGmrpStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValDot1dGmrpStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1dGmrpStatus
 Input       :  The Indices

                The Object
                setValDot1dGmrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dGmrpStatus (INT4 i4SetValDot1dGmrpStatus)
{
    UNUSED_PARAM (i4SetValDot1dGmrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dGmrpStatus
 Input       :  The Indices
 
                The Object
                retValDot1dGmrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dGmrpStatus (INT4 *pi4RetValDot1dGmrpStatus)
{
    UNUSED_PARAM (pi4RetValDot1dGmrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortGarpJoinTime
 Input       :  The Indices
                Dot1dBasePort

                The Object
                testValDot1dPortGarpJoinTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortGarpJoinTime (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                                INT4 i4TestValDot1dPortGarpJoinTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4TestValDot1dPortGarpJoinTime);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortGarpJoinTime
 Input       :  The Indices
                Dot1dBasePort

                The Object
                setValDot1dPortGarpJoinTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortGarpJoinTime (INT4 i4Dot1dBasePort,
                             INT4 i4SetValDot1dPortGarpJoinTime)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4SetValDot1dPortGarpJoinTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dPortGarpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dPortGarpTable (INT4 i4Dot1dBasePort)
{
    UNUSED_PARAM (i4Dot1dBasePort);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGarpJoinTime
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retValDot1dPortGarpJoinTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGarpJoinTime (INT4 i4Dot1dBasePort,
                             INT4 *pi4RetValDot1dPortGarpJoinTime)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pi4RetValDot1dPortGarpJoinTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortGarpLeaveTime
 Input       :  The Indices
                Dot1dBasePort

                The Object
                testValDot1dPortGarpLeaveTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortGarpLeaveTime (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                                 INT4 i4TestValDot1dPortGarpLeaveTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4TestValDot1dPortGarpLeaveTime);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortGarpLeaveTime
 Input       :  The Indices
                Dot1dBasePort

                The Object
                setValDot1dPortGarpLeaveTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortGarpLeaveTime (INT4 i4Dot1dBasePort,
                              INT4 i4SetValDot1dPortGarpLeaveTime)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4SetValDot1dPortGarpLeaveTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGarpLeaveTime
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retValDot1dPortGarpLeaveTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGarpLeaveTime (INT4 i4Dot1dBasePort,
                              INT4 *pi4RetValDot1dPortGarpLeaveTime)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pi4RetValDot1dPortGarpLeaveTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortGarpLeaveAllTime
 Input       :  The Indices
                Dot1dBasePort

                The Object
                testValDot1dPortGarpLeaveAllTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortGarpLeaveAllTime (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                                    INT4 i4TestValDot1dPortGarpLeaveAllTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4TestValDot1dPortGarpLeaveAllTime);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortGarpLeaveAllTime
 Input       :  The Indices
                Dot1dBasePort

                The Object
                setValDot1dPortGarpLeaveAllTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortGarpLeaveAllTime (INT4 i4Dot1dBasePort,
                                 INT4 i4SetValDot1dPortGarpLeaveAllTime)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4SetValDot1dPortGarpLeaveAllTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGarpLeaveAllTime
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retValDot1dPortGarpLeaveAllTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGarpLeaveAllTime (INT4 i4Dot1dBasePort,
                                 INT4 *pi4RetValDot1dPortGarpLeaveAllTime)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pi4RetValDot1dPortGarpLeaveAllTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortGmrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object
                testValDot1dPortGmrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortGmrpStatus (UINT4 *pu4ErrorCode, INT4 i4Dot1dBasePort,
                              INT4 i4TestValDot1dPortGmrpStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4TestValDot1dPortGmrpStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortGmrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object
                setValDot1dPortGmrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortGmrpStatus (INT4 i4Dot1dBasePort,
                           INT4 i4SetValDot1dPortGmrpStatus)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4SetValDot1dPortGmrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dPortGmrpTable
 Input       :  The Indice
                Dot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceDot1dPortGmrpTable (INT4 i4Dot1dBasePort)
{
    UNUSED_PARAM (i4Dot1dBasePort);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGmrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retValDot1dPortGmrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGmrpStatus (INT4 i4Dot1dBasePort,
                           INT4 *pi4RetValDot1dPortGmrpStatus)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pi4RetValDot1dPortGmrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGmrpFailedRegistrations
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retvalDot1dPortGmrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGmrpFailedRegistrations (INT4 i4Dot1dBasePort,
                                        UINT4
                                        *pu4RetValDot1dPortGmrpFailedRegistrations)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pu4RetValDot1dPortGmrpFailedRegistrations);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortGmrpLastPduOrigin
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retValDot1dPortGmrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortGmrpLastPduOrigin (INT4 i4Dot1dBasePort,
                                  tMacAddr * pRetValDot1dPortGmrpLastPduOrigin)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pRetValDot1dPortGmrpLastPduOrigin);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dPortRestrictedGroupRegistration
 Input       :  The Indices
                Dot1dBasePort

                The Object
                testValDot1dPortRestrictedGroupRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns      :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dPortRestrictedGroupRegistration (UINT4 *pu4ErrorCode,
                                               INT4 i4Dot1dBasePort,
                                               INT4
                                               i4TestValDot1dPortRestrictedGroupRegistration)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4TestValDot1dPortRestrictedGroupRegistration);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1dPortRestrictedGroupRegistration
 Input       :  The Indices
                Dot1dBasePort

                The Object
                setValDot1dPortRestrictedGroupRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dPortRestrictedGroupRegistration (INT4 i4Dot1dBasePort,
                                            INT4
                                            i4SetValDot1dPortRestrictedGroupRegistration)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (i4SetValDot1dPortRestrictedGroupRegistration);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dPortRestrictedGroupRegistration
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retValDot1dPortRestrictedGroupRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dPortRestrictedGroupRegistration (INT4 i4Dot1dBasePort,
                                            INT4
                                            *pi4RetValDot1dPortRestrictedGroupRegistration)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pi4RetValDot1dPortRestrictedGroupRegistration);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dPortGmrpTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1dPortGmrpTable (INT4 i4Dot1dBasePort,
                                   INT4 *pi4NextDot1dBasePort)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pi4NextDot1dBasePort);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dPortGmrpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1dPortGmrpTable (INT4 *pi4Dot1dBasePort)
{
    UNUSED_PARAM (pi4Dot1dBasePort);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dPortGarpTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexDot1dPortGarpTable (INT4 i4Dot1dBasePort,
                                   INT4 *pi4NextDot1dBasePort)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pi4NextDot1dBasePort);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dPortGarpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexDot1dPortGarpTable (INT4 *pi4Dot1dBasePort)
{
    UNUSED_PARAM (pi4Dot1dBasePort);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureGvrpOperStatus
 Input       :  The Indices

                The Object
                retValDot1qFutureGvrpOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureGvrpOperStatus (INT4 *pi4RetValDot1qFutureGvrpOperStatus)
{
    UNUSED_PARAM (pi4RetValDot1qFutureGvrpOperStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1qFutureGmrpOperStatus
 Input       :  The Indices

                The Object
                retValDot1qFutureGmrpOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qFutureGmrpOperStatus (INT4 *pi4RetValDot1qFutureGmrpOperStatus)
{
    UNUSED_PARAM (pi4RetValDot1qFutureGmrpOperStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qGvrpStatus
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                retValFsDot1qGvrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qGvrpStatus (INT4 i4FsDot1qVlanContextId,
                         INT4 *pi4RetValFsDot1qGvrpStatus)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (pi4RetValFsDot1qGvrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qGvrpStatus
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                setValFsDot1qGvrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qGvrpStatus (INT4 i4FsDot1qVlanContextId,
                         INT4 i4SetValFsDot1qGvrpStatus)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (i4SetValFsDot1qGvrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qGvrpStatus
 Input       :  The Indices
                FsDot1qVlanContextId

                The Object 
                testValFsDot1qGvrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qGvrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1qVlanContextId,
                            INT4 i4TestValFsDot1qGvrpStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (i4TestValFsDot1qGvrpStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPortGvrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPortGvrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPortGvrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 *pi4RetValFsDot1qPortGvrpStatus)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    *pi4RetValFsDot1qPortGvrpStatus = OSIX_DISABLED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPortGvrpFailedRegistrations
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPortGvrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPortGvrpFailedRegistrations (INT4 i4FsDot1dBasePort,
                                          UINT4
                                          *pu4RetValFsDot1qPortGvrpFailedRegistrations)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pu4RetValFsDot1qPortGvrpFailedRegistrations);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPortGvrpLastPduOrigin
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPortGvrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPortGvrpLastPduOrigin (INT4 i4FsDot1dBasePort,
                                    tMacAddr *
                                    pRetValFsDot1qPortGvrpLastPduOrigin)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pRetValFsDot1qPortGvrpLastPduOrigin);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1qPortRestrictedVlanRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                retValFsDot1qPortRestrictedVlanRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1qPortRestrictedVlanRegistration (INT4 i4FsDot1dBasePort,
                                             INT4
                                             *pi4RetValFsDot1qPortRestrictedVlanRegistration)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pi4RetValFsDot1qPortRestrictedVlanRegistration);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qPortGvrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                setValFsDot1qPortGvrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qPortGvrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 i4SetValFsDot1qPortGvrpStatus)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4SetValFsDot1qPortGvrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1qPortRestrictedVlanRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                setValFsDot1qPortRestrictedVlanRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1qPortRestrictedVlanRegistration (INT4 i4FsDot1dBasePort,
                                             INT4
                                             i4SetValFsDot1qPortRestrictedVlanRegistration)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4SetValFsDot1qPortRestrictedVlanRegistration);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qPortGvrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                testValFsDot1qPortGvrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qPortGvrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                INT4 i4TestValFsDot1qPortGvrpStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4TestValFsDot1qPortGvrpStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1qPortRestrictedVlanRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object 
                testValFsDot1qPortRestrictedVlanRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1qPortRestrictedVlanRegistration (UINT4 *pu4ErrorCode,
                                                INT4 i4FsDot1dBasePort,
                                                INT4
                                                i4TestValFsDot1qPortRestrictedVlanRegistration)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4TestValFsDot1qPortRestrictedVlanRegistration);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dGmrpStatus
 Input       :  The Indices
                FsDot1dBridgeContextId

                The Object
                retValFsDot1dGmrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dGmrpStatus (INT4 i4FsDot1dBridgeContextId,
                         INT4 *pi4RetValFsDot1dGmrpStatus)
{
    UNUSED_PARAM (i4FsDot1dBridgeContextId);
    UNUSED_PARAM (pi4RetValFsDot1dGmrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dGmrpStatus
 Input       :  The Indices
                FsDot1dBridgeContextId

                The Object
                setValFsDot1dGmrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dGmrpStatus (INT4 i4FsDot1dBridgeContextId,
                         INT4 i4SetValFsDot1dGmrpStatus)
{
    UNUSED_PARAM (i4FsDot1dBridgeContextId);
    UNUSED_PARAM (i4SetValFsDot1dGmrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dGmrpStatus
 Input       :  The Indices
                FsDot1dBridgeContextId

                The Object
                testValFsDot1dGmrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dGmrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBridgeContextId,
                            INT4 i4TestValFsDot1dGmrpStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBridgeContextId);
    UNUSED_PARAM (i4TestValFsDot1dGmrpStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dPortGarpTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dPortGarpTable (INT4 i4FsDot1dBasePort,
                                     INT4 *pi4NextFsDot1dBasePort)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pi4NextFsDot1dBasePort);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dPortGarpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDot1dPortGarpTable (INT4 *pi4FsDot1dBasePort)
{
    UNUSED_PARAM (pi4FsDot1dBasePort);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dPortGarpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsDot1dPortGarpTable (INT4 i4FsDot1dBasePort)
{
    UNUSED_PARAM (i4FsDot1dBasePort);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGarpJoinTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGarpJoinTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGarpJoinTime (INT4 i4FsDot1dBasePort,
                               INT4 *pi4RetValFsDot1dPortGarpJoinTime)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pi4RetValFsDot1dPortGarpJoinTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGarpLeaveTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGarpLeaveTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGarpLeaveTime (INT4 i4FsDot1dBasePort,
                                INT4 *pi4RetValFsDot1dPortGarpLeaveTime)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pi4RetValFsDot1dPortGarpLeaveTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGarpLeaveAllTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGarpLeaveAllTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGarpLeaveAllTime (INT4 i4FsDot1dBasePort,
                                   INT4 *pi4RetValFsDot1dPortGarpLeaveAllTime)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pi4RetValFsDot1dPortGarpLeaveAllTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortGarpJoinTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortGarpJoinTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortGarpJoinTime (INT4 i4FsDot1dBasePort,
                               INT4 i4SetValFsDot1dPortGarpJoinTime)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4SetValFsDot1dPortGarpJoinTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortGarpLeaveTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortGarpLeaveTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortGarpLeaveTime (INT4 i4FsDot1dBasePort,
                                INT4 i4SetValFsDot1dPortGarpLeaveTime)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4SetValFsDot1dPortGarpLeaveTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortGarpLeaveAllTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortGarpLeaveAllTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortGarpLeaveAllTime (INT4 i4FsDot1dBasePort,
                                   INT4 i4SetValFsDot1dPortGarpLeaveAllTime)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4SetValFsDot1dPortGarpLeaveAllTime);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortGarpJoinTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortGarpJoinTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortGarpJoinTime (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                  INT4 i4TestValFsDot1dPortGarpJoinTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4TestValFsDot1dPortGarpJoinTime);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortGarpLeaveTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortGarpLeaveTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortGarpLeaveTime (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                   INT4 i4TestValFsDot1dPortGarpLeaveTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4TestValFsDot1dPortGarpLeaveTime);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortGarpLeaveAllTime
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortGarpLeaveAllTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortGarpLeaveAllTime (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDot1dBasePort,
                                      INT4 i4TestValFsDot1dPortGarpLeaveAllTime)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4TestValFsDot1dPortGarpLeaveAllTime);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsDot1dPortGmrpTable
 Input       :  The Indices
                FsDot1dBasePort
                nextFsDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsDot1dPortGmrpTable (INT4 i4FsDot1dBasePort,
                                     INT4 *pi4NextFsDot1dBasePort)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pi4NextFsDot1dBasePort);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsDot1dPortGmrpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsDot1dPortGmrpTable (INT4 *pi4FsDot1dBasePort)
{
    UNUSED_PARAM (pi4FsDot1dBasePort);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsDot1dPortGmrpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsDot1dPortGmrpTable (INT4 i4FsDot1dBasePort)
{
    UNUSED_PARAM (i4FsDot1dBasePort);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGmrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGmrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGmrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 *pi4RetValFsDot1dPortGmrpStatus)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pi4RetValFsDot1dPortGmrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGmrpFailedRegistrations
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGmrpFailedRegistrations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGmrpFailedRegistrations (INT4 i4FsDot1dBasePort,
                                          UINT4
                                          *pu4RetValFsDot1dPortGmrpFailedRegistrations)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pu4RetValFsDot1dPortGmrpFailedRegistrations);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortGmrpLastPduOrigin
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortGmrpLastPduOrigin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortGmrpLastPduOrigin (INT4 i4FsDot1dBasePort,
                                    tMacAddr *
                                    pRetValFsDot1dPortGmrpLastPduOrigin)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pRetValFsDot1dPortGmrpLastPduOrigin);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsDot1dPortRestrictedGroupRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                retValFsDot1dPortRestrictedGroupRegistration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDot1dPortRestrictedGroupRegistration (INT4 i4FsDot1dBasePort,
                                              INT4
                                              *pi4RetValFsDot1dPortRestrictedGroupRegistration)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (pi4RetValFsDot1dPortRestrictedGroupRegistration);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortGmrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortGmrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortGmrpStatus (INT4 i4FsDot1dBasePort,
                             INT4 i4SetValFsDot1dPortGmrpStatus)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4SetValFsDot1dPortGmrpStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDot1dPortRestrictedGroupRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                setValFsDot1dPortRestrictedGroupRegistration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDot1dPortRestrictedGroupRegistration (INT4 i4FsDot1dBasePort,
                                              INT4
                                              i4SetValFsDot1dPortRestrictedGroupRegistration)
{
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4SetValFsDot1dPortRestrictedGroupRegistration);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortGmrpStatus
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortGmrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortGmrpStatus (UINT4 *pu4ErrorCode, INT4 i4FsDot1dBasePort,
                                INT4 i4TestValFsDot1dPortGmrpStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4TestValFsDot1dPortGmrpStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsDot1dPortRestrictedGroupRegistration
 Input       :  The Indices
                FsDot1dBasePort

                The Object
                testValFsDot1dPortRestrictedGroupRegistration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDot1dPortRestrictedGroupRegistration (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsDot1dBasePort,
                                                 INT4
                                                 i4TestValFsDot1dPortRestrictedGroupRegistration)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1dBasePort);
    UNUSED_PARAM (i4TestValFsDot1dPortRestrictedGroupRegistration);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureGarpShutdownStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureGarpShutdownStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureGarpShutdownStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                         INT4
                                         *pi4RetValFsMIDot1qFutureGarpShutdownStatus)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    UNUSED_PARAM (pi4RetValFsMIDot1qFutureGarpShutdownStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureGvrpOperStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureGvrpOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureGvrpOperStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                     INT4
                                     *pi4RetValFsMIDot1qFutureGvrpOperStatus)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    UNUSED_PARAM (pi4RetValFsMIDot1qFutureGvrpOperStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIDot1qFutureGmrpOperStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                retValFsMIDot1qFutureGmrpOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIDot1qFutureGmrpOperStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                     INT4
                                     *pi4RetValFsMIDot1qFutureGmrpOperStatus)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    UNUSED_PARAM (pi4RetValFsMIDot1qFutureGmrpOperStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIDot1qFutureGarpShutdownStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                setValFsMIDot1qFutureGarpShutdownStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIDot1qFutureGarpShutdownStatus (INT4 i4FsMIDot1qFutureVlanContextId,
                                         INT4
                                         i4SetValFsMIDot1qFutureGarpShutdownStatus)
{
    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    UNUSED_PARAM (i4SetValFsMIDot1qFutureGarpShutdownStatus);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIDot1qFutureGarpShutdownStatus
 Input       :  The Indices
                FsMIDot1qFutureVlanContextId

                The Object 
                testValFsMIDot1qFutureGarpShutdownStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIDot1qFutureGarpShutdownStatus (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIDot1qFutureVlanContextId,
                                            INT4
                                            i4TestValFsMIDot1qFutureGarpShutdownStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMIDot1qFutureVlanContextId);
    UNUSED_PARAM (i4TestValFsMIDot1qFutureGarpShutdownStatus);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1dGmrpStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dGmrpStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qGvrpStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qGvrpStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1qFutureGarpShutdownStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1qFutureGarpShutdownStatus (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Dot1dPortGmrpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dPortGmrpTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Dot1dPortGarpTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Dot1dPortGarpTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDot1dExtBaseTable
 Input       :  The Indices
                FsDot1dBridgeContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dExtBaseTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDot1dPortGarpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dPortGarpTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDot1dPortGmrpTable
 Input       :  The Indices
                FsDot1dBasePort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDot1dPortGmrpTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIDot1qFutureGarpGlobalTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIDot1qFutureGarpGlobalTrace (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
#endif
#ifndef MRP_WANTED
/***************************************************************/
/*  Function Name   : MrpApiCreatePort                         */
/*  Description     : This function is for adding a port.      */
/*  Input(s)        : u2Port - Port to be added.               */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : gaMrpGlobalPortTable           */
/*  Global variables Modified : gaMrpGlobalPortTable           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         :OSIX_SUCCESS on successful addition of    */
/*                    port.OSIX_FAILURE on failure             */
/***************************************************************/
INT4
MrpApiCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalPortId);
    return OSIX_SUCCESS;
}

/***************************************************************/
/*  Function Name   : MrpApiDeletePort                         */
/*  Description     : This function is for deleting a port.    */
/*  Input(s)        : u4IfIndex - Port to be deleted           */
/*  Output(s)       : None                                     */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         :OSIX_SUCCESS on successful deletion  of   */
/*                    port.OSIX_FAILURE on failure             */
/***************************************************************/
INT4
MrpApiDeletePort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MrpLock                                              */
/*                                                                           */
/* Description        : This function is used to take the MRP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    :SNMP_SUCCESS/SNMP_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
MrpLock (VOID)
{
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MrpUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the MRP mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    :SNMP_SUCCESS/SNMP_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
MrpUnLock (VOID)
{
    return SNMP_SUCCESS;
}

/****************************************************************/
/*  Function Name   :MrpApiMmrpPropagateDefGroupInfo            */
/*  Description     :This function propagates the default Group */
/*                   info to all the ports during initialisation*/
/*  Input(s)        :u1Type - Whether it is VLAN_ALL_GROUPS or  */
/*                   VLAN_UNREG_GROUPS                          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
MrpApiMmrpPropagateDefGroupInfo (UINT4 u4ContextId, UINT1 u1Type,
                                 tVlanId VlanId,
                                 tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/****************************************************************/
/*  Function Name   :MrpApiMmrpSetDefGrpForbiddPorts            */
/*  Description     :depending on u1action we will add a static */
/*                   entry or delete it                         */
/*  Input(s)        :pMacAddr - Pointer to Mac address          */
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
MrpApiMmrpSetDefGrpForbiddPorts (UINT4 u4ContextId, UINT1 u1Type,
                                 tVlanId VlanId,
                                 tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/****************************************************************/
/*  Function Name   :MrpApiMmrpSetMcastForbiddPorts             */
/*  Description     :This function set the forbidden ports for a*/
/*                   given multicast entry                      */
/*  Input(s)        :pMacAddr - Pointer to multicast mac address*/
/*                   VlanId - VlanId                            */
/*                   AddPortList - Port list to be added        */
/*                   DelPortList - Port list to be deleted      */
/*  Output(s)       :None                                       */
/*  Global Variables Referred : None                            */
/*  Global variables Modified : None                            */
/*  Returns         :None                                       */
/****************************************************************/
void
MrpApiMmrpSetMcastForbiddPorts (UINT4 u4ContextId, tMacAddr MacAddr,
                                tVlanId VlanId,
                                tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/****************************************************************************/
/*  Function Name   :MrpApiMvrpPropagateVlanInfo                            */
/*  Description     :depending on u1action we will add a static             */
/*                   entry or delete it                                     */
/*  Input(s)        :u1AttrType -The attribute type received                */
/*                   AddPortList - Ports to be added                        */
/*                   DelPortList - ports to be deleted                      */
/*                   u1Action - depending on u1Action we will add a static  */
/*                              vlan  entry or delete it                    */
/*  Output(s)       :  None                                                 */
/*  Global Variables Referred : None                                        */
/*  Global variables Modified : None                                        */
/*  Returns         : None                                                  */
/****************************************************************************/
void
MrpApiMvrpPropagateVlanInfo (UINT4 u4ContextId, tVlanId VlanId,
                             tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/**************************************************************************/
/*  Function Name    : MrpApiMrpMapUpdateMapPorts ()                      */
/*  Description      : This function transmits Join messages on the given */
/*                     port, for all the Attributes registered for the    */
/*                     given GipId.                                       */
/*                     This function is called by VLAN whenever static    */
/*                     member ports are added.                            */
/*                     Updation of Map Port list affects only MMRP.       */
/*  Input(s)         : AddPortList - Ports that is added to the           */
/*                              given MAP context.                        */
/*                     u2Gip  - Id of the MAP context for which           */
/*                              new port is added.                        */
/*  Output(s)        : None                                               */
/*  Global Variables                                                      */
/*  Referred         : None                                               */
/*  Global variables                                                      */
/*  Modified         : None                                               */
/*  Exceptions or OS                                                      */
/*  Error handling   : None                                               */
/*  Use of Recursion : None                                               */
/*  Returns          : None                                               */
/**************************************************************************/
VOID
MrpApiMrpMapUpdateMapPorts (UINT4 u4ContextId, tPortList AddPortList,
                            tPortList DelPortList, UINT2 u2MapId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    UNUSED_PARAM (u2MapId);
    return;
}

/****************************************************************************/
/*  Function Name   :MrpApiMvrpSetVlanForbiddPorts                        */
/*  Description     :This function sets forbidden ports for a specific      */
/*                    VlanId                                                */
/*  Input(s)        :VlanId - VlanId for which Forbidden ports to be updated*/
/*                   AddPortList - Ports to be added                        */
/*                   DelPortList - ports to be deleted                      */
/*  Output(s)       :  None                                                 */
/*  Global Variables Referred : None                                        */
/*  Global variables Modified : None                                        */
/*  Returns         : None                                                  */
/****************************************************************************/
void
MrpApiMvrpSetVlanForbiddPorts (UINT4 u4ContextId, tVlanId VlanId,
                               tPortList AddPortList, tPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/*****************************************************************************/
/* Function Name      : MrpApiFillBulkMessage                                  */
/*                                                                           */
/* Description        : This function is used to fill the Vlan / Mcast /     */
/*                      Def Group Info in the Message. This funtion is called*/
/*                      whenever a Mvrp and Mmrp is enabled. This function   */
/*                      also post the message if the max no of message is    */
/*                      filled.                                              */
/*                                                                           */
/* Input(s)           : ppMrpQMsg - Double Pointer to store MrpQMsg        */
/*                      u1MsgType  - Type of Message - Vlan / Mcast / Def Grp*/
/*                      VlanId     - VlanId                                  */
/*                      Ports      - Added Portlist                          */
/*                      pu1MacAddr - MacAddres                               */
/*                                                                           */
/* Output(s)          : ppMrpQMsg - pointer to Allocated Message            */
/*                      pi4Count   - No of Message Filled                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    :OSIX_SUCCESS orOSIX_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
MrpApiFillBulkMessage (UINT4 u4ContextId, tMrpQMsg ** ppMrpQMsg,
                       UINT1 u1MsgType, tVlanId VlanId, UINT2 u2Port,
                       tLocalPortList Ports, UINT1 *pu1MacAddr)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (ppMrpQMsg);
    UNUSED_PARAM (u1MsgType);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (Ports);
    UNUSED_PARAM (pu1MacAddr);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : MrpApiPostBulkCfgMessage                           */
/*                                                                           */
/*    Description         : Posts the message to Mrp Config Q.              */
/*                                                                           */
/*    Input(s)            : pMrpQMsg -  Pointer to MrpQMsg                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            :OSIX_SUCCESS /OSIX_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
MrpApiPostBulkCfgMessage (tMrpQMsg * pMrpQMsg)
{
    UNUSED_PARAM (pMrpQMsg);
    return OSIX_SUCCESS;
}

/***************************************************************/
/*  Function Name   : MrpApiIsMvrpEnabled                      */
/*  Description     : Returns the MVRP Application Module      */
/*                    status                                   */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Returns         :OSIX_TRUE /OSIX_FALSE                     */
/***************************************************************/
INT4
MrpApiIsMvrpEnabled (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return OSIX_FALSE;
}

/***************************************************************/
/*  Function Name   : MrpApiIsMmrpEnabled                      */
/*  Description     : Returns the MMRP Application Module      */
/*                    status                                   */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Use of Recursion : None                                    */
/*  Returns         :OSIX_TRUE /OSIX_FALSE                     */
/***************************************************************/
INT4
MrpApiIsMmrpEnabled (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return OSIX_FALSE;
}

/***************************************************************/
/*  Function Name   : MrpApiUpdateInstVlanMap                  */
/*                                                             */
/*  Description     : This function updates GARP of Instance   */
/*                    to VLAN map Updation                     */
/*                                                             */
/*  Input(s)        : u1Action - Update / Post Message         */
/*                    u4ContextId - Context Identifier          */
/*                    u2NewMapId - New Map Identifier          */
/*                    u2OldMapId - Old Map Identifier          */
/*                                                             */
/*                    VlanId - VLAN Identifier of mapped VLAN  */
/*                    mapped to the instance                   */
/*                                                             */
/*                    u2MapEvent - Indicates whether it is a   */
/*                    single VLAN to MSTI or list of VLAN      */
/*                    map/umap                                 */
/*                                                             */
/*                    pu1Result - output to indicate message   */
/*                    post or updation                         */
/*                                                             */
/*  Output(s)       : Buffer to be posted to GARP              */
/*                                                             */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
VOID
MrpApiUpdateInstVlanMap (UINT4 u4ContextId, UINT1 u1Action, UINT2 u2NewMapId,
                         UINT2 u2OldMapId, tVlanId VlanId,
                         UINT2 u2MapEvent, UINT1 *pu1Result)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Action);
    UNUSED_PARAM (u2NewMapId);
    UNUSED_PARAM (u2OldMapId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u2MapEvent);
    UNUSED_PARAM (pu1Result);
    return;
}

/***************************************************************/
/*  Function Name   : MrpApiIsMrpStarted                       */
/*  Description     : Returns the MRP Module Status           */
/*                    Application                              */
/*  Input(s)        :                                          */
/*  Output(s)       :                                          */
/*  Use of Recursion : None                                    */
/*  Returns         :OSIX_TRUE /OSIX_FALSE                     */
/***************************************************************/
INT4
MrpApiIsMrpStarted (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return OSIX_FALSE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiNotifyPortOperP2PChg
 *                                                                          
 *    DESCRIPTION      : This function is called by L2IWF to post a message to 
 *                       MRP task's message queue in order to notify the change
 *                       in the operational point- to-point status of the port.
 *
 *    INPUT            : u4IfIndex      - Interface Index
 *                       bOperP2PStatus - Boolean value indicating whether
 *                                        the port is a Point To Point Port
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          :OSIX_SUCCESS/MRP_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
MrpApiNotifyPortOperP2PChg (UINT4 u4IfIndex, BOOL1 bOperP2PStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (bOperP2PStatus);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiNotifyPortStateChange 
 *
 *    DESCRIPTION      : This function is called by STP to post a message
 *                       to MRP task's message queue to notify the change in
 *                       the port state.       
 *
 *    INPUT            : u4IfIndex - Interface Index        
 *                       u2MapID   - CIST or MST Instance Identifier of the port
 *                                   whose state (FORWARDING to DISCARDING or
 *                                   vice versa) has changed
 *                       u1State   - Port State                        
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          :OSIX_SUCCESS/MRP_FAILURE 
 *
 ****************************************************************************/
INT4
MrpApiNotifyPortStateChange (UINT4 u4IfIndex, UINT2 u2MapId, UINT1 u1State)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2MapId);
    UNUSED_PARAM (u1State);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiNotifyPortRoleChange
 *                                                                          
 *    DESCRIPTION      : This function is called by STP to post a message
 *                       to MRP task's message queue to notify the change in
 *                       the port role.        
 *
 *    INPUT            : u4IfIndex - Interface Index        
 *                       u2MapID   - CIST or MST Instance Identifier of the port
 *                                   whose port role has changed
 *                       u1OldRole - Old Port Role                         
 *                       u1NewRole - New Port Role                        
 *
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          :OSIX_SUCCESS /OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
MrpApiNotifyPortRoleChange (UINT4 u4IfIndex, UINT2 u2MapID, UINT1 u1OldRole,
                            UINT1 u1NewRole)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2MapID);
    UNUSED_PARAM (u1OldRole);
    UNUSED_PARAM (u1NewRole);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiNotifyTcDetectedTmrState
 *                                                                          
 *    DESCRIPTION      : This function is called by STP module to post a 
 *                       message to MRP task's message queue 
 *                       whenever the tcDetected Timer is started/
 *                       stopped/expired.
 *
 *    INPUT            : u4IfIndex  - Interface Index of the port on which
 *                                    tcDetected Timer is running
 *                       u2MapID    - MST Instance ID to which this port 
 *                                    belongs (will 0 in case of RSTP)
 *                       u1TmrState - Timer status (running or not running)
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          :OSIX_SUCCESS /OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
MrpApiNotifyTcDetectedTmrState (UINT4 u4IfIndex, UINT2 u2MapID,
                                UINT1 u1TmrState)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2MapID);
    UNUSED_PARAM (u1TmrState);
    return OSIX_SUCCESS;

}

/***************************************************************
 *  FUNCTION NAME   :MrpApiMmrpPropagateMacInfo                     
 *  DESCRIPTION     :This function is used to Propagate the MAC
 *                   Info in the MAP Context.       
 *  INPUT(s)        :pMacAddr - Pointer to mac address          
 *                   VlanId - VlanId                            
 *                   AddPortList - Port list to be added        
 *                   DelPortList - Port list to be deleted      
 *  OUTPUT(s)       :None                                       
 *  GLOBAL VARIABLES REFERRED : None                            
 *  GLOBAL VARIABLES MODIFIED : None                            
 *  RETURNS         :None                                       
 ***************************************************************/
void
MrpApiMmrpPropagateMacInfo (UINT4 u4ContextId, tMacAddr MacAddr,
                            tVlanId VlanId,
                            tLocalPortList AddPortList,
                            tLocalPortList DelPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (MacAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddPortList);
    UNUSED_PARAM (DelPortList);
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiStartModule
 *                                                                          
 *    DESCRIPTION      : This function is stats the MRP module.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS
 *                                                                          
 ****************************************************************************/
INT4
MrpApiStartModule ()
{
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiShutdownModule
 *                                                                          
 *    DESCRIPTION      : This function is shutdowns MRP in all the contexts
 *                       in the System.
 *
 *    INPUT            : None 
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          :  OSIX_SUCCESS
 *                                                                          
 ****************************************************************************/
INT4
MrpApiShutdownModule ()
{
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiPropVlanDelFromEsp     
 *
 *    DESCRIPTION      : This API will be invoked by PBB-TE module to indicate
 *                       that a VLAN has been removed from ESP list. If the vlan
 *                       was part of ESP while creating, then that VLAN will not
 *                       be propagated by MRP. Hence, now this indication needs 
 *                       to be given to MRP module to trigger vlan propagation.
 *
 *    INPUT            : u4ContextId - Denotes in which context, the config 
 *                                     has to be applied
 *                       VlanId      - Identifies the VLAN
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
MrpApiPropVlanDelFromEsp (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiRemVlanAddedInEsp      
 *
 *    DESCRIPTION      : This API will be invoked by PBB-TE module to indicate
 *                       that a VLAN has been added to ESP list. Since, the VLAN
 *                       is part of ESP now, this VLAN needs to be removed from
 *                       MRP.
 *
 *    INPUT            : u4ContextId - Denotes in which context, the config 
 *                                     has to be applied
 *                       VlanId      - Identifies the VLAN
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
MrpApiRemVlanAddedInEsp (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MrpApiEnqueueRxPkt 
 *
 *    DESCRIPTION      : This function receives the incoming MRP PDU  from
 *                       the CFA Module and enqueues it to MRP PDU Queue for 
 *                       processing. 
 *
 *    INPUT            : pFrame - pointer to the received  MVRP/MMRP 
 *                                frame buffer (CRU buff)
 *                       u4IfIndex - Src port on which the frame is 
 *                                   received.
 *                       u2MapId - MAP id. value will be as follows:
 *                                 0 in case of MVRP pkt.
 *                                 VlanId in case of MMRP pkt.
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *
 ****************************************************************************/
INT4
MrpApiEnqueueRxPkt (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4IfIndex,
                    UINT2 u2MapId)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2MapId);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *  FUNCTION NAME   : MrpApiNotifyVlanInfo        
 *
 *   DESCRIPTION    : This function is called from VLAN module in the following
 *                    scenarios:
 *                    - whenever a static VLAN entry is added or deleted
 *                    - whenever forbidden ports are set for the specified VLAN 
 *                      Identifier 
 *                    - whenever a static MAC entry is added or deleted
 *                    - whenever the forbidden ports are set for the given 
 *                      multicast entry 
 *                    - to propagate the default Group Service Requirement 
 *                      Information to all ports during initialization
 *                    - whenever forbidden ports are set for the given Group
 *                      Service Requiremenent
 *                    - whenever static member ports are added/ deleted for the
 *                      given VLAN identifier.
 *
 *   INPUT          : pVlanInfo - Pointer to MrpVLanInfo structure 
 *
 *   OUTPUT         : None
 *
 *  RETURNS         : OSIX_SUCCESS/ OSIX_FAILURE
 *
 ******************************************************************************/
INT4
MrpApiNotifyVlanInfo (tMrpVlanInfo * pVlanInfo)
{
    UNUSED_PARAM (pVlanInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : MrpApiNotifyStpInfo
 *                                                                          
 *    DESCRIPTION      : This function is called from STP/L2IWF modules 
 *                       to indicate the following.
 *                       - MRP_PORT_ROLE_CHG_MSG
 *                       - MRP_PORT_OPER_P2P_MSG
 *                       - MRP_TCDETECTED_TMR_STATUS
 *
 *    INPUT            : pMrpInfo - Pointer to the structure which 
 *                                  contains the MRP info from the above 
 *                                  modules. 
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
MrpApiNotifyStpInfo (tMrpInfo * pMrpInfo)
{
    UNUSED_PARAM (pMrpInfo);

    return OSIX_SUCCESS;
}
#endif

#ifndef RSTP_WANTED
/*****************************************************************************/
/* Function Name      : AstCreatePort                                        */
/*                                                                           */
/* Description        : This function is called from the CFA Module          */
/*                      to indicate the creation of a Port.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to be created*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfPortCreateIndication                            */
/*****************************************************************************/
INT4
AstCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2PortNum)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2PortNum);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstCreatePortFromLa                                  */
/*                                                                           */
/* Description        : This function is called from the l2iwf Module        */
/*                      to indicate the creation of a Port from LA.          */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to be created*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfPortCreateIndication                            */
/*****************************************************************************/
INT4
AstCreatePortFromLa (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2PortNum)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2PortNum);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeletePort                                        */
/*                                                                           */
/* Description        : This function is called from the CFA Module          */
/*                      to indicate the deletion of a Port.                  */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to be deleted*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfPortDeleteIndication                            */
/*****************************************************************************/
INT4
AstDeletePort (UINT4 u4PortNum)
{
    UNUSED_PARAM (u4PortNum);
    return RST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstDeletePortFromLa                                  */
/*                                                                           */
/* Description        : This function is called from the L2iwf Module        */
/*                      to indicate the deletion of a Port from LA.          */
/*                                                                           */
/* Input(s)           : u2PortNum - The Port number of the port to be created*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RST_SUCCESS / RST_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfPortCreateIndication                            */
/*****************************************************************************/
INT4
AstDeletePortFromLa (UINT4 u4PortNum)
{
    UNUSED_PARAM (u4PortNum);
    return RST_SUCCESS;
}

/***************************************************************************/
/* Function           : AstApiStpConfigInfo                               */
/* Input(s)           : tStpConfigInfo -STP Config Info                   */
/* Returns            : OSIX_SUCCESS/OSIX_FAILURE                          */
/* Action             : This API configure the RSTP module                 */
/***************************************************************************/
INT4
AstApiStpConfigInfo (tStpConfigInfo * pStpConfigInfo)
{
    UNUSED_PARAM (pStpConfigInfo);
    return OSIX_SUCCESS;
}
#endif

#ifndef VLAN_WANTED
/*****************************************************************************/
/* Function Name      : L2IwfPvlanDelMemPoolAndRBTree                        */
/*                                                                           */
/* Description        : This routine deletes the mempool and RBTree created  */
/*                      for maintaing Pvlan Mapping                          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPvlanDelMemPoolAndRBTree (VOID)
{
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPvlanCrtMemPoolAndRBTree                        */
/*                                                                           */
/* Description        : This routine creates the mempool required for        */
/*                      storing Pvlan Mapping and creates RBTree in L2Iwf    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPvlanCrtMemPoolAndRBTree (VOID)
{
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanFlushFdbTable                                */
/*                                                                           */
/*    Description         : This function flushes the Fdb entires in Fdb     */
/*                          table based on port and FdbId                    */
/*                                                                           */
/*    Input(s)            : u2Port - Port number                             */
/*                          u4FdbId - FdbId                                  */
/*                          i4OptimizeFlag - Indicates whether this call can */
/*                                           be grouped with  the flush call */
/*                                           for other ports as a single     */
/*                                           bridge  flush                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanFlushFdbTable (tVlanFlushInfo * pVlanFlushInfo)
{
    UNUSED_PARAM (pVlanFlushInfo);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanCreatePort                                   */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever any port is created by */
/*                          mgmt module                                      */
/*                                                                           */
/*    Input(s)            : u2Port    - The Number of the Port               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2PortNum)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2PortNum);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDeletePort                                   */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port is deleted   */
/*                          by management module.                            */
/*                                                                           */
/*    Input(s)            : u2Port - The number of the port that is deleted. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanDeletePort (UINT4 u4Port)
{
    UNUSED_PARAM (u4Port);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetTagLenInFrame                             */
/*                                                                           */
/*    Description         : This function returns the VLAN tag offset from   */
/*                          the incoming frame.                              */
/*                                                                           */
/*    Input(s)            : pFrame     - Incoming frame                      */
/*                                                                           */
/*    Output(s)           : pu4VlanOffset - VLAN tag offset in the incoming  */
/*                                          frame based on the number of     */
/*                                          tags present.                    */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetTagLenInFrame (tCRU_BUF_CHAIN_DESC * pFrame, UINT4 u4IfIndex,
                      UINT4 *pu4VlanOffset)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4VlanOffset);

    return VLAN_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUnTagFrame                                   */
/*                                                                           */
/*    Description         : This function removes the Vlan Tag from the      */
/*                          incoming packet.                                 */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
VlanUnTagFrame (tCRU_BUF_CHAIN_DESC * pFrame)
{
    UNUSED_PARAM (pFrame);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanTransmitCfmFrame                  */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanTransmitCfmFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                      UINT2 u2Port, tVlanTag VlanTagInfo, UINT1 u1FrameType)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (VlanTagInfo);
    UNUSED_PARAM (u1FrameType);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetStartedStatus                             */
/*                                                                           */
/*    Description         : This function returns whether VLAN is started or */
/*                          shutdown                                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gau1VlanShutDownStatus                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_TRUE or VLAN_FALSE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetStartedStatus (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);

    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetPortStats                                 */
/*                                                                           */
/*    Description         : This function is called by ECFM to get the       */
/*                          transmit and receive data counters for the port  */
/*                          and vlan                                         */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port Number                          */
/*                          VlanId - Vlan ID for which stats are required    */
/*                                                                           */
/*    Output(s)           : pu4TxFCl - Value of Transmission counter         */
/*                          pu4RxFCl - Value of Reception counter            */
/*                                                                           */
/*    Returns            : NONE                                              */
/*                                                                           */
/*****************************************************************************/
VOID
VlanGetPortStats (UINT4 u4IfIndex, tVlanId VlanId,
                  UINT4 *pu4TxFCl, UINT4 *pu4RxFCl)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pu4TxFCl);
    UNUSED_PARAM (pu4RxFCl);
    return;
}

/*****************************************************************************/
/* Function Name      : l2iwfGetPbPortType                                   */
/*                                                                           */
/* Description        : This function is used to get the port type of a      */
/*                      Physical port                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                      u4IfIndex   - IfIndex of the port.                   */
/*                                                                           */
/* Output(s)          : *pu1PortType - CEP/CNP/PNP/PCEP/PCNP/PPNP            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
PUBLIC INT4
L2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1PortType);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiIsVlanUntagMemberPort                         */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      port is a untag member of the given Vlan in the      */
/*                      given context.                                       */
/*                                                                           */
/* Input(s)           : ContextId, VlanId, Global IfIndex                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
L2IwfMiIsVlanUntagMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                              UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
    return OSIX_TRUE;
}

#endif
#if (! defined (IGS_WANTED)) && (! defined (MLDS_WANTED))
/*****************************************************************************/
/* Function Name      : SnoopMiUpdatePortList                                */
/*                                                                           */
/* Description        : This function is called from the VLAN Module         */
/*                      to indicate the port list updation for a given       */
/*                      instance.                                            */
/*                                                                           */
/* Input(s)           : u4Instance - Instance for which the operation        */
/*                                   should be done                          */
/*                      VlanId  - Vlan Identifier                            */
/*                    : McastAddress - Multicast mac address, will be NULL   */
/*                                     for vlan updation.                    */
/*                    : AddPortBitmap - Ports to be added to snoop Fwd Table */
/*                    : DelPortBitmap - Ports to be deleted from snoop Fwd   */
/*                                      Table                                */
/*                      u1PortType - To indicate VLAN/Mcast Ports updation   */
/*                                   VLAN_UPDT_VLAN_PORTS /                  */
/*                                   VLAN_UPDT_MCAST_PORTS                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopMiUpdatePortList (UINT4 u4Instance, tSnoopVlanId VlanId,
                       tMacAddr McastAddr, tSnoopIfPortBmp AddPortBitmap,
                       tSnoopIfPortBmp DelPortBitmap, UINT1 u1PortType)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (McastAddr);
    UNUSED_PARAM (AddPortBitmap);
    UNUSED_PARAM (DelPortBitmap);
    UNUSED_PARAM (u1PortType);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMiDelMcastFwdEntryForVlan                       */
/*                                                                           */
/* Description        : This function is called from the VLAN Module         */
/*                      to indicate the deletion of a VLAN for a given       */
/*                      Instance.                                            */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Identifier                             */
/*                      u4Instance - Instance for which the operation        */
/*                                   should be done                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : VLAN Module                                          */
/*****************************************************************************/
INT4
SnoopMiDelMcastFwdEntryForVlan (UINT4 u4Instance, tSnoopVlanId VlanId)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopCreatePort                                      */
/*                                                                           */
/* Description        : This function is called from the L2IWF               */
/*                      to indicate the creation of a port                   */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id mapped to the port        */
/*                      u4IfIndex     - Physical index of the port           */
/*                      u2LocalPortId - Local port id in VCM for the port    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalPortId);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopDeletePort                                      */
/*                                                                           */
/* Description        : This function is called from the CFA Module          */
/*                      to indicate the deletion of a Port.                  */
/* Input(s)           : u4Port - The Port number of the port to be deleted   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopDeletePort (UINT4 u4Port)
{
    UNUSED_PARAM (u4Port);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMapPort                                         */
/*                                                                           */
/* Description        : This function is called from the L2IWF               */
/*                      to indicate the mapping a port to acontext           */
/*                                                                           */
/* Input(s)           : u4Context - instance Id,                             */
/*                      u4IfIndex - Interface index,                         */
/*                      u2Port  - Local port number corresponding to physical*/
/*                                port                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2Port)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2Port);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUnmapPort                                       */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Modul         */
/*                      to indicate the unmap of port from a context         */
/* Input(s)           : u2Port - The Port number of the port to be unmapped  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopUnmapPort (UINT4 u4Port)
{
    UNUSED_PARAM (u4Port);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUpdateHwProperties                             */
/*                                                                           */
/* Description        : This function is called from the L2IWF Module        */
/*                      to indicate the port-channel status indication       */
/*                                                                           */
/* Input(s)           : u2AggId - AggId                                      */
/*                      u4IfIndex - Port Index                               */
/*                      u1Status  - Add/Remove from Port-channel             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : L2Iwf Module                                         */
/*****************************************************************************/
INT4
SnoopUpdateHwProperties (UINT2 u2AggId, UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u2AggId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return SNOOP_SUCCESS;
}
#endif
#ifndef CLI_WANTED

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliTftpDownloadStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function is to display the status of the      */
/*                        download initiated by TFTP                         */
/*                                                                           */
/*     INPUT            : i4Data -  Size of Data being downloaded at that    */
/*                                  instant                                  */
/*                                                                           */
/*                                                                           */
/*                        u4FileSize - Entire file size being downloaded     */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : Prints the download status                         */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
CliTftpDownloadStatus (INT4 i4Data, UINT4 u4FileSize)
{
    UNUSED_PARAM (i4Data);
    UNUSED_PARAM (u4FileSize);
    return;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliSetSessionBitMap                                 *
 *                                                                         *
 *     Description   : This exported function is called by external        *
 *                     modules to set a bit in global value in CLI         *
 *                     Modules can set a bit to disable CLI console prompt *
 *                     for a particular session                            *
 *                                                                         *
 *     Input(s)      : u1SessionType  : Type of session for which bitmap   *
 *                                      is to be set                       *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
 *                                                                         *
 ***************************************************************************/
VOID
CliSetSessionBitMap (UINT4 u4SessionType)
{
    UNUSED_PARAM (u4SessionType);
    return;
}

/***************************************************************************
 *                                                                         *
 *     Function Name : CliRemoveCommand                                    *
 *                                                                         *
 *     Description   : This function will remove the cli command from ISS  *
 *                                                                         *
 *     Input(s)      : pModeName - Mode name for the command               *
 *                     pCmdName  - Command to be removed under that mode   *
 *                                                                         *
 *                                                                         *
 *     Output(s)     : NONE                                                *
 *                                                                         *
 *     Returns       : CLI_SUCCESS/CLI_FAILURE                             *
 *                                                                         *
 ***************************************************************************/
INT1
CliRemoveCommand (INT1 *pModeName, INT1 *pCmdName)
{
    UNUSED_PARAM (pModeName);
    UNUSED_PARAM (pCmdName);
    return CLI_SUCCESS;
}

/***************************************************************************
* FUNCTION NAME : CliPrintf
* DESCRIPTION   : The message is stored in a separate buffer. If the
*                 buffer is full, then the message is written onto the
*                 output.
*
* INPUT         : CliHandle       - Index of the current Cli Context
*                                   structure in the global array.
*                 fmt             - format of the output message
*
* OUTPUT        : NULL
***************************************************************************/
INT4
CliPrintf (tCliHandle CliHandle, CONST CHR1 * fmt, ...)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (fmt);
    return CLI_SUCCESS;
}

VOID
mmi_printf (const char *fmt, ...)
{
    MEM_PRNT (fmt);
}

void
FsCustFree (void *pNode)
{
    UNUSED_PARAM (pNode);
    return;
}

void               *
FsCustRealloc (void *ptr, size_t size)
{

    UINT4              *pu4Ptr = NULL;
    UNUSED_PARAM (ptr);
    UNUSED_PARAM (size);
    return (pu4Ptr);
}

VOID               *
FsCustCalloc (size_t nmemb, size_t size)
{
    UINT4              *pu4Ptr = NULL;
    UNUSED_PARAM (nmemb);
    UNUSED_PARAM (size);
    return (pu4Ptr);

}

void               *
FsCustMalloc (size_t size)
{
    UINT4              *pu4Ptr = NULL;
    UNUSED_PARAM (size);
    return (pu4Ptr);
}

/***************************************************************************
*                                                                         *
*     Function Name : CliUtilCheckAllowUserLogin                          *
*                                                                         *
*     Description   : This function is used to check if the user is       *
*                     allowed to login or not                             * 
*                                                                         *
*     Input (s)     : pi1Name - Pointer to user name                      *
*                                                                    *
*     Output(s)     : None                                                *
*                                                                         *
*     Returns       : CLI_SUCCESS or CLI_FAILURE                          *
*                                                                         *
****************************************************************************/
INT4
CliUtilCheckAllowUserLogin (CHR1 * pi1Name)
{
    UNUSED_PARAM (pi1Name);
    return CLI_SUCCESS;
}

INT2
CliCheckUserPasswd (INT1 *pi1UserName, CONST INT1 *pi1UserPasswd)
{
    UNUSED_PARAM (pi1UserName);
    UNUSED_PARAM (pi1UserPasswd);
    return CLI_SUCCESS;
}

INT2
CliAuthenticateUserPasswd (INT1 *pi1UserName, CONST INT1 *pi1UserPasswd)
{
    UNUSED_PARAM (pi1UserName);
    UNUSED_PARAM (pi1UserPasswd);
    return CLI_SUCCESS;
}

/***************************************************************************  
 * FUNCTION NAME : CliLogOutMessage
 * DESCRIPTION   : This function displays the logout message for SSH and 
 *                 telnet sessions during session logout.
 * INPUT         : none 
 * OUTPUT        : returns the idle timer expiry message.
 ***************************************************************************/
CHR1               *
CliLogOutMessage ()
{
    return ((CHR1 *) "\r\nIdle Timer expired, Timing Out !!!\n");
}

VOID
CliSetSshClientWindowSize (INT2 i2Row, INT2 i2Col)
{
    UNUSED_PARAM (i2Row);
    UNUSED_PARAM (i2Col);
    return;
}

INT4
CliDeleteSshContext (UINT1 *pu1TaskName)
{
    UNUSED_PARAM (pu1TaskName);
    return CLI_SUCCESS;
}

INT4
CliSetErrorCode (UINT4 u4ErrCode)
{
    UNUSED_PARAM (u4ErrCode);
    return CLI_SUCCESS;
}

INT4
CfaCliGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr, UINT1 *pu1IfListArray,
                 UINT4 u4IfListArrayLen)
{
    UNUSED_PARAM (pi1IfName);
    UNUSED_PARAM (pi1IfListStr);
    UNUSED_PARAM (pu1IfListArray);
    UNUSED_PARAM (u4IfListArrayLen);
    return CFA_SUCCESS;
}

/*************************************************************************
   FUNCTION      : Routine to reset SSH context task id to 0.
   DESCRIPTION   : This Routine is responsible for resetting SSH task id to 0
   INPUT(S)      : None
   OUTPUT(S)     : None.
   RETURN(S)     : None
***************************************************************************/

VOID
CliClearSshTaskId (VOID)
{
    return;
}

/*************************************************************************
   FUNCTION      : CliGetConnectionId
   DESCRIPTION   : This Routine is responsible for getting the connection ID
   INPUT(S)      : None
   OUTPUT(S)     : None.
   RETURN(S)     : CFA_SUCCESS
***************************************************************************/

INT4
CliGetConnectionId (VOID)
{
    return CFA_SUCCESS;
}
#endif

#if !defined (LNXIP4_WANTED) && !defined (IP_WANTED)
/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetCfaIfIndexFromPort
 * *                   Provides the CFA IfIndex Corresponding to IP Port No
 *
 * Input(s)           : u4Port - IP Port Number
 * Output(s)          : CFA Interface Index
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Provides CFA IFIndex Corresponding to IP Port No
------------------------------------------------------------------- */
INT4
NetIpv4GetCfaIfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pu4IfIndex);
    return NETIPV4_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4DeRegisterHigherLayerProtocol
 *
 * Description      :   This function De-Registers the Protocol specified by
 *                      the Application.
 *
 * Inputs           :   u1ProtoId - Protocol Identifier.
 *
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4DeRegisterHigherLayerProtocol (UINT1 u1ProtoId)
{
    UNUSED_PARAM (u1ProtoId);
    return (NETIPV4_SUCCESS);
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4RegisterHigherLayerProtocol
 *
 * Description      :   This function is used by Higher Layer Protocols to
 *                      register with Ip for Receiving their packets or for
 *                      getting notification on IF changes or Route Changes.
 *
 * Inputs           :   pRegInfo - Information about the registering protocol
 *                                 and the call back functions.
 * Outputs          :   None.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE.
 *
 *******************************************************************************
 */

INT4
NetIpv4RegisterHigherLayerProtocol (tNetIpRegInfo * pRegInfo)
{
    UNUSED_PARAM (pRegInfo);
    return (NETIPV4_SUCCESS);
}

/*-------------------------------------------------------------------+
 * Function           : NetIpv4GetPortFromIfIndex
 * *                     Provides the IP Port Number Corresponding to IfIndex
 *
 * Input(s)           : u4IfIndex - CFA IfIndex
 * Output(s)          : IP Port Number.
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Provides the IP Port Number Corresponding to IfIndex
------------------------------------------------------------------- */
INT4
NetIpv4GetPortFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4Port)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4Port);
    return NETIPV4_SUCCESS;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv4GetIfInfo
 *
 * Description      :   This function provides the Interface related
 *                      Information for a given IfIndex.
 *
 * Inputs           :   u4IfIndex
 *
 * Outputs          :   pNetIpIfInfo - If-Info for the given IfIndex.
 *
 * Return Value     :   NETIPV4_SUCCESS | NETIPV4_FAILURE
 *
 *******************************************************************************
 */
INT4
NetIpv4GetIfInfo (UINT4 u4IfIndex, tNetIpv4IfInfo * pNetIpIfInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpIfInfo);
    return NETIPV4_SUCCESS;
}

#endif

#ifndef RSTP_WANTED
/*****************************************************************************/
/* Function Name      : AstIsRstEnabledInContext                             */
/*                                                                           */
/* Description        : Called by other modules to know if RSTP is enabled   */
/*                      and running in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Rstp is enabled in the given context         */
/*                      0 - No, Rstp is NOT enabled in the given context     */
/*****************************************************************************/
INT4
AstIsRstEnabledInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return 0;
}

/*****************************************************************************/
/* Function Name      : AstIsStpEnabled                                      */
/*                                                                           */
/* Description        : This function is called to check whether STP is      */
/*                      enabled                                              */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
AstIsStpEnabled (INT4 i4IfIndex, UINT4 u4ContextId)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4ContextId);
    return OSIX_FAILURE;
}
#endif

#ifndef VLAN_WANTED
/*****************************************************************************/
/* Function Name      : L2IwfGetFdbInfo                                      */
/*                                                                           */
/* Description        : This routine returns the list of Fdbs  mapped to the */
/*                      given Instance.                                      */
/*                                                                           */
/* Input(s)           : pL2IwfFdbInfo - Pointer to tL2IwfFdbInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Fdbs Mapped                                */
/*****************************************************************************/
INT4
L2IwfGetFdbInfo (tL2IwfFdbInfo * pL2IwfFdbInfo)
{
    UNUSED_PARAM (pL2IwfFdbInfo);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanFdbId                                  */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      the customer's MI-unaware protocol stacks.           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Fdb Id is to obtained       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FdbId value corresponding to the VlanId              */
/*****************************************************************************/
UINT4
L2IwfMiGetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return 0;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPvidVlanOfAnyPorts                            */
/*                                                                           */
/* Description        : This is used to check whether the VLAN is PVID       */
/*                      of any ports                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context identifier                     */
/*                      VlanId - VLAN Index                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_FALSE / L2IWF_FALSE                            */
/*****************************************************************************/
INT4
L2IwfIsPvidVlanOfAnyPorts (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return L2IWF_TRUE;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanUnTagPorts                             */
/*                                                                           */
/* Description        : This routine returns the Untagged ports for the given*/
/*                      VlanId as Local port IDs. It accesses the L2Iwf      */
/*                      common database for the given context                */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId - Service VLAN Identifier                     */
/*                      UnTagPorts                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfMiGetVlanUnTagPorts (UINT4 u4ContextId, tVlanId VlanId,
                          tPortList UnTagPorts)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (UnTagPorts);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanEgressPorts                            */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      the customer's MI-unaware protocol stacks.           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Egress ports are to be      */
/*                                    obtained                               */
/*                                                                           */
/* Output(s)          : EgressPorts - Egress Port List for the given Vlan    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfMiGetVlanEgressPorts (UINT4 u4ContextId, tVlanId VlanId,
                           tPortList EgressPorts)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    MEMSET (EgressPorts, 0, sizeof (tPortList));
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiIsVlanMemberPort                              */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      the customer's MI-unaware protocol stacks.           */
/*                                                                           */
/* Input(s)           : ContextId, VlanId, Global IfIndex                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
L2IwfMiIsVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiIsVlanActive                                  */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      the customer's MI-unaware protocol stacks.           */
/*                                                                           */
/* Input(s)           : ContextId, VlanId                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
L2IwfMiIsVlanActive (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetNextActiveVlan                             */
/*                                                                           */
/* Description        : Stub provided for backward compatibility with        */
/*                      the customer's MI-unaware protocol stacks.           */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2VlanId    - Vlan Index whose next vlan is to be    */
/*                                    determined                             */
/*                                                                           */
/* Output(s)          : *pu2NextVlanId -  pointer to the next active VlanId  */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfMiGetNextActiveVlan (UINT4 u4ContextId, UINT2 u2VlanId,
                          UINT2 *pu2NextVlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (pu2NextVlanId);
    return L2IWF_FAILURE;
}

INT4
VlanIsVlanEnabledInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiFlushFdbId                                 */
/*                                                                           */
/*    Description         : Stub provided for backward compatibility with    */
/*                          the customer's MI-unaware protocol stacks.       */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch ID                  */
/*                          u4FdbId - FdbId                                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanMiFlushFdbId (UINT4 u4ContextId, UINT4 u4FdbId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4FdbId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanApiIsPvidVlanOfAnyPorts                      */
/*                                                                           */
/*    Description         : Stub provided for backward compatibility with    */
/*                          the customer's MI-unaware protocol stacks.       */
/*                                                                           */
/*    Input(s)            : VlanId - Vlan Id                                 */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/

INT4
VlanApiIsPvidVlanOfAnyPorts (tVlanId VlanId)
{
    UNUSED_PARAM (VlanId);
    return VLAN_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiGetVlanLearningMode                        */
/*                                                                           */
/*    Description         : Stub provided for backward compatibility with    */
/*                          the customer's MI-unaware protocol stacks.       */
/*                                                                           */
/*    Input(s)            : None                          .                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : gVlanInfo.u1LearningType                          */
/*                                                                           */
/*****************************************************************************/

UINT1
VlanMiGetVlanLearningMode (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return VLAN_INDEP_LEARNING;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsIgmpTunnelEnabledOnAnyPort                    */
/*                                                                           */
/* Description        : This routine checks whether Igmp tunneling is enabled*/
/*                      on any of the port in the bridge.                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE/ L2IWF_FALSE                              */
/*****************************************************************************/
INT4
L2IwfIsIgmpTunnelEnabledOnAnyPort (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return L2IWF_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanConfCheckForPvrst                            */
/*                                                                           */
/*    Description         : This function will be called before starting     */
/*                          PVRST for the given context. This function       */
/*                          checks whether the configurations in vlan are    */
/*                          suitable for PVRST operation.                    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : pu4ErrorCode - Denotes the mismatch type.        */
/*                          VLAN_PVRST_VLAN_NOTSTARTED_ERR- Vlan Module is   */
/*                                                          Shutdown.        */
/*                          VLAN_PVRST_CXT_NOTPRESENT_ERR - Context Not      */
/*                                                          present in VLAN. */
/*                          VLAN_PVRST_HYBRID_PVID_ERR    - Hybrid port Pvid */
/*                                                          is other than    */
/*                                                          default VLAN.    */
/*                          VLAN_PVRST_HYBRID_UNTAG_ERR   - Hybrid port is   */
/*                                                          not an untagged  */
/*                                                          member of VLAN 1.*/
/*                          VLAN_PVRST_HYBRID_VLAN_ERR    - Hybrid port is   */
/*                                                          member of vlan   */
/*                                                          other than       */
/*                                                          default vlan.    */
/*                          VLAN_PVRST_ACCESS_PVID_ERR    - Access port is   */
/*                                                          member of VLANs  */
/*                                                          other than pvid  */
/*                                                          vlan.            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanConfigCheckForPvrst (UINT4 u4ContextId, UINT4 *pu4ErrorCode)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu4ErrorCode);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanConfigUpdtForPvrstStart                      */
/*                                                                           */
/*    Description         : This function is called by PVRST module when     */
/*                          it comes up. The changes required in VLAN for    */
/*                          enabling pvrst will be done in this function.    */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanConfigUpdtForPvrstStart (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanConfigUpdtForPvrstShutDown                   */
/*                                                                           */
/*    Description         : This function is called by PVRST module when     */
/*                          it goes down. The changes required in VLAN for   */
/*                          disabling pvrst will be done in this function.   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanConfigUpdtForPvrstShutDown (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsProtocolVlanSupported                      */
/*                                                                           */
/*    Description         : This function is called by LLDP to get the       */
/*                          Protocol based Vlan Classification support       */
/*                          in the switch                                    */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_TRUE / VLAN_FALSE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsProtocolVlanSupported (VOID)
{
    return VLAN_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetProtoVlanStatusOnPort                     */
/*                                                                           */
/*    Description         : This function is called by LLDP to get the       */
/*                          status of Protocol based Vlan Classification     */
/*                          on the port                                      */
/*                                                                           */
/*    Input(s)            : u4IfIndex                                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetProtoVlanStatusOnPort (UINT4 u4IfIndex, UINT1 *pu1ProtoVlanStatus)
{
    UNUSED_PARAM (u4IfIndex);
    *pu1ProtoVlanStatus = VLAN_SNMP_FALSE;
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetProtoVlanOnPort                           */
/*                                                                           */
/*    Description         : This function is called by LLDP to get the       */
/*                          list of protocol vlans mapped to the port        */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port Number                          */
/*                                                                           */
/*    Output(s)           : apConfProtoVlans - Pointer to array of protocol  */
/*                                             vlans mapped to the port      */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS / VLAN_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
VlanGetProtoVlanOnPort (UINT4 u4IfIndex, UINT2 *apConfProtoVlans,
                        UINT1 *pu1NumVlan)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (apConfProtoVlans);
    UNUSED_PARAM (pu1NumVlan);
    return VLAN_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanMiDeleteAllFdbEntries                        */
/*                                                                           */
/*    Description         : This function calls the Vlan NPAPI to flush      */
/*                          all entries learnt for this context.             */
/*                                                                           */
/*    Input(s)            : u4ContextId.                                     */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
VOID
VlanMiDeleteAllFdbEntries (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return;
}
#endif
/*****************************************************************************/
/* Function Name      : L2IwfSetDefaultVlanId                                */
/*                                                                           */
/* Description        : This function sets the Default Vlan Id in L2IWF.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Default Vlan Id                                      */
/*****************************************************************************/
VOID
L2IwfSetDefaultVlanId (UINT2 u2DefaultVlanId)
{
    UNUSED_PARAM (u2DefaultVlanId);
    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetInterfaceType                                */
/*                                                                           */
/* Description        : This function sets the Interface Type in L2IWF.      */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context ID                            */
/*                      u1InterfaceType - Interface Type to be set in L2IWF  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfSetInterfaceType (UINT4 u4ContextId, UINT1 u1InterfaceType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1InterfaceType);
    return (L2IWF_FAILURE);
}

/*****************************************************************************/
/* Function Name      : L2IwfSetAdminIntfTypeFlag                            */
/*                                                                           */
/* Description        : This function sets the Interface Type in L2IWF.      */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context ID                            */
/*                      u1AdminIntfTypeFlag - Admin Interface Type Flag to   */
/*                                              be set in L2IWF              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfSetAdminIntfTypeFlag (UINT4 u4ContextId, UINT1 u1AdminIntfTypeFlag)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1AdminIntfTypeFlag);
    return (L2IWF_FAILURE);
}

/*****************************************************************************/
/* Function Name      : L2IwfGetCnpPortCount                                 */
/*                                                                           */
/* Description        : This function returns the CNP port count             */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : pu4Count - Count of no. of internal ports            */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfGetCnpPortCount (UINT4 u4ContextId, UINT4 *pu4Count)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu4Count);
    return (L2IWF_FAILURE);
}

/*****************************************************************************/
/* Function Name      : L2IwfGetAdminIntfTypeFlag                            */
/*                                                                           */
/* Description        : This function returns the Interface Type configured  */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : pu1AdminIntfTypeFlag - Admin Interface Type Flag     */
/*                                               configured in L2IWF         */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfGetAdminIntfTypeFlag (UINT4 u4ContextId, UINT1 *pu1AdminIntfTypeFlag)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu1AdminIntfTypeFlag);
    return (L2IWF_FAILURE);
}

/*****************************************************************************/
/* Function Name      : L2IwfGetInterfaceType                                */
/*                                                                           */
/* Description        : This function returns the Interface Type configured  */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : pu1InterfaceType - Interface Type configured in L2IWF*/
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfGetInterfaceType (UINT4 u4ContextId, UINT1 *pu1InterfaceType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu1InterfaceType);
    return (L2IWF_FAILURE);
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPVlanMappingInfo                             */
/*                      This function gives the mapping between primary and  */
/*                      secondary vlan. This function will be invoked by     */
/*                      Spanning tree, GMRP, MMRP, IGS to know the type of   */
/*                      vlan and to get the primary to seconday vlan mapping */
/*                      and vice versa.                                      */
/*                                                                           */
/* Input(s)           : pL2PvlanMappingInfo - Pointer to the structure       */
/*                                            containing Private vlan mapping*/
/*                                            information. The following are */
/*                                            the fields in this structure   */
/*                                            that are input to this API.    */
/*                      u1RequestType - Type of request. It can take the     */
/*                                      following values:                    */
/*                                        1.L2IWF_VLAN_TYPE                  */
/*                                        2.L2IWF_MAPPED_VLANS               */
/*                                                                           */
/*                                      If L2IWF_VLAN_TYPE is passed, then   */
/*                                      pMappedVlans will not be filled, but */
/*                                      only value for pVlanType is filled.  */
/*                                                                           */
/*                                      If L2IWF_MAPPED_VLANS is passed,     */
/*                                      then both pVlanMapped Vlans and      */
/*                                      pVlanType are filled.                */
/*                      InVlanId - Id of the given Vlan.                     */
/*                                                                           */
/* Output(s)          : pL2PvlanMappingInfo - pointer to the structure       */
/*                                            containing Private vlan mapping*/
/*                                            information. The following are */
/*                                            the fields in this structure   */
/*                                            that are output to this API.   */
/*                                                                           */
/*                      pMappedVlans - If the InVlanId is a primary vlan,    */
/*                                     then this pointer points to an array  */
/*                                     containing the list of secondary vlans*/
/*                                     associated with that primary vlan.    */
/*                                                                           */
/*                                     If the InVlanId is a secondary vlan,  */
/*                                     this *pMappedVlans will contain the ID*/
/*                                     of primary vlan associated with the   */
/*                                     given secondary vlan (InVlanId).      */
/*                                                                           */
/*                                     If u1RequestTyps is                   */
/*                                     L2IWF_MAPPED_VLANS, then only this    */
/*                                     array will be filled. Otherwise this  */
/*                                     will not filled.                      */
/*                                                                           */
/*                      pu1VlanType - This gives the type of vlan of the     */
/*                                    given input vlan (InVlanId). Possible  */
/*                                    values for this parameter are:         */
/*                                      1.L2IWF_VLAN_TYPE_NORMAL,            */
/*                                      2.L2IWF_VLAN_TYPE_PRIMARY,           */
/*                                      3.L2IWF_VLAN_TYPE_ISOLATED,          */
/*                                      4.L2IWF_VLAN_TYPE_COMMUNITY          */
/*                                                                           */
/*                      pu2NumMappedVlans - Number of vlans present in the   */
/*                                          array pointed by pMappedVlans.   */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
L2IwfGetPVlanMappingInfo (tL2PvlanMappingInfo * pL2PvlanMappingInfo)
{
    UNUSED_PARAM (pL2PvlanMappingInfo);
    return;
}

#endif /* ifndef VLAN_WANTED */

#ifndef PVRST_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstMiVlanDeleteIndication                */
/*                                                                           */
/*    Description               : Stub provided for backward compatibility   */
/*                                with the customer's MI-unaware protocol    */
/*                                stacks.                                    */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                VlanId  - VlanId which is  deleted.        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*****************************************************************************/
INT4
PvrstMiVlanDeleteIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstMiVlanCreateIndication                */
/*                                                                           */
/*    Description               : Stub provided for backward compatibility   */
/*                                with the customer's MI-unaware protocol    */
/*                                stacks.                                    */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                VlanId  - VlanId which is created          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*****************************************************************************/
INT4
PvrstMiVlanCreateIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : PvrstMiDeleteAllVlanIndication             */
/*                                                                           */
/*    Description               : Stub provided for backward compatibility   */
/*                                with the customer's MI-unaware protocol    */
/*                                stacks.                                    */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Returns                   : PVRST_SUCCESS/PVRST_FAILURE                */
/*****************************************************************************/
INT4
PvrstMiDeleteAllVlanIndication (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIsPvrstStartedInContext                           */
/*                                                                           */
/* Description        : Called by other modules to know if MSTP is started   */
/*                      or shutdown in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_TRUE - Pvrst is started in the given context     */
/*                      AST_FALSE - Pvrst is NOT started in the given context*/
/*****************************************************************************/
INT4
AstIsPvrstStartedInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return AST_FALSE;
}

/*****************************************************************************/
/* Function Name      : AstIsPvrstEnabledInContext                           */
/*                                                                           */
/* Description        : Called by other modules to know if MSTP is enabled   */
/*                      and running in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : AST_TRUE - Pvrst is started in the given context     */
/*                      AST_FALSE - Pvrst is NOT started in the given context*/
/*****************************************************************************/
INT4
AstIsPvrstEnabledInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return AST_FALSE;
}

/*****************************************************************************/
/* Function Name      : PvrstSetNativeVlan                                   */
/*                                                                           */
/* Description        : This routine is called from L2Iwf to update the      */
/*                      Native VLAN for the port in the PVRST module.        */
/*                                                                           */
/* Input(s)           : u4IfIndex     - Interface Index.                     */
/*                      PrevVlanId    - Previous VLAN Id.                    */
/*                      VlanId        - New VLAN Id.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/ PVRST_FAILURE                         */
/*****************************************************************************/
INT4
PvrstSetNativeVlan (UINT4 u4IfIndex, tVlanId PrevVlanId, tVlanId VlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (PrevVlanId);
    UNUSED_PARAM (VlanId);
    return PVRST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PvrstSetPortType                                     */
/*                                                                           */
/* Description        : It is called in order to set the Port Type either as */
/*                      Access Port belonging to a particular Vlan or         */
/*                      Trunk Port belonging to all Vlans                     */
/*                                                                           */
/* Input(s)           : PortNum- Port no. whose Type is defined                 */
/*                        PortType- It specify the Type(Access,Trunk)to be set */
/*                        VlanId- In case PortType is Access, it specifies to  */
/*                                which Vlan this port is to be associated     */
/*                                otherwise in case of Trunk it is not required*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PVRST_SUCCESS/PVRST_FAILURE                             */
/*****************************************************************************/
INT4
PvrstSetPortType (UINT2 u2PortNum, UINT1 u1PortType, tVlanId VlanId)
{
    UNUSED_PARAM (u2PortNum);
    UNUSED_PARAM (u1PortType);
    UNUSED_PARAM (VlanId);
    return PVRST_SUCCESS;
}
#endif

#ifndef MSTP_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstMiDeleteAllVlans                        */
/*                                                                           */
/*    Description               : Stub provided for backward compatibility   */
/*                                with the customer's MI-unaware protocol    */
/*                                stacks.                                    */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*****************************************************************************/

INT4
MstMiDeleteAllVlans (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstMiVlanDeleteIndication                  */
/*                                                                           */
/*    Description               : Stub provided for backward compatibility   */
/*                                with the customer's MI-unaware protocol    */
/*                                stacks.                                    */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                VlanId  - VlanId which is  deleted.        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*****************************************************************************/
INT4
MstMiVlanDeleteIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return MST_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : MstMiVlanCreateIndication                  */
/*                                                                           */
/*    Description               : Stub provided for backward compatibility   */
/*                                with the customer's MI-unaware protocol    */
/*                                stacks.                                    */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                VlanId  - VlanId which is created          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*****************************************************************************/
INT4
MstMiVlanCreateIndication (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return MST_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AstIsMstStartedInContext                             */
/*                                                                           */
/* Description        : Called by other modules to know if MSTP is started   */
/*                      or shutdown in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Mstp is started in the given context         */
/*                      0 - No, Mstp is NOT started in the given context     */
/*****************************************************************************/
INT4
AstIsMstStartedInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return 0;
}

/*****************************************************************************/
/* Function Name      : AstIsMstEnabledInContext                             */
/*                                                                           */
/* Description        : Called by other modules to know if MSTP is enabled   */
/*                      and running in the given virtual context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1 - Yes Mstp is enabled in the given context         */
/*                      0 - No, Mstp is NOT enabled in the given context     */
/*****************************************************************************/
INT4
AstIsMstEnabledInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return AST_FALSE;
}
#endif

#ifndef ISS_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name             : IssGetContextMacAddress                    */
/*                                                                           */
/*    Description               : Stub provided for backward compatibility   */
/*                                with the customer's MI-unaware protocol    */
/*                                stacks.                                    */
/*                                                                           */
/*    Input(s)                  : u4ContextId - Virtual Switch ID            */
/*                                                                           */
/*    Output(s)                 : SwitchMac  -  The switch MAC address       */
/*                                                                           */
/*    Returns                   : MST_SUCCESS/MST_FAILURE                    */
/*****************************************************************************/
VOID
IssGetContextMacAddress (UINT4 u4ContextId, tMacAddr SwitchMac)
{
    UNUSED_PARAM (u4ContextId);
    MEMSET (SwitchMac, 0, sizeof (tMacAddr));
    return;
}

/*****************************************************************************/
/* Function Name      : IssMirrAddRemovePort                                 */
/*                                                                           */
/* Description        : This function indicates mirroring whenever the       */
/*                      port is added or removed in the portchannel.         */
/*                                                                           */
/* Input(s)           : u4IfIndex - IfIndex of the port whose mirroring      */
/*                                  config to be chacked and applied         */
/*                                  if required                              */
/*                      u2AggId   - PortChannel Id to which the port         */
/*                                  configurations to be done                */
/*                      u1MirrCfg - Mirroring Configuratio                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfAddActivePortToPortChannel                      */
/*                      L2IwfRemoveActivePortToPortChannel                   */
/*****************************************************************************/
INT4
IssMirrAddRemovePort (UINT4 u4IfIndex, UINT4 u4AggId, UINT1 u1MirrCfg)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4AggId);
    UNUSED_PARAM (u1MirrCfg);
    return ISS_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IssNotifyConfiguration                            */
/*  Description     : This function is called from the low level        */
/*                     set routines, to send configuration update event */
/*                     triggers to MSR.                                 */
/*                                                                      */
/*  Input           : SnmpNotifyInfo - Snmp Notification Strucutre      */
/*                    pu1Fmt      - The data type of indices and data.  */
/*                    variable set of inputs depending the on the       */
/*                    indices and the data.                             */
/*  Output          : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IssNotifyConfiguration (tSnmpNotifyInfo SnmpNotifyInfo, CHR1 * pu1Fmt, ...)
{

    UNUSED_PARAM (SnmpNotifyInfo);
    UNUSED_PARAM (pu1Fmt);
    return;
}

/************************************************************************/
/*  Function Name   : IssMsrNotifyConfiguration                         */
/*  Description     : This function is called from the low level        */
/*                     set routines, to send configuration update event */
/*                     triggers to MSR.                                 */
/*                                                                      */
/*  Input           : SnmpNotifyInfo - Snmp Notification Strucutre      */
/*                    pu1Fmt      - The data type of indices and data.  */
/*                    variable set of inputs depending the on the       */
/*                    indices and the data.                             */
/*  Output          : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IssMsrNotifyConfiguration (tSnmpNotifyInfo SnmpNotifyInfo, CHR1 * pu1Fmt, ...)
{

    UNUSED_PARAM (SnmpNotifyInfo);
    UNUSED_PARAM (pu1Fmt);
    return;
}

/*****************************************************************************/
/* Function Name      : IssMirrIsSrcConfigured                               */
/*                                                                           */
/* Description        : This function is called to check if any source       */
/*                      exists with this Id for the given session number     */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number to be traversed */
/*                      u4SrcNum - Source entity Id to be searched in       */
/*                                  session database                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssMirrIsSrcConfigured (UINT4 u4SessionNo, UINT4 u4ContextId, UINT4 u4SrcNum,
                        UINT1 u1MirrorType)
{
    UNUSED_PARAM (u4SessionNo);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4SrcNum);
    UNUSED_PARAM (u1MirrorType);
    return ISS_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : IssMirrIsDestConfigured                              */
/*                                                                           */
/* Description        : This function is called to check if any destination  */
/*                      exists with this Id for the given session number     */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number to be traversed */
/*                      u4DestNum - Destination entity Id to be searched in  */
/*                                  session database                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssMirrIsDestConfigured (UINT4 u4SessionNo, UINT4 u4DestNum)
{

    UNUSED_PARAM (u4SessionNo);
    UNUSED_PARAM (u4DestNum);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrIsSaveComplete                                */
/*                                                                          */
/*    Description        : This function returns whether Mib save is        */
/*                         complete or not                                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE / ISS_FALSE                             */
/****************************************************************************/
tIssBool
MsrIsSaveComplete ()
{
    return ISS_TRUE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrGetSaveStatus                                 */
/*                                                                          */
/*    Description        : This function returns the whether the save       */
/*                          process is ongoing or completed.                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE / ISS_FALSE                             */
/****************************************************************************/
tIssBool
MsrGetSaveStatus (VOID)
{
    return ISS_TRUE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrIsMibRestoreInProgress                        */
/*                                                                          */
/*    Description        : This function gets the MIB Restoration Status.   */
/*                         It checks the variable gi4MibResStatus. If it is */
/*                         MIB_RESTORE_IN_PROGRESS, returns MSR_TRUE. Else  */
/*                         returns False                                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE or MSR_FALSE                            */
/****************************************************************************/

INT1
MsrIsMibRestoreInProgress (VOID)
{
    return MSR_TRUE;
}

INT1
MsrNotifyStpPortDelete (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return MSR_SUCCESS;
}

#endif

#ifndef VCM_WANTED
/*****************************************************************************/
/* Function Name      : VcmGetL2Mode                                         */
/*                                                                           */
/* Description        : This function is used to get the mode (SI / MI)      */
/*                      of the L2 switch                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SI_MODE or VCM_MI_MODE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetL2Mode (VOID)
{
    return VCM_SI_MODE;
}

/*****************************************************************************/
/* Function Name      : VcmGetL2ModeExt                                      */
/*                                                                           */
/* Description        : This function is used to get the mode (SI / MI)      */
/*                      of the L2 switch                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SI_MODE or VCM_MI_MODE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetL2ModeExt (VOID)
{
    /* The logic ported in the function VcmGetL2Mode can be re-used as it is.
     * On need basis, this logic shall be enhanced as follows.
     * -> Mode SI will be returned in the following cases:-
     *    Either MI = NO 
     *    or when MI = YES and No of context = 1.
     * -> Mode MI will be returned in the following case:-
     *    when MI = YES and No of context > 1
     */
    return (VcmGetL2Mode ());
}

/*****************************************************************************/
/* Function Name      : VcmGetSystemModeExt                                  */
/*                                                                           */
/* Description        : This function is used to get the mode (SI / MI)      */
/*                      of the switch for the corresponding protocol.        */
/*                                                                           */
/* Input(s)           : u2ProtocolId   - Protocol Identifier.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SI_MODE or VCM_MI_MODE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetSystemModeExt (UINT2 u2ProtocolId)
{
    /* The logic ported in the function VcmGetL2Mode can be re-used as it is.
     * On need basis, this logic shall be enhanced as follows.
     * -> Mode SI will be returned in the following cases:-
     *    Either MI = NO 
     *    or when MI = YES and No of context = 1.
     * -> Mode MI will be returned in the following case:-
     *    when MI = YES and No of context > 1
     */
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : VcmSispGetPhysicalPortOfSispPort                     */
/*                                                                           */
/* Description        : This function will provide the physical IfIndex      */
/*                      of the input Logical IfIndex.                        */
/*                                                                           */
/* Input(s)           : u4ILogicalfIndex - Logical Port identifier.          */
/*                                                                           */
/* Output(s)          : pu4PhysicalIndex - Physical Index of the logical port*/
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispGetPhysicalPortOfSispPort (UINT4 u4LogicalIfIndex,
                                  UINT4 *pu4PhysicalIndex)
{
    UNUSED_PARAM (u4LogicalIfIndex);
    UNUSED_PARAM (pu4PhysicalIndex);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispGetSispPortsInfoOfPhysicalPort                */
/*                                                                           */
/* Description        : This function used to get the SISP port information  */
/*                      of a particual physical port. This API will return   */
/*                      the logical ports in IfIndex or LocalPort based on   */
/*                      the u1RetLocalPorts variable. paSispPort array will  */
/*                      be typecasted to UINT4 if the return value is to be  */
/*                      in IfIndex; will be typecasted to UINT2, if the      */
/*                      return value is to be in LocalPort.                  */
/*                                                                           */
/*                      This API can be used to get the number of SISP ports */
/*                      existing for the particular physical port. When      */
/*                      paSispPorts is NULL, SISP port count alone will be   */
/*                      returned.                                            */
/*                                                                           */
/*                      This API will fill up the PortList continously for   */
/*                      number of Sisp Ports in IfIndex based retrieval. For */
/*                      LocalPort based retrieval, bit position corresponding*/
/*                      to context will be set.                              */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex   - Physical or port channel port Id.   */
/*                      u1RetLocalPorts- Flag to tell if the output is needed*/
/*                                       in LocalPorts.                      */
/*                                       VCM_TRUE  - Local Port format       */
/*                                       VCM_FALSE - IfIndex format          */
/*                                                                           */
/* Output(s)          : paSispPorts    - Array of Sisp Ports of the Phy port */
/*                      pu4PortCount   - Number of SISP Port count           */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispGetSispPortsInfoOfPhysicalPort (UINT4 u4PhyIfIndex,
                                       UINT1 u1RetLocalPorts,
                                       VOID *paSispPorts, UINT4 *pu4PortCount)
{
    UNUSED_PARAM (u4PhyIfIndex);
    UNUSED_PARAM (u1RetLocalPorts);
    UNUSED_PARAM (paSispPorts);
    *pu4PortCount = 0;
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispGetContextInfoForPortVlan                     */
/*                                                                           */
/* Description        : This function used to get the Primary/Secondary ctx  */
/*                      in which the particular VLAN tagged packet has to be */
/*                      processed.                                           */
/*                                                                           */
/*                      This function will return the secondary context,     */
/*                      SISP port and its local port, if the SISP port is    */
/*                      member of the particular VLAN in the secondary ctxt. */
/*                                                                           */
/*                      This function will return the primary context,       */
/*                      input physical port and its local port, if the port  */
/*                      is member of the particular VLAN in the primary ctxt.*/
/*                                                                           */
/*                      This function should be called, only when SISP is    */
/*                      enabled on the particular physical or port channel   */
/*                      port.                                                */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex - Physical Port Interface Index         */
/*                      VlanId - Vlan Identifier in the ingress packet       */
/*                                                                           */
/* Output(s)          : *pu4LogicalIfIndex - Physical/Logical Port Identifier*/
/*                      *pu4ContextId   - Context Identifier                 */
/*                      *pu2LocalPortId - Local Port identifier in the given */
/*                                        context.                           */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispGetContextInfoForPortVlan (UINT4 u4PhyIfIndex, tVlanId VlanId,
                                  UINT4 *pu4ContextId, UINT4 *pu4LogicalIfIndex,
                                  UINT2 *pu2LocalPortId)
{

    UNUSED_PARAM (u4PhyIfIndex);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pu4ContextId);
    UNUSED_PARAM (pu4LogicalIfIndex);
    UNUSED_PARAM (pu2LocalPortId);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispIsSispPortPresentInCtxt                       */
/*                                                                           */
/* Description        : This API will return TRUE when there is any SISP     */
/*                      enabled physical port or any logical port is present */
/*                      in the context.                                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : VCM_TRUE / VCM_FALSE                                 */
/*****************************************************************************/
INT1
VcmSispIsSispPortPresentInCtxt (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return VCM_FALSE;
}

/*****************************************************************************/
/* Function name      : L2IwfGetSispPortCtrlStatus                           */
/*                                                                           */
/* Description        : This API will be used by Layer 2 modules to get      */
/*                      SISP enabled / disabled on a physical or port channel*/
/*                      port.                                                */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Index of the port to be created.       */
/*                      *pu1Status  - SISP enable/disable status             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfGetSispPortCtrlStatus (UINT4 u4IfIndex, UINT1 *pu1Status)
{
    UNUSED_PARAM (u4IfIndex);
    *pu1Status = L2IWF_DISABLED;
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmGetSystemMode                                     */
/*                                                                           */
/* Description        : This function is used to get the mode (SI / MI)      */
/*                      of the switch for the corresponding protocol.        */
/*                                                                           */
/* Input(s)           : u2ProtocolId   - Protocol Identifier.                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SI_MODE or VCM_MI_MODE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetSystemMode (UINT2 u2ProtocolId)
{
    UNUSED_PARAM (u2ProtocolId);
    return VCM_SI_MODE;
}

/*****************************************************************************/
/* Function Name      : VcmGetContextInfoFromIfIndex                         */
/*                                                                           */
/* Description        : This function is used to get the Context-Id and      */
/*                      the LocalPort-Id for the given IfIndex.              */
/*                      It is stubbed here and is active only in the multiple*/
/*                      instance mode of operation.                          */
/*                                                                           */
/* Input(s)           : u4IfIndex      - Interface Index                     */
/*                                                                           */
/* Output(s)          : pu4ContextId   - Corresponding Context-Id            */
/*                      pu2LocalPortId - Corresponding Local Port Id         */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                              UINT2 *pu2LocalPortId)
{
    *pu4ContextId = L2IWF_DEFAULT_CONTEXT;
    *pu2LocalPortId = (UINT2) u4IfIndex;
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmGetFirstActiveContext                             */
/*                                                                           */
/* Description        : This function is used to get the first               */
/*                      active context present in the system.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4ContextId - First Active Context.                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetFirstActiveContext (UINT4 *pu4ContextId)
{
    *pu4ContextId = L2IWF_DEFAULT_CONTEXT;
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmGetNextActiveContext                              */
/*                                                                           */
/* Description        : This function is used to get the next                */
/*                      active context present in the system.                */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                                                                           */
/* Output(s)          : pu4NextContextId - Next Active Context.              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetNextActiveContext (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu4NextContextId);
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetAliasName                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will return the alias name of the    */
/*                        given context.                                     */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu1Alias       - Switch Alias Name.                */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu1Alias);
    return VCM_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmCfaCreateIfaceMapping                           */
/*                                                                           */
/*     DESCRIPTION      : This function will create the interface mapping.   */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                        u4IfIndex   - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
VcmCfaCreateIfaceMapping (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    return;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmMapPortToContextInHw                            */
/*                                                                           */
/*     DESCRIPTION      : This function will map the given port to the       */
/*                        given context in the hardware                      */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        u4IfIndex      - Port Identifier                   */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
VcmMapPortToContextInHw (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmUnMapPortFromContextInHw                        */
/*                                                                           */
/*     DESCRIPTION      : This function will unmap the given port from the   */
/*                        given context in the hardware                      */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        u4IfIndex      - Port Identifier                   */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
VcmUnMapPortFromContextInHw (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);

    return VCM_SUCCESS;
}
#endif /* NPAPI */
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetContextIdFromCfaIfIndex                      */
/*                                                                           */
/*     DESCRIPTION      : This functions gets the context id associated      */
/*                        with the given cfa ifIndex                         */
/*                                                                           */
/*     INPUT            : u4CfaIfIndex  - Cfa interface index                */
/*                                                                           */
/*     OUTPUT           : *pu4CxtId - Context id of the interface            */
/*                                    having cfa IfIndex as u4CfaIfIndex     */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS    - If interface is found and the     */
/*                                        context is associated              */
/*                                        with that interface is returned    */
/*                        VCM_FAILURE    - Otherwise                         */
/*                                                                           */
/*****************************************************************************/

INT4
VcmGetContextIdFromCfaIfIndex (UINT4 u4CfaIfIndex, UINT4 *pu4CxtId)
{
    UNUSED_PARAM (u4CfaIfIndex);
    UNUSED_PARAM (pu4CxtId);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmGetContextPortList                                */
/*                                                                           */
/* Description        : This function is used to get the list of interfaces  */
/*                      mapped to the given context.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context - Id                           */
/*                                                                           */
/* Output(s)          : PortList    - PortList for the given context         */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS or VCM_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetContextPortList (UINT4 u4ContextId, tPortList PortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (PortList);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispIsPortVlanMappingValid                        */
/* Description        : This API will used to verify whether the Port-VLAN-  */
/*                      Context Mapping Table configuration is Valid or not. */
/*                      The Local ports will be converted to the underlying  */
/*                      physical or port channel interface before verifying  */
/*                      the entry in the table.                              */
/* Input(s)           : u4ContextId    - ContextId                           */
/*                      tVlanId        - VlanId,                             */
/*                      tLocalPortList - AddedPorts                          */
/* Output(s)          : None                                                 */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispIsPortVlanMappingValid (UINT4 u4ContextId, tVlanId VlanId,
                               tLocalPortList AddedPorts)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddedPorts);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispUpdatePortVlanMapping                         */

/* Description        : This API will update the Port-VLAN-Context Mapping   */
/*                      table with the input parameters. The Local ports     */
/*                      will be converted to the underlying physical or port */
/*                      channel interface before updating the entry in the   */
/*                      table.                                               */
/* Input(s)           :  u4ContextId    - ContextId                          */
/*                       tVlanId        - VlanId,                            */
/*                       AddedPorts     - List of newly added ports          */
/*                       DeletedPorts   - List of deleted ports              */
/* Output(s)          : None                                                 */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispUpdatePortVlanMapping (UINT4 u4ContextId, tVlanId VlanId,
                              tLocalPortList AddedPorts,
                              tLocalPortList DeletedPorts)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (AddedPorts);
    UNUSED_PARAM (DeletedPorts);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsVcExist                                       */
/*     DESCRIPTION      : This function will return, whether any context     */
/*                        with the given Context ID exists or not. The       */
/*                        context can be either an L2 context, an L3 context */
/*                        or an L2_L3 context.                               */
/*     INPUT            : u4ContextId - Context-Id                           */
/*     OUTPUT           : None                                               */
/*     RETURNS          : VCM_TRUE / VCM_FALSE                               */
/*                                                                           */
 /*****************************************************************************/
INT4
VcmIsVcExist (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return VCM_TRUE;
}

/*****************************************************************************/
/* Function Name      : VcmSispUpdatePortVlanMappingOnPort                   */
/* Description        : This API will update the Port-VLAN-Context Mapping   */
/*                      table for the given port.                            */
/* Input(s)           :  u4ContextId    - ContextId                          */
/*                       tVlanId        - VlanId,                            */
/*                       u4IfIndex      - Physical Port Identifier           */
/*                       u1Status       - Add or Delete flag                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispUpdatePortVlanMappingOnPort (UINT4 u4ContextId, tVlanId VlanId,
                                    UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispDeletePortVlanMapEntriesForPort               */
/* Description        : This API will delete all the Port-VLAN-Context Map   */
/*                      table for the particular Physical Port or logical    */
/*                      port.                                                */
/*                                                                           */
/* Input(s)           :  u4IfIndex      - Physical Port Identifier           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
 /*****************************************************************************/
INT4
VcmSispDeletePortVlanMapEntriesForPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispUpdatePortVlanMappingInHw                     */
/*                                                                           */
/* Description        : This API will update the Port-VLAN-Context Mapping   */
/*                      table with the input parameters. The Local ports     */
/*                      will be converted to the underlying physical or port */
/*                      channel interface before updating the entry in the   */
/*                      table.                                               */
/*                                                                           */
/* Input(s)           :  u4ContextId    - ContextId                          */
/*                       tVlanId        - VlanId,                            */
/*                       PortList       - List of local ports                */
/*                       u1Action       - ADD or DEL in Hardware             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispUpdatePortVlanMappingInHw (UINT4 u4ContextId, tVlanId VlanId,
                                  tLocalPortList PortList, UINT1 u1Action)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (PortList);
    UNUSED_PARAM (u1Action);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispGetSispPortOfPhysicalPortInCtx                */
/*                                                                           */
/* Description        : This function used to get the SISP interface index   */
/*                      and local port information from the  PhyIfIndex and  */
/*                      context ID.                                          */
/*                                                                           */
/* Input(s)           : u4PhyIfIndex   - Physical or port channel port Id.   */
/*                      u4ContextId    - Secondary context ID.               */
/*                                                                           */
/* Output(s)          : pu4SispPort    - Sisp IfIndex                        */
/*                      pu2LocalPort   - Local port of SispPort              */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispGetSispPortOfPhysicalPortInCtx (UINT4 u4PhyIfIndex, UINT4 u4ContextId,
                                       UINT4 *pu4SispPort, UINT2 *pu2LocalPort)
{
    UNUSED_PARAM (u4PhyIfIndex);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu4SispPort);
    UNUSED_PARAM (pu2LocalPort);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsL2VcExist                                     */
/*                                                                           */
/*     DESCRIPTION      : This function will return true if the given        */
/*                        context ID is an L2 or an L2_L3 context.  This     */
/*                        function will return false otherwise.              */
/*                                                                           */
/*     INPUT            : u4ContextId - Context-Id                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VCM_TRUE  - If the given context exists, and the   */
/*                                    context type is of either L2 or L2_L3  */
/*                        VCM_FALSE - (*) If the given context does not      */
/*                                        exists                             */
/*                                    (*) Context exists, but if its of type */
/*                                        L3                                 */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsL2VcExist (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return VCM_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetVcNextFreeLocalPortIdExt                     */
/*                                                                           */
/*     DESCRIPTION      : This function will return the Next free local      */
/*                        port Id in the context. It is invoked from VLAN    */
/*                        module                                             */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                                                                           */
/*     OUTPUT           : pu2LocalPortId - Next free local port Id.          */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetVcNextFreeHlPortIdExt (UINT4 u4ContextId, UINT2 *pu2LocalPortId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu2LocalPortId);
    return VCM_SUCCESS;
}

#endif /* VCM_WANTED */

#ifndef VCM_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsSwitchExist                                   */
/*                                                                           */
/*     DESCRIPTION      : This function check whether the entry is present   */
/*                        for the correponding Switch-name. if yes it will   */
/*                        return the Context-Id of the Switch.               */
/*                                                                           */
/*     INPUT            : pu1Alias   - Name of the Switch.                   */
/*                                                                           */
/*     OUTPUT           : pu4VcNum   - Context-Id of the switch              */
/*                                                                           */
/*     RETURNS          : VCM_TRUE / VCM_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
VcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    UNUSED_PARAM (pu1Alias);
    UNUSED_PARAM (pu4VcNum);
    return VCM_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmUnMapPortFromContext                            */
/*                                                                           */
/*     DESCRIPTION      : This function will unmap the given port from the   */
/*                        given context                                      */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Port Identifier                   */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmUnMapPortFromContext (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIfIndexFromLocalPort                         */
/*                                                                           */
/*     DESCRIPTION      : This function will return the Interface Index      */
/*                        from the given Localport and Context-Id.           */
/*                                                                           */
/*     INPUT            : u4ContextId    - Context-Id.                       */
/*                        u2LocalPort    - LocalPort.                        */
/*                                                                           */
/*     OUTPUT           : pu4IfIndex     - Interface Index.                  */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
VcmGetIfIndexFromLocalPort (UINT4 u4ContextId, UINT2 u2LocalPort,
                            UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (u4ContextId);

    *pu4IfIndex = (UINT4) u2LocalPort;
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetL2CxtIdForIpIface                            */
/*                                                                           */
/*     DESCRIPTION      : This functions gets the l2 context to which the    */
/*                        specified interace is mapped.                      */
/*                                                                           */
/*     INPUT            : u4IfIndex   - Cfa interface index                  */
/*                                                                           */
/*     OUTPUT           : *pu4L2CxtId - L2 context to which the interface    */
/*                                      is mapped.                           */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS    - If interface is found and the     */
/*                                        context is associated              */
/*                                        with that interface is returned    */
/*                        VCM_FAILURE    - Otherwise                         */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetL2CxtIdForIpIface (UINT4 u4IfIndex, UINT4 *pu4L2CxtId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4L2CxtId);

    return VCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VcmSispPortAddedToPortChannel                        */
/*                                                                           */
/* Description        : This API will destroy all the secondary context      */
/*                      mappings for the specified physical interface. This  */
/*                      should be invoked when a SISP enabled physical port  */
/*                      is moved to port channel. This API simply disables   */
/*                      the SISP functionality over the provided interface.  */
/*                                                                           */
/* Input(s)           : u4IfIndex      - Interface Index.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS / VCM_FAILURE                            */
/*****************************************************************************/
INT4
VcmSispPortAddedToPortChannel (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return VCM_SUCCESS;
}

#endif
#ifndef GARP_WANTED
/***************************************************************/
/*  Function Name   : GarpStapPortStateChange                  */
/*  Description     : This function changes the state of the   */
/*                    stap for the given port                  */
/*  Input(s)        : u2port - Port number whose state as      */
/*                   changed                                   */
/*                    u1State - New state of the port          */
/*                    u2GipId - Group Information propagation ID*/
/*  Output(s)       : State of the port gets updated           */
/*  Global Variables Referred : None                           */
/*  Global variables Modified : None                           */
/*  Exceptions or Operating System Error Handling : None       */
/*  Use of Recursion : None                                    */
/*  Returns         : None                                     */
/***************************************************************/
void
GarpStapPortStateChange (UINT2 u2Port, UINT1 u1State, UINT2 u2GipId)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u1State);
    UNUSED_PARAM (u2GipId);
    return;
}

/****************************************************************************
 Function    :  nmhGetDot1qPortGvrpStatus
 Input       :  The Indices
                Dot1dBasePort

                The Object
                retValDot1qPortGvrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1qPortGvrpStatus (INT4 i4Dot1dBasePort,
                           INT4 *pi4RetValDot1qPortGvrpStatus)
{
    UNUSED_PARAM (i4Dot1dBasePort);
    UNUSED_PARAM (pi4RetValDot1qPortGvrpStatus);

    return SNMP_SUCCESS;
}

#endif
#ifndef LCM_WANTED

tLcmPromptInfo      LcmPromptInfo;

/*****************************************************************************/
/* Function Name      : LcmGetNextEvcStatusInfo                              */
/*                                                                           */
/* Description        : This routines fetches the status information for     */
/*                      at max 10 evcs at a time.                            */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - Refernce ID of the EVC after which the  */
/*                          status of the EVCs are to be fetched             */
/*                      pLcmEvcStatusInfo - EVC status info structure to be  */
/*                          filled with the status info                      */
/*                      pu1NoEvcInfo - No of EVCs whose status got fetched   */
/*                                                                           */
/* Output(s)          : pLcmEvcStatusInfo - EVC status info structure        */
/*                      pu1NoEvcInfo - No of EVCs whose status got fetched   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmGetNextEvcStatusInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                         tLcmEvcStatusInfo * pLcmEvcStatusInfo,
                         UINT1 *pu1NoEvcInfo)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (u2EvcRefId);
    UNUSED_PARAM (pLcmEvcStatusInfo);
    UNUSED_PARAM (pu1NoEvcInfo);
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmGetNextEvcCeVlanInfo                              */
/*                                                                           */
/* Description        : This routines fetches CE vlan info for maximum of 10 */
/*                           EVCs                                            */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - Refernce ID of the EVC after which      */
/*                          CEVLAN info has to be fetched                    */
/*                      pLcmEvcCeVlanInfo - CE VLAN info structure to be     */
/*                          filled with the info                             */
/*                      pu1NoEvcInfo - No of EVCs whose CE Vlan info is      */
/*                          fetched                                          */
/*                                                                           */
/* Output(s)          : pLcmEvcCeVlanInfo- CE Vlan info structure            */
/*                      pu1NoEvcInfo - No of EVCs whose CE Vlan info is      */
/*                          fetched                                          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmGetNextEvcCeVlanInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                         tLcmEvcCeVlanInfo * pLcmEvcCeVlanInfo,
                         UINT1 *pu1NoEvcInfo)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (u2EvcRefId);
    UNUSED_PARAM (pLcmEvcCeVlanInfo);
    UNUSED_PARAM (pu1NoEvcInfo);
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmEvcStatusChange                                   */
/*                                                                           */
/* Description        : This routine changes the status of an configured EVC */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC Id                                    */
/*                      u1Status - Changed Status of the EVC                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmEvcStatusChange (UINT1 *pu1EvcId, UINT1 u1Status)
{
    UNUSED_PARAM (pu1EvcId);
    UNUSED_PARAM (u1Status);
    return;
}

/*****************************************************************************/
/* Function Name      : LcmElmiOperStatusIndication                          */
/*                                                                           */
/* Description        : This routine receives indication of  any change in   */
/*                      the Operational Status of ELMI                       */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u1ElmOperStatus - Operational Status of ELMI         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmElmiOperStatusIndication (UINT2 u2PortNo, UINT1 u1ElmOperStatus)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (u1ElmOperStatus);
    return;
}

/*****************************************************************************/
/* Function Name      : LcmPassCeVlanInfo                                    */
/*                                                                           */
/* Description        : This routine is used to pass the CVLan information   */
/*                      received from the ELMI UNI-C                         */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pElmCeVlanInfom - CE Vlan Info structure             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmPassCeVlanInfo (UINT2 u2PortNo, tLcmEvcCeVlanInfo * pElmCeVlanInfo)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (pElmCeVlanInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : LcmGetEvcInfo                                        */
/*                                                                           */
/* Description        : This routine is used to get information about single */
/*                      EVC                                                  */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - EVC refernce ID                   */
/*                      pLcmLocalEvcInfo - EVC information structure
 *                       *                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmGetEvcInfo (UINT2 u2PortNo, UINT2 u2EvcReferenceId,
               tLcmEvcInfo * pLcmEvcInfo)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (u2EvcReferenceId);
    UNUSED_PARAM (pLcmEvcInfo);
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmNextGetEvcInfo                                    */
/*                                                                           */
/* Description        : This routine is used to get information about single */
/*                      EVC, next to the passed EVC                          */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - EVC refernce ID                   */
/*                      pLcmLocalEvcInfo - EVC information structure         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS/LCM_FAILURE                              */
/*****************************************************************************/
INT4
LcmGetNextEvcInfo (UINT2 u2PortNo, UINT2 u2EvcReferenceId,
                   tLcmEvcInfo * pLcmEvcInfo)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (u2EvcReferenceId);
    UNUSED_PARAM (pLcmEvcInfo);
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmEvcStatusChangeIndication                         */
/*                                                                           */
/* Description        : This routine is called whenever status of an EVC     */
/*                      changes                                              */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - EVC refernce ID                   */
/*                      u1EvcStatus - Changed Status of the EVC              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmEvcStatusChangeIndication (UINT2 u2PortNo, UINT2 u2EvcReferenceId,
                              UINT1 u1EvcStatus)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (u2EvcReferenceId);
    UNUSED_PARAM (u1EvcStatus);
    return;
}

/*****************************************************************************/
/* Function Name      : LcmCreateNewEvcIndication                            */
/*                                                                           */
/* Description        : This routine is called whenever an EVC is created    */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pLcmEvcStatusInfo - EVC status information structure */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmCreateNewEvcIndication (UINT2 u2PortNo,
                           tLcmEvcStatusInfo * pLcmEvcStatusInfo)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (pLcmEvcStatusInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : LcmDeleteEvcIndication                               */
/*                                                                           */
/* Description        : This routine is called when an EVC is to be deleted  */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcReferenceId - Ref Id of the EVC to be deleted   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmDeleteEvcIndication (UINT2 u2PortNo, UINT2 u2EvcReferenceId)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (u2EvcReferenceId);
    return;
}

/*****************************************************************************/
/* Function Name      : LcmDatabaseUpdateIndication                          */
/*                                                                           */
/* Description        : This routine is used to indicate any change in the   */
/*                      LCM database                                         */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pLcmEvcStatusInfo - EVC status information structure */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
LcmDatabaseUpdateIndication (UINT2 u2PortNo,
                             tLcmEvcStatusInfo * pLcmEvcStatusInfo)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (pLcmEvcStatusInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : LcmLock                                             */
/*                                                                           */
/* Description        : This function is used to take the LCM mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS or LCM_FAILURE                           */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP,                                               */
/*****************************************************************************/
INT4
LcmLock ()
{
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the LCM mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS or LCM_FAILURE                           */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP                                                */
/*****************************************************************************/
INT4
LcmUnLock ()
{
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmGetEvcCeVlanInfo                                  */
/*                                                                           */
/* Description        : This routines fetches CE vlan info for the given EVC */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - Refernce ID of the EVC whose CEVLAN info*/
/*                          has to be fetched                                */
/*                      pLcmEvcCeVlanInfo - CE VLAN info structure to be     */
/*                          filled with the info                             */
/*                                                                           */
/* Output(s)          : pLcmEvcCeVlanInfo- CE Vlan info structure            */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmGetEvcCeVlanInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                     tLcmEvcCeVlanInfo * pLcmEvcCeVlanInfo)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (u2EvcRefId);
    UNUSED_PARAM (pLcmEvcCeVlanInfo);

    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmGetEvcStatusInfo                                  */
/*                                                                           */
/* Description        : This routines fetches the status information for EVC */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      u2EvcRefId - Refernce ID of the EVC whose status has */
/*                          to be fetched                                    */
/*                      pLcmEvcStatusInfo - EVC status info structure to be  */
/*                          filled with the status info                      */
/*                                                                           */
/* Output(s)          : pLcmEvcStatusInfo - EVC status info structure        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmGetEvcStatusInfo (UINT2 u2PortNo, UINT2 u2EvcRefId,
                     tLcmEvcStatusInfo * pLcmEvcStatusInfo)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (u2EvcRefId);
    UNUSED_PARAM (pLcmEvcStatusInfo);

    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmFindEvcId                                         */
/*                                                                           */
/* Description        : This routines searches for the given EVC             */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC to be searched                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmFindEvcId (UINT1 *pu1EvcId)
{
    UNUSED_PARAM (pu1EvcId);

    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmCreateEvc                                         */
/*                                                                           */
/* Description        : This routines creates an EVC                         */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC to be created                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmCreateEvc (UINT1 *pu1EvcId)
{
    UNUSED_PARAM (pu1EvcId);

    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmSetDefaultEvc                                     */
/*                                                                           */
/* Description        : This routine sets an EVC as default for all the Vlans*/
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC Id                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
VOID
LcmSetDefaultEvc (UINT1 *pu1EvcId)
{
    UNUSED_PARAM (pu1EvcId);
    return;
}

/*****************************************************************************/
/* Function Name      : LcmUpdateCVlanEvcMap                                 */
/*                                                                           */
/* Description        : This routine updates the CVLAN map for an EVC        */
/*                                                                           */
/* Input(s)           : u2PortNo - Port No                                   */
/*                      pu1EvcId - EVC Id                                    */
/*                      u4Vlan - Vlan ID                                     */
/*                      u4CommandType - Type of command: To map or unmap     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmUpdateCVlanEvcMap (UINT2 u2PortNo, UINT1 *pu1EvcId,
                      UINT4 u4Vlan, UINT4 u4CommandType)
{

    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (pu1EvcId);
    UNUSED_PARAM (u4Vlan);
    UNUSED_PARAM (u4CommandType);
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmSetEvcBwProfile                                   */
/*                                                                           */
/* Description        : This routine updates the Bandwidth Profile of an EVC */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC Id                                    */
/*                      u4Cm - Coloring Mode                                 */
/*                      u4Cf - Coupling Flag                                 */
/*                      u4PerCosBit - Per Cos Bit                            */
/*                      u4CirMagnitude - CIR Magnitude                       */
/*                      u4CirMultiplier - CIR Multiplier                     */
/*                      u4CbsMagnitude - CBS Magnitude                       */
/*                      u4CbsMultiplier - CBS Multiplier                     */
/*                      u4EirMagnitude - EIR Magnitude                       */
/*                      u4EirMultiplier - EIR Multiplier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmSetEvcBwProfile (UINT1 *pu1EvcId, UINT4 u4Cm, UINT4 u4Cf, UINT4 u4PerCosBit,
                    UINT4 u4CirMagnitude, UINT4 u4CirMultiplier,
                    UINT4 u4CbsMagnitude, UINT4 u4CbsMultiplier,
                    UINT4 u4EirMagnitude, UINT4 u4EirMultiplier,
                    UINT4 u4EbsMagnitude, UINT4 u4EbsMultiplier,
                    UINT4 u4PriorityBits)
{
    UNUSED_PARAM (pu1EvcId);
    UNUSED_PARAM (u4Cm);
    UNUSED_PARAM (u4Cf);
    UNUSED_PARAM (u4PerCosBit);
    UNUSED_PARAM (u4CirMagnitude);
    UNUSED_PARAM (u4CirMultiplier);
    UNUSED_PARAM (u4CbsMagnitude);
    UNUSED_PARAM (u4CbsMultiplier);
    UNUSED_PARAM (u4EirMagnitude);
    UNUSED_PARAM (u4EirMultiplier);
    UNUSED_PARAM (u4EbsMagnitude);
    UNUSED_PARAM (u4EbsMultiplier);
    UNUSED_PARAM (u4PriorityBits);
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : LcmDeleteEvc                                         */
/*                                                                           */
/* Description        : This routine deletes the EVC                         */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC to be deleted                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
VOID
LcmDeleteEvc (UINT1 *pu1EvcId)
{
    UNUSED_PARAM (pu1EvcId);
    return;
}

/*****************************************************************************/
/* Function Name      : LcmUpdateEvcUniCount                                 */
/*                                                                           */
/* Description        : This routine updates the UNI count for the EVC       */
/*                                                                           */
/* Input(s)           : pu1EvcId -EVC identifier whose count is to be updated*/
/*                      u2UniCount - No of attached UNIs                     */
/*                      u4UniType - UNI type                                 */
/*                                                                           */
/* Output(s)          : pLcmEvcStatusInfo - EVC status info structure        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LCM_SUCCESS / LCM_FAILURE                            */
/*****************************************************************************/
INT4
LcmUpdateEvcUniCount (UINT1 *pu1EvcId, UINT2 u2UniCount, UINT4 u4UniType)
{
    UNUSED_PARAM (pu1EvcId);
    UNUSED_PARAM (u2UniCount);
    UNUSED_PARAM (u4UniType);
    return LCM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfmCreateMA                                          */
/*                                                                           */
/* Description        : This routines creates a Maintenance Association      */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC identifier                            */
/*                      u2SVlan - SVLAN for the EVC                          */
/*                      pu1DomainName - Domain in which MA needs to be       */
/*                       created                                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfmCreateMA (UINT1 *pu1EvcId, UINT2 u2SVlan, UINT1 *pu1DomainName)
{
    UNUSED_PARAM (pu1EvcId);
    UNUSED_PARAM (u2SVlan);
    UNUSED_PARAM (pu1DomainName);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfmCreateMeps                                        */
/*                                                                           */
/* Description        : This routines creates the desired no of MEPs         */
/*                                                                           */
/* Input(s)           : pu1EvcId - EVC identifier                            */
/*                      u2NumberOfMeps - No of MEPs to be created            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfmCreateMeps (UINT1 *pu1EvcId, UINT2 u2NumberOfMeps)
{
    UNUSED_PARAM (pu1EvcId);
    UNUSED_PARAM (u2NumberOfMeps);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfmMepDown                                           */
/*                                                                           */
/* Description        : This routines is called when MEP goes down           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfmMepDown (VOID)
{
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfmFindDomain                                        */
/*                                                                           */
/* Description        : This routines searches for the given domain          */
/*                                                                           */
/* Input(s)           : pu1DomainName - Domain name to be searched           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
CfmFindDomain (UINT1 *pu1DomainName)
{
    UNUSED_PARAM (pu1DomainName);
    return CLI_SUCCESS;
}
#endif

#ifndef ELMI_WANTED
/*****************************************************************************/
/* Function Name      : ElmGetElmiPortMode                                   */
/*                                                                           */
/* Description        : This function is used to get Elmi Mode on the port   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_CUSTOMER_SIDE or ELM_PROVIDER_SIDE               */
/*                                                                           */
/* Called By          : External Modules                                     */
/*****************************************************************************/
INT4
ElmGetPortMode (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmEvcInformationChangeIndication                    */
/*                                                                           */
/* Description        : This routine is called by Virtual Connection manager */
/*                      to indicate any change in status of an EVC to E-LMI  */
/*                      UNI-N                                                */
/*                                                                           */
/* Input(s)           : EVC Reference Id                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmEvcInformationChangeIndication (UINT2 u22fIndex)
{
    UNUSED_PARAM (u22fIndex);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmEvcStatusChangeIndication                         */
/*                                                                           */
/* Description        : This routine is called by Virtual Connection manager */
/*                      to indicate any change in status of an EVC to E-LMI  */
/*                      UNI-N                                                */
/*                                                                           */
/* Input(s)           : EVC Reference Id                                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmEvcStatusChangeIndication (UINT2 u2IfIndex, UINT2 u2EvcRefId, UINT1 u1Status)
{
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (u2EvcRefId);
    UNUSED_PARAM (u1Status);
    return ELM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : ElmUniInformationChangeIndication                    */
/*                                                                           */
/* Description        : This routine is called by CFA to indicate change in  */
/*                      UNI Information database                             */
/*                                                                           */
/* Input(s)           : Port Number                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ELM_SUCCESS / ELM_FAILURE                            */
/*****************************************************************************/
INT4
ElmUniInformationChangeIndication (UINT2 u2IfIndex)
{

    UNUSED_PARAM (u2IfIndex);
    return ELM_SUCCESS;
}

#endif

#ifndef EVCPRO_WANTED
INT4
CfaGetUniInfo (UINT2 u2PortNo, tCfaUniInfo * pCfaUniInfo)
{
    UNUSED_PARAM (u2PortNo);
    UNUSED_PARAM (pCfaUniInfo);
    return CFA_FAILURE;
}

VOID
CfaUpdateUniInfo (tCfaUniInfo CfaUniInfo)
{
    UNUSED_PARAM (CfaUniInfo);
    return;
}
#endif

#ifndef TAC_WANTED
/*****************************************************************************/
/* Function Name      : TacApiFilterChannel                                  */
/*                                                                           */
/* Description        : This function scans the filter database and checks   */
/*                      whether the channeh (S,G) needs to be filterd or not */
/*                      If the profile action is permit, then the matching   */
/*                      entry should be found in the database to be allowed  */
/*                      If the profile action is deny, then the matching     */
/*                      entry should not be found in the database to be      */
/*                      allowed                                              */
/*                                                                           */
/* Input(s)           : pTacFilterPkt    - pointer to tTacFilterPkt          */
/*                      pu1SrcList       - Pointer to list of sources        */
/*                                                                           */
/* Output(s)          : pu1SrcList       - Pointer to the list of sources    */
/*                                                                           */
/* Return Value(s)    : TACM_PERMIT/TACM_DENY                                */
/*****************************************************************************/
INT4
TacApiFilterChannel (tTacFilterPkt * pTacFilterPkt, UINT1 *pu1SrcList)
{
    UNUSED_PARAM (pTacFilterPkt);
    UNUSED_PARAM (pu1SrcList);
    return TACM_PERMIT;
}

/*****************************************************************************/
/* Function Name      : TacApiUpdatePortRefCount                             */
/*                                                                           */
/* Description        : This function updates the port reference count for   */
/*                      the particular profile. A profile can be deleted     */
/*                      only if the port reference count is 0                */
/*                                                                           */
/* Input(s)           : u4ProfileId      - Profile ID                        */
/*                      u1AddressType    - Internet address type             */
/*                      u1UpdtFlag       - TACM_INTF_MAP/TACM_INTF_UNMAP     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacApiUpdatePortRefCount (UINT4 u4ProfileId, UINT1 u1AddressType,
                          UINT1 u1UpdtFlag)
{
    UNUSED_PARAM (u4ProfileId);
    UNUSED_PARAM (u1AddressType);
    UNUSED_PARAM (u1UpdtFlag);
    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacApiUpdateVlanRefCount                             */
/*                                                                           */
/* Description        : This function updates the VLAN reference count for   */
/*                      the particular profile. A profile can be deleted     */
/*                      only if the VLAN reference count is 0                */
/*                                                                           */
/* Input(s)           : u4Instance       - Context ID                        */
/*                      u4ProfileId      - Profile ID                        */
/*                      u1AddressType    - Internet address type             */
/*                      u1UpdtFlag       - TACM_INTF_MAP/TACM_INTF_UNMAP     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacApiUpdateVlanRefCount (UINT4 u4Instance, UINT4 u4ProfileId,
                          UINT1 u1AddressType, UINT1 u1UpdtFlag)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (u4ProfileId);
    UNUSED_PARAM (u1AddressType);
    UNUSED_PARAM (u1UpdtFlag);
    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacApiGetPrfStats                                    */
/*                                                                           */
/* Description        : This function gets the reference count for this      */
/*                      profile. Returns reference counts of both port       */
/*                      and VLAN.                                            */
/*                                                                           */
/* Input(s)           : u4Instance       - Context ID                        */
/*                      u4ProfileId      - Profile ID                        */
/*                      u1AddressType    - Internet address type             */
/*                      pTacmPrfStats    - Structure to update the reference */
/*                      counts of both Interface & VLAN reference count      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacApiGetPrfStats (UINT4 u4Instance, UINT4 u4ProfileId,
                   UINT1 u1AddressType, tTacmPrfStats * pTacmPrfStats)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (u4ProfileId);
    UNUSED_PARAM (u1AddressType);
    UNUSED_PARAM (pTacmPrfStats);
    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacSearchGrpSrcRecordInProfile                       */
/*                                                                           */
/* Description        : This function searches for the Group/Source(Channel) */
/*                      passed through the SLL. If the Channel is present    */
/*                      in the particular profile, then it will update the   */
/*                      status flag as TRUE                                  */
/*                                                                           */
/* Input(s)           : u4ProfileId      - Profile ID                        */
/*                      pGrpRecords      - Ptr to Group/Source records SLL   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacSearchGrpSrcRecordInProfile (UINT4 u4ProfileId, tGrpRecords * pGrpRecords)
{
    UNUSED_PARAM (u4ProfileId);
    UNUSED_PARAM (pGrpRecords);
    return TACM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : TacSearchGrpAddressInProfile                         */
/*                                                                           */
/* Description        : This function searches for the particular Group      */
/*                      address in the specified Profile. If match is found  */
/*                      it will return TRUE.                                 */
/*                                                                           */
/* Input(s)           : u4ProfileId      - Profile ID                        */
/*                      u4GrpAddr        - Group address to be searched      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_TRUE/TACM_FALSE                                 */
/*****************************************************************************/
INT4
TacSearchGrpAddressInProfile (UINT4 u4ProfileId, UINT4 u4GrpAddr)
{
    UNUSED_PARAM (u4ProfileId);
    UNUSED_PARAM (u4GrpAddr);
    return TACM_TRUE;
}

/*****************************************************************************/
/* Function Name      : TacApiGetPrfAction                                   */
/*                                                                           */
/* Description        : This function gets the Profile action  for this      */
/*                      profile.                                             */
/*                                                                           */
/* Input(s)           : u4ProfileId      - Profile ID                        */
/*                      u1AddressType    - Internet address type             */
/*                                                                           */
/* Output(s)          : pu1ProfileAction - Profile action                    */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
TacApiGetPrfAction (UINT4 u4ProfileId, UINT1 u1AddressType,
                    UINT1 *pu1ProfileAction)
{
    UNUSED_PARAM (u4ProfileId);
    UNUSED_PARAM (u1AddressType);
    UNUSED_PARAM (pu1ProfileAction);
    return TACM_SUCCESS;
}

#endif
#ifndef QOSX_WANTED
INT4
QosApiQosHwGetPfcStats (tQosPfcStats * pQosPfcStats)
{
    UNUSED_PARAM (pQosPfcStats);
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosApiDeAttachQueue                                  */
/* Description        : This function is called from the ETS subSystem       */
/*                      to detached queue from scheduler node                */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                    : u4SchedulerId - Scheduler ID                         */
/*                    : u4HierarLevel - Scheduler-Hierar Level               */
/*                    : u1Tsa - Scheduler Algorithm                          */
/* Output(s)          : None                                                 */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          :                                                      */
/*****************************************************************************/
INT4
QosApiDeAttachQueue (UINT4 u4IfIndex, UINT4 u4QueueId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4QueueId);
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosApiDeleteSchedular                                */
/* Description        : This function is called from the ETS subSystem       */
/*                      to delete the scheduler entry                        */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                    : u4SchedulerId - Scheduler ID                         */
/* Output(s)          : None                                                 */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          :                                                      */
/*****************************************************************************/
INT4
QosApiDeleteSchedular (UINT4 u4IfIndex, UINT4 u4SchedulerId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4SchedulerId);
    return (QOS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : QosApiAttachQueue                                    */
/* Description        : This function is called from the ETS subSystem       */
/*                      to attached queue in scheduler node                  */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                    : u4QueueId - Queue ID                                 */
/*                    : u4SchedulerId - Scheduler ID                         */
/*                    : u1EtsTsa - Scheduler Algorithm                       */
/*                    : u2EtsBw - Queue Bandwidth                            */
/* Output(s)          : None                                                 */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/* Called By          :                                                      */
/*****************************************************************************/
INT4
QosApiAttachQueue (UINT4 u4IfIndex, UINT4 u4QueueId,
                   UINT4 u4SchedulerId, UINT1 u1EtsTsa, UINT2 u2EtsBw)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4QueueId);
    UNUSED_PARAM (u4SchedulerId);
    UNUSED_PARAM (u1EtsTsa);
    UNUSED_PARAM (u2EtsBw);
    return (QOS_FAILURE);
}

/*****************************************************************************/
/* Function Name      : QosPortOperIndication                                */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Module        */
/*                      to indicate the port status indication               */
/*                                                                           */
/* Input(s)           : u2Port - Port number                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/*                                                                           */
/* Called By          : L2iwf Module                                         */
/*****************************************************************************/
INT4
QosPortOperIndication (UINT4 u4Port, UINT1 u1Status)
{
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Status);
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosDeletePort                                        */
/*                                                                           */
/* Description        : This function is called from the L2IWF Module        */
/*                      to indicate the deletion of a Port.                  */
/*                                                                           */
/* Input(s)           : u2Port - The Port number of the port to be deleted   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/*                                                                           */
/* Called By          : L2IWF Module                                         */
/*****************************************************************************/
INT4
QosDeletePort (UINT4 u4Port)
{
    UNUSED_PARAM (u4Port);
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosCreatePort                                        */
/*                                                                           */
/* Description        : This function is called from the L2IWF               */
/*                      to indicate the creation of a port                   */
/*                                                                           */
/* Input(s)           : u2Port     - Physical index of the port           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS/QOS_FAILURE                              */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
QosCreatePort (UINT4 u4Port)
{
    UNUSED_PARAM (u4Port);
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiDeleteMfcRateLimiter
 * */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to delete  MFC QoS configuration parameters    */
/*                            for added ACL rule                             */
/*                                                                           */
/* Input(s)           : u4FilterType - Acl filter type (L2 or L3)            */
/*                      pAclQosMapInfo - Pointer to Acl QoS common info      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
QosApiDeleteMfcRateLimiter (UINT4 u4FilterType, tAclQosMapInfo * pAclQosMapInfo)
{
    UNUSED_PARAM (u4FilterType);
    UNUSED_PARAM (pAclQosMapInfo);
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiAddMfcRateLimiter
 * */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to add MFC QoS configuration parameters        */
/*                            for the added ACL rule                         */
/*                                                                           */
/* Input(s)           : u4FilterType - Acl filter type (L2 or L3)            */
/*                          pAclQosMapInfo - Pointer to Acl QoS common info  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : QOS_SUCCESS / QOS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
QosApiAddMfcRateLimiter (UINT4 u4FilterType, tAclQosMapInfo * pAclQosMapInfo)
{
    UNUSED_PARAM (u4FilterType);
    UNUSED_PARAM (pAclQosMapInfo);
    return QOS_SUCCESS;
}

#if !defined (DIFFSRV_WANTED)
/*****************************************************************************/
/* Function Name      : QoSRegenerateVlanPriority                            */
/* Description        : This function is Called From VlanModule. It will     */
/*                      regenerate the priority value to the vlan module     */
/* Input(s)           : u4IfIndex,u2VlanId,u1InPriority.                     */
/* Output(s)          : u1RegenPri.                                          */
/* Global Variables                                                          */
/* Referred           :                                                      */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : u1RegenPri.                                          */
/* Called By          : Vlan Module                                          */
/* Calling Function   :                                                      */
/*****************************************************************************/
UINT1
QoSRegenerateVlanPriority (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 u1InPriority)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (u1InPriority);

    return 0;
}
#endif

/*****************************************************************************/
/* Function Name      : QosApiConfigAppPri                                   */
/* Description        : This function is used to create a Class Map entry    */
/*                      with next free class map id and map the incoming     */
/*                      Filter Id with that class-map Id. Also it creates an */
/*                      entry in the class to priority map and assigns       */
/*                      priority for the class-map created.                  */
/* Input(s)           : pClassMapPriInfo - pointer to ClassMapInfo        */
/* Output(s)          :                                                      */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : Applicationm Priority Utility functions              */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosApiConfigAppPri (tQosClassMapPriInfo * pClassMapPriInfo,
                    UINT1 u1FilterAction)
{
    UNUSED_PARAM (pClassMapPriInfo);
    UNUSED_PARAM (u1FilterAction);
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiConfigEts                                      */
/* Description        : This function is used to configure the bandwidth for */
/*                      queue associated with the give priority.             */
/*                      1. Get the Queue Id from the priority                */
/*                      2. Call QoSHwQueueCreate API for                     */
/*                           Configuring the bandwidth in hardware           */
/* Input(s)           : u4IfIndex -Interface Index.                          */
/*                    : u4PriIdx  - Priority value. There should be a        */
/*                                  one to one mapping between priority      */
/*                                  grouping in ETS and QOS. With that       */
/*                                  priority information only corresponding  */
/*                                  queue will be identified by QOS Module   */
/*                                  (from QMap Table)                        */
/*                                  for configuring the ETS bandwidth value  */
/*                                  for the queue.                           */
/*                    : u1EtsTCGID- Traffic Class Group ID.There should be a */
/*                                  one to one mapping between traffic class */
/*                                  grouping in ETS and QOS queue.           */
/*                                  Depending on the TCGID the scheduling    */
/*                                  algoriithm will be selected for the      */
/*                                  queue.                                   */
/*                    : u2EtsBw   - ETS Bandwidth which will be configured   */
/*                                  as queue weight.The value will           */
/*                                  come as % value                          */
/*                                  this value is percentage.                */
/*                    : u1EtsPortStatus  - ETS Enabled Status on Port.       */
/*                                  If ETS is enabled then ETS bandwidth     */
/*                                  will be configured as queue weight.      */
/*                                  Otherwise queue will be programmed       */
/*                                  with its default vale.                   */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosApiConfigEts (UINT4 u4IfIndex, UINT4 u4PriIdx,
                 UINT1 u1EtsTCGID, UINT2 u2EtsBw, UINT1 u1EtsPortStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PriIdx);
    UNUSED_PARAM (u1EtsTCGID);
    UNUSED_PARAM (u2EtsBw);
    UNUSED_PARAM (u1EtsPortStatus);
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiGetMaxTCSupport                                */
/* Description        : This function is used to give the maximum            */
/*                      number of Traffic Class Supported by QOS.            */
/* Input(s)           : u4Ifindex -Interface Index                           */
/* Output(s)          : u4MaxNumTcSupport -Maximum number of Traffic         */
/*                       Class Supported by the interface                    */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : PFC Utility functions                                */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QosApiGetMaxTCSupport (UINT4 u4IfIndex, UINT4 *pu4MaxNumTcSupport)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4MaxNumTcSupport);
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiConfigEtsTsa                                   */
/* Description        : This function is used to configure the bandwidth for */
/*                      queue associated with the give priority.             */
/*                      1. Get the Queue Id from the priority                */
/*                      2. Call QoSHwQueueCreate API for                     */
/*                           Configuring the bandwidth in hardware           */
/* Input(s)           : pPortEntry - Containing the Required data            */
/*                      to be programmed into HW.                            */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Calling Function   :                                                      */
/*****************************************************************************/
INT4
QosApiConfigEtsTsa (tQosEtsHwEntry * pPortEntry)
{
    UNUSED_PARAM (pPortEntry);
    return QOS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : QosApiConfigPfc                                      */
/* Description        : This function is used to call the PFC NPAPI          */
/*                      The u1PfcHwCfgFlag in tQosPfcHwEntry will            */
/*                      tell about the PFC operation.                        */
/*                                                                           */
/*                      QOS_PFC_PORT_CFG - PFC Enabled/Disabled              */
/*                                         configuration for                 */
/*                                         all priorities on port            */
/*                      QOS_PFC_DEL_PROFILE - PFC Profile Delete             */
/*                      QOS_PFC_PORT_CFG - PFC Threshold configuration       */
/*                                         for a switch                      */
/* Input(s)           : pPfcHwEntry - Pointer to the Pfc Hardware Entry      */
/*                              to be updated in the Hardware                */
/* Output(s)          : None.                                                */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : QOS_SUCCESS or QOS_FAILURE                           */
/* Called By          : PFC Utility functions                                */
/* Calling Function   :                                                      */
/*****************************************************************************/

INT4
QosApiConfigPfc (tQosPfcHwEntry * pPfcHwEntry)
{
    UNUSED_PARAM (pPfcHwEntry);
    return QOS_SUCCESS;
}

#endif /* QOSX_WANTED */

#ifndef ECFM_WANTED
/*****************************************************************************
 * Function Name      : EcfmStartedInContext                                 *
 *                                                                           *
 * Description        : This function is called from the VLAN module to      *
 *                      check whether ECFM is started in a context or not.   *
 *                                                                           *
 * Input(s)           : u4ContextId - Context indentifier                    *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_TRUE/ECFM_FALSE                                 *
 ****************************************************************************/
INT4
EcfmStartedInContext (UINT4 u4ContextId)
{
    /*In case if ECFM is enabled , this function return ECFM_TRUE */
    UNUSED_PARAM (u4ContextId);
    return ECFM_FALSE;
}

/******************************************************************************
 * Function Name      : EcfmLbLtHandleInFrameFromPort                        *
 *                                                                           *
 * Description        : This function is called from the L2IWF Module to     *
 *                      handover the CFMPDUs to LBLT task.                   *
 *                      OBSOLETED                                             *
 *                                                                           *
 * Input(s)           : pCruBuf - Pointer to the CFMPDU CRU Buffer           *
 *                      u4IfIndex - Interface Index of the Port              *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 *****************************************************************************/
PUBLIC INT4
EcfmLbLtHandleInFrameFromPort (tEcfmBufChainHeader * pCruBuf, UINT4 u4IfIndex)
{
    UNUSED_PARAM (pCruBuf);
    UNUSED_PARAM (u4IfIndex);
    return ECFM_SUCCESS;

}

/******************************************************************************
 * Function Name      : EcfmCcHandleInFrameFromPort                          *
 *                                                                           *
 * Description        : This function is called from the L2IWF Module to     *
 *                      handover the CFMPDUs to CC task.                     *
 *                      OBSOLETED                                             *
 *                                                                           *
 * Input(s)           : pCruBuf - Pointer to the CFMPDU CRU Buffer           *
 *                      u4IfIndex - Interface Index of the Port              *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 *                                                                           *
 ******************************************************************************/
PUBLIC INT4
EcfmCcHandleInFrameFromPort (tEcfmBufChainHeader * pCruBuf, UINT4 u4IfIndex)
{
    UNUSED_PARAM (pCruBuf);
    UNUSED_PARAM (u4IfIndex);
    return ECFM_SUCCESS;

}

/******************************************************************************
 * Function Name      : EcfmMepRegisterAndGetFltStatus                        *
 *                                                                            *
 * Description        : Routine used by protocols to register with ECFM.      *
 *                                                                            *
 * Input(s)           : tRmRegParams - Reg. params to be provided by protocols*
 *                      tEcfmMepInfoParams - Pointer to Mep information       * 
 *                      that needs to be monitored by the ECFM.               *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                           *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
EcfmMepRegisterAndGetFltStatus (tEcfmRegParams * pEcfmReg,
                                tEcfmEventNotification * pMepInfo)
{
    UNUSED_PARAM (pEcfmReg);
    UNUSED_PARAM (pMepInfo);
    return ECFM_FAILURE;
}

/*****************************************************************************
 * Function Name      : EcfmMepDeRegister                                     
 *                                                                            
 * Description        : Routine used by protocols to de-register from ECFM.   
 *                                                                            
 * Input(s)           : tRmRegParams - Reg. params to be provided by protocols
 *                      tEcfmMepInfoParams - Pointer to Mep information        
 *                      that needs to be monitored by the ECFM.
 *                      pMepInfo - Mep information. Meg, Me and Mep Id are
 *                      passed through this structure for de-registration.
 *                                                                           
 * Output(s)          : None
 *                                                                           
 * Return Value(s)    : None
 *                                                                           
 *****************************************************************************/
PUBLIC INT4
EcfmMepDeRegister (tEcfmRegParams * pEcfmReg, tEcfmEventNotification * pMepInfo)
{
    UNUSED_PARAM (pEcfmReg);
    UNUSED_PARAM (pMepInfo);
    return ECFM_FAILURE;
}

#if defined (Y1564_WANTED) || (RFC2544_WANTED)
/*****************************************************************************
 * Function Name      : EcfmSlaGetPerfTestParams                             *
 *                                                                           *
 * Description        : This function Gets the  Sla test parameters to       *
 *                      initiate the test                                    *
 *                                                                           *
 * Input(s)           : pEcfmLbLtMsg - Pointer to LBLT strcuture             *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None.                                                *
 *****************************************************************************/

PUBLIC tEcfmReqParams *
EcfmSlaGetPerfTestParams (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4MegId,
                          UINT4 u4MeId, UINT4 u4MepId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4SlaId);
    UNUSED_PARAM (u4MegId);
    UNUSED_PARAM (u4MeId);
    UNUSED_PARAM (u4MepId);
    return NULL;
}

/*****************************************************************************
 * Function Name      : Y1731ApiHandleExtRequest                             *
 *                                                                           *
 * Description        : This function is called from the Y1564 and RFC2544   *
 *                      module to indicate the creation of a Port.           *
 *                                                                           *
 * Input(s)           : pEcfmReqParams - pointer to ReqParams                *
 *                      pEcfmRespParams - pointer to ResParams               *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 ****************************************************************************/

PUBLIC INT4
Y1731ApiHandleExtRequest (tEcfmReqParams * pEcfmReqParams,
                          tEcfmRespParams * pEcfmRespParams)
{
    UNUSED_PARAM (pEcfmReqParams);
    UNUSED_PARAM (pEcfmRespParams);
    return ECFM_FAILURE;
}

/*******************************************************************************
 * Function Name      : EcfmApiValMepIndex                                      *
 *  *                                                                           *
 *  * Description        : This function is called from the ERPS Module         *
 *  *                      to validate the domain and service and MEP id        *
 *  *                                                                           *
 *  *                                                                           *
 *  *                                                                           *
 *  * Input(s)           : pMepInfo -Structure of MEP informaion in ERPS        *
 *  *                                                                           *
 *  * Output(s)          : None                                                 *
 *  *                                                                           *
 *  * Return Value(s)    : ECFM_SUCCESS / ECFM_FAILURE                          *
 *  ***************************************************************************/

PUBLIC INT4
EcfmApiValMepIndex (tEcfmMepInfoParams * pMepInfo)
{
    UNUSED_PARAM (pMepInfo);
    return ECFM_FAILURE;
}
#endif
#endif

#ifndef CFA_WANTED
/*****************************************************************************
 *    Function Name       : CfaVlanValidateL3SubIf
 *    Description         : This function checks the given port is a parent
 *                          port and part of the given vlan.
 *    Input(s)            : u4IfIndex      - Interface index
 *                          u2VlanId       - L2VlanId
 *    Output(s)           : NONE
 *    Returns             : CFA_TRUE/CFA_FALSE
 *****************************************************************************/
INT4
CfaVlanValidateL3SubIf (UINT4 u4IfIndex, UINT2 u2L2VlanId)
{

    UNUSED_PARAM (u2L2VlanId);
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *    Function Name       : CfaIsL3SubIfVLAN
 *    Description         : This function checks whether the VLAN configured
 *                          is associated to any subinterface Port.
 *    Input(s)            : u2VlanId       - Encapsulation VLAN ID
 *    Output(s)           : NONE
 *    Returns             : CFA_TRUE/CFA_FALSE
 *****************************************************************************/
UINT4
CfaIsL3SubIfVLAN (UINT2 u2VlanId)
{
    UNUSED_PARAM (u2VlanId);
    return CFA_SUCCESS;
}

/***********************************************************************************
 *
 *    Function Name       : CfaCreateDynamicSChannelInterface
 *
 *    Description         : This function creates dynamic S-Channel interface in CFA
 *                          and thereby indicating to L2IWF to create the
 *                          S-Channel interface to VLAN, IGMP Snooping and
 *                          FIP Snooping.
 *
 *    Input(s)            : u4IfIndex - S-Channel interface index.
 *                          u4UapIfIndex - UAP interface index.
 *
 *    Output(s)           : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 ************************************************************************************/
INT4
CfaCreateDynamicSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4UapIfIndex);
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
*    Function Name        : CfaDeleteDynamicSChannelInterface
*    Description :             S-Channel interface to VLAN, IGMP Snooping and
 *                          FIP Snooping.
 *
 *    Input(s)            : u4IfIndex - S-Channel interface index.
 *
 *    Output(s)           : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 *******************************************************************************************/
INT4
CfaDeleteDynamicSChannelInterface (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaUpdateSChannelOperStatus
 *
 *    Description          : This function is used to trigger CFA for
 *                           updating the OperStatus of the S-Channel Interface
 *
 *    Input(s)             : u4SchIfIndex - S-Channel IfIndex
 *                           u1OperStatus - Oper Status
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns          : CFA_SUCCESS / CFA_FAILURE.
 *****************************************************************************/

INT4
CfaUpdateSChannelOperStatus (UINT4 u4SchIfIndex, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4SchIfIndex);
    UNUSED_PARAM (u1OperStatus);
    return CFA_SUCCESS;
}

/***************************************************************************
 *
 *    Function Name       : CfaFsCfaHwClearStats
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwClearStats
 *
 *    Input(s)            : Arguments of FsCfaHwClearStats
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwClearStats (UINT4 u4Port)
{
    UNUSED_PARAM (u4Port);
    return CFA_SUCCESS;
}

/***************************************************************************
 *
 *    Function Name       : CfaFsCfaHwSetMtu
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsCfaHwSetMtu
 *
 *    Input(s)            : Arguments of FsCfaHwSetMtu
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
CfaFsCfaHwSetMtu (UINT4 u4IfIndex, UINT4 u4MtuSize)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4MtuSize);
    return CFA_SUCCESS;
}

/***************************************************************************
 *
 *    Function Name       : CfaFsHwGetStat
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsHwGetStat
 *
 *    Input(s)            : Arguments of FsHwGetStat
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
CfaFsHwGetStat (UINT4 u4IfIndex, INT1 i1StatType, UINT4 *pu4Value)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i1StatType);
    UNUSED_PARAM (pu4Value);
    return CFA_SUCCESS;
}

/******************************************************************************
 * Function           : CfaGetPeerNodeCount
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Number of standby nodes up.
 * Action             : Routine to get the number of peer nodes that are up.
 ******************************************************************************/
INT4
CfaGetPeerNodeCount ()
{
    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetLocalUnitPortInformation                     */
/*                                                                           */
/*     DESCRIPTION      : This function is called to fetch the               */
/*                        local unit port information of the current system  */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : pu4ConnectingPortIfIndex - If-Index of the         */
/*                        connecting port used in Dual Unit Stacking         */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS                                        */
/*                                                                           */
/*****************************************************************************/
VOID
CfaGetLocalUnitPortInformation (tHwPortInfo * pHwPortInfo)
{
    UNUSED_PARAM (pHwPortInfo);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name       : CfaIsSbpInterface                               */
/*                                                                          */
/*    Description         : This function checks whether the interface      */
/*                          is SBP  interface or not.                       */
/*                                                                          */
/*    Input(s)            : u4Index - interface index                       */
/*                                                                          */
/*    Output(s)           : NONE                                            */
/*                                                                          */
/*    Global Variables Referred : None.                                     */
/*                                                                          */
/*    Global Variables Modified : None.                                     */
/*                                                                          */
/*    Exceptions or Operating                                               */
/*    System Error Handling    : None.                                      */
/*                                                                          */
/*    Use of Recursion        : None.                                       */
/*                                                                          */
/*    Returns            : CFA_TRUE-If interface is a SBP interface        */
/****************************************************************************/
UINT1
CfaIsSbpInterface (UINT4 u4IfIndex)
{
    UINT1               u1RetVal = CFA_FALSE;
    UNUSED_PARAM (u4IfIndex);
    return u1RetVal;
}

/*********************************************************************************
 *    Function Name              : CfaGetIfOperMauType                           *
 *    Description                : This function updates the oper MAU type of the*
 *                                 interface.                                    *
 *    Input(s)                   : u4IfIndex - interface index whose             *
 *                                 Oper Mau Type is needed.                      *
 *                               : i4IfMauIndex - MAU index of the Interface.    *
 *                                                                               *
 *    Output(s)                  : Oper MAU Type is updated.                     *
 *    Global Variables Referred  : None.                                         *
 *    Global Variables Modified  : None.                                         *
 *    Exceptions or Operating                                                    *
 *    System Error Handling      : None.                                         *
 *    Use of Recursion           : None.                                         *
 *    Returns                    : CFA_SUCCESS/CFA_FAILURE                       *
 ********************************************************************************/
INT4
CfaGetIfOperMauType (UINT4 u4IfIndex, INT4 i4MauIndex, UINT4 *pu4RetOperMauType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4MauIndex);
    UNUSED_PARAM (*pu4RetOperMauType);
    return CFA_SUCCESS;
}
#endif

#ifndef VLAN_WANTED
/*****************************************************************************/
/* Function Name      : L2IwfGetDefaultVlanId                                */
/*                                                                           */
/* Description        : This function returns the Default Vlan Id            */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Default Vlan Id                                      */
/*****************************************************************************/
VOID
L2IwfGetDefaultVlanId (UINT2 *pDefaultVlanId)
{
    *pDefaultVlanId = 1;
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsMemberPortForCfm                           */
/*                                                                           */
/*    Description         : This function called by ECFM module to verify    */
/*                          the given port is member of the given vlan       */
/*                                                                           */
/*    Input(s)            : VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                                                                           */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanIsMemberPortForCfm (UINT4 u4ContextId, UINT2 u2Port, tVlanTag VlanTagInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (VlanTagInfo);
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanProcessPktForCep                             */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          apply the PB rules for a CFM-PDU received        */
/*                          on a CEP.                                        */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u2LocalPort- Port on which CFM-PDU was received. */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : pBuf     - CRU Buffer recieved.                  */
/*                          pVlanTag - Classified Vlan Information.          */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
VlanProcessPktForCep (UINT4 u4ContextId, UINT2 u2LocalPort,
                      tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pVlanTag);
    return (VLAN_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanDecodePcpAndTagLenInfo                       */
/*                                                                           */
/*    Description         : This function is called by L2iwf module to get   */
/*                          tag length information from the recevied frame   */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u4IfIndex - Port received the frame              */
/*                                                                           */
/*    Output(s)           : *pu4TagOffSet- offset of the protocol data presnt*/
/*                           in the frame                                    */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanDecodePcpAndTagLenInfo (UINT4 u4ContextId, UINT2 u2LocalPort,
                            tCRU_BUF_CHAIN_DESC * pBuf, UINT4 *pu4TagOffSet,
                            UINT1 *pu1Pcp, UINT1 *pu1Priority,
                            UINT1 *pu1DropEligible)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pu4TagOffSet);
    UNUSED_PARAM (pu1Pcp);
    UNUSED_PARAM (pu1Priority);
    UNUSED_PARAM (pu1DropEligible);
    return VLAN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetVlanInfoFromFrame                         */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get VlanId, priority and DE from the given frame.*/
/*                          In case of PEB, This function should not be      */
/*                          called by the ECFM module in case of the untagged*/
/*                          packet intended for Vlan unaware MEP existing on */
/*                          that port.                                       */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u4IfIndex - Port received the frame              */
/*                                                                           */
/*    Output(s)           : *pVlanTagInfo- VlanId, Priority, DE for recvd    */
/*                           frame                                           */
/*                          *pu1IngressAction - VLAN_TRUE (If Port is not    */
/*                           the member port of VLAN and Ingress filtering is*/
/*                           enabled)                                        */
/*                           VLAN_FALSE (If port is member of VLAN)          */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetVlanInfoFromFrame (UINT4 u4ContextId, UINT2 u2LocalPort,
                          tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                          UINT1 *pu1IngressAction, UINT4 *pu4TagOffSet)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pVlanTag);
    UNUSED_PARAM (pu1IngressAction);
    UNUSED_PARAM (pu4TagOffSet);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanIsEcfmTunnelPkt                              */
/*                                                                           */
/*    Description         : This function is used to check whether the       */
/*                          pkt is ecfm tunnel pkt                           */
/*                                                                           */
/*    Input(s)            : DestAddr - Destination MAc Address               */
/*                          u4ContextId - Context ID                         */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanIsEcfmTunnelPkt (UINT4 u4ContextId, tMacAddr DestAddr)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (DestAddr);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanSendFrameToPort                              */
/*                                                                           */
/*    Description         : This function chenges the ether type of valn tag */
/*                          based on ethre type configured.                  */
/*                                                                           */
/*    Input(s)            : pFrame      - Incoming frame                     */
/*                          u2Port      - Receive port                       */
/*                          u4TagOffset - tag offset, for swaping            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : VLAN_TAGGED/VLAN_UNTAGGED                  */
/*                                                                           */
/*****************************************************************************/
INT4
VlanSendFrameToPort (tCRU_BUF_CHAIN_DESC * pFrame,
                     tVlanOutIfMsg * pVlanOutIfMsg,
                     UINT4 u4ContextId, UINT2 u2Port,
                     tVlanId VlanId, UINT1 u1RegenPri)
{
    UNUSED_PARAM (pFrame);
    UNUSED_PARAM (pVlanOutIfMsg);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1RegenPri);
    return VLAN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanGetFwdPortList                               */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get the list of ports to which the pkt is fwded. */
/*                                                                           */
/*    Input(s)            : pFrame- Incoming frame                           */
/*                          u4ContextId - Context Identifier                 */
/*                          u2LocalPort - Port Identifier                    */
/*                          SrcAddr - Source MacAddress to be learnt         */
/*                          DstMacAddr - Destination Mac Address for lookup  */
/*                          VlanId - VlanId of the frame                     */
/*                                                                           */
/*    Output(s)           : FwdPortList- list of port(s) to which the pkt    */
/*                          has to be forwarded                              */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/

INT4
VlanGetFwdPortList (UINT4 u4ContextId, UINT2 u2LocalPort, tMacAddr SrcAddr,
                    tMacAddr DestAddr, tVlanId VlanId, tLocalPortList
                    FwdPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (SrcAddr);
    UNUSED_PARAM (DestAddr);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (FwdPortList);

    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanConfPortVlanLckStatus                           */
/*                                                                           */
/* Description        : This routine is called by ECFM module to update the  */
/*                      the LCK status for Data corresponding to Vlan        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index                               */
/*                      VlanId - Vlan Id                                     */
/*                      b1LckStatus - LCk status to be updated               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS or VLAN_FAILURE                       */
/*****************************************************************************/
INT4
VlanConfPortVlanLckStatus (UINT4 u4IfIndex, UINT2 u2VlanId, BOOL1 b1LckStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (b1LckStatus);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : VlanGetRmonStatsPerVlan                              */
/*                                                                           */
/* Description        : This API call collects the per VLAN statistics       */
/*                      from Hardware by calling the specific HW API         */
/*                                                                           */
/* Input(s)           : u4ContextId - ContextId                              */
/*                      VlanId      - Vlan Id                                */
/*                                                                           */
/* Output(s)          : pEthStatsEntry structure filled with VLAN statistics */
/*                      values                                               */
/*                                                                           */
/* Return Value(s)    : VLAN_SUCCESS/VLAN_FAILURE                            */
/*****************************************************************************/

INT4
VlanGetRmonStatsPerVlan (UINT4 u4ContextId,
                         tVlanId VlanId, tVlanEthStats * pVlanStatsEntry)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pVlanStatsEntry);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VlanUnMapPort                                    */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port is unmapped  */
/*                          from a context                                   */
/*                                                                           */
/*    Input(s)            : u2IfIndex - port that is unmapped .              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS                                      */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
VlanUnmapPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return VLAN_SUCCESS;
}

#endif
#ifndef PBB_WANTED
INT1
PbbGetVipFromIsid (UINT4 u4Context, UINT4 u4Isid, UINT2 *pu2Vip)
{
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (pu2Vip);
    return PBB_SUCCESS;
}

INT1
PbbGetIsidForVip (UINT4 u4Vip, UINT4 *pu4Isid)
{
    UNUSED_PARAM (u4Vip);
    *pu4Isid = *pu4Isid;
    return PBB_SUCCESS;
}

INT1
PbbGetCbpListForIsid (UINT4 u4ContextId,
                      UINT4 u4Isid, tSNMP_OCTET_STRING_TYPE * pSnmpPorts)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (pSnmpPorts);
    return PBB_SUCCESS;
}

INT1
PbbGetLocalIsidFromRelayIsid (UINT4 u4ContextId, UINT2 u2Port,
                              UINT4 u4RelayIsid, UINT4 *pu4LocalIsid)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4RelayIsid);
    UNUSED_PARAM (pu4LocalIsid);
    return PBB_SUCCESS;
}

INT4
L2IwfTransmitFrameFromCbp (UINT4 u4ContextId, UINT2 u2LocalPort,
                           tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                           tPbbTag * pPbbTag, UINT1 u1FrameType)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pVlanTag);
    UNUSED_PARAM (pPbbTag);
    UNUSED_PARAM (u1FrameType);
    return PBB_SUCCESS;
}

INT4
PbbGetIsidInfoFromFrame (UINT4 u4ContextId, UINT2 u2LocalPort,
                         tCRU_BUF_CHAIN_DESC * pBuf, tPbbTag * pPbbTag,
                         UINT4 u4IsidTagOffSet)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pPbbTag);
    UNUSED_PARAM (u4IsidTagOffSet);
    return PBB_SUCCESS;
}

INT4
L2IwfGetTagInfoFromFrame (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                          tPbbTag * pPbbTag, UINT1 *pu1IngressAction,
                          UINT4 *pu4InOffset, UINT4 *pu4TagOffSet,
                          UINT1 u1FrameType, UINT1 *pu1IsSend)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pVlanTag);
    UNUSED_PARAM (pPbbTag);
    UNUSED_PARAM (pu1IngressAction);
    UNUSED_PARAM (pu4InOffset);
    UNUSED_PARAM (pu4TagOffSet);
    UNUSED_PARAM (u1FrameType);
    UNUSED_PARAM (pu1IsSend);
    return PBB_SUCCESS;
}

INT4
PbbCliGetVipIndexOfIsid (UINT4 u4ContextId, UINT4 u4Isid, UINT4 *pu4Value)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (pu4Value);
    return PBB_SUCCESS;
}

INT1
PbbIsIsidMemberPort (UINT4 u4ContextId, UINT4 u4Isid, UINT2 u2LocalPort)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u2LocalPort);
    return PBB_SUCCESS;
}

VOID
PbbGetBCompBDA (UINT4 u4ContextId, UINT2 u2LocalPort, UINT4 u4Isid,
                tMacAddr BDA, UINT1 u1Dir, BOOL1 b1UseDefaultBDA)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (BDA);
    UNUSED_PARAM (u1Dir);
    UNUSED_PARAM (b1UseDefaultBDA);
    return;
}

INT4
PbbGetCnpMemberPortsForIsid (UINT4 u4ContextId,
                             UINT4 u4Isid, tSNMP_OCTET_STRING_TYPE * pPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (pPortList);
    return PBB_SUCCESS;
}

INT1
PbbGetPipVipWithIsid (UINT4 u4ContextId, UINT2 u2LocalPort,
                      UINT4 u4Isid, UINT2 *pu2Vip, UINT2 *pu2Pip)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (pu2Vip);
    UNUSED_PARAM (pu2Pip);
    return PBB_SUCCESS;
}

INT1
PbbGetVipIsidWithPortList (UINT4 u4ContextId, UINT2 u2LocalPort,
                           UINT1 *InPortList, UINT2 *pu2Vip, UINT2 *pu2Pip,
                           UINT4 *pu4Isid, tMacAddr * pBDaAddr)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (InPortList);
    UNUSED_PARAM (pu2Vip);
    UNUSED_PARAM (pu2Pip);
    UNUSED_PARAM (pu4Isid);
    UNUSED_PARAM (pBDaAddr);
    return PBB_SUCCESS;
}

INT4
PbbGetMemberPortsForIsid (UINT4 u4ContextId,
                          UINT4 u4Isid, tSNMP_OCTET_STRING_TYPE * pPortList)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pPortList);
    UNUSED_PARAM (u4Isid);
    return PBB_SUCCESS;
}

INT4
PbbGetPipWithPortList (UINT4 u4ContextId, UINT2 u2LocalPort, UINT1 *InPortList,
                       UINT1 *OutPortList)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (InPortList);
    UNUSED_PARAM (OutPortList);
    return PBB_SUCCESS;
}

INT4
PbbGetBvidForIsid (UINT4 u4ContextId, UINT2 u2LocalPort, UINT4 u4Isid,
                   tVlanId * pBvid)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (pBvid);
    return PBB_SUCCESS;
}

INT4
L2IwfTransmitFrameOnVip (UINT4 u4ContextId, UINT4 u4IfIndex,
                         tCRU_BUF_CHAIN_DESC * pBuf,
                         tVlanTag * pVlanTag, tPbbTag * pPbbTag,
                         UINT1 u1FrameType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pVlanTag);
    UNUSED_PARAM (pPbbTag);
    UNUSED_PARAM (u1FrameType);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPbbShutdownStatus                            */
/*                                                                           */
/* Description        : This routine is called from other modules to get PBB */
/*                      shutdown status.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE/ L2IWF_FALSE                              */
/*****************************************************************************/
INT4
L2IwfGetPbbShutdownStatus (VOID)
{
    return L2IWF_TRUE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbbProcessDataPacket                            */
/*                                                                           */
/* Description        : This routine is called for processing the data packet*/
/*                      and getting the Tag information from the packet      */
/*                      received                                             */
/*                                                                           */
/* Input(s)           : pBuf - Control Packet bufffer                        */
/*                      u4IfIndex - Port Index                               */
/*                      u4ContextId - Context Id                             */
/*                      pu1Dst - Destination Mac Address                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbbProcessDataPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                           UINT4 u4ContextId, UINT1 *pu1Dst)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pu1Dst);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfPbbTxFrameToIComp                           */
/*                                                                           */
/*    Description         : This function is called when an B-Tagged frame   */
/*                          arrives on PNP in an IB Bridge, and the frame    */
/*                          consists S-Tag also. Then the frame need to be   */
/*                          sent to I Component bridge                       */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u4IfIndex - Port received the frame              */
/*                          u2LocalPort - Local Port                         */
/*                          pVlanTag - Vlan Tag Information                  */
/*                          pPbbTag - Pbb Tag Information                    */
/*                          pu4InOffset - offset of the protocol PDU in buf  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : L2IWF_SUCCESS/                                    */
/*                         L2IWF_FAILURE                                     */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfPbbTxFrameToIComp (UINT4 u4ContextId, UINT4 u4IfIndex,
                        tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                        tPbbTag * pPbbTag, UINT4 *pu4InOffset,
                        UINT1 u1FrameType, UINT2 u2LocalPort)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pVlanTag);
    UNUSED_PARAM (pPbbTag);
    UNUSED_PARAM (pu4InOffset);
    UNUSED_PARAM (u1FrameType);
    UNUSED_PARAM (u2LocalPort);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVipOperStatusFlag                            */
/*                                                                           */
/* Description        : This routine is called from CFA to get the           */
/*                      u1VipOperStatusFlag from L2IWF port entry            */
/*                                                                           */
/* Input(s)           : u4IfIndex - VIP Interface Index                      */
/*                                                                           */
/* Output(s)          : pu1VipOperStatusFlag - Vip Oper Status Flag          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVipOperStatusFlag (UINT4 u4IfIndex, UINT1 *pu1VipOperStatusFlag)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1VipOperStatusFlag);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfTransmitPbbFrame                                */
/*                                                                           */
/* Description        : This routine sends buf on the outgoing port.         */
/*                                                                           */
/* Input(s)           : pBuf - Buffer send.                                  */
/*                      u4ContextId - context id                             */
/*                      u2Port - sending port                                */
/*                      pVlanTagInfo, pPbbTag                            */
/*                      u1FrameType - CFM_CC, CFM_LB or CFM_LT               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfTransmitPbbFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                       UINT4 u4IfIndex, tVlanTag * pVlanTagInfo,
                       tPbbTag * pPbbTag, UINT1 u1FrameType)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pVlanTagInfo);
    UNUSED_PARAM (pPbbTag);
    UNUSED_PARAM (u1FrameType);
    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfSetAllToOneBndlStatus                           */
/*                                                                           */
/* Description        : This routine is called from VLAN to set the          */
/*                      All To One Bundling Status from L2IWF Context Info   */
/*                                                                           */
/* Input(s)           : u4ContextId - ContextId                              */
/*                      u1Status -   OSIX_TRUE / OSIX_FALSE                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetAllToOneBndlStatus (UINT4 u4ContextId, UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1Status);
    return L2IWF_SUCCESS;
}

#ifdef NPAPI_WANTED
/***************************************************************************/
/* Function Name    : PbbRemovePortPropertiesFromHw ()                    */
/*                                                                         */
/* Description      : This function removes from the Hardware all the      */
/*                    port properties of "u4DstPort". The exact properties */
/*                    which need to be removed is obtained from the        */
/*                    port "u4SrcPort". This function is typically called  */
/*                    when a physical port is removed from a Port Channel. */
/*                    When the physical port is removed from a Port        */
/*                    Channel, the Port Channel properties must be removed */
/*                    from the physical port. This function will be        */
/*                    invoked with "u4DstPort" as the Physical Port Id and */
/*                    "u4SrcPort" as the Port Channel Id.                  */
/*                    "u4SrcPort" must have been created in Pbb.          */
/*                                                                         */
/*                    This function will also be invoked whenever a Port   */
/*                    is deleted in VLAN. In which, u4SrcPort and          */
/*                    u4DstPort will be same.                              */
/*                                                                         */
/* Input(s)         : u4DstPort - Port whose properties have to be removed */
/*                                from the Hardware.                       */
/*                                                                         */
/*                    u4SrcPort - Port from where the properties to be     */
/*                                removed, have to be obtained.            */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : PBB_SUCCESS on success,                             */
/*                    PBB_FAILURE otherwise.                              */
/***************************************************************************/
INT1
PbbRemovePortPropertiesFromHw (UINT4 u4DstPort, UINT4 u4SrcPort)
{
    UNUSED_PARAM (u4DstPort);
    UNUSED_PARAM (u4SrcPort);
    return PBB_SUCCESS;
}

/***************************************************************************/
/* Function Name    : PbbCopyPortPropertiesToHw ()                        */
/*                                                                         */
/* Description      : This function programs the hardware with all         */
/*                    the Port properties for the port "u4DstPort".        */
/*                    The properties to be copied to "u4DstPort" is        */
/*                    obtained from "u2Port".                              */
/*                    "u4SrcPort" must have been created in PBB. This     */
/*                    function does not program the Dynamic PBB and Mcast */
/*                    membership and hence must not be called for ports    */
/*                    that are in OPER UP state. Dynamic Learning is NOT   */
/*                    done when the port is in OPER DOWN state.            */
/*                                                                         */
/* Input(s)         : u4DstPort - The Port whose properties have to be     */
/*                                programmed in the Hardware.              */
/*                                                                         */
/*                  : u4SrcPort - The Port from where the port properties  */
/*                                have to be obtained.                     */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : PBB_SUCCESS on success,                             */
/*                    PBB_FAILURE otherwise.                              */
/***************************************************************************/
INT1
PbbCopyPortPropertiesToHw (UINT4 u4DstPort, UINT4 u4SrcPort)
{
    UNUSED_PARAM (u4DstPort);
    UNUSED_PARAM (u4SrcPort);
    return PBB_SUCCESS;
}
#endif
/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNextIsidForCBP                            */
/*                                                                          */
/*    Description        : This function is used to get ISID mapped to a CBP*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetNextIsidForCBP (UINT4 u4ContextId, UINT2 u2LocalPort, UINT4 u4CurrIsid,
                      UINT4 *pu4NextIsid)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (u4CurrIsid);
    UNUSED_PARAM (pu4NextIsid);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       :  PbbGetPortIsidStats                             */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get the transmission counters for a vlan         */
/*                          present on a particular port.                    */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port Index                           */
/*                          u4Isid    - Isid                                 */
/*                                                                           */
/*    Output(s)           : pu4TxFCl - Transmission Counter                  */
/*                          pu4RxFCl - Reception Counter                     */
/*                                                                           */
/*    Returns            : NONE                                              */
/*                                                                           */
/*****************************************************************************/
VOID
PbbGetPortIsidStats (UINT4 u4IfIndex, UINT4 u4Isid,
                     UINT4 *pu4TxFCl, UINT4 *pu4RxFCl)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (pu4TxFCl);
    UNUSED_PARAM (pu4RxFCl);
    return;
}

/*****************************************************************************
 *
 *    Function Name        : PbbConfPortIsidLckStatus
 *
 *    Description          : This function updates LCK status for Data 
 *                           corresponding to Service Instance
 *
 *    Input(s)             : u4IfIndex: Interface Index 
 *                           u4Isid : Isid for which status to be updated
 *                           b1Status: Status 
 *
 *    Output(s)            : None.
 *
 *    Returns              : PBB_SUCCESS/PBB_FAILURE.
 *****************************************************************************/
INT4
PbbConfPortIsidLckStatus (UINT4 u4IfIndex, UINT4 u4Isid, BOOL1 b1Status)
{

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (b1Status);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbVlanAuditStatusInd                            */
/*                                                                           */
/*    Description         : Invoked by VLAN whenever its Audit gets finished.*/
/*                          or when bridge mode audit is done by vlan.       */
/*                          After receiving Vlan Hw Audit complete msg Pbb   */
/*                          will start audit for its Delete Port.            */
/*                          After receiving the bridge mode change indication*/
/*                          from vlan, Pbb will configure all the            */
/*                          configurations related to that context.          */
/*                                                                           */
/*    Input(s)            : pPbbVlanStatus - u4VlanAudStatus - Audit Status  */
/*                              Possible values: PBB_VLAN_AUDIT_BRG_MODE_CHG */
/*                                               PBB_VLAN_AUDIT_COMPLETED    */
/*                        :                  u4ContetxId - ContextId of the  */
/*                        :     Context where bridge mode audit is being done*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PbbVlanAuditStatusInd (tPbbVlanAuditStatus * pPbbVlanStatus)
{
    UNUSED_PARAM (pPbbVlanStatus);
    return PBB_SUCCESS;
}
#endif
#ifndef PBBTE_WANTED
/*****************************************************************************/
/* Function Name      : PbbTeVidIsEspVid                                     */
/*                                                                           */
/* Description        : This API is exported to other modules to check       */
/*                      whether a particular VlanId is an ESP Vlan in the    */
/*                      particular context.                                  */
/*                                                                           */
/* Input(s)           : u4ContextId - ContextIdentifer                       */
/*                      EspVlanId   - An integer that represents the ESP Vlan*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE / OSIX_FALSE                               */
/*****************************************************************************/
INT4
PbbTeVidIsEspVid (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    return OSIX_FALSE;
}
#endif

#ifndef ELPS_WANTED
/***************************************************************************
 * FUNCTION NAME    : ElpsLock
 *
 * DESCRIPTION      : Function to take the mutual exclusion protocol 
 *                    semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsApiLock (VOID)
{
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUnLock
 *
 * DESCRIPTION      : Function to release the mutual exclusion protocol 
 *                    semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsApiUnLock (VOID)
{
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiCreateContext
 *
 * DESCRIPTION      : This function posts a message to ELPS task's message
 *                    queue to indicate the creation of a context
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsApiCreateContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiDeleteContext
 *
 * DESCRIPTION      : This function posts a message to ELPS task's message
 *                    queue to indicate the deletion of a context
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsApiDeleteContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiEcfmIndCallBack
 *
 * DESCRIPTION      : For receiving the APS, Signal Fail, Signal Ok for MEPs 
 *                    this function was registered with ECFM module. 
 *                    ECFM module will call this callback function whenever 
 *                    a Signal Fail/Signal Ok happens for an MEP, as well as 
 *                    on reception of the APS PDU.
 *
 * INPUT            : pEcfmEventNotification - Pointer to the structure 
 *                                             which consist of the necessary 
 *                                             information to identify the 
 *                                             SF, OK, APS PDU reception.
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE
 *
 **************************************************************************/
PUBLIC VOID
ElpsApiEcfmIndCallBack (tEcfmEventNotification * pEvent)
{
    UNUSED_PARAM (pEvent);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiModuleShutDown 
 *
 * DESCRIPTION      : This API completely shuts the module down. RM uses this
 *                    API to restart the module to handle Communication lost 
 *                    and restored scenario.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsApiModuleShutDown (VOID)
{
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiModuleStart 
 *
 * DESCRIPTION      : This API completely starts the module. RM uses this
 *                    API to restart the module to handle Communication lost 
 *                    and restored scenario.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC INT4
ElpsApiModuleStart (VOID)
{
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiIsElpsStartedInContext 
 *
 * DESCRIPTION      : API to query ELPS if ELPS is started in the given context
 *                    
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 * 
 **************************************************************************/
PUBLIC INT1
ElpsApiIsElpsStartedInContext (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return OSIX_FALSE;
}
#endif
#ifndef ECFM_WANTED
/***************************************************************************
 * FUNCTION NAME    : EcfmRegisterProtocols
 *
 * DESCRIPTION      : Routine used by protocols to register with ECFM.
 *
 * INPUT            : pEcfmReg    - Pointer to the tEcfmRegParams structure
 *                    u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 * 
 **************************************************************************/
INT4
EcfmRegisterProtocols (tEcfmRegParams * pEcfmReg, UINT4 u4ContextId)
{

    UNUSED_PARAM (pEcfmReg);
    UNUSED_PARAM (u4ContextId);
    return ECFM_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : EcfmDeRegisterProtocols
 *
 * DESCRIPTION      : Routine used by protocols to de-register with ECFM.
 *
 * INPUT            : pEcfmReg    - Pointer to the tEcfmRegParams structure
 *                    u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
EcfmDeRegisterProtocols (UINT4 u4ModId, UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ModId);
    UNUSED_PARAM (u4ContextId);
    return ECFM_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : EcfmInitiateExPdu
 *
 * DESCRIPTION      : Routine used by protocols to intiate PDU though ECFM 
 *
 * INPUT            : pMepInfo      - Pointer to structure having information 
 *                                    to select a MEP node
 *                    u1TlvOffset   - TLV offset for the PDU
 *                    u1SubOpCode   - SubOpCode for the External PDU
 *                    pu1PduData    - Pointer to External data
 *                    u4PduDataLen  - Length of External data
 *                    TxDestMacAddr - Destination Mac Address
 *                    u1OpCode      - Opcode of the PDU to be sent
 *
 * OUTPUT           : None
 * 
 * RETURNS          : ECFM_SUCCESS/ECFM_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
EcfmInitiateExPdu (tEcfmMepInfoParams * pMepInfo, UINT1 u1TlvOffset,
                   UINT1 u1SubOpCode, UINT1 *pu1PduData,
                   UINT4 u4PduDataLen, tMacAddr TxDestMacAddr, UINT1 u1OpCode)
{
    UNUSED_PARAM (pMepInfo);
    UNUSED_PARAM (u1TlvOffset);
    UNUSED_PARAM (u1SubOpCode);
    UNUSED_PARAM (pu1PduData);
    UNUSED_PARAM (u4PduDataLen);
    UNUSED_PARAM (TxDestMacAddr);
    UNUSED_PARAM (u1OpCode);
    return ECFM_SUCCESS;
}
#endif
#ifndef PB_WANTED
/*****************************************************************************/
/* Function Name      : L2iwfValidatePortType                                */
/*                                                                           */
/* Description        : This function is used to validate the bridge port    */
/*                      type for the bridge mode                             */
/*                                                                           */
/* Input(s)           : u4BridgeMode  - The Bridge mode                      */
/*                      u1BrgPortType - the port type to be validated for    */
/*                                      for the givem bridge mode            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE/L2IWF_FALSE                               */
/*****************************************************************************/
UINT4
L2iwfValidatePortType (UINT4 u4BridgeMode, UINT1 u1BrgPortType)
{
    UNUSED_PARAM (u4BridgeMode);
    UNUSED_PARAM (u1BrgPortType);

    return L2IWF_TRUE;
}
#endif /* PB_WANTED */

/* HITLESS RESTART */
#ifndef L2RED_WANTED
/***************************************************************************/
/* Function           : L2IwfRedGetHRFlag                                  */
/* Input(s)           : None                                               */
/* Output(s)          : None                                               */
/* Returns            : Hitless restart flag value.                        */
/* Action             : This API returns the hitless restart flag value.   */
/***************************************************************************/
UINT1
L2IwfRedGetHRFlag (VOID)
{
    return 0;
}

#endif
#ifndef MRP_WANTED
/***************************************************************************/
/* Function           : MrpApiMrpConfigInfo                                */
/* Input(s)           : pMrpConfigInfo -Mrp Configuration info             */
/* Output(s)          : None                                               */
/* Returns            : OSIX_SUCCESS/OSIX_FAILURE                          */
/* Action             : This API configure the MRP module                  */
/***************************************************************************/
INT4
MrpApiMrpConfigInfo (tMrpConfigInfo * pMrpConfigInfo)
{
    UNUSED_PARAM (pMrpConfigInfo);
    return OSIX_SUCCESS;
}
#endif

#ifndef VCM_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmMapPortExt                                      */
/*                                                                           */
/*     DESCRIPTION      : This is an API to create a mapping of interface to */
/*                        virtual context by creating an entry in IfMap table*/
/*                                                                           */
/*     INPUT            : u4IfIndex  - Interface to Get Map.                 */
/*                        pu1Alias   - Alias name of the context.            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmMapPortExt (UINT4 u4IfIndex, UINT1 *pu1Alias)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1Alias);
    return VCM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmUnMapPortExt                                    */
/*                                                                           */
/*     DESCRIPTION      : This is an API to  delete mapping of interface     */
/*                        from virtual context by deleting an entry in       */
/*                        IfMap table                                        */
/*     INPUT            : u4IfIndex  - Interface to Get Map.                 */
/*                        pu1Alias   - Alias name of the context.            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmUnMapPortExt (UINT4 u4IfIndex, UINT1 *pu1Alias)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1Alias);
    return VCM_SUCCESS;
}
#endif /* VCM_WANTED */

#ifndef CFA_WANTED
/*****************************************************************************
*
*    Function Name             : CfaGetGlobalModTrc
*
*    Description               : This function is used to Get the Global variable for Trace events.
*                                Here Array of 1 represents Module level trace and
*                                Array of 0 represents Link level trace
*
*    Input(s)                  : None
*
*    Output(s)                 : gau4ModTrcEvents
*
*    Global Variables Referred : gau4ModTrcEvents
*
*    Returns                   : gau4ModTrcEvents[0]
*******************************************************************************/
UINT4
CfaGetGlobalModTrc ()
{
    return CFA_SUCCESS;
}

/*****************************************************************************
 *  Name               : CfaIwfMainACValidate
 * 
 * Description        : This function is called to validate the port and cvlan
 *                      of the attachment circuit.
 * 
 * Input(s)           :  u4PhyIfIndex - Interface id
 *                       u2VlanId  - Vlan id
 * 
 * Output(s)          : pu4ACIfIndex - attchment circuit interface id
 *****************************************************************************/
INT4
CfaIwfMainACValidate (UINT4 u4PhyIndex, tVlanId u2VlanId, UINT4 *pu4ACIfIndex)
{
    UNUSED_PARAM (u4PhyIndex);
    UNUSED_PARAM (u2VlanId);
    UNUSED_PARAM (pu4ACIfIndex);
    return CFA_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaDeleteInterfaceExt                              */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the given interface          */
/*                                                                           */
/*     INPUT            : u4IfIndex - Index of the interface to be deleted   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
CfaDeleteInterfaceExt (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    return (CFA_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateAndWaitPhysicalInterface                  */
/*                                                                           */
/*     DESCRIPTION      : This is API to create an entry in IF Port table.   */
/*                        It keeps the port entry row status as in NOT READY */
/*                        state                                              */
/*     INPUT            : pu1IfName - Porrt IfIndex name                     */
/*                      : pu1IfNum -  Port number (slotId/portId)            */
/*                      : pu4IfIndex - Port IfIndex value                    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateAndWaitPhysicalInterface (UINT1 *pu1IfName,
                                   UINT1 *pu1IfNum, UINT4 *pu4IfIndex)
{
    UNUSED_PARAM (pu1IfName);
    UNUSED_PARAM (pu1IfNum);
    UNUSED_PARAM (pu4IfIndex);

    return CFA_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaActivatePhysicalInterface                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the ifIndex entry created       */
/*                        externally to active                               */
/*     INPUT            : u4IfIndex - Port IfIndex                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaActivatePhysicalInterface (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetInterfaceNameFromIfType                      */
/*                                                                           */
/*     DESCRIPTION      : This function fetches the interface name for a     */
/*                        interface IfType                                   */
/*     INPUT            : u4IfType - IfType for which ifName to be returned  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaGetInterfaceNameFromIfType (INT1 *pi1IfName, UINT4 u4IfType)
{
    UNUSED_PARAM (u4IfType);
    UNUSED_PARAM (pi1IfName);

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2dsUtilIsDhcpSnoopingEnabled                        */
/*                                                                           */
/* Description        : This function checks if Dhcp snooping feature is     */
/*                      enabled in the system or not.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
L2dsUtilIsDhcpSnoopingEnabled (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return L2DS_DISABLED;
}

/*****************************************************************************
 *
 *    Function Name        : CfaValidateIfIndex
 *
 *    Description        : Provides interface to the external modules for 
 *                         validation (interface exists and its CFA MIB's 
 *                         ifMainRowStatus is ACTIVE) of the MIB-2 ifIndex 
 *                         given to an interface. 
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : gapIfTable.
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if the ifIndex is valid,
 *                otherwise CFA_FAILURE.
 *****************************************************************************/
INT4
CfaValidateIfIndex (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaGetDefaultRouterIfIndex
 *
 *    Description          : This function gets the index of the subnetmask
 *                           from the cidr table corresponding to the given
 *                           subnet mask. Assumption of this function is that
 *                           it is called with valid subnet.
 *
 *    Input(s)             : u4SubnetMask
 *
 *    Output(s)            : None.
 *
 *    Returns              : Index of the subnetmask
 ******************************************************************************/

UINT4
CfaGetDefaultRouterIfIndex (VOID)
{
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfIpPort                                       */
/* Description        : This function gets the IP port of the interface      */
/* Input(s)           : u4IfIndex:  Interface index                          */
/* Referred           : None                                                 */
/* Modified           : None                                                 */
/* Return Value(s)    : Ip port number                                       */
/*****************************************************************************/
INT4
CfaGetIfIpPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_INVALID_INDEX;
}

/******************************************************************************
 * Function       : CfaUpdtIvrInterface
 *
 * Description    : This function will update the h/w for the L3_IPVLAN interfaces
 *                  based on the Status of the IVR interface.
 *
 * Input(s)       : u4IfIndex - Interface index of the L3_IPVLAN interface.
 *                  u1Status  - CFA_IF_CREATE/CFA_IF_DELETE.
 *
 * Output(s)      : None.
 *
 * Returns        : CFA_SUCCESS/FAILURE
 *
 ******************************************************************************/

INT4
CfaUpdtIvrInterface (UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaNotifyProtoToApp                                  */
/* Description        : Notify the Appilcations                              */
/*                                                                           */
/* Input(s)           : u1ModeId - Specifies from which module the           */
/*                      notication is given.                                 */
/*                      It can take the values -                             */
/*                      LA_NOTIFY - when indictaion is send from LA          */
/*                      VLAN_NOTIFY - when indication is from VLAN           */
/*                      RSTP_NOTIFY -when indication is from RSTP            */
/*                      MSTP_NOTIFY - when indication is from MSTP           */
/*                      and so on                                            */
/*                      NotifyProtoToApp - contains the indications specific */
/*                      to the notification.                                 */
/* Output(s)          : None.                                                */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
CfaNotifyProtoToApp (UINT1 u1ModeId, tNotifyProtoToApp NotifyProtoToApp)
{
    UNUSED_PARAM (u1ModeId);
    UNUSED_PARAM (NotifyProtoToApp);
    return;
}

/*****************************************************************************
*    Function Name            : CfaIpIfCreateIpInterface
*
*    Description              :  Creates IP interface node for the specified
*                                interface
*
*    Input(s)                  : u4CfaIfIndex - CFA Interface Index
*
*    Output(s)                 : None.
*
*    Global Variables Referred : None
*
*    Returns                   : CFA_SUCCESS/CFA_FAILURE
*****************************************************************************/
INT4
CfaIpIfCreateIpInterface (UINT4 u4CfaIfIndex)
{
    UNUSED_PARAM (u4CfaIfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfName                                         */
/*                                                                           */
/* Description        : This function gets the interface name from the given */
/*                      interface index.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : au1IfName:Pointer to the interface name.             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfName (UINT4 u4IfIndex, UINT1 *au1IfName)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (au1IfName);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfBridgedIfaceStatus                           */
/*                                                                           */
/* Description        : This function gets the bridged interface status      */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index.                           */
/*                                                                           */
/* Output(s)          : pu1Status:Pointer to the  status.                    */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfBridgedIfaceStatus (UINT4 u4IfIndex, UINT1 *pu1Status)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1Status);
    return CFA_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : CfaCheckBrgPortType                                     */
/*                                                                            */
/* DESCRIPTION      : This function validates the given ifIndex is CFA_ENET or*/
/*                    CFA_LAGG or CFA_PSEUDO_WIRE.                             */
/*                                                                            */
/* INPUT            : u4IfIndex -Interface Index.                             */
/*                                                                            */
/* OUTPUT           : NONE                                                    */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/
INT4
CfaCheckBrgPortType (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIsPortVlanExists                                   */
/*                                                                           */
/* Description        : This function function is used to check whether the  */
/*                      the VLAN ID passed is already used by cfa to achieve  */
/*                      Router-Ports or not.                                 */
/*                                                                           */
/* Input(s)           : u2PortVlanId  :VLAN ID which is to be checked        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT4
CfaIsPortVlanExists (UINT2 u2PortVlanId)
{
    UNUSED_PARAM (u2PortVlanId);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : CfaApiGetPortDesc
 *
 *    DESCRIPTION      : This function is called from VLAN module
 *                       to get the port name.
 *
 *    INPUT            : u4ContextId - Context ID.
 *                     : u4IfIndex   - Interface Index
 *    OUTPUT           : pPortDesc -PortDesc
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/
INT4
CfaApiGetPortDesc (UINT4 u4IfIndex, tSNMP_OCTET_STRING_TYPE * pPortDesc)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pPortDesc);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaTestv2IfMainRowStatus
 *
 *    Description          : This function is called from PBB to test destroy of
 *                           VIP ifindex rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.
 *
 *****************************************************************************/
INT4
CfaTestv2IfMainRowStatus (UINT4 *pu4ErrCode, INT4 u4IfIndex, INT4 u4RowStatus)
{
    UNUSED_PARAM (pu4ErrCode);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4RowStatus);
    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : CfaIfmConfigNetworkInterface
 *
 *    Description        : This function is for registration of
 *                interface with IP and updation of
 *                registration information in the ifTable.
 *
 *    Input(s)            : UINT1 u1OperCode,
 *                UINT4 u4IfIndex,
 *                u4Flag  - Parameters to be configured for the interface
 *                pIpConfigInfo -IP information for the interface
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : IfTable (gapIfTable) structure.
 *
 *    Global Variables Modified : IfTable (gapIfTable) structure.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS if configuration succeeds,
 *                otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaIfmConfigNetworkInterface (UINT1 u1OperCode, UINT4 u4IfIndex,
                              UINT4 u4Flag, tIpConfigInfo * pIpConfigInfo)
{
    UNUSED_PARAM (u1OperCode);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Flag);
    UNUSED_PARAM (pIpConfigInfo);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaIsOpenflowPort
 *
 *    Description         : This function verifies whether given interface is
 *                          Openflow Interface
 *
 *    Input(s)            : Interface Index
 *
 *    Output(s)           : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 *
 *****************************************************************************/
INT4
CfaIsOpenflowPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfType                                         */
/*                                                                           */
/* Description        : This function gets the interface type                */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1IfType:Pointer to the Interface type.             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfType (UINT4 u4IfIndex, UINT1 *pu1IfType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1IfType);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaConfigBaseBridgeMode
 *
 *    Description          : This function posts a Message to CFA Task
 *                           for Configuring the default router port and
 *                           create/delete IVR interfaces.
 *
 *    Input(s)             : None
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns          : CFA_SUCCESS / CFA_FAILURE.
 *****************************************************************************/
INT4
CfaConfigBaseBridgeMode (UINT1 u1BaseBridgeMode)
{
    UNUSED_PARAM (u1BaseBridgeMode);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaCheckNonPhysicalInterfaces
 *
 *    Description        : This function checks whether non physical
 *                         interfaces are present
 *
 *    Input(s)            : None
 *
 *    Output(s)            : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : CFA_SUCCESS - if no non-physical ports are present
 *                         CFA_FAILURE - if any 1 non-physical port is present.
 *
 *****************************************************************************/
INT4
CfaCheckNonPhysicalInterfaces ()
{
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaIsThisOpenflowVlan                                */
/*                                                                           */
/* Description        : This function checks whether the given Vlan is an    */
/*                      Openflow Vlan or not.                                */
/*                                                                           */
/* Input(s)           : VlanId                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_TRUE/ CFA_FALSE                                  */
/*****************************************************************************/

BOOL1
CfaIsThisOpenflowVlan (UINT4 u4VlanId)
{
    UNUSED_PARAM (u4VlanId);
    return CFA_TRUE;
}

/******************************************************************************
 * Function           : CfaGetIfaceType
 * Input(s)           : u4IfIndex - Index of the interface
 * Output(s)          : IfType
 * Returns            : TRUE/FALSE
 * Action             : This routine returns the type of the incoming interface
 ******************************************************************************/
INT4
CfaGetIfaceType (UINT4 u4IfIndex, UINT1 *pu1IfType)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1IfType);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaSetIfMainRowStatus
 *
 *    Description          : This function is called from PBB to set destroy of
 *                           VIP ifindex rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.
 *
 *****************************************************************************/

INT4
CfaSetIfMainRowStatus (INT4 i4IfIndex, INT4 i4RowStatus)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (i4RowStatus);
    return CFA_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : CfaGetSlotAndPortFromIfIndex
 *
 *    Description          : This function is used to get slot and
 *                           port form Interface Index value
 *
 *    Input(s)             : u4IfIndex - Interface Index
 *
 *    Output(s)            : pi4SlotNum - Slot Number
 *                           pi4PortNum - PortNumber
 *
 *    Returns              : CFA_SUCCESS / CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaGetSlotAndPortFromIfIndex (UINT4 u4IfIndex, INT4 *pi4SlotNum,
                              INT4 *pi4PortNum)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi4SlotNum);
    UNUSED_PARAM (pi4PortNum);
    return CFA_SUCCESS;

}

/******************************************************************************
 * Function           : CfaGetRetrieveNodeState
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : Node state.
 * Action             : Routine to get the node state.
 ******************************************************************************/
UINT4
CfaGetRetrieveNodeState ()
{
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaCreateSChannelInterface
 *
 *    Description         : This function creates S-Channel interface in CFA
 *                          and thereby indicating to L2IWF to create the
 *                          S-Channel interface to VLAN, IGMP Snooping and
 *                          FIP Snooping.
 *
 *    Input(s)            : u4IfIndex - S-Channel interface index.
 *                          u4UapIfIndex - UAP interface index.
 *
 *    Output(s)           : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaCreateSChannelInterface (UINT4 u4UapIfIndex, UINT4 u4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4UapIfIndex);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Status);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaDeleteSChannelInterface
 *
 *    Description         : This function creates S-Channel interface in CFA
 *                          and thereby indicating to L2IWF to create the
 *                          S-Channel interface to VLAN, IGMP Snooping and
 *                          FIP Snooping.
 *
 *    Input(s)            : u4IfIndex - S-Channel interface index.
 *
 *    Output(s)           : None.
 *
 *    Returns            : CFA_SUCCESS/CFA_FAILURE
 *
 *****************************************************************************/
INT4
CfaDeleteSChannelInterface (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetFreeSChIndexForUap
 *
 *    Description         : This function returns the first available
 *                          free interface index from SBP interface pool for
 *                          the provided UAP interface index.
 *
 *    Input(s)            : u1UapIfIndex - UAP Interface Index.
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None.
 *
 *    Returns            : CFA_SUCCESS if free index is available
 *                         otherwise CFA_FAILURE.
 *
 *****************************************************************************/
INT4
CfaGetFreeSChIndexForUap (UINT4 u4UapIfIndex, UINT4 *pu4Index)
{
    UNUSED_PARAM (u4UapIfIndex);
    UNUSED_PARAM (pu4Index);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : CfaSetIfMainBrgPortType
 *
 *    Description          : This function is called from PBB to set Bridge port type
 *                           VIP ifindex
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.
 *
 *****************************************************************************/

INT1
CfaSetIfMainBrgPortType (INT4 i4IfMainIndex, INT4 i4SetValIfMainBrgPortType)
{
    UNUSED_PARAM (i4IfMainIndex);
    UNUSED_PARAM (i4SetValIfMainBrgPortType);
    return CFA_SUCCESS;
}

#endif /* CFA_WANTED */
#ifndef SNMP_3_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : SnmpSetSysName                                   */
/*                                                                          */
/*    Description        : This function is invoked to set the System-name  */
/*                                                                          */
/*    Input(s)           : pSwitchName -pointer to the switch name          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS or SNMP_FAILURE.                    */
/****************************************************************************/
INT4
SnmpApiSetSysName (tSNMP_OCTET_STRING_TYPE * pSwitchName)
{
    UNUSED_PARAM (pSwitchName);
    return SNMP_SUCCESS;

}
#endif /*SNMP_3_WANTED */
#ifndef RSTP_WANTED
/******************************************************************************/
/*  Function           : AstIndicateSTPStatus                                 */
/*  Input(s)           : u2PortNum -port number, and stp status               */
/*  Output(s)          : None                                                 */
/*  Returns            : Hitless restart flag value.                          */
/*  Action             : This function is called to indicate                  */
/*                       STP ENABLE/DISABLE status to VLAN module.            */
/******************************************************************************/
VOID
AstIndicateSTPStatus (UINT4 u4PortNum, BOOL1 STPStatus)
{
    UNUSED_PARAM (u4PortNum);
    UNUSED_PARAM (STPStatus);
    return;
}
#endif
#ifndef DHCP_RLY_WANTED
/************************************************************************/
/*  Function Name   : DhcpGetRelayStatus                                */
/*  Description     : Gets the DHCP Relay Status                        */
/*  Input(s)        : None                                              */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_ENABLE/DHCP_DISABLE                          */
/************************************************************************/
BOOLEAN
DhcpIsRelayEnabled ()
{
    return DHCP_DISABLE_STATUS;
}
#endif
#ifndef  RM_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRMGetNodeStatus                               */
/*                                                                          */
/*    Description        : This function returns the RM node                */
/*                           status.                                        */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : returns RM_ACTIVE/RM_STANDBY                     */
/****************************************************************************/
UINT1
MsrRMGetNodeStatus (VOID)
{
    return RM_ACTIVE;
}
#endif

#ifndef CLI_WANTED
/*****************************************************************************/
/* Function Name      : CliUtilMgmtLockStatus                                */
/*                                                                           */
/* Description        : This util is used to set the global Boolean variable */
/*                      MGMT_LOCK is acquired and its is set false when      */
/*                      MGMT_LOCK is released.                               */
/*                                                                           */
/* Input(s)           : b1Flag                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
CliUtilMgmtLockStatus (BOOL1 b1Flag)
{
    UNUSED_PARAM (b1Flag);
    return;
}

/*****************************************************************************/
/* Function Name      : CliUtilMgmtClearLockStatus                          */
/*                                                                           */
/* Description        : This util is used to clear the mgmt lock status if   */
/*                      the mgmt lock is already acquired.                   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
CliUtilMgmtClearLockStatus ()
{
    return;
}

#endif
#ifndef ISSU_WANTED
/*****************************************************************************/
/* Function Name      : IssuGetMaintModeOperation                            */
/*                                                                           */
/* Description        : This function is called to decide whether            */
/*                      miantenance mode operation is to be performed or not.*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE or OSIX_FALSE                              */
/*****************************************************************************/
UINT4
IssuGetMaintModeOperation (VOID)
{
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : IssuGetMaintenanceMode                               */
/*                                                                           */
/* Description        : This function is an utility to check whether         */
/*                      maintenance mode is set or not                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : i4FsIssuMaintenanceMode                              */
/*****************************************************************************/
INT4
IssuGetMaintenanceMode (VOID)
{
    return ISSU_MAINTENANCE_MODE_DISABLE;
}

/*****************************************************************************/
/* Function Name      : IssuIsOidAllowed                                     */
/*                                                                           */
/* Description        : This function is called from SNMP module to          */
/*                      check whether the given OID is allowed when          */
/*                      maintenance mode is enabled                          */
/* Input(s)           : MIB OID                                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS / ISSU_FAILURE                          */
/*****************************************************************************/
INT4
IssuIsOidAllowed (tSNMP_OID_TYPE OID)
{
    UNUSED_PARAM (OID);

    return ISSU_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssuIsCliCmdAllowed                                  */
/*                                                                           */
/* Description        : This function is called from CLI module to           */
/*                      check whether the given CLI command is allowed when  */
/*                      maintenance mode is enabled                          */
/* Input(s)           : Cli command string.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS if command is allowed,                  */
/*                      ISSU_FAILURE if command is not allowed               */
/*****************************************************************************/
INT4
IssuIsCliCmdAllowed (INT1 *pi1Command)
{
    UNUSED_PARAM (pi1Command);

    return ISSU_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssuIsCliCmdRestricted                               */
/*                                                                           */
/* Description        : This function is called from CLI module to           */
/*                      check whether the given CLI command is restricted    */
/*                      when maintenance mode is enabled. The command must   */
/*                      be of priority one or less than one                  */
/* Input(s)           : Cli command string.                                  */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS if command is allowed,                  */
/*                      ISSU_FAILURE if command is not allowed.              */
/*****************************************************************************/
INT4
IssuIsCliCmdRestricted (INT1 *pi1Command)
{
    UNUSED_PARAM (pi1Command);
    return ISSU_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssuIsHwProgrammingAllowed                           */
/*                                                                           */
/* Description        : This function return whether NP Programming is       */
/*                      allowed depending on the ISSU status                 */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE or OSIX_FALSE                              */
/*****************************************************************************/
UINT4
IssuIsHwProgrammingAllowed (VOID)
{
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function Name      : IssuIsOidAllowedInStandby                            */
/*                                                                           */
/* Description        : This function is called from SNMP module to          */
/*                      check whether the given OID is allowed for snmpset   */
/*                      in standby node                                      */
/* Input(s)           : MIB OID                                              */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_SUCCESS / ISSU_FAILURE                          */
/*****************************************************************************/
INT4
IssuIsOidAllowedInStandby (tSNMP_OID_TYPE OID)
{
    UNUSED_PARAM (OID);
    return ISSU_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssuApiIsLoadVersionTriggered                        */
/*                                                                           */
/* Description        : This function is called from HB module to check      */
/*                      whether loadversion triggred or not                  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    :OSIX_TRUE/OSIX_FALSE                                  */
/*****************************************************************************/
UINT1
IssuApiIsLoadVersionTriggered (VOID)
{
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function Name      : IssuApiPerformLoadVersionNPCmds                      */
/*                                                                           */
/* Description        : This function is called from MSR module              */
/*                      to program the BCM for graceful shutdown             */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISSU_FAILURE/ISSU_SUCCESS                            */
/*****************************************************************************/
INT4
IssuApiPerformLoadVersionNPCmds (VOID)
{
    return ISSU_SUCCESS;
}

#endif /* ISSU_WANTED */

#if !defined(ISS_WANTED) && !defined(WSS_WANTED)

/*************************************************************************/
/* Function Name : SNMP_ReverseOctetString                               */
/* Description   : Reverse the Bit positions in pInOctetStr and store it */
/*                 in pOutOctetStr                                       */
/* Input(s)      : pInOctetStr - Input octetstring                       */
/* Output(s)     : pOutOctetStr - output octetstring                     */
/* Returns       : None                                                  */
/*************************************************************************/

VOID
SNMP_ReverseOctetString (tSNMP_OCTET_STRING_TYPE * pInOctetStr,
                         tSNMP_OCTET_STRING_TYPE * pOutOctetStr)
{
    UINT4               u4Count = 0;
    UINT4               u4MaxBits = 0;
    UINT4               u4RevBitNum = 0;
    INT4                i4MaxBytes = 0;
    BOOL1               bResult = OSIX_FALSE;

    /* Example
     *             BIT Positions 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
     * pInOctetStr:              1 1 1 . . . . .               .  .  1
     * pOutOctetStr:             1 . . . . .     . .  .  .  .  1  1  1
     */

    i4MaxBytes = pInOctetStr->i4_Length;
    pOutOctetStr->i4_Length = i4MaxBytes;

    u4MaxBits = (UINT4) (i4MaxBytes * BITS_PER_BYTE);

    for (u4Count = 1; u4Count <= u4MaxBits; u4Count++)
    {
        OSIX_BITLIST_IS_BIT_SET (pInOctetStr->pu1_OctetList, u4Count,
                                 i4MaxBytes, bResult);
        if (bResult == OSIX_TRUE)
        {
            u4RevBitNum = (u4MaxBits + 1) - u4Count;
            OSIX_BITLIST_SET_BIT (pOutOctetStr->pu1_OctetList, u4RevBitNum,
                                  i4MaxBytes);
        }
    }
    return;
}

/************************************************************************
 *  Function Name   : alloc_oid
 *  Description     : function to allocate oid
 *  Input           : i4Size - size of oid to be allocated.
 *                    actual lenght in byte = i4Size * sizeof(UINT4)
 *  Output          : None
 *  Returns         : Oid pointer or null
 ************************************************************************/
tSNMP_OID_TYPE     *
alloc_oid (INT4 i4Size)
{
    tSNMP_OID_TYPE     *pOid = NULL;

    if (gi4SnmpMemInit == SNMP_FAILURE)
    {
        return NULL;
    }

    if (i4Size > SNMP_MAX_OID_LENGTH)
    {
        return NULL;
    }
    if ((pOid = MemAllocMemBlk (gSnmpOidTypePoolId)) == NULL)
    {
        return NULL;
    }
    if ((pOid->pu4_OidList = MemAllocMemBlk (gSnmpOidListPoolId)) == NULL)
    {
        MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *) pOid);
        return NULL;
    }
    pOid->u4_Length = (UINT4) i4Size;
    return pOid;
}

/************************************************************************
 *  Function Name   : free_oid
 *  Description     : function to free oid pointer
 *  Input           : pOid - oid pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
free_oid (tSNMP_OID_TYPE * pOid)
{
    if (pOid != NULL)
    {
        if (pOid->pu4_OidList != NULL)
        {
            MemReleaseMemBlock (gSnmpOidListPoolId, (UINT1 *)
                                (pOid->pu4_OidList));
        }
        MemReleaseMemBlock (gSnmpOidTypePoolId, (UINT1 *) pOid);
    }
}

/*****************************************************************************
 *
 * Function     : SnmpUtilDecodeOid
 *
 * Description  : This function decode the ASN.1 encoded oid string. OID string
 *                in the management address TLV are encoded in ASN.1 format.
 *                This function helps to decode that OID.
 *
 * Input        : pu1InOidStr - ASN.1 encode OID string
 *
 * Output       : pOutOidStr  - Decoded OID string
 *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
SnmpUtilDecodeOid (UINT1 *pu1InOidStr, tSNMP_OID_TYPE * pOutOidStr)
{
    INT4                i4Seq = 0;
    INT4                i4Len = 0;
    UINT4               u4Split = 0;

    i4Len = *pu1InOidStr++;
    pOutOidStr->u4_Length = 0;
    pOutOidStr->pu4_OidList[pOutOidStr->u4_Length] = 0;

    for (i4Seq = 0; i4Seq < i4Len; i4Seq++)
    {
        if (pOutOidStr->u4_Length < LLDP_MAX_LEN_MAN_OID)
        {
            pOutOidStr->pu4_OidList[pOutOidStr->u4_Length] =
                (pOutOidStr->pu4_OidList[pOutOidStr->u4_Length] << 7) +
                (*pu1InOidStr & 0x7f);
            if ((*pu1InOidStr++ & 0x80) == 0)
            {
                if (pOutOidStr->u4_Length == 0)
                {
                    if (pOutOidStr->pu4_OidList[pOutOidStr->u4_Length] >
                        (UINT4) 79)
                    {
                        /* If the first sub-id value is greater than 79, then the first
                         * OID component is 2. Because, while encoding there is one
                         * condition that, if the first OID component is 0 or 1, then
                         * the second component should be within the range of 0 to 39.
                         * So the First sub-id value cannot exceed 79 if the first OID
                         * component is 0 or 1*/
                        u4Split =
                            pOutOidStr->pu4_OidList[pOutOidStr->u4_Length];
                        pOutOidStr->pu4_OidList[pOutOidStr->u4_Length++] = 2;
                        pOutOidStr->pu4_OidList[pOutOidStr->u4_Length++] =
                            u4Split - (2 * 40);
                    }
                    else
                    {
                        u4Split =
                            pOutOidStr->pu4_OidList[pOutOidStr->u4_Length];
                        pOutOidStr->pu4_OidList[pOutOidStr->u4_Length++] =
                            u4Split / 40;
                        pOutOidStr->pu4_OidList[pOutOidStr->u4_Length++] =
                            u4Split % 40;
                    }
                }
                else
                {
                    pOutOidStr->u4_Length++;
                }
                if (i4Seq < i4Len - 1)
                {
                    pOutOidStr->pu4_OidList[pOutOidStr->u4_Length] = 0;
                }
            }
            else
            {
                if ((i4Seq + 1) >= i4Len)
                {
                    /* This check is required for the following scenario. Assume the
                     * OID length is wrongly sent; So there may be a possibility that
                     * the sub-identifier cannot be decoded correctly. In that case
                     * We have to decode the value as 0.
                     * Eg: OID Length = 1 & OID Encoded string is 0x81.
                     * Since the Most significant bit of 0x81 is 1, we can know that
                     * it is a multi-byte encoded string; but the length is mentioned
                     * as 1. Length should actually to be greater than 1. We cannot
                     * decode it correctly. So decoding this value to 0 */

                    pOutOidStr->pu4_OidList[pOutOidStr->u4_Length++] = 0;
                }
            }
        }
        else
        {
            (*pu1InOidStr)++;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 * Function     : SnmpUtilEncodeOid
 *
 * Description  : This function encode the OID list to ASN.1 formatted OID string
 *                In Management address TLV OID is stored as ASN.1 encoded
 *                format. During construction of Management Address TLV this
 *                function is called to encode the OID.
 *
 * Input        : pInOidStr - OID string
 *
 * Output       : pu1OutOidStr - ASN.1 encode OID String
 *                u1ErrorCode  - Error code to identify the reason for Encoding
 *                               failure.
 *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE
 *
 *****************************************************************************/
PUBLIC INT4
SnmpUtilEncodeOid (tSNMP_OID_TYPE * pInOidStr, UINT1 *pu1OutOidStr,
                   UINT1 *u1ErrorCode)
{
    UINT4               u4Cnt = 0, u4Check = 0, u4Count = 0;
    INT4                i4ByteCount = 0;
    UINT4               u4Index = 0;
    UINT1               au1Buf[LLDP_MAX_LEN_MAN_OID] = { 0 };
    UINT4               u4Byte0 = 0, u4Byte1 = 0;
    UINT1               u1LenFlag = OSIX_FALSE;

    if (pInOidStr->u4_Length == 0)
    {
        return OSIX_FAILURE;
    }
    /* Preserve the byte 0 and byte 1 of the Input OID as we change these bytes
     * during encoding; These bytes have to be restored after the completion of
     * encoding */
    u4Byte0 = pInOidStr->pu4_OidList[0];
    u4Byte1 = pInOidStr->pu4_OidList[1];

    if ((INT4) u4Byte0 >= 0 && (INT4) u4Byte0 <= 2)
    {
        if (((INT4) u4Byte0 == 0) || ((INT4) u4Byte0 == 1))
        {
            if (!(((INT4) u4Byte1 >= 0) && ((INT4) u4Byte1 <= 39)))
            {
                *u1ErrorCode = 1;
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        *u1ErrorCode = 2;
        return OSIX_FAILURE;
    }
    if (pInOidStr->u4_Length == 1)
    {
        pInOidStr->u4_Length++;
        u1LenFlag = OSIX_TRUE;
    }
    pInOidStr->pu4_OidList[1] =
        (pInOidStr->pu4_OidList[0] * 40) + pInOidStr->pu4_OidList[1];

    /* We start encoding of the OID from the last sub-identifier;
     * Array Index starts from 0; So u4Cnt Initialised to "pInOidStr->u4Length - 1"
     * All the bytes should be encoded as per ASN.1 BER encoding; even if the first
     * subidentifier (i.e. oid_component1 * 40 + oid_component2) exceeds the value of
     * 127, then that also should be encoded as multi-byte string*/
    for (u4Cnt = pInOidStr->u4_Length - 1; u4Cnt >= 1; u4Cnt--)
    {
        au1Buf[u4Index++] = (UINT1) (pInOidStr->pu4_OidList[u4Cnt] & 0x7f);
        u4Count = 1L;
        u4Check = 0x80;
        while ((pInOidStr->pu4_OidList[u4Cnt] >= u4Check) && (u4Count < 5))
        {
            u4Check = (UINT4) (u4Check << 7);
            au1Buf[u4Index++] = (UINT1) (((pInOidStr->pu4_OidList[u4Cnt])
                                          >> (7 * u4Count)) | 0x80);
            u4Count++;

            if ((u4Index > LLDP_MAX_LEN_MAN_OID) ||
                ((u4Index == LLDP_MAX_LEN_MAN_OID) &&
                 (pInOidStr->pu4_OidList[u4Cnt] >= u4Check) && (u4Count < 5)))
            {
                *u1ErrorCode = 3;
                pInOidStr->pu4_OidList[0] = u4Byte0;
                pInOidStr->pu4_OidList[1] = u4Byte1;
                if (u1LenFlag == OSIX_TRUE)
                {
                    pInOidStr->u4_Length = 1;
                }
                return OSIX_FAILURE;
            }
        }
        if ((u4Index > LLDP_MAX_LEN_MAN_OID)
            || ((u4Index == LLDP_MAX_LEN_MAN_OID) && ((u4Cnt - 1) >= 1)))
        {
            *u1ErrorCode = 3;
            pInOidStr->pu4_OidList[0] = u4Byte0;
            pInOidStr->pu4_OidList[1] = u4Byte1;
            if (u1LenFlag == OSIX_TRUE)
            {
                pInOidStr->u4_Length = 1;
            }
            return OSIX_FAILURE;
        }
    }
    /* Fill the last byte as the number of bytes that has been encoded
     * It will be required to decode the packet accordingly */
    au1Buf[u4Index] = (UINT1) u4Index;

    /* The encoding was done in a reverse manner. So finally reversing
     * the entire bytes to make it proper */
    for (i4ByteCount = (INT4) u4Index; i4ByteCount >= 0; i4ByteCount--)
    {
        *(pu1OutOidStr) = au1Buf[i4ByteCount];
        pu1OutOidStr += 1;
    }

    /* Restoring the byte 0 and byte 1 to the original OID array */
    pInOidStr->pu4_OidList[0] = u4Byte0;
    pInOidStr->pu4_OidList[1] = u4Byte1;
    if (u1LenFlag == OSIX_TRUE)
    {
        pInOidStr->u4_Length = 1;
    }

    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : allocmem_octetstring
 *  Description     : function to allocate octet string
 *  Input           : i4Size - size of octet string
 *  Output          : None
 *  Returns         : octet string pointer or null
 ************************************************************************/
tSNMP_OCTET_STRING_TYPE *
allocmem_octetstring (INT4 i4Size)
{
    tSNMP_OCTET_STRING_TYPE *pOctet = NULL;
    if (gi4SnmpMemInit == SNMP_FAILURE)
    {
        return NULL;
    }
    if ((pOctet = MemAllocMemBlk (gOctetStrPoolId)) == NULL)
    {
        return NULL;
    }
    if ((pOctet->pu1_OctetList = MemAllocMemBlk (gSnmpOctetListPoolId)) == NULL)
    {
        MemReleaseMemBlock (gOctetStrPoolId, (UINT1 *) (pOctet));
        return NULL;
    }
    MEMSET (pOctet->pu1_OctetList, 0, sizeof (tSnmpOctetListBlock));
    pOctet->i4_Length = i4Size;
    AllocBlocks++;
    return pOctet;
}

/************************************************************************
 *  Function Name   : free_octetstring
 *  Description     : Function to free octet string
 *  Input           : pOctetStr - Octet string pointer
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
free_octetstring (tSNMP_OCTET_STRING_TYPE * pOctetStr)
{

    if (pOctetStr != NULL)
    {
        if (pOctetStr->pu1_OctetList != NULL)
        {
            MemReleaseMemBlock (gSnmpOctetListPoolId, (UINT1 *)
                                (pOctetStr->pu1_OctetList));
        }
        MemReleaseMemBlock (gOctetStrPoolId, (UINT1 *) pOctetStr);
        AllocBlocks--;

    }
}

#endif /* ISS_WANTED & WSS_WANTED */
#if !defined(LLDP_WANTED) && !defined(VLAN_WANTED) && !defined(ECFM_WANTED)
/*****************************************************************************/
/* Function Name        : L2IwfGetPortVlanEntry                              */
/*                                                                           */
/* Description          : Returns the pointer to the PortVlanEntry for  the  */
/*                        given vlan & port ID. Lock should be taken before  */
/*                        calling this function.                             */
/*                                                                           */
/* Input (s)            : u2VlanId - VLAN ID value.                          */
/*                        u4IfIndex - PortIndex Value                        */
/*                                                                           */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : Pointer to the PortValn Entry.                     */
/*****************************************************************************/
tL2PortVlanInfo    *
L2IwfGetPortVlanEntry (UINT4 u4IfIndex, UINT2 u2VlanId)
{
    tL2PortVlanInfo    *pPortVlanEntry = NULL;
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2VlanId);

    return pPortVlanEntry;
}

#endif
#ifndef VLAN_WANTED
                                                                                                                                                                                                                                                                                                                                                                                                          /*****************************************************************************//* Function Name      : L2IwfGetUntaggedPortList                             */
/*                                                                           */
/* Description        : This function is used to mark the VLAN as            */
/*                      FCoE VLAN in L2IWF.                                  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId - Service VLAN Identifier                     */
/*                      u1FCoEVlanType - L2_VLAN / FCOE_VLAN                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfGetUntaggedPortList (UINT4 u4ContextId, tVlanId VlanId,
                          tLocalPortList LocalEgressPorts)
{

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (LocalEgressPorts);
    return L2IWF_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : VlanApiFlushFdbId                                       */
/*                                                                            */
/* Description      : This function flushes the Fdb entires in FdbId          */
/*                     for the given context                                  */
/*                                                                            */
/*  Input(s)        : u4ContextId -  Context Id                               */
/*                    u4FdbId - FDB Id                                        */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : VLAN_SUCCESS/VLAN_FAILURE                               */
/******************************************************************************/
INT4
VlanApiFlushFdbId (UINT4 u4ContextId, UINT4 u4FdbId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4FdbId);
    return VLAN_SUCCESS;
}
#endif
#ifndef CFA_WANTED
#ifdef NPAPI_WANTED
/*****************************************************************************
*
*    Function Name       : CfaPostPacketToNP
*
*    Description         : This function enqueues the STP packet to CfaAstPktTx
*                          task.
*
*    Input(s)            : pBuf - CRU Buffer containing pkt to be transmitted
*                          u4IfIndex - Interface index
*             u4PktSize - Packet size
*
*    Output(s)           : None
*
*    Global Variables Modified : None.
*
*    Exceptions or Operating
*    System Error Handling    : None.
*
*    Use of Recursion        : None.
*
*    Returns            : CFA_SUCCESS/CFA_FAILURE
*
*****************************************************************************/
INT4
CfaPostPacketToNP (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                   UINT4 u4PktSize)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktSize);
    return CFA_SUCCESS;
}
#endif
#endif
#endif
