/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2la.c,v 1.68 2018/01/11 11:19:19 siva Exp $
 *
 * Description:This file contains routines to get/set LA
 *             information from the L2IWF module.                 
 *
 *******************************************************************/

#include "l2inc.h"

/*****************************************************************************/
/* Function name      : L2IwfCreatePortIndicationFromLA                      */
/*                                                                           */
/* Description        : This function is invoked by LA, for creating the     */
/*                      physical ports that have come out of an aggregation, */
/*                      in other L2 modules. This function also updates the  */
/*                      Port's oper status as maintained in LA to Bridge and */
/*                      it's higher layers.                                  */
/*                                                                           */
/* Input(s)           : u4IfIndex - Index of the port to be created.         */
/*                      u1PortOperStatus - Port Oper status from LA          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfCreatePortIndicationFromLA (UINT4 u4IfIndex, UINT1 u1PortOperStatus)
{
    tIfTypeDenyProtocolList DenyProtocolList;
    tCfaIfInfo          IfInfo;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    INT4                i4RetVal = 0;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return;
    }
    /* for a router port ,there is no need to send notification to L2 modules */
    if (IfInfo.u1BridgedIface != CFA_ENABLED)
    {
        i4RetVal = CfaIpIfCreateIpInterface (u4IfIndex);
        UNUSED_PARAM (i4RetVal);
        return;
    }

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, &IfInfo,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        return;
    }

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_SUCCESS)
    {
#ifdef BRIDGE_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_BRIDGE] == OSIX_FALSE)
        {
            BridgeCreatePort (u4IfIndex, IfInfo.u1IfType, IfInfo.u4IfSpeed);
        }
#endif
#ifdef PBB_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PBB] == OSIX_FALSE)
        {
            PbbCreatePort (u4ContextId, (INT4) u4IfIndex,
                           (UINT4) u2LocalPortId);
        }
#endif /*PBB_WANTED */

        if (DenyProtocolList[L2IWF_PROTOCOL_ID_XSTP] == OSIX_FALSE)
        {

            if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
            {
                AstCreatePort (u4ContextId, u4IfIndex, u2LocalPortId);
            }
            else
            {
                AstCreatePortFromLa (u4ContextId, u4IfIndex, u2LocalPortId);
            }

        }
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_VLAN] == OSIX_FALSE)
        {
            VlanCreatePort (u4ContextId, u4IfIndex, u2LocalPortId);
        }

        if (DenyProtocolList[L2IWF_PROTOCOL_ID_GARP] == OSIX_FALSE)
        {
            GarpCreatePort (u4ContextId, u4IfIndex, u2LocalPortId);
        }
#ifdef MRP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_MRP] == OSIX_FALSE)
        {
            MrpApiCreatePort (u4ContextId, u4IfIndex, u2LocalPortId);
        }
#endif

#ifdef ELMI_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_ELMI] == OSIX_FALSE)
        {
            ElmCreatePort (u4IfIndex);
        }
#endif
#ifdef ECFM_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_ECFM] == OSIX_FALSE)
        {
            EcfmRemovePortFromPortChannel (u4IfIndex);
        }
#endif
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_SNOOP] == OSIX_FALSE)
        {
            SnoopCreatePort (u4ContextId, u4IfIndex, u2LocalPortId);
        }

#ifdef CN_WANTED
        if ((IfInfo.u1IfType == CFA_LAGG) || (IfInfo.u1IfType == CFA_ENET))
        {
            CnApiPortRequest (u4ContextId, u4IfIndex, CN_CREATE_IF_MSG);
        }
#endif /* CN_WANTED */

#ifdef RBRG_WANTED
        RbrgApiPortRequest (u4ContextId, u4IfIndex, RBRG_CREATE_PORT);
#endif

        /* Indicate the port status to bridge, STP, VLANGARP, IGS. */
#ifdef BRIDGE_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_BRIDGE] == OSIX_FALSE)
        {
            BridgePortOperStatusIndication (u4IfIndex, u1PortOperStatus);
        }
#else
        L2IwfBrgHLPortOperIndication (u4IfIndex, u1PortOperStatus);
#endif
    }
}

/*****************************************************************************/
/* Function Name      : L2IwfGetConfiguredPortsForPortChannel                */
/*                                                                           */
/* Description        : This routine returns the list of ports configured as */
/*                      members of the given PortChannel. It accesses the    */
/*                      L2Iwf common database                                */
/*                                                                           */
/* Input(s)           : AggId - Index of the aggregator                      */
/*                                                                           */
/* Output(s)          : au2ConfPorts    - Array of configured ports for the  */
/*                                        given aggregator                   */
/*                      u2NumPorts      - Number of member ports returned in */
/*                                        the array.                         */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetConfiguredPortsForPortChannel (UINT2 u2AggId, UINT2 au2ConfPorts[],
                                       UINT2 *pu2NumPorts)
{
    UINT2               u2Index;
    UINT2               u2NumPorts;

    *pu2NumPorts = 0;

    if ((u2AggId <= BRG_MAX_PHY_PORTS)
        || (u2AggId > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IWF_PORT_CHANNEL_VALID (u2AggId) == OSIX_TRUE)
    {
        u2NumPorts = L2IWF_PORT_CHANNEL_NUM_CONF_PORTS (u2AggId);

        for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
        {
            au2ConfPorts[u2Index] =
                L2IWF_PORT_CHANNEL_CONFIGURED_PORT (u2AggId, u2Index);
        }

        *pu2NumPorts = u2NumPorts;
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortChannelForPort                           */
/*                                                                           */
/* Description        : This routine returns the Port Channel of which the   */
/*                      given port is a member. It accesses the L2Iwf common */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose         */
/*                                  PortChannel Id to be obtained.           */
/*                                                                           */
/* Output(s)          : u2AggId     - Port Channel Index                     */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortChannelForPort (UINT4 u4IfIndex, UINT2 *pu2AggId)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    *pu2AggId = (UINT2) u4IfIndex;

    if (u4IfIndex > BRG_MAX_PHY_PORTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        *pu2AggId = (UINT2) L2IWF_IFENTRY_AGG_INDEX (pL2PortEntry);
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPortInPortChannel                             */
/*                                                                           */
/* Description        : This routine checks whether the port is a member of  */
/*                      any Port Channel and if so returns SUCCESS.          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    if (u4IfIndex > BRG_MAX_PHY_PORTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_IFENTRY_AGG_INDEX (pL2PortEntry) == u4IfIndex)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeleteAndAddPortToPortChannel                   */
/*                                                                           */
/* Description        : This routine is called from LA to add the given      */
/*                      port to the given PortChannel's configured portlist  */
/*                      in the L2Iwf common database                         */
/*                                                                           */
/* Input(s)           : u2AggId     - Index of the port channel to which the */
/*                                    port is to be added.                   */
/*                      u4IfIndex - Global Index of the port to be added to  */
/*                                  the Configured port list                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfDeleteAndAddPortToPortChannel (UINT4 u4IfIndex, UINT2 u2AggId)
{
    tIfTypeDenyProtocolList DenyProtocolList;
    tCfaLaInfoMsg       CfaLaInfoMsg;
    tL2PortInfo        *pL2PortEntry = NULL;
#if (defined(CN_WANTED) || (defined(RBRG_WANTED)))
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
#endif
    UINT2               u2Index;
    UINT1               u1SispStatus = 0;
    UINT1               u1BridgeStatus = CFA_ENABLED;
#ifdef CLI_WANTED
    tCliHandle          CliHandle = 0;
#endif

    if (u2AggId <= BRG_MAX_PHY_PORTS || u2AggId > BRG_MAX_PHY_PLUS_LOG_PORTS)
    {
        return L2IWF_FAILURE;
    }

    if (u4IfIndex > BRG_MAX_PHY_PORTS)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));
    MEMSET (&CfaLaInfoMsg, 0, sizeof (tCfaLaInfoMsg));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, NULL,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgeStatus);
    if (u1BridgeStatus == CFA_DISABLED)
    {
        /* For Router port no need to send notification to L2 Layers */
        if (CFA_IF_IPPORT (u4IfIndex) != CFA_INVALID_INDEX)
        {
            CfaUpdtIvrInterface (u4IfIndex, CFA_IF_DELETE);
#ifdef CLI_WANTED
            CfaIvrSetIpAddress (CliHandle, u4IfIndex, 0, 0);
#endif
        }

    }

    if (u1BridgeStatus != CFA_DISABLED)
    {
        /* First Indicate the port delete status to bridge, STP, VLANGARP, IGS. */
#ifdef BRIDGE_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_BRIDGE] == OSIX_FALSE)
        {
            BridgePortOperStatusIndication (u4IfIndex, CFA_IF_DOWN);
        }
#endif

        if (DenyProtocolList[L2IWF_PROTOCOL_ID_SNOOP] == OSIX_FALSE)
        {
            SnoopDeletePort (u4IfIndex);
        }

        if (DenyProtocolList[L2IWF_PROTOCOL_ID_XSTP] == OSIX_FALSE)
        {
            if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
            {
                AstDeletePort (u4IfIndex);
            }
            else
            {
                AstDeletePortFromLa (u4IfIndex);
            }
        }

        if (DenyProtocolList[L2IWF_PROTOCOL_ID_GARP] == OSIX_FALSE)
        {
            GarpDeletePort ((UINT2) u4IfIndex);
        }

        if (DenyProtocolList[L2IWF_PROTOCOL_ID_MRP] == OSIX_FALSE)
        {
            MrpApiDeletePort (u4IfIndex);
        }

        if (DenyProtocolList[L2IWF_PROTOCOL_ID_VLAN] == OSIX_FALSE)
        {
            VlanDeletePortAndCopyProperties (u4IfIndex, u2AggId);
        }
#ifdef PBB_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PBB] == OSIX_FALSE)
        {
            PbbDeletePort ((INT4) u4IfIndex);
        }
#endif /*PBB_WANTED */

#ifdef BRIDGE_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_BRIDGE] == OSIX_FALSE)
        {
            BridgeDeletePort (u4IfIndex);
        }
#endif
        CfaLaInfoMsg.u4IfIndex = u4IfIndex;
        CfaLaInfoMsg.u1MsgType = CFA_LA_ADD_PORT_TO_PORT_CHANNEL_MSG;
        CfaApiPortChannelUpdateInfo (&CfaLaInfoMsg);
    }

    L2_LOCK ();

    /* Add the port to the port-channel's configured list */
    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_CHANNEL_VALID (u2AggId) != OSIX_TRUE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    u2Index = L2IWF_PORT_CHANNEL_NUM_CONF_PORTS (u2AggId);

    L2IWF_PORT_CHANNEL_CONFIGURED_PORT (u2AggId, u2Index) = (UINT2) u4IfIndex;

    L2IWF_PORT_CHANNEL_NUM_CONF_PORTS (u2AggId)++;

    L2IWF_IFENTRY_AGG_INDEX (pL2PortEntry) = u2AggId;

    u1SispStatus = pL2PortEntry->u1IsSispEnabled;
    if (u1BridgeStatus == CFA_DISABLED)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    L2_UNLOCK ();

    if (u1SispStatus == L2IWF_ENABLED)
    {
        VcmSispPortAddedToPortChannel (u4IfIndex);
    }
#ifdef ELMI_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_ELMI] == OSIX_FALSE)
    {
        ElmDeletePort (u4IfIndex);
    }
#endif
#ifdef ECFM_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_ECFM] == OSIX_FALSE)
    {
        EcfmAddPortToPortChannel (u4IfIndex);
    }
#endif

    /* Send notification to LLDP indicating that port is capable of 
     * aggregation */
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_LLDP] == OSIX_FALSE)
    {
        LldpApiNotifyPortAggCapability (u4IfIndex, LLDP_AGG_CAPABLE);
    }
    if (u1BridgeStatus == CFA_ENABLED)
    {
#ifdef CN_WANTED
        if (VcmGetContextInfoFromIfIndex
            (u4IfIndex, &u4ContextId, &u2LocalPortId) == VCM_SUCCESS)
        {

            CnApiPortToPortChannelRequest (u4ContextId, u4IfIndex,
                                           (UINT4) u2AggId, CN_ADD_LA_MEM_MSG);
        }
#endif /* CN_WANTED */
    }
#ifdef RBRG_WANTED
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_SUCCESS)
    {
        RbrgApiPortToPortChannelRequest (u4ContextId, u4IfIndex,
                                         (UINT4) u2AggId, RBRG_ADD_LAG_PORT);
    }
#endif
    AstPortSpeedChgIndication (u2AggId);
#ifdef FSB_WANTED
    FsbApiAddPortToPortChannel (u4IfIndex, u2AggId);
#endif
#ifdef ISS_WANTED
    IssPIUtilUpdatePIEntry (u4IfIndex, u2AggId, LA_PI_ADD);
#endif

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfRemovePortFromPortChannel                       */
/*                                                                           */
/* Description        : This routine is called from LA to remove the given   */
/*                      port from the given PortChannel's configured portlist*/
/*                      in the L2Iwf common database                         */
/*                                                                           */
/* Input(s)           : u2AggId     - Index of the port channel from which   */
/*                                    the port is to be removed.             */
/*                      u4IfIndex - Index of the port to be removed from   */
/*                                    the Configured port list               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfRemovePortFromPortChannel (UINT4 u4IfIndex, UINT2 u2AggId)
{
    tIfTypeDenyProtocolList DenyProtocolList;
    tL2PortInfo        *pL2PortEntry = NULL;
#if (defined(CN_WANTED) || (defined(RBRG_WANTED)))
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
#endif
    UINT2               u2Index;
    UINT2               u2NumPorts;
    UINT1               u1BridgedStatus = CFA_DISABLED;

    if (u2AggId <= BRG_MAX_PHY_PORTS || u2AggId > BRG_MAX_PHY_PLUS_LOG_PORTS)
    {
        return L2IWF_FAILURE;
    }

    if (u4IfIndex > BRG_MAX_PHY_PORTS)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, NULL,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

#ifdef NPAPI_WANTED
#ifndef LA_HW_TRUNK_SUPPORTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_VLAN] == OSIX_FALSE)
    {
        VlanRemovePortPropertiesFromHw (u4IfIndex, u2AggId);
    }

    if (DenyProtocolList[L2IWF_PROTOCOL_ID_PBB] == OSIX_FALSE)
    {
        PbbRemovePortPropertiesFromHw (u4IfIndex, u2AggId);
    }
#endif
#endif

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_CHANNEL_VALID (u2AggId) != OSIX_TRUE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    u2NumPorts = L2IWF_PORT_CHANNEL_NUM_CONF_PORTS (u2AggId);

    if (u2NumPorts == 0)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
    {
        if (L2IWF_PORT_CHANNEL_CONFIGURED_PORT (u2AggId, u2Index) == u4IfIndex)
        {
            /* Found the entry containing the given port */
            break;
        }
    }

    if (u2Index == u2NumPorts)
    {
        /* Given port entry not found */
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    if (u2NumPorts >= (SYS_DEF_MAX_PHYSICAL_INTERFACES + 1))
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    /* Shift the member ports one place left, 
     * overwriting the entry found there */
    for (; u2Index < u2NumPorts - 1; u2Index++)
    {
        L2IWF_PORT_CHANNEL_CONFIGURED_PORT (u2AggId, u2Index) =
            L2IWF_PORT_CHANNEL_CONFIGURED_PORT (u2AggId, u2Index + 1);
    }

    L2IWF_PORT_CHANNEL_CONFIGURED_PORT (u2AggId, u2Index) = 0;

    L2IWF_PORT_CHANNEL_NUM_CONF_PORTS (u2AggId)--;

    L2IWF_IFENTRY_AGG_INDEX (pL2PortEntry) = u4IfIndex;
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedStatus);
    if (u1BridgedStatus == CFA_DISABLED)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    L2_UNLOCK ();

    /* Send notification to LLDP indicating that port is not capable of 
     * aggregation */
    LldpApiNotifyPortAggCapability (u4IfIndex, LLDP_AGG_NOT_CAPABLE);

#ifdef CN_WANTED
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_SUCCESS)
    {
        CnApiPortToPortChannelRequest (u4ContextId, u4IfIndex,
                                       (UINT4) u2AggId, CN_REM_LA_MEM_MSG);
    }
#endif /*CN_WANTED */

#ifdef RBRG_WANTED
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_SUCCESS)
    {
        RbrgApiPortToPortChannelRequest (u4ContextId, u4IfIndex,
                                         (UINT4) u2AggId, RBRG_REM_LAG_PORT);
    }
#endif
    AstPortSpeedChgIndication (u2AggId);
#ifdef FSB_WANTED
    FsbApiRemovePortFromPortChannel (u4IfIndex, u2AggId);
#endif
#ifdef ISS_WANTED
    IssPIUtilUpdatePIEntry (u4IfIndex, u2AggId, LA_PI_DELETE);
#endif
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetActivePortsForPortChannel                    */
/*                                                                           */
/* Description        : This routine returns the list of active member ports */
/*                      of the given PortChannel. It accesses the L2IWF      */
/*                      common database                                      */
/*                                                                           */
/* Input(s)           : AggId    - Index of the aggregator                   */
/*                                                                           */
/* Output(s)          : ActivePorts - Active port list for the given         */
/*                                    aggregator                             */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetActivePortsForPortChannel (UINT2 u2AggId, UINT2 au2ActivePorts[],
                                   UINT2 *pu2NumPorts)
{
    UINT2               u2Index;
    UINT2               u2NumPorts;

    *pu2NumPorts = 0;

    if (u2AggId <= BRG_MAX_PHY_PORTS || u2AggId > BRG_MAX_PHY_PLUS_LOG_PORTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IWF_PORT_CHANNEL_VALID (u2AggId) == OSIX_TRUE)
    {
        u2NumPorts = L2IWF_PORT_CHANNEL_NUM_ACTIVE_PORTS (u2AggId);

        for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
        {
            au2ActivePorts[u2Index] =
                L2IWF_PORT_CHANNEL_ACTIVE_PORT (u2AggId, u2Index);
        }

        *pu2NumPorts = u2NumPorts;
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfAddActivePortToPortChannel                      */
/*                                                                           */
/* Description        : This routine is called from LA to add the given      */
/*                      port to the given PortChannel's active portlist      */
/*                      in the L2Iwf common database                         */
/*                                                                           */
/* Input(s)           : u2AggId     - Index of the port channel to which the */
/*                                    port is to be added.                   */
/*                      u4IfIndex - Index of the port to be added to the   */
/*                                    Active port list                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfAddActivePortToPortChannel (UINT4 u4IfIndex, UINT2 u2AggId)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2Index;
    UINT2               u2ActivePortIndex = 0;
#ifdef ICCH_WANTED
    UINT4               u4IcclIfIndex = 0;
#endif

    if (u2AggId <= BRG_MAX_PHY_PORTS || u2AggId > BRG_MAX_PHY_PLUS_LOG_PORTS)
    {
        return L2IWF_FAILURE;
    }

    if (u4IfIndex > BRG_MAX_PHY_PORTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_CHANNEL_VALID (u2AggId) != OSIX_TRUE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    u2Index = L2IWF_PORT_CHANNEL_NUM_ACTIVE_PORTS (u2AggId);

    if (gu2PoIntfPhysicalMAC == L2IWF_TRUE)
    {
        if (u2Index == 0)
        {
            CfaGetIfInfo (u4IfIndex, &IfInfo);
            CfaSetIfInfo (IF_MAC_ADDRESS, u2AggId, &IfInfo);
        }
    }

    for (; u2ActivePortIndex < u2Index; u2ActivePortIndex++)
    {
        if (L2IWF_PORT_CHANNEL_ACTIVE_PORT (u2AggId, u2ActivePortIndex)
            == u4IfIndex)
        {
            /* This u4IfIndex is alredy present in the active port list
             * So no need to add. Return failure */
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
    }

    L2IWF_PORT_CHANNEL_ACTIVE_PORT (u2AggId, u2Index) = (UINT2) u4IfIndex;

    L2IWF_PORT_CHANNEL_NUM_ACTIVE_PORTS (u2AggId)++;

    L2_UNLOCK ();

#ifdef ICCH_WANTED
    /* Check if port channel is ICCL, if so for port addition/deletion 
     * to LA, port isolation has to be reprogrammed and MAC learning
     * status has to be disabled */
    IcchGetIcclIfIndex (&u4IcclIfIndex);

    if ((u2AggId == (UINT2) u4IcclIfIndex))
    {
        /* Disable the MAC-Learning status for ICCL interface */
        VlanApiSetIcchPortMacLearningStatus (u4IcclIfIndex, VLAN_DISABLED);
    }
    /* Update the port Isolation table */
    L2IwfLaUpdtPortIsolationEntry (u4IfIndex, u2AggId);
#endif
    /* Send notification to LLDP to indicate that the port is in port-channel */
    LldpApiNotifyAggStatus (u4IfIndex, LLDP_AGG_ACTIVE_PORT);
    /*Send Notification to Spanning tree module */
    AstPortSpeedChgIndication (u2AggId);
    /* Check and configure miroring if required */
    IssMirrAddRemovePort (u4IfIndex, (UINT4) u2AggId, ISS_MIRR_ADD_DONE);
    SnoopUpdateHwProperties (u2AggId, u4IfIndex, SNOOP_ADD_PORT_TO_AGG);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfRemoveActivePortFromPortChannel                 */
/*                                                                           */
/* Description        : This routine is called from LA to remove the given   */
/*                      port from the given PortChannel's active portlist    */
/*                      in the L2Iwf common database                         */
/*                                                                           */
/* Input(s)           : u2AggId     - Index of the port channel from which   */
/*                                    the port is to be removed.             */
/*                      u4IfIndex - Index of the port to be removed from   */
/*                                    the Active port list                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfRemoveActivePortFromPortChannel (UINT4 u4IfIndex, UINT2 u2AggId)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2Index;
    UINT2               u2NumPorts;
    UINT4               u4TempIfIndex = 0;
    UINT2               u2MaxPortsPerAgg = LA_MAX_PORTS_PER_AGG;
#ifdef ICCH_WANTED
    UINT4               u4IcclIfIndex = 0;
#endif

    if (u2AggId <= BRG_MAX_PHY_PORTS || u2AggId > BRG_MAX_PHY_PLUS_LOG_PORTS)
    {
        return L2IWF_FAILURE;
    }

    if (u4IfIndex > BRG_MAX_PHY_PORTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_CHANNEL_VALID (u2AggId) != OSIX_TRUE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    u2NumPorts = L2IWF_PORT_CHANNEL_NUM_ACTIVE_PORTS (u2AggId);

    if (u2NumPorts == 0)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
    {
        if (L2IWF_PORT_CHANNEL_ACTIVE_PORT (u2AggId, u2Index) == u4IfIndex)
        {
            if (gu2PoIntfPhysicalMAC == L2IWF_TRUE)
            {
                if (u2NumPorts > 1)
                {
                    if (u2Index == 0)
                    {
                        u4TempIfIndex = L2IWF_PORT_CHANNEL_ACTIVE_PORT (u2AggId,
                                                                        (u2Index
                                                                         + 1));
                        CfaGetIfInfo (u4TempIfIndex, &IfInfo);
                        CfaSetIfInfo (IF_MAC_ADDRESS, u2AggId, &IfInfo);
                    }
                }
            }

            /* Found the entry containing the given port */
            break;
        }
    }

    if (u2Index == u2NumPorts)
    {
        /* Given port entry not found */
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* Shift the member ports one place above, 
     * overwriting the entry found above */
    for (; u2Index < u2NumPorts - 1; u2Index++)
    {
        if (u2Index < (u2MaxPortsPerAgg - 1))
        {
            L2IWF_PORT_CHANNEL_ACTIVE_PORT (u2AggId, u2Index) =
                L2IWF_PORT_CHANNEL_ACTIVE_PORT (u2AggId, u2Index + 1);
        }
    }

    L2IWF_PORT_CHANNEL_ACTIVE_PORT (u2AggId, u2Index) = 0;

    L2IWF_PORT_CHANNEL_NUM_ACTIVE_PORTS (u2AggId)--;

    L2_UNLOCK ();

#ifdef ICCH_WANTED
    /* Update the port Isolation table */
    IcchGetIcclIfIndex (&u4IcclIfIndex);

    if ((u2AggId == (UINT2) u4IcclIfIndex))
    {
        /* Enable the MAC-Learning status for the port which is being
         * removed from ICCL port channel */
        VlanApiSetIcchPortMacLearningStatus (u4IfIndex, VLAN_ENABLED);
    }
    L2IwfLaUpdtPortIsolationEntry (u4IfIndex, u2AggId);
#endif

    /* Send notification to LLDP to indicate that the port is not in
     * port-channel */
    LldpApiNotifyAggStatus (u4IfIndex, LLDP_AGG_NOT_ACTIVE_PORT);

    /*Send Notification to Spanning tree module */
    AstPortSpeedChgIndication (u2AggId);

    /* Remove miroring configurations if done */
    IssMirrAddRemovePort (u4IfIndex, (UINT4) u2AggId, ISS_MIRR_REMOVE_DONE);
    SnoopUpdateHwProperties (u2AggId, u4IfIndex, SNOOP_REMOVE_PORT_FROM_AGG);
    return L2IWF_SUCCESS;
}

#if 0
INT4
L2IwfDeleteAllPortsFromTrunk (UINT2 u2AggId)
{
    UINT4               u4Port;
    UINT2               u2Index;
    tPortList          *pAggConfPorts = NULL;
    tL2PortInfo        *pL2PortEntry = NULL;
#ifdef NPAPI_WANTED
#ifndef LA_HW_TRUNK_SUPPORTED
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2NumPorts;

    L2IwfGetConfiguredPortsForPortChannel (u2AggId, au2ConfPorts, &u2NumPorts);

    for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
    {
        VlanRemovePortPropertiesFromHw (L2IWF_PORT_CHANNEL_CONFIGURED_PORT
                                        (u2AggId, u2Index), u2AggId);
        PbbRemovePortPropertiesFromHw (L2IWF_PORT_CHANNEL_CONFIGURED_PORT
                                       (u2AggId, u2Index), u2AggId);
    }
#endif
#endif

    pAggConfPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pAggConfPorts == NULL)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (*pAggConfPorts, 0, sizeof (tPortList));

    L2_LOCK ();

    for (u2Index = 0; u2Index < L2IWF_PORT_CHANNEL_NUM_CONF_PORTS (u2AggId);
         u2Index++)
    {
        u4Port = L2IWF_PORT_CHANNEL_CONFIGURED_PORT (u2AggId, u2Index);
        /* Get the list of configured ports and send the notification to LLDP
         * so that the capability can be reset*/
        OSIX_BITLIST_SET_BIT ((*pAggConfPorts), u4Port, sizeof (tPortList));

        pL2PortEntry = L2IwfGetIfIndexEntry (u4Port);
        if (pL2PortEntry == NULL)
        {
            FsUtilReleaseBitList ((UINT1 *) pAggConfPorts);
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
        L2IWF_IFENTRY_AGG_INDEX (pL2PortEntry) = u4Port;
    }

    L2IWF_PORT_CHANNEL_NUM_CONF_PORTS (u2AggId) = 0;

    L2_UNLOCK ();

    LldpApiNotifyResetAggCapability (*pAggConfPorts);
    FsUtilReleaseBitList ((UINT1 *) pAggConfPorts);
    return L2IWF_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function Name      : L2IwfGetPotentialTxPortForAgg                        */
/*                                                                           */
/* Description        : This routine is invoked from CFA to obtain one       */
/*                      physical interface corresponding to the given        */
/*                      Port channel for frame transmission.                 */
/*                                                                           */
/* Input(s)           : u2AggId - Port channel index                         */
/*                                                                           */
/* Output(s)          : u2PhyPort - Global IfIndex of the Physical port      */
/*                                  corresponding to the port channel        */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetPotentialTxPortForAgg (UINT2 u2AggId, UINT2 *pu2PhyPort)
{
    if (u2AggId <= BRG_MAX_PHY_PORTS || u2AggId > BRG_MAX_PHY_PLUS_LOG_PORTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IWF_PORT_CHANNEL_VALID (u2AggId) != OSIX_TRUE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_CHANNEL_NUM_ACTIVE_PORTS (u2AggId) == 0)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* Return the first port set in the Active port list for this port
     * channel */
    *pu2PhyPort = L2IWF_PORT_CHANNEL_ACTIVE_PORT (u2AggId, 0);

    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortChannelValid                             */
/*                                                                           */
/* Description        : Sets the status of the port channel                  */
/*                                                       */
/*                                                                           */
/* Input(s)           : u4AggId                                         */
/*                u1Status                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                         */
/*****************************************************************************/
VOID
L2IwfSetPortChannelValid (UINT4 u4AggId, UINT1 u1Status)
{
    L2IWF_PORT_CHANNEL_VALID (u4AggId) = (BOOL1) u1Status;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortChannelConfigured                        */
/*                                                                           */
/* Description        : Sets the status of the port channel configuration    */
/*                                                       */
/*                                                                           */
/* Input(s)           : u4AggId                                             */
/*                u1Status                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                         */
/*****************************************************************************/
VOID
L2IwfSetPortChannelConfigured (UINT4 u4AggId, UINT1 u1Status)
{
    L2IWF_PORT_CHANNEL_CONFIGURED (u4AggId) = (BOOL1) u1Status;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortIfIndex                                  */
/*                                                                           */
/* Description        : Gets the port IfIndex                     */
/*                                                         */
/*                                                                           */
/* Input(s)           : u2Port    - Port number                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : u4IfIndex                                            */
/*****************************************************************************/
UINT4
L2IwfGetPortIfIndex (UINT2 u2Port)
{
    return L2IWF_PORT_IFINDEX (u2Port);
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPortActiveInPortChannel                       */
/*                                                                           */
/* Description        : This routine is called to check if the given port is */
/*                      an active member port of the given port-channel      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port number                              */
/*                      u2AggId   - Aggregator Index                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE / L2IWF_FALSE                             */
/*****************************************************************************/

INT4
L2IwfIsPortActiveInPortChannel (UINT4 u4IfIndex, UINT2 u2AggId)
{
    UINT2               u2Index;
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal = L2IWF_FALSE;

    L2_LOCK ();

    if (L2IWF_PORT_CHANNEL_VALID (u2AggId) == OSIX_TRUE)
    {
        u2NumPorts = L2IWF_PORT_CHANNEL_NUM_ACTIVE_PORTS (u2AggId);

        if (u2NumPorts == 0)
        {
            L2_UNLOCK ();
            return L2IWF_FALSE;
        }

        for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
        {
            if (L2IWF_PORT_CHANNEL_ACTIVE_PORT (u2AggId, u2Index) ==
                (UINT2) u4IfIndex)
            {
                i4RetVal = L2IWF_TRUE;
                break;
            }
        }
    }
    L2_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfBlockPortChannelConfig                          */
/*                                                                           */
/* Description        : This utilitity function used to update mirror status  */
/*                      in L2PortEntry.                                      */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - mirroring status (enabled/disabled)       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfBlockPortChannelConfig (UINT4 u4IfIndex, UINT1 u1Status)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (u1Status == OSIX_TRUE)
    {
        pL2PortEntry->u1LaConfigAllowed = OSIX_FALSE;
    }
    else
    {
        pL2PortEntry->u1LaConfigAllowed = OSIX_TRUE;
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsLAConfigAllowed                               */
/*                                                                           */
/* Description        : This function is called whenever lacp is enabled on  */
/*                      the port. It checks whether LA configuration is      */
/*                      allowed in the port.                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Number which is created in system   */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfIsLaConfigAllowed (UINT4 u4IfIndex)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    UINT1               u1Status = 0;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        u1Status = pL2PortEntry->u1LaConfigAllowed;
    }
    else
    {
        u1Status = OSIX_TRUE;
    }

    L2_UNLOCK ();

    return u1Status;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPortChannelConfigAllowed                      */
/*                                                                           */
/* Description        : This function is called whenever lacp is enabled on  */
/*                      the port. It checks whether Portchannel configuration*/
/*                      allowed in the port.                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Number which is created in system   */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfIsPortChannelConfigAllowed (UINT4 u4IfIndex)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    UINT1               u1Status = 0;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        u1Status = pL2PortEntry->u1LaConfigAllowed;
    }
    else
    {
        u1Status = OSIX_TRUE;
    }

    L2_UNLOCK ();

    return u1Status;
}

#ifdef ICCH_WANTED
/*****************************************************************************/
/* Function Name      : L2IwfLaUpdtPortIsolationEntry                        */
/*                                                                           */
/* Description        : This function is called whenever ports are added/    */
/*                      deleted in a Portchannel. It will decide whether     */
/*                      port isolation table has to be updated or not        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Number which is created in system   */
/*                      u2AggId - Aggregation Index of port channel          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfLaUpdtPortIsolationEntry (UINT4 u4IfIndex, UINT2 u2AggId)
{
    tIssUpdtPortIsolation PortIsolationConfig;
    tIssPortIsoInfo     PortIsoInfo;
    UINT4               au4EgressPorts[ISS_MAX_UPLINK_PORTS];
    UINT4               u4IcclIfIndex = 0;
    INT4                i4LoopCount = 0;
    INT4                i4RetVal = ISS_FAILURE;
    UINT2               u2NumEgressPorts = 0;
    UINT1               u1IsMclagEnabled = OSIX_FALSE;
    BOOL1               b1UpdtReq = OSIX_FALSE;

    UNUSED_PARAM (u4IfIndex);

    MEMSET (&au4EgressPorts, 0, sizeof (au4EgressPorts));
    MEMSET (&PortIsolationConfig, 0, sizeof (tIssUpdtPortIsolation));
    MEMSET (&PortIsoInfo, 0, sizeof (tIssPortIsoInfo));

    /* Check if port channel is ICCL or MCLAG port channel, if so
     * for port addition/deletion to LA, port isolation has to be
     * reprogrammed */
    IcchGetIcclIfIndex (&u4IcclIfIndex);
    LaApiIsMclagInterface (u2AggId, &u1IsMclagEnabled);

    if ((u2AggId == (UINT2) u4IcclIfIndex))
    {
        PortIsoInfo.u4IngressPort = u4IcclIfIndex;
        PortIsoInfo.InVlanId = 0;
        i4RetVal = IssApiGetIsolationEgressPorts (&PortIsoInfo, au4EgressPorts);
        if (i4RetVal == ISS_SUCCESS)
        {
            PortIsolationConfig.pu4EgressPorts = au4EgressPorts;
            PortIsolationConfig.u4IngressPort = u4IcclIfIndex;
            PortIsolationConfig.InVlanId = 0;
            PortIsolationConfig.u1Action = ISS_PI_ADD;

            /* To get the number of egress ports */
            while ((PortIsolationConfig.pu4EgressPorts[u2NumEgressPorts] != 0)
                   && (u2NumEgressPorts < ISS_MAX_UPLINK_PORTS))
            {
                u2NumEgressPorts++;
            }                    /*end of while */

            PortIsolationConfig.u2NumEgressPorts = u2NumEgressPorts;
            IssApiUpdtPortIsolationEntry (&PortIsolationConfig);
        }
    }
    else if ((u1IsMclagEnabled == OSIX_TRUE))
    {
        PortIsoInfo.u4IngressPort = u4IcclIfIndex;
        PortIsoInfo.InVlanId = 0;
        i4RetVal = IssApiGetIsolationEgressPorts (&PortIsoInfo, au4EgressPorts);

        if (i4RetVal == ISS_SUCCESS)
        {
            while (i4LoopCount < ISS_MAX_UPLINK_PORTS)
            {
                if (au4EgressPorts[i4LoopCount] == u2AggId)
                {
                    /* IfIndex is present in Egress ports list,so update
                     * required */
                    b1UpdtReq = OSIX_TRUE;
                    break;
                }
                i4LoopCount++;
            }
            if (b1UpdtReq == OSIX_TRUE)
            {
                MEMSET (&au4EgressPorts, 0, ISS_MAX_UPLINK_PORTS);
                /* It is enough to pass only egress port channel Index */
                au4EgressPorts[0] = u2AggId;
                PortIsolationConfig.pu4EgressPorts = au4EgressPorts;
                PortIsolationConfig.u4IngressPort = u4IcclIfIndex;
                PortIsolationConfig.InVlanId = 0;
                PortIsolationConfig.u1Action = ISS_PI_ADD;
                PortIsolationConfig.u2NumEgressPorts = 1;
                IssApiUpdtPortIsolationEntry (&PortIsolationConfig);
            }
        }
    }

    return L2IWF_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name      : L2IwfGetPortChannelAndOperStatus                     */
/*                                                                           */
/* Description        : This routine returns the Port Channel of which the   */
/*                      given port is a member and also returns oper state of*/
/*                      the port channel. It accesses the L2Iwf common       */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose         */
/*                                  PortChannel Id to be obtained.           */
/*                                                                           */
/* Output(s)          : u2AggId            - Port Channel Index              */
/*                      u1BridgeOperStatus - Operational status of           */
/*                                           port channel                    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortChannelAndOperStatus (UINT4 u4IfIndex, UINT2 *pu2AggId,
                                  UINT1 *pu1BridgeOperStatus)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tL2PortInfo        *pL2AggPortEntry = NULL;

    *pu2AggId = (UINT2) u4IfIndex;

    if (u4IfIndex > BRG_MAX_PHY_PORTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        *pu2AggId = (UINT2) L2IWF_IFENTRY_AGG_INDEX (pL2PortEntry);
        pL2AggPortEntry = L2IwfGetIfIndexEntry (*pu2AggId);
        if (NULL != pL2AggPortEntry)
        {
            *pu1BridgeOperStatus =
                L2IWF_IFENTRY_BRIDGE_OPER_STATUS (pL2AggPortEntry);
        }
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/* END OF FILE */
