
#include "l2inc.h"
/* Vlan Translation table related functions.
 * As these functions will be used only between vlan and
 * garp, we can use context id and local port id in these
 * function. */

/*****************************************************************************/
/* Function Name      : L2IwfPbDeInitMemPools                                */
/*                                                                           */
/* Description        : This routine creates the mempools required for       */
/*                      provider bridge functionality in L2Iwf.              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE     
 * $Id: l2pbvlan.c,v 1.10 2012/04/25 12:14:48 siva Exp $
 *                                                                           */
/*****************************************************************************/
INT4
L2IwfPbInitMemPools (VOID)
{

    /*NOTE: This function can be called at many times.But the
     * NULL checks ensures that mempool will be created only once*/

    if (L2IWF_VID_TRANS_ENTRY_POOLID () == 0)
    {
        /* Create Mem pool for VID translation table entries. */
        if (L2IWF_CREATE_MEMPOOL
            (sizeof (tL2VidTransEntry),
             gFsL2iwfSizingParams[L2IWF_L2VID_TRANS_SIZING_ID].
             u4PreAllocatedUnits, MEM_HEAP_MEMORY_TYPE,
             &(L2IWF_VID_TRANS_ENTRY_POOLID ())) == MEM_FAILURE)
        {
            L2IwfPbDeInitMemPools ();
            return L2IWF_FAILURE;
        }
    }

    if (L2IWF_PEPINFO_POOLID () == 0)
    {
        /* Create Mem pool for C-VLAN port table here. */
        if (L2IWF_CREATE_MEMPOOL
            (sizeof (tL2CVlanPortEntry),
             gFsL2iwfSizingParams[L2IWF_L2CVLN_PORT_SIZING_ID].
             u4PreAllocatedUnits, MEM_HEAP_MEMORY_TYPE,
             &(L2IWF_PEPINFO_POOLID ())) == MEM_FAILURE)
        {
            L2IwfPbDeInitMemPools ();
            return L2IWF_FAILURE;
        }
    }

    if (L2IWF_CVLAN_PORT_TBL () == NULL)
    {
        L2IWF_CVLAN_PORT_TBL () = RBTreeCreateEmbedded (0, L2IwfPbCmpPepEntry);

        if (L2IWF_CVLAN_PORT_TBL () == NULL)
        {
            L2IwfPbDeInitMemPools ();
            return L2IWF_FAILURE;
        }
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbDeInitMemPools                                */
/*                                                                           */
/* Description        : This routine deletes the mempools created for        */
/*                      provider bridge functionality in L2Iwf.              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPbDeInitMemPools (VOID)
{

    /* Delete Vid tranlation entry memory pool. */
    if (L2IWF_VID_TRANS_ENTRY_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_VID_TRANS_ENTRY_POOLID ());
        L2IWF_VID_TRANS_ENTRY_POOLID () = 0;
    }

    /* C-VLAN port table should have been deleted before this. */
    if (L2IWF_CVLAN_PORT_TBL () != NULL)
    {
        RBTreeDelete (L2IWF_CVLAN_PORT_TBL ());
        L2IWF_CVLAN_PORT_TBL () = NULL;
    }

    /* Delete C-VLAN port entry memory pool. */
    if (L2IWF_PEPINFO_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_PEPINFO_POOLID ());
        L2IWF_PEPINFO_POOLID () = 0;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbIngVidTransTableCmp                           */
/*                                                                           */
/* Description        : This routine will be used by RB Tree as a compare    */
/*                      function for the ingress vid translation table.      */
/*                                                                           */
/* Input(s)           : pRBElem1 - pointer to RB tree element.               */
/*                      pRBElem2 - pointer to RB tree element.               */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : 0 if the both the elements are equal                 */
/*                      1 if pRBElem1 is > pRBElem2, otherwise -1            */
/*****************************************************************************/
INT4
L2IwfPbIngVidTransTableCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1 = ((tL2VidTransEntry *) pRBElem1)->u2Port;
    UINT2               u2Port2 = ((tL2VidTransEntry *) pRBElem2)->u2Port;
    tVlanId             Vid1 = ((tL2VidTransEntry *) pRBElem1)->LocalVid;
    tVlanId             Vid2 = ((tL2VidTransEntry *) pRBElem2)->LocalVid;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 == u2Port2)
    {
        if (Vid1 < Vid2)
        {
            return -1;
        }
        if (Vid1 == Vid2)
        {
            return 0;
        }
        return 1;
    }

    return 1;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbEgrVidTransTableCmp                           */
/*                                                                           */
/* Description        : This routine will be used by RB Tree as a compare    */
/*                      function for the egress vid translation table.       */
/*                                                                           */
/* Input(s)           : pRBElem1 - pointer to RB tree element.               */
/*                      pRBElem2 - pointer to RB tree element.               */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : 0 if the both the elements are equal                 */
/*                      1 if pRBElem1 is > pRBElem2, otherwise -1            */
/*****************************************************************************/
INT4
L2IwfPbEgrVidTransTableCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT2               u2Port1 = ((tL2VidTransEntry *) pRBElem1)->u2Port;
    UINT2               u2Port2 = ((tL2VidTransEntry *) pRBElem2)->u2Port;
    tVlanId             Vid1 = ((tL2VidTransEntry *) pRBElem1)->RelayVid;
    tVlanId             Vid2 = ((tL2VidTransEntry *) pRBElem2)->RelayVid;

    if (u2Port1 < u2Port2)
    {
        return -1;
    }
    if (u2Port1 == u2Port2)
    {
        if (Vid1 < Vid2)
        {
            return -1;
        }
        if (Vid1 == Vid2)
        {
            return 0;
        }
        return 1;
    }

    return 1;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbFreeVidTransEntry                             */
/*                                                                           */
/* Description        : This routine frees the given vid translation entry.  */
/*                                                                           */
/* Input(s)           : u4ContextId - virtual context id                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbFreeVidTransEntry (tRBElem * pElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    L2IWF_RELEASE_MEMBLOCK (L2IWF_VID_TRANS_ENTRY_POOLID (),
                            (tL2VidTransEntry *) pElem);
    return 1;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbCreateVidTransTable                           */
/*                                                                           */
/* Description        : This routine creates the vid translation table for a */
/*                      given context. This function will be called when     */
/*                      vlan is started for 1ad bridge.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - virtual context id                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbCreateVidTransTable (UINT4 u4ContextId)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* Create ingress vid translation table. */
    L2IWF_ING_VID_TRANS_TBL () =
        RBTreeCreateEmbedded (0, (tRBCompareFn) L2IwfPbIngVidTransTableCmp);

    if (L2IWF_ING_VID_TRANS_TBL () == NULL)
    {
        /* Ingress vid translation table creation failed. */
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* Create egress vid translation table. As the egress vid translation 
     * table node is not the first element of tL2VidTransEntry, give the
     * offset value to the RB tree create function.*/
    L2IWF_EGR_VID_TRANS_TBL () =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tL2VidTransEntry, EgrVidTransNode),
                              (tRBCompareFn) L2IwfPbEgrVidTransTableCmp);

    if (L2IWF_EGR_VID_TRANS_TBL () == NULL)
    {
        /* Egress vid translation table creation failed. 
         * So delete the ingress vid translation table and return. */
        RBTreeDestroy (L2IWF_ING_VID_TRANS_TBL (), L2IwfPbFreeVidTransEntry, 0);
        L2IWF_ING_VID_TRANS_TBL () = NULL;

        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbDeleteVidTransTable                           */
/*                                                                           */
/* Description        : This routine deletes the vid translation table for   */
/*                      given context. This function will be called when     */
/*                      vlan is shutdown for a context.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - virtual context id                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbDeleteVidTransTable (UINT4 u4ContextId)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_ING_VID_TRANS_TBL () != NULL)
    {
        RBTreeDestroy (L2IWF_ING_VID_TRANS_TBL (), L2IwfPbFreeVidTransEntry, 0);
    }
    if (L2IWF_EGR_VID_TRANS_TBL () != NULL)
    {
        RBTreeDestroy (L2IWF_EGR_VID_TRANS_TBL (), L2IwfPbFreeVidTransEntry, 0);
    }

    L2IWF_ING_VID_TRANS_TBL () = NULL;
    L2IWF_EGR_VID_TRANS_TBL () = NULL;

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetVidTransEntryInCtxt                        */
/*                                                                           */
/* Description        : This routine based on input parameter u1IsLocalVid   */
/*                      selects ingress or egress vid translation table and  */
/*                      gets the entry from the selected table for the given */
/*                      input (u2LocalPort, SearchVid).                      */
/*                                                                           */
/* Input(s)           : u2LocalPort - Per Context port index                 */
/*                      SearchVid - Vid that will be used for searching vid  */
/*                                  translation table.                       */
/*                      u1IsLocalVid - If true then SearchVid will taken as  */
/*                                     Local vid, otherwise RelayVid.        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
tL2VidTransEntry   *
L2IwfPbGetVidTransEntryInCtxt (UINT2 u2LocalPort, tVlanId SearchVid,
                               UINT1 u1IsLocalVid)
{
    tL2VidTransEntry    DummyEntry;

    DummyEntry.u2Port = u2LocalPort;

    if (u1IsLocalVid == OSIX_TRUE)
    {
        /* Search the Ingress VID translation table and get the corresponding
         * entry. */
        DummyEntry.LocalVid = SearchVid;
        return (tL2VidTransEntry *) RBTreeGet (L2IWF_ING_VID_TRANS_TBL (),
                                               (tRBElem *) & DummyEntry);
    }

    /* Search the Egress VID translation table and get the corresponding
     * entry. */
    DummyEntry.RelayVid = SearchVid;
    return (tL2VidTransEntry *) RBTreeGet (L2IWF_EGR_VID_TRANS_TBL (),
                                           (tRBElem *) & DummyEntry);
}

/*****************************************************************************/
/* Function Name      : L2IwfPbCreateVidTransEntry                           */
/*                                                                           */
/* Description        : This routine creates the vid translation entry for   */
/*                      given port, localvid.                                */
/*                                                                           */
/* Input(s)           : u2LocalPort - Per Context port index                 */
/*                      LocalVid - Local Vid                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbCreateVidTransEntry (UINT2 u2LocalPort, tVlanId LocalVid)
{
    tL2VidTransEntry   *pVidTransEntry = NULL;

    /* Check whether the entry is already present. 
     * Already the following check is done by vlan module.
     * But it is safe to have this check here too.*/
    pVidTransEntry = L2IwfPbGetVidTransEntryInCtxt (u2LocalPort, LocalVid,
                                                    OSIX_TRUE);
    if (pVidTransEntry != NULL)
    {
        /* Entry already present. */
        return L2IWF_FAILURE;
    }

    pVidTransEntry = (tL2VidTransEntry *) L2IWF_ALLOCATE_MEMBLOCK
        (L2IWF_VID_TRANS_ENTRY_POOLID ());

    if (pVidTransEntry == NULL)
    {
        return L2IWF_FAILURE;
    }

    pVidTransEntry->u2Port = u2LocalPort;
    pVidTransEntry->LocalVid = LocalVid;
    pVidTransEntry->RelayVid = VLAN_NULL_VLAN_ID;
    pVidTransEntry->u1RowStatus = L2IWF_NOT_READY;

    /* Add the entry in to the ingress vid translation table. */
    if (RBTreeAdd (L2IWF_ING_VID_TRANS_TBL (), (tRBElem *) pVidTransEntry)
        == RB_FAILURE)
    {
        L2IWF_RELEASE_MEMBLOCK (L2IWF_VID_TRANS_ENTRY_POOLID (),
                                pVidTransEntry);
        pVidTransEntry = NULL;

        return L2IWF_FAILURE;
    }

    /* As the relay vid is not yet configured, it is not possible to insert
     * this entry in to the egress vid translation table. */

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbDelVidTransEntry                              */
/*                                                                           */
/* Description        : This routine deletes the vid translation entry for   */
/*                      given port, localvid.                                */
/*                                                                           */
/* Input(s)           : u2LocalPort - Per Context port index                 */
/*                      LocalVid - Local Vid                                 */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbDelVidTransEntry (UINT2 u2LocalPort, tVlanId LocalVid)
{
    tL2VidTransEntry   *pVidTransEntry = NULL;

    /* Search for the corresponding entry from the ingress vid translation table. 
     * If the entry is found, then 
     *    - delete it from the ingress vid translation table.
     *    - Get the relay vid from the entry. If the relay vid is valid, then 
     *      delete the node from the egress vid translation table.
     * Otherwise return failure.
     * */
    pVidTransEntry = L2IwfPbGetVidTransEntryInCtxt (u2LocalPort, LocalVid,
                                                    OSIX_TRUE);

    if (pVidTransEntry == NULL)
    {
        return L2IWF_FAILURE;
    }

    /* Remove the node from ingress vid translation table. */
    RBTreeRem (L2IWF_ING_VID_TRANS_TBL (), (tRBElem *) pVidTransEntry);

    if (pVidTransEntry->RelayVid != VLAN_NULL_VLAN_ID)
    {
        /* Remove the node from egress vid translation table. */
        RBTreeRem (L2IWF_EGR_VID_TRANS_TBL (), (tRBElem *) pVidTransEntry);
    }

    L2IWF_RELEASE_MEMBLOCK (L2IWF_VID_TRANS_ENTRY_POOLID (), pVidTransEntry);
    pVidTransEntry = NULL;

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetVidTransEntry                              */
/*                                                                           */
/* Description        : This routine gets an entry with the given inputs.    */
/*                      Based on u1IsLocalVid value, the search will be done */
/*                      on the ingress/egress vid translation table.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                      VidTransEntryInfo  - This contains the following:    */
/*                              - u2Port - Port Id.                          */
/*                              - LocalVid - Local Vid for the entry.        */
/*                              - RelayVid - Relay Vid for the entry.        */
/*                              - u1RowStatus - Row Status for the entry.    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetVidTransEntry (UINT4 u4ContextId, UINT2 u2LocalPort,
                         tVlanId SearchVid, UINT1 u1IsLocalVid,
                         tVidTransEntryInfo * pOutVidTransInfo)
{
    tL2VidTransEntry   *pVidTransEntry = NULL;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* If u1IsLocalVid == OSIX_TRUE, then search Vid translation table
     * for (port = u2LocalPort and LocalVid = SearchVid) pair.
     * If u1IsLocalVid == OSIX_FALSE, then search Vid translation table
     * for (port = u2LocalPort and RelayVid = SearchVid) pair.
     */
    pVidTransEntry = L2IwfPbGetVidTransEntryInCtxt (u2LocalPort, SearchVid,
                                                    u1IsLocalVid);

    if (pVidTransEntry == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pOutVidTransInfo->u2Port = pVidTransEntry->u2Port;
    pOutVidTransInfo->LocalVid = pVidTransEntry->LocalVid;
    pOutVidTransInfo->RelayVid = pVidTransEntry->RelayVid;
    pOutVidTransInfo->u1RowStatus = pVidTransEntry->u1RowStatus;

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbConfigVidTransEntry                           */
/*                                                                           */
/* Description        : This routine configures an entry in vid translation  */
/*                      table. An entry configured through this function     */
/*                      results in inserting the node in the ingress vid     */
/*                      translation table and also in the egress vid         */
/*                      translation table.                                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                      VidTransEntryInfo  - This contains the following:    */
/*                              - u2Port - Port Id.                          */
/*                              - LocalVid - Local Vid for the entry.        */
/*                              - RelayVid - Relay Vid for the entry.        */
/*                              - u1RowStatus - Row Status for the entry.    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbConfigVidTransEntry (UINT4 u4ContextId,
                            tVidTransEntryInfo VidTransEntryInfo)
{

    tL2VidTransEntry   *pVidTransEntry = NULL;
    INT4                i4RetVal = L2IWF_SUCCESS;
    UINT2               u2LocalPort = VidTransEntryInfo.u2Port;
    tVlanId             LocalVid = VidTransEntryInfo.LocalVid;
    tVlanId             RelayVid = VidTransEntryInfo.RelayVid;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    switch (VidTransEntryInfo.u1RowStatus)
    {
        case L2IWF_CREATE_AND_WAIT:
            i4RetVal = L2IwfPbCreateVidTransEntry (u2LocalPort, LocalVid);
            break;
        case L2IWF_NOT_IN_SERVICE:
            i4RetVal =
                L2IwfPbSetRelayVidForVidTransEntry (u2LocalPort,
                                                    LocalVid, RelayVid);
            break;

        case L2IWF_ACTIVE:
            pVidTransEntry = L2IwfPbGetVidTransEntryInCtxt (u2LocalPort,
                                                            LocalVid,
                                                            OSIX_TRUE);

            if (pVidTransEntry == NULL)
            {
                i4RetVal = L2IWF_FAILURE;
            }
            else
            {
                if (pVidTransEntry->u1RowStatus == L2IWF_ACTIVE)
                {
                    L2IwfReleaseContext ();
                    L2_UNLOCK ();
                    return L2IWF_SUCCESS;
                }

                if (pVidTransEntry->u1RowStatus == L2IWF_NOT_IN_SERVICE)
                {
                    pVidTransEntry->u1RowStatus = L2IWF_ACTIVE;
                }
                else
                {
                    i4RetVal = L2IWF_FAILURE;
                }
            }
            break;

        case L2IWF_DESTROY:
            i4RetVal = L2IwfPbDelVidTransEntry (u2LocalPort, LocalVid);
            break;

        default:
            i4RetVal = L2IWF_FAILURE;
            break;
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetRelayVidForVidTransEntry                   */
/*                                                                           */
/* Description        : This routine sets the relay vid for the corresponding*/
/*                      local vid.                                           */
/*                                                                           */
/* Input(s)           : u2LocalPort - Per Context Port Index.                */
/*                      LocalVid    - Local Vlan Id                          */
/*                      RelayVid    - Relay Vlan Id                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbSetRelayVidForVidTransEntry (UINT2 u2LocalPort, tVlanId LocalVid,
                                    tVlanId RelayVid)
{
    tL2VidTransEntry   *pVidTransEntry = NULL;
    tL2VidTransEntry   *pEgrVidTransEntry = NULL;

    /* First get the node from the ingress vid translation table. */
    pVidTransEntry = L2IwfPbGetVidTransEntryInCtxt (u2LocalPort, LocalVid,
                                                    OSIX_TRUE);

    if (pVidTransEntry == NULL)
    {
        return L2IWF_FAILURE;
    }

    if (pVidTransEntry->RelayVid == RelayVid)
    {
        pVidTransEntry->u1RowStatus = L2IWF_NOT_IN_SERVICE;
        return L2IWF_SUCCESS;
    }

    /* Check whether some other entry is having this relay
     * vid, if so then return failure.*/
    pEgrVidTransEntry = L2IwfPbGetVidTransEntryInCtxt (u2LocalPort,
                                                       RelayVid, OSIX_FALSE);

    if ((pEgrVidTransEntry != NULL) &&
        (pEgrVidTransEntry->LocalVid != LocalVid))
    {
        /* Already there is an entry with this relay vid. Hence
         * return failure. */
        return SNMP_FAILURE;
    }

    if (pVidTransEntry->RelayVid != VLAN_NULL_VLAN_ID)
    {
        /* Already some value is configured as relay vid. 
         * In this case delete the entry from egress vid
         * translation table and then change the relay vid 
         * and add.*/
        RBTreeRem (L2IWF_EGR_VID_TRANS_TBL (), pVidTransEntry);
    }

    /* Set the relay vid value. */
    pVidTransEntry->RelayVid = RelayVid;

    /* Only when the relay vid is known, the node can be added to the 
     * egress vid translation table. So now add the entry in to the
     * egress vid translation table. */
    if (RBTreeAdd (L2IWF_EGR_VID_TRANS_TBL (), (tRBElem *) pVidTransEntry)
        == RB_FAILURE)
    {
        RBTreeRem (L2IWF_ING_VID_TRANS_TBL (), pVidTransEntry);
        L2IWF_RELEASE_MEMBLOCK (L2IWF_VID_TRANS_ENTRY_POOLID (),
                                pVidTransEntry);
        pVidTransEntry = NULL;

        return L2IWF_FAILURE;
    }

    /* Now all the information to make the entry active is present.
     * So make the rowstatus as not-in-service. */
    pVidTransEntry->u1RowStatus = L2IWF_NOT_IN_SERVICE;

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetRelayVidFromLocalVid                       */
/*                                                                           */
/* Description        : This routine gets the relay vid for the corresponding*/
/*                      local vid.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                      u2LocalPort - Per Context Port Index.                */
/*                      LocalVid    - Local Vlan Id                          */
/*                                                                           */
/* Output(s)          : pRelayVid - RelayVid value for the given local vid   */
/*                      Value.                                               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetRelayVidFromLocalVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                tVlanId LocalVid, tVlanId * pRelayVid)
{
    INT4                i4RetVal;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (LocalVid > VLAN_DEV_MAX_VLAN_ID)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pRelayVid = VLAN_NULL_VLAN_ID;

    i4RetVal = L2IwfPbGetRelayVidFromLocalVidInCtxt (u2LocalPort, LocalVid,
                                                     pRelayVid);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetRelayVidFromLocalVidInCtxt                 */
/*                                                                           */
/* Description        : This routine gets the relay vid for the corresponding*/
/*                      local vid.                                           */
/*                                                                           */
/* Input(s)           : u2LocalPort - Per Context Port Index.                */
/*                      LocalVid    - Local Vlan Id                          */
/*                                                                           */
/* Output(s)          : pRelayVid - RelayVid value for the given local vid   */
/*                      Value.                                               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetRelayVidFromLocalVidInCtxt (UINT2 u2LocalPort, tVlanId LocalVid,
                                      tVlanId * pRelayVid)
{
    tL2VidTransEntry   *pVidTransEntry;

    if (u2LocalPort > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPort == 0)
    {
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_VID_TRANS_STATUS (u2LocalPort) == OSIX_DISABLED)
    {
        /* Vid translation is disabled on this port. So local vid and
         * relay vid are same on this port. */

        *pRelayVid = LocalVid;
        return L2IWF_SUCCESS;
    }

    pVidTransEntry = L2IwfPbGetVidTransEntryInCtxt (u2LocalPort, LocalVid,
                                                    OSIX_TRUE);

    /* As the node is not active, don't give the relay vid from the node. */
    if (pVidTransEntry != NULL)
    {
        if (pVidTransEntry->u1RowStatus == L2IWF_ACTIVE)
        {
            *pRelayVid = pVidTransEntry->RelayVid;
            return L2IWF_SUCCESS;
        }
        /* This is equivalent to "not having the entry". */
    }

    /* No active entry found for (u2LocalPort, LocalVid) pair in the
     * ingress vid translation table.
     * Now look in the egress vid tranlation table for entry that is
     * having the relay vid as LocalVid.
     * If such entry is found then return failure
     * otherwise return relayvid = LocalVid. */

    pVidTransEntry = NULL;

    pVidTransEntry = L2IwfPbGetVidTransEntryInCtxt (u2LocalPort, LocalVid,
                                                    OSIX_FALSE);

    if ((pVidTransEntry != NULL) &&
        (pVidTransEntry->u1RowStatus == L2IWF_ACTIVE))
    {
        /* There is an entry, whose relay vid is equal to "LocalVid". */
        return L2IWF_FAILURE;
    }
    else
    {
        /* There is no entry in the ingress and egress vid translation 
         * tables for (u2LocalPort, LocaVid) pair. This mean LocalVid = 
         * RelayVid. */
        *pRelayVid = LocalVid;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetLocalVidFromRelayVid                       */
/*                                                                           */
/* Description        : This routine gets the local vid for the corresponding*/
/*                      relay vid.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                      u2LocalPort - Per Context Port Index.                */
/*                      RelayVid    - Relay Vlan Id                          */
/*                                                                           */
/* Output(s)          : pLocalVid - LocalVid value for the given rely vid    */
/*                      Value.                                               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetLocalVidFromRelayVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                tVlanId RelayVid, tVlanId * pLocalVid)
{
    INT4                i4RetVal = L2IWF_SUCCESS;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPort > L2IWF_MAX_PORTS_PER_CONTEXT_EXT)
    {
        return L2IWF_FAILURE;
    }

    if (RelayVid > VLAN_DEV_MAX_VLAN_ID)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pLocalVid = VLAN_NULL_VLAN_ID;

    i4RetVal = L2IwfPbGetLocalVidFromRelayVidInCtxt (u2LocalPort, RelayVid,
                                                     pLocalVid);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetLocalVidFromRelayVidInCtxt                 */
/*                                                                           */
/* Description        : This routine gets the local vid for the corresponding*/
/*                      relay vid.                                           */
/*                                                                           */
/* Input(s)           : u2LocalPort - Per Context Port Index.                */
/*                      RelayVid    - Relay Vlan Id                          */
/*                                                                           */
/* Output(s)          : pLocalVid - LocalVid value for the given rely vid    */
/*                      Value.                                               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetLocalVidFromRelayVidInCtxt (UINT2 u2LocalPort, tVlanId RelayVid,
                                      tVlanId * pLocalVid)
{
    tL2VidTransEntry   *pVidTransEntry;

    if (u2LocalPort > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPort == 0)
    {
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_VID_TRANS_STATUS (u2LocalPort) == OSIX_DISABLED)
    {
        /* Vid translation is disabled on this port. So local vid and
         * relay vid are same on this port. */

        *pLocalVid = RelayVid;
        return L2IWF_SUCCESS;
    }

    /* Search egress vid translation table for (u2LocalPort, RelayVid) pair. */
    pVidTransEntry = L2IwfPbGetVidTransEntryInCtxt (u2LocalPort, RelayVid,
                                                    OSIX_FALSE);

    if (pVidTransEntry != NULL)
    {
        if (pVidTransEntry->u1RowStatus == L2IWF_ACTIVE)
        {
            *pLocalVid = pVidTransEntry->LocalVid;
            return L2IWF_SUCCESS;
        }
    }

    /* No active entry having the relay vid as RelayVid for port - u2LocalPort
     * in the egress vid translation table.
     * Now look in the ingress vid tranlation table for entry that is
     * having the local vid as RelayVid.
     * If such entry is found then return failure
     * otherwise return local vid  = RelayVid. */
    pVidTransEntry = L2IwfPbGetVidTransEntryInCtxt (u2LocalPort, RelayVid,
                                                    OSIX_TRUE);

    if ((pVidTransEntry != NULL) &&
        (pVidTransEntry->u1RowStatus == L2IWF_ACTIVE))
    {
        /* There is an entry, whose relay vid is equal to "LocalVid". */
        return L2IWF_FAILURE;
    }
    else
    {
        /* There is no entry in the ingress and egress vid translation 
         * tables for (u2LocalPort, LocaVid) pair. This mean LocalVid = 
         * RelayVid. */
        *pLocalVid = RelayVid;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetNextVidTransTblEntry                       */
/*                                                                           */
/* Description        : This routine returns the valid lowest vid translation*/
/*                      table entry whose (port, Vid) pair is greater than   */
/*                      the given (port,LocalVid) pair.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2Port      - Per Context port index                 */
/*                      LocalVid    - Local Vlan Id                          */
/*                                                                           */
/* Output(s)          : pOutVidTransEntryInfo - vid translation entry whose  */
/*                      (u2Port, LocalVid) pair is next (greater) to given   */
/*                      (u2Port, LocalVid) pair.                             */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetNextVidTransTblEntry (UINT4 u4ContextId, UINT2 u2Port,
                                tVlanId LocalVid,
                                tVidTransEntryInfo * pOutVidTransEntryInfo)
{
    tL2VidTransEntry    VidTransEntry;
    tL2VidTransEntry   *pVidTransEntry = NULL;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (LocalVid > VLAN_DEV_MAX_VLAN_ID)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    VidTransEntry.u2Port = u2Port;
    VidTransEntry.LocalVid = LocalVid;

    pVidTransEntry =
        (tL2VidTransEntry *) RBTreeGetNext (L2IWF_ING_VID_TRANS_TBL (),
                                            (tRBElem *) (&VidTransEntry), NULL);

    if (pVidTransEntry == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pOutVidTransEntryInfo->u2Port = pVidTransEntry->u2Port;
    pOutVidTransEntryInfo->LocalVid = pVidTransEntry->LocalVid;
    pOutVidTransEntryInfo->RelayVid = pVidTransEntry->RelayVid;
    pOutVidTransEntryInfo->u1RowStatus = pVidTransEntry->u1RowStatus;

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/* Vid Translation Status related routines. */
/*****************************************************************************/
/* Function Name      : L2IwfPbGetVidTransStatus                             */
/*                                                                           */
/* Description        : This routine gets the vid translation status for the */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2LocalPort - Per Context port index                 */
/*                                                                           */
/* Output(s)          : pu1Status - vid translation status for this port.    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetVidTransStatus (UINT4 u4ContextId, UINT2 u2LocalPort,
                          UINT1 *pu1Status)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPort > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPort == 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPort) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    *pu1Status = L2IWF_PORT_VID_TRANS_STATUS (u2LocalPort);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetVidTransStatus                             */
/*                                                                           */
/* Description        : This routine sets the vid translation status for the */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2LocalPortId - Per Context port index               */
/*                      u1Status - Vid translation status for this port.     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbSetVidTransStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                          UINT1 u1Status)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId == 0)
    {
        return L2IWF_FAILURE;
    }

    if ((u1Status != OSIX_ENABLED) && (u1Status != OSIX_DISABLED))
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPortId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* There is  no need to check whether whether vlan translation can be 
     * enabled on this port. As vlan takes care of this we can cooly
     * proceed.*/

    L2IWF_PORT_VID_TRANS_STATUS (u2LocalPortId) = u1Status;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetLocalVidListFromVIDTransTable              */
/*                                                                           */
/* Description        : This routine returns the list of local VLANs         */
/*                      configured for the given port.                       */
/*                                                                           */
/* Input(s)           : u4IfIndex   - IfIndex                                */
/*                      pu1LocalVidList - Local VID list                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetLocalVidListFromVIDTransTable (UINT4 u4IfIndex,
                                         UINT1 *pu1LocalVidList)
{
    tL2VidTransEntry    VidTransEntry;
    tL2VidTransEntry   *pVidTransEntry = NULL;
    tL2VidTransEntry   *pNextVidTransEntry = NULL;
    tL2PortInfo        *pL2PortInfo = NULL;
    UINT2               u2LocalPort = 0;
    UINT1               u1EntryFound = L2IWF_FALSE;

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IwfSelectContext (pL2PortInfo->u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    MEMSET (pu1LocalVidList, 0, VLAN_LIST_SIZE);

    u2LocalPort = L2IWF_IFENTRY_LOCAL_PORT (pL2PortInfo);

    VidTransEntry.u2Port = u2LocalPort;
    VidTransEntry.LocalVid = 0;

    pVidTransEntry = &VidTransEntry;

    while ((pNextVidTransEntry =
            (tL2VidTransEntry *) RBTreeGetNext (L2IWF_ING_VID_TRANS_TBL (),
                                                (tRBElem *) pVidTransEntry,
                                                NULL)) != NULL)
    {
        if (pVidTransEntry->u2Port != u2LocalPort)
        {
            /* Entry for some other port */
            break;
        }
        if (pVidTransEntry->u1RowStatus == VLAN_ACTIVE)
        {
            OSIX_BITLIST_SET_BIT (pu1LocalVidList, pNextVidTransEntry->LocalVid,
                                  VLAN_LIST_SIZE);
            u1EntryFound = L2IWF_TRUE;
        }
        pVidTransEntry = pNextVidTransEntry;
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    return ((u1EntryFound == L2IWF_TRUE) ? L2IWF_SUCCESS : L2IWF_FAILURE);
}
