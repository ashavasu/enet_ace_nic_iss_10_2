/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2pbb.c,v 1.28 2013/09/17 11:41:43 siva Exp $
 *
 * Description:This file contains routines to get/set PBB
n *             information from the L2IWF module.                 
 *
 *******************************************************************/
#ifndef _L2_PBB_C_
#define _L2_PBB_C_

#include "l2inc.h"
#include "fsvlan.h"
PRIVATE INT4        L2IwfTransmitFrameOnPip
PROTO ((UINT4, UINT2, tCRU_BUF_CHAIN_DESC *, tPbbTag *, UINT1));
/******************************************************************************
 * Function Name      : L2iwfAstHandleInFrame
 *
 * Description        : This routine is used to send STP frame to STP module.  
 *
 * Input(s)           : 
 
 * Output(s)          : None
 *
 * Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE
 *****************************************************************************/
INT4
L2iwfAstHandleInFrame (UINT4 u4ContextId, UINT2 u2LocalPort,
                       tCRU_BUF_CHAIN_DESC * pBuf)
{
    UINT4               u4IfIndx;
    if (VcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPort,
                                    &u4IfIndx) != VCM_SUCCESS)
    {
        return L2IWF_FAILURE;
    }
    CRU_BUF_Move_ValidOffset (pBuf, PBB_CDA_OFFSET_IN_ITAG_PKT);
    AstHandleInFrame (pBuf, u4IfIndx);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfCreateIsid                                      */
/*                                                                           */
/* Description        : This routine is called from PBB module to            */
/*                      indicate ECFM about Isid creation.                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch number                  */
/*                      Isid    - Isid which has been newly created.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfCreateIsid (UINT4 u4ContextId, UINT4 Isid)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    /* Indicate ECFM Module for ISID creation. */
#ifdef ECFM_WANTED
    EcfmCcmOffloadCreateIsidIndication (u4ContextId, Isid);
#else
    UNUSED_PARAM (Isid);
#endif
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeleteIsid                                      */
/*                                                                           */
/* Description        : This routine is called from PBB module to            */
/*                      indicate ECFM module about Isid deletion.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch number                  */
/*                      Isid    - Isid which is deleted.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfDeleteIsid (UINT4 u4ContextId, UINT4 Isid)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    /* Indicate ECFM Module for ISID deletion. */
#ifdef ECFM_WANTED
    EcfmCcmOffloadDeleteIsidIndication (u4ContextId, Isid);
#else
    UNUSED_PARAM (Isid);
#endif

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPbbShutdownStatus                            */
/*                                                                           */
/* Description        : This routine is called from other modules to get PBB */
/*                      shutdown status.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE/ L2IWF_FALSE                              */
/*****************************************************************************/
INT4
L2IwfGetPbbShutdownStatus (VOID)
{
    UINT1               u1Status = L2IWF_FALSE;

    L2_LOCK ();

    u1Status = L2IWF_PBB_SHUTDOWN_STATUS ();

    L2_UNLOCK ();

    return u1Status;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPbbShutdownStatus                            */
/*                                                                           */
/* Description        : This routine is called from other modules to set PBB */
/*                      shutdown status.                                     */
/*                                                                           */
/* Input(s)           : u1Status                                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetPbbShutdownStatus (UINT1 u1Status)
{
    L2_LOCK ();

    L2IWF_PBB_SHUTDOWN_STATUS () = u1Status;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVipOperStatusFlag                            */
/*                                                                           */
/* Description        : This routine is called from CFA to get the           */
/*                      u1VipOperStatusFlag from L2IWF port entry            */
/*                                                                           */
/* Input(s)           : u4IfIndex - VIP Interface Index                      */
/*                                                                           */
/* Output(s)          : pu1VipOperStatusFlag - Vip Oper Status Flag          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVipOperStatusFlag (UINT4 u4IfIndex, UINT1 *pu1VipOperStatusFlag)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);
    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1VipOperStatusFlag = pL2PortEntry->u1VipOperStatusFlag;

    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetVipOperStatusFlag                            */
/*                                                                           */
/* Description        : This routine is called from PBB to set the           */
/*                      u1VipOperStatusFlag in L2IWF port entry              */
/*                                                                           */
/* Input(s)           : u4IfIndex - VIP Interface Index                      */
/*                      u1VipOperStatusFlag -   CFA_TRUE / CFA_FALSE         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVipOperStatusFlag (UINT4 u4IfIndex, UINT1 u1VipOperStatusFlag)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);
    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pL2PortEntry->u1VipOperStatusFlag = u1VipOperStatusFlag;

    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdateVipOperStatus                             */
/*                                                                           */
/* Description        : This routine is called from PBB module to            */
/*                      indicate CFA module about VIP Oper status            */
/*                                                                           */
/* Input(s)           : u4VipIndex - VIP Interface Index                     */
/*                      u1OperStatus - VIP Oper status                       */
/*                      CFA_IF_UP/ CFA_IF_DOWN                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfUpdateVipOperStatus (UINT4 u4VipIndex, UINT1 u1OperStatus)
{

    tL2PortInfo        *pL2PortEntry = NULL;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4VipIndex);
    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2_UNLOCK ();

    /* Indicate CFA Module for VIP Oper Status Change */
    CfaUpdateVipOperStatus (u4VipIndex, u1OperStatus);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetAllToOneBndlStatus                           */
/*                                                                           */
/* Description        : This routine is called from VLAN to set the          */
/*                      All To One Bundling Status from L2IWF Context Info   */
/*                                                                           */
/* Input(s)           : u4ContextId - ContextId                              */
/*                      u1Status -   OSIX_TRUE / OSIX_FALSE                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetAllToOneBndlStatus (UINT4 u4ContextId, UINT1 u1Status)
{
    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_ALL_TO_ONE_BUNDLING_STATUS () = u1Status;

    L2IwfReleaseContext ();

    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetTagInfoFromFrame                         */
/*                                                                           */
/*    Description         : This function is called by protocol modules      */
/*                          (CFM/STP) to decode the tag information from the */
/*                          buffer information                               */
/*                          vlan id, Isid , prority, DE                      */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u4IfIndex - Port received the frame              */
/*                                                                           */
/*    Output(s)           : *pTagInfo- VlanId/Isid, Priority, DE for recvd   */
/*                           frame                                           */
/*                          *pu1IngressAction - VLAN_TRUE (If Port is not    */
/*                           the member port of VLAN and Ingress filtering is*/
/*                           enabled)                                        */
/*                           VLAN_FALSE (If port is member of VLAN)          */
/*                            pu4DataOffSet- offset of the protocol PDU in buf*/
/*                                                                           */
/*    Returns            : L2IWF_SUCCESS/                                    */
/*                         L2IWF_FAILURE                                     */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfGetTagInfoFromFrame (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                          tPbbTag * pPbbTag, UINT1 *pu1IngressAction,
                          UINT4 *pu4ITagOffset, UINT4 *pu4DataOffSet,
                          UINT1 u1FrameType, UINT1 *pu1IsSend)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4Context;
    UINT2               u2LocalPort = 0;
    INT4                i4RetVal = L2IWF_SUCCESS;

    if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                      &u4Context, &u2LocalPort) != VCM_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    i4RetVal = L2IwfGetTagInfo (u4ContextId,
                                u2LocalPort,
                                pBuf, pVlanTag,
                                pPbbTag,
                                pu1IngressAction,
                                pu4ITagOffset,
                                pu4DataOffSet,
                                u1FrameType, IfInfo.u1BrgPortType, pu1IsSend);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfTransmitPbbFrame                                */
/*                                                                           */
/* Description        : This routine sends buf on the outgoing port.         */
/*                                                                           */
/* Input(s)           : pBuf - Buffer send.                                  */
/*                      u2PortNum - sending port                             */
/*                      pVlanTagInfo, pPbbTag                            */
/*                      u1FrameType - CFM_CC, CFM_LB or CFM_LT               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfTransmitPbbFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                       tVlanTag * pVlanTagInfo, tPbbTag * pPbbTag,
                       UINT1 u1FrameType)
{
    UINT1              *pFwdPortList = NULL;
    tVlanOutIfMsg       VlanOutIfMsg;
    tCfaIfInfo          IfInfo;
    tCRU_BUF_CHAIN_HEADER *pDupCruBuf;
    tCfaILanPortStruct *pIfnode = NULL;
    tTMO_SLL            OutInfo;
    UINT4               u4TempContextId = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4LocalIsid = 0;
    UINT4               u4IfIndx = 0;
    UINT2               u2TempVip = 0;
    UINT2               u2LocalPort;
    UINT2               u2PortNum;
    UINT2               u2Vip = 0;
    UINT2               u2LenOrType = 0;
    UINT2               u2PipPort = 0;
    tVlanId             VlanId = 0;
    UINT1               u1SendPbb = OSIX_FALSE;
    UINT1               u1Priority = 0;
    UINT1               u1Result = OSIX_FALSE;
    tMacAddr            GlobalBDA;

    if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                      &u4ContextId, &u2PortNum) != VCM_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2IWF_FAILURE;
    }

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2IWF_FAILURE;
    }

    if (IfInfo.u1BrgPortType == CFA_VIRTUAL_INSTANCE_PORT)
    {
        return L2IwfTransmitFrameOnVip (u4ContextId, u4IfIndex, pBuf,
                                        pVlanTagInfo, pPbbTag, u1FrameType);
    }
    else if (IfInfo.u1BrgPortType == CFA_PROVIDER_INSTANCE_PORT)
    {
        UINT2               u2VipPortNum = 0;
        /* Get the VIP port */
        if (L2iwfPbbGetPipVipWithIsid
            (u4ContextId, u2PortNum, pPbbTag->InnerIsidTag.u4Isid,
             &u2VipPortNum, &u2LocalPort) != L2IWF_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2IWF_FAILURE;
        }
        /* Transmit to the corresponding PIP */
        if (L2IwfTransmitFrameOnPip
            (u4ContextId, u2VipPortNum, pBuf, pPbbTag,
             u1FrameType) != L2IWF_SUCCESS)
        {
            return L2IWF_FAILURE;
        }
        return L2IWF_SUCCESS;
    }
    else if (IfInfo.u1BrgPortType == CFA_CUSTOMER_BACKBONE_PORT)
    {
        pFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pFwdPortList == NULL)
        {
            return L2IWF_FAILURE;
        }
        MEMSET (pFwdPortList, 0, sizeof (tLocalPortList));

        /* Verify that the CBP is a member port of the ISID transmitted */
        if (L2IwfPbbGetCbpFwdPortListForIsid (u4ContextId, u2PortNum,
                                              pPbbTag->InnerIsidTag.u4Isid,
                                              pFwdPortList) != L2IWF_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2IWF_FAILURE;
        }

        OSIX_BITLIST_IS_BIT_SET (pFwdPortList, u2PortNum,
                                 CONTEXT_PORT_LIST_SIZE, u1Result);
        UtilPlstReleaseLocalPortList (pFwdPortList);
        if (u1Result == OSIX_FALSE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2IWF_FAILURE;
        }
        /* Modify the BDA in case we already have an I-TAG in the PDU or else
         * add local BDA and BSA of the CBP */
        if (pPbbTag->InnerIsidTag.u1TagType == VLAN_TAGGED)
        {
            /* Get the Global BSIGA */
            PbbGetBCompBDA (u4ContextId, u2PortNum,
                            pPbbTag->InnerIsidTag.u4Isid, GlobalBDA,
                            L2IWF_INGRESS, OSIX_FALSE);
            /* Check if the BDA received in the frame equals the global BSIGA */
            if (MEMCMP (pPbbTag->BDAMacAddr, GlobalBDA, sizeof (tMacAddr)) == 0)
            {
                /*if yes then replace it with local BSIGA */
                /* Get the local BSIGA */
                PbbGetBCompBDA (u4ContextId, u2PortNum,
                                pPbbTag->InnerIsidTag.u4Isid,
                                pPbbTag->BDAMacAddr, L2IWF_EGRESS, OSIX_FALSE);
            }
        }
        else
        {
            /* For multicast pdus only we need to used B-SIGA, unicast PDU will
             * be having the CDA as the B-SIGA
             */
            if ((pPbbTag->InnerIsidTag.u1UcaBitValue == OSIX_TRUE) &&
                (L2IWF_IS_MULTICAST_ADDR (pPbbTag->InnerIsidTag.CDAMacAddr) ==
                 L2IWF_TRUE))
            {
                /* Get the BDA */
                PbbGetBCompBDA (u4ContextId, u2PortNum,
                                pPbbTag->InnerIsidTag.u4Isid,
                                pPbbTag->BDAMacAddr, L2IWF_EGRESS, OSIX_FALSE);
            }
            else
            {
                MEMCPY (pPbbTag->BDAMacAddr, pPbbTag->InnerIsidTag.CDAMacAddr,
                        sizeof (tMacAddr));
            }
            /* Place the source mac address of the CBP as BSA */
            MEMCPY (pPbbTag->BSAMacAddr, IfInfo.au1MacAddr, sizeof (tMacAddr));
        }
        /* Remove the B-TAG if present in the frame */
        if (pPbbTag->OuterVlanTag.u1TagType == VLAN_TAGGED)
        {
            /* CBP will allways be a untagged member port for B-VLAN */
            if (VlanTagOutFrame
                (u4ContextId, u2PortNum, pBuf,
                 &(pPbbTag->OuterVlanTag)) != VLAN_SUCCESS)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return L2IWF_FAILURE;
            }
        }
        /* Convert to Local-ISID */
        PbbGetLocalIsidFromRelayIsid (u4ContextId, u2PortNum,
                                      pPbbTag->InnerIsidTag.u4Isid,
                                      &u4LocalIsid);
        if (u4LocalIsid != 1)
        {
            pPbbTag->InnerIsidTag.u4Isid = u4LocalIsid;
        }
        /* 
         * Add/Update the I-TAG as per the I-TAG present status of the frame.
         */
        if (pPbbTag->InnerIsidTag.u1TagType == VLAN_UNTAGGED)
        {
            L2iwfAppendIsidTagToFrame (pBuf, pPbbTag);
        }
        else
        {
            /* Update the I-TAG */
            L2iwfUpdateIsidTag (pBuf, pPbbTag);
        }
        if (CfaIsVirtualInterface (u4IfIndex) == CFA_TRUE)
        {
            /* get the associated B component context for this */
            if (CfaGetIlanPorts (u4IfIndex, &OutInfo) != CFA_SUCCESS)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return L2IWF_FAILURE;
            }
            TMO_SLL_Scan (&OutInfo, pIfnode, tCfaILanPortStruct *)
            {
                /* duplicate the buffer */
                pDupCruBuf = L2iwfDuplicateCruBuf (pBuf);
                if (pDupCruBuf == NULL)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    return L2IWF_FAILURE;
                }
                if (u1FrameType == L2_ECFM_PACKET)
                {
                    EcfmCcHandleInFrameFromPort (pDupCruBuf,
                                                 pIfnode->u4IfIndex);
                }
                else if ((u1FrameType == L2_STAP_PKT) ||
                         (u1FrameType == L2_MMRP_PKT))
                {
                    /* For STP and MMRP Pkts, all the pre-processing including
                     * removing I-TAG to be done here. Module should be rx PDU
                     * over VIP as if it is directly connected to another VIP
                     * */
                    u4TempContextId = 0;
                    VcmGetContextInfoFromIfIndex (pIfnode->u4IfIndex,
                                                  &u4TempContextId,
                                                  &u2LocalPort);

                    if (PbbGetPipVipWithIsid (u4TempContextId,
                                              u2LocalPort,
                                              pPbbTag->InnerIsidTag.u4Isid,
                                              &u2TempVip, &u2PipPort)
                        != PBB_SUCCESS)
                    {
                        CRU_BUF_Release_MsgBufChain (pDupCruBuf, FALSE);
                        continue;
                    }

                    /* Validate that the ISID(VIP mapped to PIP) in the packet
                       recived is same as the ISID(VIP) mapped to the PIP  */
                    if (u2LocalPort != u2PipPort)
                    {
                        CRU_BUF_Release_MsgBufChain (pDupCruBuf, FALSE);
                    }
                    else
                    {
                        u2Vip = u2TempVip;
                        if (L2IWF_CONTEXT_PEER_TUNNEL_INFO (u4TempContextId) >
                            0)
                        {
                            switch (u1FrameType)
                            {
                                case L2_STAP_PKT:

                                    L2iwfAstHandleInFrame (u4TempContextId,
                                                           u2Vip, pDupCruBuf);
                                    break;

                                case L2_MMRP_PKT:

                                    if (VcmGetIfIndexFromLocalPort (u4ContextId,
                                                                    u2Vip,
                                                                    &u4IfIndx)
                                        != VCM_SUCCESS)
                                    {
                                        CRU_BUF_Release_MsgBufChain (pDupCruBuf,
                                                                     FALSE);
                                        return L2IWF_FAILURE;
                                    }
                                    CRU_BUF_Move_ValidOffset (pDupCruBuf,
                                                              PBB_CDA_OFFSET_IN_ITAG_PKT);

                                    MrpApiEnqueueRxPkt (pDupCruBuf,
                                                        u4IfIndx,
                                                        MRP_STAP_CIST_ID);
                                default:
                                    /* Cannot come over here */
                                    break;
                            }
                        }
                        else
                        {
                            CRU_BUF_Release_MsgBufChain (pDupCruBuf, FALSE);
                        }

                    }
                }
                else
                {
                    CRU_BUF_Release_MsgBufChain (pDupCruBuf, FALSE);
                }

            }
            CfaFreeILanListNodes (&OutInfo);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2IWF_SUCCESS;
        }
        else
        {
            u1SendPbb = OSIX_TRUE;
        }
    }
    else if (IfInfo.u1BrgPortType == CFA_PROVIDER_NETWORK_PORT)
    {
        /* add the B-vid tag information into the pBuf */
        pPbbTag->OuterVlanTag.u1Priority = pPbbTag->InnerIsidTag.u1Priority;

        pPbbTag->OuterVlanTag.u1DropEligible =
            pPbbTag->InnerIsidTag.u1DropEligible;
        /* Check if I-TAG was present in the received frame, update/add I-TAG if
         * we have received a I-TAG or we have ISID information*/
        if (pPbbTag->InnerIsidTag.u4Isid != 0)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                                       VLAN_TAG_OFFSET, VLAN_TYPE_OR_LEN_SIZE);
            u2LenOrType = OSIX_NTOHS (u2LenOrType);
            /* Add I-TAG only when we have no I-TAG present in the frame */
            if (pPbbTag->InnerIsidTag.u1TagType == VLAN_UNTAGGED)
            {
                if (u2LenOrType != VLAN_PROVIDER_BACKBONE_PROTOCOL_ID)
                {
                    L2iwfAppendIsidTagToFrame (pBuf, pPbbTag);
                }
            }
            else
            {
                /* update only if B-TAG is not present */
                if (u2LenOrType == VLAN_PROVIDER_BACKBONE_PROTOCOL_ID)
                {
                    /* Update the I-TAG */
                    L2iwfUpdateIsidTag (pBuf, pPbbTag);
                    CRU_BUF_Copy_FromBufChain (pBuf,
                                               pPbbTag->InnerIsidTag.CDAMacAddr,
                                               0, ETHERNET_ADDR_SIZE);
                    CRU_BUF_Copy_FromBufChain (pBuf,
                                               pPbbTag->InnerIsidTag.CSAMacAddr,
                                               ETHERNET_ADDR_SIZE,
                                               ETHERNET_ADDR_SIZE);
                }
            }
        }
        if (VlanTagOutFrame
            (u4ContextId, u2PortNum, pBuf,
             &(pPbbTag->OuterVlanTag)) != VLAN_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2IWF_FAILURE;
        }
        VlanId = pPbbTag->OuterVlanTag.u2VlanId;
    }
    else
    {
        /* CNP */
        /* remove the BDA,BSA and Isid tag information present in the pBuf */
        if (pPbbTag->InnerIsidTag.u1TagType == VLAN_TAGGED)
        {
            L2iwfCheckAndRemoveItagFromFrame (pBuf);
        }
        if (VlanTagOutFrame
            (u4ContextId, u2PortNum, pBuf,
             &(pVlanTagInfo->OuterVlanTag)) != VLAN_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2IWF_FAILURE;
        }
        VlanId = pVlanTagInfo->OuterVlanTag.u2VlanId;
    }

    VlanOutIfMsg.u2Port = u2PortNum;
    /* update the length of the message here */
    VlanOutIfMsg.u2Length = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);
    L2IWF_GET_TIMESTAMP (&(VlanOutIfMsg.u4TimeStamp));
    if (u1FrameType == L2_STAP_PKT)
    {
        VlanOutIfMsg.u1FrameType = u1FrameType;
    }
    else
    {
        VlanOutIfMsg.u1FrameType = VLAN_CFM_FRAME;
    }

    /* call Vlan or PBB specific functions to send */
    if (u1SendPbb == OSIX_TRUE)
    {
        return PbbSendFrameToPort (pBuf, &VlanOutIfMsg,
                                   u4ContextId, u2PortNum,
                                   pPbbTag->InnerIsidTag.u4Isid,
                                   pPbbTag->InnerIsidTag.u1Priority);
    }
    else
    {
        return VlanSendFrameToPort (pBuf, &VlanOutIfMsg,
                                    u4ContextId, u2PortNum, VlanId, u1Priority);
    }
    return L2IWF_SUCCESS;
}

/******************************************************************************
 * Function Name      : L2IwfTransmitFrameFromCbp
 *
 * Description        : This routine is used to forward the CFM-PDU to the
 *                      Frame out of CBP to PIP
 *
 * Input(s)           : u4ContextId :- Context Identifier
                        u2LocalPort  :- Local Port
                        pBuf         :- Buffer to be forwarded
                        pPbbTag      :- BVLAN and ISID tag infomation
                        pVlanTag     :- S and C Information
                        u1FrameType  :- Frame Type
 * Output(s)          : None
 *
 * Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE
 *****************************************************************************/
INT4
L2IwfTransmitFrameFromCbp (UINT4 u4ContextId, UINT2 u2LocalPort,
                           tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                           tPbbTag * pPbbTag, UINT1 u1FrameType)
{
    UINT1              *pIfFwdPortList = NULL;
    tCRU_BUF_CHAIN_HEADER *ptempBuf = NULL;
    UINT4               u4BIfIndx = L2IWF_INIT_VAL;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2PortNum = 0;
    UINT1               u1PortFlag = 0;

    pIfFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pIfFwdPortList == NULL)
    {
        return L2IWF_FAILURE;
    }
    MEMSET (pIfFwdPortList, 0, sizeof (tLocalPortList));

    /*get the PNP port list for the B-vid */
    if (VlanGetFwdPortList (u4ContextId, u2LocalPort,
                            pPbbTag->BSAMacAddr,
                            pPbbTag->BDAMacAddr,
                            pPbbTag->OuterVlanTag.u2VlanId,
                            pIfFwdPortList) != L2IWF_SUCCESS)
    {
        if (L2IwfMiGetVlanLocalEgressPorts
            (u4ContextId, pPbbTag->OuterVlanTag.u2VlanId,
             pIfFwdPortList) == L2IWF_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            UtilPlstReleaseLocalPortList (pIfFwdPortList);
            return L2IWF_FAILURE;
        }
    }
    OSIX_BITLIST_RESET_BIT (pIfFwdPortList, u2LocalPort,
                            sizeof (tLocalPortList));

    /*Get the egress Port and forward the packet */
    for (u2ByteInd = L2IWF_INIT_VAL; u2ByteInd < L2IWF_PORT_LIST_SIZE;
         u2ByteInd++)
    {
        if (pIfFwdPortList[u2ByteInd] == 0)
        {
            continue;
        }
        u1PortFlag = pIfFwdPortList[u2ByteInd];
        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & 0x80) != 0)
            {
                u2PortNum =
                    (UINT2) ((u2ByteInd * BITS_PER_BYTE) + u2BitIndex + 1);

                if (VcmGetIfIndexFromLocalPort (u4ContextId, u2PortNum,
                                                &u4BIfIndx) != VCM_SUCCESS)
                {
                    continue;
                }

                ptempBuf = L2iwfDuplicateCruBuf (pBuf);
                if (ptempBuf != NULL)
                {
                    L2IwfTransmitPbbFrame (ptempBuf, u4BIfIndx,
                                           pVlanTag, pPbbTag, u1FrameType);
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    UtilPlstReleaseLocalPortList (pIfFwdPortList);
    return L2IWF_SUCCESS;
}

/******************************************************************************
 * Function Name      : L2IwfTransmitFrameOnVip
 *
 * Description        : This routine is used to forward the CFM-PDU on VIP port  
 *
 * Input(s)           :u4ContextId:- Context Id
                      u4IfIndex :- VIP IfIndex
                      pBuf:- Buffer to be transmitted
                      pVlanTag:- Vlan Tag Info
                      pPbbTag:- BVLAN and ISID tag information
                      u1FrameType:- Frame Type to be tranmitted
 
 * Output(s)          : None
 *
 * Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE
 *****************************************************************************/
INT4
L2IwfTransmitFrameOnVip (UINT4 u4ContextId, UINT4 u4IfIndex,
                         tCRU_BUF_CHAIN_DESC * pBuf,
                         tVlanTag * pVlanTag, tPbbTag * pPbbTag,
                         UINT1 u1FrameType)
{
    UINT4               u4Context = L2IWF_INIT_VAL;
    UINT2               u2LocalPort = L2IWF_INIT_VAL;

    if (VcmGetContextInfoFromIfIndex (u4IfIndex,
                                      &u4Context, &u2LocalPort) != VCM_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2IWF_FAILURE;
    }

    if (u1FrameType != L2_STAP_PKT)
    {
        if (VlanTagOutFrame (u4ContextId, u2LocalPort, pBuf,
                             &(pVlanTag->OuterVlanTag)) != VLAN_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2IWF_FAILURE;
        }
    }
    if (L2IwfTransmitFrameOnPip
        (u4ContextId, u2LocalPort, pBuf, pPbbTag, u1FrameType) != L2IWF_SUCCESS)
    {
        return L2IWF_FAILURE;
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************
 * Function Name      : L2IwfTransmitFrameOnPip
 *
 * Description        : This routine is used to forward the CFM-PDU on PIP port  
 *
 * Input(s)           : u4ContextId:- Context Id
 *                      u2VipPortNum - VIP local port number
 *                      pBuf:- Buffer to be transmitted
 *                      pPbbTag:- BVLAN and ISID tag information
 *                      u1FrameType:- Frame Type to be tranmitted
 * 
 * Output(s)          : None
 *
 * Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE
 *****************************************************************************/
PRIVATE INT4
L2IwfTransmitFrameOnPip (UINT4 u4ContextId, UINT2 u2VipPortNum,
                         tCRU_BUF_CHAIN_DESC * pBuf, tPbbTag * pPbbTag,
                         UINT1 u1FrameType)
{
    UINT1              *pFwdLocalPortList = NULL;
    tCfaIfInfo          IfInfo;
    tTMO_SLL            OutInfo;
    tVlanOutIfMsg       VlanOutIfMsg;
    tCRU_BUF_CHAIN_HEADER *pDuplicateBuf = NULL;
    tCfaILanPortStruct *pILanNode = NULL;
    UINT4               u4Context = L2IWF_INIT_VAL;
    UINT4               u4PipIfIndex = L2IWF_INIT_VAL;
    UINT4               u4PnPIfIndex = L2IWF_INIT_VAL;
    UINT4               u4BContext = L2IWF_INIT_VAL;
    UINT4               u4Isid = L2IWF_INIT_VAL;
    UINT2               u2PipPortNum = L2IWF_INIT_VAL;
    UINT2               u2PortNum = L2IWF_INIT_VAL;
    UINT2               u2ByteInd = L2IWF_INIT_VAL;
    UINT2               u2BitIndex = L2IWF_INIT_VAL;
    UINT2               u2CbpPort = L2IWF_INIT_VAL;
    tVlanId             Bvid = L2IWF_INIT_VAL;
    UINT1               u1PortFlag = L2IWF_INIT_VAL;
    tMacAddr            DefaultBDA;
    tMacAddr            NullMacAddr = { 0 };

    MEMSET (&IfInfo, 0x00, sizeof (tCfaIfInfo));
    MEMSET (DefaultBDA, 0, sizeof (tMacAddr));

    if (PbbGetPipIsidWithVip (u4ContextId, u2VipPortNum,
                              &u4PipIfIndex, &u4Isid, &DefaultBDA) != SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2IWF_FAILURE;
    }
    if (VcmGetContextInfoFromIfIndex (u4PipIfIndex,
                                      &u4Context, &u2PipPortNum) != VCM_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2IWF_FAILURE;
    }
    if (CfaGetIfInfo (u4PipIfIndex, &IfInfo) != CFA_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2IWF_FAILURE;
    }
    if (MEMCMP (IfInfo.au1MacAddr, NullMacAddr, sizeof (tMacAddr)) != 0)
    {
        MEMCPY (pPbbTag->BSAMacAddr, IfInfo.au1MacAddr, sizeof (tMacAddr));
    }
    else
    {
        /* Fill the bridge-mac address if we dont have any mac-address configure
         * for the port */
        IssGetContextMacAddress (u4Context, pPbbTag->BSAMacAddr);
    }
    /* For multicast pdus only we need to used B-SIGA, unicast PDU will
     * be having the CDA as the B-SIGA
     */
    if ((u1FrameType != L2_STAP_PKT) &&
        (pPbbTag->InnerIsidTag.u1UcaBitValue == OSIX_TRUE) &&
        (L2IWF_IS_MULTICAST_ADDR (pPbbTag->InnerIsidTag.CDAMacAddr)
         != L2IWF_TRUE))
    {
        MEMCPY (pPbbTag->BDAMacAddr, pPbbTag->InnerIsidTag.CDAMacAddr,
                sizeof (tMacAddr));
    }
    else
    {
        MEMCPY (pPbbTag->BDAMacAddr, DefaultBDA, sizeof (tMacAddr));
    }
    if (u1FrameType == L2_STAP_PKT)
    {
        /* For ECFM-PDU this ISID will already be filled */
        pPbbTag->InnerIsidTag.u4Isid = u4Isid;
        pPbbTag->InnerIsidTag.u1TagType = VLAN_UNTAGGED;

        CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->InnerIsidTag.CDAMacAddr,
                                   0, ETHERNET_ADDR_SIZE);
        CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->InnerIsidTag.CSAMacAddr,
                                   ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);
    }
    if (pPbbTag->InnerIsidTag.u1TagType != VLAN_TAGGED)
    {
        L2iwfAppendIsidTagToFrame (pBuf, pPbbTag);
    }
    else
    {
        /* Update the I-TAG */
        L2iwfUpdateIsidTag (pBuf, pPbbTag);
    }
    VlanOutIfMsg.u2Port = u2PipPortNum;
    /* update the length of the message here */
    VlanOutIfMsg.u2Length = (UINT2) CRU_BUF_Get_ChainValidByteCount (pBuf);
    L2IWF_GET_TIMESTAMP (&(VlanOutIfMsg.u4TimeStamp));
    VlanOutIfMsg.u1FrameType = u1FrameType;

    if (CfaIsVirtualInterface (u4PipIfIndex) == CFA_TRUE)
    {
        /* get the associated B component context for this */
        if (CfaGetIlanPorts (u4PipIfIndex, &OutInfo) != CFA_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return L2IWF_FAILURE;
        }
        pPbbTag->InnerIsidTag.u4Isid = u4Isid;
        TMO_SLL_Scan (&OutInfo, pILanNode, tCfaILanPortStruct *)
        {
            if (u1FrameType == L2_ECFM_PACKET)
            {
                pDuplicateBuf = L2iwfDuplicateCruBuf (pBuf);
                if (pDuplicateBuf == NULL)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    CfaFreeILanListNodes (&OutInfo);
                    return L2IWF_FAILURE;
                }
                EcfmCcHandleInFrameFromPort (pDuplicateBuf,
                                             pILanNode->u4IfIndex);
            }
            else if (u1FrameType == L2_STAP_PKT)
            {
                if (VcmGetContextInfoFromIfIndex (pILanNode->u4IfIndex,
                                                  &u4BContext,
                                                  &u2CbpPort) != VCM_SUCCESS)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    CfaFreeILanListNodes (&OutInfo);
                    return L2IWF_FAILURE;
                }
                if (PbbGetBvidForIsid (u4BContext, u2CbpPort, u4Isid,
                                       &Bvid) != SUCCESS)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    CfaFreeILanListNodes (&OutInfo);
                    return L2IWF_FAILURE;
                }
                if (u1FrameType == L2_ECFM_PACKET)
                {
                    pDuplicateBuf = L2iwfDuplicateCruBuf (pBuf);
                    if (pDuplicateBuf == NULL)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        CfaFreeILanListNodes (&OutInfo);
                        return L2IWF_FAILURE;
                    }
                    VlanHwAddFdbEntry (pILanNode->u4IfIndex,
                                       pPbbTag->BSAMacAddr, Bvid,
                                       VLAN_DELETE_ON_TIMEOUT);
                    EcfmCcHandleInFrameFromPort (pDuplicateBuf,
                                                 pILanNode->u4IfIndex);
                }
                else if (u1FrameType == L2_STAP_PKT)
                {

                    pPbbTag->OuterVlanTag.u2VlanId = Bvid;
                    pPbbTag->OuterVlanTag.u1TagType = VLAN_UNTAGGED;
                    pFwdLocalPortList =
                        UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

                    if (pFwdLocalPortList == NULL)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        CfaFreeILanListNodes (&OutInfo);
                        return L2IWF_FAILURE;
                    }
                    MEMSET (pFwdLocalPortList, 0, sizeof (tLocalPortList));
                    /*get the PNP port list for the B-vid */
                    if (VlanGetFwdPortList (u4BContext, u2CbpPort,
                                            pPbbTag->BSAMacAddr,
                                            pPbbTag->BDAMacAddr,
                                            Bvid,
                                            pFwdLocalPortList) != L2IWF_SUCCESS)
                    {
                        if (L2IwfMiGetVlanLocalEgressPorts
                            (u4BContext, Bvid,
                             pFwdLocalPortList) == L2IWF_FAILURE)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            CfaFreeILanListNodes (&OutInfo);
                            UtilPlstReleaseLocalPortList (pFwdLocalPortList);
                            return L2IWF_FAILURE;
                        }
                    }
                    OSIX_BITLIST_RESET_BIT (pFwdLocalPortList, u2CbpPort,
                                            sizeof (tLocalPortList));

                    /*Get the egress Port and forward the packet */
                    for (u2ByteInd = L2IWF_INIT_VAL;
                         u2ByteInd < CONTEXT_PORT_LIST_SIZE; u2ByteInd++)
                    {
                        if (pFwdLocalPortList[u2ByteInd] == 0)
                        {
                            continue;
                        }
                        u1PortFlag = pFwdLocalPortList[u2ByteInd];
                        for (u2BitIndex = 0;
                             ((u2BitIndex < BITS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
                        {
                            if ((u1PortFlag & 0x80) != 0)
                            {
                                u2PortNum =
                                    (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                             u2BitIndex + 1);

                                pDuplicateBuf = L2iwfDuplicateCruBuf (pBuf);

                                if (pDuplicateBuf != NULL)
                                {
                                    /* We dont need to sent the pVlanInfo for the PNP port as we can only
                                     * have B-VID for a PNP and which is already present in pPbbTag
                                     */
                                    if (VcmGetIfIndexFromLocalPort
                                        (u4BContext, u2PortNum,
                                         &u4PnPIfIndex) != VCM_FAILURE)
                                    {
                                        /* All the control packets from I-Component/PBN will be treated
                                         * as data packets in B-Comps of PBBN. Hence, if there are two PNP
                                         * member ports for a B-VLAN, the control packets ingressed through
                                         * one of the PNPs will try to egress through the corresponding PIP 
                                         * and another PNP. In this case, check for the spanning tree port
                                         * state for the other PNP. Otherwise, the BPDUs will loop inside the
                                         * network
                                         * */
                                        if (L2IwfGetVlanPortState
                                            (pPbbTag->OuterVlanTag.u2VlanId,
                                             u4PnPIfIndex) ==
                                            AST_PORT_STATE_FORWARDING)
                                        {
                                            L2IwfTransmitPbbFrame
                                                (pDuplicateBuf, u4PnPIfIndex,
                                                 NULL, pPbbTag, u1FrameType);
                                        }
                                        else
                                        {
                                            CRU_BUF_Release_MsgBufChain
                                                (pDuplicateBuf, FALSE);

                                        }
                                    }
                                    else
                                    {
                                        CRU_BUF_Release_MsgBufChain
                                            (pDuplicateBuf, FALSE);
                                    }
                                }
                            }
                            u1PortFlag = (UINT1) (u1PortFlag << 1);
                        }
                    }
                    UtilPlstReleaseLocalPortList (pFwdLocalPortList);
                }
            }
        }
        CfaFreeILanListNodes (&OutInfo);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    else
    {
        /* append BDA, BSA and Isid information to the pBuf from PPbbTag */
        /* copy the priority information present in the 
         * vlan tag info outer tag into isid tag info inner tag*/
        return PbbSendFrameToPort (pBuf, &VlanOutIfMsg,
                                   u4ContextId, u2PipPortNum,
                                   pPbbTag->InnerIsidTag.u4Isid,
                                   pPbbTag->InnerIsidTag.u1Priority);
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbbProcessDataPacket                            */
/*                                                                           */
/* Description        : This routine is called for processing the data packet*/
/*                      and getting the Tag information from the packet      */
/*                      received                                             */
/*                                                                           */
/* Input(s)           : pBuf - Control Packet bufffer                        */
/*                      u4IfIndex - Port Index                               */
/*                      u4ContextId - Context Id                             */
/*                      pu1Dst - Destination Mac Address                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbbProcessDataPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                           UINT4 u4ContextId, UINT1 *pu1Dst)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tMacAddr            CustDestAddr = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x00 };
    tMacAddr            ProviderDestAddr =
        { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x08 };
    tMacAddr            MmrpAddr = { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x20 };
    tPbbTag             PbbTag;
    tVlanTag            VlanTag;
    UINT4               u4ITagOffset = 0;
    UINT4               u4DataOffSet;
    UINT1               u1IngressAction;
    UINT1               u1IsSend = OSIX_FALSE;
    UINT1               u1FrameType = L2_INVALID_PKT;

    L2_LOCK ();
    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CfaIfSetInDiscard (u4IfIndex);
        return L2IWF_FAILURE;
    }

    if (pL2PortEntry->u2PbPortType ==
        L2IWF_PROVIDER_INSTANCE_PORT ||
        pL2PortEntry->u2PbPortType == L2IWF_PROVIDER_NETWORK_PORT)
    {
        /* Need to read CDA from I-Tag */
        if (pL2PortEntry->u2PbPortType == L2IWF_PROVIDER_INSTANCE_PORT)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, pu1Dst,
                                       ISID_TAGGED_HEADER_SIZE,
                                       ETHERNET_ADDR_SIZE);
        }
        else
        {
            if (pL2PortEntry->u2PbPortType == L2IWF_PROVIDER_NETWORK_PORT)
            {
                CRU_BUF_Copy_FromBufChain (pBuf,
                                           pu1Dst,
                                           PBB_CDA_OFFSET_IN_BITAG_PKT,
                                           ETHERNET_ADDR_SIZE);

            }
        }

        if ((MEMCMP (pu1Dst, CustDestAddr, ETHERNET_ADDR_SIZE) == 0) ||
            (MEMCMP (pu1Dst, ProviderDestAddr, ETHERNET_ADDR_SIZE) == 0))
        {
            u1FrameType = L2_STAP_PKT;
        }
        else if ((MEMCMP (pu1Dst, MmrpAddr, ETHERNET_ADDR_SIZE) == 0))
        {
            u1FrameType = L2_MMRP_PKT;
        }
        if (u1FrameType != L2_INVALID_PKT)
        {
            L2_UNLOCK ();
            if (L2IwfGetTagInfo (u4ContextId,
                                 pL2PortEntry->u2LocalPortId,
                                 pBuf, &VlanTag,
                                 &PbbTag,
                                 &u1IngressAction,
                                 &u4ITagOffset,
                                 &u4DataOffSet,
                                 L2_STAP_PKT,
                                 (UINT1) pL2PortEntry->u2PbPortType,
                                 &u1IsSend) != L2IWF_FAILURE)
            {
                if (u1IsSend == OSIX_TRUE)
                {
                    if (L2IwfPbbTxFrameToIComp (u4ContextId,
                                                u4IfIndex,
                                                pBuf, &VlanTag,
                                                &PbbTag,
                                                &u4ITagOffset,
                                                L2_STAP_PKT,
                                                pL2PortEntry->u2LocalPortId)
                        == L2IWF_FAILURE)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        CfaIfSetInDiscard (u4IfIndex);
                        return L2IWF_FAILURE;
                    }
                }
                return L2IWF_SUCCESS;
            }
            else
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
                return L2IWF_FAILURE;
            }
        }
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    CfaIfSetInDiscard (u4IfIndex);
    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetTagInfo                                  */
/*                                                                           */
/*    Description         : This function is called to decode the tag        */
/*                          information from the buffer information          */
/*                          vlan id, Isid , prority, DE                      */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u1PbPortType - Port Type of the Local Port       */
/*                          u2LocalPort - Local Port                         */
/*                                                                           */
/*    Output(s)           : *pTagInfo- VlanId/Isid, Priority, DE for recvd   */
/*                           frame                                           */
/*                          *pu1IngressAction - VLAN_TRUE (If Port is not    */
/*                           the member port of VLAN and Ingress filtering is*/
/*                           enabled)                                        */
/*                           VLAN_FALSE (If port is member of VLAN)          */
/*                            pu4DataOffSet- offset of the protocol PDU in buf*/
/*                                                                           */
/*    Returns            : L2IWF_SUCCESS/                                    */
/*                         L2IWF_FAILURE                                     */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfGetTagInfo (UINT4 u4ContextId, UINT2 u2LocalPort,
                 tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                 tPbbTag * pPbbTag, UINT1 *pu1IngressAction,
                 UINT4 *pu4ITagOffset, UINT4 *pu4DataOffSet, UINT1 u1FrameType,
                 UINT1 u1BrgPortType, UINT1 *pu1IsSend)
{
    UINT1              *pFwdPortList = NULL;
    UINT2               u2VipPort = 0;
    UINT2               u2PipPort = 0;
    UINT2               u2LenOrType = 0;
    UINT2               u2SavedIsidTagLen = 0;
    UINT1               u2ITagLen = 0;
    UINT1               au1SavedIsidTag[(VLAN_PBB_I_TAGGED_HEADER_SIZE -
                                         (2 * ETHERNET_ADDR_SIZE))];

    u2SavedIsidTagLen =
        (VLAN_PBB_I_TAGGED_HEADER_SIZE - (2 * ETHERNET_ADDR_SIZE));
    *pu4ITagOffset = VLAN_TAG_OFFSET;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                               *pu4ITagOffset, VLAN_TYPE_OR_LEN_SIZE);
    u2LenOrType = OSIX_NTOHS (u2LenOrType);

    if ((u1BrgPortType == CFA_PROVIDER_INSTANCE_PORT) ||
        (u1BrgPortType == CFA_CUSTOMER_BACKBONE_PORT))
    {
        /* PIP and CBP should always received I-Tagged PDU only */
        if (u2LenOrType != VLAN_PROVIDER_BACKBONE_PROTOCOL_ID)
        {
            return L2IWF_FAILURE;
        }
        /*Copy BDA and BSA into the pbb tag info */
        CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->BDAMacAddr,
                                   0, ETHERNET_ADDR_SIZE);
        CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->BSAMacAddr,
                                   ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);
        CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->InnerIsidTag.CDAMacAddr,
                                   PBB_CDA_OFFSET_IN_ITAG_PKT,
                                   ETHERNET_ADDR_SIZE);
        CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->InnerIsidTag.CSAMacAddr,
                                   (PBB_CDA_OFFSET_IN_ITAG_PKT +
                                    ETHERNET_ADDR_SIZE), ETHERNET_ADDR_SIZE);
        /* buffer contains I-tag and no b-tag */
        /* call the PBB specific function to decode the tag info */
        if (PbbGetIsidInfoFromFrame (u4ContextId,
                                     u2LocalPort,
                                     pBuf, pPbbTag, VLAN_TAG_OFFSET) != SUCCESS)
        {
            return L2IWF_FAILURE;
        }
        /* update the input offset value to the next tag offset */
        *pu4ITagOffset = VLAN_PBB_I_TAGGED_HEADER_SIZE;
        if (u1BrgPortType == CFA_PROVIDER_INSTANCE_PORT)
        {
            if (PbbGetPipVipWithIsid (u4ContextId,
                                      u2LocalPort,
                                      pPbbTag->InnerIsidTag.u4Isid,
                                      &u2VipPort, &u2PipPort) != PBB_SUCCESS)
            {
                return L2IWF_FAILURE;
            }

            /* Validate that the ISID(VIP mapped to PIP) in the packet recived
               is same as the ISID(VIP) mapped to the PIP  */
            if (u2LocalPort != u2PipPort)
            {
                return L2IWF_FAILURE;
            }

            if (u1FrameType == L2_STAP_PKT)
            {
                return L2iwfAstHandleInFrame (u4ContextId, u2VipPort, pBuf);

            }
            /* Assumption: PIP will only receive I-tagged PDUs only */
            /* Save the I-TAG - CDA and CSA in a local buffer, the vlan APIs
             * only work with the VLAN tags only */
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) au1SavedIsidTag,
                                       0, u2SavedIsidTagLen);
            CRU_BUF_Move_ValidOffset (pBuf, u2SavedIsidTagLen);
            /* decode S and C tags */
            if (VlanGetVlanInfoFromFrame (u4ContextId,
                                          u2VipPort,
                                          pBuf,
                                          pVlanTag,
                                          pu1IngressAction,
                                          pu4DataOffSet) != VLAN_SUCCESS)
            {
                /* Restore the I-TAG */
                CRU_BUF_Prepend_BufChain (pBuf, au1SavedIsidTag,
                                          u2SavedIsidTagLen);
                return L2IWF_FAILURE;
            }
            /* Restore the I-TAG */
            CRU_BUF_Prepend_BufChain (pBuf, au1SavedIsidTag, u2SavedIsidTagLen);
            /* Add the isid tag length to the ether type offset also */
            *pu4DataOffSet += u2SavedIsidTagLen;

        }
        else
        {
            if (VlanGetVlanInfoFromFrame (u4ContextId,
                                          u2LocalPort,
                                          pBuf,
                                          pVlanTag,
                                          pu1IngressAction,
                                          pu4DataOffSet) != VLAN_SUCCESS)
            {
                return L2IWF_FAILURE;
            }
            MEMCPY (&pPbbTag->OuterVlanTag, &pVlanTag->OuterVlanTag,
                    sizeof (tVlanTagInfo));
        }
    }
    else if (u1BrgPortType == CFA_PROVIDER_NETWORK_PORT)
    {
        /* buffer contains the B-tag and I-tag */
        if (VlanGetVlanInfoFromFrame (u4ContextId,
                                      u2LocalPort,
                                      pBuf, pVlanTag,
                                      pu1IngressAction,
                                      pu4DataOffSet) != VLAN_SUCCESS)
        {

            return L2IWF_FAILURE;
        }
        /* copy the BVLAN tag details */
        MEMCPY (&pPbbTag->OuterVlanTag, &pVlanTag->OuterVlanTag,
                sizeof (tVlanTagInfo));
        /* Check is the frame is B-VLAN tagged */
        *pu4ITagOffset = VLAN_TAG_OFFSET;
        if (pPbbTag->OuterVlanTag.u1TagType == VLAN_TAGGED)
        {
            *pu4ITagOffset += VLAN_TAG_PID_LEN;
        }
        /* Check if I-Tag is present in the frame, if so then decode the frame
         * for I-TAG */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                                   *pu4ITagOffset, VLAN_TYPE_OR_LEN_SIZE);
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
        if (u2LenOrType == VLAN_PROVIDER_BACKBONE_PROTOCOL_ID)
        {
            if (PbbGetIsidInfoFromFrame (u4ContextId,
                                         u2LocalPort,
                                         pBuf,
                                         pPbbTag, *pu4ITagOffset) != SUCCESS)
            {
                return L2IWF_FAILURE;
            }
            CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->InnerIsidTag.CDAMacAddr,
                                       *pu4ITagOffset + PBB_ISID_TAG_PID_LEN,
                                       ETHERNET_ADDR_SIZE);
            CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->InnerIsidTag.CSAMacAddr,
                                       *pu4ITagOffset + PBB_ISID_TAG_PID_LEN +
                                       ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);

            /* try to move to the protocol ether-type */
            u2ITagLen = ISID_TAGGED_HEADER_SIZE;
        }
        else
        {
            /* no I-TAG received set the status as untagged */
            pPbbTag->InnerIsidTag.u4Isid = 0;
            pPbbTag->InnerIsidTag.u1TagType = VLAN_UNTAGGED;
        }
        /* Copy BDA and BSA into the pbb tag info */
        CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->BDAMacAddr,
                                   0, ETHERNET_ADDR_SIZE);
        CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->BSAMacAddr,
                                   ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);

        /* Check if C/S tag is present in the frame or we have a STP-BPDU, if so
         * then forward this frame from CBP*/
        if (*pu4DataOffSet > (*pu4ITagOffset + u2ITagLen)
            || u1FrameType == L2_STAP_PKT)
        {
            *pu1IsSend = OSIX_TRUE;
            return L2IWF_SUCCESS;
        }
    }
    else
    {
        /* S-tagged/C-tagged/SC-tagged or no tagg */
        /* buffer contains the S-tag or C-tag or SC-tag information */
        if (VlanGetVlanInfoFromFrame (u4ContextId,
                                      u2LocalPort,
                                      pBuf, pVlanTag,
                                      pu1IngressAction,
                                      pu4DataOffSet) != VLAN_SUCCESS)
        {

            return L2IWF_FAILURE;
        }
        CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->InnerIsidTag.CDAMacAddr,
                                   0, ETHERNET_ADDR_SIZE);
        CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->InnerIsidTag.CSAMacAddr,
                                   ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);
        pFwdPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pFwdPortList == NULL)
        {
            return L2IWF_FAILURE;
        }
        MEMSET (pFwdPortList, 0, sizeof (tLocalPortList));

        /* Get the corresponding ISID and BDA for the incomming S-VLAN/C-VLAN */
        if (VlanGetFwdPortList
            (u4ContextId, u2LocalPort, pPbbTag->InnerIsidTag.CSAMacAddr,
             pPbbTag->InnerIsidTag.CDAMacAddr, pVlanTag->OuterVlanTag.u2VlanId,
             pFwdPortList) != VLAN_SUCCESS)
        {
            if (L2IwfMiGetVlanLocalEgressPorts
                (u4ContextId, pVlanTag->OuterVlanTag.u2VlanId,
                 pFwdPortList) == L2IWF_FAILURE)
            {
                UtilPlstReleaseLocalPortList (pFwdPortList);
                return L2IWF_FAILURE;
            }
        }
        if (PbbGetVipIsidWithPortList (u4ContextId, u2LocalPort,
                                       pFwdPortList, &u2VipPort, &u2PipPort,
                                       &(pPbbTag->InnerIsidTag.u4Isid),
                                       &(pPbbTag->BDAMacAddr)) != PBB_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pFwdPortList);
            return L2IWF_FAILURE;
        }

        /* no next/inner tag to decode so update pu4ITagOffset to the offset
         * of protocol data */
        *pu4ITagOffset = *pu4DataOffSet;
        UtilPlstReleaseLocalPortList (pFwdPortList);
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfPbbTxFrameToIComp                           */
/*                                                                           */
/*    Description         : This function is called when an B-Tagged frame   */
/*                          arrives on PNP in an IB Bridge, and the frame    */
/*                          consists S-Tag also. Then the frame need to be   */
/*                          sent to I Component bridge                       */
/*                                                                           */
/*    Input(s)            : pBuf - Incoming frame                            */
/*                          u4IfIndex - Port received the frame              */
/*                          u2LocalPort - Local Port                         */
/*                          pVlanTag - Vlan Tag Information                  */
/*                          pPbbTag - Pbb Tag Information                    */
/*                          pu4ITagOffset - offset of the protocol PDU in buf  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns            : L2IWF_SUCCESS/                                    */
/*                         L2IWF_FAILURE                                     */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfPbbTxFrameToIComp (UINT4 u4ContextId, UINT4 u4IfIndex,
                        tCRU_BUF_CHAIN_DESC * pBuf, tVlanTag * pVlanTag,
                        tPbbTag * pPbbTag, UINT4 *pu4ITagOffset,
                        UINT1 u1FrameType, UINT2 u2LocalPort)
{
    if (PbbGetIsidInfoFromFrame (u4ContextId,
                                 u2LocalPort,
                                 pBuf, pPbbTag, *pu4ITagOffset) != SUCCESS)
    {
        return L2IWF_FAILURE;
    }
    CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->InnerIsidTag.CDAMacAddr,
                               (*pu4ITagOffset + PBB_ISID_TAG_PID_LEN),
                               ETHERNET_ADDR_SIZE);
    CRU_BUF_Copy_FromBufChain (pBuf, pPbbTag->InnerIsidTag.CSAMacAddr,
                               (*pu4ITagOffset + PBB_ISID_TAG_PID_LEN +
                                ETHERNET_ADDR_SIZE), ETHERNET_ADDR_SIZE);

    if ((L2IwfGetVlanPortState (pPbbTag->OuterVlanTag.u2VlanId,
                                u4IfIndex) != AST_PORT_STATE_FORWARDING))
    {
        return L2IWF_FAILURE;
    }

    if (L2IwfTransmitFrameFromCbp
        (u4ContextId, u2LocalPort, pBuf, pVlanTag, pPbbTag,
         u1FrameType) != L2IWF_SUCCESS)
    {
        if ((u1FrameType == L2_STAP_PKT) || (u1FrameType == L2_MMRP_PKT))
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }
        return L2IWF_FAILURE;
    }
    if (u1FrameType == L2_STAP_PKT)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }

    return L2IWF_SUCCESS;
}

#endif /*End _L2_PBB_C_ */
