/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2init.c,v 1.113 2016/06/02 12:51:11 siva Exp $
 *
 * Description:This file contains initialization routine      
 *             for L2IWF module.                 
 *
 *******************************************************************/

#ifndef _L2INIT_C_
#define _L2INIT_C_

#include "l2inc.h"
#include "fsbuddy.h"

#if !defined(ISS_WANTED) && !defined(WSS_WANTED)
extern tMemPoolId          gSnmpOidTypePoolId;
extern tMemPoolId          gSnmpOidListPoolId;
extern tMemPoolId          gOctetStrPoolId;
#endif /* ISS_WANTED & WSS_WANTED */


tCfaRegInfo         gCfaRegInfo;

/*****************************************************************************/
/* Function Name      : L2IwfInit                                            */
/*                                                                           */
/* Description        : This routine allocates memory and semaphores         */
/*                      for the L2 module data structures                    */
/*                      and intialises them.                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfInit (INT1 *pi1Dummy)
{
    UNUSED_PARAM (pi1Dummy);
    MEMSET (&gL2GlobalInfo, 0, sizeof (tL2GlobalInfo));
#ifdef PBB_WANTED
    L2IWF_PBB_SHUTDOWN_STATUS () = L2IWF_TRUE;
#endif

    if (L2IWF_CREATE_SEMAPHORE (L2IWF_SEMAPHORE, 1, 0,
                                &gL2GlobalInfo.SemId) != OSIX_SUCCESS)
    {
        lrInitComplete (OSIX_FAILURE);
        return L2IWF_FAILURE;
    }

    if (L2IWF_CREATE_SEMAPHORE (L2_DB_SEM, 1, 0,
                                &gL2GlobalInfo.DBSemId) != OSIX_SUCCESS)
    {
        L2IWF_DELETE_SEMAPHORE (gL2GlobalInfo.SemId);
        lrInitComplete (OSIX_FAILURE);
        return L2IWF_FAILURE;
    }

    if (L2IWF_CREATE_SEMAPHORE (L2_SYNC_SEM, 0, 0,
                                &gL2GlobalInfo.SyncSemId) != OSIX_SUCCESS)
    {
        L2IWF_DELETE_SEMAPHORE (gL2GlobalInfo.SemId);
        L2IWF_DELETE_SEMAPHORE (gL2GlobalInfo.DBSemId);
        lrInitComplete (OSIX_FAILURE);
        return L2IWF_FAILURE;
    }
    if (L2IWF_CREATE_SEMAPHORE (L2_MI_SYNC_SEM, 0, 0,
                                &gL2GlobalInfo.MiSyncSemId) != OSIX_SUCCESS)
    {
        L2IWF_DELETE_SEMAPHORE (gL2GlobalInfo.SemId);
        L2IWF_DELETE_SEMAPHORE (gL2GlobalInfo.DBSemId);
        lrInitComplete (OSIX_FAILURE);
        return L2IWF_FAILURE;
    }
    if (L2IwfHandleMemInit () == L2IWF_FAILURE)
    {
        L2IWF_DELETE_SEMAPHORE (gL2GlobalInfo.SemId);
        L2IWF_DELETE_SEMAPHORE (gL2GlobalInfo.DBSemId);
        L2IWF_DELETE_SEMAPHORE (gL2GlobalInfo.SyncSemId);
        L2IWF_DELETE_SEMAPHORE (gL2GlobalInfo.MiSyncSemId);
        lrInitComplete (OSIX_FAILURE);
        return L2IWF_FAILURE;
    }
    lrInitComplete (OSIX_SUCCESS);
    FsUtlSzCalculateModulePreAllocatedMemory (&gFsL2iwfSizingInfo);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfHandleMemInit                    */
/*                                                                           */
/* Description        : This routine allocates memory for the L2 module data */
/*                      structures and intialises them.                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfHandleMemInit (VOID)
{
    INT4                u2AggIndex;
    UINT4               u4MinBlkAdjusted;
    UINT4               u4MaxBlkAdjusted;
    INT4                i4PortStateBuddyId;
    INT4                i4InstStateBuddyId;

    MEMSET (&gFsL2iwfSizingInfo, 0, sizeof (tFsModSizingInfo));
    MEMCPY (gFsL2iwfSizingInfo.ModName, "L2IWF", STRLEN ("L2IWF"));
    gFsL2iwfSizingInfo.u4ModMemPreAllocated = 0;
    gFsL2iwfSizingInfo.ModSizingParams = gFsL2iwfSizingParams;
    if (L2IWF_CREATE_MEMPOOL
        (sizeof (tL2PortInfo),
         gFsL2iwfSizingParams[L2IWF_L2PORT_INFO_SIZING_ID].u4PreAllocatedUnits,
         MEM_HEAP_MEMORY_TYPE, &(L2IWF_PORTINFO_POOLID ())) == MEM_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    if (L2IWF_CREATE_MEMPOOL
        (sizeof (tL2VlanInfo),
         gFsL2iwfSizingParams[L2IWF_L2VLAN_INFO_SIZING_ID].u4PreAllocatedUnits,
         MEM_HEAP_MEMORY_TYPE, &(L2IWF_VLANINFO_POOLID ())) == MEM_FAILURE)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_CREATE_MEMPOOL
        (sizeof (tL2MstInstInfo),
         gFsL2iwfSizingParams[L2IWF_L2MST_INFO_SIZING_ID].u4PreAllocatedUnits,
         MEM_HEAP_MEMORY_TYPE, &(L2IWF_MSTINFO_POOLID ())) == MEM_FAILURE)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }

/* PVLAN  related mempools */
    if (L2IwfPvlanCrtMemPoolAndRBTree () == L2IWF_FAILURE)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_CREATE_MEMPOOL (sizeof (tIfTypeProtoDenyInfo),
                              gFsL2iwfSizingParams
                              [L2IWF_IFTYPE_PROT_INFO_SIZING_ID].
                              u4PreAllocatedUnits,
                              MEM_DEFAULT_MEMORY_TYPE,
                              &(L2IWF_IFTYPE_PROTO_DENY_POOLID ()))
        == MEM_FAILURE)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }

    /* LLDP Application information */
    if (L2IWF_CREATE_MEMPOOL ((sizeof (tL2LldpAppInfo) * LLDP_MAX_APPL),
                              gFsL2iwfSizingParams
                              [L2IWF_L2LLDP_APP_INFO_SIZING_ID].
                              u4PreAllocatedUnits,
                              MEM_DEFAULT_MEMORY_TYPE,
                              &(L2IWF_LLDP_APPL_POOLID ())) == MEM_FAILURE)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }

    gL2GlobalInfo.paL2LldpAppInfo =
        L2IWF_ALLOCATE_MEMBLOCK (L2IWF_LLDP_APPL_POOLID ());

    if (gL2GlobalInfo.paL2LldpAppInfo == NULL)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }

    MEMSET (gL2GlobalInfo.paL2LldpAppInfo, 0,
            (sizeof (tL2LldpAppInfo) * LLDP_MAX_APPL));

    /* L2 Port channel information */
    if (L2IWF_CREATE_MEMPOOL
        ((sizeof (tL2PortChannelInfo) * LA_DEV_MAX_TRUNK_GROUP),
         gFsL2iwfSizingParams[L2IWF_L2PORT_CH_INFO_SIZING_ID].
         u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
         &(L2IWF_PORT_CHANNEL_INFO_POOLID ())) == MEM_FAILURE)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }

    gL2GlobalInfo.paL2PortChannelInfo =
        L2IWF_ALLOCATE_MEMBLOCK (L2IWF_PORT_CHANNEL_INFO_POOLID ());

    if (gL2GlobalInfo.paL2PortChannelInfo == NULL)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }

    MEMSET (gL2GlobalInfo.paL2PortChannelInfo, 0,
            (sizeof (tL2PortChannelInfo) * LA_DEV_MAX_TRUNK_GROUP));

    for (u2AggIndex = BRG_MAX_PHY_PORTS + 1;
         u2AggIndex <= BRG_MAX_PHY_PORTS + LA_DEV_MAX_TRUNK_GROUP; u2AggIndex++)
    {
        L2IWF_PORT_CHANNEL_VALID (u2AggIndex) = OSIX_FALSE;
    }

    /* Call L2Iwf Routine to calculate Adjustment values for minimum 
       and Max Block size before creating Buddy Memory Pool   */
    UtilCalculateBuddyMemPoolAdjust (AST_MAX_MST_INSTANCES,
                                     AST_MAX_PVRST_INSTANCES,
                                     &u4MinBlkAdjusted, &u4MaxBlkAdjusted);
    if ((i4PortStateBuddyId =
         MemBuddyCreate (u4MaxBlkAdjusted,
                         u4MinBlkAdjusted, L2IWF_MAX_PORTS_PER_CONTEXT_EXT,
                         BUDDY_HEAP_EXTN)) == BUDDY_FAILURE)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;

    }
    else
    {
        if ((i4PortStateBuddyId) >= 0
            && (i4PortStateBuddyId <= L2IWF_UINT1_ALL_ONE))
        {
            gL2GlobalInfo.u1PortStateBuddyId = (UINT1) i4PortStateBuddyId;
        }
        else
        {
            L2IwfDeInit ();
            return L2IWF_FAILURE;
        }
    }

    if ((i4InstStateBuddyId =
         MemBuddyCreate (u4MaxBlkAdjusted,
                         u4MinBlkAdjusted, 1, BUDDY_HEAP_EXTN))
        == BUDDY_FAILURE)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }
    else
    {
        if ((i4InstStateBuddyId) >= 0
            && (i4InstStateBuddyId <= L2IWF_UINT1_ALL_ONE))
        {
            gL2GlobalInfo.u1InstStateBuddyId = (UINT1) i4InstStateBuddyId;
        }
        else
        {
            L2IwfDeInit ();
            return L2IWF_FAILURE;
        }
    }

    if (L2IwfInitContextInfo () == L2IWF_FAILURE)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }

    /* Switch to default context. 
     * Then get the bridge mode from iss NvRam for the default 
     * context.
     * Validate the bridge mode.*/
    if (L2IwfSelectContext (L2IWF_DEFAULT_CONTEXT) == L2IWF_FAILURE)
    {
        L2IwfDeInit ();
        return L2IWF_FAILURE;
    }
#ifdef ISS_WANTED
    L2IWF_BRIDGE_MODE () = IssGetBridgeModeFromNvRam ();
#else
    L2IWF_BRIDGE_MODE () = L2IWF_CUSTOMER_BRIDGE_MODE;
#endif

    if (L2IwfValidateBridgeMode (L2IWF_BRIDGE_MODE ()) != L2IWF_SUCCESS)
    {
        L2IwfDeInit ();
        L2IwfReleaseContext ();
        return L2IWF_FAILURE;
    }

    /* All Mempools should be created irrespective of Bridge Mode. This is 
     * because there can be context with Customer Bridge Mode and
     * another context with provider Bridge mode.*/
    if (L2IwfPbInitMemPools () == L2IWF_FAILURE)
    {
        L2IwfPbDeInitMemPools ();
        L2IwfDeInit ();
        L2IwfReleaseContext ();
        return L2IWF_FAILURE;
    }
    /* Used for creating the mempool for Mac-based authentication
     * sessions.Irrespective of whether the port is mac-based or 
     * port-based the mempool exists*/
    if (L2IwfPnacSessCrtMemPoolAndRBTree () == L2IWF_FAILURE)
    {
        L2IwfPbDeInitMemPools ();
        L2IwfDeInit ();
        L2IwfReleaseContext ();
        return L2IWF_FAILURE;
    }

    if (L2IwfPortVlanCrtMemPoolAndRBTree () == L2IWF_FAILURE)
    {
        L2IwfPbDeInitMemPools ();
        L2IwfDeInit ();
        L2IwfReleaseContext ();
        return L2IWF_FAILURE;
    }

#if !defined(ISS_WANTED) && !defined(WSS_WANTED)

    if (MemCreateMemPool (sizeof (tSNMP_OID_TYPE), MAX_SNMP_OID_TYPE_BLOCKS,
                          MEM_DEFAULT_MEMORY_TYPE, &(gSnmpOidTypePoolId))
        == (UINT4) MEM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (MemCreateMemPool (sizeof (tSnmpOidListBlock), MAX_SNMP_OID_LIST_BLOCKS,
                          MEM_DEFAULT_MEMORY_TYPE, &(gSnmpOidListPoolId))
        == (UINT4) MEM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (MemCreateMemPool
	    (sizeof (tSNMP_OCTET_STRING_TYPE), MAX_SNMP_OCTET_STR_BLOCKS,
	     MEM_DEFAULT_MEMORY_TYPE,
	     &(gOctetStrPoolId)) == (UINT4) MEM_FAILURE)
    {
	return L2IWF_FAILURE;
    }

    L2IwfSetDefaultVlanId (IssGetDefaultVlanIdFromNvRam ());
#endif /* ISS_WANTED & WSS_WANTED*/

    L2IwfReleaseContext ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMemInit                          */
/*                                                                           */
/* Description        : This routine allocates memory for the L2 module data */
/*                      structures and intialises them.                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfMemInit (VOID)
{
    L2_LOCK ();
    if (L2IwfHandleMemInit () == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMemDeInit                                  */
/*                                                                           */
/* Description        : This is an API to perform the complete shutdown of   */
/*                      L2IWF module.                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfMemDeInit (VOID)
{
    UINT4               u4ContextId = 0;

    L2_LOCK ();
    for (u4ContextId = 0; u4ContextId < L2IWF_MAX_CONTEXTS; u4ContextId++)
    {
        if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
        {
            continue;
        }
        L2IwfPbDeInitMemPools ();
        L2IwfDeleteContext (u4ContextId);
    }
    /* Following two mempools will be deleted when 
     * no context exist during L2IwfMemDeInit */
    /* Delete Vid tranlation entry memory pool. */
#ifdef PB_WANTED
    if (L2IWF_VID_TRANS_ENTRY_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_VID_TRANS_ENTRY_POOLID ());
        L2IWF_VID_TRANS_ENTRY_POOLID () = 0;
    }
    /* Delete C-VLAN port entry memory pool. */
    if (L2IWF_PEPINFO_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_PEPINFO_POOLID ());
        L2IWF_PEPINFO_POOLID () = 0;
    }
#endif

#if !defined(ISS_WANTED) && !defined(WSS_WANTED)
    if (gSnmpOidTypePoolId != 0)
    {
       MemDeleteMemPool (gSnmpOidTypePoolId);
       gSnmpOidTypePoolId = 0;
    }
    if (gSnmpOidListPoolId != 0)
    {
       MemDeleteMemPool (gSnmpOidListPoolId);
       gSnmpOidListPoolId = 0;
    }
    if ( gOctetStrPoolId != 0)
    { 
       MemDeleteMemPool(gOctetStrPoolId);
       gOctetStrPoolId = 0;
    }
     
#endif /* ISS_WANTED & WSS_WANTED*/

    L2IwfDeInit ();
    L2_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeInit                                          */
/*                                                                           */
/* Description        : This routine de-initialises the L2 module data       */
/*                      structures and frees the allocated memory.           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
VOID
L2IwfDeInit (VOID)
{
    if (L2IWF_GLOBAL_IFINDEX_TREE () != NULL)
    {
        RBTreeDestroy (L2IWF_GLOBAL_IFINDEX_TREE (),
                       (tRBKeyFreeFn) L2IwfFreePortEntry, 0);

        L2IWF_GLOBAL_IFINDEX_TREE () = NULL;
    }

    if (L2IWF_PORT_CHANNEL_INFO_POOLID () != 0)
    {
        if (gL2GlobalInfo.paL2PortChannelInfo != NULL)
        {
            L2IWF_RELEASE_MEMBLOCK (L2IWF_PORT_CHANNEL_INFO_POOLID (),
                                    gL2GlobalInfo.paL2PortChannelInfo);
        }
        L2IWF_DELETE_MEMPOOL (L2IWF_PORT_CHANNEL_INFO_POOLID ());
        L2IWF_PORT_CHANNEL_INFO_POOLID () = 0;
        gL2GlobalInfo.paL2PortChannelInfo = NULL;
    }

    if (gL2GlobalInfo.u1PortStateBuddyId != (UINT1) BUDDY_FAILURE)
    {
        MemBuddyDestroy (gL2GlobalInfo.u1PortStateBuddyId);
        gL2GlobalInfo.u1PortStateBuddyId = (UINT1) BUDDY_FAILURE;
    }

    if (gL2GlobalInfo.u1InstStateBuddyId != (UINT1) BUDDY_FAILURE)
    {
        MemBuddyDestroy (gL2GlobalInfo.u1PortStateBuddyId);
        gL2GlobalInfo.u1PortStateBuddyId = (UINT1) BUDDY_FAILURE;
    }

    if (L2IWF_LLDP_APPL_POOLID () != 0)
    {
        if (gL2GlobalInfo.paL2LldpAppInfo != NULL)
        {
            L2IWF_RELEASE_MEMBLOCK (L2IWF_LLDP_APPL_POOLID (),
                                    gL2GlobalInfo.paL2LldpAppInfo);
        }
        L2IWF_DELETE_MEMPOOL (L2IWF_LLDP_APPL_POOLID ());
        L2IWF_LLDP_APPL_POOLID () = 0;
        gL2GlobalInfo.paL2LldpAppInfo = NULL;
    }

    if (L2IWF_PORTINFO_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_PORTINFO_POOLID ());
        L2IWF_PORTINFO_POOLID () = 0;
    }
    /* PVLAN related mempools */
    L2IwfPvlanDelMemPoolAndRBTree ();

    if (L2IWF_VLANINFO_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_VLANINFO_POOLID ());
        L2IWF_VLANINFO_POOLID () = 0;
    }

    if (L2IWF_MSTINFO_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_MSTINFO_POOLID ());
        L2IWF_MSTINFO_POOLID () = 0;
    }

    L2IwfPnacSessDelMemPoolAndRBTree ();
    L2IwfPortVlanDelMemPoolAndRBTree ();

    if (L2IWF_VLAN_FID_MST_INST_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_VLAN_FID_MST_INST_POOLID ());
        L2IWF_VLAN_FID_MST_INST_POOLID () = 0;
    }

    if (L2IWF_CONTEXTINFO_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_CONTEXTINFO_POOLID ());
        L2IWF_CONTEXTINFO_POOLID () = 0;
    }

    if (L2IWF_IFTYPE_PROTO_DENY_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_IFTYPE_PROTO_DENY_POOLID ());
        L2IWF_IFTYPE_PROTO_DENY_POOLID () = 0;
    }
}

/*****************************************************************************/
/* Function Name      : L2IwfInitContextInfo                                 */
/*                                                                           */
/* Description        : This routine allocates memory for the L2IWF module   */
/*                      context-specific data structures and intialises them */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfInitContextInfo (VOID)
{
    if (L2IWF_CREATE_MEMPOOL
        (sizeof (tL2ContextInfo),
         gFsL2iwfSizingParams[L2IWF_L2CONTEXT_INFO_SIZING_ID].
         u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
         &(L2IWF_CONTEXTINFO_POOLID ())) == MEM_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    if (L2IWF_CREATE_MEMPOOL
        ((sizeof (tL2VlanFidMstInstInfo) * VLAN_DEV_MAX_VLAN_ID),
         gFsL2iwfSizingParams[L2IWF_L2VLN_FID_MST_INST_SIZING_ID].
         u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
         &(L2IWF_VLAN_FID_MST_INST_POOLID ())) == MEM_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    L2IWF_GLOBAL_IFINDEX_TREE () =
        RBTreeCreateEmbedded (0, (tRBCompareFn) L2IwfIfIndexTblCmpFn);

    if (gL2GlobalInfo.GlobalIfIndexTable == NULL)
    {
        return L2IWF_FAILURE;
    }

    if (L2IwfCreateContext (L2IWF_DEFAULT_CONTEXT) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetNextValidPort                                */
/*                                                                           */
/* Description        : This routine returns the next active port in the     */
/*                      system. This is used by the LA and PNAC modules to   */
/*                      know the active ports in the system when they are    */
/*                      started from the shutdown state.                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index whose next port is to be    */
/*                                    determined                             */
/*                                                                           */
/* Output(s)          : u2NextPort - The next active port                    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfGetNextValidPort (UINT4 u4IfIndex, UINT2 *pu2NextPort)
{
    INT4                i4Retval = L2IWF_FAILURE;
    tL2PortInfo        *pL2PortInfo = NULL;

    *pu2NextPort = 0;

    L2_LOCK ();

    if (u4IfIndex == 0)
    {
        pL2PortInfo = L2IwfGetFirstIfIndexEntry ();
    }
    else
    {
        pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

        if (pL2PortInfo != NULL)
        {
            pL2PortInfo = L2IwfGetNextIfIndexEntry (pL2PortInfo);
        }
    }

    if (pL2PortInfo != NULL)
    {
        *pu2NextPort = (UINT2) pL2PortInfo->u4IfIndex;
        i4Retval = L2IWF_SUCCESS;
    }

    L2_UNLOCK ();

    return i4Retval;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetNextValidPortForContext                      */
/*                                                                           */
/* Description        : This routine returns the next active port in the     */
/*                      context as ordered by the HL Port ID. This is used   */
/*                      by the L2 modules to know the active ports in the    */
/*                      system when they are started from the shutdown state */
/*                                                                           */
/* Input(s)           : u2LocalPortId - HL Port Index whose next port is to  */
/*                                      be determined                        */
/*                                                                           */
/* Output(s)          : u2NextLocalPort - The next active HL Port ID         */
/*                      u2NextIfIndex - IfIndex of the next HL Port          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfGetNextValidPortForContext (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                 UINT2 *pu2NextLocalPort, UINT4 *pu4NextIfIndex)
{
    INT4                i4Index;
    INT4                i4Retval = L2IWF_FAILURE;

    *pu2NextLocalPort = 0;
    *pu4NextIfIndex = 0;

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    for (i4Index = u2LocalPortId + 1;
         i4Index <= L2IWF_MAX_PORTS_PER_CONTEXT_EXT; i4Index++)
    {
        if (L2IWF_PORT_ACTIVE (i4Index) == OSIX_TRUE)
        {
            *pu2NextLocalPort = (UINT2) i4Index;
            *pu4NextIfIndex = L2IWF_PORT_IFINDEX (i4Index);

            i4Retval = L2IWF_SUCCESS;
            break;
        }
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    return i4Retval;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetNextValidPortInCxtForProt                    */
/*                                                                           */
/* Description        : This routine returns the next active port in the     */
/*                      context for the protocol as ordered by the HL Port ID*/
/*                      The IfType based Denial of creation of ports table   */
/*                      for a protocol is looked up before returning the next*/
/*                      active port (The next port is looked up if the port  */
/*                      is denied for the protocol). This is used by the L2  */
/*                      modules to know the active ports in the              */
/*                      system when they are started from the shutdown state */
/*                                                                           */
/* Input(s)           : u2LocalPortId - HL Port Index whose next port is to  */
/*                                      be determined                        */
/*                                                                           */
/* Output(s)          : u2NextLocalPort - The next active HL Port ID         */
/*                      u2NextIfIndex - IfIndex of the next HL Port          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfGetNextValidPortForProtoCxt (tL2NextPortInfo * pL2NextPortInfo)
{
    tIfTypeProtoDenyInfo IfTypeProtoDenyInfo;
    tCfaIfInfo          CfaIfInfo;
    tIfTypeProtoDenyInfo *pIfTypeProtoDenyInfo = NULL;
    INT4                i4Index = 0;
    INT4                i4Retval = L2IWF_FAILURE;

    *pL2NextPortInfo->pu2NextLocalPort = 0;
    *pL2NextPortInfo->pu4NextIfIndex = 0;

    L2_LOCK ();

    if (L2IwfSelectContext (pL2NextPortInfo->u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    for (i4Index = (pL2NextPortInfo->u2LocalPortId + 1);
         i4Index <= L2IWF_MAX_PORTS_PER_CONTEXT_EXT; i4Index++)
    {
        if (L2IWF_PORT_ACTIVE (i4Index) == OSIX_TRUE)
        {
            MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
            CfaGetIfInfo (L2IWF_PORT_IFINDEX (i4Index), &CfaIfInfo);

            MEMSET (&IfTypeProtoDenyInfo, 0, sizeof (tIfTypeProtoDenyInfo));

            IfTypeProtoDenyInfo.u1IfType = CfaIfInfo.u1IfType;
            IfTypeProtoDenyInfo.u1BrgPortType = CfaIfInfo.u1BrgPortType;
            IfTypeProtoDenyInfo.u1Protocol = (UINT1)
                pL2NextPortInfo->u4ModuleId;

            pIfTypeProtoDenyInfo =
                RBTreeGet (L2IWF_IFTYPE_PROTO_DENY_TREE (),
                           (tRBElem *) (&IfTypeProtoDenyInfo));

            if (pIfTypeProtoDenyInfo == NULL)
            {
                *pL2NextPortInfo->pu2NextLocalPort = (UINT2) i4Index;
                *pL2NextPortInfo->pu4NextIfIndex = L2IWF_PORT_IFINDEX (i4Index);

                i4Retval = L2IWF_SUCCESS;
                break;
            }
        }
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    return i4Retval;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortOperStatus                               */
/*                                                                           */
/* Description        : This routine determines the operational status of a  */
/*                      port indicated to a given module by it's lower layer */
/*                      modules (determines the lower layer port oper status)*/
/*                                                                           */
/* Input(s)           : i4ModuleId - Module for which the lower layer        */
/*                                   oper status is to be determined         */
/*                      u4IfIndex - Global IfIndex of the port whose lower   */
/*                                  layer oper status is to be determined    */
/*                                                                           */
/* Output(s)          : u1OperStatus - Lower layer port oper status for a    */
/*                                     given module                          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfGetPortOperStatus (INT4 i4ModuleId, UINT4 u4IfIndex, UINT1 *pu1OperStatus)
{
    UINT1               u1IfType;
    tCfaIfInfo          IfInfo;
    tL2PortInfo        *pL2PortInfo = NULL;
#ifdef PNAC_WANTED
    UINT2               u2LocalPort;
#endif

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    *pu1OperStatus = CFA_IF_DOWN;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_SUCCESS)
    {
        *pu1OperStatus = IfInfo.u1IfOperStatus;
    }
    else
    {
        *pu1OperStatus = CFA_IF_DOWN;
    }

    if (*pu1OperStatus == CFA_IF_DOWN)
    {
        return L2IWF_SUCCESS;
    }

    switch (i4ModuleId)
    {
        case PNAC_MODULE:
            /* Nothing to do. Return CFA oper status */
            break;

        case LLDP_MODULE:
        case LA_MODULE:
        case BRIDGE_MODULE:

            u1IfType = IfInfo.u1IfType;

            if ((u1IfType == CFA_ENET)
                && (IfInfo.u1BridgedIface == CFA_ENABLED))
            {
                /* For Enet ports, Oper Status in BRIDGE and LA is dependent on
                 * oper status in PNAC */
                L2_LOCK ();

                pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

                if (pL2PortInfo == NULL)
                {
                    *pu1OperStatus = CFA_IF_DOWN;
                    L2_UNLOCK ();
                    break;
                }

                if (L2IwfSelectContext (pL2PortInfo->u4ContextId) ==
                    L2IWF_FAILURE)
                {
                    *pu1OperStatus = CFA_IF_DOWN;
                    L2_UNLOCK ();
                    break;
                }

#ifdef PNAC_WANTED

                u2LocalPort = L2IWF_IFENTRY_LOCAL_PORT (pL2PortInfo);

                if (u2LocalPort > L2IWF_MAX_PORTS_PER_CONTEXT_EXT
                    || u2LocalPort <= 0)
                {
                    L2IwfReleaseContext ();
                    L2_UNLOCK ();
                    return L2IWF_FAILURE;
                }

                if (L2IWF_PORT_ACTIVE (u2LocalPort) == OSIX_TRUE)
                {
                    L2IWF_GET_PNAC_HL_PORT_OPER_STATUS (u2LocalPort,
                                                        *pu1OperStatus);
                }
#endif

                L2IwfReleaseContext ();

                L2_UNLOCK ();
            }
            /* For LAGG ports, Oper Status in BRIDGE is dependent on
             * oper status in CFA. Hence nothing to do */
            break;

        case VLAN_MODULE:
        case GARP_MODULE:
        case MRP_MODULE:
        case STP_MODULE:
        case SNOOP_MODULE:
        case ERPS_MODULE:

            L2_LOCK ();

            pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

            if (pL2PortInfo == NULL)
            {
                *pu1OperStatus = CFA_IF_DOWN;
                L2_UNLOCK ();
                break;
            }

            *pu1OperStatus = L2IWF_IFENTRY_BRIDGE_OPER_STATUS (pL2PortInfo);

            L2_UNLOCK ();

            break;
        default:
            break;

    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPnacSessCrtMemPoolAndRBTree                     */
/*                                                                           */
/* Description        : This routine creates the mempool required for        */
/*                      Mac based authentication and creates RBTree in L2Iwf.*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPnacSessCrtMemPoolAndRBTree (VOID)
{
    /*NOTE: This function can be called at many times.But the
     * NULL checks ensures that mempool will be created only once*/

    if (L2IWF_PNAC_MAC_SESS_POOLID () == 0)
    {
        /* Create Mem pool for Pnac Mac session table entries. */
        if (L2IWF_CREATE_MEMPOOL
            (sizeof (tL2PnacMacInfo),
             gFsL2iwfSizingParams[L2IWF_L2PNAC_MAC_INFO_SIZING_ID].
             u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
             &(L2IWF_PNAC_MAC_SESS_POOLID ())) == MEM_FAILURE)
        {
            return L2IWF_FAILURE;
        }
    }

    if (L2IWF_PNAC_MAC_TBL () == NULL)
    {
        L2IWF_PNAC_MAC_TBL () = RBTreeCreateEmbedded (0, (tRBCompareFn)
                                                      L2IwfMacCmpEntry);

        if (L2IWF_PNAC_MAC_TBL () == NULL)
        {
            L2IwfPnacSessDelMemPoolAndRBTree ();
            return L2IWF_FAILURE;
        }
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPnacSessDelMemPoolAndRBTree                     */
/*                                                                           */
/* Description        : This routine deletes the mempool and RBTree created  */
/*                      for Mac-Based Authenitcation functionality in L2Iwf. */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPnacSessDelMemPoolAndRBTree (VOID)
{
    if (L2IWF_PNAC_MAC_TBL () != NULL)
    {
        RBTreeDelete (L2IWF_PNAC_MAC_TBL ());
        L2IWF_PNAC_MAC_TBL () = NULL;
    }

    /* Delete Pnac Mac Session entry memory pool. */
    if (L2IWF_PNAC_MAC_SESS_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_PNAC_MAC_SESS_POOLID ());
        L2IWF_PNAC_MAC_SESS_POOLID () = 0;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfMacCmpEntry                                 */
/*                                                                           */
/*    Description         : This function is used to compare two supplicant  */
/*                          mac addresses                                    */
/*                                                                           */
/*    Input(s)            : pNodeA,pNodeB - Logical port table entries       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pNodeA > pNodeB                              */
/*                          -1 - pNodeA < pNodeB                             */
/*                          0 - pNodeA == PNodeB                             */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfMacCmpEntry (tRBElem * pNodeA, tRBElem * pNodeB)
{
    tL2PnacMacInfo     *pL2PnacMacEntryA = (tL2PnacMacInfo *) pNodeA;
    tL2PnacMacInfo     *pL2PnacMacEntryB = (tL2PnacMacInfo *) pNodeB;

    return (MEMCMP (pL2PnacMacEntryA->SrcMacAddr,
                    pL2PnacMacEntryB->SrcMacAddr, sizeof (tMacAddr)));
}

/*****************************************************************************/
/* Function Name      : L2IwfContextCreateIndication                         */
/*                                                                           */
/* Description        : This routine is used by VCM to indicate to the L2    */
/*                      applications that a new context has been created     */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID created              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfContextCreateIndication (UINT4 u4ContextId)
{
    L2_LOCK ();
    if (L2IwfCreateContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    L2_UNLOCK ();

#ifdef RSTP_WANTED
    AstCreateContext (u4ContextId);
#endif
#ifdef VLAN_WANTED
    VlanCreateContext (u4ContextId);
#ifdef GARP_WANTED
    GarpCreateContext (u4ContextId);
#endif
#endif
#ifdef MRP_WANTED
    MrpApiCreateContext (u4ContextId);
#endif

#ifdef PBB_WANTED
    PbbCreateContext (u4ContextId);
#endif

#ifdef ECFM_WANTED
    EcfmCreateContext (u4ContextId);
#endif

#ifdef ELPS_WANTED
    ElpsApiCreateContext (u4ContextId);
#endif

#ifdef Y1564_WANTED
    Y1564ApiCreateContext (u4ContextId);
#endif

#ifdef RFC2544_WANTED
    R2544ApiCreateContext (u4ContextId);
#endif


#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    SnoopCreateContext (u4ContextId);
#endif

#ifdef CN_WANTED

    CnApiContextRequest (u4ContextId, CN_CREATE_CXT_MSG);

#endif /*CN_WANTED */
#ifdef RBRG_WANTED
    RbrgApiContextRequest (u4ContextId, RBRG_CREATE_CONTEXT);
#endif

#ifdef MEF_WANTED
    MefApiHandleContextIndication (u4ContextId, MEF_CREATE_CONTEXT);
#endif

#ifdef CFA_WANTED
    L2dsApiCreateContext (u4ContextId);
#endif /* CFA_WANTED */

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfContextDeleteIndication                         */
/*                                                                           */
/* Description        : This routine is used by VCM to indicate to the L2    */
/*                      applications that a context has been deleted         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID deleted              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfContextDeleteIndication (UINT4 u4ContextId)
{
#ifdef ELPS_WANTED
    ElpsApiDeleteContext (u4ContextId);
#endif

#ifdef RFC2544_WANTED
    R2544ApiDeleteContext (u4ContextId);
#endif
#ifdef PBBTE_WANTED
    PbbTeDeleteContext (u4ContextId);
#endif
#ifdef RSTP_WANTED
    AstDeleteContext (u4ContextId);
#endif
#ifdef VLAN_WANTED
    VlanDeleteContext (u4ContextId);
#ifdef GARP_WANTED
    GarpDeleteContext (u4ContextId);
#endif
#endif
#ifdef MRP_WANTED
    MrpApiDeleteContext (u4ContextId);
#endif

#ifdef PBB_WANTED
    PbbDeleteContext (u4ContextId);
#endif

#ifdef ECFM_WANTED
    EcfmDeleteContext (u4ContextId);
#endif
#ifdef ERPS_WANTED
    ErpsApiDeleteContext (u4ContextId);
#endif

#ifdef FSB_WANTED
    FsbApiDeleteContext (u4ContextId);    
#endif

#ifdef Y1564_WANTED
    Y1564ApiDeleteContext (u4ContextId);
#endif

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    SnoopDeleteContext (u4ContextId);
#endif

#ifdef PTP_WANTED
    PtpApiContextStatusChgHdlr (u4ContextId, PTP_CTXT_DELETE_MSG);
#endif

    L2_LOCK ();
    L2IwfDeleteContext (u4ContextId);
    L2_UNLOCK ();

#ifdef CN_WANTED

    CnApiContextRequest (u4ContextId, CN_DELETE_CXT_MSG);

#endif /*CN_WANTED */

#ifdef RBRG_WANTED
    RbrgApiContextRequest (u4ContextId, RBRG_DELETE_CONTEXT);
#endif

#ifdef MEF_WANTED
    MefApiHandleContextIndication (u4ContextId, MEF_DELETE_CONTEXT);
#endif

#ifdef CFA_WANTED
    L2dsApiDeleteContext (u4ContextId);
#endif /*CFA_WANTED */

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPortMapIndication                               */
/*                                                                           */
/* Description        : This routine is used by VCM to indicate to the L2    */
/*                      applications that a new port has been mapped to a    */
/*                      virtual context.                                     */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u4IfIndex  - Global Interface index of the port      */
/*                                   mapped                                  */
/*                      u2LocalPortId - Per context port number              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfPortMapIndication (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    tIfTypeDenyProtocolList DenyProtocolList;
    tCfaIfInfo          IfInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;
#ifdef LA_WANTED
    UINT1               u1OperStatus;
#endif

#ifdef POE_WANTED
    INT4                i4PoeRetVal = L2IWF_SUCCESS;
#endif

    /* Set the default bridge port type for the port, based
     * on the bridge mode. */
    i4RetVal = L2IwfPbSetDefBrgPortTypeInCfa (u4ContextId, u4IfIndex,
                                              L2IWF_TRUE, L2IWF_FALSE);
    if (i4RetVal == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_SUCCESS;
    }
    if (L2IwfCreatePort (u4ContextId, u4IfIndex, u2LocalPortId, &IfInfo)
        == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, &IfInfo,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    /* For Pseudowire and AC interfaces, the interface create indication should not
     * be given to higher layers. IGS and MLDS are exceptional case in this.
     * When this indication is given to the higher modules other than 
     * IGS/MLDS, Port related structures will be created. Where in IGS/MLDS
     * pre-defined the fields in the predefined array will be updated.
     */
    if ((IfInfo.u1IfType == CFA_PSEUDO_WIRE) ||
        ((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
         (IfInfo.u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)) ||
        (IfInfo.u1IfType == CFA_VXLAN_NVE) || 
        (IfInfo.u1IfType == CFA_TAP))
    {
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_SNOOP] == OSIX_FALSE)
        {
            if (SnoopMapPort (u4ContextId, u4IfIndex, u2LocalPortId) ==
                SNOOP_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
#endif /* IGS_WANTED */
        return L2IWF_SUCCESS;
    }

    /* Assumption : PNAC/ISS is applicable for Ethernet interfaces only */
    if (IfInfo.u1IfType == CFA_ENET)
    {
#ifdef PNAC_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PNAC] == OSIX_FALSE)
        {
            if (PnacMapPort ((UINT2) u4IfIndex) == PNAC_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
#endif
    }

#ifdef LA_WANTED
    /* Sisp Ports should not be created in LA */
    if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_LA] == OSIX_FALSE)
        {
            if (LaMapPort ((UINT2) u4IfIndex) == LA_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
    }
#endif /* LA_WANTED */

#ifdef BRIDGE_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_BRIDGE] == OSIX_FALSE)
    {
        if (BridgeCreatePort
            ((UINT2) u4IfIndex, IfInfo.u1IfType,
             IfInfo.u4IfSpeed) == BRIDGE_FAILURE)
        {
            i4RetVal = L2IWF_FAILURE;
        }
    }
#endif /* BRIDGE_WANTED */

#ifdef ECFM_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_ECFM] == OSIX_FALSE)
    {
        if (EcfmMapPort (u4ContextId, u4IfIndex, u2LocalPortId) == ECFM_FAILURE)
        {
            i4RetVal = L2IWF_FAILURE;
        }
    }
#endif /* ECFM_WANTED */

    /* VLAN and STP are PIP unaware */
    if (IfInfo.u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
    {
#ifdef RSTP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_XSTP] == OSIX_FALSE)
        {
            if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
            {
                if (AstMapPort (u4ContextId, u4IfIndex, u2LocalPortId) ==
                    RST_FAILURE)
                {
                    i4RetVal = L2IWF_FAILURE;
                }
            }
        }
#endif /* RSTP_WANTED */

#ifdef VLAN_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_VLAN] == OSIX_FALSE)
        {
		if ((L2IwfIsPortInPortChannel (u4IfIndex) != CFA_SUCCESS) && ( IfInfo.u1BridgedIface == CFA_ENABLED))
		{

			if (VlanMapPort (u4ContextId, u4IfIndex, u2LocalPortId) ==
					VLAN_FAILURE)
			{
				i4RetVal = L2IWF_FAILURE;
			}
		}
	}
#endif /* VLAN_WANTED */

#ifdef GARP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_GARP] == OSIX_FALSE)
        {
            if (GarpMapPort (u4ContextId, u4IfIndex, u2LocalPortId) ==
                GARP_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
#endif /* GARP_WANTED */

#ifdef MRP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_MRP] == OSIX_FALSE)
        {
            if (MrpApiPortMapIndication (u4ContextId, u4IfIndex, u2LocalPortId)
                == OSIX_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
#endif
    }

#ifdef PBB_WANTED
    /* Sisp Ports should not be created in PBB */
    if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))

    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PBB] == OSIX_FALSE)
        {
            if (PbbMapPortIndication
                (u4ContextId, (INT4) u4IfIndex, u2LocalPortId) == PBB_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
    }
#endif /* PBB WANTED */

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_SNOOP] == OSIX_FALSE)
    {
        if (SnoopMapPort (u4ContextId, u4IfIndex, u2LocalPortId) ==
            SNOOP_FAILURE)
        {
            i4RetVal = L2IWF_FAILURE;
        }
    }
#endif /* IGS_WANTED */
#ifdef QOSX_WANTED
    /* Sisp Ports should not be created in QOS */
    if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))

    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_QOS] == OSIX_FALSE)
        {
            if (QosCreatePort (u4IfIndex) == QOS_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
    }
#endif /* QOSX_WANTED */
    if (IfInfo.u1IfType == CFA_ENET)
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_LLDP] == OSIX_FALSE)
        {
            if (LldpApiNotifyIfMap ((UINT2) u4IfIndex) == OSIX_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
    }
#ifdef CN_WANTED
    if ((IfInfo.u1IfType == CFA_LAGG) || (IfInfo.u1IfType == CFA_ENET))
    {
        CnApiPortRequest (u4ContextId, u4IfIndex, CN_MAP_IF_MSG);
    }
#endif /* CN_WANTED */

#ifdef DCBX_WANTED
    DcbxApiPortRequest (u4IfIndex, DCBX_MAP_IF_MSG);
#endif /* DCBX_WANTED */

#ifdef RBRG_WANTED
    RbrgApiPortRequest (u4ContextId, u4IfIndex, RBRG_MAP_PORT);
#endif /* RBRG_WANTED */
#ifdef LA_WANTED
    if ((IfInfo.u1IfType == CFA_LAGG) && (IfInfo.u1IfAdminStatus == CFA_IF_UP))
    {
        /* Assumption: Port-Channel cannot be operationally UP here since
           it has just now been mapped to a context. Hence not 
           handling the oper status returned from LA */
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_LA] == OSIX_FALSE)
        {
            LaUpdatePortChannelAdminStatus ((UINT2) u4IfIndex, CFA_IF_UP,
                                            &u1OperStatus);
        }
    }
#endif
#ifdef ELMI_WANTED
    /* Sisp Ports should not be created in ELMI */
    if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
    {
        /* ELMI is shutdown by default hence will return failure */
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_ELMI] == OSIX_FALSE)
        {
            ElmMapPort ((UINT2) u4IfIndex);
        }
    }
#endif

#ifdef CFA_WANTED
    if (IfInfo.u1IfType == CFA_ENET)
    {
        IpdbCreatePort (u4IfIndex);
    }
#endif

    if (IfInfo.u1IfOperStatus == CFA_IF_UP)
    {
        L2IwfPortOperStatusIndication ((UINT2) u4IfIndex,
                                       IfInfo.u1IfOperStatus);
    }
/* When a new port is mapped to the virtual context, EoamApiNotifyIfCreate
 * and IssCreate Port, is invoked to post message to notify interface creation */

#ifdef EOAM_WANTED
    EoamApiNotifyIfCreate (u4IfIndex);
#endif

#ifdef ISS_WANTED
    IssCreatePort ((UINT2) u4IfIndex, ISS_ALL_TABLES);
#endif

#ifdef POE_WANTED
    PoeGetModuleStatus (&i4PoeRetVal);
    if (i4PoeRetVal == POE_TRUE)
    {
        PoeCreatePort (u4IfIndex);
    }
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfPortUnmapIndication                             */
/*                                                                           */
/* Description        : This routine is used by VCM to indicate to the L2    */
/*                      applications that a port has been unmapped from a    */
/*                      virtual context.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Global Interface index of the port      */
/*                                   mapped                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfPortUnmapIndication (UINT4 u4IfIndex)
{
    tIfTypeDenyProtocolList DenyProtocolList;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    tCfaIfInfo          IfInfo;
    UINT1               u1IntfTypeIndFlag = L2IWF_FALSE;

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        != VCM_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_SUCCESS;
    }
    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, &IfInfo,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

#ifdef QOSX_WANTED
    if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_QOS] == OSIX_FALSE)
        {
            QosDeletePort (u4IfIndex);
        }
    }
#endif /* QOSX_WANTED */

    /* Assumption : PNAC/ISS is applicable for Ethernet interfaces only */
    if (IfInfo.u1IfType == CFA_ENET)
    {
#ifdef PNAC_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PNAC] == OSIX_FALSE)
        {
            PnacUnmapPort ((UINT2) u4IfIndex);
        }
#endif
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_LLDP] == OSIX_FALSE)
        {
            LldpApiNotifyIfUnMap ((UINT2) u4IfIndex);
        }
    }
#ifdef LA_WANTED 
        if (IfInfo.u1IfType == CFA_LAGG) 
       { 
            if (DenyProtocolList[L2IWF_PROTOCOL_ID_LA] == OSIX_FALSE) 
            { 
                L2IwfDeleteAllPortsFromTrunk ((UINT2) u4IfIndex); 
            } 
        } 
     
        if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) && 
              (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE))) 
        { 
            if (DenyProtocolList[L2IWF_PROTOCOL_ID_LA] == OSIX_FALSE) 
            { 
                LaUnmapPort ((UINT2) u4IfIndex); 
            } 
        } 
#endif /* LA_WANTED */ 


    /* When a link aggregated Provider Instance Port is unmapped 
     * from Context, the LA (LaUnmapPort) thread gives Port Create Indication 
     * for the physical port to all the L2 Modules. It also sets the Bridge Port 
     * type of the physical port to default port type in CFA. Since the Bridge 
     * port type is accessed in the above LaUnmapPort the IfInfo has to be updated.
     */
    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_SUCCESS;
    }

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, &IfInfo,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }
#ifdef CN_WANTED
    if ((IfInfo.u1IfType == CFA_LAGG) || (IfInfo.u1IfType == CFA_ENET))
    {
        CnApiPortRequest (u4ContextId, u4IfIndex, CN_UNMAP_IF_MSG);
    }
#endif /* CN_WANTED */

#ifdef DCBX_WANTED
    DcbxApiPortRequest (u4IfIndex, DCBX_UNMAP_IF_MSG);
#endif /* DCBX_WANTED */

#ifdef FSB_WANTED
    FsbApiUnmapPort (u4IfIndex);
#endif

    if (IfInfo.u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
    {
#ifdef RSTP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_XSTP] == OSIX_FALSE)
        {
            AstUnmapPort (u4IfIndex);
        }
#endif /* RSTP_WANTED */
    }
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_SNOOP] == OSIX_FALSE)
    {
        SnoopUnmapPort (u4IfIndex);
    }
#endif /* IGS_WANTED */

#ifdef PBB_WANTED
    if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PBB] == OSIX_FALSE)
        {
            PbbUnMapPortIndication ((INT4) u4IfIndex);
        }
    }
#endif /* PBB WANTED */

    if (IfInfo.u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
    {
#ifdef MRP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_MRP] == OSIX_FALSE)
        {
            MrpApiPortUnmapIndication (u4IfIndex);
        }
#endif /* MRP_WANTED */
#ifdef VLAN_WANTED
#ifdef GARP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_GARP] == OSIX_FALSE)
        {
            GarpUnmapPort ((UINT2) u4IfIndex);
        }
#endif /* GARP_WANTED */
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_VLAN] == OSIX_FALSE)
        {
            VlanUnmapPort ((UINT2) u4IfIndex);
        }
#endif /* VLAN_WANTED */
    }
#ifdef ECFM_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_ECFM] == OSIX_FALSE)
    {
        EcfmUnMapPort (u4IfIndex);
    }
#endif /* ECFM_WANTED */

#ifdef BRIDGE_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_BRIDGE] == OSIX_FALSE)
    {
        BridgeDeletePort ((UINT2) u4IfIndex);
    }
#endif /* BRIDGE_WANTED */
#ifdef ELMI_WANTED
    /* ELMI is shutdown by default hence will return failure */
    if (!((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (IfInfo.u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_ELMI] == OSIX_FALSE)
        {
            ElmUnMapPort ((UINT2) u4IfIndex);
        }
    }
#endif

#ifdef CFA_WANTED
    if (IfInfo.u1IfType == CFA_ENET)
    {
        IpdbDeletePort (u4IfIndex);
    }
#endif

    MEMSET (&gCfaRegInfo, 0, sizeof (tCfaRegInfo));
#ifdef PTP_WANTED
    gCfaRegInfo.u4IfIndex = u4IfIndex;

    CfaGetIfInfo (u4IfIndex, &(gCfaRegInfo.CfaIntfInfo));
    PtpApiIfDeleteCallBack (&gCfaRegInfo);
#endif

#ifdef ISS_WANTED
    IssDeletePort ((UINT2) u4IfIndex, ISS_ALL_TABLES);
#endif
#ifdef SYNCE_WANTED
    gCfaRegInfo.u4IfIndex = u4IfIndex;

    CfaGetIfInfo (u4IfIndex, &(gCfaRegInfo.CfaIntfInfo));
    SynceApiIfDeleteCb (&gCfaRegInfo);
#endif

#ifdef ESAT_WANTED
    EsatApiHandleOperDownIndication (u4IfIndex);
#endif

#ifdef RBRG_WANTED
    RbrgApiPortRequest (u4ContextId, u4IfIndex, RBRG_UNMAP_PORT);
#endif /* RBRG_WANTED */
    if ((IfInfo.u1BrgPortType == L2IWF_CNP_PORTBASED_PORT) ||
        (IfInfo.u1BrgPortType == L2IWF_CNP_CTAGGED_PORT) ||
        (IfInfo.u1BrgPortType == L2IWF_CNP_STAGGED_PORT))
    {
        L2_LOCK ();

        if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
        {
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }

        /* Update CNP count */
        --L2IWF_CNP_COUNT ();
        if (L2IWF_CNP_COUNT () == 0)
        {
            L2IWF_INTERFACE_TYPE () = L2IWF_S_INTERFACE_TYPE;
            L2IWF_ADMIN_INTERFACE_TYPE_FLAG () = L2IWF_FALSE;
            u1IntfTypeIndFlag = L2IWF_TRUE;
        }
        L2_UNLOCK ();
    }
    if (u1IntfTypeIndFlag == L2IWF_TRUE)
    {
        L2IwfIntfTypeChangeIndication (u4IfIndex,
                                       (UINT1) L2IWF_S_INTERFACE_TYPE);
    }

    /* Set the bridge port type as customer bridge for the port.
     * Since we are going to delete the port, no thing can be set
     * based on bridge mode. So pass the context id as default context.*/
    L2IwfPbSetDefBrgPortTypeInCfa (L2IWF_DEFAULT_CONTEXT, u4IfIndex,
                                   L2IWF_FALSE, L2IWF_FALSE);

    return L2IwfDeletePort (u4IfIndex, IfInfo.u1IfType, L2IWF_FALSE);
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdateContextAliasName                          */
/*                                                                           */
/* Description        : This routine is used by VCM to indicate to the L2    */
/*                      applications that the alias name is modified for     */
/*                      the virtual switch.                                  */
/*                                                                           */
/* Input(s)           : u4ContextId  - Virtual Switch ID                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfUpdateContextAliasName (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
#ifdef RSTP_WANTED
    AstUpdateContextName (u4ContextId);
#endif
#ifdef VLAN_WANTED
    VlanUpdateContextName (u4ContextId);
#ifdef GARP_WANTED
    GarpUpdateContextName (u4ContextId);
#endif
#endif

#ifdef MRP_WANTED
    MrpApiUpdateContextName (u4ContextId);
#endif

#ifdef PBB_WANTED
    PbbUpdateContextName (u4ContextId);
#endif
#ifdef ERPS_WANTED
    ErpsApiUpdateCxtNameFromVcm (u4ContextId);
#endif

#ifdef Y1564_WANTED
    Y1564ApiUpdateCxtNameFromVcm (u4ContextId);
#endif

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    SnoopUpdateContextName (u4ContextId);
#endif
}

/*****************************************************************************/
/* Function Name      : L2IwfCreateContext                                   */
/*                                                                           */
/* Description        : This routine allocates memory for the L2 module data */
/*                      structures for the given context and intialises them */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfCreateContext (UINT4 u4ContextId)
{
    UINT2               u2MstInst;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (L2IWF_CONTEXT_INFO (u4ContextId) != NULL)
    {
        return L2IWF_SUCCESS;
    }

    L2IWF_CONTEXT_INFO (u4ContextId) = (tL2ContextInfo *)
        L2IWF_ALLOCATE_MEMBLOCK (L2IWF_CONTEXTINFO_POOLID ());

    if (L2IWF_CONTEXT_INFO (u4ContextId) == NULL)
    {
        return L2IWF_FAILURE;
    }

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (gpL2Context, 0, sizeof (tL2ContextInfo));

    MEMSET (gpL2Context->apL2VlanInfo, 0,
            sizeof (tL2VlanInfo *) * VLAN_DEV_MAX_VLAN_ID);

    MEMSET (gpL2Context->apL2PortInfo, 0,
            (sizeof (tL2PortInfo *) * L2IWF_MAX_PORTS_PER_CONTEXT_EXT));

    L2IWF_VID_INST_MAP_ARRAY () =
        L2IWF_ALLOCATE_MEMBLOCK (L2IWF_VLAN_FID_MST_INST_POOLID ());

    if (L2IWF_VID_INST_MAP_ARRAY () == NULL)
    {
        L2IwfReleaseContext ();
        L2IwfDeleteContext (u4ContextId);
        return L2IWF_FAILURE;
    }

    MEMSET (L2IWF_VID_INST_MAP_ARRAY (), 0,
            (sizeof (tL2VlanFidMstInstInfo) * VLAN_DEV_MAX_VLAN_ID));

    L2IWF_CVLAN_PORT_TBL () = NULL;

    L2IWF_IFTYPE_PROTO_DENY_TREE () =
        RBTreeCreateEmbedded (0, (tRBCompareFn) L2IwfIfTypeProtoDenyEntryCmp);

    if (L2IWF_IFTYPE_PROTO_DENY_TREE () == NULL)
    {
        L2IwfReleaseContext ();
        L2IwfDeleteContext (u4ContextId);
        return L2IWF_FAILURE;
    }

    L2IWF_BRIDGE_MODE () = L2IWF_DEFAULT_BRIDGE_MODE;

    L2IWF_INTERFACE_TYPE () = L2IWF_INVALID_INTERFACE_TYPE;

    L2IWF_ADMIN_INTERFACE_TYPE_FLAG () = L2IWF_FALSE;

    L2IWF_VLAN_ENH_FILTERING_STATUS () = L2IWF_FALSE;

    L2IWF_CFA_DENY_PROTOCOL_LISTCOUNT () = L2IWF_INIT_VAL;

    L2IWF_STP_PERFORMANCE_DATA_STATUS () = L2IWF_FALSE;

    L2IWF_VLAN_LEARNING_MODE () = VLAN_INDEP_LEARNING;

    L2IWF_ALL_TO_ONE_BUNDLING_STATUS () = OSIX_FALSE;

    L2IWF_INSTANCE_ACTIVE_PTR =
        MemBuddyAlloc (gL2GlobalInfo.u1InstStateBuddyId, AST_MAX_MST_INSTANCES);

    if (L2IWF_INSTANCE_ACTIVE_PTR == NULL)
    {
        L2IwfReleaseContext ();
        L2IwfDeleteContext (u4ContextId);
        return L2IWF_FAILURE;
    }

    for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES; u2MstInst++)
    {
        L2IWF_INSTANCE_ACTIVE (u2MstInst) = OSIX_FALSE;
    }

    L2IwfReleaseContext ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeleteContext                                   */
/*                                                                           */
/* Description        : This routine de-initialises the L2 module data       */
/*                      structures and frees the allocated memory for the    */
/*                      given context                                        */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfDeleteContext (UINT4 u4ContextId)
{
    UINT4               u4PortIndex;
    tVlanId             VlanId;
    UINT2               u2MstInst;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    for (u4PortIndex = 1; u4PortIndex <= L2IWF_MAX_PORTS_PER_CONTEXT_EXT;
         u4PortIndex++)
    {
        if (L2IWF_PORT_INFO (u4PortIndex) != NULL)
        {
            if (RBTreeRemove (L2IWF_GLOBAL_IFINDEX_TREE (),
                              L2IWF_PORT_INFO (u4PortIndex)) == RB_FAILURE)
            {
                return L2IWF_FAILURE;
            }

            L2IWF_RELEASE_MEMBLOCK (L2IWF_PORTINFO_POOLID (),
                                    L2IWF_PORT_INFO (u4PortIndex));
        }
    }

    MEMSET (gpL2Context->apL2PortInfo, 0,
            (sizeof (tL2PortInfo *) * L2IWF_MAX_PORTS_PER_CONTEXT_EXT));

    if (L2IWF_VID_INST_MAP_ARRAY () != NULL)
    {
        L2IWF_RELEASE_MEMBLOCK (L2IWF_VLAN_FID_MST_INST_POOLID (),
                                L2IWF_VID_INST_MAP_ARRAY ());
        L2IWF_VID_INST_MAP_ARRAY () = NULL;
    }

    for (VlanId = 1; VlanId <= VLAN_DEV_MAX_VLAN_ID; VlanId++)
    {
        if (L2IWF_VLAN_INFO (VlanId) != NULL)
        {
            L2IWF_RELEASE_MEMBLOCK (L2IWF_VLANINFO_POOLID (),
                                    L2IWF_VLAN_INFO (VlanId));
        }
    }

    for (u2MstInst = 1; u2MstInst < AST_MAX_MST_INSTANCES; u2MstInst++)
    {
        if (L2IWF_MST_INFO (u2MstInst) != NULL)
        {
            L2IWF_RELEASE_MEMBLOCK (L2IWF_MSTINFO_POOLID (),
                                    L2IWF_MST_INFO (u2MstInst));
        }
    }

    /*The CVLAN Port Table will be created in case of PEB/PCB. This RBTree
     * will be created at the time of the Bridge Mode change*/
    if (L2IWF_CVLAN_PORT_TBL () != NULL)
    {
        RBTreeDelete (L2IWF_CVLAN_PORT_TBL ());
        L2IWF_CVLAN_PORT_TBL () = NULL;
    }

    if (L2IWF_IFTYPE_PROTO_DENY_TREE () != NULL)
    {
        RBTreeDestroy (L2IWF_IFTYPE_PROTO_DENY_TREE (),
                       (tRBKeyFreeFn) L2IwfIfTypeProtoDenyFreeNode, 0);
        L2IWF_IFTYPE_PROTO_DENY_TREE () = NULL;
    }

    if (L2IWF_INSTANCE_ACTIVE_PTR != NULL)
    {
        MemBuddyFree (gL2GlobalInfo.u1InstStateBuddyId,
                      L2IWF_INSTANCE_ACTIVE_PTR);
        L2IWF_INSTANCE_ACTIVE_PTR = NULL;
    }

    L2IWF_RELEASE_MEMBLOCK (L2IWF_CONTEXTINFO_POOLID (), gpL2Context);

    /*The CVLAN Port Table will be created in case of PEB/PCB. This RBTree
     * will be created at the time of the Bridge Mode change*/

    L2IwfReleaseContext ();

    L2IWF_CONTEXT_INFO (u4ContextId) = NULL;

    return L2IWF_SUCCESS;
}

#endif /* _L2INIT_C_ */
