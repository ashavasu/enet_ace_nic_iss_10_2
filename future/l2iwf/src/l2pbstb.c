
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 * $Id: l2pbstb.c,v 1.11 2016/03/14 10:27:00 siva Exp $
 *
 * Description: This file contains all the stub functions
 *
 *******************************************************************/


#include "l2inc.h"
/* Vlan Translation table related functions. */

INT4
L2IwfPbInitMemPools (VOID)
{
    return L2IWF_SUCCESS;
}

INT4
L2IwfPbDeInitMemPools (VOID)
{
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfValidateBridgeMode                              */
/*                                                                           */
/* Description        : This function validates the Bridge mode based on     */
/*                      packages                                             */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context ID                            */
/*                      u4BridgeMode - Bridge mode to be set in L2IWF        */
/*                      This takes one of the following values               */
/*                      CustomerBridge/ProviderEdgeBridge/ProviderCoreBridge */
/*                      ProviderBridge                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfValidateBridgeMode (UINT4 u4BridgeMode)
{
    if ((u4BridgeMode < L2IWF_CUSTOMER_BRIDGE_MODE) ||
        (u4BridgeMode > L2IWF_PROVIDER_BRIDGE_MODE))
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

INT4
L2IwfSetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId, UINT1 u1PortState)
{
    UNUSED_PARAM (u4CepIfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1PortState);
    return L2IWF_SUCCESS;
}

INT4
L2IwfPbGetLocalVidFromRelayVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                tVlanId RelayVid, tVlanId * pLocalVid)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (RelayVid);
    UNUSED_PARAM (pLocalVid);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetRelayVidFromLocalVid                       */
/*                                                                           */
/* Description        : This routine gets the relay vid for the corresponding*/
/*                      local vid.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                      u2LocalPort - Per Context Port Index.                */
/*                      LocalVid    - Local Vlan Id                          */
/*                                                                           */
/* Output(s)          : pRelayVid - RelayVid value for the given local vid   */
/*                      Value.                                               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetRelayVidFromLocalVid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                tVlanId LocalVid, tVlanId * pRelayVid)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (LocalVid);
    UNUSED_PARAM (pRelayVid);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetDefBrgPortTypeInCfa                        */
/*                                                                           */
/* Description        : This function set the default bridge port type for   */
/*                      the given port based on the bridge mode. By using    */
/*                      the u1BrgModePortType argument we can force this     */
/*                      function to set the bridge port type as customerPort */
/*                      irrespective of the bridge mode.                     */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual ContextId                      */
/*                      u4IfIndex   - IfIndex of the port.                   */
/*                      u1BrgModePortType - Whether to set the bridge port   */
/*                                          type based on bridge mode or     */
/*                                          not.                             */
/*                      u1IsCnpCheck - Whether to set the CNP Count or not   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbSetDefBrgPortTypeInCfa (UINT4 u4ContextId, UINT4 u4IfIndex,
                               UINT1 u1BrgModePortType, UINT1 u1IsCnpCheck)
{
    tCfaIfInfo          CfaIfInfo;
    INT4                i4RetVal = L2IWF_SUCCESS;

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1BrgModePortType);
    UNUSED_PARAM (u1IsCnpCheck);

    if (CfaIsSbpInterface (u4IfIndex) == CFA_TRUE)
    {
        CfaIfInfo.u1BrgPortType = L2IWF_STATION_FACING_BRIDGE_PORT;
    }
    else
    {
        CfaIfInfo.u1BrgPortType = L2IWF_CUSTOMER_BRIDGE_PORT;
    }


    i4RetVal = CfaSetIfInfo (IF_BRG_PORT_TYPE, u4IfIndex, &CfaIfInfo);

    if (i4RetVal == CFA_FAILURE)
    {
        return L2IWF_FAILURE;
    }
#ifdef VLAN_WANTED
    L2IwfPbSetPortType (u4IfIndex, (INT4) CfaIfInfo.u1BrgPortType);
#endif
    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetDefPortType                                */
/*                                                                           */
/* Description        : This function sets the default port type for the     */
/*                      given port based on the bridge mode.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port IfIndex.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbSetDefPortType (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetPcPortTypeToPhyPort                        */
/*                                                                           */
/* Description        : This function sets the port type of the port channel */
/*                      to the given port.                                   */
/*                                                                           */
/* Input(s)           : u4PcIfIndex - Port-channel IfIndex.                  */
/*                      u4PhyIfIndex - IfIndex of the port                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbSetPcPortTypeToPhyPort (UINT4 u4PcIfIndex, UINT4 u4PhyIfIndex)
{
    UNUSED_PARAM (u4PcIfIndex);
    UNUSED_PARAM (u4PhyIfIndex);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsVlanElan                                      */
/*                                                                           */
/* Description        : This function is called to check whether the VLAN    */
/*                      service type is configured as ELAN                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch Identifier              */
/*                      VlanId      - Vlan Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE / OSIX_FALSE                               */
/*****************************************************************************/

BOOL1
L2IwfIsVlanElan (UINT4 u4ContextId, tVlanId VlanId)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    /*In Customer Bridges, All VLANs are ELANs */
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPbPortState                                  */
/*                                                                           */
/* Description        : This function is used to get Provider Edge Port State*/
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                                                                           */
/* Output(s)          : u1PortState - Port State of the PEP Port(CEP,SVID)   */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

UINT1
L2IwfGetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId)
{
    UNUSED_PARAM (u4CepIfIndex);
    UNUSED_PARAM (SVlanId);
    return AST_PORT_STATE_DISCARDING;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfGetNextCVlanPort                      */
/*                                                                           */
/*    Description               : This function is used to get the next PEP  */
/*                                Port in a given CVLAN component            */
/*                                                                           */
/*    Input(s)                  : u4CepIfIndex - Indicates CVLAN comp.       */
/*                                u2PrevPort - Previous PEP HL Port Id       */
/*                                                                           */
/*    Output(s)                 :*pu2NextPort - Next PEP's HL Port Id        */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/

INT4
L2IwfGetNextCVlanPort (UINT4 u4CepIfIndex, UINT2 u2PrevPort, UINT2 *pu2NextPort,
                       UINT4 *pu4NextCepIfIndex)
{
    UNUSED_PARAM (u4CepIfIndex);
    UNUSED_PARAM (u2PrevPort);
    UNUSED_PARAM (pu2NextPort);
    UNUSED_PARAM (pu4NextCepIfIndex);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPbPortOperEdgeStatus                         */
/*                                                                           */
/* Description        : This function is used to get Provider Edge Port oper */
/*                      Edge status                                          */
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                                                                           */
/* Output(s)          : *pbOperEdge - OSIX_TRUE/OSIX_FALSE                   */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
L2IwfGetPbPortOperEdgeStatus (UINT4 u4CepIfIndex, tVlanId SVlanId,
                              BOOL1 * pbOperEdge)
{
    UNUSED_PARAM (u4CepIfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (pbOperEdge);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfGetPbPortOperStatus                   */
/*                                                                           */
/*    Description               : This function is used to get the PEP       */
/*                                Port Oper Status which is updated by VLAN  */
/*                                                                           */
/*    Input(s)                  : u4CepIfIndex - Indicates CVLAN comp.       */
/*                                SVlanId - Service VLAN Identifier          */
/*                                u1Module - STP_MODULE                      */
/*                                                                           */
/*    Output(s)                 :*pu1OperStatus - PEP port oper status       */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/

INT4
L2IwfGetPbPortOperStatus (UINT1 u1Module, UINT4 u4CepIfIndex, tVlanId SVlanId,
                          UINT1 *pu1OperStatus)
{
    UNUSED_PARAM (u1Module);
    UNUSED_PARAM (u4CepIfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (pu1OperStatus);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetVlanServiceType                            */
/*                                                                           */
/* Description        : This function is used to get the type of service the */
/*                      VLAN provides. The service type is updated by VLAN   */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : VlanId - Service VLAN Identifier                     */
/*                                                                           */
/* Output(s)          : *pu1ServiceType - VLAN_E_LINE /VLAN_E_LAN            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
L2IwfPbGetVlanServiceType (UINT4 u4ContextId, tVlanId VlanId,
                           UINT1 *pu1ServiceType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pu1ServiceType);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetInstCepPortState                           */
/*                                                                           */
/* Description        : This function is called to configure CEP Port State  */
/*                      for all Instances                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - CEP Port IfIndex                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/

INT4
L2IwfPbSetInstCepPortState (UINT4 u4CepIfIndex, UINT1 u1PortState)
{
    UNUSED_PARAM (u4CepIfIndex);
    UNUSED_PARAM (u1PortState);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfSendCustBpduOnPep                     */
/*                                                                           */
/*    Description               : This API is used by L2 modules to transmit */
/*                                packets on a set of port through CFA.      */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                u2IfIndex - ifIndex of the driver port.    */
/*                                u4PktSize - Size of the data buffer        */
/*                                u2Protocol - Protocol type                 */
/*                                u1EncapType - Encapsulation Type           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfSendCustBpduOnPep (UINT4 u4CepIfIndex, tVlanId SVlanId,
                        tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4PktSize)
{
    UNUSED_PARAM (u4CepIfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4PktSize);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPbPortOperEdgeStatus                         */
/*                                                                           */
/* Description        : This function is used to set Provider Edge Port oper */
/*                      Edge status                                          */
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                      bOperEdge - OSIX_TRUE/OSIX_FALSE                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
L2IwfSetPbPortOperEdgeStatus (UINT4 u4CepIfIndex, tVlanId SVlanId,
                              BOOL1 bOperEdge)
{
    UNUSED_PARAM (u4CepIfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (bOperEdge);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfHwPbPepCreate                                   */
/*                                                                           */
/* Description        : This function is used to indicate Ast about          */
/*                      provider edge port is created in Hw.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      SVlanId    - Service vlan Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfHwPbPepCreate (UINT4 u4IfIndex, tVlanId SVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbCreateProviderEdgePort                        */
/*                                                                           */
/* Description        : This function is used to create Provider Edge Port   */
/*                      in L2IWF. This would be called by VLAN whenever the  */
/*                      provider edge port is created in VLAN.               */
/*                                                                           */
/* Input(s)           : u2IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfPbCreateProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbPepDeleteIndication                           */
/*                                                                           */
/* Description        : This function is used to delete Provider Edge Port   */
/*                      in L2IWF. This would be called by VLAN whenever the  */
/*                      provider edge port is deleted from VLAN.             */
/*                      This function also indicated PEP deletion to ASTP.   */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      SVlanId    - Service Vlan Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPbPepDeleteIndication (UINT4 u4IfIndex, tVlanId SVlanId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfPbPepHandleInCustBpdu                 */
/*                                                                           */
/*    Description               : This API is used to post the customer BPDUs*/
/*                                received on PEPs to AST task.              */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                u2Port - Incoming port.                    */
/*                                SVlanId - Service VLAN ID                  */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/

VOID
L2IwfPbPepHandleInCustBpdu (tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4IfIndex,
                            tVlanId SVlanId)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbUpdatePepOperStatus                           */
/*                                                                           */
/* Description        : This function is used to delete Provider Edge Port   */
/*                      in L2IWF. This would be called by VLAN whenever the  */
/*                      provider edge port is deleted from VLAN.             */
/*                                                                           */
/* Input(s)           : u2IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfPbUpdatePepOperStatus (UINT4 u4IfIndex, tVlanId SVlanId,
                            UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
    UNUSED_PARAM (u1OperStatus);
    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbConfigVidTransEntry                           */
/*                                                                           */
/* Description        : This routine configures an entry in vid translation  */
/*                      table. An entry configured through this function     */
/*                      results in inserting the node in the ingress vid     */
/*                      translation table and also in the egress vid         */
/*                      translation table.                                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                      VidTransEntryInfo  - This contains the following:    */
/*                              - u2Port - Port Id.                          */
/*                              - LocalVid - Local Vid for the entry.        */
/*                              - RelayVid - Relay Vid for the entry.        */
/*                              - u1RowStatus - Row Status for the entry.    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbConfigVidTransEntry (UINT4 u4ContextId,
                            tVidTransEntryInfo VidTransEntryInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VidTransEntryInfo);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbCreateVidTransTable                           */
/*                                                                           */
/* Description        : This routine creates the vid translation table for a */
/*                      given context. This function will be called when     */
/*                      vlan is started for 1ad bridge.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - virtual context id                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbCreateVidTransTable (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbDeleteVidTransTable                           */
/*                                                                           */
/* Description        : This routine deletes the vid translation table for   */
/*                      given context. This function will be called when     */
/*                      vlan is shutdown for a context.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - virtual context id                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbDeleteVidTransTable (UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetNextVidTransTblEntry                       */
/*                                                                           */
/* Description        : This routine returns the valid lowest vid translation*/
/*                      table entry whose (port, Vid) pair is greater than   */
/*                      the given (port,LocalVid) pair.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2Port      - Per Context port index                 */
/*                      LocalVid    - Local Vlan Id                          */
/*                                                                           */
/* Output(s)          : pOutVidTransEntryInfo - vid translation entry whose  */
/*                      (u2Port, LocalVid) pair is next (greater) to given   */
/*                      (u2Port, LocalVid) pair.                             */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetNextVidTransTblEntry (UINT4 u4ContextId, UINT2 u2Port,
                                tVlanId LocalVid,
                                tVidTransEntryInfo * pOutVidTransEntryInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (LocalVid);
    UNUSED_PARAM (pOutVidTransEntryInfo);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetVidTransEntry                              */
/*                                                                           */
/* Description        : This routine gets an entry with the given inputs.    */
/*                      Based on u1IsLocalVid value, the search will be done */
/*                      on the ingress/egress vid translation table.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id.                            */
/*                      VidTransEntryInfo  - This contains the following:    */
/*                              - u2Port - Port Id.                          */
/*                              - LocalVid - Local Vid for the entry.        */
/*                              - RelayVid - Relay Vid for the entry.        */
/*                              - u1RowStatus - Row Status for the entry.    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetVidTransEntry (UINT4 u4ContextId, UINT2 u2LocalPort,
                         tVlanId SearchVid, UINT1 u1IsLocalVid,
                         tVidTransEntryInfo * pOutVidTransInfo)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (SearchVid);
    UNUSED_PARAM (u1IsLocalVid);
    UNUSED_PARAM (pOutVidTransInfo);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetVidTransStatus                             */
/*                                                                           */
/* Description        : This routine gets the vid translation status for the */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2LocalPort - Per Context port index                 */
/*                                                                           */
/* Output(s)          : pu1Status - vid translation status for this port.    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetVidTransStatus (UINT4 u4ContextId, UINT2 u2LocalPort,
                          UINT1 *pu1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPort);
    UNUSED_PARAM (pu1Status);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetVidTransStatus                             */
/*                                                                           */
/* Description        : This routine sets the vid translation status for the */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2LocalPortId - Per Context port index               */
/*                      u1Status - Vid translation status for this port.     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbSetVidTransStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                          UINT1 u1Status)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u2LocalPortId);
    UNUSED_PARAM (u1Status);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetVlanServiceType                            */
/*                                                                           */
/* Description        : This function is used to set the VLAN service type   */
/*                      in L2IWF. The service type is updated by VLAN        */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : VlanId - Service VLAN Identifier                     */
/*                                                                           */
/* Output(s)          : u1ServiceType - VLAN_E_LINE /VLAN_E_LAN              */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfPbSetVlanServiceType (UINT4 u4ContextId, tVlanId VlanId,
                           UINT1 u1ServiceType)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1ServiceType);
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetLocalVidListFromVIDTransTable              */
/*                                                                           */
/* Description        : This routine returns the list of local VLANs         */
/*                      configured for the given port.                       */
/*                                                                           */
/* Input(s)           : u4IfIndex   - IfIndex                                */
/*                      pu1LocalVidList - Local VID list                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbGetLocalVidListFromVIDTransTable (UINT4 u4IfIndex,
                                         UINT1 *pu1LocalVidList)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1LocalVidList);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbUtilDeletePEPEntry                            */
/*                                                                           */
/* Description        : This function is used to delete the PEP entry        */
/*                      in L2IWF                                             */
/*                                                                           */
/* Input(s)           : u2LocalPort - Local Port Id                          */
/*                      SVlanId    - Service vlan Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfPbUtilDeletePEPEntry (UINT2 u2LocalPortId, tVlanId SVlanId)
{
    UNUSED_PARAM (u2LocalPortId);
    UNUSED_PARAM (SVlanId);
    return L2IWF_SUCCESS;
}
