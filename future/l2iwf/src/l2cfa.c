/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: l2cfa.c,v 1.49 2014/11/03 12:14:58 siva Exp $ 
 *
 * Description: This file has L2 related CFA functions 
 *
 *******************************************************************/
#include "l2inc.h"
#ifdef NPAPI_WANTED
#include "rstnp.h"
#endif
/*****************************************************************************
 * Important Note:
 * ===============
 * Please note that switches for Layer 2 protocols have been introduced in this
 * file  to make the porting process easier for the individual Layer 2 stacks.
 * Such switches may not be introduced else where in the code.
 *****************************************************************************/

/*****************************************************************************/
/* Function Name      : L2IwfPortCreateIndication                            */
/*                                                                           */
/* Description        : This routine is used by CFA or Management module to  */
/*                      indicate to the L2 applications about a new port     */
/*                      in the system.                                       */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfPortCreateIndication (UINT4 u4IfIndex)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
#ifdef ISS_WANTED
    UINT1               u1IfEtherType;
#endif

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_FAILURE)
    {
        /* Port not mapped - hence return */
        return;
    }

#ifdef NPAPI_WANTED
    if (VcmMapPortToContextInHw (u4ContextId, u4IfIndex) == VCM_FAILURE)
    {
        return;
    }
#endif

    CfaGetIfInfo (u4IfIndex, &IfInfo);
    if (IfInfo.u1BrgPortType == L2IWF_INVALID_PROVIDER_PORT)
    {
        /* Set the default bridge port type for the port, based
           on the bridge mode. */
        if ((L2IwfPbSetDefBrgPortTypeInCfa (u4ContextId, u4IfIndex,
                                            L2IWF_TRUE, L2IWF_FALSE))
            == L2IWF_FAILURE)
        {
            return;
        }
    }

    CfaGetIfInfo (u4IfIndex, &IfInfo);

    if (L2IwfCreatePort (u4ContextId, u4IfIndex, u2LocalPortId, &IfInfo)
        == L2IWF_FAILURE)
    {
        return;
    }

    L2IwfSendPortCreateInd (u4ContextId, u4IfIndex, u2LocalPortId, &IfInfo);

#ifdef ISS_WANTED
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        CfaGetEthernetType (u4IfIndex, &u1IfEtherType);
        if (u1IfEtherType == CFA_STACK_ENET)
        {
            /* Stack Ports should not be visible to L2 */
            return;
        }
    }
#endif

    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfPortDeleteIndication                            */
/*                                                                           */
/* Description        : This routine is used by CFA or Management module to  */
/*                      inform the L2 applications that a port has been      */
/*                      deleted from the system.                             */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index being deleted.          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfPortDeleteIndication (UINT4 u4IfIndex)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return;
    }

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_FAILURE)
    {
        /* Port not mapped - hence return */
        return;
    }
    if (u4ContextId == VCM_INVALID_VC)
    {
        /* Mapping is under creation. So return */
        return;
    }

    L2IwfSendPortDeleteInd (u4IfIndex, &IfInfo);
    L2IwfDeletePort (u4IfIndex, IfInfo.u1IfType, L2IWF_TRUE);

    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfPortDelIndication                            */
/*                                                                           */
/* Description        : This routine is used by CFA or Management module to  */
/*                      inform the L2 applications that a router port is     */
/*                      created and dont delete entry in l2iwf db for that port */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index being deleted.          */
/*                      u1BridgedIface - Interface status                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
L2IwfPortDelIndication (UINT4 u4IfIndex, UINT1 u1BridgedIface)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    MEMSET (&IfInfo, OSIX_FALSE, sizeof (tCfaIfInfo));

    UNUSED_PARAM (u1BridgedIface);

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return;
    }

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_FAILURE)
    {
        /* Port not mapped - hence return */
        return;
    }
    if (u4ContextId == VCM_INVALID_VC)
    {
        /* Mapping is under creation. So return */
        return;
    }

    L2IwfSendPortDeleteInd (u4IfIndex, &IfInfo);
    if((IfInfo.u1IfType == CFA_ENET) && (u1BridgedIface == CFA_DISABLED))
    {
        L2IwfDelPort (u4IfIndex, IfInfo.u1IfType, L2IWF_TRUE);
    }
    else
    {
        L2IwfDeletePort (u4IfIndex, IfInfo.u1IfType, L2IWF_TRUE);
    }

    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfPortOperStatusIndication                        */
/*                                                                           */
/* Description        : This routine is used by CFA whenever the physical    */
/*                      operational status of the port changes to up or down */
/*                      This routine informs this to PNAC if it is enabled   */
/*                      in the system. PNAC will inturn indicate to its      */
/*                      higher layers. This routine takes care of indicating */
/*                      the port oper status change to PNAC's higher layers  */
/*                      if PNAC is not enabled in the system.                */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfPortOperStatusIndication (UINT4 u4IfIndex, UINT1 u1Status)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
#ifdef PNAC_WANTED
    tCfaIfInfo          IfInfo;
#endif

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_FAILURE)
    {
        /* Port not mapped - hence return */
        return;
    }

#ifdef PNAC_WANTED
    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return;
    }

    /* Assumption : PNAC is applicable for Ethernet interfaces only */
    if ((IfInfo.u1IfType == CFA_ENET)
        || (IfInfo.u1IfType == CFA_CAPWAP_DOT11_BSS))
    {
        if (L2IwfIfTypeProtoDenyGetNode ((INT4) u4ContextId,
                                         (INT4) IfInfo.u1IfType,
                                         (INT4) IfInfo.u1BrgPortType,
                                         (INT4) L2IWF_PROTOCOL_ID_PNAC) == NULL)
        {
            if (PnacGetPnacEnableStatus () == (INT4) PNAC_SUCCESS)
            {
                /* Directly inform to PNAC rather than sending it out on a queue */
                PnacL2IwfProcPortStatUpdEvent ((UINT2) u4IfIndex, u1Status);
                return;
            }
        }
    }
#endif

    if (u1Status == CFA_IF_UP)
    {
        L2IwfSetPortPnacAuthStatus (u4IfIndex, PNAC_PORTSTATUS_AUTHORIZED);
    }
    else
    {
        L2IwfSetPortPnacAuthStatus (u4IfIndex, PNAC_PORTSTATUS_UNAUTHORIZED);
    }

    /* If PNAC is disabled, the below function should be called from 
     * L2Iwf itself to indicate the oper status to higher layers of PNAC */
    L2IwfPnacHLPortOperIndication (u4IfIndex, u1Status);

    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetBridgePortOperStatus                         */
/*                                                                           */
/* Description        : This routine updates the given port's Bridge Oper    */
/*                      Status in the L2Iwf common database.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Control status is to be updated.         */
/*                      u1BridgeOperStatus - Value of BridgePortOperStatus   */
/*                                           to be updated                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetBridgePortOperStatus (UINT4 u4IfIndex, UINT1 u1BridgeOperStatus)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_IFENTRY_BRIDGE_OPER_STATUS (pL2PortEntry) = u1BridgeOperStatus;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetBridgePortOperStatus                         */
/*                                                                           */
/* Description        : This routine Gets the given port's Bridge Oper       */
/*                      Status from the L2Iwf common database.               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port               */
/*                      u1BridgeOperStatus - Value of BridgePortOperStatus   */
/*                                           to be updated                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetBridgePortOperStatus (UINT4 u4IfIndex, UINT1 *pu1BridgeOperStatus)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1BridgeOperStatus = L2IWF_IFENTRY_BRIDGE_OPER_STATUS (pL2PortEntry);

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetEoamTunnelStatus                             */
/*                                                                           */
/* Description        : This routine gets the EOAM tunnel                    */
/*                      status from the L2Iwf common database.               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port               */
/*                      pu1EoamTunnelStatus - Tunnel status                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetEoamTunnelStatus (UINT4 u4IfIndex, UINT1 *pu1EoamTunnelStatus)
{
    L2IwfGetProtocolTunnelStatusOnPort (u4IfIndex, L2_PROTO_EOAM,
                                        pu1EoamTunnelStatus);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIntfTypeChangeIndication                        */
/*                                                                           */
/* Description        : This routine is used by CFA or Management module to  */
/*                      indicate to the L2 applications about change in      */
/*                      interface type.                                      */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index                         */
/*                    : u1IntfType  - Interface type                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfIntfTypeChangeIndication (UINT4 u4IfIndex, UINT1 u1IntfType)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        == VCM_FAILURE)
    {
        /* Port not mapped - hence return */
        return;
    }

    L2IwfSendIntfTypeChangeInd (u4ContextId, u1IntfType);

    return;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* Function Name      : L2CfaRedSetBridgePortOperStatus                      */
/*                                                                           */
/* Description        : This routine is used by CFA module for updating the  */
/*                      oper status to l2iwf in the standby node             */
/*                                                                           */
/* Input(s)           : u4IfIndex    - Interface index                       */
/*                    : u1OperStatus - oper status                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
L2CfaRedSetBridgePortOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tCfaIfInfo          IfInfo;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return;
    }

#ifdef PNAC_WANTED
    if (IfInfo.u1IfType == CFA_ENET)
    {
        if (PnacGetPnacEnableStatus () == (INT4) PNAC_SUCCESS)
        {
            return;
        }
    }
#endif

#ifdef LA_WANTED
    if (IfInfo.u1IfType == CFA_ENET)
    {
        if (LaGetLaEnableStatus () == LA_ENABLED)
        {
            return;
        }
    }
#endif
    if (u1OperStatus == CFA_IF_UP)
    {
        L2IwfSetPortPnacAuthStatus (u4IfIndex, PNAC_PORTSTATUS_AUTHORIZED);
    }
    else
    {
        L2IwfSetPortPnacAuthStatus (u4IfIndex, PNAC_PORTSTATUS_UNAUTHORIZED);
    }

    L2IwfSetBridgePortOperStatus (u4IfIndex, u1OperStatus);
}
#endif
