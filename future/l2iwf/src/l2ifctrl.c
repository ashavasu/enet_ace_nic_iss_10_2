#ifndef __L2IFCTRL_C__
#define __L2IFCTRL_C__
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2ifctrl.c,v 1.45 2017/12/28 10:02:23 siva Exp $
 *
 * Description:This file contains routines to get/set Port state         
 *             information from the L2IWF module.                 
 *
 *******************************************************************/

#include "l2inc.h"
#include "fsbuddy.h"

/*****************************************************************************/
/* Function Name      : L2IwfAllocInstPortStateMem                           */
/*                                                                           */
/* Description        : This routine is called from PVRST to allocate the    */
/*                      memory for Port's state info of instance in the      */
/*                      L2Iwf common database for the given context.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2LocalPortId - Local HLIndex of the port            */
/*                      u1Mode - Specifies mode PVRST START/SHUTDOWN         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfAllocInstPortStateMem (UINT4 u4ContextId, UINT2 u2LocalPortId,
                            UINT1 u1Mode)
{
    UINT2               u2MstInst;
    tL2PortInfo        *pL2PortInfo = NULL;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pL2PortInfo = L2IWF_PORT_INFO (u2LocalPortId);

    if (pL2PortInfo == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    if (L2IWF_INST_PORT_STATE_PTR (u2LocalPortId) != NULL)
    {
        MemBuddyFree (gL2GlobalInfo.u1PortStateBuddyId,
                      L2IWF_INST_PORT_STATE_PTR (u2LocalPortId));
    }

    if (u1Mode == PVRST_START)
    {
        L2IWF_INST_PORT_STATE_PTR (u2LocalPortId) =
            MemBuddyAlloc (gL2GlobalInfo.u1PortStateBuddyId,
                           AST_MAX_PVRST_INSTANCES + 1);

        if (L2IWF_INST_PORT_STATE_PTR (u2LocalPortId) == NULL)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }

        for (u2MstInst = 1; u2MstInst <= AST_MAX_PVRST_INSTANCES; u2MstInst++)
        {
            /* When PVRST is started, it goes to disabled state
             * by default and hence the port states should be set to
             * forwarding to allow data packets to be forwarded. 
             * When PVRST is enabled, port-states will be updated 
             * as per the state event machines */
            L2IwfMiSetInstPortState (u4ContextId, u2MstInst,
                                     u2LocalPortId, AST_PORT_STATE_FORWARDING);
        }
    }
    else
    {
        L2IWF_INST_PORT_STATE_PTR (u2LocalPortId) = (UINT1 *)
            MemBuddyAlloc (gL2GlobalInfo.u1PortStateBuddyId,
                           AST_MAX_MST_INSTANCES);

        if (L2IWF_INST_PORT_STATE_PTR (u2LocalPortId) == NULL)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }

        for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES; u2MstInst++)
        {
            /* Port State is set to Forwarding to handle data packets 
             * when PVRST is shutdown & no other STP (RSTP/MSTP) is started*/
            L2IwfMiSetInstPortState (u4ContextId, u2MstInst,
                                     u2LocalPortId, AST_PORT_STATE_FORWARDING);
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfFlushFdbForGivenInst                            */
/*                                                                           */
/*                                                                           */
/* Description        : This function constructs the fdblist from the        */
/*                      Instance Id and Interface Index and then             */
/*                      calls the VLAN module to flush the                   */
/*                      FDB list .                                           */
/*                                                                           */
/* Input(s)           : u4Port - Port number                                 */
/*                      pu1FdbList - List of FDB Identifiers                 */
/*                      i2OptimizeFlag - Indicates whether this call can be  */
/*                      grouped with the flush call for other ports as a     */
/*                      as a single flush call.                              */
/*                      u2Flag  -  Flag that indicates whether the entire    */
/*                                 list will be flushed or a single fdbid    */
/*                                 will be flushed.                          */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfFlushFdbForGivenInst (UINT4 u4ContextId, UINT4 u4IfIndex,
                           UINT2 u2InstanceId, INT2 i2OptimizeFlag,
                           UINT1 u1Module, UINT2 u2FlushFlag)
{
    tVlanFlushInfo      VlanFlushInfo;

    UNUSED_PARAM (u2FlushFlag);

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (&VlanFlushInfo, 0, sizeof (tVlanFlushInfo));

    /* If Flush Flag is called with value as FALSE ( Eg., from modules like
     * MSTPP) flush the fdb entres one by one.
     * If Flag is true (Eg., called from modules like ERPS , obtain the FDB 
     * List and flush the list */

    L2IwfMiGetFdbListInInstance (u4ContextId, u2InstanceId, &VlanFlushInfo);

    VlanFlushInfo.u4IfIndex = u4IfIndex;
    VlanFlushInfo.i2OptimizeFlag = i2OptimizeFlag;
    VlanFlushInfo.u1Module = u1Module;
    VlanFlushInfo.u2InstanceId = u2InstanceId;

    /* Flush the Fdb List */
    if (VlanFlushInfo.u2FdbCount != 0)
    {
        VlanFlushFdbTable (&VlanFlushInfo);
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfAllocInstActiveStateMem                         */
/*                                                                           */
/* Description        : This routine is called from PVRST to allocate the    */
/*                      memory for Active state info of instance in the      */
/*                      L2Iwf common database for the given context.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u1Mode - Specifies Mode (PVRST START/SHUTDOWN)       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfAllocInstActiveStateMem (UINT4 u4ContextId, UINT1 u1Mode)
{
    UINT2               u2MstInst;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_INSTANCE_ACTIVE_PTR != NULL)
    {
        MemBuddyFree (gL2GlobalInfo.u1InstStateBuddyId,
                      (UINT1 *) L2IWF_INSTANCE_ACTIVE_PTR);
    }

    if (u1Mode == PVRST_START)
    {
        L2IWF_INSTANCE_ACTIVE_PTR =
            MemBuddyAlloc (gL2GlobalInfo.u1InstStateBuddyId,
                           AST_MAX_PVRST_INSTANCES + 1);

        if (L2IWF_INSTANCE_ACTIVE_PTR == NULL)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }

        MEMSET (L2IWF_INSTANCE_ACTIVE_PTR, 0, AST_MAX_PVRST_INSTANCES + 1);

        for (u2MstInst = 1; u2MstInst <= AST_MAX_PVRST_INSTANCES; u2MstInst++)
        {
            L2IWF_INSTANCE_ACTIVE (u2MstInst) = OSIX_FALSE;
        }
    }
    else
    {
        L2IWF_INSTANCE_ACTIVE_PTR =
            MemBuddyAlloc (gL2GlobalInfo.u1InstStateBuddyId,
                           AST_MAX_MST_INSTANCES);

        if (L2IWF_INSTANCE_ACTIVE_PTR == NULL)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }

        MEMSET (L2IWF_INSTANCE_ACTIVE_PTR, 0, AST_MAX_MST_INSTANCES);

        for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES; u2MstInst++)
        {
            L2IWF_INSTANCE_ACTIVE (u2MstInst) = OSIX_FALSE;
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetInstPortState                                */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Port's state for that instance in the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u2MstInst - InstanceId for which the port state is   */
/*                                  to be updated.                           */
/*                    : u4IfIndex - Global IfIndex of the port whose state   */
/*                                  is to be updated.                        */
/*                      u1PortState - Port state to be set                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetInstPortState (UINT2 u2MstInst, UINT4 u4IfIndex, UINT1 u1PortState)
{
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    UINT1              *pVlanList = NULL;
#endif
    UINT4               u4ContextId;
    tL2PortInfo        *pL2PortEntry = NULL;
    tMrpInfo            MrpInfo;
    UINT2               u2VlanIndex;
    UINT2               u2InstIndex;
    UINT1               u1Result = VLAN_FALSE;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    if (u2MstInst != ISS_ALL_STP_INSTANCES)
    {
        MEMSET (&MrpInfo, 0, sizeof (tMrpInfo));
        if (AstIsPvrstStartedInContext (L2IWF_DEFAULT_CONTEXT) == AST_TRUE)
        {
            if (u2MstInst > AST_MAX_PVRST_INSTANCES ||
                (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE))
            {
                L2_UNLOCK ();
                return L2IWF_FAILURE;
            }
        }
        else if (AstIsMstEnabledInContext (L2IWF_DEFAULT_CONTEXT) == AST_TRUE)
        {
            if (((u2MstInst >= AST_MAX_MST_INSTANCES) &&
                 (u2MstInst != AST_TE_MSTID)) ||
                (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE))
            {
                L2_UNLOCK ();
                return L2IWF_FAILURE;
            }
        }
        if (u2MstInst != AST_TE_MSTID)
        {
            /* AST_TE_MSTID instance memory will not be allocated in L2IWF */
            if (L2IWF_IFENTRY_PORT_STATE (pL2PortEntry, u2MstInst) ==
                u1PortState)
            {
                L2_UNLOCK ();
                return L2IWF_SUCCESS;
            }

            L2IWF_IFENTRY_PORT_STATE (pL2PortEntry, u2MstInst) = u1PortState;
        }
    }

    u4ContextId = pL2PortEntry->u4ContextId;

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (u2MstInst == ISS_ALL_STP_INSTANCES)
    {
        if (AstIsPvrstStartedInContext (u4ContextId) == AST_TRUE)
        {
            for (u2InstIndex = 0; u2InstIndex <= AST_MAX_PVRST_INSTANCES;
                 u2InstIndex++)
            {
                L2IWF_IFENTRY_PORT_STATE (pL2PortEntry, u2InstIndex)
                    = u1PortState;
            }
        }
        else if (AstIsMstStartedInContext (u4ContextId) == AST_TRUE)
        {
            for (u2InstIndex = 0; u2InstIndex < AST_MAX_MST_INSTANCES;
                 u2InstIndex++)
            {
                L2IWF_IFENTRY_PORT_STATE (pL2PortEntry, u2InstIndex)
                    = u1PortState;
            }
        }
        else
        {
            L2IWF_IFENTRY_PORT_STATE (pL2PortEntry, RST_DEFAULT_INSTANCE)
                = u1PortState;
        }
    }
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    pVlanList = UtilVlanAllocVlanListSize (sizeof (tSnoopVlanList));
    if (pVlanList == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    MEMSET (pVlanList, 0, sizeof (tSnoopVlanList));
#endif
    /* Get the List of Vlans mapped to the MSTP Instance. This time consuming 
     * operation must be performed only when Enhanced filtering is enabled in 
     * the system */
    if (L2IWF_VLAN_ENH_FILTERING_STATUS () == L2IWF_TRUE)
    {
        for (u2VlanIndex = 1; u2VlanIndex <= VLAN_DEV_MAX_VLAN_ID;
             u2VlanIndex++)
        {
            if (L2IWF_VLAN_ACTIVE (u2VlanIndex) == OSIX_TRUE)
            {
                if (u2MstInst != ISS_ALL_STP_INSTANCES)
                {
#ifdef MSTP_WANTED
                    if (u2MstInst != L2IWF_VID_INST_MAP (u2VlanIndex))
                    {
                        continue;
                    }
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
                    if (AstIsMstStartedInContext (u4ContextId))
                    {
                        OSIX_BITLIST_SET_BIT (pVlanList, u2VlanIndex,
                                              L2IWF_VLAN_LIST_SIZE);
                    }
#endif
#endif
                }
                /*If port is member port of vlan and port is in learning
                 * state increase the forward port count*/

                L2IWF_VLAN_IS_MEMBER_PORT
                    (L2IWF_VLAN_EGRESS_PORTS (u2VlanIndex),
                     pL2PortEntry->u2LocalPortId, u1Result);

                if (u1Result == VLAN_TRUE)
                {
                    if (u1PortState == AST_PORT_STATE_LEARNING)
                    {
                        L2IWF_VLAN_IS_MEMBER_PORT
                            (L2IWF_VLAN_FORWARD_PORTS (u2VlanIndex),
                             pL2PortEntry->u2LocalPortId, u1Result);

                        if (u1Result != VLAN_TRUE)
                        {
                            OSIX_BITLIST_SET_BIT (L2IWF_VLAN_FORWARD_PORTS
                                                  (u2VlanIndex),
                                                  pL2PortEntry->u2LocalPortId,
                                                  sizeof (tLocalPortList));

                            L2IWF_FWD_PORT_COUNT (u2VlanIndex)++;
                        }
                    }
                    else if (u1PortState == AST_PORT_STATE_DISCARDING)
                    {
                        L2IWF_VLAN_IS_MEMBER_PORT
                            (L2IWF_VLAN_FORWARD_PORTS (u2VlanIndex),
                             pL2PortEntry->u2LocalPortId, u1Result);

                        if (u1Result == VLAN_TRUE)
                        {

                            OSIX_BITLIST_RESET_BIT (L2IWF_VLAN_FORWARD_PORTS
                                                    (u2VlanIndex),
                                                    pL2PortEntry->u2LocalPortId,
                                                    sizeof (tLocalPortList));

                            L2IWF_FWD_PORT_COUNT (u2VlanIndex)--;

                        }

                    }

                }
            }
        }
    }
    L2IwfReleaseContext ();
    L2_UNLOCK ();
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    /*Snoop should be indicated the topology change  only when the PortState 
     *is set to FORWARDING */
    if (u1PortState == AST_PORT_STATE_FORWARDING)
    {
        SnoopIndicateTopoChange (u4IfIndex, pVlanList);
    }
    UtilVlanReleaseVlanListSize (pVlanList);
#endif

#ifdef ECFM_WANTED
    EcfmPortStateChangeIndication (u2MstInst, u4IfIndex, u1PortState);
#endif

#ifdef FSB_WANTED
    if (u1PortState == AST_PORT_STATE_DISCARDING)
    {
        FsbApiNotifyPortDiscarding (u4IfIndex);
    }
#endif
    /*trigger for Vlan when the port status changes to
     * Learning or Discarding to re-evaluate enhance filtering
     * for all ports*/
    if (u1PortState != AST_PORT_STATE_FORWARDING)
    {
        VlanPortStpStateUpdate (u4IfIndex);
    }

    /* Module status check will be performed within the APIs
     * */
    GarpStapPortStateChange ((UINT2) u4IfIndex, u1PortState, u2MstInst);

    MrpInfo.u1Flag = MSG_PORT_STATE_CHANGE;
    MrpInfo.u1PortState = u1PortState;
    MrpInfo.u4IfIndex = u4IfIndex;
    MrpInfo.u2MapId = u2MstInst;

    MrpApiNotifyStpInfo (&MrpInfo);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiSetInstPortState                              */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Port's state for that instance in the L2Iwf common   */
/*                      database for the given context.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2MstInst - InstanceId for which the port state is   */
/*                                  to be updated.                           */
/*                      u2LocalPortId - Local HLIndex of the port whose state*/
/*                                      is to be updated.                    */
/*                      u1PortState - Port state to be set                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfMiSetInstPortState (UINT4 u4ContextId, UINT2 u2MstInst,
                         UINT2 u2LocalPortId, UINT1 u1PortState)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }
    if (AstIsPvrstStartedInContext (u4ContextId) == AST_TRUE)
    {
        if (u2MstInst > AST_MAX_PVRST_INSTANCES ||
            u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT)
        {
            return L2IWF_FAILURE;
        }
    }
    else if (AstIsMstEnabledInContext (u4ContextId) == AST_TRUE)
    {
        if (u2MstInst == AST_TE_MSTID)
        {
            return L2IWF_SUCCESS;
        }

        if (u2MstInst >= AST_MAX_MST_INSTANCES ||
            u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT)
        {
            return L2IWF_FAILURE;
        }
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2IWF_INST_PORT_STATE (u2MstInst, u2LocalPortId) = u1PortState;

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetInstPortState                                */
/*                                                                           */
/* Description        : This routine returns the port state given the        */
/*                      Instance Index and the Port Index. It accesses the   */
/*                      L2Iwf common database.                               */
/*                                                                           */
/* Input(s)           : u2MstInst - InstanceId for which the port state is   */
/*                                  to be obtained.                          */
/*                    : u4IfIndex - Global IfIndex of the port whose port    */
/*                                  state is to be obtained.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Port State                                           */
/*****************************************************************************/
UINT1
L2IwfGetInstPortState (UINT2 u2MstInst, UINT4 u4IfIndex)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    /* Return Discarding if port is NOT active */
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;
    if (AstIsPvrstStartedInContext (L2IWF_DEFAULT_CONTEXT) == AST_TRUE)
    {
        if ((u2MstInst > AST_MAX_PVRST_INSTANCES) ||
            (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE))
        {
            return AST_PORT_STATE_DISCARDING;
        }
    }
    else if (AstIsMstEnabledInContext (L2IWF_DEFAULT_CONTEXT) == AST_TRUE)
    {
        if (((u2MstInst >= AST_MAX_MST_INSTANCES) &&
             (u2MstInst != AST_TE_MSTID)) ||
            (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE))
        {
            return AST_PORT_STATE_DISCARDING;
        }
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return RST_FALSE;
    }

    if (u2MstInst == AST_TE_MSTID)
    {
        u1PortState = AST_PORT_STATE_FORWARDING;
        L2_UNLOCK ();
        return u1PortState;
    }

    u1PortState = L2IWF_IFENTRY_PORT_STATE (pL2PortEntry, u2MstInst);
    L2_UNLOCK ();

    return u1PortState;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanPortState                                */
/*                                                                           */
/* Description        : This routine returns the port state given the        */
/*                      Vlan Id and the Port Index. It accesses the L2Iwf    */
/*                      common database to find the Mst Instance to which    */
/*                      the Vlan is mapped and then gets the ports state for */
/*                      that instance.                                       */
/*                                                                           */
/* Input(s)           : VlanId - VlanId for which the port state is to be    */
/*                               obtained.                                   */
/*                    : u4IfIndex - Global IfIndex of the port whose state   */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Port State                                           */
/*****************************************************************************/
UINT1
L2IwfGetVlanPortState (tVlanId VlanId, UINT4 u4IfIndex)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2MstInst;
    /* Return Discarding if port is NOT active */
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;
    UINT1               u1ProtocolId = 0;

    if ((VlanId > VLAN_DEV_MAX_VLAN_ID) ||
        (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE))
    {
        return AST_PORT_STATE_DISCARDING;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        if (L2IwfSelectContext (L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry))
            == L2IWF_SUCCESS)
        {
            u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);
#if defined (RSTP_WANTED) || defined (ERPS_WANTED)

            if (
#ifdef RSTP_WANTED
                   (AstIsRstStartedInContext (u4ContextId)) ||
                   (AstIsMstStartedInContext (u4ContextId)) ||
#endif
#ifdef PVRST_WANTED
                   (AstIsPvrstStartedInContext (u4ContextId)) ||
#endif
                   ((L2IwfIsErpsStartedInContext (u4ContextId))
                    &&
                    (L2IwfGetPortStateCtrlOwner
                     (u4IfIndex, &u1ProtocolId, OSIX_FALSE) == L2IWF_SUCCESS)
                    && (u1ProtocolId == ERPS_MODULE)))

            {
                u2MstInst = L2IWF_VID_INST_MAP (VlanId);

                if ((u2MstInst >= AST_MAX_MST_INSTANCES)
                    && (u2MstInst != AST_TE_MSTID))
                {
                    L2IwfReleaseContext ();
                    L2_UNLOCK ();
                    return u1PortState;
                }

                if (u2MstInst == AST_TE_MSTID)

                {
                    u1PortState = AST_PORT_STATE_FORWARDING;
                    L2IwfReleaseContext ();
                    L2_UNLOCK ();
                    return u1PortState;
                }

                u1PortState =
                    L2IWF_IFENTRY_PORT_STATE (pL2PortEntry, u2MstInst);
            }
            else
            {
                /* If STP is not defined in a build but ERPS is defined 
                 * but ERPS is not controlling any of the port state then port 
                 * state should be returned as Forwarding to make the successful 
                 * control packet transmission */

                UNUSED_PARAM (u2MstInst);
                u1PortState = AST_PORT_STATE_FORWARDING;

            }
#else
            /* Either of STP or ERPS is not defined in a build then port state 
             * should be returned as Forwarding to make the successful control packet 
             * transmission */

            UNUSED_PARAM (u2MstInst);
            u1PortState = AST_PORT_STATE_FORWARDING;
#endif
            L2IwfReleaseContext ();
        }
    }

    L2_UNLOCK ();

    return u1PortState;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : L2IwfGetInstPortStateForPort                         */
/*                                                                           */
/* Description        : This routine is called from NPAPI to get the given   */
/*                      port's state in all active instances.                */
/*                                                                           */
/* Input(s)           : u4IfIndex    - Global IfIndex of the port            */
/*                                                                           */
/* Output(s)          : InstPortStateInfo - Array of instance, port state    */
/*                                          pairs.                           */
/*                      u2Count           - Number of such pairs filled.     */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfGetInstPortStateForPort (UINT4 u4IfIndex,
                              tMstInstPortStateInfo * pInstPortStateInfo,
                              UINT2 *pu2Count)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    UINT4               u4ContextId;
    UINT2               u2MstInst = 0;
    UINT2               u2PvrstInst = 0;
    UINT2               u2Index = 0;
    UINT2               u2LocalPortId;
    UINT1               u1ProtocolId = 0;

    *pu2Count = 0;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    u2Index = 0;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    u2LocalPortId = L2IWF_IFENTRY_LOCAL_PORT (pL2PortEntry);
    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* If MSTP is enabled fill the port state for each spanning tree
     * else fill it for the default spanning tree alone */
    if ((L2IwfIsErpsStartedInContext (u4ContextId))
        && (L2IwfGetPortStateCtrlOwner (u4IfIndex, &u1ProtocolId, OSIX_FALSE) ==
            L2IWF_SUCCESS) && (u1ProtocolId == ERPS_MODULE))
    {
        for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES; u2MstInst++)
        {
            if (L2IWF_INSTANCE_ACTIVE (u2MstInst) == OSIX_FALSE)
            {
                continue;
            }

            pInstPortStateInfo[u2Index].u2Inst = u2MstInst;

            if (L2IWF_PORT_ACTIVE (u2LocalPortId) == OSIX_TRUE)
            {
                pInstPortStateInfo[u2Index].u1PortState =
                    L2IWF_INST_PORT_STATE (u2MstInst, u2LocalPortId);
            }
            else
            {
                /* Set the port state as discarding if this port is not
                 * active */
                pInstPortStateInfo[u2Index].u1PortState =
                    AST_PORT_STATE_DISCARDING;
            }

            u2Index++;
        }
    }
    else
    {
        if (AstIsMstEnabledInContext (u4ContextId))
        {
            for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES; u2MstInst++)
            {
                if (L2IWF_INSTANCE_ACTIVE (u2MstInst) == OSIX_FALSE)
                {
                    continue;
                }

                pInstPortStateInfo[u2Index].u2Inst = u2MstInst;

                if (L2IWF_PORT_ACTIVE (u2LocalPortId) == OSIX_TRUE)
                {
                    pInstPortStateInfo[u2Index].u1PortState =
                        L2IWF_INST_PORT_STATE (u2MstInst, u2LocalPortId);
                }
                else
                {
                    /* Set the port state as discarding if this port is not 
                     * active */
                    pInstPortStateInfo[u2Index].u1PortState =
                        AST_PORT_STATE_DISCARDING;
                }

                u2Index++;
            }
        }
        else if (AstIsPvrstStartedInContext (u4ContextId))
        {
            for (u2PvrstInst = 1; u2PvrstInst <= AST_MAX_PVRST_INSTANCES;
                 u2PvrstInst++)
            {
                if (L2IWF_INSTANCE_ACTIVE (u2PvrstInst) == OSIX_FALSE)
                {
                    continue;
                }

                pInstPortStateInfo[u2Index].u2Inst = u2PvrstInst;

                if (L2IWF_PORT_ACTIVE (u2LocalPortId) == OSIX_TRUE)
                {
                    pInstPortStateInfo[u2Index].u1PortState =
                        L2IWF_INST_PORT_STATE (u2PvrstInst, u2LocalPortId);
                }
                else
                {
                    /* Set the port state as discarding if this port is not 
                     * active */
                    pInstPortStateInfo[u2Index].u1PortState =
                        AST_PORT_STATE_DISCARDING;
                }

                u2Index++;
            }
        }
        else
        {
            pInstPortStateInfo[u2Index].u2Inst = RST_DEFAULT_INSTANCE;

            if (L2IWF_PORT_ACTIVE (u2LocalPortId) == OSIX_TRUE)
            {
                pInstPortStateInfo[u2Index].u1PortState =
                    L2IWF_INST_PORT_STATE (RST_DEFAULT_INSTANCE, u2LocalPortId);
            }
            else
            {
                /* Set the port state as discarding if this port is not 
                 * active */
                pInstPortStateInfo[u2Index].u1PortState =
                    AST_PORT_STATE_DISCARDING;
            }

            u2Index++;
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    *pu2Count = u2Index;

    return L2IWF_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name      : L2IwfSetPortStateCtrlOwner                           */
/*                                                                           */
/* Description        : This routine is called to set the Owner protocol for */
/*                      the given port to avoid race condition between       */
/*                      protocol, while updating same port info.             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose owner   */
/*                                  protocol to be set.                      */
/*                      u1ProtId  - Owner Protocol Id.                       */
/*                      u1OwnerStatus - L2IWF_SET - to set the given Owner Id*/
/*                                      L2IWF_RESET - to reset Owner Id.     */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Owner Protocol Id.                                   */
/*****************************************************************************/

INT4
L2IwfSetPortStateCtrlOwner (UINT4 u4IfIndex, UINT1 u1ProtId,
                            UINT1 u1OwnerStatus)
{
    tL2PortInfo        *pL2PortInfo = NULL;
    INT4                i4RetVal = L2IWF_FAILURE;

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo != NULL)
    {
        if (u1OwnerStatus == L2IWF_SET)
        {
            pL2PortInfo->u1PortOwner = u1ProtId;
        }
        else
        {
            pL2PortInfo->u1PortOwner = STP_MODULE;
        }

        i4RetVal = L2IWF_SUCCESS;
    }

    L2_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortStateCtrlOwner                           */
/*                                                                           */
/* Description        : This routine is called to get the Owner protocol of  */
/*                      the given port to avoid race condition between       */
/*                      protocol, while updating same port info.             */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose owner   */
/*                                  protocol to be returned.                 */
/*                                                                           */
/* Output(s)          : pu1OwnerProtId  - Owner Protocol Id.                 */
/*                                                                           */
/* Return Value(s)    : Owner Protocol Id.                                   */
/*****************************************************************************/

INT4
L2IwfGetPortStateCtrlOwner (UINT4 u4IfIndex, UINT1 *pu1OwnerProtId,
                            BOOL1 bLockReq)
{
    tL2PortInfo        *pL2PortInfo = NULL;
    INT4                i4RetVal = L2IWF_FAILURE;

    L2IWF_LOCK_REQ (bLockReq);

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo != NULL)
    {
        *pu1OwnerProtId = pL2PortInfo->u1PortOwner;
        i4RetVal = L2IWF_SUCCESS;
    }

    L2IWF_UNLOCK_REQ (bLockReq);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanInstMapping                              */
/*                                                                           */
/* Description        : This routine returns the Instance to which the given */
/*                      Vlan Id is mapped. It accesses the L2Iwf common      */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Mstp Instance                                        */
/*****************************************************************************/
UINT2
L2IwfGetVlanInstMapping (tVlanId VlanId)
{
    return (L2IwfMiGetVlanInstMapping (L2IWF_DEFAULT_CONTEXT, VlanId));
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanInstMapping                            */
/*                                                                           */
/* Description        : This routine returns the Instance to which the given */
/*                      Vlan Id is mapped. It accesses the L2Iwf common      */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Mstp Instance                                        */
/*****************************************************************************/
UINT2
L2IwfMiGetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId)
{
    UINT2               u2MstInst;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return 0;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID)
    {
        return 0;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return 0;
    }

    u2MstInst = L2IWF_VID_INST_MAP (VlanId);
    if (AstIsPvrstStartedInContext (u4ContextId) == AST_TRUE)
    {
        if (u2MstInst > AST_MAX_PVRST_INSTANCES)
        {
            u2MstInst = L2IWF_INIT_VAL;
        }
    }
    else if (AstIsMstEnabledInContext (u4ContextId) == AST_TRUE)
    {
        if (u2MstInst >= AST_MAX_MST_INSTANCES)
        {
            u2MstInst = L2IWF_INIT_VAL;
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return u2MstInst;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetVlanInstMapping                              */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Vlan to Instance mapping for the given VLAN in the   */
/*                      L2Iwf common database or the given context.          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                      u2MstInst - InstanceId to be set for this Vlan       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVlanInstMapping (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2MstInst)
{
    UINT2               u2OldInst = 0;
    UINT1              *pVlanList = NULL;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID)
    {
        return L2IWF_FAILURE;
    }

    if (u2MstInst == AST_TE_MSTID)
    {
        return L2IWF_SUCCESS;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if ((AstIsRstStartedInContext (u4ContextId)) ||
        (AstIsMstStartedInContext (u4ContextId)) ||
        (L2IwfIsErpsStartedInContext (u4ContextId)))
    {
        if (L2IWF_MST_INFO (u2MstInst) == NULL)
        {
            L2IWF_MST_INFO (u2MstInst) =
                (tL2MstInstInfo *)
                L2IWF_ALLOCATE_MEMBLOCK (L2IWF_MSTINFO_POOLID ());

            if (L2IWF_MST_INFO (u2MstInst) == NULL)
            {
                L2IwfReleaseContext ();
                L2_UNLOCK ();
                return L2IWF_FAILURE;
            }
            MEMSET (L2IWF_MST_INFO (u2MstInst), 0, sizeof (tL2MstInstInfo));
        }

        u2OldInst = L2IWF_VID_INST_MAP (VlanId);
        if (u2OldInst >= AST_MAX_MST_INSTANCES)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }

        if (u2OldInst != u2MstInst)
        {
            pVlanList = L2IWF_MST_VLAN_LIST (u2OldInst);

            if (pVlanList != NULL)
            {
                /* UnMap the VLAN ID from the Old instance */
                OSIX_BITLIST_RESET_BIT (pVlanList, VlanId, VLAN_LIST_SIZE);

                L2IWF_MST_FDB_COUNT (u2OldInst)--;
            }

            pVlanList = L2IWF_MST_VLAN_LIST (u2MstInst);
            if (pVlanList != NULL)
            {
                /* Map the VLAN to the new instance ID */
                OSIX_BITLIST_SET_BIT (L2IWF_MST_VLAN_LIST (u2MstInst),
                                      VlanId, VLAN_LIST_SIZE);

                L2IWF_MST_FDB_COUNT (u2MstInst)++;
            }
        }
    }

    L2IWF_VID_INST_MAP (VlanId) = u2MstInst;

    /* Calculate the Digest for MST instances using CRC32 algorithm */
    L2IwfCalculateVidDigest (u4ContextId);

    /* Send the VID Digest to LLDP for sending VID Digest TLV */
    L2IwfSendVidDigestToLLDP (u4ContextId, gpL2Context->u4VIDDigest);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfVlanToInstMappingInit                           */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Vlan to Instance mapping for the given VLAN in the   */
/*                      L2Iwf common database for the given context.         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      VlanId - VlanId for which the Instance is to be      */
/*                               obtained.                                   */
/*                      u2MstInst - InstanceId to be set.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfVlanToInstMappingInit (UINT4 u4ContextId)
{
    UINT4               u4VlanIndex;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* Initialize the instance mapping of VLAN's to zero. */
    for (u4VlanIndex = 1; u4VlanIndex <= VLAN_DEV_MAX_VLAN_ID; u4VlanIndex++)
    {
        L2IWF_VID_INST_MAP (u4VlanIndex) = 0;
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfCreateSpanningTreeInstance                      */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to indicate    */
/*                      that the given instance has been created and is      */
/*                      active in the given context.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2InstanceId - Index of the instance                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfCreateSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId)
{
    UINT2               u2VlanIndex;
    UINT1               u1IsCistCreated = L2IWF_TRUE;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }
    if (AstIsPvrstStartedInContext (u4ContextId) == AST_TRUE)
    {
        if (u2InstanceId > AST_MAX_PVRST_INSTANCES)
        {
            return L2IWF_FAILURE;
        }
    }
    else if (AstIsMstStartedInContext (u4ContextId) == AST_TRUE)
    {
        if (u2InstanceId >= AST_MAX_MST_INSTANCES)
        {
            return L2IWF_FAILURE;
        }
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* Instance to FDB mapping is required only for MSTP instances, 
     * not for PVRST instances. L2IWF FDB info is manintained for
     * maximum AST_MAX_MST_INSTANCES. Hence don't do the mapping 
     * for PVRST instances beyond AST_MAX_MST_INSTANCES */
    if (u2InstanceId < AST_MAX_MST_INSTANCES)
    {
        /* CIST will be created during context creation. Check whether the 
         * incoming instance is CIST. Update the 'u1IsCistCreated'
         * variable when entry is not created for CIST */
        if ((u2InstanceId == MST_CIST_CONTEXT) &&
            (L2IWF_MST_INFO (u2InstanceId) == NULL))
        {
            u1IsCistCreated = L2IWF_FALSE;
        }

        if (L2IWF_MST_INFO (u2InstanceId) == NULL)
        {
            L2IWF_MST_INFO (u2InstanceId) =
                (tL2MstInstInfo *)
                L2IWF_ALLOCATE_MEMBLOCK (L2IWF_MSTINFO_POOLID ());

            if (L2IWF_MST_INFO (u2InstanceId) == NULL)
            {
                L2IwfReleaseContext ();
                L2_UNLOCK ();
                return L2IWF_FAILURE;
            }
            MEMSET (L2IWF_MST_INFO (u2InstanceId), 0, sizeof (tL2MstInstInfo));
        }

        /* When context is created, all VLAN's should be mapped to CIST. 
         * Hence when CIST entry is created for the first time, set all 
         * VLAN's in the L2IWF_MST_VLAN_LIST(CIST_CONTEXT) */
        if (u1IsCistCreated == L2IWF_FALSE)
        {
            MEMSET (L2IWF_MST_VLAN_LIST (u2InstanceId), 0xff,
                    (VLAN_LIST_SIZE - 1));

            u2VlanIndex = ((VLAN_LIST_SIZE - 1) * BITS_PER_BYTE);
            for (; u2VlanIndex <= VLAN_MAX_VLAN_ID; u2VlanIndex++)
            {
                if (VLAN_IS_VLAN_ID_VALID (u2VlanIndex))
                {
                    OSIX_BITLIST_SET_BIT (L2IWF_MST_VLAN_LIST (u2InstanceId),
                                          u2VlanIndex, VLAN_LIST_SIZE);
                }
            }

            L2IWF_MST_FDB_COUNT (MST_CIST_CONTEXT) = VLAN_MAX_VLAN_ID;
        }
    }

    L2IWF_INSTANCE_ACTIVE (u2InstanceId) = OSIX_TRUE;

    /* Calculate the Digest for MST instances using CRC32 algorithm */
    L2IwfCalculateVidDigest (u4ContextId);

    /* Send the VID Digest to LLDP for sending VID Digest TLV */
    L2IwfSendVidDigestToLLDP (u4ContextId, gpL2Context->u4VIDDigest);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeleteSpanningTreeInstance                      */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to indicate    */
/*                      that the given instance has been deleted and is not  */
/*                      active in the given context.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2InstanceId - Index of the instance                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfDeleteSpanningTreeInstance (UINT4 u4ContextId, UINT2 u2InstanceId)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }
    if (AstIsPvrstStartedInContext (u4ContextId) == AST_TRUE)
    {
        if (u2InstanceId > AST_MAX_PVRST_INSTANCES)
        {
            return L2IWF_FAILURE;
        }
    }

    else if (AstIsMstStartedInContext (u4ContextId) == AST_TRUE)
    {
        if (u2InstanceId >= AST_MAX_MST_INSTANCES)
        {
            return L2IWF_FAILURE;
        }
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if ((AstIsRstStartedInContext (u4ContextId)) ||
        (AstIsMstStartedInContext (u4ContextId)))
    {
        if (u2InstanceId >= AST_MAX_MST_INSTANCES)
        {
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
        L2IWF_RELEASE_MEMBLOCK (L2IWF_MSTINFO_POOLID (),
                                L2IWF_MST_INFO (u2InstanceId));
        L2IWF_MST_INFO (u2InstanceId) = NULL;
    }

    L2IWF_INSTANCE_ACTIVE (u2InstanceId) = OSIX_FALSE;

    /* Calculate the Digest for MST instances using CRC32 algorithm */
    L2IwfCalculateVidDigest (u4ContextId);

    /* Send the VID Digest to LLDP for sending VID Digest TLV */
    L2IwfSendVidDigestToLLDP (u4ContextId, gpL2Context->u4VIDDigest);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanListInInstance                         */
/*                                                                           */
/* Description        : This routine returns the list of vlans mapped to the */
/*                      given Instance.                                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2MstInst   - InstanceId for which the list of vlans */
/*                                    mapped needs to be obtained.           */
/*                      pu1VlanList - VlanList to be returned                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Vlans Mapped                               */
/*****************************************************************************/
UINT2
L2IwfMiGetVlanListInInstance (UINT4 u4ContextId, UINT2 u2MstInst,
                              UINT1 *pu1VlanList)
{
    UINT2               u2NumVlans = 0;
    UINT2               u2VlanIndex;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return 0;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return 0;
    }

    if (gpL2Context->apL2MstInstInfo[(u2MstInst)] == NULL)
    {

        for (u2VlanIndex = 1; u2VlanIndex <= VLAN_DEV_MAX_VLAN_ID;
             u2VlanIndex++)
        {
            if ((L2IWF_VLAN_ACTIVE (u2VlanIndex) == OSIX_TRUE) &&
                (u2MstInst == L2IWF_VID_INST_MAP (u2VlanIndex)))
            {
                u2NumVlans++;
                OSIX_BITLIST_SET_BIT (pu1VlanList, u2VlanIndex,
                                      L2IWF_VLAN_LIST_SIZE);
            }
        }
    }
    else
    {
        u2NumVlans = L2IWF_MST_FDB_COUNT (u2MstInst);
        MEMCPY (pu1VlanList, L2IWF_MST_VLAN_LIST (u2MstInst), VLAN_LIST_SIZE);
    }
    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return u2NumVlans;

}

/*****************************************************************************/
/* Function Name      : L2IwfGetInstanceInfo                                 */
/*                                                                           */
/* Description        : This routine returns the instance info of the given  */
/*                      Spanning tree instance.                              */
/*                                                                           */
/* Input(s)           : u2Instance - Spanning tree Instance id, for which    */
/*                                   instance info to be returned.           */
/*                                                                           */
/* Output(s)          : pStpInstInfo - Pointer to Instance info.             */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetInstanceInfo (UINT4 u4ContextId, UINT2 u2Instance,
                      tStpInstanceInfo * pStpInstInfo)
{
    INT4                i4RetVal = L2IWF_FAILURE;
    UINT2               u2InstIndex;
    UINT2               u2VlanIndex;
    UINT2               u2NumVlans = 0;

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return i4RetVal;
    }

    switch (pStpInstInfo->u1RequestFlag)
    {
        case L2IWF_REQ_INSTANCE_STATUS:
            pStpInstInfo->u1InstanceStatus = L2IWF_INSTANCE_ACTIVE (u2Instance);

            i4RetVal = L2IWF_SUCCESS;

            break;

        case L2IWF_REQ_INSTANCE_MAP_COUNT:
            for (u2VlanIndex = 1; u2VlanIndex <= VLAN_DEV_MAX_VLAN_ID;
                 u2VlanIndex++)
            {
                if (u2Instance == L2IWF_VID_INST_MAP (u2VlanIndex))
                {
                    u2NumVlans++;
                }
            }

            pStpInstInfo->u2MappingCount = u2NumVlans;

            i4RetVal = L2IWF_SUCCESS;

            break;

        case L2IWF_REQ_NEXT_INSTANCE_ID:
            /* intentional fallthrough */
        default:                /* This section is Backward compatible */
            for (u2InstIndex = (UINT2) (u2Instance + 1);
                 u2InstIndex < AST_MAX_MST_INSTANCES; u2InstIndex++)
            {
                if (L2IWF_INSTANCE_ACTIVE (u2InstIndex) != OSIX_TRUE)
                {
                    continue;
                }

                pStpInstInfo->u2NextValidInstance = u2InstIndex;

                i4RetVal = L2IWF_SUCCESS;
            }
    }

    L2_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetFdbListInInstance                          */
/*                                                                           */
/* Description        : This routine returns the list of FID mapped to the   */
/*                      given Instance.                                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2MstInst   - InstanceId for which the list of vlans */
/*                                    mapped needs to be obtained.           */
/*                      pVlanFlushInfo  - FDB list for a instance            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Vlans Mapped                               */
/*****************************************************************************/
INT4
L2IwfMiGetFdbListInInstance (UINT4 u4ContextId, UINT2 u2MstInst,
                             tVlanFlushInfo * pVlanFlushInfo)
{

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (AstIsPvrstStartedInContext (u4ContextId))
    {
        pVlanFlushInfo->u2FdbCount = 1;
    }
    else
    {

        if (L2IWF_MST_INFO (u2MstInst) != NULL)
        {
            MEMCPY (pVlanFlushInfo->au1FdbList, L2IWF_MST_VLAN_LIST (u2MstInst),
                    VLAN_LIST_SIZE);
            pVlanFlushInfo->u2FdbCount = L2IWF_MST_FDB_COUNT (u2MstInst);
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanMapInInstance                          */
/*                                                                           */
/* Description        : This routine returns the list of vlans mapped to the */
/*                      given Instance even the if VLAN is not active        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2MstInst   - InstanceId for which the list of vlans */
/*                                    mapped needs to be obtained.           */
/*                      pu1VlanList - VlanList to be returned                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Vlans Mapped                               */
/*****************************************************************************/
UINT2
L2IwfMiGetVlanMapInInstance (UINT4 u4ContextId, UINT2 u2MstInst,
                             UINT1 *pu1VlanList)
{
    UINT2               u2NumVlans = 0;
    UINT2               u2VlanIndex = 0;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return 0;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return 0;
    }

    for (u2VlanIndex = 1; u2VlanIndex <= VLAN_DEV_MAX_VLAN_ID; u2VlanIndex++)
    {
        if ((u2MstInst == L2IWF_VID_INST_MAP (u2VlanIndex)))
        {
            u2NumVlans++;
            OSIX_BITLIST_SET_BIT (pu1VlanList, u2VlanIndex,
                                  L2IWF_VLAN_LIST_SIZE);
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return u2NumVlans;
}

 /*****************************************************************************/
/* Function Name      : L2IwfIsRapsVlanPresent                               */
/*                                                                           */
/* Description        : This routine validates whether RAPS_VLAN ID is       */
/*                      mapped to the instance.                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2MstInst   - InstanceId to which the list of vlan   */
/*                                    mapped are obtained.                   */
/*                      u2RapsVlanId  - RAPS VlanId which is mappped to the  */
/*                                    Instance.                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfIsRapsVlanPresent (UINT4 u4ContextId, UINT2 u2RapsVlanId, UINT2 u2MstInst)
{

    UINT1               bResult = 0;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_MST_INFO (u2MstInst) != NULL)
    {
        OSIX_BITLIST_IS_BIT_SET (L2IWF_MST_VLAN_LIST (u2MstInst), u2RapsVlanId,
                                 VLAN_LIST_SIZE, bResult);
        if (bResult == OSIX_TRUE)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_SUCCESS;
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_FAILURE;
}

 /*****************************************************************************/
/* Function Name      : L2IwfSetPoAddLinkProgress                            */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update      */
/*                      the status of the port link progress                 */
/*                                                                           */
/* Input(s)           : u2AggId     - Global IfIndex of the port             */
/*                      bStatus     - The status of the link progress        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfSetPoAddLinkProgress (UINT2 u2AggId, BOOL1 bStatus)
{
    if (L2IWF_PORT_CHANNEL_VALID (u2AggId) == OSIX_TRUE)
    {
        L2IWF_PORT_CHANNEL_NEED_REPROGRAM (u2AggId) = bStatus;
    }
    return;
}

 /*****************************************************************************/
/* Function Name      : L2IwfGetPoAddLinkProgress                            */
/*                                                                           */
/* Description        : This routine returns the status                      */
/*                      of the port link progress                            */
/*                                                                           */
/* Input(s)           : u2AggId     - Global IfIndex of the port             */
/*                      bStatus     - The status of the link progress        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE or L2IWF_FALSE                            */
/*****************************************************************************/
BOOL1
L2IwfGetPoAddLinkProgress (UINT2 u2AggId)
{
    if (L2IWF_PORT_CHANNEL_VALID (u2AggId) == OSIX_TRUE)
    {
        return L2IWF_PORT_CHANNEL_NEED_REPROGRAM (u2AggId);
    }
    return L2IWF_FALSE;
}

#endif /*__L2IFCTRL_C__*/
