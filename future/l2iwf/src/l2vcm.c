/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2vcm.c,v 1.2 2009/09/24 14:30:01 prabuc Exp $
 *
 * Description: This file has L2 related information from the VCM module.
 *
 *******************************************************************/

#include "l2inc.h"

/*****************************************************************************/
/* Function name      : L2IwfUpdateSispPortCtrlStatus                        */
/*                                                                           */
/* Description        : This API will be used by SISP, when SISP feature is  */
/*                      enabled / disabled on a physical or port channel     */
/*                      port. From L2IWF, SISP aware modules (STP, VLAN, and */
/*                      IGS) will be indicated about this SISP enable status.*/
/*                                                                           */
/* Input(s)           : u4IfIndex - Index of the port to be created.         */
/*                      u1Status  - SISP enable/disable status               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfUpdateSispPortCtrlStatus (UINT4 u4IfIndex, UINT1 u1Status)
{
    tL2PortInfo        *pL2PortInfo = NULL;
    UINT4               u4ContextId = L2IWF_MAX_CONTEXTS + 1;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (pL2PortInfo->u1IsSispEnabled == u1Status)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    pL2PortInfo->u1IsSispEnabled = u1Status;

    u4ContextId = pL2PortInfo->u4ContextId;

    L2_UNLOCK ();

    L2IwfHlUpdateSispStatusOnPort (u4IfIndex, u4ContextId, u1Status);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function name      : L2IwfGetSispPortCtrlStatus                           */
/*                                                                           */
/* Description        : This API will be used by Layer 2 modules to get      */
/*                      SISP enabled / disabled on a physical or port channel*/
/*                      port.                                                */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Index of the port to be created.       */
/*                      *pu1Status  - SISP enable/disable status             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfGetSispPortCtrlStatus (UINT4 u4IfIndex, UINT1 *pu1Status)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    *pu1Status = L2IWF_DISABLED;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1Status = pL2PortInfo->u1IsSispEnabled;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}
