/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2pvlan.c,v 1.5 2010/11/17 05:35:16 prabuc Exp $
 *
 * Description:This file contains routines to get/set pVlan
 *             information from the L2IWF module.                 
 *
 *******************************************************************/
#ifndef _L2_PVLAN_C_
#define _L2_PVLAN_C_

#include "l2inc.h"

PRIVATE INT4
 
 
 
 L2IwfRemSecVlanFromPrimaryVlan (tL2PvlanInfo * pPvlanInfo,
                                 tVlanId SecondaryVlanId);
PRIVATE INT4
 
 
 
 L2IwfConfigPvlanPrimaryVid (UINT4 u4ContextId, tVlanId VlanId,
                             tVlanId PrimaryVlanId);
PRIVATE INT4
 
 
 
 L2IwfCreatePvlanEntry (UINT4 u4ContextId, tVlanId VlanId, UINT1 u1PvlanType,
                        tVlanId PrimaryVlanId, tL2PvlanInfo ** pPvlanInfo);
PRIVATE INT4
 
 
 
 L2IwfCreatePvlanSecVlanEntry (tVlanId SecondaryVlanId,
                               tL2SecondaryVlanInfo ** pSecondaryVlanInfo);
PRIVATE INT4
 
 
 
 L2IwfAddPvlanSecondaryVlan (tTMO_SLL * pSecondaryVlanList,
                             tVlanId SecondaryVlanId);
PRIVATE tL2SecondaryVlanInfo *L2iwfGetPvlanSecVlanEntry (tTMO_SLL *
                                                         pSecondaryVlanList,
                                                         tVlanId
                                                         SecondaryVlanId);
PRIVATE INT4        L2IwfRBFreePvlanInfo (tRBElem * pRBElem, UINT4 u4Arg);
PRIVATE INT4
       L2IwfPvlanTableCmpEntry (tRBElem * pNodeA, tRBElem * pNodeB);
PRIVATE tL2PvlanInfo *L2IwfGetPvlanEntry (UINT4 u4ContextId, tVlanId VlanId);
/*****************************************************************************/
/* Function Name      : L2IwfConfigPvlanMapping                              */
/*                                                                           */
/* Description        : This routine is called from VLAN to configure the    */
/*                      pvlan mapping in the given context.                  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                      tVlanId     - Vlan Id as index                       */
/*                      u1PvlanType - Type of PVLAN, it can any of the       */
/*                                    following values.                      */
/*                                     - Normal                              */
/*                                     - Primary                             */
/*                                     - Isolated                            */
/*                                     - Community                           */
/*                      PrimaryVlanId     - PrimaryVlanId                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfConfigPvlanMapping (UINT4 u4ContextId, tVlanId VlanId, UINT1 u1PvlanType,
                         tVlanId PrimaryVlanId, UINT1 u1ConfigType)
{
    tL2PvlanInfo       *pPvlanInfo = NULL;

    L2_LOCK ();
    pPvlanInfo = L2IwfGetPvlanEntry (u4ContextId, VlanId);

    if (u1ConfigType == L2IWF_TYPE_CONFIG)
    {
        if (pPvlanInfo == NULL)
        {
            if (L2IwfCreatePvlanEntry (u4ContextId, VlanId, u1PvlanType,
                                       PrimaryVlanId,
                                       &pPvlanInfo) == L2IWF_FAILURE)
            {
                L2_UNLOCK ();
                return L2IWF_FAILURE;
            }
        }

        pPvlanInfo->u1VlanType = u1PvlanType;
    }
    else
    {
        /* pvlan entry for this vlan should be created previously. So if it is
         * not present return failure here. */
        if (pPvlanInfo == NULL)
        {
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
        if ((pPvlanInfo->u1VlanType == L2IWF_ISOLATED_VLAN) ||
            (pPvlanInfo->u1VlanType == L2IWF_COMMUNITY_VLAN))
        {
            if (L2IwfConfigPvlanPrimaryVid (u4ContextId, VlanId,
                                            PrimaryVlanId) == L2IWF_FAILURE)
            {
                L2_UNLOCK ();
                return L2IWF_FAILURE;
            }
        }
    }

    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfCreatePvlanEntry                                */
/*                                                                           */
/* Description        : This routine is called to create a new pvlan entry.  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                      tVlanId     - Vlan Id as index                       */
/*                      u1PvlanType - Type of PVLAN, it can any of the       */
/*                                    following values.                      */
/*                                     - Normal                              */
/*                                     - Primary                             */
/*                                     - Isolated                            */
/*                                     - Community                           */
/*                      tVlanId     - PrimaryVlanId                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
PRIVATE INT4
L2IwfCreatePvlanEntry (UINT4 u4ContextId, tVlanId VlanId, UINT1 u1PvlanType,
                       tVlanId PrimaryVlanId, tL2PvlanInfo ** ppPvlanInfo)
{
    tL2PvlanInfo       *pTempPvlanInfo = NULL;

    pTempPvlanInfo = MemAllocMemBlk (L2IWF_PVLANINFO_POOLID ());

    if (pTempPvlanInfo == NULL)
    {
        return L2IWF_FAILURE;
    }

    pTempPvlanInfo->u4ContextId = u4ContextId;
    pTempPvlanInfo->VlanId = VlanId;
    pTempPvlanInfo->u1VlanType = u1PvlanType;
    pTempPvlanInfo->PrimaryVlanId = PrimaryVlanId;

    TMO_SLL_Init (&pTempPvlanInfo->SecondaryVlanList);

    if (RBTreeAdd (gL2GlobalInfo.PvlanTable,
                   (tRBElem *) pTempPvlanInfo) == RB_FAILURE)
    {
        MemReleaseMemBlock (gL2GlobalInfo.PvlanTablePoolId,
                            (UINT1 *) pTempPvlanInfo);
        return L2IWF_FAILURE;
    }

    *ppPvlanInfo = pTempPvlanInfo;

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfCreatePvlanSecVlanEntry                         */
/*                                                                           */
/* Description        : This routine is called to create a new secondary     */
/*                      vlan entry node.                                     */
/*                                                                           */
/* Input(s)           : SecondaryVlanId - Secondary vlan id.                 */
/*                                                                           */
/* Output(s)          : pSecondaryVlanInfo - Pointer to the new node         */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
PRIVATE INT4
L2IwfCreatePvlanSecVlanEntry (tVlanId SecondaryVlanId,
                              tL2SecondaryVlanInfo ** pSecondaryVlanInfo)
{
    tL2SecondaryVlanInfo *pTempPvlanInfo = NULL;

    pTempPvlanInfo = MemAllocMemBlk (gL2GlobalInfo.PvlanSecondaryVlanPoolId);

    if (pTempPvlanInfo == NULL)
    {
        return L2IWF_FAILURE;
    }

    pTempPvlanInfo->SecondaryVlanId = SecondaryVlanId;

    *pSecondaryVlanInfo = pTempPvlanInfo;

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfAddPvlanSecondaryVlan                           */
/*                                                                           */
/* Description        : This routine is called to Add a new secondary        */
/*                      vlan entry node in to the SLL present in primary     */
/*                      vlan RBtree node.                                    */
/*                                                                           */
/* Input(s)           : pSecondaryVlanList - Head pointer of Secondary vlan  */
/*                                          List.                            */
/*                      Secondary  - Secondary vlan id to be inserted.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
PRIVATE INT4
L2IwfAddPvlanSecondaryVlan (tTMO_SLL * pSecondaryVlanList,
                            tVlanId SecondaryVlanId)
{
    tL2SecondaryVlanInfo *pPrevSecondaryVlanInfo = NULL;
    tL2SecondaryVlanInfo *pTempSecondaryVlanInfo = NULL;
    tL2SecondaryVlanInfo *pNewSecondaryVlanInfo = NULL;

    TMO_SLL_Scan (pSecondaryVlanList, pTempSecondaryVlanInfo,
                  tL2SecondaryVlanInfo *)
    {
        if (pTempSecondaryVlanInfo->SecondaryVlanId < SecondaryVlanId)
        {
            pPrevSecondaryVlanInfo = pTempSecondaryVlanInfo;
            continue;
        }

        /* Control has come here means pTempSecondaryVlanInfo->SecondaryVlanId
         * >= Given secondary vlanid. */
        if (pTempSecondaryVlanInfo->SecondaryVlanId == SecondaryVlanId)
        {
            return L2IWF_SUCCESS;
        }

        /* pTempSecondaryVlanInfo->SecondaryVlanId > SecondaryVlanId */

        if (L2IwfCreatePvlanSecVlanEntry (SecondaryVlanId,
                                          &pNewSecondaryVlanInfo)
            == L2IWF_SUCCESS)
        {
            TMO_SLL_Insert (pSecondaryVlanList,
                            &(pPrevSecondaryVlanInfo->NextNode),
                            &(pNewSecondaryVlanInfo->NextNode));
            return L2IWF_SUCCESS;
        }
        else
        {
            return L2IWF_FAILURE;
        }
    }

    if (pTempSecondaryVlanInfo == NULL)
    {
        /* This means, either of the following:
         *   - the list is empty or 
         *   - whole list is scanned and this given vlan id is greater than all
         *     present in the existing list.
         * So add this node at the last of the list.
         */
        if (L2IwfCreatePvlanSecVlanEntry (SecondaryVlanId,
                                          &pNewSecondaryVlanInfo)
            == L2IWF_SUCCESS)
        {
            TMO_SLL_Add (pSecondaryVlanList,
                         &(pNewSecondaryVlanInfo->NextNode));
        }
        else
        {
            return L2IWF_FAILURE;
        }
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2iwfGetPvlanSecVlanEntry                            */
/*                                                                           */
/* Description        : This routine is called to get the secondary          */
/*                      vlan entry node from the SLL present in primary      */
/*                      vlan RBtree node.                                    */
/*                                                                           */
/* Input(s)           : pSecondaryVlanList - Head pointer of Secondary vlan  */
/*                                          List.                            */
/*                       pSecondaryVlanInfo - Pointer to the new node        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
PRIVATE tL2SecondaryVlanInfo *
L2iwfGetPvlanSecVlanEntry (tTMO_SLL * pSecondaryVlanList,
                           tVlanId SecondaryVlanId)
{
    tL2SecondaryVlanInfo *pNextPvlanInfo = NULL;

    TMO_SLL_Scan (pSecondaryVlanList, pNextPvlanInfo, tL2SecondaryVlanInfo *)
    {
        if (pNextPvlanInfo->SecondaryVlanId == SecondaryVlanId)
        {
            return pNextPvlanInfo;
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPVlanMappingInfo                             */
/*                      This function gives the mapping between primary and  */
/*                      secondary vlan. This function will be invoked by     */
/*                      Spanning tree, GMRP, MMRP, IGS to know the type of   */
/*                      vlan and to get the primary to seconday vlan mapping */
/*                      and vice versa.                                      */
/*                                                                           */
/* Input(s)           : pL2PvlanMappingInfo - Pointer to the structure       */
/*                                            containing Private vlan mapping*/
/*                                            information. The following are */
/*                                            the fields in this structure   */
/*                                            that are input to this API.    */
/*                      u1RequestType - Type of request. It can take the     */
/*                                      following values:                    */
/*                                        1.L2IWF_VLAN_TYPE                  */
/*                                        2.L2IWF_MAPPED_VLANS               */
/*                                        3.L2IWF_NUM_MAPPED_VLANS           */
/*                                                                           */
/*                                      If L2IWF_VLAN_TYPE is passed, then   */
/*                                      pMappedVlans will not be filled, but */
/*                                      only value for pVlanType is filled.  */
/*                                                                           */
/*                                      If L2IWF_MAPPED_VLANS is passed,     */
/*                                      then both pVlanMapped Vlans and      */
/*                                      pVlanType are filled.                */
/*                                                                           */
/*                                      If L2IWF_NUM_MAPPED_VLANS is passed, */
/*                                      the the number of asscoiated vlans   */
/*                                      for the given vlan will be filled.   */
/*                      InVlanId - Id of the given Vlan.                     */
/*                                                                           */
/* Output(s)          : pL2PvlanMappingInfo - pointer to the structure       */
/*                                            containing Private vlan mapping*/
/*                                            information. The following are */
/*                                            the fields in this structure   */
/*                                            that are output to this API.   */
/*                                                                           */
/*                      pMappedVlans - If the InVlanId is a primary vlan,    */
/*                                     then this pointer points to an array  */
/*                                     containing the list of secondary vlans*/
/*                                     associated with that primary vlan.    */
/*                                                                           */
/*                                     If the InVlanId is a secondary vlan,  */
/*                                     this *pMappedVlans will contain the ID*/
/*                                     of primary vlan associated with the   */
/*                                     given secondary vlan (InVlanId).      */
/*                                                                           */
/*                                     If u1RequestTyps is                   */
/*                                     L2IWF_MAPPED_VLANS, then only this    */
/*                                     array will be filled. Otherwise this  */
/*                                     will not filled.                      */
/*                                                                           */
/*                      pu1VlanType - This gives the type of vlan of the     */
/*                                    given input vlan (InVlanId). Possible  */
/*                                    values for this parameter are:         */
/*                                      1.L2IWF_VLAN_TYPE_NORMAL,            */
/*                                      2.L2IWF_VLAN_TYPE_PRIMARY,           */
/*                                      3.L2IWF_VLAN_TYPE_ISOLATED,          */
/*                                      4.L2IWF_VLAN_TYPE_COMMUNITY          */
/*                                                                           */
/*                      pu2NumMappedVlans - Number of vlans present in the   */
/*                                          array pointed by pMappedVlans.   */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
L2IwfGetPVlanMappingInfo (tL2PvlanMappingInfo * pL2PvlanMappingInfo)
{
    tL2PvlanInfo       *pPvlanInfo = NULL;
    tL2SecondaryVlanInfo *pSecondaryVlanInfo = NULL;
    UINT2               u2Index = 0;

    L2_LOCK ();

    pPvlanInfo = L2IwfGetPvlanEntry (pL2PvlanMappingInfo->u4ContextId,
                                     pL2PvlanMappingInfo->InVlanId);

    if (pPvlanInfo == NULL)
    {
        /* Return TYPE as NORMAL */
        pL2PvlanMappingInfo->u1VlanType = L2IWF_NORMAL_VLAN;

        L2_UNLOCK ();
        return;
    }

    pL2PvlanMappingInfo->u1VlanType = pPvlanInfo->u1VlanType;

    if (pL2PvlanMappingInfo->u1RequestType == L2IWF_MAPPED_VLANS)
    {
        if (pL2PvlanMappingInfo->pMappedVlans == NULL)
        {
            L2_UNLOCK ();
            return;
        }
        if (pPvlanInfo->u1VlanType == L2IWF_PRIMARY_VLAN)
        {
            TMO_SLL_Scan (&pPvlanInfo->SecondaryVlanList, pSecondaryVlanInfo,
                          tL2SecondaryVlanInfo *)
            {
                pL2PvlanMappingInfo->pMappedVlans[u2Index] =
                    pSecondaryVlanInfo->SecondaryVlanId;

                u2Index++;

            }
            pL2PvlanMappingInfo->u2NumMappedVlans = u2Index;
        }
        else if ((pPvlanInfo->u1VlanType == L2IWF_ISOLATED_VLAN) ||
                 (pPvlanInfo->u1VlanType == L2IWF_COMMUNITY_VLAN))
        {
            *pL2PvlanMappingInfo->pMappedVlans = pPvlanInfo->PrimaryVlanId;
            if (pPvlanInfo->PrimaryVlanId != 0)
            {
                pL2PvlanMappingInfo->u2NumMappedVlans = 1;
            }
            else
            {
                pL2PvlanMappingInfo->u2NumMappedVlans = 0;
            }
        }

    }
    else if (pL2PvlanMappingInfo->u1RequestType == L2IWF_NUM_MAPPED_VLANS)
    {
        if (pPvlanInfo->u1VlanType == L2IWF_PRIMARY_VLAN)
        {
            pL2PvlanMappingInfo->u2NumMappedVlans =
                (UINT2) pPvlanInfo->SecondaryVlanList.u4_Count;
        }
        else if ((pPvlanInfo->u1VlanType == L2IWF_ISOLATED_VLAN) ||
                 (pPvlanInfo->u1VlanType == L2IWF_COMMUNITY_VLAN))
        {
            if (pPvlanInfo->PrimaryVlanId != 0)
            {
                pL2PvlanMappingInfo->u2NumMappedVlans = 1;
            }
            else
            {
                pL2PvlanMappingInfo->u2NumMappedVlans = 0;
            }
        }
    }
    L2_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfPvlanCrtMemPoolAndRBTree                        */
/*                                                                           */
/* Description        : This routine creates the mempool required for        */
/*                      storing Pvlan Mapping and creates RBTree in L2Iwf    */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPvlanCrtMemPoolAndRBTree (VOID)
{
    if (L2IWF_CREATE_MEMPOOL
        (gFsL2iwfSizingParams[L2IWF_PVLAN_TABLE].u4StructSize,
         gFsL2iwfSizingParams[L2IWF_PVLAN_TABLE].u4PreAllocatedUnits,
         MEM_DEFAULT_MEMORY_TYPE, &(L2IWF_PVLANINFO_POOLID ())) == MEM_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    /* PVLAN secondary vlan mempool */
    if (L2IWF_CREATE_MEMPOOL
        (gFsL2iwfSizingParams[L2IWF_PVLAN_SECONDARY_VLAN_TABLE].u4StructSize,
         gFsL2iwfSizingParams[L2IWF_PVLAN_SECONDARY_VLAN_TABLE].
         u4PreAllocatedUnits,
         MEM_DEFAULT_MEMORY_TYPE, &(L2IWF_PVLAN_SECONDARY_VLAN_POOLID ()))
        == MEM_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    if ((gL2GlobalInfo.PvlanTable =
         RBTreeCreateEmbedded (0, (tRBCompareFn) L2IwfPvlanTableCmpEntry))
        == NULL)
    {
        return L2IWF_FAILURE;
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPvlanDelMemPoolAndRBTree                        */
/*                                                                           */
/* Description        : This routine deletes the mempool and RBTree created  */
/*                      for maintaing Pvlan Mapping                          */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPvlanDelMemPoolAndRBTree (VOID)
{
    /* Delete the mempool for secondary vlan */
    if (L2IWF_PVLAN_SECONDARY_VLAN_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_PVLAN_SECONDARY_VLAN_POOLID ());
        L2IWF_PVLAN_SECONDARY_VLAN_POOLID () = 0;
    }

    /* Deleting the RBTree node for the PvlanTable */
    if (gL2GlobalInfo.PvlanTable != NULL)
    {
        RBTreeDestroy (gL2GlobalInfo.PvlanTable, L2IwfRBFreePvlanInfo, 0);
        gL2GlobalInfo.PortVlanTable = NULL;
    }

    /* Deleting the mempool for the pvlan table */
    if (L2IWF_PVLANINFO_POOLID () != 0)
    {
        L2IWF_DELETE_MEMPOOL (L2IWF_PVLANINFO_POOLID ());
        L2IWF_PVLANINFO_POOLID () = 0;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfRBFreePvlanInfo                                 */
/*                                                                           */
/* Description        : This routine releases the memory allocated to each   */
/*                      node of PVlan RBTree                                 */
/*                                                                           */
/* Input(s)           : pRBElem - pointer to the node to be freed            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
PRIVATE INT4
L2IwfRBFreePvlanInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        L2IWF_RELEASE_MEMBLOCK (gL2GlobalInfo.PvlanTablePoolId,
                                (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfPvlanTableCmpEntry                          */
/*                                                                           */
/*    Description         : This function is used to compare the indices of  */
/*                          two pvlan entries. The indices are context       */
/*                          id and vlan id.                                  */
/*                                                                           */
/*    Input(s)            : pNodeA,pNodeB - PVlan Entries.                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pNodeA > pNodeB                              */
/*                          -1 - pNodeA < pNodeB                             */
/*                          0 - pNodeA == PNodeB                             */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
L2IwfPvlanTableCmpEntry (tRBElem * pNodeA, tRBElem * pNodeB)
{
    tL2PvlanInfo       *pPvlanEntryA = (tL2PvlanInfo *) pNodeA;
    tL2PvlanInfo       *pPvlanEntryB = (tL2PvlanInfo *) pNodeB;

    if (pPvlanEntryA->u4ContextId != pPvlanEntryB->u4ContextId)
    {
        if (pPvlanEntryA->u4ContextId > pPvlanEntryB->u4ContextId)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    if (pPvlanEntryA->VlanId != pPvlanEntryB->VlanId)
    {
        if (pPvlanEntryA->VlanId > pPvlanEntryB->VlanId)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    return 0;
}

/*****************************************************************************/
/* Function Name        : L2IwfGetPvlanEntry                                 */
/*                                                                           */
/* Description          : Returns the pointer to the PVlanEntry for  the     */
/*                        given contex ID & Vlan ID. Lock should be taken    */
/*                        before calling this function.                      */
/*                                                                           */
/* Input (s)            : u4ContextId - Context ID value.                    */
/*                        VlanId - Vlan Id Value                             */
/*                                                                           */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : Pointer to the PValn Entry.                        */
/*****************************************************************************/
PRIVATE tL2PvlanInfo *
L2IwfGetPvlanEntry (UINT4 u4ContextId, tVlanId VlanId)
{
    tL2PvlanInfo        PvlanEntry;
    tL2PvlanInfo       *pPvlanEntry;

    MEMSET (&PvlanEntry, 0, sizeof (tL2PvlanInfo));
    PvlanEntry.u4ContextId = u4ContextId;
    PvlanEntry.VlanId = VlanId;

    pPvlanEntry = (tL2PvlanInfo *) RBTreeGet (gL2GlobalInfo.PvlanTable,
                                              (tRBElem *) & PvlanEntry);
    return pPvlanEntry;
}

/*****************************************************************************/
/* Function Name        : L2IwfDeletePvlanMapping                            */
/*                                                                           */
/* Description          : This function called by vlan module to delete the  */
/*                        pvlan entry.                                       */
/*                                                                           */
/* Input (s)            : u4ContextId - Context ID value.                    */
/*                        VlanId -  Vlan Id Value to be delete from the pvlan*/
/*                                          domain.                          */
/*                                                                           */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS or L2IWF_FAILURE.                    */
/*****************************************************************************/
INT4
L2IwfDeletePvlanMapping (UINT4 u4ContextId, tVlanId VlanId)
{
    tL2PvlanInfo       *pPvlanInfo = NULL;

    L2_LOCK ();
    pPvlanInfo = L2IwfGetPvlanEntry (u4ContextId, VlanId);

    if (pPvlanInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    if (L2IwfDeletePvlanEntry (pPvlanInfo) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2_UNLOCK ();
    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name        : L2IwfRemSecVlanFromPrimaryVlan                     */
/*                                                                           */
/* Description          : This function called to delete the secondary vlan  */
/*                        from the primary vlan.                             */
/*                                                                           */
/* Input (s)            : pPvlanInfo - Pointer to the pvlan node.           */
/*                        SecondaryVlanId - Secondary Vlan Id Value to be    */
/*                                          disassociate from the primary    */
/*                                          vlan.                            */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS or L2IWF_FAILURE.                    */
/*****************************************************************************/
PRIVATE INT4
L2IwfRemSecVlanFromPrimaryVlan (tL2PvlanInfo * pPvlanInfo,
                                tVlanId SecondaryVlanId)
{
    tL2SecondaryVlanInfo *pSecondaryVlanInfo = NULL;

    pSecondaryVlanInfo =
        L2iwfGetPvlanSecVlanEntry (&pPvlanInfo->SecondaryVlanList,
                                   SecondaryVlanId);

    if (pSecondaryVlanInfo == NULL)
    {
        return L2IWF_SUCCESS;
    }

    TMO_SLL_Delete (&pPvlanInfo->SecondaryVlanList,
                    &(pSecondaryVlanInfo->NextNode));

    MemReleaseMemBlock (gL2GlobalInfo.PvlanSecondaryVlanPoolId,
                        (UINT1 *) pSecondaryVlanInfo);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeletePvlanEntry                                */
/*                                                                           */
/* Description        : This routine is called to delete a pvlan entry.      */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                      tVlanId     - Vlan Id as index                       */
/*                      u1PvlanType - Type of PVLAN, it can any of the       */
/*                                    following values.                      */
/*                                     - Normal                              */
/*                                     - Primary                             */
/*                                     - Isolated                            */
/*                                     - Community                           */
/*                      tVlanId     - PrimaryVlanId                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfDeletePvlanEntry (tL2PvlanInfo * pPvlanInfo)
{
    if (RBTreeRemove (gL2GlobalInfo.PvlanTable,
                      (tRBElem *) pPvlanInfo) == RB_FAILURE)
    {
        return L2IWF_FAILURE;
    }
    MemReleaseMemBlock (gL2GlobalInfo.PvlanTablePoolId, (UINT1 *) pPvlanInfo);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfConfigPvlanPrimaryVid                           */
/*                                                                           */
/* Description        : This routine is called to configure the primary vlan */
/*                      id for a given secondary vlan.                       */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                      tVlanId     - Vlan Id as index                       */
/*                      PrimaryVlanId - Primary vlan id                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
PRIVATE INT4
L2IwfConfigPvlanPrimaryVid (UINT4 u4ContextId, tVlanId VlanId,
                            tVlanId PrimaryVlanId)
{
    tL2PvlanInfo       *pSecPvlanInfo = NULL;
    tL2PvlanInfo       *pPrimaryPvlanInfo = NULL;

    pSecPvlanInfo = L2IwfGetPvlanEntry (u4ContextId, VlanId);

    if (pSecPvlanInfo == NULL)
    {
        return L2IWF_FAILURE;
    }

    /* Associating a secondary vlan to a primary vlan */
    if (PrimaryVlanId != 0)
    {
        pPrimaryPvlanInfo = L2IwfGetPvlanEntry (u4ContextId, PrimaryVlanId);
        /* Already this secondary vlan is associated to a primary vlan,
         * dis-associate that first. */
        if (pSecPvlanInfo->PrimaryVlanId != 0)
        {
            if (L2IwfRemSecVlanFromPrimaryVlan (pPrimaryPvlanInfo,
                                                pSecPvlanInfo->VlanId)
                == L2IWF_FAILURE)
            {
                return L2IWF_FAILURE;
            }
        }

        /* Now associate the new primary vlan id to the secondary vlan */
        pSecPvlanInfo->PrimaryVlanId = PrimaryVlanId;

        if (pPrimaryPvlanInfo != NULL)
        {
            if (L2IwfAddPvlanSecondaryVlan
                (&pPrimaryPvlanInfo->SecondaryVlanList, VlanId)
                == L2IWF_FAILURE)
            {
                return L2IWF_FAILURE;
            }
        }
    }
    else
    {
        pPrimaryPvlanInfo = L2IwfGetPvlanEntry (u4ContextId,
                                                pSecPvlanInfo->PrimaryVlanId);
        /* Dis-associating the secondary vlan from primary vlan */
        if (L2IwfRemSecVlanFromPrimaryVlan (pPrimaryPvlanInfo,
                                            pSecPvlanInfo->VlanId)
            == L2IWF_FAILURE)
        {
            return L2IWF_FAILURE;
        }

        /* Assign 0 to the primary vlan id of the secondary vlan */
        pSecPvlanInfo->PrimaryVlanId = 0;
    }

    return L2IWF_SUCCESS;
}
#endif /*End _L2_PVLAN_C_ */
