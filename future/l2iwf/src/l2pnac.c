/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2pnac.c,v 1.15 2015/04/10 07:01:29 siva Exp $
 *
 * Description:This file contains routines to get/set PNAC
 *             information from the L2IWF module.                 
 *
 *******************************************************************/

#include "l2inc.h"

/*****************************************************************************/
/* Function Name      : L2IwfGetPortPnacAuthControl                          */
/*                                                                           */
/* Description        : This routine returns the port's Auth Control Status  */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Control status is to be obtained.        */
/*                                                                           */
/* Output(s)          : u2PortAuthControl - Port's Auth Control status       */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortPnacAuthControl (UINT4 u4IfIndex, UINT2 *pu2PortAuthControl)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    *pu2PortAuthControl = PNAC_PORTCNTRL_FORCEAUTHORIZED;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        *pu2PortAuthControl =
            L2IWF_IFENTRY_AUTH_CONTROL_PORT_CONTROL (pL2PortEntry);
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortPnacAuthControl                          */
/*                                                                           */
/* Description        : This routine is called from PNAC to update the       */
/*                      Port's Auth Control in the L2Iwf common database.    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Control status is to be updated.         */
/*                      u2PortAuthControl - Auth Control value to be updated */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetPortPnacAuthControl (UINT4 u4IfIndex, UINT2 u2PortAuthControl)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_IFENTRY_AUTH_CONTROL_PORT_CONTROL (pL2PortEntry) = u2PortAuthControl;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortPnacAuthStatus                           */
/*                                                                           */
/* Description        : This routine returns the port's Auth Status          */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Status is to be obtained.                */
/*                                                                           */
/* Output(s)          : u2PortAuthStatus  - Port's Auth status               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortPnacAuthStatus (UINT4 u4IfIndex, UINT2 *pu2PortAuthStatus)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    *pu2PortAuthStatus = PNAC_PORTSTATUS_UNAUTHORIZED;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        *pu2PortAuthStatus =
            L2IWF_IFENTRY_AUTH_CONTROL_PORT_STATUS (pL2PortEntry);
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPortAuthorized                                */
/*                                                                           */
/* Description        : This routine checks whether the port is a authorized */
/*                      or not.                                              */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfIsPortAuthorized (UINT4 u4IfIndex)
{
    UINT2               u2PortAuthStatus;

    L2IwfGetPortPnacAuthStatus (u4IfIndex, &u2PortAuthStatus);
    if (u2PortAuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
    {
        return L2IWF_SUCCESS;
    }
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortPnacControlDir                           */
/*                                                                           */
/* Description        : This routine returns the port's Control direction    */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Control */
/*                                    direction is to be obtained.           */
/*                                                                           */
/* Output(s)          : u2PortControlDir  - Port's Control dirction          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortPnacControlDir (UINT4 u4IfIndex, UINT2 *pu2PortControlDir)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    *pu2PortControlDir = PNAC_CNTRLD_DIR_BOTH;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        *pu2PortControlDir = L2IWF_IFENTRY_OPER_CONTROL_DIR (pL2PortEntry);
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortPnacControlDir                           */
/*                                                                           */
/* Description        : This routine is called from PNAC to update the       */
/*                      Port's Control direction in the L2Iwf common database*/
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Control */
/*                                  dir is to be updated.                    */
/*                      u2PortControlDir - Control Dir  value to be updated  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetPortPnacControlDir (UINT4 u4IfIndex, UINT2 u2PortControlDir)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_IFENTRY_OPER_CONTROL_DIR (pL2PortEntry) = u2PortControlDir;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortPnacAuthMode                             */
/*                                                                           */
/* Description        : This routine is called from PNAC to update the       */
/*                      Port's Auth Mode in the L2Iwf common database.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Mode  status is to be updated.           */
/*                      u2PortAuthMode - Auth Mode value to be updated       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetPortPnacAuthMode (UINT4 u4IfIndex, UINT2 u2PortAuthMode)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pL2PortEntry->u2PortAuthMode = u2PortAuthMode;
    pL2PortEntry->u2OperControlDir = PNAC_CNTRLD_DIR_BOTH;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortPnacPaeStatus                            */
/*                                                                           */
/* Description        : This routine is called from PNAC to update the       */
/*                      Port's Pae Status in the L2Iwf common database.      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Pae Status is to be updated.             */
/*                      u1PortPaeStatus - PNAC enabled/disabled status       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetPortPnacPaeStatus (UINT4 u4IfIndex, UINT1 u1PortPaeStatus)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pL2PortEntry->u1PortPaeStatus = u1PortPaeStatus;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortPnacAuthMode                             */
/*                                                                           */
/* Description        : This routine is called from CFA to get the           */
/*                      Port's Auth Mode in the L2Iwf common database.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Mode  is to be retrieved.                */
/*                                                                           */
/* Output(s)          : pu2PortAuthMode - Auth Mode value to be retrieved    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortPnacAuthMode (UINT4 u4IfIndex, UINT2 *pu2PortAuthMode)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    *pu2PortAuthMode = PNAC_PORT_AUTHMODE_PORTBASED;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu2PortAuthMode = pL2PortEntry->u2PortAuthMode;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}


/*****************************************************************************/
/* Function Name      : L2IwfGetPortPnacPaeStatus                            */
/*                                                                           */
/* Description        : This routine is called from CFA to get the           */
/*                      Port's Pae status in the L2Iwf common database.      */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose Auth    */
/*                                  Pae status is to be retrieved.           */
/*                                                                           */
/* Output(s)          : pu1PortPaeStatus - Port pnac enable/disable          */
/*                                         status value to be retrieved      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortPnacPaeStatus (UINT4 u4IfIndex, UINT1 *pu1PortPaeStatus)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    *pu1PortPaeStatus = OSIX_ENABLED;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1PortPaeStatus = pL2PortEntry->u1PortPaeStatus;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsSrcMacAuthorized                              */
/*                                                                           */
/* Description        : This routine checks whether the supplicant mac       */
/*                      is authorized for mac based authentication mode      */
/*                                                                           */
/* Input(s)           : SrcMacAddr -Source Mac Address in the received frame */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfIsSrcMacAuthorized (UINT1 *pSrcMacAddr)
{
    tL2PnacMacInfo     *pL2PnacMacEntry = NULL;
    tL2PnacMacInfo      L2PnacMacEntry;

    L2_LOCK ();

    MEMCPY (L2PnacMacEntry.SrcMacAddr, pSrcMacAddr, sizeof (tMacAddr));

    pL2PnacMacEntry = RBTreeGet (L2IWF_PNAC_MAC_TBL (), &L2PnacMacEntry);

    /* Supplicant might have logged off then pL2PnacMacEntry
     * entry will be NULL*/
    if (pL2PnacMacEntry != NULL)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    L2_UNLOCK ();

    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetSuppMacAuthStatus                            */
/*                                                                           */
/* Description        : This routine is called from PNAC to update the       */
/*                      Supplicant Mac Auth Status in the L2Iwf common       */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : SuppMacAddr - Mac Address of supplicant whose Auth   */
/*                                   Status is to be updated.                */
/*                      u2MacAuthStatus - Auth Status  value to be updated   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetSuppMacAuthStatus (tMacAddr SuppMacAddr, UINT2 u2MacAuthStatus)
{
    tL2PnacMacInfo     *pL2PnacMacEntry = NULL;
    tL2PnacMacInfo      L2PnacMacEntry;

    L2_LOCK ();

    if (u2MacAuthStatus == PNAC_PORTSTATUS_AUTHORIZED)
    {
        if ((pL2PnacMacEntry = (tL2PnacMacInfo *)
             L2IWF_ALLOCATE_MEMBLOCK (L2IWF_PNAC_MAC_SESS_POOLID ())) == NULL)
        {
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
        MEMCPY (pL2PnacMacEntry->SrcMacAddr, SuppMacAddr, sizeof (tMacAddr));
        if (RBTreeAdd (L2IWF_PNAC_MAC_TBL (), pL2PnacMacEntry) != RB_SUCCESS)
        {
            L2IWF_RELEASE_MEMBLOCK (L2IWF_PNAC_MAC_SESS_POOLID (),
                                    pL2PnacMacEntry);
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
    }
    else
    {
        MEMCPY (L2PnacMacEntry.SrcMacAddr, SuppMacAddr, sizeof (tMacAddr));
        pL2PnacMacEntry = (tL2PnacMacInfo *)
            RBTreeRem (L2IWF_PNAC_MAC_TBL (), &L2PnacMacEntry);

        if (pL2PnacMacEntry != NULL)
        {
            L2IWF_RELEASE_MEMBLOCK (L2IWF_PNAC_MAC_SESS_POOLID (),
                                    pL2PnacMacEntry);
        }
    }

    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortPnacAuthStatus                           */
/*                                                                           */
/* Description        : This routine is called from PNAC to update the       */
/*                      Port's Auth Status  in the L2Iwf common database.    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Index of the port whose Auth Status      */
/*                                    is to be updated.                      */
/*                      u2PortAuthControl - Auth Status  value to be updated */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetPortPnacAuthStatus (UINT4 u4IfIndex, UINT2 u2PortAuthStatus)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_IFENTRY_AUTH_CONTROL_PORT_STATUS (pL2PortEntry) = u2PortAuthStatus;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* Function Name      : L2PnacRedSetBridgePortOperStatus                     */
/*                                                                           */
/* Description        : This routine is used by PNAC module for updating the */
/*                      oper status to l2iwf in the standby node             */
/*                                                                           */
/* Input(s)           : u4IfIndex    - Interface index                       */
/*                    : u1OperStatus - oper status                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
L2PnacRedSetBridgePortOperStatus (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tCfaIfInfo          IfInfo;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return;
    }

#ifdef LA_WANTED
    if (IfInfo.u1IfType == CFA_ENET)
    {
        if (LaGetLaEnableStatus () == LA_ENABLED)
        {
            return;
        }
    }
#endif

    L2IwfSetBridgePortOperStatus (u4IfIndex, u1OperStatus);
}
#endif
