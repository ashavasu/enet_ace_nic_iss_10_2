/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: l2pkt.c,v 1.115.2.1 2018/05/03 11:42:43 siva Exp $
 *
 * Description:This file contains l2iwf API's
 *
 *******************************************************************/
#include "l2inc.h"

#ifdef OPENFLOW_WANTED
#include "ofcl.h"
#endif /* OPENFLOW_WANTED */

PRIVATE INT4
     
     
     
     L2IwfGetPktInfoFromPacket
PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
        tPktHandleInfo * pPktHandleInfo));

/*****************************************************************************/
/* Function Name      : L2IwfHandleIncomingLayer2Pkt                         */
/*                                                                           */
/* Description        : This API is invoked when the system receives a packet*/
/*                      meant for a Layer 2 MAC Client (Bridge, VLAN, STP,   */
/*                      GARP, IGS, LA , LLDP or PNAC. It checks if the frame */
/*                      is a Control or data packet and invokes corresponding*/
/*                      interface APIs of appropriate modules.               */
/*                                                                           */
/* Input(s)           : pBuf - Incoming packet                               */
/*                      u4IfIndex - Interface index.                         */
/*                      u1FrameType - Data frame or control frame.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfHandleIncomingLayer2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                              UINT1 u1FrameType)
{
    tPktHandleInfo      PktHandleInfo;

    MEMSET (&PktHandleInfo, 0, sizeof (tPktHandleInfo));

    /* Populate the PktHandleInfo Structure for backward compatibility */
    if (L2IwfGetPktInfoFromPacket (pBuf, u4IfIndex, &PktHandleInfo)
        == L2IWF_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2IWF_FAILURE;
    }

    return (L2IwfWrHandleIncomingLayer2Pkt (pBuf, &PktHandleInfo, u1FrameType));
}

/*****************************************************************************/
/* Function Name      : L2IwfWrHandleIncomingLayer2Pkt                       */
/*                                                                           */
/* Description        : This Wrapper function processes the packet meant for */
/*                      Layer 2 MAC Client (Bridge, VLAN, STP, GARP, IGS, LA,*/
/*                      LLDP or PNAC)                                        */
/*                                                                           */
/*                      Incoming Packet Handler Structure holds information  */
/*                      about the incoming packet so as avoid redundant      */
/*                      packet buffer lookup's. Based on the packet type, it */
/*                      invokes corresponding interface APIs of appropriate  */
/*                      modules.                                             */
/*                                                                           */
/* Input(s)           : pBuf - Incoming packet                               */
/*                      pPktHandleInfo - Information required to process the */
/*                      packet                                               */
/*                      u1FrameType - Data frame or control frame.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfWrHandleIncomingLayer2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                                tPktHandleInfo * pPktHandleInfo,
                                UINT1 u1FrameType)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tSecIntfInfo        SecIntfInfo;
    tEnetV2Header      *pEthHdr;
    tL2ProtStatus       L2ProtocolStatus;
    tVlanTag            VlanTag;
    tMacAddr            DestAddr;
    INT4                i4RetVal;
    INT4                i4BrgPortType = 0;
#if defined IGS_WANTED || defined MLDS_WANTED
    INT4                i4ResvAddr = 0;
#endif
#ifdef MRP_WANTED
    INT4                i4IsMvrpTaggedFrame = VLAN_TAGGED;
#endif
    UINT4               u4IfIndex = pPktHandleInfo->u4IfIndex;
    UINT4               u4ContextId;
    UINT4               u4LogicalIfIndex = u4IfIndex;    /* The Port/PortChannel 
                                                           Global Ifindex */
    UINT4               u4SispIfIndex = u4IfIndex;    /* SISP Port */
#ifdef ECFM_WANTED
    INT4                i4CfmTunnelResult = 0;    /*Return value for ECFM module */
#ifndef NPAPI_WANTED
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
#endif
    UINT1               u1PktType;
    UINT4               u4EcfmSharedPort = u4IfIndex;    /* For Ecfm */
    UINT1               u1TunnelStatus = 0;
    UINT1               u1L2CPProtType = 0;

#endif
    UINT4               u4BridgeMode;
    UINT4               u4TagOffset = 0;
    UINT4               u4AcIfIndex = 0;
    tVlanId             VlanId = 0;
    UINT2               u2PbPortType = CFA_INVALID_BRIDGE_PORT;

    UINT1               u1Result = 0;

    UINT1               u1EgrOption = 0;
    UINT1               u1IsSecWantedForBridgedTraffic = OSIX_FALSE;
    BOOL1               bSecVerdict = 0;
    UINT1               u1BridgedIfaceStatus = CFA_ENABLED;
#ifdef OPENFLOW_WANTED
    UINT4               u4OfcContextId = ZERO;
#endif /* OPENFLOW_WANTED */
    UINT4               u4SChIfIndex = 0;

    MEMSET (&SecIntfInfo, 0, sizeof (tSecIntfInfo));
    pEthHdr = (tEnetV2Header *) (VOID *) CRU_BMC_Get_DataPointer (pBuf);

    /*Initialise the Interface Id in the Pkt Buffer. This is done bcoz
     * L2IwfIsIgmpControlPacket is using the Interface Id in Pkt Buffer*/

    pBuf->ModuleData.InterfaceId.u4IfIndex = u4IfIndex;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        /* If the frame type is BR_IGMP_FRAME, then the buffer
         * would not have been duplicated in cfa. If L2IWF_FAILURE
         * is returned, then cfa will free the buffer. Since the
         * buffer is not duplicated and it is getting freed in
         * l2iwf, success should be returned. */
        if (u1FrameType == BRG_IGMP_FRAME)
        {
            return L2IWF_SUCCESS;
        }
        else
        {
            return L2IWF_FAILURE;
        }
    }

    /* Update the port type and Local Port Id here itself. 
     * This will be used  below.
     * */

    u2PbPortType = pL2PortEntry->u2PbPortType;

    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    L2ProtocolStatus = pL2PortEntry->ProtocolStatus;
    /* Get the bridge mode in the same lock */
    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2IWF_FAILURE;
    }

    u4BridgeMode = L2IWF_BRIDGE_MODE ();

    L2IwfReleaseContext ();

    L2_UNLOCK ();
    /* Check if the PDU received is ECFM as PNAC rule handling is taken care in
     *      * ECFM itself.
     *           */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) DestAddr, 0, ETHERNET_ADDR_SIZE);
#ifdef ECFM_WANTED
    if (L2IwfIsEcfmControlPacket (pBuf, &u1PktType) != L2IWF_FAILURE)
    {
#ifndef NPAPI_WANTED            /* NPSIM will take care of ERPS PDU forwarding */
        if (L2IwfIsErpsPacket (pBuf) != L2IWF_SUCCESS)
        {
#endif

            /* Get the outer and inner Vlan information */
            if (VlanGetVlanInfoFromFrame (u4ContextId,
                                          (UINT2) u4LogicalIfIndex,
                                          pBuf, &VlanTag,
                                          &u1EgrOption,
                                          &u4TagOffset) == VLAN_SUCCESS)
            {
                VlanId = VlanTag.OuterVlanTag.u2VlanId;
            }

            /* Get the Protocol Tunnel status based on (per port/per vlan) */
            i4CfmTunnelResult =
                L2IwfGetL2ProtocolTunnelStatus (u4ContextId, u4IfIndex, VlanId,
                                                L2_PROTO_ECFM, &u1TunnelStatus,
                                                &u1L2CPProtType);
            if ((i4CfmTunnelResult == L2IWF_SUCCESS)
                && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL))

            {
                i4CfmTunnelResult = L2_DATA_PKT;
            }
            else if ((i4CfmTunnelResult == L2IWF_SUCCESS)
                     && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                if (u1L2CPProtType == L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)

                {
                    VlanIncrRxDiscardCounters (pPktHandleInfo->u4IfIndex,
                                               L2_PROTO_ECFM);

                    i4CfmTunnelResult = L2_INVALID_PKT;
                }
                else if (u1L2CPProtType == L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
                {
                    VlanIncrRxDiscardCountersPerVlan (u4ContextId, VlanId,
                                                      L2_PROTO_ECFM);
                    i4CfmTunnelResult = L2_INVALID_PKT;
                }

            }
            else if (VlanIsEcfmTunnelPkt (u4ContextId, DestAddr) ==
                     VLAN_SUCCESS)
            {
                i4CfmTunnelResult = L2_DATA_PKT;
            }
            else
            {
                L2IwfGetSispPortInfo (pBuf, u4IfIndex, &u4EcfmSharedPort);
                EcfmHandleInFrameFromPort (pBuf, u4EcfmSharedPort);
                return L2IWF_SUCCESS;
            }

#ifndef NPAPI_WANTED            /* NPSIM will take care of ERPS PDU forwarding */
        }
        else
        {
            /* Duplicate the ERPS packet give one to ERPS (through ECFM)
               and another one to VLAN to forward */
            pCruBuf = CRU_BUF_Duplicate_BufChain (pBuf);
            if (pCruBuf == NULL)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return L2IWF_FAILURE;
            }

            L2IwfGetSispPortInfo (pCruBuf, u4IfIndex, &u4EcfmSharedPort);
            EcfmHandleInFrameFromPort (pCruBuf, u4EcfmSharedPort);
        }
#endif
        if (i4CfmTunnelResult != 0)
        {
            switch (i4CfmTunnelResult)
            {
                case L2_DATA_PKT:
                    /* May be Other than ECFM contol packet that is to be
                     * Tunneled */
                    break;

                case L2_INVALID_PKT:
                default:
                    /* This will happend in 2 cases
                       1. ECFM interface tunnle status may be Discard.
                     */
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    CFA_IF_SET_IN_DISCARD (u4IfIndex);
                    i4RetVal =
                        (u1FrameType ==
                         BRG_IGMP_FRAME) ? L2IWF_SUCCESS : L2IWF_FAILURE;
                    return i4RetVal;
            }
        }
    }
#endif

#if OPENFLOW_WANTED
    /* 
     * If the port type is LA and is configured to be used
     * for openflow hybrid L2 communication, directly send it for
     * vlan processing.
     */
    if ((u4IfIndex >= OFC_LA_MIN_IF) && (u4IfIndex <= OFC_LA_MAX_IF))
    {
        if (OfcHybridRxPktFromIssCheck (u4IfIndex, &u4OfcContextId) ==
            OFC_SUCCESS)
        {
            VlanProcessPacket (pBuf, u4IfIndex);
            return L2IWF_SUCCESS;
        }
    }
#endif /* OPENFLOW_WANTED */

#if VXLAN_WANTED

    if ((u4IfIndex >= CFA_MIN_NVE_IF_INDEX)
        && (u4IfIndex <= CFA_MAX_NVE_IF_INDEX))
    {
        VlanProcessPacket (pBuf, u4IfIndex);
        return L2IWF_SUCCESS;
    }
#endif

    /* Validate the Port using PNAC */

    /* If dot1x tunnel status is peer and pnac control frams is received,
     * then L2IwfHandleIfPnacFrame will give that pkt to pnac.*/
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgedIfaceStatus);
    if (u1BridgedIfaceStatus != CFA_DISABLED)
    {

        i4RetVal =
            L2IwfHandleIfPnacFrame (pBuf, pPktHandleInfo, u1FrameType,
                                    L2ProtocolStatus.u1Dot1xProtocolStatus);
    }
    else
    {
        i4RetVal = PNAC_DATA;
    }
    switch (i4RetVal)
    {
        case PNAC_DATA:
            /* Go on. This is a PNAC data frame and is validated */
            break;

        case PNAC_DROP:
            /* Stop here!! This port is not authorized yet */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);

            /* If the frame type is BR_IGMP_FRAME, then the buffer
             * would not have been duplicated in cfa. If L2IWF_FAILURE
             * is returned, then cfa will free the buffer. Since the
             * buffer is not duplicated and it is getting freed in
             * l2iwf, success should be returned. */
            if (u1FrameType == BRG_IGMP_FRAME)
            {
                return L2IWF_SUCCESS;
            }
            else
            {
                return L2IWF_FAILURE;
            }

        case PNAC_CONTROL:
            /* This is an EAPOL packet and is consumed by PNAC. */
            return L2IWF_SUCCESS;

        default:
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);

            /* If the frame type is BR_IGMP_FRAME, then the buffer
             * would not have been duplicated in cfa. If L2IWF_FAILURE
             * is returned, then cfa will free the buffer. Since the
             * buffer is not duplicated and it is getting freed in
             * l2iwf, success should be returned. */
            if (u1FrameType == BRG_IGMP_FRAME)
            {
                return L2IWF_SUCCESS;
            }
            else
            {
                return L2IWF_FAILURE;
            }
    }
    if (u1BridgedIfaceStatus != CFA_DISABLED)
    {
        i4RetVal = L2IwfHandleIfLldpFrame (pBuf, pPktHandleInfo, u1FrameType,
                                           L2ProtocolStatus.
                                           u1LldpProtocolStatus);
    }
    else
    {
        i4RetVal = L2_DATA_PKT;
    }

    switch (i4RetVal)
    {
        case L2_LLDP_PKT:
            /* LLDP packet is consumed by LLDP */
            return L2IWF_SUCCESS;

        case L2_DATA_PKT:
            /* May be Other than LLDP or LLDP contol packet that is to be
             * Tunneled */
            break;

        case L2_INVALID_PKT:
        default:
            /* This will happend in 2 cases
               1. LLDP interface tunnle status may be Discard.
               2. LLDP interface tunnle status is peer but LLDP is shutdown
             */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);
            i4RetVal =
                (u1FrameType == BRG_IGMP_FRAME) ? L2IWF_SUCCESS : L2IWF_FAILURE;
            return i4RetVal;
    }

    i4RetVal =
        L2IwfHandleIfLaFrame (pBuf, pPktHandleInfo, &u4LogicalIfIndex,
                              L2ProtocolStatus.u1LacpProtocolStatus,
                              u1FrameType);

    switch (i4RetVal)
    {
        case LA_DATA:
            /* Go on. This LA data frame is good to go with the 
             * aggregated port index. The applications after this needs to
             * know only logical index.
             */
            if (CFA_IS_ENET_MAC_BCAST (pEthHdr))
            {
                CFA_IF_SET_IN_BCAST (u4LogicalIfIndex);
            }
            else if (CFA_IS_ENET_MAC_MCAST (pEthHdr))
            {
                CFA_IF_SET_IN_MCAST (u4LogicalIfIndex);
            }
            else
            {
                CFA_IF_SET_IN_UCAST (u4LogicalIfIndex);
            }
            break;

        case LA_DROP:
            /* Stop here!! This LA data frame  has to be dropped */
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);

            /* If the frame type is BR_IGMP_FRAME, then the buffer
             * would not have been duplicated in cfa. If L2IWF_FAILURE
             * is returned, then cfa will free the buffer. Since the
             * buffer is not duplicated and it is getting freed in
             * l2iwf, success should be returned. */
            if (u1FrameType == BRG_IGMP_FRAME)
            {
                return L2IWF_SUCCESS;
            }
            else
            {
                return L2IWF_FAILURE;
            }

        case LA_CONTROL:
            /* This is a LACP PDU and is consumed by LA. */
            return L2IWF_SUCCESS;

        default:
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CFA_IF_SET_IN_DISCARD (u4IfIndex);

            /* If the frame type is BR_IGMP_FRAME, then the buffer
             * would not have been duplicated in cfa. If L2IWF_FAILURE
             * is returned, then cfa will free the buffer. Since the
             * buffer is not duplicated and it is getting freed in
             * l2iwf, success should be returned. */
            if (u1FrameType == BRG_IGMP_FRAME)
            {
                return L2IWF_SUCCESS;
            }
            else
            {
                return L2IWF_FAILURE;
            }
    }

    if (u4LogicalIfIndex != pPktHandleInfo->u4IfIndex)
    {
        pPktHandleInfo->u4IfIndex = u4LogicalIfIndex;
        pPktHandleInfo->u1IfType = CFA_LAGG;
    }

    L2IwfGetSispPortInfo (pBuf, u4LogicalIfIndex, &u4SispIfIndex);

    if (u4SispIfIndex != u4IfIndex)
    {
        L2_LOCK ();

        pL2PortEntry = L2IwfGetIfIndexEntry (u4SispIfIndex);

        if (pL2PortEntry == NULL)
        {
            L2_UNLOCK ();

            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

            /* If the frame type is BR_IGMP_FRAME, then the buffer
             * would not have been duplicated in cfa. If L2IWF_FAILURE
             * is returned, then cfa will free the buffer. Since the
             * buffer is not duplicated and it is getting freed in
             * l2iwf, success should be returned. */
            if (u1FrameType == BRG_IGMP_FRAME)
            {
                return L2IWF_SUCCESS;
            }
            else
            {
                return L2IWF_FAILURE;
            }
        }

        /* Update the port type and Local Port Id here itself. 
         * This will be used  below.
         * */
        u2PbPortType = pL2PortEntry->u2PbPortType;

        u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

        /* In case of LA, Physical Port will reside in Port Channel.So
         * Protocol Tunnel Status should be taken from PortChannel. */

        L2ProtocolStatus = pL2PortEntry->ProtocolStatus;

        L2_UNLOCK ();
    }

#ifdef IGS_WANTED
    if ((u1FrameType == BRG_IGMP_FRAME) &&
        (L2ProtocolStatus.u1IgmpProtocolStatus
         != VLAN_TUNNEL_PROTOCOL_DISCARD) &&
        (SnoopIsIgmpSnoopingEnabled (u4ContextId) == SNOOP_ENABLED) &&
        (CFA_IS_ENET_MAC_MCAST (pEthHdr)))
    {
        /* Enqueue IGMP Control packets to Snoop queue */
        if (L2IwfSnoopEnqueuePkt
            (pBuf, u4ContextId, u4SispIfIndex, u4IfIndex, pEthHdr,
             u2PbPortType) != L2_SNOOP_ENQ_CONSUMED)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CfaIfSetInDiscard (u4IfIndex);
        }
        return L2IWF_SUCCESS;
    }
#endif

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) DestAddr, 0, ETHERNET_ADDR_SIZE);
    i4RetVal =
        L2IwfClassifyFrame (u4ContextId, pBuf,
                            pPktHandleInfo->EnetHdr.au1DstAddr,
                            &L2ProtocolStatus,
                            u2PbPortType, u4BridgeMode,
                            pPktHandleInfo->u4ModuleId);
    /* If any of the STP or ELM module are not enabled then the packet has to 
     * be given to VLAN module*/
    switch (i4RetVal)
    {
#ifdef RSTP_WANTED
        case L2_STAP_PKT:
            /* When ISSU Maintenance Mode is in progress,
             * no protocol changes are allowed so drop all the PDU's */
            if (IssuGetMaintModeOperation () == OSIX_TRUE)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return L2IWF_SUCCESS;
            }

            i4RetVal = AstWrHandleInFrame (pBuf, u4SispIfIndex,
                                           pPktHandleInfo->u4ContextId,
                                           pPktHandleInfo->u1IfType);
            if ((i4RetVal == RST_SUCCESS) || (i4RetVal == RST_FAILURE))
            {
                return L2IWF_SUCCESS;
            }

            if (i4RetVal == RST_DATA)
            {
                /* In case of STAP module is disabled and tunnel status is peer,
                 * then the STAP packet has to be dropped */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
                return L2IWF_SUCCESS;
            }
            break;
#endif

#ifdef ELMI_WANTED
        case L2_ELMI_PKT:
            i4RetVal = ElmHandleInFrame (pBuf, (UINT2) u4LogicalIfIndex);
            if ((i4RetVal == ELM_SUCCESS) || (i4RetVal == ELM_FAILURE))
            {
                return L2IWF_SUCCESS;
            }
            break;
#endif
        default:
            break;
    }

    /* The i4RetVal returned from AstHandleInFrame or ElmHandleInFrame would be 
       L2_DATA_PKT if the STP or ELMI module is shutdown, therefore in
       that case the packet needs to be processed in VLAN.  That is why again
       check the i4RetVal using the second switch. */

    switch (i4RetVal)
    {
#ifdef GARP_WANTED
        case L2_GVRP_PKT:

            i4RetVal = VlanUnTagGvrpPacket (pBuf, u4LogicalIfIndex);

            if (i4RetVal == VLAN_SUCCESS)
            {
                GarpMessageInd (pBuf, (UINT2) u4LogicalIfIndex,
                                GARP_STAP_CIST_ID);
            }
            else
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
            }
            break;

        case L2_GMRP_PKT:

            i4RetVal = VlanIdentifyVlanIdAndUntagFrame (GARP_MODULE, pBuf,
                                                        u4SispIfIndex, &VlanId);

            if (i4RetVal == VLAN_FORWARD)
            {
                GarpMessageInd (pBuf, (UINT2) u4SispIfIndex, VlanId);
            }
            else
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
            }
            break;
#endif
#ifdef MRP_WANTED
        case L2_MVRP_PKT:
            i4RetVal = VlanIsMvrpFrameTagged (pBuf, u4LogicalIfIndex,
                                              &i4IsMvrpTaggedFrame);

            if ((i4RetVal == VLAN_SUCCESS)
                && (i4IsMvrpTaggedFrame == VLAN_UNTAGGED))
            {
                MrpApiEnqueueRxPkt (pBuf, u4LogicalIfIndex, MRP_STAP_CIST_ID);
            }
            else if ((i4RetVal == VLAN_SUCCESS) &&
                     (i4IsMvrpTaggedFrame == VLAN_TAGGED))
            {
                i4RetVal = VlanIdentifyVlanIdAndUntagFrame (MRP_MODULE, pBuf,
                                                            u4LogicalIfIndex,
                                                            &VlanId);
                if (i4RetVal == VLAN_FORWARD)
                {
                    MrpApiEnqueueRxPkt (pBuf, u4LogicalIfIndex,
                                        MRP_STAP_CIST_ID);
                }
                else
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    CfaIfSetInDiscard (u4IfIndex);
                }
            }
            else
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
            }
            break;

        case L2_MMRP_PKT:
            i4RetVal = VlanIdentifyVlanIdAndUntagFrame (MRP_MODULE, pBuf,
                                                        u4LogicalIfIndex,
                                                        &VlanId);

            if (i4RetVal == VLAN_FORWARD)
            {
                MrpApiEnqueueRxPkt (pBuf, u4LogicalIfIndex, VlanId);
            }
            else
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
            }
            break;
#endif

        case L2_DATA_PKT:
            /* L2 Data Pkts Switched on a WAN Port should be secured */
            CfaCheckSecurityForBridging (pBuf, u4IfIndex,
                                         &u1IsSecWantedForBridgedTraffic);
            if (u1IsSecWantedForBridgedTraffic == OSIX_TRUE)
            {
                SecIntfInfo.u4PhyIfIndex = u4IfIndex;
                SecIntfInfo.u4VlanIfIndex = CfaGetSecIvrIndex ();
                i4RetVal =
                    (INT4) SecProcessFrame (pBuf, &SecIntfInfo, SEC_INBOUND,
                                            &bSecVerdict);
                if ((OSIX_SUCCESS == i4RetVal) && (SEC_STOLEN == bSecVerdict))
                {
                    /* Indicates that the frame is stolen for 
                     * security  processing.*/
                    return L2IWF_SUCCESS;
                }
                if (i4RetVal == OSIX_FAILURE)
                {
                    return L2IWF_FAILURE;
                }
            }

#if defined IGS_WANTED || MLDS_WANTED
#ifdef VLAN_WANTED
            i4ResvAddr = VlanCheckReservedGroupForFwding (u4ContextId,
                                                          DestAddr);
#endif
#ifdef IGS_WANTED
            if ((i4ResvAddr == VLAN_FALSE) &&
                (SnoopIsIgmpSnoopingEnabled (u4ContextId) == SNOOP_ENABLED) &&
                (CFA_IS_ENET_MAC_MCAST (pEthHdr)))
            {
                /* Enqueue Mcast data packets to Snoop queue */
                i4RetVal = L2IwfSnoopEnqueuePkt (pBuf, u4ContextId,
                                                 u4SispIfIndex, u4IfIndex,
                                                 pEthHdr, u2PbPortType);

                if (i4RetVal != L2_SNOOP_ENQ_CONSUMED)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    CfaIfSetInDiscard (u4IfIndex);
                    return L2IWF_SUCCESS;
                }
                else
                {
                    return L2IWF_SUCCESS;
                }
            }
#endif
#ifdef MLDS_WANTED
            if ((i4ResvAddr == VLAN_FALSE) &&
                (SnoopIsMldSnoopingEnabled (u4ContextId) == SNOOP_ENABLED) &&
                (CFA_IS_ENET_IPV6_MAC_MCAST (pEthHdr)))
            {
                /* Enqueue Mcast data packets to Snoop queue */
                i4RetVal = L2IwfSnoopEnqueuePkt (pBuf, u4ContextId,
                                                 u4SispIfIndex, u4IfIndex,
                                                 pEthHdr, u2PbPortType);

                if (i4RetVal != L2_SNOOP_ENQ_CONSUMED)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    CfaIfSetInDiscard (u4IfIndex);
                    return L2IWF_SUCCESS;
                }
                else
                {
                    return L2IWF_SUCCESS;
                }
            }
#endif
#endif

            /* If DHCP Relay is enabled , Layer 2 forwarding should not happen 
               for DHCP packets .DHCP Relay will take care of forwarding the
               packets . In all other cases, irrespective of whether DHCP 
               snooping is enabled or not Layer 2 forwarding should happen
               for DHCP packets */
            if (!(DhcpIsRelayEnabled ()))
            {
                if (L2IwfIsDhcpPacket (pBuf, &u1Result, u4IfIndex) ==
                    L2IWF_SUCCESS)
                {
                    if (u1Result == OSIX_TRUE)
                    {

                        if (VlanGetVlanInfoFromFrame
                            (u4ContextId, (UINT2) u4LogicalIfIndex, pBuf,
                             &VlanTag, &u1EgrOption,
                             &u4TagOffset) == VLAN_SUCCESS)
                        {
                            if (L2IwfGetVlanPortState
                                (VlanTag.OuterVlanTag.u2VlanId,
                                 u4LogicalIfIndex) == AST_PORT_STATE_FORWARDING)
                            {
                                L2dsPortEnquesDhcpPkts (pBuf, u4LogicalIfIndex,
                                                        VlanTag);
                            }
                            else
                            {
                                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                                CfaIfSetInDiscard (u4IfIndex);
                            }
                            break;
                        }
                    }
                }
            }

            /* Data packet to be forwarded by Vlan */
#ifdef VLAN_WANTED
            /* Additional check to save time. If Module ID is not
             * known, parse the incoming buffer and identify its packet type */
            if (pPktHandleInfo->u4ModuleId == 0)
            {
                L2IwfIsIgmpControlPacket (pBuf, &u1Result);
            }

            /* In h/w IGMP CONTROL packets are forwarded on vlan member
             * ports if ip multicast is disabled. Packets are dropped 
             * if IGS is enabled and the ingress port is not a member port 
             * of the vlan. The same needs to be done when NPAPI_WANTED is not
             * defined
             */

            if ((pPktHandleInfo->u4ModuleId == PACKET_HDLR_PROTO_SNOOP_CONTROL)
                || (u1Result == OSIX_TRUE))
            {
                /* The packet is sent to member ports of the vlan if IGS is 
                 * disabled */
                if ((SnoopIsIgmpSnoopingEnabled (u4ContextId) ==
                     SNOOP_DISABLED))
                {
                    /* Modified for attachment circuit */
                    if (L2IwfGetACIfIndexFromFrame
                        (pBuf, u4IfIndex, &u4AcIfIndex) == L2IWF_SUCCESS)
                    {
                        u4SispIfIndex = u4AcIfIndex;
                    }
                    VlanProcessPacket (pBuf, u4SispIfIndex);
                }
                else
                    /* The packet is dropped as IGS is enabled and 
                     * the ingress port is not a member port of the vlan.
                     */
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    CfaIfSetInDiscard (u4IfIndex);
                }
            }
            else                /* Received Data packet */
            {
                if (L2IwfGetBridgeMode (u4ContextId,
                                        &u4BridgeMode) == L2IWF_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    CfaIfSetInDiscard (u4IfIndex);
                    break;
                }
                if ((u4BridgeMode == L2IWF_PBB_ICOMPONENT_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PBB_BCOMPONENT_BRIDGE_MODE))
                {
                    if (L2IwfPbbProcessDataPacket (pBuf, u4IfIndex,
                                                   u4ContextId,
                                                   (UINT1 *) DestAddr)
                        == L2IWF_SUCCESS)
                    {
                        return L2IWF_SUCCESS;
                    }
                    else
                    {
                        return L2IWF_FAILURE;
                    }
                }
                else
                {
                    if (L2IwfGetACIfIndexFromFrame
                        (pBuf, u4IfIndex, &u4AcIfIndex) == L2IWF_SUCCESS)
                    {
                        u4SispIfIndex = u4AcIfIndex;
                    }

                    CfaGetInterfaceBrgPortType (u4IfIndex, &i4BrgPortType);
                    if (i4BrgPortType == CFA_UPLINK_ACCESS_PORT)
                    {
                        if (VlanGetVlanInfoAndUntagFrame (pBuf, u4IfIndex,
                                                          &VlanTag) ==
                            VLAN_NO_FORWARD)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            return L2IWF_FAILURE;
                        }
                        if (VlanApiGetSChIfIndex (u4ContextId, u4IfIndex,
                                                  VlanTag.OuterVlanTag.u2VlanId,
                                                  &u4SChIfIndex) ==
                            VLAN_SUCCESS)
                        {
                            u4SispIfIndex = u4SChIfIndex;
                            VlanTag.OuterVlanTag.u2VlanId =
                                VlanTag.InnerVlanTag.u2VlanId;
                        }
                    }
                    VlanProcessPacket (pBuf, u4SispIfIndex);
                }
            }
#else
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CfaIfSetInDiscard (u4IfIndex);
#endif
            break;

        default:
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            CfaIfSetInDiscard (u4IfIndex);
            break;
    }
    UNUSED_PARAM (VlanId);
    return L2IWF_SUCCESS;
}

#if defined IGS_WANTED || MLDS_WANTED
/*****************************************************************************/
/* Function Name      : L2IwfSnoopEnqueuePkt                             */
/*                                                                           */
/* Description        : This routine is invoked when L2IWF receives an  IGMP */
/*             packet and enqueues the frame to IGS queue         */
/* Input(s)           : pBuf - Incoming packet                               */
/*                      u4ContextId- Switch Instance Identifier             */
/*                      u4IfIndex - Interface index.                 */
/*                      PEthHdr - Ethernet Header of the Rcvd Frame         */
/*                      u1IgmpProtocolStatus - Igmp Status             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfSnoopEnqueuePkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                      UINT4 u4LogicalIfIndex, UINT4 u4IfIndex,
                      tEnetV2Header * pEthHdr, UINT2 u2PbPortType)
{
    UINT1               u1IpvxFamily = 0;
    tVlanTag            VlanTag;
    UINT4               u4AcIfIndex = 0;
    UINT4               u4SChIfIndex = 0;
    UINT2               u2TagLen = 0;
#ifdef IGS_WANTED
    tMacAddr            IgsAddr = { 0x01, 0x00, 0x5e, 0x00, 0x00, 0x00 };
#endif
    UNUSED_PARAM (u4IfIndex);
    MEMSET (&VlanTag, 0, sizeof (tVlanTag));

#ifdef IGS_WANTED
    /* Check if it is a multicast non-STP non-GARP frame. 
     * If so send it to IGS after obtaining the Vlan Info and untagging
     * the frame*/
    if ((CFA_IS_ENET_MAC_MCAST (pEthHdr)) && (!CFA_IS_ENET_MAC_BCAST (pEthHdr)))
    {
        u1IpvxFamily = IPVX_ADDR_FMLY_IPV4;
        /* Snoop packets should have the destination mac of 01 00 5E */
        if (MEMCMP (pEthHdr->au1DstAddr, IgsAddr, (CFA_ENET_ADDR_LEN - 3)) != 0)
        {
            return L2_SNOOP_ENQ_FAIL;
        }
    }
#endif
#ifdef MLDS_WANTED
    if (CFA_IS_ENET_IPV6_MAC_MCAST (pEthHdr))
    {
        u1IpvxFamily = IPVX_ADDR_FMLY_IPV6;
    }

#endif

    if ((u1IpvxFamily == IPVX_ADDR_FMLY_IPV4) ||
        (u1IpvxFamily == IPVX_ADDR_FMLY_IPV6))
    {
        if (VlanGetVlanInfoAndUntagFrame (pBuf, u4LogicalIfIndex,
                                          &VlanTag) == VLAN_NO_FORWARD)
        {
            return L2_SNOOP_ENQ_FAIL;
        }

        /*Modified for attachment circuit */
        if (L2IwfGetACIfIndexFromFrame (pBuf, u4IfIndex, &u4AcIfIndex)
            == L2IWF_SUCCESS)
        {
            u4LogicalIfIndex = u4AcIfIndex;
        }

        if (VlanApiGetSChIfIndex (u4ContextId, u4IfIndex,
                                  VlanTag.OuterVlanTag.u2VlanId,
                                  &u4SChIfIndex) == VLAN_SUCCESS)
        {
            u4LogicalIfIndex = u4SChIfIndex;
            VlanTag.OuterVlanTag.u2VlanId = VlanTag.InnerVlanTag.u2VlanId;

        }

        if (L2IwfMiIsVlanMemberPort (u4ContextId, VlanTag.OuterVlanTag.u2VlanId,
                                     u4LogicalIfIndex) == OSIX_TRUE)
        {
            /*
             * IGS packets must be processed only if the reception
             * port is a member of the Vlan. 
             *
             * If the reception port is NOT a member of the Vlan,
             * then the frame must be given to VLAN module for
             * forwarding.
             */
            L2IwfUpdateIgmpTagInfo (pBuf, u2PbPortType, &VlanTag, &u2TagLen);

            SnoopEnqueueMulticastFrame (pBuf, (UINT2) u4LogicalIfIndex, VlanTag,
                                        u2TagLen, u1IpvxFamily);
            return L2_SNOOP_ENQ_CONSUMED;
        }
    }
    return L2_SNOOP_ENQ_CONTINUE;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfHandleOutgoingPktOnPort               */
/*                                                                           */
/*    Description               : This API is used by L2 modules to transmit */
/*                                packets on a set of port through CFA.      */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                u4IfIndex - ifIndex of the driver port.    */
/*                                u4PktSize - Size of the data buffer        */
/*                                u2Protocol - Protocol type                 */
/*                                u1EncapType - Encapsulation Type           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                              UINT4 u4PktSize, UINT2 u2Protocol,
                              UINT1 u1EncapType)
{
    /*  HITLESS RESTART */
    INT4                i4RetVal = L2IWF_FAILURE;
    UINT4               u4ApsFlag = APS_INVALID;

    if (L2IWF_HR_STATUS () != L2IWF_HR_STATUS_DISABLE)
    {
        /* Find if the Packet is an APS Steady State Packet */
        if (L2IwfIsApsSteadyStatePacket (pBuf, &u4ApsFlag) == L2IWF_SUCCESS)
        {
            i4RetVal = L2IwfSendApsSteadyStatePkt (pBuf, u4IfIndex,
                                                   u4PktSize, u4ApsFlag);
            return i4RetVal;
        }
    }

#if OPENFLOW_WANTED
    /* 
     * If the port type is LA and is configured to be used
     * for openflow hybrid L2 communication, directly send it for
     * openflow processing.
     */
    if (u4IfIndex >= OFC_LA_MIN_IF && u4IfIndex <= OFC_LA_MAX_IF)
    {
        if (OfcHandleOpenflowSwModeProcess (&u4IfIndex, pBuf) == OFC_SUCCESS)
        {
            if (OfcHybridHandlePktFromIss (u4IfIndex, pBuf) == OFC_SUCCESS)
            {
                return L2IWF_SUCCESS;
            }
            return L2IWF_FAILURE;
        }
    }
#endif /* OPENFLOW_WANTED */

    if (CfaPostPktFromL2 (pBuf, u4IfIndex, u4PktSize, u2Protocol,
                          u1EncapType) == CFA_SUCCESS)
    {
        return L2IWF_SUCCESS;
    }

    return L2IWF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfHandleOutgoingPktOnPortList           */
/*                                                                           */
/*    Description               : This API is used by L2 modules to transmit */
/*                                packets on a port through CFA.             */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                PortList  - Bitlist of ports on which      */
/*                                            frame is to be transmitted.    */
/*                                u4PktSize - Size of the data buffer        */
/*                                u2Protocol - Protocol type                 */
/*                                u1EncapType - Encapsulation Type           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfHandleOutgoingPktOnPortList (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  tPortList PortList,
                                  UINT4 u4PktSize, UINT2 u2Protocol,
                                  UINT1 u1EncapType)
{
    if (CfaPostPktFromL2OnPortList (pBuf, PortList, u4PktSize, u2Protocol,
                                    u1EncapType) == CFA_SUCCESS)
    {
        return L2IWF_SUCCESS;
    }

    return L2IWF_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/* Function Name  : L2IwfPreprocessOutgoingFrameFromCfa ()                  */
/*                                                                          */
/* Description    : This function is called by CFA. If the interface is a   */
/*                  Port-Channel interface, then the potential physical     */
/*                  port is obtained. The neccessary PNAC validation is     */
/*                  also done.                                              */
/*                                                                          */
/* Input(s)       : pBuf        - Pointer to the Frame                      */
/*                  u4IfIndex   - Interface Index                           */
/*                  u4PktLen    - Length of the Frame                       */
/*                  u2Protocol  - Protocol type                             */
/*                  u1EncapType - Encapsulation Type                        */
/*                                                                          */
/* Output(s)      : pu4EgressPort - Actual egress port on which the frame is*/
/*                                to be sent                                */
/*                                                                          */
/* Returns        : L2IWF_SUCCESS or L2IWF_FAILURE.                         */
/*                                                                          */
/****************************************************************************/
INT4
L2IwfPreprocessOutgoingFrameFromCfa (tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 u4PktLen, UINT4 u4IfIndex,
                                     UINT2 u2Protocol, UINT1 u1EncapType,
                                     UINT4 *pu4EgressPort)
{
    UINT1               u1IfType;

    if (CfaGetIfaceType (u4IfIndex, &u1IfType) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    return (L2IwfWrPreprocessOutgoingFrameFromCfa (pBuf, u4PktLen, u4IfIndex,
                                                   u2Protocol, u1EncapType,
                                                   pu4EgressPort, u1IfType));
}

/****************************************************************************/
/*                                                                          */
/* Function Name  : L2IwfWrPreprocessOutgoingFrameFromCfa ()                */
/*                                                                          */
/* Description    : This function is called by CFA. If the interface is a   */
/*                  Port-Channel interface, then the potential physical     */
/*                  port is obtained. The neccessary PNAC validation is     */
/*                  also done.                                              */
/*                                                                          */
/* Input(s)       : pBuf        - Pointer to the Frame                      */
/*                  u4IfIndex   - Interface Index                           */
/*                  u4PktLen    - Length of the Frame                       */
/*                  u2Protocol  - Protocol type                             */
/*                  u1EncapType - Encapsulation Type                        */
/*                  u1IfType    - Interface type                            */
/*                                                                          */
/* Output(s)      : pu4EgressPort - Actual egress port on which the frame is*/
/*                                to be sent                                */
/*                                                                          */
/* Returns        : L2IWF_SUCCESS or L2IWF_FAILURE.                         */
/*                                                                          */
/****************************************************************************/
INT4
L2IwfWrPreprocessOutgoingFrameFromCfa (tCRU_BUF_CHAIN_HEADER * pBuf,
                                       UINT4 u4PktLen, UINT4 u4IfIndex,
                                       UINT2 u2Protocol, UINT1 u1EncapType,
                                       UINT4 *pu4EgressPort, UINT1 u1IfType)
{
    tMacAddr            DestAddr;
    UINT4               u4PhyIfIndex = 0;
#ifdef LA_WANTED
    UINT2               u2Port;
#endif
    UINT1               u1Status = CFA_DISABLED;
    UNUSED_PARAM (u4PktLen);
    UNUSED_PARAM (u1EncapType);
    UNUSED_PARAM (u2Protocol);

    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1Status);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) DestAddr, 0, CFA_ENET_ADDR_LEN);

    *pu4EgressPort = u4IfIndex;

    /* For SISP ports, L2 modules will call with logical port, that has to be
     * converted to Physical port, before passing to the CFA module */
    if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_TRUE)
    {
        VcmSispGetPhysicalPortOfSispPort (u4IfIndex, &u4PhyIfIndex);

        u4IfIndex = u4PhyIfIndex;
        *pu4EgressPort = u4PhyIfIndex;
    }

    if (u2Protocol == CFA_DATA)
    {
        /* This needs to be treated as a Data Packet. Hence check for the port
         * state. Also, it is assumed that only Control packets that do not 
         * have a valid VLAN associated come here. Hence, check is not done
         * for instance based Port State.
         * */
        if (L2IwfGetInstPortState (MST_CIST_CONTEXT, u4IfIndex) ==
            AST_PORT_STATE_DISCARDING)
        {
            return L2IWF_FAILURE;
        }
    }

    switch (u1IfType)
    {
#ifdef LA_WANTED
        case CFA_LAGG:
            /* This check is done as LLDP frame need to be sent out through
             * all the physical ports even if it is a member of the port-channel
             */
            if (u2Protocol != LLDP_ENET_TYPE)
            {
                if (LaHandleOutgoingFrame (pBuf, (UINT2) u4IfIndex, u2Protocol,
                                           u1EncapType, &u2Port) == LA_FAILURE)
                {
                    return L2IWF_FAILURE;
                }
                u4IfIndex = u2Port;
                *pu4EgressPort = (UINT4) u2Port;
            }
#endif
            /* Intentional Fall Through */
#ifdef PNAC_WANTED
        case CFA_ENET:
            if (u1Status == CFA_ENABLED)
            {

                if (PnacHandleOutFrame (pBuf, (UINT2) u4IfIndex, DestAddr,
                                        u4PktLen, u2Protocol, u1EncapType)
                    == PNAC_FAILURE)
                {
                    return L2IWF_FAILURE;
                }
            }
            break;
#endif /* PNAC_WANTED */

        default:
            break;
    }

    return L2IWF_SUCCESS;
}

/***************************************************************************/
/* Function Name    : L2IwfClassifyFrame                                   */
/*                                                                         */
/* Description      : Classifies the received frame based on its           */
/*                    destination address, whether the port is a tunnel    */
/*                    port or not, whether GVRP/ GMRP is enabled or not    */
/*                    and based on the control/data frame classification   */
/*                    provided by CFA.                                     */
/*                                                                         */
/* Input(s)         : u4ContextId - Context ID                             */
/*                    u4IfIndex - Global IfIndex of the port on which the  */
/*                                frame was received.                      */
/*                    DestAddr - Destination Mac Addres of the packet.     */
/*                    pL2ProtStatus - Pointer to Port ProtocolTunnelStatus.*/
/*                    u2PbPortType - Pb Port Type.                         */
/*                                                                         */
/* Output(s)        : None                                                 */
/*                                                                         */
/* Return Value(s)  : L2_STAP_PKT/L2_GVRP_PKT/L2_GMRP_PKT/L2_DATA_PKT/     */
/*                    L2_INVALID_PKT                                       */
/***************************************************************************/
INT4
L2IwfClassifyFrame (UINT4 u4ContextId, tCRU_BUF_CHAIN_DESC * pFrame,
                    tMacAddr DestAddr, tL2ProtStatus * pL2ProtStatus,
                    UINT2 u2PbPortType, UINT4 u4BridgeMode, UINT4 u4ModuleId)
{
    UINT4               u4IfIndex;
    UINT4               u4TagOffset = 0;
    tVlanId             VlanId = 0;
    tVlanTag            VlanTag;
    INT4                i4RetVal;
    static tMacAddr     BrgResvAddr = { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x0F };
#ifdef PVRST_WANTED
    /* ISL Destination Addresses */
    static tMacAddr     CiscoISLAddr1 = { 0x01, 0x00, 0x0C, 0x00, 0x00 };
    static tMacAddr     CiscoISLAddr2 = { 0x03, 0x00, 0x0C, 0x00, 0x00 };

    /* Dot1q Destination Address */
    static tMacAddr     Dot1qAddr = { 0x01, 0x00, 0x0C, 0xCC, 0xCC, 0xCD };
#endif
    UINT1               u1TunnelStatus;
    UINT1               u1InterfaceType;
    UINT1               u1Result;
    UINT1               u1L2CPProtType = 0;
    UINT1               u1EgrOption = 0;

    UINT4               u4VlanOffset = VLAN_TAG_OFFSET;
    UINT2               u2EnetType = 0;
    UINT2               u2AggId = 0;
    UINT2               u2LocalPort = 0;

    u4IfIndex = pFrame->ModuleData.InterfaceId.u4IfIndex;

    /* If the port belongs to port-channel, then get the port-channel id.
     * If the port is not part of any port-channel, then u2AggId will be
     * same as the give u4IfIndex. */
    if (L2IwfGetPortChannelForPort ((UINT2) u4IfIndex, &u2AggId) ==
        L2IWF_SUCCESS)
    {
        u4IfIndex = u2AggId;
    }

    VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPort);
    if (VlanGetVlanInfoFromFrame (u4ContextId,
                                  u2LocalPort,
                                  pFrame, &VlanTag,
                                  &u1EgrOption, &u4TagOffset) == VLAN_SUCCESS)
    {
        VlanId = VlanTag.OuterVlanTag.u2VlanId;
    }

    if (u4ModuleId != 0)
    {
        switch (u4ModuleId)
        {
#ifdef RSTP_WANTED
            case PACKET_HDLR_PROTO_RSTP_BRG:
            {
                /* Customer STP BPDU */
                u1TunnelStatus = pL2ProtStatus->u1StpProtocolStatus;

                if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
                {
                    return L2_STAP_PKT;
                }
                if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    return L2_DATA_PKT;
                }
#ifdef VLAN_WANTED
                if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                {
                    VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_STP);
                    return L2_INVALID_PKT;
                }
#endif
                return L2_INVALID_PKT;
            }
#endif

#ifdef PVRST_WANTED
            case PACKET_HDLR_PROTO_PVRST_CISCO_ISL1:
            case PACKET_HDLR_PROTO_PVRST_CISCO_ISL2:
            {
                if ((MEMCMP (DestAddr, CiscoISLAddr1, ETHERNET_ADDR_SIZE - 1) ==
                     0)
                    || (MEMCMP (DestAddr, CiscoISLAddr2, ETHERNET_ADDR_SIZE - 1)
                        == 0))
                {
                    CRU_BUF_Copy_FromBufChain (pFrame, (UINT1 *) DestAddr,
                                               ISL_HEADER_SIZE,
                                               ETHERNET_ADDR_SIZE);
                    if (MEMCMP (DestAddr, BrgResvAddr, (ETHERNET_ADDR_SIZE - 1))
                        != 0)
                    {
                        return L2_DATA_PKT;
                    }
                }
            }
                /* Intentional Fall through. The above is an additional check for ISL
                 * packets*/
            case PACKET_HDLR_PROTO_PVRST_DOT1Q:
            {
                /* Customer STP BPDU */
                u1TunnelStatus = pL2ProtStatus->u1StpProtocolStatus;

                if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
                {
                    /* Checking wether PVRST is enabled in the interface */
                    /* if disabled */
                    if (AstIsPvrstEnabledInContext (u4ContextId) == AST_FALSE)
                    {
                        return L2_DATA_PKT;
                    }
                    else
                    {
                        return L2_STAP_PKT;
                    }
                }
                if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                {
                    return L2_DATA_PKT;
                }
                return L2_INVALID_PKT;
            }
#endif
            case PACKET_HDLR_PROTO_VRP:
            {
                /*
                 * Customer MVRP/GVRP packets.
                 * Handle the MVRP/GVRP packets based on the tunnel
                 * protocol status configured for this protocol on this port.
                 */

                /* Get the Protocol Tunnel status based on (per port/per vlan) */
                if (MrpApiIsMrpStarted (u4ContextId) == OSIX_TRUE)
                {
                    i4RetVal =
                        L2IwfGetL2ProtocolTunnelStatus (u4ContextId, u4IfIndex,
                                                        VlanId, L2_PROTO_MVRP,
                                                        &u1TunnelStatus,
                                                        &u1L2CPProtType);
                }
                else
                {
                    i4RetVal =
                        L2IwfGetL2ProtocolTunnelStatus (u4ContextId, u4IfIndex,
                                                        VlanId, L2_PROTO_GVRP,
                                                        &u1TunnelStatus,
                                                        &u1L2CPProtType);
                }
                if ((i4RetVal == L2IWF_SUCCESS)
                    && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL))
                {
                    return L2_DATA_PKT;
                }
#ifdef VLAN_WANTED
                else if ((i4RetVal == L2IWF_SUCCESS)
                         && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
                {
                    if (MrpApiIsMvrpEnabled (u4ContextId) == OSIX_TRUE)
                    {
                        if (u1L2CPProtType ==
                            L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)
                        {
                            VlanIncrRxDiscardCounters (u4IfIndex,
                                                       L2_PROTO_MVRP);
                            return L2_INVALID_PKT;
                        }
                        else if (u1L2CPProtType ==
                                 L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
                        {
                            VlanIncrRxDiscardCountersPerVlan (u4ContextId,
                                                              VlanId,
                                                              L2_PROTO_MVRP);
                            return L2_INVALID_PKT;
                        }
                    }
                    else
                    {
                        if (u1L2CPProtType ==
                            L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)
                        {
                            VlanIncrRxDiscardCounters (u4IfIndex,
                                                       L2_PROTO_GVRP);
                            return L2_INVALID_PKT;
                        }
                        else if (u1L2CPProtType ==
                                 L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
                        {
                            VlanIncrRxDiscardCountersPerVlan (u4ContextId,
                                                              VlanId,
                                                              L2_PROTO_GVRP);
                            return L2_INVALID_PKT;
                        }
                    }
                }
#endif
                else
                {
                    if (MrpApiIsMvrpEnabled (u4ContextId) == OSIX_TRUE)
                    {
                        return L2_MVRP_PKT;
                    }
#ifdef GARP_WANTED
                    if (GvrpIsGvrpEnabledInContext (u4ContextId) == GVRP_TRUE)
                    {
                        return L2_GVRP_PKT;
                    }
#endif
                    return L2_DATA_PKT;
                }

                return L2_INVALID_PKT;
            }

            case PACKET_HDLR_PROTO_MRP:
            {
                /*
                 * Customer MMRP/GMRP packets.
                 * Handle the MMRP/GMRP packets based on the tunnel protocol status
                 * configured for this protocol on this port.
                 */
                /* Get the Protocol Tunnel status based on (per port/per vlan) */
                if (MrpApiIsMrpStarted (u4ContextId) == OSIX_TRUE)
                {
                    i4RetVal =
                        L2IwfGetL2ProtocolTunnelStatus (u4ContextId, u4IfIndex,
                                                        VlanId, L2_PROTO_MMRP,
                                                        &u1TunnelStatus,
                                                        &u1L2CPProtType);
                }
                else
                {
                    i4RetVal =
                        L2IwfGetL2ProtocolTunnelStatus (u4ContextId, u4IfIndex,
                                                        VlanId, L2_PROTO_GMRP,
                                                        &u1TunnelStatus,
                                                        &u1L2CPProtType);
                }
                if ((i4RetVal == L2IWF_SUCCESS)
                    && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL))
                {
                    return L2_DATA_PKT;
                }
#ifdef VLAN_WANTED
                else if ((i4RetVal == L2IWF_SUCCESS)
                         && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
                {
                    if (MrpApiIsMmrpEnabled (u4ContextId) == OSIX_TRUE)
                    {
                        if (u1L2CPProtType ==
                            L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)
                        {
                            VlanIncrRxDiscardCounters (u4IfIndex,
                                                       L2_PROTO_MMRP);
                            return L2_INVALID_PKT;
                        }
                        else if (u1L2CPProtType ==
                                 L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
                        {
                            VlanIncrRxDiscardCountersPerVlan (u4ContextId,
                                                              VlanId,
                                                              L2_PROTO_MMRP);
                            return L2_INVALID_PKT;
                        }
                    }
                    else
                    {
                        if (u1L2CPProtType ==
                            L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)
                        {
                            VlanIncrRxDiscardCounters (u4IfIndex,
                                                       L2_PROTO_GMRP);
                            return L2_INVALID_PKT;
                        }
                        else if (u1L2CPProtType ==
                                 L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
                        {
                            VlanIncrRxDiscardCountersPerVlan (u4ContextId,
                                                              VlanId,
                                                              L2_PROTO_GMRP);
                            return L2_INVALID_PKT;
                        }
                    }
                }
#endif
                else
                {
                    if (MrpApiIsMmrpEnabled (u4ContextId) == OSIX_TRUE)
                    {
                        return L2_MMRP_PKT;
                    }
#ifdef GARP_WANTED
                    if (GmrpIsGmrpEnabledInContext (u4ContextId) == GMRP_TRUE)
                    {
                        return L2_GMRP_PKT;
                    }
#endif
                    return L2_DATA_PKT;
                }
                return L2_INVALID_PKT;
            }

            case PACKET_HDLR_PROTO_PROVIDER_MVRP:
            {

                if ((u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE))
                {
                    /* Provider MVRP/GVRP address.
                     * Should be processed on CNPs and PNPs. Dropped on 
                     * other ports. Give it to MVRP/GVRP, this will take care of 
                     * processing or dropping the packets based on port types.
                     */
                    if (MrpApiIsMvrpEnabled (u4ContextId) == OSIX_TRUE)
                    {
                        return L2_MVRP_PKT;
                    }
#ifdef GARP_WANTED
                    if (GvrpIsGvrpEnabledInContext (u4ContextId) == GVRP_TRUE)
                    {
                        return L2_GVRP_PKT;
                    }
#endif
                    return L2_DATA_PKT;
                }
                else if ((u4BridgeMode == L2IWF_PBB_ICOMPONENT_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PBB_BCOMPONENT_BRIDGE_MODE))
                {
                    /* Provider MVRP address.
                     * Should be processed on CBPs, VIPs and PNPs. Dropped on 
                     * other ports. Give it to MVRP, this will take care of 
                     * processing or dropping the packets based on port types.
                     */
                    if (MrpApiIsMvrpEnabled (u4ContextId) == OSIX_TRUE)
                    {
                        return L2_MVRP_PKT;
                    }
                    return L2_DATA_PKT;
                }
            }
                break;

            case PACKET_HDLR_PROTO_PROVIDER_STP:
            {

                if ((u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE) ||
                    (u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE))
                {
                    /* Provider STP address - must be dropped in case of 
                     * CEP and PPNP ports.
                     */
                    if ((u2PbPortType == L2IWF_CUSTOMER_EDGE_PORT)
                        || (u2PbPortType == L2IWF_PROP_PROVIDER_NETWORK_PORT))
                    {
                        return L2_INVALID_PKT;
                    }
                    return L2_STAP_PKT;
                }
                else if ((u4BridgeMode == L2IWF_PBB_ICOMPONENT_BRIDGE_MODE) ||
                         (u4BridgeMode == L2IWF_PBB_BCOMPONENT_BRIDGE_MODE))
                {
                    /* Provider STP address - must be dropped in case of 
                     * CEP and PPNP ports.
                     */
                    if (L2IwfGetInterfaceType (u4ContextId,
                                               &u1InterfaceType) ==
                        L2IWF_FAILURE)
                    {
                        return L2_INVALID_PKT;
                    }
                    if ((u2PbPortType == L2IWF_CNP_STAGGED_PORT)
                        && (u1InterfaceType == L2IWF_C_INTERFACE_TYPE))
                    {
                        return L2_INVALID_PKT;
                    }
                    return L2_STAP_PKT;

                }
            }
                break;

#ifdef IGS_WANTED
            case PACKET_HDLR_PROTO_SNOOP_CONTROL:
            {
                i4RetVal =
                    L2IwfGetL2ProtocolTunnelStatus (u4ContextId, u4IfIndex,
                                                    VlanId, L2_PROTO_IGMP,
                                                    &u1TunnelStatus,
                                                    &u1L2CPProtType);
                if ((i4RetVal == L2IWF_SUCCESS)
                    && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL))
                {
                    return L2_DATA_PKT;
                }
#ifdef VLAN_WANTED
                else if ((i4RetVal == L2IWF_SUCCESS)
                         && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
                {
                    if (u1L2CPProtType == L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)
                    {
                        VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_IGMP);
                        return L2_INVALID_PKT;
                    }
                    else if (u1L2CPProtType ==
                             L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
                    {
                        VlanIncrRxDiscardCountersPerVlan (u4ContextId, VlanId,
                                                          L2_PROTO_IGMP);
                        return L2_INVALID_PKT;
                    }
                }
#endif
                else
                {
                    return L2_DATA_PKT;
                }
                return L2_INVALID_PKT;

            }

#endif

            default:
                return L2_DATA_PKT;
        }                        /* Switch u4Moduleid */

    }

    /* Module Id is 0. We need to determine the packet from the content */

#ifdef PVRST_WANTED
    if (AstIsPvrstEnabledInContext (u4ContextId) == AST_TRUE)
    {
        if ((MEMCMP (DestAddr, CiscoISLAddr1, ETHERNET_ADDR_SIZE - 1) == 0)
            || (MEMCMP (DestAddr, CiscoISLAddr2, ETHERNET_ADDR_SIZE - 1) == 0)
            || (MEMCMP (DestAddr, Dot1qAddr, ETHERNET_ADDR_SIZE) == 0))
        {
            /* Only ISL PDUs should be checked for Ethernet Header Type
             * If Ethernet Header is not found after ISL header then treat
             * the packet as Data Packet */
            if ((MEMCMP (DestAddr, CiscoISLAddr1, ETHERNET_ADDR_SIZE - 1) == 0)
                || (MEMCMP (DestAddr, CiscoISLAddr2, ETHERNET_ADDR_SIZE - 1)
                    == 0))
            {
                CRU_BUF_Copy_FromBufChain (pFrame, (UINT1 *) DestAddr,
                                           ISL_HEADER_SIZE, ETHERNET_ADDR_SIZE);
                if (MEMCMP (DestAddr, BrgResvAddr, (ETHERNET_ADDR_SIZE - 1))
                    != 0)
                {
                    return L2_DATA_PKT;
                }
            }

            /* Customer STP BPDU */
            u1TunnelStatus = pL2ProtStatus->u1StpProtocolStatus;

            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
            {
                return L2_STAP_PKT;
            }
            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                return L2_DATA_PKT;
            }
            return L2_INVALID_PKT;
        }
    }
#endif

    if (MEMCMP (DestAddr, BrgResvAddr, (ETHERNET_ADDR_SIZE - 1)) == 0)
    {

        if ((DestAddr[ETHERNET_ADDR_SIZE - 1] == 0x00) &&
            (u4BridgeMode != L2IWF_PBB_BCOMPONENT_BRIDGE_MODE))
        {
            /* Customer STP BPDU will always be considered as data
             * packet on PNP and CNP-Stagged Ports. 
             * */
            if ((u2PbPortType == L2IWF_PROVIDER_NETWORK_PORT) ||
                (u2PbPortType == L2IWF_CNP_STAGGED_PORT))
            {
                return L2_DATA_PKT;
            }

            /* Customer STP BPDU */
            u1TunnelStatus = pL2ProtStatus->u1StpProtocolStatus;

            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
            {
                return L2_STAP_PKT;
            }
            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                return L2_DATA_PKT;
            }
#ifdef VLAN_WANTED
            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
            {
                VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_STP);
                return L2_INVALID_PKT;
            }
#endif
            return L2_INVALID_PKT;
        }

        if (DestAddr[ETHERNET_ADDR_SIZE - 1] == 0x07)
        {
            /* ELMI packet is received. Check for the interface tunnle status */
            u1TunnelStatus = pL2ProtStatus->u1ElmiProtocolStatus;

            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
            {
                return L2_ELMI_PKT;
            }
            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
            {
                return L2_DATA_PKT;
            }
            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
            {
                VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_ELMI);
                return L2_INVALID_PKT;
            }
        }

        if ((DestAddr[ETHERNET_ADDR_SIZE - 1] == L2_MVRP_PROTOCOL_ID) ||
            (DestAddr[ETHERNET_ADDR_SIZE - 1] == L2_GVRP_PROTOCOL_ID))
        {
/* Customer Gvrp packets will always be considered as data
             * packet on PNP and CNP-Stagged Ports. 
             * */
            if ((u2PbPortType == L2IWF_PROVIDER_NETWORK_PORT) ||
                (u2PbPortType == L2IWF_CNP_STAGGED_PORT))
            {
                return L2_DATA_PKT;
            }
            /* 
             * Customer MVRP/GVRP packets.
             * Handle the MVRP/GVRP packets based on the tunnel 
             * protocol status configured for this protocol on this port.
             */
            VlanGetTagLenInFrame (pFrame, (UINT2) u4IfIndex, &u4VlanOffset);
            CRU_BUF_Copy_FromBufChain (pFrame, (UINT1 *) &u2EnetType,
                                       u4VlanOffset, VLAN_TYPE_OR_LEN_SIZE);

            u2EnetType = (UINT2) OSIX_NTOHS (u2EnetType);

            if (u2EnetType == L2_MVRP_ENET_TYPE)
            {
                i4RetVal =
                    L2IwfGetL2ProtocolTunnelStatus (u4ContextId, u4IfIndex,
                                                    VlanId, L2_PROTO_MVRP,
                                                    &u1TunnelStatus,
                                                    &u1L2CPProtType);

            }
            else
            {
                i4RetVal =
                    L2IwfGetL2ProtocolTunnelStatus (u4ContextId, u4IfIndex,
                                                    VlanId, L2_PROTO_GVRP,
                                                    &u1TunnelStatus,
                                                    &u1L2CPProtType);

            }

            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
            {
                if (MrpApiIsMvrpEnabled (u4ContextId) == OSIX_TRUE)
                {
                    return L2_MVRP_PKT;
                }
#ifdef GARP_WANTED
                if (GvrpIsGvrpEnabledInContext (u4ContextId) == GVRP_TRUE)
                {
                    return L2_GVRP_PKT;
                }
#endif
                return L2_DATA_PKT;
            }
            if ((i4RetVal == L2IWF_SUCCESS)
                && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL))
            {
                return L2_DATA_PKT;
            }
#ifdef VLAN_WANTED
            if ((i4RetVal == L2IWF_SUCCESS)
                && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                if (u2EnetType == L2_MVRP_ENET_TYPE)
                {
                    if (u1L2CPProtType == L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)
                    {
                        VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_MVRP);
                        return L2_INVALID_PKT;
                    }
                    else if (u1L2CPProtType ==
                             L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
                    {
                        VlanIncrRxDiscardCountersPerVlan (u4ContextId, VlanId,
                                                          L2_PROTO_MVRP);
                        return L2_INVALID_PKT;
                    }
                }
                else
                {
                    if (u1L2CPProtType == L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)
                    {
                        VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_GVRP);
                        return L2_INVALID_PKT;
                    }
                    else if (u1L2CPProtType ==
                             L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
                    {
                        VlanIncrRxDiscardCountersPerVlan (u4ContextId, VlanId,
                                                          L2_PROTO_GVRP);
                        return L2_INVALID_PKT;
                    }
                }
            }
#endif
            return L2_INVALID_PKT;
        }

        if ((DestAddr[ETHERNET_ADDR_SIZE - 1] == L2_GMRP_PROTOCOL_ID) ||
            (DestAddr[ETHERNET_ADDR_SIZE - 1] == L2_MMRP_PROTOCOL_ID))

        {
            /* 
             * Customer MMRP/GMRP packets.
             * Handle the MMRP/GMRP packets based on the tunnel protocol status 
             * configured for this protocol on this port.
             */
            VlanGetTagLenInFrame (pFrame, (UINT2) u4IfIndex, &u4VlanOffset);
            CRU_BUF_Copy_FromBufChain (pFrame, (UINT1 *) &u2EnetType,
                                       u4VlanOffset, VLAN_TYPE_OR_LEN_SIZE);

            u2EnetType = (UINT2) OSIX_NTOHS (u2EnetType);

            if (u2EnetType == L2_MMRP_ENET_TYPE)
            {
                i4RetVal =
                    L2IwfGetL2ProtocolTunnelStatus (u4ContextId, u4IfIndex,
                                                    VlanId, L2_PROTO_MMRP,
                                                    &u1TunnelStatus,
                                                    &u1L2CPProtType);
            }
            else
            {
                i4RetVal =
                    L2IwfGetL2ProtocolTunnelStatus (u4ContextId, u4IfIndex,
                                                    VlanId, L2_PROTO_GMRP,
                                                    &u1TunnelStatus,
                                                    &u1L2CPProtType);
            }

            if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
            {
                if (MrpApiIsMmrpEnabled (u4ContextId) == OSIX_TRUE)
                {
                    return L2_MMRP_PKT;
                }
#ifdef GARP_WANTED
                if (GmrpIsGmrpEnabledInContext (u4ContextId) == GMRP_TRUE)
                {
                    return L2_GMRP_PKT;
                }
#endif
                return L2_DATA_PKT;
            }
            if ((i4RetVal == L2IWF_SUCCESS)
                && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL))
            {
                return L2_DATA_PKT;
            }
#ifdef VLAN_WANTED
            if ((i4RetVal == L2IWF_SUCCESS)
                && (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                if (u2EnetType == L2_MMRP_ENET_TYPE)
                {
                    if (u1L2CPProtType == L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)
                    {
                        VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_MMRP);
                        return L2_INVALID_PKT;
                    }
                    else if (u1L2CPProtType ==
                             L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
                    {
                        VlanIncrRxDiscardCountersPerVlan (u4ContextId, VlanId,
                                                          L2_PROTO_MMRP);
                        return L2_INVALID_PKT;
                    }

                }
                else
                {
                    if (u1L2CPProtType == L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)
                    {
                        VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_GMRP);
                        return L2_INVALID_PKT;
                    }
                    else if (u1L2CPProtType ==
                             L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
                    {
                        VlanIncrRxDiscardCountersPerVlan (u4ContextId, VlanId,
                                                          L2_PROTO_GMRP);
                        return L2_INVALID_PKT;
                    }

                }
                return L2_INVALID_PKT;
            }
#endif

            return L2_INVALID_PKT;
        }

        if ((u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE) ||
            (u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE))
        {
            /* Provider Bridge BPDU and GVRP packets can be tunneled
             * in the PPNP Ports only.
             */
            if (DestAddr[ETHERNET_ADDR_SIZE - 1] == 0x0D)
            {
                /* Provider MVRP/GVRP address.
                 * Should be processed on CNPs and PNPs. Dropped on 
                 * other ports. Give it to MVRP/GVRP, this will take care of 
                 * processing or dropping the packets based on port types.
                 */
                if (MrpApiIsMvrpEnabled (u4ContextId) == OSIX_TRUE)
                {
                    return L2_MVRP_PKT;
                }
#ifdef GARP_WANTED
                if (u2PbPortType == L2IWF_PROP_PROVIDER_NETWORK_PORT)
                {
                    u1TunnelStatus = pL2ProtStatus->u1GvrpProtocolStatus;

                    if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                    {
                        return L2_DATA_PKT;
                    }
#ifdef VLAN_WANTED
                    if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                    {
                        VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_STP);
                        return L2_INVALID_PKT;
                    }
#endif
                }
                if (GvrpIsGvrpEnabledInContext (u4ContextId) == GVRP_TRUE)
                {
                    return L2_GVRP_PKT;
                }
#endif
                return L2_DATA_PKT;
            }
            if (DestAddr[ETHERNET_ADDR_SIZE - 1] == 0x08)
            {
                /* Provider STP address - must be dropped in case of 
                 * CEP and PPNP ports.
                 */

                if (u2PbPortType == L2IWF_CUSTOMER_EDGE_PORT)
                {
                    return L2_INVALID_PKT;
                }

                if (u2PbPortType == L2IWF_PROP_PROVIDER_NETWORK_PORT)
                {
                    u1TunnelStatus = pL2ProtStatus->u1StpProtocolStatus;

                    if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL)
                    {
                        return L2_DATA_PKT;
                    }
#ifdef VLAN_WANTED
                    if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
                    {
                        VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_STP);
                        return L2_INVALID_PKT;
                    }
#endif
                    return L2_INVALID_PKT;
                }

                return L2_STAP_PKT;
            }
        }
        else if ((u4BridgeMode == L2IWF_PBB_ICOMPONENT_BRIDGE_MODE) ||
                 (u4BridgeMode == L2IWF_PBB_BCOMPONENT_BRIDGE_MODE))
        {
            if (DestAddr[ETHERNET_ADDR_SIZE - 1] == 0x08)
            {
                /* Provider STP address - must be dropped in case of 
                 * CEP and PPNP ports.
                 */
                if (L2IwfGetInterfaceType (u4ContextId,
                                           &u1InterfaceType) == L2IWF_FAILURE)
                {
                    return L2_INVALID_PKT;
                }
                if ((u2PbPortType == L2IWF_CNP_STAGGED_PORT) &&
                    (u1InterfaceType == L2IWF_C_INTERFACE_TYPE))
                {
                    return L2_INVALID_PKT;
                }
                return L2_STAP_PKT;
            }
            else if (DestAddr[ETHERNET_ADDR_SIZE - 1] == 0x0D)
            {
                /* Provider MVRP address.
                 * Should be processed on CBPs, VIPs and PNPs. Dropped on 
                 * other ports. Give it to MVRP, this will take care of 
                 * processing or dropping the packets based on port types.
                 */
                if (MrpApiIsMvrpEnabled (u4ContextId) == OSIX_TRUE)
                {
                    return L2_MVRP_PKT;
                }
                return L2_DATA_PKT;
            }
        }

        if ((DestAddr[ETHERNET_ADDR_SIZE - 1] > 0x22)
            && (DestAddr[ETHERNET_ADDR_SIZE - 1] <= 0x2F))
        {
            /*
             * The mac address is in the MRP/GARP application reserved
             * Mac address range. Since MVRP/GVRP and MMRP/GMRP are the only
             * known MRP/GARP applications, the packets in this range
             * should be treated like data packets.
             */
            return L2_DATA_PKT;
        }
    }

    L2IwfIsIgmpControlPacket (pFrame, &u1Result);
    if (u1Result == OSIX_TRUE)
    {
        u1TunnelStatus = pL2ProtStatus->u1IgmpProtocolStatus;

        if ((u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER) ||
            (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_TUNNEL))
        {
            return L2_DATA_PKT;
        }
#ifdef VLAN_WANTED
        if (u1TunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
        {
            VlanIncrRxDiscardCounters (u4IfIndex, L2_PROTO_IGMP);
            return L2_INVALID_PKT;
        }
#endif
        return L2_INVALID_PKT;
    }

    /* Treat the other packets than packets destined to L2 reserved group
     * address/IGMP control packets, as data packets.
     */
    return L2_DATA_PKT;
}

/***************************************************************************/
/* Function Name    : L2IwfHandleIfPnacFrame                               */
/*                                                                         */
/* Description      : This handles the incoming dot1x frames and           */
/*                    process them based on the tunnel/peer/discard status */
/*                                                                         */
/* Input(s)         : pBuf  - Incoming frame                               */
/*                    pPktHandleInfo - Information for packet processing   */
/*                    u1FrameType   - Frame type                           */
/*                    u1ProtTunnelStatus   - Pnac tunnel status on this    */
/*                                           port.                         */
/*                                                                         */
/* Output(s)        : None                                                 */
/*                                                                         */
/* Return Value(s)  : PNAC_DATA/PNAC_DROP/PNAC_CONTROL                     */
/*                                                                         */
/***************************************************************************/
INT4
L2IwfHandleIfPnacFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                        tPktHandleInfo * pPktHandleInfo, UINT1 u1FrameType,
                        UINT1 u1ProtTunnelStatus)
{

#ifndef PNAC_WANTED
    tMacAddr            DstAddr;
    tMacAddr            Dot1qTunnelMacAddr;
    MEMSET (&DstAddr, 0, sizeof (tMacAddr));
    MEMSET (&Dot1qTunnelMacAddr, 0, sizeof (tMacAddr));

    /*Fetch the Mac Address from the Incoming Packet */
    if (CRU_BUF_Copy_FromBufChain (pBuf, DstAddr, 0,
                                   MAC_ADDR_LEN) == CRU_FAILURE)
    {
        return PNAC_DROP;
    }

    /*Get the Tunnel Mac Address for DOT1X */
    VLAN_LOCK ();
    VlanTunnelProtocolMac (VLAN_NP_DOT1X_PROTO_ID, Dot1qTunnelMacAddr);
    VLAN_UNLOCK ();
#endif

    if ((pPktHandleInfo->u1IfType == CFA_PSEUDO_WIRE) ||
        (pPktHandleInfo->u1IfType == CFA_LAGG))
    {
        return PNAC_DATA;
    }

    if ((u1FrameType != BRG_IGMP_FRAME) && (u1FrameType != BRG_MRP_FRAME))
    {
        if ((pPktHandleInfo->u2EtherProto == PNAC_PAE_ENET_TYPE) ||
            (pPktHandleInfo->u2EtherProto == PNAC_PAE_RSN_PREAUTH_ENET_TYPE))
        {
            if (u1ProtTunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
            {
#ifdef PNAC_WANTED
                return (PnacWrHandleReceivedFrame (pBuf,
                                                   (UINT2) pPktHandleInfo->
                                                   u4IfIndex,
                                                   L2_PNAC_CTRL_FRAME,
                                                   pPktHandleInfo->
                                                   u4ContextId));

#else
                if (MEMCMP (Dot1qTunnelMacAddr, DstAddr, MAC_ADDR_LEN) != 0)
                {
                    return PNAC_DROP;
                }
#endif /*PNAC_WANTED */
            }
#ifdef VLAN_WANTED
            if (u1ProtTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
            {
                VlanIncrRxDiscardCounters (pPktHandleInfo->u4IfIndex,
                                           L2_PROTO_DOT1X);
                return PNAC_DROP;
            }
#endif
            return PNAC_DATA;
        }
    }
#ifdef PNAC_WANTED
    return (PnacWrHandleReceivedFrame (pBuf, (UINT2) pPktHandleInfo->u4IfIndex,
                                       u1FrameType,
                                       pPktHandleInfo->u4ContextId));
#else
    UNUSED_PARAM (pBuf);
    return PNAC_DATA;
#endif /*PNAC_WANTED */

}

/***************************************************************************/
/* Function Name    : L2IwfHandleIfLldpFrame                               */
/*                                                                         */
/* Description      : This function verify if the given packet is LLDP     */
/*                    control packet or not if it is LLDP control packet   */
/*                    it decides whether paket is to be tunneled or peer   */
/*                    or discard.                                          */
/*                                                                         */
/* Input(s)         : pBuf  - Incoming frame                               */
/*                    pPktHandleInfo - Information for packet processing   */
/*                    u1FrameType   - Frame type                           */
/*                    u1ProtTunnelStatus   - LLDP tunnel status on this    */
/*                                           port.                         */
/*                                                                         */
/* Output(s)        : None                                                 */
/*                                                                         */
/* Return Value(s)  : L2_DATA_PKT/L2_LLDP_PKT/L2_INVALID_PKT               */
/*                                                                         */
/***************************************************************************/
INT4
L2IwfHandleIfLldpFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                        tPktHandleInfo * pPktHandleInfo, UINT1 u1FrameType,
                        UINT1 u1ProtTunnelStatus)
{
    if (u1FrameType == BRG_IGMP_FRAME)
    {
        return L2_DATA_PKT;
    }

#ifdef VLAN_WANTED
    if (pPktHandleInfo->u2EtherProto == LLDP_ENET_TYPE)
    {
        if (u1ProtTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
        {
            /* Increment the VLAN LLDP Drop counters */
            VlanIncrRxDiscardCounters (pPktHandleInfo->u4IfIndex,
                                       L2_PROTO_LLDP);
            return L2_INVALID_PKT;
        }
    }
#endif /*VLAN_WANTED */

#ifdef LLDP_WANTED
    /* Additional check to save time. The other fields need to
     * be verified only if Ethernet protocol matches. If Module ID is not
     * known, parse the incoming buffer and identify its packet type */

    if (LldpIsLldpStarted () == LLDP_TRUE)
    {
        if ((pPktHandleInfo->u4ModuleId == PACKET_HDLR_PROTO_LLDP) ||
            (LldpApiIsLldpCtrlFrame (pBuf, pPktHandleInfo->u4IfIndex) ==
             OSIX_TRUE))
        {
            if (u1ProtTunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
            {
                if (LldpApiEnqIncomingFrame (pBuf, pPktHandleInfo->u4IfIndex)
                    != OSIX_SUCCESS)
                {
                    return L2_INVALID_PKT;
                }
                else
                {
                    return L2_LLDP_PKT;
                }
            }
            else if (u1ProtTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
            {
                /* Increment the VLAN LLDP Drop counters */
                VlanIncrRxDiscardCounters (pPktHandleInfo->u4IfIndex,
                                           L2_PROTO_LLDP);
                return L2_INVALID_PKT;
            }
            else
            {
                return L2_DATA_PKT;
            }
        }
    }
    else
    {
        UNUSED_PARAM (pBuf);
        UNUSED_PARAM (pPktHandleInfo);
        UNUSED_PARAM (u1ProtTunnelStatus);
    }
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pPktHandleInfo);
    UNUSED_PARAM (u1ProtTunnelStatus);
#endif

    return L2_DATA_PKT;
}

/***************************************************************************/
/* Function Name    : L2IwfHandleIfLaFrame                                 */
/*                                                                         */
/* Description      : This handles the incoming LACP packets and           */
/*                    process them based on the tunnel/peer/discard status */
/*                                                                         */
/* Input(s)         : pBuf  - Incoming frame                               */
/*                    u4IfIndex    - Interface index                       */
/*                    DestAddr     - Destination address of the packet.    */
/*                    u1ProtTunnelStatus - Lacp tunnel status on this port.*/
/*                                                                         */
/* Output(s)        : u2LogicalIfIndex - Port channel index                */
/*                                                                         */
/* Return Value(s)  : LA_DATA/LA_DROP/LA_CONTROL                           */
/*                                                                         */
/***************************************************************************/
INT4
L2IwfHandleIfLaFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                      tPktHandleInfo * pPktHandleInfo, UINT4 *pu4LogicalIfIndex,
                      UINT1 u1ProtTunnelStatus, UINT1 u1FrameType)
{
    INT4                i4RetVal;
    tMacAddr            DestAddr;
    UINT4               u4VlanOffset = CFA_VLAN_TAG_OFFSET;
#ifdef LA_WANTED
    UINT2               u2LogicalIndex = 0;
#endif

    if (VlanGetTagLenInFrame (pBuf, pPktHandleInfo->u4IfIndex, &u4VlanOffset)
        == VLAN_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    if (u1FrameType != BRG_IGMP_FRAME)
    {
        /* Additional check to save time. The other fields need to
         * be verified only if Ethernet protocol matches. If Module ID is not
         * known, parse the incoming buffer and identify its packet type */
        if (((pPktHandleInfo->u4ModuleId == PACKET_HDLR_PROTO_LA_MARKER) ||
             (pPktHandleInfo->u4ModuleId == PACKET_HDLR_PROTO_LA_LACP)) ||
            (L2IwfIsLaCtrlFrame (pBuf, DestAddr, pPktHandleInfo->u4IfIndex) ==
             L2IWF_TRUE))

        {
            if (u1ProtTunnelStatus == VLAN_TUNNEL_PROTOCOL_PEER)
            {
                /*if the PDU is VLAN tagged , strip the VLAN tag */
                if (u4VlanOffset ==
                    (LA_PROTOCOL_TYPE_OFFSET + VLAN_TAG_PID_LEN))
                {
                    VlanUnTagFrame (pBuf);
                }

#ifdef LA_WANTED
                i4RetVal = LA_CONTROL;

                if (LaHandleReceivedFrame (pBuf,
                                           (UINT2) pPktHandleInfo->u4IfIndex,
                                           &u2LogicalIndex,
                                           L2_LA_CTRL_FRAME) != LA_CONTROL)
                {
                    /* In case of LA module is disabled and tunnel status is peer,
                     * then the LA packet has to be dropped */
                    i4RetVal = LA_DROP;
                }
                *pu4LogicalIfIndex = (UINT4) u2LogicalIndex;

#else
                i4RetVal = LA_DROP;
                *pu4LogicalIfIndex = pPktHandleInfo->u4IfIndex;
#endif /*LA_WANTED */

                return i4RetVal;
            }

#ifdef VLAN_WANTED
            if (u1ProtTunnelStatus == VLAN_TUNNEL_PROTOCOL_DISCARD)
            {
                VlanIncrRxDiscardCounters (pPktHandleInfo->u4IfIndex,
                                           L2_PROTO_LACP);
                return LA_DROP;
            }
#endif
            /* We don't have to get logical port index here. If LACP tunnel status 
             * is tunnel, then it means that the received port is not in any 
             * port-channel. */
            *pu4LogicalIfIndex = pPktHandleInfo->u4IfIndex;
            return LA_DATA;
        }
    }

#ifdef LA_WANTED
    i4RetVal = LaHandleReceivedFrame (pBuf, (UINT2) pPktHandleInfo->u4IfIndex,
                                      &u2LogicalIndex, u1FrameType);
    *pu4LogicalIfIndex = (UINT4) u2LogicalIndex;
#else
    i4RetVal = LA_DATA;
    *pu4LogicalIfIndex = pPktHandleInfo->u4IfIndex;
#endif /*LA_WANTED */

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdateIgmpTagInfo                               */
/*                                                                           */
/* Description        : This routine is used to update the Inner Tag related */
/*                      information (VlanTag, Tag Length) in the packet.     */
/*                                                                           */
/* Input(s)           : pBuf         - Pointer to the Frame                  */
/*                      pL2PortEntry - Pointer to the Port entry             */
/*                                                                           */
/* Output(s)          : pVlantag  - Pointer to TAG Info of the packet        */
/*                      pu1TagLen - Pointer to TAG length of the packet      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
VOID
L2IwfUpdateIgmpTagInfo (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2PbPortType,
                        tVlanTag * pVlanTag, UINT2 *pu2TagLen)
{
    UINT2               u2VlanOffset = VLAN_TAG_OFFSET;
    UINT2               u2EtherType;
    UINT2               u2VlanTag;

    pVlanTag->InnerVlanTag.u2VlanId = VLAN_NULL_VLAN_ID;
    pVlanTag->InnerVlanTag.u1TagType = VLAN_UNTAGGED;
    pVlanTag->InnerVlanTag.u1Priority = 0;
    pVlanTag->InnerVlanTag.u1DropEligible = VLAN_FALSE;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EtherType,
                               VLAN_TAG_OFFSET, VLAN_TYPE_OR_LEN_SIZE);
    if (OSIX_NTOHS (u2EtherType) == VLAN_PROTOCOL_ID)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanTag,
                                   VLAN_TAG_VLANID_OFFSET, VLAN_TAG_SIZE);
        u2VlanTag = OSIX_NTOHS (u2VlanTag);
        pVlanTag->InnerVlanTag.u2VlanId = (tVlanId) (u2VlanTag & VLAN_ID_MASK);
        pVlanTag->InnerVlanTag.u1Priority =
            (UINT1) (u2VlanTag >> VLAN_TAG_PRIORITY_SHIFT);

        if (pVlanTag->InnerVlanTag.u2VlanId == VLAN_NULL_VLAN_ID)
        {
            pVlanTag->InnerVlanTag.u1TagType = VLAN_PRIORITY_TAGGED;
        }
        else
        {
            pVlanTag->InnerVlanTag.u1TagType = VLAN_TAGGED;
        }
        *pu2TagLen = VLAN_TAG_PID_LEN;
        u2VlanOffset = (UINT2) (u2VlanOffset + VLAN_TAG_PID_LEN);
    }

    if (u2PbPortType == CFA_CUSTOMER_BRIDGE_PORT)
    {
        /* Packet comes with S-Tag in customer bridge the tag length 
         * will be calculated as follows */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EtherType,
                                   u2VlanOffset, CFA_ENET_TYPE_OR_LEN);
        while ((OSIX_NTOHS (u2EtherType) == VLAN_PROTOCOL_ID) ||
               (OSIX_NTOHS (u2EtherType) == VLAN_PROVIDER_PROTOCOL_ID))
        {
            *pu2TagLen = (UINT2) (*pu2TagLen + VLAN_TAG_PID_LEN);
            u2VlanOffset = (UINT2) (u2VlanOffset + VLAN_TAG_PID_LEN);
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EtherType,
                                       u2VlanOffset, CFA_ENET_TYPE_OR_LEN);
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfHandleOutgoingPktOnPort               */
/*                                                                           */
/*    Description               : This API is used by L2 modules to transmit */
/*                                packets on backplane interfaces through CFA*/
/*                                The structure tCfaBackPlaneParams specifies   */
/*                                the set of parameters that needs to be     */
/*                                added into this PDU in CFA.                */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                pBackPlaneParams - Pointer to values to be */
/*                                                   pushed into the PDU.    */
/*                                u4IfIndex - ifIndex of the driver port.    */
/*                                u4PktSize - Size of the data buffer        */
/*                                u2Protocol - Protocol type                 */
/*                                u1EncapType - Encapsulation Type           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfHandleOutgoinPktOnBackPlane (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  tCfaBackPlaneParams * pBackPlaneParams,
                                  UINT4 u4IfIndex, UINT4 u4PktSize,
                                  UINT2 u2Protocol, UINT1 u1EncapType)
{
    if (CfaPostPktFromL2 (pBuf, u4IfIndex, u4PktSize, u2Protocol,
                          u1EncapType) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    if (CfaPostPktParamsFromL2 (pBackPlaneParams) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfHandleIncominPktOnBackPlane                     */
/*                                                                           */
/* Description        : This API is used by L2 modules to receive packets  on*/
/*                      backplane interfaces through CFA. The structure      */
/*                      tCfaBackPlaneParams specifies the set of parameters that*/
/*                      are retrieved from the PDU. These properties should  */
/*                      be used by the corresponding modules during processin*/
/*                      of this PDU.                                         */
/*                                                                           */
/* Input(s)           : pBuf - Incoming packet                               */
/*                      pBackPlaneParams - Pointer to structure containing   */
/*                                         params associated to backplane    */
/*                                         interface.                        */
/*                      u4IfIndex - Interface index.                         */
/*                      u1FrameType - Data frame or control frame.           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfHandleIncominPktOnBackPlane (tCRU_BUF_CHAIN_HEADER * pBuf,
                                  tCfaBackPlaneParams * pBackPlaneParams,
                                  UINT4 u4IfIndex, UINT1 u1FrameType)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tMacAddr            DestAddr;
    INT4                i4RetVal = 0;
    tL2ProtStatus       L2ProtocolStatus;
    UINT4               u4BridgeMode = 0;
#ifdef MRP_WANTED
    INT4                i4IsMvrpTaggedFrame = VLAN_TAGGED;
    tVlanId             VlanId = 0;
#endif
    UINT2               u2PbPortType = CFA_INVALID_BRIDGE_PORT;

    MEMSET (DestAddr, 0, sizeof (tMacAddr));

    /*Initialise the Interface Id in the Pkt Buffer. This is done bcoz
     * L2IwfIsIgmpControlPacket is using the Interface Id in Pkt Buffer*/
    pBuf->ModuleData.InterfaceId.u4IfIndex = u4IfIndex;

    L2_LOCK ();
    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);
    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        /* If the frame type is BR_IGMP_FRAME, then the buffer
         * would not have been duplicated in cfa. If L2IWF_FAILURE
         * is returned, then cfa will free the buffer. Since the
         * buffer is not duplicated and it is getting freed in
         * l2iwf, success should be returned. */
        if (u1FrameType == BRG_IGMP_FRAME)
        {
            return L2IWF_SUCCESS;
        }
        else
        {
            return L2IWF_FAILURE;
        }
    }

    u2PbPortType = pL2PortEntry->u2PbPortType;
    L2ProtocolStatus = pL2PortEntry->ProtocolStatus;
    /* Get the bridge mode in the same lock */
    if (L2IwfSelectContext (pBackPlaneParams->u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return L2IWF_FAILURE;
    }

    u4BridgeMode = L2IWF_BRIDGE_MODE ();

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    /* !!!!!!!!!!!!!!!!IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!! */
    /* Currently this is used for supporting MRP alone. This needs to be
     * enhanced once all protocols support Distributed Framework.
     * */

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) DestAddr, 0, ETHERNET_ADDR_SIZE);

    /* Classify the Reveived Frame */
    /* The last parameter is module ID. Since we do not have it, we pass 0 */
    i4RetVal = L2IwfClassifyFrame (pBackPlaneParams->u4ContextId, pBuf,
                                   DestAddr, &L2ProtocolStatus, u2PbPortType,
                                   u4BridgeMode, 0);

    /* For MRP PNAC and LA checks are not needed */
    switch (i4RetVal)
    {
#ifdef MRP_WANTED
        case L2_MVRP_PKT:
            i4RetVal = VlanIsMvrpFrameTagged (pBuf, u4IfIndex,
                                              &i4IsMvrpTaggedFrame);

            if ((i4RetVal == VLAN_SUCCESS)
                && (i4IsMvrpTaggedFrame == VLAN_UNTAGGED))
            {
                MrpApiEnqueueRxPktOnBackPlane (pBuf, pBackPlaneParams,
                                               u4IfIndex, MRP_STAP_CIST_ID);
            }
            else
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
            }
            break;

        case L2_MMRP_PKT:
            i4RetVal = VlanIdentifyVlanIdAndUntagFrame (MRP_MODULE, pBuf,
                                                        u4IfIndex, &VlanId);

            if (i4RetVal == VLAN_FORWARD)
            {
                MrpApiEnqueueRxPktOnBackPlane (pBuf, pBackPlaneParams,
                                               u4IfIndex, VlanId);
            }
            else
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                CfaIfSetInDiscard (u4IfIndex);
            }
            break;
#endif
        default:
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            break;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************
 * Name               : L2IwfGetPktInfoFromPacket
 *
 * Description        : This function is called on packet reception to validate
 *                      the packet and extract the header information.
 * 
 * Input(s)           : pBuf - Packet Buffer
 *                      u4IfIndex - Interface on which packet is received
 *
 * Output(s)          : pu2LenOrType - Ethertype of the packet
 *                      pu1IpHdrProto - IP Protocol in the packet
 *
 * Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       
 *
 *****************************************************************************/

PRIVATE INT4
L2IwfGetPktInfoFromPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                           tPktHandleInfo * pPktHandleInfo)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4VlanOffset = CFA_VLAN_TAG_OFFSET;

    /* Get the Interface information once and for all.
     * This should be passed onto subsequent functions to 
     * avoid locks.*/
    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    if (IfInfo.i4Active != CFA_TRUE)
    {
        return L2IWF_FAILURE;
    }

    /* Fill the interface related parameters */
    pPktHandleInfo->u4IfIndex = u4IfIndex;
    pPktHandleInfo->u1IfType = IfInfo.u1IfType;

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &pPktHandleInfo->u4ContextId,
                                      &pPktHandleInfo->u2LocalPortId) !=
        VCM_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    /* first we extract the Enet Frame Header - we assume it to be Enet by
       default. It is assumed that the Header is located linearly in the
       buffer since Enet is the lower most interface layer */
    MEMSET (&pPktHandleInfo->EnetHdr, 0, CFA_ENET_V2_HEADER_SIZE);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &pPktHandleInfo->EnetHdr, 0,
                               CFA_ENET_V2_HEADER_SIZE);

    pPktHandleInfo->u2EtherProto =
        OSIX_NTOHS (pPktHandleInfo->EnetHdr.u2LenOrType);

    if (IfInfo.u1BridgedIface == CFA_ENABLED)
    {
        if (VlanGetTagLenInFrame (pBuf, u4IfIndex, &u4VlanOffset)
            == VLAN_FAILURE)
        {
            return L2IWF_FAILURE;
        }

        if (u4VlanOffset > CFA_VLAN_TAG_OFFSET)
        {
            /* Vlan Tag is present, copy the Ethernet Protocol with
             *  the new offset */
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &pPktHandleInfo->u2EtherProto,
                                       u4VlanOffset, CFA_ENET_TYPE_OR_LEN);

            pPktHandleInfo->u2EtherProto =
                OSIX_NTOHS (pPktHandleInfo->u2EtherProto);
        }
    }

    /* Set the Vlan OfSet to the Packet Info */
    pPktHandleInfo->u4VlanOffset = u4VlanOffset;

    if (pPktHandleInfo->u2EtherProto == L2_ENET_IP)
    {
        /* Extracting IP header from the packet to identify the
         * protocol type.
         */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &pPktHandleInfo->u1IpProto,
                                   u4VlanOffset + VLAN_TYPE_OR_LEN_SIZE +
                                   CFA_IP_PROTO_OFFSET_IN_IPHDR,
                                   sizeof (UINT1));

    }

    if (CfaIsMacAllZeros (pPktHandleInfo->EnetHdr.au1SrcAddr) == CFA_TRUE)
    {
        CFA_IF_SET_IN_DISCARD (u4IfIndex);
        return L2IWF_FAILURE;
    }

    /* we determine the type of Enet Frame */
    if (CFA_IS_ENET_MAC_BCAST (&pPktHandleInfo->EnetHdr))
    {
        pPktHandleInfo->u1LinkFrameType = CFA_LINK_BCAST;
        CFA_IF_SET_IN_BCAST (u4IfIndex);
    }
    else if (CFA_IS_ENET_MAC_MCAST (&pPktHandleInfo->EnetHdr))
    {
        pPktHandleInfo->u1LinkFrameType = CFA_LINK_MCAST;
        CFA_IF_SET_IN_MCAST (u4IfIndex);
    }
    else
    {
        pPktHandleInfo->u1LinkFrameType = CFA_LINK_UCAST;
        CFA_IF_SET_IN_UCAST (u4IfIndex);
    }

    return L2IWF_SUCCESS;
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : L2IwfIsApsSteadyStatePacket                          */
/*                                                                           */
/* Description        : This function checks whether the packet is a steady  */
/*                      state packet of ELPS/ERPS module                     */
/*                                                                           */
/* Input(s)           : pBuf      - APS or R-APS PDU                         */
/*                      u4ApsFlag - Flag to indicate whether it is APS/R-APS */
/*                                  PDU                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS /L2IWF_FAILURE.                        */
/*****************************************************************************/
INT4
L2IwfIsApsSteadyStatePacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *u4ApsFlag)
{
    UINT1               u1Opcode = 0;
    UINT1               u1RequestOrState = 0;
    UINT1               u1Status = 0;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Opcode,
                               L2IWF_APS_OPCODE_OFFSET, 1);

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1RequestOrState,
                               L2IWF_APS_REQ_STATE_OFFSET, 1);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1Status,
                               L2IWF_APS_STATUS_OFFSET, 1);

    *u4ApsFlag = APS_INVALID;

    if (IS_STEADY_STATE_PKT_FOR_ELPS (u1Opcode, u1RequestOrState))
    {
        *u4ApsFlag = APS_SSP;
    }
    else if (IS_STEADY_STATE_PKT_FOR_ERPS (u1Opcode, u1RequestOrState,
                                           u1Status))
    {
        *u4ApsFlag = RAPS_SSP;
    }
    else
    {
        return L2IWF_FAILURE;
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSendApsSteadyStatePkt                           */
/*                                                                           */
/* Description        : This function checks whether the packet is a steady  */
/*                      state packet of ELPS/ERPS module                     */
/*                                                                           */
/* Input(s)           : pBuf      - APS or R-APS PDU                         */
/*                      u4IfIndex - Interface Index                          */
/*                      u4PktSize - Packet Size                              */
/*                      u4ApsFlag - Flag to indicate whether it is APS/R-APS */
/*                                  PDU                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS /L2IWF_FAILURE.                        */
/*****************************************************************************/
INT4
L2IwfSendApsSteadyStatePkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                            UINT4 u4PktSize, UINT4 u4ApsFlag)
{
#ifdef ELPS_WANTED
    if (u4ApsFlag == APS_SSP)
    {
        if (ElpsPortSendStdyStPkt (pBuf, u4PktSize, u4IfIndex) == OSIX_FAILURE)
        {
            return L2IWF_FAILURE;
        }
    }
#endif
#ifdef ERPS_WANTED
    if (u4ApsFlag == RAPS_SSP)
    {
        if (ErpsPortSendStdyStPkt (pBuf, u4PktSize, u4IfIndex) == OSIX_FAILURE)
        {
            return L2IWF_FAILURE;
        }
    }
#endif
#if !defined (ERPS_WANTED) && !defined (ELPS_WANTED)
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4PktSize);
    UNUSED_PARAM (u4ApsFlag);
#endif
    return L2IWF_FAILURE;
}

#ifdef L2RED_WANTED
/***************************************************************************/
/* Function           : L2IwfRedGetHRFlag                                  */
/* Input(s)           : None                                               */
/* Output(s)          : None                                               */
/* Returns            : Hitless restart flag value.                        */
/* Action             : This API returns the hitless restart flag value.   */
/***************************************************************************/
UINT1
L2IwfRedGetHRFlag (VOID)
{
    UINT1               u1HRFlag = 0;

    u1HRFlag = RmGetHRFlag ();

    return (u1HRFlag);
}
#endif
