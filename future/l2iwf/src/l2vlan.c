/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2vlan.c,v 1.119 2018/01/31 10:57:21 siva Exp $
 *
 * Description:This file contains routines to get/set Vlan
 *             information from the L2IWF module.                 
 *
 *******************************************************************/
#ifndef _L2_VLAN_C_
#define _L2_VLAN_C_

#include "l2inc.h"

UINT2               gL2DefaultVlanId;

/*****************************************************************************/
/* Function Name      : L2IwfCreateVlan                                      */
/*                                                                           */
/* Description        : This routine is called from VLAN module to           */
/*                      intialise the entry corresponding to the given Vlan  */
/*                      in the L2Iwf common database. It also indicates MSTP */
/*                      about the Vlan creation                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch number                  */
/*                      VlanId    - VlanId which has been newly created.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfCreateVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_VLAN_INFO (VlanId) =
        (tL2VlanInfo *) L2IWF_ALLOCATE_MEMBLOCK (L2IWF_VLANINFO_POOLID ());

    if (L2IWF_VLAN_INFO (VlanId) == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    MEMSET (L2IWF_VLAN_INFO (VlanId), 0, sizeof (tL2VlanInfo));

    L2IWF_VLAN_VLANID (VlanId) = VlanId;

    L2IWF_VLAN_SERVICE_TYPE (VlanId) = VLAN_E_LAN;
    L2IWF_FCOE_VLAN_TYPE (VlanId) = L2_VLAN;
    L2IwfReleaseContext ();

    L2_UNLOCK ();

    if (AstIsMstEnabledInContext (u4ContextId) == AST_TRUE)
    {
        MstMiVlanCreateIndication (u4ContextId, VlanId);
    }

    /* Indicate ECFM Module for VLAN creation. This is required for CCM
     * Offloading only.
     * */
#ifdef ECFM_WANTED
    EcfmCcmOffloadCreateVlanIndication (u4ContextId, VlanId);
#endif
#ifdef IGS_WANTED
    SnoopCreateVlanIndication (u4ContextId, VlanId);
#endif

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeleteVlan                                      */
/*                                                                           */
/* Description        : This routine is called from VLAN module to           */
/*                      deintialise the entry corresponding to the given Vlan*/
/*                      in the L2Iwf common database. It also indicates MSTP */
/*                      about the Vlan deletion                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch number                  */
/*                      VlanId    - VlanId which has been newly created.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfDeleteVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    UINT1              *pLocalEgressPorts = NULL;
#ifdef MSTP_WANTED
    UINT1              *pNullLocalPortList = NULL;
#endif
    tPortList          *pEgressPorts = NULL;

#ifdef PTP_WANTED
    tPtpL2IfStatusChgInfo PtpL2IfStatusChgInfo;
    MEMSET (&PtpL2IfStatusChgInfo, 0, sizeof (tPtpL2IfStatusChgInfo));
#endif

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return L2IWF_FAILURE;
    }

    if (AstIsMstEnabledInContext (u4ContextId) == AST_TRUE)
    {
        MstMiVlanDeleteIndication (u4ContextId, VlanId);
    }
    else
    {
        PvrstMiVlanDeleteIndication (u4ContextId, VlanId);
    }

    pEgressPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPorts == NULL)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (pEgressPorts, 0, sizeof (tPortList));

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pLocalEgressPorts = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pLocalEgressPorts == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    MEMSET (pLocalEgressPorts, 0, sizeof (tLocalPortList));
    if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_TRUE)
    {
        MEMCPY (pLocalEgressPorts, L2IWF_VLAN_EGRESS_PORTS (VlanId),
                sizeof (tLocalPortList));
        L2IwfConvToGlblIfIndexList (pLocalEgressPorts, *pEgressPorts);
    }
#ifdef FSB_WANTED
    if (L2IWF_FCOE_VLAN_TYPE (VlanId) == FCOE_VLAN)
    {
        FsbDeleteVlanIndication (u4ContextId, VlanId);
    }
#endif
    L2IWF_RELEASE_MEMBLOCK (L2IWF_VLANINFO_POOLID (), L2IWF_VLAN_INFO (VlanId));
    L2IWF_VLAN_INFO (VlanId) = NULL;

    L2IwfReleaseContext ();

    L2_UNLOCK ();
    L2IwfDeletePortVlanEntries (VlanId, *pEgressPorts);

#ifdef MSTP_WANTED
    pNullLocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pNullLocalPortList == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        UtilPlstReleaseLocalPortList (pLocalEgressPorts);
        return L2IWF_FAILURE;
    }
    MEMSET (pNullLocalPortList, 0, sizeof (tLocalPortList));
    MstUpdateVlanPortList (u4ContextId, VlanId, pNullLocalPortList,
                           pLocalEgressPorts);
    UtilPlstReleaseLocalPortList (pNullLocalPortList);
#endif
    UtilPlstReleaseLocalPortList (pLocalEgressPorts);

    L2IwfDeletePortVlanEntries (VlanId, *pEgressPorts);
#ifdef ECFM_WANTED
    EcfmCcmOffloadDeleteVlanIndication (u4ContextId, VlanId);
#endif
    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
#ifdef PTP_WANTED
    PtpL2IfStatusChgInfo.u4ContextId = u4ContextId;
    PtpL2IfStatusChgInfo.u4IfIndex = (UINT4) VlanId;
    PtpL2IfStatusChgInfo.u1IfType = PTP_IFACE_VLAN;
    PtpL2IfStatusChgInfo.u1Status = CFA_IF_DEL;
    PtpApiIfStatusChgHdlr (&PtpL2IfStatusChgInfo);
#endif

#ifdef CFA_WANTED
    /* Indicate the VLAN deletion to IPDB module */
    IpdbPortVlanDelete (u4ContextId, VlanId);
    L2dsPortVlanDelete (u4ContextId, VlanId);
#endif

#ifdef RMON_WANTED
    RmonDeleteVlan (u4ContextId, VlanId);
#endif

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeleteAllVlans                                  */
/*                                                                           */
/* Description        : This routine is called when VLAN module is disabled. */
/*                      It deintialises entries corresponding to all Vlans   */
/*                      in the L2Iwf common database. It also indicates MSTP */
/*                      about each Vlan deletion                             */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch number                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfDeleteAllVlans (UINT4 u4ContextId)
{
    tPortList          *pIfDelPortList = NULL;
    INT4                i4Index;
    UINT4               u4IfIndex;
    tVlanId             VlanId;

    if (AstIsMstEnabledInContext (u4ContextId) == AST_TRUE)
    {
        MstMiDeleteAllVlans (u4ContextId);
    }
    else
    {
        PvrstMiDeleteAllVlanIndication (u4ContextId);
    }

    pIfDelPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pIfDelPortList == NULL)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pIfDelPortList);
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    for (VlanId = 1; VlanId <= VLAN_DEV_MAX_VLAN_ID; VlanId++)
    {
        if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_TRUE)
        {
            if (L2IWF_VLAN_INFO (VlanId) != NULL)
            {
                L2IWF_RELEASE_MEMBLOCK (L2IWF_VLANINFO_POOLID (),
                                        L2IWF_VLAN_INFO (VlanId));
                L2IWF_VLAN_INFO (VlanId) = NULL;
            }
        }
    }

    /* As all the vlans mapped to this context have been deleted, the 
     * entries in the PortVlanRBTree also need to be deleted. Hence get all the
     * active ports in this Context and using this the PortVlan nodes in the 
     * RBTree can be deleted.
     */

    MEMSET (*pIfDelPortList, 0, sizeof (tPortList));
    for (i4Index = 1; i4Index <= L2IWF_MAX_PORTS_PER_CONTEXT_EXT; i4Index++)
    {
        if (L2IWF_PORT_ACTIVE (i4Index) == OSIX_TRUE)
        {
            u4IfIndex = L2IWF_PORT_IFINDEX (i4Index);
            OSIX_BITLIST_SET_BIT ((*pIfDelPortList), u4IfIndex,
                                  sizeof (tPortList));
        }
    }

    L2IwfReleaseContext ();

    L2IwfDelPortVlanEntriesForPorts (*pIfDelPortList);
    FsUtilReleaseBitList ((UINT1 *) pIfDelPortList);
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortVlanTunnelStatus                         */
/*                                                                           */
/* Description        : This routine returns the port's tunnel status        */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global Index of the port whose tunnel    */
/*                                    status is to be obtained.              */
/*                                                                           */
/* Output(s)          : bTunnelPort - Boolean value indicating whether       */
/*                                    Tunnelling is enabled on the port      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetPortVlanTunnelStatus (UINT4 u4IfIndex, BOOL1 * pbTunnelPort)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    *pbTunnelPort = OSIX_FALSE;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo != NULL)
    {
        *pbTunnelPort = (BOOL1) L2IWF_IFENTRY_TUNNEL_STATUS (pL2PortInfo);
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanPortType                                 */
/*                                                                           */
/* Description        : This routine returns the port tunnel type            */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global Port Index of the port whose port */
/*                               type is to be obtained.                     */
/*                                                                           */
/* Output(s)          : pPortType   - Port Type VLAN_ACCESS_PORT/            */
/*                                              VLAN_HYBRID_PORT/            */
/*                                              VLAN_TRUNK_PORT/             */
/*                                              VLAN_PROMISCOUS_PORT/        */
/*                                              VLAN_HOST_PORT               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVlanPortType (UINT4 u4IfIndex, UINT1 *pPortType)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pPortType = L2IWF_IFENTRY_TYPE (pL2PortInfo);

    L2_UNLOCK ();

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanPortAccpFrmType                          */
/*                                                                           */
/* Description        : This routine returns the port acceptable frame type  */
/*                      for the given Port Index. It accesses the L2Iwf      */
/*                      common database.                                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global Port Index of the port whose port */
/*                      acceptable frame type is to be obtained.             */
/*                                                                           */
/* Output(s)          : pPortAccpFrmType - Port Acceptable frame Type        */
/*                      VLAN_ADMIT_ALL_FRAMES/                               */
/*                      VLAN_ADMIT_ONLY_VLAN_TAGGED_FRAMES/                  */
/*                      VLAN_ADMIT_ONLY_UNTAGGED_AND_PRIORITY_TAGGED_FRAME   */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVlanPortAccpFrmType (UINT4 u4IfIndex, UINT1 *pPortAccpFrmType)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pPortAccpFrmType = L2IWF_IFENTRY_ACCP_FRAME_TYPE (pL2PortInfo);

    L2_UNLOCK ();

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanPortPvid                                 */
/*                                                                           */
/* Description        : This routine returns the port VLAN Id for the given  */
/*                      given Port Index. It accesses the L2Iwf common       */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global Port Index of the port whose port */
/*                               type is to be obtained.                     */
/*                                                                           */
/* Output(s)          : pPortPvid   - Port VLAN Id.                          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVlanPortPvid (UINT4 u4IfIndex, tVlanId * pVlanId)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pVlanId = L2IWF_IFENTRY_PVID (pL2PortInfo);

    L2_UNLOCK ();

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanPortGvrpStatus                           */
/*                                                                           */
/* Description        : This routine returns the port's gvrp enabled/disabled*/
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Index of the port whose port type      */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : u1GvrpStatus- Gvrp enabled/disabled                  */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVlanPortGvrpStatus (UINT4 u4IfIndex, UINT1 *pu1GvrpStatus)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1GvrpStatus = L2IWF_IFENTRY_GVRP_STATUS (pL2PortInfo);

    L2_UNLOCK ();

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanPortMvrpStatus                           */
/*                                                                           */
/* Description        : This routine returns the port's mvrp enabled/disabled*/
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Index of the port whose port type      */
/*                                    is to be obtained.                     */
/*                                                                           */
/* Output(s)          : u1MvrpStatus- Mvrp enabled/disabled                  */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVlanPortMvrpStatus (UINT4 u4IfIndex, UINT1 *pu1MvrpStatus)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1MvrpStatus = L2IWF_IFENTRY_MVRP_STATUS (pL2PortInfo);

    L2_UNLOCK ();

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfIsTunnelPortPresent                             */
/*                                                                           */
/* Description        : This routine scans the L2Iwf common database and     */
/*                      returns TRUE if any port has tunnelling enabled.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
L2IwfIsTunnelPortPresent (VOID)
{
    BOOL1               bTunnelPortPresent = OSIX_FALSE;

    L2_LOCK ();

    RBTreeWalk (L2IWF_GLOBAL_IFINDEX_TREE (),
                L2IwfWalkFnIsTunnelPort, NULL, &bTunnelPortPresent);

    L2_UNLOCK ();

    return bTunnelPortPresent;
}

/*****************************************************************************/
/* Function Name      : L2IwfWalkFnIsTunnelPort                              */
/*                                                                           */
/* Description        : This is the walk function that is applied to each    */
/*                      node of the Global IfIndex tree for checking whether */
/*                      it is a tunnel port.                                 */
/*                                                                           */
/* Input(s)           : pElem   - pointer to a node in the tree              */
/*                      visit   - preorder, postorder, endorder or leaf      */
/*                      u4level - level of this node in the tree             */
/*                      pArg    - general input to all nodes in the tree     */
/*                      pOut    - Output from the function                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfWalkFnIsTunnelPort (tRBElem * pElem, eRBVisit visit,
                         UINT4 u4Level, void *pArg, void *pOut)
{
    BOOL1              *pbRetVal = pOut;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (pArg);

    if (visit == leaf || visit == postorder)
    {
        if (L2IWF_IFENTRY_TUNNEL_STATUS ((tL2PortInfo *) pElem) == OSIX_TRUE)
        {
            *pbRetVal = OSIX_TRUE;
            return RB_WALK_BREAK;
        }
    }

    return RB_WALK_CONT;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortVlanTunnelStatus                         */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the       */
/*                      Port's Tunnelling status in the L2Iwf common         */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2LocalPortId - Local port index of the port whose   */
/*                                    oper edge                              */
/*                                    status is to be updated.               */
/*                      bTunnelPort - Boolean value indicating whether the   */
/*                                    port is a Tunnel port                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetPortVlanTunnelStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                              BOOL1 bTunnelPort)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPortId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    L2IWF_PORT_TUNNEL_STATUS (u2LocalPortId) = (UINT1) bTunnelPort;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetVlanPortType                                 */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the       */
/*                      Port type in the L2Iwf common                        */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2LocalPortId - Local port Index of the port whose   */
/*                                      port type is to be updated.          */
/*                      u1PortType  - Port Type could be one of Hybrid,      */
/*                                    Access, Trunk, promiscous or host.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVlanPortType (UINT4 u4ContextId, UINT2 u2LocalPortId, UINT1 u1PortType)
{
    UINT4               u4IfIndex;
    tVlanId             VlanId;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPortId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    L2IWF_PORT_TYPE (u2LocalPortId) = u1PortType;
    u4IfIndex = L2IWF_PORT_IFINDEX (u2LocalPortId);
    VlanId = L2IWF_PORT_PVID (u2LocalPortId);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    if (AstIsPvrstStartedInContext (u4ContextId) == AST_TRUE)
    {
        if (PvrstSetPortType ((UINT2) u4IfIndex,
                              u1PortType, VlanId) != PVRST_SUCCESS)
        {
            return L2IWF_FAILURE;
        }
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetVlanPortAccpFrmType                          */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the       */
/*                      Port acceptable frame type in the L2Iwf common       */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId   - Virtual Switch ID                    */
/*                      u2LocalPortId - Local port Index of the port whose   */
/*                                    acceptable frame type is to be updated */
/*                      u1PortAccpFrmType - Port acceptable frame Type       */
/*                           could be one of admit all, admit only tagged    */
/*                           or admit untagged and priority tagged frames    */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVlanPortAccpFrmType (UINT4 u4ContextId, UINT2 u2LocalPortId,
                             UINT1 u1PortAccpFrmType)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPortId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    L2IWF_PORT_ACCP_FRAME_TYPE (u2LocalPortId) = u1PortAccpFrmType;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfSetVlanPortPvid                                 */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the       */
/*                      Port VLAN Id in the L2Iwf common database.           */
/*                                                                           */
/* Input(s)           : u4ContextId   - Virtual Switch ID                    */
/*                      u2LocalPortId - Local port Index of the port whose   */
/*                                      port type is to be updated.          */
/*                      VlanId        - Port VLAN Id.                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVlanPortPvid (UINT4 u4ContextId, UINT2 u2LocalPortId, tVlanId VlanId)
{
    UINT4               u4IfIndex;
    tVlanId             PrevVlanId;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPortId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    PrevVlanId = L2IWF_PORT_PVID (u2LocalPortId);
    u4IfIndex = L2IWF_PORT_IFINDEX (u2LocalPortId);

    L2IWF_PORT_PVID (u2LocalPortId) = VlanId;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    if (PrevVlanId != VlanId)
    {
        /* Send notification to LLDP */
        LldpApiNotifyPortVlanId (u4IfIndex, VlanId);
    }

    if (AstIsPvrstStartedInContext (u4ContextId) == AST_TRUE)
    {
        if (PvrstSetNativeVlan (u4IfIndex, PrevVlanId, VlanId) != PVRST_SUCCESS)
        {
            return L2IWF_FAILURE;
        }
    }

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanFdbId                                    */
/*                                                                           */
/* Description        : This routine returns the FdbId corresponding to the  */
/*                      given VlanId. It accesses the L2Iwf common database  */
/*                                                                           */
/* Input(s)           : VlanId      - Vlan whose Fdb Id is to obtained       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FdbId value corresponding to the VlanId              */
/*****************************************************************************/
UINT4
L2IwfGetVlanFdbId (tVlanId VlanId)
{
    return L2IwfMiGetVlanFdbId (L2IWF_DEFAULT_CONTEXT, VlanId);
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanFdbId                                  */
/*                                                                           */
/* Description        : This routine returns the FdbId corresponding to the  */
/*                      given VlanId. It accesses the L2Iwf common database  */
/*                      for the given context                                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Fdb Id is to obtained       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : FdbId value corresponding to the VlanId              */
/*****************************************************************************/
UINT4
L2IwfMiGetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId)
{
    UINT4               u4FdbId;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return 0;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID)
    {
        return 0;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return 0;
    }

    u4FdbId = L2IWF_VID_FID_MAP (VlanId);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return u4FdbId;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetFdbInfo                                      */
/*                                                                           */
/* Description        : This routine returns the list of Fdbs  mapped to the */
/*                      given vlan list.                                     */
/*                                                                           */
/* Input(s)           : pL2IwfFdbInfo - Pointer to tL2IwfFdbInfo             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Fdbs Mapped                                */
/*****************************************************************************/
INT4
L2IwfGetFdbInfo (tL2IwfFdbInfo * pL2IwfFdbInfo)
{
    UINT4               u4FdbId = 0;
    UINT2               u2VlanIndex;
    UINT1               u1Result = OSIX_FALSE;

    if (pL2IwfFdbInfo->u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (pL2IwfFdbInfo->u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    for (u2VlanIndex = 1; u2VlanIndex <= VLAN_DEV_MAX_VLAN_ID; u2VlanIndex++)
    {
        OSIX_BITLIST_IS_BIT_SET (pL2IwfFdbInfo->au1VlanList, u2VlanIndex,
                                 L2IWF_VLAN_LIST_SIZE, u1Result);
        if ((u1Result == OSIX_TRUE) &&
            (L2IWF_VLAN_ACTIVE (u2VlanIndex) == OSIX_TRUE))
        {
            u4FdbId = L2IWF_VID_FID_MAP (u2VlanIndex);
            pL2IwfFdbInfo->u2FdbCount++;
            OSIX_BITLIST_SET_BIT (pL2IwfFdbInfo->au1FdbList, u4FdbId,
                                  L2IWF_VLAN_LIST_SIZE);
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfMiGetVlanIdFromFdbId                        */
/*                                                                           */
/*    Description         : This function returns the first Vlan mapped to   */
/*                          the given FDB.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Virtual Switch ID                  */
/*                          u4FdbId - FDB Id                                 */
/*                                                                           */
/*    Output(s)           : VlanId - Index of the output Vlan                */
/*                                                                           */
/*    Returns             : L2IWF_SUCCESS / L2IWF_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfMiGetVlanIdFromFdbId (UINT4 u4ContextId, UINT4 u4FdbId, tVlanId * pVlanId)
{
    tVlanId             VlanId;
    UINT1               u1VlanLearningType;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    u1VlanLearningType = VlanMiGetVlanLearningMode (u4ContextId);

    if (u1VlanLearningType == VLAN_INDEP_LEARNING)
    {
        /* In Independent Vlan Learning, Fdb Id is same as the Vlan Id */
        *pVlanId = (tVlanId) u4FdbId;

        return L2IWF_SUCCESS;
    }

    L2_LOCK ();

    if (u1VlanLearningType == VLAN_SHARED_LEARNING)
    {
        *pVlanId = L2IWF_DEFAULT_VLAN_ID ();
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    for (VlanId = 1; VlanId <= VLAN_DEV_MAX_VLAN_ID; VlanId++)
    {
        if (L2IWF_VID_FID_MAP (VlanId) == (UINT2) u4FdbId)
        {
            *pVlanId = VlanId;
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_SUCCESS;
        }
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetVlanFdbId                                    */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the       */
/*                      Vlan's FdbId in the L2Iwf common database for the    */
/*                      given context.                                       */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Fdb Id is to updated        */
/*                      u4FdbId     - FdbId corresponding to the VlanId      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVlanFdbId (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4FdbId)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_VID_FID_MAP (VlanId) = (UINT2) u4FdbId;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanLocalEgressPorts                       */
/*                                                                           */
/* Description        : This routine returns the Egress ports for the given  */
/*                      VlanId as Local port IDs. It accesses the L2Iwf      */
/*                      common database for the given context                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Egress ports are to be      */
/*                                    obtained                               */
/*                                                                           */
/* Output(s)          : EgressPorts - Egress Port List for the given Vlan    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfMiGetVlanLocalEgressPorts (UINT4 u4ContextId, tVlanId VlanId,
                                tLocalPortList LocalEgressPorts)
{
    MEMSET (LocalEgressPorts, 0, sizeof (tLocalPortList));

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_TRUE)
    {
        MEMCPY (LocalEgressPorts, L2IWF_VLAN_EGRESS_PORTS (VlanId),
                sizeof (tLocalPortList));
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanEgressPorts                              */
/*                                                                           */
/* Description        : This routine returns the Egress ports for the given  */
/*                      VlanId. It accesses the L2Iwf common database        */
/*                                                                           */
/* Input(s)           : VlanId      - Vlan whose Egress ports are to be      */
/*                                    obtained                               */
/*                                                                           */
/* Output(s)          : EgressPorts - Egress Port List for the given Vlan    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVlanEgressPorts (tVlanId VlanId, tPortList EgressPorts)
{
    return L2IwfMiGetVlanEgressPorts (L2IWF_DEFAULT_CONTEXT, VlanId,
                                      EgressPorts);
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanEgressPorts                            */
/*                                                                           */
/* Description        : This routine returns the Egress ports for the given  */
/*                      VlanId. It accesses the L2Iwf common database for    */
/*                      the given context.                                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Egress ports are to be      */
/*                                    obtained                               */
/*                                                                           */
/* Output(s)          : EgressPorts - Egress Port List for the given Vlan    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfMiGetVlanEgressPorts (UINT4 u4ContextId, tVlanId VlanId,
                           tPortList EgressPorts)
{
    UINT1              *pLocalEgressPorts = NULL;

    MEMSET (EgressPorts, 0, sizeof (tPortList));

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_TRUE)
    {
        pLocalEgressPorts =
            UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pLocalEgressPorts == NULL)
        {
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
        MEMSET (pLocalEgressPorts, 0, sizeof (tLocalPortList));

        MEMCPY (pLocalEgressPorts, L2IWF_VLAN_EGRESS_PORTS (VlanId),
                sizeof (tLocalPortList));

        L2IwfConvToGlblIfIndexList (pLocalEgressPorts, EgressPorts);
        UtilPlstReleaseLocalPortList (pLocalEgressPorts);
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiIsVlanMemberPort                              */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      port is a member of the given Vlan in the given      */
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : ContextId, VlanId, Global IfIndex                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
L2IwfMiIsVlanMemberPort (UINT4 u4ContextId, tVlanId VlanId, UINT4 u4IfIndex)
{
    UINT2               u2LocalPort;
    tL2PortInfo        *pL2PortInfo = NULL;
    BOOL1               bResult = OSIX_FALSE;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return OSIX_FALSE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return OSIX_FALSE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return OSIX_FALSE;
    }

    u2LocalPort = L2IWF_IFENTRY_LOCAL_PORT (pL2PortInfo);

    if (L2IWF_IFENTRY_CONTEXT_ID (pL2PortInfo) != u4ContextId)
    {
        /* The given port does not belong to the given context */
        L2_UNLOCK ();
        return OSIX_FALSE;
    }

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return OSIX_FALSE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_TRUE)
    {
        OSIX_BITLIST_IS_BIT_SET (L2IWF_VLAN_EGRESS_PORTS (VlanId),
                                 u2LocalPort, sizeof (tLocalPortList), bResult);
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return bResult;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiIsVlanUntagMemberPort                         */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      port is a untag member of the given Vlan in the      */
/*                      given context.                                       */
/*                                                                           */
/* Input(s)           : ContextId, VlanId, Global IfIndex                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
L2IwfMiIsVlanUntagMemberPort (UINT4 u4ContextId, tVlanId VlanId,
                              UINT4 u4IfIndex)
{
    UINT2               u2LocalPort;
    tL2PortInfo        *pL2PortInfo = NULL;
    BOOL1               bResult = OSIX_FALSE;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return OSIX_FALSE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return OSIX_FALSE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return OSIX_FALSE;
    }

    u2LocalPort = L2IWF_IFENTRY_LOCAL_PORT (pL2PortInfo);

    if (L2IWF_IFENTRY_CONTEXT_ID (pL2PortInfo) != u4ContextId)
    {
        /* The given port does not belong to the given context */
        L2_UNLOCK ();
        return OSIX_FALSE;
    }

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return OSIX_FALSE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_TRUE)
    {
        OSIX_BITLIST_IS_BIT_SET (L2IWF_VLAN_UNTAG_PORTS (VlanId),
                                 u2LocalPort, sizeof (tLocalPortList), bResult);
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return bResult;
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdateVlanEgressPortList                        */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the given */
/*                      portlist to the Vlan's Egress port list in the L2Iwf */
/*                      common database. It also updates the PortVlan RBTree.*/
/*                                                                           */
/* Input(s)           : u4ContextId - Context to which the vlan belongs      */
/*                      VlanId      - Vlan whose Egress portlist is to be    */
/*                                    updated.                               */
/*                      pu1VlanName - Pointer to Vlan Name                   */
/*                      AddedPorts - Added members                           */
/*                      DeletedPorts - Deleted members                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfUpdateVlanEgressPortList (UINT4 u4ContextId, tVlanId VlanId,
                               UINT1 *pu1VlanName, tLocalPortList AddedPorts,
                               tLocalPortList DeletedPorts)
{
    UINT1              *pNullLocalPortList = NULL;
    tPortList          *pIfAddPortList = NULL;
    tPortList          *pIfDelPortList = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2LocalPort;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bValidAddPortList = OSIX_FALSE;
    BOOL1               bValidDelPortList = OSIX_FALSE;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return L2IWF_FAILURE;
    }

    pIfAddPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pIfAddPortList == NULL)
    {
        return L2IWF_FAILURE;
    }

    pIfDelPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pIfDelPortList == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pIfAddPortList);
        return L2IWF_FAILURE;
    }

    MEMSET (*pIfAddPortList, 0, sizeof (tPortList));
    MEMSET (*pIfDelPortList, 0, sizeof (tPortList));

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pIfAddPortList);
        FsUtilReleaseBitList ((UINT1 *) pIfDelPortList);
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) != OSIX_TRUE)
    {
        FsUtilReleaseBitList ((UINT1 *) pIfAddPortList);
        FsUtilReleaseBitList ((UINT1 *) pIfDelPortList);
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pNullLocalPortList = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pNullLocalPortList == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pIfAddPortList);
        FsUtilReleaseBitList ((UINT1 *) pIfDelPortList);
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    MEMSET (pNullLocalPortList, 0, sizeof (tLocalPortList));
    if (MEMCMP (AddedPorts, pNullLocalPortList, sizeof (tLocalPortList)) != 0)
    {
        bValidAddPortList = OSIX_TRUE;
    }

    if (MEMCMP (DeletedPorts, pNullLocalPortList, sizeof (tLocalPortList)) != 0)
    {
        bValidDelPortList = OSIX_TRUE;
    }
    UtilPlstReleaseLocalPortList (pNullLocalPortList);

    for (u2LocalPort = 1; u2LocalPort <= L2IWF_MAX_PORTS_PER_CONTEXT_EXT;
         u2LocalPort++)
    {
        if (L2IWF_PORT_ACTIVE (u2LocalPort) != OSIX_TRUE)
        {
            continue;
        }

        u4IfIndex = L2IWF_PORT_IFINDEX (u2LocalPort);

        if (bValidAddPortList == OSIX_TRUE)
        {
            OSIX_BITLIST_IS_BIT_SET (AddedPorts, u2LocalPort,
                                     sizeof (tLocalPortList), bResult);
            if (bResult == OSIX_TRUE)
            {
                OSIX_BITLIST_SET_BIT (L2IWF_VLAN_EGRESS_PORTS (VlanId),
                                      u2LocalPort, sizeof (tLocalPortList));
                OSIX_BITLIST_SET_BIT (L2IWF_PORT_ALL_VLAN (u2LocalPort), VlanId,
                                      VLAN_LIST_SIZE);

                /*If new member port is in forwarding state update vlan forward 
                 * count.
                 */
                L2IwfUpdateVlanFwdCount (VlanId, u2LocalPort, L2IWF_TRUE);
                OSIX_BITLIST_SET_BIT ((*pIfAddPortList), u4IfIndex,
                                      sizeof (tPortList));
            }
        }

        if (bValidDelPortList == OSIX_TRUE)
        {
            OSIX_BITLIST_IS_BIT_SET (DeletedPorts, u2LocalPort,
                                     sizeof (tLocalPortList), bResult);
            if (bResult == OSIX_TRUE)
            {
                /* If removed port was in vlan forward port list, decrement the
                 * vlan forward count.
                 */

                L2IwfUpdateVlanFwdCount (VlanId, u2LocalPort, L2IWF_FALSE);

                OSIX_BITLIST_RESET_BIT (L2IWF_VLAN_EGRESS_PORTS (VlanId),
                                        u2LocalPort, sizeof (tLocalPortList));
                OSIX_BITLIST_RESET_BIT (L2IWF_PORT_ALL_VLAN (u2LocalPort),
                                        VlanId, VLAN_LIST_SIZE);

                OSIX_BITLIST_SET_BIT ((*pIfDelPortList), u4IfIndex,
                                      sizeof (tPortList));
            }
        }
    }
#ifdef FSB_WANTED
    if (L2IWF_FCOE_VLAN_TYPE (VlanId) == FCOE_VLAN)
    {
        FsbPortListUpdateNotification (u4ContextId, VlanId, AddedPorts,
                                       DeletedPorts);
    }
#endif
    L2IwfReleaseContext ();

    L2_UNLOCK ();

    if (bValidAddPortList == OSIX_TRUE)
    {
        L2IwfAddPortVlanEntries (VlanId, *pIfAddPortList, pu1VlanName);
    }

    if (bValidDelPortList == OSIX_TRUE)
    {
        L2IwfDeletePortVlanEntries (VlanId, *pIfDelPortList);
    }
    if (L2IwfMiGetVlanInstMapping (u4ContextId, VlanId) == L2IWF_INIT_VAL)
    {
#ifdef PVRST_WANTED
        if (AstIsMstEnabledInContext (u4ContextId) != AST_TRUE)
        {
            PvrstMiVlanCreateIndication (u4ContextId, VlanId);
        }
#endif
    }
#ifdef ECFM_WANTED
    EcfmVlanPortMembershipIndication (u4ContextId, VlanId);
#endif

#ifdef MSTP_WANTED
    MstUpdateVlanPortList (u4ContextId, VlanId, AddedPorts, DeletedPorts);
#endif

    FsUtilReleaseBitList ((UINT1 *) pIfAddPortList);
    FsUtilReleaseBitList ((UINT1 *) pIfDelPortList);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdateVlanUntagPortList                         */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the given */
/*                      portlist to the Vlan's Untag port list in the L2Iwf  */
/*                      common database. It also updates the PortVlan RBTree.*/
/*                                                                           */
/* Input(s)           : u4ContextId - Context to which the vlan belongs      */
/*                      VlanId      - Vlan whose Egress portlist is to be    */
/*                                    updated.                               */
/*                      pu1VlanName - Pointer to Vlan Name                   */
/*                      AddedPorts - Added members                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfUpdateVlanUntagPortList (UINT4 u4ContextId, tVlanId VlanId,
                              UINT1 *pu1VlanName, tLocalPortList AddedPorts)
{
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;

    UNUSED_PARAM (pu1VlanName);

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    MEMCPY (L2IWF_VLAN_UNTAG_PORTS (VlanId), AddedPorts,
            sizeof (tLocalPortList));
    for (u2ByteInd = 0; u2ByteInd < CONTEXT_PORT_LIST_SIZE; u2ByteInd++)
    {
        if (AddedPorts[u2ByteInd] == 0)
        {
            continue;
        }
        u1PortFlag = AddedPorts[u2ByteInd];
        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & 0x80) != 0)
            {
                u2Port = (UINT2) ((u2ByteInd * BITS_PER_BYTE) + u2BitIndex + 1);
                if ((u2Port < L2IWF_MAX_PORTS_PER_CONTEXT_EXT)
                    && (L2IWF_PORT_INFO (u2Port) != NULL))
                {
                    OSIX_BITLIST_SET_BIT (L2IWF_PORT_UNTAGGED_VLAN (u2Port),
                                          VlanId, VLAN_LIST_SIZE);
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

#ifdef ECFM_WANTED
    EcfmVlanPortMembershipIndication (u4ContextId, VlanId);
#endif

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdateVlanFwdCount                              */
/*                                                                           */
/* Description        : This routine is called to update the vlan forward    */
/*                      count. If new member port is in forwarding or        */
/*                      learning state then set the vlan forward portlist and*/
/*                      increment the vlan forward count.                    */
/*                      If a port is removed from the vlan membership and    */
/*                      if the removed port was in vlan forward port list,   */
/*                      then reset the vlan forward port list and decrement  */
/*                      the vlan forward count.                              */
/*                                                                           */
/* Input(s)           : VlanId          - Vlan Id                            */
/*                      u2LocalPort     - Local Port number of the port      */
/*                      bVlanMemberFlag - Indicates whether to set or reset  */
/*                                        the vlan forward port list.        */
/*                                        If the value is L2IWF_TRUE then    */
/*                                        update the forward list depending  */
/*                                        upon the port state else reset the */
/*                                        portlist and decrement the count.  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfUpdateVlanFwdCount (tVlanId VlanId, UINT2 u2LocalPort,
                         BOOL1 bVlanMemberFlag)
{
    UINT2               u2MstInst;
    UINT1               u1Result = VLAN_FALSE;
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;

    if (u2LocalPort > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPort <= 0)
    {
        return;
    }

    if (bVlanMemberFlag == L2IWF_TRUE)
    {
        u2MstInst = L2IWF_VID_INST_MAP (VlanId);

        if (u2MstInst >= AST_MAX_MST_INSTANCES)
        {
            return;
        }

        if (L2IWF_PORT_INFO (u2LocalPort) != NULL)
        {
            u1PortState =
                L2IWF_IFENTRY_PORT_STATE (L2IWF_PORT_INFO (u2LocalPort),
                                          u2MstInst);
        }

        if ((u1PortState == AST_PORT_STATE_FORWARDING) ||
            (u1PortState == AST_PORT_STATE_LEARNING))
        {
            if (VlanId >= CONTEXT_PORT_LIST_SIZE || VlanId <= 0)
            {
                return;
            }
            L2IWF_VLAN_IS_MEMBER_PORT (L2IWF_VLAN_FORWARD_PORTS
                                       (VlanId), u2LocalPort, u1Result);
            if (u1Result != VLAN_TRUE)
            {
                OSIX_BITLIST_SET_BIT (L2IWF_VLAN_FORWARD_PORTS
                                      (VlanId), u2LocalPort,
                                      sizeof (tLocalPortList));

                L2IWF_FWD_PORT_COUNT (VlanId)++;
            }
        }
    }
    else
    {
        if (VlanId > CONTEXT_PORT_LIST_SIZE || VlanId <= 0)
        {
            return;
        }
        L2IWF_VLAN_IS_MEMBER_PORT (L2IWF_VLAN_FORWARD_PORTS
                                   (VlanId), u2LocalPort, u1Result);
        if (u1Result == VLAN_TRUE)
        {
            OSIX_BITLIST_RESET_BIT (L2IWF_VLAN_FORWARD_PORTS (VlanId),
                                    u2LocalPort, sizeof (tLocalPortList));

            L2IWF_FWD_PORT_COUNT (VlanId)--;
        }
    }
}

/*****************************************************************************/
/* Function Name      : L2IwfSetVlanEgressPort                               */
/*                                                                           */
/* Description        : This routine is called from VLAN to add the given    */
/*                      port to the Vlan's Egress port list in the L2Iwf     */
/*                      common database. It also updates the PortVlan RBTree.*/
/*                                                                           */
/* Input(s)           : u4ContextId - Context to which the vlan belongs      */
/*                      VlanId      - Vlan whose Egress portlist is to be    */
/*                                    updated.                               */
/*                      u2LocalPortId - Local Port number of the port to be  */
/*                                    added to the Egress port list          */
/*                      pu1VlanName - Pointer to Vlan Name                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVlanEgressPort (UINT4 u4ContextId, tVlanId VlanId, UINT1 *pu1VlanName,
                        UINT2 u2LocalPortId, UINT1 u1IsUntagged)
{
    UINT4               u4IfIndex = 0;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return L2IWF_FAILURE;
    }
    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    OSIX_BITLIST_SET_BIT (L2IWF_VLAN_EGRESS_PORTS (VlanId), u2LocalPortId,
                          sizeof (tLocalPortList));

    if (L2IWF_PORT_INFO (u2LocalPortId) != NULL)
    {
        OSIX_BITLIST_SET_BIT (L2IWF_PORT_ALL_VLAN (u2LocalPortId), VlanId,
                              VLAN_LIST_SIZE);
    }

    if (u1IsUntagged == L2IWF_TRUE)
    {
        OSIX_BITLIST_SET_BIT (L2IWF_VLAN_UNTAG_PORTS (VlanId), u2LocalPortId,
                              sizeof (tLocalPortList));

        if (L2IWF_PORT_INFO (u2LocalPortId) != NULL)
        {
            OSIX_BITLIST_SET_BIT (L2IWF_PORT_UNTAGGED_VLAN (u2LocalPortId),
                                  VlanId, VLAN_LIST_SIZE);
        }
    }
    /*If new member port is in forwarding state update vlan forward 
     * count.
     */
    L2IwfUpdateVlanFwdCount (VlanId, u2LocalPortId, L2IWF_TRUE);

    if (L2IWF_PORT_INFO (u2LocalPortId) != NULL)
    {
        u4IfIndex = L2IWF_PORT_IFINDEX (u2LocalPortId);
    }

#ifdef FSB_WANTED
    if (L2IWF_VLAN_INFO (VlanId) != NULL)
    {
        if (L2IWF_FCOE_VLAN_TYPE (VlanId) == FCOE_VLAN)
        {
            FsbPortUpdateNotification (u4ContextId, VlanId, u2LocalPortId);
        }
    }
#endif

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    L2IwfUpdatePortVlanEntry (u4IfIndex, VlanId, pu1VlanName, L2IWF_ADD);

#ifdef ECFM_WANTED
    EcfmVlanPortMembershipIndication (u4ContextId, VlanId);
#endif

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfResetVlanEgressPort                             */
/*                                                                           */
/* Description        : This routine is called from VLAN to remove the given */
/*                      port from the Vlan's Egress port list in the L2Iwf   */
/*                      common database. It also updates the PortVlan RBTree */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      VlanId      - Vlan whose Egress portlist is to be    */
/*                                    updated.                               */
/*                      u2LocalPortId - Local Port number of the port to be  */
/*                                    added to the Egress port list          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfResetVlanEgressPort (UINT4 u4ContextId, tVlanId VlanId,
                          UINT2 u2LocalPortId)
{
#ifdef FSB_WANTED
    tLocalPortList      AddedPorts;
    tLocalPortList      DeletedPorts;
#endif
    UINT4               u4IfIndex = 0;

#ifdef FSB_WANTED
    MEMSET (AddedPorts, 0, sizeof (tLocalPortList));
    MEMSET (DeletedPorts, 0, sizeof (tLocalPortList));
#endif

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* If removed port was in vlan forward port list, decriment the vlan
     * forward count.
     */

    L2IwfUpdateVlanFwdCount (VlanId, u2LocalPortId, L2IWF_FALSE);

    OSIX_BITLIST_RESET_BIT (L2IWF_VLAN_UNTAG_PORTS (VlanId), u2LocalPortId,
                            sizeof (tLocalPortList));

    OSIX_BITLIST_RESET_BIT (L2IWF_VLAN_EGRESS_PORTS (VlanId), u2LocalPortId,
                            sizeof (tLocalPortList));

    if (L2IWF_PORT_INFO (u2LocalPortId) != NULL)
    {
        u4IfIndex = L2IWF_PORT_IFINDEX (u2LocalPortId);
        OSIX_BITLIST_RESET_BIT (L2IWF_PORT_UNTAGGED_VLAN (u2LocalPortId),
                                VlanId, VLAN_LIST_SIZE);
        OSIX_BITLIST_RESET_BIT (L2IWF_PORT_ALL_VLAN (u2LocalPortId), VlanId,
                                VLAN_LIST_SIZE);
    }

#ifdef FSB_WANTED
    if (L2IWF_FCOE_VLAN_TYPE (VlanId) == FCOE_VLAN)
    {
        OSIX_BITLIST_SET_BIT (DeletedPorts, u4IfIndex, sizeof (tLocalPortList));

        FsbPortListUpdateNotification (u4ContextId, VlanId, AddedPorts,
                                       DeletedPorts);
    }
#endif

    L2IwfReleaseContext ();

    L2_UNLOCK ();
    /* Update the PortVlanTable RBTree */
    L2IwfUpdatePortVlanEntry (u4IfIndex, VlanId, NULL, L2IWF_DELETE);

#ifdef ECFM_WANTED
    EcfmVlanPortMembershipIndication (u4ContextId, VlanId);
#endif

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetBridgeMode                                   */
/*                                                                           */
/* Description        : This function returns the Bridge mode configured     */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : pu4BridgeMode - Bridge mode configured in L2IWF      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu4BridgeMode = L2IWF_BRIDGE_MODE ();

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetBridgeMode                                   */
/*                                                                           */
/* Description        : This function sets the Bridge mode in L2IWF.         */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context ID                            */
/*                      i4BridgeMode - Bridge mode to be set in L2IWF        */
/*                      This takes one of the following values               */
/*                      CustomerBridge/ProviderEdgeBridge/ProviderCoreBridge */
/*                      ProviderBridge                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfSetBridgeMode (UINT4 u4ContextId, UINT4 u4BridgeMode)
{
    INT4                i4PrevBridgeMode;

#ifdef ISS_WANTED
    INT4                i4IncrStatus;
    INT4                i4AutoSaveStatus;
#endif
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    i4PrevBridgeMode = (INT4) L2IWF_BRIDGE_MODE ();
    L2IWF_BRIDGE_MODE () = u4BridgeMode;

    if (L2IwfPbInitMemPools () != L2IWF_SUCCESS)
    {
        L2IWF_BRIDGE_MODE () = (UINT4) i4PrevBridgeMode;
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_CNP_COUNT () = L2IWF_INIT_VAL;

    L2IwfReleaseContext ();
    L2_UNLOCK ();

#ifdef ISS_WANTED
    if (u4ContextId == L2IWF_DEFAULT_CONTEXT)
    {
        IssSetBridgeModeToNvRam (u4BridgeMode);

        /* if incremental save is on and auto save is on, do not
         * reset the RestoreOption as the configuration changes
         * will be immediately saved to the configuration file */
        i4IncrStatus = IssGetIncrementalSaveStatus ();
        i4AutoSaveStatus = IssGetAutoSaveStatus ();

        if ((i4IncrStatus == ISS_FALSE) || (i4AutoSaveStatus == ISS_FALSE))
        {
            IssSetConfigRestoreOption (ISS_CONFIG_NO_RESTORE);
        }
    }
#endif

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetDefaultVlanId                                */
/*                                                                           */
/* Description        : This function returns the Default Vlan Id            */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Default Vlan Id                                      */
/*****************************************************************************/
VOID
L2IwfGetDefaultVlanId (UINT2 *pDefaultVlanId)
{
    L2_LOCK ();
    *pDefaultVlanId = L2IWF_DEFAULT_VLAN_ID ();
    L2_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : L2IwfSetDefaultVlanId                                */
/*                                                                           */
/* Description        : This function sets the Default Vlan Id in L2IWF.     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Default Vlan Id                                      */
/*****************************************************************************/
VOID
L2IwfSetDefaultVlanId (UINT2 u2DefaultVlanId)
{
    L2_LOCK ();
    L2IWF_DEFAULT_VLAN_ID () = u2DefaultVlanId;
    L2_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : L2IwfIsVlanActive                                    */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      Vlan is active.                                      */
/*                                                                           */
/* Input(s)           : VlanId                                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
L2IwfIsVlanActive (tVlanId VlanId)
{
    return L2IwfMiIsVlanActive (L2IWF_DEFAULT_CONTEXT, VlanId);
}

/*****************************************************************************/
/* Function Name      : L2IwfMiIsVlanActive                                  */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      Vlan is active.                                      */
/*                                                                           */
/* Input(s)           : ContextId, VlanId                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
L2IwfMiIsVlanActive (UINT4 u4ContextId, tVlanId VlanId)
{
    BOOL1               bRetval;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return OSIX_FALSE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return OSIX_FALSE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return OSIX_FALSE;
    }

    bRetval = L2IWF_VLAN_ACTIVE (VlanId);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return bRetval;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetNextActiveVlan                               */
/*                                                                           */
/* Description        : This routine returns the next active vlan in the     */
/*                      system. This is used by the AST module  to know the  */
/*                      active vlans in the system.                          */
/*                                                                           */
/* Input(s)           : u2VlanId    - Vlan Index whose next vlan is to be    */
/*                                    determined                             */
/*                                                                           */
/* Output(s)          : *pu2NextVlanId -  pointer to the next active VlanId  */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfGetNextActiveVlan (UINT2 u2VlanId, UINT2 *pu2NextVlanId)
{
    return L2IwfMiGetNextActiveVlan (L2IWF_DEFAULT_CONTEXT, u2VlanId,
                                     pu2NextVlanId);
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetNextActiveVlan                             */
/*                                                                           */
/* Description        : This routine returns the next active vlan in the     */
/*                      system. This is used by the AST module  to know the  */
/*                      active vlans in the system.                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2VlanId    - Vlan Index whose next vlan is to be    */
/*                                    determined                             */
/*                                                                           */
/* Output(s)          : *pu2NextVlanId -  pointer to the next active VlanId  */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfMiGetNextActiveVlan (UINT4 u4ContextId, UINT2 u2VlanId,
                          UINT2 *pu2NextVlanId)
{
    INT4                i4VlanIndex;
    INT4                i4Retval = L2IWF_FAILURE;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    for (i4VlanIndex = u2VlanId + 1; i4VlanIndex <= VLAN_DEV_MAX_VLAN_ID;
         i4VlanIndex++)
    {
        if (L2IWF_VLAN_ACTIVE (i4VlanIndex) == OSIX_TRUE)
        {
            *pu2NextVlanId = (UINT2) i4VlanIndex;
            i4Retval = L2IWF_SUCCESS;
            break;
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return i4Retval;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfSetProtocolTunnelStatusOnPort               */
/*                                                                           */
/*    Description         : This function sets the tunnel status (Tunnel/    */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) on a port        */
/*                                                                           */
/*    Input(s)            : u2Port         - Port number                     */
/*                          u2Protocol     - L2 Protocol                     */
/*                          u1TunnelStatus - Protocol tunnel status          */
/*                                           on a port.                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : L2IWF_SUCCESS / L2IWF_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                    UINT2 u2Protocol, UINT1 u1TunnelStatus)
{
    UINT1               u1PrevTunnelStatus;
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2Port > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2Port <= 0)
    {
        return L2IWF_FAILURE;
    }
    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_INFO (u2Port) == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    switch (u2Protocol)
    {
        case L2_PROTO_ELMI:
            L2IWF_ELMI_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_LLDP:
            L2IWF_LLDP_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_DOT1X:
            L2IWF_DOT1X_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_LACP:
            L2IWF_LA_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) = u1TunnelStatus;
            break;
        case L2_PROTO_STP:
            u1PrevTunnelStatus =
                L2IWF_STP_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port));
            switch (u1PrevTunnelStatus)
            {
                case VLAN_TUNNEL_PROTOCOL_PEER:
                    DECR_L2IWF_CONTEXT_PEER_TUNNEL_INFO (u4ContextId);
                    break;

                case VLAN_TUNNEL_PROTOCOL_TUNNEL:
                    DECR_L2IWF_CONTEXT_TUNNEL_TUNNEL_INFO (u4ContextId);
                    break;

                case VLAN_TUNNEL_PROTOCOL_DISCARD:
                    DECR_L2IWF_CONTEXT_DISCARD_TUNNEL_INFO (u4ContextId);
                    break;

                case VLAN_TUNNEL_PROTOCOL_INVALID:
                    break;

                default:
                    break;

            }
            L2IWF_STP_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) = u1TunnelStatus;
            switch (u1TunnelStatus)
            {
                case VLAN_TUNNEL_PROTOCOL_PEER:
                    INCR_L2IWF_CONTEXT_PEER_TUNNEL_INFO (u4ContextId);
                    break;

                case VLAN_TUNNEL_PROTOCOL_TUNNEL:
                    INCR_L2IWF_CONTEXT_TUNNEL_TUNNEL_INFO (u4ContextId);
                    break;

                case VLAN_TUNNEL_PROTOCOL_DISCARD:
                    INCR_L2IWF_CONTEXT_DISCARD_TUNNEL_INFO (u4ContextId);
                    break;

                case VLAN_TUNNEL_PROTOCOL_INVALID:
                    break;
                default:
                    break;
            }
            break;
        case L2_PROTO_GVRP:
            L2IWF_GVRP_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_GMRP:
            L2IWF_GMRP_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_MVRP:
            L2IWF_MVRP_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_MMRP:
            L2IWF_MMRP_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_IGMP:
            L2IWF_IGMP_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_ECFM:
            L2IWF_ECFM_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_EOAM:
            L2IWF_EOAM_TUNNEL_STATUS (L2IWF_PORT_INFO (u2Port)) =
                u1TunnelStatus;
            break;
        default:
            break;
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetProtocolTunnelStatusOnPort               */
/*                                                                           */
/*    Description         : This function returns the tunnel status (Tunnel/ */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) on a port        */
/*                                                                           */
/*    Input(s)            : u2Port - Port number                             */
/*                          u2Protocol     - L2 Protocol                     */
/*                                                                           */
/*    Output(s)           : u1TunnelStatus - Protocol Tunnel status          */
/*                                                                           */
/*    Returns             : None.                                            */
/*                                                                           */
/*****************************************************************************/
VOID
L2IwfGetProtocolTunnelStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                    UINT1 *pu1TunnelStatus)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return;
    }

    L2_LOCK ();
    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return;
    }

    switch (u2Protocol)
    {
        case L2_PROTO_ELMI:
            *pu1TunnelStatus = L2IWF_ELMI_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_LLDP:
            *pu1TunnelStatus = L2IWF_LLDP_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_DOT1X:
            *pu1TunnelStatus = L2IWF_DOT1X_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_LACP:
            *pu1TunnelStatus = L2IWF_LA_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_STP:
            *pu1TunnelStatus = L2IWF_STP_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_GVRP:
            *pu1TunnelStatus = L2IWF_GVRP_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_GMRP:
            *pu1TunnelStatus = L2IWF_GMRP_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_IGMP:
            *pu1TunnelStatus = L2IWF_IGMP_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_MVRP:
            *pu1TunnelStatus = L2IWF_MVRP_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_MMRP:
            *pu1TunnelStatus = L2IWF_MMRP_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_ECFM:
            *pu1TunnelStatus = L2IWF_ECFM_TUNNEL_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_EOAM:
            *pu1TunnelStatus = L2IWF_EOAM_TUNNEL_STATUS (pL2PortInfo);
            break;
        default:
            break;
    }

    L2_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetPortType                                   */
/*                                                                           */
/* Description        : This function is used to set the port type of a      */
/*                      Physical port in L2IWF                               */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                      u2Port      - port Index.                            */
/*                      i4PortType  - Port type                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfPbSetPortType (UINT4 u4IfIndex, INT4 i4PortType)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_PB_PORT_TYPE (pL2PortEntry) = (UINT2) i4PortType;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPbPortType                                   */
/*                                                                           */
/* Description        : This function is used to get the port type of a      */
/*                      Physical port                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                      u4IfIndex   - IfIndex of the port.                   */
/*                                                                           */
/* Output(s)          : *pu1PortType - CEP/CNP/PNP/PCEP/PCNP/PPNP            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfGetPbPortType (UINT4 u4IfIndex, UINT1 *pu1PortType)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1PortType = (UINT1) L2IWF_PB_PORT_TYPE (pL2PortEntry);

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsDot1xConfigAllowed                            */
/*                                                                           */
/* Description        : This routine checks whether dot1x can be enabled on  */
/*                      this port.                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE/ L2IWF_FALSE                              */
/*****************************************************************************/
INT4
L2IwfIsDot1xConfigAllowed (UINT4 u4IfIndex)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    UINT4               u4BridgeMode;
    UINT1               u1RetVal = L2IWF_TRUE;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        u1RetVal = L2IWF_FALSE;
        L2_UNLOCK ();
        return u1RetVal;
    }

    if (L2IwfSelectContext (pL2PortEntry->u4ContextId) == L2IWF_FAILURE)
    {
        u1RetVal = L2IWF_FALSE;
        L2_UNLOCK ();
        return u1RetVal;
    }

    u4BridgeMode = L2IWF_BRIDGE_MODE ();

    L2IwfReleaseContext ();

    if (u4BridgeMode == L2IWF_PROVIDER_BRIDGE_MODE)
    {
        if (L2IWF_IFENTRY_TUNNEL_STATUS (pL2PortEntry) == OSIX_TRUE)
        {
            u1RetVal = L2IWF_FALSE;
        }
    }
    else if ((u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE) ||
             (u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE))
    {
        if (L2IWF_DOT1X_TUNNEL_STATUS (pL2PortEntry) ==
            VLAN_TUNNEL_PROTOCOL_TUNNEL)
        {
            u1RetVal = L2IWF_FALSE;
        }
    }

    L2_UNLOCK ();
    return u1RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsIgmpTunnelEnabledOnAnyPort                    */
/*                                                                           */
/* Description        : This routine checks whether Igmp tunneling is enabled*/
/*                      on any of the port in the bridge.                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE/ L2IWF_FALSE                              */
/*****************************************************************************/
INT4
L2IwfIsIgmpTunnelEnabledOnAnyPort (UINT4 u4ContextId)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    INT4                i4Index = 0;
    UINT2               u2LocalPort = 0;

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();
        return L2IWF_TRUE;
    }

    for (i4Index = u2LocalPort + 1; i4Index <= L2IWF_MAX_PORTS_PER_CONTEXT_EXT;
         i4Index++)
    {
        if (L2IWF_PORT_ACTIVE (i4Index) == OSIX_TRUE)
        {
            pL2PortEntry = L2IWF_PORT_INFO (i4Index);

            if ((L2IWF_IGMP_TUNNEL_STATUS (pL2PortEntry) ==
                 VLAN_TUNNEL_PROTOCOL_TUNNEL) ||
                (L2IWF_IGMP_TUNNEL_STATUS (pL2PortEntry) ==
                 VLAN_TUNNEL_PROTOCOL_DISCARD))
            {
                L2IwfReleaseContext ();
                L2_UNLOCK ();
                return L2IWF_TRUE;
            }
        }
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_FALSE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetFwdPortCount                                 */
/*                                                                           */
/* Description        : This routine is called from VLAN to get the number   */
/*                      ports are in forwarding state                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2VlanId - Vlan Index                                */
/*                      *pFwdPortCount - Number of port for given vlan are   */
/*                      in forwarding state.                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetFwdPortCount (UINT4 u4ContextId, tVlanId u2VlanId, UINT2 *pFwdPortCount)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2VlanId > VLAN_DEV_MAX_VLAN_ID || u2VlanId <= 0)
    {
        return OSIX_FALSE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (u2VlanId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;

    }

    *pFwdPortCount = L2IWF_FWD_PORT_COUNT (u2VlanId);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetInterfaceType                                */
/*                                                                           */
/* Description        : This function returns the Interface Type configured  */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : pu1InterfaceType - Interface Type configured in L2IWF*/
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfGetInterfaceType (UINT4 u4ContextId, UINT1 *pu1InterfaceType)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1InterfaceType = L2IWF_INTERFACE_TYPE ();

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetInterfaceType                                */
/*                                                                           */
/* Description        : This function sets the Interface Type in L2IWF.      */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context ID                            */
/*                      u1InterfaceType - Interface Type to be set in L2IWF  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfSetInterfaceType (UINT4 u4ContextId, UINT1 u1InterfaceType)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_INTERFACE_TYPE () = u1InterfaceType;

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetAdminIntfTypeFlag                            */
/*                                                                           */
/* Description        : This function returns the Interface Type configured  */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : pu1AdminIntfTypeFlag - Admin Interface Type Flag     */
/*                                               configured in L2IWF         */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfGetAdminIntfTypeFlag (UINT4 u4ContextId, UINT1 *pu1AdminIntfTypeFlag)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1AdminIntfTypeFlag = L2IWF_ADMIN_INTERFACE_TYPE_FLAG ();

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetAdminIntfTypeFlag                            */
/*                                                                           */
/* Description        : This function sets the Interface Type in L2IWF.      */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context ID                            */
/*                      u1AdminIntfTypeFlag - Admin Interface Type Flag to   */
/*                                              be set in L2IWF              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfSetAdminIntfTypeFlag (UINT4 u4ContextId, UINT1 u1AdminIntfTypeFlag)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_ADMIN_INTERFACE_TYPE_FLAG () = u1AdminIntfTypeFlag;

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetInternalPortCount                            */
/*                                                                           */
/* Description        : This function returns the Internal port count        */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : pu4Count - Count of no. of internal ports            */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfGetInternalPortCount (UINT4 u4ContextId, UINT4 *pu4Count)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu4Count = L2IWF_INTERNAL_COUNT ();

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetCnpPortCount                                 */
/*                                                                           */
/* Description        : This function returns the CNP port count             */
/*                      in L2IWF.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : pu4Count - Count of no. of internal ports            */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfGetCnpPortCount (UINT4 u4ContextId, UINT4 *pu4Count)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu4Count = L2IWF_CNP_COUNT ();

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfChangeBridgeModeChange                          */
/*                                                                           */
/* Description        : This function sends indication to PBB in case of     */
/*                      bridge mode change                                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Context ID                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfChangeBridgeModeChange (UINT4 u4ContextId)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }
    /*Note: For threads which are not handling any protocol messages like
     *      timer expiry, pdu processing etc can signal delete/create context
     *      for bridge mode change handing whereas for the other threads we
     *      need to have single event processing only.
     */
#ifdef PBB_WANTED
    PbbDeleteContext (u4ContextId);
    if (PbbCreateContext (u4ContextId) != PBB_SUCCESS)
    {
        return L2IWF_FAILURE;
    }
#endif
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanFloodingStatus                           */
/*                                                                           */
/* Description        : This routine is called to get the vlan flooding      */
/*                      status.                                              */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2VlanId - Vlan Index                                */
/*                                                                           */
/* Output(s)          : pu1VlanFloodStatus - VLAN_ENABLED                    */
/*                                           VLAN_DISABLED                   */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetVlanFloodingStatus (UINT4 u4ContextId, tVlanId u2VlanId,
                            UINT1 *pu1VlanFloodStatus)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2VlanId > VLAN_DEV_MAX_VLAN_ID || u2VlanId <= 0)
    {
        return OSIX_FALSE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (u2VlanId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    /* Check if the Vlan Flooding disabled bit is set */
    if ((L2IWF_VLAN_FLAGS (u2VlanId) & L2IWF_VLAN_FLOOD_DISABLED)
        == L2IWF_VLAN_FLOOD_DISABLED)
    {
        *pu1VlanFloodStatus = VLAN_DISABLED;
    }
    else
    {
        *pu1VlanFloodStatus = VLAN_ENABLED;
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetVlanFloodingStatus                           */
/*                                                                           */
/* Description        : This routine is called to set/unset the flooding     */
/*                      status of a vlan.                                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u2VlanId - Vlan Index                                */
/*                      u1VlanFloodStatus - VLAN_DISABLED/                   */
/*                                          VLAN_ENABLED                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVlanFloodingStatus (UINT4 u4ContextId, tVlanId u2VlanId,
                            UINT1 u1VlanFloodStatus)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2VlanId > VLAN_DEV_MAX_VLAN_ID || u2VlanId <= 0)
    {
        return OSIX_FALSE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (u2VlanId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (u1VlanFloodStatus == VLAN_DISABLED)
    {
        /* Set the Vlan Flooding disabled bit */
        L2IWF_VLAN_FLAGS (u2VlanId) |= L2IWF_VLAN_FLOOD_DISABLED;
    }
    else
    {
        /* Reset the Vlan Flooding disabled bit */
        L2IWF_VLAN_FLAGS (u2VlanId) =
            (UINT1) (L2IWF_VLAN_FLAGS (u2VlanId) &
                     (~(L2IWF_VLAN_FLOOD_DISABLED)));
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdatePortDeiBit                                */
/*                                                                           */
/* Description        : This routine is called from VLAN to update the       */
/*                      Port Dei Bit Value in the L2Iwf common database.     */
/*                                                                           */
/* Input(s)           : u4IfIndex   - Actual Port Number                     */
/*                      u1DeiBitValue    - Dei Bit Value                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfUpdatePortDeiBit (UINT4 u4IfIndex, UINT1 u1DeiBitValue)
{

    if (CFA_IFINDEX_IS_VALID_IFACE (u4IfIndex) == FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    L2IwfReleaseContext ();

    L2_UNLOCK ();
#ifdef ECFM_WANTED
    EcfmPortDeiBitChangeIndication (u4IfIndex, u1DeiBitValue);
#else
    UNUSED_PARAM (u1DeiBitValue);
#endif
    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortVlanListFromIfIndex                      */
/*                                                                           */
/* Description        : This routine is called to get the vlan list for the  */
/*                      given IfIndex.                                       */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id                           */
/*                      u4IfIndex     - IfIndex                              */
/*                      u1VlanPortType - Port Vlan Type                      */
/*                      u1VlanPortType values may be                         */
/*                                   1. UnTagged member port -0              */
/*                                   2. tagged member port - 1               */
/*                                   3. member port (tagged or untagged)-2   */
/*                                                                           */
/* Output(s)          : pVlanIdList  -Vlan List                              */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetPortVlanListFromIfIndex (UINT4 u4ContextId,
                                 tSNMP_OCTET_STRING_TYPE * pVlanIdList,
                                 UINT4 u4IfIndex, UINT1 u1VlanPortType)
{
    tL2PortInfo        *pL2PortInfo = NULL;
    UINT2               u2LocalPort;

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    u2LocalPort = L2IWF_IFENTRY_LOCAL_PORT (pL2PortInfo);

    L2_UNLOCK ();

    if (L2IwfGetPortVlanList (u4ContextId, pVlanIdList, u2LocalPort,
                              u1VlanPortType) != L2IWF_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortVlanList                                 */
/*                                                                           */
/* Description        : This routine is called to get the vlan list.if       */
/*                      argument u1VlanPortType is Tagged then this function */
/*                      will return the List of vlans for which this port is */
/*                      untagged Member.If the u1VlanPortType is untagged or */
/*                      mamber port then this functiion will return the list */
/*                      of vlans for which port is untagged memberor both    */
/*                      tagged & untagged List                               */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id                           */
/*                      u2LocalPortId- Local Port Number                     */
/*                      u1VlanPortType - Port Vlan Type                      */
/*                      u1VlanPortType values may be                         */
/*                                   1. UnTagged member port -0              */
/*                                   2. tagged member port - 1               */
/*                                   3. member port (tagged or untagged)-2   */
/*                                                                           */
/* Output(s)          : pVlanIdList  -Vlan List                              */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetPortVlanList (UINT4 u4ContextId, tSNMP_OCTET_STRING_TYPE * pVlanIdList,
                      UINT2 u2LocalPortId, UINT1 u1VlanPortType)
{
    UINT2               u2ByteInd = 0;

    if ((pVlanIdList == NULL) || (pVlanIdList->pu1_OctetList == NULL))
    {
        return L2IWF_FAILURE;
    }
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)

    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPortId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    if (u1VlanPortType == L2_UNTAGGED_MEMBER_PORT)
    {
        MEMCPY (pVlanIdList->pu1_OctetList,
                L2IWF_PORT_UNTAGGED_VLAN (u2LocalPortId), VLAN_LIST_SIZE);
    }
    else if (u1VlanPortType == L2_MEMBER_PORT)
    {
        MEMCPY (pVlanIdList->pu1_OctetList,
                L2IWF_PORT_ALL_VLAN (u2LocalPortId), VLAN_LIST_SIZE);
    }
    else
    {
        for (u2ByteInd = 0; u2ByteInd < VLAN_LIST_SIZE; u2ByteInd++)
        {

            pVlanIdList->pu1_OctetList[u2ByteInd] =
                (UINT1) ((~(L2IWF_PORT_UNTAGGED_VLAN (u2LocalPortId))
                          [u2ByteInd]) &
                         ((L2IWF_PORT_ALL_VLAN (u2LocalPortId))[u2ByteInd]));
        }
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name        : L2IwfGetPortVlanMemberList                         */
/*                                                                           */
/* Description          : Returns the List of VLANs for which the particular */
/*                        port is member of.                                 */
/*                                                                           */
/* Input (s)            : u2VlanId - VLAN ID value.                          */
/*                        pu1VlanList - VLAN List                            */
/*                                                                           */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE.                     */
/*****************************************************************************/
INT4
L2IwfGetPortVlanMemberList (UINT4 u4IfIndex, UINT1 *pu1VlanList)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IwfGetPortVlanMembers (u4IfIndex, pu1VlanList) == L2IWF_SUCCESS)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }
    L2_UNLOCK ();
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetAllToOneBndlStatus                           */
/*                                                                           */
/* Description        : This routine is called from VLAN to get the          */
/*                      All To One Bundling Status from L2IWF COntext Info   */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                                                                           */
/* Output(s)          : pu1Status - All to one bundling status               */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetAllToOneBndlStatus (UINT4 u4ContextId, UINT1 *pu1Status)
{
    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1Status = (UINT1) L2IWF_ALL_TO_ONE_BUNDLING_STATUS ();

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsBridgeModeChangeAllowed                       */
/*                                                                           */
/* Description        : This routine is called from VLAN to get the          */
/*                      information from L2IWF if Bridge mode change is      */
/*                      allowed or not. This Call queries all the dependent  */
/*                      module APIs to know if Bridge Mode change is allowed */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Id                             */
/*                      u4BrgMode   - New Bridge Mode                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE/L2IWF_FALSE/L2IWF_PBB_FALSE               */
/*****************************************************************************/
INT1
L2IwfIsBridgeModeChangeAllowed (UINT4 u4ContextId, UINT4 u4BrgMode)
{
    INT1                i1RetVal = VCM_FALSE;

    /* Module ShutDown Dependency - These Modules must be shut down 
     * in the context to change the bridge mode.
     */
    if ((AstIsRstStartedInContext (u4ContextId)) ||
        (AstIsPvrstStartedInContext (u4ContextId)) ||
        (AstIsMstStartedInContext (u4ContextId)))
    {
        return L2IWF_FALSE;
    }

    if (MrpApiIsMrpStarted (u4ContextId) == OSIX_TRUE)
    {
        return L2IWF_FALSE;
    }

    if (GarpIsGarpEnabledInContext (u4ContextId) == GARP_TRUE)
    {
        return L2IWF_FALSE;
    }

    if ((u4BrgMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE) ||
        (u4BrgMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE))
    {
        if (SnoopIsIgmpSnoopingEnabled (u4ContextId) == SNOOP_ENABLED)
        {
            return L2IWF_FALSE;
        }
    }

    if ((EcfmStartedInContext (u4ContextId)) ||
        (ElpsApiIsElpsStartedInContext (u4ContextId) == OSIX_TRUE))
    {
        return L2IWF_FALSE;
    }

#ifdef ERPS_WANTED
    if (ErpsApiIsErpsStartedInContext (u4ContextId) == OSIX_TRUE)
    {
        return L2IWF_FALSE;
    }
#endif

    /* Start of Other Specific Dependencies for the Bridge mode change 
     */
    i1RetVal = VcmSispIsSispPortPresentInCtxt (u4ContextId);

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FALSE;
    }

    /* PBB must be started globally to change the Bridge mode to one of 
     * the PBB Bridge modes I-Component BEB or B-Component BEB
     */
    if ((u4BrgMode == L2IWF_PBB_ICOMPONENT_BRIDGE_MODE) ||
        (u4BrgMode == L2IWF_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        if (L2IWF_PBB_SHUTDOWN_STATUS () == L2IWF_TRUE)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_PBB_FALSE;
        }
    }

    /* When SISP enabled ports or SISP ports are present in a
     * context, then the bridge mode change will be allowed only 
     * from PCB to PEB and vice-versa */

    if (i1RetVal == VCM_TRUE)
    {
        switch (L2IWF_BRIDGE_MODE ())
        {
            case L2IWF_PROVIDER_EDGE_BRIDGE_MODE:
            case L2IWF_PROVIDER_CORE_BRIDGE_MODE:

                if ((u4BrgMode ==
                     L2IWF_PROVIDER_EDGE_BRIDGE_MODE)
                    || (u4BrgMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
                {
                    break;
                }

            default:

                L2IwfReleaseContext ();
                L2_UNLOCK ();
                return L2IWF_FALSE;
        }
    }

    /* Check for PBB Edge Bridge Modes - If any Internal port (CBP or PIP)is 
     * present, then they need to be deleted before changing the 
     * bridge mode from one of PBB bridge modes
     */
    if ((L2IWF_BRIDGE_MODE () == L2IWF_PBB_ICOMPONENT_BRIDGE_MODE) ||
        (L2IWF_BRIDGE_MODE () == L2IWF_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        if (L2IWF_INTERNAL_COUNT () != 0)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_FALSE;
        }
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_TRUE;
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdateEnhFilterStatus                           */
/*                                                                           */
/* Description        : This routine updates the status of Enhanced Filtering*/
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id to which this vlan belongs*/
/*                      bEnhFilterStatus - Status of Enhanced Filtering      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfUpdateEnhFilterStatus (UINT4 u4ContextId, BOOL1 b1EnhFilterStatus)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_VLAN_ENH_FILTERING_STATUS () = b1EnhFilterStatus;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanLearningType                             */
/*                                                                           */
/* Description        : This routine is called to get the vlan learning      */
/*                      type.                                                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Learning Type                                        */
/*****************************************************************************/
UINT1
L2IwfGetVlanLearningType (UINT4 u4ContextId)
{
    UINT1               u1LearnMode;

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return VLAN_INVALID_LEARNING;
    }

    u1LearnMode = L2IWF_VLAN_LEARNING_MODE ();

    L2IwfReleaseContext ();

    L2_UNLOCK ();
    return u1LearnMode;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetVlanLearningType                             */
/*                                                                           */
/* Description        : This routine is called to set the vlan learning      */
/*                      type.                                                */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch ID                      */
/*                      u1LearnMode - Learning type                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetVlanLearningType (UINT4 u4ContextId, UINT1 u1LearnMode)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_VLAN_LEARNING_MODE () = u1LearnMode;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetOverrideOption                               */
/*                                                                           */
/* Description        : This routine is called to set the override           */
/*                      option.                                              */
/*                                                                           */
/* Input(s)           : u2Port      - Port ID                                */
/*                      u1OverrideOption - OverrideOption                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetOverrideOption (UINT4 u4Port, UINT1 u1OverrideOption)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4Port);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_OVERRIDE_OPTION_STATUS (pL2PortInfo) = u1OverrideOption;

    L2_UNLOCK ();

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfGetOverrideOption                               */
/*                                                                           */
/* Description        : This routine is called to get the override           */
/*                      option status.                                       */
/*                                                                           */
/* Input(s)           : u2Port      - Port ID                                */
/*                      u1OverrideOption - OverrideOption                    */
/*                                                                           */
/* Output(s)          : pu1OverrideOption - OverrideOption                    */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
VOID
L2IwfGetOverrideOption (UINT4 u4Port, UINT1 *pu1OverrideOption)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    L2_LOCK ();
    pL2PortInfo = L2IwfGetIfIndexEntry (u4Port);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return;
    }

    *pu1OverrideOption = L2IWF_OVERRIDE_OPTION_STATUS (pL2PortInfo);
    L2_UNLOCK ();
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfSetProtocolTunnelStatusPerVlan              */
/*                                                                           */
/*    Description         : This function sets the tunnel status (Tunnel/    */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) per vlan         */
/*                                                                           */
/*    Input(s)            : u4VlanId       - Vlan Id                         */
/*                          u4ContextId    - Context Id                      */
/*                          u2Protocol     - L2 Protocol                     */
/*                          u1TunnelStatus - Protocol tunnel status          */
/*                                           on a particular vlan.           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : L2IWF_SUCCESS / L2IWF_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfSetProtocolTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                     UINT2 u2Protocol, UINT1 u1TunnelStatus)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_INFO (u4VlanId) == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    switch (u2Protocol)
    {
        case L2_PROTO_ELMI:
            L2IWF_ELMI_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_LLDP:
            L2IWF_LLDP_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_DOT1X:
            L2IWF_DOT1X_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_LACP:
            L2IWF_LA_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_STP:
            L2IWF_STP_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_GVRP:
            L2IWF_GVRP_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_GMRP:
            L2IWF_GMRP_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_MVRP:
            L2IWF_MVRP_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_MMRP:
            L2IWF_MMRP_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_IGMP:
            L2IWF_IGMP_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_ECFM:
            L2IWF_ECFM_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_EOAM:
            L2IWF_EOAM_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        case L2_PROTO_PTP:
            L2IWF_PTP_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId)) =
                u1TunnelStatus;
            break;
        default:
            break;
    }

    if ((u2Protocol >= L2_PROTO_OTHER) && (u2Protocol <= L2_PROTO_MAX))
    {
        L2IWF_OTHER_TUNNEL_STATUS_PER_VLAN (L2IWF_VLAN_INFO (u4VlanId),
                                            (u2Protocol - L2_PROTO_OTHER)) =
            u1TunnelStatus;
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetProtocolTunnelStatusPerVlan              */
/*                                                                           */
/*    Description         : This function returns the tunnel status (Tunnel/ */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) on this vlan     */
/*                                                                           */
/*    Input(s)            : u4ContextId - contextId                          */
/*                u4VlanId    - VlanId                 */
/*                          u2Protocol  - L2 Protocol                        */
/*                                                                           */
/*    Output(s)           : pu1TunnelStatus - Protocol Tunnel status          */
/*                                                                           */
/*    Returns             : None.                                            */
/*                                                                           */
/*****************************************************************************/
VOID
L2IwfGetProtocolTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                                     UINT2 u2Protocol, UINT1 *pu1TunnelStatus)
{
    tL2VlanInfo        *pL2VlanInfo = NULL;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return;
    }

    if (u4VlanId > VLAN_DEV_MAX_VLAN_ID || u4VlanId <= 0)
    {
        return;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return;
    }

    if (L2IWF_VLAN_ACTIVE (u4VlanId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return;
    }

    if (L2IWF_VLAN_INFO (u4VlanId) != NULL)
    {
        pL2VlanInfo = L2IWF_VLAN_INFO (u4VlanId);
    }

    switch (u2Protocol)
    {
        case L2_PROTO_ELMI:
            *pu1TunnelStatus = L2IWF_ELMI_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_LLDP:
            *pu1TunnelStatus = L2IWF_LLDP_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_DOT1X:
            *pu1TunnelStatus = L2IWF_DOT1X_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_LACP:
            *pu1TunnelStatus = L2IWF_LA_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_STP:
            *pu1TunnelStatus = L2IWF_STP_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_GVRP:
            *pu1TunnelStatus = L2IWF_GVRP_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_GMRP:
            *pu1TunnelStatus = L2IWF_GMRP_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_IGMP:
            *pu1TunnelStatus = L2IWF_IGMP_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_MVRP:
            *pu1TunnelStatus = L2IWF_MVRP_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_MMRP:
            *pu1TunnelStatus = L2IWF_MMRP_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_ECFM:
            *pu1TunnelStatus = L2IWF_ECFM_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_EOAM:
            *pu1TunnelStatus = L2IWF_EOAM_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        case L2_PROTO_PTP:
            *pu1TunnelStatus = L2IWF_PTP_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo);
            break;
        default:
            break;
    }

    if ((u2Protocol >= L2_PROTO_OTHER) && (u2Protocol <= L2_PROTO_MAX))
    {
        *pu1TunnelStatus =
            L2IWF_OTHER_TUNNEL_STATUS_PER_VLAN (pL2VlanInfo,
                                                (u2Protocol - L2_PROTO_OTHER));
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetL2CPProtcolType                              */
/*                                                                           */
/* Description        : This routine is called to get the L2CP               */
/*                      protocol type(Port/Vlan) based.                      */
/*                                                                           */
/* Input(s)           : u4ProtocolId      - Portocol ID                      */
/*                                                                           */
/* Output(s)          : pu1L2CPProtType - Protocol Type                      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetL2CPProtcolType (UINT4 u4ProtocolId, UINT1 *pu1L2CPProtType)
{
    if ((u4ProtocolId == L2_PROTO_DOT1X) || (u4ProtocolId == L2_PROTO_LACP) ||
        (u4ProtocolId == L2_PROTO_STP) || (u4ProtocolId == L2_PROTO_ELMI) ||
        (u4ProtocolId == L2_PROTO_LLDP) || (u4ProtocolId == L2_PROTO_EOAM) ||
        (u4ProtocolId == L2_PROTO_PTP))
    {
        *pu1L2CPProtType = L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED;
        return L2IWF_SUCCESS;
    }
    /* Protocols that can be validated on per Vlan */
    else if ((u4ProtocolId == L2_PROTO_GVRP) || (u4ProtocolId == L2_PROTO_GMRP)
             || (u4ProtocolId == L2_PROTO_MVRP)
             || (u4ProtocolId == L2_PROTO_MMRP)
             || (u4ProtocolId == L2_PROTO_ECFM)
             || (u4ProtocolId == L2_PROTO_IGMP))
    {
        *pu1L2CPProtType = L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED;
        return L2IWF_SUCCESS;
    }

    return L2IWF_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetL2ProtocolTunnelStatus                   */
/*                                                                           */
/*    Description         : This function returns the tunnel status (Tunnel/ */
/*                          Peer/Discard) for protocols based on Per Port/   */
/*                          Per Vlan Service type                            */
/*                                                                           */
/*    Input(s)            : u4ContextId - contextId                          */
/*                          u4IfIndex   - PortId                             */
/*                          VlanId      - VlanId                             */
/*                          u4ProtocolId - L2 Protocol                       */
/*                                                                           */
/*    Output(s)           : pu1TunnelStatus - Protocol Tunnel status         */
/*                          pu1L2CPProtType - Protocol Type(Port/VLan) based */
/*                                                                           */
/*    Returns             : L2IWF_FAILURE/L2IWF_SUCCESS.                     */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfGetL2ProtocolTunnelStatus (UINT4 u4ContextId, UINT4 u4IfIndex,
                                tVlanId VlanId, UINT4 u4ProtocolId,
                                UINT1 *pu1TunnelStatus, UINT1 *pu1L2CPProtType)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tL2VlanInfo        *pL2VlanInfo = NULL;
    UINT1               u1VlanTunnelStatus = OSIX_FALSE;
    UINT1               u1PortTunnelStatus = OSIX_FALSE;
    UINT1               u1RetVal = L2IWF_TRUE;
    UINT1               u1OverrideOption = 0;
    UINT2               u2AggId = 0;

    if (L2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
    {
        /* If the port belongs to port-channel, then get the port-channel id.
         * If the port is not part of any port-channel, then u2AggId will be
         * same as the give u4IfIndex. */
        if (L2IwfGetPortChannelForPort ((UINT2) u4IfIndex, &u2AggId) ==
            L2IWF_SUCCESS)
        {
            u4IfIndex = (UINT4) u2AggId;
        }
    }
    L2_LOCK ();

    /* Function to get the Protocol Service Type */
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    u1RetVal = (UINT1) L2IwfGetL2CPProtcolType (u4ProtocolId, pu1L2CPProtType);
    if (u1RetVal != L2IWF_SUCCESS)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (!(VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0))
    {
        if (L2IWF_VLAN_ACTIVE (VlanId) != OSIX_TRUE)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }

        if (L2IWF_VLAN_INFO (VlanId) != NULL)
        {
            pL2VlanInfo = L2IWF_VLAN_INFO (VlanId);
        }
        if (L2IWF_VLAN_TUNNEL_STATUS (VlanId) == VLAN_ENABLED)
        {
            u1VlanTunnelStatus = FALSE;
        }
        else
        {
            u1VlanTunnelStatus = TRUE;
        }
    }

    if (L2IWF_IFENTRY_TUNNEL_STATUS (pL2PortEntry) == OSIX_TRUE)
    {
        u1PortTunnelStatus = FALSE;
    }
    else
    {
        u1PortTunnelStatus = TRUE;
    }

    /* Tunnel status Disabled */
    /* If Both the Port and Vlan tunnel status is Disabled, then protocol tunnel 
     * status is set as PEER */
    if ((u1VlanTunnelStatus == TRUE) && (u1PortTunnelStatus == TRUE))
    {
        *pu1TunnelStatus = VLAN_TUNNEL_PROTOCOL_PEER;
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    L2_UNLOCK ();
    L2IwfGetOverrideOption (pL2PortEntry->u4IfIndex, &u1OverrideOption);
    L2_LOCK ();

    /* If the Override option is enabled and Prot. Type is VLAN based, set the Prot. Type as port based. */
    if ((u1OverrideOption == L2IWF_OVERRIDE_OPTION_ENABLE)
        && (*pu1L2CPProtType == L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED))
    {
        *pu1L2CPProtType = L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED;
    }

    /* Tunnel status Enabled */
    /* Protocol service type is port based, then tunnel status is referred from 
     * port table */
    if (*pu1L2CPProtType == L2IWF_L2CP_PROTOCOL_TYPE_PORT_BASED)
    {
        L2_UNLOCK ();
        L2IwfGetProtocolTunnelStatusOnPort (u4IfIndex, (UINT2) u4ProtocolId,
                                            pu1TunnelStatus);
        L2_LOCK ();
    }
    else if (*pu1L2CPProtType == L2IWF_L2CP_PROTOCOL_TYPE_VLAN_BASED)
    {
        /* Tunnel status is referred  from vlan table */
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        L2IwfGetProtocolTunnelStatusPerVlan (u4ContextId, VlanId,
                                             (UINT2) u4ProtocolId,
                                             pu1TunnelStatus);
        return L2IWF_SUCCESS;
    }
    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetTunnelStatusPerVlan                          */
/*                                                                           */
/* Description        : This routine is called to set the                    */
/*                      tunnel status.                                       */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context ID                            */
/*                      u4VlanId     - Vlan Id                               */
/*                      u1TunnelStatus - TunnelStatus(ENABLE/DISABLE)        */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetTunnelStatusPerVlan (UINT4 u4ContextId, UINT4 u4VlanId,
                             UINT1 u1TunnelStatus)
{
    tL2VlanInfo        *pL2VlanInfo = NULL;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_INFO (u4VlanId) != NULL)
    {
        pL2VlanInfo = L2IWF_VLAN_INFO (u4VlanId);

        L2IWF_VLAN_TUNNEL_STATUS (pL2VlanInfo) = u1TunnelStatus;
    }
    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMiGetVlanUnTagPorts                             */
/*                                                                           */
/* Description        : This routine returns the Untagged ports for the given*/
/*                      VlanId as Local port IDs. It accesses the L2Iwf      */
/*                      common database for the given context                */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId - Service VLAN Identifier                     */
/*                      UnTagPorts                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfMiGetVlanUnTagPorts (UINT4 u4ContextId, tVlanId VlanId,
                          tPortList UnTagPorts)
{
    tLocalPortList     *pLocalUnTagPorts = NULL;

    MEMSET (UnTagPorts, 0, sizeof (tPortList));

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_TRUE)
    {
        pLocalUnTagPorts =
            (tLocalPortList *)
            UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pLocalUnTagPorts == NULL)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
        MEMSET (pLocalUnTagPorts, 0, sizeof (tLocalPortList));

        MEMCPY (pLocalUnTagPorts, L2IWF_VLAN_UNTAG_PORTS (VlanId),
                sizeof (tLocalPortList));

        L2IwfConvToGlblIfIndexList (*pLocalUnTagPorts, UnTagPorts);
        UtilPlstReleaseLocalPortList ((UINT1 *) pLocalUnTagPorts);
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPvidVlanOfAnyPorts                            */
/*                                                                           */
/* Description        : This is used to check whether the VLAN is PVID       */
/*                      of any ports                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context identifier                     */
/*                      VlanId - VLAN Index                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_FALSE / L2IWF_FALSE                            */
/*****************************************************************************/
INT4
L2IwfIsPvidVlanOfAnyPorts (UINT4 u4ContextId, tVlanId VlanId)
{
    INT4                i4Index = 0;
    UINT2               u2LocalPort = 0;

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();
        return L2IWF_FALSE;
    }

    for (i4Index = u2LocalPort + 1; i4Index <= L2IWF_MAX_PORTS_PER_CONTEXT_EXT;
         i4Index++)
    {
        if (L2IWF_PORT_ACTIVE (i4Index) == OSIX_TRUE)
        {
            if (L2IWF_PORT_PVID (i4Index) == VlanId)
            {
                L2IwfReleaseContext ();
                L2_UNLOCK ();
                return L2IWF_TRUE;
            }
        }
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_FALSE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetUntaggedPortList                             */
/*                                                                           */
/* Description        : This function is used to mark the VLAN as            */
/*                      FCoE VLAN in L2IWF.                                  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId - Service VLAN Identifier                     */
/*                      u1FCoEVlanType - L2_VLAN / FCOE_VLAN                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfGetUntaggedPortList (UINT4 u4ContextId, tVlanId VlanId,
                          tLocalPortList LocalEgressPorts)
{

    MEMSET (LocalEgressPorts, 0, sizeof (tLocalPortList));

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_TRUE)
    {
        MEMCPY (LocalEgressPorts, L2IWF_VLAN_UNTAG_PORTS (VlanId),
                sizeof (tLocalPortList));
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

#ifdef FSB_WANTED
/*****************************************************************************/
/* Function Name      : L2IwfSetFCoEVlanType                                 */
/*                                                                           */
/* Description        : This function is used to mark the VLAN as            */
/*                      FCoE VLAN in L2IWF.                                  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId - Service VLAN Identifier                     */
/*                      u1FCoEVlanType - L2_VLAN / FCOE_VLAN                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfSetFCoEVlanType (UINT4 u4ContextId, tVlanId VlanId, UINT1 u1FCoEVlanType)
{
    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId == 0)
    {
        return L2IWF_FAILURE;
    }
    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_INFO (VlanId) != NULL)
    {
        L2IWF_FCOE_VLAN_TYPE (VlanId) = u1FCoEVlanType;
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}
#endif
#endif /*End _L2_VLAN_C_ */
