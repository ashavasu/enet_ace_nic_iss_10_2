/*************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2vlist.c,v 1.16 2016/03/08 11:59:02 siva Exp $
 *
 * Description: This file contains routines related to the PortVlan RBTree.
 *              This file will be part of both VLAN as well as LLDP stack.
 *
 *************************************************************************/
#ifndef _L2_VLIST_C_
#define _L2_VLIST_C_

#include "l2inc.h"

/*****************************************************************************/
/* Function Name      : L2IwfPortVlanCrtMemPoolAndRBTree                     */
/*                                                                           */
/* Description        : This routine creates the mempool required for        */
/*                      storing Port Vlan Mapping and creates RBTree in L2Iwf*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPortVlanCrtMemPoolAndRBTree (VOID)
{
    if (L2IWF_CREATE_MEMPOOL (sizeof (tL2PortVlanInfo),
                              L2IWF_PORT_VLAN_MEMBLK_COUNT,
                              MEM_HEAP_MEMORY_TYPE,
                              &gL2GlobalInfo.PortVlanInfoPoolId) == MEM_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    if ((gL2GlobalInfo.PortVlanTable =
         RBTreeCreateEmbedded (0, (tRBCompareFn) L2IwfPortVlanInfoCmpEntry))
        == NULL)
    {
        L2IwfPortVlanDelMemPoolAndRBTree ();
        return L2IWF_FAILURE;
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPortVlanDelMemPoolAndRBTree                     */
/*                                                                           */
/* Description        : This routine deletes the mempool and RBTree created  */
/*                      for maintaing Port Vlan Mapping                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPortVlanDelMemPoolAndRBTree (VOID)
{
    if (gL2GlobalInfo.PortVlanTable != NULL)
    {
        RBTreeDestroy (gL2GlobalInfo.PortVlanTable, L2IwfRBFreePortVlanInfo, 0);
        gL2GlobalInfo.PortVlanTable = NULL;
    }

    /* Delete the memory pool. */
    if (gL2GlobalInfo.PortVlanInfoPoolId != 0)
    {
        L2IWF_DELETE_MEMPOOL (gL2GlobalInfo.PortVlanInfoPoolId);
        gL2GlobalInfo.PortVlanInfoPoolId = 0;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfRBFreePortVlanInfo                              */
/*                                                                           */
/* Description        : This routine releases the memory allocated to each   */
/*                      node of PortVlan RBTree                              */
/*                                                                           */
/* Input(s)           : pRBElem - pointer to the node to be freed            */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfRBFreePortVlanInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (pRBElem != NULL)
    {
        L2IWF_RELEASE_MEMBLOCK (gL2GlobalInfo.PortVlanInfoPoolId,
                                (UINT1 *) pRBElem);
        pRBElem = NULL;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfPortVlanInfoCmpEntry                        */
/*                                                                           */
/*    Description         : This function is used to compare the indices of  */
/*                          two port vlan entries. The indices are port      */
/*                          number and vlan id.                              */
/*                                                                           */
/*    Input(s)            : pNodeA,pNodeB - PortVlan Entries.                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pNodeA > pNodeB                              */
/*                          -1 - pNodeA < pNodeB                             */
/*                          0 - pNodeA == PNodeB                             */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfPortVlanInfoCmpEntry (tRBElem * pNodeA, tRBElem * pNodeB)
{
    tL2PortVlanInfo    *pVlanEntryA = (tL2PortVlanInfo *) pNodeA;
    tL2PortVlanInfo    *pVlanEntryB = (tL2PortVlanInfo *) pNodeB;

    if (pVlanEntryA->u4IfIndex != pVlanEntryB->u4IfIndex)
    {
        if (pVlanEntryA->u4IfIndex > pVlanEntryB->u4IfIndex)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    if (pVlanEntryA->u2VlanId != pVlanEntryB->u2VlanId)
    {
        if (pVlanEntryA->u2VlanId > pVlanEntryB->u2VlanId)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    return 0;
}

/*****************************************************************************/
/* Function Name        : L2IwfGetPortVlanEntry                              */
/*                                                                           */
/* Description          : Returns the pointer to the PortVlanEntry for  the  */
/*                        given vlan & port ID. Lock should be taken before  */
/*                        calling this function.                             */
/*                                                                           */
/* Input (s)            : u2VlanId - VLAN ID value.                          */
/*                        u4IfIndex - PortIndex Value                        */
/*                                                                           */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : Pointer to the PortValn Entry.                     */
/*****************************************************************************/
tL2PortVlanInfo    *
L2IwfGetPortVlanEntry (UINT4 u4IfIndex, UINT2 u2VlanId)
{
    tL2PortVlanInfo     PortVlanEntry;
    tL2PortVlanInfo    *pPortVlanEntry;

    MEMSET (&PortVlanEntry, 0, sizeof (tL2PortVlanInfo));
    PortVlanEntry.u4IfIndex = u4IfIndex;
    PortVlanEntry.u2VlanId = u2VlanId;

    pPortVlanEntry = (tL2PortVlanInfo *) RBTreeGet (gL2GlobalInfo.PortVlanTable,
                                                    (tRBElem *) &
                                                    PortVlanEntry);
    return pPortVlanEntry;
}

/*****************************************************************************/
/* Function Name        : L2IwfGetPortVlanMembers                            */
/*                                                                           */
/* Description          : Returns the List of VLANs for which the particular */
/*                        port is member of. Lock should be taken before     */
/*                        calling this function.                             */
/*                                                                           */
/* Input (s)            : u2VlanId - VLAN ID value.                          */
/*                        pu1VlanList - VLAN List                            */
/*                                                                           */
/* Output               : None                                               */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE.                     */
/*****************************************************************************/
INT4
L2IwfGetPortVlanMembers (UINT4 u4IfIndex, UINT1 *pu1VlanList)
{
    tL2PortVlanInfo    *pPortVlanEntry = NULL;
    tL2PortVlanInfo    *pNextPortVlanEntry = NULL;
    tL2PortVlanInfo     PortVlanEntry;
    UINT2               u2Count = 0;

    MEMSET (pu1VlanList, 0, VLAN_LIST_SIZE);

    MEMSET (&PortVlanEntry, 0, sizeof (tL2PortVlanInfo));
    PortVlanEntry.u4IfIndex = u4IfIndex;
    PortVlanEntry.u2VlanId = 0;

    pPortVlanEntry = &PortVlanEntry;

    while ((pNextPortVlanEntry =
            (tL2PortVlanInfo *) RBTreeGetNext (gL2GlobalInfo.PortVlanTable,
                                               (tRBElem *) pPortVlanEntry,
                                               NULL)) != NULL)
    {
        if (pNextPortVlanEntry->u4IfIndex == u4IfIndex)
        {
            OSIX_BITLIST_SET_BIT (pu1VlanList, pNextPortVlanEntry->u2VlanId,
                                  VLAN_LIST_SIZE);
            pPortVlanEntry = pNextPortVlanEntry;
            u2Count++;
        }
        else
        {
            break;
        }
    }

    if (u2Count)
    {
        return L2IWF_SUCCESS;
    }
    /* If port is not member of any VLAN return failure */
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name        : L2IwfValidatePortVlanTableIndex                    */
/*                                                                           */
/* Description          : Validates the given indices of the PortVlanTable   */
/*                                                                           */
/* Input (s)            : u4IfIndex - PortIndex Value                        */
/*                        u2VlanId  - VLAN ID value                          */
/*                                                                           */
/* Output (s)           : None                                               */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE                      */
/*****************************************************************************/
INT4
L2IwfValidatePortVlanTableIndex (UINT4 u4IfIndex, UINT2 u2VlanId)
{
    tL2PortVlanInfo     PortVlanEntry;
    tL2PortVlanInfo    *pPortVlanEntry;
    INT4                i4Retval = L2IWF_FAILURE;

    MEMSET (&PortVlanEntry, 0, sizeof (tL2PortVlanInfo));
    PortVlanEntry.u4IfIndex = u4IfIndex;
    PortVlanEntry.u2VlanId = u2VlanId;

    L2_DB_LOCK ();
    pPortVlanEntry = L2IwfGetPortVlanEntry (u4IfIndex, u2VlanId);

    if (pPortVlanEntry != NULL)
    {
        i4Retval = L2IWF_SUCCESS;
    }
    L2_DB_UNLOCK ();
    return i4Retval;
}

/*****************************************************************************/
/* Function Name        : L2IwfGetFirstPortVlanTableIndex                    */
/*                                                                           */
/* Description          : Returns the first index of the PortVlanTable       */
/*                                                                           */
/* Input                : None                                               */
/*                                                                           */
/* Output (s)           : *pu4IfIndex - PortIndex Value                      */
/*                        *pu2VlanId - VLAN ID value                         */
/*                                                                           */
/* output               : None                                               */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE.                     */
/*****************************************************************************/
INT4
L2IwfGetFirstPortVlanTableIndex (UINT4 *pu4IfIndex, UINT2 *pu2VlanId)
{
    tL2PortVlanInfo    *pPortVlanEntry;
    INT4                i4Retval = L2IWF_FAILURE;

    L2_DB_LOCK ();
    pPortVlanEntry =
        (tL2PortVlanInfo *) RBTreeGetFirst (gL2GlobalInfo.PortVlanTable);

    if (pPortVlanEntry != NULL)
    {
        *pu4IfIndex = pPortVlanEntry->u4IfIndex;
        *pu2VlanId = pPortVlanEntry->u2VlanId;
        i4Retval = L2IWF_SUCCESS;
    }
    L2_DB_UNLOCK ();
    return i4Retval;
}

/*****************************************************************************/
/* Function Name        : L2IwfGetNextPortVlanTableIndex                     */
/*                                                                           */
/* Description          : Returns the next indices of the PortVlanTable for  */
/*                        the given port id and vlan id                      */
/*                                                                           */
/* Input (s)            : u4IfIndex - PortIndex Value                        */
/*                        u2VlanId - VLAN ID value                           */
/*                                                                           */
/* Output               : *pi4NextIfIndex - Next port index value            */
/*                        *pu2NextVlanId - Next VLAN ID value                */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE                      */
/*****************************************************************************/

INT4
L2IwfGetNextPortVlanTableIndex (UINT4 u4IfIndex,
                                UINT4 *pi4NextIfIndex,
                                UINT2 u2VlanId, UINT2 *pu2NextVlanId)
{
    tL2PortVlanInfo     PortVlanEntry;
    tL2PortVlanInfo    *pPortVlanEntry = NULL;
    tL2PortVlanInfo    *pNextPortVlanEntry = NULL;
    INT4                i4Retval = L2IWF_FAILURE;

    MEMSET (&PortVlanEntry, 0, sizeof (tL2PortVlanInfo));
    PortVlanEntry.u4IfIndex = u4IfIndex;
    PortVlanEntry.u2VlanId = u2VlanId;

    L2_DB_LOCK ();
    pPortVlanEntry = RBTreeGet (gL2GlobalInfo.PortVlanTable,
                                (tRBElem *) & PortVlanEntry);
    if (pPortVlanEntry != NULL)
    {
        pNextPortVlanEntry =
            (tL2PortVlanInfo *) RBTreeGetNext (gL2GlobalInfo.PortVlanTable,
                                               (tRBElem *) pPortVlanEntry,
                                               NULL);
        if (pNextPortVlanEntry != NULL)
        {
            *pi4NextIfIndex = pNextPortVlanEntry->u4IfIndex;
            *pu2NextVlanId = pNextPortVlanEntry->u2VlanId;
            i4Retval = L2IWF_SUCCESS;
        }
    }
    L2_DB_UNLOCK ();
    return i4Retval;
}

/*****************************************************************************/
/* Function Name        : L2IwfGetVlanName                                   */
/*                                                                           */
/* Description          : Returns the vlan name for the given port id        */
/*                        and vlan id                                        */
/*                                                                           */
/* Input (s)            : u4IfIndex - PortIndex Value                        */
/*                        u2VlanId - VLAN ID value                           */
/*                                                                           */
/* Output               : *pu1VlanName - Vlan name                           */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE                      */
/*****************************************************************************/
INT4
L2IwfGetVlanName (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 *pu1VlanName)
{

    INT4                i4Retval = L2IWF_FAILURE;
    tL2PortVlanInfo    *pPortVlanEntry;

    L2_DB_LOCK ();
    pPortVlanEntry = L2IwfGetPortVlanEntry (u4IfIndex, u2VlanId);

    if (pPortVlanEntry != NULL)
    {
        MEMCPY (pu1VlanName, pPortVlanEntry->au1VlanName,
                STRLEN (pPortVlanEntry->au1VlanName));
        i4Retval = L2IWF_SUCCESS;
    }
    L2_DB_UNLOCK ();
    return i4Retval;
}

/*****************************************************************************/
/* Function Name        : L2IwfResetAllVlanNameIfTxEnabled                   */
/*                                                                           */
/* Description          : Resets vlan name tlv transmission for all the      */
/*                        entries present in the tL2PortVlanInfo.            */
/*                                                                           */
/* Input (s)            : NONE                                               */
/*                                                                           */
/* Output               : NONE                                               */
/*                                                                           */
/* Return Value         : NONE                                               */
/*****************************************************************************/
VOID
L2IwfResetAllVlanNameIfTxEnabled (VOID)
{
    tL2PortVlanInfo    *pPortVlanEntry = NULL;
    tL2PortVlanInfo    *pNextPortVlanEntry = NULL;
    tL2PortVlanInfo     TmpPortVlanEntry;

    MEMSET (&TmpPortVlanEntry, 0, sizeof (tL2PortVlanInfo));

    pPortVlanEntry = &TmpPortVlanEntry;

    L2_DB_LOCK ();

    while ((pNextPortVlanEntry =
            (tL2PortVlanInfo *) RBTreeGetNext (gL2GlobalInfo.PortVlanTable,
                                               (tRBElem *) pPortVlanEntry,
                                               NULL)) != NULL)
    {
        pNextPortVlanEntry->u1VlanNameTxEnable = LLDP_FALSE;
        pPortVlanEntry = pNextPortVlanEntry;
    }

    L2_DB_UNLOCK ();
}

/*****************************************************************************/
/* Function Name        : L2IwfGetAllVlanNameIfTxEnabled                     */
/*                                                                           */
/* Description          : Returns all the vlan id and vlan name mapped to the*/
/*                        given port id if the transmission of vlan name tlv */
/*                        is enabled on that port.                           */
/*                                                                           */
/* Input (s)            : u4IfIndex - PortIndex Value                        */
/*                                                                           */
/* Output               : *pu1VlanNameInfo - Array of structure containing   */
/*                         Vlan id and vlan name                             */
/*                        *pu2VlanCount - Count of the vlans mapped to the   */
/*                                        port                               */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE                      */
/*****************************************************************************/
INT4
L2IwfGetAllVlanNameIfTxEnabled (UINT4 u4IfIndex, tVlanNameInfo * pVlanNameInfo,
                                UINT2 *pu2VlanCount)
{
    tL2PortVlanInfo    *pPortVlanEntry = NULL;
    tL2PortVlanInfo    *pNextPortVlanEntry = NULL;
    tL2PortVlanInfo     TmpPortVlanEntry;
    UINT2               u2Count = 0;

    MEMSET (&TmpPortVlanEntry, 0, sizeof (tL2PortVlanInfo));
    TmpPortVlanEntry.u4IfIndex = u4IfIndex;
    TmpPortVlanEntry.u2VlanId = 0;
    pPortVlanEntry = &TmpPortVlanEntry;

    L2_DB_LOCK ();

    while ((pNextPortVlanEntry =
            (tL2PortVlanInfo *) RBTreeGetNext (gL2GlobalInfo.PortVlanTable,
                                               (tRBElem *) pPortVlanEntry,
                                               NULL)) != NULL)
    {
        if ((pNextPortVlanEntry->u4IfIndex == u4IfIndex) &&
            (pNextPortVlanEntry->u1VlanNameTxEnable == LLDP_TRUE))
        {
            pVlanNameInfo[u2Count].u2VlanId = pNextPortVlanEntry->u2VlanId;
            MEMCPY (pVlanNameInfo[u2Count].au1VlanName,
                    pNextPortVlanEntry->au1VlanName,
                    STRLEN (pNextPortVlanEntry->au1VlanName));
            pPortVlanEntry = pNextPortVlanEntry;
            u2Count++;
        }
        else
        {
            if (pNextPortVlanEntry->u4IfIndex != u4IfIndex)
            {
                break;
            }
            else if (pNextPortVlanEntry->u1VlanNameTxEnable != LLDP_TRUE)
            {
                pPortVlanEntry = pNextPortVlanEntry;
            }
        }
    }
    *pu2VlanCount = u2Count;
    L2_DB_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name        : L2IwfGetVlanNameTxStatus                           */
/*                                                                           */
/* Description          : Returns the vlan name TLV transmission status for  */
/*                        the given port id and vlan id                      */
/*                                                                           */
/* Input (s)            : u4IfIndex - PortIndex Value                        */
/*                        u2VlanId - VLAN ID value                           */
/*                                                                           */
/* Output               : *pu1VlanNameTxStatus - Vlan name TLV transmission  */
/*                                              status                       */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE                      */
/*****************************************************************************/
INT4
L2IwfGetVlanNameTxStatus (UINT4 u4IfIndex, UINT2 u2VlanId,
                          UINT1 *pu1VlanNameTxStatus)
{
    INT4                i4Retval = L2IWF_FAILURE;
    tL2PortVlanInfo    *pPortVlanEntry;

    L2_DB_LOCK ();
    pPortVlanEntry = L2IwfGetPortVlanEntry (u4IfIndex, u2VlanId);

    if (pPortVlanEntry != NULL)
    {
        *pu1VlanNameTxStatus = pPortVlanEntry->u1VlanNameTxEnable;
        i4Retval = L2IWF_SUCCESS;
    }
    L2_DB_UNLOCK ();
    return i4Retval;
}

/*****************************************************************************/
/* Function Name        : L2IwfSetVlanNameTxStatus                           */
/*                                                                           */
/* Description          : Sets the vlan name TLV transmission status for the */
/*                        given port id and vlan id.                         */
/*                                                                           */
/* Input (s)            : u4IfIndex - PortIndex Value                        */
/*                        u2VlanId - VLAN ID value                           */
/*                        bVlanNameTxStatus - Vlan name TLV transmission     */
/*                                             status                        */
/*                                                                           */
/* Output               : *pu1ConfTxEnable - Set to TRUE if transmission      */
/*                                          status is configured with the    */
/*                                          value to be set.                 */
/*                                                                           */
/* Return Value         : L2IWF_SUCCESS / L2IWF_FAILURE                      */
/*****************************************************************************/
INT4
L2IwfSetVlanNameTxStatus (UINT4 u4IfIndex, UINT2 u2VlanId,
                          UINT1 u1VlanNameTxStatus, UINT1 *pu1ConfTxEnable)
{
    INT4                i4Retval = L2IWF_FAILURE;
    tL2PortVlanInfo    *pPortVlanEntry = NULL;

    L2_DB_LOCK ();
    pPortVlanEntry = L2IwfGetPortVlanEntry (u4IfIndex, u2VlanId);

    if (pPortVlanEntry != NULL)
    {
        if (pPortVlanEntry->u1VlanNameTxEnable != u1VlanNameTxStatus)
        {
            pPortVlanEntry->u1VlanNameTxEnable = u1VlanNameTxStatus;
            *pu1ConfTxEnable = OSIX_TRUE;
        }
        /* Same value has been already configured. Hence return success */
        i4Retval = L2IWF_SUCCESS;
    }
    L2_DB_UNLOCK ();
    return i4Retval;
}

/*****************************************************************************/
/* Function Name      : L2IwfAddPortVlanEntries                              */
/*                                                                           */
/* Description        : This routine adds new nodes to the PortVlan          */
/*                      RBTree for the ports in the given portlist and vlan  */
/*                      id                                                   */
/*                                                                           */
/* Input(s)           : u2VlanId      - VLAN ID value                        */
/*                      IfAddPortList - List of ports mapped to this vlan    */
/*                                      (Physical port list)                 */
/*                      pu1VlanName   - Pointer to vlan name                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfAddPortVlanEntries (UINT2 u2VlanId, tPortList IfAddPortList,
                         UINT1 *pu1VlanName)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2BytIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT1               u1TxEnabled = LLDP_FALSE;

    L2_DB_LOCK ();

    if (FsUtilBitListIsAllZeros (IfAddPortList,
                                 sizeof (tPortList)) == OSIX_FALSE)
    {
        for (u2BytIndex = 0; u2BytIndex < BRG_PORT_LIST_SIZE; u2BytIndex++)
        {
            u1PortFlag = IfAddPortList[u2BytIndex];

            for (u2BitIndex = 0; ((u2BitIndex < BRG_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & L2IWF_BIT8) != 0)
                {
                    u4IfIndex = (UINT4) ((u2BytIndex * BRG_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);
                    if (L2IwfUpdatePortVlanRBTree (u4IfIndex, u2VlanId,
                                                   pu1VlanName, L2IWF_ADD,
                                                   &u1TxEnabled)
                        != L2IWF_SUCCESS)
                    {
                        L2_DB_UNLOCK ();
                        return L2IWF_FAILURE;
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    L2_DB_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeletePortVlanEntries                           */
/*                                                                           */
/* Description        : This routine deletes the nodes from the PortVlan     */
/*                      RBTree for the ports in the given portlist and vlan  */
/*                      id                                                   */
/*                                                                           */
/* Input(s)           : u2VlanId      - VLAN ID value                        */
/*                      IfDelPortList - List of ports unmapped  from this    */
/*                                      Vlan (Physical port list)            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfDeletePortVlanEntries (UINT2 u2VlanId, tPortList IfDelPortList)
{
    tPortList          *pIfNotifyPortList = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2BytIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT1               u1TxEnabled = LLDP_FALSE;

    pIfNotifyPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pIfNotifyPortList == NULL)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (*pIfNotifyPortList, 0, sizeof (tPortList));

    L2_DB_LOCK ();

    if (FsUtilBitListIsAllZeros (IfDelPortList,
                                 sizeof (tPortList)) == OSIX_FALSE)
    {
        for (u2BytIndex = 0; u2BytIndex < BRG_PORT_LIST_SIZE; u2BytIndex++)
        {
            u1PortFlag = IfDelPortList[u2BytIndex];

            for (u2BitIndex = 0; ((u2BitIndex < BRG_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & L2IWF_BIT8) != 0)
                {
                    u4IfIndex = (UINT4) ((u2BytIndex * BRG_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);
                    if (L2IwfUpdatePortVlanRBTree (u4IfIndex, u2VlanId,
                                                   NULL, L2IWF_DELETE,
                                                   &u1TxEnabled)
                        != L2IWF_SUCCESS)
                    {
                        FsUtilReleaseBitList ((UINT1 *) pIfNotifyPortList);
                        L2_DB_UNLOCK ();
                        return L2IWF_FAILURE;
                    }
                    if (u1TxEnabled == LLDP_TRUE)
                    {
                        OSIX_BITLIST_SET_BIT ((*pIfNotifyPortList), u4IfIndex,
                                              sizeof (tPortList));
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    L2_DB_UNLOCK ();

    if (FsUtilBitListIsAllZeros (*pIfNotifyPortList,
                                 sizeof (tPortList)) == OSIX_FALSE)
    {
        LldpVlndbNotifyVlanInfoChg (*pIfNotifyPortList);
    }
    FsUtilReleaseBitList ((UINT1 *) pIfNotifyPortList);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdatePortVlanEntry                             */
/*                                                                           */
/* Description        : This routine adds or deletes the PortVlan Node in the*/
/*                      PortVlan RBTree for the given port number and vlan id*/
/*                                                                           */
/* Input(s)           : u4IfIndex     - Memeber port of the given Vlan       */
/*                      u2VlanId      - VLAN ID value                        */
/*                      pu1VlanName   - Vlan Name                            */
/*                      u1Action      - Add / Delete the given port and vlan */
/*                                      to the PortVlanTable RBTree          */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfUpdatePortVlanEntry (UINT4 u4IfIndex, tVlanId VlanId, UINT1 *pu1VlanName,
                          UINT1 u1Action)
{
    UINT1               u1TxEnabled = LLDP_FALSE;

    L2_DB_LOCK ();
    if (L2IwfUpdatePortVlanRBTree (u4IfIndex, VlanId, pu1VlanName, u1Action,
                                   &u1TxEnabled) != L2IWF_SUCCESS)
    {
        L2_DB_UNLOCK ();
        return L2IWF_FAILURE;
    }
    L2_DB_UNLOCK ();

    if (u1TxEnabled == LLDP_TRUE)
    {
        LldpVlndbNotifyVlnInfoChgForPort (u4IfIndex);
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdateVlanName                                  */
/*                                                                           */
/* Description        : This routine updates the vlan name for the given     */
/*                      vlan id in the PortVlan RBTree                       */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id to which this vlan belongs*/
/*                      VlanId        - VLAN ID value                        */
/*                      pVlanName     - Pointer to vlan name                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfUpdateVlanName (UINT4 u4ContextId, tVlanId VlanId, UINT1 *pVlanName)
{
    UINT1              *pLocalEgressPorts = NULL;
    tPortList          *pEgressPorts = NULL;
    tPortList          *pIfNotifyPortList = NULL;
    UINT4               u4IfIndex;
    UINT2               u2BytIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT1               u1TxEnabled = LLDP_FALSE;

    pEgressPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPorts == NULL)
    {
        return L2IWF_FAILURE;
    }

    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId <= 0)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return OSIX_FALSE;
    }

    pIfNotifyPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pIfNotifyPortList == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        return L2IWF_FAILURE;
    }

    MEMSET (*pEgressPorts, 0, sizeof (tPortList));
    MEMSET (*pIfNotifyPortList, 0, sizeof (tPortList));
    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        FsUtilReleaseBitList ((UINT1 *) pIfNotifyPortList);
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_TRUE)
    {
        pLocalEgressPorts =
            UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pLocalEgressPorts == NULL)
        {
            FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
            FsUtilReleaseBitList ((UINT1 *) pIfNotifyPortList);
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
        MEMSET (pLocalEgressPorts, 0, sizeof (tLocalPortList));
        MEMCPY (pLocalEgressPorts, L2IWF_VLAN_EGRESS_PORTS (VlanId),
                sizeof (tLocalPortList));
        L2IwfConvToGlblIfIndexList (pLocalEgressPorts, *pEgressPorts);
        UtilPlstReleaseLocalPortList (pLocalEgressPorts);
    }
    L2IwfReleaseContext ();

    L2_UNLOCK ();

    if (FsUtilBitListIsAllZeros (*pEgressPorts,
                                 sizeof (tPortList)) == OSIX_TRUE)
    {
        FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
        FsUtilReleaseBitList ((UINT1 *) pIfNotifyPortList);
        return L2IWF_FAILURE;
    }

    L2_DB_LOCK ();
    for (u2BytIndex = 0; u2BytIndex < BRG_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = (*pEgressPorts)[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < BRG_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & L2IWF_BIT8) != 0)
            {
                u4IfIndex = (UINT4) ((u2BytIndex * BRG_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);
                if (L2IwfUpdatePortVlanRBTree (u4IfIndex, VlanId,
                                               pVlanName, L2IWF_UPDATE,
                                               &u1TxEnabled) != L2IWF_SUCCESS)
                {
                    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
                    FsUtilReleaseBitList ((UINT1 *) pIfNotifyPortList);
                    L2_DB_UNLOCK ();
                    return L2IWF_FAILURE;
                }
                if (u1TxEnabled == LLDP_TRUE)
                {
                    OSIX_BITLIST_SET_BIT ((*pIfNotifyPortList), u4IfIndex,
                                          sizeof (tPortList));
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    L2_DB_UNLOCK ();

    if (FsUtilBitListIsAllZeros (*pIfNotifyPortList,
                                 sizeof (tPortList)) == OSIX_FALSE)
    {
        LldpVlndbNotifyVlanInfoChg (*pIfNotifyPortList);
    }

    FsUtilReleaseBitList ((UINT1 *) pEgressPorts);
    FsUtilReleaseBitList ((UINT1 *) pIfNotifyPortList);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfUpdatePortVlanRBTree                            */
/*                                                                           */
/* Description        : This routine Adds , Deletes and Modifies the PortVlan*/
/*                      Entry depending upon the value of u1Action           */
/*                                                                           */
/* Input(s)           : u4IfIndex    - Port Index Value.                     */
/*                      u2VlanId     - VLAN ID value                         */
/*                      pu1VlanName  - Pointer to Vlan name.                 */
/*                      u1Action     - Action  update/delete/add             */
/* Output(s)          : *pu1TxEnabled - Set to true if the transmission of   */
/*                                     vlan name tlv is enabled on the port  */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfUpdatePortVlanRBTree (UINT4 u4IfIndex, UINT2 u2VlanId, UINT1 *pu1VlanName,
                           UINT1 u1Action, UINT1 *pu1TxEnabled)
{
    tL2PortVlanInfo    *pPortVlanEntry = NULL;
    tL2PortVlanInfo     PortVlanEntry;

    MEMSET (&PortVlanEntry, 0, sizeof (tL2PortVlanInfo));
    /* VIP are virtaul ports, so we need not check for physical ports */
    PortVlanEntry.u4IfIndex = u4IfIndex;
    PortVlanEntry.u2VlanId = u2VlanId;

    switch (u1Action)
    {
        case L2IWF_ADD:
            pPortVlanEntry =
                (tL2PortVlanInfo *) RBTreeGet (gL2GlobalInfo.PortVlanTable,
                                               (tRBElem *) & PortVlanEntry);
            /* Update VLAN name, if VLAN entry is already present in the 
             * PortVlanTable */

            /* VLAN entry is added to the PortVlanTable in one of the following 
             * scenarios, 
             * - When VLAN is learnt through GVRP(if GVRP is enabled) OR
             * - When user configures VLAN
             * If a VLAN entry, for eg: vlan 1 is learnt through GVRP and
             * added to the table and now the user configures vlan 1, only the
             * updatation to the VLAN entry vlan 1 will happen(VLAN name will 
             * be updated) */
            if (pPortVlanEntry != NULL)
            {
                *pu1TxEnabled = pPortVlanEntry->u1VlanNameTxEnable;
                MEMSET (pPortVlanEntry->au1VlanName, 0,
                        (VLAN_STATIC_MAX_NAME_LEN + 1));
                STRNCPY (pPortVlanEntry->au1VlanName, pu1VlanName,
                         (sizeof (pPortVlanEntry->au1VlanName) - 1));
                return L2IWF_SUCCESS;
            }

            pPortVlanEntry = (tL2PortVlanInfo *)
                L2IWF_ALLOCATE_MEMBLOCK (gL2GlobalInfo.PortVlanInfoPoolId);
            if (pPortVlanEntry == NULL)
            {
                return L2IWF_FAILURE;
            }
            MEMSET (pPortVlanEntry, 0, sizeof (tL2PortVlanInfo));
            pPortVlanEntry->u4IfIndex = u4IfIndex;
            pPortVlanEntry->u2VlanId = u2VlanId;
            pPortVlanEntry->u1VlanNameTxEnable = LLDP_FALSE;
            if (pu1VlanName != NULL)
            {
                MEMSET (pPortVlanEntry->au1VlanName, 0,
                        (VLAN_STATIC_MAX_NAME_LEN + 1));
                MEMCPY (pPortVlanEntry->au1VlanName, pu1VlanName,
                        STRLEN (pu1VlanName));
            }
            if (RBTreeAdd (gL2GlobalInfo.PortVlanTable,
                           (tRBElem *) pPortVlanEntry) == RB_FAILURE)
            {
                L2IWF_RELEASE_MEMBLOCK (gL2GlobalInfo.PortVlanInfoPoolId,
                                        pPortVlanEntry);
                return L2IWF_FAILURE;
            }
            break;
        case L2IWF_DELETE:
            pPortVlanEntry =
                (tL2PortVlanInfo *) RBTreeGet (gL2GlobalInfo.PortVlanTable,
                                               (tRBElem *) & PortVlanEntry);
            if (pPortVlanEntry != NULL)
            {
                *pu1TxEnabled = pPortVlanEntry->u1VlanNameTxEnable;
                RBTreeRem (gL2GlobalInfo.PortVlanTable,
                           (tRBElem *) pPortVlanEntry);
                L2IWF_RELEASE_MEMBLOCK (gL2GlobalInfo.PortVlanInfoPoolId,
                                        pPortVlanEntry);
                pPortVlanEntry = NULL;
            }
            else
            {
                return L2IWF_FAILURE;
            }
            break;
        case L2IWF_UPDATE:
            pPortVlanEntry =
                (tL2PortVlanInfo *) RBTreeGet (gL2GlobalInfo.PortVlanTable,
                                               (tRBElem *) & PortVlanEntry);
            if (pPortVlanEntry != NULL)
            {
                *pu1TxEnabled = pPortVlanEntry->u1VlanNameTxEnable;
                MEMSET (pPortVlanEntry->au1VlanName, 0,
                        (VLAN_STATIC_MAX_NAME_LEN + 1));
                MEMCPY (pPortVlanEntry->au1VlanName, pu1VlanName,
                        STRLEN (pu1VlanName));
            }
            else
            {
                return L2IWF_FAILURE;
            }
            break;
        default:
            break;
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDelPortVlanEntriesForPorts                      */
/*                                                                           */
/* Description        : This routine is called to delete the Port Vlan nodes */
/*                      for those ports listed in the port list. Port list   */
/*                      contains the list of ports that are mapped to deleted*/
/*                      vlans. The same need to be updated in the            */
/*                      PortVlanRBTree.                                      */
/*                                                                           */
/* Input(s)           : IfDelPortList - List of ports whose port vlan node   */
/*                                      if present in the RBTree need to be  */
/*                                      deleted as the mapped vlans has been */
/*                                      deleted.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfDelPortVlanEntriesForPorts (tPortList IfDelPortList)
{
    tPortList          *pIfNotifyPortList = NULL;
    UINT4               u4IfIndex;
    UINT2               u2BytIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;
    UINT1               u1TxEnabled = LLDP_FALSE;

    pIfNotifyPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pIfNotifyPortList == NULL)
    {
        return;
    }

    MEMSET (*pIfNotifyPortList, 0, sizeof (tPortList));

    for (u2BytIndex = 0; u2BytIndex < BRG_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = IfDelPortList[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < BRG_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & L2IWF_BIT8) != 0)
            {
                u4IfIndex = (UINT4) ((u2BytIndex * BRG_PORTS_PER_BYTE)
                                     + u2BitIndex + 1);
                L2IwfScanAndDeletePortVlanEntry (u4IfIndex, &u1TxEnabled);
                if (u1TxEnabled == LLDP_TRUE)
                {
                    OSIX_BITLIST_SET_BIT ((*pIfNotifyPortList), u4IfIndex,
                                          sizeof (tPortList));
                }

            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    if (FsUtilBitListIsAllZeros (*pIfNotifyPortList,
                                 sizeof (tPortList)) == OSIX_FALSE)
    {
        LldpVlndbNotifyVlanInfoChg (*pIfNotifyPortList);
    }
    FsUtilReleaseBitList ((UINT1 *) pIfNotifyPortList);
}

/*****************************************************************************/
/* Function Name        : L2IwfScanAndDeletePortVlanEntry                    */
/*                                                                           */
/* Description          : This function searches the RBTree and deletes all  */
/*                        Nodes with the given port number as the index.     */
/*                                                                           */
/* Input (s)            : u4IfIndex - Port Number                            */
/*                                                                           */
/* Output(s)            : *pu1TxEnabled - Set to true if the transmission of */
/*                                      vlan name tlv is enabled on this port*/
/*                                                                           */
/* Return Value         : None                                               */
/*****************************************************************************/
VOID
L2IwfScanAndDeletePortVlanEntry (UINT4 u4IfIndex, UINT1 *pu1TxEnabled)
{
    tL2PortVlanInfo    *pPortVlanEntry = NULL;
    tL2PortVlanInfo    *pNextPortVlanEntry = NULL;
    tL2PortVlanInfo     TmpPortVlanEntry;

    MEMSET (&TmpPortVlanEntry, 0, sizeof (tL2PortVlanInfo));
    TmpPortVlanEntry.u4IfIndex = u4IfIndex;
    TmpPortVlanEntry.u2VlanId = 0;
    pPortVlanEntry = &TmpPortVlanEntry;

    L2_DB_LOCK ();

    /* Delete all the Port Vlan Nodes with the given port as the index */
    while ((pNextPortVlanEntry =
            (tL2PortVlanInfo *) RBTreeGetNext (gL2GlobalInfo.PortVlanTable,
                                               (tRBElem *) pPortVlanEntry,
                                               NULL)) != NULL)
    {
        if (pNextPortVlanEntry->u4IfIndex != u4IfIndex)
        {
            break;
        }

        if (*pu1TxEnabled != LLDP_TRUE)
        {
            /* If the transmission of vlan name TLV is enabled for atleast
             * one of the vlans, then lldp need to be notified so that the
             * pre-formed buffer can be reconstructed.*/
            *pu1TxEnabled = pNextPortVlanEntry->u1VlanNameTxEnable;
        }

        RBTreeRem (gL2GlobalInfo.PortVlanTable, (tRBElem *) pNextPortVlanEntry);
        L2IWF_RELEASE_MEMBLOCK (gL2GlobalInfo.PortVlanInfoPoolId,
                                pNextPortVlanEntry);
        pNextPortVlanEntry = NULL;
    }
    L2_DB_UNLOCK ();
    return;

}

/**************************************************************************
 *
 *     FUNCTION NAME    : L2IwfGetPortVlanNameInfo
 *
 *     DESCRIPTION      : This function returns the vlan id, vlan name and the
 *                        transmission status of the vlans to which the given
 *                        port is a member.
 *
 *     INPUT            : u4IfIndex         - Port Number
 *
 *     OUTPUT           : pVlanNameInfo - pointer to vlan id, vlan name & 
 *                                        tx status of the vlan
 *                        pu2VlanCount - pointer to the no. of vlans 
 *
 *     RETURNS          : L2IWF_SUCCESS/L2IWF_FAILURE
 *
 *****************************************************************************/
INT4
L2IwfGetPortLocVlanNameInfo (UINT4 u4IfIndex, tVlanNameInfo * pVlanNameInfo,
                             UINT2 *pu2VlanCount)
{
    tL2PortVlanInfo    *pPortVlanEntry = NULL;
    tL2PortVlanInfo     TmpPortVlanEntry;
    tL2PortVlanInfo    *pNextPortVlanEntry = NULL;
    UINT2               u2Count = 0;

    MEMSET (&TmpPortVlanEntry, 0, sizeof (tL2PortVlanInfo));
    TmpPortVlanEntry.u4IfIndex = u4IfIndex;
    TmpPortVlanEntry.u2VlanId = 0;
    pPortVlanEntry = &TmpPortVlanEntry;

    L2_DB_LOCK ();
    while ((pNextPortVlanEntry =
            (tL2PortVlanInfo *) RBTreeGetNext (gL2GlobalInfo.PortVlanTable,
                                               (tRBElem *) pPortVlanEntry,
                                               NULL)) != NULL)
    {
        if (pNextPortVlanEntry->u4IfIndex == u4IfIndex)
        {
            pVlanNameInfo[u2Count].u2VlanId = pNextPortVlanEntry->u2VlanId;
            MEMCPY (pVlanNameInfo[u2Count].au1VlanName,
                    pNextPortVlanEntry->au1VlanName,
                    STRLEN (pNextPortVlanEntry->au1VlanName));
            pVlanNameInfo[u2Count].u1VlanNameTxEnable =
                pNextPortVlanEntry->u1VlanNameTxEnable;
            pPortVlanEntry = pNextPortVlanEntry;
            u2Count++;
        }
        else
        {
            if (pNextPortVlanEntry->u4IfIndex != u4IfIndex)
            {
                break;
            }
        }
    }
    *pu2VlanCount = u2Count;
    L2_DB_UNLOCK ();

    return L2IWF_SUCCESS;
}

#endif /*End _L2_VLIST_C_ */
