/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2stp.c,v 1.45 2014/05/27 13:32:11 siva Exp $
 *
 * Description:This file contains routines to get/set Spanning tree      
 *             information from the L2IWF module.                 
 *
 *******************************************************************/
#ifndef _L2_STP_C_
#define _L2_STP_C_

#include "l2inc.h"

/*****************************************************************************/
/* Function Name      : L2IwfGetPortOperEdgeStatus                           */
/*                                                                           */
/* Description        : This routine returns the port's oper edge status     */
/*                      given the Port Index. It accesses the L2Iwf common   */
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose oper    */
/*                                    edge status is to be obtained.         */
/*                                                                           */
/* Output(s)          : bOperEdge - Boolean value indicating whether the     */
/*                                  port is an Edge Port                     */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortOperEdgeStatus (UINT4 u4IfIndex, BOOL1 * pbOperEdge)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    *pbOperEdge = OSIX_FALSE;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        *pbOperEdge = L2IWF_IFENTRY_OPEREDGE_STATUS (pL2PortEntry);
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortOperEdgeStatus                           */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Port's oper edge status in the L2Iwf common database.*/
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u2LocalPortId - Index of the port whose oper edge      */
/*                                    status is to be updated.               */
/*                      bOperEdge - Boolean value indicating whether the     */
/*                                  port is an Edge Port                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetPortOperEdgeStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                            BOOL1 bOperEdge)
{
    UINT4               u4IfIndex;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPortId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    if (bOperEdge == (L2IWF_PORT_INFO (u2LocalPortId))->bOperEdge)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_SUCCESS;
    }

    (L2IWF_PORT_INFO (u2LocalPortId))->bOperEdge = bOperEdge;

    u4IfIndex = (UINT4) L2IWF_PORT_IFINDEX (u2LocalPortId);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    if (bOperEdge == OSIX_TRUE)
    {
        /*trigger for PNAC when the port oper edge status changes */
        PnacHandleEvent ((UINT2) u4IfIndex, PNAC_BRIDGE_NOT_DETECTED);
    }
    else
    {
        /*trigger for PNAC when the port oper edge status changes */
        PnacHandleEvent ((UINT2) u4IfIndex, PNAC_BRIDGE_DETECTED);
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfFillDigestInfo                                  */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to fill the    */
/*                      config digest information with the MSTI that each    */
/*                      Vlan is mapped to. Each entry should occupy 2 bytes. */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      au1DigPtr - Array in which the Config Digest info is */
/*                                  to be filled                             */
/*                                                                           */
/*                      i4NumEntries - Number of entries to be filled        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfFillConfigDigest (UINT4 u4ContextId, UINT1 au1DigPtr[], INT4 i4NumEntries)
{
    UINT2               u2Index;
    UINT2               u2MstInst;
    INT4                i4TmpNumEntries = 0;
    UINT1              *pu1DigPtr = NULL;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    pu1DigPtr = au1DigPtr;

    if (i4NumEntries <= VLAN_DEV_MAX_VLAN_ID)
    {
        i4TmpNumEntries = i4NumEntries;
    }
    else
    {
        i4TmpNumEntries = VLAN_DEV_MAX_VLAN_ID;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    for (u2Index = 1; u2Index <= i4TmpNumEntries; u2Index++, pu1DigPtr += 2)
    {
        u2MstInst = OSIX_HTONS (L2IWF_VID_INST_MAP (u2Index));
        MEMCPY (pu1DigPtr, &u2MstInst, sizeof (UINT2));
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    if (i4NumEntries > VLAN_DEV_MAX_VLAN_ID)
    {
        /* If VLAN_DEV_MAX_VLAN_ID is changed to less than i4NumEntries then
         * this loop will fill the remaining entries as if those remaining 
         * vlans are mapped to the CIST */
        for (; u2Index <= i4NumEntries; u2Index++, pu1DigPtr += 2)
        {
            u2MstInst = MST_CIST_CONTEXT;
            u2MstInst = OSIX_HTONS (u2MstInst);
            MEMCPY (pu1DigPtr, &u2MstInst, sizeof (UINT2));
        }
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPerformanceDataStatus                        */
/*                                                                           */
/* Description        : This routine updates the status of Enhanced Filtering*/
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id to which this vlan belongs*/
/*                      b1PerfDataStatus - Status of Enhanced Filtering     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetPerformanceDataStatus (UINT4 u4ContextId, BOOL1 b1PerfDataStatus)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_STP_PERFORMANCE_DATA_STATUS () = b1PerfDataStatus;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

INT4
L2IwfGetPerformanceDataStatus (UINT4 u4ContextId, BOOL1 * pbPerfDataStatus)
{
    L2_LOCK ();
    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pbPerfDataStatus = L2IWF_STP_PERFORMANCE_DATA_STATUS ();
    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetVlanListInInstance                           */
/*                                                                           */
/* Description        : This routine returns the list of vlans mapped to an  */
/*                      MSTP instance.                                       */
/*                                                                           */
/* Input(s)           :  u2MstInst   - InstanceId for which the list of      */
/*                                     vlans mapped needs to be obtained.    */
/*                       pu1VlanList - VlanList to be returned               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of vlans mapped to the instance               */
/*****************************************************************************/
UINT2
L2IwfGetVlanListInInstance (UINT2 u2MstInst, UINT1 *pu1VlanList)
{
    return L2IwfMiGetVlanListInInstance (L2IWF_DEFAULT_CONTEXT, u2MstInst,
                                         pu1VlanList);
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPortOperPointToPointStatus                   */
/*                                                                           */
/* Description        : This routine returns the port's oper point to point  */
/*                      status of given the Port Index.                      */
/*                      It accesses the L2Iwf common database.               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose oper    */
/*                                  point to point status is to be obtained. */
/*                                                                           */
/* Output(s)          : bOperPointToPoint - Boolean value indicating whether */
/*                      the port is Point To Point                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfGetPortOperPointToPointStatus (UINT4 u4IfIndex, BOOL1 * pbOperPointToPoint)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    *pbOperPointToPoint = OSIX_FALSE;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        *pbOperPointToPoint = L2IWF_IFENTRY_OPER_P2P_STATUS (pL2PortEntry);
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPortOperPointToPointStatus                   */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      Port's oper point to point status in the L2Iwf common*/
/*                      database.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                                                                           */
/*                      u2LocalPortId - Index of the port whose oper point to*/
/*                      point status is to be updated.                       */
/*                                                                           */
/*                      bOperPointToPoint - Boolean value indicating whether */
/*                      the port is an Point To Point Port                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfSetPortOperPointToPointStatus (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                    BOOL1 bOperPointToPoint)
{
    tMrpInfo            MrpInfo;
    UINT4               u4IfIndex;

    MEMSET (&MrpInfo, 0, sizeof (tMrpInfo));

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPortId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    if (bOperPointToPoint ==
        (L2IWF_PORT_INFO (u2LocalPortId))->bOperPointToPoint)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_SUCCESS;
    }

    (L2IWF_PORT_INFO (u2LocalPortId))->bOperPointToPoint = bOperPointToPoint;

    u4IfIndex = (UINT4) L2IWF_PORT_IFINDEX (u2LocalPortId);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    /*trigger for Vlan when the port oper point to point status changes */
    VlanPortOperPointToPointUpdate (u4IfIndex);

    MrpInfo.u1Flag = MSG_PORT_OPER_P2P;
    MrpInfo.u4IfIndex = u4IfIndex;
    MrpInfo.b1TruthVal = bOperPointToPoint;

    MrpApiNotifyStpInfo (&MrpInfo);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfMrpApiNotifyStpInfo                             */
/*                                                                           */
/* Description        : This routine is called from RSTP/MSTP to update the  */
/*                      TC Detected and point to point status in the         */
/*                      L2Iwf common database.                               */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : pMrpInfo - Pointer to the structure which contains   */
/*                                 the information that needs to be indicate */
/*                                 to MRP.                                   */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/ OSIX_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfMrpApiNotifyStpInfo (tMrpInfo * pMrpInfo)
{
    return (MrpApiNotifyStpInfo (pMrpInfo));
}

#endif /*End _L2_STP_C_ */
