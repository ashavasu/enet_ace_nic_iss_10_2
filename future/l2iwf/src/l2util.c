/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved   
 *
 * $Id: l2util.c,v 1.148 2018/01/11 11:18:53 siva Exp $
 *
 * Description:This file contains the utilitiy functions exported
 *             by L2IWF
 *
 *******************************************************************/
#include "l2inc.h"
#ifdef KERNEL_WANTED
#include "chrdev.h"
#endif
#ifdef NPAPI_WANTED
#include "rstnp.h"
#endif
#include "garp.h"
#include "fsbuddy.h"

extern UINT4        gu4L2FramePoolId;
/*****************************************************************************
 * Important Note:
 * ===============
 * Please note that switches for Layer 2 protocols have been introduced in this
 * file  to make the porting process easier for the individual Layer 2 stacks.
 * Such switches may not be introduced else where in the code.
 *****************************************************************************/

/*****************************************************************************/
/* Function Name      : L2Lock                                               */
/*                                                                           */
/* Description        : This function is used to take the L2 mutual          */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      the common L2IWF data structures.                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/

INT4
L2Lock (VOID)
{
    if (L2IWF_TAKE_SEM (gL2GlobalInfo.SemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2UnLock                                             */
/*                                                                           */
/* Description        : This function is used to give the L2  mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      the common L2IWF data structures.                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/

INT4
L2Unlock (VOID)
{
    L2IWF_GIVE_SEM (gL2GlobalInfo.SemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2DBLock                                             */
/*                                                                           */
/* Description        : This function is used to take the L2 mutual          */
/*                      exclusion Database SEMa4 to avoid simultaneous       */
/*                      access to the Port Vlan Table data structure.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/

INT4
L2DBLock (VOID)
{
    if (L2IWF_TAKE_SEM (gL2GlobalInfo.DBSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2DBUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the L2  mutual         */
/*                      exclusion Database SEMa4.                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/

INT4
L2DBUnlock (VOID)
{
    L2IWF_GIVE_SEM (gL2GlobalInfo.DBSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2SyncTakeSem                                        */
/*                                                                           */
/* Description        : This function is used to take the L2 mutual          */
/*                      exclusion SEMa4 to inform synchronously events like  */
/*                      create port, port oper indication etc.               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/

INT4
L2SyncTakeSem (VOID)
{
    if (L2IWF_TAKE_SEM (gL2GlobalInfo.SyncSemId) != OSIX_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2SyncGiveSem                                        */
/*                                                                           */
/* Description        : This function is used to take the L2 mutual          */
/*                      exclusion SEMa4 to inform synchronously events like  */
/*                      create port, port oper indication etc.               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
L2SyncGiveSem (VOID)
{
    L2IWF_GIVE_SEM (gL2GlobalInfo.SyncSemId);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2MiSyncTakeSem                                      */
/*                                                                           */
/* Description        : This function is used to take the L2 mutual          */
/*                      exclusion SEMa4 to inform synchronously events like  */
/*                      create port, port oper indication etc.               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/

INT4
L2MiSyncTakeSem (VOID)
{
    if (L2IWF_TAKE_SEM (gL2GlobalInfo.MiSyncSemId) != OSIX_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2MiSyncGiveSem                                      */
/*                                                                           */
/* Description        : This function is used to take the L2 mutual          */
/*                      exclusion SEMa4 to inform synchronously events like  */
/*                      create port, port oper indication etc.               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*****************************************************************************/
INT4
L2MiSyncGiveSem (VOID)
{
    L2IWF_GIVE_SEM (gL2GlobalInfo.MiSyncSemId);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSelectContext                                   */
/*                                                                           */
/* Description        : This routine is called to update the current context */
/*                      pointer so that processing can be started for that   */
/*                      context.                                             */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfSelectContext (UINT4 u4ContextId)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    gpL2Context = L2IWF_CONTEXT_INFO (u4ContextId);

    if (gpL2Context == NULL)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfReleaseContext                                  */
/*                                                                           */
/* Description        : This routine is called to indicate that the          */
/*                      processing is complete in this context.              */
/*                                                                           */
/* Input(s)           : u4ContextId                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfReleaseContext (VOID)
{
    gpL2Context = NULL;

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfCreatePort                                      */
/*                                                                           */
/* Description        : This routine makes the given port active and         */
/*                      initialises the entry for the port in the L2IWF      */
/*                      data structure                                       */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Swith ID                       */
/*                      u4IfIndex - Global Port Index                        */
/*                      u2LocalPortId - Per Context port index               */
/*                      u1IfType - Type of the port                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/

INT4
L2IwfCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId,
                 tCfaIfInfo * pCfaIfInfo)
{
    tL2PortInfo        *pL2PortEntry = NULL;
#ifdef RSTP_WANTED
    UINT2               u2MstInst;
#endif
    UINT2               u2SessionNo = 0;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry != NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pL2PortEntry = (tL2PortInfo *)
        L2IWF_ALLOCATE_MEMBLOCK (L2IWF_PORTINFO_POOLID ());

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    MEMSET (pL2PortEntry, 0, sizeof (tL2PortInfo));

    pL2PortEntry->u4IfIndex = u4IfIndex;
    L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry) = u4ContextId;
    L2IWF_IFENTRY_LOCAL_PORT (pL2PortEntry) = u2LocalPortId;
    pL2PortEntry->u1PortOwner = STP_MODULE;

    if (L2IwfAddIfIndexEntry (pL2PortEntry) == L2IWF_FAILURE)
    {
        L2IWF_RELEASE_MEMBLOCK (L2IWF_PORTINFO_POOLID (), pL2PortEntry);
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2IwfRemoveIfIndexEntry (pL2PortEntry);
        L2IWF_RELEASE_MEMBLOCK (L2IWF_PORTINFO_POOLID (), pL2PortEntry);
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        L2IwfRemoveIfIndexEntry (pL2PortEntry);
        L2IWF_RELEASE_MEMBLOCK (L2IWF_PORTINFO_POOLID (), pL2PortEntry);
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_PORT_INFO (u2LocalPortId) = pL2PortEntry;

    if ((pCfaIfInfo->u1BrgPortType == L2IWF_CNP_PORTBASED_PORT) ||
        (pCfaIfInfo->u1BrgPortType == L2IWF_CNP_CTAGGED_PORT) ||
        (pCfaIfInfo->u1BrgPortType == L2IWF_CNP_STAGGED_PORT))
    {
        /* Update CNP count */
        ++L2IWF_CNP_COUNT ();
    }

    if (pCfaIfInfo->i4IsInternalPort == L2IWF_TRUE)
    {
        ++L2IWF_INTERNAL_COUNT ();
    }

    /* Initially set Port Channel Index to be the same as the Port Index
     * indicating that the port is not a member of any port channel */
    L2IWF_AGG_INDEX_FOR_PORT (u2LocalPortId) = u4IfIndex;
    L2IWF_PORT_TUNNEL_STATUS (u2LocalPortId) = OSIX_FALSE;
    L2IWF_PORT_OPER_CONTROL_DIR (u2LocalPortId) = PNAC_CNTRLD_DIR_BOTH;
    L2IWF_PORT_AUTH_CONTROL_PORT_CONTROL (u2LocalPortId) =
        PNAC_PORTCNTRL_FORCEAUTHORIZED;
    L2IWF_PORT_AUTH_CONTROL_PORT_STATUS (u2LocalPortId) =
        PNAC_PORTSTATUS_UNAUTHORIZED;
    pL2PortEntry->u2PortAuthMode = PNAC_PORT_AUTHMODE_PORTBASED;
    pL2PortEntry->u1PortPaeStatus = OSIX_ENABLED;
    L2IWF_PORT_BRIDGE_OPER_STATUS (u2LocalPortId) = CFA_IF_DOWN;
    L2IWF_PORT_OPER_EDGE (u2LocalPortId) = OSIX_FALSE;
    L2IWF_PORT_TYPE (u2LocalPortId) = VLAN_DEFAULT_PORT;
#ifdef GARP_WANTED
    L2IWF_PORT_GVRP_STATUS (pL2PortEntry) = OSIX_DISABLED;
    L2IWF_PORT_GMRP_STATUS (pL2PortEntry) = OSIX_DISABLED;
#endif
    L2IWF_PORT_VID_TRANS_STATUS (u2LocalPortId) = OSIX_DISABLED;

#if defined (RSTP_WANTED) || defined (MSTP_WANTED) || defined (PVRST_WANTED)
    if (((IssGetAutoPortCreateFlag () == ISS_DISABLE)) ||
        (pCfaIfInfo->u1IfType == CFA_PSEUDO_WIRE) ||
        ((pCfaIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
         (pCfaIfInfo->u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)) ||
        (pCfaIfInfo->u1IfType == CFA_VXLAN_NVE) ||
        (pCfaIfInfo->u1IfType == CFA_TAP))
    {
        L2IWF_PORT_STP_STATUS (pL2PortEntry) = OSIX_DISABLED;
    }
    else
    {
        if ((AstIsPvrstStartedInContext (u4ContextId) ||
             AstIsMstStartedInContext (u4ContextId) ||
             AstIsPvrstStartedInContext (u4ContextId)))
        {
            L2IWF_PORT_STP_STATUS (pL2PortEntry) = OSIX_ENABLED;
        }
    }
#else
    L2IWF_PORT_STP_STATUS (pL2PortEntry) = OSIX_DISABLED;
#endif
    L2IWF_PB_PORT_TYPE (pL2PortEntry) = pCfaIfInfo->u1BrgPortType;
    /* Reset the Oper Flag for VIP */
    pL2PortEntry->u1VipOperStatusFlag = CFA_FALSE;

    /* SISP should be disabled by default */
    pL2PortEntry->u1IsSispEnabled = L2IWF_DISABLED;
    /* Port should be configurable in LA by default */
    pL2PortEntry->u1LaConfigAllowed = OSIX_TRUE;

    /* Check whether the mirror source is configured.
       u2SessionNo is passed with '0' value to check whether the port is configured with any of the sessions   */
    if ((IssMirrIsSrcConfigured
         (u2SessionNo, L2IWF_DEFAULT_CONTEXT, u4IfIndex,
          ISS_MIRR_PORT_BASED) == ISS_SUCCESS)
        || (IssMirrIsDestConfigured (u2SessionNo, u4IfIndex) == ISS_SUCCESS))
    {
        pL2PortEntry->u1LaConfigAllowed = OSIX_FALSE;
    }

    L2IWF_INST_PORT_STATE_PTR (u2LocalPortId) = (UINT1 *)
        MemBuddyAlloc (gL2GlobalInfo.u1PortStateBuddyId, AST_MAX_MST_INSTANCES);

    if (L2IWF_INST_PORT_STATE_PTR (u2LocalPortId) == NULL)
    {
        L2IwfRemoveIfIndexEntry (pL2PortEntry);
        L2IWF_RELEASE_MEMBLOCK (L2IWF_PORTINFO_POOLID (), pL2PortEntry);
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

#ifdef RSTP_WANTED
    for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES; u2MstInst++)
    {
        if (L2IWF_PORT_STP_STATUS (pL2PortEntry) == OSIX_DISABLED)
        {
            L2IwfMiSetInstPortState (u4ContextId, u2MstInst, u2LocalPortId,
                                     AST_PORT_STATE_FORWARDING);
        }
        else
        {
            L2IwfMiSetInstPortState (u4ContextId, u2MstInst,
                                     u2LocalPortId, AST_PORT_STATE_DISCARDING);
        }
    }

#endif
    L2IwfReleaseContext ();

    if (pCfaIfInfo->u1IfType == CFA_LAGG)
    {
        if (L2IWF_PORT_CHANNEL_CONFIGURED (u4IfIndex) == OSIX_FALSE)
        {
            MEMSET (L2IWF_PORT_CHANNEL_INFO (u4IfIndex),
                    0, sizeof (tL2PortChannelInfo));
            L2IWF_PORT_CHANNEL_CONFIGURED (u4IfIndex) = OSIX_TRUE;
        }

        L2IWF_PORT_CHANNEL_VALID (u4IfIndex) = OSIX_TRUE;
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeleteEntry                                     */
/*                                                                           */
/* Description        : This routine performs L2IwfRemoveIfIndexEntry and    */
/*                      releases the corresponding memory associated with it */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index                               */
/*                      u1IfType - Type of the port                          */
/*                      u1IsCnpCheck - Check whether CNP count needs to be   */
/*                                     updated or not.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfDeleteEntry (UINT4 u4IfIndex, UINT1 u1IfType, UINT1 u1IsCnpCheck)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    UINT2               u2LocalPortId;
    tCfaIfInfo          IfInfo;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    u2LocalPortId = L2IWF_IFENTRY_LOCAL_PORT (pL2PortEntry);

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IwfRemoveIfIndexEntry (pL2PortEntry);
    L2IWF_RELEASE_MEMBLOCK (L2IWF_PORTINFO_POOLID (), pL2PortEntry);

    L2_UNLOCK ();
    UNUSED_PARAM (u1IfType);
    UNUSED_PARAM (u1IsCnpCheck);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDelPort                                         */
/*                                                                           */
/* Description        : This routine makes the given port inactive in the    */
/*                      L2 data structure. This is special to preserve       */
/*                      IfIndexEntry without removing                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index                               */
/*                      u1IfType - Type of the port                          */
/*                      u1IsCnpCheck - Check whether CNP count needs to be   */
/*                                     updated or not.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfDelPort (UINT4 u4IfIndex, UINT1 u1IfType, UINT1 u1IsCnpCheck)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tVlanId             i4SVlanId = 0;
    UINT2               u2LocalPortId;
    tCfaIfInfo          IfInfo;
    UINT1               u1IntfTypeIndFlag = L2IWF_FALSE;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    u2LocalPortId = L2IWF_IFENTRY_LOCAL_PORT (pL2PortEntry);

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IwfSelectContext (L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry))
        == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    if ((L2IWF_PORT_INFO ((u2LocalPortId)) != NULL))
    {
        if (L2IWF_INST_PORT_STATE_PTR (u2LocalPortId) != NULL)
        {
            MemBuddyFree (gL2GlobalInfo.u1PortStateBuddyId,
                          L2IWF_INST_PORT_STATE_PTR (u2LocalPortId));
        }
    }

    L2IWF_PORT_INFO (u2LocalPortId) = NULL;

    if ((u1IsCnpCheck == L2IWF_TRUE) &&
        ((IfInfo.u1BrgPortType == L2IWF_CNP_PORTBASED_PORT) ||
         (IfInfo.u1BrgPortType == L2IWF_CNP_CTAGGED_PORT) ||
         (IfInfo.u1BrgPortType == L2IWF_CNP_STAGGED_PORT)))
    {
        /* Update CNP count */
        --L2IWF_CNP_COUNT ();
        if (L2IWF_CNP_COUNT () == 0)
        {
            L2IWF_INTERFACE_TYPE () = L2IWF_S_INTERFACE_TYPE;
            L2IWF_ADMIN_INTERFACE_TYPE_FLAG () = L2IWF_FALSE;
            u1IntfTypeIndFlag = L2IWF_TRUE;
        }
    }

    if (IfInfo.i4IsInternalPort == L2IWF_TRUE)
    {
        --L2IWF_INTERNAL_COUNT ();
    }
#ifdef PB_WANTED
    /* Previously, PEP entries are not deleted in the L2IwfDeletePort function
     * because, it is believed that in VlanHandleDeletePort,it will be taken cared.
     * But L2IwfApi for deleting the PEP port will fail when it is called from VLAN
     * because, the basic L2Iwf Port Entry is deleted here.
     * This issue has not popped up quite sometime, somehow, the context switching
     * of the tasks ensured that the VLAN deleted the PEP entries for the port.*/

    for (i4SVlanId = 1; i4SVlanId <= VLAN_MAX_VLAN_ID; i4SVlanId++)
    {
        L2IwfPbUtilDeletePEPEntry (u2LocalPortId, i4SVlanId);
    }
#else
    UNUSED_PARAM (i4SVlanId);
#endif

    L2IwfReleaseContext ();

    L2_UNLOCK ();
    if (u1IntfTypeIndFlag == L2IWF_TRUE)
    {
        L2IwfIntfTypeChangeIndication (u4IfIndex,
                                       (UINT1) L2IWF_S_INTERFACE_TYPE);
    }
    UNUSED_PARAM (u1IfType);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfDeletePort                                      */
/*                                                                           */
/* Description        : This routine makes the given port inactive in the    */
/*                      L2 data structure                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index                               */
/*                      u1IfType - Type of the port                          */
/*                      u1IsCnpCheck - Check whether CNP count needs to be   */
/*                                     updated or not.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfDeletePort (UINT4 u4IfIndex, UINT1 u1IfType, UINT1 u1IsCnpCheck)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tVlanId             i4SVlanId = 0;
    UINT2               u2LocalPortId;
    tCfaIfInfo          IfInfo;
    UINT1               u1IntfTypeIndFlag = L2IWF_FALSE;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    u2LocalPortId = L2IWF_IFENTRY_LOCAL_PORT (pL2PortEntry);

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IwfSelectContext (L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry))
        == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    if ((L2IWF_PORT_INFO ((u2LocalPortId)) != NULL))
    {
        if (L2IWF_INST_PORT_STATE_PTR (u2LocalPortId) != NULL)
        {
            MemBuddyFree (gL2GlobalInfo.u1PortStateBuddyId,
                          L2IWF_INST_PORT_STATE_PTR (u2LocalPortId));
        }
    }

    L2IWF_PORT_INFO (u2LocalPortId) = NULL;

    if ((u1IsCnpCheck == L2IWF_TRUE) &&
        ((IfInfo.u1BrgPortType == L2IWF_CNP_PORTBASED_PORT) ||
         (IfInfo.u1BrgPortType == L2IWF_CNP_CTAGGED_PORT) ||
         (IfInfo.u1BrgPortType == L2IWF_CNP_STAGGED_PORT)))
    {
        /* Update CNP count */
        --L2IWF_CNP_COUNT ();
        if (L2IWF_CNP_COUNT () == 0)
        {
            L2IWF_INTERFACE_TYPE () = L2IWF_S_INTERFACE_TYPE;
            L2IWF_ADMIN_INTERFACE_TYPE_FLAG () = L2IWF_FALSE;
            u1IntfTypeIndFlag = L2IWF_TRUE;
        }
    }

    if (IfInfo.i4IsInternalPort == L2IWF_TRUE)
    {
        --L2IWF_INTERNAL_COUNT ();
    }
#ifdef PB_WANTED
    /* Previously, PEP entries are not deleted in the L2IwfDeletePort function 
     * because, it is believed that in VlanHandleDeletePort,it will be taken cared.
     * But L2IwfApi for deleting the PEP port will fail when it is called from VLAN 
     * because, the basic L2Iwf Port Entry is deleted here.
     * This issue has not popped up quite sometime, somehow, the context switching
     * of the tasks ensured that the VLAN deleted the PEP entries for the port.*/

    for (i4SVlanId = 1; i4SVlanId <= VLAN_MAX_VLAN_ID; i4SVlanId++)
    {
        L2IwfPbUtilDeletePEPEntry (u2LocalPortId, i4SVlanId);
    }
#else
    UNUSED_PARAM (i4SVlanId);
#endif

    L2IwfReleaseContext ();

    L2IwfRemoveIfIndexEntry (pL2PortEntry);

    L2IWF_RELEASE_MEMBLOCK (L2IWF_PORTINFO_POOLID (), pL2PortEntry);

    L2_UNLOCK ();
    if (u1IntfTypeIndFlag == L2IWF_TRUE)
    {
        L2IwfIntfTypeChangeIndication (u4IfIndex,
                                       (UINT1) L2IWF_S_INTERFACE_TYPE);
    }
    UNUSED_PARAM (u1IfType);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsIgmpReportPacket                              */
/*                                                                           */
/* Description        : This routine is used to identify if the received     */
/*                      frame is an igmp report packet                       */
/*                                                                           */
/* Input(s)           : pBuf        - Pointer to the Frame                   */
/*                                                                           */
/* Output(s)          : u1Result    - Pointer to the result that indicates   */
/*                      OSIX_TRUE if the frame is an igmp report and         */
/*                      OSIX_FALSE otherwise.                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfIsIgmpControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Result)
{
    UINT4               u4VlanOffset = VLAN_TAG_OFFSET;
    UINT4               u4IfIndex;
    UINT2               u2LenOrType = 0;
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         IpHdr;

    MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
    pIpHdr = &IpHdr;

    u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;

    if (VlanGetTagLenInFrame (pBuf, u4IfIndex, &u4VlanOffset) == VLAN_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                                   u4VlanOffset, VLAN_TYPE_OR_LEN_SIZE);
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
    }

    /* If the packet type is not IP return FALSE.
     */
    if (u2LenOrType != L2_ENET_IP)
    {
        *pu1Result = OSIX_FALSE;

        return L2IWF_SUCCESS;
    }

    /* Extracting IP header from the packet to identify the
     * protocol type.
     */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr,
                               u4VlanOffset + VLAN_TYPE_OR_LEN_SIZE,
                               sizeof (t_IP_HEADER));

    if (pIpHdr->u1Proto == L2_IGMP_CONTROL_PKT)
    {
        *pu1Result = OSIX_TRUE;
    }
    else
    {
        *pu1Result = OSIX_FALSE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsEcfmControlPacket                             */
/*                                                                           */
/* Description        : This routine is used to identify if the received     */
/*                      frame is an CFM control packet.                      */
/*                      Get the offset where Length/Type field is            */
/*                      (VlanGetVlanInfoFromFrame).                          */
/*                      For S-ECFM packets in 1ah bridges give it to         */
/*                      L2iwfForwardFrameToPip.                              */
/*                      If the ether type is ECFM ether type the return the  */
/*                      L2_ECFM_PACKET.                                      */
/*                      B-ECFM/I-ECFM packets are returned as L2_ECFM_PACKET */
/*                                                                           */
/* Input(s)           : pBuf        - Pointer to the Frame                   */
/*                                                                           */
/* Output(s)          : u1Result    - Pointer to the result that indicates   */
/*                      OSIX_TRUE if the frame is an igmp report and         */
/*                      OSIX_FALSE otherwise.                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfIsEcfmControlPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1PktType)
{
#ifdef ECFM_WANTED
    UINT4               u4VlanOffset = VLAN_TAG_OFFSET;
    UINT4               u4IfIndex;
    UINT4               u4TypeOffsetInLlcHdr;
    UINT2               u2LenOrType = 0;
    /*To classify the llc encapsulated
       frames */

    *pu1PktType = L2_DATA_PKT;

    u4IfIndex = pBuf->ModuleData.InterfaceId.u4IfIndex;

    if (VlanGetTagLenInFrame (pBuf, u4IfIndex, &u4VlanOffset) == VLAN_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                                   u4VlanOffset, VLAN_TYPE_OR_LEN_SIZE);
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
    }

    /* To classify the llc encapsulated frames */
    if ((u2LenOrType <= VLAN_FRAME_MAXIMUM_LENGTH) &&
        (u2LenOrType != ECFM_PDU_TYPE_CFM))
    {
        /*8 is 2 byte of typelength in VLAN hdr and 6byte of the LLC */
        u4TypeOffsetInLlcHdr = u4VlanOffset + 8;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                                   u4TypeOffsetInLlcHdr, VLAN_TYPE_OR_LEN_SIZE);
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
    }

    /*Received CFM frames contains the Type */
    if (u2LenOrType == ECFM_PDU_TYPE_CFM)
    {
        *pu1PktType = L2_ECFM_PACKET;
        return L2IWF_SUCCESS;
    }
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pu1PktType);
#endif /*ECFM_WANTED */
    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsErpsPacket                                    */
/*                                                                           */
/* Description        : This routine is used to identify if the received     */
/*                      frame is an ERPS packet.                             */
/* Input(s)           : pBuf        - Pointer to the Frame                   */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS incase of ERPS PDU else returns        */
/*                      L2IWF_FAILURE                                        */
/*****************************************************************************/
INT4
L2IwfIsErpsPacket (tCRU_BUF_CHAIN_HEADER * pBuf)
{
#ifdef ERPS_WANTED
    tMacAddr            RapsGrpMac = { 0x01, 0x19, 0xa7, 0x0, 0x0, 0x01 };
    tMacAddr            DestAddr;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) DestAddr, 0, MAC_ADDR_LEN);

    if (MEMCMP (DestAddr, RapsGrpMac, sizeof (tMacAddr)) == 0)
    {
        return L2IWF_SUCCESS;
    }
#else
    UNUSED_PARAM (pBuf);
#endif
    return L2IWF_FAILURE;
}

/***************************************************************************/
/* FUNCTION NAME    : L2IwfIsErpsStartedInContext                          */
/*                                                                         */
/* DESCRIPTION      : This function will check whether ERPS is started     */
/*                    in the context or not to map the vlan to instance    */
/*                                                                         */
/* INPUT            : u4ContextId - Context Identifier                     */
/*                                                                         */
/* OUTPUT           : NONE                                                 */
/*                                                                         */
/* RETURNS          : OSIX_TRUE/OSIX_FALSE                                 */
/*                                                                         */
/***************************************************************************/
PUBLIC INT4
L2IwfIsErpsStartedInContext (UINT4 u4ContextId)
{
#ifdef ERPS_WANTED
    if (ErpsApiIsErpsStartedInContext (u4ContextId) == OSIX_TRUE)
    {
        return OSIX_TRUE;
    }
    else
    {
        return OSIX_FALSE;
    }
#else
    UNUSED_PARAM (u4ContextId);
#endif
    return OSIX_FALSE;
}

/*******************************************************************/
/*  Function Name     : L2IwfL3LAGPortOperIndication               */
/*                                                                 */
/* Description        : This function will send notifiction for    */
/*                      OperStatus up and Down to LA for Routerport*/
/* Input(s)           : UINT4 u4IfIndex ,UINT1 u1OperStatus        */
/*                                                                 */
/* Return Value(s)    : L2IWF_SUCCESS                              */
/*                      L2IWF_FAILURE                              */
/*******************************************************************/
INT4
L2IwfL3LAGPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{

    tIfTypeDenyProtocolList DenyProtocolList;
    tCfaIfInfo          IfInfo;
    MEMSET (&IfInfo, OSIX_FALSE, sizeof (tCfaIfInfo));

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

#ifdef LA_WANTED
    /* Assumption : LA is applicable for Ethernet interfaces only */
    if (LaGetLaEnableStatus () == LA_ENABLED)
    {
        LaUpdatePortStatus ((UINT2) u4IfIndex, u1OperStatus);

        /* NOTE : If LA is enabled, CFA should not inform other
         * applications about the physical ports. That will be done
         * after receiving an acknowledgement from LA module.
         *                                                    */
        return L2IWF_SUCCESS;
    }
#else /* LA_WANTED */
    UNUSED_PARAM (u1OperStatus);
#endif
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPnacHLPortOperIndication                        */
/*                                                                           */
/* Description        : This routine is used by PNAC whenever the operational*/
/*                      status of the port changes in PNAC.                  */
/*                      This routine informs this to LA   if it is enabled   */
/*                      in the system. LA   will inturn indicate to its      */
/*                      higher layers. This routine takes care of indicating */
/*                      the port oper status change to LA's higher layers    */
/*                      if LA   is not enabled in the system.                */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Global Interface index.                 */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfPnacHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tIfTypeDenyProtocolList DenyProtocolList;
    tCfaIfInfo          IfInfo;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, &IfInfo,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    if (IfInfo.u1IfType == CFA_ENET)
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_LLDP] == OSIX_FALSE)
        {
            /* Send oper staus change indication to LLDP module */
            LldpApiNotifyIfOperStatusChg (u4IfIndex, u1OperStatus);
        }
    }

#ifdef LA_WANTED
    /* Assumption : LA is applicable for Ethernet interfaces only */
    if (IfInfo.u1IfType == CFA_ENET)
    {
#ifdef RM_WANTED
        if (RmGetNodeState () == RM_ACTIVE)
        {
#endif /*RM_WANTED */
            if (DenyProtocolList[L2IWF_PROTOCOL_ID_LA] == OSIX_FALSE)
            {
                if (LaGetLaEnableStatus () == LA_ENABLED)
                {
                    LaUpdatePortStatus ((UINT2) u4IfIndex, u1OperStatus);

                    /* NOTE : If LA is enabled, CFA should not inform other 
                     * applications about the physical ports. That will be done 
                     * after receiving an acknowledgement from LA module.
                     */
                    return L2IWF_SUCCESS;
                }
            }
#ifdef RM_WANTED
        }
#endif /*RM_WANTED */
    }
#endif /* LA_WANTED */

    L2IwfLaHLPortOperIndication (u4IfIndex, u1OperStatus);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfLaHLPortOperIndication                          */
/*                                                                           */
/* Description        : This routine is invoked from LA when the operational */
/*                      status of the port in LA changes. The routine informs*/
/*                      this to LA's higher layers.                          */
/*                                                                           */
/* Input(s)           : u4IfIndex    - Global IfIndex of the port whose oper */
/*                                     status has changed                    */
/*                      u1OperStatus - Changed status                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
L2IwfLaHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    INT4                i4RetStatus = L2IWF_SUCCESS;
    UINT1               u1IfType = 0;

    CfaGetIfaceType (u4IfIndex, &u1IfType);

    if (u1IfType == CFA_LAGG)
    {
        /* For port channel, the oper indication comes from the LA module.
         * L2SyncTakeSem must be handled via CFA module, hence posting the
         * event to CFA module */
        CfaInterfaceHLPortOperIndication (u4IfIndex, u1OperStatus);
    }
    else
    {
        i4RetStatus = L2IwfUtlBrgHLPortOperIndication (u4IfIndex, u1OperStatus);
    }

    return i4RetStatus;
}

/*****************************************************************************/
/* Function Name      : L2IwfUtlBrgHLPortOperIndication                      */
/*                                                                           */
/* Description        : This routine is invoked from CFA when the bridge     */
/*                      operational status of the port changes to up or down.*/
/*                      The routine informs this to BRIDGE's higher layers.  */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfUtlBrgHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1OperStatus)
{

    tIfTypeDenyProtocolList DenyProtocolList;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1BridgeStatus = CFA_ENABLED;
#ifdef KERNEL_WANTED
    tKernIntfParams     KernIntfParams;
    INT4                i4RetVal;

    KernIntfParams.u4Action = GDD_PORT_OPER_STATUS;
    KernIntfParams.u4IfIndex = u4IfIndex;
    KernIntfParams.InterfaceSet.u1OperStatus = u1OperStatus;

    i4RetVal = KAPIUpdateInfo (GDD_INTERFACE,
                               (tKernCmdParam *) & KernIntfParams);

    if (i4RetVal < 0)
    {
        return L2IWF_FAILURE;
    }
#endif
    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    CfaGetIfName (u4IfIndex, au1IfName);
    CfaGetIfBridgedIfaceStatus (u4IfIndex, &u1BridgeStatus);

    if (u1BridgeStatus == CFA_ENABLED)
    {
        if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, NULL,
                                            DenyProtocolList) == L2IWF_FAILURE)
        {
            return L2IWF_FAILURE;
        }
    }
    /* Send it to all higher layers here */
#ifdef BRIDGE_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_BRIDGE] == OSIX_FALSE)
    {
        BridgePortOperStatusIndication (u4IfIndex, u1OperStatus);
    }
#else
    L2IwfBrgHLPortOperIndication (u4IfIndex, u1OperStatus);
#endif /* BRIDGE_WANTED */

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfBrgHLPortOperIndication                         */
/*                                                                           */
/* Description        : This routine is invoked from BRIDGE when the bridge  */
/*                      operational status of the port changes to up or down.*/
/*                      The routine informs this to BRIDGE's higher layers.  */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfBrgHLPortOperIndication (UINT4 u4IfIndex, UINT1 u1Status)
{

    tIfTypeDenyProtocolList DenyProtocolList;
    tL2IwfContextInfo   L2IwfContextInfo;
    UINT4               au4SispPorts[L2IWF_MAX_CONTEXTS];
    UINT4               u4PortCount;
    UINT4               u4TempPortCnt = 0;

    if (VcmGetContextInfoFromIfIndex
        (u4IfIndex, &L2IwfContextInfo.u4ContextId,
         &L2IwfContextInfo.u2LocalPortId) != VCM_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, NULL,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (au4SispPorts, 0, sizeof (au4SispPorts));

    L2IwfSetBridgePortOperStatus (u4IfIndex, u1Status);
#ifdef QOSX_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_QOS] == OSIX_FALSE)
    {
        QosPortOperIndication (u4IfIndex, u1Status);
    }
#endif

    L2IwfHigherLayerModulesIndication (u4IfIndex, u1Status, &L2IwfContextInfo);

    /* Give indication to logical (SISP) ports of the particular physical or
       port channel port */

    if (VcmSispGetSispPortsInfoOfPhysicalPort (u4IfIndex, VCM_FALSE,
                                               (VOID *) au4SispPorts,
                                               &u4PortCount) == VCM_SUCCESS)
    {
        for (; u4TempPortCnt < u4PortCount; u4TempPortCnt++)
        {
            L2IwfSetBridgePortOperStatus (au4SispPorts[u4TempPortCnt],
                                          u1Status);

            L2IwfHigherLayerModulesIndication (au4SispPorts[u4TempPortCnt],
                                               u1Status, &L2IwfContextInfo);
        }
    }
    return CFA_SUCCESS;
}

#if defined (BRIDGE_WANTED) || (VLAN_WANTED)
/*****************************************************************************/
/* Function Name      : L2IwfGetAgeoutInt                                    */
/*                                                                           */
/* Description        : This routine is called to get the mac address table  */
/*                      aging time.                                          */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                      Vlan if bridge module is disabled                    */
/*****************************************************************************/

UINT4
L2IwfGetAgeoutInt ()
{
    /* TODO: Add Context ID or call BrgGetAgeOutInt from VLAN itself */
#ifdef BRIDGE_WANTED
    return BRG_GET_AGEOUT_TIME ();
#else
    return VLAN_GET_AGEOUT_TIME ();
#endif
}

/*****************************************************************************/
/* Function Name      : L2IwfIncrFilterInDiscards                            */
/*                                                                           */
/* Description        : This routine is called to register the standard      */
/*                      bridge mib when bridge module is disabled            */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfIncrFilterInDiscards (UINT2 u2Port)
{
    /* TODO: Add Context ID or call BrgIncrFilter from VLAN itself */
#ifdef BRIDGE_WANTED
    BrgIncrFilterInDiscards (u2Port);
#else
    VlanIncrFilterInDiscards (u2Port);
#endif
}

/*****************************************************************************/
/* Function Name      : L2IwfStartHwAgeTime                                  */
/*                                                                           */
/* Description        : This routine is called to register the standard      */
/*                      bridge mib when bridge module is disabled            */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfStartHwAgeTimer (UINT4 u4ContextId, UINT4 u4AgeOutInt)
{
    VlanStartHwAgeTimer (u4ContextId, u4AgeOutInt);
}

/*****************************************************************************/
/* Function Name      : L2IwfRegisterSTDBRI                                  */
/*                                                                           */
/* Description        : This routine is called to register the standard      */
/*                      bridge mib when bridge module is disabled            */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
L2IwfRegisterSTDBRI (VOID)
{
#ifndef BRIDGE_WANTED
#ifdef VLAN_WANTED
    VlanRegisterSTDBRI ();
#endif
#endif
}

#endif

/*****************************************************************************/
/* Function Name      : L2IwfSendPortCreateInd                               */
/*                                                                           */
/* Description        : This routine propagates the port create indication   */
/*                      to all L2 modules                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSendPortCreateInd (UINT4 u4ContextId, UINT4 u4IfIndex,
                        UINT2 u2LocalPortId, tCfaIfInfo * pIfInfo)
{
    tIfTypeDenyProtocolList DenyProtocolList;
    INT4                i4RetVal = L2IWF_SUCCESS;

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u2LocalPortId);

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, pIfInfo,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        i4RetVal = L2IWF_FAILURE;
    }

    /* For Pseudowire and AC interfaces, the interface create indication should not 
     * be given to higher layers. IGS and MLDS are exceptional case in this.
     * When this indication is given to the higher modules other than 
     * IGS/MLDS, Port related structures will be created. Where in IGS/MLDS
     * pre-defined the fields in the predefined array will be updated.
     */
    if ((pIfInfo->u1IfType == CFA_PSEUDO_WIRE) ||
        ((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
         (pIfInfo->u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)) ||
        (pIfInfo->u1IfType == CFA_VXLAN_NVE))
    {
        if (pIfInfo->u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
        {
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
            if (DenyProtocolList[L2IWF_PROTOCOL_ID_SNOOP] == OSIX_FALSE)
            {
                SnoopCreatePort (u4ContextId, u4IfIndex, u2LocalPortId);
            }
#endif /* IGS_WANTED */
        }
        return L2IWF_SUCCESS;
    }
    /* Assumption : PNAC/ISS is applicable for Ethernet interfaces && WAP
     * interfaces only */
    if ((pIfInfo->u1IfType == CFA_ENET)
        || (pIfInfo->u1IfType == CFA_CAPWAP_DOT11_BSS))
    {
#ifdef PNAC_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PNAC] == OSIX_FALSE)
        {
            if (PnacCreatePort ((UINT2) u4IfIndex) == PNAC_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
#endif
    }
    if ((pIfInfo->u1IfType != CFA_BRIDGED_INTERFACE)
        && (pIfInfo->u1IfType != CFA_PIP)
        && (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
              (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE))))
    {
#ifdef LA_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_LA] == OSIX_FALSE)
        {
            if (LaCreatePort ((UINT2) u4IfIndex) == LA_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
#endif /* LA_WANTED */
    }

#ifdef BRIDGE_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_BRIDGE] == OSIX_FALSE)
    {
        if (BridgeCreatePort (u4IfIndex, pIfInfo->u1IfType,
                              pIfInfo->u4IfSpeed) == BRIDGE_FAILURE)
        {
            i4RetVal = L2IWF_FAILURE;
        }
    }
#endif /* BRIDGE_WANTED */

    /* If the PIP Port create Indication is NOT given to VLAN then
     * due to synchronisation issues between VLAN and PBB, Bridge Port 
     * type may be wrongly set in hardware. To avoid this PIP create 
     * message is posted to STP, VLAN and GARP but they ignore it.
     */

#ifdef RSTP_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_XSTP] == OSIX_FALSE)
    {
        if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
        {
            if (AstCreatePort (u4ContextId, u4IfIndex, u2LocalPortId) ==
                RST_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
    }
#endif /* RSTP_WANTED */
#ifdef ECFM_WANTED
    if (pIfInfo->u1BrgPortType != L2IWF_VIRTUAL_INSTANCE_PORT)
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_ECFM] == OSIX_FALSE)
        {
            if (EcfmCreatePort (u4ContextId, u4IfIndex, u2LocalPortId) ==
                ECFM_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
    }
#endif /*ECFM_WANTED */

#ifdef VLAN_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_VLAN] == OSIX_FALSE)
    {
        if (VlanCreatePort (u4ContextId, u4IfIndex, u2LocalPortId) ==
            VLAN_FAILURE)
        {
            i4RetVal = L2IWF_FAILURE;
        }
    }

#ifdef GARP_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_GARP] == OSIX_FALSE)
    {
        if (GarpCreatePort (u4ContextId, u4IfIndex, u2LocalPortId) ==
            GARP_FAILURE)
        {
            i4RetVal = L2IWF_FAILURE;
        }
    }
#endif /* GARP_WANTED */
#endif /* VLAN_WANTED */

#ifdef MRP_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_MRP] == OSIX_FALSE)
    {
        if (MrpApiCreatePort (u4ContextId, u4IfIndex, u2LocalPortId) ==
            OSIX_FAILURE)
        {
            i4RetVal = L2IWF_FAILURE;
        }
    }
#endif /* MRP_WANTED */

#ifdef PBB_WANTED
    if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PBB] == OSIX_FALSE)
        {
            PbbCreatePort (u4ContextId, (INT4) u4IfIndex,
                           (UINT4) u2LocalPortId);
        }
    }
#endif /*PBB_WANTED */

#ifdef ELMI_WANTED
    if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_ELMI] == OSIX_FALSE)
        {
            ElmCreatePort (u4IfIndex);
        }
    }
#endif /* ELMI_WANTED */

    if (pIfInfo->u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
    {
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_SNOOP] == OSIX_FALSE)
        {
            SnoopCreatePort (u4ContextId, u4IfIndex, u2LocalPortId);
        }
#endif /* IGS_WANTED */
    }

    if (pIfInfo->u1IfType == CFA_ENET)
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_LLDP] == OSIX_FALSE)
        {
            if (LldpApiNotifyIfCreate (u4IfIndex) == OSIX_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
        }
    }

#ifdef CN_WANTED
    if ((pIfInfo->u1IfType == CFA_LAGG) || (pIfInfo->u1IfType == CFA_ENET))
    {
        CnApiPortRequest (u4ContextId, u4IfIndex, CN_CREATE_IF_MSG);
    }
#endif /* CN_WANTED */

#ifdef DCBX_WANTED
    DcbxApiPortRequest (u4IfIndex, DCBX_CREATE_IF_MSG);
#endif /* DCBX_WANTED */
#ifdef RBRG_WANTED
    RbrgApiPortRequest (u4ContextId, u4IfIndex, RBRG_CREATE_PORT);
#endif
#ifdef CFA_WANTED
    if (pIfInfo->u1IfType == CFA_ENET)
    {
        IpdbCreatePort (u4IfIndex);
    }
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfSendPortDeleteInd                               */
/*                                                                           */
/* Description        : This routine propagates the port delete indication   */
/*                      to all L2 modules                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
VOID
L2IwfSendPortDeleteInd (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    tIfTypeDenyProtocolList DenyProtocolList;
    UNUSED_PARAM (u4IfIndex);

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, pIfInfo,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        return;
    }

#ifdef CN_WANTED
    if ((pIfInfo->u1IfType == CFA_LAGG) || (pIfInfo->u1IfType == CFA_ENET))
    {
        CnApiPortRequest (0 /* Context Id */ , u4IfIndex, CN_DELETE_IF_MSG);
    }
#endif /* CN_WANTED */

#ifdef DCBX_WANTED
    DcbxApiPortRequest (u4IfIndex, DCBX_DELETE_IF_MSG);
#endif /* DCBX_WANTED */
#ifdef FSB_WANTED
    if (pIfInfo->u1BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        FsbApiUnmapPort (u4IfIndex);
    }
#endif
#ifdef RBRG_WANTED
    RbrgApiPortRequest (0 /* Context Id */ , u4IfIndex, RBRG_DELETE_PORT);
#endif
    /* Assumption : PNAC/ISS is applicable for Ethernet interfaces only */
    if (pIfInfo->u1IfType == CFA_ENET
        || pIfInfo->u1IfType == CFA_CAPWAP_DOT11_BSS)
    {
#ifdef PNAC_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PNAC] == OSIX_FALSE)
        {
            PnacDeletePort ((UINT2) u4IfIndex);
        }
#endif
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_LLDP] == OSIX_FALSE)
        {
            LldpApiNotifyIfDelete (u4IfIndex);
        }
    }
#ifndef ISS_WANTED
#ifndef WSS_WANTED
#ifdef LA_WANTED
    LaDeletePort ((UINT2) u4IfIndex);
#endif /* LA_WANTED */
#endif
#endif

    if (pIfInfo->u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
    {
#ifdef RSTP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_XSTP] == OSIX_FALSE)
        {
            AstDeletePort ((UINT2) u4IfIndex);
        }
#endif /* RSTP_WANTED */
    }

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_SNOOP] == OSIX_FALSE)
    {
        SnoopDeletePort (u4IfIndex);
    }
#endif /* IGS_WANTED */

#ifdef ELMI_WANTED
    if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_ELMI] == OSIX_FALSE)
        {
            ElmDeletePort ((UINT2) u4IfIndex);
        }
    }
#endif

#ifdef ECFM_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_ECFM] == OSIX_FALSE)
    {
        EcfmDeletePort ((UINT2) u4IfIndex);
    }
#endif /*ECFM_WANTED */

#ifdef PBB_WANTED
    if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
          (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))

    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PBB] == OSIX_FALSE)
        {
            PbbDeletePort ((INT4) u4IfIndex);
        }
    }
#endif /*PBB_WANTED */

    if (pIfInfo->u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
    {
#ifdef MRP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_MRP] == OSIX_FALSE)
        {
            MrpApiDeletePort (u4IfIndex);
        }
#endif /*MRP_WANTED */
#ifdef VLAN_WANTED
#ifdef GARP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_GARP] == OSIX_FALSE)
        {
            GarpDeletePort ((UINT2) u4IfIndex);
        }
#endif /* GARP_WANTED */
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_VLAN] == OSIX_FALSE)
        {
            VlanDeletePort ((UINT2) u4IfIndex);
        }
#endif /* VLAN_WANTED */
    }

#ifdef BRIDGE_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_BRIDGE] == OSIX_FALSE)
    {
        BridgeDeletePort ((UINT2) u4IfIndex);
    }
#endif /* BRIDGE_WANTED */

#ifdef CFA_WANTED
    if (pIfInfo->u1IfType == CFA_ENET)
    {
        IpdbDeletePort (u4IfIndex);
    }
#endif

    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetProtocolEnabledStatusOnPort                  */
/*                                                                           */
/* Description        : This routine returns the given protocols status on   */
/*                      given Port. It accesses the L2Iwf common database.   */
/*                                                                           */
/* Input(s)           : u4IfIndex    - Index of the port whose port type     */
/*                                     is to be obtained.                    */
/*                      u2Protocol   - Protocol whose port status is needed. */
/*                                                                           */
/* Output(s)          : pu1Status    - Status of the given protocol on the   */
/*                                     given port.                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetProtocolEnabledStatusOnPort (UINT4 u4IfIndex, UINT2 u2Protocol,
                                     UINT1 *pu1Status)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    if (L2IWF_IS_INTERFACE_RANGE_VALID (u4IfIndex) == L2IWF_FALSE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    pL2PortInfo = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortInfo == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    switch (u2Protocol)
    {
        case L2_PROTO_STP:
            *pu1Status = L2IWF_PORT_STP_STATUS (pL2PortInfo);
            break;

        case L2_PROTO_GVRP:
            *pu1Status = L2IWF_PORT_GVRP_STATUS (pL2PortInfo);
            break;

        case L2_PROTO_GMRP:
            *pu1Status = L2IWF_PORT_GMRP_STATUS (pL2PortInfo);
            break;
        case L2_PROTO_MVRP:
            *pu1Status = L2IWF_PORT_MVRP_STATUS (pL2PortInfo);
            break;

        case L2_PROTO_MMRP:
            *pu1Status = L2IWF_PORT_MMRP_STATUS (pL2PortInfo);
            break;

        default:
            L2_UNLOCK ();
            return L2IWF_FAILURE;
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetProtocolEnabledStatusOnPort                  */
/*                                                                           */
/* Description        : This routine will be called by protocol routines to  */
/*                      update the corresponding protocol's enabled status   */
/*                      on a port.                                           */
/*                                                                           */
/* Input(s)           : u4ContextId   - Virtual Switch ID                    */
/*                      u2LocalPortId - Local port Index of the port whose   */
/*                                      protocols enabled status is to be    */
/*                                      updated.                             */
/*                      u2Protocol    - Protocol whose port status needs to  */
/*                                      updated.                             */
/*                      u1Status      - Protocol status (enabled/disable)    */
/*                                      status for the port.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSetProtocolEnabledStatusOnPort (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                     UINT2 u2Protocol, UINT1 u1Status)
{
    tL2PortInfo        *pL2PortInfo = NULL;

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT || u2LocalPortId <= 0)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPortId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    pL2PortInfo = L2IWF_PORT_INFO (u2LocalPortId);

    switch (u2Protocol)
    {
        case L2_PROTO_STP:
            L2IWF_PORT_STP_STATUS (pL2PortInfo) = u1Status;
            break;

        case L2_PROTO_GVRP:
            L2IWF_PORT_GVRP_STATUS (pL2PortInfo) = u1Status;
            break;

        case L2_PROTO_GMRP:
            L2IWF_PORT_GMRP_STATUS (pL2PortInfo) = u1Status;
            break;

        case L2_PROTO_MVRP:
            L2IWF_PORT_MVRP_STATUS (pL2PortInfo) = u1Status;
            break;

        case L2_PROTO_MMRP:
            L2IWF_PORT_MMRP_STATUS (pL2PortInfo) = u1Status;
            break;

        default:

            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_FAILURE;
    }

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfConvToLocalPortList                             */
/*                                                                           */
/* Description        : This routine converts the global IfIndex based port  */
/*                      lists to local port lists.                           */
/*                                                                           */
/* Input(s)           : IfPortList - IfIndex based port list                 */
/*                                                                           */
/* Output(s)          : LocalPortList - Local Port List                      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfConvToLocalPortList (tPortList IfPortList, tLocalPortList LocalPortList)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    UINT4               u4IfIndex = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;

    for (u2ByteIndex = 0; u2ByteIndex < BRG_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = IfPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < BRG_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {

            if ((u1PortFlag & L2IWF_BIT8) != 0)
            {
                u4IfIndex = (UINT4) ((u2ByteIndex * BRG_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);

                L2_LOCK ();

                pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

                if (pL2PortEntry == NULL)
                {
                    L2_UNLOCK ();
                    return;
                }

                L2_UNLOCK ();

                OSIX_BITLIST_SET_BIT (LocalPortList,
                                      pL2PortEntry->u2LocalPortId,
                                      CONTEXT_PORT_LIST_SIZE);
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
}

/*****************************************************************************/
/* Function Name      : L2IwfConvToGlblIfIndexList                           */
/*                                                                           */
/* Description        : This routine converts local port number based        */
/*                      portlists to global IfIndex based port lists.        */
/*                                                                           */
/* Input(s)           : LocalEgressPorts                                     */
/*                                                                           */
/* Output(s)          : EgressPorts                                          */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfConvToGlblIfIndexList (tLocalPortList LocalEgressPorts,
                            tPortList EgressPorts)
{
    UINT4               u4IfIndex;
    UINT2               u2LocalPort;
    UINT2               u2BytIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;

    for (u2BytIndex = 0; u2BytIndex < CONTEXT_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = LocalEgressPorts[u2BytIndex];
        for (u2BitIndex = 0; ((u2BitIndex < 8)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {

            if ((u1PortFlag & 0x80) != 0)
            {
                u2LocalPort = (UINT2) ((u2BytIndex * 8) + u2BitIndex + 1);
                if (u2LocalPort < L2IWF_MAX_PORTS_PER_CONTEXT_EXT)
                {
                    if (L2IWF_PORT_INFO (u2LocalPort) != NULL)
                    {
                        u4IfIndex = L2IWF_PORT_IFINDEX (u2LocalPort);
                        OSIX_BITLIST_SET_BIT (EgressPorts, u4IfIndex,
                                              sizeof (tPortList));
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
}

/*****************************************************************************/
/* Function Name      : L2IwfAddIfIndexEntry                                 */
/*                                                                           */
/* Description        : This routine adds the given port entry to both the   */
/*                      global interface database and the per context        */
/*                      interface database both of which are indexed by the  */
/*                      global ifindex                                       */
/*                                                                           */
/* Input(s)           : pL2PortEntry - Port entry to be added                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfAddIfIndexEntry (tL2PortInfo * pL2PortEntry)
{
    if (RBTreeAdd (L2IWF_GLOBAL_IFINDEX_TREE (), (tRBElem *) pL2PortEntry)
        == RB_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfRemoveIfIndexEntry                              */
/*                                                                           */
/* Description        : This routine removes the given port entry from both  */
/*                      the global interface database and the per context    */
/*                      interface database both of which are indexed by the  */
/*                      global ifindex                                       */
/*                                                                           */
/* Input(s)           : pL2PortEntry - Port entry to be removed              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfRemoveIfIndexEntry (tL2PortInfo * pL2PortEntry)
{
    if (RBTreeRemove (L2IWF_GLOBAL_IFINDEX_TREE (), (tRBElem *) pL2PortEntry)
        == RB_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetIfIndexEntry                                 */
/*                                                                           */
/* Description        : Returns a pointer to a port Entry for given IfIndex. */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : pointer to the PortEntry if key found.               */
/*                      NULL if not found                                    */
/*****************************************************************************/

tL2PortInfo        *
L2IwfGetIfIndexEntry (UINT4 u4IfIndex)
{

    tL2PortInfo        *pDummyEntry = NULL;

    tL2PortInfo        *pL2PortEntry = NULL;

    pDummyEntry =
        (tL2PortInfo *) L2IWF_ALLOCATE_MEMBLOCK (L2IWF_PORTINFO_POOLID ());

    if (NULL == pDummyEntry)
    {
        return NULL;
    }

    pDummyEntry->u4IfIndex = u4IfIndex;

    pL2PortEntry = (tL2PortInfo *) RBTreeGet (L2IWF_GLOBAL_IFINDEX_TREE (),
                                              (tRBElem *) pDummyEntry);

    L2IWF_RELEASE_MEMBLOCK (L2IWF_PORTINFO_POOLID (), pDummyEntry);

    return pL2PortEntry;

}

/*****************************************************************************/
/* Function Name      : L2IwfGetFirstIfIndexEntry                            */
/*                                                                           */
/* Description        : Returns a pointer to the port entry of the first     */
/*                      interface in the system known to L2IWF               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pointer to the PortEntry if key found.               */
/*                      NULL if not found                                    */
/*****************************************************************************/
tL2PortInfo        *
L2IwfGetFirstIfIndexEntry (VOID)
{
    return (tL2PortInfo *) RBTreeGetFirst (L2IWF_GLOBAL_IFINDEX_TREE ());
}

/*****************************************************************************/
/* Function Name      : L2IwfGetNextIfIndexEntry                             */
/*                                                                           */
/* Description        : Returns a pointer to the port entry of the next      */
/*                      interface in the system known to L2Iwf               */
/*                                                                           */
/* Input(s)           : pL2PortEntry - The next interface to this needs to   */
/*                                     returned.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pointer to the PortEntry if key found.               */
/*                      NULL if not found                                    */
/*****************************************************************************/
tL2PortInfo        *
L2IwfGetNextIfIndexEntry (tL2PortInfo * pL2PortEntry)
{
    return (tL2PortInfo *) RBTreeGetNext (L2IWF_GLOBAL_IFINDEX_TREE (),
                                          (tRBElem *) pL2PortEntry, NULL);
}

/*****************************************************************************/
/* Function Name      : L2IwfIfIndexTblCmpFn                                 */
/*                                                                           */
/* Description        : This routine will be invoked by the RB Tree library  */
/*                      to traverse through the global ifIndex based RB      */
/*                      tree.                                                */
/*                                                                           */
/* Input(s)           : pKey1 - Pointer to the first port entry.             */
/*                      pKey2 - Pointer to the second port entry.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1, if pKey1 is greater than pKey2                    */
/*                     -1, if pKey2 is greater than pKey1                    */
/*                      0, if pKey2 and pKey1 are equal.                     */
/*****************************************************************************/
INT4
L2IwfIfIndexTblCmpFn (tRBElem * pKey1, tRBElem * pKey2)
{
    tL2PortInfo        *pL2PortInfo1 = (tL2PortInfo *) pKey1;
    tL2PortInfo        *pL2PortInfo2 = (tL2PortInfo *) pKey2;

    if (pL2PortInfo1->u4IfIndex > pL2PortInfo2->u4IfIndex)
    {
        return 1;
    }
    else if (pL2PortInfo1->u4IfIndex < pL2PortInfo2->u4IfIndex)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : L2IwfFreePortEntry                                   */
/*                                                                           */
/* Description        : This routine is called when the Global IfIndex table */
/*                      is destroyed. It frees the memory occupied by the    */
/*                      given port entry pointer.                            */
/*                                                                           */
/* Input(s)           : u2PortIndex - Port Index                             */
/*                      u1IfType - Type of the port                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfFreePortEntry (tRBElem * pElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    L2IWF_RELEASE_MEMBLOCK (L2IWF_PORTINFO_POOLID (), (tL2PortInfo *) pElem);

    return 1;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPortMappedToContext                           */
/*                                                                           */
/* Description        : This routine is called to find whether the given port*/
/*                      is mapped to a context.                              */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfIsPortMappedToContext (UINT4 u4IfIndex)
{

    UINT4               u4ContextId;
    UINT2               u2LocalPort;

    if (VcmGetSystemModeExt (L2IWF_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPort) == VCM_SUCCESS)
        {
            return L2IWF_SUCCESS;
        }
    }
    return L2IWF_FAILURE;
}

/*****************************************************************************
 * Name               : L2iwfDupCruBuf 
 *
 * Description        : This is used to duplicate the CRU-Buffer
 *
 * Input(s)           : pSrcCruBuf - Pointer to source CRU-Buffer which needs to 
 *               be duplicated
 * Output(s)          : None
 *
 * Return Value(s)    : tCRU_BUF_CHAIN_HEADER*
 *
 *****************************************************************************/
tCRU_BUF_CHAIN_HEADER *
L2iwfDuplicateCruBuf (tCRU_BUF_CHAIN_HEADER * pSrcCruBuf)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    tL2FrameBuf        *pBuf = NULL;
    UINT4               u4BufSize = L2IWF_INIT_VAL;
    UINT1              *pu1Frame = NULL;
    UINT1               u1OpCode = 0;

    pBuf = (tL2FrameBuf *) MemAllocMemBlk (gu4L2FramePoolId);
    if (NULL == pBuf)
    {
        return NULL;
    }

    u4BufSize = CRU_BUF_Get_ChainValidByteCount (pSrcCruBuf);
    if (u4BufSize == L2IWF_INIT_VAL)
    {
        MemReleaseMemBlock (gu4L2FramePoolId, (UINT1 *) pBuf);
        return NULL;
    }
    pCruBuf = CRU_BUF_Allocate_MsgBufChain (u4BufSize, L2IWF_INIT_VAL);
    if (pCruBuf == NULL)
    {
        MemReleaseMemBlock (gu4L2FramePoolId, (UINT1 *) pBuf);
        return NULL;
    }
    pu1Frame = CRU_BUF_Get_DataPtr_IfLinear (pSrcCruBuf, 0, u4BufSize);
    if (pu1Frame == NULL)
    {
        pu1Frame = (UINT1 *) pBuf;
        MEMSET (pu1Frame, L2IWF_INIT_VAL, u4BufSize);
        CRU_BUF_Copy_FromBufChain (pSrcCruBuf, pu1Frame, 0, u4BufSize);
    }
    CRU_BUF_Copy_OverBufChain (pCruBuf, pu1Frame, 0, u4BufSize);
    u1OpCode = L2IWF_BUF_GET_OPCODE (pSrcCruBuf);
    L2IWF_BUF_SET_OPCODE (pCruBuf, u1OpCode);
    MemReleaseMemBlock (gu4L2FramePoolId, (UINT1 *) pBuf);
    return pCruBuf;
}

/******************************************************************************
 * Function Name      : l2iwfPktFree
 *
 * Description        : Free CRU Buffer memory
 *
 * Input(s)           : pBuf - pointer to CRU Buffer
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
l2iwfPktFree (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2iwfAppendIsidTagToFrame                        */
/*                                                                           */
/*    Description         : This function constructs ISID Tag for the        */
/*                          outgoing frame.                                  */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : PbbTag   - Isid tag filled with Isid,            */
/*                                     priority and drop-eligible.           */
/*                                                                           */
/*    Output(s)           : pu4Tag     - The tag to be put in the outgoing   */
/*                                       frame.                              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
VOID
L2iwfAppendIsidTagToFrame (tCRU_BUF_CHAIN_DESC * pFrame, tPbbTag * pPbbTag)
{
    UINT4               u4Tag = 0;
    UINT2               u2Protocol;
    UINT1               au1Buf[ISID_TAGGED_HEADER_SIZE];
    IsidFormTag (pPbbTag, &u4Tag);
    u2Protocol = OSIX_HTONS (VLAN_PROVIDER_BACKBONE_PROTOCOL_ID);

    MEMCPY (&au1Buf, pPbbTag->BDAMacAddr, MAC_ADDRESS_SIZE);
    MEMCPY (&au1Buf[MAC_ADDRESS_SIZE], pPbbTag->BSAMacAddr, MAC_ADDRESS_SIZE);

    MEMCPY (&au1Buf[VLAN_TAG_OFFSET], (UINT1 *) &u2Protocol,
            VLAN_TYPE_OR_LEN_SIZE);

    MEMCPY (&au1Buf[VLAN_TAG_VLANID_OFFSET], (UINT1 *) &u4Tag, ISID_TAG_SIZE);

    CRU_BUF_Prepend_BufChain (pFrame, au1Buf, ISID_TAGGED_HEADER_SIZE);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2iwfUpdateIsidTag                               */
/*                                                                           */
/*    Description         : This function updates the I tag for the          */
/*                          outgoing frame.                                  */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pPbbTag   - Isid tag filled with Isid,            */
/*                                     priority and drop-eligible.           */
/*                                                                           */
/*    Output(s)           : pFrame   - Frame pointer                         */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
VOID
L2iwfUpdateIsidTag (tCRU_BUF_CHAIN_DESC * pFrame, tPbbTag * pPbbTag)
{
    UINT4               u4Tag = 0;
    UINT2               u2Protocol;
    UINT1               au1Buf[ISID_TAGGED_HEADER_SIZE];
    IsidFormTag (pPbbTag, &u4Tag);
    u2Protocol = OSIX_HTONS (VLAN_PROVIDER_BACKBONE_PROTOCOL_ID);

    MEMCPY (&au1Buf, pPbbTag->BDAMacAddr, MAC_ADDRESS_SIZE);
    MEMCPY (&au1Buf[MAC_ADDRESS_SIZE], pPbbTag->BSAMacAddr, MAC_ADDRESS_SIZE);

    MEMCPY (&au1Buf[VLAN_TAG_OFFSET], (UINT1 *) &u2Protocol,
            VLAN_TYPE_OR_LEN_SIZE);

    MEMCPY (&au1Buf[VLAN_TAG_VLANID_OFFSET], (UINT1 *) &u4Tag, ISID_TAG_SIZE);
    CRU_BUF_Copy_OverBufChain (pFrame, au1Buf, 0, ISID_TAGGED_HEADER_SIZE);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IsidFormTag                                      */
/*                                                                           */
/*    Description         : This function constructs VLAN tag for the        */
/*                          outgoing frame. In case of 802.1ad bridges, this */
/*                          the VLAN tag is formed after PCP encoding.       */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : pPbbTag   - Isid tag filled with Isid,            */
/*                                     priority and drop-eligible.           */
/*                                                                           */
/*    Output(s)           : pu4Tag     - The tag to be put in the outgoing   */
/*                                       frame.                              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
VOID
IsidFormTag (tPbbTag * pPbbTag, UINT4 *pu4Tag)
{
    UINT1               u1Priority;

    u1Priority = (UINT1) ((pPbbTag->InnerIsidTag.u1Priority) << 1);

    if (pPbbTag->InnerIsidTag.u1DropEligible == VLAN_DE_TRUE)
    {
        u1Priority = (UINT1) (u1Priority | 0x0001);
    }
    *pu4Tag = (UINT4) u1Priority;
    *pu4Tag = (UINT4) (*pu4Tag << ISID_TAG_PRIORITY_SHIFT);
    *pu4Tag = (UINT4) (*pu4Tag | pPbbTag->InnerIsidTag.u4Isid);
    if (pPbbTag->InnerIsidTag.u1UcaBitValue == OSIX_TRUE)
    {
        *pu4Tag = *pu4Tag | ISID_UCA_BIT_MASK;
    }
    *pu4Tag = (UINT4) (OSIX_HTONL (*pu4Tag));
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2iwfFormBTag                                    */
/*                                                                           */
/*    Description         : This function constructs VLAN tag for the        */
/*                          outgoing frame. In case of 802.1ad bridges, this */
/*                          the VLAN tag is formed after PCP encoding.       */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : VlanTag  - VLAN tag filled with VLAN-ID,         */
/*                                     priority and drop-eligible.           */
/*                        : pVlanPortEntry - Pointer to port entry.          */
/*                                                                           */
/*    Output(s)           : pu2Tag     - The tag to be put in the outgoing   */
/*                                       frame.                              */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS / VLAN_FAILURE                      */
/*****************************************************************************/
VOID
L2iwfFormBTag (tPbbTag PbbTag, UINT2 *pu2Tag)
{
    UINT1               u1Priority;

    u1Priority = (UINT1) ((PbbTag.OuterVlanTag.u1Priority) << 1);

    if (PbbTag.OuterVlanTag.u1DropEligible == VLAN_DE_TRUE)
    {
        u1Priority = (UINT1) (u1Priority | 0x0001);
    }

    *pu2Tag = (UINT2) u1Priority;

    *pu2Tag = (UINT2) (*pu2Tag << VLAN_TAG_PRIORITY_DEI_SHIFT);

    *pu2Tag = (UINT2) (*pu2Tag | PbbTag.OuterVlanTag.u2VlanId);

    *pu2Tag = (UINT2) (OSIX_HTONS (*pu2Tag));

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2iwfCheckAndRemoveItagFromFrame                 */
/*                                                                           */
/*    Description         : This function removes all Vlan Tags from the     */
/*                          incoming packet.                                 */
/*                                                                           */
/*    Input(s)            : pFrame - Pointer to the frame.                   */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
L2iwfCheckAndRemoveItagFromFrame (tCRU_BUF_CHAIN_DESC * pFrame)
{
    CRU_BUF_Move_ValidOffset (pFrame, VLAN_TAG_OFFSET + PBB_ISID_TAG_PID_LEN);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfPbbGetCbpFwdPortListForIsid                 */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/                                     */
/*                         VLAN_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
L2IwfPbbGetCbpFwdPortListForIsid (UINT4 u4ContextId,
                                  UINT2 u2Port, UINT4 u4Isid,
                                  tLocalPortList IfFwdPortList)
{
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    UNUSED_PARAM (u2Port);
    SnmpPorts.pu1_OctetList = IfFwdPortList;
    SnmpPorts.i4_Length = sizeof (tLocalPortList);
    /* fllowing function returns the CBP  port list */
    return (PbbGetCbpListForIsid (u4ContextId, u4Isid, &SnmpPorts));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetIsid                                     */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          transmit CFM PDUs.                               */
/*                                                                           */
/*    Input(s)            : pBuf     - Pointer to the incoming packet        */
/*                          VlanTagInfo - Tag Info to be filled in PDU       */
/*                          u2Port   - Port on which the CFM PDU is to be    */
/*                                     transmitted.                          */
/*                          u1FrameType - VLAN_CFM_FRAME                     */
/*                                                                           */
/*    Output(s)           : pBuf - Modified Pbuf.                            */
/*                                                                           */
/*    Returns            : L2IWF_SUCCESS/L2IWF_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfGetIsid (UINT4 u4Vip, UINT4 *pu4Isid)
{
    if (PbbGetIsidForVip (u4Vip, pu4Isid) != PBB_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetVip                                      */
/*                                                                           */
/*    Description         : This function is called by STP module to         */
/*                          get vip for an isid                              */
/*                                                                           */
/*    Input(s)            : u4Context- context id                            */
/*                          u4Isid Info - isid                               */
/*                                                                           */
/*    Output(s)           : pu4Vip - vip value.                              */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/                                      */
/*                         PBB_FAILURE                                       */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfGetVip (UINT4 u4Context, UINT4 u4Isid, UINT2 *pu2Vip)
{
    return (PbbGetVipFromIsid (u4Context, u4Isid, pu2Vip));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfGetVipIfIndex                               */
/*                                                                           */
/*    Description         : This function is called by STP module to         */
/*                          get vip for an isid                              */
/*                                                                           */
/*    Input(s)            : u4Context- context id                            */
/*                          u4Isid Info - isid                               */
/*                                                                           */
/*    Output(s)           : pu4Vip - vip value.                              */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/                                      */
/*                         PBB_FAILURE                                       */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfGetVipIfIndex (UINT4 u4Context, UINT4 u4Isid, UINT4 *pu4Vip)
{
    UINT2               u2LocalPortId = 0;
    INT4                i4RetVal = L2IWF_FAILURE;
    UINT2               u2LocalPort = 0;
    if (PbbGetVipFromIsid (u4Context, u4Isid, &u2LocalPort) == L2IWF_SUCCESS)
    {
        u2LocalPortId = u2LocalPort;
        i4RetVal = VcmGetIfIndexFromLocalPort (u4Context, u2LocalPortId,
                                               pu4Vip);
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfSendIntfTypeChangeInd                           */
/*                                                                           */
/* Description        : This routine propagates the interface tyoe change    */
/*                      indication to STP and ECFM modules.                  */
/*                                                                           */
/* Input(s)           : u4ContextId : Context Id                             */
/*                      u1IntfType : Interface Type                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSendIntfTypeChangeInd (UINT4 u4ContextId, UINT1 u1IntfType)
{
    INT4                i4RetVal = L2IWF_SUCCESS;

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u1IntfType);

#ifdef VLAN_WANTED
    if (VlanUpdateIntfType (u4ContextId, u1IntfType) == VLAN_FAILURE)
    {
        i4RetVal = L2IWF_FAILURE;
    }
#endif

#ifdef PBB_WANTED
#ifdef RSTP_WANTED
    if (AstUpdateIntfType (u4ContextId, u1IntfType) == RST_FAILURE)
    {
        i4RetVal = L2IWF_FAILURE;
    }
#endif /* RSTP_WANTED */
#endif

#ifdef ECFM_WANTED
    if (EcfmUpdateIntfType (u4ContextId, u1IntfType) == ECFM_FAILURE)
    {
        i4RetVal = L2IWF_FAILURE;
    }
#endif /*ECFM_WANTED */
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsPnacCtrlFrame                                 */
/*                                                                           */
/* Description        : This routine is called from VLAN / l2Pkt handling    */
/*                      threads, to check whether the received pkt is Pnac   */
/*                      control packet                                       */
/*                                                                           */
/* Input(s)           : pFrame - Incoming Frame (CRU Buffer)                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE / L2IWF_FALSE                             */
/*****************************************************************************/

INT4
L2IwfIsPnacCtrlFrame (tCRU_BUF_CHAIN_DESC * pFrame)
{
    UINT2               u2EnetType;

    /* read the Ethernet frame type */
    if (CRU_BUF_Copy_FromBufChain (pFrame, (UINT1 *) &u2EnetType,
                                   PNAC_ENETTYPE_OFFSET, PNAC_ENET_TYPE_OR_LEN)
        == CRU_FAILURE)
    {
        return L2IWF_FALSE;
    }

    u2EnetType = (UINT2) OSIX_NTOHS (u2EnetType);

    /* handle if received frame is priority tagged */
    if (u2EnetType == VLAN_PROTOCOL_ID)
    {
        /* ignore the Tag-Priority, since all Frames are treated with 
         * equal priority */
        CRU_BUF_Copy_FromBufChain (pFrame, (UINT1 *) &u2EnetType,
                                   VLAN_TAGGED_HEADER_SIZE,
                                   PNAC_ENET_TYPE_OR_LEN);

        u2EnetType = (UINT2) OSIX_NTOHS (u2EnetType);
    }                            /*handling tagged frame, ends */

    if ((u2EnetType == PNAC_PAE_ENET_TYPE) ||
        (u2EnetType == PNAC_PAE_RSN_PREAUTH_ENET_TYPE))
    {
        return L2IWF_TRUE;
    }

    return L2IWF_FALSE;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsLaCtrlFrame                                   */
/*                                                                           */
/* Description        : This routine is called from VLAN / l2Pkt handling    */
/*                      threads, to check whether the received pkt is LA     */
/*                      control packet                                       */
/*                                                                           */
/* Input(s)           : pFrame - Incoming Frame (CRU Buffer)                 */
/*                      DestMacAddr - Destination Mac address                */
/*                      u4IfIndex   - Interface index                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE / L2IWF_FALSE                             */
/*****************************************************************************/

INT4
L2IwfIsLaCtrlFrame (tCRU_BUF_CHAIN_DESC * pFrame, tMacAddr DestMacAddr,
                    UINT4 u4IfIndex)
{
    UINT2               u2ProtType = 0;
    UINT1               u1SubType = 0;
    UINT1               au1LaSlowMcastAddr[] =
        { 0x01, 0x80, 0xC2, 0x00, 0x00, 0x02 };
    UINT4               u4VlanOffset = CFA_VLAN_TAG_OFFSET;

    if (CRU_BUF_Copy_FromBufChain (pFrame, DestMacAddr, LA_DESTADDR_OFFSET,
                                   ETHERNET_ADDR_SIZE) == CRU_FAILURE)
    {
        return L2IWF_FALSE;
    }

    if (VlanGetTagLenInFrame (pFrame, u4IfIndex, &u4VlanOffset) == VLAN_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    if ((MEMCMP (DestMacAddr, au1LaSlowMcastAddr, ETHERNET_ADDR_SIZE)) == 0)
    {
        if ((CRU_BUF_Copy_FromBufChain (pFrame, (UINT1 *) &u2ProtType, u4VlanOffset, 2) == CRU_FAILURE) || (CRU_BUF_Copy_FromBufChain (pFrame, (UINT1 *) &u1SubType, u4VlanOffset + 2,    /*SubType */
                                                                                                                                       1)
                                                                                                            ==
                                                                                                            CRU_FAILURE))
        {
            return L2IWF_FALSE;
        }

        u2ProtType = OSIX_NTOHS (u2ProtType);

        /* Check the Type and sub type */
        if ((u2ProtType == LA_SLOW_PROT_TYPE) &&
            ((u1SubType == LA_LACP_SUBTYPE) ||
             (u1SubType == LA_MARKER_SUBTYPE)))
        {
            return L2IWF_TRUE;
        }
    }
    return L2IWF_FALSE;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetSispPortInfo                                 */
/*                                                                           */
/* Description        : This routine used to get the secondary context       */
/*                      (based on the logical port) in which the particular  */
/*                      packet has to be processed. This will be determined  */
/*                      based on the VlanId received in the ingress packet.  */
/*                                                                           */
/* Input(s)           : pBuf       : Pointer to the Incoming Buffer          */
/*                      u4IfIndex  : Incoming port.                          */
/*                                                                           */
/* Output(s)          : pu4SharedPort : Shared logical port IfIndex          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetSispPortInfo (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                      UINT4 *pu4SharedPort)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tVlanId             VlanId;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId = 0;

    *pu4SharedPort = u4IfIndex;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (pL2PortEntry->u1IsSispEnabled == L2IWF_DISABLED)
    {
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    L2_UNLOCK ();

    if (VlanGetVlanTagInfo (pBuf, u4IfIndex, &VlanId) == VLAN_SUCCESS)
    {
        VcmSispGetContextInfoForPortVlan (u4IfIndex, VlanId, &u4ContextId,
                                          pu4SharedPort, &u2LocalPortId);
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfHigherLayerModulesIndication                    */
/*                                                                           */
/* Description        : This utilitity function used to give oper staus      */
/*                      indication to higher layer modules.                  */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfHigherLayerModulesIndication (UINT4 u4IfIndex, UINT1 u1Status,
                                   tL2IwfContextInfo * pL2IwfContextInfo)
{
    tIfTypeDenyProtocolList DenyProtocolList;
    tL2PortInfo        *pL2PortEntry;
    UINT2               u2PortType;
    UINT1               u1PortOwner = STP_MODULE;

    /* This is added to avoid compilation warning for all 
     * stack combinations 
     */
    UNUSED_PARAM (u1Status);

    MEMSET (DenyProtocolList, OSIX_FALSE, sizeof (tIfTypeDenyProtocolList));

    if (L2IwfGetIfTypeDenyProtoForPort (u4IfIndex, NULL,
                                        DenyProtocolList) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (pL2IwfContextInfo->u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    if (pL2IwfContextInfo->u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT_EXT ||
        pL2IwfContextInfo->u2LocalPortId <= 0)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    pL2PortEntry = L2IWF_PORT_INFO (pL2IwfContextInfo->u2LocalPortId);

    if (pL2PortEntry == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    u2PortType = L2IWF_PB_PORT_TYPE (pL2PortEntry);
    u1PortOwner = pL2PortEntry->u1PortOwner;

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    if (u2PortType != CFA_PROVIDER_INSTANCE_PORT)
    {
#ifdef RSTP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_XSTP] == OSIX_FALSE)
        {
            if (u1PortOwner == STP_MODULE)
            {

                AstWrUpdatePortStatus (u4IfIndex, u1Status,
                                       pL2IwfContextInfo->u4ContextId);
            }
        }
#else
        UNUSED_PARAM (u1PortOwner);
#endif

#ifdef ELMI_WANTED
        /* Port oper indication for SISP ports should not be given
         * to ELMI module */
        if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_FALSE)
        {
            if (DenyProtocolList[L2IWF_PROTOCOL_ID_ELMI] == OSIX_FALSE)
            {
                ElmUpdatePortStatus (u4IfIndex, u1Status);
            }
        }
#endif
#ifdef FSB_WANTED
        FsbApiNotifyPortDown (u4IfIndex, u1Status);
#endif
#ifdef VLAN_WANTED
#ifdef GARP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_GARP] == OSIX_FALSE)
        {
            GarpWrPortOperInd ((UINT2) u4IfIndex, u1Status, pL2IwfContextInfo);
        }
#endif
#ifdef MRP_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_MRP] == OSIX_FALSE)
        {
            MrpApiPortOperInd (u4IfIndex, u1Status);
        }
#endif
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_VLAN] == OSIX_FALSE)
        {
            VlanPortOperInd (u4IfIndex, u1Status);
        }
#endif
#if defined IGS_WANTED || MLDS_WANTED
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_SNOOP] == OSIX_FALSE)
        {
            SnoopWrPortOperIndication (u4IfIndex, u1Status, pL2IwfContextInfo);
        }
#endif
    }

#ifdef PBB_WANTED
    /* Port oper indication for SISP ports should not be given
     * to PBB */
    if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_FALSE)
    {
        if (DenyProtocolList[L2IWF_PROTOCOL_ID_PBB] == OSIX_FALSE)
        {
            PbbUpdatePortStatus ((INT4) u4IfIndex, u1Status);
        }
    }
#endif

#ifdef ECFM_WANTED
    if (DenyProtocolList[L2IWF_PROTOCOL_ID_ECFM] == OSIX_FALSE)
    {
        EcfmUpdatePortStatus (u4IfIndex, u1Status);
    }
#endif

#ifdef ESAT_WANTED
    EsatApiHandleOperDownIndication (u4IfIndex);
#endif

    return L2IWF_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : L2iwfPbbGetPipVipWithIsid                        */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
L2iwfPbbGetPipVipWithIsid (UINT4 u4ContextId, UINT2 u2LocalPort,
                           UINT4 u4Isid, UINT2 *pu2Vip, UINT2 *pu2Pip)
{
    if (PbbGetPipVipWithIsid (u4ContextId, u2LocalPort, u4Isid, pu2Vip, pu2Pip)
        != PBB_SUCCESS)
    {
        return L2IWF_FAILURE;
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfHlUpdateSispStatusOnPort                        */
/*                                                                           */
/* Description        : This utilitity function used to update  SISP status  */
/*                      indication to higher layer modules.                  */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u4ContextId - Context Identifier                     */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfHlUpdateSispStatusOnPort (UINT4 u4IfIndex, UINT4 u4ContextId,
                               UINT1 u1Status)
{
    /* Update the new status in MSTP alone. All other modules will refer
     * L2IWF for getting the SISP control status. Since, MSTP has to do some
     * special processing for BPDU Rx and Tx. the status should be updated to
     * MSTP
     * */
    MstUpdateSispStatusOnPort (u4IfIndex, u4ContextId, (tAstBoolean) u1Status);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIfTypeProtoDenyGetNode                          */
/*                                                                           */
/* Description        : This utilitity function used to get the node from    */
/*                      IfTypeProtoDeny Table.                               */
/*                                                                           */
/* Input(s)           : i4ContextId   - Context ID.                          */
/*                      i4IfType      - Interface Type.                      */
/*                      i4BrgPortType - Bridge Port Type.                    */
/*                      i4Protocol    - Protocol                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pointer to tIfTypeProtoDenyInfo entry if exist or    */
/*                      NULL, otherwise                                      */
/*****************************************************************************/
tIfTypeProtoDenyInfo *
L2IwfIfTypeProtoDenyGetNode (INT4 i4ContextId, INT4 i4IfType,
                             INT4 i4BrgPortType, INT4 i4Protocol)
{
    tIfTypeProtoDenyInfo IfTypeProtoDenyInfo;
    tIfTypeProtoDenyInfo *pIfTypeProtoDenyInfo = NULL;

    if ((i4ContextId < 0) || (i4ContextId >= L2IWF_MAX_CONTEXTS))
    {
        return NULL;
    }

    L2_LOCK ();

    if (L2IwfSelectContext ((UINT4) i4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return NULL;
    }

    IfTypeProtoDenyInfo.u1IfType = (UINT1) i4IfType;
    IfTypeProtoDenyInfo.u1BrgPortType = (UINT1) i4BrgPortType;
    IfTypeProtoDenyInfo.u1Protocol = (UINT1) i4Protocol;

    pIfTypeProtoDenyInfo =
        RBTreeGet (L2IWF_IFTYPE_PROTO_DENY_TREE (),
                   (tRBElem *) (&IfTypeProtoDenyInfo));

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return pIfTypeProtoDenyInfo;
}

/*****************************************************************************/
/* Function Name      : L2IwfIfTypeProtoDenyGetNextNode                      */
/*                                                                           */
/* Description        : This utilitity function used to get the next node    */
/*                      from IfTypeProtoDeny Table for the given entry.      */
/*                                                                           */
/* Input(s)           : i4ContextId   - Context ID.                          */
/*                      i4IfType      - Interface Type.                      */
/*                      i4BrgPortType - Bridge Port Type.                    */
/*                      i4Protocol    - Protocol                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pointer to tIfTypeProtoDenyInfo entry if exist or    */
/*                      NULL, otherwise                                      */
/*****************************************************************************/
tIfTypeProtoDenyInfo *
L2IwfIfTypeProtoDenyGetNextNode (INT4 i4ContextId, INT4 i4IfType,
                                 INT4 i4BrgPortType, INT4 i4Protocol)
{
    tIfTypeProtoDenyInfo IfTypeProtoDenyInfo;
    tIfTypeProtoDenyInfo *pIfTypeProtoDenyInfo = NULL;

    if ((i4ContextId < 0) || (i4ContextId >= L2IWF_MAX_CONTEXTS))
    {
        return NULL;
    }

    L2_LOCK ();

    if (L2IwfSelectContext ((UINT4) i4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return NULL;
    }

    IfTypeProtoDenyInfo.u1IfType = (UINT1) i4IfType;
    IfTypeProtoDenyInfo.u1BrgPortType = (UINT1) i4BrgPortType;
    IfTypeProtoDenyInfo.u1Protocol = (UINT1) i4Protocol;

    pIfTypeProtoDenyInfo =
        RBTreeGetNext (L2IWF_IFTYPE_PROTO_DENY_TREE (),
                       (tRBElem *) (&IfTypeProtoDenyInfo), NULL);

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return pIfTypeProtoDenyInfo;
}

/*****************************************************************************/
/* Function Name      : L2IwfIfTypeProtoDenyCreateNode                       */
/*                                                                           */
/* Description        : This utilitity function used to create node for      */
/*                      IfTypeProtoDeny Table.                               */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : pointer to tIfTypeProtoDenyInfo entry if created or  */
/*                      NULL, otherwise                                      */
/*****************************************************************************/
tIfTypeProtoDenyInfo *
L2IwfIfTypeProtoDenyCreateNode ()
{
    tIfTypeProtoDenyInfo *pIfTypeProtoDenyInfo = NULL;

    L2_LOCK ();

    pIfTypeProtoDenyInfo = (tIfTypeProtoDenyInfo *)
        L2IWF_ALLOCATE_MEMBLOCK (L2IWF_IFTYPE_PROTO_DENY_POOLID ());

    L2_UNLOCK ();

    if (pIfTypeProtoDenyInfo == NULL)
    {
        return NULL;
    }

    MEMSET (pIfTypeProtoDenyInfo, 0, sizeof (tIfTypeProtoDenyInfo));

    return pIfTypeProtoDenyInfo;
}

/*****************************************************************************/
/* Function Name      : L2IwfIfTypeProtoDenyAddNode                          */
/*                                                                           */
/* Description        : This utilitity function used to add node to          */
/*                      IfTypeProtoDeny Table.                               */
/*                                                                           */
/* Input(s)           : IfTypeProtoDenyInfoNode - New Node to add in table.  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE .                      */
/*****************************************************************************/
INT4
L2IwfIfTypeProtoDenyAddNode (INT4 i4ContextId,
                             tIfTypeProtoDenyInfo * pIfTypeProtoDenyInfoNode)
{
    if ((i4ContextId < 0) || (i4ContextId >= L2IWF_MAX_CONTEXTS))
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext ((UINT4) i4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (RBTreeAdd (L2IWF_IFTYPE_PROTO_DENY_TREE (),
                   pIfTypeProtoDenyInfoNode) == RB_FAILURE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIfTypeProtoDenyFreeNode                         */
/*                                                                           */
/* Description        : This utility function used to free a allocate node   */
/*                                                                           */
/* Input(s)           : IfTypeProtoDenyInfoNode - Node to delete from table. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE .                      */
/*****************************************************************************/
INT4
L2IwfIfTypeProtoDenyFreeNode (tIfTypeProtoDenyInfo * pIfTypeProtoDenyInfoNode)
{
    L2IWF_RELEASE_MEMBLOCK (L2IWF_IFTYPE_PROTO_DENY_POOLID (),
                            pIfTypeProtoDenyInfoNode);

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIfTypeProtoDenyEntryCmp                         */
/*                                                                           */
/* Description        : RBTree Comparison function registered with RBTree    */
/*                      utilities to search, add and delete nodes into it    */
/*                                                                           */
/* Input(s)           : pKey1 - First Comparison Entry                       */
/*                      pKey2 - Second Comparison Entry                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : 1/-1/0                                               */
/*****************************************************************************/
INT4
L2IwfIfTypeProtoDenyEntryCmp (tRBElem * pKey1, tRBElem * pKey2)
{
    tIfTypeProtoDenyInfo *pIfTypeProtoDenyInfoNode1 =
        (tIfTypeProtoDenyInfo *) pKey1;
    tIfTypeProtoDenyInfo *pIfTypeProtoDenyInfoNode2 =
        (tIfTypeProtoDenyInfo *) pKey2;

    if (pIfTypeProtoDenyInfoNode1->u1IfType >
        pIfTypeProtoDenyInfoNode2->u1IfType)
    {
        return 1;
    }
    else if (pIfTypeProtoDenyInfoNode1->u1IfType <
             pIfTypeProtoDenyInfoNode2->u1IfType)
    {
        return -1;
    }

    if (pIfTypeProtoDenyInfoNode1->u1BrgPortType >
        pIfTypeProtoDenyInfoNode2->u1BrgPortType)
    {
        return 1;
    }
    else if (pIfTypeProtoDenyInfoNode1->u1BrgPortType <
             pIfTypeProtoDenyInfoNode2->u1BrgPortType)
    {
        return -1;
    }

    if (pIfTypeProtoDenyInfoNode1->u1Protocol >
        pIfTypeProtoDenyInfoNode2->u1Protocol)
    {
        return 1;
    }
    else if (pIfTypeProtoDenyInfoNode1->u1Protocol <
             pIfTypeProtoDenyInfoNode2->u1Protocol)
    {
        return -1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : L2IwfIfTypeProtoDenyDelNode                          */
/*                                                                           */
/* Description        : This utility function used to remove the entry from  */
/*                      RBTree Table                                         */
/*                                                                           */
/* Input(s)           : i4ContextId - Context Identifier                     */
/*                      IfTypeProtoDenyInfoNode - Node to delete from table. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE .                      */
/*****************************************************************************/
INT4
L2IwfIfTypeProtoDenyDelNode (INT4 i4ContextId,
                             tIfTypeProtoDenyInfo * pIfTypeProtoDenyInfoNode)
{
    if ((i4ContextId < 0) || (i4ContextId >= L2IWF_MAX_CONTEXTS))
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext ((UINT4) i4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (RBTreeRemove (L2IWF_IFTYPE_PROTO_DENY_TREE (),
                      (tRBElem *) pIfTypeProtoDenyInfoNode) == RB_FAILURE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetIfTypeDenyProtoForPort                     */
/*                                                                           */
/* Description        : This utility function used to get the protocols for  */
/*                      which the If type and bridge port type based entry is*/
/*                      created to deny the port create and port delete      */
/*                      indications.                                         */
/*                                                                           */
/* Input(s)           : i4ContextId - Context Identifier                     */
/*                      pCfaIfInfo - Cfa Information                         */
/*                                                                           */
/* Output(s)          : pu1DenyProtocols - Array of Denied protocols for port*/
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE .                      */
/*****************************************************************************/
INT4
L2IwfGetIfTypeDenyProtoForPort (UINT4 u4IfIndex, tCfaIfInfo * pCfaIfInfo,
                                UINT1 *pu1DenyProtocols)
{
    tIfTypeProtoDenyInfo IfTypeProtoDenyInfo;
    tCfaIfInfo          CfaIfInfo;
    tIfTypeProtoDenyInfo *pIfTypeProtoDenyInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        != VCM_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    if (pCfaIfInfo == NULL)
    {
        /* If CfaInfo is not passed in the input parameter, this Call 
         * is used 
         */
        if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
        {
            return L2IWF_FAILURE;
        }
        pCfaIfInfo = &CfaIfInfo;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

/*     if (L2IWF_CFA_DENY_PROTOCOL_LISTCOUNT () > L2IWF_INIT_VAL) */
    {
        /* For the IfType and Bridge Port type of the port, all the valid
         * denied protocol entries are got here.
         */
        IfTypeProtoDenyInfo.u1IfType = (UINT1) pCfaIfInfo->u1IfType;
        IfTypeProtoDenyInfo.u1BrgPortType = (UINT1) pCfaIfInfo->u1BrgPortType;
        IfTypeProtoDenyInfo.u1Protocol = 0;

        pIfTypeProtoDenyInfo =
            RBTreeGetNext (L2IWF_IFTYPE_PROTO_DENY_TREE (),
                           (tRBElem *) (&IfTypeProtoDenyInfo), NULL);

        if (pIfTypeProtoDenyInfo == NULL)
        {
            MEMSET (pu1DenyProtocols, OSIX_FALSE, L2IWF_PROTOCOL_ID_MAX);
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_SUCCESS;
        }

        do
        {
            if ((pIfTypeProtoDenyInfo->u1IfType ==
                 pCfaIfInfo->u1IfType) &&
                (pIfTypeProtoDenyInfo->u1BrgPortType ==
                 pCfaIfInfo->u1BrgPortType))
            {
                if (pIfTypeProtoDenyInfo->u1Protocol < L2IWF_PROTOCOL_ID_MAX)
                {
                    pu1DenyProtocols[pIfTypeProtoDenyInfo->u1Protocol] =
                        OSIX_TRUE;

                    IfTypeProtoDenyInfo.u1IfType =
                        pIfTypeProtoDenyInfo->u1IfType;
                    IfTypeProtoDenyInfo.u1BrgPortType =
                        pCfaIfInfo->u1BrgPortType;
                    IfTypeProtoDenyInfo.u1Protocol =
                        pIfTypeProtoDenyInfo->u1Protocol;
                }
            }
            else
            {
                /* The output array pu1DenyProtocols is memset to OSIX_FALSE in 
                 * the calling routine 
                 */
                break;
            }
        }
        while ((pIfTypeProtoDenyInfo = RBTreeGetNext
                (L2IWF_IFTYPE_PROTO_DENY_TREE (),
                 (tRBElem *) (&IfTypeProtoDenyInfo), NULL)) != NULL);

    }
    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIfDenyProtocolEntry                          */
/*                                                                           */
/* Description        : This interaction function between CFA and L2IWF      */
/*                      where CFA calls this function from Low Level routine */
/*                      of If Type Cfa Deny Protocols Table of CFA.          */
/*                                                                           */
/* Input(s)           : pL2IwfCfaIfTypeDenyProt - Interaction Structure      */
/*                      CFA and L2IWF                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE .                      */
/*****************************************************************************/
INT4
L2IwfIfDenyProtocolEntry (tL2IwfCfaIfTypeDenyProt * pL2IwfCfaIfTypeDenyProt)
{
    tIfTypeProtoDenyInfo *pIfTypeProtoDenyNode = NULL;
    INT4                i4RetVal = L2IWF_FAILURE;
    switch (pL2IwfCfaIfTypeDenyProt->u4Action)
    {
        case L2IWF_CFA_GET_RS:

            pIfTypeProtoDenyNode = L2IwfIfTypeProtoDenyGetNode
                ((INT4) pL2IwfCfaIfTypeDenyProt->u4ContextId,
                 pL2IwfCfaIfTypeDenyProt->i4IfType,
                 pL2IwfCfaIfTypeDenyProt->i4BrgPortType,
                 pL2IwfCfaIfTypeDenyProt->i4Protocol);

            if (pIfTypeProtoDenyNode == NULL)
            {
                i4RetVal = L2IWF_FAILURE;
            }
            else
            {
                *pL2IwfCfaIfTypeDenyProt->pi4RowStatus = ACTIVE;
                i4RetVal = L2IWF_SUCCESS;
            }

            break;

        case L2IWF_CFA_GET_NEXT:

            pIfTypeProtoDenyNode = L2IwfIfTypeProtoDenyGetNextNode
                ((INT4) pL2IwfCfaIfTypeDenyProt->u4ContextId,
                 pL2IwfCfaIfTypeDenyProt->i4IfType,
                 pL2IwfCfaIfTypeDenyProt->i4BrgPortType,
                 pL2IwfCfaIfTypeDenyProt->i4Protocol);

            if (pIfTypeProtoDenyNode == NULL)
            {
                i4RetVal = L2IWF_FAILURE;
            }
            else
            {
                *pL2IwfCfaIfTypeDenyProt->pu4NextContextId =
                    pL2IwfCfaIfTypeDenyProt->u4ContextId;
                *pL2IwfCfaIfTypeDenyProt->pi4NextIfType =
                    pIfTypeProtoDenyNode->u1IfType;
                *pL2IwfCfaIfTypeDenyProt->pi4NextBrgPortType =
                    pIfTypeProtoDenyNode->u1BrgPortType;
                *pL2IwfCfaIfTypeDenyProt->pi4NextProtocol =
                    pIfTypeProtoDenyNode->u1Protocol;

                i4RetVal = L2IWF_SUCCESS;
            }

            break;

        case L2IWF_CFA_SET_RS_CRT:

            pIfTypeProtoDenyNode = L2IwfIfTypeProtoDenyCreateNode ();

            if (pIfTypeProtoDenyNode == NULL)
            {
                i4RetVal = L2IWF_FAILURE;
            }
            else
            {
                pIfTypeProtoDenyNode->u1IfType =
                    (UINT1) pL2IwfCfaIfTypeDenyProt->i4IfType;
                pIfTypeProtoDenyNode->u1BrgPortType =
                    (UINT1) pL2IwfCfaIfTypeDenyProt->i4BrgPortType;
                pIfTypeProtoDenyNode->u1Protocol =
                    (UINT1) pL2IwfCfaIfTypeDenyProt->i4Protocol;

                if (L2IwfIfTypeProtoDenyAddNode
                    ((INT4) pL2IwfCfaIfTypeDenyProt->u4ContextId,
                     pIfTypeProtoDenyNode) == L2IWF_FAILURE)

                {
                    L2_LOCK ();
                    L2IwfIfTypeProtoDenyFreeNode (pIfTypeProtoDenyNode);
                    L2_UNLOCK ();
                    i4RetVal = L2IWF_FAILURE;
                }
                else
                {
                    i4RetVal = L2IWF_SUCCESS;
                }
            }

            break;

        case L2IWF_CFA_SET_RS_DES:

            pIfTypeProtoDenyNode = L2IwfIfTypeProtoDenyGetNode
                ((INT4) pL2IwfCfaIfTypeDenyProt->u4ContextId,
                 pL2IwfCfaIfTypeDenyProt->i4IfType,
                 pL2IwfCfaIfTypeDenyProt->i4BrgPortType,
                 pL2IwfCfaIfTypeDenyProt->i4Protocol);

            if (pIfTypeProtoDenyNode != NULL)
            {
                L2IwfIfTypeProtoDenyDelNode ((INT4) pL2IwfCfaIfTypeDenyProt->
                                             u4ContextId, pIfTypeProtoDenyNode);
                L2_LOCK ();
                L2IwfIfTypeProtoDenyFreeNode (pIfTypeProtoDenyNode);
                L2_UNLOCK ();
            }

            i4RetVal = L2IWF_SUCCESS;
            break;

        case L2IWF_CFA_CRT_PORT:

            /* Send Port Create Indication for the Protocol */
            if (L2IwfSendPortCrtIndForProtocol
                (pL2IwfCfaIfTypeDenyProt->u4IfIndex,
                 pL2IwfCfaIfTypeDenyProt->pIfInfo,
                 pL2IwfCfaIfTypeDenyProt->i4Protocol) == L2IWF_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
            else
            {
                /* Send Port Oper Status Indication for the Protocol */
                L2IwfPortOperStatIndForProtocol
                    (pL2IwfCfaIfTypeDenyProt->u4IfIndex,
                     pL2IwfCfaIfTypeDenyProt->pIfInfo,
                     pL2IwfCfaIfTypeDenyProt->i4Protocol);

                i4RetVal = L2IWF_SUCCESS;
            }

            break;

        case L2IWF_CFA_DEL_PORT:

            /* Send Port Delete Indication for the Protocol */
            L2IwfSendPortDelIndForProtocol
                (pL2IwfCfaIfTypeDenyProt->u4IfIndex,
                 pL2IwfCfaIfTypeDenyProt->pIfInfo,
                 pL2IwfCfaIfTypeDenyProt->i4Protocol);

            i4RetVal = L2IWF_SUCCESS;

            break;

        default:

            i4RetVal = L2IWF_FAILURE;
            break;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfSendPortCrtIndForProtocol                       */
/*                                                                           */
/* Description        : This routine propagates the port create indication   */
/*                      to the given protocol for a port                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Id                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfSendPortCrtIndForProtocol (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo,
                                INT4 i4ProtocolId)
{
    INT4                i4RetVal = L2IWF_SUCCESS;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPortId)
        != VCM_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    switch (i4ProtocolId)
    {
        case L2IWF_PROTOCOL_ID_PNAC:

            /* Assumption : PNAC/ISS is applicable for Ethernet interfaces 
             * only */

            if (pIfInfo->u1IfType == CFA_ENET)
            {
#ifdef PNAC_WANTED
                if (PnacCreatePort ((UINT2) u4IfIndex) == PNAC_FAILURE)
                {
                    i4RetVal = L2IWF_FAILURE;
                }
#endif
            }

            break;

        case L2IWF_PROTOCOL_ID_LA:

            if ((pIfInfo->u1IfType != CFA_BRIDGED_INTERFACE)
                && (pIfInfo->u1IfType != CFA_PIP)
                && (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                      (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE))))
            {
#ifdef LA_WANTED
                if (LaCreatePort ((UINT2) u4IfIndex) == LA_FAILURE)
                {
                    i4RetVal = L2IWF_FAILURE;
                }
#endif /* LA_WANTED */
            }

            break;

        case L2IWF_PROTOCOL_ID_BRIDGE:

#ifdef BRIDGE_WANTED
            if (BridgeCreatePort (u4IfIndex, pIfInfo->u1IfType,
                                  pIfInfo->u4IfSpeed) == BRIDGE_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
#endif /* BRIDGE_WANTED */

            break;

            /* If the PIP Port create Indication is NOT given to VLAN then
             * due to synchronisation issues between VLAN and PBB, Bridge Port 
             * type may be wrongly set in hardware. To avoid this PIP create 
             * message is posted to STP, VLAN and GARP but they ignore it.
             */

        case L2IWF_PROTOCOL_ID_XSTP:

#ifdef RSTP_WANTED
            if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
            {
                if (AstCreatePort (u4ContextId, u4IfIndex, u2LocalPortId) ==
                    RST_FAILURE)
                {
                    i4RetVal = L2IWF_FAILURE;
                }
            }
#endif /* RSTP_WANTED */
            break;

        case L2IWF_PROTOCOL_ID_VLAN:

#ifdef VLAN_WANTED
            if (VlanCreatePort (u4ContextId, u4IfIndex, u2LocalPortId) ==
                VLAN_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }

            break;

        case L2IWF_PROTOCOL_ID_GARP:

#ifdef GARP_WANTED
            if (GarpCreatePort (u4ContextId, u4IfIndex, u2LocalPortId) ==
                GARP_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
#endif /* GARP_WANTED */
            break;

#endif /* VLAN_WANTED */

        case L2IWF_PROTOCOL_ID_MRP:

#ifdef MRP_WANTED
            if (MrpApiCreatePort (u4ContextId, u4IfIndex, u2LocalPortId) ==
                OSIX_FAILURE)
            {
                i4RetVal = L2IWF_FAILURE;
            }
#endif /* MRP_WANTED */

            break;

        case L2IWF_PROTOCOL_ID_PBB:
#ifdef PBB_WANTED
            if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                PbbCreatePort (u4ContextId, (INT4) u4IfIndex,
                               (UINT4) u2LocalPortId);
            }
#endif /*PBB_WANTED */
            break;

        case L2IWF_PROTOCOL_ID_ECFM:
#ifdef ECFM_WANTED
            if (pIfInfo->u1BrgPortType != L2IWF_VIRTUAL_INSTANCE_PORT)
            {
                if (EcfmCreatePort (u4ContextId, u4IfIndex, u2LocalPortId) ==
                    ECFM_FAILURE)
                {
                    i4RetVal = L2IWF_FAILURE;
                }
            }
#endif /*ECFM_WANTED */
            break;

        case L2IWF_PROTOCOL_ID_ELMI:
#ifdef ELMI_WANTED
            if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                ElmCreatePort (u4IfIndex);
            }
#endif /* ELMI_WANTED */
            break;

        case L2IWF_PROTOCOL_ID_SNOOP:
            if (pIfInfo->u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
            {
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
                SnoopCreatePort (u4ContextId, u4IfIndex, u2LocalPortId);
#endif /* IGS_WANTED */
            }

            break;

        case L2IWF_PROTOCOL_ID_QOS:
#ifdef QOSX_WANTED
            if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                QosCreatePort (u4IfIndex);
            }
#endif /* QOSX_WANTED */
            break;

        case L2IWF_PROTOCOL_ID_LLDP:

            if (pIfInfo->u1IfType == CFA_ENET)
            {
                if (LldpApiNotifyIfCreate (u4IfIndex) == OSIX_FAILURE)
                {
                    i4RetVal = L2IWF_FAILURE;
                }
            }
            break;

        default:
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfSendPortDelIndForProtocol                       */
/*                                                                           */
/* Description        : This routine propagates the port delete indication   */
/*                      to the given protocol                                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
VOID
L2IwfSendPortDelIndForProtocol (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo,
                                INT4 i4ProtocolId)
{
    switch (i4ProtocolId)
    {

        case L2IWF_PROTOCOL_ID_PNAC:

            /* Assumption : PNAC/ISS is applicable for Ethernet 
             * interfaces only 
             * */
            if (pIfInfo->u1IfType == CFA_ENET)
            {
#ifdef PNAC_WANTED
                PnacDeletePort ((UINT2) u4IfIndex);
#endif
            }
            break;

        case L2IWF_PROTOCOL_ID_LLDP:

            if (pIfInfo->u1IfType == CFA_ENET)
            {
                LldpApiNotifyIfDelete (u4IfIndex);
            }
            break;

        case L2IWF_PROTOCOL_ID_LA:
#ifdef LA_WANTED
            if (pIfInfo->u1IfType == CFA_LAGG)
            {
                L2IwfDeleteAllPortsFromTrunk ((UINT2) u4IfIndex);
            }

            if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                LaDeletePort ((UINT2) u4IfIndex);
            }
#endif /* LA_WANTED */
            break;

        case L2IWF_PROTOCOL_ID_XSTP:

            if (pIfInfo->u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
            {
#ifdef RSTP_WANTED
                AstDeletePort ((UINT2) u4IfIndex);
#endif /* RSTP_WANTED */
            }

            break;

        case L2IWF_PROTOCOL_ID_SNOOP:
#if defined (IGS_WANTED) || defined (MLDS_WANTED)
            SnoopDeletePort (u4IfIndex);
#endif /* IGS_WANTED */

            break;

        case L2IWF_PROTOCOL_ID_QOS:
#ifdef QOSX_WANTED
            if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                QosDeletePort (u4IfIndex);
            }
#endif
            break;

        case L2IWF_PROTOCOL_ID_ELMI:
#ifdef ELMI_WANTED
            if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                ElmDeletePort ((UINT2) u4IfIndex);
            }
#endif
            break;

        case L2IWF_PROTOCOL_ID_ECFM:
#ifdef ECFM_WANTED
            EcfmDeletePort ((UINT2) u4IfIndex);
#endif /*ECFM_WANTED */
            break;

        case L2IWF_PROTOCOL_ID_PBB:
#ifdef PBB_WANTED
            if (!((pIfInfo->u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
                  (pIfInfo->u1IfSubType == CFA_SUBTYPE_SISP_INTERFACE)))
            {
                PbbDeletePort ((INT4) u4IfIndex);
            }
#endif /*PBB_WANTED */
            break;

        case L2IWF_PROTOCOL_ID_MRP:
#ifdef MRP_WANTED
            if (pIfInfo->u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
            {

                MrpApiDeletePort ((UINT2) u4IfIndex);
            }
#endif /*MRP_WANTED */
            break;

#ifdef VLAN_WANTED

        case L2IWF_PROTOCOL_ID_GARP:
            if (pIfInfo->u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
            {
#ifdef GARP_WANTED
                GarpDeletePort ((UINT2) u4IfIndex);
#endif /* GARP_WANTED */
            }

            break;

        case L2IWF_PROTOCOL_ID_VLAN:

            if (pIfInfo->u1BrgPortType != L2IWF_PROVIDER_INSTANCE_PORT)
            {
                VlanDeletePort ((UINT2) u4IfIndex);
            }

            break;
#endif /* VLAN_WANTED */

        case L2IWF_PROTOCOL_ID_BRIDGE:
#ifdef BRIDGE_WANTED
            BridgeDeletePort ((UINT2) u4IfIndex);
#endif /* BRIDGE_WANTED */
            break;

        default:
            break;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfPortOperStatIndForProtocol                      */
/*                                                                           */
/* Description        : This routine is used by Cfa IfType Protocol Deny     */
/*                      table. When an entry is Protocol deny table entry is */
/*                      deleted, those ports which are not created in the    */
/*                      protocol needs to be created and oper State indicated*/
/*                      The OperStatus of the port is taken from L2IWF       */
/*                      data base.                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfPortOperStatIndForProtocol (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo,
                                 INT4 i4ProtocolId)
{
    UINT1               u1Status = 0;

    L2IwfGetBridgePortOperStatus (u4IfIndex, &u1Status);

    switch (i4ProtocolId)
    {
        case L2IWF_PROTOCOL_ID_PNAC:

            /* Assumption : PNAC is applicable for Ethernet interfaces only */
            if (pIfInfo->u1IfType == CFA_ENET)
            {
#ifdef PNAC_WANTED
                if (PnacGetPnacEnableStatus () == (INT4) PNAC_SUCCESS)
                {
                    PnacPortStatusUpdate ((UINT2) u4IfIndex, u1Status);
                    /* NOTE : If PNAC is enabled, CFA should not inform other 
                     * applications about the physical ports. That will be done 
                     * after receiving an acknowledgement from PNAC module 
                     * depending on the port authorization status and control 
                     * direction value.
                     */
                    return;
                }
#endif
            }

            break;

        case L2IWF_PROTOCOL_ID_LLDP:

            if (pIfInfo->u1IfType == CFA_ENET)
            {
                /* Send oper staus change indication to LLDP module */
                LldpApiNotifyIfOperStatusChg (u4IfIndex, u1Status);
            }

            break;

        case L2IWF_PROTOCOL_ID_LA:

            if ((pIfInfo->u1IfType == CFA_ENET) ||
                ((pIfInfo->u1IfType == CFA_LAGG) &&
                 (pIfInfo->u1BridgedIface == CFA_DISABLED)))
            {
#ifdef LA_WANTED
                /* Assumption : LA is applicable for Ethernet interfaces only */
                if (LaGetLaEnableStatus () == LA_ENABLED)
                {
                    LaUpdatePortStatus ((UINT2) u4IfIndex, u1Status);
                }
#endif
            }

            break;

        case L2IWF_PROTOCOL_ID_BRIDGE:

#ifdef BRIDGE_WANTED
            BridgePortOperStatusIndication (u4IfIndex, u1Status);
#endif

            break;

        case L2IWF_PROTOCOL_ID_QOS:

#ifdef QOSX_WANTED
            QosPortOperIndication (u4IfIndex, u1Status);
#endif
            break;

        case L2IWF_PROTOCOL_ID_XSTP:

            if (pIfInfo->u1BrgPortType != CFA_PROVIDER_INSTANCE_PORT)
            {
#ifdef RSTP_WANTED
                AstUpdatePortStatus (u4IfIndex, u1Status);
#endif
            }

            break;

        case L2IWF_PROTOCOL_ID_ELMI:

#ifdef ELMI_WANTED
            if (pIfInfo->u1BrgPortType != CFA_PROVIDER_INSTANCE_PORT)
            {
                if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_FALSE)
                {
                    ElmUpdatePortStatus (u4IfIndex, u1Status);
                }
            }
#endif
            break;

#ifdef VLAN_WANTED
        case L2IWF_PROTOCOL_ID_GARP:
#ifdef GARP_WANTED

            if (pIfInfo->u1BrgPortType != CFA_PROVIDER_INSTANCE_PORT)
            {
                GarpPortOperInd ((UINT2) u4IfIndex, u1Status);
            }

#endif
            break;

        case L2IWF_PROTOCOL_ID_VLAN:

            if (pIfInfo->u1BrgPortType != CFA_PROVIDER_INSTANCE_PORT)
            {
                VlanPortOperInd (u4IfIndex, u1Status);
            }

            break;
#endif
        case L2IWF_PROTOCOL_ID_MRP:
#ifdef MRP_WANTED
            if (pIfInfo->u1BrgPortType != CFA_PROVIDER_INSTANCE_PORT)
            {
                MrpApiPortOperInd (u4IfIndex, u1Status);
            }
#endif
            break;

        case L2IWF_PROTOCOL_ID_SNOOP:
#if defined IGS_WANTED || MLDS_WANTED
            if (pIfInfo->u1BrgPortType != CFA_PROVIDER_INSTANCE_PORT)
            {
                SnoopPortOperIndication (u4IfIndex, u1Status);
            }
#endif
            break;

        case L2IWF_PROTOCOL_ID_PBB:
#ifdef PBB_WANTED
            /* Port oper indication for SISP ports should not be given
             * to PBB */
            if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_FALSE)
            {
                PbbUpdatePortStatus ((INT4) u4IfIndex, u1Status);
            }
#endif
            break;

        case L2IWF_PROTOCOL_ID_ECFM:
#ifdef ECFM_WANTED
            EcfmUpdatePortStatus (u4IfIndex, u1Status);
#endif
            break;
        default:
            break;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfLldpHandleApplRequest                           */
/*                                                                           */
/* Description        : This routine stores the application data in the next */
/*                      free index of the array of applications              */
/*                                                                           */
/* Input(s)           : pL2LldpAppInfo - Pointer to LLDP application info    */
/*                      u1ApplRequest -                                      */
/*                          L2IWF_LLDP_APPL_REGISTER - To register in L2IWF  */
/*                                                     module database       */
/*                          L2IWF_LLDP_APPL_DEREGISTER - To de-register from */
/*                                                     LLDP module database  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*****************************************************************************/
PUBLIC INT4
L2IwfLldpHandleApplRequest (tL2LldpAppInfo * pL2LldpAppInfo,
                            UINT1 u1ApplRequest)
{
    tLldpAppId          TmpLldpAppId;
    UINT2               u2Count = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&TmpLldpAppId, 0, sizeof (tLldpAppId));

    L2_LOCK ();

    switch (u1ApplRequest)
    {
        case L2IWF_LLDP_APPL_REGISTER:
            /* This case stores the application data in the next
             * free index of the array of applications */
            for (u2Count = 0;
                 ((u2Count < LLDP_MAX_APPL) && (i4RetVal == OSIX_FAILURE));
                 u2Count++)
            {
                /* Get the free index by searching for app.id as all zeros */
                if (MEMCMP (&gL2GlobalInfo.paL2LldpAppInfo[u2Count].LldpAppId,
                            &TmpLldpAppId, sizeof (tLldpAppId)) == 0)
                {
                    /* Store the application info into the free index */
                    MEMCPY (&gL2GlobalInfo.paL2LldpAppInfo[u2Count],
                            pL2LldpAppInfo, sizeof (tL2LldpAppInfo));

                    i4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        case L2IWF_LLDP_APPL_DEREGISTER:
            /* This routine removes the application data from the
             * array of applications */
            for (u2Count = 0;
                 ((u2Count < LLDP_MAX_APPL) && (i4RetVal == OSIX_FAILURE));
                 u2Count++)
            {
                /* Get the free index by searching for app.id as all zeros */
                if (MEMCMP (&gL2GlobalInfo.paL2LldpAppInfo[u2Count].LldpAppId,
                            &pL2LldpAppInfo->LldpAppId,
                            sizeof (tLldpAppId)) == 0)
                {
                    /* Reset the application info in the database */
                    MEMSET (&gL2GlobalInfo.paL2LldpAppInfo[u2Count], 0,
                            sizeof (tL2LldpAppInfo));

                    i4RetVal = OSIX_SUCCESS;
                }
            }
            break;
        default:
            break;
    }

    L2_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfLldpPostApplReReg                               */
/*                                                                           */
/* Description        : This routine is invoked by LLDP during its module    */
/*                      initialization and all the applications should be    */
/*                      intimated to re-register again with LLDP.            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
L2IwfLldpPostApplReReg (VOID)
{
    tLldpAppTlv         LldpAppTlv;
    tLldpAppId          TmpLldpAppId;
    UINT2               u2Count = 0;

    MEMSET (&TmpLldpAppId, 0, sizeof (tLldpAppId));
    MEMSET (&LldpAppTlv, 0, sizeof (tLldpAppTlv));

    LldpAppTlv.u4MsgType = L2IWF_LLDP_APPL_RE_REG;

    L2_LOCK ();

    for (u2Count = 0; u2Count < LLDP_MAX_APPL; u2Count++)
    {
        /* Ensure the application is valid by comparing against
         * all zero application Id */
        if (MEMCMP (&gL2GlobalInfo.paL2LldpAppInfo[u2Count].LldpAppId,
                    &TmpLldpAppId, sizeof (tLldpAppId)) != 0)
        {
            /* Set the appropriate application ID */
            MEMCPY (&LldpAppTlv.LldpAppId,
                    &gL2GlobalInfo.paL2LldpAppInfo[u2Count].LldpAppId,
                    sizeof (tLldpAppId));
            /* Invoke the call back routine of the application */
            gL2GlobalInfo.paL2LldpAppInfo[u2Count].pAppCallBackFn (&LldpAppTlv);
        }
    }

    L2_UNLOCK ();
    /* Posted to all the registered applications */
    return;
}

/*****************************************************************************/
/* Function Name      : L2IwfLldpHandleApplPortRequest                       */
/*                                                                           */
/* Description        : This function is invoked by LLDP applications to     */
/*                      register / update / de-register its information on a */
/*                      port                                                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port on which the application is to      */
/*                                  be registered                            */
/*                      pLldpAppPortMsg - Message to be stored in LLDP       */
/*                      u1Request -                                          */
/*                           L2IWF_LLDP_APPL_PORT_REGISTER - To register     */
/*                           L2IWF_LLDP_APPL_PORT_UPDATE - To update         */
/*                           L2IWF_LLDP_APPL_PORT_DEREGISTER - To de-register*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS or OSIX_FAILURE                         */
/*****************************************************************************/
PUBLIC INT4
L2IwfLldpHandleApplPortRequest (UINT4 u4IfIndex,
                                tLldpAppPortMsg * pLldpAppPortMsg,
                                UINT1 u1Request)
{
    return (LldpApiHandleApplPortRequest (u4IfIndex, pLldpAppPortMsg,
                                          u1Request));
}

/*****************************************************************************/
/* Function Name      : L2IwfIsDhcpPacket                                    */
/*                                                                           */
/* Description        : This routine is used to identify if the received     */
/*                      frame is a DHCP packet                               */
/*                                                                           */
/* Input(s)           : pBuf        - Pointer to the Frame                   */
/*                                                                           */
/* Output(s)          : u1Result    - Pointer to the result that indicates   */
/*                      OSIX_TRUE if the frame is an dhcp packet and         */
/*                      OSIX_FALSE otherwise.                                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfIsDhcpPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Result,
                   UINT4 u4IfIndex)
{
    UINT4               u4VlanOffset = VLAN_TAG_OFFSET;
    UINT2               u2LenOrType = 0;
    UINT2               u2SrcPort = 0;
    UINT2               u2DstPort = 0;
    UINT1               u1IpHdrLen = 0;
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         IpHdr;

    MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
    pIpHdr = &IpHdr;

    *pu1Result = OSIX_FALSE;

    if (VlanGetTagLenInFrame (pBuf, u4IfIndex, &u4VlanOffset) == VLAN_SUCCESS)
    {
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2LenOrType,
                                   u4VlanOffset, VLAN_TYPE_OR_LEN_SIZE);
        u2LenOrType = OSIX_NTOHS (u2LenOrType);
    }
    /* If the packet type is not IP return FALSE. */
    if (u2LenOrType != L2_ENET_IP)
    {
        return L2IWF_SUCCESS;
    }
    /* Extracting IP header from the packet to identify the
     *      * protocol type.
     *           */
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr,
                               u4VlanOffset + VLAN_TYPE_OR_LEN_SIZE,
                               sizeof (t_IP_HEADER));

    if (pIpHdr->u1Proto != L2_UDP_PROTOCOL)
    {
        return L2IWF_SUCCESS;
    }

    u1IpHdrLen =
        (UINT1) (((pIpHdr->u1Ver_hdrlen) & L2_IP_HDRLEN_BITMASK) *
                 L2_BYTE_IN_WORD);

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2SrcPort,
                               (u4VlanOffset + VLAN_TYPE_OR_LEN_SIZE
                                + u1IpHdrLen), sizeof (UINT2));
    u2SrcPort = OSIX_NTOHS (u2SrcPort);

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2DstPort,
                               (u4VlanOffset + VLAN_TYPE_OR_LEN_SIZE +
                                u1IpHdrLen + sizeof (UINT2)), sizeof (UINT2));
    u2DstPort = OSIX_NTOHS (u2DstPort);

    if ((u2SrcPort == L2_DHCP_SERVER_PORT)
        || (u2SrcPort == L2_DHCP_CLIENT_PORT))
    {
        *pu1Result = OSIX_TRUE;
        return L2IWF_SUCCESS;
    }
    if ((u2DstPort == L2_DHCP_SERVER_PORT)
        || (u2DstPort == L2_DHCP_CLIENT_PORT))
    {
        *pu1Result = OSIX_TRUE;
    }
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetOuterVlanInfoFromFrame                       */
/*                                                                           */
/* Description        : This routine is used to get OuterVlan.               */
/*                                                                           */
/* Input(s)           : pBuf        - Pointer to the Frame                   */
/*                                                                           */
/* Output(s)          : pu2OuterVlanId    - pointer Outer VlanId             */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
L2IwfGetOuterVlanInfoFromFrame (tCRU_BUF_CHAIN_DESC * pBuf,
                                UINT2 *pu2OuterVlanId)
{

    UINT4               u4VlanOffset = 0;
    UINT2               u2Protocol = 0;
    UINT2               u2VlanIdInPkt = 0;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Protocol,
                               CFA_VLAN_TAG_OFFSET, CFA_VLAN_PROTOCOL_SIZE);

    u2Protocol = OSIX_NTOHS (u2Protocol);

    if ((u2Protocol == VLAN_PROVIDER_PROTOCOL_ID) ||
        (u2Protocol == VLAN_PROTOCOL_ID))
    {
        u4VlanOffset = CFA_VLAN_TAG_OFFSET + CFA_VLAN_PROTOCOL_SIZE;
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2VlanIdInPkt,
                                   u4VlanOffset, 2);
        u2VlanIdInPkt = OSIX_NTOHS (u2VlanIdInPkt);
        u2VlanIdInPkt = 0x0fff & u2VlanIdInPkt;
    }

    *pu2OuterVlanId = u2VlanIdInPkt;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetACIfIndexFromFrame                           */
/*                                                                           */
/* Description        : This routine is used to get AC IfIndex.              */
/*                                                                           */
/* Input(s)           : pBuf        - Pointer to the Frame                   */
/*                      u4IfIndex   - Interface Index                        */
/*                                                                           */
/* Output(s)          : *pu4AcIfIndex  - pointer Outer VlanId                */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/
INT4
L2IwfGetACIfIndexFromFrame (tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4IfIndex,
                            UINT4 *pu4AcIfIndex)
{
    UINT4               u4RetValFsDot1qPvid = 0;
    UINT2               u2OuterVlanId = 0;

    /* if only the physical port is configured for AC than All packet recive on that port should be 
     * send throug that mapped AC */
    if (CfaIwfMainACValidate (u4IfIndex, u2OuterVlanId, pu4AcIfIndex) ==
        CFA_SUCCESS)
    {
        return L2IWF_SUCCESS;
    }

    L2IwfGetOuterVlanInfoFromFrame (pBuf, &u2OuterVlanId);

    /*if physical port and vlan is configured for AC than tagged packet recive on that port should be
     * send by the mapped AC(port, Vlan) */
    if (u2OuterVlanId != 0)
    {
        if (CfaIwfMainACValidate (u4IfIndex, u2OuterVlanId, pu4AcIfIndex) ==
            CFA_SUCCESS)
        {
            return L2IWF_SUCCESS;
        }
    }
    /*In AC (port,vlan) if untagged packet is coming then the packet is Control packet and it should send 
     * by AC that is mapped to port and switch port ID configured for that port.*/
    else
    {
        if (VlanApiGetPvid (u4IfIndex, &u4RetValFsDot1qPvid) == VLAN_SUCCESS)
        {

            if (CfaIwfMainACValidate
                (u4IfIndex, (UINT2) u4RetValFsDot1qPvid,
                 pu4AcIfIndex) == CFA_SUCCESS)
            {
                return L2IWF_SUCCESS;
            }
        }
    }

    return L2IWF_FAILURE;
}

/*****************************************************************************/
/* Function Name      : L2IwfFillVidDigestBuffer                             */
/*                                                                           */
/* Description        : This routine is used to get the MST instance values 
 *                      in the given inpit.                                  */
/*                                                                           */
/* Input(s)           : pBuf        - Pointer to Buf to fill the MST instance*/
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS                                        */
/*****************************************************************************/
INT4
L2IwfCalculateVidDigest (UINT4 u4ContextId)
{

    UINT4               u4Len = AST_MAX_MST_INSTANCES;
    UINT1               au1VidDigestInput[AST_MAX_MST_INSTANCES * 2] = { 0 };
    UINT2               u2MstInstance = 0;

    MEMSET (au1VidDigestInput, 0, AST_MAX_MST_INSTANCES * 2);

    if (AstIsMstStartedInContext (u4ContextId) == AST_TRUE)
    {
        for (u2MstInstance = 1; u2MstInstance < AST_MAX_MST_INSTANCES;
             u2MstInstance = (UINT2) (u2MstInstance + 2))
        {
            if (u2MstInstance < LLDP_MAX_VID_DIGEST_LEN)
            {
                if (L2IWF_MST_INFO (u2MstInstance) != NULL)
                {
                    MEMCPY (&au1VidDigestInput[u2MstInstance], &u2MstInstance,
                            sizeof (UINT2));
                }
            }
        }

        gpL2Context->u4VIDDigest =
            UtilCalculateCRC32 (au1VidDigestInput, u4Len);
    }

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfSendVidDigestToLLDP                             */
/*                                                                           */
/* Description        : This routine is used to send the Vid digest to LLDP  */
/*                                                                           */
/* Input(s)           : u4Context Id                                         */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS                                        */
/*****************************************************************************/

INT4
L2IwfSendVidDigestToLLDP (UINT4 u4ContextId, UINT4 u4CRC32)
{
#ifdef LLDP_WANTED
    UINT1              *pPortList = NULL;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;
    tMacAddr            AgentMacAddr;

    MEMSET (AgentMacAddr, 0, sizeof (tMacAddr));

    pPortList = (UINT1 *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        return L2IWF_FAILURE;
    }

    if (VcmGetContextPortList (u4ContextId, pPortList) == VCM_FAILURE)
    {
        FsUtilReleaseBitList (pPortList);
        return L2IWF_FAILURE;
    }

    for (u2ByteInd = 0; u2ByteInd < CONTEXT_PORT_LIST_SIZE; u2ByteInd++)
    {
        if (pPortList[u2ByteInd] == 0)
        {
            continue;
        }
        u1PortFlag = *(pPortList + u2ByteInd);

        for (u2BitIndex = 0;
             ((u2BitIndex < BITS_PER_BYTE) && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & 0x80) != 0)
            {
                u2Port = (UINT2) ((u2ByteInd * BITS_PER_BYTE) + u2BitIndex + 1);
                if (LldpApiUpdateDigestInput (u2Port, &AgentMacAddr,
                                              u4CRC32, NULL) == OSIX_FAILURE)
                {
                    FsUtilReleaseBitList (pPortList);
                    return L2IWF_FAILURE;
                }

            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    FsUtilReleaseBitList (pPortList);
    return L2IWF_SUCCESS;
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4CRC32);
    return L2IWF_SUCCESS;
#endif
}

INT4
L2IwfDeleteAllPortsFromTrunk (UINT2 u2AggId)
{
    UINT4               u4Port;
    UINT2               u2Index;
    tPortList          *pAggConfPorts = NULL;
    tL2PortInfo        *pL2PortEntry = NULL;
#ifdef NPAPI_WANTED
#ifndef LA_HW_TRUNK_SUPPORTED
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2NumPorts;

    L2IwfGetConfiguredPortsForPortChannel (u2AggId, au2ConfPorts, &u2NumPorts);

    for (u2Index = 0; u2Index < u2NumPorts; u2Index++)
    {
        VlanRemovePortPropertiesFromHw (L2IWF_PORT_CHANNEL_CONFIGURED_PORT
                                        (u2AggId, u2Index), u2AggId);
        PbbRemovePortPropertiesFromHw (L2IWF_PORT_CHANNEL_CONFIGURED_PORT
                                       (u2AggId, u2Index), u2AggId);
    }
#endif
#endif

    pAggConfPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pAggConfPorts == NULL)
    {
        return L2IWF_FAILURE;
    }

    MEMSET (*pAggConfPorts, 0, sizeof (tPortList));

    L2_LOCK ();

    for (u2Index = 0; u2Index < L2IWF_PORT_CHANNEL_NUM_CONF_PORTS (u2AggId);
         u2Index++)
    {
        u4Port = L2IWF_PORT_CHANNEL_CONFIGURED_PORT (u2AggId, u2Index);
        /* Get the list of configured ports and send the notification to LLDP
         * so that the capability can be reset*/
        OSIX_BITLIST_SET_BIT ((*pAggConfPorts), u4Port, sizeof (tPortList));

        pL2PortEntry = L2IwfGetIfIndexEntry (u4Port);
        if (pL2PortEntry == NULL)
        {
            FsUtilReleaseBitList ((UINT1 *) pAggConfPorts);
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
        L2IWF_IFENTRY_AGG_INDEX (pL2PortEntry) = u4Port;
    }

    L2IWF_PORT_CHANNEL_NUM_CONF_PORTS (u2AggId) = 0;

    L2_UNLOCK ();

    LldpApiNotifyResetAggCapability (*pAggConfPorts);
    FsUtilReleaseBitList ((UINT1 *) pAggConfPorts);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfLldpApiGetInstanceId                            */
/*                                                                           */
/* Description        : This routine is used to Get Instance Id from   LLDP  */
/*                                                                           */
/* Input(s)           : pu1MacAddr    - destination mac address              */
/*                      pu4InstanceId - destination mac address instance id  */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/FAILURE                                 */
/*****************************************************************************/

PUBLIC INT4
L2IwfLldpApiGetInstanceId (UINT1 *pu1MacAddr, UINT4 *pu4InstanceId)
{
    return (LldpApiGetInstanceId (pu1MacAddr, pu4InstanceId));
}

/*****************************************************************************/
/* Function Name      : L2IwfLldpApiSetDstMac                                */
/*                                                                           */
/* Description        : This routine is used to Api Destinatin Mac in  LLDP  */
/*                                                                           */
/* Input(s)           : i4UapIfIndex - Uap Index                             */
/*                      pu1MacAddr   - Destination Mac Address               */
/*                      RowStatus    - Row Staus                             */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/FAILURE                                 */
/*****************************************************************************/

PUBLIC INT4
L2IwfLldpApiSetDstMac (INT4 i4UapIfIndex, UINT1 *pu1MacAddr, UINT1 u1RowStatus)
{
    return (LldpApiSetDstMac (i4UapIfIndex, pu1MacAddr, u1RowStatus));
}

/*****************************************************************************/
/* Function Name      : L2IwfApiGetPortType                                  */
/*                                                                           */
/* Description        : This routine is used to Api Port type in  Y1564      */
/*                                                                           */
/* Input(s)           : u4IfIndex    - Interface Index                       */
/*                      u4PortType   - Port Type                             */
/*                                                                           */
/* Output(s)          :                                                      */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/FAILURE                                 */
/*****************************************************************************/

PUBLIC INT4
L2IwfApiGetPortType (UINT4 u4IfIndex, UINT4 *u4PortType)
{

    if (CfaGetIfOperMauType (u4IfIndex, ZERO, u4PortType) == CFA_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/**********************************************************************************
 * * Function           : L2IwfMbsmGetNpSyncStatus
 * * Input(s)           : *pu1Status - pointer to get status of np bulk sync
 * * Output(s)          : *pu1Status - Status of Np bulk sync
 * * Returns            : This function gets the Mbsm's Np bulk sync status
 ** ******************************************************************************/
VOID
L2IwfMbsmGetNpSyncStatus (UINT1 *pu1Status)
{
    if (pu1Status == NULL)
    {
        return;
    }
    L2_LOCK ();
    *pu1Status = L2IWF_MBSM_NP_BULK_SYNC_STATUS ();
    L2_UNLOCK ();
    return;
}

/******************************************************************************
 * * Function           : L2IwfMbsmSetNpSyncStatus
 * * Input(s)           : u1Status - Status of NP bulk sync
 *                        RM_STARTED/RM_COMPLETED/RM_FAILED
 * * Output(s)          : None
 * * Returns            : This function sets the Mbsm's Np bulk sync status
 * ******************************************************************************/

VOID
L2IwfMbsmSetNpSyncStatus (UINT1 u1Status)
{
    L2_LOCK ();
    L2IWF_MBSM_NP_BULK_SYNC_STATUS () = u1Status;
    L2_UNLOCK ();
    return;
}

/******************************************************************************
 * * Function           : L2IwfLldpApiUpdateSvidOnUap
 * * Input(s)           : u4UapIfIndex - UAP Interface Index
 * *                    : u4Svid - SVLAN Id
 * *                    : u4Request - To Add/Delete Request in the LLDP DB
 * * Output(s)          : None
 * * Returns            :
 * ******************************************************************************/
PUBLIC VOID
L2IwfLldpApiUpdateSvidOnUap (UINT4 u4UapIfIndex, UINT4 u4Svid, UINT4 u4Request)
{
#ifdef LLDP_WANTED
    LldpApiUpdateSvidOnUap (u4UapIfIndex, u4Svid, u4Request);
#else
    UNUSED_PARAM (u4UapIfIndex);
    UNUSED_PARAM (u4Svid);
    UNUSED_PARAM (u4Request);
#endif
    return;
}

/**********************************************************************************
 * * Function           : L2IwfMbsmGetNpSyncProgressStatus
 * * Input(s)           : None
 * * Output(s)          : *pu4Status - Status of Np bulk sync
 * * Returns            : This function gets  Mbsm's Np bulk sync Progress status
 *                        which will help in restarting the timer when MBSM sync 
 *                        is in Progress
 ** ******************************************************************************/
VOID
L2IwfMbsmGetNpSyncProgressStatus (UINT4 *pu4Status)
{
#ifdef MBSM_WANTED
    MbsmGetNpSyncProgressStatus (pu4Status);
#else
    UNUSED_PARAM (pu4Status);
#endif
    return;
}

/**********************************************************************************
 * * Function           : L2IwfIsPortInInst
 * * Input(s)           : u4ContextId - Context ID
 *                        u4IfIndex - Interface index
 *                        u2InstId - Instance ID
 * * Output(s)          : None
 * * Return             : L2IWF_FAILURE / L2IWF_SUCCESS
 * * Returns            : This function checks whether the port is a member 
 *                        of any vlan mapped to this instance
 ** ******************************************************************************/

INT4
L2IwfIsPortInInst (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2InstId)
{
    /*Return Success if this port is a member of any vlan mapped to this instance */
    tL2PortVlanInfo    *pPortVlanEntry;
    UINT2               u2LocalPortId = 0;
    tVlanId             VlanId = 0;
    BOOL1               bMemberResult = OSIX_FALSE;

    if (VcmGetContextInfoFromIfIndex
        (u4IfIndex, &u4ContextId, &u2LocalPortId) != VCM_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

    if (u4ContextId >= L2IWF_MAX_CONTEXTS)

    {
        return L2IWF_FAILURE;
    }

    if (u2LocalPortId > L2IWF_MAX_PORTS_PER_CONTEXT)
    {
        return L2IWF_FAILURE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    if (L2IWF_PORT_ACTIVE (u2LocalPortId) != OSIX_TRUE)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }
    u4IfIndex = L2IWF_PORT_IFINDEX (u2LocalPortId);

    for (VlanId = 1; VlanId <= VLAN_DEV_MAX_VLAN_ID; VlanId++)
    {
        if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_TRUE)
        {
            pPortVlanEntry = L2IwfGetPortVlanEntry (u4IfIndex, VlanId);
            if (pPortVlanEntry == NULL)
            {
                continue;
            }
            else
            {
                bMemberResult = OSIX_FALSE;
                OSIX_BITLIST_IS_BIT_SET (L2IWF_VLAN_EGRESS_PORTS (VlanId),
                                         u2LocalPortId, sizeof
                                         (tLocalPortList), bMemberResult);
                if (bMemberResult == OSIX_TRUE)
                {
                    /*Check if the vlan is part of instance */

                    if (u2InstId == L2IWF_VID_INST_MAP (VlanId))
                    {
                        L2IwfReleaseContext ();
                        L2_UNLOCK ();

                        return L2IWF_SUCCESS;
                    }

                }

            }
        }
    }                            /* For Loop Close */

    L2IwfReleaseContext ();
    L2_UNLOCK ();

    /*Return Failure if this port is not a member of any vlan mapped to this instance */
    return L2IWF_FAILURE;

}

/* END OF FILE */
