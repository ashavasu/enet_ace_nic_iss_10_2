/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: l2pb.c,v 1.33 2016/03/14 10:26:59 siva Exp $ 
 *
 * Description: This file has L2 related Provider Bridge functions 
 *
 *******************************************************************/
#include "l2inc.h"

/*****************************************************************************/
/* Function Name      : L2IwfPbSetVlanServiceType                            */
/*                                                                           */
/* Description        : This function is used to set the VLAN service type   */
/*                      in L2IWF. The service type is updated by VLAN        */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : VlanId - Service VLAN Identifier                     */
/*                                                                           */
/* Output(s)          : u1ServiceType - VLAN_E_LINE /VLAN_E_LAN              */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfPbSetVlanServiceType (UINT4 u4ContextId, tVlanId VlanId,
                           UINT1 u1ServiceType)
{
    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId == 0)
    {
        return L2IWF_FAILURE;
    }
    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IWF_VLAN_SERVICE_TYPE (VlanId) = u1ServiceType;
    L2IwfReleaseContext ();

    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbGetVlanServiceType                            */
/*                                                                           */
/* Description        : This function is used to get the type of service the */
/*                      VLAN provides. The service type is updated by VLAN   */
/*                      module                                               */
/*                                                                           */
/* Input(s)           : VlanId - Service VLAN Identifier                     */
/*                                                                           */
/* Output(s)          : *pu1ServiceType - VLAN_E_LINE /VLAN_E_LAN            */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
L2IwfPbGetVlanServiceType (UINT4 u4ContextId, tVlanId VlanId,
                           UINT1 *pu1ServiceType)
{
    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId == 0)
    {
        return L2IWF_FAILURE;
    }
    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_FALSE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1ServiceType = L2IWF_VLAN_SERVICE_TYPE (VlanId);
    L2IwfReleaseContext ();

    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfPbPepHandleInCustBpdu                 */
/*                                                                           */
/*    Description               : This API is used to post the customer BPDUs*/
/*                                received on PEPs to AST task.              */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                u2Port - Incoming port.                    */
/*                                SVlanId - Service VLAN ID                  */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/

VOID
L2IwfPbPepHandleInCustBpdu (tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4IfIndex,
                            tVlanId SVlanId)
{
#ifdef RSTP_WANTED
    AstPbHandleInFrame (pBuf, (UINT2) u4IfIndex, SVlanId);
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfSendCustBpduOnPep                     */
/*                                                                           */
/*    Description               : This API is used by L2 modules to transmit */
/*                                packets on a set of port through CFA.      */
/*                                                                           */
/*    Input(s)                  : pBuf - Pointer to the CRU Buffer.          */
/*                                u4IfIndex - ifIndex of the driver port.    */
/*                                u4PktSize - Size of the data buffer        */
/*                                u2Protocol - Protocol type                 */
/*                                u1EncapType - Encapsulation Type           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfSendCustBpduOnPep (UINT4 u4CepIfIndex, tVlanId SVlanId,
                        tCRU_BUF_CHAIN_DESC * pBuf, UINT4 u4PktSize)
{
    tL2PortInfo        *pL2PortEntry = NULL;

    UNUSED_PARAM (u4PktSize);
    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4CepIfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        return L2IWF_FAILURE;
    }

    L2_UNLOCK ();

    VlanPbSendCustBpduOnPep (pBuf, u4CepIfIndex, SVlanId);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfGetNextCVlanPort                      */
/*                                                                           */
/*    Description               : This function is used to get the next PEP  */
/*                                Port in a given CVLAN component            */
/*                                                                           */
/*    Input(s)                  : u4CepIfIndex - Indicates CVLAN comp.       */
/*                                u2PrevPort - Previous PEP HL Port Id       */
/*                                                                           */
/*    Output(s)                 :*pu2NextPort - Next PEP's HL Port Id        */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/

INT4
L2IwfGetNextCVlanPort (UINT4 u4CepIfIndex, UINT2 u2PrevPort, UINT2 *pu2NextPort,
                       UINT4 *pu4NextCepIfIndex)
{

    tL2PortInfo        *pL2PortEntry = NULL;
    tL2CVlanPortEntry  *pL2CVlanPortEntry = NULL;
    tL2CVlanPortEntry   TempCVlanPortEntry;
    UINT4               u4ContextId = 0;

    MEMSET (&TempCVlanPortEntry, 0, sizeof (TempCVlanPortEntry));

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4CepIfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    TempCVlanPortEntry.u2Port = pL2PortEntry->u2LocalPortId;

    TempCVlanPortEntry.SVid = u2PrevPort;

    pL2CVlanPortEntry = RBTreeGetNext (L2IWF_CVLAN_PORT_TBL (),
                                       &TempCVlanPortEntry, L2IwfPbCmpPepEntry);

    if (pL2CVlanPortEntry == NULL)
    {
        L2IwfReleaseContext ();

        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    *pu2NextPort = pL2CVlanPortEntry->SVid;

    *pu4NextCepIfIndex = L2IWF_PORT_IFINDEX (pL2CVlanPortEntry->u2Port);

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPbPortOperEdgeStatus                         */
/*                                                                           */
/* Description        : This function is used to set Provider Edge Port oper */
/*                      Edge status                                          */
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                      bOperEdge - OSIX_TRUE/OSIX_FALSE                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
L2IwfSetPbPortOperEdgeStatus (UINT4 u4CepIfIndex, tVlanId SVlanId,
                              BOOL1 bOperEdge)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tL2CVlanPortEntry  *pL2CVlanPortEntry = NULL;
    UINT4               u4ContextId = 0;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4CepIfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    pL2CVlanPortEntry =
        L2IwfPbGetPepEntry (pL2PortEntry->u2LocalPortId, SVlanId);

    if (pL2CVlanPortEntry == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pL2CVlanPortEntry->bOperEdge = bOperEdge;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPbPortOperEdgeStatus                         */
/*                                                                           */
/* Description        : This function is used to get Provider Edge Port oper */
/*                      Edge status                                          */
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                                                                           */
/* Output(s)          : *pbOperEdge - OSIX_TRUE/OSIX_FALSE                   */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
L2IwfGetPbPortOperEdgeStatus (UINT4 u4CepIfIndex, tVlanId SVlanId,
                              BOOL1 * pbOperEdge)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tL2CVlanPortEntry  *pL2CVlanPortEntry = NULL;
    UINT4               u4ContextId = 0;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4CepIfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    pL2CVlanPortEntry =
        L2IwfPbGetPepEntry (pL2PortEntry->u2LocalPortId, SVlanId);

    if (pL2CVlanPortEntry == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pbOperEdge = pL2CVlanPortEntry->bOperEdge;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfGetPbPortState                                  */
/*                                                                           */
/* Description        : This function is used to get Provider Edge Port State*/
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                                                                           */
/* Output(s)          : u1PortState - Port State of the PEP Port(CEP,SVID)   */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

UINT1
L2IwfGetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tL2CVlanPortEntry  *pL2CVlanPortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT1               u1PortState = AST_PORT_STATE_DISCARDING;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4CepIfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();

        return u1PortState;
    }

    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();

        return u1PortState;
    }

    pL2CVlanPortEntry =
        L2IwfPbGetPepEntry (pL2PortEntry->u2LocalPortId, SVlanId);

    if (pL2CVlanPortEntry == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return u1PortState;
    }

    u1PortState = pL2CVlanPortEntry->u1PortState;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return u1PortState;
}

/*****************************************************************************/
/* Function Name      : L2IwfSetPbPortState                                  */
/*                                                                           */
/* Description        : This function is used to set Provider Edge Port State*/
/*                                                                           */
/* Input(s)           : u4CepIfIndex - CEP port Index.                       */
/*                      SVlanId - Service VLAN Identifier                    */
/*                      u1PortState - Port State of the PEP Port(CEP,SVID)   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/

INT4
L2IwfSetPbPortState (UINT4 u4CepIfIndex, tVlanId SVlanId, UINT1 u1PortState)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tL2CVlanPortEntry  *pL2CVlanPortEntry = NULL;
    UINT4               u4ContextId = 0;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4CepIfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    pL2CVlanPortEntry =
        L2IwfPbGetPepEntry (pL2PortEntry->u2LocalPortId, SVlanId);

    if (pL2CVlanPortEntry == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    pL2CVlanPortEntry->u1PortState = u1PortState;

#ifdef ECFM_WANTED
    EcfmPbPortStateChangeIndication (u4CepIfIndex, SVlanId, u1PortState);
#endif

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : L2IwfGetPbPortOperStatus                   */
/*                                                                           */
/*    Description               : This function is used to get the PEP       */
/*                                Port Oper Status which is updated by VLAN  */
/*                                                                           */
/*    Input(s)                  : u4CepIfIndex - Indicates CVLAN comp.       */
/*                                SVlanId - Service VLAN Identifier          */
/*                                u1Module - STP_MODULE                      */
/*                                                                           */
/*    Output(s)                 :*pu1OperStatus - PEP port oper status       */
/*                                                                           */
/*    Returns                   : L2IWF_SUCCESS or L2IWF_FAILURE.            */
/*                                                                           */
/*****************************************************************************/

INT4
L2IwfGetPbPortOperStatus (UINT1 u1Module, UINT4 u4CepIfIndex, tVlanId SVlanId,
                          UINT1 *pu1OperStatus)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    tL2CVlanPortEntry  *pL2CVlanPortEntry = NULL;
    UINT4               u4ContextId = 0;

    UNUSED_PARAM (u1Module);

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4CepIfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    pL2CVlanPortEntry =
        L2IwfPbGetPepEntry (pL2PortEntry->u2LocalPortId, SVlanId);

    if (pL2CVlanPortEntry == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    *pu1OperStatus = pL2CVlanPortEntry->u1OperStatus;

    L2IwfReleaseContext ();

    L2_UNLOCK ();

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfPbCmpPepEntry                               */
/*                                                                           */
/*    Description         : This function is used to compare two PEP         */
/*                          table entries                                    */
/*                                                                           */
/*    Input(s)            : pNodeA,pNodeB - Logical port table entries       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pNodeA > pNodeB                              */
/*                          -1 - pNodeA < pNodeB                             */
/*                          0 - pNodeA == PNodeB                             */
/*                                                                           */
/*****************************************************************************/
INT4
L2IwfPbCmpPepEntry (tRBElem * pNodeA, tRBElem * pNodeB)
{
    tL2CVlanPortEntry  *pL2CVlanPortEntryA = pNodeA;
    tL2CVlanPortEntry  *pL2CVlanPortEntryB = pNodeB;

    if (pL2CVlanPortEntryA->u2Port != pL2CVlanPortEntryB->u2Port)
    {
        if (pL2CVlanPortEntryA->u2Port > pL2CVlanPortEntryB->u2Port)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }

    if (pL2CVlanPortEntryA->SVid != pL2CVlanPortEntryB->SVid)
    {
        if (pL2CVlanPortEntryA->SVid > pL2CVlanPortEntryB->SVid)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    return 0;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbCreateProviderEdgePort                        */
/*                                                                           */
/* Description        : This function is used to create Provider Edge Port   */
/*                      in L2IWF. This would be called by VLAN whenever the  */
/*                      provider edge port is created in VLAN.               */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfPbCreateProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId)
{
    tL2CVlanPortEntry  *pL2CVlanPortEntry = NULL;
    tL2PortInfo        *pL2PortEntry = NULL;
    UINT4               u4ContextId = 0;
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    CfaGetIfInfo (u4IfIndex, &IfInfo);

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pL2CVlanPortEntry =
        L2IwfPbGetPepEntry (pL2PortEntry->u2LocalPortId, SVlanId);

    if (pL2CVlanPortEntry != NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_SUCCESS;
    }

    if ((pL2CVlanPortEntry = (tL2CVlanPortEntry *)
         L2IWF_ALLOCATE_MEMBLOCK (L2IWF_PEPINFO_POOLID ())) == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    pL2CVlanPortEntry->u2Port = pL2PortEntry->u2LocalPortId;
    pL2CVlanPortEntry->SVid = SVlanId;
    pL2CVlanPortEntry->u1OperStatus = CFA_IF_DOWN;

    if (IfInfo.u1IfSubType == CFA_SUBTYPE_AC_INTERFACE)
    {
        pL2CVlanPortEntry->u1PortState = AST_PORT_STATE_FORWARDING;
    }
    else
    {
        pL2CVlanPortEntry->u1PortState = AST_PORT_STATE_DISCARDING;
    }

    pL2CVlanPortEntry->bOperEdge = OSIX_FALSE;

    if (RBTreeAdd (L2IWF_CVLAN_PORT_TBL (), pL2CVlanPortEntry) != RB_SUCCESS)
    {
        if (L2IWF_RELEASE_MEMBLOCK (L2IWF_PEPINFO_POOLID (),
                                    pL2CVlanPortEntry) == MEM_FAILURE)
        {
            L2IwfReleaseContext ();
            L2_UNLOCK ();
            return L2IWF_FAILURE;
        }
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();

#ifdef RSTP_WANTED
    AstCreateProviderEdgePort (u4IfIndex, SVlanId);
#endif

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbDeleteProviderEdgePort                        */
/*                                                                           */
/* Description        : This function is used to delete Provider Edge Port   */
/*                      in L2IWF. This would be called by STP                */
/*                      The Provider Edge Port deletion in invoked from the  */
/*                      VLAN module.                                         */
/*                      Vlan deletes the PEP from it's database & indicates  */
/*                      to STP module.                                       */
/*                      STP on deleting the PEP from it's database, gives    */
/*                      indication to L2IWF through this function to delete  */
/*                      PEP from L2IWF.                                      */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
L2IwfPbDeleteProviderEdgePort (UINT4 u4IfIndex, tVlanId SVlanId)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    u2LocalPortId = pL2PortEntry->u2LocalPortId;

    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();

        return L2IWF_FAILURE;
    }

    if (L2IwfPbUtilDeletePEPEntry (u2LocalPortId, SVlanId) != L2IWF_SUCCESS)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbPepDeleteIndication                           */
/*                                                                           */
/* Description        : This function is used to delete Provider Edge Port   */
/*                      in L2IWF. This would be called by VLAN whenever the  */
/*                      provider edge port is deleted from VLAN.             */
/*                      This function also indicated PEP deletion to ASTP.   */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      SVlanId    - Service Vlan Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS / L2IWF_FAILURE                        */
/*****************************************************************************/
INT4
L2IwfPbPepDeleteIndication (UINT4 u4IfIndex, tVlanId SVlanId)
{
    tCfaIfInfo          IfInfo;

    if (CfaGetIfInfo (u4IfIndex, &IfInfo) != CFA_SUCCESS)
    {
        return L2IWF_FAILURE;
    }

#ifdef RSTP_WANTED
    /* Indicate spanning tree about PEP deletion. */
    AstDeleteProviderEdgePort (u4IfIndex, SVlanId);
#endif

    /* Delete PEP from L2Iwf. */
    L2IwfPbDeleteProviderEdgePort (u4IfIndex, SVlanId);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbUpdatePepOperStatus                           */
/*                                                                           */
/* Description        : This function is used to delete Provider Edge Port   */
/*                      in L2IWF. This would be called by VLAN whenever the  */
/*                      provider edge port is deleted from VLAN.             */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      u1Status - Changed status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
L2IwfPbUpdatePepOperStatus (UINT4 u4IfIndex, tVlanId SVlanId,
                            UINT1 u1OperStatus)
{
    tL2CVlanPortEntry  *pL2CVlanPortEntry;
    tL2PortInfo        *pL2PortEntry;
    UINT4               u4ContextId;

    L2_LOCK ();
    pL2PortEntry = L2IwfGetIfIndexEntry (u4IfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return;
    }

    u4ContextId = L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry);

    if (L2IwfSelectContext (u4ContextId) != L2IWF_SUCCESS)
    {
        L2_UNLOCK ();
        return;
    }

    pL2CVlanPortEntry =
        L2IwfPbGetPepEntry (pL2PortEntry->u2LocalPortId, SVlanId);

    if (pL2CVlanPortEntry == NULL)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return;
    }

    pL2CVlanPortEntry->u1OperStatus = u1OperStatus;
    L2IwfReleaseContext ();
    L2_UNLOCK ();

    if ((AstIsMstStartedInContext (L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry))) ||
        (AstIsRstStartedInContext (L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry))))
    {
        /*Setting the default PEP port state as DISCARDING.*/
        L2IwfSetPbPortState (u4IfIndex, SVlanId, AST_PORT_STATE_DISCARDING);
    }
    else 
    {
        if (u1OperStatus == CFA_IF_UP)
        {
            L2IwfSetPbPortState (u4IfIndex, SVlanId, AST_PORT_STATE_FORWARDING);
        }
        else
        {
            L2IwfSetPbPortState (u4IfIndex, SVlanId, AST_PORT_STATE_DISCARDING);
        }
    }
#ifdef RSTP_WANTED
#ifdef L2RED_WANTED
    /* In standby node, don't give the pep oper status indication to
     * stp. Pep oper status indication will be synced by active stp
     * module to standby stp module.*/
    if (RmGetNodeState () == RM_STANDBY)
    {
        return;
    }
#endif
    AstPepOperStatusIndication (u4IfIndex, SVlanId, u1OperStatus);
#endif

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : L2IwfPbGetPepEntry                               */
/*                                                                           */
/*    Description         : This function returns the PEP port information   */
/*                          entry                                            */
/*                                                                           */
/*    Input(s)            : u2Port - CEP port number                         */
/*                          SVlanId - Service VLAN ID                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : Pointer to the tL2CVlanPortEntry.                */
/*                                                                           */
/*****************************************************************************/
tL2CVlanPortEntry  *
L2IwfPbGetPepEntry (UINT2 u2Port, tVlanId SVlanId)
{
    tL2CVlanPortEntry   L2CVlanPortEntry;
    tL2CVlanPortEntry  *pL2CVlanPortEntry = NULL;

    MEMSET (&L2CVlanPortEntry, 0, sizeof (tL2CVlanPortEntry));

    L2CVlanPortEntry.u2Port = u2Port;
    L2CVlanPortEntry.SVid = SVlanId;

    pL2CVlanPortEntry = RBTreeGet (L2IWF_CVLAN_PORT_TBL (), &L2CVlanPortEntry);

    return pL2CVlanPortEntry;
}

/*****************************************************************************/
/* Function Name      : L2IwfValidateBridgeMode                              */
/*                                                                           */
/* Description        : This function validates the Bridge mode based on     */
/*                      packages                                             */
/*                                                                           */
/* Input(s)           : u4ContextId  - Context ID                            */
/*                      u4BridgeMode - Bridge mode to be set in L2IWF        */
/*                      This takes one of the following values               */
/*                      CustomerBridge/ProviderEdgeBridge/ProviderCoreBridge */
/*                      ProviderBridge                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfValidateBridgeMode (UINT4 u4BridgeMode)
{
    if ((u4BridgeMode < L2IWF_CUSTOMER_BRIDGE_MODE) ||
        (u4BridgeMode > L2IWF_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetInstCepPortState                           */
/*                                                                           */
/* Description        : This function is called to configure CEP Port State  */
/*                      for all Instances                                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - CEP Port IfIndex                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/

INT4
L2IwfPbSetInstCepPortState (UINT4 u4CepIfIndex, UINT1 u1PortState)
{
    tL2PortInfo        *pL2PortEntry = NULL;
    UINT2               u2MstInst = 0;

    L2_LOCK ();

    pL2PortEntry = L2IwfGetIfIndexEntry (u4CepIfIndex);

    if (pL2PortEntry == NULL)
    {
        L2_UNLOCK ();
        return L2IWF_FAILURE;
    }
    if (AstIsPvrstStartedInContext (L2IWF_IFENTRY_CONTEXT_ID (pL2PortEntry)))
    {
        for (u2MstInst = 1; u2MstInst <= AST_MAX_PVRST_INSTANCES; u2MstInst++)
        {
            L2IWF_IFENTRY_PORT_STATE (pL2PortEntry, u2MstInst) = u1PortState;
        }
    }
    else
    {
        for (u2MstInst = 0; u2MstInst < AST_MAX_MST_INSTANCES; u2MstInst++)
        {
            L2IWF_IFENTRY_PORT_STATE (pL2PortEntry, u2MstInst) = u1PortState;
        }
    }

    L2_UNLOCK ();

    return L2IWF_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetDefBrgPortTypeInCfa                        */
/*                                                                           */
/* Description        : This function set the default bridge port type for   */
/*                      the given port based on the bridge mode. By using    */
/*                      the u1BrgModePortType argument we can force this     */
/*                      function to set the bridge port type as customerPort */
/*                      irrespective of the bridge mode.                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port IfIndex                             */
/*                      u4BridgeMode - Bridge Mode                           */
/*                      u1BrgModePortType - Whether to set the bridge port   */
/*                                          type based on bridge mode or     */
/*                                          not.                             */
/*                      u1IsCnpCheck - Whether to set the CNP Count or not   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbSetDefBrgPortTypeInCfa (UINT4 u4ContextId, UINT4 u4IfIndex,
                               UINT1 u1BrgModePortType, UINT1 u1IsCnpCheck)
{
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4BridgeMode;
    UINT4               u4PhyIfIndex;
    INT4                i4RetVal = L2IWF_SUCCESS;
    UINT1               u1PortType;

    L2_LOCK ();
    u4BridgeMode = L2IWF_GET_BRG_MODE_FROM_CTXTID (u4ContextId);
    L2_UNLOCK ();

    if ((u1BrgModePortType == L2IWF_TRUE) &&
        ((u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE) ||
         (u4BridgeMode == L2IWF_PROVIDER_EDGE_BRIDGE_MODE)))
    {
        /* For the SISP port, port type of its physical port has to 
         * be applied */

        if (SISP_IS_LOGICAL_PORT (u4IfIndex) == VCM_TRUE)
        {
            VcmSispGetPhysicalPortOfSispPort (u4IfIndex, &u4PhyIfIndex);

            L2IwfGetPbPortType (u4PhyIfIndex, &u1PortType);

            CfaIfInfo.u1BrgPortType = u1PortType;
        }
        else
        {
            CfaIfInfo.u1BrgPortType = L2IWF_PROVIDER_NETWORK_PORT;
        }
    }
    else if ((u1BrgModePortType == L2IWF_TRUE) &&
             (u4BridgeMode == L2IWF_PBB_ICOMPONENT_BRIDGE_MODE))
    {
        if (CfaIsVirtualInterface (u4IfIndex) == CFA_TRUE)
        {
            CfaIfInfo.u1BrgPortType = L2IWF_PROVIDER_INSTANCE_PORT;
            CfaSetIfType (u4IfIndex, CFA_PIP);
        }
        else if (CfaIsVipInterface (u4IfIndex) == CFA_TRUE)
        {
            CfaIfInfo.u1BrgPortType = L2IWF_VIRTUAL_INSTANCE_PORT;
        }
        else
        {
            CfaIfInfo.u1BrgPortType = L2IWF_CNP_STAGGED_PORT;
            if (u1IsCnpCheck == L2IWF_TRUE)
            {
                L2_LOCK ();
                if (L2IwfSelectContext (u4ContextId) == L2IWF_SUCCESS)
                {
                    ++L2IWF_CNP_COUNT ();
                    L2IwfReleaseContext ();
                }
                L2_UNLOCK ();
            }
        }
    }
    else if ((u1BrgModePortType == L2IWF_TRUE) &&
             (u4BridgeMode == L2IWF_PBB_BCOMPONENT_BRIDGE_MODE))
    {
        if (CfaIsVirtualInterface (u4IfIndex) == CFA_TRUE)
        {
            CfaIfInfo.u1BrgPortType = L2IWF_CUSTOMER_BACKBONE_PORT;
        }
        else
        {
            CfaIfInfo.u1BrgPortType = L2IWF_PROVIDER_NETWORK_PORT;
        }
    }
    else
    {
        if (CfaIsSbpInterface (u4IfIndex) == CFA_TRUE)
        {
            CfaIfInfo.u1BrgPortType = L2IWF_STATION_FACING_BRIDGE_PORT; 
        }
        else
        {
            CfaIfInfo.u1BrgPortType = L2IWF_CUSTOMER_BRIDGE_PORT;
        }
    }

    i4RetVal = CfaSetIfInfo (IF_BRG_PORT_TYPE, u4IfIndex, &CfaIfInfo);

    if (i4RetVal == CFA_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    /* 
     * Set the Pb Port type in L2Iwf too. 
     * 1. If the port is not yet created in L2Iwf, then the below call will 
     *    fail. But when the port is created in L2Iwf, it will get the brg 
     *    port type from Cfa. This case will happen when the brg port type is 
     *    changed.
     * 2. When the bridge mode is changed, Vlan sets the default brg mode for
     *    all ports in that bridge. That time the port will be present in L2Iwf
     *    and hence the below call will be useful. Note in this thread vlan 
     *    programs brg port type in Cfa as well as in L2Iwf.
     */

    L2IwfPbSetPortType (u4IfIndex, (INT4) CfaIfInfo.u1BrgPortType);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetDefPortType                                */
/*                                                                           */
/* Description        : This function sets the default port type for the     */
/*                      given port based on the bridge mode.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port IfIndex.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbSetDefPortType (UINT4 u4IfIndex)
{
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = L2IWF_SUCCESS;
    UINT2               u2LocalPort = 0;

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId, &u2LocalPort)
        == VCM_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    i4RetVal = L2IwfPbSetDefBrgPortTypeInCfa (u4ContextId, u4IfIndex,
                                              L2IWF_TRUE, L2IWF_FALSE);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbSetPcPortTypeToPhyPort                        */
/*                                                                           */
/* Description        : This function sets the port type of the port channel */
/*                      to the given port.                                   */
/*                                                                           */
/* Input(s)           : u4PcIfIndex - Port-channel IfIndex.                  */
/*                      u4PhyIfIndex - IfIndex of the port                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
L2IwfPbSetPcPortTypeToPhyPort (UINT4 u4PcIfIndex, UINT4 u4PhyIfIndex)
{
    tCfaIfInfo          CfaIfInfo;
    INT4                i4RetVal = CFA_SUCCESS;
    UINT1               u1PortType;

    if (L2IwfGetPbPortType (u4PcIfIndex, &u1PortType) == L2IWF_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    CfaIfInfo.u1BrgPortType = u1PortType;

    i4RetVal = CfaSetIfInfo (IF_BRG_PORT_TYPE, u4PhyIfIndex, &CfaIfInfo);

    if (i4RetVal == CFA_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    L2IwfPbSetPortType (u4PhyIfIndex, (INT4) CfaIfInfo.u1BrgPortType);
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfIsVlanElan                                      */
/*                                                                           */
/* Description        : This function is called to check whether the VLAN    */
/*                      service type is configured as ELAN                   */
/*                                                                           */
/* Input(s)           : u4ContextId - Virtual Switch Identifier              */
/*                      VlanId      - Vlan Identifier                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE / OSIX_FALSE                               */
/*****************************************************************************/

BOOL1
L2IwfIsVlanElan (UINT4 u4ContextId, tVlanId VlanId)
{
    if (u4ContextId >= L2IWF_MAX_CONTEXTS)
    {
        return OSIX_FALSE;
    }
    if (VlanId > VLAN_DEV_MAX_VLAN_ID || VlanId == 0)
    {
        return OSIX_FALSE;
    }

    L2_LOCK ();

    if (L2IwfSelectContext (u4ContextId) == L2IWF_FAILURE)
    {
        L2_UNLOCK ();
        return OSIX_FALSE;
    }

    if (L2IWF_VLAN_ACTIVE (VlanId) == OSIX_FALSE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return OSIX_FALSE;
    }

    if (L2IWF_VLAN_SERVICE_TYPE (VlanId) == VLAN_E_LINE)
    {
        L2IwfReleaseContext ();
        L2_UNLOCK ();
        return OSIX_FALSE;
    }

    L2IwfReleaseContext ();
    L2_UNLOCK ();
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function Name      : L2IwfHwPbPepCreate                                   */
/*                                                                           */
/* Description        : This function is used to indicate Ast about          */
/*                      provider edge port is created in Hw.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex  - Interface index.                        */
/*                      SVlanId    - Service vlan Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfHwPbPepCreate (UINT4 u4IfIndex, tVlanId SVlanId)
{
#ifdef RSTP_WANTED
    AstRedHwPepCreateNotify (u4IfIndex, SVlanId);
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (SVlanId);
#endif
    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2IwfPbUtilDeletePEPEntry                            */
/*                                                                           */
/* Description        : This function is used to delete the PEP entry        */
/*                      in L2IWF                                             */
/*                                                                           */
/* Input(s)           : u2LocalPort - Local Port Id                          */
/*                      SVlanId    - Service vlan Id                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/L2IWF_FAILURE                          */
/*****************************************************************************/
INT4
L2IwfPbUtilDeletePEPEntry (UINT2 u2LocalPortId, tVlanId SVlanId)
{
    tL2CVlanPortEntry  *pL2CVlanPortEntry = NULL;
    tL2CVlanPortEntry   TempL2CVlanPortEntry;

    MEMSET (&TempL2CVlanPortEntry, 0, sizeof (tL2CVlanPortEntry));

    TempL2CVlanPortEntry.u2Port = u2LocalPortId;
    TempL2CVlanPortEntry.SVid = SVlanId;

    pL2CVlanPortEntry = (tL2CVlanPortEntry *)
        RBTreeRem (L2IWF_CVLAN_PORT_TBL (), &TempL2CVlanPortEntry);

    if (pL2CVlanPortEntry == NULL)
    {
        return L2IWF_FAILURE;
    }

    if (L2IWF_RELEASE_MEMBLOCK (L2IWF_PEPINFO_POOLID (),
                                pL2CVlanPortEntry) == MEM_FAILURE)
    {
        return L2IWF_FAILURE;
    }

    return L2IWF_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : L2iwfValidatePortType                                */
/*                                                                           */
/* Description        : This function is used to validate the bridge port    */
/*                      type for the bridge mode                             */
/*                                                                           */
/* Input(s)           : u4BridgeMode  - The Bridge mode                      */
/*                      u1BrgPortType - the port type to be validated for    */
/*                                      for the givem bridge mode            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : L2IWF_TRUE/L2IWF_FALSE                               */
/*****************************************************************************/
UINT4
L2iwfValidatePortType (UINT4 u4BridgeMode, UINT1 u1BrgPortType)
{
    switch (u4BridgeMode)
    {
        case L2IWF_CUSTOMER_BRIDGE_MODE:
        case L2IWF_PROVIDER_BRIDGE_MODE:
            if (u1BrgPortType != L2IWF_CUSTOMER_BRIDGE_PORT)
            {
                return L2IWF_FALSE;
            }
            break;
        case L2IWF_PROVIDER_EDGE_BRIDGE_MODE:
        case L2IWF_PROVIDER_CORE_BRIDGE_MODE:
            if (u1BrgPortType == L2IWF_CUSTOMER_BRIDGE_PORT)
            {
                return L2IWF_FALSE;
            }
            else if ((u1BrgPortType == L2IWF_CUSTOMER_EDGE_PORT) &&
                     (u4BridgeMode == L2IWF_PROVIDER_CORE_BRIDGE_MODE))
            {
                return L2IWF_FALSE;
            }
            if ((u1BrgPortType == L2IWF_CNP_CTAGGED_PORT) ||
                (u1BrgPortType == L2IWF_VIRTUAL_INSTANCE_PORT) ||
                (u1BrgPortType == L2IWF_PROVIDER_INSTANCE_PORT) ||
                (u1BrgPortType == L2IWF_CUSTOMER_BACKBONE_PORT))
            {
                return L2IWF_FALSE;
            }
            break;
        case L2IWF_PBB_ICOMPONENT_BRIDGE_MODE:
        case L2IWF_PBB_BCOMPONENT_BRIDGE_MODE:
            if (!((u1BrgPortType == L2IWF_CNP_CTAGGED_PORT) ||
                  (u1BrgPortType == L2IWF_VIRTUAL_INSTANCE_PORT) ||
                  (u1BrgPortType == L2IWF_PROVIDER_INSTANCE_PORT) ||
                  (u1BrgPortType == L2IWF_CUSTOMER_BACKBONE_PORT)))
            {
                return L2IWF_FALSE;
            }
            break;
        default:
            return L2IWF_FALSE;
    }

    if (u1BrgPortType == L2IWF_INVALID_PROVIDER_PORT)
    {
        return L2IWF_FALSE;
    }

    return L2IWF_TRUE;
}
