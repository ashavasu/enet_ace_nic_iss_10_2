#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                     |
# |                                                                          |
# |   MAKE TOOL(S) USED      : None                                          |
# |                                                                          |
# |   TARGET ENVIRONMENT     : Any                                           | 
# |                                                                          |
# |   DATE                   : 10th October 2001                             |
# |                                                                          |
# |   DESCRIPTION            : It specifies the environvent variables for    |
# |                            compiling FutureTCP                           |
# +--------------------------------------------------------------------------+

# Top level make include files
# ----------------------------

include ../LR/make.h
include ../LR/make.rule

# Compilation switches
# --------------------

SLI_FINAL_COMPILATION_SWITCHES = ${GENERAL_COMPILATION_SWITCHES} \
                                 ${SYSTEM_COMPILATION_SWITCHES}

#### FS SLI IS INCLUDED FOR RIP6 WITH LNXIP FSIP6 COMBINATION #####
ifeq (${LNXIP4}, YES)
ifeq (${IP6RTR}, YES)
SLI_FINAL_COMPILATION_SWITCHES +=-DSLI_WANTED
SLI_FINAL_COMPILATION_SWITCHES +=-UBSDCOMP_SLI_WANTED
SLI_FINAL_COMPILATION_SWITCHES +=-DIS_SLI_WRAPPER_MODULE
endif
endif


# Directories
# -----------

TCP_BASE_DIR    = ${BASE_DIR}/tcp
TCP_INC_DIR     = ${TCP_BASE_DIR}
SLI_BASE_DIR    = ${BASE_DIR}/sli
SLI_INC_DIR     = ${SLI_BASE_DIR}
SLI_SRC_DIR     = ${SLI_BASE_DIR}
SLI_OBJ_DIR     = ${SLI_BASE_DIR}
SLI_LIB_DIR     = ${SLI_BASE_DIR}


# Include files
# -------------

SLI_INCLUDE_FILES = ${SLI_INC_DIR}/sligenp.h \
                    ${COMN_INCL_DIR}/fssocket.h \
                    ${SLI_INC_DIR}/sliport.h \
                    ${SLI_INC_DIR}/slitdfs.h \
                    ${SLI_INC_DIR}/slimacs.h
                
SLI_FINAL_INCLUDE_FILES = ${SLI_INCLUDE_FILES}


# Include directories
# -------------------

SLI_INCLUDE_DIRS = -I${SLI_INC_DIR} -I${TCP_INC_DIR}

SLI_FINAL_INCLUDE_DIRS = ${SLI_INCLUDE_DIRS} \
                         ${COMMON_INCLUDE_DIRS}

# Project dependencies
# --------------------

SLI_DEPENDENCIES = $(COMMON_DEPENDENCIES) \
                   $(SLI_FINAL_INCLUDE_FILES) \
                   $(SLI_BASE_DIR)/Makefile \
                   $(SLI_BASE_DIR)/make.h

# -----------------------------  END OF FILE  -------------------------------
