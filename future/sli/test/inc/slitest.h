/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: slitest.h,v 1.1.1.1 2011/09/12 10:07:00 siva Exp $
 **
 ** Description: SLI  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#ifndef _SLITEST_H_
#define _SLITEST_H_
#include "sliinc.h"
#include "tcpcli.h"

VOID SliExecuteUtCase(UINT4 u4FileNumber, UINT4 u4TestNumber);
VOID SliExecuteUtAll (VOID);
VOID SliExecuteUtFile (UINT4 u4File);

typedef struct
{
    UINT1     u1File;
    UINT1     u1Case;
}tTestCase;

#define FILE_COUNT     15
tTestCase gTestCase[] = {
    {1,29}, /* sligenp */
    {2,15}, /* file2 */
    {3,15}, /* file3 */
    {4,15}, /* file4 */
    {5,15}, /* file5 */
    {6,15}, /* file6 */
    {7,15}, /* file7 */
    {8,15}, /* file8 */
    {9,15}  /* file9 */
};

/* sligenp UT cases */
INT4 SliUtSligenp_1 (VOID);
INT4 SliUtSligenp_2 (VOID);
INT4 SliUtSligenp_3 (VOID);
INT4 SliUtSligenp_4 (VOID);
INT4 SliUtSligenp_5 (VOID);
INT4 SliUtSligenp_6 (VOID);
INT4 SliUtSligenp_7 (VOID);
INT4 SliUtSligenp_8 (VOID);
INT4 SliUtSligenp_9 (VOID);
INT4 SliUtSligenp_10 (VOID);
INT4 SliUtSligenp_11 (VOID);
INT4 SliUtSligenp_12 (VOID);
INT4 SliUtSligenp_13 (VOID);
INT4 SliUtSligenp_14 (VOID);
INT4 SliUtSligenp_15 (VOID);
INT4 SliUtSligenp_16 (VOID);
INT4 SliUtSligenp_17 (VOID);
INT4 SliUtSligenp_18 (VOID);
INT4 SliUtSligenp_19 (VOID);
INT4 SliUtSligenp_20 (VOID);
INT4 SliUtSligenp_21 (VOID);
INT4 SliUtSligenp_22 (VOID);
INT4 SliUtSligenp_23 (VOID);
INT4 SliUtSligenp_24 (VOID);
INT4 SliUtSligenp_25 (VOID);
INT4 SliUtSligenp_26 (VOID);
INT4 SliUtSligenp_27 (VOID);
INT4 SliUtSligenp_28 (VOID);
INT4 SliUtSligenp_29 (VOID);

/* file2 UT cases */
INT4 SliUtFile2_1 (VOID);
INT4 SliUtFile2_2 (VOID);
INT4 SliUtFile2_3 (VOID);
INT4 SliUtFile2_4 (VOID);
INT4 SliUtFile2_5 (VOID);
INT4 SliUtFile2_6 (VOID);
INT4 SliUtFile2_7 (VOID);
INT4 SliUtFile2_8 (VOID);
INT4 SliUtFile2_9 (VOID);
INT4 SliUtFile2_10 (VOID);
INT4 SliUtFile2_11 (VOID);
INT4 SliUtFile2_12 (VOID);
INT4 SliUtFile2_13 (VOID);
INT4 SliUtFile2_14 (VOID);
INT4 SliUtFile2_15 (VOID);

/* file3 UT cases */
INT4 SliUtFile3_1 (VOID);
INT4 SliUtFile3_2 (VOID);
INT4 SliUtFile3_3 (VOID);
INT4 SliUtFile3_4 (VOID);
INT4 SliUtFile3_5 (VOID);
INT4 SliUtFile3_6 (VOID);
INT4 SliUtFile3_7 (VOID);
INT4 SliUtFile3_8 (VOID);
INT4 SliUtFile3_9 (VOID);
INT4 SliUtFile3_10 (VOID);
INT4 SliUtFile3_11 (VOID);
INT4 SliUtFile3_12 (VOID);
INT4 SliUtFile3_13 (VOID);
INT4 SliUtFile3_14 (VOID);
INT4 SliUtFile3_15 (VOID);

/* file4 UT cases */
INT4 SliUtFile4_1 (VOID);
INT4 SliUtFile4_2 (VOID);
INT4 SliUtFile4_3 (VOID);
INT4 SliUtFile4_4 (VOID);
INT4 SliUtFile4_5 (VOID);
INT4 SliUtFile4_6 (VOID);
INT4 SliUtFile4_7 (VOID);
INT4 SliUtFile4_8 (VOID);
INT4 SliUtFile4_9 (VOID);
INT4 SliUtFile4_10 (VOID);
INT4 SliUtFile4_11 (VOID);
INT4 SliUtFile4_12 (VOID);
INT4 SliUtFile4_13 (VOID);
INT4 SliUtFile4_14 (VOID);
INT4 SliUtFile4_15 (VOID);

/* file5 UT cases */
INT4 SliUtFile5_1 (VOID);
INT4 SliUtFile5_2 (VOID);
INT4 SliUtFile5_3 (VOID);
INT4 SliUtFile5_4 (VOID);
INT4 SliUtFile5_5 (VOID);
INT4 SliUtFile5_6 (VOID);
INT4 SliUtFile5_7 (VOID);
INT4 SliUtFile5_8 (VOID);
INT4 SliUtFile5_9 (VOID);
INT4 SliUtFile5_10 (VOID);
INT4 SliUtFile5_11 (VOID);
INT4 SliUtFile5_12 (VOID);
INT4 SliUtFile5_13 (VOID);
INT4 SliUtFile5_14 (VOID);
INT4 SliUtFile5_15 (VOID);

/* file6 UT cases */
INT4 SliUtFile6_1 (VOID);
INT4 SliUtFile6_2 (VOID);
INT4 SliUtFile6_3 (VOID);
INT4 SliUtFile6_4 (VOID);
INT4 SliUtFile6_5 (VOID);
INT4 SliUtFile6_6 (VOID);
INT4 SliUtFile6_7 (VOID);
INT4 SliUtFile6_8 (VOID);
INT4 SliUtFile6_9 (VOID);
INT4 SliUtFile6_10 (VOID);
INT4 SliUtFile6_11 (VOID);
INT4 SliUtFile6_12 (VOID);
INT4 SliUtFile6_13 (VOID);
INT4 SliUtFile6_14 (VOID);
INT4 SliUtFile6_15 (VOID);

/* file7 UT cases */
INT4 SliUtFile7_1 (VOID);
INT4 SliUtFile7_2 (VOID);
INT4 SliUtFile7_3 (VOID);
INT4 SliUtFile7_4 (VOID);
INT4 SliUtFile7_5 (VOID);
INT4 SliUtFile7_6 (VOID);
INT4 SliUtFile7_7 (VOID);
INT4 SliUtFile7_8 (VOID);
INT4 SliUtFile7_9 (VOID);
INT4 SliUtFile7_10 (VOID);
INT4 SliUtFile7_11 (VOID);
INT4 SliUtFile7_12 (VOID);
INT4 SliUtFile7_13 (VOID);
INT4 SliUtFile7_14 (VOID);
INT4 SliUtFile7_15 (VOID);

/* file8 UT cases */
INT4 SliUtFile8_1 (VOID);
INT4 SliUtFile8_2 (VOID);
INT4 SliUtFile8_3 (VOID);
INT4 SliUtFile8_4 (VOID);
INT4 SliUtFile8_5 (VOID);
INT4 SliUtFile8_6 (VOID);
INT4 SliUtFile8_7 (VOID);
INT4 SliUtFile8_8 (VOID);
INT4 SliUtFile8_9 (VOID);
INT4 SliUtFile8_10 (VOID);
INT4 SliUtFile8_11 (VOID);
INT4 SliUtFile8_12 (VOID);
INT4 SliUtFile8_13 (VOID);
INT4 SliUtFile8_14 (VOID);
INT4 SliUtFile8_15 (VOID);

/* file9 UT cases */
INT4 SliUtFile9_1 (VOID);
INT4 SliUtFile9_2 (VOID);
INT4 SliUtFile9_3 (VOID);
INT4 SliUtFile9_4 (VOID);
INT4 SliUtFile9_5 (VOID);
INT4 SliUtFile9_6 (VOID);
INT4 SliUtFile9_7 (VOID);
INT4 SliUtFile9_8 (VOID);
INT4 SliUtFile9_9 (VOID);
INT4 SliUtFile9_10 (VOID);
INT4 SliUtFile9_11 (VOID);
INT4 SliUtFile9_12 (VOID);
INT4 SliUtFile9_13 (VOID);
INT4 SliUtFile9_14 (VOID);
INT4 SliUtFile9_15 (VOID);

extern tMemPoolId   gSliMd5NodePoolId;

#endif /* _SLITEST_H_ */ 
