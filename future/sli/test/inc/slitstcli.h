
/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: slitstcli.h,v 1.1.1.1 2011/09/12 10:07:00 siva Exp $
 **
 ** Description: SLI  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"

INT4 cli_process_sli_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...);

INT4
SliUt(INT4 );


