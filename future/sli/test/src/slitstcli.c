/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: slitstcli.c,v 1.1.1.1 2011/09/12 10:07:00 siva Exp $
 **
 ** Description: SLI  UT Test cases cli file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"
#include "sliinc.h"
#include "tcpcli.h"
#include "slitstcli.h"

#define MAX_ARGS 3

extern VOID SliExecuteUtCase (UINT4 u4FileNumber, UINT4 u4TestNumber);
extern VOID SliExecuteUtAll (VOID);
extern VOID SliExecuteUtFile (UINT4 u4File);

/*  Function is called from slitstcmd.def file */

INT4
cli_process_sli_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[MAX_ARGS];
    INT4                i4Inst = 0;
    INT1                argno = 0;
    /*CliRegisterLock (CliHandle, SliMainTaskLock, SliMainTaskUnLock);*/
   /*SLI_LOCK;*/

    va_start (ap, u4Command);
    UNUSED_PARAM (CliHandle);
    i4Inst = va_arg (ap, INT4);

    /* Walk through the arguments and store in args array.
     */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);

    switch (u4Command)
    {
        case SLI_UT_TEST:
            if (args[0] == NULL)
            {
                /* specified case on specified file */
                if (args[2] != NULL)
                {
                    SliExecuteUtCase (*(UINT4 *) (args[1]), *(UINT4 *) (args[2]));
                }
                /* all cases on specified file */
                else
                {
                    SliExecuteUtFile (*(UINT4 *) (args[1]));
                }
            }
            else
            {
            /* all cases in all the files */
            SliExecuteUtAll ();
            }
        break;
    }
    /*CliUnRegisterLock (CliHandle);*/
    /*SLI_UNLOCK;*/
    return CLI_SUCCESS;
}
