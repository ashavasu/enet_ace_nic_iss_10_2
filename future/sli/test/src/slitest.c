/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: slitest.c,v 1.1.1.1 2011/09/12 10:07:00 siva Exp $
 **
 ** Description: SLI  UT Test cases file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "slitest.h"
#include "iss.h"
#include "params.h"

VOID
SliExecuteUtCase (UINT4 u4FileNumber, UINT4 u4TestNumber)
{
    INT4                i4RetVal = -1;

    switch (u4FileNumber)
    {
            /* sligenp */
        case 1:
            switch (u4TestNumber)
            {
                case 1:
                   i4RetVal = SliUtSligenp_1 ();
                   break;
                case 2:
                   i4RetVal = SliUtSligenp_2 ();
                   break;
                case 3:
                   i4RetVal = SliUtSligenp_3 ();
                   break;
                case 4:
                   i4RetVal = SliUtSligenp_4 ();
                   break;
                case 5:
                   i4RetVal = SliUtSligenp_5 ();
                   break;
                case 6:
                   i4RetVal = SliUtSligenp_6 ();
                   break;
                case 7:
                   i4RetVal = SliUtSligenp_7 ();
                   break;
                case 8:
                   i4RetVal = SliUtSligenp_8 ();
                   break;
                case 9:
                   i4RetVal = SliUtSligenp_9 ();
                   break;
                case 10:
                   i4RetVal = SliUtSligenp_10 ();
                   break;
                case 11:
                   i4RetVal = SliUtSligenp_11 ();
                   break;
                case 12:
                   i4RetVal = SliUtSligenp_12 ();
                   break;
                case 13:
                   i4RetVal = SliUtSligenp_13 ();
                   break;
                case 14:
                   i4RetVal = SliUtSligenp_14 ();
                   break;
                case 15:
                   i4RetVal = SliUtSligenp_15 ();
                   break;
                case 16:
                   i4RetVal = SliUtSligenp_16 ();
                   break;
                case 17:
                   i4RetVal = SliUtSligenp_17 ();
                   break;
                case 18:
                   i4RetVal = SliUtSligenp_18 ();
                   break;
                case 19:
                   i4RetVal = SliUtSligenp_19 ();
                   break;
                case 20:
                   i4RetVal = SliUtSligenp_20 ();
                   break;
                case 21:
                   i4RetVal = SliUtSligenp_21 ();
                   break;
                case 22:
                   i4RetVal = SliUtSligenp_22 ();
                   break;
                case 23:
                   i4RetVal = SliUtSligenp_23 ();
                   break;
                case 24:
                   i4RetVal = SliUtSligenp_24 ();
                   break;
                case 25:
                   i4RetVal = SliUtSligenp_25 ();
                   break;
                case 26:
                   i4RetVal = SliUtSligenp_26 ();
                   break;
                case 27:
                   i4RetVal = SliUtSligenp_27 ();
                   break;
                case 28:
                   i4RetVal = SliUtSligenp_28 ();
                   break;
                case 29:
                   i4RetVal = SliUtSligenp_29 ();
                   break;
                default:
                    printf ("Invalid Test for file sligenp.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of sligenp.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of sligenp.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* file2 */
        case 2:
            switch (u4TestNumber)
            {
                case 1:
                   i4RetVal = SliUtFile2_1 ();
                   break;
                case 2:
                   i4RetVal = SliUtFile2_2 ();
                   break;
                case 3:
                   i4RetVal = SliUtFile2_3 ();
                   break;
                case 4:
                   i4RetVal = SliUtFile2_4 ();
                   break;
                case 5:
                   i4RetVal = SliUtFile2_5 ();
                   break;
                case 6:
                   i4RetVal = SliUtFile2_6 ();
                   break;
                case 7:
                   i4RetVal = SliUtFile2_7 ();
                   break;
                case 8:
                   i4RetVal = SliUtFile2_8 ();
                   break;
                case 9:
                   i4RetVal = SliUtFile2_9 ();
                   break;
                case 10:
                   i4RetVal = SliUtFile2_10 ();
                   break;
                case 11:
                   i4RetVal = SliUtFile2_11 ();
                   break;
                case 12:
                   i4RetVal = SliUtFile2_12 ();
                   break;
                case 13:
                   i4RetVal = SliUtFile2_13 ();
                   break;
                case 14:
                   i4RetVal = SliUtFile2_14 ();
                   break;
                case 15:
                   i4RetVal = SliUtFile2_15 ();
                   break;
                default:
                    printf ("Invalid Test for file slifile2.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of slifile2.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of slifile2.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* file3 */
        case 3:
            switch (u4TestNumber)
            {
                case 1:
                   i4RetVal = SliUtFile3_1 ();
                   break;
                case 2:
                   i4RetVal = SliUtFile3_2 ();
                   break;
                case 3:
                   i4RetVal = SliUtFile3_3 ();
                   break;
                case 4:
                   i4RetVal = SliUtFile3_4 ();
                   break;
                case 5:
                   i4RetVal = SliUtFile3_5 ();
                   break;
                case 6:
                   i4RetVal = SliUtFile3_6 ();
                   break;
                case 7:
                   i4RetVal = SliUtFile3_7 ();
                   break;
                case 8:
                   i4RetVal = SliUtFile3_8 ();
                   break;
                case 9:
                   i4RetVal = SliUtFile3_9 ();
                   break;
                case 10:
                   i4RetVal = SliUtFile3_10 ();
                   break;
                case 11:
                   i4RetVal = SliUtFile3_11 ();
                   break;
                case 12:
                   i4RetVal = SliUtFile3_12 ();
                   break;
                case 13:
                   i4RetVal = SliUtFile3_13 ();
                   break;
                case 14:
                   i4RetVal = SliUtFile3_14 ();
                   break;
                case 15:
                   i4RetVal = SliUtFile3_15 ();
                   break;
                default:
                    printf ("Invalid Test for file slifile3.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of slifile3.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of slifile3.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* file4 */
        case 4:
            switch (u4TestNumber)
            {
                case 1:
                   i4RetVal = SliUtFile4_1 ();
                   break;
                case 2:
                   i4RetVal = SliUtFile4_2 ();
                   break;
                case 3:
                   i4RetVal = SliUtFile4_3 ();
                   break;
                case 4:
                   i4RetVal = SliUtFile4_4 ();
                   break;
                case 5:
                   i4RetVal = SliUtFile4_5 ();
                   break;
                case 6:
                   i4RetVal = SliUtFile4_6 ();
                   break;
                case 7:
                   i4RetVal = SliUtFile4_7 ();
                   break;
                case 8:
                   i4RetVal = SliUtFile4_8 ();
                   break;
                case 9:
                   i4RetVal = SliUtFile4_9 ();
                   break;
                case 10:
                   i4RetVal = SliUtFile4_10 ();
                   break;
                case 11:
                   i4RetVal = SliUtFile4_11 ();
                   break;
                case 12:
                   i4RetVal = SliUtFile4_12 ();
                   break;
                case 13:
                   i4RetVal = SliUtFile4_13 ();
                   break;
                case 14:
                   i4RetVal = SliUtFile4_14 ();
                   break;
                case 15:
                   i4RetVal = SliUtFile4_15 ();
                   break;
                default:
                    printf ("Invalid Test for file slifile4.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of slifile4.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of slifile4.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* file5 */
        case 5:
            switch (u4TestNumber)
            {
                case 1:
                   i4RetVal = SliUtFile6_1 ();
                   break;
                case 2:
                   i4RetVal = SliUtFile6_2 ();
                   break;
                case 3:
                   i4RetVal = SliUtFile6_3 ();
                   break;
                case 4:
                   i4RetVal = SliUtFile6_4 ();
                   break;
                case 5:
                   i4RetVal = SliUtFile6_5 ();
                   break;
                case 6:
                   i4RetVal = SliUtFile6_6 ();
                   break;
                case 7:
                   i4RetVal = SliUtFile6_7 ();
                   break;
                case 8:
                   i4RetVal = SliUtFile6_8 ();
                   break;
                case 9:
                   i4RetVal = SliUtFile6_9 ();
                   break;
                case 10:
                   i4RetVal = SliUtFile6_10 ();
                   break;
                case 11:
                   i4RetVal = SliUtFile6_11 ();
                   break;
                case 12:
                   i4RetVal = SliUtFile6_12 ();
                   break;
                case 13:
                   i4RetVal = SliUtFile6_13 ();
                   break;
                case 14:
                   i4RetVal = SliUtFile6_14 ();
                   break;
                case 15:
                   i4RetVal = SliUtFile6_15 ();
                   break;
                default:
                    printf ("Invalid Test for file slifile6.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of slifile6.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of slifile6.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* file7 */
        case 7:
            switch (u4TestNumber)
            {
                case 1:
                   i4RetVal = SliUtFile7_1 ();
                   break;
                case 2:
                   i4RetVal = SliUtFile7_2 ();
                   break;
                case 3:
                   i4RetVal = SliUtFile7_3 ();
                   break;
                case 4:
                   i4RetVal = SliUtFile7_4 ();
                   break;
                case 5:
                   i4RetVal = SliUtFile7_5 ();
                   break;
                case 6:
                   i4RetVal = SliUtFile7_6 ();
                   break;
                case 7:
                   i4RetVal = SliUtFile7_7 ();
                   break;
                case 8:
                   i4RetVal = SliUtFile7_8 ();
                   break;
                case 9:
                   i4RetVal = SliUtFile7_9 ();
                   break;
                case 10:
                   i4RetVal = SliUtFile7_10 ();
                   break;
                case 11:
                   i4RetVal = SliUtFile7_11 ();
                   break;
                case 12:
                   i4RetVal = SliUtFile7_12 ();
                   break;
                case 13:
                   i4RetVal = SliUtFile7_13 ();
                   break;
                case 14:
                   i4RetVal = SliUtFile7_14 ();
                   break;
                case 15:
                   i4RetVal = SliUtFile7_15 ();
                   break;
                default:
                    printf ("Invalid Test for file slifile7.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of slifile7.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of slifile7.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* file8 */
        case 8:
            switch (u4TestNumber)
            {
                case 1:
                   i4RetVal = SliUtFile8_1 ();
                   break;
                case 2:
                   i4RetVal = SliUtFile8_2 ();
                   break;
                case 3:
                   i4RetVal = SliUtFile8_3 ();
                   break;
                case 4:
                   i4RetVal = SliUtFile8_4 ();
                   break;
                case 5:
                   i4RetVal = SliUtFile8_5 ();
                   break;
                case 6:
                   i4RetVal = SliUtFile8_6 ();
                   break;
                case 7:
                   i4RetVal = SliUtFile8_7 ();
                   break;
                case 8:
                   i4RetVal = SliUtFile8_8 ();
                   break;
                case 9:
                   i4RetVal = SliUtFile8_9 ();
                   break;
                case 10:
                   i4RetVal = SliUtFile8_10 ();
                   break;
                case 11:
                   i4RetVal = SliUtFile8_11 ();
                   break;
                case 12:
                   i4RetVal = SliUtFile8_12 ();
                   break;
                case 13:
                   i4RetVal = SliUtFile8_13 ();
                   break;
                case 14:
                   i4RetVal = SliUtFile8_14 ();
                   break;
                case 15:
                   i4RetVal = SliUtFile8_15 ();
                   break;
                default:
                    printf ("Invalid Test for file slifile8.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of slifile8.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of slifile8.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* file9 */
        case 9:
            switch (u4TestNumber)
            {
                case 1:
                   i4RetVal = SliUtFile9_1 ();
                   break;
                case 2:
                   i4RetVal = SliUtFile9_2 ();
                   break;
                case 3:
                   i4RetVal = SliUtFile9_3 ();
                   break;
                case 4:
                   i4RetVal = SliUtFile9_4 ();
                   break;
                case 5:
                   i4RetVal = SliUtFile9_5 ();
                   break;
                case 6:
                   i4RetVal = SliUtFile9_6 ();
                   break;
                case 7:
                   i4RetVal = SliUtFile9_7 ();
                   break;
                case 8:
                   i4RetVal = SliUtFile9_8 ();
                   break;
                case 9:
                   i4RetVal = SliUtFile9_9 ();
                   break;
                case 10:
                   i4RetVal = SliUtFile9_10 ();
                   break;
                case 11:
                   i4RetVal = SliUtFile9_11 ();
                   break;
                case 12:
                   i4RetVal = SliUtFile9_12 ();
                   break;
                case 13:
                   i4RetVal = SliUtFile9_13 ();
                   break;
                case 14:
                   i4RetVal = SliUtFile9_14 ();
                   break;
                case 15:
                   i4RetVal = SliUtFile9_15 ();
                   break;
                default:
                    printf ("Invalid Test for file slifile9.c \r\n");
                    return;
            }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of slifile9.c is Passed \r\n",
                        u4TestNumber);
            }
            else if (i4RetVal == OSIX_FAILURE)
            {
                printf ("Unit Test Case: %d of slifile9.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
    }
    return;
}

VOID
SliExecuteUtAll (VOID)
{
    UINT1               u1File = 0;

    for (u1File = 1; u1File <= FILE_COUNT; u1File++)
    {
        SliExecuteUtFile (u1File);
    }
}

VOID
SliExecuteUtFile (UINT4 u4File)
{
    UINT1               u1Case = 0;

    for (u1Case = 1; u1Case <= gTestCase[u4File - 1].u1Case; u1Case++)
    {
        SliExecuteUtCase (u4File, u1Case);
    }
}

/* sligenp UT cases */
/* SliValidateMd5Option - For valid V4 address
 * and password of length <80, function must return success*/
INT4 
SliUtSligenp_1 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in  v4PeerAddr;
    CHR1   *password = "qwertyuiopasdfghjkl[];',.12345678900:0:0";

    v4PeerAddr.sin_family = AF_INET;
    v4PeerAddr.sin_addr.s_addr = INET_ADDR ("12.0.0.1");
    md5sig.tcpm_keylen = STRLEN (password);
    MEMCPY (&md5sig.tcpm_key, password, md5sig.tcpm_keylen);

    MEMCPY (&md5sig.tcpm_addr,
            &v4PeerAddr, sizeof(struct sockaddr_in));

    if (SLI_SUCCESS != SliValidateMd5Option (&md5sig))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS; 
}

/* SliValidateMd5Option - For valid V6 address
 * and password of length =80, function must return success*/
INT4 
SliUtSligenp_2 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in6 v6PeerAddr;
    CHR1    *pIp6Addr = "5555::7789";
    CHR1    *password = "qwertyuiopasdfghjkl[];',.aqazwsxedcrfvtgbyhnujmikolpqwertyui1234567890@@@@1QAZER";

    v6PeerAddr.sin6_family = AF_INET6;
    INET_ATON6 ((UINT1 *) pIp6Addr, v6PeerAddr.sin6_addr.s6_addr);

    md5sig.tcpm_keylen = STRLEN (password);
    printf ("\n[SliValidateMd5Option] UT case 2--Password length before= %d\n", md5sig.tcpm_keylen);
    MEMCPY (&md5sig.tcpm_key, password, md5sig.tcpm_keylen);

    MEMCPY (&md5sig.tcpm_addr,
            &v6PeerAddr, sizeof(struct sockaddr_in6));

    printf ("\n[SliValidateMd5Option] UT case 2--Password length after= %d\n", md5sig.tcpm_keylen);

    if (SLI_SUCCESS != SliValidateMd5Option (&md5sig))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS; 
}

/* SliValidateMd5Option - For password of length > 80, 
 * function must return failure*/
INT4 
SliUtSligenp_3 (VOID)
{
    struct tcp_md5sig   md5sig;
    CHR1    *password = "qwertyuiopasdfghjkl[];',.aqazwsxedcrfvtgbyhnujmikolpqwertyui1234567890@@@@1QAZER1234";

    md5sig.tcpm_keylen = STRLEN (password);
    printf ("\n[SliValidateMd5Option] UT case 3--Password length before= %d\n", md5sig.tcpm_keylen);
    MEMCPY (&md5sig.tcpm_key, password, md5sig.tcpm_keylen);

    printf ("\n[SliValidateMd5Option] UT case 3--Password length after = %d\n", md5sig.tcpm_keylen);
    
    if (SLI_FAILURE != SliValidateMd5Option (&md5sig))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS; 
}

/* SliValidateMd5Option - If unsupported address family 
 * given (other than af_inet/af_inet6), function must return failure*/
INT4 
SliUtSligenp_4 (VOID)
{
    struct tcp_md5sig   md5sig;

    MEMSET (&md5sig, 0, sizeof (md5sig));
    md5sig.tcpm_addr.ss_family = AF_UNSPEC;

    if (SLI_FAILURE != SliValidateMd5Option (&md5sig))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS; 
}

/* SliValidateMd5Option - If a multicast V4 address given,
 * function must return failure*/
INT4 
SliUtSligenp_5 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in  v4PeerAddr;

    MEMSET (&md5sig, 0, sizeof (md5sig));
    v4PeerAddr.sin_family = AF_INET;
    v4PeerAddr.sin_addr.s_addr = INET_ADDR ("224.0.0.1");

    MEMCPY (&md5sig.tcpm_addr,
            &v4PeerAddr, sizeof(struct sockaddr_in));
    printf ("\n SliUtSligenp_5 called");
    if (SLI_FAILURE != SliValidateMd5Option (&md5sig))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS; 
}

/* SliValidateMd5Option - If V4 address is unspecified 
 * function must return failure*/
INT4 
SliUtSligenp_6 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in  v4PeerAddr;

    MEMSET (&md5sig, 0, sizeof (md5sig));
    MEMSET (&v4PeerAddr, 0, sizeof (v4PeerAddr));
    v4PeerAddr.sin_family = AF_INET;

    MEMCPY (&md5sig.tcpm_addr,
            &v4PeerAddr, sizeof(struct sockaddr_in));
    printf ("\n SliUtSligenp_6 called");
    if (SLI_FAILURE != SliValidateMd5Option (&md5sig))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS; 
}

/* SliValidateMd5Option - If V4 address given is broadcast, 
 * function must return failure*/
INT4 
SliUtSligenp_7 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in  v4PeerAddr;

    MEMSET (&md5sig, 0, sizeof (md5sig));
    v4PeerAddr.sin_family = AF_INET;
    v4PeerAddr.sin_addr.s_addr = INET_ADDR ("10.255.255.255");

    MEMCPY (&md5sig.tcpm_addr,
            &v4PeerAddr, sizeof(struct sockaddr_in));
    printf ("\n SliUtSligenp_7 called");
    if (SLI_FAILURE != SliValidateMd5Option (&md5sig))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS; 
}

/* SliValidateMd5Option - If V6 address given is multicast, 
 * function must return failure*/
INT4 
SliUtSligenp_8 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in6 v6PeerAddr;
    CHR1    *pIp6Addr = "ff01::5";

    MEMSET (&md5sig, 0, sizeof (md5sig));
    v6PeerAddr.sin6_family = AF_INET6;
    INET_ATON6 ((UINT1 *) pIp6Addr, v6PeerAddr.sin6_addr.s6_addr);

    MEMCPY (&md5sig.tcpm_addr,
            &v6PeerAddr, sizeof(struct sockaddr_in6));
    printf ("\n SliUtSligenp_8 called");
    if (SLI_FAILURE != SliValidateMd5Option (&md5sig))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS; 
}

/* SliValidateMd5Option - If V6 address given is unspecified, 
 * function must return failure*/
INT4 
SliUtSligenp_9 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in6 v6PeerAddr;

    MEMSET (&md5sig, 0, sizeof (md5sig));
    MEMSET (&v6PeerAddr, 0, sizeof (v6PeerAddr));

    v6PeerAddr.sin6_family = AF_INET6;

    MEMCPY (&md5sig.tcpm_addr,
            &v6PeerAddr, sizeof(struct sockaddr_in6));
    printf ("\n SliUtSligenp_9 called");
    if (SLI_FAILURE != SliValidateMd5Option (&md5sig))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS; 
}

/* SliGetMd5SA - When MD5 list contains node for V4 address,
 * function must return its address*/
INT4 
SliUtSligenp_10 (VOID)
{
    tIpAddr IpAddr;
    tIpAddr IpAddrTmp;
    tSliMd5SA   *pMd5SA = NULL;
    tSdtEntry   *pSdtSock = NULL;
    CHR1    *password1 = "secret123";
    CHR1    *password2 = "QAZWSXqazwsx123";
    CHR1    *password3 = "test****";
    INT4    i4Sock =0;
    UINT1   au1Addr[16];

    MEMSET (au1Addr, 0, 16);
    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliGetMd5SA]Ut case 1-- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    /* Add node for IP 12.0.0.1 */
    MEMSET (&IpAddr, 0, sizeof(IpAddr));
    V4_MAPPED_ADDR_COPY (&IpAddr, INET_ADDR ("12.0.0.1"));

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password1, 
                STRLEN (password1));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password1);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddr);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliGetMd5SA]Ut case 1-- md5 node allocation failed");
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    /* Add node for IP 172.30.4.92 */
    MEMSET (&IpAddrTmp, 0, sizeof(IpAddrTmp));
    V4_MAPPED_ADDR_COPY (&IpAddrTmp, INET_ADDR ("172.30.4.92"));

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password2, 
                STRLEN (password2));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password2);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddrTmp);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliGetMd5SA]Ut case 1--- md5 node allocation failed");
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    /* Add node for V6 IP 5557::2222 */
    MEMSET (&IpAddrTmp, 0, sizeof(IpAddrTmp));
    INET_ATON6 ("5557::2222", au1Addr);
    V6_ADDR_COPY (&IpAddrTmp, (tIpAddr *) au1Addr);

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password3, 
                STRLEN (password3));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password3);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddrTmp);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliGetMd5SA]Ut case 1-- md5 node allocation failed");
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    /* Function must return node added for 12.0.0.1 */
    if ((pMd5SA = SliGetMd5SA (i4Sock, IpAddr)) == NULL )
    {
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }
    else if ((V6_ADDR_CMP (&pMd5SA->PeerIp, &IpAddr)) != 0)
    {
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }
    SliClose (i4Sock);
    return OSIX_SUCCESS;
}

/* SliGetMd5SA - When MD5 list contains node for V6 address,
 * function must return its address*/
INT4 
SliUtSligenp_11 (VOID)
{
    tIpAddr IpAddr;
    tIpAddr IpAddrTmp;
    tSliMd5SA   *pMd5SA = NULL;
    tSdtEntry   *pSdtSock = NULL;
    CHR1    *password1 = "secret123";
    CHR1    *password2 = "QAZWSXqazwsx123";
    CHR1    *password3 = "test****";
    INT4    i4Sock;
    UINT1   au1Addr[16];

    MEMSET (au1Addr, 0, 16);
    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n [SliGetMd5SA]Ut case 2-- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    /* Add node for IP 12.0.0.1 */
    MEMSET (&IpAddrTmp, 0, sizeof (IpAddrTmp));
    V4_MAPPED_ADDR_COPY (&IpAddrTmp, INET_ADDR ("12.0.0.1"));

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password1, 
                STRLEN (password1));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password1);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddrTmp);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n [SliGetMd5SA]Ut case 2-- md5 node allocation failed");
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    /* Add node for IP 172.30.4.92 */
    MEMSET (&IpAddrTmp, 0, sizeof (IpAddrTmp));
    V4_MAPPED_ADDR_COPY (&IpAddrTmp, INET_ADDR ("172.30.4.92"));

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password2, 
                STRLEN (password2));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password2);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddrTmp);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliGetMd5SA]Ut case 2-- md5 node allocation failed");
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    /* Add node for V6 IP 5557::2222 */
    MEMSET (&IpAddr, 0, sizeof (IpAddr));
    INET_ATON6 ("5557::2222", au1Addr);
    V6_ADDR_COPY (&IpAddr, (tIpAddr *) au1Addr);

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password3, 
                STRLEN (password3));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password3);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddr);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\ni[SliGetMd5SA]Ut case 2- md5 node allocation failed");
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    /* Function must return node added for V6 address - 5557::2222*/
    if ((pMd5SA = SliGetMd5SA (i4Sock, IpAddr)) == NULL )
    {
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }
    else if ((V6_ADDR_CMP (&pMd5SA->PeerIp, &IpAddr)) != 0)
    {
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }
    SliClose (i4Sock);
    return OSIX_SUCCESS;
}

/* SliGetMd5SA - When MD5 list does not contain node for V4 address,
 * function must return its NULL*/
INT4 
SliUtSligenp_12 (VOID)
{
    tIpAddr IpAddr;
    tIpAddr IpAddrTmp;
    tSliMd5SA   *pMd5SA = NULL;
    tSdtEntry   *pSdtSock = NULL;
    CHR1    *password1 = "QAZWSXqazwsx123";
    CHR1    *password2 = "test****";
    INT4    i4Sock;
    UINT1   au1Addr[16];

    MEMSET (au1Addr, 0, 16);

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n [SliGetMd5SA]Ut case 3-- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    /* Add node for IP 172.30.4.92 */
    MEMSET (&IpAddrTmp, 0, sizeof (IpAddrTmp));
    V4_MAPPED_ADDR_COPY (&IpAddrTmp, INET_ADDR ("172.30.4.92"));

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password1, 
                STRLEN (password1));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password1);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddrTmp);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliGetMd5SA]Ut case 3--- md5 node allocation failed");
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    /* Add node for V6 IP 5557::2222 */
    MEMSET (&IpAddrTmp, 0, sizeof (IpAddrTmp));
    INET_ATON6 ("5557::2222", au1Addr);
    V6_ADDR_COPY (&IpAddrTmp, (tIpAddr *) au1Addr);

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password2, 
                STRLEN (password2));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password2);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddrTmp);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliGetMd5SA]Ut case 3-- md5 node allocation failed");
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    V4_MAPPED_ADDR_COPY (&IpAddr, INET_ADDR ("12.0.0.1"));

    /* Function must return NULL as there is no node for 12.0.0.1 */
    if (NULL != SliGetMd5SA (i4Sock, IpAddr))
    {
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    SliClose (i4Sock);
    return OSIX_SUCCESS;
}

/* SliGetMd5SA - When MD5 list does not contain node for V6 address,
 * function must return NULL*/
INT4 
SliUtSligenp_13 (VOID)
{
    tIpAddr IpAddr;
    tIpAddr IpAddrTmp;
    tSliMd5SA   *pMd5SA = NULL;
    tSdtEntry   *pSdtSock = NULL;
    CHR1    *password1 = "QAZWSXqazwsx123";
    CHR1    *password2 = "test****";
    INT4    i4Sock;
    UINT1   au1Addr[16];

    MEMSET (au1Addr, 0, 16);
    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliGetMd5SA]Ut case 4-- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    /* Add node for IP 172.30.4.92 */
    MEMSET (&IpAddrTmp, 0, sizeof (IpAddrTmp));
    V4_MAPPED_ADDR_COPY (&IpAddrTmp, INET_ADDR ("172.30.4.92"));

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password1, 
                STRLEN (password1));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password1);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddrTmp);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliGetMd5SA]Ut case 4-- md5 node allocation failed");
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    /* Add node for V6 IP 5557::2222 */
    MEMSET (&IpAddrTmp, 0, sizeof (IpAddrTmp));
    INET_ATON6 ("5557::2222", au1Addr);
    V6_ADDR_COPY (&IpAddrTmp, (tIpAddr *) au1Addr);

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password2, 
                STRLEN (password2));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password2);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddrTmp);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliGetMd5SA]Ut case 4-- md5 node allocation failed");
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    MEMSET (&IpAddr, 0, sizeof (IpAddr));
    INET_ATON6 ("5557::11", au1Addr);
    V6_ADDR_COPY (&IpAddr, (tIpAddr *) au1Addr);

    /* Function must return NULL as there is no node for 5557::11 */
    if (NULL != SliGetMd5SA (i4Sock, IpAddr))
    {
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    SliClose (i4Sock);
    return OSIX_SUCCESS;
}

/* SliGetMd5SA - When MD5 list is empty,
 * function must return NULL*/
INT4 
SliUtSligenp_14 (VOID)
{
    tIpAddr IpAddr;
    INT4    i4Sock;
    UINT1   au1Addr[16];

    MEMSET (au1Addr, 0, 16);
    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n Ut case 14 in file sligenp.c --- error during socket creation");
        return OSIX_FAILURE;
    }

    MEMSET (&IpAddr, 0, sizeof (IpAddr));
    INET_ATON6 ("5557::11", au1Addr);
    V6_ADDR_COPY (&IpAddr, (tIpAddr *) au1Addr);

    /* Function must return NULL as list is empty*/
    if (NULL != SliGetMd5SA (i4Sock, IpAddr))
    {
        SliClose (i4Sock);
        return OSIX_FAILURE;
    }

    SliClose (i4Sock);
    return OSIX_SUCCESS;
}

/* SliAddMd5SA - When valid tcp_md5sig option is provided
 * for V4 address for which there is no node in list,
 * function must return success*/
INT4 
SliUtSligenp_15 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in  v4PeerAddr;
    tSdtEntry   *pSdtSock = NULL;
    tSliMd5SA   *pMd5SA = NULL;
    tIpAddr IpAddr;
    INT4    i4Sock = 0;
    INT4    i4Ret = OSIX_FAILURE;
    CHR1    *password = "qwertyuiopasdfghjkl[];',.12345678900:0:0";

    MEMSET (&md5sig, 0, sizeof (md5sig));
    MEMSET (&v4PeerAddr, 0, sizeof (v4PeerAddr));
    MEMSET (&IpAddr, 0, sizeof (IpAddr));

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliAddMd5SA] Ut case 1-- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    v4PeerAddr.sin_family = AF_INET;
    v4PeerAddr.sin_addr.s_addr = INET_ADDR ("12.0.0.1");

    MEMCPY (&md5sig.tcpm_addr,
            &v4PeerAddr, sizeof(struct sockaddr_in));
    MEMCPY (&md5sig.tcpm_key, password, STRLEN (password));
    md5sig.tcpm_keylen = STRLEN (password);

    V4_MAPPED_ADDR_COPY (&IpAddr, v4PeerAddr.sin_addr.s_addr);

    /* As no node exists for IP 12.0.0.1, function must 
     * return success and node for 12.0.0.1  must be added to the list
     * Node must contain the password and length specified in the option*/
    if (SLI_SUCCESS == SliAddMd5SA (i4Sock, &md5sig))
    {
        TMO_SLL_Scan(&(pSdtSock->Md5SAList), pMd5SA, tSliMd5SA *)
        {
            if (((V6_ADDR_CMP (&pMd5SA->PeerIp, &IpAddr)) == 0) &&
                ((MEMCMP (pMd5SA->MD5Key.au1Key, password, STRLEN (password))) == 0) &&
                (pMd5SA->MD5Key.u1Keylen == STRLEN (password)))
            {
                i4Ret = OSIX_SUCCESS;
            }
        }
    }

    SliClose (i4Sock);
    return i4Ret;
}

/* SliAddMd5SA - When valid tcp_md5sig option is provided
 * for V6 address for which there is no node in list,
 * function must return success*/
INT4 
SliUtSligenp_16 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in6  v6PeerAddr;
    tSdtEntry   *pSdtSock = NULL;
    tSliMd5SA   *pMd5SA = NULL;
    tIpAddr IpAddr;
    INT4    i4Sock = 0;
    INT4    i4Ret = OSIX_FAILURE;
    CHR1    *password = "qwertyuiopasdfghjkl[];',.12345678900:0:0";

    MEMSET (&md5sig, 0, sizeof (md5sig));
    MEMSET (&v6PeerAddr, 0, sizeof (v6PeerAddr));
    MEMSET (&IpAddr, 0, sizeof (IpAddr));

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliAddMd5SA] Ut case 2-- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    v6PeerAddr.sin6_family = AF_INET6;
    INET_ATON6 ("2345::11", v6PeerAddr.sin6_addr.s6_addr);

    MEMCPY (&md5sig.tcpm_addr,
            &v6PeerAddr, sizeof(struct sockaddr_in6));
    
    MEMCPY (&md5sig.tcpm_key, password, STRLEN (password));
    md5sig.tcpm_keylen = STRLEN (password);

    V6_ADDR_COPY (&IpAddr, (tIpAddr *) &(v6PeerAddr.sin6_addr));

    /* As no node exists for V6 IP 2345::11, function must 
     * return success and node for 2345::11  must be added to the list
     * Node must contain the password and length specified in the option*/
    if (SLI_SUCCESS == SliAddMd5SA (i4Sock, &md5sig))
    {
        TMO_SLL_Scan(&(pSdtSock->Md5SAList), pMd5SA, tSliMd5SA *)
        {
            if (((V6_ADDR_CMP (&pMd5SA->PeerIp, &IpAddr)) == 0) &&
                ((MEMCMP (pMd5SA->MD5Key.au1Key, password, STRLEN (password))) == 0) &&
                (pMd5SA->MD5Key.u1Keylen == STRLEN (password)))
            {
                i4Ret = OSIX_SUCCESS;
            }
        }
    }

    SliClose (i4Sock);
    return i4Ret;
}

/* SliAddMd5SA - When valid tcp_md5sig option is provided
 * for V4 address for which node exists in list,
 * function must return success*/
INT4 
SliUtSligenp_17 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in  v4PeerAddr;
    tSdtEntry   *pSdtSock = NULL;
    tSliMd5SA   *pMd5SA = NULL;
    tIpAddr IpAddr;
    tIpAddr IpAddrTmp;
    UINT1   au1Addr[16];
    INT4    i4Sock = 0;
    INT4    i4Ret = OSIX_FAILURE;
    CHR1    *password1 = "secret123";
    CHR1    *password2 = "QAZWSXqazwsx123";
    CHR1    *password3 = "test****";

    MEMSET (&v4PeerAddr, 0, sizeof (v4PeerAddr));
    MEMSET (&md5sig, 0, sizeof (md5sig));

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliAddMd5SA] Ut case 3-- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    /* Add two nodes into the list */
    MEMSET (&IpAddr, 0, sizeof(IpAddr));
    V4_MAPPED_ADDR_COPY (&IpAddr, INET_ADDR ("12.0.0.1"));

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password1,
                STRLEN (password1));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password1);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddr);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliAddMd5SA] Ut case 3-- md5 node allocation failed");
        goto CLEANUP;
    }

    MEMSET (&IpAddrTmp, 0, sizeof(IpAddrTmp));
    INET_ATON6 ("5557::2222", au1Addr);
    V6_ADDR_COPY (&IpAddrTmp, (tIpAddr *) au1Addr);

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password2,
                STRLEN (password2));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password2);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddrTmp);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliAddMd5SA] Ut case 3-- md5 node allocation failed");
        goto CLEANUP;
    }

    /* md5sig option value for 12.0.0.1 */
    v4PeerAddr.sin_family = AF_INET;
    v4PeerAddr.sin_addr.s_addr = INET_ADDR ("12.0.0.1");

    MEMCPY (&md5sig.tcpm_addr,
            &v4PeerAddr, sizeof(struct sockaddr_in));
    MEMCPY (&md5sig.tcpm_key, password3, STRLEN (password3));
    md5sig.tcpm_keylen = STRLEN (password3);

    /* As a node exists for IP 12.0.0.1, function must 
     * return success and node for 12.0.0.1  must be replaced
     * with new password and length specified in the option*/
    if (SLI_SUCCESS == SliAddMd5SA (i4Sock, &md5sig))
    {
        if (TMO_SLL_Count (&(pSdtSock->Md5SAList)) != 2)
        {
            printf ("\n[SliAddMd5SA] Ut case 3-- md5 list count not correct");
            goto CLEANUP;
        }
        TMO_SLL_Scan(&(pSdtSock->Md5SAList), pMd5SA, tSliMd5SA *)
        {
            if (((V6_ADDR_CMP (&pMd5SA->PeerIp, &IpAddr)) == 0) &&
                ((MEMCMP (pMd5SA->MD5Key.au1Key, password3, STRLEN (password3))) == 0) &&
                (pMd5SA->MD5Key.u1Keylen == STRLEN (password3)))
            {
                i4Ret = OSIX_SUCCESS;
            }
        }
    }

CLEANUP:
    SliClose (i4Sock);
    return i4Ret;
}

/* SliAddMd5SA - When valid tcp_md5sig option is provided
 * for V6 address for which node exists in list,
 * function must return success*/
INT4 
SliUtSligenp_18 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in6  v6PeerAddr;
    tSdtEntry   *pSdtSock = NULL;
    tSliMd5SA   *pMd5SA = NULL;
    tIpAddr IpAddr;
    tIpAddr IpAddrTmp;
    UINT1   au1Addr[16];
    INT4    i4Sock = 0;
    INT4    i4Ret = OSIX_FAILURE;
    CHR1    *password1 = "secret123";
    CHR1    *password2 = "QAZWSXqazwsx123";
    CHR1    *password3 = "test****";

    MEMSET (&v6PeerAddr, 0, sizeof (v6PeerAddr));
    MEMSET (&md5sig, 0, sizeof (md5sig));

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliAddMd5SA] Ut case 4-- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    /* Add two nodes into the list */
    MEMSET (&IpAddrTmp, 0, sizeof(IpAddrTmp));
    V4_MAPPED_ADDR_COPY (&IpAddrTmp, INET_ADDR ("12.0.0.1"));

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password1,
                STRLEN (password1));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password1);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddrTmp);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliAddMd5SA] Ut case 4-- md5 node allocation failed");
        goto CLEANUP;
    }

    MEMSET (&IpAddr, 0, sizeof(IpAddr));
    INET_ATON6 ("5557::2222", au1Addr);
    V6_ADDR_COPY (&IpAddr, (tIpAddr *) au1Addr);

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password2,
                STRLEN (password2));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password2);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddr);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliAddMd5SA] Ut case 4-- md5 node allocation failed");
        goto CLEANUP;
    }

    /* md5sig option value for 5557::2222 */
    v6PeerAddr.sin6_family = AF_INET6;
    MEMCPY (&(v6PeerAddr.sin6_addr.s6_addr), 
            au1Addr, 16);

    MEMCPY (&md5sig.tcpm_addr,
            &v6PeerAddr, sizeof(struct sockaddr_in6));
    MEMCPY (&md5sig.tcpm_key, password3, STRLEN (password3));
    md5sig.tcpm_keylen = STRLEN (password3);

    /* As a node exists for V6 IP 5557::2222, function must 
     * return success and node for 5557::2222  must be replaced 
     * with new password and length specified in the option*/
    if (SLI_SUCCESS == SliAddMd5SA (i4Sock, &md5sig))
    {
        if (TMO_SLL_Count (&(pSdtSock->Md5SAList)) != 2)
        {
            printf ("\n[SliAddMd5SA] Ut case 4-- md5 list count not correct");
            goto CLEANUP;
        }
        TMO_SLL_Scan(&(pSdtSock->Md5SAList), pMd5SA, tSliMd5SA *)
        {
            if (((V6_ADDR_CMP (&pMd5SA->PeerIp, &IpAddr)) == 0) &&
                ((MEMCMP (pMd5SA->MD5Key.au1Key, password3, STRLEN (password3))) == 0) &&
                (pMd5SA->MD5Key.u1Keylen == STRLEN (password3)))
            {
                i4Ret = OSIX_SUCCESS;
            }
        }
    }

CLEANUP:
    SliClose (i4Sock);
    return i4Ret;
}

/* SliAddMd5SA - When md5 pool is exhausted,
 * function must return failure*/
INT4
SliUtSligenp_19 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in  v4PeerAddr;
    tSliMd5SA   *pMd5SA[MAX_SLI_MD5_SLL_NODES];
    UINT4   u4Count = 0;
    INT4    i4Sock = 0;
    INT4    i4Ret = OSIX_SUCCESS;
    CHR1    *password = "qwertyuiopasdfghjkl[];',.12345678900:0:0";

    MEMSET (&md5sig, 0, sizeof (md5sig));
    MEMSET (&v4PeerAddr, 0, sizeof (v4PeerAddr));
    
    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliAddMd5SA] Ut case 5-- error during socket creation");
        return OSIX_FAILURE;
    }

    for (u4Count = 0; u4Count < MAX_SLI_MD5_SLL_NODES; u4Count ++)
    {
        pMd5SA[u4Count] = MemAllocMemBlk (gSliMd5NodePoolId);
    }

    v4PeerAddr.sin_family = AF_INET;
    v4PeerAddr.sin_addr.s_addr = INET_ADDR ("12.0.0.1");
    MEMCPY (&md5sig.tcpm_addr,
            &v4PeerAddr, sizeof(struct sockaddr_in));
    MEMCPY (&md5sig.tcpm_key, password, STRLEN (password));
    md5sig.tcpm_keylen = STRLEN (password);

    if (SLI_FAILURE != SliAddMd5SA (i4Sock, &md5sig))
    {
        i4Ret = OSIX_FAILURE;
    }
    else if (errno != SLI_EMEMFAIL)
    {
        i4Ret = OSIX_FAILURE;
    }
    
    for (u4Count = 0; u4Count < MAX_SLI_MD5_SLL_NODES; u4Count ++)
    {
        SliFreeMemToPool (gSliMd5NodePoolId, pMd5SA[u4Count]);
    }
    
    SliClose (i4Sock);
    return i4Ret;
}

/* SliDelMd5SA - When valid tcp_md5sig option provided
 * with V4 address, if node exists in list must be deleted
 * function must return success*/ 
INT4
SliUtSligenp_20 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in  v4PeerAddr;
    tSdtEntry   *pSdtSock = NULL;
    tSliMd5SA   *pMd5SA = NULL;
    tIpAddr IpAddr;
    INT4    i4Sock = 0;
    INT4    i4Ret = OSIX_SUCCESS;
    CHR1    *password = "test****";

    MEMSET (&v4PeerAddr, 0, sizeof (v4PeerAddr));
    MEMSET (&md5sig, 0, sizeof (md5sig));

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliDelMd5SA] Ut case 1--- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    /* Add 12.0.0.1 node in list */
    MEMSET (&IpAddr, 0, sizeof(IpAddr));
    V4_MAPPED_ADDR_COPY (&IpAddr, INET_ADDR ("12.0.0.1"));

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password,
                STRLEN (password));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddr);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliDelMd5SA] Ut case 1-- md5 node allocation failed");
        goto CLEANUP;
    }

    /* md5sig option value for 12.0.0.1 */
    v4PeerAddr.sin_family = AF_INET;
    v4PeerAddr.sin_addr.s_addr = INET_ADDR ("12.0.0.1");

    MEMCPY (&md5sig.tcpm_addr,
            &v4PeerAddr, sizeof(struct sockaddr_in));

    /* As a node exists for IP 12.0.0.1, function must 
     * return success and node for 12.0.0.1  must be deleted*/
    if (SLI_SUCCESS != SliDelMd5SA (i4Sock, &md5sig))
    {
        i4Ret = OSIX_FAILURE;
    }
    else if (TMO_SLL_Count (&(pSdtSock->Md5SAList)) != 0)
    {
        i4Ret = OSIX_FAILURE;
    }

CLEANUP:
    SliClose (i4Sock);
    return i4Ret;
}

/* SliDelMd5SA - When valid tcp_md5sig option provided
 * with V6 address, if node exists in list must be deleted
 * function must return success*/ 
INT4
SliUtSligenp_21 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in6  v6PeerAddr;
    tSdtEntry   *pSdtSock = NULL;
    tSliMd5SA   *pMd5SA = NULL;
    tIpAddr IpAddr;
    UINT1   au1Addr[16];
    INT4    i4Sock = 0;
    INT4    i4Ret = OSIX_SUCCESS;
    CHR1    *password = "secret123";

    MEMSET (&v6PeerAddr, 0, sizeof (v6PeerAddr));
    MEMSET (&md5sig, 0, sizeof (md5sig));

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliDelMd5SA] Ut case 2-- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    MEMSET (&IpAddr, 0, sizeof(IpAddr));
    INET_ATON6 ("5557::2222", au1Addr);
    V6_ADDR_COPY (&IpAddr, (tIpAddr *) au1Addr);

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password,
                STRLEN (password));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddr);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliDelMd5SA] Ut case 2-- md5 node allocation failed");
        goto CLEANUP;
    }

    /* md5sig option value for 5557::2222 */
    v6PeerAddr.sin6_family = AF_INET6;
    MEMCPY (&(v6PeerAddr.sin6_addr.s6_addr), 
            au1Addr, 16);

    MEMCPY (&md5sig.tcpm_addr,
            &v6PeerAddr, sizeof(struct sockaddr_in6));

    /* As a node exists for V6 IP 5557::2222, function must 
     * return success and node for 5557::2222  must be deleted*/
    if (SLI_SUCCESS != SliDelMd5SA (i4Sock, &md5sig))
    {
        i4Ret = OSIX_FAILURE;
    }
    else if (TMO_SLL_Count (&(pSdtSock->Md5SAList)) != 0)
    {
        i4Ret = OSIX_FAILURE;
    }

CLEANUP:
    SliClose (i4Sock);
    return i4Ret;
}

/* SliDelMd5SA - When valid tcp_md5sig option, if node does 
 * not exist in list for IP, function must return failure
 * with errno - SLI_ENOENT*/ 
INT4
SliUtSligenp_22 (VOID)
{
    struct tcp_md5sig   md5sig;
    struct sockaddr_in6  v6PeerAddr;
    tSdtEntry   *pSdtSock = NULL;
    tSliMd5SA   *pMd5SA = NULL;
    tIpAddr IpAddr;
    UINT1   au1Addr[16];
    INT4    i4Sock = 0;
    INT4    i4Ret = OSIX_SUCCESS;
    CHR1    *password = "secret123";

    MEMSET (&v6PeerAddr, 0, sizeof (v6PeerAddr));
    MEMSET (&md5sig, 0, sizeof (md5sig));

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliDelMd5SA] Ut case 3-- error during socket creation");
        return OSIX_FAILURE;
    }

    pSdtSock = SOCK_DESC_TABLE[i4Sock];

    MEMSET (&IpAddr, 0, sizeof(IpAddr));
    INET_ATON6 ("5557::2222", au1Addr);
    V6_ADDR_COPY (&IpAddr, (tIpAddr *) au1Addr);

    if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
    {
        MEMSET (pMd5SA, 0, sizeof(tSliMd5SA));
        MEMCPY (pMd5SA->MD5Key.au1Key, password,
                STRLEN (password));
        pMd5SA->MD5Key.u1Keylen = STRLEN (password);
        V6_ADDR_COPY (&pMd5SA->PeerIp, &IpAddr);
        TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
    }
    else
    {
        printf ("\n[SliDelMd5SA] Ut case 3-- md5 node allocation failed");
        goto CLEANUP;
    }

    /* md5sig option value for 5557::1111 */
    v6PeerAddr.sin6_family = AF_INET6;
    INET_ATON6 ("5557::1111", v6PeerAddr.sin6_addr.s6_addr);

    MEMCPY (&md5sig.tcpm_addr,
            &v6PeerAddr, sizeof(struct sockaddr_in6));

    /* As no node exists for V6 IP 5557::1111, function must 
     * return failure*/
    if (SLI_FAILURE != SliDelMd5SA (i4Sock, &md5sig))
    {
        i4Ret = OSIX_FAILURE;
    }
    else if (errno != SLI_ENOENT)
    {
        i4Ret = OSIX_FAILURE;
    }

CLEANUP:
    SliClose (i4Sock);
    return i4Ret;
}

/* SliTcpFindMd5Key */
INT4
SliUtSligenp_23 (VOID)
{
    tIpAddr IpAddr;
    struct tcp_md5sig   md5sig;
    struct sockaddr_in  v4Addr;
    INT4    i4Sock = 0, i4Ret = OSIX_SUCCESS;
    UINT1   au1Key[80];
    CHR1    *password = "secret";
    UINT1   u1KeyLen = 0;

    MEMSET (&v4Addr, 0, sizeof (v4Addr));
    MEMSET (&md5sig, 0, sizeof (md5sig));
    MEMSET (&IpAddr, 0, sizeof (IpAddr));
    MEMSET (au1Key, 0, 80);

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 1-- error during socket creation");
        return OSIX_FAILURE;
    }

    v4Addr.sin_family = AF_INET;
    v4Addr.sin_addr.s_addr = INET_ADDR ("12.0.0.1");

    MEMCPY (&md5sig.tcpm_addr,
            &v4Addr, sizeof(struct sockaddr_in));
    MEMCPY (&md5sig.tcpm_key, password, STRLEN (password));
    md5sig.tcpm_keylen = STRLEN (password);

    if (SLI_SUCCESS!= SliAddMd5SA (i4Sock, &md5sig))
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 1-- SliAddMd5SA failed");
        i4Ret = OSIX_FAILURE;
        goto CLEANUP;
    }

    V4_MAPPED_ADDR_COPY (&IpAddr, INET_ADDR ("12.0.0.1"));

    if (TRUE != SliTcpFindMd5Key (i4Sock, IpAddr, au1Key, &u1KeyLen))
    {
        i4Ret = OSIX_FAILURE;
    }
    else if (((MEMCMP (au1Key, password, STRLEN (password)))!= 0) ||
             (u1KeyLen == 0))
    {
        i4Ret = OSIX_FAILURE;
    }
CLEANUP:
    SliClose (i4Sock);
    return i4Ret;
}

INT4
SliUtSligenp_24 (VOID)
{
    tIpAddr IpAddr;
    struct tcp_md5sig   md5sig;
    struct sockaddr_in6  v6Addr;
    INT4    i4Sock = 0, i4Ret = OSIX_SUCCESS;
    UINT1   au1Key[80];
    CHR1    *password = "secret";
    UINT1   u1KeyLen = 0;

    MEMSET (&v6Addr, 0, sizeof (v6Addr));
    MEMSET (&md5sig, 0, sizeof (md5sig));
    MEMSET (&IpAddr, 0, sizeof (IpAddr));
    MEMSET (au1Key, 0, 80);

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 2-- error during socket creation");
        return OSIX_FAILURE;
    }

    v6Addr.sin6_family = AF_INET6;
    INET_ATON6 ("2345::11", v6Addr.sin6_addr.s6_addr);

    MEMCPY (&md5sig.tcpm_addr,
            &v6Addr, sizeof(struct sockaddr_in6));
    MEMCPY (&md5sig.tcpm_key, password, STRLEN (password));
    md5sig.tcpm_keylen = STRLEN (password);

    if (SLI_SUCCESS!= SliAddMd5SA (i4Sock, &md5sig))
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 1-- SliAddMd5SA failed");
        i4Ret = OSIX_FAILURE;
        goto CLEANUP;
    }

    V6_ADDR_COPY (&IpAddr, (tIpAddr *) &(v6Addr.sin6_addr));

    if (TRUE != SliTcpFindMd5Key (i4Sock, IpAddr, au1Key, &u1KeyLen))
    {
        i4Ret = OSIX_FAILURE;
    }
    else if (((MEMCMP (au1Key, password, STRLEN (password)))!= 0) ||
             (u1KeyLen == 0))
    {
        i4Ret = OSIX_FAILURE;
    }
CLEANUP:
    SliClose (i4Sock);
    return i4Ret;
}

INT4
SliUtSligenp_25 (VOID)
{
    tIpAddr IpAddr;
    struct tcp_md5sig   md5sig;
    struct sockaddr_in6  v6Addr;
    INT4    i4Sock = 0, i4Ret = OSIX_SUCCESS;
    UINT1   au1Key[80];
    CHR1    *password = "secret";
    UINT1   u1KeyLen = 0;

    MEMSET (&v6Addr, 0, sizeof (v6Addr));
    MEMSET (&md5sig, 0, sizeof (md5sig));
    MEMSET (&IpAddr, 0, sizeof (IpAddr));
    MEMSET (au1Key, 0, 80);

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 3-- error during socket creation");
        return OSIX_FAILURE;
    }

    v6Addr.sin6_family = AF_INET6;
    INET_ATON6 ("2345::11", v6Addr.sin6_addr.s6_addr);

    MEMCPY (&md5sig.tcpm_addr,
            &v6Addr, sizeof(struct sockaddr_in6));
    MEMCPY (&md5sig.tcpm_key, password, STRLEN (password));
    md5sig.tcpm_keylen = STRLEN (password);

    if (SLI_SUCCESS!= SliAddMd5SA (i4Sock, &md5sig))
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 3-- SliAddMd5SA failed");
        i4Ret = OSIX_FAILURE;
        goto CLEANUP;
    }

    V4_MAPPED_ADDR_COPY (&IpAddr, INET_ADDR ("12.0.0.1"));

    if (FALSE != SliTcpFindMd5Key (i4Sock, IpAddr, au1Key, &u1KeyLen))
    {
        i4Ret = OSIX_FAILURE;
    }

CLEANUP:
    SliClose (i4Sock);
    return i4Ret;
}

INT4
SliUtSligenp_26 (VOID)
{
    tIpAddr IpAddr;
    struct tcp_md5sig   md5sig;
    struct sockaddr_in  v4Addr;
    INT4    i4Sock = 0, i4Ret = OSIX_SUCCESS;
    UINT1   au1Key[80];
    UINT1   au1Addr[16];
    CHR1    *password = "secret";
    UINT1   u1KeyLen = 0;

    MEMSET (&v4Addr, 0, sizeof (v4Addr));
    MEMSET (&md5sig, 0, sizeof (md5sig));
    MEMSET (&IpAddr, 0, sizeof (IpAddr));
    MEMSET (au1Key, 0, 80);
    MEMSET (au1Addr, 0, 16);

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 4-- error during socket creation");
        return OSIX_FAILURE;
    }

    v4Addr.sin_family = AF_INET;
    v4Addr.sin_addr.s_addr = INET_ADDR ("12.0.0.1");

    MEMCPY (&md5sig.tcpm_addr,
            &v4Addr, sizeof(struct sockaddr_in));
    MEMCPY (&md5sig.tcpm_key, password, STRLEN (password));
    md5sig.tcpm_keylen = STRLEN (password);

    if (SLI_SUCCESS!= SliAddMd5SA (i4Sock, &md5sig))
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 4-- SliAddMd5SA failed");
        i4Ret = OSIX_FAILURE;
        goto CLEANUP;
    }

    INET_ATON6 ("5555::1111", au1Addr);
    V6_ADDR_COPY (&IpAddr, (tIpAddr *) au1Addr);

    if (FALSE != SliTcpFindMd5Key (i4Sock, IpAddr, au1Key, &u1KeyLen))
    {
        i4Ret = OSIX_FAILURE;
    }

CLEANUP:
    SliClose (i4Sock);
    return i4Ret;
}

INT4
SliUtSligenp_27 (VOID)
{
    tIpAddr IpAddr;
    INT4    i4Sock = 0, i4Ret = OSIX_SUCCESS;
    UINT1   au1Key[80];
    UINT1   u1KeyLen = 0;

    MEMSET (&IpAddr, 0, sizeof (IpAddr));
    MEMSET (au1Key, 0, 80);

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 5--- error during socket creation");
        return OSIX_FAILURE;
    }

    SliClose (i4Sock);

    if (FALSE != SliTcpFindMd5Key (i4Sock, IpAddr, au1Key, &u1KeyLen))
    {
        i4Ret = OSIX_FAILURE;
    }

    return i4Ret;
}

INT4
SliUtSligenp_28 (VOID)
{
    tIpAddr IpAddr;
    INT4    i4Sock = 0, i4Ret = OSIX_SUCCESS;
    UINT1   u1KeyLen = 0;

    MEMSET (&IpAddr, 0, sizeof (IpAddr));

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 6-- error during socket creation");
        return OSIX_FAILURE;
    }

    if (FALSE != SliTcpFindMd5Key (i4Sock, IpAddr, NULL, &u1KeyLen))
    {
        i4Ret = OSIX_FAILURE;
    }

    SliClose (i4Sock);
    return i4Ret;
}

INT4
SliUtSligenp_29 (VOID)
{
    tIpAddr IpAddr;
    UINT1   au1Key[80];
    INT4    i4Sock = 0, i4Ret = OSIX_SUCCESS;

    MEMSET (&IpAddr, 0, sizeof (IpAddr));

    i4Sock = SliSocket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (i4Sock < 0)
    {
        printf ("\n[SliTcpFindMd5Key] Ut case 7-- error during socket creation");
        return OSIX_FAILURE;
    }

    if (FALSE != SliTcpFindMd5Key (i4Sock, IpAddr, au1Key, NULL))
    {
        i4Ret = OSIX_FAILURE;
    }

    SliClose (i4Sock);
    return i4Ret;
}

/* slifile2 UT cases */
INT4 
SliUtFile2_1 (VOID)
{ 
    return -1;
}
INT4 
SliUtFile2_2 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_3 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_4 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_5 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_6 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_7 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_8 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_9 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_10 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_11 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_12 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_13 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_14 (VOID)
{
    return -1;
}
INT4 
SliUtFile2_15 (VOID)
{
    return -1;
}

/* slifile3 UT cases */
INT4 
SliUtFile3_1 (VOID)
{ 
    return -1;
}
INT4 
SliUtFile3_2 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_3 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_4 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_5 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_6 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_7 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_8 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_9 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_10 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_11 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_12 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_13 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_14 (VOID)
{
    return -1;
}
INT4 
SliUtFile3_15 (VOID)
{
    return -1;
}

/* slifile4 UT cases */
INT4 
SliUtFile4_1 (VOID)
{ 
    return -1;
}
INT4 
SliUtFile4_2 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_3 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_4 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_5 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_6 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_7 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_8 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_9 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_10 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_11 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_12 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_13 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_14 (VOID)
{
    return -1;
}
INT4 
SliUtFile4_15 (VOID)
{
    return -1;
}

/* slifile5 UT cases */
INT4 
SliUtFile5_1 (VOID)
{ 
    return -1;
}
INT4 
SliUtFile5_2 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_3 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_4 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_5 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_6 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_7 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_8 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_9 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_10 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_11 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_12 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_13 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_14 (VOID)
{
    return -1;
}
INT4 
SliUtFile5_15 (VOID)
{
    return -1;
}

/* slifile6 UT cases */
INT4 
SliUtFile6_1 (VOID)
{ 
    return -1;
}
INT4 
SliUtFile6_2 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_3 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_4 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_5 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_6 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_7 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_8 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_9 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_10 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_11 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_12 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_13 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_14 (VOID)
{
    return -1;
}
INT4 
SliUtFile6_15 (VOID)
{
    return -1;
}

/* slifile7 UT cases */
INT4 
SliUtFile7_1 (VOID)
{ 
    return -1;
}
INT4 
SliUtFile7_2 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_3 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_4 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_5 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_6 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_7 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_8 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_9 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_10 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_11 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_12 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_13 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_14 (VOID)
{
    return -1;
}
INT4 
SliUtFile7_15 (VOID)
{
    return -1;
}

/* slifile8 UT cases */
INT4 
SliUtFile8_1 (VOID)
{ 
    return -1;
}
INT4 
SliUtFile8_2 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_3 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_4 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_5 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_6 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_7 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_8 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_9 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_10 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_11 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_12 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_13 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_14 (VOID)
{
    return -1;
}
INT4 
SliUtFile8_15 (VOID)
{
    return -1;
}

/* slifile9 UT cases */
INT4 
SliUtFile9_1 (VOID)
{ 
    return -1;
}
INT4 
SliUtFile9_2 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_3 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_4 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_5 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_6 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_7 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_8 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_9 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_10 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_11 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_12 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_13 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_14 (VOID)
{
    return -1;
}
INT4 
SliUtFile9_15 (VOID)
{
    return -1;
}


