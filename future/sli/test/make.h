##########################################################################
# Copyright (C) Future Software Limited,2010			                 #
#                                                                        #
# $Id: make.h,v 1.1.1.1 2011/09/12 10:07:00 siva Exp $                     #
#								                                         #
# Description : Contains information fro creating the make file          #
#		for this module      				                             #
#					                                 			         #
##########################################################################

#include the make.h and make.rule from LR
include ../../LR/make.h
include ../../LR/make.rule

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)

#project directories
SLI_INCL_DIR           = $(BASE_DIR)/sli

SLI_TEST_BASE_DIR  = ${BASE_DIR}/sli/test
SLI_TEST_SRC_DIR   = ${SLI_TEST_BASE_DIR}/src
SLI_TEST_INC_DIR   = ${SLI_TEST_BASE_DIR}/inc
SLI_TEST_OBJ_DIR   = ${SLI_TEST_BASE_DIR}/obj

SLI_TEST_INCLUDES  = -I$(SLI_TEST_INC_DIR) \
                     -I$(SLI_INCL_DIR) \
                     $(COMMON_INCLUDE_DIRS)


##########################################################################
#                    End of file make.h					 #
##########################################################################
