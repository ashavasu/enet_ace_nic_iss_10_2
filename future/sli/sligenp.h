/** $Id: sligenp.h,v 1.27 2017/09/13 13:34:07 siva Exp $ */
/********************************************************************
 *                                                                  *
 * $RCSfile: sligenp.h,v $
 *                                                                  *
 * $Date: 2017/09/13 13:34:07 $                                     *
 *                                                                  *
 * $Revision: 1.27 $                                                 *
 *                                                                  *
 *******************************************************************/

/***************************************************************************  */
/*FILE NAME             : sligenp.h                                           */
/*PRINCIPAL AUTHOR      : Sri. Chellappa.                                     */
/*SUBSYSTEM NAME        : SLI                                                 */
/*MODULE NAME           : Socket Layer Interface                              */
/*LANGUAGE              : C                                                   */
/*TARGET ENVIRONMENT    :                                                     */
/*DATE OF FIRST RELEASE :                                                     */
/*DESCRIPTION           : This file contains function prototypes for          */
/*                        the routines in sligenp.c                           */
/* ------------------------------------------------------------------------   */
/*   VERSION   AUTHOR/DATE   DESCRIPTION OF CHANGE                            */
/* ------------------------------------------------------------------------   */
/*     1.5      N.R.Soma     + SliValidateSocket prototype added.             */
/*              18-MAR-99    + Functions names are changed.                   */
/*                           ->Above changes are as per RFC chreq02           */
/*     1.6      Bhuvana      + Some Macros and Prototype declarations         */
/*              25-Dec-99        have been placed within the switch           */
/*                               TCP_WANTED                                   */
/*     -        Ramakrishnan + Added protos for SliSetFdZero and              */
/*                             SliWaitForSelectEvent and included             */
/*                             socket.h under compilation flag ST_ENHLISTINTF */
/* ------------------------------------------------------------------------   */
/*     1.7      Saravanan.M    Implementation of                              */
/*              Purush            - Non Blocking Connect                      */
/*                                - Non Blocking Accept                       */
/*                                - IfMsg Interface between TCP and IP        */
/*                                - One Queue per Socket                      */
/* ------------------------------------------------------------------------   */
/***************************************************************************  */
#ifndef __SLIGEN_H__
#define __SLIGEN_H__

#define SLI_SELECT_ENTER_CS()   \
 (OsixSemTake (gSelectSemId))
#define SLI_SELECT_EXIT_CS()    \
 (OsixSemGive(gSelectSemId))
  
#define IS_V6DGRAM_PKT_FOR_ME(x,y,z,w)  ( (((V6_ADDR_CMP(x,y)) == 0) && (z == w)) || ((IS_ADDR_IN6ADDRANYINIT(*x)) && (z == w)) || ((IS_ADDR_IN6ADDRANYINIT(*x)) && (z == 0)) )

#define IS_V4DGRAM_PKT_FOR_ME(x,y,z,w) ( ((x == y) && (z == w)) || ((x == 0) && (z == w)) || ((x == 0) && (z == 0)) )

#define SLI_RAWHP_ENTER_CS()  \
 (OsixSemTake(gHashPoolSemId)) 
#define SLI_RAWHP_EXIT_CS()  \
 (OsixSemGive(gHashPoolSemId))
#define SLI_RAWHL_ENTER_CS() \
 (OsixSemTake(gHashListSemId))
#define SLI_RAWHL_EXIT_CS() \
        (OsixSemGive(gHashListSemId))

INT4 SliLookup         PROTO ((INT4 i4SockDesc));

INT4 SliGetFreeSock    PROTO ((VOID));

VOID SliDestroySocket  PROTO ((INT4 i4SockDesc));

INT4 SliValidateSocket PROTO ((INT4 i4Family,INT4 i4SockType,INT4 *i4Protocol));

#ifdef TCP_WANTED

#define  SLI_TCP_RCV_URG_DATA(x,y,z)    \
                SliTcpRcvData(x, y, z, URG_DATA_NEEDED)

#define  SLI_TCP_RCV_NORMAL_DATA(x,y,z)  \
                SliTcpRcvData(x, y, z, NORMAL_DATA_NEEDED)

INT4 SliTcpIssueOpenRequest     PROTO ((INT4 i4SockDesc, UINT1 u1Opentype));

INT4 SliTcpIssueRegAsyncRequest PROTO ((INT4 i4SockDesc, VOID (*async_handler) (INT4 SockDesc, INT1 *PtaPRetParms)));

tTcpToApplicationParms *SliTcpIssueCloseRequest  PROTO ((INT4 i4SockDesc));

tTcpToApplicationParms *SliTcpIssueAbortRequest  PROTO ((INT4 i4SockDesc));

tTcpToApplicationParms *SliTcpIssueWriteRequest  PROTO ((INT4 i4SockDesc, INT1 *Cpi1WriteBuf, INT4 i4NumToWrite, UINT1 u1Flag, UINT4 u4Timeout));

tTcpToApplicationParms *SliTcpIssueOptionRequest PROTO ((INT4 i4SockDesc, UINT1 u1Optiontype, UINT2 u2OptName, UINT1 u1Optlen, AR_UINT8 u8Value));

tTcpToApplicationParms *SliTcpIssueStatusRequest PROTO ((INT4 i4SockDesc));

INT4 SliTcpRcvData           PROTO ((INT4 i4SockDesc, tApplicationToTcpParms Parms, INT4 *pi4NumRcvd, INT4 i4WhatData));

INT4 SliTcpProcessReturnMesg PROTO ((INT4 i4SockDesc, tTcpToApplicationParms *PtaPRetParms));


INT4 SliGetFreeLocalTcpPort PROTO ((UINT2*));

INT4 SliValidateMd5Option PROTO ((struct tcp_md5sig *));

INT4 SliAddMd5SA PROTO ((INT4, struct tcp_md5sig *));

INT4 SliDelMd5SA PROTO ((INT4, struct tcp_md5sig *));

tSliMd5SA * SliGetMd5SA PROTO ((INT4, tIpAddr));

INT4 SliAddTcpAoMkt PROTO ((INT4 , tTcpAoMktAddr *));

INT4 SliDelTcpAoMkt PROTO ((INT4 , tTcpAoMktAddr *));

INT4 SliValidateTcpAoOption PROTO ((tTcpAoMktAddr* ));

INT4 SliValidateTcpAoNghCfg PROTO((tTcpAoNeighCfg *));

INT4 SliAddTcpAoNghMktCfg PROTO((INT4 , tTcpAoNeighCfg *));

INT4 SliDelTcpAoNghMktCfg PROTO((INT4 , tTcpAoNeighCfg *));

INT4 SliAddTcpAoNghIcmpCfg PROTO((INT4 , tTcpAoNeighCfg *));

INT4 SliDelTcpAoNghIcmpCfg PROTO((INT4 , tTcpAoNeighCfg *));

#ifndef SLI_TCPAO_GET
#define SLI_TCPAO_GET
tsliTcpAoMktListNode * SliGetTcpAo PROTO ((INT4 i4SockDesc, tIpAddr IpAddr));

UINT1 * SliGetTcpAoNghMktCfg PROTO((INT4 , tIpAddr ));

UINT1 * SliGetTcpAoNghIcmpCfg PROTO((INT4 , tIpAddr ));
#endif
#endif

INT4 SliFindSockDescFromPortAddrInCxt PROTO ((UINT4, UINT2, tIpAddr, INT2));

tSliRawLlToIp6Params * SliRawIp6GetParms  PROTO (( tCRU_BUF_CHAIN_HEADER * ));
INT4 SliProcessSelect PROTO ((INT4, SliFdSet *, SliFdSet *, SliFdSet *));

INT4 SliSelectScanListForRead PROTO ((INT4));

INT4 SliCheckSocketConnection(UINT4 u4ConnId);

INT4 SliWaitForSelectEvent  PROTO ((struct timeval *, INT4 i4SockDesc));

UINT4 SliIsFdSetClear PROTO ((SliFdSet *pFd));

#endif /* _SLIGEN_H */
