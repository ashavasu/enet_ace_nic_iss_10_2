/*
 *  $Id: slimacs.h,v 1.17 2016/07/05 08:22:23 siva Exp $ 
 * +--------------------------------------------------------------------------
 * | Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * |
 * | Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * |
 * | FILE NAME               : slimacs.h
 * |
 * | PRINCIPAL AUTHOR        : Shankar Vasudevan
 * |
 * | SUBSYSTEM NAME          : TCP
 * |
 * | MODULE NAME             : SLI
 * |
 * | LANGUAGE                : C
 * |
 * | TARGET ENVIRONMENT      : ANY
 * |
 * | DATE OF FIRST RELEASE   : 12-01-2000
 * |
 * | DESCRIPTION             : This file contains macros used by the SLI. 
 * +--------------------------------------------------------------------------
 * |
 * | CHANGE RECORD:
 * |
 * +---------+---------+--------------------+---------------------------------
 * | Version |  Date   |       Author       |       Description of change
 * +---------+---------+--------------------+---------------------------------
 * |  -      |Jan 2000 | Shankar Vasudevan  | Created Original
 * +--------------------------------------------------------------------------
 */
/* address size definitons used by inet_pton and inet_ntop */ 

#ifndef __SLI_MACS_H__
#define __SLI_MACS_H__

#define  NS_INT16SZ               2
#define  NS_IN6ADDRSZ             16
#define  SLI_INADDRSZ             4                      
#define  SLI_IN6ADDRSZ            16                     
#define  SLI_INT16SZ              sizeof(UINT2)          

/*  flag values for sending and receiving data */

#define  IS_PEEK_SET(flag)           ((flag & MSG_PEEK) == MSG_PEEK)           
#define  IS_DONTWAIT_SET(flag)       ((flag & MSG_DONTWAIT) == MSG_DONTWAIT)   
#define  IS_DONTROUTE_SET(flag)      ((flag & MSG_DONTROUTE) == MSG_DONTROUTE) 
#define  IS_OOB_SET(flag)            ((flag & MSG_OOB) == MSG_OOB)             

/* Type of data to receive by the Recv call */
#define  URG_DATA_NEEDED     0 
#define  NORMAL_DATA_NEEDED  1
#define  INVALID_INTERFACE           0xFFFF

/* Initial sema4 count values */
#define  ZERO_COUNT          0
#define  INIT_SEM_COUNT      1

/* other definitions required */
#define  SLI_MAX_BACK_LOG    50 
#define  MILLION             1000000

/* Definition used by the select call */
#define SLI_SELECT_SEM_NAME     (const UINT1 *)"SLIE"
#define SOCKET_READY_EVENT      0x80000000

#define  MAX_RAW_Q_SIZE                 20

/* other definition's required by SLI */
#define  MAX_INET_PROTOS                256
#define  MAX_MAC_HDR_LEN                32
#define  DOUBLE_WORD_LEN                4

/* IP address class definitions */
#define  IP_CLASS_A_DEF_MASK            0xff000000         
#define  IP_CLASS_A_DEF_BROADCAST_MASK  0xffffff           
#define  IP_CLASS_B_DEF_MASK            0xffff0000         
#define  IP_CLASS_B_DEF_BROADCAST_MASK  0xffff             
#define  IP_CLASS_C_DEF_MASK            0xffffff00         
#define  IP_CLASS_C_DEF_BROADCAST_MASK  0xff               
#define  IP_FWD_TASK_ID                 CRU_IP_FWD_TASK_ID 

/* SoReuseAddr Socket Option - 02 - S */
#define  SLI_CONN_NOT_EXIST             (0)                
#define  SLI_CONN_EXIST                 (1)                
/* SoReuseAddr Socket Option - 02 - E */


/* private port values use dby TCP and UDP */
#define  TCP_PRIVATE_PORT_START  49152
#define  TCP_PRIVATE_PORT_END    65535

#define  SLI_DEF_HOP_LIMIT 64 
#define  SLI_MAX_HOP_LIMIT 255 

#define SLI_ZERO                      0
#define SLI_ONE                       1
#define SLI_TWO                       2
#define SLI_THREE                     3
#define SLI_FOUR                      4
#define SLI_SEM_NAME_LEN              4
#define SLI_V6_THIRD_OCTET            3
#define SLI_MULTICAST_LOOP            1
#define SLI_BROADCAST_FLAG            1
#define SLI_DELAY_TASK_ONE_UNIT       1
#define SLI_HOP_LIMIT                -1
#define SLI_TCP_FLAG                  1
#define SLI_PEER_ADDRESS_WRITE_OFFSET 8
#define SLI_SRC_ADDR_OFFSET           8
#define SLI_DEST_ADDR_OFFSET          24
#define SLI_V4_ADDR_LEN               4
#define SLI_RECV_QUEUE_NAME_FIRST_DECIDER  1000
#define SLI_RECV_QUEUE_NAME_SECOND_DECIDER 100
#define SLI_RECV_QUEUE_NAME_THIRD_DECIDER  10
#define SLI_V4_DIGITS "0123456789"
#define SLI_MULTIPLY_FACTOR  10
#define SLI_MAX_UNIT1        255
#define SLI_MAX_UINT1_BITS   8
#define SLI_MAX_UINT2        0xffff
#define SLI_NULL_CHAR        '\0'
#define SLI_DOT_CHAR         '.'
#define SLI_C_CHAR         'C'
#define SLI_S_CHAR         'S'
#define SLI_COLON_CHAR       ':'
#define SLI_V6_DIGIT_LOW "0123456789abcdef"
#define SLI_V6_DIGIT_UP "0123456789ABCDEF"
#define SLI_ALL_BCAST_ADDR "255.255.255.255"

/*  IPV6 addressing related macros */
#define  IN6_IS_ADDR_UNSPECIFIED(a)  IS_ADDR_UNSPECIFIED(a)                    
#define  IN6_IS_ADDR_MUTLICAST(a)    IS_ADDR_MULTI(a)                          
#define  IN6_IS_ADDR_COMPAT(a)       IS_ADDR_V4_COMPAT(a)                      

#define  IN6_IS_ADDR_LINKLOCAL(a)     \
   (((a).u4_addr[0] & CRU_HTONL(0xFFC00000)) == CRU_HTONL(0xFE800000))

#define IN6_IS_ADDR_SITELOCAL(a)      \
   (((a).u4_addr[0] & CRU_HTONL(0xFFC00000)) == CRU_HTONL(0xFFC00000))

#define IS_ADDR_UNICAST(a)    \
   (((a).u4_addr[0] & 0x20000000) == 0x20000000)

#define IN6_IS_ADDR_LOOPBACK(a)    \
            ( (a).u4_addr[3] == 0x00000001 && \
              (a).u4_addr[2] == 0 && \
     (a).u4_addr[1] == 0 && \
              (a).u4_addr[0] == 0)

#define  V6_ADDR_INIT_CMP  IS_ADDR_IN6ADDRANYINIT 

/* Macros used to replace hardcoded values*/
#define   SLI_TWO        2

/* in case of ping data, IP header + ICMP header will be extra. So add 20 + 8 bytes in the macro along with data size.
 * Also to cater default size of ping can be 2080, add extra spaces from default 2000 to additional following items
 * 20 (IP header) + 8 (ICMP header) + 80 (to handle extra data bytes)
 */
#define SLI_MAX_PKT_LEN        (CFA_ENET_MTU + 108)

#define SLI_FD_ARR_BLOCK_SIZE  (SLI_THREE * SLI_MAX_FD * sizeof (UINT4))
#endif
