/** $Id: slitdfs.h,v 1.11 2012/11/26 11:54:40 siva Exp $ */
/********************************************************************
 *                                                                  *
 * $RCSfile: slitdfs.h,v $
 *                                                                  *
 * $Date: 2012/11/26 11:54:40 $                                     *
 *                                                                  *
 * $Revision: 1.11 $                                                 *
 *                                                                  *
 *******************************************************************/

/*
 * +--------------------------------------------------------------------------
 * | Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * |
 * | Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * |
 * | FILE NAME               : slitdfs.h
 * |
 * | PRINCIPAL AUTHOR        : Shankar Vasudevan
 * |
 * | SUBSYSTEM NAME          : TCP
 * |
 * | MODULE NAME             : SLI
 * |
 * | LANGUAGE                : C
 * |
 * | TARGET ENVIRONMENT      : ANY
 * |
 * | DATE OF FIRST RELEASE   : 12-01-2000
 * |
 * | DESCRIPTION             : This file contains the data structure
 * |                           definitions for the TCP/UDP/IP SLI.
 ------------------------------------------------------------------------*/
#ifndef __SLITDFS_H__
#define __SLITDFS_H__

/* definition of HashTable */
typedef tTMO_SLL tSliHashTable;

/* definition of Raw socket node */
typedef struct {
    tTMO_SLL_NODE          NextNodeInQueue;   /* next node of raw pkt Q   */
    tCRU_BUF_CHAIN_HEADER  *pRawPkt;          /* pointer to raw packet    */ 
    UINT4                  u4ContextId;       /* Context Identifier */
    UINT2                  u2IpHdrLen;        /* Ip header len in raw pkt */
    UINT2                  u2ReservedWord;    /* for byte alignment       */
} tSliRawRecvQnode;

/* This structure will be used to copy data into the buffer as IP header
 * when the IP_HDRINCL option is specified for RAW sockets */

typedef struct {
    UINT1  u1VHlen;   /* Ip version (4) and header length */
    UINT1  u1TOS;     /* IP type of service */
    UINT2  u2Totlen;  /* Total length  IP header + DATA */
    UINT2  u2Id;      /* Identification */
    UINT2  u2FlOffs;  /* Flags + fragment offset */
    UINT1  u1TTL;     /* Time to live */
    UINT1  u1Proto;   /* Protocol */
    UINT2  u2Cksum;   /* Checksum value */
    UINT4  u4Src;     /* Source address */
    UINT4  u4Dst;     /* Destination address */
} tSliRawIpHdrInfo;

typedef struct SLI_SLL_Node
{
    tTMO_SLL_NODE  *pNextNode;   /* pointer to the next node */
    void           *pReplyPkt;  /* Data of the current node */
} tSliSllNode;

typedef struct {
    tCRU_BUF_CHAIN_HEADER * pBuf; /* Received Packet buffer */
    tIpAddr  IpSrcAddr;          /* Address of the originator */
    tIpAddr  IpDstAddr;          /* final destnation address  */
    UINT4    u4IfIndex;          /* logical interface index   */
    UINT4    u4ContextId;        /* Context Identifier of VRF */
    UINT2    u2SrcPort;          /* originators port value    */
    UINT2    u2DstPort;          /* Destination port in pkt   */
    INT4     i4unicastHlmt;      /* hop limit in the IP pkt   */
    UINT2    u2Len;              /* length of the data gram   */
    UINT1    au1Padding[2];           /* added for byted alignment */
} tSliUdp6Parms;

typedef struct _LL_IP6_PARAMETERS { 
       UINT1 u1Cmd; 
       UINT1 u1LinkType; 
       UINT1 u1Dos;      /* Flag for Denial of Service */ 
       UINT1 u1Reserved; /* For Alignment */ 
       UINT4 u4Port; 
} tSliRawLlToIp6Params;  /* Currently this struct is in ip6/ip6core/ip6sys.h, 
                           * needs to be exported to inc/ipv6.h 
                           */ 
typedef struct SelectNode
{
    tTMO_SLL_NODE    *pNextNode;         /* Next node in the list    */
    tOsixTaskId       u4TaskId;          /* ID of the polling task */
    SliFdSet         *pReadFd;           /* Read Fd Sset             */
    SliFdSet         *pWriteFd;          /* Write Fd Set             */
    SliFdSet         *pExceptFd;         /* Exceptional Fd Set       */
} tSelectNode;

typedef struct
{
    tTMO_SLL_NODE    NextNodeInList;     /* Next Raw hash node       */
    INT4             i4SockDesc;         /* Socket desc of the node  */
    tSdtEntry       *pRawSk;             /* pointer to the socket    */
} tSliRawHashNode;

typedef struct _tSliDescTblBlock
{
    VOID             *pSliDescTblBlock[MAX_NO_OF_SOCKETS];
}tSliDescTblBlock;

typedef struct _tSliIpOptionBlock
{
    UINT1             au1SliIpOptionBlock[MAX_IP_OPT_LEN];
}tSliIpOptionBlock;

typedef struct _tSliTempBufBlock
{
    UINT1             au1SliTempBufBlock[SLI_MAX_PKT_LEN];
}tSliTempBufBlock;

typedef struct _tSliFdArrBlock
{
    UINT1             au1SliFdArrBlock[SLI_FD_ARR_BLOCK_SIZE];
}tSliFdArrBlock;

#endif

