/* $Id: sliport.h,v 1.14 2011/12/03 07:31:06 siva Exp $*/
/*
 * +--------------------------------------------------------------------------
 * | Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * |
 * | Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * |
 * | FILE NAME               : sliport.h
 * |
 * | PRINCIPAL AUTHOR        : Shankar Vasudevan
 * |
 * | SUBSYSTEM NAME          : TCP
 * |
 * | MODULE NAME             : SLI
 * |
 * | LANGUAGE                : C
 * |
 * | TARGET ENVIRONMENT      : ANY
 * |
 * | DATE OF FIRST RELEASE   : 12-01-2000
 * |
 * | DESCRIPTION             : This file contains wrappers for CRU, TMO
 * |                           library function calls.
 * +--------------------------------------------------------------------------
 * |
 * | CHANGE RECORD:
 * |
 * +---------+---------+--------------------+---------------------------------
 * | Version |  Date   |       Author       |       Description of change
 * +---------+---------+--------------------+---------------------------------
 * |  -      |Jan 2000 | Shankar Vasudevan  | Created Original
 * ------------------------------------------------------------------------*/
/*     1.1      Saravanan.M    Implementation of                           */
/*              Purush            - Non Blocking Connect                   */ 
/*                                - Non Blocking Accept                    */
/*                                - IfMsg Interface between TCP and IP     */
/*                                - One Queue per Socket                   */
/* ------------------------------------------------------------------------*/

#ifndef __SLI_PORT_H__
#define __SLI_PORT_H__

#define SliAllocMemFromPool(PoolId) \
           MemAllocMemBlk(PoolId)

#define SliFreeMemToPool(PoolId, pu1Block) \
           MemReleaseMemBlock((PoolId), (UINT1 *)(pu1Block))

/* SLL related wrappers */
#define SLI_SLL_Get_Count(pList) \
           TMO_SLL_Count((pList))

#define SLI_SLL_First(pList) \
           TMO_SLL_First((pList))

#define SLI_SLL_Delete(pList, pNode) \
           TMO_SLL_Delete((pList), (tTMO_SLL_NODE *)(pNode))

#define SLI_SLL_Next(pList, pNode) \
           TMO_SLL_Next((pList), (tTMO_SLL_NODE *)(pNode))

#define SLI_SLL_Init(pList) \
           TMO_SLL_Init((pList))

#define SLI_SLL_Add(pList, pNode) \
           TMO_SLL_Add((pList), (tTMO_SLL_NODE *)(pNode))

#define SLI_SLL_Scan(x, y, z) \
           TMO_SLL_Scan(x, y, z)

/* Event related wrappers */
#define SLI_Wait_Event(u4Mask, u4Type) \
           TMO_Wait_Event((u4Mask), (u4Type))

#define SLI_Send_Event(u4TaskId, u4Event) \
           TMO_Send_Event((u4TaskId), (u4Event))

/* Buffer Related wrappers */
#define SLI_Alloc_Buffer(size, offset) \
        CRU_BUF_Allocate_MsgBufChain((size), (offset))

#define SLI_Release_Buffer(pBuf, force) \
        CRU_BUF_Release_MsgBufChain((pBuf), (force))

#define  SLI_GET_BUF_LENGTH(pBuf) \
         CRU_BUF_Get_ChainValidByteCount((pBuf))


#define SLI_PUT_1_BYTE(pBuf, value, offset) \
{\
   UINT1 au1BufByte;\
   CRU_BUF_Copy_OverBufChain(pBuf, \
                             (au1BufByte = (value), \
                             &au1BufByte), \
                             (offset), 1);\
}

#define SLI_GET_2_BYTE(pBuf, value, offset)\
{\
   UINT2  u2Val = 0; \
   CRU_BUF_Copy_FromBufChain((pBuf), \
                             (UINT1 *)&(u2Val), \
                             (offset), 2);\
   value = CRU_NTOHS(u2Val); \
}

#define SLI_BUF_Copy_OverBufChain(pmsg, psrc, offset, size) \
        CRU_BUF_Copy_OverBufChain((pmsg), (psrc), (offset), (size))

#define IP6_PKT_MOVE_TO_DATA(pBufChnHdr) CRU_BUF_Move_ValidOffset(pBufChnHdr,40)

#define SLI_DELAY_TASK(x) (OsixTskDelay(x))

#define SLI_STOP_TIMER(x,y)    (TmrStopTimer(x,y))

        
#define SLI_SELECT_WAIT_EVENTS SLI_UDP_WAIT_EVENT | \
                               SLI_UDP6_WAIT_EVENT | \
                               SLI_RAW_PKT_ARRIVAL_EVENT

#define  SLI_SOCKET_SEM_COUNT     1                                       
#define  SLI_SOCKET_SEM_NAME      (const UINT1 *)"SLIS"                                  
#define  SLI_SOCKET_SEM_FLAGS     OSIX_DEFAULT_SEM_MODE                   

#define  SLI_RAWHP_SEM_NAME       (const UINT1 *)"SLIR"

#define  SLI_INPUT_FWD_Q_NAME     IPFW_PACKET_ARRIVAL_Q_NAME              

#define  SLI_RAWHL_SEM_NAME       (const UINT1 *)"SLRA"

#define  SLI_WRITE_SEM_NAME       (const UINT1 *)"WSem"

#define  SLI_Q_DEPTH              4000                                      
#define  SLI_Q_MODE               OSIX_GLOBAL                             
   
#define  SLI_EVENT_WAIT_FLAGS     (OSIX_WAIT | OSIX_EV_ANY)               
#define  SLI_EVENT_WAIT_TIMEOUT   0                                       

#define  SLI_CLOCK_TICK           SYS_NUM_OF_TIME_UNITS_IN_A_SEC          
#define  SLI_DEFAULT_IF           1

#define  SLI_ERR(x)              errno=x;     

#define SLI_INIT_COMPLETE(u4Status)  lrInitComplete(u4Status)
 
#endif
