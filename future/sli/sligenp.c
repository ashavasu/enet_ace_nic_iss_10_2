/*************************************************************************/
/*  FILE NAME             : sligenp.c                                    */
/*  PRINCIPAL AUTHOR      : Sri. Chellappa.                              */
/*  SUBSYSTEM NAME        : SLI                                          */
/*  MODULE NAME           : Socket Layer Interface                       */
/*  LANGUAGE              : C                                            */
/*  TARGET ENVIRONMENT    :                                              */
/*  DATE OF FIRST RELEASE :                                              */
/*  DESCRIPTION           : This file contains all the general purpose   */
/*                          used by FutureTCP/UDP's SLI.                 */
/*  $Id: sligenp.c,v 1.72 2017/12/29 09:27:48 siva Exp $*/
/* --------------------------------------------------------------------- */
/*   VERSION  AUTHOR/DATE    DESCRIPTION OF CHANGE                       */
/* --------------------------------------------------------------------- */
/*     1.5    Rebecca Rufus    Enabling of Blocking/Non-Blocking made    */
/*                             Dynamic and hence removed                 */
/*                             SLI_TCP_PROC_IF_WANTED switch             */
/*                             Added Procedural/Queue Interface for      */
/*                             both the Blocking and Non-Blocking mode   */
/* ----------------------------------------------------------------------*/
/*     1.6    Rebecca Rufus    Enabled the Get/Set of TCP_ENBL_ICMP_MSGS */
/*             07-Oct-'98.     TCP_TIMEOUT ,TCP_ASY_REPORT,              */
/*                             TCP_SECURITY, TCP_TOS options.            */
/*                             Refer Problem Id 9.                       */
/* ----------------------------------------------------------------------*/
/*     1.7    N.R.Soma       + New function SliValidateSocket is added.  */
/*            18-MAR-99      + Functions names are changed.              */
/*                           + Function SliRegisterTaskId is modified    */
/*                             to support UDP.                           */
/*                           + Fixed bug and memory leakage in SLI-TCP   */
/*                             Interface.                                */
/*                           ->Above changes are as per RFC chreq02      */
/* ----------------------------------------------------------------------*/
/*     1.8    Bhuvana        + Some of the API's are defined within the  */
/*            25-Dec-99        switch TCP_WANTED                         */
/*                           + In function SliTcpIssueOpenRequest        */
/*                             local ip addr and remote ip  addr are     */
/*                             converted to host order                   */
/* --------------------------------------------------------------------- */
/*      -     Shankar V      + Added SOCK_RAW support.                   */
/* --------------------------------------------------------------------- */
/*      -     Ramakrishnan   + Added support for select call. Available  */
/*                             only under the ST_ENHLISTINTF compilation */
/*                             switch.                                   */
/* --------------------------------------------------------------------- */
/*     1.11     Saravanan.M    Implementation of                         */
/*              Purush            - Non Blocking Connect                 */
/*                                - Non Blocking Accept                  */
/*                                - One Queue per Socket                 */
/*************************************************************************/

#include "sliinc.h"

#include "tcp.h"
#ifdef TCP_WANTED
#include "tcpinc.h"
#endif
extern tMemPoolId   SdtMemPoolId;
extern tMemPoolId   gSliMemPoolId;
extern tTMO_SLL     gSelectTable;
extern tMemPoolId   gSliAcceptListPoolId;
extern tMemPoolId   gSliIpOptionPoolId;
extern tMemPoolId   gSliTempBufPoolId;
extern tMemPoolId   gSliFdArrPoolId;
extern tMemPoolId   gSliMd5NodePoolId;
extern tMemPoolId   gSliTcpAoNodePoolId;
extern tOsixSemId   gSdtSemId;
extern INT4         i4SocketsInUse;
int                 zero = SLI_ONE, ten = SLI_ONE, hun = SLI_ONE;

#ifndef __UTILRAND__
#define __UTILRAND__
extern INT4         UtilRand (VOID);
#endif

tTmrAppTimer        gReference[MAX_NO_OF_SOCKETS + 1];
tTimerListId        gTimerListId[MAX_NO_OF_SOCKETS + 1];

/**************************************************************************/
/*  Function Name   : SliLookup                                           */
/*  Description     : This function looks up into the socket desc         */
/*                    table for the specified socket.                     */
/*  Input(s)        :                                                     */
/*                    i4SockDesc - The socket to search for.              */
/*  Output(s)       :                                                     */
/*                  : None.                                               */
/*  Returns         :                                                     */
/*                    TRUE - The Requested Sock Desc Was found.           */
/*                    FALSE - The Requested Sock Desc Was not found.      */
/**************************************************************************/
INT4
SliLookup (INT4 i4SockDesc)
{
    tSdtEntry          *CpsdtSock = NULL;

    if ((i4SockDesc < SLI_FIRST_SOCKET_FD)
        || (i4SockDesc >= (INT4) MAX_NO_OF_SOCKETS))
    {
        return FALSE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];

    if ((CpsdtSock != (tSdtEntry * const) NULL))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

#ifdef TCP_WANTED
/**************************************************************************/
/*  Function Name   : SliTcpFindSockDesc()                                */
/*  Description     : This function looks up into the socket desc         */
/*                    table for a socket entry matching the specified     */
/*                    TCP connection ID.                                  */
/*  Input(s)        :                                                     */
/*                    u1TcpConnid - The Connection ID matching which      */
/*                                  a socket entry has to be found.       */
/*  Output(s)       : None.                                               */
/*  Returns         : The socket entry corresponding to the Connection    */
/*                    ID specified.                                       */
/**************************************************************************/
INT4
SliTcpFindSockDesc (UINT4 u4TcpConnid)
{
    return TCBtable[u4TcpConnid].i4SockDesc;
}
#endif

/**************************************************************************/
/*  Function Name   : SliGetFreeSock()                                    */
/*  Description     : This function gets a free socket for use,           */
/*                    if one is available.                                */
/*  Input(s)        : None.                                               */
/*  Output(s)       : None.                                               */
/*  Returns         : Free sock desc - If one could be found.             */
/*                    SLI_ERR_NO_FREE_SOCKET - Socket descriptor table    */
/*                               is full.                                 */
/**************************************************************************/
INT4
SliGetFreeSock (VOID)
{
    INT4                i4Count = SLI_ZERO;

    for (i4Count = SLI_FIRST_SOCKET_FD;
         ((i4Count < (INT4) MAX_NO_OF_SOCKETS) &&
          (SOCK_DESC_TABLE[i4Count] != NULL)); i4Count++)
    {
        /* empty */
    }
    if (i4Count >= (INT4) MAX_NO_OF_SOCKETS)
    {
        return SLI_EMFILE;
    }
    else
    {
        return i4Count;
    }
}

/****************************************************************************/
/*  Function Name   : SliDestroySocket                                      */
/*  Description     : This function frees the socket entry identified       */
/*                    by the socket descriptor specified.                   */
/*  Input(s)        : i4SockDesc - The socket identifier.                   */
/*  Output(s)       : None.                                                 */
/*  Returns         : None.                                                 */
/****************************************************************************/
VOID
SliDestroySocket (INT4 i4SockDesc)
{
    tSdtEntry          *const CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
#ifdef TCP_WANTED
    tSliSllNode        *pAcceptNode = NULL;
#endif
    tSelectNode        *pSelectNode = NULL;
    INT4                i4DelSelFd = 0;
    tOsixSemId          ProtectSemId = 0;
    tOsixSemId          ConnSemId = 0;
    tSliMd5SA          *pMd5SA = NULL;
    tTcpToApplicationParms *PtaPRetParms = NULL;
    /* Check if the socket is present in the select table. If so remove
     * the same. If the FD list becomes NULL then we need to remove the
     * select node itself.
     */

    SLI_SELECT_ENTER_CS ();
    TMO_SLL_Scan (&gSelectTable, pSelectNode, tSelectNode *)
    {
        if ((pSelectNode->pReadFd != NULL) &&
            (SLI_FD_ISSET (i4SockDesc, pSelectNode->pReadFd)))
        {
            SLI_FD_CLR (i4SockDesc, pSelectNode->pReadFd);
            i4DelSelFd = 1;
        }
        if ((pSelectNode->pWriteFd != NULL) &&
            (SLI_FD_ISSET (i4SockDesc, pSelectNode->pWriteFd)))
        {
            SLI_FD_CLR (i4SockDesc, pSelectNode->pWriteFd);
            i4DelSelFd = 1;
        }
        if ((pSelectNode->pExceptFd != NULL) &&
            (SLI_FD_ISSET (i4SockDesc, pSelectNode->pExceptFd)))
        {
            SLI_FD_CLR (i4SockDesc, pSelectNode->pExceptFd);
            i4DelSelFd = 1;
        }
        if (i4DelSelFd == 1)
        {
            break;
        }
    }
    SLI_SELECT_EXIT_CS ();

    if (CpsdtSock == NULL)
    {
        return;
    }

    if (CpsdtSock->i2SdtSockType == SOCK_DGRAM)
    {
        /* In failure case, queue might not be created.So to check it */
        if (CpsdtSock->UdpQId != 0)
        {
            OsixQueDel (CpsdtSock->UdpQId);
        }
    }

    ProtectSemId = CpsdtSock->ProtectSemId;
    ConnSemId = CpsdtSock->ConnSemId;
#ifdef TCP_WANTED
    /* SLL delete should be done here */
    if (CpsdtSock->pAccept)
    {
        /* Free all the node to  MemPool */
        while ((pAcceptNode = (tSliSllNode *) TMO_SLL_Get (CpsdtSock->pAccept)))
        {
            PtaPRetParms = (tTcpToApplicationParms *) pAcceptNode->pReplyPkt;
            if (PtaPRetParms != NULL)
            {
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
            }

            SliFreeMemToPool (gSliMemPoolId, pAcceptNode);
        }
        SliFreeMemToPool (gSliAcceptListPoolId, CpsdtSock->pAccept);
    }
    if (CpsdtSock->pAsyncParams)
    {
        FREE_TCP_TO_APPLICATION_PARMS (CpsdtSock->pAsyncParams);
    }
    if (CpsdtSock->pConnect)
    {
        FREE_TCP_TO_APPLICATION_PARMS (CpsdtSock->pConnect);
    }
#endif
    /* MD5 SA list delete */
    while ((pMd5SA = (tSliMd5SA *) TMO_SLL_Get (&(CpsdtSock->Md5SAList))))
    {
        SliFreeMemToPool (gSliMd5NodePoolId, pMd5SA);
    }

    if (CpsdtSock->IpOption.pu1Option)
    {
        SliFreeMemToPool (gSliIpOptionPoolId, (CpsdtSock->IpOption.pu1Option));
    }

    if ((SliFreeMemToPool (SdtMemPoolId, CpsdtSock)) != MEM_SUCCESS)
    {
        return;
    }
    OsixSemTake (gSdtSemId);
    SOCK_DESC_TABLE[i4SockDesc] = NULL;
    i4SocketsInUse--;

    /* Delete Protected Semaphore created in Socket API */
    if (ProtectSemId != 0)
    {
        OsixSemGive (ProtectSemId);
        OsixSemDel (ProtectSemId);
    }

    /* Delete Connection Semaphore created in Socket API */
    if (ConnSemId != 0)
    {
        OsixSemGive (ConnSemId);
        OsixSemDel (ConnSemId);
    }
    OsixSemGive (gSdtSemId);
}

#ifdef TCP_WANTED
/**************************************************************************/
/*  Function Name   : SliTcpIssueRegAsyncRequest                          */
/*  Description     : This function enqueues a Register Async handler     */
/*                    to TCP.                                             */
/*  Input(s)        :                                                     */
/*                    i4SockDesc - The socket identifier.                 */
/*                    AsyncHandler- The Asynchrnous Message handler.      */
/*  Output(s)       : None.                                               */
/*  Returns         : SUCCESS - If Sucessful.                             */
/*                    FAILURE - The Close Request to TCP could not be     */
/*                              issued.                                   */
/**************************************************************************/
INT4
SliTcpIssueRegAsyncRequest (INT4 i4SockDesc,
                            VOID (*AsyncHandler) (INT4, INT1 *))
{
    tSdtEntry          *const CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tApplicationToTcpParms PatPParms;

    MEMSET (&PatPParms, 0, sizeof (PatPParms));
    PatPParms.u1Cmd = REG_ASYNC_HANDLER;
    PatPParms.u4ContextId = CpsdtSock->u4TxContextId;
    PatPParms.cmdtype.RegAsyncHandler.u4ConnId = CpsdtSock->u4ConnId;
    PatPParms.cmdtype.RegAsyncHandler.fpTcpHandleAsyncMesg = AsyncHandler;
    if (TakeTCBmainsem (PatPParms.cmdtype.RegAsyncHandler.u4ConnId) !=
        OSIX_SUCCESS)
    {
        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
        SLI_ERR (SLI_EMEMFAIL);
        return SLI_FAILURE;
    }

    HliRegAsyncHandler (&PatPParms.cmdtype.RegAsyncHandler, &PatPParms);
    GiveTCBmainsem (PatPParms.cmdtype.RegAsyncHandler.u4ConnId);
    return SLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : SliTcpIssueCloseRequest.                            */
/*  Description     : This function enqueues a close request to TCP.      */
/*  Input(s)        : i4SockDesc - The socket identifier.                 */
/*  Output(s)       : None.                                               */
/*  Returns         : TcpToApplicationParms - If Sucessful.               */
/*                    NULL - The Close Request to TCP could not be issued */
/**************************************************************************/
tTcpToApplicationParms *
SliTcpIssueCloseRequest (INT4 i4SockDesc)
{
    tSdtEntry          *const CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tApplicationToTcpParms PatPParms;
    tTcpToApplicationParms *pRetParms = NULL;

    MEMSET (&PatPParms, 0, sizeof (PatPParms));

    PatPParms.u1Cmd = CLOSE_CMD;
    PatPParms.u4ContextId = CpsdtSock->u4TxContextId;
    PatPParms.cmdtype.close.u4ConnId = CpsdtSock->u4ConnId;

    if (TCBtable[PatPParms.cmdtype.close.u4ConnId].MainSemId == 0)
    {
        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
        SLI_ERR (SLI_EMEMFAIL);
        return NULL;
    }
    OsixSemGive (CpsdtSock->ProtectSemId);
    pRetParms = HliClose (&PatPParms.cmdtype.close, &PatPParms);

    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SLI_ERR (SLI_ENOBUFS);
        FREE_TCP_TO_APPLICATION_PARMS (pRetParms);
        return NULL;
    }
    return pRetParms;
}

/**************************************************************************/
/*  Function Name   : SliTcpIssueAbortRequest                             */
/*  Description     : This function enqueues an abort request to          */
/*                    TCP.                                                */
/*  Input(s)        : i4SockDesc - The socket identifier.                 */
/*  Output(s)       : None.                                               */
/*  Returns         : TcpToApplicationParms - If Sucessful.               */
/*                    NULL - The Abort Request to TCP could not be issued */
/**************************************************************************/
tTcpToApplicationParms *
SliTcpIssueAbortRequest (INT4 i4SockDesc)
{
    tSdtEntry          *const CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tApplicationToTcpParms PatPParms;
    tTcpToApplicationParms *pRetParms = NULL;

    MEMSET (&PatPParms, 0, sizeof (PatPParms));

    PatPParms.u1Cmd = ABORT_CMD;
    PatPParms.u4ContextId = CpsdtSock->u4TxContextId;
    PatPParms.cmdtype.abort.u4ConnId = CpsdtSock->u4ConnId;
    if (TakeTCBmainsem (PatPParms.cmdtype.abort.u4ConnId) != OSIX_SUCCESS)
    {
        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
        SLI_ERR (SLI_EMEMFAIL);
        return (tTcpToApplicationParms *) NULL;
    }
    pRetParms = HliAbort (&PatPParms.cmdtype.abort, &PatPParms);
    GiveTCBmainsem (PatPParms.cmdtype.abort.u4ConnId);
    return pRetParms;

}

/**************************************************************************/
/*  Function Name   : SliTcpIssueOptionRequest                            */
/*  Description     : This function enqueues a option request to TCP.     */
/*  Input(s)        :                                                     */
/*                    i4SockDesc - The socket identifier.                 */
/*                    u1Optiontype - Specifies whether to get or          */
/*                                   set the option.                      */
/*                    u2OptName - The option to get/set.                  */
/*                    u1Optlen - The bytelength of the option value.      */
/*                    u8Value - The value of the option.                  */
/*  Output(s)       : None.                                               */
/*  Returns         : TcpToApplicationParms - If Sucessful.               */
/*                    NULL -The Option Request to TCP could not be issued */
/**************************************************************************/
tTcpToApplicationParms *
SliTcpIssueOptionRequest (INT4 i4SockDesc, UINT1 u1Optiontype, UINT2 u2OptName,
                          UINT1 u1Optlen, AR_UINT8 u8Value)
{
    tSdtEntry          *const CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tApplicationToTcpParms PatPParms;
    tTcpToApplicationParms *pRetParms = NULL;
    UINT4               u4LocalOption;

    MEMSET (&PatPParms, 0, sizeof (PatPParms));

    /* Map the SLI Option to TCP Option */
    switch (u2OptName)
    {
        case TCP_MAXSEG:
            u4LocalOption = TCPO_MSS;
            break;
        case SO_SNDBUF:
            u4LocalOption = TCPO_SENDBUF;
            break;
        case SO_RCVBUF:
            u4LocalOption = TCPO_RCVBUF;
            break;
        case SO_OOBINLINE:
            u4LocalOption = TCPO_OOBINLINE;
            break;
        case SO_TYPE:
            u4LocalOption = SO_TYPE;
            break;
        case IP_OPTIONS:
            u4LocalOption = TCPO_IPOPTION;
            break;
        case TCP_NODELAY:
            u4LocalOption = TCPO_NODELAY;
            break;
        case TCP_TOS:
            u4LocalOption = TCPO_TOS;
            break;
        case TCP_SECURITY:
            u4LocalOption = TCPO_SECURITY;
            break;
        case TCP_ASY_REPORT:
            u4LocalOption = TCPO_ASY_REPORT;
            break;
        case TCP_TIMEOUT:
            u4LocalOption = TCPO_TIMEOUT;
            break;
        case TCP_ENBL_ICMP_MSGS:
            u4LocalOption = TCPO_ENBL_ICMP_MSGS;
            break;
        case SO_KEEPALIVE:
            u4LocalOption = TCPO_KEEPALIVE;
            break;
        case SO_LINGER:
            u4LocalOption = TCPO_SO_LINGER;
            break;
        case SO_REUSEADDR:
            u4LocalOption = TCPO_SO_REUSEADDR;
            break;
        case IP_TTL:
        case IPV6_UNICAST_HOPS:
        {
            CpsdtSock->i4unicasthlmt = (UINT1) u8Value;
            u4LocalOption = TCPO_UNICAST_HOPS;
            PatPParms.cmdtype.option.u1Ttl = (UINT1) u8Value;
            break;
        }
        case TCP_MD5SIG:
            u4LocalOption = TCPO_MD5SIG;
            break;
        case TCP_AO_SIG:
            u4LocalOption = TCPO_AO;
            break;
        case TCP_AO_NOMKT_MCH:
            u4LocalOption = TCPO_AO_MKTMCH;
            break;
        case TCP_AO_ICMP_ACC:
            u4LocalOption = TCPO_AO_ICMP;
            break;
        default:                /* Return Error params */
            if ((pRetParms = ALLOCATE_TCP_TO_APPLICATION_PARMS ()) == NULL)
            {
                CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                SLI_ERR (SLI_EMEMFAIL);
                return (tTcpToApplicationParms *) NULL;
            }
            pRetParms->u1Cmd = ERROR_MSG;
            pRetParms->u4ConnId = CpsdtSock->u4ConnId;
            pRetParms->cmdtype.error.u1ErrorCmd = OPTIONS_CMD;
            pRetParms->cmdtype.error.i1ErrorCode = SLI_ENOPROTOOPT;
            return pRetParms;
    }

    PatPParms.u1Cmd = OPTIONS_CMD;
    PatPParms.u4ContextId = CpsdtSock->u4TxContextId;
    PatPParms.cmdtype.option.u4ConnId = CpsdtSock->u4ConnId;
    PatPParms.cmdtype.option.u1Flag = u1Optiontype;
    PatPParms.cmdtype.option.u4Option = u4LocalOption;
    PatPParms.cmdtype.option.u1Optlen = u1Optlen;
    PatPParms.cmdtype.option.u8Value = u8Value;
    PatPParms.cmdtype.option.u1Ttl = u8Value;

    if (TakeTCBmainsem (PatPParms.cmdtype.option.u4ConnId) != OSIX_SUCCESS)
    {
        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
        SLI_ERR (SLI_EMEMFAIL);
        return (tTcpToApplicationParms *) NULL;
    }
    pRetParms = HliOptions (&PatPParms.cmdtype.option, &PatPParms);
    GiveTCBmainsem (PatPParms.cmdtype.option.u4ConnId);
    return pRetParms;
}

/**************************************************************************/
/*  Function Name   : SliTcpIssueWriteRequest                             */
/*  Description     : This function enqueues a write request to TCP.      */
/*  Input(s)        :                                                     */
/*                    i4SockDesc - The socket identifier.                 */
/*                    Cpi1WriteBuf - Buffer containing the data to be     */
/*                                   written.                             */
/*                    i4NumToWrite - Number of bytes to wtite.            */
/*                    u1Flag - Flag to specify urgent/normal data         */
/*                    u4Timeout - The timeout value.                      */
/*  Output(s)       : None.                                               */
/*  Returns         : TcpToApplicationParms - If Sucessful.               */
/*                    NULL - The Write Request to TCP could not be issued.*/
/**************************************************************************/
tTcpToApplicationParms *
SliTcpIssueWriteRequest (INT4 i4SockDesc, INT1 *Cpi1WriteBuf, INT4 i4NumToWrite,
                         UINT1 u1Flag, UINT4 u4Timeout)
{
    tApplicationToTcpParms PatPParms;
    tTcpToApplicationParms *pRetParms = NULL;
    tSdtEntry          *const CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];

    MEMSET (&PatPParms, 0, sizeof (PatPParms));
    if (CpsdtSock == NULL)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return NULL;
    }

    PatPParms.u1Cmd = SEND_CMD;
    PatPParms.u4ContextId = CpsdtSock->u4TxContextId;
    PatPParms.cmdtype.send.u4ConnId = CpsdtSock->u4ConnId;
    PatPParms.cmdtype.send.pBuf = (UINT1 *) Cpi1WriteBuf;
    PatPParms.cmdtype.send.u2BufLen = (UINT2) i4NumToWrite;
    PatPParms.cmdtype.send.u1Flag = u1Flag;
    PatPParms.cmdtype.send.u4Timeout = u4Timeout;

    if (CpsdtSock->u4ValidOptions & SO_DONTROUTE)
    {
        if (CpsdtSock->i2SdtFamily == AF_INET)
        {
            PatPParms.cmdtype.send.u4IfIndex = CpsdtSock->inPktinfo.ipi_ifindex;
        }
        else if (CpsdtSock->i2SdtFamily == AF_INET6)
        {
            PatPParms.cmdtype.send.u4IfIndex =
                CpsdtSock->in6Pktinfo.ipi6_ifindex;
        }
    }
    else
    {
        PatPParms.cmdtype.send.u4IfIndex = CpsdtSock->u4IfIndex;
    }

    if ((TCBtable[PatPParms.cmdtype.send.u4ConnId].MainSemId == 0) ||
        ((TakeTCBmainsem (PatPParms.cmdtype.send.u4ConnId)) != OSIX_SUCCESS))
    {
        PatPParms.cmdtype.send.pBuf = NULL;
        /* context switching could have happened
         * and socket could have been closed
         */
        if (CpsdtSock == NULL)
        {
            SLI_ERR (SLI_ENOTSOCK);
            return NULL;
        }

        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
        SLI_ERR (SLI_EMEMFAIL);
        return NULL;
    }
    pRetParms = HliWrite (&PatPParms.cmdtype.send, &PatPParms);
    GiveTCBmainsem (PatPParms.cmdtype.send.u4ConnId);
    return pRetParms;
}

/**************************************************************************/
/*  Function Name   : SliTcpRcvData                                       */
/*  Description     : This function waits for any urgent data that        */
/*                    might have been sent by the peer.                   */
/*  Input(s)        :                                                     */
/*                    i4SockDesc - The socket identifier.                 */
/*                    Parms  - Details from application to TCP            */
/*                    i4WhatData - Urgent?? Normal??                      */
/*                    Queue - UrgentQ?? ReceiveQ??                        */
/*  Output(s)       : pi4NumRcvd - The number of bytes received.          */
/*  Returns         : SLI-SUCCESS - If Sucessful.                         */
/*                    SLI-FAILURE - The Read Request to TCP failed.       */
/*                    SLI_EWOULDBLOCK - No data has been Received         */
/**************************************************************************/
INT4
SliTcpRcvData (INT4 i4SockDesc, tApplicationToTcpParms Parms,
               INT4 *pi4NumRcvd, INT4 i4WhatData)
{
    tSdtEntry          *const CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    INT4                i4RetError = 0;
    tTcpToApplicationParms RetParms;

    UNUSED_PARAM (i4WhatData);

    if (TakeTCBmainsem (Parms.cmdtype.receive.u4ConnId) != OSIX_SUCCESS)
    {
        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
        SLI_ERR (SLI_EMEMFAIL);
        return SLI_FAILURE;
    }
    if ((i4RetError = TcpSliReceiveData (&Parms, &RetParms)) != SLI_SUCCESS)
    {
        GiveTCBmainsem (Parms.cmdtype.receive.u4ConnId);
        CpsdtSock->i1ErrorCode = (UINT1) i4RetError;
        if (i4RetError == TCPE_RESET)
        {
            SLI_ERR (SLI_ENOTCONN);
        }
        else
        {
            SLI_ERR (i4RetError);
        }
        return (SLI_FAILURE);
    }

    GiveTCBmainsem (Parms.cmdtype.receive.u4ConnId);
    *pi4NumRcvd = RetParms.cmdtype.receive.u2BufLen;
    return SLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : SliTcpIssueStatusRequest                            */
/*  Description     : This function enqueues a status request to TCP.     */
/*  Input(s)        : i4SockDesc - The socket identifier.                 */
/*  Output(s)       : None.                                               */
/*  Returns         : TcpToApplicationParms - If Sucessful.               */
/*                    NULL - The status Request to TCP could not be issued*/
/**************************************************************************/
tTcpToApplicationParms *
SliTcpIssueStatusRequest (INT4 i4SockDesc)
{
    tSdtEntry          *const CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tApplicationToTcpParms PatPParms;
    tTcpToApplicationParms *pRetParms = NULL;

    MEMSET (&PatPParms, 0, sizeof (PatPParms));
    PatPParms.u1Cmd = STATUS_CMD;
    PatPParms.u4ContextId = CpsdtSock->u4TxContextId;
    PatPParms.cmdtype.status.u4ConnId = CpsdtSock->u4ConnId;

    if (TakeTCBmainsem (PatPParms.cmdtype.status.u4ConnId) != OSIX_SUCCESS)
    {
        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
        SLI_ERR (SLI_EMEMFAIL);
        return (tTcpToApplicationParms *) NULL;
    }
    pRetParms = HliStatus (&PatPParms.cmdtype.status, &PatPParms);
    GiveTCBmainsem (PatPParms.cmdtype.status.u4ConnId);
    return pRetParms;

}

/******************************************************************************/
/*  Function Name   : SliTcpProcessReturnMesg                                 */
/*  Description     : This function processes the return message for open     */
/*                        requests.                                           */
/*  Input(s)        :                                                         */
/*                    i4SockDesc : The Socket Identifier.                     */
/*                    PtaPRetParms: TCP to Application Return parameters      */
/*  Output(s)       :                                                         */
/*                    SLI_ERR_COULDNT_OPEN_CONNECTION :conn not established   */
/*  Returns         : SUCCESS - If Sucessful                                  */
/*                    FAILURE - If connection could not be opened.            */
/******************************************************************************/
INT4
SliTcpProcessReturnMesg (INT4 i4SockDesc, tTcpToApplicationParms * PtaPRetParms)
{
    tSdtEntry          *const CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if (PtaPRetParms == NULL)
    {
        return SLI_FAILURE;
    }

    if (PtaPRetParms->u1Cmd == ERROR_MSG)
    {
        CpsdtSock->i1ErrorCode = PtaPRetParms->cmdtype.error.i1ErrorCode;
        SLI_ERR (PtaPRetParms->cmdtype.error.i1ErrorCode);
        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
        /* set the appropriate error
           SLI_ERR (SLI_ERR_COULDNT_OPEN_CONNECTION); */
        return SLI_FAILURE;
    }
    V6_ADDR_COPY (&(CpsdtSock->LocalIpAddr),
                  &PtaPRetParms->cmdtype.conn.LocalIp);
    CpsdtSock->u2LocalPort = PtaPRetParms->cmdtype.conn.u2LocalPort;
    V6_ADDR_COPY (&CpsdtSock->RemoteIpAddr,
                  &PtaPRetParms->cmdtype.conn.RemoteIp);
    CpsdtSock->u2RemotePort = PtaPRetParms->cmdtype.conn.u2RemotePort;
    CpsdtSock->u4ConnId = PtaPRetParms->u4ConnId;
    CpsdtSock->state = SS_CONNECTED;

    FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
    return SLI_SUCCESS;
}
#endif /* TCP_WANTED */

/**************************************************************************/
/*  Function Name   : SliSetFdZero                                        */
/*  Description     : Sets all descriptors in the fd set to zero          */
/*  Input(s)        : fdset - the fdset which has to be set to zero       */
/*  Output(s)       : fdset.                                              */
/*  Returns         : None.                                               */
/**************************************************************************/
void
SliSetFdZero (SliFdSet * fdset)
{
    UINT4               u4Index = 0;

    for (u4Index = ZERO_COUNT;
         u4Index < (SLI_MAX_FD / (sizeof (UINT4) * BITS_PER_BYTE)); u4Index++)
    {
        (*fdset)[u4Index] = (UINT4) ZERO;
    }
}

/**************************************************************************/
/*  Function Name   : SliValidateSocket                                   */
/*  Description     : Validates parameters passed to create a socket.     */
/*  Input(s)        : i4Family - Protocol family.                         */
/*                    i4SocketType - The socket type.                     */
/*                    i4Protocol - The transport protocol to use          */
/*  Output(s)       : i4Protocol                                          */
/*  Return Value    : SUCCESS - Validation is successful.                 */
/*                    SLI_EPROTNOSUPP - Protocol is not supported.        */
/*                    SLI_EFAMNOSUPP - Address Family is not supported.   */
/*                    SLI_ESOCKNOSUPP - Socket Type not supported.        */
/**************************************************************************/
INT4
SliValidateSocket (INT4 i4Family, INT4 i4SockType, INT4 *i4Protocol)
{
    INT4                i4RetVal = 0;

    i4RetVal = SLI_SUCCESS;
    if ((i4Family == AF_INET) || (i4Family == AF_INET6))
    {
        switch (i4SockType)
        {
#ifdef TCP_WANTED
            case SOCK_STREAM:
                if (*i4Protocol == IPPROTO_UNSPEC)
                {
                    *i4Protocol = IPPROTO_TCP;
                }
                else if (*i4Protocol != IPPROTO_TCP)
                {
                    i4RetVal = SLI_EPROTNOSUPP;
                }
                break;
#endif
            case SOCK_DGRAM:
                if (*i4Protocol == IPPROTO_UNSPEC)
                {
                    *i4Protocol = IPPROTO_UDP;
                }
                else if (*i4Protocol != IPPROTO_UDP)
                {
                    i4RetVal = SLI_EPROTNOSUPP;
                }
                break;
            case SOCK_RAW:
                if (*i4Protocol == IPPROTO_UNSPEC)
                {
                    *i4Protocol = IPPROTO_RAW;
                }
                else if ((*i4Protocol < IPPROTO_IP)
                         || (*i4Protocol >= IPPROTO_MAX))
                {
                    i4RetVal = SLI_EPROTNOSUPP;
                }
                break;
            default:
                i4RetVal = SLI_ESOCKNOSUPP;
                break;
        }
    }
    else
    {
        i4RetVal = SLI_EFAMNOSUPP;
    }
    return i4RetVal;
}

/****************************************************************************/
/*  Function Name   : SliDefaultBcastAddr                                   */
/*  Description     : Returns the default broadcast address for the given   */
/*                    class of address.                                     */
/*  Input(s)        : u4HostAddr - Ip Adddress.                             */
/*  Output(s)       : None.                                                 */
/*  Returns         : u4Addr - Broadcast address corresponding to u4HostAddr*/
/*****************************************************************************/
UINT4
SliDefaultBcastAddr (UINT4 u4HostAddr)
{
    UINT4               u4Addr = 0;

    if (IP_IS_ADDR_CLASS_A (u4HostAddr))
    {
        u4Addr = (IP_CLASS_A_DEF_BROADCAST_MASK
                  | (IP_CLASS_A_DEF_MASK & u4HostAddr));
    }
    else if (IP_IS_ADDR_CLASS_B (u4HostAddr))
    {
        u4Addr = (IP_CLASS_B_DEF_BROADCAST_MASK
                  | (IP_CLASS_B_DEF_MASK & u4HostAddr));
    }
    else if (IP_IS_ADDR_CLASS_C (u4HostAddr))
    {
        u4Addr = (IP_CLASS_C_DEF_BROADCAST_MASK
                  | (IP_CLASS_C_DEF_MASK & u4HostAddr));
    }
    else
    {
        u4Addr = IP_ANY_ADDR;
    }

    return u4Addr;
}

/****************************************************************************/
/*  Function Name   : IsBroadcast                                           */
/*  Description     : Checks if the given address is a Broadcast address.   */
/*  Input(s)        : u4Addr - Ip Address to be checked.                    */
/*  Output(s)       : None.                                                 */
/*  Returns         : TRUE - If Address is a broadcast address              */
/*                    FALSE - Not a broadcast address.                      */
/*****************************************************************************/
INT1
IsBroadcast (UINT4 u4Addr)
{
    UINT4               u4DefBcastAddr = 0;
    INT1                i1RetVal = 0;

    i1RetVal = FALSE;

    if (u4Addr == IP_GEN_BCAST_ADDR)
    {
        i1RetVal = TRUE;
    }
    else
    {
        u4DefBcastAddr = SliDefaultBcastAddr (u4Addr);
        if (u4DefBcastAddr == u4Addr)
        {
            i1RetVal = TRUE;
        }
    }
    return i1RetVal;
}

#ifdef TCP_WANTED
/****************************************************************************/
/*  Function Name   : SliReplyForListen                                     */
/*  Description     : Call Back function provided by SLI for TCP to add the */
/*                    reply packet of listen pointer for SliListen.         */
/*  Input(s)        : pParms - TcpToApp parameters for Tcp                  */
/*  Output(s)       : None.                                                 */
/*  Returns         : SLI_SUCCESS                                           */
/****************************************************************************/
INT1
SliReplyForListen (tTcpToApplicationParms * pParms)
{
    tSdtEntry          *pSocket = NULL;
    INT4                i4SockDesc = FAILURE;

    if (pParms == NULL)
    {
        return SLI_FAILURE;
    }
    i4SockDesc = SliTcpFindSockDesc (pParms->u4ConnId);
    OsixSemTake (gSdtSemId);
    if ((i4SockDesc == FAILURE) || (SOCK_DESC_TABLE[i4SockDesc] == NULL))
    {
        OsixSemGive (gSdtSemId);
        return SLI_FAILURE;
    }

    pSocket = SOCK_DESC_TABLE[i4SockDesc];
    OsixSemGive (gSdtSemId);
    if (pSocket->pListen)
    {
        FREE_TCP_TO_APPLICATION_PARMS (pSocket->pListen);
    }
    pSocket->pListen = pParms;
    return SLI_SUCCESS;
}

/****************************************************************************/
/*  Function Name   : SliReplyForAsyncParams                                     */
/*  Description     : Call Back function provided by SLI for TCP to add the */
/*                    reply packet of SliConnect in the SLL for connect     */
/*                    pointer for connect and in the Async pointer          */
/*  Input(s)        : pParms - TcpToApp parameters for Tcp                  */
/*                    i4SockDesc - Socket descriptor                        */
/*                    u1State - state of the tcp connection.                */
/*  Output(s)       : None.                                                 */
/*  Returns         : SLI_SUCCESS                                           */
/****************************************************************************/
INT1
SliReplyForAsyncParams (tTcpToApplicationParms * pParms, INT4 i4SockDesc,
                        UINT1 u1State)
{
    tSdtEntry          *pSocket = NULL;

    if ((OsixSemTake (gSdtSemId)) != OSIX_SUCCESS)
    {
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if ((i4SockDesc == FAILURE) || (SOCK_DESC_TABLE[i4SockDesc] == NULL))
    {
        OsixSemGive (gSdtSemId);
        return SLI_FAILURE;
    }

    pSocket = SOCK_DESC_TABLE[i4SockDesc];

    OsixSemGive (gSdtSemId);

    if ((OsixSemTake (pSocket->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }
    if (u1State == TCPS_SYNSENT)
    {
        if (pSocket->pConnect)
            FREE_TCP_TO_APPLICATION_PARMS (pSocket->pConnect);
        pSocket->pConnect = pParms;
    }
    else
    {
        if (pSocket->pAsyncParams)
            FREE_TCP_TO_APPLICATION_PARMS (pSocket->pAsyncParams);
        pSocket->pAsyncParams = (tTcpToAppAsyncParms *) pParms;

        if (u1State == TCPS_FREE)
        {
            /* clear the async parameter and free the memory */
            FREE_TCP_TO_APPLICATION_PARMS (pSocket->pAsyncParams);
            pSocket->pAsyncParams = NULL;
        }
    }

    switch (pParms->cmdtype.message.u1Message)
    {
        case TCPE_CONN_REFUSED:
            pSocket->i1ErrorCode = SLI_ECONNREFUSED;
            break;

        case TCPE_TIMEDOUT:
            pSocket->i1ErrorCode = SLI_ETIMEDOUT;
            break;

        case TCPE_ICMP_MSG_ARRIVED:
#ifdef IP_WANTED
            if (pParms->cmdtype.message.u1IcmpType == ICMP_DEST_UNREACH)
            {
                pSocket->i1ErrorCode = SLI_ENETUNREACH;
            }
#endif
#ifdef IP6_WANTED
            if (pParms->cmdtype.message.u1IcmpType == ICMP6_DEST_UNREACHABLE)
            {
                pSocket->i1ErrorCode = SLI_ENETUNREACH;
            }
#endif
            break;
    }
    OsixSemGive (pSocket->ProtectSemId);

    return SLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name   : SliReply                                                */
/* Description     : Call Back function provided by SLI for TCP to add the   */
/*                   reply packet of SliAccept and SliConnect in the SLL     */
/*                   for accept and in the connect pointer for connect.      */
/* Input(s)        : Pointer to Tcp to App return parms.                     */
/* Output(s)       : None                                                    */
/* Returns         : OK or FAILURE.                                          */
/*****************************************************************************/
INT1
SliReply (tTcpToApplicationParms * pRetParms)
{
    tSliSllNode        *pSliSllNode = NULL;
    INT4                i4SockDesc;
    tSdtEntry          *PsdtSock = NULL;

    if (pRetParms == NULL)
    {
        return SLI_FAILURE;
    }
    i4SockDesc = GetTCBSockDesc (pRetParms->u4ConnId);
    OsixSemTake (gSdtSemId);
    if ((i4SockDesc == FAILURE) || (SOCK_DESC_TABLE[i4SockDesc] == NULL))
    {
        OsixSemGive (gSdtSemId);
        return SLI_FAILURE;
    }

    PsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    OsixSemGive (gSdtSemId);
    if ((OsixSemTake (PsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if (pRetParms->u1Cmd == NEW_CONN)
    {
        if ((pSliSllNode = (tSliSllNode *) SliAllocMemFromPool
             (gSliMemPoolId)) == NULL)
        {
            OsixSemGive (PsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
        pSliSllNode->pReplyPkt = (tTcpToApplicationParms *) pRetParms;
        TMO_SLL_Add ((tTMO_SLL *) PsdtSock->pAccept,
                     (tTMO_SLL_NODE *) & (pSliSllNode->pNextNode));

        if (SliSelectScanListForRead (i4SockDesc) == OSIX_FALSE)
        {
            if (TMO_SLL_Count ((tTMO_SLL *) PsdtSock->pAccept)
                > (UINT4) PsdtSock->i4MaxPendingOpens)
            {
                /* cannot add node more than max pending list where
                 * notification is also failed.
                 */
                TMO_SLL_Delete ((tTMO_SLL *) PsdtSock->pAccept,
                                (tTMO_SLL_NODE *) & (pSliSllNode->pNextNode));
                SliFreeMemToPool (gSliMemPoolId, pSliSllNode);
                OsixSemGive (PsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
        }

    }
    else if (pRetParms->u1Cmd == OPEN_CMD || pRetParms->u1Cmd == ERROR_MSG)
    {
        if (PsdtSock->pConnect)
            FREE_TCP_TO_APPLICATION_PARMS (PsdtSock->pConnect);
        PsdtSock->pConnect = pRetParms;
        if (pRetParms->u1Cmd == OPEN_CMD)
            PsdtSock->i1ErrorCode = ZERO;
    }
    else
    {
        OsixSemGive (PsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    OsixSemGive (PsdtSock->ProtectSemId);
    OsixSemGive (PsdtSock->ConnSemId);
    return SLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name   : AcceptMesgCount()                                       */
/* Description     : Call Back function provided by SLI for TCP to get the   */
/*                   count of pending accept messages.                       */
/* Input(s)        : Connection ID.                                          */
/* Output(s)       : None                                                    */
/* Returns         : Number of Pending Accept Messages                       */
/*****************************************************************************/
INT4
AcceptMesgCount (UINT4 u4ConnId, UINT4 *pu4PendingMsgs)
{
    UINT4               u4PendingMsgs = ZERO;
    INT4                i4SockDesc;
    tSdtEntry          *PsdtSock = NULL;
    i4SockDesc = SliTcpFindSockDesc (u4ConnId);
    OsixSemTake (gSdtSemId);
    if ((i4SockDesc == FAILURE) || (SOCK_DESC_TABLE[i4SockDesc] == NULL))
    {
        OsixSemGive (gSdtSemId);
        return SLI_FAILURE;
    }

    PsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    OsixSemGive (gSdtSemId);
    if (PsdtSock->pAccept)
    {
        u4PendingMsgs = TMO_SLL_Count (PsdtSock->pAccept);
    }
    *pu4PendingMsgs = u4PendingMsgs;
    return SLI_SUCCESS;
}

/*****************************************************************************/
/*  Function Name : SliTcpRegisterConnid                                     */
/*  Description   : This function updates the conn id for the specified      */
/*                  socket descriptor.                                       */
/*  Input(s)      : i4SockDesc - Socket descriptor                           */
/*                  u4ConnId - Corresponding tcp Connection Identifier.      */
/*  Output(s)     : None.                                                    */
/*  Returns       : OK.                                                      */
/*****************************************************************************/
INT4
SliTcpRegisterConnid (INT4 i4SockDesc, UINT4 u4ConnId)
{

    tSdtEntry          *PsdtSock = NULL;

    OsixSemTake (gSdtSemId);
    PsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    OsixSemGive (gSdtSemId);
    if (PsdtSock == NULL)
    {
        return SLI_FAILURE;
    }
    PsdtSock->u4ConnId = u4ConnId;
    PsdtSock->state = SS_CONNECTING;

    return SLI_SUCCESS;
}
#endif

/***********************************************************************************/
/*  Function Name : SliCheck LocalAddrPortInCxt                                    */
/*  Description   : This function does check if there is a socket descriptor */
/*                  existing with the given Ip Address and Port for this context.  */
/*  Input(s)      : u4ContextId - Context Identifier of VRF                        */
/*                  u4LocalIpAddr - Local Ip address                               */
/*                  u2LocalPort - LocalPort                                        */
/*  Output(s)     : None.                                                          */
/*  Returns       : SLI_CONN_EXIST - There a socket with given IpAddr & Port       */
/*                  SLI_CONN_NOT_EXIST - No socket with given IpAddr & Port        */
/***********************************************************************************/
INT4
SliCheckLocalAddrPortInCxt (UINT4 u4ContextId, tIpAddr LocalIpAddr,
                            UINT2 u2LocalPort, INT4 i4Protocol)
{
    INT4                i4Count = 0;
    tSdtEntry          *PsdtSock = NULL;

    for (i4Count = SLI_FIRST_SOCKET_FD; i4Count < (INT4) MAX_NO_OF_SOCKETS;
         i4Count++)
    {
        PsdtSock = SOCK_DESC_TABLE[i4Count];
        if ((PsdtSock) && (u2LocalPort != ZERO))
        {
            if ((PsdtSock->u2LocalPort == u2LocalPort) &&
                ((V6_ADDR_CMP (&PsdtSock->LocalIpAddr, &LocalIpAddr) == 0))
                && (PsdtSock->i4Protocol == i4Protocol)
                && (PsdtSock->u4TxContextId == u4ContextId))
            {
                return SLI_CONN_EXIST;
            }
        }
    }
    return SLI_CONN_NOT_EXIST;
}

#ifdef TCP_WANTED
/**************************************************************************/
/*  Function Name   : SliTcpIssueOpenRequest                              */
/*  Description     : This function enqueues an open request to TCP.      */
/*  Input(s)        :                                                     */
/*                    i4SockDesc - The socket identifier.                 */
/*                    u1Opentype - The type of open PASSIVE ?? ACTIVE ??  */
/*  Output(s)       : None.                                               */
/*  Returns         : SLI_SUCCESS - If Sucessful.                         */
/*                    SLI_FAILURE - The Open Request to TCP could not     */
/*                                    not be issued                       */
/**************************************************************************/
INT4
SliTcpIssueOpenRequest (INT4 i4SockDesc, UINT1 u1Opentype)
{
    tSdtEntry          *const CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tApplicationToTcpParms PatPParms;
    INT4                i4RetVal = SLI_SUCCESS;
    tSliMd5SA          *pMd5SA = NULL;
    tIpAddr             RemoteIpAddr;
    tsliTcpAoMktListNode *pSliTcpAoNd = NULL;

    MEMSET (&PatPParms, 0, sizeof (PatPParms));
    MEMSET (&RemoteIpAddr, ZERO, sizeof (tIpAddr));
    V6_HTONL (&RemoteIpAddr, &CpsdtSock->RemoteIpAddr);

    PatPParms.i4SockId = i4SockDesc;
    PatPParms.u1Cmd = OPEN_CMD;
    PatPParms.u4ContextId = CpsdtSock->u4TxContextId;

    PatPParms.cmdtype.open.i4MaxPendingMsgs = CpsdtSock->i4MaxPendingOpens;
    if (CpsdtSock->u2LocalPort == UNALLOTTED_PORT)
    {
        SliGetFreeLocalTcpPort (&(CpsdtSock->u2LocalPort));
    }
    PatPParms.cmdtype.open.u2LocalPort = CpsdtSock->u2LocalPort;
    PatPParms.cmdtype.open.u2RemotePort = CpsdtSock->u2RemotePort;
    V6_ADDR_COPY (&PatPParms.cmdtype.open.LocalIp, &CpsdtSock->LocalIpAddr);
    V6_ADDR_COPY (&PatPParms.cmdtype.open.RemoteIp, &CpsdtSock->RemoteIpAddr);
    PatPParms.cmdtype.open.u1Flag = u1Opentype;
    PatPParms.cmdtype.open.u4ValidOptions = CpsdtSock->u4ValidOptions;
    PatPParms.cmdtype.open.u4IfIndex = CpsdtSock->u4IfIndex;
    PatPParms.cmdtype.open.pMd5Key = NULL;
    PatPParms.cmdtype.open.u1Md5Keylen = ZERO;
    /* For TCP active open, if key is set for remote peer, pass it */
    if (u1Opentype == ACTIVE_OPEN)
    {
        /* if TCP-AO configured pass it */
        if ((pSliTcpAoNd = SliGetTcpAo (i4SockDesc, RemoteIpAddr)) != NULL)
        {
            PatPParms.cmdtype.open.pTcpAoMkt = &(pSliTcpAoNd->TcpAoKey);
            PatPParms.cmdtype.open.u1TcpAoIcmpAcc = pSliTcpAoNd->u1IcmpAccpt;
            PatPParms.cmdtype.open.u1TcpAoNoMktDisc =
                pSliTcpAoNd->u1NoMktMchPckDsc;
        }
        else
        {
            UINT1              *pu1TcpAoCfg;
            if ((pu1TcpAoCfg =
                 SliGetTcpAoNghMktCfg (i4SockDesc, RemoteIpAddr)) != NULL)
            {
                /* TCP-AO config exists for remote address */
                PatPParms.cmdtype.open.u1TcpAoNoMktDisc = (*pu1TcpAoCfg);
                pu1TcpAoCfg = SliGetTcpAoNghIcmpCfg (i4SockDesc, RemoteIpAddr);
                if (pu1TcpAoCfg != NULL)
                    PatPParms.cmdtype.open.u1TcpAoIcmpAcc = (*pu1TcpAoCfg);

            }
            else
            {
                /* Set default values */
                PatPParms.cmdtype.open.u1TcpAoNoMktDisc = TCP_ONE;
                PatPParms.cmdtype.open.u1TcpAoIcmpAcc = TCP_TWO;
            }
        }
        if ((pMd5SA = SliGetMd5SA (i4SockDesc, RemoteIpAddr)) != NULL)
        {
            PatPParms.cmdtype.open.pMd5Key = (UINT1 *) (pMd5SA->MD5Key.au1Key);
            PatPParms.cmdtype.open.u1Md5Keylen = pMd5SA->MD5Key.u1Keylen;
        }
    }
    if ((i4RetVal =
         (HliOpen (&PatPParms.cmdtype.open, &PatPParms))) != SLI_SUCCESS)
    {
        return i4RetVal;
    }
    return SLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : SliGetFreeLocalTcpPort                           */
/*  Description     : This function allocates a new local port for the */
/*                    connection requested by the application.         */
/*  Input(s)        : None.                                            */
/*  Output(s)       : Port                                             */
/*  Returns         : SUCCESS             .                            */
/*                    FAILURE - If No free port is avalaible.          */
/***********************************************************************/
INT4
SliGetFreeLocalTcpPort (UINT2 *u2TcpPort)
{
    INT4                i4Count1 = 0, i4Count2 = 0, i4Flag = 0;
    INT4                i4RandCount = 0;

    /* For obtaining the free local tcp port. Start from a random no. 
     * in the tcp port range and scan till the end of the range to find
     * a free port */
    i4Count1 = UtilRand ();
    while ((i4Count1 < TCP_PRIVATE_PORT_START)
           || (i4Count1 > TCP_PRIVATE_PORT_END))
    {
        i4Count1 = i4Count1 % TCP_PRIVATE_PORT_END;
        if (i4Count1 < TCP_PRIVATE_PORT_START)
        {
            i4Count1 += TCP_PRIVATE_PORT_START;
        }
    }
    i4RandCount = i4Count1;
    for (; i4Count1 < TCP_PRIVATE_PORT_END; i4Count1++)
    {
        i4Flag = TRUE;
        for (i4Count2 = ZERO_COUNT; (UINT4) i4Count2 < (MAX_NUM_OF_TCB);
             i4Count2++)
        {
            if (GetTCBstate (i4Count2) == TCPS_FREE)
            {
                continue;
            }
            if (i4Count1 != GetTCBlport (i4Count2))
            {
                continue;
            }
            else
            {
                i4Flag = FALSE;
                break;
            }
        }
        if (i4Flag == TRUE)
        {
            *u2TcpPort = (UINT2) i4Count1;
            return SLI_SUCCESS;
        }
    }
    /* No free is available in the range from random no. obtained to the end range.
     * Search from start of range to random no. to find the free port */
    for (i4Count1 = (i4RandCount - 1); i4Count1 >= TCP_PRIVATE_PORT_START;
         i4Count1--)
    {
        i4Flag = TRUE;
        for (i4Count2 = ZERO_COUNT; (UINT4) i4Count2 < (MAX_NUM_OF_TCB);
             i4Count2++)
        {
            if (GetTCBstate (i4Count2) == TCPS_FREE)
            {
                continue;
            }
            if (i4Count1 != GetTCBlport (i4Count2))
            {
                continue;
            }
            else
            {
                i4Flag = FALSE;
                break;
            }
        }
        if (i4Flag == TRUE)
        {
            *u2TcpPort = (UINT2) i4Count1;
            return SLI_SUCCESS;
        }
    }
    return SLI_FAILURE;
}
#endif

/***********************************************************************/
/*  Function Name   : GenerateSemName                                  */
/*  Description     : This function Generates a unique Name .          */
/*  Input(s)        : first - First Character in sem name. U ?? S??    */
/*                    char - rest of Name.                             */
/*  Output(s)       : None.                                            */
/*  Returns         : None.                                            */
/***********************************************************************/
VOID
GenerateSemName (char first, char a[SLI_SEM_NAME_LEN])
{
    a[SLI_THREE] = (INT1) zero;
    a[SLI_TWO] = (INT1) ten;
    a[SLI_ONE] = (INT1) hun;
    a[SLI_ZERO] = first;

    if (zero == SLI_MAX_UNIT1)
    {
        ten++;
        zero = SLI_ONE;
    }
    else
        zero++;

    if (ten == SLI_MAX_UNIT1)
    {
        hun++;
        ten = SLI_ONE;
    }

    if (hun == SLI_MAX_UNIT1)
    {
        zero = SLI_ONE;
        ten = SLI_ONE;
        hun = SLI_ONE;
    }
}

/***********************************************************************/
/*  Function Name   : SliFindSockDescFromPortAddrInCxt                 */
/*  Description     : This function a socket for a given Port          */
/*                    for the same destination Ip address    .         */
/*                    in this context                                  */
/*  Input(s)        : u4ContextId - Context Identifier                 */
/*                    u2RPort -  Port Number.                          */
/*  Output(s)       : None.                                            */
/*  Returns         : SockId  - Socket Identifier If Successful.       */
/*                    ERR - If failed.                                 */
/***********************************************************************/
INT4
SliFindSockDescFromPortAddrInCxt (UINT4 u4ContextId, UINT2 u2RPort,
                                  tIpAddr RemoteAddr, INT2 i2SdtFamily)
{
    INT4                i4Count = ERR, i4SockDesc = ERR;
    tSdtEntry          *PsdtSock = NULL;

    for (i4Count = SLI_FIRST_SOCKET_FD; i4Count < (INT4) MAX_NO_OF_SOCKETS;
         i4Count++)
    {
        PsdtSock = SOCK_DESC_TABLE[i4Count];

        if ((PsdtSock) && (PsdtSock->i4Protocol == IPPROTO_UDP)
            && (PsdtSock->i2SdtFamily == i2SdtFamily))
        {
            if ((PsdtSock->u2LocalPort == u2RPort)
                && ((V6_ADDR_CMP (&RemoteAddr, &(PsdtSock->LocalIpAddr)) == 0))
                && ((PsdtSock->u1FdMode == SOCK_GLOBAL_MODE) ||
                    (PsdtSock->u4TxContextId == u4ContextId)))
            {
                return i4Count;
            }
            else if (PsdtSock->u2LocalPort == u2RPort)
            {
                if ((PsdtSock->i2SdtFamily == AF_INET)
                    && (V4_FROM_V6_ADDR (PsdtSock->LocalIpAddr) == ZERO)
                    && ((PsdtSock->u1FdMode == SOCK_GLOBAL_MODE) ||
                        (PsdtSock->u4TxContextId == u4ContextId)))
                {
                    i4SockDesc = i4Count;
                }
                if ((PsdtSock->i2SdtFamily == AF_INET6)
                    && (IS_ADDR_IN6ADDRANYINIT (PsdtSock->LocalIpAddr))
                    && ((PsdtSock->u1FdMode == SOCK_GLOBAL_MODE) ||
                        (PsdtSock->u4TxContextId == u4ContextId)))
                {
                    i4SockDesc = i4Count;
                }
            }
        }
    }

    if (i4SockDesc != ERR)
    {
        return i4SockDesc;
    }

    return ERR;

}

tSliRawLlToIp6Params *
SliRawIp6GetParms (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    return (tSliRawLlToIp6Params *) (FS_ULONG) (CRU_BUF_Get_ModuleData (pBuf));
}

/*******************************************************************************
 * DESCRIPTION : This routine will return a pointer to data to be prepended
 * INPUTS      : dest buffer pointer , src buffer pointer
 *               dest offset , source offset , size of data to be copied 
 * OUTPUTS     : None.
 * RETURNS     : SLI_SUCCESS/SLI_FAILURE
 ******************************************************************************/

INT4
SliCruBufCopyBufChains (tCRU_BUF_CHAIN_HEADER * pDstChainDesc,
                        tCRU_BUF_CHAIN_HEADER * pSrcChainDesc,
                        UINT4 u4DstOffset, UINT4 u4SrcOffset, UINT4 u4Size)
{
    UINT1              *pu1TmpBuf = NULL;
    INT4                i4RetVal = CRU_FAILURE;

    if (u4Size == SLI_ZERO)
    {
        return (SLI_SUCCESS);
    }
    if (u4Size > SLI_MAX_PKT_LEN)
    {
        SLI_ERR (SLI_EMEMFAIL);
        return (SLI_FAILURE);
    }

    pu1TmpBuf = MemAllocMemBlk (gSliTempBufPoolId);

    if (pu1TmpBuf == NULL)
    {
        return (SLI_FAILURE);
    }

    i4RetVal =
        CRU_BUF_Copy_FromBufChain ((tCRU_BUF_CHAIN_HEADER *) pSrcChainDesc,
                                   pu1TmpBuf, u4SrcOffset, u4Size);
    if (i4RetVal == CRU_FAILURE)
    {
        MemReleaseMemBlock (gSliTempBufPoolId, (UINT1 *) pu1TmpBuf);
        return (SLI_FAILURE);
    }

    i4RetVal =
        CRU_BUF_Copy_OverBufChain ((tCRU_BUF_CHAIN_HEADER *) pDstChainDesc,
                                   pu1TmpBuf, u4DstOffset, u4Size);

    MemReleaseMemBlock (gSliTempBufPoolId, (UINT1 *) pu1TmpBuf);
    if (i4RetVal == CRU_FAILURE)
    {
        return (SLI_FAILURE);
    }
    else
    {
        return (SLI_SUCCESS);
    }

}

/***************************************************************************
 * DESCRIPTION : Scan the Fd Set's sockets to check if any socket is ready *
 * INPUTS      : Max no of fd's.                                           *
 *             : ReadFd set                                                *
 *             : WriteFd set                                               *
 *             : ExceptFd set                                              *
 * OUTPUTS     : ReadFd, WriteFd, ExceptFd                                 *
 * RETURNS     : SLI_FAILURE/No of sockets ready                           *
 ***************************************************************************/
INT4
SliProcessSelect (INT4 i4MaxFd, SliFdSet * pReadFd, SliFdSet * pWriteFd,
                  SliFdSet * pExceptFd)
{
#ifdef TCP_WANTED
    UINT4               u4Rdsize = ZERO;
    UINT4               u4Wrsize = ZERO;
    UINT4               u4Exsize = ZERO;
#endif

    INT4                i4Index = ZERO;
    UINT4               u4Count = ZERO;
    UINT4               u4MsgCnt = ZERO;
    tSdtEntry          *pSdtSock = NULL;

#ifdef TCP_WANTED
    INT4                i4SockDesc = SLI_FAILURE;
    UINT4               u4Index = ZERO;
    UINT4              *pFdArr = NULL;
    UINT4              *pTcpReadFdArr = NULL;
    UINT4              *pTcpWriteFdArr = NULL;
    UINT4              *pTcpExceptFdArr = NULL;

    if (i4MaxFd <= ZERO)
    {
        SLI_ERR (SLI_EINVAL);
        return SLI_FAILURE;
    }

    if ((pFdArr = MemAllocMemBlk (gSliFdArrPoolId)) == NULL)
    {
        return SLI_FAILURE;
    }

    pTcpReadFdArr = pFdArr;
    pTcpWriteFdArr = pTcpReadFdArr + i4MaxFd;
    pTcpExceptFdArr = pTcpWriteFdArr + i4MaxFd;
#endif
    if (pReadFd != NULL)
    {
        for (i4Index = SLI_FIRST_SOCKET_FD; i4Index < i4MaxFd; i4Index++)
        {
            if (SLI_FD_ISSET (i4Index, pReadFd))
            {
                if (SliLookup (i4Index) != TRUE)
                {
                    SLI_ERR (SLI_ENOTSOCK);
#ifdef TCP_WANTED
                    MemReleaseMemBlock (gSliFdArrPoolId, (UINT1 *) pFdArr);
#endif
                    return SLI_FAILURE;
                }
                pSdtSock = SOCK_DESC_TABLE[i4Index];
                switch (pSdtSock->i2SdtSockType)
                {
                    case SOCK_DGRAM:
                    {
                        if (((OsixQueNumMsg (pSdtSock->UdpQId,
                                             &u4MsgCnt)) == SLI_SUCCESS)
                            && (u4MsgCnt > ZERO))
                        {
                            u4Count++;
                        }
                        else
                        {
                            SLI_FD_CLR (i4Index, pReadFd);
                        }
                    }
                        break;

                    case SOCK_RAW:
                    {
                        if (SLI_SLL_Get_Count
                            ((tSliRawQueue *) & pSdtSock->RecvQueue) > ZERO)
                        {
                            u4Count++;
                        }
                        else
                        {
                            SLI_FD_CLR (i4Index, pReadFd);
                        }
                        break;
                    }

#ifdef TCP_WANTED
                    case SOCK_STREAM:
                    {
                        if ((pSdtSock->state == SS_CONNECTED) ||
                            (pSdtSock->state == SS_CONNECTING) ||
                            ((pSdtSock->state != SS_RX_SHUTDOWN) &&
                             ((pSdtSock->state == SS_RX_SHUTDOWN) ||
                              pSdtSock->state == SS_DISCONNECTING)))
                        {
                            SLI_FD_CLR (i4Index, pReadFd);
                            pTcpReadFdArr[u4Rdsize] = pSdtSock->u4ConnId;
                            u4Rdsize++;
                        }
                        else
                            SLI_FD_CLR (i4Index, pReadFd);
                        break;
                    }
#endif
                }                /* end - switch */
            }                    /* end - if */
        }                        /* end -for */
    }                            /* end - reafds != NULL */

    if (pWriteFd != NULL)
    {
        for (i4Index = SLI_FIRST_SOCKET_FD; i4Index < i4MaxFd; i4Index++)
        {
            if (SLI_FD_ISSET (i4Index, pWriteFd))
            {
                if (SliLookup (i4Index) != TRUE)
                {
                    SLI_ERR (SLI_ENOTSOCK);
#ifdef TCP_WANTED
                    MemReleaseMemBlock (gSliFdArrPoolId, (UINT1 *) pFdArr);
#endif
                    return SLI_FAILURE;
                }

                pSdtSock = SOCK_DESC_TABLE[i4Index];

                switch (pSdtSock->i2SdtSockType)
                {
                    case SOCK_DGRAM:
                    case SOCK_RAW:
                        u4Count++;
                        break;
#ifdef TCP_WANTED
                    case SOCK_STREAM:
                    {
                        if (((pSdtSock->state == SS_CONNECTED) ||
                             (pSdtSock->state == SS_CONNECTING) ||
                             ((pSdtSock->state != SS_TX_SHUTDOWN) &&
                              (pSdtSock->state == SS_RX_SHUTDOWN))))
                        {
                            SLI_FD_CLR (i4Index, pWriteFd);
                            pTcpWriteFdArr[u4Wrsize] = pSdtSock->u4ConnId;
                            u4Wrsize++;
                        }
                        else
                            SLI_FD_CLR (i4Index, pWriteFd);
                        break;
                    }
#endif
                }                /* end - switch */
            }                    /* end - if */
        }                        /* end - for pWriteFd */
    }                            /* end - if pWriteFd != NULL */

#ifdef TCP_WANTED
    if (pExceptFd != NULL)
    {
        for (i4Index = SLI_FIRST_SOCKET_FD; i4Index < i4MaxFd; i4Index++)
        {
            if (SLI_FD_ISSET (i4Index, pExceptFd))
            {
                if (SliLookup (i4Index) != TRUE)
                {
                    SLI_ERR (SLI_ENOTSOCK);
                    MemReleaseMemBlock (gSliFdArrPoolId, (UINT1 *) pFdArr);
                    return SLI_FAILURE;
                }
                pSdtSock = SOCK_DESC_TABLE[i4Index];
                switch (pSdtSock->i2SdtSockType)
                {
                    case SOCK_STREAM:
                    {
                        if ((pSdtSock->state == SS_CONNECTED) ||
                            (pSdtSock->state != SS_RX_SHUTDOWN))
                        {
                            SLI_FD_CLR (i4Index, pExceptFd);
                            pTcpExceptFdArr[u4Exsize] = pSdtSock->u4ConnId;
                            u4Exsize++;
                        }
                        else
                            SLI_FD_CLR (i4Index, pExceptFd);
                        break;
                    }
                    default:
                    {
                        SLI_FD_CLR (i4Index, pExceptFd);
                    }
                }                /* end - switch */
            }                    /* end -if */
        }                        /* end - for */
    }                            /* end - if except != null */
#else
    if (pExceptFd != NULL)
        SLI_FD_ZERO (pExceptFd);
#endif

#ifdef TCP_WANTED
    if ((u4Rdsize > ZERO) || (u4Wrsize > ZERO) || (u4Exsize > ZERO))
    {
        TcpGetSelectStatus (pTcpReadFdArr, pTcpWriteFdArr, pTcpExceptFdArr,
                            &u4Rdsize, &u4Wrsize, &u4Exsize);

        for (u4Index = ZERO; u4Index < u4Rdsize; u4Index++)
        {
            i4SockDesc = SliTcpFindSockDesc (pTcpReadFdArr[u4Index]);
            if (pReadFd != NULL)
            {
                SLI_FD_SET (i4SockDesc, pReadFd);
            }
            if (i4SockDesc >= 0)
            {
                u4Count++;
            }
        }

        for (u4Index = ZERO; u4Index < u4Wrsize; u4Index++)
        {
            i4SockDesc = SliTcpFindSockDesc (pTcpWriteFdArr[u4Index]);
            if (pWriteFd != NULL)
            {
                SLI_FD_SET (i4SockDesc, pWriteFd);
            }
            if (i4SockDesc >= 0)
            {
                u4Count++;
            }
        }

        for (u4Index = ZERO; u4Index < u4Exsize; u4Index++)
        {
            i4SockDesc = SliTcpFindSockDesc (pTcpExceptFdArr[u4Index]);
            if (pExceptFd != NULL)
            {
                SLI_FD_SET (i4SockDesc, pExceptFd);
            }
            if (i4SockDesc >= 0)
            {
                u4Count++;
            }
        }
    }
    MemReleaseMemBlock (gSliFdArrPoolId, (UINT1 *) pFdArr);
#endif
    return u4Count;
}

/***************************************************************************
 * DESCRIPTION : Sends an event to all tasks polling for READ/WRITE/EXCEPT *
 *               depending on BitMap set.                                  *
 * INPUTS      : socket(connection) identifier                             *
 *             : BitMap(READ/WRITE/EXCEPT)                                 *
 ***************************************************************************/

INT4
SliSelectScanList (INT4 i4SockId, UINT4 u4BitMap)
{
    tSelectNode        *pSelectNode = NULL;
    INT4                i4Flag = OSIX_FALSE;

    /* Critical section, lock it */
    SLI_SELECT_ENTER_CS ();

    /* Scan the SLL of this socket identifier */
    TMO_SLL_Scan (&gSelectTable, pSelectNode, tSelectNode *)
    {
        /* If the READ|WRITE|EXCEPT bit is set */
        if (((u4BitMap & SELECT_READ) && (pSelectNode->pReadFd)
             && (SLI_FD_ISSET (i4SockId, pSelectNode->pReadFd)))
            || ((u4BitMap & SELECT_WRITE) && (pSelectNode->pWriteFd)
                && (SLI_FD_ISSET (i4SockId, pSelectNode->pWriteFd)))
            || ((u4BitMap & SELECT_EXCEPT) && (pSelectNode->pExceptFd)
                && (SLI_FD_ISSET (i4SockId, pSelectNode->pExceptFd))))
        {
            /* Send Event to task that is polling */
            OsixEvtSend (pSelectNode->u4TaskId, SOCKET_READY_EVENT);
            i4Flag = OSIX_TRUE;
            break;
        }
    }
    /* End of critical section */
    SLI_SELECT_EXIT_CS ();
    return i4Flag;
}

/***************************************************************************
*  DESCRIPTION : Sends an event to all tasks polling for READ              *
*                                                                          *
*  INPUTS      : socket(connection) identifier                             *
*  OUTPUT      : None                                                      *
*  Return      : OSIX_TRUE - If Event is posted for any task               *
*              : OSIX_FALSE- If Event is not posted for any task           *
****************************************************************************/
INT4
SliSelectScanListForRead (INT4 i4SockId)
{
    tSelectNode        *pSelectNode = NULL;
    UINT4               u4EventSendStatus = OSIX_FALSE;

    /* Critical section, lock it */
    SLI_SELECT_ENTER_CS ();

    /* Scan the SLL of this socket identifier */
    TMO_SLL_Scan (&gSelectTable, pSelectNode, tSelectNode *)
    {
        /* If the READ bit is set */
        if ((pSelectNode->pReadFd)
            && (SLI_FD_ISSET (i4SockId, pSelectNode->pReadFd)))
        {
            /* Send Event to task that is polling */
            OsixEvtSend (pSelectNode->u4TaskId, SOCKET_READY_EVENT);
            u4EventSendStatus = OSIX_TRUE;
            break;
        }
    }
    /* End of critical section */
    SLI_SELECT_EXIT_CS ();
    return u4EventSendStatus;
}

/***************************************************************************
 * DESCRIPTION : starts a timer waits for  SOCKET_READY_EVENT from
 *               TDP/UDP/IP or SELECT_TIMER_EVENT.
 * INPUTS      : poniter to timeout(timeval structure)
 * RETURNS     : SLI_SUCCESS/SLI_FAILURE
 **************************************************************************/

INT4
SliWaitForSelectEvent (struct timeval *pTimeOut, INT4 i4SockDesc)
{
    UINT4               u4Event = SLI_ZERO;
    tOsixTaskId         TskId = 0;

    MEMSET (&gTimerListId[i4SockDesc], 0, sizeof (tTimerListId));
    MEMSET (&gReference[i4SockDesc], 0, sizeof (tTmrAppTimer));
    OsixTskIdSelf (&TskId);
    if (pTimeOut != NULL)
    {
        if (TmrCreateTimerList
            (OsixExGetTaskName (TskId), SELECT_TIMER_EVENT, NULL,
             &gTimerListId[i4SockDesc]) == TMR_FAILURE)
        {
            SLI_ERR (SLI_EMEMFAIL);
            return SLI_FAILURE;
        }

        if (TmrStartTimer
            (gTimerListId[i4SockDesc], &gReference[i4SockDesc],
             pTimeOut->tv_sec * SLI_CLOCK_TICK +
             ((pTimeOut->tv_usec * SLI_CLOCK_TICK) / MILLION)) == TMR_FAILURE)
        {
            SLI_ERR (SLI_EMEMFAIL);
            TmrDeleteTimerList (gTimerListId[i4SockDesc]);
            return SLI_FAILURE;
        }

        OsixEvtRecv
            (TskId, SOCKET_READY_EVENT | SELECT_TIMER_EVENT,
             SLI_EVENT_WAIT_FLAGS, &u4Event);
    }
    else
    {
        OsixEvtRecv
            (TskId, (UINT4) SOCKET_READY_EVENT, SLI_EVENT_WAIT_FLAGS, &u4Event);
    }

    if (u4Event & SELECT_TIMER_EVENT)
    {
        if (pTimeOut != NULL)
        {
            pTimeOut->tv_sec = 0;
            pTimeOut->tv_usec = 0;
            SLI_STOP_TIMER (gTimerListId[i4SockDesc], &gReference[i4SockDesc]);
            TmrDeleteTimerList (gTimerListId[i4SockDesc]);
        }
        SLI_ERR (SLI_EINTR);
        return SLI_EINTR;
    }
    if (u4Event & SOCKET_READY_EVENT)
    {
        if (pTimeOut != NULL)
        {
            SLI_STOP_TIMER (gTimerListId[i4SockDesc], &gReference[i4SockDesc]);
            TmrDeleteTimerList (gTimerListId[i4SockDesc]);
        }
        return SLI_SUCCESS;
    }
    return SLI_FAILURE;
}

tSdtEntry          *
SliGetSockEntry (INT4 i4SockDesc)
{
    tSdtEntry          *CpsdtSock = NULL;
    OsixSemTake (gSdtSemId);
    if ((SliLookup (i4SockDesc)) == TRUE)
    {
        CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
        OsixSemGive (gSdtSemId);
        return CpsdtSock;
    }
    else
    {
        OsixSemGive (gSdtSemId);
        return NULL;
    }
}

UINT4
SliIsFdSetClear (SliFdSet * pFd)
{
    UINT4               u4Index = 0;

    if (pFd == NULL)
    {
        return 1;
    }

    for (u4Index = ZERO_COUNT;
         u4Index < (SLI_MAX_FD / (sizeof (UINT4) * BITS_PER_BYTE)); u4Index++)
    {
        if ((*pFd)[u4Index] != ZERO)
        {
            return 0;
        }
    }
    return 1;
}

#ifdef TCP_WANTED
/***********************************************************************/
/*  Function Name   : SliValidateMd5Option                             */
/*  Description     : This function validates the option value         */
/*                    for TCP_MD5SIG option. Key length and Peer IP    */
/*                    Address are validated.                           */
/*  Input(s)        : pMd5Option - Pointer to option                   */
/*                    value                                            */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If Option value is valid          */
/*                    SLI_FAILURE -  If Option value is invalid        */
/***********************************************************************/
INT4
SliValidateMd5Option (struct tcp_md5sig *pMd5Option)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tIpAddr             PeerAddr;

    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));

    /* Validate key length is <= 80 bytes */
    if (pMd5Option->tcpm_keylen > TCP_MD5SIG_MAXKEYLEN)
    {
        return SLI_FAILURE;
    }

    /* Validate V4 peer address is neither a Class D, broadcast
     * or an unspecified IP address */
    if (pMd5Option->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pMd5Option->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, OSIX_NTOHL (pSinTmp->sin_addr.s_addr));

        if ((IP_IS_ADDR_CLASS_D (V4_FROM_V6_ADDR (PeerAddr)) == TRUE) ||
            (IsBroadcast (V4_FROM_V6_ADDR (PeerAddr)) == TRUE) ||
            (V4_FROM_V6_ADDR (PeerAddr) == INADDR_ANY))
        {
            return SLI_FAILURE;
        }
    }

    /* Validate V6 peer address is neither a multicast or an
     * unspecified IP address */
    else if (pMd5Option->tcpm_addr.ss_family == AF_INET6)
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pMd5Option->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));

        if ((IS_ADDR_MULTI (PeerAddr) == TRUE) ||
            (IS_ADDR_IN6ADDRANYINIT (PeerAddr)))
        {
            return SLI_FAILURE;
        }
    }

    /* If address family is neither AF_INET/AF_INET6, return error */
    else
    {
        return SLI_FAILURE;
    }

    return SLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : SliAddMd5SA                                      */
/*  Description     : This function extracts key, keylength and peer IP*/
/*                    from TCP_MD5SIG option value and adds in MD5 SA  */
/*                    list. If a node for peer IP exists, replaces key.*/
/*  Input(s)        : i4SockDesc - Socket Identifier                   */
/*                    pMd5Option - Pointer to option value             */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If SA is added                    */
/*                    SLI_FAILURE -  If a memory allocation fails      */
/***********************************************************************/
INT4
SliAddMd5SA (INT4 i4SockDesc, struct tcp_md5sig *pMd5Option)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tSliMd5SA          *pMd5SA = NULL;
    tIpAddr             PeerAddr;

    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));
    /* Get peer address based on address family. */
    /* Address is stored in network-byte order itself */
    if (pMd5Option->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pMd5Option->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, (pSinTmp->sin_addr.s_addr));
    }
    else
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pMd5Option->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));
    }

    /* Entry exists for peer IP then replace key */
    if ((pMd5SA = SliGetMd5SA (i4SockDesc, PeerAddr)) != NULL)
    {
        MEMSET (pMd5SA->MD5Key.au1Key, ZERO, TCP_MD5SIG_MAXKEYLEN);
        MEMCPY (pMd5SA->MD5Key.au1Key, pMd5Option->tcpm_key,
                pMd5Option->tcpm_keylen);
        pMd5SA->MD5Key.u1Keylen = (UINT1) pMd5Option->tcpm_keylen;
    }
    /* New peer IP */
    else
    {
        if ((pMd5SA = MemAllocMemBlk (gSliMd5NodePoolId)) != NULL)
        {
            MEMSET (pMd5SA, ZERO, sizeof (tSliMd5SA));
            MEMCPY (pMd5SA->MD5Key.au1Key, pMd5Option->tcpm_key,
                    pMd5Option->tcpm_keylen);
            pMd5SA->MD5Key.u1Keylen = (UINT1) pMd5Option->tcpm_keylen;
            V6_ADDR_COPY (&pMd5SA->PeerIp, &PeerAddr);
            TMO_SLL_Add (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
        }
        else
        {
            pSdtSock->i1ErrorCode = SLI_EMEMFAIL;
            SLI_ERR (SLI_EMEMFAIL);
            return SLI_FAILURE;
        }
    }
    return SLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : SliAddTcpAoMkt                                   */
/*  Description     : This function extracts MKT details and peer IP   */
/*                    from TcpAo  option value and adds in TcpAoList   */
/*                    If a node for peer IP exists, replaces MKT       */
/*                    values.                                          */
/*  Input(s)        : i4SockDesc - Socket Identifier                   */
/*                    pAoOption - Pointer to option value              */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If MKT is added                   */
/*                    SLI_FAILURE -  If a memory allocation fails      */
/***********************************************************************/

INT4
SliAddTcpAoMkt (INT4 i4SockDesc, tTcpAoMktAddr * pAoOption)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tsliTcpAoMktListNode *pAoMkt = NULL;
    tIpAddr             PeerAddr;
    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));
    if (pAoOption->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pAoOption->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, (pSinTmp->sin_addr.s_addr));
    }
    else
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pAoOption->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));
    }
    if ((pAoMkt = SliGetTcpAo (i4SockDesc, PeerAddr)) != NULL)
    {
        pAoMkt->TcpAoKey.u1SendKeyId = pAoOption->u1SndKeyId;
        pAoMkt->TcpAoKey.u1RcvKeyId = pAoOption->u1RcvKeyId;
        pAoMkt->TcpAoKey.u1TcpOptIgnore = pAoOption->u1TcpOptIgn;
        pAoMkt->TcpAoKey.u1ShaAlgo = pAoOption->u1Algo;
        MEMSET (pAoMkt->TcpAoKey.au1Key, ZERO, TCP_MD5SIG_MAXKEYLEN);
        MEMCPY (pAoMkt->TcpAoKey.au1Key, pAoOption->au1Key,
                pAoOption->u1KeyLen);
        pAoMkt->u1IsMktCfg = TRUE;

    }
    else
    {
        if ((pAoMkt = MemAllocMemBlk (gSliTcpAoNodePoolId)) != NULL)
        {
            MEMSET (pAoMkt, ZERO, sizeof (tSliTcpAoMkt));
            pAoMkt->TcpAoKey.u1SendKeyId = pAoOption->u1SndKeyId;
            pAoMkt->TcpAoKey.u1RcvKeyId = pAoOption->u1RcvKeyId;
            pAoMkt->TcpAoKey.u1TcpOptIgnore = pAoOption->u1TcpOptIgn;
            pAoMkt->TcpAoKey.u1ShaAlgo = pAoOption->u1Algo;
            MEMSET (pAoMkt->TcpAoKey.au1Key, ZERO, TCP_MD5SIG_MAXKEYLEN);
            MEMCPY (pAoMkt->TcpAoKey.au1Key, pAoOption->au1Key,
                    pAoOption->u1KeyLen);
            V6_ADDR_COPY (&pAoMkt->PeerIp, &PeerAddr);
            TMO_SLL_Add (&(pSdtSock->TcpAoList), &(pAoMkt->TSNext));
            pAoMkt->u1IsMktCfg = TRUE;
            /* Initialise other configs to default value */
            pAoMkt->u1NoMktMchPckDsc = TCP_ONE;
            pAoMkt->u1IcmpAccpt = TCP_TWO;

        }
        else
        {
            pSdtSock->i1ErrorCode = SLI_EMEMFAIL;
            SLI_ERR (SLI_EMEMFAIL);
            return SLI_FAILURE;
        }
    }
    return SLI_SUCCESS;
}

/*  Function Name   : SliDelMd5SA                                      */
/*  Description     : This function extracts the peer IP from the      */
/*                    option value for TCP_MD5SIG. Deletes node with   */
/*                    peer IP from socket's SA list                    */
/*  Input(s)        : i4SockDesc - Socket Identifier                   */
/*                    pMd5Option - Pointer to option value             */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If SA is deleted                  */
/*                    SLI_FAILURE -  If SA for peer IP does not exist  */
/***********************************************************************/
INT4
SliDelMd5SA (INT4 i4SockDesc, struct tcp_md5sig *pMd5Option)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tSliMd5SA          *pMd5SA = NULL;
    tIpAddr             PeerAddr;

    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));

    /* Get peer address based on address family. */
    /* Address is stored in network-byte order itself */
    if (pMd5Option->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pMd5Option->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, (pSinTmp->sin_addr.s_addr));
    }
    else
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pMd5Option->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));
    }

    /* Get the MD5 SA for peer IP and delete it */
    if ((pMd5SA = SliGetMd5SA (i4SockDesc, PeerAddr)) != NULL)
    {
        TMO_SLL_Delete (&(pSdtSock->Md5SAList), &(pMd5SA->TSNext));
        MemReleaseMemBlock (gSliMd5NodePoolId, (UINT1 *) pMd5SA);
    }
    else
    {
        pSdtSock->i1ErrorCode = SLI_ENOENT;
        SLI_ERR (SLI_ENOENT);
        return SLI_FAILURE;
    }
    return SLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : SliDelTcpAoMkt                                   */
/*  Description     : This function extracts peer IP details from      */
/*                    TcpAo option value and remove MKT details from   */
/*                    TcpAoList. The node will be deleted if there is  */
/*                    no other non-default TCP-AO configuration for    */
/*                    that IP exists                                   */
/*  Input(s)        : i4SockDesc - Socket Identifier                   */
/*                    pAoOption - Pointer to option value              */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If MKT is added                   */
/*                    SLI_FAILURE -  If a memory allocation fails      */
/***********************************************************************/

INT4
SliDelTcpAoMkt (INT4 i4SockDesc, tTcpAoMktAddr * pAoOption)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tsliTcpAoMktListNode *pAoMkt = NULL;
    tIpAddr             PeerAddr;
    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));
    if (pAoOption->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pAoOption->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, (pSinTmp->sin_addr.s_addr));
    }
    else
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pAoOption->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));
    }
    if ((pAoMkt = SliGetTcpAo (i4SockDesc, PeerAddr)) != NULL)
    {
        pAoMkt->u1IsMktCfg = FALSE;
        /* if there is no non-default TCP-AO config delete the node */
        if ((pAoMkt->u1NoMktMchPckDsc != TCP_TWO)
            && (pAoMkt->u1IcmpAccpt != TCP_ONE))
        {
            TMO_SLL_Delete (&(pSdtSock->TcpAoList), &(pAoMkt->TSNext));
            MemReleaseMemBlock (gSliTcpAoNodePoolId, (UINT1 *) pAoMkt);
        }
    }
    else
    {
        pSdtSock->i1ErrorCode = SLI_ENOENT;
        SLI_ERR (SLI_ENOENT);
        return SLI_FAILURE;
    }
    return SLI_SUCCESS;
}

/*  Function Name   : SliGetMd5SA                                      */
/*  Description     : This function searches MD5 SA list for a node    */
/*                    matching specified IP address.                   */
/*  Input(s)        : i4SockDesc - Socket ID                           */
/*                    IpAddr - V4/V6 address in network-byte order     */
/*  Output(s)       : None.                                            */
/*  Returns         : Pointer to MD5 SA - If match found               */
/*                    NULL -  If no match is found                     */
/***********************************************************************/
tSliMd5SA          *
SliGetMd5SA (INT4 i4SockDesc, tIpAddr IpAddr)
{
    tSliMd5SA          *pMd5SA = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];

    TMO_SLL_Scan (&(pSdtSock->Md5SAList), pMd5SA, tSliMd5SA *)
    {
        if ((V6_ADDR_CMP (&pMd5SA->PeerIp, &IpAddr)) == ZERO)
        {
            return (pMd5SA);
        }
    }
    return (NULL);
}

/***********************************************************************/
/*  Function Name   : SliGetTcpAo                                      */
/*  Description     : This function searches TcpAoList for a node      */
/*                    matching specified IP address.                   */
/*  Input(s)        : i4SockDesc - Socket ID                           */
/*                    IpAddr - V4/V6 address in network-byte order     */
/*  Output(s)       : None.                                            */
/*  Returns         : Pointer to TCP-AO MKT - If match found           */
/*                    NULL -  If no match is found                     */
/***********************************************************************/

tsliTcpAoMktListNode *
SliGetTcpAo (INT4 i4SockDesc, tIpAddr IpAddr)
{
    tsliTcpAoMktListNode *pTcpAoMkt = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];

    TMO_SLL_Scan (&(pSdtSock->TcpAoList), pTcpAoMkt, tsliTcpAoMktListNode *)
    {
        if ((V6_ADDR_CMP (&pTcpAoMkt->PeerIp, &IpAddr)) == ZERO)
        {
            if (pTcpAoMkt->u1IsMktCfg == TRUE)
            {
                return (pTcpAoMkt);
            }
        }
    }
    return (NULL);
}

/***********************************************************************/
/*  Function Name   : SliTcpFindMd5Key                                 */
/*  Description     : Call back function provided to TCP to search if  */
/*                    MD5 key is set for an incoming peer on           */
/*                    listen socket.                                   */
/*  Input(s)        : i4SockDesc - Socket Identifier                   */
/*                    IpAddr - V4/V6 peer IP address in                */
/*                             network-byte order                      */
/*  Output(s)       : pOutKey - Key set for peer                       */
/*                    pOutKeylen - Key length                          */
/*  Returns         : TRUE  - If Key is found                          */
/*                    FALSE - If Key is not found/OsixSemTake          */
/*                                   fails                             */
/***********************************************************************/
INT1                SliTcpFindMd5Key
    (INT4 i4SockDesc, tIpAddr IpAddr, UINT1 *pOutKey, UINT1 *pOutKeylen)
{
    tSliMd5SA          *pMd5SA = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];

    if ((pOutKey == NULL) || (pOutKeylen == NULL) || (pSdtSock == NULL))
    {
        return FALSE;
    }

    if ((OsixSemTake (pSdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        /* Ideally this should never occur. Can only happen if the 
         * socket's sema4 is deleted, which is done only during a close */
        SLI_ERR (SLI_ENOBUFS);
        return FALSE;
    }

    TMO_SLL_Scan (&(pSdtSock->Md5SAList), pMd5SA, tSliMd5SA *)
    {
        if ((V6_ADDR_CMP (&pMd5SA->PeerIp, &IpAddr)) == ZERO)
        {
            MEMCPY (pOutKey, pMd5SA->MD5Key.au1Key, pMd5SA->MD5Key.u1Keylen);
            *(pOutKeylen) = pMd5SA->MD5Key.u1Keylen;
            OsixSemGive (pSdtSock->ProtectSemId);
            return TRUE;
        }
    }

    OsixSemGive (pSdtSock->ProtectSemId);
    return FALSE;
}

/***********************************************************************/
/*  Function Name   : SliValidateTcpAoOPtion                           */
/*  Description     : This function validates the option value         */
/*                    for TCP_AO_SIG option. Key length and Peer IP     */
/*                    Address are validated.                           */
/*  Input(s)        : pAoOption - Pointer to option                    */
/*                    value                                            */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If Option value is valid          */
/*                    SLI_FAILURE -  If Option value is invalid        */
/***********************************************************************/
INT4
SliValidateTcpAoOption (tTcpAoMktAddr * pAoOption)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tIpAddr             PeerAddr;
    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));
/* Validate key length is <= 80 bytes */
    if (pAoOption->u1KeyLen > TCP_MD5SIG_MAXKEYLEN)
    {
        return SLI_FAILURE;
    }

    /* Validate V4 peer address is neither a Class D, broadcast
     * or an unspecified IP address */
    if (pAoOption->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pAoOption->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, OSIX_NTOHL (pSinTmp->sin_addr.s_addr));

        if ((IP_IS_ADDR_CLASS_D (V4_FROM_V6_ADDR (PeerAddr)) == TRUE) ||
            (IsBroadcast (V4_FROM_V6_ADDR (PeerAddr)) == TRUE) ||
            (V4_FROM_V6_ADDR (PeerAddr) == INADDR_ANY))
        {
            return SLI_FAILURE;
        }
    }

    /* Validate V6 peer address is neither a multicast or an
     * unspecified IP address */
    else if (pAoOption->tcpm_addr.ss_family == AF_INET6)
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pAoOption->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));

        if ((IS_ADDR_MULTI (PeerAddr) == TRUE) ||
            (IS_ADDR_IN6ADDRANYINIT (PeerAddr)))
        {
            return SLI_FAILURE;
        }
    }

    /* If address family is neither AF_INET/AF_INET6, return error */
    else
    {
        return SLI_FAILURE;
    }

    return SLI_SUCCESS;
}

/*  Function Name   : SliCheckSocketConnection                         */
/*  Description     : Checks if the connection exist                   */
/*  Input(s)        : u4ConnId - connection Id                         */
/*  Returns         : TRUE  - If connection is found                   */
/*                    FALSE - If connection is not found               */
/***********************************************************************/
INT4
SliCheckSocketConnection (UINT4 u4ConnId)
{
    INT4                i4SockDesc = 0;
    tSdtEntry          *CpsdtSock = NULL;

    for (i4SockDesc = SLI_FIRST_SOCKET_FD; i4SockDesc < MAX_NO_OF_SOCKETS;
         i4SockDesc++)
    {

        /* Take the socket sem, because this function is called in tcp
         * context, whereas socket deletion can occur in SLI context, which
         * may lead to race conditions
         */
        if ((OsixSemTake (gSdtSemId)) != OSIX_SUCCESS)
        {
            SLI_ERR (SLI_ENOBUFS);
            return FALSE;
        }

        if (TRUE == SliLookup (i4SockDesc))
        {
            CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
            if (u4ConnId == CpsdtSock->u4ConnId)
            {
                OsixSemGive (gSdtSemId);
                return TRUE;
            }
        }
        OsixSemGive (gSdtSemId);
    }
    return FALSE;
}

/***********************************************************************/
/*  Function Name   : SliValidateTcpAoNghCfg                           */
/*  Description     : This function validates the option value         */
/*                    for TCP_AO Neighbor Configs.                     */
/*                    Peer IP Address is validated.                    */
/*  Input(s)        : pTcpAoCfg - Pointer to option                   */
/*                    value                                            */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If Option value is valid          */
/*                    SLI_FAILURE -  If Option value is invalid        */
/***********************************************************************/
INT4
SliValidateTcpAoNghCfg (tTcpAoNeighCfg * pTcpAoCfg)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tIpAddr             PeerAddr;

    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));

    /* Validate V4 peer address is neither a Class D, broadcast
     * or an unspecified IP address */
    if (pTcpAoCfg->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pTcpAoCfg->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, OSIX_NTOHL (pSinTmp->sin_addr.s_addr));

        if ((IP_IS_ADDR_CLASS_D (V4_FROM_V6_ADDR (PeerAddr)) == TRUE) ||
            (IsBroadcast (V4_FROM_V6_ADDR (PeerAddr)) == TRUE) ||
            (V4_FROM_V6_ADDR (PeerAddr) == INADDR_ANY))
        {
            return SLI_FAILURE;
        }
    }

    /* Validate V6 peer address is neither a multicast or an
     * unspecified IP address */
    else if (pTcpAoCfg->tcpm_addr.ss_family == AF_INET6)
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pTcpAoCfg->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));

        if ((IS_ADDR_MULTI (PeerAddr) == TRUE) ||
            (IS_ADDR_IN6ADDRANYINIT (PeerAddr)))
        {
            return SLI_FAILURE;
        }
    }

    /* If address family is neither AF_INET/AF_INET6, return error */
    else
    {
        return SLI_FAILURE;
    }

    return SLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : SliAddTcpAoNghMktCfg                             */
/*  Description     : This function extracts no mkt match packet       */
/*                     discard config and peer IP                      */
/*                    from TcpAo  option value and adds in TcpAoList   */
/*                    If a node for peer IP exists, replaces the       */
/*                    no mkt match packet discard config values.       */
/*  Input(s)        : i4SockDesc - Socket Identifier                   */
/*                    pAoOption - Pointer to option value              */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If MKT is added                   */
/*                    SLI_FAILURE -  If a memory allocation fails      */
/***********************************************************************/

INT4
SliAddTcpAoNghMktCfg (INT4 i4SockDesc, tTcpAoNeighCfg * pAoOption)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tsliTcpAoMktListNode *pAoMkt = NULL;
    tIpAddr             PeerAddr;
    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));
    if (pAoOption->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pAoOption->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, (pSinTmp->sin_addr.s_addr));
    }
    else
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pAoOption->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));
    }
    if ((pAoMkt = SliGetTcpAo (i4SockDesc, PeerAddr)) != NULL)
    {
        pAoMkt->u1NoMktMchPckDsc = pAoOption->u1NoMktMchPckDsc;
    }
    else
    {
        if ((pAoMkt = MemAllocMemBlk (gSliTcpAoNodePoolId)) != NULL)
        {
            MEMSET (pAoMkt, ZERO, sizeof (tSliTcpAoMkt));
            pAoMkt->u1NoMktMchPckDsc = pAoOption->u1NoMktMchPckDsc;
            pAoMkt->u1IsMktCfg = FALSE;
            pAoMkt->u1IcmpAccpt = TCP_TWO;
            V6_ADDR_COPY (&pAoMkt->PeerIp, &PeerAddr);
            TMO_SLL_Add (&(pSdtSock->TcpAoList), &(pAoMkt->TSNext));
        }
        else
        {
            pSdtSock->i1ErrorCode = SLI_EMEMFAIL;
            SLI_ERR (SLI_EMEMFAIL);
            return SLI_FAILURE;
        }

    }
    return SLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : SliDelTcpAoNghMktCfg                             */
/*  Description     : This function extracts peer IP details from      */
/*                    TcpAo option value and delete no mkt-match       */
/*                    packet discard config from TcpAoList.            */
/*                    The node will be deleted if there is             */
/*                    no other non-default TCP-AO configuration for    */
/*                    that IP exists                                   */
/*  Input(s)        : i4SockDesc - Socket Identifier                   */
/*                    pAoOption - Pointer to option value              */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If MKT is added                   */
/*                    SLI_FAILURE -  If a memory allocation fails      */
/***********************************************************************/

INT4
SliDelTcpAoNghMktCfg (INT4 i4SockDesc, tTcpAoNeighCfg * pAoOption)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tsliTcpAoMktListNode *pAoMkt = NULL;
    tIpAddr             PeerAddr;
    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));
    if (pAoOption->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pAoOption->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, (pSinTmp->sin_addr.s_addr));
    }
    else
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pAoOption->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));
    }
    if ((pAoMkt = SliGetTcpAo (i4SockDesc, PeerAddr)) != NULL)
    {
        /* Set the value to default */
        pAoMkt->u1NoMktMchPckDsc = TCP_ONE;
        /* if there is no non-default configuration delete the node */
        if ((pAoMkt->u1IsMktCfg != TRUE) && (pAoMkt->u1IcmpAccpt != TCP_ONE))
        {
            TMO_SLL_Delete (&(pSdtSock->TcpAoList), &(pAoMkt->TSNext));
            MemReleaseMemBlock (gSliTcpAoNodePoolId, (UINT1 *) pAoMkt);
        }
    }
    else
    {
        pSdtSock->i1ErrorCode = SLI_ENOENT;
        SLI_ERR (SLI_ENOENT);
        return SLI_FAILURE;
    }
    return SLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name   :  SliGetTcpAoNghMktCfg                            */
/*  Description     : This function searches TcpAoList for a node      */
/*                    matching specified IP address.                   */
/*  Input(s)        : i4SockDesc - Socket ID                           */
/*                    IpAddr - V4/V6 address in network-byte order     */
/*  Output(s)       : None.                                            */
/*  Returns         : Pointer to TCP-AO no mkt match                   */
/*                    packet discard config  - If match found          */
/*                    NULL -  If no match is found                     */
/***********************************************************************/

UINT1              *
SliGetTcpAoNghMktCfg (INT4 i4SockDesc, tIpAddr IpAddr)
{
    tsliTcpAoMktListNode *pTcpAoMkt = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];

    TMO_SLL_Scan (&(pSdtSock->TcpAoList), pTcpAoMkt, tsliTcpAoMktListNode *)
    {
        if ((V6_ADDR_CMP (&pTcpAoMkt->PeerIp, &IpAddr)) == ZERO)
        {
            return (&pTcpAoMkt->u1NoMktMchPckDsc);
        }
    }
    return (NULL);
}

/***********************************************************************/
/*  Function Name   : SliAddTcpAoNghMktCfg                             */
/*  Description     : This function extracts no mkt match packet       */
/*                     discard config and peer IP                      */
/*                    from TcpAo  option value and adds in TcpAoList   */
/*                    If a node for peer IP exists, replaces the       */
/*                    no mkt match packet discard config values.       */
/*  Input(s)        : i4SockDesc - Socket Identifier                   */
/*                    pAoOption - Pointer to option value              */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If MKT is added                   */
/*                    SLI_FAILURE -  If a memory allocation fails      */
/***********************************************************************/

INT4
SliAddTcpAoNghIcmpCfg (INT4 i4SockDesc, tTcpAoNeighCfg * pAoOption)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tsliTcpAoMktListNode *pAoMkt = NULL;
    tIpAddr             PeerAddr;
    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));
    if (pAoOption->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pAoOption->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, (pSinTmp->sin_addr.s_addr));
    }
    else
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pAoOption->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));
    }
    if ((pAoMkt = SliGetTcpAo (i4SockDesc, PeerAddr)) != NULL)
    {
        pAoMkt->u1IcmpAccpt = pAoOption->u1IcmpAccpt;
    }
    else
    {
        if ((pAoMkt = MemAllocMemBlk (gSliTcpAoNodePoolId)) != NULL)
        {
            MEMSET (pAoMkt, ZERO, sizeof (tSliTcpAoMkt));
            pAoMkt->u1IcmpAccpt = pAoOption->u1IcmpAccpt;
            pAoMkt->u1IsMktCfg = FALSE;
            pAoMkt->u1NoMktMchPckDsc = TCP_ONE;
            V6_ADDR_COPY (&pAoMkt->PeerIp, &PeerAddr);
            TMO_SLL_Add (&(pSdtSock->TcpAoList), &(pAoMkt->TSNext));
        }
        else
        {
            pSdtSock->i1ErrorCode = SLI_EMEMFAIL;
            SLI_ERR (SLI_EMEMFAIL);
            return SLI_FAILURE;
        }

    }
    return SLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : SliDelTcpAoNghIcmpCfg                            */
/*  Description     : This function extracts peer IP details from      */
/*                    TcpAo option value and delete icmp accept        */
/*                    config from TcpAoList.                           */
/*                    The node will be deleted if there is             */
/*                    no other non-default TCP-AO configuration for    */
/*                    that IP exists                                   */
/*  Input(s)        : i4SockDesc - Socket Identifier                   */
/*                    pAoOption - Pointer to option value              */
/*  Output(s)       : None.                                            */
/*  Returns         : SLI_SUCCESS  - If MKT is added                   */
/*                    SLI_FAILURE -  If a memory allocation fails      */
/***********************************************************************/

INT4
SliDelTcpAoNghIcmpCfg (INT4 i4SockDesc, tTcpAoNeighCfg * pAoOption)
{
    tSin               *pSinTmp = NULL;
    tSin6              *pSin6Tmp = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];
    tsliTcpAoMktListNode *pAoMkt = NULL;
    tIpAddr             PeerAddr;
    MEMSET (&PeerAddr, ZERO, sizeof (tIpAddr));
    if (pAoOption->tcpm_addr.ss_family == AF_INET)
    {
        pSinTmp = (tSin *) (VOID *) &(pAoOption->tcpm_addr);
        V4_MAPPED_ADDR_COPY (&PeerAddr, (pSinTmp->sin_addr.s_addr));
    }
    else
    {
        pSin6Tmp = (tSin6 *) (VOID *) &(pAoOption->tcpm_addr);
        V6_ADDR_COPY (&PeerAddr, (tIpAddr *) (VOID *) &(pSin6Tmp->sin6_addr));
    }
    if ((pAoMkt = SliGetTcpAo (i4SockDesc, PeerAddr)) != NULL)
    {
        /* Set the value to default */
        pAoMkt->u1IcmpAccpt = TCP_TWO;
        /* if there is no non-default configuration delete the node */
        if ((pAoMkt->u1IsMktCfg != TRUE)
            && (pAoMkt->u1NoMktMchPckDsc != TCP_TWO))
        {
            TMO_SLL_Delete (&(pSdtSock->TcpAoList), &(pAoMkt->TSNext));
            MemReleaseMemBlock (gSliTcpAoNodePoolId, (UINT1 *) pAoMkt);
        }
    }
    else
    {
        pSdtSock->i1ErrorCode = SLI_ENOENT;
        SLI_ERR (SLI_ENOENT);
        return SLI_FAILURE;
    }
    return SLI_SUCCESS;
}

/***********************************************************************/
/*  Function Name   : SliGetTcpAoNghIcmpCfg                            */
/*  Description     : This function searches TcpAoList for a node      */
/*                    matching specified IP address.                   */
/*  Input(s)        : i4SockDesc - Socket ID                           */
/*                    IpAddr - V4/V6 address in network-byte order     */
/*  Output(s)       : None.                                            */
/*  Returns         : Pointer to TCP-AO icmp accept config             */
/*                    If match found                                   */
/*                    NULL -  If no match is found                     */
/***********************************************************************/

UINT1              *
SliGetTcpAoNghIcmpCfg (INT4 i4SockDesc, tIpAddr IpAddr)
{
    tsliTcpAoMktListNode *pTcpAoMkt = NULL;
    tSdtEntry          *const pSdtSock = SOCK_DESC_TABLE[i4SockDesc];

    TMO_SLL_Scan (&(pSdtSock->TcpAoList), pTcpAoMkt, tsliTcpAoMktListNode *)
    {
        if ((V6_ADDR_CMP (&pTcpAoMkt->PeerIp, &IpAddr)) == ZERO)
        {
            return (&pTcpAoMkt->u1IcmpAccpt);
        }
    }
    return (NULL);
}

/***************************************************************************
 * DESCRIPTION : Checks if the socket is existing for given                *
 *               bitmap (read/write/exception)                             *
 * INPUTS      : socket(connection) identifier                             *
 *             : BitMap(READ/WRITE/EXCEPT)                                 *
 * Return      : i4Flag - OSIX_TRUE if exists/ OSIX_FALSE if not exists    *
 ***************************************************************************/

INT4
SliCheckSelectScanList (INT4 i4SockId, UINT4 u4BitMap)
{
    tSelectNode        *pSelectNode = NULL;
    INT4                i4Flag = OSIX_FALSE;

    /* Critical section, lock it */
    SLI_SELECT_ENTER_CS ();

    /* Scan the SLL of this socket identifier */
    TMO_SLL_Scan (&gSelectTable, pSelectNode, tSelectNode *)
    {
        /* If the READ|WRITE|EXCEPT bit is set */
        if (((u4BitMap & SELECT_READ) && (pSelectNode->pReadFd)
             && (SLI_FD_ISSET (i4SockId, pSelectNode->pReadFd)))
            || ((u4BitMap & SELECT_WRITE) && (pSelectNode->pWriteFd)
                && (SLI_FD_ISSET (i4SockId, pSelectNode->pWriteFd)))
            || ((u4BitMap & SELECT_EXCEPT) && (pSelectNode->pExceptFd)
                && (SLI_FD_ISSET (i4SockId, pSelectNode->pExceptFd))))
        {
            i4Flag = OSIX_TRUE;
            break;
        }
    }
    /* End of critical section */
    SLI_SELECT_EXIT_CS ();
    return i4Flag;
}

#endif
