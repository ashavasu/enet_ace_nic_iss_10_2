/* $Id: sliproto.h,v 1.5 2011/10/25 10:16:13 siva Exp $*/
/***************************************************************************/
/* FILE NAME             : sliproto.h                                      */
/* PRINCIPAL AUTHOR      : Bhuvaneswari                                    */
/* SUBSYSTEM NAME        : SLI                                             */
/* MODULE NAME           : Socket Layer Interface                          */
/* LANGUAGE              : C                                               */
/* TARGET ENVIRONMENT    :                                                 */
/* DATE OF FIRST RELEASE :                                                 */
/* DESCRIPTION           : This file defines the prototypes of thw SLI     */
/*                         API's required for Socket Layer Interface.      */
/* ------------------------------------------------------------------------*/
/*   VERSION   AUTHOR/DATE   DESCRIPTION OF CHANGE                         */
/* ------------------------------------------------------------------------*/
/*   1.0       Bhuvana            File Created.                            */
/*            07/12/2000                                                   */
/* ------------------------------------------------------------------------*/

/* The Following Functions Conform to the BSD standard */
#ifndef __SLI_PROTO_H__
#define __SLI_PROTO_H__

VOID  SliMemInit PROTO ((VOID));

VOID  SliMemFree PROTO ((VOID));

VOID  SliSemInit PROTO ((VOID));

VOID  SliSemFree PROTO ((VOID));

UINT4 SliDefaultBcastAddr
      PROTO ((UINT4 u4HostAddr));

INT1 IsBroadcast
      PROTO ((UINT4 u4Addr));
#ifdef IP6_WANTED
INT4
SliUdp6RcvInCxt PROTO ((UINT4 u4ContextId, tIpAddr dstaddr,
                 tIpAddr srcaddr, UINT2 u2dportno,
                 UINT2 u2sportno, tCRU_BUF_CHAIN_HEADER * pBuf,
                 UINT4 u4IfIndex, UINT2 u2Len, UINT1 u1HopLimit));
#endif
INT4 SliHandleChangeInIp6PmtuInCxt 
      PROTO ((UINT4, tIp6Addr, UINT4));

#ifdef TCP_WANTED
tTcpToApplicationParms *SliStatus 
     PROTO (( INT4, INT1 *));

INT4 SliAbort 
     PROTO (( INT4)); 

INT4 SliRegisterAsyncHandler 
    PROTO (( INT4, VOID (*async_handler) (INT4, INT1 *)));

#endif

INT4    SliCheckLocalAddrPortInCxt      PROTO ((UINT4, tIpAddr, UINT2, INT4));
INT4    SliIsIpAddrLocalInCxt           PROTO((UINT4, tIpAddr, UINT4 *)); 

INT4 SliCruBufCopyBufChains PROTO ((tCRU_BUF_CHAIN_HEADER *, tCRU_BUF_CHAIN_HEADER *, UINT4 , UINT4 , UINT4 ));
#endif /* _SLI_PROTO_H */
