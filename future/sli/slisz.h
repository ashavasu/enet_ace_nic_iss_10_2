/** $Id: slisz.h,v 1.4 2013/06/07 13:30:17 siva Exp $ */
enum {
    MAX_SLI_ACCEPT_LIST_BLOCKS_SIZING_ID,
    MAX_SLI_BUFF_BLOCKS_SIZING_ID,
    MAX_SLI_DESC_TBL_BLOCKS_SIZING_ID,
    MAX_SLI_FD_ARR_BLOCKS_SIZING_ID,
    MAX_SLI_IP_OPT_BLOCKS_SIZING_ID,
    MAX_SLI_MD5_SLL_NODES_SIZING_ID,
    MAX_SLI_RAW_HASH_NODES_SIZING_ID,
    MAX_SLI_RAW_RCV_Q_NODES_SIZING_ID,
    MAX_SLI_SDT_BLOCKS_SIZING_ID,
    MAX_SLI_SLL_NODES_SIZING_ID,
    MAX_SLI_UDP4_BLOCKS_SIZING_ID,
    MAX_SLI_UDP6_BLOCKS_SIZING_ID,
    MAX_SLI_TCPAO_SLL_NODES_SIZING_ID,
    SLI_MAX_SIZING_ID
};


#ifdef  _SLISZ_C
tMemPoolId SLIMemPoolIds[ SLI_MAX_SIZING_ID];
INT4  SliSizingMemCreateMemPools(VOID);
VOID  SliSizingMemDeleteMemPools(VOID);
INT4  SliSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _SLISZ_C  */
extern tMemPoolId SLIMemPoolIds[ ];
extern INT4  SliSizingMemCreateMemPools(VOID);
extern VOID  SliSizingMemDeleteMemPools(VOID);
extern INT4  SliSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _SLISZ_C  */


#ifdef  _SLISZ_C
tFsModSizingParams FsSLISizingParams [] = {
{ "tTMO_SLL", "MAX_SLI_ACCEPT_LIST_BLOCKS", sizeof(tTMO_SLL),MAX_SLI_ACCEPT_LIST_BLOCKS, MAX_SLI_ACCEPT_LIST_BLOCKS,0 },
{ "tSliTempBufBlock", "MAX_SLI_BUFF_BLOCKS", sizeof(tSliTempBufBlock),MAX_SLI_BUFF_BLOCKS, MAX_SLI_BUFF_BLOCKS,0 },
{ "tSliDescTblBlock", "MAX_SLI_DESC_TBL_BLOCKS", sizeof(tSliDescTblBlock),MAX_SLI_DESC_TBL_BLOCKS, MAX_SLI_DESC_TBL_BLOCKS,0 },
{ "tSliFdArrBlock", "MAX_SLI_FD_ARR_BLOCKS", sizeof(tSliFdArrBlock),MAX_SLI_FD_ARR_BLOCKS, MAX_SLI_FD_ARR_BLOCKS,0 },
{ "tSliIpOptionBlock", "MAX_SLI_IP_OPT_BLOCKS", sizeof(tSliIpOptionBlock),MAX_SLI_IP_OPT_BLOCKS, MAX_SLI_IP_OPT_BLOCKS,0 },
{ "tSliMd5SA", "MAX_SLI_MD5_SLL_NODES", sizeof(tSliMd5SA),MAX_SLI_MD5_SLL_NODES, MAX_SLI_MD5_SLL_NODES,0 },
{ "tSliRawHashNode", "MAX_SLI_RAW_HASH_NODES", sizeof(tSliRawHashNode),MAX_SLI_RAW_HASH_NODES, MAX_SLI_RAW_HASH_NODES,0 },
{ "tSliRawRecvQnode", "MAX_SLI_RAW_RCV_Q_NODES", sizeof(tSliRawRecvQnode),MAX_SLI_RAW_RCV_Q_NODES, MAX_SLI_RAW_RCV_Q_NODES,0 },
{ "tSdtEntry", "MAX_SLI_SDT_BLOCKS", sizeof(tSdtEntry),MAX_SLI_SDT_BLOCKS, MAX_SLI_SDT_BLOCKS,0 },
{ "tSliSllNode", "MAX_SLI_SLL_NODES", sizeof(tSliSllNode),MAX_SLI_SLL_NODES, MAX_SLI_SLL_NODES,0 },
{ "t_UDP_TO_APP_MSG_PARMS", "MAX_SLI_UDP4_BLOCKS", sizeof(t_UDP_TO_APP_MSG_PARMS),MAX_SLI_UDP4_BLOCKS, MAX_SLI_UDP4_BLOCKS,0 },
{ "tSliUdp6Parms", "MAX_SLI_UDP6_BLOCKS", sizeof(tSliUdp6Parms),MAX_SLI_UDP6_BLOCKS, MAX_SLI_UDP6_BLOCKS,0 },
{ "tsliTcpAoMktListNode","MAX_SLI_TCPAO_SLL_NODES", sizeof(tsliTcpAoMktListNode), MAX_SLI_TCPAO_SLL_NODES,  MAX_SLI_TCPAO_SLL_NODES, 0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _SLISZ_C  */
extern tFsModSizingParams FsSLISizingParams [];
#endif /*  _SLISZ_C  */


