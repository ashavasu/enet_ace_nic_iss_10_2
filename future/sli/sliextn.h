/** $Id: sliextn.h,v 1.6 2011/10/25 10:16:13 siva Exp $ */

/* This file contains the extern variables */ 
#ifndef __SLIEXTN_H__
#define __SLIEXTN_H__

extern VOID **ppSockDescTable;
#define  SOCK_DESC_TABLE  ppSockDescTable 
#ifdef TCP_WANTED
extern tTcpSystemSize      gSliTcpSystemSizingParams;
#endif
extern tOsixSemId          gSelectSemId;
#endif
