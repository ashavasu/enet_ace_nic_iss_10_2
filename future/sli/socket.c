/***************************************************************************
*  FILE NAME             : socket.c                                      
*  PRINCIPAL AUTHOR      : Sri. Chellappa.                              
*  SUBSYSTEM NAME        : SLI                                         
*  MODULE NAME           : Socket Layer Interface                     
*  LANGUAGE              : C                                         
*  TARGET ENVIRONMENT    :                                          
*  DATE OF FIRST RELEASE :                                         
*  DESCRIPTION           : This file contains functions that constitute 
*                          FutureTCP/UDP's SLI.                        
*                            
*   CHANGE RECORD        :                                               
*  $Id: socket.c,v 1.126.2.1 2018/03/16 13:44:19 siva Exp $
* ------------------------------------------------------------------------+
*   VERSION   AUTHOR/DATE   DESCRIPTION OF CHANGE                         |
* ------------------------------------------------------------------------+
*     1.4     Rebecca Rufus + Enabling of Blocking/Non-Blocking made 
*                             Dynamic and hence removed               
*                             SLI_TCP_PROC_IF_WANTED switch.           
*                           + Added Procedural/Queue Interface for both 
*                             the Blocking and Non-Blocking Mode.    
*                           + Added abort function.               
*     1.5     Rebecca Rufus + Enabled the binding of local IP address. 
*              28-SEP-98    ->Refer Problem Id 6.                       
*     1.6     N.R.Soma      + All functions are modified to support UDP. 
*              18-MAR-99    + Fixed bug and memory leakage in SLI-TCP 
*                             Interface.                              
*                           + New functions recvfrom and sendto
*                             are added to receive and send datagrams. 
*                           - >Above changes are as per RFC chreq02     
*
*      1.7     Bhuvana       + Some of the API's are defined within the    
*              25-Dec-99      switch TCP_WANTED                          
*                                                                       
*      1.8     Shankar Vasudevan
*              Jan 2000     + Renamed init_socket_desc_table to SliInitialize
*                           + Deleted init.c
*                             + Moved the two functions 
*                               -init_socket_desc_table ()
*                               from init.c to this file (socket.c)
*                           + Semaphore Lock for Socket Descriptor Table
*                           + Memory allocation from Memory Pool.
*                           + Added SOCK_RAW support.
*       -      Ramakrishnan + Added support for Select call and made recvfrom 
*                             non blocking using FCNTL_OPNS in the sdt entry.
*                           + Added the UDP to APP linked list interface
*                             for implementing the select call.
*                           + Both available only under the ST_ENHLISTINTF
*                             compilation switch.
*       -      Ramakrishnan + Converted to FSAP2 under the FSAP2_PORT
*                             compilation switch.
*                           + Changed all the ST_ENHLISTINTF flags to ST_ENH
** ------------------------------------------------------------------------*/
/*     1.11     Saravanan.M    Implementation of                           */
/*              Purush            - Non Blocking Connect                   */
/*                                - Non Blocking Accept                    */
/*                                - IfMsg Interface between TCP and IP     */
/*                                - One Queue per Socket                   */
/* ------------------------------------------------------------------------*/
/***************************************************************************/

#include "sliinc.h"
#include "tcp.h"
#ifdef TCP_WANTED
#include "tcpinc.h"
#endif

/* System Sizing */
VOID              **ppSockDescTable;
tSliHashTable       gSliRawHashTable[IPPROTO_MAX];

tMemPoolId          QueueMemPoolId;
tMemPoolId          HashMemPoolId;
tMemPoolId          SdtMemPoolId;
tMemPoolId          gSliMemPoolId;
tMemPoolId          gUdp4MemPoolId;
tMemPoolId          gUdp6MemPoolId;
tMemPoolId          gSliAcceptListPoolId;
tMemPoolId          gSliIpOptionPoolId;
tMemPoolId          gSliTempBufPoolId;
tMemPoolId          gSliFdArrPoolId;
tMemPoolId          gSliDescTblPoolId;
tMemPoolId          gSliMd5NodePoolId;
tMemPoolId          gSliTcpAoNodePoolId;

tOsixSemId          gSdtSemId;
tOsixSemId          gHashPoolSemId;
tOsixSemId          gHashListSemId;
tOsixSemId          gSelectSemId;

INT4                i4SocketsInUse = ZERO;    /* No. Of Sockets Currently in use */

/* private function prototypes */
static INT4         SliRawHash (INT4);
static INT4         SliRawUnHash (INT4);
static VOID         SliDeleteHashNode (UINT2 u2HashKey,
                                       tSliRawHashNode * pHashNode);
static tSliRawHashNode *SliRawIpv4HashLookupInCxt (UINT4 u4ContextId,
                                                   UINT2 u2HashKey,
                                                   tSliRawHashNode * pHashNode,
                                                   UINT4 u4SrcAddr,
                                                   UINT4 u4DestAddr);
static tSliRawHashNode *SliRawIpv6HashLookupInCxt (UINT1 u4ContextId,
                                                   UINT2 u2HashKey,
                                                   tSliRawHashNode * pHashNode,
                                                   tIpAddr SrcIpAddr,
                                                   tIpAddr DestIpAddr);

#ifdef IP_WANTED
PRIVATE UINT2       SliInetPton4 (const UINT1 *, UINT1 *);
PRIVATE const UINT1 *SliInetNtop4 (const UINT1 *, UINT1 *, INT4);
#endif

#ifdef IP6_WANTED
PRIVATE UINT2       SliInetPton6 (const UINT1 *, UINT1 *);
PRIVATE const UINT1 *SliInetNtop6 (const UINT1 *, UINT1 *, INT4);
#endif

const struct in6_addr sli_in6addr_any =
    { {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };
const struct in6_addr sli_in6addr_loopback =
    { {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1} };

#ifdef TCP_WANTED
tTcpSystemSize      gSliTcpSystemSizingParams;
#endif

/* Global select table should be available to IP/TCP/UDP */
tTMO_SLL            gSelectTable;

/*************************************************************************/
/*  Function Name   : SliInit                                            */
/*  Description     : This function initialises the socket descriptor    */
/*                    table.                                             */
/*************************************************************************/

VOID
SliInit (INT1 *pi1Dummy)
{
    UNUSED_PARAM (pi1Dummy);

    /* does the system sizing parameters */
#ifdef TCP_WANTED
    GetTcpSizingParams (&gSliTcpSystemSizingParams);
#endif

    /* Initialise memory */
    SliMemInit ();

    /* create semaphores */
    SliSemInit ();

    /* register Pmtu handler for IPV6 */
#ifdef IP6_WANTED
    Ip6PmtuRegHl (SliHandleChangeInIp6PmtuInCxt);
#endif
    /* Indicate the status of initialization to the main routine */
    SLI_INIT_COMPLETE (OSIX_SUCCESS);
}

/*************************************************************************/
/*  Function Name   : SliShutDown                                        */
/*  Description     : This function deallocates all the resource of SLI  */
/*************************************************************************/
VOID
SliShutDown ()
{
    /* free all the memory resources allocated */
    SliMemFree ();
    /*delete all the sema4s allocated */
    SliSemFree ();
}

/*************************************************************************/
/*  Function Name   : SliMemInit                                         */
/*  Description     : This function allocates  all the memory resources  */
/*                    required                                           */
/*************************************************************************/
VOID
SliMemInit ()
{
    INT4                i4Index = ZERO;

    /* Dummy pointers for system sizing during run time */
    tSliDescTblBlock   *pSliDescTblBlock = NULL;
    tSliIpOptionBlock  *pSliIpOptionBlock = NULL;
    tSliTempBufBlock   *pSliTempBufBlock = NULL;
    tSliFdArrBlock     *pSliFdArrBlock = NULL;

    UNUSED_PARAM (pSliDescTblBlock);
    UNUSED_PARAM (pSliIpOptionBlock);
    UNUSED_PARAM (pSliTempBufBlock);
    UNUSED_PARAM (pSliFdArrBlock);

    /* intialise the socket desctiprot table */

    if (SliSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return;
    }

    gSliDescTblPoolId = SLIMemPoolIds[MAX_SLI_DESC_TBL_BLOCKS_SIZING_ID];
    SdtMemPoolId = SLIMemPoolIds[MAX_SLI_SDT_BLOCKS_SIZING_ID];
    gSliMemPoolId = SLIMemPoolIds[MAX_SLI_SLL_NODES_SIZING_ID];
    HashMemPoolId = SLIMemPoolIds[MAX_SLI_RAW_HASH_NODES_SIZING_ID];
    QueueMemPoolId = SLIMemPoolIds[MAX_SLI_RAW_RCV_Q_NODES_SIZING_ID];
    gUdp4MemPoolId = SLIMemPoolIds[MAX_SLI_UDP4_BLOCKS_SIZING_ID];
    gUdp6MemPoolId = SLIMemPoolIds[MAX_SLI_UDP6_BLOCKS_SIZING_ID];
    gSliAcceptListPoolId = SLIMemPoolIds[MAX_SLI_ACCEPT_LIST_BLOCKS_SIZING_ID];
    gSliIpOptionPoolId = SLIMemPoolIds[MAX_SLI_IP_OPT_BLOCKS_SIZING_ID];
    gSliTempBufPoolId = SLIMemPoolIds[MAX_SLI_BUFF_BLOCKS_SIZING_ID];
    gSliFdArrPoolId = SLIMemPoolIds[MAX_SLI_FD_ARR_BLOCKS_SIZING_ID];
    gSliMd5NodePoolId = SLIMemPoolIds[MAX_SLI_MD5_SLL_NODES_SIZING_ID];
    gSliTcpAoNodePoolId = SLIMemPoolIds[MAX_SLI_TCPAO_SLL_NODES_SIZING_ID];

    SOCK_DESC_TABLE = MemAllocMemBlk (gSliDescTblPoolId);
    if (!SOCK_DESC_TABLE)
    {
        return;
    }

    for (i4Index = ZERO_COUNT; i4Index < (INT4) MAX_NO_OF_SOCKETS; i4Index++)
    {
        SOCK_DESC_TABLE[i4Index] = (VOID *) NULL;
    }

    /* initialise raw hash table */
    for (i4Index = ZERO_COUNT; i4Index < IPPROTO_MAX; i4Index++)
    {
        SLI_SLL_Init (&gSliRawHashTable[i4Index]);
    }

    /* initialise select table */
    TMO_SLL_Init (&gSelectTable);

}

/*************************************************************************/
/*  Function Name   : SliShutDown                                        */
/*  Description     : This function deallocates all memory resource      */
/*************************************************************************/
VOID
SliMemFree ()
{
    INT4                i4Index = ZERO;

    /* close all the sockets */
    for (i4Index = ZERO_COUNT; i4Index < (INT4) MAX_NO_OF_SOCKETS; i4Index++)
    {
        if (SOCK_DESC_TABLE[i4Index])
        {
            SliClose (i4Index);
        }
    }

    /* initialise raw hash table */
    for (i4Index = ZERO_COUNT; i4Index < MAX_INET_PROTOS; i4Index++)
    {
        TMO_SLL_FreeNodes (&gSliRawHashTable[i4Index], HashMemPoolId);
    }

    SliSizingMemDeleteMemPools ();
}

/*************************************************************************/
/*  Function Name   : SliShutDown                                        */
/*  Description     : creates all the global sema4's required            */
/*************************************************************************/

VOID
SliSemInit ()
{

    if (OsixCreateSem (SLI_SOCKET_SEM_NAME,
                       SLI_SOCKET_SEM_COUNT,
                       SLI_SOCKET_SEM_FLAGS, &gSdtSemId) != OSIX_SUCCESS)
    {
        return;
    }

    if (OsixCreateSem ((const UINT1 *) SLI_RAWHP_SEM_NAME,
                       SLI_SOCKET_SEM_COUNT,
                       SLI_SOCKET_SEM_FLAGS, &gHashPoolSemId) != OSIX_SUCCESS)
    {
        return;
    }

    if (OsixCreateSem ((const UINT1 *) SLI_RAWHL_SEM_NAME,
                       SLI_SOCKET_SEM_COUNT,
                       SLI_SOCKET_SEM_FLAGS, &gHashListSemId) != OSIX_SUCCESS)
    {
        return;
    }

    if (OsixCreateSem ((const UINT1 *) SLI_SELECT_SEM_NAME,
                       SLI_SOCKET_SEM_COUNT,
                       SLI_SOCKET_SEM_FLAGS, &gSelectSemId) != OSIX_SUCCESS)
    {
        return;
    }

}

/*************************************************************************/
/*  Function Name   : SliShutDown                                        */
/*  Description     : deletes all the global sema4's required            */
/*************************************************************************/
VOID
SliSemFree ()
{
    OsixSemDel (gSdtSemId);
    OsixSemDel (gHashPoolSemId);
    OsixSemDel (gHashListSemId);
    OsixSemDel (gSelectSemId);
}

/**************************************************************************/
/*  Function Name   : SliSocket                                           */
/*  Description     : This function creates an endpoint for communication */
/*  Input(s)        : i4Family - Protocol family                          */
/*                    i4SocketType - The socket type.                     */
/*                    i4Protocol - The transport protocol to use          */
/*  Output(s)       : None.                                               */
/*  Return Value    :                                                     */
/*                    Socket Id - If success                              */
/*                    SLI_ENFILE, if table is full            */
/*                    SLI_EINVAL, if invalid combination       */
/*                    SLI_EMEMFAIL, if memory allocation failed      */
/**************************************************************************/

INT4
SliSocket (INT4 i4Family, INT4 i4SockType, INT4 i4Protocol)
{
    tSdtEntry          *pSocket = NULL;
    INT4                i4SockDesc = ZERO;
    INT4                i4RetVal = ZERO;

    INT4                i4QNameVar = ZERO;
    INT4                i4ArIndex = ZERO;
    /* Connection sema4 name */
    UINT1               au1ConnSemName[SLI_SEM_NAME_LEN];
    /* Protection sema4 name */
    UINT1               au1ProtectSemName[SLI_SEM_NAME_LEN];
    UINT1               au1QName[SLI_SEM_NAME_LEN];    /* used for UDP */
#ifdef IP_WANTED
    UINT2               u2McPort = ZERO;
#endif

    i4RetVal = SliValidateSocket (i4Family, i4SockType, &i4Protocol);

    if (i4RetVal == SLI_SUCCESS)
    {
        /* Critical Section lock it */
        if ((OsixSemTake (gSdtSemId)) != OSIX_SUCCESS)
        {
            SLI_ERR (SLI_ENOMEM);
            return SLI_FAILURE;
        }

        i4SockDesc = SliGetFreeSock ();

        if (i4SockDesc < ZERO)
        {
            OsixSemGive (gSdtSemId);
            SLI_ERR (SLI_ENFILE);
            return SLI_FAILURE;
        }

        if ((SOCK_DESC_TABLE[i4SockDesc] = (tSdtEntry *) SliAllocMemFromPool
             (SdtMemPoolId)) == NULL)
        {
            OsixSemGive (gSdtSemId);
            SLI_ERR (SLI_EMEMFAIL);
            return SLI_FAILURE;
        }

        pSocket = SOCK_DESC_TABLE[i4SockDesc];
        if (pSocket != NULL)
        {
            i4SocketsInUse++;
            GenerateSemName ('P', (char *) au1ProtectSemName);
            MEMSET (pSocket, ZERO, sizeof (tSdtEntry));
            if (OsixCreateSem (au1ProtectSemName, 1, 1,
                               &pSocket->ProtectSemId) != OSIX_SUCCESS)
            {
                SliDestroySocket (i4SockDesc);
                OsixSemGive (gSdtSemId);
                SLI_ERR (SLI_ENOBUFS);
                return SLI_FAILURE;
            }

            if ((OsixSemTake (pSocket->ProtectSemId)) != OSIX_SUCCESS)
            {
                SliDestroySocket (i4SockDesc);
                SLI_ERR (SLI_ENOBUFS);
                OsixSemGive (gSdtSemId);
                KW_FALSEPOSITIVE_FIX (pSocket->ProtectSemId);
                return SLI_FAILURE;
            }
            /* End of Critical region unlock */
            OsixSemGive (gSdtSemId);
            pSocket->i2SdtFamily = (INT2) i4Family;
            pSocket->state = SS_UNCONNECTED;
            pSocket->i2SdtSockType = (INT2) i4SockType;
            pSocket->i4Protocol = i4Protocol;
            if (i4Family == AF_INET)
            {
                V4_MAPPED_ADDR_COPY (&pSocket->LocalIpAddr, INADDR_ANY);
            }
            pSocket->u2LocalPort = UNALLOTTED_PORT;
            pSocket->i4IpTtl = SLI_UDP_TTL;
            pSocket->i4IpTos = SLI_UDP_TOS;
            pSocket->i4PmtuDfFlag = SLI_UDP_DF | IPPMTUDISC_DONT;
            pSocket->fpSliHandlePmtuChange = NULL;
            pSocket->u4IpPktinfoFlag = IP_PKTINFO_DIS;
            pSocket->inPktinfo.ipi_ifindex = SLI_UDP_PORT;
            pSocket->in6Pktinfo.ipi6_ifindex = SLI_UDP_PORT;
            pSocket->in6Pktinfo.in6_hoplimit = ZERO;
            pSocket->i4multicastloop = SLI_MULTICAST_LOOP;
            pSocket->i4multicasthlmt = ZERO;
            pSocket->i4unicasthlmt = ZERO;
            pSocket->u4IfIndex = INVALID_INTERFACE;
#ifdef IP_WANTED
            IpGetPortFromIfIndex (SLI_DEFAULT_IF, &u2McPort);
            pSocket->u4MCastIf = u2McPort;
#endif
#ifdef TCP_WANTED
            pSocket->pConnect = NULL;
            pSocket->pAccept = NULL;
            pSocket->u4ConnId = ZERO;
            pSocket->u4ValidOptions = TCPO_ENBL_ICMP_MSGS | TCPO_ASY_REPORT;
#endif
            /* If socket mode is GLOBAL means, it is common for
             * all contexts. If socket mode is UNIQUE, it is specific
             * to a single context.
             * Default socket mode is GLOBAL */
            pSocket->u1FdMode = SOCK_GLOBAL_MODE;
            pSocket->u4RxContextId = VCM_DEFAULT_CONTEXT;
            pSocket->u4TxContextId = VCM_DEFAULT_CONTEXT;
            GenerateSemName ('C', (char *) au1ConnSemName);
            if (OsixCreateSem (au1ConnSemName, ZERO, ZERO,
                               &pSocket->ConnSemId) != OSIX_SUCCESS)
            {
                OsixSemGive (pSocket->ProtectSemId);
                OsixSemDel (pSocket->ProtectSemId);
                /* assign to zero, else again semgive and semdel will
                 * be called in SliDestroySocket */
                pSocket->ProtectSemId = 0;
                SliDestroySocket (i4SockDesc);
                SLI_ERR (SLI_ENOBUFS);
                return SLI_FAILURE;
            }

            /* Init MD5 SA List */
            TMO_SLL_Init (&(pSocket->Md5SAList));

            TMO_SLL_Init (&(pSocket->TcpAoList));
            switch (i4SockType)
            {
                case SOCK_RAW:
                {
                    if (SliRawHash (i4SockDesc) != SLI_SUCCESS)
                    {
                        OsixSemGive (pSocket->ProtectSemId);
                        OsixSemDel (pSocket->ProtectSemId);
                        /* assign to zero, else again semgive and semdel will
                         * be called in SliDestroySocket */
                        pSocket->ProtectSemId = 0;
                        SliDestroySocket (i4SockDesc);
                        SLI_ERR (SLI_EMEMFAIL);
                        KW_FALSEPOSITIVE_FIX (pSocket->ProtectSemId);
                        KW_FALSEPOSITIVE_FIX1 (pSocket->ConnSemId);
                        return SLI_FAILURE;
                    }
                    break;
                }
                case SOCK_DGRAM:
                {
                    i4QNameVar = i4SockDesc;
                    au1QName[i4ArIndex++] = 'S';
                    i4QNameVar %= SLI_RECV_QUEUE_NAME_FIRST_DECIDER;
                    au1QName[i4ArIndex++] = (UINT1)
                        ((i4SockDesc / SLI_RECV_QUEUE_NAME_SECOND_DECIDER) +
                         ASCII_OFFSET);
                    i4QNameVar %= SLI_RECV_QUEUE_NAME_SECOND_DECIDER;
                    au1QName[i4ArIndex++] = (UINT1)
                        ((i4SockDesc / SLI_RECV_QUEUE_NAME_THIRD_DECIDER) +
                         ASCII_OFFSET);
                    i4QNameVar %= SLI_RECV_QUEUE_NAME_THIRD_DECIDER;
                    au1QName[i4ArIndex++] =
                        (UINT1) ((i4QNameVar) + ASCII_OFFSET);

                    if (OsixQueCrt
                        (au1QName, OSIX_MAX_Q_MSG_LEN, SLI_Q_DEPTH,
                         &pSocket->UdpQId) != OSIX_SUCCESS)
                    {
                        OsixSemGive (pSocket->ProtectSemId);
                        OsixSemDel (pSocket->ProtectSemId);
                        /* assign to zero, else again semgive and semdel will
                         * be called in SliDestroySocket */
                        pSocket->ProtectSemId = 0;
                        pSocket->UdpQId = 0;
                        SliDestroySocket (i4SockDesc);
                        SLI_ERR (SLI_EMEMFAIL);
                        KW_FALSEPOSITIVE_FIX (pSocket->ProtectSemId);
                        KW_FALSEPOSITIVE_FIX1 (pSocket->ConnSemId);
                        return SLI_FAILURE;
                    }
                    break;
                }
            }
            OsixSemGive (pSocket->ProtectSemId);
            i4RetVal = i4SockDesc;
        }
        else
        {
            SLI_ERR (SLI_EMEMFAIL);
            OsixSemGive (gSdtSemId);
            return SLI_FAILURE;
        }
    }
    if (pSocket != NULL)
    {
        KW_FALSEPOSITIVE_FIX (pSocket->ConnSemId);
        KW_FALSEPOSITIVE_FIX1 (pSocket->ProtectSemId);
    }
    return i4RetVal;
}

/***************************************************************************/
/*  Function Name   : SliFcntl                                             */
/*  Description     : Can be used to set a socket to be non-blocking       */
/*  Input(s)        :                                                      */
/*                    i4Sd - Socket Identifier                             */
/*                    i4Cmd - Command GET ?? SET ??                        */
/*                    i4Arg - Value                                        */
/*  Output(s)       : None.                                                */
/*  Returns         : SLI_SUCCESS - If SET opeartion is Success.           */
/*                    SLI_FAILURE - If invalid parameters                  */
/*                    Option Value - If Get is a success                   */
/***************************************************************************/

INT4
SliFcntl (INT4 i4Sd, INT4 i4Cmd, INT4 i4Arg)
{
    tSdtEntry          *pSocket = NULL;

    if (SliLookup (i4Sd) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    pSocket = SOCK_DESC_TABLE[i4Sd];
    if ((OsixSemTake (pSocket->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4Sd);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    switch (i4Cmd)
    {
        case SLI_F_SETFL:
        {
            if ((i4Arg & SLI_O_NONBLOCK) == SLI_O_NONBLOCK)
            {
                i4Arg = SLI_O_NONBLOCK;
            }
            switch (i4Arg)
            {
                case SLI_O_NONBLOCK:
                    pSocket->u1FcntlOpns = FCNTL_NOBLK;
#ifdef TCP_WANTED
                    pSocket->u4ValidOptions |= TCPO_NODELAY;
#endif
                    OsixSemGive (pSocket->ProtectSemId);
                    return SLI_SUCCESS;
                case ~SLI_O_NONBLOCK:
                    pSocket->u1FcntlOpns = FCNTL_BLK;
#ifdef TCP_WANTED
                    pSocket->u4ValidOptions &= ~TCPO_NODELAY;
#endif
                    OsixSemGive (pSocket->ProtectSemId);
                    return SLI_SUCCESS;
                default:
                    SLI_ERR (SLI_EINVAL);
                    pSocket->i1ErrorCode = SLI_EINVAL;
                    OsixSemGive (pSocket->ProtectSemId);
                    return SLI_FAILURE;
            }
        }
        case SLI_F_GETFL:
            OsixSemGive (pSocket->ProtectSemId);
            return ((INT4) pSocket->u1FcntlOpns);
        default:
            SLI_ERR (SLI_EINVAL);
            pSocket->i1ErrorCode = SLI_EINVAL;
            OsixSemGive (pSocket->ProtectSemId);
            return SLI_FAILURE;
    }
}

/****************************************************************************/
/* Function Name   : SliUdp6RcvInCxt                                          */
/* Description     : Call back function registered with UDP6 for each context */
/* Input(s)        : u4ContextId,dstaddr,portno,buffer,interface index,length           */
/* Output(s)       : NONE.                                                    */
/* Returns         : SLI_SUCCESS/SLI_FAILURE                                  */
/******************************************************************************/
#ifdef IP6_WANTED
INT4
SliUdp6RcvInCxt (UINT4 u4ContextId, tIpAddr dstaddr,
                 tIpAddr srcaddr, UINT2 u2dportno,
                 UINT2 u2sportno, tCRU_BUF_CHAIN_HEADER * pBuf,
                 UINT4 u4IfIndex, UINT2 u2Len, UINT1 u1HopLimit)
{
    tSliUdp6Parms      *pParms = NULL;
    tSdtEntry          *CpsdtSock = NULL;
    INT4                i4SockDesc = INIT_VALUE;
    tIpAddr             IpTempAddr;
    INT2                i2SdtFamily = AF_INET6;

    V6_NTOHL (&IpTempAddr, &dstaddr);
    i4SockDesc = SliFindSockDescFromPortAddrInCxt (u4ContextId,
                                                   u2dportno, IpTempAddr,
                                                   i2SdtFamily);
    if (i4SockDesc == ERR)
    {
        SLI_Release_Buffer (pBuf, FALSE);
        return SLI_FAILURE;
    }
    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];

    /* Allocate memory for SLI udp6 message and pass the 
     * received packet starting address to SLI queue in
     * tSliUdp6Parms itself */
    pParms = (tSliUdp6Parms *) MemAllocMemBlk (gUdp6MemPoolId);
    if (pParms == NULL)
    {
        SLI_Release_Buffer (pBuf, FALSE);
        return SLI_FAILURE;
    }

    MEMSET (pParms, 0, sizeof (tSliUdp6Parms));
    pParms->u4ContextId = u4ContextId;
    pParms->pBuf = pBuf;

    V6_ADDR_COPY (&(pParms->IpSrcAddr), &srcaddr);
    pParms->u2SrcPort = u2sportno;
    V6_ADDR_COPY (&(pParms->IpDstAddr), &dstaddr);
    pParms->u2DstPort = u2dportno;
    pParms->u4IfIndex = u4IfIndex;
    pParms->u2Len = u2Len;
    pParms->i4unicastHlmt = (INT4) u1HopLimit;
    CpsdtSock->in6Pktinfo.in6_hoplimit = u1HopLimit;

    if (OsixQueSend (CpsdtSock->UdpQId,
                     (UINT1 *) &pParms, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gUdp6MemPoolId, (UINT1 *) pParms);
        SLI_Release_Buffer (pBuf, FALSE);
        return SLI_FAILURE;
    }
    OsixSemGive (CpsdtSock->ConnSemId);
    SliSelectScanList (i4SockDesc, SELECT_READ);
    return SLI_SUCCESS;
}

#endif

/***************************************************************************/
/*  Function Name   : SliBind                                              */
/*  Description     : This function binds the caller's address specified   */
/*                    to the specified socket.                             */
/*  Input(s)        :                                                      */
/*                    i4SockDesc - The Socket Identifier.                  */
/*                    cpsa_myaddr - The Caller's address.                  */
/*                    i4Addrlen - The Caller's address length.             */
/*  Output(s)       : None.                                                */
/*  Returns         : SLI_SUCCESS - If Sucessful                           */
/*                    SLI_ENOTSOCK                              */
/*                                         if the socket desc is invalid   */
/*                    SLI_EINVAL,                               */
/*                                             if invalid address passed   */
/*                    SLI_ERR_BIND_FAILED,                                 */
/*                                             if bind operation failed    */
/***************************************************************************/

INT4
SliBind (INT4 i4SockDesc, struct sockaddr *cpsa_myaddr, INT4 i4Addrlen)
{
    tSdtEntry          *CpsdtSock = NULL;
    tSin               *CpsinTmp = NULL;
    tSin6              *Cpsin6Tmp = NULL;
    tIpAddr             v6RecvAddr;
    INT2                i2family = ZERO;
    UINT2               u2portno = ZERO;
#ifdef IP_WANTED
    UINT4               u4RecvAddr = ZERO;
#endif
#ifdef IP6_WANTED
    tIpAddr             IpTempAddr;
    tIpAddr             IpTempRemoteAddr;
    UINT4               u4IfIndex = INVALID_INTERFACE;
#endif
#ifdef TCP_WANTED
    UINT1               u1State = ZERO;
#endif

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }
    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }
    i2family = cpsa_myaddr->sa_family;
    if ((i2family != CpsdtSock->i2SdtFamily) && (i2family != AF_UNSPEC))
    {
        CpsdtSock->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    if (((i2family == AF_INET) && (i4Addrlen != sizeof (tSin)))
        || ((i2family == AF_INET6) && (i4Addrlen != sizeof (tSin6))))
    {
        CpsdtSock->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    if (i2family == AF_INET)
    {
        CpsinTmp = (tSin *) (VOID *) cpsa_myaddr;
        V4_MAPPED_ADDR_COPY (&v6RecvAddr,
                             OSIX_NTOHL (CpsinTmp->sin_addr.s_addr));
        u2portno = OSIX_NTOHS (CpsinTmp->sin_port);
    }
    else
    {
        Cpsin6Tmp = (tSin6 *) (VOID *) cpsa_myaddr;
        V6_NTOHL (&v6RecvAddr, (tIpAddr *) (VOID *) &Cpsin6Tmp->sin6_addr);
        u2portno = OSIX_NTOHS (Cpsin6Tmp->sin6_port);
    }
#ifdef TCP_WANTED
    if (CpsdtSock->i2SdtSockType == SOCK_STREAM)
    {
        if (u2portno == ZERO)
        {
            SliGetFreeLocalTcpPort (&u2portno);
        }
    }
#endif
    if (((!(IN6_IS_ADDR_V4MAPPED (v6RecvAddr)))
         && ((IS_ADDR_IN6ADDRANYINIT (v6RecvAddr)) == SLI_ZERO))
        || ((IN6_IS_ADDR_V4MAPPED (v6RecvAddr))
            && (V4_FROM_V6_ADDR (v6RecvAddr) != INADDR_ANY)))
    {
        if ((i2family == AF_INET) || (IN6_IS_ADDR_V4MAPPED (v6RecvAddr)))
        {
#ifdef IP_WANTED
            u4RecvAddr = V4_FROM_V6_ADDR (v6RecvAddr);
            if ((IP_IS_ADDR_CLASS_D (u4RecvAddr) == TRUE)
                || (IsBroadcast (u4RecvAddr) == TRUE))
            {
                /* Enables multicast & broadcast address to listen */
                V6_ADDR_INIT (&CpsdtSock->LocalIpAddr);
            }
            else if (IpifIsOurAddressInCxt (CpsdtSock->u4TxContextId,
                                            u4RecvAddr) == TRUE)
            {
                /* u4RecvAddr is created in this particular virtual router */
                V4_MAPPED_ADDR_COPY (&CpsdtSock->LocalIpAddr, u4RecvAddr);
            }
            else
            {
                CpsdtSock->i1ErrorCode = SLI_EADDRNOTAVAIL;
                SLI_ERR (SLI_EADDRNOTAVAIL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
#endif
        }
        else
        {
#ifdef IP6_WANTED
            if (IS_ADDR_MULTI (v6RecvAddr))
            {
                /* Enables multicast address to listen */
                V6_ADDR_INIT (&CpsdtSock->LocalIpAddr);
            }
            else if (NetIpv6IsOurAddressInCxt (CpsdtSock->u4TxContextId,
                                               (tIp6Addr
                                                *) (V6_HTONL (&IpTempAddr,
                                                              &v6RecvAddr)),
                                               &u4IfIndex) == SLI_SUCCESS)
            {
                /* v6RecvAddr is created in this particular virtual router */
                if ((IN6_IS_ADDR_LINKLOCAL (IpTempAddr))
                    && (((tSin6 *) (VOID *) cpsa_myaddr)->sin6_scope_id !=
                        (UINT4) u4IfIndex))
                {
                    CpsdtSock->i1ErrorCode = SLI_EADDRNOTAVAIL;
                    SLI_ERR (SLI_EADDRNOTAVAIL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
                CpsdtSock->u4IfIndex = u4IfIndex;
                V6_ADDR_COPY (&CpsdtSock->LocalIpAddr, &v6RecvAddr);
            }
            else
            {
                CpsdtSock->i1ErrorCode = SLI_EADDRNOTAVAIL;
                SLI_ERR (SLI_EADDRNOTAVAIL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
#endif
        }
        V6_ADDR_COPY (&CpsdtSock->RecvAddr, &v6RecvAddr);
        if (CpsdtSock->i2SdtSockType == SOCK_RAW)
        {
            CpsdtSock->u1Padding = TRUE;
        }
    }

    if (SliCheckLocalAddrPortInCxt (CpsdtSock->u4TxContextId, v6RecvAddr,
                                    u2portno, CpsdtSock->i4Protocol)
        == SLI_CONN_EXIST)
    {
        CpsdtSock->i1ErrorCode = SLI_EADDRINUSE;
        SLI_ERR (SLI_EADDRINUSE);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

#ifdef TCP_WANTED
    if (CpsdtSock->i2SdtSockType == SOCK_STREAM)
    {
        if (TcpCheckLocalAddrPortInCxt (CpsdtSock->u4TxContextId, v6RecvAddr,
                                        u2portno, &u1State) == TCP_CONN_EXIST)
        {
            if (!(CpsdtSock->u4ValidOptions & SO_REUSEADDR))
            {
                CpsdtSock->i1ErrorCode = SLI_EADDRINUSE;
                SLI_ERR (SLI_EADDRINUSE);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            else
            {
                if (u1State != TCPS_TIMEWAIT)
                {
                    CpsdtSock->i1ErrorCode = SLI_EADDRINUSE;
                    SLI_ERR (SLI_EADDRINUSE);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
            }
        }
    }                            /* End of if(sock type == SOCK_STREAM) */
#endif

    CpsdtSock->u2LocalPort = u2portno;
    if (i2family == AF_INET6)
    {
        V6_ADDR_INIT (&CpsdtSock->RemoteIpAddr);
    }
    else
    {
        V4_MAPPED_ADDR_COPY (&CpsdtSock->RemoteIpAddr, IP_ANY_ADDR);
    }
    CpsdtSock->u2RemotePort = UNALLOTTED_PORT;

    if (CpsdtSock->i2SdtSockType == SOCK_DGRAM)
    {
        if (i2family == AF_INET6)
        {
#ifdef IP6_WANTED
            /* When FdMode is GLOBAL, UDP6 control block will be 
             * included in global IP6 structure 
             * otherwise if it is UNIQUE mode, UDP6 control block will
             * be included in IP6 context structure.*/
            if (Udp6OpenInCxt (CpsdtSock->u4TxContextId, CpsdtSock->u1FdMode,
                               i4SockDesc, &(CpsdtSock->u2LocalPort), NULL,
                               NULL,
                               ((tIp6Addr
                                 *) (V6_HTONL (&IpTempAddr,
                                               &CpsdtSock->LocalIpAddr))),
                               &(CpsdtSock->u2RemotePort),
                               ((tIp6Addr
                                 *) (V6_HTONL (&IpTempRemoteAddr,
                                               &CpsdtSock->RemoteIpAddr))),
                               (VOID *) SliUdp6RcvInCxt) == FAILURE)
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
#endif
        }
        else
        {
#ifdef IP_WANTED
            /* When FdMode is GLOBAL, UDP control block will be 
             * included in global UDP structure 
             * otherwise if it is UNIQUE mode, UDP control block will
             * be included in UDP context structure.*/
            if (udp_sli_open_in_cxt
                (CpsdtSock->u1FdMode, CpsdtSock->u4TxContextId,
                 i4SockDesc, &(CpsdtSock->u2LocalPort),
                 V4_FROM_V6_ADDR (CpsdtSock->LocalIpAddr),
                 &(CpsdtSock->u2RemotePort),
                 V4_FROM_V6_ADDR (CpsdtSock->RemoteIpAddr),
                 CpsdtSock->ConnSemId, CpsdtSock->UdpQId) < ZERO)
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
#endif
        }
        CpsdtSock->u1Padding = TRUE;
    }
    OsixSemGive (CpsdtSock->ProtectSemId);
    return SLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : SliConnect                                          */
/*  Description     : This function connects the caller to the specified  */
/*                    server address.                                     */
/*  Input(s)        :                                                     */
/*                    i4SockDesc - The socket Identifier.                 */
/*                    cpsa_serv_addr - The server address to connect      */
/*                    i4Addrlen - The address length.                     */
/*  Output(s)       : pi4SockDesc - The socket descriptor for the         */
/*                                  current task                          */
/*  Returns           : SUCCESS  - If Sucessful                           */
/*                      SLI_ENOTSOCK - If the socket desc is   */
/*                                                invalid.                */
/*                      SLI_ERR_COULDNT_OPEN_CONNECTION -                 */
/*                        If the Active open request failed.              */
/**************************************************************************/

INT4
SliConnect (INT4 i4SockDesc, struct sockaddr *cpsa_serv_addr, INT4 i4Addrlen)
{

    tSdtEntry          *CpsdtSock = NULL;
    tSin6              *Cpsin6Tmp = NULL;
    tIpAddr             v6RemoteAddr;
#ifdef IP6_WANTED
    tIpAddr             IpTempRemoteAddr;
#endif
    INT2                i2family;
    UINT2               u2portno;
    tSin               *CpsinTmp = NULL;
#ifdef TCP_WANTED
    tTcpToApplicationParms *PtaPRetParms = NULL;
    INT4                i4bReplyRcvd = FALSE;    /* BOOLEAN Var */
    INT4                i4AsyncSockDesc;
    INT4                i4RetVal = SLI_FAILURE;
#endif /* TCP_WANTED */
#ifdef IP_WANTED
    UINT4               u4LocalAddr = ZERO;
#endif

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if ((CpsdtSock->i2SdtSockType == SOCK_STREAM)
        && (CpsdtSock->state == SS_CONNECTED))
    {
        CpsdtSock->i1ErrorCode = SLI_EISCONN;
        SLI_ERR (SLI_EISCONN);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    /* Only Internet Address Family is supported as of now */
    i2family = cpsa_serv_addr->sa_family;
    if ((i2family != CpsdtSock->i2SdtFamily) && (i2family != AF_UNSPEC))
    {
        CpsdtSock->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    if (((i2family == AF_INET) && (i4Addrlen != sizeof (tSin)))
        || ((i2family == AF_INET6) && (i4Addrlen != sizeof (tSin6))))
    {
        CpsdtSock->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    if (i2family == AF_INET)
    {
        CpsinTmp = (tSin *) (VOID *) cpsa_serv_addr;
        u2portno = OSIX_NTOHS (CpsinTmp->sin_port);
        V4_MAPPED_ADDR_COPY (&v6RemoteAddr,
                             OSIX_NTOHL (CpsinTmp->sin_addr.s_addr));
    }
    else
    {
        if (i2family == AF_INET6)
        {
            if (CpsdtSock->u4IfIndex == INVALID_INTERFACE)
            {
                CpsdtSock->u4IfIndex = (UINT2)
                    ((tSin6 *) (VOID *) cpsa_serv_addr)->sin6_scope_id;
            }
            Cpsin6Tmp = (tSin6 *) (VOID *) cpsa_serv_addr;
            u2portno = OSIX_NTOHS (Cpsin6Tmp->sin6_port);
            V6_NTOHL (&v6RemoteAddr,
                      (tIpAddr *) (VOID *) &Cpsin6Tmp->sin6_addr);
        }
        else
        {
            CpsdtSock->state = SS_UNCONNECTED;
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_SUCCESS;
        }
    }

    if (i2family == AF_INET)
    {                            /* for v4 packets */
        if ((IP_IS_ADDR_CLASS_D (V4_FROM_V6_ADDR (v6RemoteAddr)) == TRUE) ||
            (IsBroadcast (V4_FROM_V6_ADDR (v6RemoteAddr)) == TRUE))
        {
            CpsdtSock->i1ErrorCode = SLI_EBADADDR;
            SLI_ERR (SLI_EBADADDR);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
    }
    else if (IS_ADDR_MULTI (v6RemoteAddr) == TRUE)
    {                            /* for v6 packets */
        CpsdtSock->i1ErrorCode = SLI_EBADADDR;
        SLI_ERR (SLI_EBADADDR);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    if ((CpsdtSock->i2SdtSockType == SOCK_STREAM)
        && (((!IN6_IS_ADDR_V4MAPPED (v6RemoteAddr))
             && (IS_ADDR_IN6ADDRANYINIT (v6RemoteAddr)))
            || ((IN6_IS_ADDR_V4MAPPED (v6RemoteAddr))
                && (V4_FROM_V6_ADDR (v6RemoteAddr) == INADDR_ANY))))
    {
        CpsdtSock->i1ErrorCode = SLI_EDESTADDRREQ;
        SLI_ERR (SLI_EDESTADDRREQ);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if ((CpsdtSock->i1ErrorCode == SLI_EINPROGRESS) ||
        (CpsdtSock->i1ErrorCode == SLI_EALREADY))
    {
        CpsdtSock->i1ErrorCode = SLI_EALREADY;
        SLI_ERR (SLI_EALREADY);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    V6_ADDR_COPY (&CpsdtSock->RemoteIpAddr, &v6RemoteAddr);
    CpsdtSock->u2RemotePort = u2portno;

    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM)
        || (CpsdtSock->i2SdtSockType == SOCK_RAW))
    {
        if (CpsdtSock->u1Padding != TRUE)
        {
            if (i2family == AF_INET)
            {
#ifdef IP_WANTED
                /* To get source address in this particular context */
                if (ip_src_addr_to_use_for_dest_InCxt
                    (CpsdtSock->u4TxContextId,
                     V4_FROM_V6_ADDR (v6RemoteAddr),
                     &u4LocalAddr) == IP_FAILURE)
                {
                    CpsdtSock->i1ErrorCode = SLI_ENETUNREACH;
                    SLI_ERR (SLI_ENETUNREACH);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }

                V4_MAPPED_ADDR_COPY (&CpsdtSock->LocalIpAddr, u4LocalAddr);
                if (CpsdtSock->i2SdtSockType == SOCK_DGRAM)
                {
                    /* When FdMode is GLOBAL, UDP control block will be 
                     * included in global UDP structure 
                     * otherwise if it is UNIQUE mode, UDP control block will
                     * be included in UDP context structure.*/
                    if (udp_sli_open_in_cxt
                        (CpsdtSock->u1FdMode, CpsdtSock->u4TxContextId,
                         i4SockDesc, &(CpsdtSock->u2LocalPort),
                         V4_FROM_V6_ADDR (CpsdtSock->LocalIpAddr),
                         &(CpsdtSock->u2RemotePort),
                         V4_FROM_V6_ADDR (CpsdtSock->RemoteIpAddr),
                         CpsdtSock->ConnSemId, CpsdtSock->UdpQId) < ZERO)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    CpsdtSock->u1Padding = TRUE;
                }
#endif
            }
            else
            {
#ifdef IP6_WANTED
                /* v6RemoteAddr is used to get the local address in 
                 * the particular context */
                if (Ip6SrcAddrForDestAddrInCxt
                    (CpsdtSock->u4TxContextId,
                     (tIp6Addr *) (VOID *) &Cpsin6Tmp->sin6_addr,
                     (tIp6Addr *) (VOID *) &v6RemoteAddr) == IP6_FAILURE)
                {
                    CpsdtSock->i1ErrorCode = SLI_ENETUNREACH;
                    SLI_ERR (SLI_ENETUNREACH);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
                V6_NTOHL (&CpsdtSock->LocalIpAddr, &v6RemoteAddr);
                if (CpsdtSock->i2SdtSockType == SOCK_DGRAM)
                {
                    /* When FdMode is GLOBAL, UDP6 control block will be 
                     * included in global IP6 structure 
                     * otherwise if it is UNIQUE mode, UDP6 control block will
                     * be included in IP6 context structure.*/
                    if (Udp6OpenInCxt
                        (CpsdtSock->u4TxContextId, CpsdtSock->u1FdMode,
                         i4SockDesc, &(CpsdtSock->u2LocalPort), NULL, NULL,
                         ((tIp6Addr
                           *) (V6_HTONL (&v6RemoteAddr,
                                         &CpsdtSock->LocalIpAddr))),
                         &(CpsdtSock->u2RemotePort),
                         ((tIp6Addr *) (V6_HTONL (&IpTempRemoteAddr,
                                                  &CpsdtSock->RemoteIpAddr))),
                         (VOID *) SliUdp6RcvInCxt) == FAILURE)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    CpsdtSock->u1Padding = TRUE;
                }
#endif
            }
        }
        CpsdtSock->state = SS_CONNECTED;
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_SUCCESS;
    }
    /* Issuing a Connect Request procedurally */

#ifdef TCP_WANTED
    if (CpsdtSock->pConnect == NULL)
    {
        i4RetVal = SliTcpIssueOpenRequest (i4SockDesc, ACTIVE_OPEN);
        if (i4RetVal != SLI_SUCCESS)
        {
            CpsdtSock->i1ErrorCode = (INT1) i4RetVal;
            SLI_ERR (i4RetVal);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
    }

    while (i4bReplyRcvd == FALSE)
    {
        PtaPRetParms = CpsdtSock->pConnect;
        if (PtaPRetParms == NULL)
        {
            if (CpsdtSock->u1FcntlOpns & FCNTL_NOBLK)
            {
                CpsdtSock->i1ErrorCode = SLI_EINPROGRESS;
                CpsdtSock->state = SS_CONNECTING;
                SLI_ERR (SLI_EINPROGRESS);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            else
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                if ((OsixSemTake (CpsdtSock->ConnSemId)) != OSIX_SUCCESS)
                {
                    SliDestroySocket (i4SockDesc);
                    SLI_ERR (SLI_ENOBUFS);
                    return SLI_FAILURE;
                }
                PtaPRetParms = CpsdtSock->pConnect;
            }
            if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
            {
                SliDestroySocket (i4SockDesc);
                SLI_ERR (SLI_ENOBUFS);
                return SLI_FAILURE;
            }
        }
        if (CpsdtSock->pConnect)
        {
            CpsdtSock->pConnect = NULL;
        }

        if (PtaPRetParms == NULL)
        {
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }

        if ((CpsdtSock->u4ValidOptions & TCPO_NODELAY) ||
            (PtaPRetParms->u1Cmd == OPEN_CMD) ||
            (((PtaPRetParms->u1Cmd == ERROR_MSG) &&
              (PtaPRetParms->cmdtype.error.u1ErrorCmd == OPEN_CMD))))
        {
            i4bReplyRcvd = TRUE;
        }

        if (PtaPRetParms->u1Cmd == CONN_REPORT)
        {
            /* Asynchronous Message arrived. Handle it. Check to see 
             * if the remote end refused Connection.If yes return error.
             */
            switch (PtaPRetParms->cmdtype.message.u1Message)
            {
                case TCPE_CONN_REFUSED:
                    CpsdtSock->i1ErrorCode = SLI_ECONNREFUSED;
                    SLI_ERR (SLI_ECONNREFUSED);
                    i4RetVal = SLI_FAILURE;
                    break;

                case TCPE_TIMEDOUT:
                    CpsdtSock->i1ErrorCode = SLI_ETIMEDOUT;
                    SLI_ERR (SLI_ETIMEDOUT);
                    i4RetVal = SLI_FAILURE;
                    break;

                case TCPE_ICMP_MSG_ARRIVED:
                {
#ifdef IP_WANTED
                    if (PtaPRetParms->cmdtype.message.u1IcmpType ==
                        ICMP_DEST_UNREACH)
                    {
                        switch (PtaPRetParms->cmdtype.message.u1IcmpCode)
                        {
                            case ICMP_NET_UNREACH:
                            case ICMP_HOST_UNREACH:
                            case ICMP_PORT_UNREACH:
                            case ICMP_PROTO_UNREACH:
                                CpsdtSock->i1ErrorCode = SLI_ENETUNREACH;
                                SLI_ERR (SLI_ENETUNREACH);
                        }
                        i4RetVal = SLI_FAILURE;
                    }
#endif
#ifdef IP6_WANTED

                    if (PtaPRetParms->cmdtype.message.u1IcmpType ==
                        ICMP6_DEST_UNREACHABLE)
                    {
                        switch (PtaPRetParms->cmdtype.message.u1IcmpCode)
                        {
                            case ICMP6_NO_ROUTE_TO_DEST:
                            case ICMP6_ADDRESS_UNREACHABLE:
                            case ICMP6_PORT_UNREACHABLE:
                            case ICMP6_COMM_ADM_PROHIBITED:
                                CpsdtSock->i1ErrorCode = SLI_ENETUNREACH;
                                SLI_ERR (SLI_ENETUNREACH);
                        }
                        i4RetVal = SLI_FAILURE;
                    }
#endif
                }
                    break;
            }

            if (CpsdtSock->fpSliTcpHandleAsyncMesg)
            {
                i4AsyncSockDesc = GetTCBSockDesc (PtaPRetParms->u4ConnId);
                CpsdtSock->fpSliTcpHandleAsyncMesg (i4AsyncSockDesc,
                                                    PtaPRetParms);
            }
            else
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);

            OsixSemGive (CpsdtSock->ProtectSemId);
            return i4RetVal;
        }
    }
    OsixSemGive (CpsdtSock->ProtectSemId);
    return (SliTcpProcessReturnMesg (i4SockDesc, PtaPRetParms));
#else
    OsixSemGive (CpsdtSock->ProtectSemId);
    return SLI_FAILURE;
#endif
}

#ifdef TCP_WANTED
/*************************************************************************/
/*  Function Name   : SliListen                                          */
/*  Description     : This function sets the max number of connection    */
/*                    requests that can be outstanding for a connection  */
/*                    oriented server.                                   */
/*  Input(s)        :                                                    */
/*                    i4SockDesc - The socket identifier.                */
/*                    i4Backlog - The max number of out standing requests*/
/*  Output(s)       : None.                                              */
/*  Returns         : SUCCESS - If Sucessful                             */
/*                    SLI_ENOTSOCK - If the socket desc is    */
/*                                              invalid.                 */
/*************************************************************************/

INT4
SliListen (INT4 i4SockDesc, INT4 i4Backlog)
{
    tSdtEntry          *CpsdtSock = NULL;
    tTcpToApplicationParms *PtaPRetParms = NULL;
    INT4                i4bReplyRcvd = FALSE;
    INT4                i4RetVal = ZERO;

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) ||
        (CpsdtSock->i2SdtSockType == SOCK_RAW))
    {
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    /* negative values for backlog should not be allowed */
    if ((i4Backlog < ZERO) || (i4Backlog > SLI_MAX_BACK_LOG))
    {
        CpsdtSock->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    CpsdtSock->pAccept = MemAllocMemBlk (gSliAcceptListPoolId);
    if (CpsdtSock->pAccept == NULL)
    {
        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
        SLI_ERR (SLI_EMEMFAIL);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    TMO_SLL_Init (CpsdtSock->pAccept);

    CpsdtSock->i4MaxPendingOpens = i4Backlog;

    while (i4bReplyRcvd == FALSE)
    {
        i4RetVal = SliTcpIssueOpenRequest (i4SockDesc, PASSIVE_OPEN);
        if (i4RetVal != SLI_SUCCESS)
        {
            CpsdtSock->i1ErrorCode = (INT1) i4RetVal;
            SLI_ERR (i4RetVal);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }

        PtaPRetParms = (tTcpToApplicationParms *) CpsdtSock->pListen;
        if (PtaPRetParms == NULL)
        {
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
        if (CpsdtSock->pListen)
        {
            CpsdtSock->pListen = NULL;
        }

        if ((CpsdtSock->u4ValidOptions & TCPO_NODELAY) ||
            (PtaPRetParms->u1Cmd == OPEN_CMD) ||
            (((PtaPRetParms->u1Cmd == ERROR_MSG) &&
              (PtaPRetParms->cmdtype.error.u1ErrorCmd == OPEN_CMD))))
        {
            i4bReplyRcvd = TRUE;
        }
    }
    OsixSemGive (CpsdtSock->ProtectSemId);

    return (SliTcpProcessReturnMesg (i4SockDesc, PtaPRetParms));
}

/*************************************************************************/
/*  Function Name   : SliAccept                                          */
/*  Description     : This function waits for a connection request to    */
/*                    arrive and, after the request arrives, fills up    */
/*                    the return addrs info of the peer.                 */
/*  Input(s)        : i4SockDesc - The Socket Identifier.                */
/*  Output(s)       :                                                    */
/*                    cpsa_peer_addr - The address of the peer that      */
/*                                issued the connection request.         */
/*                    pi4Addrlen - The length of the address.            */
/*  Returns         : New Socket Id - On sucess of the Connection on     */
/*                             which the connection request arrived.     */
/*                    SLI_ENOTSOCK : If the socket desc is    */
/*                                          invalid.                     */
/**************************************************************************/

INT4
SliAccept (INT4 i4SockDesc, struct sockaddr *cpsa_peer_addr, INT4 *pi4Addrlen)
{
    tTcpToApplicationParms *PtaPRetParms = NULL;

    tSdtEntry          *CpsdtSock = NULL;
    tSin6              *Cpsin6Tmp = NULL;
    tSin               *CpsinTmp = NULL;
    tSdtEntry          *PsdtTmpSock = NULL;
    INT4                i4NewSockDesc = SLI_FAILURE;
    INT4                i4bReplyRcvd = FALSE;
    tSliSllNode        *node = NULL;

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) ||
        (CpsdtSock->i2SdtSockType == SOCK_RAW))
    {
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    while (i4bReplyRcvd == FALSE)
    {
        node = (tSliSllNode *) TMO_SLL_Get (CpsdtSock->pAccept);
        if (node == NULL)
        {
            if (CpsdtSock->u1FcntlOpns & FCNTL_NOBLK)
            {
                CpsdtSock->i1ErrorCode = SLI_EAGAIN;
                SLI_ERR (SLI_EAGAIN);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            else
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                if ((OsixSemTake (CpsdtSock->ConnSemId)) != OSIX_SUCCESS)
                {
                    SliDestroySocket (i4SockDesc);
                    SLI_ERR (SLI_ENOBUFS);
                    return SLI_FAILURE;
                }
                node = (tSliSllNode *) TMO_SLL_Get (CpsdtSock->pAccept);
            }
            if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
            {
                SliDestroySocket (i4SockDesc);
                SLI_ERR (SLI_ENOBUFS);
                return SLI_FAILURE;
            }
        }
        if (node != NULL)
        {
            PtaPRetParms = (tTcpToApplicationParms *) node->pReplyPkt;
            if (PtaPRetParms->u1Cmd == NEW_CONN)
            {
                i4bReplyRcvd = TRUE;
            }
            else
            {
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                SliFreeMemToPool (gSliMemPoolId, node);
            }
        }
    }

    /*   Allocate a new socket descriptor and fill in the addresses */
    i4NewSockDesc =
        SliSocket (CpsdtSock->i2SdtFamily, SOCK_STREAM, IPPROTO_TCP);
    if (i4NewSockDesc < ZERO)
    {
        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
        SliFreeMemToPool (gSliMemPoolId, node);
        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
        SLI_ERR (SLI_EMEMFAIL);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    PsdtTmpSock = SOCK_DESC_TABLE[i4NewSockDesc];
    if (PsdtTmpSock == NULL)
    {
        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
        SliFreeMemToPool (gSliMemPoolId, node);
        SliDestroySocket (i4NewSockDesc);
        SLI_ERR (SLI_ENOBUFS);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    if ((OsixSemTake (PsdtTmpSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
        SliFreeMemToPool (gSliMemPoolId, node);
        SliDestroySocket (i4NewSockDesc);
        SLI_ERR (SLI_ENOBUFS);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    V6_ADDR_COPY (&PsdtTmpSock->LocalIpAddr,
                  &PtaPRetParms->cmdtype.conn.LocalIp);
    PsdtTmpSock->u2LocalPort = PtaPRetParms->cmdtype.conn.u2LocalPort;
    V6_ADDR_COPY (&PsdtTmpSock->RemoteIpAddr,
                  &PtaPRetParms->cmdtype.conn.RemoteIp);
    PsdtTmpSock->u2RemotePort = PtaPRetParms->cmdtype.conn.u2RemotePort;
    PsdtTmpSock->u4ConnId = PtaPRetParms->cmdtype.conn.u4NewConn;
    PsdtTmpSock->u4IfIndex = PtaPRetParms->cmdtype.conn.u4IfIndex;
    GetTCBSockDesc (PsdtTmpSock->u4ConnId) = i4NewSockDesc;
    PsdtTmpSock->state = SS_CONNECTED;
    PsdtTmpSock->u4ValidOptions = CpsdtSock->u4ValidOptions;
    PsdtTmpSock->u1FcntlOpns = CpsdtSock->u1FcntlOpns;
    PsdtTmpSock->u4TxContextId = CpsdtSock->u4TxContextId;
    PsdtTmpSock->u4RxContextId = CpsdtSock->u4RxContextId;
    if (CpsdtSock->i2SdtFamily == AF_INET6)
    {
        Cpsin6Tmp = (tSin6 *) (VOID *) cpsa_peer_addr;
        Cpsin6Tmp->sin6_family = PsdtTmpSock->i2SdtFamily;
        Cpsin6Tmp->sin6_port = OSIX_HTONS (PsdtTmpSock->u2RemotePort);
        V6_HTONL ((tIpAddr *) (VOID *) (&Cpsin6Tmp->sin6_addr),
                  &PsdtTmpSock->RemoteIpAddr);
        *(pi4Addrlen) = sizeof (tSin6);
    }
    else
    {
        CpsinTmp = (tSin *) (VOID *) cpsa_peer_addr;
        CpsinTmp->sin_family = PsdtTmpSock->i2SdtFamily;
        CpsinTmp->sin_port = OSIX_HTONS (PsdtTmpSock->u2RemotePort);
        /* return to the application the v4 address */
        CpsinTmp->sin_addr.s_addr =
            OSIX_HTONL (V4_FROM_V6_ADDR (PsdtTmpSock->RemoteIpAddr));
        *(pi4Addrlen) = sizeof (tSin);
    }

    FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
    SliFreeMemToPool (gSliMemPoolId, node);
    OsixSemGive (CpsdtSock->ProtectSemId);
    OsixSemGive (PsdtTmpSock->ProtectSemId);

    return (i4NewSockDesc);
}
#endif /* TCP_WANTED */

/************************************************************************/
/*  Function Name   : SliRead                                           */
/*  Description     : This function reads the requested number of bytes */
/*                    from the socket specified.                        */
/*  Input(s)        :                                                   */
/*                    i4SockDesc - The socket identifier.               */
/*                    i4NumToRead - Number of bytes to read.            */
/*  Output(s)       : cpi1ReadBuf - Buffer for storing the data read    */
/*  Returns         : Number of bytes read - if the read was sucessful  */
/*                    SLI_ENOTSOCK - If the socket desc is   */
/*                                           invalid.                   */
/*                    SLI_ENOTCONN: The socket is not in  */
/*                                        a connected state.            */
/*                    SLI_ERR_COULDNT_READ_DATA : The read request to   */
/*                                                       failed.        */
/************************************************************************/

INT4
SliRead (INT4 i4SockDesc, VOID *cpi1ReadBuf, INT4 i4NumToRead)
{
    tSdtEntry          *CpsdtSock = NULL;
    INT4                i4NumRcvd = ZERO;
#ifdef TCP_WANTED
    INT4                i4RetError = ZERO;
    tApplicationToTcpParms Parms;
#endif

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if (((CpsdtSock->state != SS_CONNECTED)
         && (CpsdtSock->state != SS_TX_SHUTDOWN))
        || (CpsdtSock->state == SS_RX_SHUTDOWN))
    {
        CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
        SLI_ERR (SLI_ENOTCONN);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) ||
        (CpsdtSock->i2SdtSockType == SOCK_RAW))
    {
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SliRecvfrom (i4SockDesc, cpi1ReadBuf, i4NumToRead, ZERO, NULL,
                            ZERO);
    }

#ifdef TCP_WANTED

    Parms.u1Cmd = RECEIVE_CMD;
    Parms.u4ContextId = CpsdtSock->u4RxContextId;
    Parms.cmdtype.receive.u4ConnId = CpsdtSock->u4ConnId;
    Parms.cmdtype.receive.pBuf = (UINT1 *) cpi1ReadBuf;
    Parms.cmdtype.receive.u2BufLen = (UINT2) i4NumToRead;
    OsixSemGive (CpsdtSock->ProtectSemId);

    if ((i4RetError =
         SLI_TCP_RCV_NORMAL_DATA (i4SockDesc, Parms, &i4NumRcvd)) != OK)
    {
        return SLI_FAILURE;
    }
#endif /* TCP_WANTED */
    return i4NumRcvd;
}

/*************************************************************************/
/*  Function Name   : SliWrite                                           */
/*  Description     : This function writes the requested number of bytes */
/*                    to the socket specified.                           */
/*  Input(s)        :                                                    */
/*                    i4SockDesc - The socket identifier.                */
/*                    Cpi1WriteBuf - Buffer containing the data to       */
/*                                              be written.              */
/*                   i4NumToWrite - Number of bytes to write.            */
/*  Output(s)       : None.                                              */
/*  Returns         : Number of bytes written if the write was sucessful */
/*                    SLI_ENOTSOCK : If the socket desc is    */
/*                                          invalid.                     */
/*                    SLI_ENOTCONN: The socket is not in   */
/*                                           a connected state.          */
/*                    SLI_ERR_COULDNT_WRITE_DATA : The write request to  */
/*                                          TCP failed.                  */
/*************************************************************************/

INT4
SliWrite (INT4 i4SockDesc, CONST VOID *Cpi1WriteBuf, INT4 i4NumToWrite)
{
    tSdtEntry          *CpsdtSock = NULL;
#ifdef TCP_WANTED
    tTcpToApplicationParms *PtaPRetParms = NULL;
    INT4                i4bReplyRcvd = FALSE;
    INT4                i4FragmentToWrite = SLI_ZERO;
    INT4                i4TmpNumToWrite = SLI_ZERO;
    INT4                i4NumOctetsWritten = SLI_ZERO;
    UINT4               u4IfIndex = ZERO;
#endif

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if (((CpsdtSock->state != SS_CONNECTED)
         && (CpsdtSock->state != SS_RX_SHUTDOWN))
        || (CpsdtSock->state == SS_TX_SHUTDOWN))
    {
        CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
        SLI_ERR (SLI_ENOTCONN);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) ||
        (CpsdtSock->i2SdtSockType == SOCK_RAW))
    {
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SliSendto (i4SockDesc, Cpi1WriteBuf, i4NumToWrite, ZERO, NULL,
                          ZERO);
    }

#ifdef TCP_WANTED
    if (CpsdtSock->u1SockOpns & SO_DONTROUTE)
    {
        if (SliIsIpAddrLocalInCxt (CpsdtSock->u4TxContextId,
                                   CpsdtSock->RemoteIpAddr, &u4IfIndex) ==
            IP_FAILURE)
        {
            CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
            SLI_ERR (SLI_ENETUNREACH);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
        else
        {
            if (CpsdtSock->i2SdtFamily == AF_INET)
            {
                CpsdtSock->inPktinfo.ipi_ifindex = u4IfIndex;
            }
            else if (CpsdtSock->i2SdtFamily == AF_INET6)
            {
                CpsdtSock->in6Pktinfo.ipi6_ifindex = u4IfIndex;
            }
        }
    }

    i4FragmentToWrite = ((TCP_MAX_SEGMENT_SIZE > i4NumToWrite)
                         ? i4NumToWrite : TCP_MAX_SEGMENT_SIZE);

    i4TmpNumToWrite = i4NumToWrite;

    while (i4FragmentToWrite > ZERO)
    {
        PtaPRetParms = SliTcpIssueWriteRequest (i4SockDesc,
                                                (((INT1 *) (FS_ULONG)
                                                  Cpi1WriteBuf) +
                                                 i4NumOctetsWritten),
                                                i4FragmentToWrite,
                                                (UINT1) NULL_FLAG,
                                                NULL_TIMEOUT);

        if (PtaPRetParms == NULL)
        {
            OsixSemGive (CpsdtSock->ProtectSemId);
            return i4NumOctetsWritten;
        }

        if ((CpsdtSock->u4ValidOptions & TCPO_NODELAY) ||
            (PtaPRetParms->u1Cmd == SEND_CMD) ||
            ((PtaPRetParms->u1Cmd == ERROR_MSG) &&
             (PtaPRetParms->cmdtype.error.u1ErrorCmd == SEND_CMD)))
        {
            i4bReplyRcvd = TRUE;
        }

        if (i4bReplyRcvd != TRUE)
        {
            FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }

        if (PtaPRetParms->u1Cmd == ERROR_MSG)
        {
            if (CpsdtSock->u4ValidOptions & TCPO_NODELAY)
            {
                /* return for the non-blocking send call */
                CpsdtSock->i1ErrorCode =
                    PtaPRetParms->cmdtype.error.i1ErrorCode;
                SLI_ERR (PtaPRetParms->cmdtype.error.i1ErrorCode);
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                OsixSemGive (CpsdtSock->ProtectSemId);
                if (i4NumOctetsWritten > 0)
                {
                    /* partial data written */
                    return i4NumOctetsWritten;
                }
                else
                {
                    /* No data written */
                    return SLI_FAILURE;
                }
            }

            if (PtaPRetParms->cmdtype.error.i1ErrorCode == TCPE_AGAIN)
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                SLI_DELAY_TASK (SLI_DELAY_TASK_ONE_UNIT);
                OsixSemTake (CpsdtSock->ProtectSemId);
            }
            else
            {
                if (i4NumOctetsWritten > ZERO)
                {
                    FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return i4NumOctetsWritten;
                }
                else
                {
                    CpsdtSock->i1ErrorCode =
                        PtaPRetParms->cmdtype.error.i1ErrorCode;
                    SLI_ERR (PtaPRetParms->cmdtype.error.i1ErrorCode);
                    FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
            }
        }
        else
        {
            if (i4FragmentToWrite != (INT4) PtaPRetParms->cmdtype.send.u4BufLen)
            {
                i4FragmentToWrite = (INT4) PtaPRetParms->cmdtype.send.u4BufLen;
                if (CpsdtSock->u4ValidOptions & TCPO_NODELAY)
                {
                    i4NumOctetsWritten += i4FragmentToWrite;
                    /* For the non-blocking write, return with
                     * partial data count as return value.
                     * since partial data is sent, put
                     * EGAIN error code additionally */
                    if (i4NumOctetsWritten == 0)
                    {
                        CpsdtSock->i1ErrorCode = SLI_ENOBUFS;
                        SLI_ERR (SLI_ENOBUFS);
                        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    CpsdtSock->i1ErrorCode = TCPE_AGAIN;
                    SLI_ERR (CpsdtSock->i1ErrorCode);
                    FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                    break;
                }
                else
                {
                    /* In blocking case, give time to recover */
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    SLI_DELAY_TASK (SLI_DELAY_TASK_ONE_UNIT);
                    OsixSemTake (CpsdtSock->ProtectSemId);
                }
            }
        }

        i4NumOctetsWritten += i4FragmentToWrite;
        i4TmpNumToWrite -= i4FragmentToWrite;
        if (i4TmpNumToWrite < TCP_MAX_SEGMENT_SIZE)
        {
            i4FragmentToWrite = i4TmpNumToWrite;
        }
        else
        {
            i4FragmentToWrite = TCP_MAX_SEGMENT_SIZE;
        }

        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
    }

    OsixSemGive (CpsdtSock->ProtectSemId);
    return i4NumOctetsWritten;
#else
    OsixSemGive (CpsdtSock->ProtectSemId);
    return SLI_SUCCESS;
#endif
}

/************************************************************************/
/*  Function Name   : SliRecv                                           */
/*  Description     : This function is used to read as well as          */
/*                    control the reception of data.                    */
/*  Input(s)        :                                                   */
/*                    i4SockDesc - The socket identifier.               */
/*                    i4NumToRead - Number of octets to read.           */
/*                    u4Flags - Control flags.                          */
/*  Output(s)       : cpi1ReadBuf - The storage for holding the data    */
/*                                  to be read.                         */
/*  Returns         : Number of bytes read if the read was sucessful    */
/*                    SLI_ENOTSOCK :                         */
/*                       If the socket desc is invalid.                 */
/*                    SLI_ENOTCONN:                       */
/*                       The socket is not in a connected stat          */
/*                    SLI_ERR_COULDNT_READ_DATA :                       */
/*                       The read request to TCP failed.                */
/*                    SLI_EINVAL:                             */
/*                       The flags param is invalid                     */
/************************************************************************/

INT4
SliRecv (INT4 i4SockDesc, VOID *cpi1ReadBuf, INT4 i4NumToRead, UINT4 u4Flags)
{

    tSdtEntry          *CpsdtSock = NULL;

#ifdef TCP_WANTED
    INT4                i4NumRcvd = ZERO;
    INT4                i4bRcvUrgData = FALSE;
    INT4                i4RetError = ZERO;
    tApplicationToTcpParms Parms;
#endif

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if ((CpsdtSock->i2SdtSockType == SOCK_STREAM) &&
        (((CpsdtSock->state != SS_CONNECTED)
          && (CpsdtSock->state != SS_TX_SHUTDOWN))
         || (CpsdtSock->state == SS_RX_SHUTDOWN)))
    {
        CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
        SLI_ERR (SLI_ENOTCONN);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) ||
        (CpsdtSock->i2SdtSockType == SOCK_RAW))
    {
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SliRecvfrom (i4SockDesc, cpi1ReadBuf, i4NumToRead,
                            u4Flags, NULL, ZERO);
    }
    if (u4Flags & MSG_PEEK)
    {
        /* Currently Peek Option is not Supported */
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

#ifdef TCP_WANTED
    if (u4Flags)
    {
        if (u4Flags & MSG_OOB)
        {
            i4bRcvUrgData = TRUE;
        }
        if (u4Flags & MSG_NOSIGNAL)
        {
            /* Currently this option is not supported */
        }
        else
        {
            if (i4bRcvUrgData != TRUE)
            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
        }
    }
    Parms.u1Cmd = RECEIVE_CMD;
    Parms.u4ContextId = CpsdtSock->u4RxContextId;
    Parms.cmdtype.receive.u4ConnId = CpsdtSock->u4ConnId;
    Parms.cmdtype.receive.pBuf = (UINT1 *) cpi1ReadBuf;
    Parms.cmdtype.receive.u2BufLen = (UINT2) i4NumToRead;
    OsixSemGive (CpsdtSock->ProtectSemId);

    if (i4bRcvUrgData)
    {
        if ((i4RetError =
             SLI_TCP_RCV_URG_DATA (i4SockDesc, Parms, &i4NumRcvd)) != OK)
        {
            return SLI_FAILURE;
        }
    }                            /* If Urg. Data needed. */
    else
    {
        if ((i4RetError =
             SLI_TCP_RCV_NORMAL_DATA (i4SockDesc, Parms, &i4NumRcvd)) != OK)
        {
            return SLI_FAILURE;
        }
    }                            /* else if normal data needed */
    return i4NumRcvd;
#else
    OsixSemGive (CpsdtSock->ProtectSemId);
    return SLI_SUCCESS;
#endif
}

/*************************************************************************/
/*  Function Name   : SliSend                                            */
/*  Description     : This function can be used by the caller to send    */
/*                    data as well as to have control over the           */
/*                    transmission.                                      */
/*  Input(s)        :                                                    */
/*                    i4SockDesc - The socket identifier.                */
/*                    Cpi1WriteBuf - The buffer containing data to       */
/*                                   be sent.                            */
/*                    i4NumToWrite - Number of bytes to be sent.         */
/*                    u4Flags -  Control flags.                          */
/*  Output(s)       : None.                                              */
/*  Returns         : Number of bytes written if the write was sucessful */
/*                    SLI_ENOTSOCK - If the socket desc is    */
/*                                          invalid.                     */
/*                    SLI_ENOTCONN: The socket is not in a */
/*                                            connected state.           */
/*                    SLI_ERR_COULDNT_WRITE_DATA: The write request to   */
/*                                           TCP failed.                 */
/*                    SLI_EINVAL: The flags param is invalid   */
/*************************************************************************/
INT4
SliSend (INT4 i4SockDesc, CONST VOID *Cpi1WriteBuf, INT4 i4NumToWrite,
         UINT4 u4Flags)
{

    tSdtEntry          *CpsdtSock = NULL;
#ifdef TCP_WANTED
    tTcpToApplicationParms *PtaPRetParms = NULL;
    INT4                i4bReplyRcvd = FALSE;
    UINT1               u1TcpFlag = ZERO;
    INT4                i4FragmentToWrite = SLI_ZERO;
    INT4                i4TmpNumToWrite = SLI_ZERO;
    INT4                i4NumOctetsWritten = SLI_ZERO;
    UINT4               u4IfIndex = INVALID_INTERFACE;
#endif

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if (((CpsdtSock->state != SS_CONNECTED)
         && (CpsdtSock->state != SS_RX_SHUTDOWN))
        || (CpsdtSock->state == SS_TX_SHUTDOWN))
    {
        CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
        SLI_ERR (SLI_ENOTCONN);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) ||
        (CpsdtSock->i2SdtSockType == SOCK_RAW))
    {
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SliSendto (i4SockDesc, Cpi1WriteBuf, i4NumToWrite,
                          u4Flags, NULL, ZERO);
    }

#ifdef TCP_WANTED
    if (!((u4Flags & MSG_OOB) ||
          (u4Flags & MSG_PUSH) ||
          (u4Flags == MSG_ZERO) || (u4Flags & MSG_NOSIGNAL)))
    {
        CpsdtSock->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if (u4Flags & MSG_OOB)
    {
        u1TcpFlag |= URGENT_DATA;
    }
    if (u4Flags & MSG_PUSH)
    {
        u1TcpFlag |= PUSH_DATA;
    }
    if ((u4Flags == MSG_ZERO) || (u4Flags & MSG_NOSIGNAL))
    {
        /* Not supported */
    }

    if (CpsdtSock->u1SockOpns & SO_DONTROUTE)
    {
        /* Get IfIndex of RemoteIpAddr in the particular context */
        if (SliIsIpAddrLocalInCxt (CpsdtSock->u4TxContextId,
                                   CpsdtSock->RemoteIpAddr, &u4IfIndex) ==
            IP_FAILURE)
        {
            CpsdtSock->i1ErrorCode = SLI_ENETUNREACH;
            SLI_ERR (SLI_ENETUNREACH);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
        else
        {
            if (CpsdtSock->i2SdtFamily == AF_INET)
            {
                CpsdtSock->inPktinfo.ipi_ifindex = u4IfIndex;
            }
            else if (CpsdtSock->i2SdtFamily == AF_INET6)
            {
                CpsdtSock->in6Pktinfo.ipi6_ifindex = u4IfIndex;
            }
        }
    }

    i4FragmentToWrite = ((TCP_MAX_SEGMENT_SIZE > i4NumToWrite)
                         ? i4NumToWrite : TCP_MAX_SEGMENT_SIZE);

    i4TmpNumToWrite = i4NumToWrite;

    while (i4FragmentToWrite > ZERO)
    {
        PtaPRetParms = SliTcpIssueWriteRequest (i4SockDesc,
                                                (((INT1 *) (FS_ULONG)
                                                  Cpi1WriteBuf) +
                                                 i4NumOctetsWritten),
                                                i4FragmentToWrite,
                                                (UINT1) u1TcpFlag,
                                                NULL_TIMEOUT);

        if (PtaPRetParms == NULL)
        {
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }

        if ((CpsdtSock->u4ValidOptions & TCPO_NODELAY) ||
            (PtaPRetParms->u1Cmd == SEND_CMD) ||
            ((PtaPRetParms->u1Cmd == ERROR_MSG) &&
             (PtaPRetParms->cmdtype.error.u1ErrorCmd == SEND_CMD)))
        {
            i4bReplyRcvd = TRUE;
        }

        if (i4bReplyRcvd != TRUE)
        {
            FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }

        if (PtaPRetParms->u1Cmd == ERROR_MSG)
        {
            if (CpsdtSock->u4ValidOptions & TCPO_NODELAY)
            {
                /* return for the non-blocking send call */
                CpsdtSock->i1ErrorCode =
                    PtaPRetParms->cmdtype.error.i1ErrorCode;
                SLI_ERR (PtaPRetParms->cmdtype.error.i1ErrorCode);
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                OsixSemGive (CpsdtSock->ProtectSemId);
                if (i4NumOctetsWritten > 0)
                {
                    /* partial data written */
                    return i4NumOctetsWritten;
                }
                else
                {
                    /* No data written */
                    return SLI_FAILURE;
                }

            }

            if (PtaPRetParms->cmdtype.error.i1ErrorCode == TCPE_AGAIN)
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                SLI_DELAY_TASK (SLI_DELAY_TASK_ONE_UNIT);

                if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
                {
                    FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                    SLI_ERR (SLI_ENOBUFS);
                    return SLI_FAILURE;
                }
            }
            else
            {
                if (i4NumOctetsWritten > ZERO)
                {
                    FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return i4NumOctetsWritten;
                }
                else
                {
                    CpsdtSock->i1ErrorCode =
                        PtaPRetParms->cmdtype.error.i1ErrorCode;
                    SLI_ERR (PtaPRetParms->cmdtype.error.i1ErrorCode);
                    FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
            }
        }

        if (PtaPRetParms->u1Cmd == SEND_CMD)
        {
            if (((UINT4) i4FragmentToWrite) !=
                PtaPRetParms->cmdtype.send.u4BufLen)
            {
                i4FragmentToWrite = (INT4) PtaPRetParms->cmdtype.send.u4BufLen;
                i4NumOctetsWritten += i4FragmentToWrite;
                if (CpsdtSock->u4ValidOptions & TCPO_NODELAY)
                {
                    /* For the non-blocking send, return with
                     * partial data count as return value.
                     * since partial data is sent, put
                     * EGAIN error code additionally */
                    if (i4NumOctetsWritten == 0)
                    {
                        CpsdtSock->i1ErrorCode = SLI_ENOBUFS;
                        SLI_ERR (SLI_ENOBUFS);
                        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    CpsdtSock->i1ErrorCode = TCPE_AGAIN;
                    SLI_ERR (CpsdtSock->i1ErrorCode);
                    FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                    break;
                }
                else
                {
                    /* In blocking case, give time to recover */
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    SLI_DELAY_TASK (SLI_DELAY_TASK_ONE_UNIT);
                    OsixSemTake (CpsdtSock->ProtectSemId);
                }
            }
            else
            {
                i4NumOctetsWritten += i4FragmentToWrite;
            }
        }
        i4TmpNumToWrite -= i4FragmentToWrite;
        if (i4TmpNumToWrite < TCP_MAX_SEGMENT_SIZE)
        {
            i4FragmentToWrite = i4TmpNumToWrite;
        }
        else
        {
            i4FragmentToWrite = TCP_MAX_SEGMENT_SIZE;
        }
        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
    }

    OsixSemGive (CpsdtSock->ProtectSemId);
    return i4NumOctetsWritten;
#else
    OsixSemGive (CpsdtSock->ProtectSemId);
    return SLI_SUCCESS;
#endif
}

/**************************************************************************/
/*  Function Name   : SliSelect                                           */
/*  Description     : This function can be used to give the SLI a set of  */
/*                    descriptors for read, write and exception pending   */
/*                    events with the option of a timeout.If time = 0,    */
/*                    the status of all the descriptors is returned.      */
/*  Input(s)        : i4Maxfdpl - Maximum value among socket descriptors  */
/*                    readfds - set of descriptors for read pending       */
/*                    writefds - set of descriptors for write pending     */
/*                    exceptfds - set of descriptors for exception pendin */
/*                    timeval : the amount of time to wait                */
/*  Output(s)       : if time = 0, the fdsets contain the status of the   */
/*                    descriptors.                                        */
/*  Returns         : No. of descriptors ready,                           */
/*                    0 for timeout or error code                         */
/**************************************************************************/

INT4
SliSelect (INT4 i4MaxFd, SliFdSet * pReadFd, SliFdSet * pWriteFd,
           SliFdSet * pExceptFd, struct timeval *pTimeOut)
{
    SliFdSet            TempReadFd, *pTempReadFd = NULL;
    SliFdSet            TempWriteFd, *pTempWriteFd = NULL;
    SliFdSet            TempExceptFd, *pTempExceptFd = NULL;
    INT4                i4RetVal = SLI_FAILURE;
    tSelectNode         SelectNode;
    UINT4               u4Event = ZERO;

    MEMSET (&SelectNode, 0, sizeof (SelectNode));
    /* Validate the input parameters */
    /* Validate Maximum no of Fd's */
    if ((i4MaxFd < SLI_FIRST_SOCKET_FD) || (i4MaxFd > (INT4) MAX_NO_OF_SOCKETS))
    {
        SLI_ERR (SLI_EINVAL);
        return SLI_FAILURE;
    }

    if ((pReadFd == NULL) && (pWriteFd == NULL) && (pExceptFd == NULL))
    {
        if (pTimeOut != NULL)
        {
            OsixTskDelay ((pTimeOut->tv_sec * SLI_CLOCK_TICK) +
                          ((pTimeOut->tv_usec * SLI_CLOCK_TICK) / MILLION));
            return SLI_SUCCESS;
        }
        else
        {
            SLI_ERR (SLI_EINVAL);
            return SLI_FAILURE;
        }
    }

    /* Allocate the node that would be added to the Select 
     *  Database(SLL) */

    SLI_SELECT_ENTER_CS ();

    if (OsixTskIdSelf (&(SelectNode.u4TaskId)) != OSIX_SUCCESS)
    {
        SLI_SELECT_EXIT_CS ();
        return SLI_FAILURE;
    }
    SelectNode.pReadFd = pReadFd;
    SelectNode.pWriteFd = pWriteFd;
    SelectNode.pExceptFd = pExceptFd;

    /* No socket is yet ready so wait for the maximum timeout, 
     * Add the select Fd to the Select table */

    TMO_SLL_Add (&gSelectTable, (tTMO_SLL_NODE *) & (SelectNode.pNextNode));
    SLI_SELECT_EXIT_CS ();

    if ((pTimeOut != NULL)
        && ((pTimeOut->tv_sec == ZERO) && (pTimeOut->tv_usec == ZERO)))
    {
        /* Issue Non-blocking select Request, SliProcessSelect does check 
         * if any of the socket is ready, if so It increments the count and
         * return the count, if any error it set the Error appropricately
         * and return SLI_FAILURE */
        SLI_SELECT_ENTER_CS ();
        i4RetVal = SliProcessSelect (i4MaxFd, pReadFd, pWriteFd, pExceptFd);
        TMO_SLL_Delete (&gSelectTable,
                        (tTMO_SLL_NODE *) & (SelectNode.pNextNode));
        SLI_SELECT_EXIT_CS ();
        OsixReceiveEvent ((UINT4) SOCKET_READY_EVENT, OSIX_NO_WAIT,
                          0, &u4Event);
        return i4RetVal;
    }
    else
    {
        do
        {
            /* This is a Blocking call with or without timeout, Copy the 
             * original Fd sets to Temporary Fd sets, so the original one's 
             * are not lost, incase there is a need to block, Do scan the 
             * Fdset's sockets's one */
            SLI_SELECT_ENTER_CS ();
            if (pReadFd)
            {
                MEMCPY (&TempReadFd, SelectNode.pReadFd, sizeof (SliFdSet));
                pTempReadFd = &TempReadFd;
            }
            if (pWriteFd)
            {
                MEMCPY (&TempWriteFd, SelectNode.pWriteFd, sizeof (SliFdSet));
                pTempWriteFd = &TempWriteFd;
            }
            if (pExceptFd)
            {
                MEMCPY (&TempExceptFd, SelectNode.pExceptFd, sizeof (SliFdSet));
                pTempExceptFd = &TempExceptFd;
            }

            /* Any packet to the socket will be queued and event will not
             * be posted from Layer 4 protocol, if there is a context switch
             * after SliProcessSelect function (where queue is verified for 
             * message presence) and before the select node addition to list.
             * To solve this issue,  LOCK is taken here itself. */

            /* Check if any socket is ready */
            i4RetVal =
                SliProcessSelect (i4MaxFd, pTempReadFd, pTempWriteFd,
                                  pTempExceptFd);

            if ((i4RetVal == SLI_FAILURE) || (i4RetVal > ZERO))
            {
                /* at least one socket is ready */
                if (pReadFd)
                {
                    MEMCPY (pReadFd, pTempReadFd, sizeof (SliFdSet));
                }
                if (pWriteFd)
                {
                    MEMCPY (pWriteFd, pTempWriteFd, sizeof (SliFdSet));
                }
                if (pExceptFd)
                {
                    MEMCPY (pExceptFd, pTempExceptFd, sizeof (SliFdSet));
                }

                TMO_SLL_Delete (&gSelectTable,
                                (tTMO_SLL_NODE *) & (SelectNode.pNextNode));
                SLI_SELECT_EXIT_CS ();
                OsixReceiveEvent ((UINT4) SOCKET_READY_EVENT, OSIX_NO_WAIT,
                                  0, &u4Event);
                return i4RetVal;
            }
            SLI_SELECT_EXIT_CS ();

            /* start the Select timeout timer here */
            i4RetVal = SliWaitForSelectEvent (pTimeOut, (i4MaxFd - 1));

            /* Error in FAIL and EINTR case is set by SliWaitForSelectEvent */
            if (i4RetVal == SLI_FAILURE)
            {
                SLI_SELECT_ENTER_CS ();
                TMO_SLL_Delete (&gSelectTable,
                                (tTMO_SLL_NODE *) & (SelectNode.pNextNode));
                SLI_SELECT_EXIT_CS ();
                return SLI_FAILURE;
            }

            if (i4RetVal == SLI_EINTR)
            {
                SLI_SELECT_ENTER_CS ();
                TMO_SLL_Delete (&gSelectTable,
                                (tTMO_SLL_NODE *) & (SelectNode.pNextNode));
                SLI_SELECT_EXIT_CS ();
                return SLI_SUCCESS;
            }

        }
        while ((pReadFd != NULL) && (i4RetVal == 0));
        SLI_SELECT_ENTER_CS ();
        /* packet arrived */
        i4RetVal = SliProcessSelect (i4MaxFd, pReadFd, pWriteFd, pExceptFd);

        TMO_SLL_Delete (&gSelectTable,
                        (tTMO_SLL_NODE *) & (SelectNode.pNextNode));
        SLI_SELECT_EXIT_CS ();
        return i4RetVal;
    }
}

/**************************************************************************/
/*   Function Name   : SliRecvfrom                                        */
/*   Description     : This function receives a dataram and fills up      */
/*                     source address in pPeerAddress and data in pi1Buf. */
/*   Input(s)        : i4SockDesc - Socket Descriptor                     */
/*                     pi1Buf - Pointer to buffer to store data received  */
/*                     i4BufLen - Indicates length of buffer              */
/*                     u4Flags - Flags not supported as of now            */
/*   Output(s)       :                                                    */
/*                     pi1Buf - Pointer to buffer to store data received  */
/*                     pPeerAddr - Pointer which holds peer address       */
/*                     pi4AddrLen - Pointer which holds peer address      */
/*                                  length.                               */
/*   Return Value    : Number of bytes received, on success               */
/*                     SLI_ENOTSOCK, on error                  */
/*                     SLI_EOPNOTSUPP, on error               */
/**************************************************************************/

INT4
SliRecvfrom (INT4 i4SockDesc, VOID *pi1Buf, INT4 i4BufLen, UINT4 u4Flags,
             struct sockaddr *pPeerAddr, INT4 *pi4AddrLen)
{
    UINT4               u4DataLen = ZERO;
    tSin6              *pPeerAddrIn6 = (tSin6 *) (VOID *) pPeerAddr;
    tIpAddr             IpTempAddr;
    tSin               *pPeerAddrIn = (tSin *) (VOID *) pPeerAddr;
    tCRU_BUF_CHAIN_HEADER *pBufChnHdr = NULL;
    t_UDP_TO_APP_MSG_PARMS *pParms = NULL;
    tIpParms           *pRawIpParms = NULL;
    tSliUdp6Parms      *pUdp6Parms = NULL;
    tSliRawLlToIp6Params *pRawIp6Parms = NULL;
    tSdtEntry          *CpsdtSock = NULL;
    tSliRawRecvQnode   *pRawQnode = NULL;
    tIpAddr             RemoteAddr;
    UINT2               u2RemotePort = 0;
    UINT1               u1DgramRcvd = SLI_ZERO;
    UINT4               u4ReadOffSet = ZERO;
    UINT1               u1Ttl = SLI_ZERO;

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }
    if (CpsdtSock->i2SdtSockType == SOCK_STREAM)
    {
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if ((IS_OOB_SET (u4Flags) == TRUE) || (IS_DONTROUTE_SET (u4Flags) == TRUE))
    {
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if (IS_PEEK_SET (u4Flags))
    {
        /* Currently Peek Option is not Supported */
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if (pi1Buf == NULL)
    {
        CpsdtSock->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if (CpsdtSock->state == SS_CONNECTED)
    {
        if (CpsdtSock->i2SdtFamily == AF_INET)
        {
            V4_MAPPED_ADDR_COPY (&RemoteAddr,
                                 V4_FROM_V6_ADDR (CpsdtSock->RemoteIpAddr));
        }
        else
        {
            V6_ADDR_COPY (&RemoteAddr, &CpsdtSock->RemoteIpAddr);
        }
        u2RemotePort = CpsdtSock->u2RemotePort;
    }

    if (CpsdtSock->i2SdtSockType == SOCK_DGRAM)
    {
        while (!u1DgramRcvd)
        {
            if (OsixQueRecv (CpsdtSock->UdpQId,
                             (UINT1 *) &pParms,
                             OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) != OSIX_SUCCESS)
            {
                if (CpsdtSock->u1FcntlOpns & FCNTL_NOBLK)
                {
                    CpsdtSock->i1ErrorCode = SLI_EWOULDBLOCK;
                    SLI_ERR (SLI_EWOULDBLOCK);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
                else
                {
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    if ((OsixSemTake (CpsdtSock->ConnSemId)) != OSIX_SUCCESS)
                    {
                        SliDestroySocket (i4SockDesc);
                        SLI_ERR (SLI_ENOBUFS);
                        return SLI_FAILURE;
                    }

                    if (OsixQueRecv (CpsdtSock->UdpQId,
                                     (UINT1 *) &pParms,
                                     OSIX_DEF_MSG_LEN,
                                     OSIX_NO_WAIT) != OSIX_SUCCESS)
                    {
                        return SLI_FAILURE;
                    }
                }
                if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
                {
                    SliDestroySocket (i4SockDesc);
                    SLI_ERR (SLI_ENOBUFS);
                    return SLI_FAILURE;
                }
            }
            /* taking the pparms from bufchnhdr. It is in the 
             * place of module data */
            if (CpsdtSock->i2SdtFamily == AF_INET6)
            {
                /* Get Packet buf and ContextId from pParms and
                 * process the arrived V6 packet */
                /* UDP6 Gives Address in Network Order */
                pUdp6Parms = (tSliUdp6Parms *) pParms;
                CpsdtSock->u4RxContextId = pUdp6Parms->u4ContextId;
                pBufChnHdr = pUdp6Parms->pBuf;

                V6_NTOHL (&IpTempAddr, &pUdp6Parms->IpSrcAddr);
                /* To be fixed for LOOP BACK ADDRESS ALSO */
                if ((IN6_IS_ADDR_LOOPBACK (IpTempAddr))
                    || ((CpsdtSock->state == SS_CONNECTED)
                        &&
                        (IS_V6DGRAM_PKT_FOR_ME
                         (&RemoteAddr, &IpTempAddr, u2RemotePort,
                          pUdp6Parms->u2SrcPort)))
                    || (CpsdtSock->state != SS_CONNECTED))
                {
                    u1DgramRcvd = SLI_ONE;
                }
                else
                {
                    /* Free the allocated mem for Params 
                     * and release recived Packet buffer */
                    /* If this socket descriptor is common for
                     * all context means, clear contextid */
                    if (CpsdtSock->u1FdMode == SOCK_GLOBAL_MODE)
                    {
                        CpsdtSock->u4RxContextId = VCM_DEFAULT_CONTEXT;
                    }
                    CRU_BUF_Release_MsgBufChain (pBufChnHdr, FALSE);
                    MemReleaseMemBlock (gUdp6MemPoolId, (UINT1 *) pUdp6Parms);
                    continue;
                }

                if (pPeerAddrIn6 != NULL)
                {
                    pPeerAddrIn6->sin6_family = AF_INET6;
                    V6_ADDR_COPY (&pPeerAddrIn6->sin6_addr,
                                  &pUdp6Parms->IpSrcAddr);
                    pPeerAddrIn6->sin6_port =
                        OSIX_HTONS (pUdp6Parms->u2SrcPort);
                    pPeerAddrIn6->sin6_scope_id = pUdp6Parms->u4IfIndex;
                    pPeerAddrIn = NULL;
                }

                if (CpsdtSock->u4IpPktinfoFlag != IP_PKTINFO_DIS)
                {
                    CpsdtSock->in6Pktinfo.ipi6_ifindex = pUdp6Parms->u4IfIndex;
                    V6_NTOHL ((tIpAddr *) (VOID *) &CpsdtSock->in6Pktinfo.
                              ipi6_addr,
                              (tIpAddr *) (VOID *) &pUdp6Parms->IpDstAddr);
                }
                u4DataLen = (UINT4) pUdp6Parms->u2Len;

            }
            else if (CpsdtSock->i2SdtFamily == AF_INET)
            {
                /* Get Packet buf and ContextId from pParms and
                 * process the arrived V4 packet */
                CpsdtSock->u4RxContextId = pParms->u4ContextId;
                pBufChnHdr = pParms->pBuf;
                if ((pParms->u4SrcIpAddr == INADDR_LOOPBACK)
                    || ((CpsdtSock->state == SS_CONNECTED)
                        &&
                        (IS_V4DGRAM_PKT_FOR_ME
                         (V4_FROM_V6_ADDR (RemoteAddr), pParms->u4SrcIpAddr,
                          u2RemotePort, pParms->u2SrcUdpPort)))
                    || (CpsdtSock->state != SS_CONNECTED))
                {
                    u1DgramRcvd = SLI_ONE;
                }
                else
                {
                    /* Free the allocated mem for Params 
                     * and release recived Packet buffer */
                    /* If this socket descriptor is common for
                     * all context means, clear contextid */
                    if (CpsdtSock->u1FdMode == SOCK_GLOBAL_MODE)
                    {
                        CpsdtSock->u4RxContextId = VCM_DEFAULT_CONTEXT;
                    }
                    CRU_BUF_Release_MsgBufChain (pBufChnHdr, FALSE);
                    MemReleaseMemBlock (gUdp4MemPoolId, (UINT1 *) pParms);
                    continue;
                }
                if (pPeerAddrIn != NULL)
                {
                    pPeerAddrIn->sin_family = AF_INET;
                    pPeerAddrIn->sin_addr.s_addr =
                        CRU_HTONL (pParms->u4SrcIpAddr);
                    pPeerAddrIn->sin_port = CRU_HTONS (pParms->u2SrcUdpPort);
                    pPeerAddrIn6 = NULL;
                }
                if (CpsdtSock->u4IpPktinfoFlag != IP_PKTINFO_DIS)
                {
                    CpsdtSock->inPktinfo.ipi_ifindex =
                        IP_PARMS_GET_IFACE_FROM_UDP_TO_APP_MSG_PARMS (pParms);
                    MEMCPY (&CpsdtSock->inPktinfo.ipi_spec_dst,
                            &pParms->u4SrcIpAddr, sizeof (UINT4));
                    MEMCPY (&CpsdtSock->inPktinfo.ipi_addr,
                            &pParms->u4DstIpAddr, sizeof (UINT4));
                }
                u4DataLen = (UINT4) pParms->u2Len;
            }
        }
    }

    else if (CpsdtSock->i2SdtSockType == SOCK_RAW)
    {
        if (SLI_SLL_Get_Count ((tSliRawQueue *) & CpsdtSock->RecvQueue) == ZERO)
        {
            if ((CpsdtSock->u1FcntlOpns & FCNTL_NOBLK)
                || (IS_DONTWAIT_SET (u4Flags) == TRUE))
            {
                CpsdtSock->i1ErrorCode = SLI_EWOULDBLOCK;
                SLI_ERR (SLI_EWOULDBLOCK);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            else
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                if ((OsixSemTake (CpsdtSock->ConnSemId)) != OSIX_SUCCESS)
                {
                    SliDestroySocket (i4SockDesc);
                    SLI_ERR (SLI_ENOBUFS);
                    return SLI_FAILURE;
                }
            }
            if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
            {
                SliDestroySocket (i4SockDesc);
                SLI_ERR (SLI_ENOBUFS);
                return SLI_FAILURE;
            }
        }
        pRawQnode = (tSliRawRecvQnode *) SLI_SLL_First ((tSliRawQueue *)
                                                        & CpsdtSock->RecvQueue);
        if (pRawQnode != NULL)
        {
            pBufChnHdr = pRawQnode->pRawPkt;
            CpsdtSock->u4RxContextId = pRawQnode->u4ContextId;
            if (CpsdtSock->i2SdtFamily == AF_INET)
            {
                SLI_GET_2_BYTE (pBufChnHdr, u4DataLen, IP_PKT_OFF_LEN);
                if (pPeerAddrIn != NULL)
                {
                    IP_COPY_FROM_BUF (pBufChnHdr,
                                      (UINT1 *) &pPeerAddrIn->sin_addr.s_addr,
                                      IP_PKT_OFF_SRC, sizeof (UINT4));
                }
                if (CpsdtSock->u4IpPktinfoFlag != IP_PKTINFO_DIS)
                {
                    pRawIpParms =
                        (tIpParms *) IP_GET_MODULE_DATA_PTR (pBufChnHdr);
                    CpsdtSock->inPktinfo.ipi_ifindex = pRawIpParms->u2Port;
                }
                u4ReadOffSet = 0;
            }
            else
            {
                SLI_GET_2_BYTE (pBufChnHdr, u4DataLen, IPV6_OFF_PAYLOAD_LEN);
                if (CpsdtSock->u4IpPktinfoFlag != IP_PKTINFO_DIS)
                {
                    pRawIp6Parms = SliRawIp6GetParms (pBufChnHdr);
                    CpsdtSock->in6Pktinfo.ipi6_ifindex = pRawIp6Parms->u4Port;

                    CRU_BUF_Copy_FromBufChain (pBufChnHdr,
                                               (UINT1 *) &u1Ttl,
                                               IP6_OFFSET_FOR_HOPLIMIT_FIELD,
                                               sizeof (UINT1));
                    CpsdtSock->in6Pktinfo.in6_hoplimit = u1Ttl;

                    CRU_BUF_Copy_FromBufChain (pBufChnHdr,
                                               (UINT1 *)
                                               (&(CpsdtSock->in6Pktinfo.
                                                  ipi6_addr.s6_addr)),
                                               IP6_OFFSET_FOR_DESTADDR_FIELD,
                                               IP6_ADDR_SIZE);

                    /* This conversion is required so that Recv Msg works
                     * correctly. */
                    V6_NTOHL ((tIpAddr *)
                              (VOID *) &CpsdtSock->in6Pktinfo.ipi6_addr,
                              (tIpAddr *)
                              (VOID *) &CpsdtSock->in6Pktinfo.ipi6_addr);
                }
                if (pPeerAddrIn6 != NULL)
                {
                    CRU_BUF_Copy_FromBufChain (pBufChnHdr,
                                               (UINT1
                                                *) (&(pPeerAddrIn6->sin6_addr)),
                                               IP6_OFFSET_FOR_SRCADDR_FIELD,
                                               IP6_ADDR_SIZE);
                }
                /* Move the valid offset to start of the Next header */
                u4ReadOffSet = IPV6_HEADER_LEN;
            }

            SLI_SLL_Delete (&CpsdtSock->RecvQueue, &pRawQnode->NextNodeInQueue);
            SliFreeMemToPool (QueueMemPoolId, pRawQnode);
        }
        else
        {
            CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
            SLI_ERR (SLI_EMEMFAIL);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
    }

    /* Check for invalid parameters and insufficient space */
    if ((i4BufLen <= ZERO) || ((UINT4) i4BufLen < u4DataLen))
    {
        /* If this socket descriptor is common for
         * all context means, clear contextid */
        if (CpsdtSock->u1FdMode == SOCK_GLOBAL_MODE)
        {
            CpsdtSock->u4RxContextId = VCM_DEFAULT_CONTEXT;
        }

        /* Release the received Packet buffer */
        CRU_BUF_Release_MsgBufChain (pBufChnHdr, FALSE);

        /* Free the allocated mem block for pParams */
        if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) &&
            (CpsdtSock->i2SdtFamily == AF_INET))
        {
            MemReleaseMemBlock (gUdp4MemPoolId, (UINT1 *) pParms);
        }
        else if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) &&
                 (CpsdtSock->i2SdtFamily == AF_INET6))
        {
            MemReleaseMemBlock (gUdp6MemPoolId, (UINT1 *) pParms);
        }
        CpsdtSock->i1ErrorCode = SLI_EMSGSIZE;
        SLI_ERR (SLI_EMSGSIZE);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    if ((CpsdtSock->i2SdtFamily == AF_INET) && (pPeerAddrIn != NULL) &&
        (pi4AddrLen != NULL))
    {
        *pi4AddrLen = sizeof (struct sockaddr_in);
    }

    if ((CpsdtSock->i2SdtFamily == AF_INET6) && (pPeerAddrIn6 != NULL) &&
        (pi4AddrLen != NULL))
    {
        *pi4AddrLen = sizeof (struct sockaddr_in6);
    }

    /* Copy data received to buffer passed by application */
    u4DataLen =
        CRU_BUF_Copy_FromBufChain (pBufChnHdr, pi1Buf, u4ReadOffSet, u4DataLen);

    /* Release the received Packet buffer */
    CRU_BUF_Release_MsgBufChain (pBufChnHdr, FALSE);

    /* Free the allocated mem block for pParams */
    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) &&
        (CpsdtSock->i2SdtFamily == AF_INET))
    {
        MemReleaseMemBlock (gUdp4MemPoolId, (UINT1 *) pParms);
    }
    else if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) &&
             (CpsdtSock->i2SdtFamily == AF_INET6))
    {
        MemReleaseMemBlock (gUdp6MemPoolId, (UINT1 *) pParms);
    }
    OsixSemGive (CpsdtSock->ProtectSemId);
    return (u4DataLen);
}

/*************************************************************************/
/*  Function Name   : SliSendto                                          */
/*  Description     : This function sends a datagram to the destination  */
/*                    specified by pPeerAddr.                            */
/*  Input(s)        : i4SockDesc - Socket Descriptor                     */
/*                    pi1Buf - Pointer to buffer which contains data     */
/*                    i4BufLen - Indicates length of buffer              */
/*                    u4Flags - Flags - Not supported as of now          */
/*  Output(s)       :                                                    */
/*                    pPeerAddr - Pointer which holds peer address       */
/*                    pi4AddrLen - Pointer which holds peer address len  */
/*  Returns         : Number of bytes received, on success               */
/*                    SLI_ENOTSOCK, on error                  */
/*                    SLI_EOPNOTSUPP, on error               */
/*************************************************************************/

INT4
SliSendto (INT4 i4SockDesc, CONST VOID *pi1Buf, INT4 i4BufLen,
           UINT4 u4Flags, struct sockaddr *pPeerAddr, INT4 i4AddrLen)
{
#ifdef IP_WANTED
    tSin               *pPeerAddrIn = NULL;
#endif
    tIpAddr             v6RemoteAddr;
    tIpAddr             v6LocalAddr;

    tCRU_BUF_CHAIN_HEADER *pBufChnHdr = NULL;
#ifdef IP6_WANTED
    tCRU_BUF_CHAIN_HEADER *pDupBufChnHdr = NULL;
    tIpAddr             IpTempAddr;
    tIpAddr             IpTempRemoteAddr;
    tSin6              *pPeerAddrIn6 = NULL;
#endif
    tSdtEntry          *CpsdtSock = NULL;
    UINT2               u2RemotePort = ZERO;
    UINT1               u1OptLen = ZERO;
    UINT1               u1LooseBytes = ZERO;
    UINT2               u2LoopCount = ZERO;
    UINT2               u2Index = ZERO;
    UINT4               u4WriteOffset = ZERO;
    tSliRawIpHdrInfo    sliRawIpHdrInfo;
    tIpParms           *pSliIpParms = NULL;
    UINT4               u4IfIndex = SLI_UDP_PORT;
    UINT1               u1BcastFlag = ZERO;
    INT4                i4PeerSockDesc = ZERO;
    UINT4               u4SrcIpAddr = ZERO;
    tSdtEntry          *cpPeerSdtSock = NULL;
    t_UDP_TO_APP_MSG_PARMS *pParms = NULL;
#ifdef IP6_WANTED
    UINT1               u1HopLmt = ZERO;
    tIpAddr             IpTempAddr1;
#endif
    tHlToIp6Params      Ip6SendParms;
#ifdef IP_WANTED
    tIfConfigRecord     IpInfo;
#endif
    UINT2               u2DF_BIT = IP_DF;

    V6_ADDR_INIT (&v6RemoteAddr);
    u2RemotePort = ZERO;
    u1OptLen = ZERO;
    u1LooseBytes = ZERO;
    u2LoopCount = ZERO;
    u4WriteOffset = ZERO;

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }
    u4IfIndex = CpsdtSock->u4IfIndex;

    /* Fix to avoid the crash when the buffer is null. 
     * The CpsdtSock is used to set the error code without allocating
     * memory for it */
    /* Check for invalid parameters and insufficient space */
    if ((pi1Buf == NULL) || (i4BufLen <= ZERO))
    {
        CpsdtSock->i1ErrorCode = SLI_EMSGSIZE;
        SLI_ERR (SLI_EMSGSIZE);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    /* Source address for the datagram */
    V6_ADDR_COPY (&v6LocalAddr, &CpsdtSock->LocalIpAddr);

    if (IS_OOB_SET (u4Flags) == TRUE)
    {
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if (CpsdtSock->i2SdtSockType == SOCK_STREAM)
    {
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if ((i4AddrLen == SLI_ZERO) && (pPeerAddr == NULL))
    {
        /* TO be verified once */
        if ((CpsdtSock->state == SS_UNCONNECTED) || (CpsdtSock->state ==
                                                     SS_TX_SHUTDOWN))
        {
            CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
            SLI_ERR (SLI_ENOTCONN);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }

        if (CpsdtSock->i2SdtFamily == AF_INET)
        {
            V4_MAPPED_ADDR_COPY (&v6RemoteAddr,
                                 V4_FROM_V6_ADDR (CpsdtSock->RemoteIpAddr));
        }
        else
        {
            /* Remote Address in Host order */
            V6_ADDR_COPY (&v6RemoteAddr, &CpsdtSock->RemoteIpAddr);
        }
        u2RemotePort = CpsdtSock->u2RemotePort;
    }
    else
    {
        if ((pPeerAddr != NULL) && ((i4AddrLen == sizeof (tSin6)) ||
                                    (i4AddrLen == sizeof (tSin))))
        {
            if ((pPeerAddr->sa_family != AF_INET) &&
                (pPeerAddr->sa_family != AF_INET6) &&
                (pPeerAddr->sa_family != AF_UNSPEC))
            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            if (pPeerAddr->sa_family == AF_INET6)
            {
                /* if the socket is bound, bind it */
#ifdef IP6_WANTED
                /* Check for DGRAM Socket - Only DGRAM Socket to create a UDP Session */
                if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) &&
                    (CpsdtSock->u1Padding != TRUE))
                {
                    /* When FdMode is GLOBAL, UDP6 control block will be 
                     * included in global IP6 structure 
                     * otherwise if it is UNIQUE mode, UDP6 control block will
                     * be included in IP6 context structure.*/
                    if (Udp6OpenInCxt (CpsdtSock->u4TxContextId,
                                       CpsdtSock->u1FdMode, i4SockDesc,
                                       &(CpsdtSock->u2LocalPort), NULL, NULL,
                                       ((tIp6Addr *) (V6_HTONL (&IpTempAddr,
                                                                &CpsdtSock->
                                                                LocalIpAddr))),
                                       &(CpsdtSock->u2RemotePort),
                                       ((tIp6Addr
                                         *) (V6_HTONL (&IpTempRemoteAddr,
                                                       &CpsdtSock->
                                                       RemoteIpAddr))),
                                       (VOID *) SliUdp6RcvInCxt) == FAILURE)
                    {
                        /* If this sock desc is common for all context
                         * means, reset ContextId in socket descriptor table*/
                        if (CpsdtSock->u1FdMode == SOCK_GLOBAL_MODE)
                        {
                            CpsdtSock->u4TxContextId = VCM_DEFAULT_CONTEXT;
                        }
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    /* Setting of Bound should happen only with in the DGRAM Socket Code */
                    CpsdtSock->u1Padding = TRUE;
                }
                pPeerAddrIn6 = (tSin6 *) (VOID *) pPeerAddr;
                /* Remote address is in Host Order */
                V6_NTOHL (&v6RemoteAddr,
                          (tIpAddr *) (VOID *) &pPeerAddrIn6->sin6_addr);
                u2RemotePort = OSIX_NTOHS (pPeerAddrIn6->sin6_port);
                if (IN6_IS_ADDR_LINKLOCAL
                    (*((tIpAddr *) (VOID *) &pPeerAddrIn6->sin6_addr)))
                {
                    u4IfIndex = pPeerAddrIn6->sin6_scope_id;
                }
#endif
            }
            else
            {
#ifdef IP_WANTED
                /* Check for DGRAM Socket - Only DGRAM Socket to create a UDP Session */
                if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) &&
                    (CpsdtSock->u1Padding != TRUE))
                {
                    /* When FdMode is GLOBAL, UDP control block will be 
                     * included in global UDP structure 
                     * otherwise if it is UNIQUE mode, UDP control block will
                     * be included in UDP context structure.*/
                    if (udp_sli_open_in_cxt (CpsdtSock->u1FdMode,
                                             CpsdtSock->u4TxContextId,
                                             i4SockDesc,
                                             &(CpsdtSock->u2LocalPort),
                                             V4_FROM_V6_ADDR (CpsdtSock->
                                                              LocalIpAddr),
                                             &(CpsdtSock->u2RemotePort),
                                             V4_FROM_V6_ADDR (CpsdtSock->
                                                              RemoteIpAddr),
                                             CpsdtSock->ConnSemId,
                                             CpsdtSock->UdpQId) == FAILURE)
                    {
                        /* If this sock desc is common for all context
                         * means, reset ContextId in socket descriptor table*/
                        if (CpsdtSock->u1FdMode == SOCK_GLOBAL_MODE)
                        {
                            CpsdtSock->u4TxContextId = VCM_DEFAULT_CONTEXT;
                        }
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    CpsdtSock->u1Padding = TRUE;
                }
                pPeerAddrIn = (tSin *) (VOID *) pPeerAddr;
                V4_MAPPED_ADDR_COPY (&v6RemoteAddr,
                                     OSIX_NTOHL (pPeerAddrIn->sin_addr.s_addr));
                u2RemotePort = OSIX_NTOHS (pPeerAddrIn->sin_port);
#endif
            }
        }
    }

    if (CpsdtSock->i2SdtSockType == SOCK_DGRAM)
    {
        if ((pBufChnHdr = CRU_BUF_Allocate_MsgBufChain (i4BufLen +
                                                        SLI_UDP_DATA_OFFSET,
                                                        SLI_UDP_DATA_OFFSET)) ==
            NULL)
        {
            CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
            SLI_ERR (SLI_EMEMFAIL);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
        MEMSET (CRU_BUF_Get_ModuleData (pBufChnHdr), SLI_ZERO,
                sizeof (tMODULE_DATA));

        if ((CRU_BUF_Copy_OverBufChain
             (pBufChnHdr, (UINT1 *) (FS_ULONG) (pi1Buf), ZERO,
              (UINT4) i4BufLen)) != CRU_SUCCESS)
        {
            SLI_Release_Buffer (pBufChnHdr, FALSE);
            CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
            SLI_ERR (SLI_EMEMFAIL);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }

        if (!(CpsdtSock->u1SockOpns & SO_BROADCAST))
        {
            if (CpsdtSock->i2SdtFamily != AF_INET6)
            {
#ifdef IP_WANTED
                if (IpifIsBroadCastLocalInCxt (CpsdtSock->u4TxContextId,
                                               V4_FROM_V6_ADDR (v6RemoteAddr))
                    == IP_SUCCESS)
                {
                    u1BcastFlag = SLI_ONE;
                }
#endif
            }
        }

        if (u1BcastFlag)
        {
            SLI_Release_Buffer (pBufChnHdr, FALSE);
            CpsdtSock->i1ErrorCode = SLI_EACCES;
            SLI_ERR (SLI_EACCES);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }

/* The IpIsLocalAddr function fills up the interface index if the 
 * destination address does not necessitate a route table lookup */
        if (CpsdtSock->u1SockOpns & SO_DONTROUTE)
        {
            if (IP_IS_ADDR_CLASS_D (V4_FROM_V6_ADDR (v6RemoteAddr)) == TRUE)
            {
                u4IfIndex = CpsdtSock->u4MCastIf;
            }
            else if (SliIsIpAddrLocalInCxt (CpsdtSock->u4TxContextId,
                                            v6RemoteAddr,
                                            &u4IfIndex) == IP_FAILURE)
            {
                SLI_Release_Buffer (pBufChnHdr, FALSE);
                CpsdtSock->i1ErrorCode = SLI_ENETUNREACH;
                SLI_ERR (SLI_ENETUNREACH);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
        }

        if (CpsdtSock->u4IpPktinfoFlag != IP_PKTINFO_DIS)
        {
            if (CpsdtSock->i2SdtFamily == AF_INET)
            {
                u4IfIndex = (UINT4) CpsdtSock->inPktinfo.ipi_ifindex;
                V4_MAPPED_ADDR_COPY (&v6LocalAddr,
                                     CpsdtSock->inPktinfo.ipi_spec_dst.s_addr);
            }
            else if (CpsdtSock->i2SdtFamily == AF_INET6)
            {
                u4IfIndex = (UINT4) CpsdtSock->in6Pktinfo.ipi6_ifindex;
                /* RemoteAddress  is in Host Order */
                V6_ADDR_COPY (&v6LocalAddr,
                              (tIpAddr *) (VOID *) &CpsdtSock->in6Pktinfo.
                              ipi6_addr);
            }
        }
        if (CpsdtSock->u4IpPktinfoFlag != IP_PKTINFO_DIS)
        {
            u4SrcIpAddr = V4_FROM_V6_ADDR (v6LocalAddr);
        }
        else
        {
            u4SrcIpAddr = V4_FROM_V6_ADDR (CpsdtSock->LocalIpAddr);
        }
        if (IP_IS_ADDR_CLASS_D (V4_FROM_V6_ADDR (v6RemoteAddr)) == TRUE)
        {
            u4IfIndex = CpsdtSock->u4MCastIf;
            if (CpsdtSock->u4MCastAddr != 0)
            {
                u4SrcIpAddr = CpsdtSock->u4MCastAddr;
            }
        }

        /* Loop back interface for IPv4 */
        if (V4_FROM_V6_ADDR (v6RemoteAddr) == INADDR_LOOPBACK)
        {
            i4PeerSockDesc =
                SliFindSockDescFromPortAddrInCxt (CpsdtSock->u4TxContextId,
                                                  u2RemotePort, v6RemoteAddr,
                                                  CpsdtSock->i2SdtFamily);
            if (i4PeerSockDesc == ERR)
            {
                SLI_Release_Buffer (pBufChnHdr, FALSE);
                CpsdtSock->i1ErrorCode = SLI_CONN_NOT_EXIST;
                SLI_ERR (SLI_CONN_NOT_EXIST);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            cpPeerSdtSock = SOCK_DESC_TABLE[i4PeerSockDesc];

            /* allocate memory for UDP message from UDP4 mem pool */
            pParms = (t_UDP_TO_APP_MSG_PARMS *) MemAllocMemBlk (gUdp4MemPoolId);
            if (pParms == NULL)
            {
                SLI_Release_Buffer (pBufChnHdr, FALSE);
                CpsdtSock->i1ErrorCode = SLI_CONN_NOT_EXIST;
                SLI_ERR (SLI_CONN_NOT_EXIST);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            MEMSET (pParms, 0, sizeof (t_UDP_TO_APP_MSG_PARMS));
            pParms->u4ContextId = CpsdtSock->u4TxContextId;
            pParms->pBuf = pBufChnHdr;
            pParms->u4SrcIpAddr = V4_FROM_V6_ADDR (v6RemoteAddr);
            pParms->u4DstIpAddr = V4_FROM_V6_ADDR (v6RemoteAddr);
            pParms->u2DstUdpPort = cpPeerSdtSock->u2LocalPort;
            pParms->u2SrcUdpPort = CpsdtSock->u2LocalPort;
            pParms->u2Len = (UINT2) i4BufLen;

            if (OsixQueSend (cpPeerSdtSock->UdpQId,
                             (UINT1 *) &pParms,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
            {
                SLI_Release_Buffer (pBufChnHdr, FALSE);
                MemReleaseMemBlock (gUdp4MemPoolId, (UINT1 *) pParms);
                CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                SLI_ERR (SLI_EMEMFAIL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            OsixSemGive (cpPeerSdtSock->ConnSemId);
            SliSelectScanList (i4PeerSockDesc, SELECT_READ);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return i4BufLen;
        }

        /* if interface is not set by appln.
         * udp6send will return failure for mutlicast and link local addr */
        if (CpsdtSock->i2SdtFamily == AF_INET6)
        {
#ifdef IP6_WANTED
            if (IS_ADDR_MULTI (*(V6_HTONL (&IpTempAddr, &v6RemoteAddr))))
            {
                u1HopLmt = (UINT1) CpsdtSock->i4multicasthlmt;
            }
            else if (Ip6AddrType
                     ((tIp6Addr *) (V6_HTONL (&IpTempAddr, &v6RemoteAddr))) ==
                     ADDR6_UNICAST)
            {
                u1HopLmt = (UINT1) CpsdtSock->i4unicasthlmt;
            }

            if ((CpsdtSock->i4multicastloop)
                && (IS_ADDR_MULTI (*(V6_HTONL (&IpTempAddr, &v6RemoteAddr)))))
            {
                pDupBufChnHdr = CRU_BUF_Duplicate_BufChain (pBufChnHdr);
                /* Convert the addresses from Host network order before giving
                   it to UDP6 */
                SliUdp6RcvInCxt (CpsdtSock->u4TxContextId,
                                 *(V6_HTONL (&IpTempAddr1, &v6LocalAddr)),
                                 *(V6_HTONL (&IpTempAddr, &v6RemoteAddr)),
                                 CpsdtSock->u2LocalPort, u2RemotePort,
                                 pDupBufChnHdr, u4IfIndex,
                                 (UINT2) i4BufLen, u1HopLmt);
            }
            if (Udp6SendInCxt (CpsdtSock->u4TxContextId,
                               CpsdtSock->u2LocalPort,
                               u2RemotePort, SLI_GET_BUF_LENGTH (pBufChnHdr),
                               (UINT2) u4IfIndex,
                               (tIp6Addr
                                *) (V6_HTONL (&IpTempAddr1, &v6LocalAddr)),
                               (tIp6Addr
                                *) (V6_HTONL (&IpTempAddr, &v6RemoteAddr)),
                               pBufChnHdr, u1HopLmt) == IP6_FAILURE)
            {
                if (CpsdtSock->u1FdMode == SOCK_GLOBAL_MODE)
                {
                    CpsdtSock->u4TxContextId = VCM_DEFAULT_CONTEXT;
                }
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
#endif
        }
        else
        {
#ifdef IP_WANTED
            if (udp_send_srcaddr_in_cxt (CpsdtSock->u4TxContextId,
                                         u4SrcIpAddr,
                                         CpsdtSock->u2LocalPort,
                                         V4_FROM_V6_ADDR (v6RemoteAddr),
                                         u2RemotePort,
                                         pBufChnHdr,
                                         (INT2) SLI_GET_BUF_LENGTH (pBufChnHdr),
                                         (UINT1) CpsdtSock->i4IpTos,
                                         (UINT1) CpsdtSock->i4IpTtl, SLI_UDP_ID,
                                         (UINT1) CpsdtSock->i4PmtuDfFlag,
                                         SLI_UDP_OLEN, (UINT2) u4IfIndex,
                                         TRUE) == FAILURE)

            {
                /* If this sock desc is common for all context
                 * means, reset ContextId in socket descriptor table*/
                if (CpsdtSock->u1FdMode == SOCK_GLOBAL_MODE)
                {
                    CpsdtSock->u4TxContextId = VCM_DEFAULT_CONTEXT;
                }
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
#endif
        }
    }
    else if (CpsdtSock->i2SdtSockType == SOCK_RAW)
    {
        if (CpsdtSock->i2SdtFamily == AF_INET)
        {
            if (CpsdtSock->IpOption.pu1Option != NULL)
            {
                u1OptLen = CpsdtSock->IpOption.u1OptLen;
                u1LooseBytes = u1OptLen & (DOUBLE_WORD_LEN - SLI_ONE);
                if (u1LooseBytes != ZERO)
                {
                    u1OptLen =
                        (UINT1) (u1OptLen - u1LooseBytes + DOUBLE_WORD_LEN);
                }
            }
            if (CpsdtSock->i4IpHdrIncl == FALSE)
            {
                pBufChnHdr =
                    SLI_Alloc_Buffer (i4BufLen + IP_HDR_LEN + u1OptLen +
                                      MAX_MAC_HDR_LEN, MAX_MAC_HDR_LEN);
                if (pBufChnHdr == NULL)
                {
                    CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                    SLI_ERR (SLI_EMEMFAIL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }

                /* Instead of calling the SLI_PUT_X_BYTE for every part of the 
                 * IP header, which will result in function calls for every such
                 * macro call, we will write to this structure and call 
                 * copy_buf_chain once with this structure */

                sliRawIpHdrInfo.u1VHlen =
                    IP_VERS_AND_HLEN (IP_VERSION_4, u1OptLen);
                sliRawIpHdrInfo.u1TOS = (UINT1) CpsdtSock->i4IpTos;
                sliRawIpHdrInfo.u2Totlen =
                    OSIX_HTONS ((UINT2) (i4BufLen + IP_HDR_LEN + u1OptLen));
                sliRawIpHdrInfo.u2Id = ZERO;
                sliRawIpHdrInfo.u2FlOffs = ZERO;
                sliRawIpHdrInfo.u1TTL = (UINT1) CpsdtSock->i4IpTtl;
                sliRawIpHdrInfo.u1Proto = (UINT1) CpsdtSock->i4Protocol;
                sliRawIpHdrInfo.u2Cksum = ZERO;
                if (IPPMTUDISC_DO == CpsdtSock->i4PmtuDfFlag)
                {
                    sliRawIpHdrInfo.u2FlOffs = OSIX_HTONS (u2DF_BIT);
                }
                else
                {
                    sliRawIpHdrInfo.u2FlOffs = ZERO;
                }

                if (IP_IS_ADDR_CLASS_D (V4_FROM_V6_ADDR (v6RemoteAddr)) == TRUE)
                {
                    u4IfIndex = CpsdtSock->u4MCastIf;
#ifdef IP_WANTED
                    if (IpGetIfConfigRecord ((UINT2) u4IfIndex, &IpInfo)
                        != IP_SUCCESS)
                    {
                        SLI_Release_Buffer (pBufChnHdr, FALSE);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }

                    sliRawIpHdrInfo.u4Src = OSIX_HTONL (IpInfo.u4Addr);
#else
                    sliRawIpHdrInfo.u4Src = OSIX_HTONL (V4_FROM_V6_ADDR
                                                        (CpsdtSock->
                                                         LocalIpAddr));
#endif
                }
                else
                {
                    sliRawIpHdrInfo.u4Src = OSIX_HTONL (V4_FROM_V6_ADDR
                                                        (CpsdtSock->
                                                         LocalIpAddr));
                }
                sliRawIpHdrInfo.u4Dst = OSIX_HTONL (V4_FROM_V6_ADDR
                                                    (v6RemoteAddr));
                if ((SLI_BUF_Copy_OverBufChain (pBufChnHdr,
                                                (UINT1 *) &sliRawIpHdrInfo,
                                                ZERO,
                                                IP_HDR_LEN)) != OSIX_SUCCESS)
                {
                    SLI_Release_Buffer (pBufChnHdr, FALSE);
                    CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                    SLI_ERR (SLI_EMEMFAIL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }

                u4WriteOffset = IP_HDR_LEN;

                if (CpsdtSock->IpOption.pu1Option != NULL)
                {
                    if ((SLI_BUF_Copy_OverBufChain (pBufChnHdr,
                                                    CpsdtSock->IpOption.
                                                    pu1Option, u4WriteOffset,
                                                    CpsdtSock->IpOption.
                                                    u1OptLen)) != OSIX_SUCCESS)
                    {
                        SLI_Release_Buffer (pBufChnHdr, FALSE);
                        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                        SLI_ERR (SLI_EMEMFAIL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    u4WriteOffset += CpsdtSock->IpOption.u1OptLen;
                    if (u1LooseBytes != ZERO)
                    {
                        u2LoopCount = (UINT2) (DOUBLE_WORD_LEN - u1LooseBytes);
                        for (u2Index = ZERO_COUNT; u2Index < u2LoopCount;
                             u2Index++)
                        {
                            SLI_PUT_1_BYTE (pBufChnHdr, ZERO, u4WriteOffset++);
                        }
                        u4WriteOffset = (UINT4) (u4WriteOffset + u1OptLen);
                    }
                }
            }
            else
            {
                pBufChnHdr = SLI_Alloc_Buffer (i4BufLen + MAX_MAC_HDR_LEN,
                                               MAX_MAC_HDR_LEN);
                if (pBufChnHdr == NULL)
                {
                    CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                    SLI_ERR (SLI_EMEMFAIL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
            }
            if ((SLI_BUF_Copy_OverBufChain
                 (pBufChnHdr, (UINT1 *) (FS_ULONG) (pi1Buf), u4WriteOffset,
                  i4BufLen)) != OSIX_SUCCESS)
            {
                SLI_Release_Buffer (pBufChnHdr, FALSE);
                CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                SLI_ERR (SLI_EMEMFAIL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            pSliIpParms = (tIpParms *) CRU_BUF_Get_ModuleData (pBufChnHdr);
            pSliIpParms->u4ContextId = CpsdtSock->u4TxContextId;
            pSliIpParms->u1Cmd = IP_RAW_DATA;
            if (IP_IS_ADDR_CLASS_D (V4_FROM_V6_ADDR (v6RemoteAddr)) == TRUE)
            {
                pSliIpParms->u2Port = (UINT2) CpsdtSock->u4MCastIf;
            }

#ifdef IP_WANTED

            if (IpEnquePktToIpFromHLWithCxtId (pBufChnHdr) == IP_FAILURE)
            {
                CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                SLI_ERR (SLI_EMEMFAIL);
                /* If this sock desc is common for all context
                 * means, reset ContextId in socket descriptor table*/
                if (CpsdtSock->u1FdMode == SOCK_GLOBAL_MODE)
                {
                    CpsdtSock->u4TxContextId = VCM_DEFAULT_CONTEXT;
                }
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
#endif
        }
        else
        {
            pBufChnHdr = SLI_Alloc_Buffer (i4BufLen + MAX_MAC_HDR_LEN,
                                           MAX_MAC_HDR_LEN);
            if (pBufChnHdr == NULL)
            {
                CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                SLI_ERR (SLI_EMEMFAIL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            if ((SLI_BUF_Copy_OverBufChain
                 (pBufChnHdr, (UINT1 *) (FS_ULONG) (pi1Buf), u4WriteOffset,
                  i4BufLen)) != OSIX_SUCCESS)
            {
                SLI_Release_Buffer (pBufChnHdr, FALSE);
                CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                SLI_ERR (SLI_EMEMFAIL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            MEMSET (&Ip6SendParms, 0, sizeof (Ip6SendParms));
#ifdef IP6_WANTED

            Ip6SendParms.u1Cmd = IP6_RAW_DATA;
            if ((pPeerAddrIn6 != NULL) &&
                (IN6_IS_ADDR_MUTLICAST
                 (*((tIpAddr *) (VOID *) &pPeerAddrIn6->sin6_addr))))
            {
                Ip6SendParms.u1Hlim = (UINT1) CpsdtSock->i4multicasthlmt;
            }
            else
            {
                Ip6SendParms.u1Hlim = (UINT1) CpsdtSock->i4unicasthlmt;
            }
#endif
            V6_ADDR_COPY (&v6LocalAddr,
                          (tIpAddr *) (VOID *) &CpsdtSock->in6Pktinfo.
                          ipi6_addr);
            Ip6SendParms.u1Proto = (UINT1) (CpsdtSock->i4Protocol);
            Ip6SendParms.u4Index = (UINT4) (CpsdtSock->in6Pktinfo.ipi6_ifindex);
            Ip6SendParms.u4Len = (UINT4) i4BufLen;
            Ip6SendParms.u4ContextId = CpsdtSock->u4TxContextId;

            /* The HdrIncl option is supported for IP6 in two ways.
             * 1. If HdrIncl flag is TRUE, Hdr is passed from HL,
             *    IP6 extracts the header in IP6TaskMain and copies the
             *    contents in a tHlToIp6Params structure and moves the Buffer
             *    to an offset after the IP6HeaderLen, and in Ip6Send they
             *    reconstruct the header & prepend it to the buffer.
             * 2. If HdrIncl flag is FALSE, Hdr is not passed from HL,
             *    SLI passes the header in Ip6SendParms, IP6 extracts the
             *    Ip6SendParms values passed to Ip6RcvFromHl where the
             *    value is copied inside Ip6RcvFromHl and passed to IP6 Q.
             *    So no need to construct the IP6 Hdr inside SLI.
             */
            if (CpsdtSock->i4IpHdrIncl != FALSE)
            {
                Ip6SendParms.u1Reserved = IP6_HDR_INC;
            }
            V6_HTONL ((tIpAddr *) & (Ip6SendParms.Ip6SrcAddr), &v6LocalAddr);
            V6_HTONL ((tIpAddr *) & (Ip6SendParms.Ip6DstAddr), &v6RemoteAddr);
#ifdef IP6_WANTED
            if (Ip6RcvFromHl (pBufChnHdr, &Ip6SendParms) == FAILURE)
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
#endif
        }
    }

    /* If this sock desc is common for all context
     * means, reset ContextId in socket descriptor table*/
    if (CpsdtSock->u1FdMode == SOCK_GLOBAL_MODE)
    {
        CpsdtSock->u4TxContextId = VCM_DEFAULT_CONTEXT;
    }
    OsixSemGive (CpsdtSock->ProtectSemId);
    return i4BufLen;
}

/**************************************************************************/
/*  Function Name   : SliClose                                            */
/*  Description     : This function disconnects the caller from           */
/*                    the connection established before and destroys the  */
/*                                  corresponding socket.                 */
/*  Input(s)        : i4SockDesc - The socket identifier.                 */
/*  Output(s)       : None.                                               */
/*  Returns         : SUCCESS - If Sucessful                              */
/*                    SLI_ENOTSOCK  : If the socket desc is    */
/*                                               invalid.                 */
/*                    SLI_ENOTCONN : The socket is not in   */
/*                                               connected state.         */
/*                    SLI_EINTR : The close request to */
/*                                              TCP failed.               */
/**************************************************************************/

INT4
SliClose (INT4 i4SockDesc)
{
    tSdtEntry          *CpsdtSock = NULL;
    t_UDP_TO_APP_MSG_PARMS *pParms = NULL;
#ifdef IP6_WANTED
    tIpAddr             IpTempAddr;
#endif
#ifdef TCP_WANTED
    tTcpToApplicationParms *PtaPRetParms = NULL;
    INT4                i4bReplyRcvd = FALSE;
    INT4                i4AsyncSockDesc = 0;
#endif

    tsliTcpAoMktListNode *pAoMkt;
    tsliTcpAoMktListNode *pTmpAoMkt;

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];

    if (CpsdtSock->ProtectSemId == NULL)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if (CpsdtSock->i2SdtSockType == SOCK_STREAM)
    {
#ifdef TCP_WANTED
        if (CpsdtSock->state == SS_UNCONNECTED)
        {
            OsixSemGive (CpsdtSock->ProtectSemId);
            SliDestroySocket (i4SockDesc);
            return SLI_SUCCESS;
        }

        CpsdtSock->state = SS_DISCONNECTING;

        while (i4bReplyRcvd == FALSE)
        {
            PtaPRetParms = SliTcpIssueCloseRequest (i4SockDesc);
            if (PtaPRetParms == NULL)
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                SliDestroySocket (i4SockDesc);
                return SLI_FAILURE;
            }
            if ((CpsdtSock->u4ValidOptions & TCPO_NODELAY) ||
                (PtaPRetParms->u1Cmd == CLOSE_CMD) ||
                ((PtaPRetParms->u1Cmd == ERROR_MSG) &&
                 (PtaPRetParms->cmdtype.error.u1ErrorCmd == CLOSE_CMD)))
            {
                i4bReplyRcvd = TRUE;
            }
            if (PtaPRetParms->u1Cmd == CONN_REPORT)
            {
                /* Asynchronous Message arrived. Handle it. */
                i4AsyncSockDesc = SliTcpFindSockDesc (PtaPRetParms->u4ConnId);
                if (CpsdtSock->fpSliTcpHandleAsyncMesg != NULL)
                {
                    CpsdtSock->fpSliTcpHandleAsyncMesg (i4AsyncSockDesc,
                                                        PtaPRetParms);
                }
            }
        }
        if ((PtaPRetParms->u1Cmd == ERROR_MSG))
        {
            FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
            OsixSemGive (CpsdtSock->ProtectSemId);
            SliDestroySocket (i4SockDesc);
            SLI_ERR (SLI_EINTR);
            return SLI_FAILURE;
        }
        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
#endif

/*Ao deletion*/
        TMO_DYN_SLL_Scan (&(CpsdtSock->TcpAoList), pAoMkt, pTmpAoMkt,
                          tsliTcpAoMktListNode *)
        {

            TMO_SLL_Delete (&(CpsdtSock->TcpAoList), &(pAoMkt->TSNext));
            MemReleaseMemBlock (gSliTcpAoNodePoolId, (UINT1 *) pAoMkt);
        }

    }                            /*SOCK_STREAM */
    else if (CpsdtSock->i2SdtSockType == SOCK_DGRAM)
    {
        if (CpsdtSock->i2SdtFamily == AF_INET6)
        {
#ifdef IP6_WANTED
            if (Udp6CloseInCxt (CpsdtSock->u4TxContextId,
                                CpsdtSock->u2LocalPort,
                                ((tIp6Addr *) (V6_HTONL (&IpTempAddr,
                                                         (tIpAddr *) &
                                                         CpsdtSock->
                                                         LocalIpAddr)))) ==
                IP6_FAILURE)
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                SliDestroySocket (i4SockDesc);
                return SLI_FAILURE;
            }
#endif
        }
        else
        {
#ifdef IP_WANTED
            if (udp_close_srcaddr_in_cxt
                (CpsdtSock->u4TxContextId,
                 CpsdtSock->u2LocalPort,
                 V4_FROM_V6_ADDR (CpsdtSock->LocalIpAddr)) == FAILURE)
            {
                OsixSemGive (CpsdtSock->ProtectSemId);
                SliDestroySocket (i4SockDesc);
                SLI_ERR (SLI_EINTR);
                return SLI_FAILURE;
            }
#endif
        }

        while (OsixQueRecv (CpsdtSock->UdpQId,
                            (UINT1 *) &pParms,
                            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            SLI_Release_Buffer (pParms->pBuf, FALSE);
            /* Free the allocated mem block for pParams */
            if (CpsdtSock->i2SdtFamily == AF_INET)
            {
                MemReleaseMemBlock (gUdp4MemPoolId, (UINT1 *) pParms);
            }
            else if (CpsdtSock->i2SdtFamily == AF_INET6)
            {
                MemReleaseMemBlock (gUdp6MemPoolId, (UINT1 *) pParms);
            }
        }
    }                            /*SOCK_DGRAM */
    else if (CpsdtSock->i2SdtSockType == SOCK_RAW)
    {
        OsixSemGive (CpsdtSock->ProtectSemId);
        if (SliRawUnHash (i4SockDesc) != SLI_SUCCESS)
        {
            SliDestroySocket (i4SockDesc);
            return SLI_FAILURE;
        }
        if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
        {
            SliDestroySocket (i4SockDesc);
            SLI_ERR (SLI_EACCES);
            return SLI_FAILURE;
        }
    }

    OsixSemGive (CpsdtSock->ProtectSemId);
    SliDestroySocket (i4SockDesc);
    return SLI_SUCCESS;
}

#ifdef TCP_WANTED
/**************************************************************************/
/*  Function Name   : SliShutdown                                         */
/*  Description     : This function shuts down a portion or all           */
/*                          of a connection.                              */
/*  Input(s)        :                                                     */
/*                    i4SockDesc - The socket identifier.                 */
/*                    i4How - specifies what to shut down.                */
/*  Output(s)       : None.                                               */
/*  Returns         : SUCCESS  -If Sucessful                              */
/*                    SLI_ENOTSOCK - If the socket desc is     */
/*                                          invalid.                      */
/**************************************************************************/

INT4
SliShutdown (INT4 i4SockDesc, INT4 i4How)
{
    INT1                i1ZeroVal = ZERO;
    tSdtEntry          *CpsdtSock = NULL;

/* should be used while setting the options(SENDBUF & RCVBUF) at TCP Level */
    UNUSED_PARAM (i1ZeroVal);

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) ||
        (CpsdtSock->i2SdtSockType == SOCK_RAW))
    {
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        return SLI_FAILURE;
    }

    /*
     * If a portion is already shutdown and the other portion's   
     * shutdown id requested, or if a total shutdown is requested 
     * then close this operation is shutdown() becomes         
     * equivalent to close()                 
     */

    if (((CpsdtSock->state == SS_TX_SHUTDOWN) && (i4How == SHUTDOWN_RX)) ||
        ((CpsdtSock->state == SS_RX_SHUTDOWN) && (i4How == SHUTDOWN_TX)) ||
        (i4How == SHUTDOWN_ALL))
    {
        SliClose (i4SockDesc);
        return SLI_SUCCESS;
    }

    /*
     * Call setsockopt() to deallocate the transmission/receivei
     * buffer of TCP depending on the value of "i4How" .                  */

    switch (i4How)
    {
        case SHUTDOWN_RX:
        {
            CpsdtSock->state = SS_RX_SHUTDOWN;
            break;
        }

        case SHUTDOWN_TX:
        {
            CpsdtSock->state = SS_TX_SHUTDOWN;
            break;
        }
        default:
            CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
            SLI_ERR (SLI_EOPNOTSUPP);
            return SLI_FAILURE;
    }
    return SLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : SliGetFtSrvSockFdTCBTableIndex                      */
/*  Description     : This function returns the TCB table index           */
/*  Input(s)        : None.                                               */
/*  Output(s)       : None.                                               */
/*  Returns         : i4TCBIndex                                          */
/**************************************************************************/
INT4
SliGetFtSrvSockFdTCBTableIndex ()
{
    INT4                i4TCBIndex = -1;
    INT4                i4SrvSockFd = -1;
#ifdef L2RED_WANTED
    i4SrvSockFd = RmGetFtSrvSockFd ();
#endif

    if (i4SrvSockFd != -1)
    {
        if (SOCK_DESC_TABLE[i4SrvSockFd] == NULL)
        {
            return i4TCBIndex;
        }
        if (((tSdtEntry *) SOCK_DESC_TABLE[i4SrvSockFd])->i2SdtSockType ==
            SOCK_STREAM)
        {
            i4TCBIndex =
                (UINT4) ((tSdtEntry *) SOCK_DESC_TABLE[i4SrvSockFd])->u4ConnId;
        }
    }
    return i4TCBIndex;
}

/**************************************************************************/
/*  Function Name   : SliGetTcpSrvSockFdTCBTableIndex                     */
/*  Description     : This function returns the TCB table index           */
/*  Input(s)        : None.                                               */
/*  Output(s)       : None.                                               */
/*  Returns         : i4TCBIndex                                          */
/**************************************************************************/
INT4
SliGetTcpSrvSockFdTCBTableIndex ()
{
    INT4                i4TCBIndex = -1;
    INT4                i4SrvSockFd = -1;
#ifdef L2RED_WANTED
    i4SrvSockFd = RmGetTcpSrvSockFd ();
#endif

    if (i4SrvSockFd != -1)
    {
        if (SOCK_DESC_TABLE[i4SrvSockFd] == NULL)
        {
            return i4TCBIndex;
        }

        if (((tSdtEntry *) SOCK_DESC_TABLE[i4SrvSockFd])->i2SdtSockType ==
            SOCK_STREAM)
        {
            i4TCBIndex =
                (UINT4) ((tSdtEntry *) SOCK_DESC_TABLE[i4SrvSockFd])->u4ConnId;
        }
    }
    return i4TCBIndex;
}
#endif

/**************************************************************************/
/*  Function Name   : SliGetsockname                                      */
/*  Description     : This function gets the local address of the         */
/*                    given socket.                                       */
/*  Input(s)        : i4SockDesc - The socket identifier.                 */
/*  Output(s)       :                                                     */
/*                    cpsa_myaddr - The local address of the given socket */
/*                    pi4Addrlen - The address length.                    */
/*  Returns         : SUCCESS - If Sucessful                              */
/*                    SLI_ENOTSOCK : If the socket desc is     */
/*                                          invalid.                      */
/*                    SLI_ENOTCONN: The socket is not in a  */
/*                                            connected state.            */
/**************************************************************************/

INT4
SliGetsockname (INT4 i4SockDesc, struct sockaddr *cpsa_myaddr, INT4 *pi4Addrlen)
{
    tSin6              *Cpsin6Tmp = NULL;
    tSin               *CpsinTmp = NULL;
    tSdtEntry          *CpsdtSock = NULL;

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }
    if (CpsdtSock->i2SdtFamily == AF_INET6)
    {
        Cpsin6Tmp = (tSin6 *) (VOID *) cpsa_myaddr;
        Cpsin6Tmp->sin6_family = CpsdtSock->i2SdtFamily;
        Cpsin6Tmp->sin6_port = OSIX_HTONS (CpsdtSock->u2LocalPort);
    }
    else
    {
        CpsinTmp = (tSin *) (VOID *) cpsa_myaddr;
        CpsinTmp->sin_family = CpsdtSock->i2SdtFamily;
        CpsinTmp->sin_port = OSIX_HTONS (CpsdtSock->u2LocalPort);
    }

    if (CpsdtSock->i2SdtFamily == AF_INET6)
    {
        if ((IS_ADDR_IN6ADDRANYINIT (CpsdtSock->LocalIpAddr))
            && (!(IS_ADDR_IN6ADDRANYINIT (CpsdtSock->RecvAddr))))
        {
            V6_HTONL ((tIpAddr *) (VOID *) &Cpsin6Tmp->sin6_addr,
                      &CpsdtSock->RecvAddr);
        }
        else
        {
            V6_HTONL ((tIpAddr *) (VOID *) &Cpsin6Tmp->sin6_addr,
                      &CpsdtSock->LocalIpAddr);
        }

        *pi4Addrlen = sizeof (tSin6);
    }
    else
    {
        if ((V4_FROM_V6_ADDR (CpsdtSock->LocalIpAddr) == IP_ANY_ADDR)
            && (V4_FROM_V6_ADDR (CpsdtSock->RecvAddr) != ZERO))
        {
            CpsinTmp->sin_addr.s_addr =
                OSIX_HTONL (V4_FROM_V6_ADDR (CpsdtSock->RecvAddr));
        }
        else
        {
            CpsinTmp->sin_addr.s_addr =
                OSIX_HTONL (V4_FROM_V6_ADDR (CpsdtSock->LocalIpAddr));
        }

        *pi4Addrlen = sizeof (tSin);
    }
    OsixSemGive (CpsdtSock->ProtectSemId);
    return SLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : SliGetpeername                                      */
/*  Description     : This function gets the peer address of a given      */
/*                    socket.                                             */
/*  Input(s)        : i4SockDesc - The socket identifier.                 */
/*  Output(s)       :                                                     */
/*                    cpsa_peeraddr - The peer address of the specified   */
/*                                    socket.                             */
/*                    pi4Addrlen - The address length.                    */
/*  Returns         : SUCCESS - If Sucessful                              */
/*                    SLI_ENOTSOCK - If the socket desc is     */
/*                                              invalid.                  */
/*                    SLI_ENOTCONN - The socket is not in   */
/*                                                 a connected state.     */
/**************************************************************************/

INT4
SliGetpeername (INT4 i4SockDesc, struct sockaddr *cpsa_peeraddr,
                INT4 *pi4Addrlen)
{
    tSin6              *Cpsin6Tmp = NULL;
    tSin               *CpsinTmp = NULL;
    tSdtEntry          *CpsdtSock = NULL;

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if (CpsdtSock->state != SS_CONNECTED)
    {
        CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
        SLI_ERR (SLI_ENOTCONN);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }
    if (CpsdtSock->i2SdtFamily == AF_INET6)
    {
        Cpsin6Tmp = (tSin6 *) (VOID *) cpsa_peeraddr;
        Cpsin6Tmp->sin6_family = CpsdtSock->i2SdtFamily;
        Cpsin6Tmp->sin6_port = OSIX_HTONS (CpsdtSock->u2RemotePort);
        V6_HTONL ((tIpAddr *) (VOID *) &Cpsin6Tmp->sin6_addr,
                  &CpsdtSock->RemoteIpAddr);
        *pi4Addrlen = sizeof (tSin6);
    }
    else
    {
        CpsinTmp = (tSin *) (VOID *) cpsa_peeraddr;
        CpsinTmp->sin_family = CpsdtSock->i2SdtFamily;
        CpsinTmp->sin_port = OSIX_HTONS (CpsdtSock->u2RemotePort);
        CpsinTmp->sin_addr.s_addr =
            OSIX_HTONL (V4_FROM_V6_ADDR (CpsdtSock->RemoteIpAddr));
        *pi4Addrlen = sizeof (tSin);
    }
    OsixSemGive (CpsdtSock->ProtectSemId);
    return (SLI_SUCCESS);
}

/**************************************************************************/
/*  Function Name  : SliGetsockopt                                        */
/*  Description    : This function gets the value of the specified option */
/*  Input(s)       :                                                      */
/*                   i4SockDesc - The socket identifier.                  */
/*                   i4Level - The level to operate. eg TCP               */
/*                   i4Optname - The option to get.                       */
/*  Output(s)      :                                                      */
/*                   Cpi1Optval - The value of the option got.            */
/*                   pi4Optlen - The length of the option got.            */
/*  Returns        : SUCCESS - If Sucessful                               */
/*                   SLI_ENOTSOCK - If the socket desc is      */
/*                                             invalid.                   */
/*                   SLI_ENOTCONN - The socket is not in a  */
/*                                            connected state.            */
/*                   SLI_ENOPROTOOPT:The option request to     */
/*                                           TCPfailed.                   */
/**************************************************************************/

INT4
SliGetsockopt (INT4 i4SockDesc, INT4 i4Level, INT4 i4Optname, VOID *Cpi1Optval,
               INT4 *pi4Optlen)
{
#ifdef TCP_WANTED
    INT4                i4bReplyRcvd = FALSE;
    INT4                i4AsyncSockDesc = 0;
    tTcpToApplicationParms *PtaPRetParms = NULL;
#endif
    tSdtEntry          *CpsdtSock = NULL;
#ifdef IP_WANTED
    tIfConfigRecord     IpInfo;
#endif

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }
    /*
     * The SO_TYPE option doesn't need the request to be    
     * sent. We could get it from the sock descriptor table 
     */
    if ((Cpi1Optval == NULL) || (pi4Optlen == NULL))
    {
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    if (i4Optname == SO_TYPE)
    {
        *(INT1 *) Cpi1Optval = (INT1) CpsdtSock->i2SdtSockType;
        *pi4Optlen = sizeof (INT1);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_SUCCESS;
    }

    if (i4Optname == SO_ERROR)
    {
        *(INT1 *) Cpi1Optval = CpsdtSock->i1ErrorCode;
        *pi4Optlen = sizeof (INT1);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_SUCCESS;
    }

    if ((i4Level == IPPROTO_IPV6) && (CpsdtSock->i2SdtFamily == AF_INET6))
    {
        switch (i4Optname)
        {
            case IPV6_MULTICAST_IF:
                *(UINT4 *) Cpi1Optval = (UINT4) CpsdtSock->u4IfIndex;
                *pi4Optlen = sizeof (UINT4);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;

            case IPV6_MULTICAST_HOPS:
                *(INT4 *) Cpi1Optval = CpsdtSock->i4multicasthlmt;
                *pi4Optlen = sizeof (INT4);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;

            case IPV6_MULTICAST_LOOP:
                *(INT4 *) Cpi1Optval = CpsdtSock->i4multicastloop;
                *pi4Optlen = sizeof (INT4);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;

            case IPV6_UNICAST_HOPS:
                *(INT4 *) Cpi1Optval = CpsdtSock->i4unicasthlmt;
                *pi4Optlen = sizeof (INT4);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;

            case IPV6_RECVHOPLIMIT:
                *(INT4 *) Cpi1Optval = CpsdtSock->i4recvhoplimit;
                *pi4Optlen = sizeof (INT4);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;
        }
    }

    if (CpsdtSock->i2SdtSockType == SOCK_STREAM)
    {
#ifdef TCP_WANTED
        if (CpsdtSock->state != SS_CONNECTED)
        {
            if ((i4Optname == SO_REUSEADDR) && (i4Level == SOL_SOCKET))
            {
                *(UINT4 *) Cpi1Optval = CpsdtSock->u4ValidOptions &
                    SO_REUSEADDR;
                *pi4Optlen = sizeof (UINT4);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;
            }
            CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
            SLI_ERR (SLI_ENOTCONN);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }

        while (i4bReplyRcvd == FALSE)
        {
            if (PtaPRetParms != NULL)
            {
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
            }
            PtaPRetParms = SliTcpIssueOptionRequest (i4SockDesc, GET,
                                                     (UINT2) i4Optname,
                                                     IGNORE_OPTLEN,
                                                     IGNORE_OPTVAL);

            if (PtaPRetParms == NULL)
            {
                if (CpsdtSock->u4ValidOptions & TCPO_NODELAY)
                {
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
                else
                    continue;
            }

            if ((CpsdtSock->u4ValidOptions & TCPO_NODELAY) ||
                (PtaPRetParms->u1Cmd == OPTIONS_CMD) ||
                ((PtaPRetParms->u1Cmd == ERROR_MSG) &&
                 (PtaPRetParms->cmdtype.error.u1ErrorCmd == OPTIONS_CMD)))
            {
                i4bReplyRcvd = TRUE;
            }

            if (PtaPRetParms->u1Cmd == CONN_REPORT)
            {
                /* Asynchronous Message arrived. Handle it. */
                i4AsyncSockDesc = SliTcpFindSockDesc (PtaPRetParms->u4ConnId);
                if (CpsdtSock->fpSliTcpHandleAsyncMesg != NULL)
                    CpsdtSock->fpSliTcpHandleAsyncMesg (i4AsyncSockDesc,
                                                        PtaPRetParms);
            }
        }

        if (PtaPRetParms->u1Cmd == ERROR_MSG)
        {
            FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
            CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
            SLI_ERR (SLI_ENOPROTOOPT);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
        else
        {
            /*
             * If a GET on IP_OPTIONS was requested, then the reply       
             * would contain an address in u4Value, not an actual value 
             */
            if (i4Optname == IP_OPTIONS)
            {
                MEMCPY ((INT1 *) Cpi1Optval,
                        (INT1 *) (FS_ULONG) PtaPRetParms->cmdtype.option.
                        u4Value, PtaPRetParms->cmdtype.option.u1Optlen);
                *pi4Optlen = PtaPRetParms->cmdtype.option.u1Optlen;
            }
            else
            {
                if (i4Optname == SO_LINGER)
                {

                    ((tSliLINGERPARMS *) Cpi1Optval)->u2LingerOnOff =
                        ((tSliLINGERPARMS
                          *) (FS_ULONG) (PtaPRetParms->cmdtype.option.
                                         u4Value))->u2LingerOnOff;
                    ((tSliLINGERPARMS *) Cpi1Optval)->u2LingerTimer =
                        ((tSliLINGERPARMS *) (FS_ULONG) (PtaPRetParms->cmdtype.
                                                         option.u4Value))->
                        u2LingerTimer;
                    *pi4Optlen = PtaPRetParms->cmdtype.option.u1Optlen;
                }
                else if (i4Optname == TCP_MD5SIG)
                {
                    ((struct tcp_md5sig *) Cpi1Optval)->tcpm_keylen =
                        PtaPRetParms->cmdtype.option.u1Optlen;
                    MEMCPY (((struct tcp_md5sig *) Cpi1Optval)->tcpm_key,
                            (INT1 *) (FS_ULONG) PtaPRetParms->cmdtype.option.
                            u4Value, PtaPRetParms->cmdtype.option.u1Optlen);
                    *pi4Optlen = sizeof (struct tcp_md5sig);
                }
                else if (i4Optname == TCP_AO_SIG)
                {
                    tTcpAOAuthMktTcb   *pMkt =
                        (tTcpAOAuthMktTcb *) (FS_ULONG) PtaPRetParms->cmdtype.
                        option.u4Value;
                    if (pMkt != NULL)
                    {
                        ((tTcpAoMktAddr *) Cpi1Optval)->u1SndKeyId =
                            pMkt->u1SendKeyId;
                        ((tTcpAoMktAddr *) Cpi1Optval)->u1RcvKeyId =
                            pMkt->u1ReceiveKeyId;
                        ((tTcpAoMktAddr *) Cpi1Optval)->u1Algo =
                            pMkt->u1MACAlgo;
                        ((tTcpAoMktAddr *) Cpi1Optval)->u1KeyLen =
                            pMkt->u1TcpAOPasswdLength;
                        ((tTcpAoMktAddr *) Cpi1Optval)->u1TcpOptIgn =
                            pMkt->u1TcpOptionIgnore;
                        MEMCPY (((tTcpAoMktAddr *) Cpi1Optval)->au1Key,
                                pMkt->au1TcpAOMasterKey,
                                pMkt->u1TcpAOPasswdLength);

                    }
                    else
                    {
                        ((tTcpAoMktAddr *) Cpi1Optval)->u1KeyLen =
                            PtaPRetParms->cmdtype.option.u1Optlen;
                    }
                }
                else
                {
                    *(UINT4 *) Cpi1Optval =
                        PtaPRetParms->cmdtype.option.u4Value;
                    *pi4Optlen = PtaPRetParms->cmdtype.option.u1Optlen;
                }
            }
        }
        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
#endif
    }
    else if (CpsdtSock->i2SdtSockType == SOCK_RAW)
    {
        if (i4Level == IPPROTO_IP)
        {
            switch (i4Optname)
            {
#ifdef IP_WANTED
                case IP_MULTICAST_IF:
                {
                    tIpMreqn            MCReq;

                    if ((UINT4) *pi4Optlen > sizeof (tIpMreqn))
                    {
                        *pi4Optlen = sizeof (tIpMreqn);
                    }

                    if (IpGetIfConfigRecord ((UINT2) CpsdtSock->u4MCastIf,
                                             &IpInfo) != IP_SUCCESS)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }

                    MCReq.imr_ifindex = IpInfo.InterfaceId.u4IfIndex;
                    MCReq.imr_address.s_addr =
                        OSIX_HTONL (CpsdtSock->u4MCastAddr);
                    MCReq.imr_multiaddr.s_addr = ZERO;
                    MEMCPY (Cpi1Optval, &MCReq, *pi4Optlen);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;
                }
#endif
                case IP_MTU_DISCOVER:
                    *(INT4 *) Cpi1Optval = (INT4)
                        (CpsdtSock->i4PmtuDfFlag & SLI_PMTU_FLAG_MASK);
                    *pi4Optlen = sizeof (INT4);
                    break;
                case IP_PKTINFO:
                    *(INT4 *) Cpi1Optval = CpsdtSock->u4IpPktinfoFlag;
                    *pi4Optlen = sizeof (UINT4);
                    break;
                case IP_OPTIONS:
                    if (CpsdtSock->IpOption.pu1Option != NULL)
                    {
                        MEMCPY ((INT1 *) Cpi1Optval,
                                CpsdtSock->IpOption.pu1Option,
                                CpsdtSock->IpOption.u1OptLen);

                        *pi4Optlen = CpsdtSock->IpOption.u1OptLen;
                    }
                    break;
                case IP_TOS:
                    *(INT4 *) Cpi1Optval = CpsdtSock->i4IpTos;
                    *pi4Optlen = sizeof (INT4);
                    break;
                case IP_TTL:
                    *(INT4 *) Cpi1Optval = CpsdtSock->i4IpTtl;
                    *pi4Optlen = sizeof (INT4);
                    break;
                case IP_SOCK_MODE:
                    *(UINT1 *) Cpi1Optval = CpsdtSock->u1FdMode;
                    *pi4Optlen = sizeof (UINT1);
                    break;
                case IP_PKT_TX_CXTID:
                    *(UINT4 *) Cpi1Optval = CpsdtSock->u4TxContextId;
                    *pi4Optlen = sizeof (UINT4);
                    break;
                case IP_PKT_RX_CXTID:
                    *(UINT4 *) Cpi1Optval = CpsdtSock->u4RxContextId;
                    *pi4Optlen = sizeof (UINT4);
                    break;
                default:
                    CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
                    SLI_ERR (SLI_ENOPROTOOPT);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
            }
        }
        else if (i4Level == IPPROTO_IPV6)
        {
            switch (i4Optname)
            {

                case IP_SOCK_MODE:
                    *(UINT1 *) Cpi1Optval = CpsdtSock->u1FdMode;
                    *pi4Optlen = sizeof (UINT1);
                    break;
                case IP_PKT_TX_CXTID:
                    *(UINT4 *) Cpi1Optval = CpsdtSock->u4TxContextId;
                    *pi4Optlen = sizeof (UINT4);
                    break;
                case IP_PKT_RX_CXTID:
                    *(UINT4 *) Cpi1Optval = CpsdtSock->u4RxContextId;
                    *pi4Optlen = sizeof (UINT4);
                    break;
                default:
                    CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
                    SLI_ERR (SLI_ENOPROTOOPT);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
            }

        }

    }
    else if (CpsdtSock->i2SdtSockType == SOCK_DGRAM)
    {
        if (((i4Level == IPPROTO_IPV6) && (CpsdtSock->i2SdtFamily == AF_INET6))
            || ((i4Level == IPPROTO_IP) && (CpsdtSock->i2SdtFamily == AF_INET)))
        {
            switch (i4Optname)
            {
#ifdef IP_WANTED
                case IP_MULTICAST_IF:
                {
                    tIpMreqn            MCReq;

                    if ((UINT4) *pi4Optlen > sizeof (tIpMreqn))
                    {
                        *pi4Optlen = sizeof (tIpMreqn);
                    }

                    if (IpGetIfConfigRecord ((UINT2) CpsdtSock->u4MCastIf,
                                             &IpInfo) != IP_SUCCESS)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    MCReq.imr_ifindex = IpInfo.InterfaceId.u4IfIndex;
                    MCReq.imr_address.s_addr =
                        OSIX_HTONL (CpsdtSock->u4MCastAddr);
                    MCReq.imr_multiaddr.s_addr = ZERO;
                    MEMCPY (Cpi1Optval, &MCReq, *pi4Optlen);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;
                }
#endif
                case IP_MTU_DISCOVER:
                    *(INT4 *) Cpi1Optval = (INT4)
                        (CpsdtSock->i4PmtuDfFlag & SLI_PMTU_FLAG_MASK);
                    *pi4Optlen = sizeof (INT4);
                    break;
                case IP_PKTINFO:
                    *(UINT1 *) Cpi1Optval = CpsdtSock->u4IpPktinfoFlag;
                    *pi4Optlen = sizeof (UINT4);
                    break;
                case IP_TOS:
                    *(INT4 *) Cpi1Optval = CpsdtSock->i4IpTos;
                    *pi4Optlen = sizeof (INT4);
                    break;
                case IP_TTL:
                    *(INT4 *) Cpi1Optval = CpsdtSock->i4IpTtl;
                    *pi4Optlen = sizeof (INT4);
                    break;
                case IP_RECVIF:
                    *(UINT1 *) Cpi1Optval = CpsdtSock->u4IpPktinfoFlag;
                    *pi4Optlen = sizeof (UINT4);
                    break;
                case IP_SOCK_MODE:
                    *(UINT1 *) Cpi1Optval = CpsdtSock->u1FdMode;
                    *pi4Optlen = sizeof (UINT1);
                    break;
                case IP_PKT_TX_CXTID:
                    *(UINT4 *) Cpi1Optval = CpsdtSock->u4TxContextId;
                    *pi4Optlen = sizeof (UINT4);
                    break;
                case IP_PKT_RX_CXTID:
                    *(UINT4 *) Cpi1Optval = CpsdtSock->u4RxContextId;
                    *pi4Optlen = sizeof (UINT4);
                    break;
                default:
                    CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
                    SLI_ERR (SLI_ENOPROTOOPT);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
            }
        }

        if (i4Level == SOL_SOCKET)
        {
            switch (i4Optname)
            {
                case SO_BROADCAST:
                    *(INT4 *) Cpi1Optval = CpsdtSock->u1SockOpns & SO_BROADCAST;
                    *pi4Optlen = sizeof (INT4);
                    break;
                case SO_DONTROUTE:
                    *(INT4 *) Cpi1Optval = CpsdtSock->u1SockOpns & SO_DONTROUTE;
                    *pi4Optlen = sizeof (INT4);
                    break;
                default:
                    CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
                    SLI_ERR (SLI_ENOPROTOOPT);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
            }
        }
    }

    OsixSemGive (CpsdtSock->ProtectSemId);
    return SLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : SliSetsockopt                                       */
/*  Description     : This function sets the socket options specified     */
/*  Input(s)        :                                                     */
/*                    i4SockDesc - The socket identifier.                 */
/*                    i4Level - Level to operate. eg TCP.                 */
/*                    i4Optname - The option to be set.                   */
/*                    Cpi1Optval - The value of the option to be set      */
/*                    i4Optlen - The length of the option to be set.      */
/*  Output(s)       : None.                                               */
/*  Returns         : SUCCESS - If Sucessful                              */
/*                    SLI_ENOTSOCK - If the socket desc is     */
/*                                          invalid.                      */
/*                    SLI_ENOTCONN - The socket is not in   */
/*                                           a  connected state.          */
/*                    SLI_ENOPROTOOPT - The option request to  */
/*                                                 TCP failed.            */
/**************************************************************************/

INT4
SliSetsockopt (INT4 i4SockDesc, INT4 i4Level, INT4 i4Optname, VOID *Cpi1Optval,
               INT4 i4Optlen)
{
#ifdef TCP_WANTED
    tTcpToApplicationParms *PtaPRetParms = NULL;
    INT4                i4bReplyRcvd = FALSE;
    UINT4               u4Value = 0;
    INT4                i4AsyncSockDesc = 0;
#endif
    tSdtEntry          *CpsdtSock = NULL;
    tNetIpv4IfInfo      NetIpIfInf;

#ifdef MLD_WANTED
    tIpAddr             McastAddr;
    UINT4               u4IfIndex = 0;
#endif
#ifdef IP_WANTED
    UINT2               u2McPort = 0;
#endif

    MEMSET ((UINT1 *) &NetIpIfInf, 0, sizeof (tNetIpv4IfInfo));
    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];
    if ((OsixSemTake (CpsdtSock->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4SockDesc);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }

    if (((INT1 *) Cpi1Optval == NULL) || (i4Optlen <= ZERO))
    {
        CpsdtSock->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_FAILURE;
    }

    /* AF_INET6 family opens socket for both IPv4 and IPv6 if we are using LINUX
     * socket interface. In case of our SLI we have to open sockets for
     * individually. IPV6_V6ONLY option in LINUX socket interface opens IPV6
     * socket only. But SLI does not support this feature. To avoid the
     * Switches for FSIP and LINUX IP cases this option is stubbed to return
     * SLI_SUCCESS always*/
    if (i4Optname == IPV6_V6ONLY)
    {
        OsixSemGive (CpsdtSock->ProtectSemId);
        return SLI_SUCCESS;
    }

    if (CpsdtSock->i2SdtSockType == SOCK_DGRAM)
    {
        if (i4Level == SOL_SOCKET)
        {
            switch (i4Optname)
            {
                case SO_BROADCAST:
                    if (*(INT4 *) Cpi1Optval != ZERO)
                    {
                        CpsdtSock->u1SockOpns |= SO_BROADCAST;
                    }
                    else
                    {
                        CpsdtSock->u1SockOpns &= ~SO_BROADCAST;
                    }
                    break;
                case SO_DONTROUTE:
                    if (*(INT4 *) Cpi1Optval != ZERO)
                    {
                        CpsdtSock->u1SockOpns |= SO_DONTROUTE;
                    }
                    else
                    {
                        CpsdtSock->u1SockOpns &= ~SO_DONTROUTE;
                    }
                    break;
                default:
                    CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
                    SLI_ERR (SLI_ENOPROTOOPT);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
            }
        }
        if ((i4Level == IPPROTO_IPV6) && (CpsdtSock->i2SdtFamily == AF_INET6))
        {
            switch (i4Optname)
            {
                case IPV6_MULTICAST_IF:
                    CpsdtSock->u4IfIndex = *(UINT4 *) Cpi1Optval;
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;

                case IPV6_MULTICAST_HOPS:
                {
                    if ((*(INT4 *) Cpi1Optval < INIT_VALUE)
                        || (*(INT4 *) Cpi1Optval > SLI_MAX_HOP_LIMIT))
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    if (*(INT4 *) Cpi1Optval == INIT_VALUE)
                    {
                        CpsdtSock->i4multicasthlmt = SLI_DEF_HOP_LIMIT;
                    }
                    else
                    {
                        CpsdtSock->i4multicasthlmt = *(INT4 *) Cpi1Optval;
                    }
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;
                }
                case IPV6_MULTICAST_LOOP:
                    CpsdtSock->i4multicastloop = *(INT4 *) Cpi1Optval;
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;

                case IPV6_RECVHOPLIMIT:
                    CpsdtSock->i4recvhoplimit = *(INT4 *) Cpi1Optval;
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;

                case IPV6_UNICAST_HOPS:
                {
                    if ((*(INT4 *) Cpi1Optval < INIT_VALUE)
                        || (*(INT4 *) Cpi1Optval > SLI_MAX_HOP_LIMIT))
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    if (*(INT4 *) Cpi1Optval == INIT_VALUE)
                    {
                        CpsdtSock->i4unicasthlmt = SLI_DEF_HOP_LIMIT;
                    }
                    else
                    {
                        CpsdtSock->i4unicasthlmt = *(INT4 *) Cpi1Optval;
                    }
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;
                }

                case IPV6_JOIN_GROUP:
                {
#ifdef MLD_WANTED
                    MEMCPY (&McastAddr,
                            &((tSliIpv6Mreq *) Cpi1Optval)->ipv6mr_multiaddr,
                            sizeof (tIpAddr));
                    u4IfIndex = ((tSliIpv6Mreq *) Cpi1Optval)->ipv6mr_ifindex;

                    /* if the interface index is specified as ZERO,
                     * default interface should be taken
                     */
                    if (u4IfIndex == ZERO)
                    {
                        u4IfIndex = SLI_DEFAULT_IF;
                    }
                    if (IS_ADDR_MULTI (McastAddr))
                    {
                        if (MLDStartListening (u4IfIndex,
                                               (tIp6Addr *) & McastAddr) ==
                            SLI_FAILURE)
                        {
                            OsixSemGive (CpsdtSock->ProtectSemId);
                            return SLI_FAILURE;
                        }
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_SUCCESS;
                    }
#endif
                    CpsdtSock->i1ErrorCode = SLI_EINVAL;
                    SLI_ERR (SLI_EINVAL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
                case IPV6_LEAVE_GROUP:
                {
#ifdef MLD_WANTED
                    MEMCPY (&McastAddr,
                            &((tSliIpv6Mreq *) Cpi1Optval)->ipv6mr_multiaddr,
                            sizeof (tIpAddr));
                    u4IfIndex = ((tSliIpv6Mreq *) Cpi1Optval)->ipv6mr_ifindex;

                    /* if the interface index is specified as ZERO,
                     * default interface should be taken
                     */
                    if (u4IfIndex == ZERO)
                    {
                        u4IfIndex = SLI_DEFAULT_IF;
                    }

                    if (IS_ADDR_MULTI (McastAddr))
                    {
                        MLDStopListening (u4IfIndex, (tIp6Addr *) & McastAddr);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_SUCCESS;
                    }
#endif
                    CpsdtSock->i1ErrorCode = SLI_EINVAL;
                    SLI_ERR (SLI_EINVAL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
            }
        }

        if (((i4Level == IPPROTO_IPV6) && (CpsdtSock->i2SdtFamily == AF_INET6))
            || ((i4Level == IPPROTO_IP) && (CpsdtSock->i2SdtFamily == AF_INET)))
        {
            switch (i4Optname)
            {
#ifdef IP_WANTED
                case IP_MULTICAST_IF:
                {
                    tIpMreqn            MCReq;
                    INT4                i4MCIf = INVALID_INTERFACE;

                    MEMSET (&MCReq, ZERO, sizeof (tIpMreqn));
                    if ((UINT4) i4Optlen >= sizeof (tIpMreqn))
                    {
                        MEMCPY (&MCReq, Cpi1Optval, sizeof (tIpMreqn));
                    }
                    else if ((UINT4) i4Optlen >= sizeof (struct in_addr))
                    {
                        MEMCPY (&MCReq.imr_address, Cpi1Optval,
                                sizeof (struct in_addr));
                        MCReq.imr_ifindex = INVALID_INTERFACE;
                    }
                    else
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }

                    if (MCReq.imr_ifindex == INVALID_INTERFACE)
                    {
                        if (MCReq.imr_address.s_addr == INADDR_ANY)
                        {
                            IpGetPortFromIfIndex (SLI_DEFAULT_IF, &u2McPort);
                            CpsdtSock->u4MCastIf = u2McPort;
                            CpsdtSock->u4MCastAddr = ZERO;
                            OsixSemGive (CpsdtSock->ProtectSemId);
                            return SLI_SUCCESS;
                        }
                        if ((IpifIsOurAddressInCxt
                             (CpsdtSock->u4TxContextId,
                              OSIX_HTONL (MCReq.imr_address.s_addr))) == TRUE)
                        {
                            if ((IpGetIfIndexFromAddrInCxt (CpsdtSock->
                                                            u4TxContextId,
                                                            OSIX_HTONL (MCReq.
                                                                        imr_address.
                                                                        s_addr),
                                                            (UINT4 *) &i4MCIf))
                                != IP_FAILURE)
                            {
                                IpGetPortFromIfIndex ((UINT2) i4MCIf,
                                                      &u2McPort);
                                CpsdtSock->u4MCastIf = u2McPort;
                                CpsdtSock->u4MCastAddr =
                                    OSIX_HTONL (MCReq.imr_address.s_addr);
                                OsixSemGive (CpsdtSock->ProtectSemId);
                                return SLI_SUCCESS;
                            }
                            CpsdtSock->i1ErrorCode = SLI_EADDRNOTAVAIL;
                            SLI_ERR (SLI_EADDRNOTAVAIL);
                            OsixSemGive (CpsdtSock->ProtectSemId);
                            return SLI_FAILURE;
                        }
                    }
                    /* Check the IP interface exists */
                    if (NetIpv4GetIfInfo (MCReq.imr_ifindex, &NetIpIfInf) ==
                        NETIPV4_SUCCESS)
                    {
                        CpsdtSock->u4MCastIf = MCReq.imr_ifindex;
                        CpsdtSock->u4MCastAddr =
                            OSIX_HTONL (MCReq.imr_address.s_addr);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_SUCCESS;
                    }
                    CpsdtSock->i1ErrorCode = SLI_EADDRNOTAVAIL;
                    SLI_ERR (SLI_EADDRNOTAVAIL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
#endif
                case IP_MTU_DISCOVER:
                    if ((*(INT4 *) Cpi1Optval == IPPMTUDISC_DONT) ||
                        (*(INT4 *) Cpi1Optval == IPPMTUDISC_DO) ||
                        (*(INT4 *) Cpi1Optval == IPPMTUDISC_WANT))
                    {
                        CpsdtSock->i4PmtuDfFlag = *(INT4 *) Cpi1Optval;
                        if (*(INT4 *) Cpi1Optval == IPPMTUDISC_DONT)
                        {
                            CpsdtSock->fpSliHandlePmtuChange = NULL;
                        }
                    }
                    else
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    break;
                case IP_PKTINFO:
                    if (*(UINT4 *) Cpi1Optval != ZERO)
                    {
                        CpsdtSock->u4IpPktinfoFlag = IP_PKTINFO_ENA;
                    }
                    else
                    {
                        CpsdtSock->u4IpPktinfoFlag = IP_PKTINFO_DIS;
                    }
                    break;
                case IP_TOS:
                    CpsdtSock->i4IpTos = *(INT4 *) Cpi1Optval;
                    break;
                case IP_TTL:
                    CpsdtSock->i4IpTtl = *(INT4 *) Cpi1Optval;
                    break;
                case IP_SOCK_MODE:
                    CpsdtSock->u1FdMode = *(UINT1 *) Cpi1Optval;
                    break;
                case IP_PKT_TX_CXTID:
                    CpsdtSock->u4TxContextId = *(UINT4 *) Cpi1Optval;
                    break;
                case IP_PKT_RX_CXTID:
                    CpsdtSock->u4RxContextId = *(UINT4 *) Cpi1Optval;
                    break;
                default:
                    CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
                    SLI_ERR (SLI_ENOPROTOOPT);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
            }
        }
    }

    if (CpsdtSock->i2SdtSockType == SOCK_RAW)
    {
        if ((i4Level == IPPROTO_IP) && (CpsdtSock->i2SdtFamily == AF_INET))
        {
            switch (i4Optname)
            {
#ifdef IP_WANTED
                case IP_MULTICAST_IF:
                {
                    tIpMreqn            MCReq;
                    INT4                i4MCIf = INVALID_INTERFACE;

                    MEMSET (&MCReq, ZERO, sizeof (tIpMreqn));
                    if ((UINT4) i4Optlen >= sizeof (tIpMreqn))
                    {
                        MEMCPY (&MCReq, Cpi1Optval, sizeof (tIpMreqn));
                    }
                    else if ((UINT4) i4Optlen >= sizeof (struct in_addr))
                    {
                        MEMCPY (&MCReq.imr_address, Cpi1Optval,
                                sizeof (struct in_addr));
                        MCReq.imr_ifindex = INVALID_INTERFACE;
                    }
                    else
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }

                    if (MCReq.imr_ifindex == INVALID_INTERFACE)
                    {
                        if (MCReq.imr_address.s_addr == INADDR_ANY)
                        {
                            IpGetPortFromIfIndex (SLI_DEFAULT_IF, &u2McPort);
                            CpsdtSock->u4MCastIf = u2McPort;
                            CpsdtSock->u4MCastAddr = ZERO;
                            OsixSemGive (CpsdtSock->ProtectSemId);
                            return SLI_SUCCESS;
                        }
                        if ((IpifIsOurAddressInCxt
                             (CpsdtSock->u4TxContextId,
                              OSIX_NTOHL (MCReq.imr_address.s_addr))) == TRUE)
                        {
                            if ((IpGetIfIndexFromAddrInCxt (CpsdtSock->
                                                            u4TxContextId,
                                                            OSIX_NTOHL (MCReq.
                                                                        imr_address.
                                                                        s_addr),
                                                            (UINT4 *) &i4MCIf))
                                != IP_FAILURE)
                            {
                                IpGetPortFromIfIndex ((UINT2) i4MCIf,
                                                      &u2McPort);
                                CpsdtSock->u4MCastIf = u2McPort;
                                CpsdtSock->u4MCastAddr =
                                    OSIX_NTOHL (MCReq.imr_address.s_addr);
                                OsixSemGive (CpsdtSock->ProtectSemId);
                                return SLI_SUCCESS;
                            }

                            CpsdtSock->i1ErrorCode = SLI_EADDRNOTAVAIL;
                            SLI_ERR (SLI_EADDRNOTAVAIL);
                            OsixSemGive (CpsdtSock->ProtectSemId);
                            return SLI_FAILURE;

                        }
                    }
                    /* Check the IP interface exists */
                    if (NetIpv4GetIfInfo (MCReq.imr_ifindex, &NetIpIfInf) ==
                        NETIPV4_SUCCESS)
                    {
                        CpsdtSock->u4MCastIf = MCReq.imr_ifindex;
                        CpsdtSock->u4MCastAddr =
                            OSIX_NTOHL (MCReq.imr_address.s_addr);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_SUCCESS;
                    }
                    CpsdtSock->i1ErrorCode = SLI_EADDRNOTAVAIL;
                    SLI_ERR (SLI_EADDRNOTAVAIL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
#endif
                case IP_MTU_DISCOVER:
                    if ((*(INT1 *) Cpi1Optval == IPPMTUDISC_DONT) ||
                        (*(INT1 *) Cpi1Optval == IPPMTUDISC_DO) ||
                        (*(INT1 *) Cpi1Optval == IPPMTUDISC_WANT))
                    {
                        CpsdtSock->i4PmtuDfFlag = *(INT1 *) Cpi1Optval;
                        if (*(INT1 *) Cpi1Optval == IPPMTUDISC_DONT)
                        {
                            CpsdtSock->fpSliHandlePmtuChange = NULL;
                        }
                    }
                    else
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    break;

                case IP_OPTIONS:
                    if (i4Optlen > MAX_IP_OPT_LEN)
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    CpsdtSock->IpOption.pu1Option =
                        MemAllocMemBlk (gSliIpOptionPoolId);

                    if (CpsdtSock->IpOption.pu1Option == NULL)
                    {
                        CpsdtSock->i1ErrorCode = SLI_EMEMFAIL;
                        SLI_ERR (SLI_EMEMFAIL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }

                    CpsdtSock->IpOption.u1OptLen = (UINT1) i4Optlen;
                    MEMCPY (CpsdtSock->IpOption.pu1Option,
                            (INT1 *) Cpi1Optval, i4Optlen);
                    break;

                case IP_PKTINFO:
                    if (*(UINT4 *) Cpi1Optval != ZERO)
                    {
                        CpsdtSock->u4IpPktinfoFlag = IP_PKTINFO_ENA;
                    }
                    else
                    {
                        CpsdtSock->u4IpPktinfoFlag = IP_PKTINFO_DIS;
                    }
                    break;

                case IP_TOS:
                    CpsdtSock->i4IpTos = *(INT4 *) Cpi1Optval;
                    break;
                case IP_TTL:
                    CpsdtSock->i4IpTtl = *(INT4 *) Cpi1Optval;
                    break;
                case IP_HDRINCL:
                    if ((*(INT1 *) Cpi1Optval != TRUE)
                        && (*(INT1 *) Cpi1Optval != FALSE))
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    CpsdtSock->i4IpHdrIncl = *(INT4 *) Cpi1Optval;
                    break;
                case IP_ROUTER_ALERT:
                    /* Currently this option is not supported */
                    break;
                case IP_SOCK_MODE:
                    CpsdtSock->u1FdMode = *(UINT1 *) Cpi1Optval;
                    break;
                case IP_PKT_TX_CXTID:
                    CpsdtSock->u4TxContextId = *(UINT4 *) Cpi1Optval;
                    break;
                case IP_PKT_RX_CXTID:
                    CpsdtSock->u4RxContextId = *(UINT4 *) Cpi1Optval;
                    break;
                default:
                    CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
                    SLI_ERR (SLI_ENOPROTOOPT);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
            }
        }
        if ((i4Level == IPPROTO_IPV6) && (CpsdtSock->i2SdtFamily == AF_INET6))
        {
            switch (i4Optname)
            {
                case IPV6_MULTICAST_IF:
                    CpsdtSock->u4IfIndex = *(UINT4 *) Cpi1Optval;
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;

                case IPV6_MULTICAST_HOPS:
                {
                    if ((*(INT4 *) Cpi1Optval < INIT_VALUE)
                        || (*(INT4 *) Cpi1Optval > SLI_MAX_HOP_LIMIT))
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    if (*(INT4 *) Cpi1Optval == INIT_VALUE)
                    {
                        CpsdtSock->i4multicasthlmt = SLI_DEF_HOP_LIMIT;
                    }
                    else
                    {
                        CpsdtSock->i4multicasthlmt = *(INT4 *) Cpi1Optval;
                    }
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;
                }
                case IPV6_MULTICAST_LOOP:
                    CpsdtSock->i4multicastloop = *(INT4 *) Cpi1Optval;
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;
                case IPV6_RECVHOPLIMIT:
                    CpsdtSock->i4recvhoplimit = *(INT4 *) Cpi1Optval;
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;
                case IPV6_UNICAST_HOPS:
                {
                    if ((*(INT4 *) Cpi1Optval < INIT_VALUE)
                        || (*(INT4 *) Cpi1Optval > SLI_MAX_HOP_LIMIT))
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    if (*(INT4 *) Cpi1Optval == INIT_VALUE)
                        CpsdtSock->i4unicasthlmt = SLI_DEF_HOP_LIMIT;
                    else
                        CpsdtSock->i4unicasthlmt = *(INT4 *) Cpi1Optval;
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_SUCCESS;
                }
                case IP_MTU_DISCOVER:
                    if ((*(INT1 *) Cpi1Optval == IPPMTUDISC_DONT) ||
                        (*(INT1 *) Cpi1Optval == IPPMTUDISC_DO) ||
                        (*(INT1 *) Cpi1Optval == IPPMTUDISC_WANT))
                    {
                        CpsdtSock->i4PmtuDfFlag = *(INT1 *) Cpi1Optval;
                        if (*(INT1 *) Cpi1Optval == IPPMTUDISC_DONT)
                        {
                            CpsdtSock->fpSliHandlePmtuChange = NULL;
                        }
                    }
                    else
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    break;

                case IP_HDRINCL:
                    if ((*(INT1 *) Cpi1Optval != TRUE)
                        && (*(INT1 *) Cpi1Optval != FALSE))
                    {
                        CpsdtSock->i1ErrorCode = SLI_EINVAL;
                        SLI_ERR (SLI_EINVAL);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    CpsdtSock->i4IpHdrIncl = *(INT4 *) Cpi1Optval;
                    break;
                case IP_RECVIF:
                    if (*(UINT4 *) Cpi1Optval != ZERO)
                    {
                        CpsdtSock->u4IpPktinfoFlag = IP_PKTINFO_ENA;
                    }
                    else
                    {
                        CpsdtSock->u4IpPktinfoFlag = IP_PKTINFO_DIS;
                    }
                    break;
                case IP_SOCK_MODE:
                    CpsdtSock->u1FdMode = *(UINT1 *) Cpi1Optval;
                    break;
                case IP_PKT_TX_CXTID:
                    CpsdtSock->u4TxContextId = *(UINT4 *) Cpi1Optval;
                    break;
                case IP_PKT_RX_CXTID:
                    CpsdtSock->u4RxContextId = *(UINT4 *) Cpi1Optval;
                    break;
#if defined IP6_WANTED && defined MLD_WANTED

                case IPV6_ADD_MEMBERSHIP:
                {
                    MEMCPY (&McastAddr,
                            &((tSliIpv6Mreq *) Cpi1Optval)->ipv6mr_multiaddr,
                            sizeof (tIpAddr));
                    u4IfIndex =
                        (UINT4) ((tSliIpv6Mreq *) Cpi1Optval)->ipv6mr_ifindex;

                    /* if the interface index is specified as ZERO,
                     * default interface should be taken
                     */
                    if (u4IfIndex == ZERO)
                    {
                        u4IfIndex = SLI_DEFAULT_IF;
                    }
                    if ((IS_ADDR_MULTI (McastAddr)) &&
                        (NetIpv6McastJoin (u4IfIndex,
                                           (tIp6Addr *) & McastAddr) ==
                         NETIPV6_SUCCESS))
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_SUCCESS;
                    }
                    CpsdtSock->i1ErrorCode = SLI_EINVAL;
                    SLI_ERR (SLI_EINVAL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
                case IPV6_DROP_MEMBERSHIP:
                {
                    MEMCPY (&McastAddr,
                            &((tSliIpv6Mreq *) Cpi1Optval)->ipv6mr_multiaddr,
                            sizeof (tIpAddr));
                    u4IfIndex =
                        (UINT4) ((tSliIpv6Mreq *) Cpi1Optval)->ipv6mr_ifindex;

                    /* if the interface index is specified as ZERO,
                     * default interface should be taken
                     */
                    if (u4IfIndex == ZERO)
                    {
                        u4IfIndex = SLI_DEFAULT_IF;
                    }

                    if ((IS_ADDR_MULTI (McastAddr)) &&
                        (NetIpv6McastLeave (u4IfIndex,
                                            (tIp6Addr *) & McastAddr) ==
                         NETIPV6_SUCCESS))
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_SUCCESS;
                    }
                    CpsdtSock->i1ErrorCode = SLI_EINVAL;
                    SLI_ERR (SLI_EINVAL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
#endif

                default:
                    CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
                    SLI_ERR (SLI_ENOPROTOOPT);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
            }
        }
    }
#ifdef TCP_WANTED
    else if (CpsdtSock->i2SdtSockType == SOCK_STREAM)
    {

        if ((CpsdtSock->state != SS_CONNECTED) && (i4Optname == SO_REUSEADDR))
        {
            u4Value = *(INT4 *) Cpi1Optval;
            if (u4Value != SLI_ZERO)    /* Setting   the Options */
            {
                CpsdtSock->u4ValidOptions |= SO_REUSEADDR;
            }
            else
            {
                CpsdtSock->u4ValidOptions &= (~(SO_REUSEADDR));
            }
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_SUCCESS;
        }

        if (i4Optname == IP_TTL)
        {
            CpsdtSock->i4IpTtl = *(INT4 *) Cpi1Optval;
            u4Value = *(INT4 *) Cpi1Optval;
            PtaPRetParms = SliTcpIssueOptionRequest (i4SockDesc, SET,
                                                     (UINT2) i4Optname,
                                                     (UINT1) i4Optlen, u4Value);
            if (PtaPRetParms == NULL)
            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }

            FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_SUCCESS;
        }

        /* Set TCP Sender & receive buffer size */
        if ((i4Optname == SO_RCVBUF) || (i4Optname == SO_SNDBUF))
        {
            u4Value = *(UINT4 *) Cpi1Optval;

            if (i4Optname == SO_RCVBUF)
            {
                if ((u4Value < TCP_MIN_RECV_BUF_SIZE) ||
                    (u4Value > TCP_MAX_RECV_BUF_SIZE))
                {
                    CpsdtSock->i1ErrorCode = SLI_EINVAL;
                    SLI_ERR (SLI_EINVAL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
            }

            PtaPRetParms = SliTcpIssueOptionRequest (i4SockDesc, SET,
                                                     (UINT2) i4Optname,
                                                     (UINT1) i4Optlen, u4Value);
            if (PtaPRetParms == NULL)
            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }

            FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_SUCCESS;
        }

        /* Set TCP MD5 Key for Peer */
        if (i4Optname == TCP_MD5SIG)
        {
            struct tcp_md5sig  *pMd5Option = (struct tcp_md5sig *) Cpi1Optval;

            if (i4Optlen != sizeof (struct tcp_md5sig))

            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return (SLI_FAILURE);
            }

            if ((SliValidateMd5Option (pMd5Option)) == SLI_FAILURE)
            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return (SLI_FAILURE);
            }
            /* For listen or unconnected active stream sockets
             * security associations must be maintained -
             * <peer IP, password>. MD5 SA list is maintained here */
            if ((CpsdtSock->state == SS_UNCONNECTED) ||
                ((CpsdtSock->state == SS_CONNECTED) &&
                 (TcpCheckListenConn (CpsdtSock->u4ConnId) == TRUE)))
            {
                if (pMd5Option->tcpm_keylen != ZERO)
                {
                    if ((SliAddMd5SA (i4SockDesc, pMd5Option)) == SLI_FAILURE)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                }
                else
                {
                    if ((SliDelMd5SA (i4SockDesc, pMd5Option)) == SLI_FAILURE)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                }
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;
            }
            /* For connected socket pass the key and key length directly 
             * to TCP. Need not be maintained in SLI */
            else if (CpsdtSock->state == SS_CONNECTED)
            {
                if (pMd5Option->tcpm_keylen != ZERO)
                {
                    u4Value = PTR_TO_U4 (pMd5Option->tcpm_key);
                }
                else
                {
                    u4Value = ZERO;
                }

                PtaPRetParms = SliTcpIssueOptionRequest
                    (i4SockDesc, SET, (UINT2) i4Optname,
                     (UINT1) pMd5Option->tcpm_keylen, u4Value);

                if (PtaPRetParms == NULL)
                {
                    if (pMd5Option->tcpm_keylen != ZERO)
                    {
                        SLI_ERR (SLI_EPERM);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    else
                    {
                        SLI_ERR (SLI_ENOENT);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                }
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;
            }
        }

        if (i4Optname == TCP_AO_SIG)
        {
            tTcpAoMktAddr      *pAoOption = (tTcpAoMktAddr *) Cpi1Optval;

            if (i4Optlen != sizeof (tTcpAoMktAddr))
            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return (SLI_FAILURE);
            }

            if ((SliValidateTcpAoOption (pAoOption)) == SLI_FAILURE)
            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return (SLI_FAILURE);
            }
            /* For listen or unconnected active stream sockets
             * security associations must be maintained -
             * TCP-AO MKT list is maintained here */
            if ((CpsdtSock->state == SS_UNCONNECTED) ||
                ((CpsdtSock->state == SS_CONNECTED) &&
                 (TcpCheckListenConn (CpsdtSock->u4ConnId) == TRUE)))
            {
                if (pAoOption->u1KeyLen != ZERO)
                {
                    if ((SliAddTcpAoMkt (i4SockDesc, pAoOption)) == SLI_FAILURE)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                }
                else
                {
                    if ((SliDelTcpAoMkt (i4SockDesc, pAoOption)) == SLI_FAILURE)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                }
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;
            }
            /* For connected socket pass the key and key length directly 
             * to TCP. Need not be maintained in SLI */
            else if (CpsdtSock->state == SS_CONNECTED)
            {
                if (pAoOption->u1KeyLen != ZERO)
                {
                    u4Value = PTR_TO_U4 (pAoOption);    /* Converting pAoOption as U4 */
                }
                else
                {
                    u4Value = ZERO;
                }

                PtaPRetParms = SliTcpIssueOptionRequest
                    (i4SockDesc, SET, (UINT2) i4Optname,
                     (UINT1) sizeof (pAoOption), u4Value);

                if (PtaPRetParms == NULL)
                {
                    if (pAoOption->u1KeyLen != ZERO)
                    {
                        SLI_ERR (SLI_EPERM);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                    else
                    {
                        SLI_ERR (SLI_ENOENT);
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                }
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;
            }
        }
        /* Set TCP-AO neighbor MKT Match Packet Discard Config for Peer */
        if (i4Optname == TCP_AO_NOMKT_MCH)
        {
            tTcpAoNeighCfg     *pTcpAoNghCfg = (tTcpAoNeighCfg *) Cpi1Optval;

            if (i4Optlen != sizeof (tTcpAoNeighCfg))

            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return (SLI_FAILURE);
            }

            if ((SliValidateTcpAoNghCfg (pTcpAoNghCfg)) == SLI_FAILURE)
            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return (SLI_FAILURE);
            }
            /* 
             * For listen or unconnected active stream sockets
             * TCP-AO configs must be maintained -
             */
            if ((CpsdtSock->state == SS_UNCONNECTED) ||
                ((CpsdtSock->state == SS_CONNECTED) &&
                 (TcpCheckListenConn (CpsdtSock->u4ConnId) == TRUE)))
            {
                if (pTcpAoNghCfg->u1NoMktMchPckDsc != TCP_ONE)
                {
                    if ((SliAddTcpAoNghMktCfg (i4SockDesc, pTcpAoNghCfg)) ==
                        SLI_FAILURE)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                }
                else
                {
                    if ((SliDelTcpAoNghMktCfg (i4SockDesc, pTcpAoNghCfg)) ==
                        SLI_FAILURE)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                }
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;
            }
            /* For connected socket pass config directly 
             * to TCP. Need not be maintained in SLI */
            else if (CpsdtSock->state == SS_CONNECTED)
            {

                u4Value = ((UINT4) pTcpAoNghCfg->u1NoMktMchPckDsc);

                PtaPRetParms = SliTcpIssueOptionRequest
                    (i4SockDesc, SET, (UINT2) i4Optname,
                     (UINT1) sizeof (UINT4), u4Value);

                if (PtaPRetParms == NULL)
                {
                    CpsdtSock->i1ErrorCode = SLI_EINVAL;
                    SLI_ERR (SLI_EINVAL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;
            }
        }

        /* Set TCP-AO neighbor Icmp Config for Peer */
        if (i4Optname == TCP_AO_ICMP_ACC)
        {
            tTcpAoNeighCfg     *pTcpAoNghCfg = (tTcpAoNeighCfg *) Cpi1Optval;

            if (i4Optlen != sizeof (tTcpAoNeighCfg))

            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return (SLI_FAILURE);
            }

            if ((SliValidateTcpAoNghCfg (pTcpAoNghCfg)) == SLI_FAILURE)
            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return (SLI_FAILURE);
            }
            /* 
             * For listen or unconnected active stream sockets
             * TCP-AO configs must be maintained -
             */
            if ((CpsdtSock->state == SS_UNCONNECTED) ||
                ((CpsdtSock->state == SS_CONNECTED) &&
                 (TcpCheckListenConn (CpsdtSock->u4ConnId) == TRUE)))
            {
                if (pTcpAoNghCfg->u1IcmpAccpt != TCP_TWO)
                {
                    if ((SliAddTcpAoNghIcmpCfg (i4SockDesc, pTcpAoNghCfg)) ==
                        SLI_FAILURE)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                }
                else
                {
                    if ((SliDelTcpAoNghIcmpCfg (i4SockDesc, pTcpAoNghCfg)) ==
                        SLI_FAILURE)
                    {
                        OsixSemGive (CpsdtSock->ProtectSemId);
                        return SLI_FAILURE;
                    }
                }
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;
            }
            /* For connected socket pass config directly 
             * to TCP. Need not be maintained in SLI */
            else if (CpsdtSock->state == SS_CONNECTED)
            {

                u4Value = ((UINT4) pTcpAoNghCfg->u1IcmpAccpt);

                PtaPRetParms = SliTcpIssueOptionRequest
                    (i4SockDesc, SET, (UINT2) i4Optname,
                     (UINT1) sizeof (UINT4), u4Value);

                if (PtaPRetParms == NULL)
                {
                    CpsdtSock->i1ErrorCode = SLI_EINVAL;
                    SLI_ERR (SLI_EINVAL);
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_SUCCESS;
            }
        }
        if ((i4Optname == IP_PKT_TX_CXTID) || (i4Optname == IP_PKT_RX_CXTID))
        {
            if (i4Optname == IP_PKT_TX_CXTID)
            {
                CpsdtSock->u4TxContextId = *(UINT4 *) Cpi1Optval;
            }
            else if (i4Optname == IP_PKT_RX_CXTID)
            {
                CpsdtSock->u4RxContextId = *(UINT4 *) Cpi1Optval;
            }
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_SUCCESS;
        }

        if (CpsdtSock->state != SS_CONNECTED)
        {
            CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
            SLI_ERR (SLI_ENOTCONN);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }

        /* SET operation is not valid for TCP_MAXSEG and SO_TYPE options */
        if ((i4Optname == TCP_MAXSEG) || (i4Optname == SO_TYPE))
        {
            CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
            SLI_ERR (SLI_ENOPROTOOPT);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
        /*
         * Only IP options will be of variable length. Thus, rather than   
         * using an explicit char pointer, we can use the u4Value field   
         * itself to accomodate the address of (INT1 *)Cpi1Optval.        
         */

        if ((i4Optname == IP_OPTIONS) || (i4Optname == SO_LINGER))
        {
            u4Value = *(UINT4 *) Cpi1Optval;
        }
        else
        {
            u4Value = *(INT1 *) Cpi1Optval;
        }

        if (i4Optname == IPV6_UNICAST_HOPS)
        {
            if (CpsdtSock->i2SdtFamily != AF_INET6)
            {
                CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
                SLI_ERR (SLI_ENOPROTOOPT);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            if ((*(INT2 *) Cpi1Optval < INIT_VALUE)
                || (*(INT2 *) Cpi1Optval > SLI_MAX_HOP_LIMIT))
            {
                CpsdtSock->i1ErrorCode = SLI_EINVAL;
                SLI_ERR (SLI_EINVAL);
                OsixSemGive (CpsdtSock->ProtectSemId);
                return SLI_FAILURE;
            }
            if (*(INT2 *) Cpi1Optval == INIT_VALUE)
                u4Value = SLI_DEF_HOP_LIMIT;

        }

        while (i4bReplyRcvd == FALSE)
        {
            if (PtaPRetParms != NULL)
            {
                FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
                PtaPRetParms = NULL;
            }
            PtaPRetParms = SliTcpIssueOptionRequest (i4SockDesc, SET,
                                                     (UINT2) i4Optname,
                                                     (UINT1) i4Optlen, u4Value);

            if (PtaPRetParms == NULL)
            {
                if (CpsdtSock->u4ValidOptions & TCPO_NODELAY)
                {
                    OsixSemGive (CpsdtSock->ProtectSemId);
                    return SLI_FAILURE;
                }
                else
                    continue;
            }
            if ((CpsdtSock->u4ValidOptions & TCPO_NODELAY) ||
                (PtaPRetParms->u1Cmd == OPTIONS_CMD) ||
                ((PtaPRetParms->u1Cmd == ERROR_MSG) &&
                 (PtaPRetParms->cmdtype.error.u1ErrorCmd == OPTIONS_CMD)))
            {
                i4bReplyRcvd = TRUE;
            }
            if (PtaPRetParms->u1Cmd == CONN_REPORT)
            {
                /* Asynchronous Message arrived. Handle it. */
                i4AsyncSockDesc = SliTcpFindSockDesc (PtaPRetParms->u4ConnId);
                if (CpsdtSock->fpSliTcpHandleAsyncMesg != NULL)
                {
                    CpsdtSock->
                        fpSliTcpHandleAsyncMesg (i4AsyncSockDesc, PtaPRetParms);
                }
            }
        }

        if (PtaPRetParms->u1Cmd == ERROR_MSG)
        {
            FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
            CpsdtSock->i1ErrorCode = SLI_ENOPROTOOPT;
            SLI_ERR (SLI_ENOPROTOOPT);
            OsixSemGive (CpsdtSock->ProtectSemId);
            return SLI_FAILURE;
        }
        else
        {
            FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
        }
    }
#endif
    OsixSemGive (CpsdtSock->ProtectSemId);
    return SLI_SUCCESS;
}

#ifdef TCP_WANTED
/**************************************************************************/
/*  Function Name   : SliRegisterAsyncHandler                             */
/*  Description     : This function registers the asynchronous            */
/*                    message handler for the specified socket.           */
/*                    This call has to be called just after the           */
/*                    connection establishment has taken place.           */
/*  Input(s)        : i4SockDesc - The socket descriptor for the          */
/*                    async_handler - The asynchronous message handler    */
/*  Output(s)       : None.                                               */
/*  Returns         : SUCCESS - If Sucessful                              */
/*                    SLI_ENOTSOCK - If the socket desc is     */
/*                                          invalid.                      */
/**************************************************************************/

INT4
SliRegisterAsyncHandler (INT4 i4SockDesc, VOID (*async_handler) (INT4, INT1 *))
{
    INT4                i4RetError = ZERO;
    tSdtEntry          *PsdtSock = NULL;

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    PsdtSock = SOCK_DESC_TABLE[i4SockDesc];

    if ((PsdtSock->i2SdtSockType == SOCK_DGRAM) ||
        (PsdtSock->i2SdtSockType == SOCK_RAW))
    {
        PsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        return SLI_FAILURE;
    }

    if ((i4RetError = SliTcpIssueRegAsyncRequest (i4SockDesc,
                                                  async_handler)) != OK)
    {                            /*error is set by SliTcpIssueRegAsyncRequest */
        return SLI_FAILURE;
    }
    return SLI_SUCCESS;
}

#ifdef TCP_WANTED
/* This function is not called */
/**************************************************************************/
/*  Function Name   : SliStatus                                           */
/*  Description     : This function can be used by the caller to          */
/*                    get the status of a connection as in TCP            */
/*  Input(s)        : i4SockDesc - The socket identifier.                 */
/*  Output(s)       : ErrCode                                             */
/*                    SUCCESS - If successful                             */
/*                    SLI_ENOTSOCK - If the socket desc is     */
/*                                              invalid.                  */
/*                    SLI_ENOTCONN - The socket is not in   */
/*                                           a connected state.           */
/*                    SLI_ERR_STATUS_OPER_FAILED: The status request to   */
/*                                          TCP failed.                   */
/*  Returns         : Pointer to TcpToAppParameters                       */
/**************************************************************************/

tTcpToApplicationParms *
SliStatus (INT4 i4SockDesc, INT1 *ErrCode)
{
    tSdtEntry          *CpsdtSock = NULL;
    INT4                i4bReplyRcvd = FALSE;
    tTcpToApplicationParms *pStatus = NULL;

    if (SliLookup (i4SockDesc) == FALSE)
    {
        *ErrCode = SLI_ENOTSOCK;
        SLI_ERR (SLI_ENOTSOCK);
        return pStatus;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];

    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) ||
        (CpsdtSock->i2SdtSockType == SOCK_RAW))
    {
        *ErrCode = SLI_EOPNOTSUPP;
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        return pStatus;
    }
    if ((CpsdtSock->state != SS_CONNECTED) ||
        (CpsdtSock->state == SS_TX_SHUTDOWN))
    {
        *ErrCode = SLI_ENOTCONN;
        CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
        SLI_ERR (SLI_ENOTCONN);
        return pStatus;
    }

    while (i4bReplyRcvd == FALSE)
    {
        pStatus = SliTcpIssueStatusRequest (i4SockDesc);
        if (pStatus == NULL)
        {
            if (CpsdtSock->u4ValidOptions & TCPO_NODELAY)
            {
                *ErrCode = SLI_EAGAIN;
                CpsdtSock->i1ErrorCode = SLI_EAGAIN;
                SLI_ERR (SLI_EAGAIN);
                return NULL;
            }
            else
            {
                continue;
            }
        }
        if ((CpsdtSock->u4ValidOptions & TCPO_NODELAY) ||
            (pStatus->u1Cmd == STATUS_CMD) ||
            (((pStatus->u1Cmd == ERROR_MSG) &&
              (pStatus->cmdtype.error.u1ErrorCmd == STATUS_CMD))))
        {
            i4bReplyRcvd = TRUE;
        }
    }
    if (pStatus->u1Cmd == ERROR_MSG)
    {
        FREE_TCP_TO_APPLICATION_PARMS (pStatus);
        /**ErrCode = SLI_ERR_STATUS_OPER_FAILED;
        SLI_ERR (SLI_ERR_STATUS_OPER_FAILED);*/
        return NULL;
    }
    *ErrCode = SUCCESS;
    return pStatus;
}

/**************************************************************************/
/*  Function Name   : SliAbort                                            */
/*  Description     : This function aborts a connection.                  */
/*  Input(s)        : i4SockDesc - The Socket Identifier.                 */
/*  Output(s)       : None.                                               */
/*  Returns         : SUCCESS - If Sucessful                              */
/*                    SLI_ENOTSOCK - If the socket             */
/*                                           desc is invalid              */
/*                    SLI_ENOTCONN - If the socket is       */
/*                                        an unconnected state.           */
/*                    SLI_ERR_COULDNT_ABORT_CONN : If the Connection      */
/*                                       could not be aborted             */
/**************************************************************************/

INT4
SliAbort (INT4 i4SockDesc)
{
    tTcpToApplicationParms *PtaPRetParms = NULL;
    tSdtEntry          *CpsdtSock = NULL;
    INT4                i4bReplyRcvd = FALSE;
    INT4                i4AsyncSockDesc;

    if (SliLookup (i4SockDesc) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    CpsdtSock = SOCK_DESC_TABLE[i4SockDesc];

    if ((CpsdtSock->i2SdtSockType == SOCK_DGRAM) ||
        (CpsdtSock->i2SdtSockType == SOCK_RAW))
    {
        CpsdtSock->i1ErrorCode = SLI_EOPNOTSUPP;
        SLI_ERR (SLI_EOPNOTSUPP);
        return SLI_FAILURE;
    }

    if (CpsdtSock->state != SS_CONNECTED)
    {
        CpsdtSock->i1ErrorCode = SLI_ENOTCONN;
        SLI_ERR (SLI_ENOTCONN);
        return SLI_FAILURE;
    }

    while (i4bReplyRcvd == FALSE)
    {
        PtaPRetParms = SliTcpIssueAbortRequest (i4SockDesc);
        if (PtaPRetParms == NULL)
        {
            if (CpsdtSock->u4ValidOptions & TCPO_NODELAY)
            {
                return SLI_FAILURE;
            }
            else
            {
                continue;
            }
        }
        if ((CpsdtSock->u4ValidOptions & TCPO_NODELAY) ||
            (PtaPRetParms->u1Cmd == ABORT_CMD) ||
            ((PtaPRetParms->u1Cmd == ERROR_MSG) &&
             (PtaPRetParms->cmdtype.error.u1ErrorCmd == ABORT_CMD)))
        {
            i4bReplyRcvd = TRUE;
        }
        if (PtaPRetParms->u1Cmd == CONN_REPORT)
        {

            /* Asynchronous Message arrived. Handle it. */
            i4AsyncSockDesc = SliTcpFindSockDesc (PtaPRetParms->u4ConnId);
            if (CpsdtSock->fpSliTcpHandleAsyncMesg != NULL)
            {
                CpsdtSock->fpSliTcpHandleAsyncMesg (i4AsyncSockDesc,
                                                    PtaPRetParms);
            }
        }
    }

    if (PtaPRetParms->u1Cmd == ERROR_MSG)
    {
        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
        return SLI_FAILURE;
    }
    else
    {
        FREE_TCP_TO_APPLICATION_PARMS (PtaPRetParms);
        CpsdtSock->state = SS_UNCONNECTED;
        SliDestroySocket (i4SockDesc);
        return SLI_SUCCESS;
    }
}
#endif
#endif /* TCP_WANTED */

/***************************************************************************/
/*  Function Name   : SliSendmsg                                           */
/*  Description     : Used to send Ancillary data about socket.            */
/*  Input(s)        : i4Sd - the socket descriptor                         */
/*                    msg - the message header pointer                     */
/*                    flags - flags associated with the call.              */
/*  Output(s)       : None.                                                */
/*  Returns         : No. of characters sent or Error Code.                */
/***************************************************************************/

INT4
SliSendmsg (INT4 i4Sd, const struct msghdr *pMsg, UINT4 u4Flags)
{
    tSdtEntry          *pSocket = NULL;
    tInPktinfo         *pInPktinfo = NULL;
    UINT4               u4ContextId = ZERO;
    INT2                i2family = ZERO;
    INT4                i4retval = SLI_FAILURE;
#ifdef IP6_WANTED
    tIn6Pktinfo        *pIn6Pktinfo;
#endif

    UNUSED_PARAM (u4Flags);
    if (SliLookup (i4Sd) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    pSocket = SOCK_DESC_TABLE[i4Sd];
    if ((OsixSemTake (pSocket->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4Sd);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }
    i2family = pSocket->i2SdtFamily;

    /* Since this call is supported for IP_PKTINFO option
     * the following check is made */
    if (((pSocket->i2SdtSockType != SOCK_DGRAM) &&
         (pSocket->i2SdtSockType != SOCK_RAW)) ||
        (pSocket->u4IpPktinfoFlag != IP_PKTINFO_ENA))
    {
        pSocket->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        OsixSemGive (pSocket->ProtectSemId);
        return SLI_FAILURE;
    }
    if (i2family == AF_INET)
    {
        pInPktinfo = (tInPktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (pMsg));
        pSocket->inPktinfo.ipi_ifindex = pInPktinfo->ipi_ifindex;
        if (pInPktinfo->ipi_ifindex != SLI_UDP_PORT)
        {
            VcmGetContextInfoFromIpIfIndex (pInPktinfo->ipi_ifindex,
                                            &u4ContextId);
            pSocket->u4TxContextId = u4ContextId;
        }
        pSocket->inPktinfo.ipi_spec_dst = pInPktinfo->ipi_spec_dst;
        pSocket->inPktinfo.ipi_addr = pInPktinfo->ipi_addr;
        i4retval = sizeof (tInPktinfo);
    }
#ifdef IP6_WANTED
    else if (i2family == AF_INET6)
    {
        pIn6Pktinfo = (tIn6Pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (pMsg));
        pSocket->in6Pktinfo.ipi6_ifindex = pIn6Pktinfo->ipi6_ifindex;
        pSocket->in6Pktinfo.in6_hoplimit = pIn6Pktinfo->in6_hoplimit;
        V6_NTOHL ((tIpAddr *) (VOID *) &pSocket->in6Pktinfo.ipi6_addr,
                  (tIpAddr *) (VOID *) &pIn6Pktinfo->ipi6_addr);
        i4retval = sizeof (tIn6Pktinfo);
    }
#endif
    OsixSemGive (pSocket->ProtectSemId);
    return i4retval;

}

/**************************************************************************/
/*  Function Name   : SliRecvmsg                                          */
/*  Description     : Used to receive Ancillary data about socket.        */
/*  Input(s)        : i4Sd - the socket descriptor                        */
/*                    pMsg - the message header pointer                   */
/*                    flags - flags associated with the call.             */
/*  Output(s)       : None.                                               */
/*  Returns         : No. of characters received or Error Code.           */
/**************************************************************************/

INT4
SliRecvmsg (INT4 i4Sd, struct msghdr *pMsg, UINT4 u4Flags)
{
    tSdtEntry          *pSocket = NULL;
    tInPktinfo         *pInPktinfo = NULL;
    INT2                i2family = ZERO;
    INT4                i4retval = SLI_FAILURE;
#ifdef IP6_WANTED
    tIn6Pktinfo        *pIn6Pktinfo = NULL;
#endif

    UNUSED_PARAM (u4Flags);

    if (SliLookup (i4Sd) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    pSocket = SOCK_DESC_TABLE[i4Sd];
    if ((OsixSemTake (pSocket->ProtectSemId)) != OSIX_SUCCESS)
    {
        SliDestroySocket (i4Sd);
        SLI_ERR (SLI_ENOBUFS);
        return SLI_FAILURE;
    }
    i2family = pSocket->i2SdtFamily;

    /* Since this call is supported for IP_PKTINFO option
     * the following check is made */
    if (((pSocket->i2SdtSockType != SOCK_DGRAM) &&
         (pSocket->i2SdtSockType != SOCK_RAW)) ||
        (pSocket->u4IpPktinfoFlag == IP_PKTINFO_DIS))
    {
        pSocket->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        OsixSemGive (pSocket->ProtectSemId);
        return SLI_FAILURE;
    }
    if (i2family == AF_INET)
    {
        pInPktinfo = (tInPktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (pMsg));
        pInPktinfo->ipi_ifindex = pSocket->inPktinfo.ipi_ifindex;
        pInPktinfo->ipi_spec_dst = pSocket->inPktinfo.ipi_spec_dst;
        pInPktinfo->ipi_addr = pSocket->inPktinfo.ipi_addr;
        i4retval = sizeof (tInPktinfo);
    }
#ifdef IP6_WANTED
    else if (i2family == AF_INET6)
    {
        pIn6Pktinfo = (tIn6Pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (pMsg));
        pIn6Pktinfo->ipi6_ifindex = pSocket->in6Pktinfo.ipi6_ifindex;
        V6_HTONL ((tIpAddr *) (VOID *) &pIn6Pktinfo->ipi6_addr,
                  (tIpAddr *) (VOID *) &pSocket->in6Pktinfo.ipi6_addr);
        if (pSocket->i4recvhoplimit == TRUE)
        {
            pIn6Pktinfo->in6_hoplimit = pSocket->in6Pktinfo.in6_hoplimit;
        }
        else
        {
            pIn6Pktinfo->in6_hoplimit = ZERO;
        }
        i4retval = sizeof (tIn6Pktinfo);
    }
#endif
    OsixSemGive (pSocket->ProtectSemId);

    return i4retval;
}

/***************************************************************************/
/*  Function Name   : SliRawHash                                            */
/*  Description     : Adds the Socket Pointer to the Hash Table.            */
/*  Input(s)        : pRawSk -- pointer to the socket descriptor entry.     */
/*  Output(s)       : None.                                                 */
/*  Returns         : SUCCESS or Error Code.                                */
/***************************************************************************/
PRIVATE INT4
SliRawHash (INT4 i4SockDesc)
{
    UINT2               u2HashKey;
    tSliRawHashNode    *pHashNode = NULL;
    tSdtEntry          *pRawSk = SOCK_DESC_TABLE[i4SockDesc];

    u2HashKey = (UINT2) (pRawSk->i4Protocol);
    SLI_SLL_Init ((tSliRawQueue *) & pRawSk->RecvQueue);

    if ((SLI_RAWHP_ENTER_CS ()) != OSIX_SUCCESS)
        return SLI_FAILURE;

    if ((pHashNode = (tSliRawHashNode *) SliAllocMemFromPool (HashMemPoolId)) ==
        NULL)
    {
        SLI_RAWHP_EXIT_CS ();
        return SLI_FAILURE;
    }

    pHashNode->i4SockDesc = i4SockDesc;
    pHashNode->pRawSk = pRawSk;

    /*
     * The Linked list is shared by all the application which are 
     * listening for the same protocol packets.
     */

    if ((SLI_RAWHL_ENTER_CS ()) != OSIX_SUCCESS)
    {
        SliFreeMemToPool (HashMemPoolId, pHashNode);
        SLI_RAWHP_EXIT_CS ();
        return SLI_FAILURE;
    }

    if (u2HashKey >= IPPROTO_MAX)
    {
        SliFreeMemToPool (HashMemPoolId, pHashNode);
        SLI_RAWHP_EXIT_CS ();
        return SLI_FAILURE;
    }

    SLI_SLL_Add (&gSliRawHashTable[u2HashKey], &pHashNode->NextNodeInList);
    SLI_RAWHL_EXIT_CS ();
    SLI_RAWHP_EXIT_CS ();
    return SLI_SUCCESS;
}

/***************************************************************************/
/*  Function Name   : SliRawUnHash                                         */
/*  Description     : Removes the Socket Pointer from the Hash Table.      */
/*  Input(s)        : pRawSk -- pointer to the socket descriptor entry.    */
/*  Output(s)       : None.                                                */
/*  Returns         : None.                                                */
/***************************************************************************/
PRIVATE INT4
SliRawUnHash (INT4 i4SockDesc)
{
    UINT2               u2HashKey = ZERO;
    tSliRawHashNode    *pHashNode = NULL;
    tSdtEntry          *pRawSk = SOCK_DESC_TABLE[i4SockDesc];

    if ((pRawSk->i4Protocol < IPPROTO_IP)
        || (pRawSk->i4Protocol >= IPPROTO_MAX))
    {
        return SLI_FAILURE;
    }

    u2HashKey = (UINT2) (pRawSk->i4Protocol);

    if ((SLI_RAWHL_ENTER_CS ()) != SLI_SUCCESS)
    {
        return SLI_FAILURE;
    }

    SLI_SLL_Scan (&gSliRawHashTable[u2HashKey], pHashNode, tSliRawHashNode *)
    {
        if (pHashNode->pRawSk == pRawSk)
        {
            SliDeleteHashNode (u2HashKey, pHashNode);
            break;
        }
    }

    SLI_RAWHL_EXIT_CS ();
    return SLI_SUCCESS;
}

/****************************************************************************/
/*  Function Name   : SliRawIpv4HashLookupInCxt                             */
/*  Description     : Searches for a matching RAW Socket.                   */
/*  Input(s)        : pHashNode, u1HashKey, u4SrcAddr, u4DestAddr.          */
/*  Output(s)       : None.                                                 */
/*  Returns         : pointer to the Hash node which has the matching sock  */
/****************************************************************************/
PRIVATE tSliRawHashNode *
SliRawIpv4HashLookupInCxt (UINT4 u4ContextId,
                           UINT2 u2HashKey,
                           tSliRawHashNode * pHashNode,
                           UINT4 u4SrcAddr, UINT4 u4DestAddr)
{
    tSliRawHashNode    *pTmpNode = NULL;

    /* As the HL semaphore is released and retaken after SliEnqueueRawPkt,
     * the socket corresponding to pHashNode could have been deleted due
     * a race condition and the pHashNode can be removed from the
     * gSliRawHashTable. Hence, start from the start of the list */
    if ((pHashNode == NULL) || (pHashNode->pRawSk == NULL))
    {
        pHashNode =
            (tSliRawHashNode *) SLI_SLL_First (&gSliRawHashTable[u2HashKey]);
    }
    else
    {
        pTmpNode =
            (tSliRawHashNode *)
            SLI_SLL_Next (&gSliRawHashTable[u2HashKey],
                          &pHashNode->NextNodeInList);
        pHashNode = pTmpNode;
    }

    while (pHashNode != NULL)
    {
        if ((pHashNode->pRawSk != NULL)
            && (pHashNode->pRawSk->i2SdtFamily == AF_INET)
            &&
            ((V4_FROM_V6_ADDR (pHashNode->pRawSk->RemoteIpAddr) == INADDR_ANY)
             || (V4_FROM_V6_ADDR (pHashNode->pRawSk->RemoteIpAddr) ==
                 u4SrcAddr))
            && ((V4_FROM_V6_ADDR (pHashNode->pRawSk->RecvAddr) == INADDR_ANY)
                || (V4_FROM_V6_ADDR (pHashNode->pRawSk->RecvAddr) ==
                    u4DestAddr)))
        {
            if (pHashNode->pRawSk->u1FdMode == SOCK_UNIQUE_MODE)
            {
                if (pHashNode->pRawSk->u4RxContextId == u4ContextId)
                    return pHashNode;
            }
            else
                return pHashNode;
        }
        else
        {
            pTmpNode =
                (tSliRawHashNode *) SLI_SLL_Next (&gSliRawHashTable[u2HashKey],
                                                  &pHashNode->NextNodeInList);
        }
        pHashNode = pTmpNode;
    }
    return (tSliRawHashNode *) NULL;
}

/****************************************************************************/
/*  Function Name   : SliRawIpv6HashLookupInCxt                             */
/*  Description     : Searches for a matching RAW Socket.                   */
/*  Input(s)        : pHashNode, u1HashKey, SrcIpAddr, DestIpAddr.          */
/*  Output(s)       : None.                                                 */
/*  Returns         : pointer to the Hash node which has the matching sock  */
/****************************************************************************/
PRIVATE tSliRawHashNode *
SliRawIpv6HashLookupInCxt (UINT1 u4ContextId, UINT2 u2HashKey,
                           tSliRawHashNode * pHashNode,
                           tIpAddr SrcIpAddr, tIpAddr DestIpAddr)
{
    tSliRawHashNode    *pTmpNode = NULL;

    /* As the HL semaphore is released and retaken after SliEnqueueRawPkt,
     * the socket corresponding to pHashNode could have been deleted due
     * a race condition and the pHashNode can be removed from the
     * gSliRawHashTable. Hence, start from the start of the list */
    if ((pHashNode == NULL) || (pHashNode->pRawSk == NULL))
    {
        pHashNode =
            (tSliRawHashNode *) SLI_SLL_First (&gSliRawHashTable[u2HashKey]);
    }
    else
    {
        pTmpNode =
            (tSliRawHashNode *)
            SLI_SLL_Next (&gSliRawHashTable[u2HashKey],
                          &pHashNode->NextNodeInList);
        pHashNode = pTmpNode;
    }

    while (pHashNode != NULL)
    {
        if ((pHashNode->pRawSk != NULL)
            && (pHashNode->pRawSk->i2SdtFamily == AF_INET6)
            && ((IS_ADDR_IN6ADDRANYINIT (pHashNode->pRawSk->RemoteIpAddr))
                ||
                (!V6_ADDR_CMP (&pHashNode->pRawSk->RemoteIpAddr, &SrcIpAddr)))
            && ((IS_ADDR_IN6ADDRANYINIT (pHashNode->pRawSk->RecvAddr))
                || (!V6_ADDR_CMP (&pHashNode->pRawSk->RecvAddr, &DestIpAddr))))
        {
            if (pHashNode->pRawSk->u1FdMode == SOCK_UNIQUE_MODE)
            {
                if (pHashNode->pRawSk->u4RxContextId == u4ContextId)
                    return pHashNode;
            }
            else
                return pHashNode;

            return pHashNode;
        }
        else
        {
            pTmpNode =
                (tSliRawHashNode *) SLI_SLL_Next (&gSliRawHashTable[u2HashKey],
                                                  &pHashNode->NextNodeInList);
        }
        pHashNode = pTmpNode;
    }
    return (tSliRawHashNode *) NULL;
}

/****************************************************************************/
/* Function Name   : SliEnqueueRawPktInCxt                                  */
/* Description     : Enqueues the Pkt to the socket.                        */
/* Input(s)        : u4ContextId, u1HashKey, pHashNode, pBuf.                */
/* Output(s)       : None.                                                  */
/* Returns         : SLI_SUCCESS or SLI_FAILURE.                            */
/****************************************************************************/
PRIVATE INT4
SliEnqueueRawPktInCxt (UINT4 u4ContextId, tSliRawHashNode * pHashNode,
                       tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IpHdrLen)
{
    tSliRawRecvQnode   *pRawQnode = NULL;

    if ((pRawQnode = (tSliRawRecvQnode *) SliAllocMemFromPool (QueueMemPoolId))
        == NULL)
    {
        return SLI_FAILURE;
    }
    else
    {
        /* Include ContextId also in the RAW hash node */
        pRawQnode->pRawPkt = pBuf;
        pRawQnode->u4ContextId = u4ContextId;
        pRawQnode->u2IpHdrLen = u2IpHdrLen;
        if (pHashNode->pRawSk == NULL)
        {
            /* As the RAW_HL semaphore is released before this function,
             * due to race condition, the raw socket pointer corresponding
             * to this hash node can be deleted.
             * So, do not enque the packet and return failure */
            SLI_ERR (SLI_ENOTSOCK);
            SliFreeMemToPool (QueueMemPoolId, pRawQnode);
            return SLI_FAILURE;
        }
        if (OsixSemTake (pHashNode->pRawSk->ProtectSemId) == OSIX_SUCCESS)
        {
            SLI_SLL_Add (&(pHashNode->pRawSk->RecvQueue),
                         &pRawQnode->NextNodeInQueue);
            OsixSemGive (pHashNode->pRawSk->ProtectSemId);
        }
        else
        {
            SLI_ERR (SLI_ENOBUFS);
            SliFreeMemToPool (QueueMemPoolId, pRawQnode);
            return SLI_FAILURE;
        }
        OsixSemGive (pHashNode->pRawSk->ConnSemId);
        SliSelectScanList (pHashNode->i4SockDesc, SELECT_READ);
        return SLI_SUCCESS;
    }
}

/****************************************************************************/
/* Function Name   : SliEnqueueIpv4RawPacketInCxt                           */
/* Description     : Enqueues the Raw Packet to the Socket.                 */
/* Input(s)        : u4contextId, u1Protocol, u4DataLen, u4SrcAddr,         */
/*                   u4DestAddr, pRecvBuf                                   */
/* Output(s)       : None.                                                  */
/* Returns         : SUCCESS or FAILURE.                                    */
/****************************************************************************/
INT4
SliEnqueueIpv4RawPacketInCxt (UINT4 u4ContextId, UINT1 u1Protocol,
                              UINT4 u4DataLen, UINT4 u4SrcAddr,
                              UINT4 u4DestAddr,
                              tCRU_BUF_CHAIN_HEADER * pRecvBuf)
{
    UINT2               u2HashKey = ZERO;
    tSliRawHashNode    *pNextHashNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBufClone = NULL;
    INT4                i4RetVal = SLI_SUCCESS;

    u2HashKey = (UINT2) u1Protocol;

    if ((SLI_RAWHL_ENTER_CS ()) != SLI_SUCCESS)
    {
        return SLI_FAILURE;
    }

    if (SliRawIpv4HashLookupInCxt
        (u4ContextId, u2HashKey, NULL, u4SrcAddr, u4DestAddr))
    {
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4DataLen, SLI_ZERO)))
        {
            /* Copy the Higher layer protocol header */
            if ((SliCruBufCopyBufChains
                 (pBuf, pRecvBuf, SLI_ZERO, SLI_ZERO,
                  u4DataLen)) == SLI_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                SLI_RAWHL_EXIT_CS ();
                return SLI_FAILURE;
            }
        }
        else
        {
            SLI_RAWHL_EXIT_CS ();
            return SLI_FAILURE;
        }
    }
    else
    {
        SLI_RAWHL_EXIT_CS ();
        return SLI_FAILURE;
    }

    MEMCPY (IP_GET_MODULE_DATA_PTR (pBuf),
            ((tIpParms *) IP_GET_MODULE_DATA_PTR
             (pRecvBuf)), sizeof (tIpParms));

    pNextHashNode =
        SliRawIpv4HashLookupInCxt (u4ContextId, u2HashKey, NULL, u4SrcAddr,
                                   u4DestAddr);
    while (pNextHashNode != NULL)
    {
        pBufClone = CRU_BUF_Duplicate_BufChain (pBuf);
        if (pBufClone != NULL)
        {
            SLI_RAWHL_EXIT_CS ();
            i4RetVal =
                SliEnqueueRawPktInCxt (u4ContextId, pNextHashNode, pBufClone,
                                       (UINT2) u4DataLen);
            if (i4RetVal == SLI_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain (pBufClone, FALSE);
            }
            if ((SLI_RAWHL_ENTER_CS ()) != SLI_SUCCESS)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return SLI_FAILURE;
            }

            pNextHashNode =
                SliRawIpv4HashLookupInCxt (u4ContextId, u2HashKey,
                                           pNextHashNode, u4SrcAddr,
                                           u4DestAddr);
        }
        else
        {
            break;
        }
    }

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    SLI_RAWHL_EXIT_CS ();
    return SLI_SUCCESS;
}

/****************************************************************************/
/* Function Name   : SliEnqueueIpv6RawPacketInCxt                           */
/* Description     : Enqueues the Raw Packet to the Socket.                 */
/* Input(s)        : u4ContextId, u1Protocol, u4DataLen,                    */
/*                   u4ReadOffSet,pIp6Hdr,pRecvBuf                          */
/* Output(s)       : None.                                                  */
/* Returns         : SUCCESS or FAILURE.                                    */
/****************************************************************************/
INT4
SliEnqueueIpv6RawPacketInCxt (UINT4 u4ContextId, UINT1 u1Protocol,
                              UINT4 u4DataLen, UINT4 u4ReadOffSet,
                              UINT1 *pIp6Hdr, tCRU_BUF_CHAIN_HEADER * pRecvBuf)
{
    INT4                i4RetVal = ZERO;
    UINT2               u2HashKey = ZERO;
    tSliRawHashNode    *pNextHashNode = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pBufClone = NULL;
    tIpAddr             SrcIpAddr;
    tIpAddr             DestIpAddr;

    V6_HTONL (&SrcIpAddr,
              (tIpAddr *) (VOID *) &pIp6Hdr[IP6_OFFSET_FOR_SRCADDR_FIELD]);

    V6_HTONL (&DestIpAddr,
              (tIpAddr *) (VOID *) &pIp6Hdr[IP6_OFFSET_FOR_DESTADDR_FIELD]);

    if (CRU_BUF_Get_ChainValidByteCount (pRecvBuf) > SLI_MAX_PKT_LEN)
    {
        /*Ensure  Size of the incoming buf including fragment should not > SLI_MAX_PKT_LEN.
         * Note: Inorder to rcv more than 1500 MTU. increase SLI_MAX_PKT_LEN as per need*/
        return SLI_FAILURE;
    }

    u2HashKey = (UINT2) u1Protocol;

    if ((SLI_RAWHL_ENTER_CS ()) != SLI_SUCCESS)
    {
        return SLI_FAILURE;
    }

    if (SliRawIpv6HashLookupInCxt
        ((UINT1) u4ContextId, u2HashKey, NULL, SrcIpAddr, DestIpAddr))
    {
        if ((pBuf = CRU_BUF_Allocate_MsgBufChain (u4DataLen, SLI_ZERO)))
        {
            /* Copy the Higher layer protocol header */
            if ((SliCruBufCopyBufChains
                 (pBuf, pRecvBuf, SLI_ZERO, u4ReadOffSet,
                  u4DataLen)) == SLI_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                SLI_RAWHL_EXIT_CS ();
                return SLI_FAILURE;
            }
            else
            {
                if ((CRU_BUF_Prepend_BufChain
                     (pBuf, pIp6Hdr, IPV6_HEADER_LEN)) == CRU_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    SLI_RAWHL_EXIT_CS ();
                    return SLI_FAILURE;
                }
            }
        }
    }
    else
    {
        SLI_RAWHL_EXIT_CS ();
        return SLI_SUCCESS;
    }

    if (pBuf == NULL)
    {
        SLI_RAWHL_EXIT_CS ();
        return SLI_FAILURE;
    }
    /* tSliRawRecvQnode structure is used for RAW and 
     * RAW6 messages */
    MEMCPY (SliRawIp6GetParms (pBuf),
            ((tSliRawLlToIp6Params *) SliRawIp6GetParms (pRecvBuf)),
            sizeof (tSliRawLlToIp6Params));
    pNextHashNode =
        SliRawIpv6HashLookupInCxt ((UINT1) u4ContextId, u2HashKey, NULL,
                                   SrcIpAddr, DestIpAddr);
    while (pNextHashNode != NULL)
    {
        pBufClone = CRU_BUF_Duplicate_BufChain (pBuf);
        if (pBufClone != NULL)
        {
            SLI_RAWHL_EXIT_CS ();
            i4RetVal =
                SliEnqueueRawPktInCxt (u4ContextId, pNextHashNode, pBufClone,
                                       (UINT2) (u4DataLen + IPV6_HEADER_LEN));
            if (i4RetVal == SLI_FAILURE)
                CRU_BUF_Release_MsgBufChain (pBufClone, FALSE);

            if ((SLI_RAWHL_ENTER_CS ()) != SLI_SUCCESS)
            {
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return SLI_FAILURE;
            }

            pNextHashNode =
                SliRawIpv6HashLookupInCxt ((UINT1) u4ContextId, u2HashKey,
                                           pNextHashNode, SrcIpAddr,
                                           DestIpAddr);
        }
        else
        {
            break;
        }
    }

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    SLI_RAWHL_EXIT_CS ();
    return SLI_SUCCESS;
}

/****************************************************************************/
/* Function Name   : SliDeleteHashNode                                      */
/* Description     : Unlinks the node, Frees memory.                        */
/* Input(s)        : u1HashKey - Key to hash table.                         */
/*                   pHashNode - Node to delete from the hash table         */
/* Output(s)       : None.                                                  */
/* Returns         : None.                                                  */
/****************************************************************************/
static VOID
SliDeleteHashNode (UINT2 u2HashKey, tSliRawHashNode * pHashNode)
{
    tSdtEntry          *pRawSk = NULL;
    tSliRawRecvQnode   *pRawQnode = NULL;

    pRawSk = pHashNode->pRawSk;

    while (SLI_SLL_Get_Count ((tSliRawQueue *) & pRawSk->RecvQueue) > ZERO)
    {
        pRawQnode =
            (tSliRawRecvQnode *) SLI_SLL_First ((tSliRawQueue *) &
                                                pRawSk->RecvQueue);
        SLI_Release_Buffer (pRawQnode->pRawPkt, FALSE);
        SLI_SLL_Delete (&pRawSk->RecvQueue, &pRawQnode->NextNodeInQueue);
        SliFreeMemToPool (QueueMemPoolId, pRawQnode);
    }

    /* Unlink Hash Node from table */
    SLI_SLL_Delete (&gSliRawHashTable[u2HashKey], &pHashNode->NextNodeInList);

    /* Free Hash Node */
    SliFreeMemToPool (HashMemPoolId, pHashNode);
}

/****************************************************************************/
/* Function Name   : SliHandleChangeInPmtuInCxt                             */
/* Description     : Called by IP when PMTU changes.                        */
/* Input(s)        : u4ContextId - Context Identifier                       */
/*                   u4DestAddr - Ip address for which Path MTU changed     */
/*                   u1Tos - Type of service                                */
/* Output(s)       : u2Pmtu - New MTU value.                                */
/* Returns         : IP_SUCCESS                                             */
/****************************************************************************/

INT4
SliHandleChangeInPmtuInCxt (UINT4 u4ContextId, UINT4 u4DestAddr,
                            UINT1 u1Tos, UINT2 u2Pmtu)
{
    INT4                i4SdIndex = ZERO;
    tSdtEntry          *pSocket = NULL;

    for (i4SdIndex = SLI_FIRST_SOCKET_FD; i4SdIndex < (INT4) MAX_NO_OF_SOCKETS;
         i4SdIndex++)
    {
        pSocket = SOCK_DESC_TABLE[i4SdIndex];
        if ((pSocket != NULL) && (pSocket->state == SS_CONNECTED) &&
            (V4_FROM_V6_ADDR (pSocket->RemoteIpAddr) == u4DestAddr) &&
            (pSocket->i4IpTos == u1Tos) &&
            (pSocket->fpSliHandlePmtuChange != NULL) &&
            (pSocket->u4RxContextId == u4ContextId))
        {
            pSocket->fpSliHandlePmtuChange (u4ContextId, i4SdIndex, u2Pmtu);
        }
    }
    return SLI_SUCCESS;
}

/****************************************************************************/
/* Function Name   : SliHandleChangeInIp6PmtuInCxt                          */
/* Description     : Called by IP6 when PMTU changes.                       */
/* Input(s)        : IpDestAddr - Ip6 address for which Path MTU changed    */
/* Output(s)       : u4Pmtu - New MTU value.                                */
/* Returns         : IP_SUCCESS                                             */
/****************************************************************************/

INT4
SliHandleChangeInIp6PmtuInCxt (UINT4 u4ContextId, tIp6Addr IpDestAddr,
                               UINT4 u4Pmtu)
{
    INT4                i4SdIndex = ZERO;
    tSdtEntry          *pSocket = NULL;
    tIpAddr             IpTempAddr;

    V6_NTOHL (&IpTempAddr, (tIpAddr *) & IpDestAddr);

    for (i4SdIndex = SLI_FIRST_SOCKET_FD; i4SdIndex < (INT4) MAX_NO_OF_SOCKETS;
         i4SdIndex++)
    {
        pSocket = SOCK_DESC_TABLE[i4SdIndex];
        if ((pSocket != NULL) && (pSocket->state == SS_CONNECTED) &&
            ((V6_ADDR_CMP (&(pSocket->RemoteIpAddr), &IpTempAddr)) == 0) &&
            (pSocket->u4RxContextId == u4ContextId) &&
            (pSocket->fpSliHandlePmtuChange != NULL))
        {
            pSocket->fpSliHandlePmtuChange (u4ContextId, i4SdIndex,
                                            (UINT2) u4Pmtu);
        }
    }
    return SLI_SUCCESS;
}

/****************************************************************************/
/* Function Name   : SliRegisterPmtuHandler                                 */
/* Description     : Registers PMTU call back function pointer              */
/* Input(s)        : i4Sd - Socket Descriptor                               */
/*                   fpPmtuHandler - Function to be register                */
/* Output(s)       : None.                                                  */
/* Returns         : SUCCESS or error code.                                 */
/****************************************************************************/

INT4
SliRegisterPmtuHandler (INT4 i4Sd, void (*fpPmtuHandler) (UINT4, INT4, UINT2))
{
    tSdtEntry          *pSocket = NULL;

    if (SliLookup (i4Sd) == FALSE)
    {
        SLI_ERR (SLI_ENOTSOCK);
        return SLI_FAILURE;
    }

    pSocket = SOCK_DESC_TABLE[i4Sd];
    if ((fpPmtuHandler != NULL) && (pSocket->i4PmtuDfFlag != IPPMTUDISC_DONT))
    {
        pSocket->fpSliHandlePmtuChange = fpPmtuHandler;
        return SLI_SUCCESS;
    }
    else
    {
        pSocket->i1ErrorCode = SLI_EINVAL;
        SLI_ERR (SLI_EINVAL);
        return SLI_FAILURE;
    }
}

/****************************************************************************/
/* Function Name   : SliIsIpAddrLocalInCxt                                  */
/* Description     : Checks if the given address belongs to the local net   */
/*                   in this particular context                             */
/* Input(s)        : u4ContextId - Context Identifier                       */
/*                   ipaddr - address                                       */
/* Output(s)       : u4IfIndex -interface to which the address belongs      */
/* Returns         : SUCCESS /FAILURE.                                      */
/****************************************************************************/

INT4
SliIsIpAddrLocalInCxt (UINT4 u4ContextId, tIpAddr ipaddr, UINT4 *pu4IfIndex)
{
    if (IN6_IS_ADDR_V4MAPPED (ipaddr))
    {
#ifdef IP_WANTED
        return (IpIsLocalAddrInCxt
                (u4ContextId, V4_FROM_V6_ADDR (ipaddr), (UINT2 *) pu4IfIndex));
#else
        UNUSED_PARAM (pu4IfIndex);
        return SLI_FAILURE;
#endif
    }
    else
    {
#ifdef IP6_WANTED
        return (NetIpv6IsOurAddressInCxt
                (u4ContextId, (tIp6Addr *) & ipaddr, pu4IfIndex));
#else
        UNUSED_PARAM (pu4IfIndex);
        return SLI_FAILURE;
#endif
    }

}

/****************************************************************************/
/* Function Name   : V6_NTOHL                                               */
/* Description     : Converts a given v6 address to network order.          */
/* Input(s)        : ipaddr in host order.                                  */
/* Output(s)       : NONE.                                                  */
/* Returns         : ipaddr in network order.                               */
/****************************************************************************/
tIpAddr            *
V6_NTOHL (tIpAddr * pDest, const tIpAddr * pSrc)
{
    UINT2               u2Count = SLI_ZERO;

    if (IN6_IS_ADDR_V4MAPPED (*pSrc))
    {
        V4_MAPPED_ADDR_COPY (pDest, (OSIX_NTOHL (pSrc->u4_addr[SLI_THREE])));
    }
    else
    {
        for (u2Count = SLI_ZERO; u2Count < SLI_FOUR; u2Count++)
        {
            pDest->u4_addr[u2Count] = OSIX_NTOHL (pSrc->u4_addr[u2Count]);
        }
    }
    return pDest;
}

/****************************************************************************/
/* Function Name   : V6_HTONL                                               */
/* Description     : Converts a given v6 address to host order.             */
/* Input(s)        : ipaddr in network order.                               */
/* Output(s)       : NONE.                                                  */
/* Returns         : ipaddr in host order.                                  */
/****************************************************************************/

tIpAddr            *
V6_HTONL (tIpAddr * pDest, const tIpAddr * pSrc)
{
    UINT2               u2Count = SLI_ZERO;

    if (IN6_IS_ADDR_V4MAPPED (*pSrc))
    {
        V4_MAPPED_ADDR_COPY (pDest, (OSIX_HTONL (pSrc->u4_addr[SLI_THREE])));
    }
    else
    {
        for (u2Count = SLI_ZERO; u2Count < SLI_FOUR; u2Count++)
        {
            pDest->u4_addr[u2Count] = OSIX_HTONL (pSrc->u4_addr[u2Count]);
        }
    }
    return pDest;
}

#ifdef IP6_WANTED
/****************************************************************************/
/* Function Name   : SliIfNametoIndex                                       */
/* Description     : This fn. maps an interface name into its corresponding */
/*                 : index.                                                 */
/* Input(s)        : interface name                                         */
/* Output(s)       : NONE.                                                  */
/* Returns         : Corresponding index                                    */
/****************************************************************************/

INT4
SliIfNametoIndex (UINT1 *u1ifname)
{
    tNetIpv6IfInfo      NetIpv6IfInfo;

    UINT4               u4IfIndex;

    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        return SLI_FAILURE;
    }

    for (;;)
    {
        u4IfIndex = NetIpv6IfInfo.u4IfIndex;

        /* Check whether the If name matches with the given name */
        if (MEMCMP (NetIpv6IfInfo.au1IfName, u1ifname, IP6_MAX_IF_NAME_LEN) ==
            0)
        {
            return (UINT2) u4IfIndex;
        }

        /* Get the next interface information. */
        MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
        if (NetIpv6GetNextIfInfo (u4IfIndex, &NetIpv6IfInfo) == NETIPV6_FAILURE)
        {
            return SLI_FAILURE;
        }
    }
}

/****************************************************************************/
/* Function Name   : SliIfIndextoName                                       */
/* Description     : This fn. maps an interface index to its corresponding  */
/*                 : name.                                                  */
/* Input(s)        : Interface index.                                       */
/* Output(s)       : NONE.                                                  */
/* Returns         : Corresponding interface name.                          */
/****************************************************************************/

UINT1              *
SliIfIndextoName (UINT2 u2ifindex, UINT1 *u1ifname)
{
    tNetIpv6IfInfo      NetIpv6IfInfo;

    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    if (NetIpv6GetIfInfo ((UINT4) u2ifindex, &NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        SLI_ERR (SLI_ENXIO);
        return NULL;
    }

    MEMCPY (u1ifname, NetIpv6IfInfo.au1IfName,
            sizeof (NetIpv6IfInfo.au1IfName));
    return (u1ifname);
}

#endif

/****************************************************************************/
/* Function Name   : SliInetPton                                            */
/* Description     :                                                        */
/* Input(s)        :                                                        */
/* Output(s)       : NONE.                                                  */
/* Returns         : NONE.                                                  */
/****************************************************************************/

INT4
SliInetPton (INT4 af, const UINT1 *src, VOID *dst)
{
    switch (af)
    {
#ifdef IP_WANTED
        case AF_INET:
            return (SliInetPton4 (src, dst));
#endif
#ifdef IP6_WANTED
        case AF_INET6:
            return (SliInetPton6 (src, dst));
#endif
        default:
        {
            UNUSED_PARAM (src);
            UNUSED_PARAM (dst);

            SLI_ERR (SLI_EFAMNOSUPP);
            return (SLI_FAILURE);
        }
    }
}

#ifdef IP_WANTED
/****************************************************************************/
/* Function Name   : SliInetPton4                                           */
/* Description     :                                                        */
/* Input(s)        :                                                        */
/* Output(s)       : NONE.                                                  */
/* Returns         : NONE.                                                  */
/****************************************************************************/
PRIVATE UINT2
SliInetPton4 (const UINT1 *src, UINT1 *dst)
{
    static const UINT1  au1digits[] = SLI_V4_DIGITS;
    UINT2               u2sawdigit = ZERO, u2octets = ZERO, u2ch = ZERO;
    UINT1               au1tmp[SLI_INADDRSZ], *tp = NULL;

    u2sawdigit = SLI_ZERO;
    u2octets = SLI_ZERO;
    *(tp = au1tmp) = SLI_ZERO;
    while ((u2ch = *src++) != SLI_NULL_CHAR)
    {
        const UINT1        *pch = NULL;

        if ((pch = (const UINT1 *) STRCHR (au1digits, u2ch)) != NULL)
        {
            UINT2               u2new = (UINT2)
                (*tp * SLI_MULTIPLY_FACTOR + (pch - au1digits));

            if (u2new > SLI_MAX_UNIT1)
            {
                return (SLI_ZERO);
            }
            *tp = (UINT1) u2new;
            if (!u2sawdigit)
            {
                if (++u2octets > SLI_V4_ADDR_LEN)
                {
                    return (SLI_ZERO);
                }
                u2sawdigit = SLI_ONE;
            }
        }
        else if ((u2ch == SLI_DOT_CHAR) && u2sawdigit)
        {
            if (u2octets == SLI_V4_ADDR_LEN)
            {
                return (SLI_ZERO);
            }
            *++tp = SLI_ZERO;
            u2sawdigit = SLI_ZERO;
        }
        else
            return (SLI_ZERO);
    }
    if (u2octets < SLI_V4_ADDR_LEN)
    {
        return (SLI_ZERO);
    }

    MEMCPY (dst, au1tmp, SLI_INADDRSZ);
    return (SLI_ONE);
}
#endif

/****************************************************************************/
/* Function Name   : SliInetPton6                                           */
/* Description     :                                                        */
/* Input(s)        :                                                        */
/* Output(s)       : NONE.                                                  */
/* Returns         : NONE.                                                  */
/****************************************************************************/
#ifdef IP6_WANTED
PRIVATE UINT2
SliInetPton6 (const UINT1 *src, UINT1 *dst)
{
    static const UINT1  u1xdigits_l[] = SLI_V6_DIGIT_LOW,
        u1xdigits_u[] = SLI_V6_DIGIT_UP;
    UINT1               tmp[SLI_IN6ADDRSZ], *tp, *endp, *colonp;
    const UINT1        *u1xdigits, *u1curtok;
    UINT2               u2ch, u2sawxdigit;
    UINT2               u2val;

    MEMSET ((tp = tmp), SLI_NULL_CHAR, SLI_IN6ADDRSZ);
    endp = tp + SLI_IN6ADDRSZ;
    colonp = NULL;
    /* Leading :: requires some special handling. */
    if (*src == SLI_COLON_CHAR)
    {
        if (*++src != SLI_COLON_CHAR)
        {
            return (SLI_ZERO);
        }
    }
    u1curtok = src;
    u2sawxdigit = SLI_ZERO;
    u2val = SLI_ZERO;
    while ((u2ch = *src++) != SLI_NULL_CHAR)
    {
        const UINT1        *pch;

        if ((pch =
             (const UINT1 *) STRCHR ((u1xdigits = u1xdigits_l), u2ch)) == NULL)
        {
            pch = (const UINT1 *) STRCHR ((u1xdigits = u1xdigits_u), u2ch);
        }
        if (pch != NULL)
        {
            u2val <<= SLI_V4_ADDR_LEN;
            u2val |= (pch - u1xdigits);
            u2sawxdigit = SLI_ONE;
            continue;
        }
        if (u2ch == SLI_COLON_CHAR)
        {
            u1curtok = src;
            if (!u2sawxdigit)
            {
                if (colonp)
                {
                    return (SLI_ZERO);
                }
                colonp = tp;
                continue;
            }
            else if (*src == SLI_NULL_CHAR)
            {
                return (SLI_ZERO);
            }
            if (tp + SLI_INT16SZ > endp)
            {
                return (SLI_ZERO);
            }
            *tp++ = (UINT1) (u2val >> SLI_MAX_UINT1_BITS) & SLI_MAX_UNIT1;
            *tp++ = (UINT1) u2val & SLI_MAX_UNIT1;
            u2sawxdigit = SLI_ZERO;
            u2val = SLI_ZERO;
            continue;
        }
        if ((u2ch == SLI_DOT_CHAR) && (((tp + SLI_INADDRSZ) <= endp))
#ifdef IP_WANTED
            && (SliInetPton4 (u1curtok, tp) > SLI_ZERO)
#endif
            )
        {
            tp += SLI_INADDRSZ;
            u2sawxdigit = SLI_ZERO;
            break;                /* '\0' was seen by inet_pton4(). */
        }
        return (SLI_ZERO);
    }
    if (u2sawxdigit)
    {
        if (tp + SLI_INT16SZ > endp)
        {
            return (SLI_ZERO);
        }
        *tp++ = (UINT1) (u2val >> SLI_MAX_UINT1_BITS) & SLI_MAX_UNIT1;
        *tp++ = (UINT1) u2val & SLI_MAX_UNIT1;
    }
    if (colonp != NULL)
    {
        /*
         * Since some memmove()'s erroneously fail to handle
         * overlapping regions, we'll do the shift by hand.
         */
        const UINT2         n = (UINT2) (tp - colonp);
        UINT2               i;

        if (tp == endp)
        {
            return (SLI_ZERO);
        }
        for (i = SLI_ONE; i <= n; i++)
        {
            endp[-i] = colonp[n - i];
            colonp[n - i] = SLI_ZERO;
        }
        tp = endp;
    }
    if (tp != endp)
    {
        return (SLI_ZERO);
    }
    MEMCPY (dst, tmp, SLI_IN6ADDRSZ);
    return (SLI_ONE);
}
#endif

/****************************************************************************/
/* Function Name   : SliInetNtop                                            */
/* Description     : convert a network format address to presentation       */
/*                   format                                                 */
/* Input(s)        :                                                        */
/* Output(s)       :                                                        */
/* Returns         : NULL/ Pointer to the presentation format string        */
/****************************************************************************/
const UINT1        *
SliInetNtop (INT4 i4Family, const VOID *pSrc, UINT1 *pu1Dst, INT4 i4Size)
{
    switch (i4Family)
    {
#ifdef IP_WANTED
        case AF_INET:
            return (SliInetNtop4 (pSrc, pu1Dst, i4Size));
#endif
#ifdef IP6_WANTED
        case AF_INET6:
            return (SliInetNtop6 (pSrc, pu1Dst, i4Size));
#endif
        default:
        {
            UNUSED_PARAM (pSrc);
            UNUSED_PARAM (pu1Dst);
            UNUSED_PARAM (i4Size);

            SLI_ERR (SLI_EFAMNOSUPP);
            return (NULL);
        }
    }
}

#ifdef IP_WANTED
/****************************************************************************/
/* Function Name   : SliInetNtop4                                           */
/* Description     : convert IPv4 binary address into presentation format   */
/* Input(s)        :                                                        */
/* Output(s)       : NONE.                                                  */
/* Returns         : NONE.                                                  */
/****************************************************************************/
PRIVATE const UINT1 *
SliInetNtop4 (const UINT1 *pu1Src, UINT1 *pu1Dst, INT4 i4Size)
{
    const char         *pu1AddrFmt = "%u.%u.%u.%u";
    UINT1               au1Temp[sizeof SLI_ALL_BCAST_ADDR];

    if ((!pu1Src) || (!pu1Dst))
    {
        SLI_ERR (SLI_EINVAL) return (NULL);
    }

    if ((INT4)
        SPRINTF ((char *) au1Temp, pu1AddrFmt, pu1Src[SLI_ZERO],
                 pu1Src[SLI_ONE], pu1Src[SLI_TWO], pu1Src[SLI_THREE]) >= i4Size)
    {
        SLI_ERR (SLI_EINVAL) return (NULL);
    }
    STRNCPY (pu1Dst, au1Temp, STRLEN (au1Temp));
    pu1Dst[STRLEN (au1Temp)] = '\0';
    return (pu1Dst);
}
#endif

/****************************************************************************/
/* Function Name   : SliInetNtop6                                           */
/* Description     : convert IPv6 binary address into presentation format   */
/* Input(s)        :                                                        */
/* Output(s)       : NONE.                                                  */
/* Returns         : NONE.                                                  */
/****************************************************************************/
#ifdef IP6_WANTED
PRIVATE const UINT1 *
SliInetNtop6 (const UINT1 *pu1Src, UINT1 *pu1Dst, INT4 i4Size)
{
    UINT1               u1Temp[sizeof
                               "ffff:ffff:ffff:ffff:ffff:ffff:255.255.255.255"];
    UINT1              *pu1Tp = NULL;
    INT4                i4BestBase = SLI_FAILURE, i4BestLen = SLI_FAILURE;
    INT4                i4CurBase = SLI_FAILURE, i4CurLen = SLI_FAILURE;
    INT4                i4AddrOff = ZERO;
    UINT2               au2Words[NS_IN6ADDRSZ / NS_INT16SZ];

    if ((!pu1Src) || (!pu1Dst))
    {
        SLI_ERR (SLI_EINVAL) return (NULL);
    }

    MEMSET (au2Words, SLI_NULL_CHAR, sizeof (au2Words));
    for (i4AddrOff = SLI_ZERO; i4AddrOff < NS_IN6ADDRSZ; i4AddrOff++)
    {
        au2Words[i4AddrOff / SLI_TWO] |=
            (pu1Src[i4AddrOff] <<
             ((SLI_ONE - (i4AddrOff % SLI_TWO)) << SLI_THREE));
    }
    i4BestBase = SLI_FAILURE;
    i4CurBase = SLI_FAILURE;
    for (i4AddrOff = SLI_ZERO; i4AddrOff < (NS_IN6ADDRSZ / NS_INT16SZ);
         i4AddrOff++)
    {
        if (au2Words[i4AddrOff] == SLI_ZERO)
        {
            if (i4CurBase == SLI_FAILURE)
            {
                i4CurBase = i4AddrOff;
                i4CurLen = SLI_ONE;
            }
            else
                i4CurLen++;
        }
        else
        {
            if (i4CurBase != SLI_FAILURE)
            {
                if ((i4BestBase == SLI_FAILURE) || (i4CurLen > i4BestLen))
                {
                    i4BestBase = i4CurBase;
                    i4BestLen = i4CurLen;
                }
                i4CurBase = SLI_FAILURE;
            }
        }
    }

    if (i4CurBase != SLI_FAILURE)
    {
        if ((i4BestBase == SLI_FAILURE) || (i4CurLen > i4BestLen))
        {
            i4BestBase = i4CurBase;
            i4BestLen = i4CurLen;
        }
    }
    if ((i4BestBase != SLI_FAILURE) && (i4BestLen < SLI_TWO))
    {
        i4BestBase = SLI_FAILURE;
    }

/*  Format the result.  */
    pu1Tp = u1Temp;
    for (i4AddrOff = SLI_ZERO; i4AddrOff < (NS_IN6ADDRSZ / NS_INT16SZ);
         i4AddrOff++)
    {
/* Are we inside the best run of 0x00's? */
        if ((i4BestBase != SLI_FAILURE) && (i4AddrOff >= i4BestBase)
            && (i4AddrOff < (i4BestBase + i4BestLen)))
        {
            if (i4AddrOff == i4BestBase)
            {
                *pu1Tp++ = SLI_COLON_CHAR;
            }
            continue;
        }
/* Are we following an initial run of 0x00s or any real hex? */
        if (i4AddrOff != SLI_ZERO)
        {
            *pu1Tp++ = SLI_COLON_CHAR;
        }
/* Is this address an encapsulated IPv4? */
        if ((i4AddrOff == 6) && (i4BestBase == SLI_ZERO) &&
            ((i4BestLen == 6) || ((i4BestLen == 5) &&
                                  (au2Words[5] == SLI_MAX_UINT2))))
        {
#ifdef IP_WANTED
            if (!SliInetNtop4
                (pu1Src + 12, pu1Tp, sizeof u1Temp - (pu1Tp - u1Temp)))
            {
                return (NULL);
            }
#endif
            pu1Tp += STRLEN (pu1Tp);
            break;
        }
        pu1Tp += SPRINTF ((char *) pu1Tp, "%x", au2Words[i4AddrOff]);
    }
/* Was it a trailing run of 0x00's? */
    if ((i4BestBase != SLI_FAILURE) && (i4BestBase + i4BestLen) ==
        (NS_IN6ADDRSZ / NS_INT16SZ))
    {
        *pu1Tp++ = SLI_COLON_CHAR;
    }
    *pu1Tp++ = SLI_NULL_CHAR;

/* Check for overflow, copy, and we're done.  */
    if ((pu1Tp - u1Temp) > i4Size)
    {
        SLI_ERR (SLI_EINVAL) return (NULL);
    }
    STRNCPY (pu1Dst, u1Temp, STRLEN (u1Temp));
    pu1Dst[STRLEN (u1Temp)] = '\0';
    return (pu1Dst);
}
#endif
