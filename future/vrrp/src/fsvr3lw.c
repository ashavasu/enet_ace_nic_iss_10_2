/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvr3lw.c,v 1.2 2014/03/08 13:40:06 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include "vrrpinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVrrpVersionSupported
 Input       :  The Indices

                The Object 
                retValFsVrrpVersionSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpVersionSupported (INT4 *pi4RetValFsVrrpVersionSupported)
{
    tSNMP_MULTI_DATA_TYPE   Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_3, VRRP_MIB_VERSION, &Data);

    *pi4RetValFsVrrpVersionSupported = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3TraceOption
 Input       :  The Indices

                The Object 
                retValFsVrrpv3TraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpv3TraceOption (INT4 *pi4RetValFsVrrpv3TraceOption)
{
    tSNMP_MULTI_DATA_TYPE   Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_3, VRRP_MIB_TRACE_OPTION, &Data);

    *pi4RetValFsVrrpv3TraceOption = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3NotificationCntl
 Input       :  The Indices

                The Object 
                retValFsVrrpv3NotificationCntl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpv3NotificationCntl (INT4 *pi4RetValFsVrrpv3NotificationCntl)
{
    tSNMP_MULTI_DATA_TYPE   Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_3, VRRP_MIB_NOTIF_CTRL, &Data);

    *pi4RetValFsVrrpv3NotificationCntl = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3Status
 Input       :  The Indices

                The Object 
                retValFsVrrpv3Status
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsVrrpv3Status (INT4 *pi4RetValFsVrrpv3Status)
{
    tSNMP_MULTI_DATA_TYPE   Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_3, VRRP_MIB_GBL_STATUS, &Data);

    *pi4RetValFsVrrpv3Status = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3MaxOperEntries
 Input       :  The Indices

                The Object 
                retValFsVrrpv3MaxOperEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsVrrpv3MaxOperEntries (INT4 *pi4RetValFsVrrpv3MaxOperEntries)
{
    tSNMP_MULTI_DATA_TYPE   Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_3, VRRP_MIB_MAX_OPER_ENTRIES, &Data);

    *pi4RetValFsVrrpv3MaxOperEntries = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3MaxAssociatedIpEntries
 Input       :  The Indices

                The Object 
                retValFsVrrpv3MaxAssociatedIpEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsVrrpv3MaxAssociatedIpEntries (INT4 *pi4RetValFsVrrpv3MaxAssociatedIpEntries)
{
    tSNMP_MULTI_DATA_TYPE   Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_3, VRRP_MIB_MAX_ASSOC_ENTRIES, &Data);

    *pi4RetValFsVrrpv3MaxAssociatedIpEntries = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}


/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVrrpVersionSupported
 Input       :  The Indices

                The Object 
                setValFsVrrpVersionSupported
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpVersionSupported (INT4 i4SetValFsVrrpVersionSupported)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpVersionSupported;

    i4RetValue = VrrpSetAllScalars (VRRP_VERSION_3, VRRP_MIB_VERSION,
                                    &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetFsVrrpv3TraceOption
 Input       :  The Indices

                The Object 
                setValFsVrrpv3TraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpv3TraceOption (INT4 i4SetValFsVrrpv3TraceOption)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpv3TraceOption;

    i4RetValue = VrrpSetAllScalars (VRRP_VERSION_3, VRRP_MIB_TRACE_OPTION,
                                    &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetFsVrrpv3NotificationCntl
 Input       :  The Indices

                The Object 
                setValFsVrrpv3NotificationCntl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpv3NotificationCntl (INT4 i4SetValFsVrrpv3NotificationCntl)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpv3NotificationCntl;

    i4RetValue = VrrpSetAllScalars (VRRP_VERSION_3, VRRP_MIB_NOTIF_CTRL,
                                    &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetFsVrrpv3Status
 Input       :  The Indices

                The Object 
                setValFsVrrpv3Status
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsVrrpv3Status(INT4 i4SetValFsVrrpv3Status)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpv3Status;

    i4RetValue = VrrpSetAllScalars (VRRP_VERSION_3, VRRP_MIB_GBL_STATUS,
                                    &Data);

    return ((INT1) i4RetValue);
}


/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVrrpVersionSupported
 Input       :  The Indices

                The Object 
                testValFsVrrpVersionSupported
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpVersionSupported (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsVrrpVersionSupported)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpVersionSupported;

    i4RetValue = VrrpTestAllScalars (VRRP_VERSION_3, VRRP_MIB_VERSION,
                                     &Data, pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsVrrpv3TraceOption
 Input       :  The Indices

                The Object 
                testValFsVrrpv3TraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpv3TraceOption (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsVrrpv3TraceOption)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpv3TraceOption;

    i4RetValue = VrrpTestAllScalars (VRRP_VERSION_3, VRRP_MIB_TRACE_OPTION,
                                     &Data, pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsVrrpv3NotificationCntl
 Input       :  The Indices

                The Object 
                testValFsVrrpv3NotificationCntl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpv3NotificationCntl (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsVrrpv3NotificationCntl)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpv3NotificationCntl;

    i4RetValue = VrrpTestAllScalars (VRRP_VERSION_3, VRRP_MIB_NOTIF_CTRL,
                                     &Data, pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsVrrpv3Status
 Input       :  The Indices

                The Object 
                testValFsVrrpv3Status
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsVrrpv3Status(UINT4 *pu4ErrorCode , INT4 i4TestValFsVrrpv3Status)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpv3Status;

    i4RetValue = VrrpTestAllScalars (VRRP_VERSION_3, VRRP_MIB_GBL_STATUS,
                                     &Data, pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVrrpVersionSupported
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVrrpVersionSupported (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (*pSnmpIndexList);
    UNUSED_PARAM (*pSnmpVarBind);
   
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhDepv2FsVrrpv3TraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVrrpv3TraceOption (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (*pSnmpIndexList);
    UNUSED_PARAM (*pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhDepv2FsVrrpv3NotificationCntl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVrrpv3NotificationCntl (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (*pSnmpIndexList);
    UNUSED_PARAM (*pSnmpVarBind);
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhDepv2FsVrrpv3Status
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsVrrpv3Status(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return ((INT1) SNMP_SUCCESS);
}


/* LOW LEVEL Routines for Table : FsVrrpv3OperationsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVrrpv3OperationsTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVrrpv3OperationsTable (INT4 i4IfIndex,
                                                 INT4 i4Vrrpv3OperationsVrId,
                                                 INT4
                                                 i4Vrrpv3OperationsInetAddrType)
{
    return (nmhValidateIndexInstanceVrrpv3OperationsTable (i4IfIndex,
                                                           i4Vrrpv3OperationsVrId,
                                                           i4Vrrpv3OperationsInetAddrType));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVrrpv3OperationsTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVrrpv3OperationsTable (INT4 *pi4IfIndex,
                                         INT4 *pi4Vrrpv3OperationsVrId,
                                         INT4 *pi4Vrrpv3OperationsInetAddrType)
{
    return (nmhGetFirstIndexVrrpv3OperationsTable (pi4IfIndex,
                                                   pi4Vrrpv3OperationsVrId,
                                                   pi4Vrrpv3OperationsInetAddrType));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVrrpv3OperationsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                Vrrpv3OperationsVrId
                nextVrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                nextVrrpv3OperationsInetAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVrrpv3OperationsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                        INT4 i4Vrrpv3OperationsVrId,
                                        INT4 *pi4NextVrrpv3OperationsVrId,
                                        INT4 i4Vrrpv3OperationsInetAddrType,
                                        INT4
                                        *pi4NextVrrpv3OperationsInetAddrType)
{
    return (nmhGetNextIndexVrrpv3OperationsTable (i4IfIndex, pi4NextIfIndex,
                                                  i4Vrrpv3OperationsVrId,
                                                  pi4NextVrrpv3OperationsVrId,
                                                  i4Vrrpv3OperationsInetAddrType,
                                                  pi4NextVrrpv3OperationsInetAddrType));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVrrpv3OperationsTrackGroupId
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValFsVrrpv3OperationsTrackGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpv3OperationsTrackGroupId (INT4 i4IfIndex,
                                      INT4 i4Vrrpv3OperationsVrId,
                                      INT4 i4Vrrpv3OperationsInetAddrType,
                                      UINT4
                                      *pu4RetValFsVrrpv3OperationsTrackGroupId)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_TRACK_GRP_ID, &Data);

    *pu4RetValFsVrrpv3OperationsTrackGroupId = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3OperationsDecrementPriority
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValFsVrrpv3OperationsDecrementPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpv3OperationsDecrementPriority (INT4 i4IfIndex,
                                           INT4 i4Vrrpv3OperationsVrId,
                                           INT4 i4Vrrpv3OperationsInetAddrType,
                                           UINT4
                                           *pu4RetValFsVrrpv3OperationsDecrementPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_DEC_PRIORITY, &Data);

    *pu4RetValFsVrrpv3OperationsDecrementPriority = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVrrpv3OperationsTrackGroupId
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                setValFsVrrpv3OperationsTrackGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpv3OperationsTrackGroupId (INT4 i4IfIndex,
                                      INT4 i4Vrrpv3OperationsVrId,
                                      INT4 i4Vrrpv3OperationsInetAddrType,
                                      UINT4
                                      u4SetValFsVrrpv3OperationsTrackGroupId)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4SetValFsVrrpv3OperationsTrackGroupId;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_TRACK_GRP_ID, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetFsVrrpv3OperationsDecrementPriority
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                setValFsVrrpv3OperationsDecrementPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpv3OperationsDecrementPriority (INT4 i4IfIndex,
                                           INT4 i4Vrrpv3OperationsVrId,
                                           INT4 i4Vrrpv3OperationsInetAddrType,
                                           UINT4
                                           u4SetValFsVrrpv3OperationsDecrementPriority)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4SetValFsVrrpv3OperationsDecrementPriority;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_DEC_PRIORITY, &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVrrpv3OperationsTrackGroupId
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                testValFsVrrpv3OperationsTrackGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpv3OperationsTrackGroupId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                         INT4 i4Vrrpv3OperationsVrId,
                                         INT4 i4Vrrpv3OperationsInetAddrType,
                                         UINT4
                                         u4TestValFsVrrpv3OperationsTrackGroupId)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4TestValFsVrrpv3OperationsTrackGroupId;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_TRACK_GRP_ID, &Data,
                                      pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsVrrpv3OperationsDecrementPriority
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                testValFsVrrpv3OperationsDecrementPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpv3OperationsDecrementPriority (UINT4 *pu4ErrorCode,
                                              INT4 i4IfIndex,
                                              INT4 i4Vrrpv3OperationsVrId,
                                              INT4
                                              i4Vrrpv3OperationsInetAddrType,
                                              UINT4
                                              u4TestValFsVrrpv3OperationsDecrementPriority)
{
    tSNMP_MULTI_DATA_TYPE   Data;
    INT4                    i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4TestValFsVrrpv3OperationsDecrementPriority;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_DEC_PRIORITY, &Data,
                                      pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVrrpv3OperationsTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVrrpv3OperationsTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (*pSnmpIndexList);
    UNUSED_PARAM (*pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsVrrpv3OperationsTrackGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVrrpv3OperationsTrackGroupTable
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVrrpv3OperationsTrackGroupTable (UINT4
                                                           u4FsVrrpv3OperationsTrackGroupIndex)
{
    UNUSED_PARAM (u4FsVrrpv3OperationsTrackGroupIndex);

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVrrpv3OperationsTrackGroupTable
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVrrpv3OperationsTrackGroupTable (UINT4
                                                   *pu4FsVrrpv3OperationsTrackGroupIndex)
{
    return (nmhGetNextIndexFsVrrpv3OperationsTrackGroupTable (0,
                                                              pu4FsVrrpv3OperationsTrackGroupIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVrrpv3OperationsTrackGroupTable
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
                nextFsVrrpv3OperationsTrackGroupIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVrrpv3OperationsTrackGroupTable (UINT4
                                                  u4FsVrrpv3OperationsTrackGroupIndex,
                                                  UINT4
                                                  *pu4NextFsVrrpv3OperationsTrackGroupIndex)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    BOOL1                 b1VersionMatched = FALSE;
    
    b1VersionMatched = VrrpCompCheckVersion (VRRP_VERSION_3);

    if (b1VersionMatched == FALSE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    pTrackGroupEntry
        = VrrpDbGetNextTrackGroupEntry (u4FsVrrpv3OperationsTrackGroupIndex);

    if (pTrackGroupEntry == NULL)
    {
        return ((INT1) SNMP_FAILURE);
    }

    *pu4NextFsVrrpv3OperationsTrackGroupIndex = pTrackGroupEntry->u4GroupIndex;

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVrrpv3OperationsTrackedGroupTrackedLinks
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex

                The Object 
                retValFsVrrpv3OperationsTrackedGroupTrackedLinks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsVrrpv3OperationsTrackedGroupTrackedLinks (UINT4 u4FsVrrpv3OperationsTrackGroupIndex,
                                                       UINT4 *pu4RetValFsVrrpv3OperationsTrackedGroupTrackedLinks)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllTrackGroupTable (VRRP_VERSION_3,
                                            u4FsVrrpv3OperationsTrackGroupIndex,
                                            VRRP_MIB_TRACK_GROUP_LINKS, &Data);

    *pu4RetValFsVrrpv3OperationsTrackedGroupTrackedLinks = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3OperationsTrackRowStatus
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex

                The Object 
                retValFsVrrpv3OperationsTrackRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpv3OperationsTrackRowStatus (UINT4
                                        u4FsVrrpv3OperationsTrackGroupIndex,
                                        INT4
                                        *pi4RetValFsVrrpv3OperationsTrackRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllTrackGroupTable (VRRP_VERSION_3,
                                            u4FsVrrpv3OperationsTrackGroupIndex,
                                            VRRP_MIB_TRACK_ROW_STATUS, &Data);

    *pi4RetValFsVrrpv3OperationsTrackRowStatus = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVrrpv3OperationsTrackedGroupTrackedLinks
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex

                The Object 
                setValFsVrrpv3OperationsTrackedGroupTrackedLinks
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsVrrpv3OperationsTrackedGroupTrackedLinks (UINT4 u4FsVrrpv3OperationsTrackGroupIndex,
                                                       UINT4 u4SetValFsVrrpv3OperationsTrackedGroupTrackedLinks)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4SetValFsVrrpv3OperationsTrackedGroupTrackedLinks;

    i4RetValue = VrrpSetAllTrackGroupTable (VRRP_VERSION_3,
                                            u4FsVrrpv3OperationsTrackGroupIndex,
                                            VRRP_MIB_TRACK_GROUP_LINKS, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetFsVrrpv3OperationsTrackRowStatus
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex

                The Object 
                setValFsVrrpv3OperationsTrackRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpv3OperationsTrackRowStatus (UINT4
                                        u4FsVrrpv3OperationsTrackGroupIndex,
                                        INT4
                                        i4SetValFsVrrpv3OperationsTrackRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpv3OperationsTrackRowStatus;

    i4RetValue = VrrpSetAllTrackGroupTable (VRRP_VERSION_3,
                                            u4FsVrrpv3OperationsTrackGroupIndex,
                                            VRRP_MIB_TRACK_ROW_STATUS, &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVrrpv3OperationsTrackedGroupTrackedLinks
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex

                The Object 
                testValFsVrrpv3OperationsTrackedGroupTrackedLinks
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsVrrpv3OperationsTrackedGroupTrackedLinks (UINT4 *pu4ErrorCode,
                                                          UINT4 u4FsVrrpv3OperationsTrackGroupIndex,
                                                          UINT4 u4TestValFsVrrpv3OperationsTrackedGroupTrackedLinks)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4TestValFsVrrpv3OperationsTrackedGroupTrackedLinks;

    i4RetValue = VrrpTestAllTrackGroupTable (VRRP_VERSION_3,
                                             u4FsVrrpv3OperationsTrackGroupIndex,
                                             VRRP_MIB_TRACK_GROUP_LINKS, &Data,
                                             pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsVrrpv3OperationsTrackRowStatus
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex

                The Object 
                testValFsVrrpv3OperationsTrackRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpv3OperationsTrackRowStatus (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4FsVrrpv3OperationsTrackGroupIndex,
                                           INT4
                                           i4TestValFsVrrpv3OperationsTrackRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpv3OperationsTrackRowStatus;

    i4RetValue = VrrpTestAllTrackGroupTable (VRRP_VERSION_3,
                                             u4FsVrrpv3OperationsTrackGroupIndex,
                                             VRRP_MIB_TRACK_ROW_STATUS, &Data,
                                             pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVrrpv3OperationsTrackGroupTable
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVrrpv3OperationsTrackGroupTable (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (*pSnmpIndexList);
    UNUSED_PARAM (*pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsVrrpv3OperationsTrackGroupIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVrrpv3OperationsTrackGroupIfTable
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
                FsVrrpv3OperationsTrackGroupIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsVrrpv3OperationsTrackGroupIfTable (UINT4 u4FsVrrpv3OperationsTrackGroupIndex,
                                                                  INT4 i4FsVrrpv3OperationsTrackGroupIfIndex)
{
    UNUSED_PARAM (u4FsVrrpv3OperationsTrackGroupIndex);
    UNUSED_PARAM (i4FsVrrpv3OperationsTrackGroupIfIndex);

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVrrpv3OperationsTrackGroupIfTable
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
                FsVrrpv3OperationsTrackGroupIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsVrrpv3OperationsTrackGroupIfTable (UINT4 *pu4FsVrrpv3OperationsTrackGroupIndex,
                                                          INT4 *pi4FsVrrpv3OperationsTrackGroupIfIndex)
{
    return (nmhGetNextIndexFsVrrpv3OperationsTrackGroupIfTable (0,
                                                                pu4FsVrrpv3OperationsTrackGroupIndex,
                                                                0,
                                                                pi4FsVrrpv3OperationsTrackGroupIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVrrpv3OperationsTrackGroupIfTable
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
                nextFsVrrpv3OperationsTrackGroupIndex
                FsVrrpv3OperationsTrackGroupIfIndex
                nextFsVrrpv3OperationsTrackGroupIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsVrrpv3OperationsTrackGroupIfTable (UINT4 u4FsVrrpv3OperationsTrackGroupIndex,
                                                         UINT4 *pu4NextFsVrrpv3OperationsTrackGroupIndex,
                                                         INT4 i4FsVrrpv3OperationsTrackGroupIfIndex,
                                                         INT4 *pi4NextFsVrrpv3OperationsTrackGroupIfIndex )
{
    tVrrpTrackGroupTable   *pTrackGroupEntry = NULL;
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;
    BOOL1                   b1VersionMatched = FALSE;
    
    b1VersionMatched = VrrpCompCheckVersion (VRRP_VERSION_3);

    if (b1VersionMatched == FALSE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    pTrackGroupEntry
        = VrrpDbGetTrackGroupEntry (u4FsVrrpv3OperationsTrackGroupIndex);

    if (pTrackGroupEntry != NULL)
    {
        pTrackGroupIfEntry 
            = VrrpDbGetNextTrackGroupIfEntry (pTrackGroupEntry,
                                              i4FsVrrpv3OperationsTrackGroupIfIndex);

        if (pTrackGroupIfEntry != NULL)
        {
            *pu4NextFsVrrpv3OperationsTrackGroupIndex
                = u4FsVrrpv3OperationsTrackGroupIndex;
            *pi4NextFsVrrpv3OperationsTrackGroupIfIndex
                = pTrackGroupIfEntry->i4IfIndex;

            return ((INT1) SNMP_SUCCESS);
        }
    }

    pTrackGroupEntry
        = VrrpDbGetNextTrackGroupEntry (u4FsVrrpv3OperationsTrackGroupIndex);

    if (pTrackGroupEntry == NULL)
    {
        return ((INT1) SNMP_FAILURE);
    }

    pTrackGroupIfEntry
        = VrrpDbGetNextTrackGroupIfEntry (pTrackGroupEntry, 0);

    if (pTrackGroupIfEntry == NULL)
    {
        return ((INT1) SNMP_FAILURE);
    }

    *pu4NextFsVrrpv3OperationsTrackGroupIndex = pTrackGroupEntry->u4GroupIndex;
    *pi4NextFsVrrpv3OperationsTrackGroupIfIndex = pTrackGroupIfEntry->i4IfIndex;

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVrrpv3OperationsTrackGroupIfRowStatus
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
                FsVrrpv3OperationsTrackGroupIfIndex

                The Object 
                retValFsVrrpv3OperationsTrackGroupIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsVrrpv3OperationsTrackGroupIfRowStatus (UINT4 u4FsVrrpv3OperationsTrackGroupIndex,
                                                    INT4 i4FsVrrpv3OperationsTrackGroupIfIndex,
                                                    INT4 *pi4RetValFsVrrpv3OperationsTrackGroupIfRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllTrackGroupIfTable (VRRP_VERSION_3,
                                              u4FsVrrpv3OperationsTrackGroupIndex,
                                              i4FsVrrpv3OperationsTrackGroupIfIndex,
                                              VRRP_MIB_TRACK_IF_ROW_STATUS,
                                              &Data);

    *pi4RetValFsVrrpv3OperationsTrackGroupIfRowStatus = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVrrpv3OperationsTrackGroupIfRowStatus
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
                FsVrrpv3OperationsTrackGroupIfIndex

                The Object 
                setValFsVrrpv3OperationsTrackGroupIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsVrrpv3OperationsTrackGroupIfRowStatus (UINT4 u4FsVrrpv3OperationsTrackGroupIndex,
                                                    INT4 i4FsVrrpv3OperationsTrackGroupIfIndex,
                                                    INT4 i4SetValFsVrrpv3OperationsTrackGroupIfRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpv3OperationsTrackGroupIfRowStatus;

    i4RetValue = VrrpSetAllTrackGroupIfTable (VRRP_VERSION_3,
                                              u4FsVrrpv3OperationsTrackGroupIndex,
                                              i4FsVrrpv3OperationsTrackGroupIfIndex,
                                              VRRP_MIB_TRACK_IF_ROW_STATUS,
                                              &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVrrpv3OperationsTrackGroupIfRowStatus
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
                FsVrrpv3OperationsTrackGroupIfIndex

                The Object 
                testValFsVrrpv3OperationsTrackGroupIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsVrrpv3OperationsTrackGroupIfRowStatus (UINT4 *pu4ErrorCode,
                                                       UINT4 u4FsVrrpv3OperationsTrackGroupIndex,
                                                       INT4 i4FsVrrpv3OperationsTrackGroupIfIndex,
                                                       INT4 i4TestValFsVrrpv3OperationsTrackGroupIfRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpv3OperationsTrackGroupIfRowStatus;

    i4RetValue = VrrpTestAllTrackGroupIfTable (VRRP_VERSION_3,
                                               u4FsVrrpv3OperationsTrackGroupIndex,
                                               i4FsVrrpv3OperationsTrackGroupIfIndex,
                                               VRRP_MIB_TRACK_IF_ROW_STATUS,
                                               &Data, pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVrrpv3OperationsTrackGroupIfTable
 Input       :  The Indices
                FsVrrpv3OperationsTrackGroupIndex
                FsVrrpv3OperationsTrackGroupIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsVrrpv3OperationsTrackGroupIfTable (UINT4 *pu4ErrorCode,
                                                  tSnmpIndexList *pSnmpIndexList,
                                                  tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);

	return ((INT1) SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsVrrpv3StatisticsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVrrpv3StatisticsTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVrrpv3StatisticsTable (INT4 i4IfIndex,
                                                 INT4 i4Vrrpv3OperationsVrId,
                                                 INT4
                                                 i4Vrrpv3OperationsInetAddrType)
{
    return (nmhValidateIndexInstanceVrrpv3StatisticsTable (i4IfIndex,
                                                           i4Vrrpv3OperationsVrId,
                                                           i4Vrrpv3OperationsInetAddrType));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVrrpv3StatisticsTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVrrpv3StatisticsTable (INT4 *pi4IfIndex,
                                         INT4 *pi4Vrrpv3OperationsVrId,
                                         INT4 *pi4Vrrpv3OperationsInetAddrType)
{
    return (nmhGetFirstIndexVrrpv3StatisticsTable (pi4IfIndex,
                                                   pi4Vrrpv3OperationsVrId,
                                                   pi4Vrrpv3OperationsInetAddrType));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVrrpv3StatisticsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                Vrrpv3OperationsVrId
                nextVrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                nextVrrpv3OperationsInetAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVrrpv3StatisticsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                        INT4 i4Vrrpv3OperationsVrId,
                                        INT4 *pi4NextVrrpv3OperationsVrId,
                                        INT4 i4Vrrpv3OperationsInetAddrType,
                                        INT4
                                        *pi4NextVrrpv3OperationsInetAddrType)
{
    return (nmhGetNextIndexVrrpv3StatisticsTable (i4IfIndex, pi4NextIfIndex,
                                                  i4Vrrpv3OperationsVrId,
                                                  pi4NextVrrpv3OperationsVrId,
                                                  i4Vrrpv3OperationsInetAddrType,
                                                  pi4NextVrrpv3OperationsInetAddrType));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVrrpv3StatisticsTxedAdvertisements
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValFsVrrpv3StatisticsTxedAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpv3StatisticsTxedAdvertisements (INT4 i4IfIndex,
                                            INT4 i4Vrrpv3OperationsVrId,
                                            INT4 i4Vrrpv3OperationsInetAddrType,
                                            tSNMP_COUNTER64_TYPE *
                                            pu8RetValFsVrrpv3StatisticsTxedAdvertisements)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,                                            
                                       VRRP_MIB_STAT_TXED_ADVT, &Data);

    pu8RetValFsVrrpv3StatisticsTxedAdvertisements->msn
        = Data.u8_Counter64Value.msn;
    pu8RetValFsVrrpv3StatisticsTxedAdvertisements->lsn
        = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3StatisticsTxedV2Advertisements
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValFsVrrpv3StatisticsTxedV2Advertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsVrrpv3StatisticsTxedV2Advertisements (INT4 i4IfIndex,
                                                   INT4 i4Vrrpv3OperationsVrId,
                                                   INT4 i4Vrrpv3OperationsInetAddrType,
                                                   tSNMP_COUNTER64_TYPE *pu8RetValFsVrrpv3StatisticsTxedV2Advertisements)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,                                            
                                       VRRP_MIB_STAT_V3_TXED_ADVT, &Data);

    pu8RetValFsVrrpv3StatisticsTxedV2Advertisements->msn
        = Data.u8_Counter64Value.msn;
    pu8RetValFsVrrpv3StatisticsTxedV2Advertisements->lsn
        = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3StatisticsV2AdvertiseIgnored
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValFsVrrpv3StatisticsV2AdvertiseIgnored
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsVrrpv3StatisticsV2AdvertiseIgnored (INT4 i4IfIndex,
                                                 INT4 i4Vrrpv3OperationsVrId,
                                                 INT4 i4Vrrpv3OperationsInetAddrType,
                                                 tSNMP_COUNTER64_TYPE *pu8RetValFsVrrpv3StatisticsV2AdvertiseIgnored)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,                                            
                                       VRRP_MIB_STAT_V2_ADVT_IGNORED, &Data);

    pu8RetValFsVrrpv3StatisticsV2AdvertiseIgnored->msn
        = Data.u8_Counter64Value.msn;
    pu8RetValFsVrrpv3StatisticsV2AdvertiseIgnored->lsn
        = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3StatisticsMasterAdverInterval
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValFsVrrpv3StatisticsMasterAdverInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpv3StatisticsMasterAdverInterval (INT4 i4IfIndex,
                                             INT4 i4Vrrpv3OperationsVrId,
                                             INT4
                                             i4Vrrpv3OperationsInetAddrType,
                                             INT4
                                             *pi4RetValFsVrrpv3StatisticsMasterAdverInterval)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_MASTER_ADVT_INT, &Data);

    *pi4RetValFsVrrpv3StatisticsMasterAdverInterval = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3StatisticsSkewTime
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValFsVrrpv3StatisticsSkewTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpv3StatisticsSkewTime (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                  INT4 i4Vrrpv3OperationsInetAddrType,
                                  INT4 *pi4RetValFsVrrpv3StatisticsSkewTime)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_SKEW_TIME, &Data);

    *pi4RetValFsVrrpv3StatisticsSkewTime = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpv3StatisticsMasterDownInterval
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValFsVrrpv3StatisticsMasterDownInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpv3StatisticsMasterDownInterval (INT4 i4IfIndex,
                                            INT4 i4Vrrpv3OperationsVrId,
                                            INT4 i4Vrrpv3OperationsInetAddrType,
                                            INT4
                                            *pi4RetValFsVrrpv3StatisticsMasterDownInterval)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                  i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_MASTER_DOWN_INT, &Data);

    *pi4RetValFsVrrpv3StatisticsMasterDownInterval = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}
