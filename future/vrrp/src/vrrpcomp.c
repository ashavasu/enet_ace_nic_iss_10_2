/*  $Id: vrrpcomp.c,v 1.21 2017/12/16 11:57:11 siva Exp $ */
/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrpcomp.c                                   |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Core Protocol.                          |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : This is the interface between the core        |
 * |                            protocol and the external modules.            |
 * |                                                                          |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.2.0.0        |  Padmanaban. B       |   Base Line Version             |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#include "vrrpinc.h"
#include "vcm.h"

VOID                VrrpStateChangeResponse (INT4 i4OperIndex);

/*******************************************************************************
 * Function Name    : VrrpStateChangeRequest
 * Description      : This Function is called when there is a change of state
 *                    in sem for an instance of VRRP. This will interact with
 *                    the external modules to complete the election process.
 *
 * Global Variables : None
 *
 * Inputs           : u1Event
 *                    i4OperIndex
 *
 * Output           : None
 * Returns          : None
 ******************************************************************************/

INT1
VrrpStateChangeRequest (UINT1 u1Event, INT4 i4OperIndex)
{
    tVrrpIPvXAddr       MasterIpAddress;
    UINT1               au1VMacAddr[MACADDRSIZE];
    UINT1               u1IsMclagEnabled = OSIX_FALSE;
    UINT1               au1IpAddr[IPVX_IPV6_ADDR_LEN];

    MEMSET (au1IpAddr, 0, IPVX_IPV6_ADDR_LEN);

    /* Fetch whether MC-LAG is enabled on member port of 
     * VLAN corresponding to IVR interface */
    VrrpIsMCLAGEnabled ((UINT4) OPERIFINDEX (i4OperIndex), &u1IsMclagEnabled);

    VRRP_IPVX_ADDR_CLEAR (MasterIpAddress);

    if (u1Event == INIT_MSTR_IND)
    {
        /* Fill in Master IP Address from PrimaryIP address */
        VrrpMasterIpAddressSelection (i4OperIndex, INITIAL_STATE);

        VRRP_IPVX_COPY (MasterIpAddress, OPERMASTERIPADDR (i4OperIndex));
    }
    else if (u1Event == BKUP_MSTR_IND)
    {
        /* Fill in Master IP Address from PrimaryIP address */
        VrrpMasterIpAddressSelection (i4OperIndex, MASTER_STATE);

        VRRP_IPVX_COPY (MasterIpAddress, OPERMASTERIPADDR (i4OperIndex));
    }
    else if ((u1Event == INIT_BKUP_IND) && (u1IsMclagEnabled == OSIX_TRUE))
    {
        VRRP_IPVX_ADDR_INIT_IPVX (OPERMASTERIPADDR (i4OperIndex),
                                  OPERADDRTYPE (i4OperIndex), au1IpAddr);
        VRRP_IPVX_COPY (MasterIpAddress, OPERMASTERIPADDR (i4OperIndex));
    }
    else
    {
        VRRP_IPVX_COPY (MasterIpAddress, OPERMASTERIPADDR (i4OperIndex));
    }

    VRRP_MEMCPY (au1VMacAddr, &OPERMACADDR (i4OperIndex), MACADDRSIZE);

    switch (u1Event)
    {
        case IFACE_INFO_IND:

            VrrpFillIfIpAddr (i4OperIndex);

            VrrpCompSetIpvXOwner (i4OperIndex);

            if (VRRP_IPVX_COMPARE (INTERFACEIPADDR (i4OperIndex),
                                   gVrrpZeroIpAddr) != 0)
            {
                VrrpSem (STARTUP_EVENT, i4OperIndex, ZERO, ZERO);
            }

            /* State Change Response not required for this event */
            return VRRP_OK;

        case INIT_MSTR_IND:
            VRRP_DBG (VRRP_TRC_EVENTS,
                      "COMP: Changing Intial to Master Sem \n");
            EthRegisterMacAddr (MasterIpAddress, au1VMacAddr, i4OperIndex);
            break;

        case INIT_BKUP_IND:
            if (u1IsMclagEnabled == OSIX_TRUE)
            {
                if (VRRP_OK != EthRegisterMacAddr (MasterIpAddress, au1VMacAddr,
                                                   i4OperIndex))
                {
                    VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                              "Failure in Registering MAC address\r\n");
                }
            }
            VRRPOPERHWSTATUS (i4OperIndex) = NP_UPDATED;
            break;

        case BKUP_MSTR_IND:
            if (u1IsMclagEnabled == OSIX_TRUE)
            {
                break;
            }
            else
            {
                EthRegisterMacAddr (MasterIpAddress, au1VMacAddr, i4OperIndex);
            }
            break;

        case BKUP_INIT_IND:
            if (u1IsMclagEnabled == OSIX_TRUE)
            {
                EthDeRegisterMacAddr (MasterIpAddress, au1VMacAddr,
                                      i4OperIndex);
            }
            break;

        case MSTR_INIT_IND:
            EthDeRegisterMacAddr (MasterIpAddress, au1VMacAddr, i4OperIndex);
            break;

        case MSTR_BKUP_IND:
            if (u1IsMclagEnabled == OSIX_TRUE)
            {
                break;
            }
            else
            {
                EthDeRegisterMacAddr (MasterIpAddress, au1VMacAddr,
                                      i4OperIndex);
                break;
            }
        default:
            VRRP_DBG (VRRP_TRC_EVENTS,
                      "COMP: Invalid Event Received from Sem \n");

    }
    VrrpStateChangeResponse (i4OperIndex);
    return VRRP_OK;

}

/*******************************************************************************
 * Function Name    : VrrpStateChangeResponse
 * Description      : This function is called after all the services required
 *                    from the external module have been completed.
 *
 * Global Variables : None
 *
 * Inputs           : i4OperIndex  - Oper Index of VR Entry
 * Output           : state of OperTable
 * Returns          : None
 ******************************************************************************/
VOID
VrrpStateChangeResponse (INT4 i4OperIndex)
{
    UINT4               u4IfIndex = 0;
    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) &au1IfName[0];

    if (((VrrpFuncPtr)
         SemTable[OPERSTATE (i4OperIndex) -
                  1][OPEREVENTRCVD (i4OperIndex)].FuncPtr) (i4OperIndex,
                                                            OPERRCVDPRIORITY
                                                            (i4OperIndex)) ==
        VRRP_OK)
    {
        OPERSTATE (i4OperIndex) = OPERNEXTSTATE (i4OperIndex);
        u4IfIndex = (UINT4) OPERIFINDEX (i4OperIndex);
        CfaCliConfGetIfName (u4IfIndex, piIfName);
        if (STRLEN (piIfName))
        {
            if (OPERSTATE (i4OperIndex) == INITIAL_STATE)
            {
                UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, (UINT4) VRRP_MASK,
                              VRRP_CLI_TRC_STATE, "VRRP", "VRRP",
                              "Interface %s state changed to INIT\n", piIfName);
            }
            if (OPERSTATE (i4OperIndex) == BACKUP_STATE)
            {
                UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, (UINT4) VRRP_MASK,
                              VRRP_CLI_TRC_STATE, "VRRP", "VRRP",
                              "Interface %s state elected as BACKUP\n",
                              piIfName);
            }
            if (OPERSTATE (i4OperIndex) == MASTER_STATE)
            {
                UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, (UINT4) VRRP_MASK,
                              VRRP_CLI_TRC_STATE, "VRRP", "VRRP",
                              "Interface %s state elected as MASTER\n",
                              piIfName);
            }
        }
        VrrpRedDbUtilAddTblNode (&gVrrpDynInfoList, &OPERDBNODE (i4OperIndex));
        VrrpRedSyncDynInfo ();
    }
    return;
}

/*******************************************************************************
 * Function Name    : VrrpMasterIpAddressSelection
 * Description      : This function is used for select master ip address
 *                    of numerically lowest IP Address of the Index.
 * Global Variables : None
 * Inputs           : i4OperIndex  - Oper Index of VR Entry
 *                    i4State      - Oper State of VR Entry
 * Output           : state of OperTable
 * Returns          : None
 *******************************************************************************/
VOID
VrrpMasterIpAddressSelection (INT4 i4OperIndex, INT4 i4State)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;
    tVrrpIPvXAddr       PrevIpAddr;

    VRRP_IPVX_COPY (PrevIpAddr, OPERMASTERIPADDR (i4OperIndex));

    if (i4State == MASTER_STATE)
    {
        VRRP_IPVX_COPY (OPERMASTERIPADDR (i4OperIndex),
                        INTERFACEIPADDR (i4OperIndex));
    }
    else if (i4State == INITIAL_STATE)
    {
        /* If primary ip address is available this ip address is set to master address */
        if (VRRP_IPVX_COMPARE (OPERPRIMARYIPADDR (i4OperIndex),
                               gVrrpZeroIpAddr) != 0)
        {
            VRRP_IPVX_COPY (OPERMASTERIPADDR (i4OperIndex),
                            OPERPRIMARYIPADDR (i4OperIndex));
        }
        else
        {
            UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                                 pTempIpAddrEntry, tIpAddrTable *)
            {
                /* Associated IP Address is in sorted order, so copy the first
                 * Address */
                VRRP_IPVX_COPY (OPERMASTERIPADDR (i4OperIndex),
                                pIpAddrEntry->AssoIpAddr);
                break;
            }
        }
    }
    else
    {
        VRRP_IPVX_COPY (OPERMASTERIPADDR (i4OperIndex), gSenderIpAddr);
    }

    if (VRRP_IPVX_COMPARE (PrevIpAddr, OPERMASTERIPADDR (i4OperIndex)) != 0)
    {
        VrrpRedDbUtilAddTblNode (&gVrrpDynInfoList, &OPERDBNODE (i4OperIndex));
        VrrpRedSyncDynInfo ();
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompIpv4IsOurIp                              */
/*                                                                           */
/*    Description         : This function checks if the passed IPv4 Address  */
/*                          is present in either primary or secondary address*/
/*                          list for passed interface                        */
/*                                                                           */
/*    Input(s)            : i4IfIndex      - Interface Index                 */
/*                          IpAddr         - IpAddr                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK        - If Our IP                       */
/*                          VRRP_NOT_OK    - If Not Our IP                   */
/*****************************************************************************/
INT1
VrrpCompIpv4IsOurIp (INT4 i4IfIndex, tIPvXAddr IpAddr)
{
    INT1                i1RetVal = VRRP_NOT_OK;
    UINT4               u4TempIpAddr = 0;
    UINT4               u4TempNetMask = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4Port = 0;
    tNetIpv4IfInfo      IpIntfInfo;

    MEMSET (&IpIntfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
        == NETIPV4_FAILURE)
    {
        return i1RetVal;
    }

    if (NetIpv4GetIfInfo ((UINT2) u4Port, &IpIntfInfo) == NETIPV4_FAILURE)
    {
        return i1RetVal;
    }

    u4TempIpAddr = IpIntfInfo.u4Addr;
    u4TempNetMask = IpIntfInfo.u4NetMask;

    VRRP_IPVX_COPY_TO_IPV4 (u4IpAddr, IpAddr);

    do
    {
        if (u4TempIpAddr == u4IpAddr)
        {
            i1RetVal = VRRP_OK;
            break;
        }
    }
    while (NetIpv4GetNextSecondaryAddress (u4Port,
                                           u4TempIpAddr, &u4TempIpAddr,
                                           &u4TempNetMask) == NETIPV4_SUCCESS);

    return i1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompIpv6IsOurIp                              */
/*                                                                           */
/*    Description         : This function checks if the passed IPv6 Address  */
/*                          is present in this router on the corresponding   */
/*                          interface or not                                 */
/*                                                                           */
/*    Input(s)            : i4IfIndex      - Interface Index                 */
/*                          IpAddr         - IpAddr                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK        - If Our IP                       */
/*                          VRRP_NOT_OK    - If Not Our IP                   */
/*****************************************************************************/
INT1
VrrpCompIpv6IsOurIp (INT4 i4IfIndex, tIPvXAddr IpAddr)
{
    tIp6Addr            Ip6Addr;
    UINT4               u4IpIfIndex = 0;
    INT1                i1RetVal = VRRP_NOT_OK;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    MEMCPY (&Ip6Addr.ip6_addr_u.u1ByteAddr[0], IpAddr.au1Addr,
            IPVX_IPV6_ADDR_LEN);

#ifdef IP6_WANTED
    if (NetIpv6IsOurAddress (&Ip6Addr, &u4IpIfIndex) == NETIPV4_FAILURE)
    {
        return i1RetVal;
    }
#else
    UNUSED_PARAM (u4IpIfIndex);
    UNUSED_PARAM (i1RetVal);
#endif

    UNUSED_PARAM (i4IfIndex);

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompIsOurIp                                  */
/*                                                                           */
/*    Description         : This function checks if the passed IP Address    */
/*                          is present in this router or not based on IPv4   */
/*                          or IPv6                                          */
/*                          or Not                                           */
/*                                                                           */
/*    Input(s)            : u1AddrType     - Address Type                    */
/*                          i4IfIndex      - CFA If Index                    */
/*                          IpAddr         - IpAddr                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK        - If Our IP                       */
/*                          VRRP_NOT_OK    - If Not Our IP                   */
/*****************************************************************************/
INT1
VrrpCompIsOurIp (UINT1 u1AddrType, INT4 i4IfIndex, tIPvXAddr IpAddr)
{
    INT1                i1RetVal = VRRP_NOT_OK;

    if (u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        i1RetVal = VrrpCompIpv4IsOurIp (i4IfIndex, IpAddr);
    }
    else
    {
        i1RetVal = VrrpCompIpv6IsOurIp (i4IfIndex, IpAddr);
    }

    return i1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompSetIpvXOwner                             */
/*                                                                           */
/*    Description         : This function decides if this router is the IPvX */
/*                          or Not                                           */
/*                                                                           */
/*    Input(s)            : i4OperIndex   - Oper Index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : fSkewTime    - Skew Time                         */
/*****************************************************************************/
VOID
VrrpCompSetIpvXOwner (INT4 i4OperIndex)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;
    INT1                i1RetVal = VRRP_NOT_OK;

    UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                         pTempIpAddrEntry, tIpAddrTable *)
    {
        i1RetVal = VrrpCompIsOurIp (OPERADDRTYPE (i4OperIndex),
                                    OPERIFINDEX (i4OperIndex),
                                    pIpAddrEntry->AssoIpAddr);

        if (i1RetVal == VRRP_OK)
        {
            break;
        }
    }

    if (i1RetVal == VRRP_NOT_OK)
    {
        OPERISOWNER (i4OperIndex) = FALSE;
    }
    else
    {
        OPERISOWNER (i4OperIndex) = TRUE;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompCalcSkewTime                             */
/*                                                                           */
/*    Description         : This function calculates Skew Time as required   */
/*                          by VRRP Version 2 or Version 3.                  */
/*                                                                           */
/*    Input(s)            : i4OperIndex   - Oper Index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : fSkewTime    - Skew Time                         */
/*****************************************************************************/
FLT4
VrrpCompCalcSkewTime (INT4 i4OperIndex)
{
    FLT4                fTime = 0.0;

    if (gi4VrrpNodeVersion == VRRP_VERSION_2)
    {
        fTime = (FLT4) ((256 - OPERPRIORITY (i4OperIndex)) / 256);
    }
    else
    {
        fTime = (FLT4) (((256 - OPERPRIORITY (i4OperIndex)) *
                         OPERRCVDMASTERADVTINT (i4OperIndex)) / 256);

        OPERSKEWTIME (i4OperIndex) = (INT4) fTime;
    }

    return fTime;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompCalcMasterDownInterval                   */
/*                                                                           */
/*    Description         : This function calculates Master Down Interval    */
/*                          as required by VRRP Version 2 or Version 3.      */
/*                                                                           */
/*    Input(s)            : i4OperIndex   - Oper Index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : fMasterDownInterval - Master Down Interval       */
/*****************************************************************************/
FLT4
VrrpCompCalcMasterDownInterval (INT4 i4OperIndex)
{
    FLT4                fTime = 0.0;
    FLT4                fSkewTime = 0.0;

    fSkewTime = VrrpCompCalcSkewTime (i4OperIndex);

    if (gi4VrrpNodeVersion == VRRP_VERSION_2)
    {
        if ((OPERFIRSTPKTRECVFROMNP (i4OperIndex) == VRRP_FALSE)
            || (OPERPREEMPTMODE (i4OperIndex) == VRRP_DISABLE))
        {
            fTime = ((FLT4) ADVER_INTERVAL_VRRP_V2 (i4OperIndex)) + fSkewTime;
            OPERFIRSTPKTRECVFROMNP (i4OperIndex) = VRRP_TRUE;
        }
        else
        {
            fTime = ((FLT4) (3 * OPERADVERINTL (i4OperIndex))) + fSkewTime;
        }

    }
    else
    {
        if ((OPERFIRSTPKTRECVFROMNP (i4OperIndex) == VRRP_FALSE)
            || (OPERPREEMPTMODE (i4OperIndex) == VRRP_DISABLE))
        {
            fTime = ((FLT4) ADVER_INTERVAL_VRRP_V3 (i4OperIndex)) + fSkewTime;
            OPERFIRSTPKTRECVFROMNP (i4OperIndex) = VRRP_TRUE;
        }
        else
        {
            fTime =
                ((FLT4) (3 * OPERRCVDMASTERADVTINT (i4OperIndex))) + fSkewTime;
        }

        OPERMASTERDOWNINT (i4OperIndex) = (INT4) fTime;

    }

    return fTime;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompCheckVersion                             */
/*                                                                           */
/*    Description         : This function validates VRRP version.            */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : TRUE  - If version matched                       */
/*                          FALSE - If version not matched                   */
/*****************************************************************************/
BOOL1
VrrpCompCheckVersion (INT4 i4Version)
{
    if (i4Version == VRRP_VERSION_2)
    {
        if ((gi4VrrpNodeVersion == VRRP_VERSION_2) ||
            (gi4VrrpNodeVersion == VRRP_VERSION_2_3))
        {
            return TRUE;
        }
    }
    else if (i4Version == VRRP_VERSION_3)
    {
        if ((gi4VrrpNodeVersion == VRRP_VERSION_3) ||
            (gi4VrrpNodeVersion == VRRP_VERSION_2_3))
        {
            return TRUE;
        }
    }

    return FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpHandleLinkDownForTrackGroup                  */
/*                                                                           */
/*    Description         : This function handles link down event for        */
/*                          track group                                      */
/*                                                                           */
/*    Input(s)            : u4IfIndex  - Interface Index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpHandleLinkDownForTrackGroup (UINT4 u4IfIndex)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tVrrpTrackGroupTable *pTempTrackGroupEntry = NULL;
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;
    UINT4               u4TrackedLinks = 0;

    UTL_DLL_OFFSET_SCAN (&(gTrackGroupHead), pTrackGroupEntry,
                         pTempTrackGroupEntry, tVrrpTrackGroupTable *)
    {
        u4TrackedLinks = TMO_DLL_Count (&pTrackGroupEntry->TrackGroupIfHead);

        if (pTrackGroupEntry->u1RowStatus != (UINT1) ACTIVE)
        {
            continue;
        }

        pTrackGroupIfEntry =
            VrrpDbGetTrackGroupIfEntry (pTrackGroupEntry, (INT4) u4IfIndex);

        if (pTrackGroupIfEntry == NULL)
        {
            continue;
        }

        if (pTrackGroupIfEntry->u1IfStatus == VRRP_TRACK_IF_DOWN)
        {
            continue;
        }

        pTrackGroupIfEntry->u1IfStatus = VRRP_TRACK_IF_DOWN;

        if (pTrackGroupEntry->u1CurActiveLinks <= VRRP_ONE)
        {
            pTrackGroupEntry->u1CurActiveLinks = 0;
        }
        else
        {
            pTrackGroupEntry->u1CurActiveLinks--;
        }

        if ((pTrackGroupEntry->u1TrackedLinks == 0) &&
            (pTrackGroupEntry->u1CurActiveLinks >
             pTrackGroupEntry->u1TrackedLinks))
        {
            continue;
        }
        if ((pTrackGroupEntry->u1TrackedLinks != 0) &&
            ((pTrackGroupEntry->u1CurActiveLinks) >
             (u4TrackedLinks - pTrackGroupEntry->u1TrackedLinks)))
        {
            continue;
        }

        VrrpCompActivateTrackGroup (pTrackGroupEntry);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpHandleLinkUpForTrackGroup                    */
/*                                                                           */
/*    Description         : This function handles link up event for          */
/*                          track group                                      */
/*                                                                           */
/*    Input(s)            : u4IfIndex  - Interface Index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpHandleLinkUpForTrackGroup (UINT4 u4IfIndex)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tVrrpTrackGroupTable *pTempTrackGroupEntry = NULL;
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;
    UINT4               u4TrackedLinks = 0;

    UTL_DLL_OFFSET_SCAN (&(gTrackGroupHead), pTrackGroupEntry,
                         pTempTrackGroupEntry, tVrrpTrackGroupTable *)
    {
        u4TrackedLinks = TMO_DLL_Count (&pTrackGroupEntry->TrackGroupIfHead);

        /* Process the Groups that are only ACTIVE */
        if (pTrackGroupEntry->u1RowStatus != (UINT1) ACTIVE)
        {
            continue;
        }

        pTrackGroupIfEntry =
            VrrpDbGetTrackGroupIfEntry (pTrackGroupEntry, (INT4) u4IfIndex);

        /* Go to next track group if Track If is not present for
         * the interface index passed */
        if (pTrackGroupIfEntry == NULL)
        {
            continue;
        }

        /* Go to the next track group if link status is already up. */
        if (pTrackGroupIfEntry->u1IfStatus == VRRP_TRACK_IF_UP)
        {
            continue;
        }

        pTrackGroupIfEntry->u1IfStatus = VRRP_TRACK_IF_UP;
        pTrackGroupEntry->u1CurActiveLinks++;

        /* Go to the next group if the tracked group is already Up. */
        if (pTrackGroupEntry->u1GroupStatus == VRRP_GROUP_UP)
        {
            continue;
        }

        if (pTrackGroupEntry->u1TrackedLinks == 0)
        {
            /* Restart the SEM of Oper Entries corresponding to the
             * track group now since action to be taken is only when
             * all links go down and only link has come up.
             */
            VrrpCompDeactivateTrackGroup (pTrackGroupEntry);
        }
        else
        {
            if (pTrackGroupEntry->u1CurActiveLinks >
                (u4TrackedLinks - pTrackGroupEntry->u1TrackedLinks))
            {
                /* Restart the SEM of Oper Entries corresponding to
                 * the track group now since number of links that 
                 * has come up has exceeded links to be tracked. */
                VrrpCompDeactivateTrackGroup (pTrackGroupEntry);
            }
            else
            {
                /* Go to next group since number of links that
                 * has come up has not exceeded links to be tracked. */
                continue;
            }
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompActivateTrackGroup                       */
/*                                                                           */
/*    Description         : This function activates Track Group Entry        */
/*                          and all its Oper Table Entries                   */
/*                                                                           */
/*    Input(s)            : pTrackGroupEntry - Pointer to Track Group Entry  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpCompActivateTrackGroup (tVrrpTrackGroupTable * pTrackGroupEntry)
{
    tVrrpOperTable     *pVrrpOperEntry = NULL;
    tVrrpOperTable     *pTempVrrpOperEntry = NULL;

    pTrackGroupEntry->u1GroupStatus = VRRP_GROUP_DOWN;

    UTL_DLL_OFFSET_SCAN (&(pTrackGroupEntry->VrrpOperHead),
                         pVrrpOperEntry, pTempVrrpOperEntry, tVrrpOperTable *)
    {
        pVrrpOperEntry->u1TrackedIfStatus = VRRP_TRACK_IF_DOWN;

        VrrpCompActivateOperTrack (pVrrpOperEntry->i4OperIndex);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompDeactivateTrackGroup                     */
/*                                                                           */
/*    Description         : This function deactivates Track Group Entry      */
/*                          and all its Oper Table Entries                   */
/*                                                                           */
/*    Input(s)            : pTrackGroupEntry - Pointer to Track Group Entry  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpCompDeactivateTrackGroup (tVrrpTrackGroupTable * pTrackGroupEntry)
{
    tVrrpOperTable     *pVrrpOperEntry = NULL;
    tVrrpOperTable     *pTempVrrpOperEntry = NULL;

    pTrackGroupEntry->u1GroupStatus = VRRP_GROUP_UP;

    UTL_DLL_OFFSET_SCAN (&(pTrackGroupEntry->VrrpOperHead),
                         pVrrpOperEntry, pTempVrrpOperEntry, tVrrpOperTable *)
    {
        pVrrpOperEntry->u1TrackedIfStatus = VRRP_TRACK_IF_UP;

        VrrpCompDeactivateOperTrack (pVrrpOperEntry->i4OperIndex);
    }

    return;
}

/****************************************************************************/
/* Function Name         :   VrrpPrintIpv4Address                           */
/*                                                                          */
/* Description           :   Converts the Input Ipv6Address into String and */
/*                           Prints the IPAddress in the standard format    */
/*                                                                          */
/* Input (s)             :   Addr - IPvX Address                            */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : string containing the IpAddress in printable format*/
/****************************************************************************/

UINT1              *
VrrpPrintIpv4Address (tVrrpIPvXAddr Addr)
{
    CHR1               *pc1Address = NULL;
    UINT1              *pu1Address = NULL;
    UINT4               u4IpVal = 0;

    VRRP_IPVX_COPY_TO_IPV4 (u4IpVal, Addr);
    CLI_CONVERT_IPADDR_TO_STR (pc1Address, u4IpVal);

    pu1Address = (UINT1 *) (VOID *) pc1Address;

    return (pu1Address);
}

/****************************************************************************/
/* Function Name         :   VrrpPrintIpv6Address                           */
/*                                                                          */
/* Description           :   Converts the Input Ipv6Address into String and */
/*                           Prints the IPAddress in the standard format    */
/*                                                                          */
/* Input (s)             :   Addr - IPvX Address                            */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : string containing the IpAddress in printable format*/
/****************************************************************************/

UINT1              *
VrrpPrintIpv6Address (tVrrpIPvXAddr Addr)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    MEMCPY (&Ip6Addr.u1_addr, Addr.au1Addr, IPVX_IPV6_ADDR_LEN);

    return (Ip6PrintAddr (&Ip6Addr));
}

/******************************************************************************
* Function Name :  VrrpValIfTrackIfExists
*
* Description   :  This function checks whether the ifindex is associated with
*                  tracked links or not.
* Input (s)     :  i4RecvdIfIndex     - Interface Index
* Output (s)    :  None
* Returns       :  VRRP_OK or VRRP_NOT_OK
*******************************************************************************/

INT4
VrrpValIfTrackIfExists (INT4 i4RecvdIfIndex)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tVrrpTrackGroupTable *pTempTrackGroupEntry = NULL;
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;

    UTL_DLL_OFFSET_SCAN (&(gTrackGroupHead), pTrackGroupEntry,
                         pTempTrackGroupEntry, tVrrpTrackGroupTable *)
    {
        pTrackGroupIfEntry =
            VrrpDbGetTrackGroupIfEntry (pTrackGroupEntry,
                                        (INT4) i4RecvdIfIndex);

        if (pTrackGroupIfEntry != NULL)
        {
            return VRRP_OK;
        }
    }

    return VRRP_NOT_OK;
}

/******************************************************************************
 * Function Name   : VrrpValIfIpChange
 * Description     : To check whether the changed interface ip address belong to
 *                   the same network.
 * Inputs          : u4IfIndex - interface address to be added
 *                   u4IpAddr  - IP address(Current interface address)
 *                    u4IpSubnetMask - Subnet mask of current interface address
 * Returns         : VRRP_OK - on success
 *                   VRRP_NOT_OK - on failure.
 ******************************************************************************/
INT4
VrrpValIfIpChange (UINT4 u4IfIndex, tIPvXAddr CurIpAddr, UINT4 u4IpSubnetMask)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;
    UINT4               u4PrevAddr = 0;
    UINT4               u4IpAddr = 0;
    INT4                i4Count = 0;

    for (i4Count = 0; i4Count < (INT4) gu4VrrpMaxOperEntries; i4Count++)
    {
        if (OPERIFINDEX (i4Count) != (INT4) u4IfIndex)
        {
            continue;
        }

        UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4Count), pIpAddrEntry,
                             pTempIpAddrEntry, tIpAddrTable *)
        {
            if (u4IpSubnetMask == 0)
            {
                if ((VRRP_IPVX_COMPARE (pIpAddrEntry->AssoIpAddr, CurIpAddr) ==
                     0))
                {
                    return VRRP_OK;
                }
            }
            else
            {
                VRRP_IPVX_COPY_TO_IPV4 (u4PrevAddr, pIpAddrEntry->AssoIpAddr);
                VRRP_IPVX_COPY_TO_IPV4 (u4IpAddr, CurIpAddr);

                if ((u4PrevAddr & u4IpSubnetMask) ==
                    (u4IpAddr & u4IpSubnetMask))
                {
                    return VRRP_OK;
                }
            }
        }
    }

    return VRRP_NOT_OK;
}

/****************************************************************************/
/* Function Name         :   VrrpExtValDecPriority                          */
/*                                                                          */
/* Description           :   This function validates the decrement priority */
/*                           against current priority and returns           */
/*                           VRRP_OK if current priority is greater than    */
/*                           decrement priority and VRRP_NOT_OK otherwise.  */
/*                                                                          */
/* Input (s)             :   u4IfIndex       - If Index                     */
/*                           u1Operation     - Operation to be performed    */
/*                                             1. IP Add                    */
/*                                             2. IP Delete                 */
/*                                             3. IP Modify                 */
/*                                                                          */
/*                           PrevIpAddr      - Prev IP Address              */
/*                           CurIpAddr       - Current IP Address           */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns               :  VRRP_OK or VRRP_NOT_OK                          */
/****************************************************************************/
INT1
VrrpExtValDecPriority (UINT4 u4IfIndex, UINT1 u1Operation,
                       tIPvXAddr PrevIpAddr, tIPvXAddr CurIpAddr)
{
    INT4                i4Count = 0;

    for (i4Count = 0; i4Count < (INT4) gu4VrrpMaxOperEntries; i4Count++)
    {
        if (OPERIFINDEX (i4Count) != (INT4) u4IfIndex)
        {
            continue;
        }

        if (VrrpValidateDecPriority (i4Count, u1Operation, PrevIpAddr,
                                     CurIpAddr, 0, OPERDECPRIO (i4Count))
            == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    return VRRP_OK;
}

/****************************************************************************/
/* Function Name         :   VrrpValidateDecPriority                        */
/*                                                                          */
/* Description           :   This function validates the decrement priority */
/*                           against current priority and returns           */
/*                           VRRP_OK if current priority is greater than    */
/*                           decrement priority and VRRP_NOT_OK otherwise.  */
/*                                                                          */
/* Input (s)             :   i4OperIndex     - Oper Index                   */
/*                           u1Operation     - Operation to be performed    */
/*                                             1. IP Add                    */
/*                                             2. IP Delete                 */
/*                                             3. IP Modify                 */
/*                                                                          */
/*                           PrevIpAddr      - Prev IP Address              */
/*                           CurIpAddr       - Current IP Address           */
/*                           i4CurPriority   - Current Priority             */
/*                           i4DecPriority   - Decrement Priority           */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns               :  VRRP_OK or VRRP_NOT_OK                          */
/****************************************************************************/
INT1
VrrpValidateDecPriority (INT4 i4OperIndex, UINT1 u1Operation,
                         tIPvXAddr PrevIpAddr, tIPvXAddr CurIpAddr,
                         INT4 i4CurPriority, INT4 i4DecPriority)
{
    INT4                i4Priority = 0;
    BOOL1               b1PrevOwner = FALSE;
    BOOL1               b1CurOwner1 = FALSE;
    BOOL1               b1CurOwner2 = FALSE;

    if (OPERTRACKGRPID (i4OperIndex) == VRRP_ZERO)
    {
        return VRRP_OK;
    }

    b1PrevOwner = OPERISOWNER (i4OperIndex);

    if (u1Operation == VRRP_VAL_PRIO_OPER_IP_ADD)
    {
        if (VrrpCompIsOurIp (OPERADDRTYPE (i4OperIndex),
                             OPERIFINDEX (i4OperIndex), CurIpAddr) == VRRP_OK)
        {
            b1CurOwner1 = TRUE;
        }

        if (b1PrevOwner == b1CurOwner1)
        {
            return VRRP_OK;
        }
        else
        {
            /* When IP is added, if it is our IP and if previous owner
             * and current owner is different, it means change from
             * non-owner to owner is happening. */
            i4Priority = VRRP_IPOWNER_PRIORITY;
        }
    }
    else if (u1Operation == VRRP_VAL_PRIO_OPER_IP_DEL)
    {
        /* Get the Owner excluding IP to be deleted. */
        b1CurOwner1 = VrrpCompGetIPvXOwner (i4OperIndex, CurIpAddr);

        if (b1PrevOwner == b1CurOwner1)
        {
            return VRRP_OK;
        }
        else
        {
            /* When IP is deleted, if owner state is different when
             * calculating is our IP (excluding this IP), it means change
             * from owner to non-owner is happening. */
            i4Priority = OPERADMINPRIORITY (i4OperIndex);
        }
    }
    else if (u1Operation == VRRP_VAL_PRIO_OPER_IP_MODIFY)
    {
        /* Get the Owner excluding previous IP. */
        b1CurOwner1 = VrrpCompGetIPvXOwner (i4OperIndex, PrevIpAddr);
        if (VrrpCompIsOurIp (OPERADDRTYPE (i4OperIndex),
                             OPERIFINDEX (i4OperIndex), CurIpAddr) == VRRP_OK)
        {

            b1CurOwner2 = TRUE;
        }
        if (b1PrevOwner == FALSE)
        {
            /* This Entry is previously non-owner. With the modified IP,
             * check if this becomes owner and set priority accordingly. */
            if (b1CurOwner2 == TRUE)
            {
                i4Priority = VRRP_IPOWNER_PRIORITY;
            }
            else
            {
                i4Priority = OPERADMINPRIORITY (i4OperIndex);
            }
        }
        else
        {
            /* This Entry is previous owner. With the deleted IP,
             * check if this becomes non-owner and set priority accordingly. */
            if (b1CurOwner1 == TRUE)
            {
                i4Priority = VRRP_IPOWNER_PRIORITY;
            }
            else
            {
                i4Priority = OPERADMINPRIORITY (i4OperIndex);
            }
        }
    }
    else
    {
        if (OPERISOWNER (i4OperIndex) == TRUE)
        {
            i4Priority = VRRP_IPOWNER_PRIORITY;
        }
        else
        {
            i4Priority = i4CurPriority;
        }
    }

    if (i4Priority > i4DecPriority)
    {
        return VRRP_OK;
    }

    return VRRP_NOT_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompGetIPvXOwner                             */
/*                                                                           */
/*    Description         : This function checks if IP present in the        */
/*                          associated IP list excluding the IP mentioned    */
/*                          (if it is non-zero) is present in this system.   */
/*                          and returns IPvX owner value.                    */
/*                                                                           */
/*    Input(s)            : i4OperIndex   - Oper Index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : fSkewTime    - Skew Time                         */
/*****************************************************************************/
BOOL1
VrrpCompGetIPvXOwner (INT4 i4OperIndex, tIPvXAddr IpAddr)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;

    UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                         pTempIpAddrEntry, tIpAddrTable *)
    {
        if ((VRRP_IPVX_COMPARE (IpAddr, gVrrpZeroIpAddr) != 0) &&
            (VRRP_IPVX_COMPARE (pIpAddrEntry->AssoIpAddr, IpAddr) == 0))
        {
            continue;
        }
        if (pIpAddrEntry->b1IsIpvXOwner == TRUE)
        {
            return TRUE;
        }
    }

    return FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompGetTrackOperCount                        */
/*                                                                           */
/*    Description         : This function gives the count of number of vrrp  */
/*                          oper entries present for this track group        */
/*                                                                           */
/*    Input(s)            : u4GroupIndex   - Group Index                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : i4OperCount    - Oper Count                      */
/*****************************************************************************/
UINT4
VrrpCompGetTrackOperCount (UINT4 u4GroupIndex)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;

    pTrackGroupEntry = VrrpDbGetTrackGroupEntry (u4GroupIndex);

    if (pTrackGroupEntry == NULL)
    {
        return 0;
    }

    return (TMO_DLL_Count (&(pTrackGroupEntry->VrrpOperHead)));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompCalcTrackGroupStatusAndAct               */
/*                                                                           */
/*    Description         : This function calculates the track group status  */
/*                          by calculating number of active links.           */
/*                                                                           */
/*    Input(s)            : pTrackGroupEntry   - Track Group Entry           */
/*                          pTrackGroupIfEntry - Track Group If Entry        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpCompCalcTrackGroupStatusAndAct (tVrrpTrackGroupTable * pTrackGroupEntry,
                                    tVrrpTrackGroupIfTable * pTrackGroupIfEntry)
{
    tVrrpTrackGroupIfTable *pTempTrackGroupIfEntry = NULL;
    UINT1               u1CurActiveLinks = 0;
    UINT1               u1TrackedLinks = 0;
    UINT1               u1OperStatus = 0;
    UINT1               u1PrevGroupStatus = 0;
    UINT1               u1LinksToBeDown = 0;

    u1TrackedLinks =
        (UINT1) TMO_DLL_Count (&pTrackGroupEntry->TrackGroupIfHead);

    if (pTrackGroupIfEntry == NULL)
    {
        UTL_DLL_OFFSET_SCAN (&(pTrackGroupEntry->TrackGroupIfHead),
                             pTrackGroupIfEntry, pTempTrackGroupIfEntry,
                             tVrrpTrackGroupIfTable *)
        {
            if (pTrackGroupIfEntry->u1IfStatus == VRRP_TRACK_IF_UP)
            {
                u1CurActiveLinks++;
            }
        }

        pTrackGroupEntry->u1CurActiveLinks = u1CurActiveLinks;
    }
    else
    {
        CfaGetIfOperStatus ((UINT4) pTrackGroupIfEntry->i4IfIndex,
                            &u1OperStatus);

        if (u1OperStatus == CFA_IF_UP)
        {
            pTrackGroupIfEntry->u1IfStatus = VRRP_TRACK_IF_UP;
            pTrackGroupEntry->u1CurActiveLinks++;
        }
        else
        {
            pTrackGroupIfEntry->u1IfStatus = VRRP_TRACK_IF_DOWN;
        }
    }

    u1PrevGroupStatus = pTrackGroupEntry->u1GroupStatus;

    if (pTrackGroupEntry->u1TrackedLinks == 0)
    {
        u1LinksToBeDown = u1TrackedLinks;
    }
    else
    {
        u1LinksToBeDown = pTrackGroupEntry->u1TrackedLinks;
    }

    if (pTrackGroupEntry->u1CurActiveLinks > (u1TrackedLinks - u1LinksToBeDown))
    {
        pTrackGroupEntry->u1GroupStatus = VRRP_GROUP_UP;
    }
    else
    {
        pTrackGroupEntry->u1GroupStatus = VRRP_GROUP_DOWN;
    }

    if ((u1PrevGroupStatus == VRRP_GROUP_UP) &&
        (pTrackGroupEntry->u1GroupStatus == VRRP_GROUP_DOWN))
    {
        VrrpCompActivateTrackGroup (pTrackGroupEntry);
    }
    else if ((u1PrevGroupStatus == VRRP_GROUP_DOWN) &&
             (pTrackGroupEntry->u1GroupStatus == VRRP_GROUP_UP))
    {
        VrrpCompDeactivateTrackGroup (pTrackGroupEntry);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompActivateOperTrack                        */
/*                                                                           */
/*    Description         : This function activates oper entry for track     */
/*                          group configured.                                */
/*                                                                           */
/*    Input(s)            : i4OperIndex   - Oper Index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpCompActivateOperTrack (INT4 i4OperIndex)
{
    tVrrpTimerRef      *pTmrRef = NULL;

    pTmrRef = &OPERTMR (i4OperIndex);

    if (OPERSTATE (i4OperIndex) == MASTER_STATE)
    {
        TmrIfStopTimer (pTmrRef);

        VrrpSem (ADVERTIMER_EVENT, i4OperIndex, 0, 0);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompDeactivateOperTrack                      */
/*                                                                           */
/*    Description         : This function deactivates oper entry for track   */
/*                          group configured.                                */
/*                                                                           */
/*    Input(s)            : i4OperIndex   - Oper Index                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpCompDeactivateOperTrack (INT4 i4OperIndex)
{
    if (OPERSTATE (i4OperIndex) == BACKUP_STATE)
    {
        VrrpSem (SHUTDOWN_EVENT, i4OperIndex, 0, 0);

        VrrpSem (STARTUP_EVENT, i4OperIndex, 0, 0);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCompCheckTrackAndActOnOper                   */
/*                                                                           */
/*    Description         : This function checks the track group status,     */
/*                          fills tracked status in Oper Entry and           */
/*                          activates / deactivates Oper Entry.              */
/*                                                                           */
/*    Input(s)            : pTrackGroupEntry   - Pointer to Track Group      */
/*                          i4OperIndex        - Oper Index                  */
/*                          b1IsResetTrack     - Boolean variable to         */
/*                          indicate if Track Reset is done at oper entry    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpCompCheckTrackAndActOnOper (tVrrpTrackGroupTable * pTrackGroupEntry,
                                INT4 i4OperIndex, BOOL1 b1IsResetTrack)
{
    if (b1IsResetTrack == TRUE)
    {
        if (OPERTRACKIFSTATUS (i4OperIndex) == VRRP_TRACK_IF_DOWN)
        {
            VrrpCompDeactivateOperTrack (i4OperIndex);
        }

        OPERTRACKIFSTATUS (i4OperIndex) = VRRP_TRACK_IF_NOT_KNOWN;
    }
    else if (pTrackGroupEntry->u1GroupStatus == VRRP_GROUP_UP)
    {
        OPERTRACKIFSTATUS (i4OperIndex) = VRRP_TRACK_IF_UP;
    }
    else if (pTrackGroupEntry->u1GroupStatus == VRRP_GROUP_DOWN)
    {
        OPERTRACKIFSTATUS (i4OperIndex) = VRRP_TRACK_IF_DOWN;
        VrrpCompActivateOperTrack (i4OperIndex);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpIsMCLAGEnabled                               */
/*                                                                           */
/*    Description         : This function Fetches the Mc-lag status of VLAN  */
/*                          corresponding to the IVR interface               */
/*                                                                           */
/*    Input(s)            : u4IfIndex        - IVR index                     */
/*                                                                           */
/*    Output(s)           : pu1IsMclagEnabled  - Will be set to OSIX_TRUE/   */
/*                                               OSIX_FALSE depending on     */
/*                                               MCLAG Enabled or Disabled.  */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpIsMCLAGEnabled (UINT4 u4IfIndex, UINT1 *pu1IsMclagEnabled)
{
    UINT4               u4ContextId = 0;
    tVlanId             VlanId = 0;

    if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId) == VCM_FAILURE)
    {
        *pu1IsMclagEnabled = OSIX_FALSE;
        return;
    }

    if (CfaGetVlanId (u4IfIndex, &VlanId) == CFA_SUCCESS)
    {
        VlanIvrGetMclagEnabledStatus (u4ContextId, VlanId, pu1IsMclagEnabled);
    }

    return;
}

/*****************************************************************************/
/*    Function Name       : VrrpHandleMclagIvrOperStatus                     */
/*                                                                           */
/*    Description         : This function is called when an MCLAG interface  */
/*                          is added/removed from IVR based on which VRRP    */
/*                          configuration's are applied/removed in H/w       */
/*                                                                           */
/*    Input(s)            : i4OperIndex      - IVR index                     */
/*                        : u1OperStatus     - Oper Status of IVR interface  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/

VOID
VrrpHandleMclagIvrOperStatus (INT4 i4PortIndex, UINT1 u1OperStatus)
{
    tVrrpIPvXAddr       MasterIpAddress;
    INT4                i4OperIndex = 0;
    UINT1               au1VMacAddr[MACADDRSIZE];
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);
    VRRP_IPVX_ADDR_CLEAR (MasterIpAddress);
    MEMSET (au1VMacAddr, 0, MACADDRSIZE);
    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);

    if (VrrpEnabledOnInterface (i4PortIndex) == VRRP_NOT_OK)
    {
        VRRP_DBG1 (VRRP_TRC_EVENTS,
                   "VrrpHandleMclagIvrOperStatus: Vrrp not enabled on interface %d "
                   " Ignoring Oper Status indication from Layer 2",
                   i4PortIndex);
        return;

    }

    for (i4OperIndex = 0; i4OperIndex < (INT4) gu4VrrpMaxOperEntries;
         i4OperIndex++)
    {
        if (OPERIFINDEX (i4OperIndex) == i4PortIndex)
        {

            VRRP_DBG3 (VRRP_TRC_EVENTS,
                       "VrrpHandleMclagIvrOperStatus for VR %d to be activated due to link up/down "
                       "event on interface %d and addressType %s\n\r",
                       OPERVRID (i4OperIndex), i4PortIndex, ac1AddrType);

            break;
        }
    }                            /* End of For Loop */

    /* If the Node State is Backup, we need to program the VRRP configurations

       1) If Oper status is CFA_IF_UP, atleast one MCLAG interface exists
       and MCLAG enabled on port-channel's, program the IP and Mac in H/w

       2) If Oper status is CFA_IF_DOWN, no MCLAG interface exists and
       MCLAG disabled on port-channel's, remove the IP and Mac from  H/w.
     */

    if (OPERSTATE ((i4OperIndex)) == BACKUP_STATE)
    {
        VRRP_IPVX_COPY (MasterIpAddress, OPERMASTERIPADDR (i4OperIndex));
        VRRP_MEMCPY (au1VMacAddr, &OPERMACADDR (i4OperIndex), MACADDRSIZE);

        if (u1OperStatus == CFA_IF_UP)
        {
            EthRegisterMacAddr (MasterIpAddress, au1VMacAddr, i4OperIndex);
        }
        else
        {
            EthDeRegisterMacAddr (MasterIpAddress, au1VMacAddr, i4OperIndex);
        }
    }

    return;
}
