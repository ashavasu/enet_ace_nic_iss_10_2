/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: vrrptst.c,v 1.5 2017/12/16 11:57:12 siva Exp $
 *
 * Description: This file contains Get All routines for all tables
 *              of VRRP.
 *           
 *******************************************************************/
#include "vrrpinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpTestAllOperTable                             */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Oper Table and sets the value of requested       */
/*                          Object.                                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          u4ObjectId   - Object Identifier                 */
/*                          pData        - Pointer to Data to be set         */
/*                                                                           */
/*    Output(s)           : pu4ErrorCode - Error Code                        */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpTestAllOperTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                      INT4 i4AddrType, UINT4 u4ObjectId,
                      tSNMP_MULTI_DATA_TYPE * pData, UINT4 *pu4ErrorCode)
{
    tIPvXAddr           IpAddr;
    tIp6Addr            Ip6Addr;
    tCfaIfInfo          CfaIfInfo;
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    INT4                i4Index = VRRP_NOT_OK;
    UINT4               u4IpAddr = 0;
    BOOL1               b1VersionMatched = FALSE;
    UINT1               u1AddressType = 0;

    VRRP_IPVX_ADDR_CLEAR (IpAddr);
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (gi4VrrpNodeVersion == VRRP_VERSION_2_3)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_V2_V3_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i4Index = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

    if (u4ObjectId != VRRP_MIB_OPER_ROW_STATUS)
    {
        if (i4Index == VRRP_NOT_OK)
        {
            CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if ((i4Version == VRRP_VERSION_2) &&
                 (u4ObjectId != VRRP_MIB_OPER_ADMIN_STATE) &&
                 (OPERROWSTATUS (i4Index) == ACTIVE))
        {
            CLI_SET_ERR (CLI_VRRP_VR_ACTIVE_OR_DOWN);

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if ((pData->i4_SLongValue != CREATE_AND_WAIT) &&
             (pData->i4_SLongValue != CREATE_AND_GO))
    {
        if (i4Index == VRRP_NOT_OK)
        {
            CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (i4Index != VRRP_NOT_OK)
    {
        CLI_SET_ERR (CLI_VRRP_DUP_VR_ENTRY);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_OPER_PRIM_IP:
            VRRP_IPVX_ADDR_INIT_IPVX (IpAddr, (UINT1) i4AddrType,
                                      pData->pOctetStrValue->pu1_OctetList);

            if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
            {
                if (pData->pOctetStrValue->i4_Length != IPVX_IPV4_ADDR_LEN)
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_IP);

                    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                    return SNMP_FAILURE;
                }

                VRRP_IPVX_COPY_TO_IPV4 (u4IpAddr, IpAddr);

                if (IP_ADDRESS_ABOVE_CLASS_C (u4IpAddr) ||
                    IP_IS_LOOPBACK (u4IpAddr))
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_IP);

                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }

                if ((u4IpAddr != 0) &&
                    (CfaIpIfIsIpValidOnLocalNetIf ((UINT4) i4IfIndex,
                                                   u4IpAddr) == CFA_FAILURE))
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_NETWORK);
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if (pData->pOctetStrValue->i4_Length != IPVX_IPV6_ADDR_LEN)
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_IP);

                    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                    return SNMP_FAILURE;
                }

                MEMCPY (Ip6Addr.u1_addr, IpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

                if ((IS_ADDR_LOOPBACK (Ip6Addr)) ||
                    (IS_CONSTANT_MULTI (Ip6Addr)) ||
                    (IS_ADDR_MULTI (Ip6Addr)) ||
                    (IS_ADDR_LINK_SCOPE_MULTI (Ip6Addr)))
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_IP);

                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }

                u1AddressType = (UINT1) Ip6AddrType (&Ip6Addr);

                if ((u1AddressType != ADDR6_UNSPECIFIED) &&
                    (u1AddressType != ADDR6_LLOCAL))
                {
                    CLI_SET_ERR (CLI_VRRP_NOT_LINK_LOCAL_IP);

                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;

        case VRRP_MIB_OPER_PRIORITY:
        case VRRP_MIB_OPER_ADMIN_PRIO:
            if ((pData->i4_SLongValue < MINPRIORITY) ||
                (pData->i4_SLongValue > MAXPRIORITY))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_PRIORITY);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (VrrpValidateDecPriority (i4Index, 0, gVrrpZeroIpAddr,
                                         gVrrpZeroIpAddr,
                                         pData->i4_SLongValue,
                                         OPERDECPRIO (i4Index)) == VRRP_NOT_OK)
            {
                CLI_SET_ERR (CLI_VRRP_INCONS_PRIORITY);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;

        case VRRP_MIB_OPER_PREMP_MODE:
            if ((pData->i4_SLongValue < VRRP_ENABLE) ||
                (pData->i4_SLongValue > VRRP_DISABLE))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_PREEMPT);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VRRP_MIB_OPER_ACCEPT_MODE:
            if ((pData->i4_SLongValue < VRRP_ENABLE) ||
                (pData->i4_SLongValue > VRRP_DISABLE))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_ACCEPT);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            /* Accept mode is applicable for both IPv4 and IPv6
             * even though MIB do not mention it for IPv4. */
            break;

        case VRRP_MIB_OPER_ROW_STATUS:
            if ((pData->i4_SLongValue == NOT_READY) ||
                (pData->i4_SLongValue < ACTIVE) ||
                (pData->i4_SLongValue > DESTROY))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_ROW_STATUS);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if ((pData->i4_SLongValue == ACTIVE) &&
                (OPERIPADDRCOUNT (i4Index) <= 0))
            {
                CLI_SET_ERR (CLI_VRRP_NO_ASSOC_IP);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pData->i4_SLongValue == DESTROY) &&
                (OPERIPADDRCOUNT (i4Index) > 0))
            {
                CLI_SET_ERR (CLI_VRRP_MORE_ASSOC_IP);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (CfaGetIfInfo ((UINT4) i4IfIndex, &CfaIfInfo) == CFA_FAILURE)
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_IFINDEX);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (!((CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
                  ((CfaIfInfo.u1IfType == CFA_ENET) &&
                   (CfaIfInfo.u1BridgedIface == CFA_DISABLED))))
            {
                CLI_SET_ERR (CLI_VRRP_INCORRECT_IFINDEX);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((i4VrId < VRRP_MIN_VRID) || (i4VrId > VRRP_MAX_VRID))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_VRID);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((i4AddrType < VRRP_IPVX_ADDR_FMLY_IPV4) ||
                (i4AddrType > VRRP_IPVX_ADDR_FMLY_IPV6))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_ADDR_TYPE);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pData->i4_SLongValue == ACTIVE) &&
                (gi4VrrpNodeVersion == VRRP_VERSION_3) &&
                (VRRP_IPVX_COMPARE (OPERPRIMARYIPADDR (i4Index),
                                    gVrrpZeroIpAddr) == 0))
            {
                CLI_SET_ERR (CLI_VRRP_SET_VRIP);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pData->i4_SLongValue == ACTIVE) &&
                (gi4VrrpNodeVersion == VRRP_VERSION_3) &&
                (VrrpDbOperGetAssocIpEntry (i4Index,
                                            OPERPRIMARYIPADDR (i4Index))
                 == NULL))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VRRP_MIB_OPER_ADMIN_STATE:
            if ((pData->i4_SLongValue < ADMINUP) ||
                (pData->i4_SLongValue > ADMINDOWN))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_ADMIN_STATE);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;

        case VRRP_MIB_OPER_AUTH_TYPE:
            if ((pData->i4_SLongValue < NO_AUTHENTICATION_MIB_VAL) ||
                (pData->i4_SLongValue > HMAC_IP_AUTHENTICATION_MIB_VAL))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_AUTH_TYPE);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;

        case VRRP_MIB_OPER_AUTH_KEY:
            if (OPERAUTHTYPE (i4Index) == NO_AUTHENTICATION_PROT_VAL)
            {
                CLI_SET_ERR (CLI_VRRP_AUTH_KEY_FOR_NO_AUTH);

                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }

            if ((OPERAUTHTYPE (i4Index)
                 == SIMPLE_TEXT_AUTHENTICATION_MIB_VAL) &&
                ((pData->pOctetStrValue->i4_Length > TEXT_AUTHKEY_SIZE) ||
                 (pData->pOctetStrValue->i4_Length < ZERO)))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_AUTH_KEY_LEN);

                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }

            if ((OPERAUTHTYPE (i4Index) == HMAC_IP_AUTHENTICATION_MIB_VAL) &&
                ((pData->pOctetStrValue->i4_Length > HMAKEYSIZE) ||
                 (pData->pOctetStrValue->i4_Length < ZERO)))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_AUTH_KEY_LEN);

                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }

            break;

        case VRRP_MIB_OPER_TRACK_GRP_ID:

            if (pData->u4_ULongValue != 0)
            {
                pTrackGroupEntry
                    = VrrpDbGetTrackGroupEntry (pData->u4_ULongValue);

                if (pTrackGroupEntry == NULL)
                {
                    CLI_SET_ERR (CLI_VRRP_NO_TRACK_GROUP);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;

        case VRRP_MIB_OPER_DEC_PRIORITY:
            if (pData->u4_ULongValue > MAXPRIORITY)
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_PRIORITY);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (VrrpValidateDecPriority (i4Index, 0, gVrrpZeroIpAddr,
                                         gVrrpZeroIpAddr,
                                         OPERPRIORITY (i4Index),
                                         (INT4) pData->u4_ULongValue) ==
                VRRP_NOT_OK)
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_DEC_PRIORITY);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;

        case VRRP_MIB_OPER_ADVT_INT:
            if (i4Version == VRRP_VERSION_2)
            {
                if ((pData->i4_SLongValue < V2_ADVERTISEMENTINTLINMSEC) ||
                    (pData->i4_SLongValue > V2_MAX_ADVERTISEMENTINTLINMSEC))
                {
                    CLI_SET_ERR (CLI_VRRP_ADVT_INT_V2_ERROR);

                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if ((pData->i4_SLongValue < V3_ADVERTISEMENTINTLINMSEC) ||
                    (pData->i4_SLongValue > V3_MAX_ADVERTISEMENTINTLINMSEC))
                {
                    CLI_SET_ERR (CLI_VRRP_ADVT_INT_V3_ERROR);

                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;

        case VRRP_MIB_OPER_ADVT_INT_MSEC:
            if ((pData->i4_SLongValue < ADVERTISEMENTINTLINMSEC) ||
                (pData->i4_SLongValue > MAX_ADVERTISEMENTINTLINMSEC))
            {
                CLI_SET_ERR (CLI_VRRP_ADVT_INT_MSEC_V2_ERROR);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        case VRRP_MIB_OPER_PROTOCOL:
            if ((pData->i4_SLongValue < 0) || (pData->i4_SLongValue > 4))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_PROTOCOL);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpTestAllAssocIpTable                          */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Associated IP Address Table and sets  value of   */
/*                          requested Object.                                */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          pAssocIpAddr - Associated IP Address             */
/*                          u4ObjectId   - Object Identifier                 */
/*                          pData        - Pointer to Data to be set         */
/*                                                                           */
/*    Output(s)           : pu4ErrorCode - Error Code                        */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpTestAllAssocIpTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                         INT4 i4AddrType,
                         tSNMP_OCTET_STRING_TYPE * pAssocIpAddr,
                         UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE * pData,
                         UINT4 *pu4ErrorCode)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIPvXAddr           IpAddr;
    tIp6Addr            Ip6Addr;
    UINT4               u4IpAddr = 0;
    INT4                i4Index = VRRP_OK;
    BOOL1               b1VersionMatched = FALSE;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    VRRP_IPVX_ADDR_CLEAR (IpAddr);
    VRRP_IPVX_ADDR_INIT_IPVX (IpAddr, (UINT1) i4AddrType,
                              pAssocIpAddr->pu1_OctetList);

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (gi4VrrpNodeVersion == VRRP_VERSION_2_3)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_V2_V3_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    i4Index = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

    if (i4Index == VRRP_NOT_OK)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pIpAddrEntry = VrrpDbOperGetAssocIpEntry (i4Index, IpAddr);

    switch (u4ObjectId)
    {
        case VRRP_MIB_ASSO_ROW_STATUS:
            if ((pData->i4_SLongValue == NOT_READY) ||
                (pData->i4_SLongValue == NOT_IN_SERVICE) ||
                (pData->i4_SLongValue < ACTIVE) ||
                (pData->i4_SLongValue > DESTROY))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_ROW_STATUS);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
            {
                if (pAssocIpAddr->i4_Length != IPVX_IPV4_ADDR_LEN)
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_IP);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                MEMCPY (&u4IpAddr, pAssocIpAddr->pu1_OctetList,
                        pAssocIpAddr->i4_Length);
                u4IpAddr = OSIX_NTOHL (u4IpAddr);

                if ((u4IpAddr == 0) ||
                    (IP_ADDRESS_ABOVE_CLASS_C (u4IpAddr)) ||
                    (IP_IS_LOOPBACK (u4IpAddr)))
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_IP);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if ((u4IpAddr != 0) &&
                    (CfaIpIfIsIpValidOnLocalNetIf ((UINT4) i4IfIndex, u4IpAddr)
                     == CFA_FAILURE))
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_NETWORK);
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if (pAssocIpAddr->i4_Length != IPVX_IPV6_ADDR_LEN)
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_IP);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                MEMCPY (Ip6Addr.u1_addr, pAssocIpAddr->pu1_OctetList,
                        pAssocIpAddr->i4_Length);

                if ((IS_ADDR_UNSPECIFIED (Ip6Addr)) ||
                    (IS_ADDR_LOOPBACK (Ip6Addr)) ||
                    (IS_CONSTANT_MULTI (Ip6Addr)) ||
                    (IS_ADDR_MULTI (Ip6Addr)) ||
                    (IS_ADDR_LINK_SCOPE_MULTI (Ip6Addr)))
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_IP);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

            }
            if ((pData->i4_SLongValue == CREATE_AND_WAIT) ||
                (pData->i4_SLongValue == CREATE_AND_GO))
            {
                if (OPERIPADDRCOUNT (i4Index) >= MAX_ASSO_ADDR_ENTRY)
                {
                    CLI_SET_ERR (CLI_VRRP_MAX_ASSOC_PER_VRID);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if (pIpAddrEntry != NULL)
                {
                    CLI_SET_ERR (CLI_VRRP_DUP_ASSO_ENTRY);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else if (pIpAddrEntry == NULL)
            {
                CLI_SET_ERR (CLI_VRRP_NO_ASSOC_ENTRY);

                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }

            /* Implementations should not allow deletion of last
             * associated IP address table corresponding to an active Row
             * in Oper Table. */
            if ((pData->i4_SLongValue == DESTROY) &&
                (OPERIPADDRCOUNT (i4Index) == VRRP_ONE) &&
                (OPERROWSTATUS (i4Index) == ACTIVE))
            {
                CLI_SET_ERR (CLI_VRRP_DEL_ASSOC_WITH_ACTIVE_VR);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (OPERISTESTNOTREQD (i4Index) == TRUE)
            {
                break;
            }

            if ((pData->i4_SLongValue == CREATE_AND_GO) ||
                (pData->i4_SLongValue == CREATE_AND_WAIT))
            {
                if (VrrpValidateDecPriority (i4Index, VRRP_VAL_PRIO_OPER_IP_ADD,
                                             gVrrpZeroIpAddr, IpAddr, 0,
                                             OPERDECPRIO (i4Index))
                    == VRRP_NOT_OK)
                {
                    CLI_SET_ERR (CLI_VRRP_DEC_PRIO_NO_IP_ADD);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else if (pData->i4_SLongValue == DESTROY)
            {
                if (VrrpValidateDecPriority (i4Index, VRRP_VAL_PRIO_OPER_IP_DEL,
                                             gVrrpZeroIpAddr, IpAddr, 0,
                                             OPERDECPRIO (i4Index))
                    == VRRP_NOT_OK)
                {
                    CLI_SET_ERR (CLI_VRRP_DEC_PRIO_NO_IP_DEL);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpTestAllTrackGroupTable                       */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Track Group Table and sets value of requested    */
/*                          Object.                                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          u4GroupIndex - Group Index                       */
/*                          u4ObjectId   - Object Identifier                 */
/*                          pData        - Pointer to Data to be set         */
/*                                                                           */
/*    Output(s)           : pu4ErrorCode - Error Code                        */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpTestAllTrackGroupTable (INT4 i4Version, UINT4 u4GroupIndex,
                            UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE * pData,
                            UINT4 *pu4ErrorCode)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    BOOL1               b1VersionMatched = FALSE;

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pTrackGroupEntry = VrrpDbGetTrackGroupEntry (u4GroupIndex);

    if (u4ObjectId != VRRP_MIB_TRACK_ROW_STATUS)
    {
        if (pTrackGroupEntry == NULL)
        {
            CLI_SET_ERR (CLI_VRRP_NO_TRACK_GROUP);

            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (pData->i4_SLongValue != CREATE_AND_WAIT)
    {
        if (pTrackGroupEntry == NULL)
        {
            CLI_SET_ERR (CLI_VRRP_NO_TRACK_GROUP);

            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (pTrackGroupEntry != NULL)
    {
        CLI_SET_ERR (CLI_VRRP_DUP_TRACK_GROUP);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_TRACK_GROUP_LINKS:
            if (pData->u4_ULongValue >
                TMO_DLL_Count (&(pTrackGroupEntry->TrackGroupIfHead)))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_LINKS);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VRRP_MIB_TRACK_ROW_STATUS:
            if ((pData->i4_SLongValue == NOT_READY) ||
                (pData->i4_SLongValue == NOT_IN_SERVICE) ||
                (pData->i4_SLongValue < ACTIVE) ||
                (pData->i4_SLongValue > DESTROY))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_ROW_STATUS);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (u4GroupIndex == 0)
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_GROUP);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pData->i4_SLongValue == ACTIVE) &&
                (TMO_DLL_Count (&(pTrackGroupEntry->TrackGroupIfHead)) <= 0))
            {
                CLI_SET_ERR (CLI_VRRP_ACTIVE_TRACK_WITH_NO_IF);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pData->i4_SLongValue == DESTROY) &&
                ((TMO_DLL_Count (&(pTrackGroupEntry->VrrpOperHead)) > 0)))
            {
                CLI_SET_ERR (CLI_VRRP_DEL_TRACK_WITH_MORE_VR);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pData->i4_SLongValue == DESTROY) &&
                (TMO_DLL_Count (&(pTrackGroupEntry->TrackGroupIfHead)) > 0))
            {
                CLI_SET_ERR (CLI_VRRP_DEL_TRACK_WITH_MORE_IF);

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpTestAllTrackGroupIfTable                     */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Track Group If Table and sets value of requested */
/*                          Object.                                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          u4GroupIndex - Group Index                       */
/*                          i4IfIndex    - Interface Index                   */
/*                          u4ObjectId   - Object Identifier                 */
/*                          pData        - Pointer to Data to be set         */
/*                                                                           */
/*    Output(s)           : pu4ErrorCode - Error Code                        */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpTestAllTrackGroupIfTable (INT4 i4Version, UINT4 u4GroupIndex,
                              INT4 i4IfIndex, UINT4 u4ObjectId,
                              tSNMP_MULTI_DATA_TYPE * pData,
                              UINT4 *pu4ErrorCode)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    BOOL1               b1VersionMatched = FALSE;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (gi4VrrpNodeVersion == VRRP_VERSION_2_3)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_V2_V3_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pTrackGroupEntry = VrrpDbGetTrackGroupEntry (u4GroupIndex);

    if (pTrackGroupEntry == NULL)
    {
        CLI_SET_ERR (CLI_VRRP_NO_TRACK_GROUP);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pTrackGroupIfEntry = VrrpDbGetTrackGroupIfEntry (pTrackGroupEntry,
                                                     i4IfIndex);

    switch (u4ObjectId)
    {
        case VRRP_MIB_TRACK_IF_ROW_STATUS:
            if ((pData->i4_SLongValue != CREATE_AND_GO) &&
                (pData->i4_SLongValue != DESTROY))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_ROW_STATUS);

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (pData->i4_SLongValue == CREATE_AND_GO)
            {
                if (pTrackGroupIfEntry != NULL)
                {
                    CLI_SET_ERR (CLI_VRRP_DUP_TRACK_IF);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if (u4GroupIndex == 0)
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_GROUP);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if (CfaGetIfInfo ((UINT4) i4IfIndex, &CfaIfInfo) == CFA_FAILURE)
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_IFINDEX);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if (!((CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
                      ((CfaIfInfo.u1IfType == CFA_ENET) &&
                       (CfaIfInfo.u1BridgedIface == CFA_DISABLED))))
                {
                    CLI_SET_ERR (CLI_VRRP_INCORRECT_IFINDEX);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if (pTrackGroupIfEntry == NULL)
                {
                    CLI_SET_ERR (CLI_VRRP_NO_TRACK_IF);

                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }

                if (pTrackGroupEntry->u1TrackedLinks >= (UINT1)
                    TMO_DLL_Count (&(pTrackGroupEntry->TrackGroupIfHead)))
                {
                    CLI_SET_ERR (CLI_VRRP_INVALID_LINKS);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if ((pData->i4_SLongValue == DESTROY) &&
                    (TMO_DLL_Count (&(pTrackGroupEntry->TrackGroupIfHead)) <= 1)
                    && (TMO_DLL_Count (&(pTrackGroupEntry->VrrpOperHead)) > 0))
                {
                    CLI_SET_ERR (CLI_VRRP_DEL_TRACK_IF_WITH_MORE_VR);

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpTestAllScalars                               */
/*                                                                           */
/*    Description         : This function validates VRRP version and if      */
/*                          validation is successful, tests the value        */
/*                          else errors is returned                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          u4ObjectId   - Object Identifier                 */
/*                          pData        - Pointer to Data to be set         */
/*                                                                           */
/*    Output(s)           : pu4ErroCode  - Error Code                        */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
INT4
VrrpTestAllScalars (INT4 i4Version, UINT4 u4ObjectId,
                    tSNMP_MULTI_DATA_TYPE * pData, UINT4 *pu4ErroCode)
{
    BOOL1               b1VersionMatched = TRUE;

    if (u4ObjectId != VRRP_MIB_VERSION)
    {
        b1VersionMatched = VrrpCompCheckVersion (i4Version);
    }

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        *pu4ErroCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((gi4VrrpNodeVersion == VRRP_VERSION_2_3))
    {
        if ((u4ObjectId != VRRP_MIB_VERSION) &&
            (u4ObjectId != VRRP_MIB_GBL_STATUS))
        {
            CLI_SET_ERR (CLI_VRRP_VERSION_V2_V3_ERR);
            *pu4ErroCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_VERSION:
            if ((pData->i4_SLongValue < VRRP_VERSION_2) ||
                (pData->i4_SLongValue > VRRP_VERSION_3))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

                *pu4ErroCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if ((gi4VrrpNodeVersion == VRRP_VERSION_3) &&
                (pData->i4_SLongValue < gi4VrrpNodeVersion))
            {
                CLI_SET_ERR (CLI_VRRP_DOWNGRADE_VERSION_ERROR);

                *pu4ErroCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VRRP_MIB_NOTIF_CTRL:
            if ((pData->i4_SLongValue < VRRP_ENABLE) ||
                (pData->i4_SLongValue > VRRP_DISABLE))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_NOTIF);

                *pu4ErroCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VRRP_MIB_GBL_STATUS:
            if ((pData->i4_SLongValue < VRRP_ENABLE) ||
                (pData->i4_SLongValue > VRRP_DISABLE))
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_RTR_STATUS);

                *pu4ErroCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VRRP_MIB_TRACE_OPTION:
            if ((pData->i4_SLongValue < VRRP_TRC_NONE) ||
                (pData->i4_SLongValue > VRRP_TRC_ALL))
            {
                CLI_SET_ERR (CLI_VRRP_INV_TRC);

                *pu4ErroCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VRRP_MIB_AUTH_DEPRECATE:
            if ((pData->i4_SLongValue < VRRP_AUTHDEPRECATE_ENABLE) ||
                (pData->i4_SLongValue > VRRP_AUTHDEPRECATE_DISABLE))
            {
                CLI_SET_ERR (CLI_VRRP_SET_AUTHDEPRECATE_FLAG);

                *pu4ErroCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
