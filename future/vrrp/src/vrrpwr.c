 /* <!-- $Id: vrrpwr.c,v 1.6 2014/03/11 14:22:22 siva Exp $--> */

#include  "lr.h"
#include  "fssnmp.h"
#include  "ip.h"
#include  "vrrp.h"
#include  "vrrplow.h"
#include  "vrrpwr.h"
#include  "vrrpdb.h"

VOID
RegisterSTDVRR ()
{
    SNMPRegisterMibWithLock (&stdvrrOID, &stdvrrEntry, VrrpLock,
                             VrrpUnLock, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdvrrOID, (const UINT1 *) "stdvrrp");
}

INT4
VrrpNodeVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetVrrpNodeVersion (&(pMultiData->i4_SLongValue)));
}

INT4
VrrpNotificationCntlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetVrrpNotificationCntl (&(pMultiData->i4_SLongValue)));
}

INT4
VrrpNotificationCntlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetVrrpNotificationCntl (pMultiData->i4_SLongValue));
}

INT4
VrrpNotificationCntlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2VrrpNotificationCntl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
VrrpNotificationCntlDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2VrrpNotificationCntl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexVrrpOperTable (tSnmpIndex * pFirstMultiIndex,
                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexVrrpOperTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexVrrpOperTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
VrrpOperVirtualMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetVrrpOperVirtualMacAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          (tMacAddr *) pMultiData->
                                          pOctetStrValue->pu1_OctetList));

}

INT4
VrrpOperStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperState (pMultiIndex->pIndex[0].i4_SLongValue,
                                 pMultiIndex->pIndex[1].i4_SLongValue,
                                 &(pMultiData->i4_SLongValue)));

}

INT4
VrrpOperAdminStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperAdminState (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
VrrpOperPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
VrrpOperIpAddrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperIpAddrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
VrrpOperMasterIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperMasterIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
VrrpOperPrimaryIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperPrimaryIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
VrrpOperAuthTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperAuthType (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
VrrpOperAuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
VrrpOperAdvertisementIntervalGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperAdvertisementInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
VrrpOperPreemptModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperPreemptMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
VrrpOperVirtualRouterUpTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperVirtualRouterUpTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
VrrpOperProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
VrrpOperRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpOperTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpOperRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
VrrpOperAdminStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetVrrpOperAdminState (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
VrrpOperPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetVrrpOperPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
VrrpOperPrimaryIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetVrrpOperPrimaryIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiData->u4_ULongValue));

}

INT4
VrrpOperAuthTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetVrrpOperAuthType (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
VrrpOperAuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetVrrpOperAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiIndex->pIndex[1].i4_SLongValue,
                                   pMultiData->pOctetStrValue));

}

INT4
VrrpOperAdvertisementIntervalSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetVrrpOperAdvertisementInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
VrrpOperPreemptModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetVrrpOperPreemptMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
VrrpOperProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetVrrpOperProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiIndex->pIndex[1].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
VrrpOperRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetVrrpOperRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
VrrpOperAdminStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2VrrpOperAdminState (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
VrrpOperPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2VrrpOperPriority (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
VrrpOperPrimaryIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2VrrpOperPrimaryIpAddr (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiData->u4_ULongValue));

}

INT4
VrrpOperAuthTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2VrrpOperAuthType (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
VrrpOperAuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2VrrpOperAuthKey (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiIndex->pIndex[1].i4_SLongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
VrrpOperAdvertisementIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2VrrpOperAdvertisementInterval (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
VrrpOperPreemptModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2VrrpOperPreemptMode (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
VrrpOperProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2VrrpOperProtocol (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
VrrpOperRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2VrrpOperRowStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
VrrpOperTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2VrrpOperTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexVrrpAssoIpAddrTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexVrrpAssoIpAddrTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexVrrpAssoIpAddrTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
VrrpAssoIpAddrRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpAssoIpAddrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpAssoIpAddrRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
VrrpAssoIpAddrRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetVrrpAssoIpAddrRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
VrrpAssoIpAddrRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2VrrpAssoIpAddrRowStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[2].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
VrrpAssoIpAddrTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2VrrpAssoIpAddrTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
VrrpRouterChecksumErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetVrrpRouterChecksumErrors (&(pMultiData->u4_ULongValue)));
}

INT4
VrrpRouterVersionErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetVrrpRouterVersionErrors (&(pMultiData->u4_ULongValue)));
}

INT4
VrrpRouterVrIdErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetVrrpRouterVrIdErrors (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexVrrpRouterStatsTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexVrrpRouterStatsTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexVrrpRouterStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
VrrpStatsBecomeMasterGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsBecomeMaster (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsAdvertiseRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsAdvertiseRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsAdvertiseIntervalErrorsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsAdvertiseIntervalErrors
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsAuthFailuresGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsAuthFailures (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsIpTtlErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsIpTtlErrors (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsPriorityZeroPktsRcvdGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsPriorityZeroPktsRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsPriorityZeroPktsSentGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsPriorityZeroPktsSent
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsInvalidTypePktsRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsInvalidTypePktsRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsAddressListErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsAddressListErrors
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsInvalidAuthTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsInvalidAuthType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsAuthTypeMismatchGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsAuthTypeMismatch
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
VrrpStatsPacketLengthErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceVrrpRouterStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetVrrpStatsPacketLengthErrors
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}
