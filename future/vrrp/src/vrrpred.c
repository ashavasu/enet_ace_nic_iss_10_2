/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: vrrpred.c,v 1.7 2014/08/07 11:27:25 siva Exp $
 *
 * Description: This file contains VRRP Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __VRRPRED_C
#define __VRRPRED_C

#include "vrrpinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpRedDynDataDescInit                          */
/*                                                                           */
/*    Description         : This function will initialize vrrp dynamic info  */
/*                          data descriptor.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VrrpRedDynDataDescInit (VOID)
{
    tDbDataDescInfo    *pVrrpDynDataDesc = NULL;

    pVrrpDynDataDesc = &(gaVrrpDynDataDescList[VRRP_DYN_INFO]);

    pVrrpDynDataDesc->pDbDataHandleFunction = NULL;

    /* Assumption:
     * All dynamic info in data structure are placed after the DbNode.

     * Size of the dynamic info =
     * Total size of structure - offset of first dynamic info
     */

    pVrrpDynDataDesc->u4DbDataSize =
        sizeof (tVrrpOperTable) - FSAP_OFFSETOF (tVrrpOperTable, u1Version);

    pVrrpDynDataDesc->pDbDataOffsetTbl = gaVrrpOffsetTbl;

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpRedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Vrrp descriptor    */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VrrpRedDescrTblInit (VOID)
{
    tDbDescrParams      VrrpDescrParams;
    MEMSET (&VrrpDescrParams, 0, sizeof (tDbDescrParams));

    VrrpDescrParams.u4ModuleId = RM_VRRP_APP_ID;

    VrrpDescrParams.pDbDataDescList = gaVrrpDynDataDescList;

    DbUtilTblInit (&gVrrpDynInfoList, &VrrpDescrParams);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpRedDbNodeInit                                */
/*                                                                           */
/*    Description         : This function will initialize Vrrp Oper db node. */
/*                                                                           */
/*    Input(s)            : pVrrpDbTblNode - pointer to Vrrp Db Entry.       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VrrpRedDbNodeInit (tDbTblNode * pVrrpDbTblNode)
{
    DbUtilNodeInit (pVrrpDbTblNode, VRRP_DYN_INFO);

    return;
}

/************************************************************************
 *  Function Name   : VrrpRedInitGlobalInfo                            
 *                                                                    
 *  Description     : This function is invoked by the VRRP module while
 *                    task initialisation. It initialises the red     
 *                    global variables.
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
VrrpRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedInitGlobalInfo\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Entering VrrpRedInitGlobalInfo"));

    gVrrpRedTable = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tVrrpRedTable, RbNode), VrrpRBTreeRedEntryCmp);

    if (gVrrpRedTable == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Create the RM Packet arrival Q */

    if (OsixQueCrt ((UINT1 *) VRRP_RM_PKT_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    VRRP_RM_QUE_DEPTH, &gVrrpRmPktQId) != OSIX_SUCCESS)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "INIT: Queue Creation Failed\n");
        return OSIX_FAILURE;
    }

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_VRRP_APP_ID;
    RmRegParams.pFnRcvPkt = VrrpRedRmCallBack;

    /* Registering VRRP with RM */
    if (RmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {

        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedInitGlobalInfo: Registration with RM failed\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedInitGlobalInfo: Registration with RM failed"));
        return OSIX_FAILURE;
    }
    VRRP_GET_NODE_STATUS () = RM_INIT;
    VRRP_NUM_STANDBY_NODES () = 0;
    gVrrpRedGblInfo.bBulkReqRcvd = OSIX_FALSE;
    /* BulkReqRcvd is set as FALSE initially. When bulk request is received
     * the flag is set to true. This is checked for sending bulk update
     * message to the standby node */

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Exiting VrrpRedInitGlobalInfo"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedInitGlobalInfo\r \n");

    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : VrrpRedDeInitGlobalInfo                        
 *                                                                    
 * Description        : This function is invoked by the VRRP module    
 *                      during module shutdown and this function      
 *                      deinitializes the redundancy global variables 
 *                      and de-register VRRP with RM.            
 *                                                                    
 * Input(s)           : None                                          
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                   
 ************************************************************************/

INT4
VrrpRedDeInitGlobalInfo (VOID)
{
    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedDeInitGlobalInfo\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Entering VrrpRedDeInitGlobalInfo"));
    /* Deregister from SYSLOG */
    gVrrpRedTable = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tVrrpRedTable, RbNode), VrrpRBTreeRedEntryCmp);

    if (gVrrpRedTable != NULL)
    {
        RBTreeDelete (gVrrpRedTable);
        gVrrpRedTable = NULL;
    }

    if (gVrrpRmPktQId != NULL)
    {
        OsixQueDel (gVrrpRmPktQId);
        gVrrpRmPktQId = IP_ZERO;
    }

    if (RmDeRegisterProtocols (RM_VRRP_APP_ID) == RM_FAILURE)
    {

        SYS_LOG_DEREGISTER (gi4VrrpSysLogId);
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedDeInitGlobalInfo: De-Registration with RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedDeInitGlobalInfo: De-Registration with RM failed"));
        return OSIX_FAILURE;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Exiting VrrpRedDeInitGlobalInfo"));
    SYS_LOG_DEREGISTER (gi4VrrpSysLogId);
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedDeInitGlobalInfo\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : VrrpRedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to VRRP        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
VrrpRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tVrrpRmMsg         *pMsg = NULL;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedRmCallBack\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Entering VrrpRedRmCallBack"));
    /* Callback function for RM events. The event and the message is sent as 
     * parameters to this function */

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedRmCallBack:This event is not associated with RM \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedRmCallBack:This event is not associated with RM"));
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedRmCallBack: Queue Message associated with the event "
                  "is not sent by RM \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedRmCallBack: Queue Message associated with the "
                      "event is not sent by RM"));
        return OSIX_FAILURE;
    }

    pMsg = (tVrrpRmMsg *) MemAllocMemBlk ((tMemPoolId) VRRP_RM_MSG_MEMPOOL_ID);
    if (pMsg == NULL)
    {

        VRRP_DBG (VRRP_TRC_MEMORY,
                  "VrrpRedRmCallBack: Queue message allocation failure\r \n");

        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT4) gi4VrrpSysLogId,
                      " VrrpRedRmCallBack: Queue message allocation failure"));
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            VrrpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }
    MEMSET (pMsg, 0, sizeof (tVrrpRmMsg));

    pMsg->RmCtrlMsg.pData = pData;
    pMsg->RmCtrlMsg.u1Event = u1Event;
    pMsg->RmCtrlMsg.u2DataLen = u2DataLen;

    /* Send the message associated with the event to VRRP module in a queue */
    if (OsixQueSend (VRRP_RM_PKT_Q_ID,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock ((tMemPoolId) VRRP_RM_MSG_MEMPOOL_ID,
                            (UINT1 *) pMsg);

        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            VrrpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        VRRP_DBG (VRRP_TRC_ALL, "VrrpRedRmCallBack: Q send failure\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedRmCallBack: Q send failure"));
        return OSIX_FAILURE;
    }

    /* Post a event to VRRP to process RM events */
    OsixEvtSend (gVrrpTaskId, VRRP_RM_PKT_EVENT);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Exiting VrrpRedRmCallBack"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedRmCallBack\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : VrrpRedHandleRmEvents                          
 *                                                                    
 * Description        : This function is invoked by the VRRP module to 
 *                      process all the events and messages posted by 
 *                      the RM module                                 
 *                                                                    
 * Input(s)           : pMsg -- Pointer to the VRRP Q Msg       
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : None                                          
 ************************************************************************/

PUBLIC VOID
VrrpRedHandleRmEvents ()
{
    tVrrpRmMsg         *pMsg = NULL;
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedHandleRmEvents\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Entering VrrpRedHandleRmEvents"));
    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    while (OsixQueRecv (VRRP_RM_PKT_Q_ID,
                        (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pMsg->RmCtrlMsg.u1Event)
        {
            case GO_ACTIVE:

                VRRP_DBG (VRRP_TRC_ALL,
                          "VrrpRedHandleRmEvents:Received GO_ACTIVE event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                              "VrrpRedHandleRmEvents: Received GO_ACTIVE"
                              " event"));
                VrrpRedHandleGoActive ();
                break;
            case GO_STANDBY:
                VRRP_DBG (VRRP_TRC_ALL,
                          "VrrpRedHandleRmEvents:Received GO_STANDBY event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                              "VrrpRedHandleRmEvents:Received GO_STANDBY event"));
                VrrpRedHandleGoStandby (pMsg);
                /* pMsg is passed as a argument to GoStandby function and
                 * it is released there. If the transformation is from active 
                 * to standby then there is no need for releasing the mempool
                 * as we are calling DeInit function. This function clears
                 * all the mempools, so it is returned here instead of break.*/
                return;
            case RM_STANDBY_UP:
                VRRP_DBG (VRRP_TRC_ALL,
                          "VrrpRedHandleRmEvents: Received RM_STANDBY_UP"
                          " event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                              "VrrpRedHandleRmEvents: Received RM_STANDBY_UP"
                              " event"));
                /* Standby up event, number of standby node is updated. 
                 * BulkReqRcvd flag is checked, if it is true, then Bulk update
                 * message is sent to the standby node and the flag is reset */

                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                VRRP_NUM_STANDBY_NODES () = pData->u1NumStandby;
                VrrpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                if (VRRP_RM_BULK_REQ_RCVD () == OSIX_TRUE)
                {
                    VRRP_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                    gVrrpRedGblInfo.u1BulkUpdStatus = VRRP_HA_UPD_NOT_STARTED;
                    VrrpRedSendBulkUpdMsg ();
                }
                break;
            case RM_STANDBY_DOWN:
                VRRP_DBG (VRRP_TRC_ALL,
                          "VrrpRedHandleRmEvents: Received RM_STANDBY_DOWN "
                          "event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                              "VrrpRedHandleRmEvents: Received RM_STANDBY_DOWN "
                              "event"));
                /* Standby down event, number of standby nodes is updated */
                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                VRRP_NUM_STANDBY_NODES () = pData->u1NumStandby;
                VrrpRedRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            case RM_MESSAGE:
                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pMsg->RmCtrlMsg.pData, &u4SeqNum);
                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pMsg->RmCtrlMsg.pData,
                                     pMsg->RmCtrlMsg.u2DataLen);
                VRRP_DBG (VRRP_TRC_ALL,
                          "VrrpRedHandleRmEvents:Received RM_MESSAGE event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                              "VrrpRedHandleRmEvents:Received RM_MESSAGE event"));
                ProtoAck.u4AppId = RM_VRRP_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (gVrrpRedGblInfo.u1NodeStatus == RM_ACTIVE)
                {
                    /* Process the message at active */
                    VrrpRedProcessPeerMsgAtActive (pMsg->RmCtrlMsg.pData,
                                                   pMsg->RmCtrlMsg.u2DataLen);
                }
                else if (gVrrpRedGblInfo.u1NodeStatus == RM_STANDBY)
                {
                    /* Process the message at standby */
                    VrrpRedProcessPeerMsgAtStandby (pMsg->RmCtrlMsg.pData,
                                                    pMsg->RmCtrlMsg.u2DataLen);
                }
                else
                {
                    /* Message is received at the idle node so ignore */
                    VRRP_DBG (VRRP_TRC_ALL,
                              "VrrpRedHandleRmEvents: Sync-up message received"
                              " at Idle Node!!!!\r\n");
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                                  "VrrpRedHandleRmEvents: Sync-up message "
                                  "received at Idle Node"));
                }

                RM_FREE (pMsg->RmCtrlMsg.pData);
                RmApiSendProtoAckToRM (&ProtoAck);
                break;
            case RM_CONFIG_RESTORE_COMPLETE:
                /* Config restore complete event is sent by the RM on completing
                 * the static configurations. If the node status is init, 
                 * and the rm state is standby, then the VRRP status is changed 
                 * from idle to standby */
                if (gVrrpRedGblInfo.u1NodeStatus == RM_INIT)
                {
                    if (VRRP_GET_RMNODE_STATUS () == RM_STANDBY)
                    {
                        VrrpRedHandleIdleToStandby ();

                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                        RmApiHandleProtocolEvent (&ProtoEvt);
                    }
                }
                break;
            case L2_INITIATE_BULK_UPDATES:
                /* L2 Initiate bulk update is sent by RM to the standby node 
                 * to send a bulk update request to the active node */
                VRRP_DBG (VRRP_TRC_ALL,
                          "VrrpRedHandleRmEvents: Received "
                          "L2_INITIATE_BULK_UPDATES \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                              "VrrpRedHandleRmEvents: Received "
                              "L2_INITIATE_BULK_UPDATES"));
                VrrpRedSendBulkReqMsg ();
                break;
            default:
                VRRP_DBG (VRRP_TRC_ALL,
                          "VrrpRedHandleRmEvents:Invalid RM event received\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT4) gi4VrrpSysLogId,
                              "VrrpRedHandleRmEvents: Invalid RM event received"));
                break;

        }
        MemReleaseMemBlock (VRRP_RM_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Exiting VrrpRedHandleRmEvents"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedHandleRmEvents\r \n");
    return;
}

/************************************************************************
 * Function Name      : VrrpRedHandleGoActive                          
 *                                                                      
 * Description        : This function is invoked by the VRRP upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
VrrpRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedHandleGoActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Entering VrrpRedHandleGoActive"));
    ProtoEvt.u4AppId = RM_VRRP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    gVrrpRedGblInfo.u1BulkUpdStatus = VRRP_HA_UPD_NOT_STARTED;

    if (VRRP_GET_NODE_STATUS () == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedHandleGoActive: GO_ACTIVE event reached when node "
                  "is already active \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedHandleGoActive: GO_ACTIVE event reached when "
                      "node is already active"));
        return;
    }
    if (VRRP_GET_NODE_STATUS () == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedHandleGoActive: Idle to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedHandleGoActive: Idle to Active transition"));

        VrrpRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    if (VRRP_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go active received by standby node, 
         * Do hardware audit, and start the timers for all the arp entries 
         * and change the state to active */
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedHandleGoActive: Standby to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedHandleGoActive: Standby to Active transition"));
        VrrpRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (VRRP_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        VRRP_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gVrrpRedGblInfo.u1BulkUpdStatus = VRRP_HA_UPD_NOT_STARTED;
        VrrpRedSendBulkUpdMsg ();
    }
    RmApiHandleProtocolEvent (&ProtoEvt);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Exiting VrrpRedHandleGoActive"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedHandleGoActive\r \n");
    return;
}

/************************************************************************
 * Function Name      : VrrpRedHandleGoStandby                         
 *                                                                    
 * Description        : This function is invoked by the VRRP upon
 *                      receiving the GO_STANBY indication from RM    
 *                      module. And this function responds to RM module 
 *                      with an acknowledgement.                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
VrrpRedHandleGoStandby (tVrrpRmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedHandleGoStandby\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Entering VrrpRedHandleGoStandby"));
    ProtoEvt.u4AppId = RM_VRRP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to Update not started */
    MemReleaseMemBlock (VRRP_RM_MSG_MEMPOOL_ID, (UINT1 *) pMsg);
    gVrrpRedGblInfo.u1BulkUpdStatus = VRRP_HA_UPD_NOT_STARTED;

    if (VRRP_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go standby received by standby node. So ignore */
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedHandleGoStandby: GO_STANDBY event reached when "
                  "node is already in standby \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already in standby"));
        return;
    }
    if (VRRP_GET_NODE_STATUS () == RM_INIT)
    {
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedHandleGoStandby: GO_STANDBY event reached when node "
                  "is already idle \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already idle"));

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else
    {
        /* Go standby received by active event. Clear all the dynamic data and
         * populate again by bulk updates */
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedHandleGoStandby: Active to Standby transition..\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                      "VrrpRedHandleGoStandby: Active to Standby transition"));
        VrrpRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Exiting VrrpRedHandleGoStandby"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedHandleGoStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : VrrpRedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VrrpRedHandleIdleToActive (VOID)
{
    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedHandleIdleToActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Entering VrrpRedHandleIdleToActive"));

    /* Node status is set to active and the number of standby nodes 
     * are updated */
    VRRP_GET_NODE_STATUS () = RM_ACTIVE;
    VRRP_RM_GET_NUM_STANDBY_NODES_UP ();

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Exiting VrrpRedHandleIdleToActive"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedHandleIdleToActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : VrrpRedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VrrpRedHandleIdleToStandby (VOID)
{
    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedHandleIdleToStandby\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Entering VrrpRedHandleIdleToStandby"));

    /* the node status is set to standby and no of standby nodes is set to 0 */
    VRRP_GET_NODE_STATUS () = RM_STANDBY;
    VRRP_NUM_STANDBY_NODES () = 0;

    VRRP_DBG (VRRP_TRC_ALL,
              "VrrpRedHandleIdleToStandby: Node Status Idle to Standby\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "VrrpRedHandleIdleToStandby: Node Status Idle to Standby"));
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Exiting VrrpRedHandleIdleToStandby"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedHandleIdleToStandby\r \n");

    return;
}

/************************************************************************/
/* Function Name      : VrrpStartTimers                                 */
/*                                                                      */
/* Description        : This is a will start all the necessary timers   */
/*                      for all the VRRP instances. It will walk        */
/*                      array and start the timers. If the VRRP state   */
/*                      is Master, then Advertisement timer is started. */
/*                      If the state is Backup, then Master-Down timer  */
/*                      is started.                                     */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : VRRP_OK/VRRP_NOT_OK                             */
/************************************************************************/

PUBLIC INT4
VrrpStartTimers ()
{
    tVrrpTimerRef      *pTmrRef = NULL;
    INT4                i4OperIndex = 0;
    FLT4                fTime = 0.0;

    for (i4OperIndex = 0; i4OperIndex < (INT4) gu4VrrpMaxOperEntries;
         i4OperIndex++)
    {
        if (OPERIFINDEX (i4OperIndex) == 0)
        {
            continue;
        }

        VrrpIpifJoinMcastGroup (i4OperIndex);

        if (OPERSTATE (i4OperIndex) == INITIAL_STATE)
        {
            if ((OPERADMINSTATE (i4OperIndex) == ADMINUP) &&
                (OPERROWSTATUS (i4OperIndex) == ACTIVE))
            {
                VrrpSem (STARTUP_EVENT, i4OperIndex, OPERPRIORITY (i4OperIndex),
                         0);
            }
        }
        else if (OPERSTATE (i4OperIndex) == BACKUP_STATE)
        {
            OPERADMINSTATE (i4OperIndex) = ADMINUP;

            pTmrRef = &(OPERTMR (i4OperIndex));
            pTmrRef->u1TimerType = MASTERDOWN_TIMER;
            pTmrRef->i4OperIndex = i4OperIndex;

            fTime = VrrpCompCalcMasterDownInterval (i4OperIndex);

            TmrIfStartTimer (pTmrRef, fTime);
        }
        else
        {
            /* Non -Zero Priority Packets to be transmitted */
            if ((PktProcessTransmitPacket (i4OperIndex, VRRP_FALSE))
                == VRRP_NOT_OK)
            {
                VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                          "RED : -E- Failure in Transmitting Adver packet:\n");
            }

            /* Send Gratitous Arp */
            if (VrrpSendGratArp (i4OperIndex) == VRRP_NOT_OK)
            {

                VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                          "RED : -E- Failure in Transmitting Gratituous Arp:\n");
            }

            if (VrrpCreateSecIpOrJoinSoliMCast (i4OperIndex, gVrrpZeroIpAddr,
                                                FALSE) == VRRP_NOT_OK)
            {
                VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                          "RED: Failure in creating secondary IP\n");
            }

            if (VrrpSendNDNeighborAdvtToAllAssocIp (i4OperIndex) == VRRP_NOT_OK)
            {
                VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                          "RED : Failure in transmitting ND Neighbor Advertisement\n");
            }

            /* Set Adver Timer to Advertisement interval */
            pTmrRef = &OPERTMR (i4OperIndex);
            pTmrRef->u1TimerType = ADVER_TIMER;
            pTmrRef->i4OperIndex = i4OperIndex;
            TmrIfStartTimer (pTmrRef, OPERADVERINTL (i4OperIndex));
        }
    }
    return VRRP_OK;
}

/************************************************************************/
/* Function Name      : VrrpRedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VrrpRedHandleStandbyToActive (VOID)
{
    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedHandleStandbyToActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Entering VrrpRedHandleStandbyToActive"));

    /* Hardware audit is done and the hardware and software entries are 
     * checked for synchronization */
    VrrpRedHwAudit ();

    VRRP_GET_NODE_STATUS () = RM_ACTIVE;
    VRRP_RM_GET_NUM_STANDBY_NODES_UP ();

    SelAddFd (gi4VrrpIpv4Socket, VrrpPktInSocket);
    SelAddFd (gi4VrrpIpv6Socket, VrrpPktInIpv6Socket);

    /* Timers to be started, check the dynamic arp entry. 
     * If in pending/ageout state, then start the arp age out 
     * timer and send the arp request. If in dynamic state,
     * start the arp cache timer. */

    VrrpStartTimers ();

    VRRP_DBG (VRRP_TRC_ALL,
              "VrrpRedHandleStandbyToActive: Node Status Standby to "
              "Active\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "VrrpRedHandleStandbyToActive: Node Status Standby to Active"));
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, (UINT4) gi4VrrpSysLogId,
                  "Exiting VrrpRedHandleStandbyToActive"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedHandleStandbyToActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : VrrpRedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VrrpRedHandleActiveToStandby (VOID)
{
    tVrrpTimerRef      *pTmrRef = NULL;
    INT4                i4OperIndex = 0;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedHandleActiveToStandby\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedHandleActiveToStandby"));

    /*update the statistics */
    VRRP_GET_NODE_STATUS () = RM_STANDBY;
    VRRP_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Stop the running timers in the active node. Check whether any timers are 
     * running, if running, stop those timers. If there are any messages in the 
     * queue, discard those messages. Delete all the dynamically learnt entries
     * and flush those memories. These entries will be learnt from the new
     * active node through bulk updated */

    for (i4OperIndex = 0; i4OperIndex < (INT4) gu4VrrpMaxOperEntries;
         i4OperIndex++)
    {
        if (OPERIFINDEX (i4OperIndex) == 0)
        {
            break;
        }

        pTmrRef = &(OPERTMR (i4OperIndex));

        TmrIfStopTimer (pTmrRef);

        VRRP_DBG (VRRP_TRC_TIMERS, "SEM: Cancelling Master Down Timer\n");
        VRRP_IPVX_ADDR_CLEAR (OPERMASTERIPADDR (i4OperIndex));
        OPERSTATE (i4OperIndex) = INITIAL_STATE;
    }

    VRRP_DBG (VRRP_TRC_ALL,
              "VrrpRedHandleActiveToStandby: Node Status Active to "
              "Standby\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "VrrpRedHandleActiveToStandby: Node Status Active to Standby"));
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedHandleActiveToStandby"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedHandleActiveToStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : VrrpRedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VrrpRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedProcessPeerMsgAtActive\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedProcessPeerMsgAtActive"));
    ProtoEvt.u4AppId = RM_VRRP_APP_ID;

    VRRP_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
    VRRP_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

    if (u2OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u2Length != VRRP_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u1MsgType == VRRP_RED_BULK_REQ_MESSAGE)
    {
        gVrrpRedGblInfo.u1BulkUpdStatus = VRRP_HA_UPD_NOT_STARTED;
        if (!VRRP_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gVrrpRedGblInfo.bBulkReqRcvd = OSIX_TRUE;

            VRRP_DBG (VRRP_TRC_ALL,
                      "VrrpRedProcessPeerMsgAtActive:Bulk request message "
                      "before RM_STANDBY_UP\r \n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                          "VrrpRedProcessPeerMsgAtActive:Bulk request message "
                          "before RM_STANDBY_UP"));
            return;
        }
        VrrpRedSendBulkUpdMsg ();
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedProcessPeerMsgAtActive"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedProcessPeerMsgAtActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : VrrpRedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VrrpRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2ExtractMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedProcessPeerMsgAtStandby\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedProcessPeerMsgAtStandby"));
    ProtoEvt.u4AppId = RM_VRRP_APP_ID;
    u2MinLen = VRRP_RED_TYPE_FIELD_SIZE + VRRP_RED_LEN_FIELD_SIZE;

    while (u2OffSet <= u2DataLen)
    {
        u2ExtractMsgLen = u2OffSet;
        VRRP_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

        VRRP_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            u2OffSet += u2Length;
            continue;
        }
        /* If extracting information upto the length present in the length
         * field goes beyond the total length, means length is not corrent
         * discard the remaining information.
         */
        u2ExtractMsgLen += u2Length;

        if (u2ExtractMsgLen > u2DataLen)
        {
            /* If there is a length mismatch between the message and the given 
             * length, ignore the message and send error to RM */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;

            RmApiHandleProtocolEvent (&ProtoEvt);
            VRRP_DBG (VRRP_TRC_ALL,
                      "VrrpRedProcessPeerMsgAtStandby:RM_PROCESS Failure\r \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4VrrpSysLogId,
                          "VrrpRedProcessPeerMsgAtStandby:RM_PROCESS Failure"));
            return;
        }

        switch (u1MsgType)
        {
            case VRRP_RED_BULK_UPD_TAIL_MESSAGE:
                VrrpRedProcessBulkTailMsg (pMsg, &u2OffSet);
                break;
            case VRRP_RED_DYN_CACHE_INFO:
                VrrpRedProcessDynamicInfo (pMsg, &u2OffSet);
                break;
        }
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedProcessPeerMsgAtStandby"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedProcessPeerMsgAtStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : VrrpRedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the VRRP module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
VrrpRedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedRmReleaseMemoryForMsg\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedRmReleaseMemoryForMsg"));
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {

        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedRmReleaseMemoryForMsg:Failure in releasing allocated"
                  " memory\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4VrrpSysLogId,
                      "VrrpRedRmReleaseMemoryForMsg:Failure in releasing "
                      "allocated memory"));
        return OSIX_FAILURE;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedRmReleaseMemoryForMsg"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedRmReleaseMemoryForMsg\r \n");

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : VrrpRedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
VrrpRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = 0;
    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedSendMsgToRm\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedSendMsgToRm"));
    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_VRRP_APP_ID, RM_VRRP_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedSendMsgToRm:Freememory due to message send "
                  "failure\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4VrrpSysLogId,
                      "VrrpRedSendMsgToRm:Freememory due to message send"
                      " failure"));
        return OSIX_FAILURE;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedSendMsgToRm"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedSendMsgToRm\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : VrrpRedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VrrpRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = 0;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedSendBulkReqMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedSendBulkReqMsg"));
    ProtoEvt.u4AppId = RM_VRRP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    VRRP Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (VRRP_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedSendBulkReqMsg: RM Memory allocation failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4VrrpSysLogId,
                      "VrrpRedSendBulkReqMsg: RM Memory allocation failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    VRRP_RM_PUT_1_BYTE (pMsg, &u2OffSet, VRRP_RED_BULK_REQ_MESSAGE);
    VRRP_RM_PUT_2_BYTE (pMsg, &u2OffSet, VRRP_RED_BULK_REQ_MSG_SIZE);

    if (VrrpRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedSendBulkReqMsg: Send message to RM failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4VrrpSysLogId,
                      "VrrpRedSendBulkReqMsg: Send message to RM failed"));
        return;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedSendBulkReqMsg"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedSendBulkReqMsg\r \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpRedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pVrrpDataDesc - This is Vrrp sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pVrrpDbNode - This is db node defined in the VRRP*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VrrpRedDbUtilAddTblNode (tDbTblDescriptor * pVrrpDataDesc,
                         tDbTblNode * pVrrpDbNode)
{
    if ((VRRP_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gVrrpRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilAddTblNode (pVrrpDataDesc, pVrrpDbNode);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpRedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate VRRP dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VrrpRedSyncDynInfo (VOID)
{
    if ((VRRP_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gVrrpRedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }

    DbUtilSyncModuleNodes (&gVrrpDynInfoList);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpRedAddAllNodeInDbTbl                         */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
VrrpRedAddAllNodeInDbTbl (VOID)
{
    INT4                i4OperIndex = 0;

    for (i4OperIndex = 0; i4OperIndex < (INT4) gu4VrrpMaxOperEntries;
         i4OperIndex++)
    {
        if (OPERIFINDEX (i4OperIndex) == 0)
        {
            break;
        }

        VrrpRedDbUtilAddTblNode (&gVrrpDynInfoList, &OPERDBNODE (i4OperIndex));
    }

    return;
}

/************************************************************************/
/* Function Name      : VrrpRedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VrrpRedSendBulkUpdMsg (VOID)
{
    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedSendBulkUpdMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedSendBulkUpdMsg"));
    if (VRRP_IS_STANDBY_UP () == OSIX_FALSE)
    {
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedSendBulkUpMsg:VRRP Standby up failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4VrrpSysLogId,
                      "VrrpRedSendBulkUpMsg:VRRP stand by up failure"));
        return;
    }

    if (gVrrpRedGblInfo.u1BulkUpdStatus == VRRP_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        VrrpRedAddAllNodeInDbTbl ();

        VrrpRedSyncDynInfo ();

        gVrrpRedGblInfo.u1BulkUpdStatus = VRRP_HA_UPD_COMPLETED;
        VrrpRedSendBulkUpdTailMsg ();
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedSendBulkUpdMsg"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedSendBulkUpdMsg\r \n");
    return;

}

/************************************************************************/
/* Function Name      : VrrpRedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the VRRP offers an IP address         */
/*                      to the VRRP client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pVrrpBindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
VrrpRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedSendBulkUpdTailMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedSendBulkUpdTailMsg"));
    ProtoEvt.u4AppId = RM_VRRP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr * VRRP_RED_BULK_UPD_TAIL_MSG * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (VRRP_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedSendBulkUpdTailMsg: RM Memory allocation"
                  " failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4VrrpSysLogId,
                      "VrrpRedSendBulkUpdTailMsg: RM Memory allocation "
                      "failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u2OffSet = 0;

    VRRP_RM_PUT_1_BYTE (pMsg, &u2OffSet, VRRP_RED_BULK_UPD_TAIL_MESSAGE);
    VRRP_RM_PUT_2_BYTE (pMsg, &u2OffSet, VRRP_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (VrrpRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        VRRP_DBG (VRRP_TRC_ALL,
                  "VrrpRedSendBulkUpdTailMsg:Send message to RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gi4VrrpSysLogId,
                      "VrrpRedSendBulkUpdTailMsg:Send message to RM failed"));
        return;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedSendBulkUpdTailMsg"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedSendBulkUpdTailMsg\r \n");
    return;
}

/************************************************************************/
/* Function Name      : VrrpRedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
VrrpRedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_VRRP_APP_ID;
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu2OffSet);
    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedProcessBulkTailMsg\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedProcessBulkTailMsg"));

    VRRP_DBG (VRRP_TRC_ALL,
              "VrrpRedProcessBulkTailMsg: Bulk Update Tail Message received"
              " at Standby node.\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "VrrpRedProcessBulkTailMsg: Bulk Update Tail Message"
                  " received at Standby node"));

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    RmApiHandleProtocolEvent (&ProtoEvt);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedProcessBulkTailMsg"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedProcessBulkTailMsg\r \n");

    return;
}

/************************************************************************/
/* Function Name      : VrrpRedProcessDynamicInfo                        */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the binding information */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u2OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
VrrpRedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tVrrpRedTable      *pVrrpRedNode = NULL;
    tVrrpRedTable       VrrpRedNode;
    UINT4               u4IpAddr = 0;
    UINT4               u4RetVal = 0;
    INT4                i4OperIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4VrrpOperVrId = 0;
    INT4                i4RcvdAdvtInterval = 0;
    UINT1               aTempAddr[IPVX_MAX_INET_ADDR_LEN + 4];
    UINT1               aTempPad[3];
    UINT1               aIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1AddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
    UINT1               u1Version = 0;
    UINT1               u1HwStatus = 0;
    UINT1               u1TrackedIfStatus = 0;
    BOOL1               b1V3AdvtRcvd = 0;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedProcessDynamicInfo\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedProcessDynamicInfo"));

    MEMSET (&VrrpRedNode, 0, sizeof (tVrrpRedTable));
    MEMSET (aTempAddr, 0, (IPVX_MAX_INET_ADDR_LEN + 4));
    MEMSET (aTempPad, 0, sizeof (aTempPad));
    MEMSET (aIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    /* First check the first byte. If first byte is 2, ACTIVE is
     * in sync with STANDBY. Else, ACTIVE is in older version. 
     * Decoding should be done based on this. */
    RM_GET_DATA_1_BYTE (pMsg, *pu2OffSet, u1Version);

    if (u1Version == VRRP_HA_VERSION)
    {
        VRRP_RM_GET_1_BYTE (pMsg, pu2OffSet, u1Version);
        VRRP_RM_GET_N_BYTE (pMsg, pu2OffSet, aTempPad, 3);
        VRRP_RM_GET_4_BYTE (pMsg, pu2OffSet, i4IfIndex);
        VRRP_RM_GET_4_BYTE (pMsg, pu2OffSet, i4VrrpOperVrId);
        VRRP_RM_GET_4_BYTE (pMsg, pu2OffSet, VrrpRedNode.i4OperState);
        VRRP_RM_GET_N_BYTE (pMsg, pu2OffSet, aTempAddr,
                            (IPVX_MAX_INET_ADDR_LEN + 4));
        VRRP_RM_GET_1_BYTE (pMsg, pu2OffSet, u1HwStatus);
        VRRP_RM_GET_1_BYTE (pMsg, pu2OffSet, u1AddrType);
        VRRP_RM_GET_1_BYTE (pMsg, pu2OffSet, u1TrackedIfStatus);
        VRRP_RM_GET_1_BYTE (pMsg, pu2OffSet, b1V3AdvtRcvd);
        VRRP_RM_GET_4_BYTE (pMsg, pu2OffSet, i4RcvdAdvtInterval);
    }
    else
    {
        VRRP_RM_GET_4_BYTE (pMsg, pu2OffSet, i4IfIndex);
        VRRP_RM_GET_4_BYTE (pMsg, pu2OffSet, i4VrrpOperVrId);
        VRRP_RM_GET_4_BYTE (pMsg, pu2OffSet, VrrpRedNode.i4OperState);
        VRRP_RM_GET_4_BYTE (pMsg, pu2OffSet, u4IpAddr);
        VRRP_RM_GET_1_BYTE (pMsg, pu2OffSet, u1HwStatus);
        VRRP_RM_GET_N_BYTE (pMsg, pu2OffSet, aTempPad, VRRP_THREE);

        u4IpAddr = OSIX_HTONL (u4IpAddr);
        MEMCPY (aTempAddr, (UINT1 *) &u4IpAddr, IPVX_IPV4_ADDR_LEN);
    }

    i4OperIndex = VrrpDbGetOperEntry (i4IfIndex, i4VrrpOperVrId, u1AddrType);

    if (i4OperIndex == VRRP_NOT_OK)
    {
        RM_FREE (pMsg);
        return;
    }

    if (u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&aIpAddr[0], &aTempAddr[4], IPVX_IPV4_ADDR_LEN);
    }
    else
    {
        MEMCPY (&aIpAddr[0], &aTempAddr[4], IPVX_IPV6_ADDR_LEN);
    }

    VRRP_IPVX_ADDR_INIT_IPVX (VrrpRedNode.MasterIpAddr, u1AddrType, aIpAddr);
    VRRPOPERHWSTATUS (i4OperIndex) = u1HwStatus;
    OPERTRACKIFSTATUS (i4OperIndex) = u1TrackedIfStatus;
    OPERV3ADVTRCVD (i4OperIndex) = b1V3AdvtRcvd;
    OPERRCVDMASTERADVTINT (i4OperIndex) = i4RcvdAdvtInterval;

    VRRP_DBG3 (VRRP_TRC_EVENTS,
               "Data synchronized from Active Node for IfIndex: %d "
               "VRID: %d Addr Type: %d is\r\n",
               i4IfIndex, i4VrrpOperVrId, u1AddrType);

    VRRP_DBG4 (VRRP_TRC_EVENTS,
               "Master IP: %s State: %d HwStatus: %d TrackIfStatus: %d\r\n",
               VRRP_IPVX_PRINT_ADDR (VrrpRedNode.MasterIpAddr, u1AddrType),
               VrrpRedNode.i4OperState, u1HwStatus, u1TrackedIfStatus);

    VRRP_DBG2 (VRRP_TRC_EVENTS,
               "V3AdvtStatus: %d RcvdAdvtInterval: %d\r\n",
               b1V3AdvtRcvd, i4RcvdAdvtInterval);

    if (VRRPOPERHWSTATUS (i4OperIndex) == NP_UPDATED)
    {
        VrrpRedNode.i4IfIndex = OPERIFINDEX (i4OperIndex);
        VrrpRedNode.i4VrId = OPERVRID (i4OperIndex);
        VrrpRedNode.u1AddressType = OPERADDRTYPE (i4OperIndex);

        pVrrpRedNode = RBTreeGet (gVrrpRedTable, (tRBElem *) & VrrpRedNode);

        if (pVrrpRedNode != NULL)
        {
            RBTreeRem (gVrrpRedTable, pVrrpRedNode);
            MemReleaseMemBlock (VRRP_DYN_MSG_MEMPOOL_ID,
                                (UINT1 *) pVrrpRedNode);
        }

        OPERSTATE (i4OperIndex) = VrrpRedNode.i4OperState;
        VRRP_IPVX_COPY (OPERMASTERIPADDR (i4OperIndex),
                        VrrpRedNode.MasterIpAddr);
    }
    else
    {
        VrrpRedNode.i4IfIndex = OPERIFINDEX (i4OperIndex);
        VrrpRedNode.i4VrId = OPERVRID (i4OperIndex);
        VrrpRedNode.u1AddressType = OPERADDRTYPE (i4OperIndex);

        pVrrpRedNode = RBTreeGet (gVrrpRedTable, (tRBElem *) & VrrpRedNode);

        if (pVrrpRedNode == NULL)
        {
            pVrrpRedNode = (tVrrpRedTable *) MemAllocMemBlk
                ((tMemPoolId) VRRP_DYN_MSG_MEMPOOL_ID);

            if (pVrrpRedNode == NULL)
            {
                VRRP_DBG (VRRP_TRC_MEMORY,
                          "Memory Allocation failed for RED DYN MSG\r\n");

                return;
            }
            pVrrpRedNode->i4IfIndex = OPERIFINDEX (i4OperIndex);
            pVrrpRedNode->i4VrId = OPERVRID (i4OperIndex);
            pVrrpRedNode->u1AddressType = OPERADDRTYPE (i4OperIndex);

            u4RetVal = RBTreeAdd (gVrrpRedTable, pVrrpRedNode);
        }

        pVrrpRedNode->i4OperState = VrrpRedNode.i4OperState;
        VRRP_IPVX_COPY (pVrrpRedNode->MasterIpAddr, VrrpRedNode.MasterIpAddr);
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedProcessDynamicInfo"));
    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedProcessDynamicInfo\r \n");
    UNUSED_PARAM (u4RetVal);
    return;
}

/************************************************************************
 * Function Name      : VrrpRedHwAudit                                  
 *                                                                      
 * Description        : This function does the hardware audit in the    
 *                      following approach.                             
 *                                                                      
 *                      When there is a transaction between standby and 
 *                      active node, the VrrpRedTable is walked, if     
 *                      there  are any entries in the table, they are   
 *                      verified with the hardware, if the entry is     
 *                      present in the hardware, then the entry is      
 *                      added to the sofware. If not, the entry is      
 *                      deleted.                                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/
VOID
VrrpRedHwAudit ()
{
    tVrrpRedTable      *pVrrpRedInfo = NULL;
    UINT4               u4OutCome = VRRP_NOT_OK;
    INT4                i4OperIndex = 0;
    tVrrpNwIntf         VrrpNwInIntf;
    tVrrpNwIntf         VrrpNwOutIntf;

    VRRP_DBG (VRRP_TRC_ALL, "Entering VrrpRedHwAudit\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Entering VrrpRedHwAudit"));

    /* HW Audit for VRRP trust entries in hardware and software is
     * adjusted accordingly. */

    pVrrpRedInfo = RBTreeGetFirst (gVrrpRedTable);
    while (pVrrpRedInfo != NULL)
    {

        MEMSET (&VrrpNwInIntf, 0, sizeof (tVrrpNwIntf));
        MEMSET (&VrrpNwOutIntf, 0, sizeof (tVrrpNwIntf));

        VrrpNwInIntf.u4IfIndex = pVrrpRedInfo->i4IfIndex;
        VrrpNwInIntf.u4VrId = pVrrpRedInfo->i4VrId;
        VrrpNwInIntf.u1AddrType = pVrrpRedInfo->u1AddressType;

        u4OutCome = VrrpNetIpGetWr (&VrrpNwInIntf, &VrrpNwOutIntf);

        i4OperIndex = VrrpDbGetOperEntry (pVrrpRedInfo->i4IfIndex,
                                          pVrrpRedInfo->i4VrId,
                                          pVrrpRedInfo->u1AddressType);

        if (i4OperIndex < ZERO)
        {
            RBTreeRem (gVrrpRedTable, pVrrpRedInfo);
            MemReleaseMemBlock (VRRP_DYN_MSG_MEMPOOL_ID,
                                (UINT1 *) pVrrpRedInfo);
            pVrrpRedInfo = RBTreeGetFirst (gVrrpRedTable);
            continue;
        }

        if (u4OutCome == VRRP_OK)
        {
            if (OPERSTATE (i4OperIndex) != MASTER_STATE)
            {
                OPERSTATE (i4OperIndex) = MASTER_STATE;
                VRRP_IPVX_COPY (OPERMASTERIPADDR (i4OperIndex),
                                pVrrpRedInfo->MasterIpAddr);
            }
        }
        else
        {
            if (OPERSTATE (i4OperIndex) == MASTER_STATE)
            {
                OPERSTATE (i4OperIndex) = pVrrpRedInfo->i4OperState;
                VRRP_IPVX_COPY (OPERMASTERIPADDR (i4OperIndex),
                                pVrrpRedInfo->MasterIpAddr);
            }
        }

        VRRPOPERHWSTATUS (i4OperIndex) = NP_UPDATED;
        RBTreeRem (gVrrpRedTable, pVrrpRedInfo);
        MemReleaseMemBlock (VRRP_DYN_MSG_MEMPOOL_ID, (UINT1 *) pVrrpRedInfo);
        pVrrpRedInfo = RBTreeGetFirst (gVrrpRedTable);
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gi4VrrpSysLogId,
                  "Exiting VrrpRedHwAudit"));

    VRRP_DBG (VRRP_TRC_ALL, "Exiting VrrpRedHwAudit\r \n");
    return;
}

/*-------------------------------------------------------------------+
 * Function           : VrrpRBTreeRedEntryCmp
 *
 * Input(s)           : pRBElem, pRBElemIn
 *
 * Output(s)          : None.
 *
 * Returns            : VRRP_RB_LESSER  if pRBElem <  pRBElemIn
 *                      VRRP_RB_GREATER if pRBElem >  pRBElemIn
 *                      VRRP_RB_EQUAL   if pRBElem == pRBElemIn
 * Action :
 * This procedure compares the two Redundancy cache entries in lexicographic
 * order of the indices of the Redundancy cache entry.
+-------------------------------------------------------------------*/

INT4
VrrpRBTreeRedEntryCmp (tRBElem * pRBElem, tRBElem * pRBElemIn)
{
    tVrrpRedTable      *pVrrpRed = pRBElem;
    tVrrpRedTable      *pVrrpRedIn = pRBElemIn;

    if (pVrrpRed->i4IfIndex < pVrrpRedIn->i4IfIndex)
    {
        return VRRP_RB_LESSER;
    }
    else if (pVrrpRed->i4IfIndex > pVrrpRedIn->i4IfIndex)
    {
        return VRRP_RB_GREATER;
    }

    if (pVrrpRed->i4VrId < pVrrpRedIn->i4VrId)
    {
        return VRRP_RB_LESSER;
    }
    else if (pVrrpRed->i4VrId > pVrrpRedIn->i4VrId)
    {
        return VRRP_RB_GREATER;
    }

    if (pVrrpRed->u1AddressType < pVrrpRedIn->u1AddressType)
    {
        return VRRP_RB_LESSER;
    }
    else if (pVrrpRed->u1AddressType > pVrrpRedIn->u1AddressType)
    {
        return VRRP_RB_GREATER;
    }

    return VRRP_RB_EQUAL;
}

#endif
