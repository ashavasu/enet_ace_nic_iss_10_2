/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: vrrpget.c,v 1.5 2017/12/16 11:57:11 siva Exp $
 *
 * Description: This file contains Get All routines for all tables
 *              of VRRP.
 *           
 *******************************************************************/
#include "vrrpinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpGetAllOperTable                              */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Oper Table and returns value of requested        */
/*                          Object.                                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          u4ObjectId   - Object Identifier                 */
/*                                                                           */
/*    Output(s)           : pData        - Pointer to Data to be retrived    */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpGetAllOperTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                     INT4 i4AddrType, UINT4 u4ObjectId,
                     tSNMP_MULTI_DATA_TYPE * pData)
{
    BOOL1               b1VersionMatched = FALSE;
    INT4                i4Index = VRRP_NOT_OK;

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return SNMP_FAILURE;
    }

    i4Index = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

    if (i4Index == VRRP_NOT_OK)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_OPER_MASTER_IP:
            VRRP_IPVX_COPY_TO_OCTET_STR (OPERMASTERIPADDR (i4Index),
                                         pData->pOctetStrValue);
            break;

        case VRRP_MIB_OPER_PRIM_IP:
            VRRP_IPVX_COPY_TO_OCTET_STR (OPERPRIMARYIPADDR (i4Index),
                                         pData->pOctetStrValue);
            break;

        case VRRP_MIB_OPER_VIRT_MAC:
            MEMCPY (pData->pOctetStrValue->pu1_OctetList, OPERMACADDR (i4Index),
                    MAC_ADDR_LEN);
            pData->pOctetStrValue->i4_Length = MAC_ADDR_LEN;
            break;

        case VRRP_MIB_OPER_STATUS:
            pData->i4_SLongValue = OPERSTATE (i4Index);
            break;

        case VRRP_MIB_OPER_PRIORITY:
        case VRRP_MIB_OPER_ADMIN_PRIO:
            pData->i4_SLongValue = OPERADMINPRIORITY (i4Index);
            break;

        case VRRP_MIB_OPER_ADDR_COUNT:
            pData->i4_SLongValue = OPERIPADDRCOUNT (i4Index);
            break;

        case VRRP_MIB_OPER_ADVT_INT:
        case VRRP_MIB_OPER_ADVT_INT_MSEC:
            pData->i4_SLongValue = (INT4) OPERADVERINTL (i4Index);
            break;

        case VRRP_MIB_OPER_PREMP_MODE:
            pData->i4_SLongValue = OPERPREEMPTMODE (i4Index);
            break;

        case VRRP_MIB_OPER_ACCEPT_MODE:
            pData->i4_SLongValue = (INT4) OPERACCEPTMODE (i4Index);
            break;

        case VRRP_MIB_OPER_UP_TIME:
            pData->u4_ULongValue = OPERROUTERUPTIME (i4Index);
            break;

        case VRRP_MIB_OPER_ROW_STATUS:
            pData->i4_SLongValue = OPERROWSTATUS (i4Index);
            break;

        case VRRP_MIB_OPER_ADMIN_STATE:
            pData->i4_SLongValue = OPERADMINSTATE (i4Index);
            break;

        case VRRP_MIB_OPER_AUTH_TYPE:
            pData->i4_SLongValue = OPERAUTHTYPE (i4Index);
            break;

        case VRRP_MIB_OPER_AUTH_KEY:
            pData->pOctetStrValue->i4_Length = 0;
            break;

        case VRRP_MIB_OPER_PROTOCOL:
            pData->i4_SLongValue = OPERPROTOCOL (i4Index);
            break;

        case VRRP_MIB_OPER_TRACK_GRP_ID:
            pData->u4_ULongValue = OPERTRACKGRPID (i4Index);
            break;

        case VRRP_MIB_OPER_DEC_PRIORITY:
            pData->u4_ULongValue = (UINT4) OPERDECPRIO (i4Index);
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpGetAllAssocIpTable                           */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Associated IP Address Table and returns value of */
/*                          requested Object.                                */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          pAssocIpAddr - Associated IP Address             */
/*                          u4ObjectId   - Object Identifier                 */
/*                                                                           */
/*    Output(s)           : pData        - Pointer to Data to be retrived    */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpGetAllAssocIpTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                        INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pAssocIpAddr,
                        UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE * pData)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    BOOL1               b1VersionMatched = FALSE;
    INT4                i4Index = VRRP_NOT_OK;
    tIPvXAddr           IpAddr;

    VRRP_IPVX_ADDR_CLEAR (IpAddr);
    VRRP_IPVX_ADDR_INIT_IPVX (IpAddr, (UINT1) i4AddrType,
                              pAssocIpAddr->pu1_OctetList);

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return SNMP_FAILURE;
    }

    i4Index = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

    if (i4Index == VRRP_NOT_OK)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return SNMP_FAILURE;
    }

    pIpAddrEntry = VrrpDbOperGetAssocIpEntry (i4Index, IpAddr);

    if (pIpAddrEntry == NULL)
    {
        CLI_SET_ERR (CLI_VRRP_NO_ASSOC_ENTRY);

        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_ASSO_ROW_STATUS:
            pData->i4_SLongValue = (INT4) pIpAddrEntry->u1AssoIpAddrRowStatus;
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpGetAllStatsTable                             */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Stats Table and returns value of requested       */
/*                          Object.                                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          u4ObjectId   - Object Identifier                 */
/*                                                                           */
/*    Output(s)           : pData        - Pointer to Data to be retrived    */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpGetAllStatsTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                      INT4 i4AddrType, UINT4 u4ObjectId,
                      tSNMP_MULTI_DATA_TYPE * pData)
{
    BOOL1               b1VersionMatched = FALSE;
    INT4                i4Index = VRRP_NOT_OK;

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return SNMP_FAILURE;
    }

    i4Index = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

    if (i4Index == VRRP_NOT_OK)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_STAT_VRRPSEMFAIL:
            pData->u4_ULongValue = STATVRRPSEMFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPSENDNDNEIGHBORADVTFAIL:
            pData->u4_ULongValue = STATVRRPSENDNDNEIGHBORADVTFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPSENDPACKETTOIPV4FAIL:
            pData->u4_ULongValue = STATVRRPSENDPACKETTOIPV4FAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPSENDPACKETTOIPV6FAIL:
            pData->u4_ULongValue = STATVRRPSENDPACKETTOIPV6FAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPGETIPV4PORTFROMOPERINDEXFAIL:
            pData->u4_ULongValue =
                STATVRRPGETIPV4PORTFROMOPERINDEXFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPGETIPV6PORTFROMOPERINDEXFAIL:
            pData->u4_ULongValue =
                STATVRRPGETIPV6PORTFROMOPERINDEXFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPSENDGRATARPFAIL:
            pData->u4_ULongValue = STATVRRPSENDGRATARPFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPETHREGISTERMACADDRFAIL:
            pData->u4_ULongValue = STATVRRPETHREGISTERMACADDRFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPNETIP4DELINTFFAIL:
            pData->u4_ULongValue = STATVRRPNETIP4DELINTFFAIL (i4Index);
            break;
        case VRRP_MIB_STAT_VRRPNETIP6DELINTFFAIL:
            pData->u4_ULongValue = STATVRRPNETIP6DELINTFFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPIPIFJOINMCASTGROUPFAIL:
            pData->u4_ULongValue = STATVRRPIPIFJOINMCASTGROUPFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPIPIFLEAVEMCASTGROUPFAIL:
            pData->u4_ULongValue = STATVRRPIPIFLEAVEMCASTGROUPFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_VRRPSOCKETFAIL:
            pData->u4_ULongValue = STATVRRPSOCKETFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERPACKETFAIL:
            pData->u4_ULongValue =
                STATPKTPROCESSVALIDATEADVERPACKETFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERV2PACKETFAIL:
            pData->u4_ULongValue =
                STATPKTPROCESSVALIDATEADVERV2PACKETFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERV3PACKETFAIL:
            pData->u4_ULongValue =
                STATPKTPROCESSVALIDATEADVERV3PACKETFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_PKTPROCESSTRANSMITV2PACKETFAIL:
            pData->u4_ULongValue = STATPKTPROCESSTRANSMITV2PACKETFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_PKTPROCESSTRANSMITV3PACKETFAIL:
            pData->u4_ULongValue = STATPKTPROCESSTRANSMITV3PACKETFAIL (i4Index);
            break;

        case VRRP_MIB_STAT_MASTER_TRANS:
            pData->u4_ULongValue = STATSBECOMEMASTER (i4Index);
            break;

        case VRRP_MIB_STAT_MASTER_REASON:
            pData->i4_SLongValue = STATSMASTERREASON (i4Index);
            break;

        case VRRP_MIB_STAT_RCVD_ADVT:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSADVERTISERCVD64 (i4Index).u4Lo;
            }
            else
            {
                pData->u8_Counter64Value.msn
                    = STATSADVERTISERCVD64 (i4Index).u4Hi;
                pData->u8_Counter64Value.lsn
                    = STATSADVERTISERCVD64 (i4Index).u4Lo;
            }
            break;

        case VRRP_MIB_STAT_ADVT_INT_ERR:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSADINTLERRORS64 (i4Index).u4Lo;
            }
            else
            {
                pData->u8_Counter64Value.msn
                    = STATSADINTLERRORS64 (i4Index).u4Hi;
                pData->u8_Counter64Value.lsn
                    = STATSADINTLERRORS64 (i4Index).u4Lo;
            }
            break;

        case VRRP_MIB_STAT_IP_TTL_ERR:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSIPTTLERRORS64 (i4Index).u4Lo;
            }
            else
            {
                pData->u8_Counter64Value.msn
                    = STATSIPTTLERRORS64 (i4Index).u4Hi;
                pData->u8_Counter64Value.lsn
                    = STATSIPTTLERRORS64 (i4Index).u4Lo;
            }
            break;

        case VRRP_MIB_STAT_PROTO_ERR_REASON:
            pData->i4_SLongValue = STATSPROTOERRREASON (i4Index);
            break;

        case VRRP_MIB_STAT_RCVD_ZERO_PRI:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSPTYZERORCVD64 (i4Index).u4Lo;
            }
            else
            {
                pData->u8_Counter64Value.msn
                    = STATSPTYZERORCVD64 (i4Index).u4Hi;
                pData->u8_Counter64Value.lsn
                    = STATSPTYZERORCVD64 (i4Index).u4Lo;
            }
            break;

        case VRRP_MIB_STAT_SENT_ZERO_PRI:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSPTYZEROSENT64 (i4Index).u4Lo;
            }
            else
            {
                pData->u8_Counter64Value.msn
                    = STATSPTYZEROSENT64 (i4Index).u4Hi;
                pData->u8_Counter64Value.lsn
                    = STATSPTYZEROSENT64 (i4Index).u4Lo;
            }
            break;

        case VRRP_MIB_STAT_RCVD_INVD_TYPE:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSINVALIDTYPE64 (i4Index).u4Lo;
            }
            else
            {
                pData->u8_Counter64Value.msn
                    = STATSINVALIDTYPE64 (i4Index).u4Hi;
                pData->u8_Counter64Value.lsn
                    = STATSINVALIDTYPE64 (i4Index).u4Lo;
            }
            break;

        case VRRP_MIB_STAT_ADDR_LIST_ERR:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSADDRLISTERROR64 (i4Index).u4Lo;
            }
            else
            {
                pData->u8_Counter64Value.msn
                    = STATSADDRLISTERROR64 (i4Index).u4Hi;
                pData->u8_Counter64Value.lsn
                    = STATSADDRLISTERROR64 (i4Index).u4Lo;
            }
            break;

        case VRRP_MIB_STAT_PKT_LEN_ERR:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSPACKETLENERROR64 (i4Index).u4Lo;
            }
            else
            {
                pData->u8_Counter64Value.msn
                    = STATSPACKETLENERROR64 (i4Index).u4Hi;
                pData->u8_Counter64Value.lsn
                    = STATSPACKETLENERROR64 (i4Index).u4Lo;
            }
            break;

        case VRRP_MIB_STAT_ROW_DISC_TIME:
            pData->u4_ULongValue = STATSDISCTIME (i4Index);
            break;

        case VRRP_MIB_STAT_TXED_ADVT:
            pData->u8_Counter64Value.msn = STATSTXEDADVT64 (i4Index).u4Hi;
            pData->u8_Counter64Value.lsn = STATSTXEDADVT64 (i4Index).u4Lo;
            break;

        case VRRP_MIB_STAT_MASTER_ADVT_INT:
            pData->i4_SLongValue
                =
                (OPERRCVDMASTERADVTINT (i4Index) / V3_ADVERTISEMENTINTLINMSEC);
            break;

        case VRRP_MIB_STAT_SKEW_TIME:
            pData->i4_SLongValue
                = (OPERSKEWTIME (i4Index) / V3_ADVERTISEMENTINTLINMSEC);
            break;

        case VRRP_MIB_STAT_MASTER_DOWN_INT:
            pData->i4_SLongValue
                = (OPERMASTERDOWNINT (i4Index) / V3_ADVERTISEMENTINTLINMSEC);
            break;

        case VRRP_MIB_STAT_AUTH_FAILURES:
            pData->u4_ULongValue = STATSAUTHFAILURES (i4Index);
            break;

        case VRRP_MIB_STAT_INVALID_AUTH:
            pData->u4_ULongValue = STATSINVALIDAUTHTYPE (i4Index);
            break;

        case VRRP_MIB_STAT_AUTH_TYPE_MISMATCH:
            pData->u4_ULongValue = STATSAUTHTYPEMISMATCH (i4Index);
            break;

        case VRRP_MIB_STAT_V2_ADVT_IGNORED:
            pData->u8_Counter64Value.msn = STATSV2ADVTIGNORED (i4Index).u4Hi;
            pData->u8_Counter64Value.lsn = STATSV2ADVTIGNORED (i4Index).u4Lo;
            break;

        case VRRP_MIB_STAT_V3_TXED_ADVT:
            pData->u8_Counter64Value.msn = STATSTXEDADVT (i4Index).u4Hi;
            pData->u8_Counter64Value.lsn = STATSTXEDADVT (i4Index).u4Lo;
            break;

        case VRRP_MIB_STAT_REFRESH_RATE:
            pData->u4_ULongValue = 0;
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpGetAllTrackGroupTable                        */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Track Group Table and returns value of requested */
/*                          Object.                                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          u4GroupIndex - Group Index                       */
/*                          u4ObjectId   - Object Identifier                 */
/*                                                                           */
/*    Output(s)           : pData        - Pointer to Data to be retrived    */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpGetAllTrackGroupTable (INT4 i4Version, UINT4 u4GroupIndex,
                           UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE * pData)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    BOOL1               b1VersionMatched = FALSE;

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return SNMP_FAILURE;
    }

    pTrackGroupEntry = VrrpDbGetTrackGroupEntry (u4GroupIndex);

    if (pTrackGroupEntry == NULL)
    {
        CLI_SET_ERR (CLI_VRRP_NO_TRACK_GROUP);

        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_TRACK_GROUP_LINKS:
            pData->u4_ULongValue = (UINT4) pTrackGroupEntry->u1TrackedLinks;
            break;

        case VRRP_MIB_TRACK_ROW_STATUS:
            pData->i4_SLongValue = (INT4) pTrackGroupEntry->u1RowStatus;
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpGetAllTrackGroupIfTable                      */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Track Group If Table and returns value of        */
/*                          requested Object.                                */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          u4GroupIndex - Group Index                       */
/*                          i4IfIndex    - Interface Index                   */
/*                          u4ObjectId   - Object Identifier                 */
/*                                                                           */
/*    Output(s)           : pData        - Pointer to Data to be retrived    */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpGetAllTrackGroupIfTable (INT4 i4Version, UINT4 u4GroupIndex, INT4 i4IfIndex,
                             UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE * pData)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;
    BOOL1               b1VersionMatched = FALSE;

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return SNMP_FAILURE;
    }

    pTrackGroupEntry = VrrpDbGetTrackGroupEntry (u4GroupIndex);

    if (pTrackGroupEntry == NULL)
    {
        CLI_SET_ERR (CLI_VRRP_NO_TRACK_GROUP);

        return SNMP_FAILURE;
    }

    pTrackGroupIfEntry =
        VrrpDbGetTrackGroupIfEntry (pTrackGroupEntry, i4IfIndex);

    if (pTrackGroupIfEntry == NULL)
    {
        CLI_SET_ERR (CLI_VRRP_NO_TRACK_IF);

        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_TRACK_IF_ROW_STATUS:
            pData->i4_SLongValue = (INT4) pTrackGroupIfEntry->u1RowStatus;
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpGetAllScalars                                */
/*                                                                           */
/*    Description         : This function validates VRRP version and if      */
/*                          validation is successful, correct values are     */
/*                          returned else zero is returned                   */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          u4ObjectId   - Object Identifier                 */
/*                                                                           */
/*    Output(s)           : pData        - Pointer to Data to be retrived    */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpGetAllScalars (INT4 i4Version, UINT4 u4ObjectId,
                   tSNMP_MULTI_DATA_TYPE * pData)
{
    BOOL1               b1VersionMatched = TRUE;

    if (u4ObjectId != VRRP_MIB_VERSION)
    {
        b1VersionMatched = VrrpCompCheckVersion (i4Version);
    }

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_VERSION:
            pData->i4_SLongValue = gi4VrrpNodeVersion;
            break;

        case VRRP_MIB_NOTIF_CTRL:
            pData->i4_SLongValue = gi4VrrpNotificationCntl;
            break;

        case VRRP_MIB_GBL_STATUS:
            pData->i4_SLongValue = gi4VrrpStatus;
            break;

        case VRRP_MIB_MAX_OPER_ENTRIES:
            pData->i4_SLongValue = (INT4) gu4VrrpMaxOperEntries;
            break;

        case VRRP_MIB_MAX_ASSOC_ENTRIES:
            pData->i4_SLongValue = (INT4) gu4VrrpMaxAssoEntries;
            break;

        case VRRP_MIB_TRACE_OPTION:
            pData->i4_SLongValue = gi4DbugFlag;
            break;

        case VRRP_MIB_AUTH_DEPRECATE:
            pData->i4_SLongValue = gi4VrrpAuthDeprecate;
            break;

        case VRRP_MIB_CHECKSUM_ERRORS:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSCHECKSUMERRORS64.u4Lo;
            }
            else if (i4Version == VRRP_VERSION_3)
            {
                pData->u8_Counter64Value.msn = STATSCHECKSUMERRORS64.u4Hi;
                pData->u8_Counter64Value.lsn = STATSCHECKSUMERRORS64.u4Lo;
            }
            break;

        case VRRP_MIB_VERSION_ERRORS:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSVERSIONERRORS;
            }
            else if (i4Version == VRRP_VERSION_3)
            {
                pData->u8_Counter64Value.msn = STATSVERSIONERRORS64.u4Hi;
                pData->u8_Counter64Value.lsn = STATSVERSIONERRORS64.u4Lo;
            }
            break;

        case VRRP_MIB_VRID_ERRORS:
            if (i4Version == VRRP_VERSION_2)
            {
                pData->u4_ULongValue = STATSVIDERRORS;
            }
            else if (i4Version == VRRP_VERSION_3)
            {
                pData->u8_Counter64Value.msn = STATSVIDERRORS64.u4Hi;
                pData->u8_Counter64Value.lsn = STATSVIDERRORS64.u4Lo;
            }
            break;

        case VRRP_MIB_GBL_DISC_TIME:
            pData->u4_ULongValue = gGblDiscTime;
            break;

        default:
            break;
    }

    return;
}
