/*  $Id: vrrpsem.c,v 1.27 2018/02/12 11:09:27 siva Exp $ */
/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrpsem.c                                    |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Interface.                              |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : Vrrp Sem Handling routines.                   |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |  Padmanaban. B       |   Base Line Version             |
 * |                 |                      |                                 |
 * |  1.1.0.0        |  Padmanaban. B       |   Changes for IMS 1600          |
 * |                 |                      |                                 |
 * |  1.2.0.0        |  Padmanaban. B       |   Change in Init->Master action |
 * |                 |                      |   routine and Master->Backup    |
 * |                 |                      |   action routine . Also in func |
 * |                 |                      |   vrrpSem.                      |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#define ELECTION_MODULE
#include "vrrpinc.h"

extern INT1         nmhGetSysUpTime (UINT4 *);

/*
 * Given a current state and next state this function would convert it into
 * appropriate event to be transmitted to the Configuration Manager
 */
int                 cmEvent[MAX_STATES][MAX_STATES] = {
    {

   /*** INIT_STATE  ***/
     /* INIT_STATE */ NO_EVENT,
     /* BACKUP_STATE */ INIT_BKUP_IND,
     /* MASTER_STATE */ INIT_MSTR_IND
     },

    {

   /*** BACKUP_STATE ***/
     /* INIT_STATE */ BKUP_INIT_IND,
     /* BACKUP_STATE */ NO_EVENT,
     /* MASTER_STATE */ BKUP_MSTR_IND
     },

    {

   /*** MASTER_STATE ***/
     /* INIT_STATE */ MSTR_INIT_IND,
     /* BACKUP_STATE */ MSTR_BKUP_IND,
     /* MASTER_STATE */ NO_EVENT
     }

};

/*******************************************************************************
* Function Name   : ElectRcvStartupInInitialState 
* Description      : This Funtion does prilminary operations when a 
*                   Start-Up event is received in Initial state.
* Global Varibles : gpVrrpOperTable.
* Inputs          : i4OperIndex
*                   i4Priority                
* Output          : None.                                                   
* Returns      : VRRP_OK
*                VRRP_NOT_OK 
*******************************************************************************/
INT1
ElectRcvStartupInInitialState (INT4 i4OperIndex, INT4 i4Priority)
{
    UINT4               u4RetValSysUpTime = 0;

    OsixGetSysTime (&u4RetValSysUpTime);
    OPERROUTERUPTIME (i4OperIndex) = u4RetValSysUpTime;
    if (OPERNEXTSTATE (i4OperIndex) == MASTER_STATE)
    {
        return (ElectRcvStartMstrInInitState (i4OperIndex, i4Priority));
    }
    else
    {
        return (ElectRcvStartBkUpInInitState (i4OperIndex, i4Priority));
    }
}

/*******************************************************************************
*Function Name     : ElectRcvStartMstrInInitState    
*Description       : This Funtion does prilminary operations when a 
*                   Start-Up event is received  by Permanent Master 
*                   in Initial State
*Global Varibles  : gpVrrpOperTable.
*Inputs           : i4OperIndex
*                   i4Priority
*                   
*Output           : None.                                                   
*Returns      : VRRP_OK
*                VRRP_NOT_OK 
*******************************************************************************/
INT1
ElectRcvStartMstrInInitState (INT4 i4OperIndex, INT4 i4Priority)
{
    tVrrpTimerRef      *pTmrRef;

    VRRP_DBG (VRRP_TRC_INIT, "SEM: Entering ElectRcvStartMstrInInitState\n");

    OPERADMINSTATE (i4OperIndex) = ADMINUP;

    /* Non -Zero Priority Packets to be transmitted */
    if ((PktProcessTransmitPacket (i4OperIndex, VRRP_FALSE)) == VRRP_NOT_OK)
    {

        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "SEM : -E- Failure in Transmitting Adver packet:\n");

    }
    VRRP_DBG (VRRP_TRC_PKT, "SEM : Sucessfully Transmited Adver packet:\n");

    /* Send Gratitous Arp */
    if (VrrpSendGratArp (i4OperIndex) == VRRP_NOT_OK)
    {

        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "SEM : -E- Failure in Transmitting Gratituous Arp:\n");
    }

    if (VrrpSendNDNeighborAdvtToAllAssocIp (i4OperIndex) == VRRP_NOT_OK)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "SEM : Failure in transmitting ND Neighbor Advertisement\n");
    }

    VRRP_DBG (VRRP_TRC_PKT, "SEM : Sucessfully Transmited Gratituous Arp:\n");

    /* Set Adver Timer to Advertisement interval */
    pTmrRef = &OPERTMR (i4OperIndex);
    pTmrRef->u1TimerType = ADVER_TIMER;
    pTmrRef->i4OperIndex = i4OperIndex;

    TmrIfStartTimer (pTmrRef, (FLT4) OPERADVERINTL (i4OperIndex));

    PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSBECOMEMASTER);

    PktProcessSetAndNotifyStats (i4OperIndex,
                                 (UINT1) VRRPSTATSNEWMASTERREASON,
                                 (UINT1) VRRP_MASTER_REASON_PRIORITY);

    UNUSED_PARAM (i4Priority);

    return VRRP_OK;
}

/*******************************************************************************
*Function Name       : ElectRcvStartBkUpInInitState    
*Description      : This Funtion does prilminary operations when a 
*                   Start-Up event is received by router other than Permanent
*                   Master in Initial State
*Global Varibles  : gpVrrpOperTable.
*Inputs           : i4OperIndex
*                   i4Priority
*                   
*Output           : None.                                                   
*Returns      : VRRP_OK
*            VRRP_NOT_OK
*******************************************************************************/
INT1
ElectRcvStartBkUpInInitState (INT4 i4OperIndex, INT4 i4Priority)
{
    tVrrpTimerRef      *pTmrRef = NULL;
    FLT4                fTime = 0.0;

    VRRP_DBG (VRRP_TRC_INIT,
              "SEM: Entering to Elect backup state from Init state when start-up events Received\n");
    /* BackUp Router StartUp */

    OPERADMINSTATE (i4OperIndex) = ADMINUP;

    pTmrRef = &(OPERTMR (i4OperIndex));
    pTmrRef->u1TimerType = MASTERDOWN_TIMER;
    pTmrRef->i4OperIndex = i4OperIndex;

    if ((gi4VrrpNodeVersion == VRRP_VERSION_2_3) ||
        (gi4VrrpNodeVersion == VRRP_VERSION_3))
    {
        OPERRCVDMASTERADVTINT (i4OperIndex) = OPERADVERINTL (i4OperIndex);
    }

    fTime = VrrpCompCalcMasterDownInterval (i4OperIndex);

    TmrIfStartTimer (pTmrRef, fTime);

    PktProcessSetAndNotifyStats (i4OperIndex,
                                 (UINT1) VRRPSTATSNEWMASTERREASON,
                                 (UINT1) VRRP_MASTER_REASON_NOT_MASTER);

    UNUSED_PARAM (i4Priority);

    return VRRP_OK;
}

/*******************************************************************************
*Function Name     :    ElectRcvMstrDwnTmrInBackUpState                          
*Description       :    This Funtion does prilminary operations when Master down
*                    timer is received in BackUp state.
*Global Varibles  :  gpVrrpOperTable.
*Inputs           :  i4OperIndex
*                    i4Priority
*                    
*Output           :  None.                                                   
*Returns             :  VRRP_OK
*                        VRRP_NOT_OK
*******************************************************************************/
INT1
ElectRcvMstrDwnTmrInBackUpState (INT4 i4OperIndex, INT4 i4Priority)
{
    tVrrpTimerRef      *pTmrRef;

    VRRP_DBG (VRRP_TRC_TIMERS, "SEM: Master Down Timer Expiry\n");

    /* Non -Zero Priority Packets to be transmitted */
    if ((PktProcessTransmitPacket (i4OperIndex, VRRP_FALSE)) == VRRP_NOT_OK)
    {

        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "SEM : -E- Failure in Transmitting Adver packet:\n");

    }

    VRRP_DBG (VRRP_TRC_PKT, "SEM : Sucessfully Transmited Adver packet:\n");

    /*   Send Gratitous Arp   */
    if (VrrpSendGratArp (i4OperIndex) == VRRP_NOT_OK)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "SEM : -E- Failure in Transmitting Gratitution Arp:\n");
    }

    if (VrrpSendNDNeighborAdvtToAllAssocIp (i4OperIndex) == VRRP_NOT_OK)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "SEM : Failure in transmitting ND Neighbor Advertisemtn\n");
    }

    if (OPERACCEPTMODE (i4OperIndex) == (UINT1) VRRP_DISABLE)
    {
        if (VrrpJoinSolicitedMulticastGroup (i4OperIndex, FALSE) == VRRP_NOT_OK)
        {
            VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                      "SEM : Failure in joining multicast group\n");
        }
    }

    /* Set Adver timer to advertisement interval */
    pTmrRef = &(OPERTMR (i4OperIndex));
    pTmrRef->u1TimerType = ADVER_TIMER;
    pTmrRef->i4OperIndex = i4OperIndex;

    OPERSKEWTIME (i4OperIndex) = 0;
    OPERRCVDMASTERADVTINT (i4OperIndex) = 0;
    OPERMASTERDOWNINT (i4OperIndex) = 0;

    TmrIfStartTimer (pTmrRef, (FLT4) OPERADVERINTL (i4OperIndex));

    PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSBECOMEMASTER);

    PktProcessSetAndNotifyStats (i4OperIndex,
                                 (UINT1) VRRPSTATSNEWMASTERREASON,
                                 (UINT1) VRRP_MASTER_REASON_NO_RESPONSE);

    VRRP_DBG (VRRP_TRC_TIMERS,
              "SEM : Advertisement Timer Started Sucessfully:\n");

    UNUSED_PARAM (i4Priority);
    return VRRP_OK;
}

/*******************************************************************************
*Function Name       :    ElectRcvAdverPktInBackUpState.
*Description       :    This Funtion describes the set of action to be taken on 
*                         BackUp State when AdverPacket Event is received.
*Global Varibles  :  gpVrrpOperTable
*Inputs              :  i4OperIndex
*                        i4Priority
*                    
*Output              :  None
*Returns             :  VRRP_OK
*******************************************************************************/
INT1
ElectRcvAdverPktInBackUpState (INT4 i4OperIndex, INT4 i4Priority)
{
    tVrrpTimerRef      *pTmrRef;
    INT4                i4OperPriority = 0;
    FLT4                fTime = 0.0;

    i4OperPriority = OPERPRIORITY (i4OperIndex);

    if (i4Priority == ZERO)
    {
        /* Stop the already running
         * master down timer before starting 
         * a new (skew) timer */
        pTmrRef = &(OPERTMR (i4OperIndex));
        TmrIfStopTimer (pTmrRef);

        VRRP_DBG (VRRP_TRC_PKT,
                  "SEM: Adver packet of priority zero received\n");

        pTmrRef = &(OPERTMR (i4OperIndex));
        pTmrRef->u1TimerType = MASTERDOWN_TIMER;
        pTmrRef->i4OperIndex = i4OperIndex;

        fTime = VrrpCompCalcSkewTime (i4OperIndex);

        TmrIfStartTimer (pTmrRef, fTime);

        VRRP_DBG (VRRP_TRC_TIMERS, "SEM : Sucessfully Started Skew Timer\n");
    }
    else
    {
        if ((OPERPREEMPTMODE (i4OperIndex) == VRRP_DISABLE) ||
            (i4Priority > i4OperPriority) ||
            ((i4Priority == i4OperPriority) &&
             (VRRP_IPVX_COMPARE (gSenderIpAddr,
                                 INTERFACEIPADDR (i4OperIndex)) > 0)))

        {
            VRRP_DBG (VRRP_TRC_PKT,
                      "SEM: Higher priority packet received Maintain state\n");
            /*  Set To Master Down Interval  */
            pTmrRef = &(OPERTMR (i4OperIndex));
            TmrIfStopTimer (pTmrRef);
            pTmrRef->u1TimerType = MASTERDOWN_TIMER;
            pTmrRef->i4OperIndex = i4OperIndex;

            VrrpMasterIpAddressSelection (i4OperIndex, BACKUP_STATE);

            fTime = VrrpCompCalcMasterDownInterval (i4OperIndex);

            TmrIfStartTimer (pTmrRef, fTime);

            VRRP_DBG (VRRP_TRC_TIMERS,
                      "SEM : MasterDown Timer Reset Sucessfully\n");
        }
        else
        {
            VRRP_DBG (VRRP_TRC_PKT,
                      "SEM: Lower Priority packet received. Would change"
                      "to Master State\n");
        }
        if (OPERNEXTSTATE (i4OperIndex) == BACKUP_STATE)
        {
            return VRRP_OK;
        }
        else
        {
            PktProcessSetAndNotifyStats (i4OperIndex,
                                         (UINT1) VRRPSTATSNEWMASTERREASON,
                                         (UINT1) VRRP_MASTER_REASON_PREEMPTED);
        }

    }

    return VRRP_OK;
}

/*******************************************************************************
*Function Name        :    ElectRcvShutDownInBackState                              
*Description        :    This Funtion Stops the Master Down Timer.
*Global Varibles   : gpVrrpOperTable
*Inputs               : i4OperIndex
*             i4Priority                                             
*                    
*Output               : None
*Returns              : VRRP_OK
*******************************************************************************/
INT1
ElectRcvShutDownInBackState (INT4 i4OperIndex, INT4 i4Priority)
{
    tVrrpTimerRef      *pTmrRef;

    pTmrRef = &(OPERTMR (i4OperIndex));

    TmrIfStopTimer (pTmrRef);

    VRRP_DBG (VRRP_TRC_TIMERS, "SEM: Cancelling Master Down Timer\n");

    VRRP_IPVX_ADDR_CLEAR (INTERFACEIPADDR (i4OperIndex));
    VRRP_IPVX_ADDR_CLEAR (OPERMASTERIPADDR (i4OperIndex));
    OPERISOWNER (i4OperIndex) = FALSE;

    PktProcessSetAndNotifyStats (i4OperIndex,
                                 (UINT1) VRRPSTATSNEWMASTERREASON,
                                 (UINT1) VRRP_MASTER_REASON_NOT_MASTER);

    UNUSED_PARAM (i4Priority);

    return VRRP_OK;
}

/*******************************************************************************
*Function Name       :  ElectRcvAdverPktInMasterState 
*Description       :    This Funtion describes the set of action to be taken 
*                        when AdverPacket of is received.
*Global Varibles  :  gpVrrpOperTable
*Inputs              :  i4OperIndex
*                        i4Priority
*                    
*Output              :  None
*Returns             :  VRRP_OK 
*                    VRRP_NOT_OK
*******************************************************************************/
INT1
ElectRcvAdverPktInMasterState (INT4 i4OperIndex, INT4 i4Priority)
{
    if (OPERNEXTSTATE (i4OperIndex) == MASTER_STATE)
    {
        return (ElectRcvAdverLPInMasterState (i4OperIndex, i4Priority));
    }
    else
    {
        return (ElectRcvAdverHPInMasterState (i4OperIndex, i4Priority));
    }
}

/*******************************************************************************
*Function Name       :    ElectRcvAdverHPInMasterState
*Description       :    This Funtion describes the set of action to be taken on 
*                        Master State when AdverPacket of High Priority is received.
*Global Varibles  :  gpVrrpOperTable
*Inputs              :  i4OperIndex
*                        i4Priority
*                    
*Output              :  None
*Returns             :  VRRP_OK 
*                    VRRP_NOT_OK
*******************************************************************************/
INT1
ElectRcvAdverHPInMasterState (INT4 i4OperIndex, INT4 i4Priority)
{
    tVrrpTimerRef      *pTmrRef = NULL;
    FLT4                fTime = 0.0;

    VRRP_DBG (VRRP_TRC_PKT,
              "SEM: Adver Packet of Higher Priority Received in"
              "MasterState\n");

    /* Moving to BackUp State */
    pTmrRef = &(OPERTMR (i4OperIndex));

    /* Cancel Timer */
    TmrIfStopTimer (pTmrRef);
    /* release added */
    pTmrRef->u1TimerType = MASTERDOWN_TIMER;
    pTmrRef->i4OperIndex = i4OperIndex;

    /* added Start Timer for Masterdown interval */
    fTime = VrrpCompCalcMasterDownInterval (i4OperIndex);

    TmrIfStartTimer (pTmrRef, fTime);

    if (OPERACCEPTMODE (i4OperIndex) == (UINT1) VRRP_DISABLE)
    {
        if (VrrpLeaveSolicitedMulticastGroup (i4OperIndex, FALSE) ==
            VRRP_NOT_OK)
        {
            VRRP_DBG1 (VRRP_TRC_EVENTS,
                       "Leaving Solicited Node Multicast group address failed for "
                       "VRID %d\n", OPERVRID (i4OperIndex));
        }
    }

    PktProcessSetAndNotifyStats (i4OperIndex,
                                 (UINT1) VRRPSTATSNEWMASTERREASON,
                                 (UINT1) VRRP_MASTER_REASON_PREEMPTED);

    VRRP_DBG (VRRP_TRC_TIMERS, "SEM: Master Down Timer Started Successfully\n");

    UNUSED_PARAM (i4Priority);
    return VRRP_OK;
}

/*******************************************************************************
*Function Name       :    ElectRcvAdverLPInMasterState
*Description       :    This Funtion describes the set of action to be taken on 
*                        Master State when AdverPacket of low priority is received.
*Global Varibles  :  gpVrrpOperTable
*Inputs              :  i4OperIndex
*             i4Priority
*                    
*Output              :  None
*Returns             :  VRRP_OK
*                        VRRP_NOT_OK
*******************************************************************************/
INT1
ElectRcvAdverLPInMasterState (INT4 i4OperIndex, INT4 i4Priority)
{
    tVrrpTimerRef      *pTmrRef = NULL;

    if (i4Priority == ZERO)
    {
        VRRP_DBG (VRRP_TRC_PKT,
                  "SEM: Recevied packet with Zero Priority at Master state\n");

        /* Non -Zero Priority Packets to be transmitted */
        if (PktProcessTransmitPacket (i4OperIndex, VRRP_FALSE) == VRRP_NOT_OK)
        {
            /* Non Zero Priority Packets */
            VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                      "SEM: -E- Advertisement Packet Transmission UnSuccessful  in MasterState\n");
        }

        VRRP_DBG (VRRP_TRC_PKT,
                  "SEM: Advertisement Packet Transmission Successful"
                  "in MasterState\n");

        /*   Tansmit Advertisements   */
        pTmrRef = &(OPERTMR (i4OperIndex));
        pTmrRef->u1TimerType = ADVER_TIMER;
        pTmrRef->i4OperIndex = i4OperIndex;

        TmrIfStopTimer (pTmrRef);

        TmrIfStartTimer (pTmrRef, (FLT4) OPERADVERINTL (i4OperIndex));

        VRRP_DBG (VRRP_TRC_TIMERS, "SEM: Adver Timer Started Sucessfully");
    }
    else
    {
        VRRP_DBG (VRRP_TRC_PKT,
                  "SEM: Adver Packet of Lower priority received in"
                  "Master state\n");
    }
    return VRRP_OK;
}

/*******************************************************************************
*Function Name         :    ElectRcvAdverTmrInMasterState
*Description         :    This Funtion Sets the Advertisement Timer to 
*                       Advertisement Interval.
*Global Varibles    :   gpVrrpOperTable
*Inputs               :   i4OperIndex
*                            i4Priority
*                       
*Output                :   None
*Returns               :   VRRP_OK
*                            VRRP_NOT_OK
*******************************************************************************/
INT1
ElectRcvAdverTmrInMasterState (INT4 i4OperIndex, INT4 i4Priority)
{
    tVrrpTimerRef      *pTmrRef = NULL;

    /* Non -Zero Priority Packets to be transmitted */
    if (PktProcessTransmitPacket (i4OperIndex, VRRP_FALSE) == VRRP_NOT_OK)
    {

        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "SEM: -E- Transmission of Adver Packet at the"
                  " expiry of Adver Timer Failed\n");

    }

    pTmrRef = &(OPERTMR (i4OperIndex));
    pTmrRef->u1TimerType = ADVER_TIMER;
    pTmrRef->i4OperIndex = i4OperIndex;

    TmrIfStartTimer (pTmrRef, (FLT4) OPERADVERINTL (i4OperIndex));

    UNUSED_PARAM (i4Priority);
    return VRRP_OK;
}

/*******************************************************************************
*Function Name       :    ElectRcvShutDownInmasterState                            
*Description       :    This Funtion does some premilinary operations before 
*                      moving to Intial State form Master state.
*Global Varibles  :  gpVrrpOperTable
*Inputs            :  i4OperIndex
*                        i4Priority
*                    
*Output             :  None
*Returns             :  VRRP_OK
*                        VRRP_NOT_OK
*******************************************************************************/
INT1
ElectRcvShutDownInmasterState (INT4 i4OperIndex, INT4 i4Priority)
{
    tVrrpTimerRef      *pTmrRef;
    pTmrRef = &(OPERTMR (i4OperIndex));
    TmrIfStopTimer (pTmrRef);
    VRRP_DBG (VRRP_TRC_TIMERS,
              "SEM: Advertisement Timer Cancelled Successfully at"
              "Shutdown in MasterState\n");
    /*   Transmit Zero Priority Packet indicating shut down  */
    if ((PktProcessTransmitPacket (i4OperIndex, VRRP_TRUE)) == VRRP_NOT_OK)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "SEM: -E- Transmission of Priority  Zero UnSuccessful"
                  "in MasterState\n");
    }
    else
    {
        VRRP_DBG (VRRP_TRC_PKT,
                  "SEM: Transmission of Priority  Zero Successful  in "
                  "MasterState\n");
    }

    VRRP_IPVX_ADDR_CLEAR (INTERFACEIPADDR (i4OperIndex));
    VRRP_IPVX_ADDR_CLEAR (OPERMASTERIPADDR (i4OperIndex));
    OPERISOWNER (i4OperIndex) = FALSE;

    PktProcessSetAndNotifyStats (i4OperIndex,
                                 (UINT1) VRRPSTATSNEWMASTERREASON,
                                 (UINT1) VRRP_MASTER_REASON_NOT_MASTER);

    UNUSED_PARAM (i4Priority);
    return VRRP_OK;
}

/*******************************************************************************
*Function Name         :    ElectRcvdInvalidEvent
*Description         :    This Funtion Logs the Discarding Packet action.
*Global Varibles    :   gpVrrpOperTable
*Inputs                :   i4OperIndex
*                           i4Priority
*                       
*Output                :   None
*Returns               :   VRRP_NOT_OK                                           
*******************************************************************************/
INT1
ElectRcvdInvalidEvent (INT4 i4OperIndex, INT4 i4Priority)
{
    VRRP_DBG (VRRP_TRC_EVENTS, "SEM: -E- Discarding the Event\n");
    UNUSED_PARAM (i4OperIndex);
    UNUSED_PARAM (i4Priority);
    return VRRP_OK;
}

/*******************************************************************************
*Function Name         :  VrrpSem
*Description           :  This Funtion is called by other Modules to Trigger
*                         appropriate Event to State Event Machine.
*Global Varibles       :  gpVrrpOperTable   - OperEntryTable
*Input                 :  u1Event-      General Event Trigger by other Modules
*                         i4OperIndex-  Index to the Oper Entry
*                         i4Priority-   Priority
*                         u4IpAddrCount - IP Address Count
*Output                :  None
*Returns               :  VRRP_OK
*                         VRRP_NOT_OK;
*******************************************************************************/
INT1
VrrpSem (UINT4 u1Event, INT4 i4OperIndex, INT4 i4Priority, UINT4 u4IpAddrCount)
{
    UINT1               returnStatus = 0;
    UINT1               u1CmEvent = 0;
    INT4                i4OperPriority = 0;

    if ((u1Event == STARTUP_EVENT)
        && (OPERSTATE (i4OperIndex) == INITIAL_STATE))
    {
        VRRP_DBG (VRRP_TRC_EVENTS, "SEM: Received StartUp Event\n");

        if (EthGetIfOperStatus (OPERIFINDEX (i4OperIndex),
                                OPERADDRTYPE (i4OperIndex)) != VRRP_ENABLE)
        {
            return VRRP_OK;
        }

        if (VRRP_IPVX_COMPARE (INTERFACEIPADDR (i4OperIndex),
                               gVrrpZeroIpAddr) == 0)
        {
            VrrpStateChangeRequest (IFACE_INFO_IND, i4OperIndex);
            return VRRP_OK;
        }

        if (OPERISOWNER (i4OperIndex) == TRUE)
        {
            OPERNEXTSTATE (i4OperIndex) = MASTER_STATE;
            VRRP_DBG (VRRP_TRC_EVENTS, "SEM: Next change is MASTER_STATE\n");
        }
        else
        {
            OPERNEXTSTATE (i4OperIndex) = BACKUP_STATE;
            VRRP_DBG (VRRP_TRC_EVENTS, "SEM: Next change is BACKUP_STATE\n");
        }
    }
    else
    {
        if ((u1Event == ADVERPKT_EVENT) &&
            (OPERSTATE (i4OperIndex) != INITIAL_STATE))
        {
            i4OperPriority = OPERPRIORITY (i4OperIndex);

            VRRP_DBG (VRRP_TRC_EVENTS, "SEM: Received AdverPkt  Event\n");
            /*Non IP owners should go to back up state if any of the
             *following conditions are met
             *
             *1. Incoming priority is greater than configured priority
             *2. If the priorities are same and the sender's interface IP
             *   is less than our interface IP
             */
            if ((i4OperPriority != VRRP_IPOWNER_PRIORITY) &&
                ((i4Priority > i4OperPriority) ||
                 ((i4Priority == i4OperPriority) &&
                  (VRRP_IPVX_COMPARE (gSenderIpAddr,
                                      INTERFACEIPADDR (i4OperIndex)) > 0))))
            {
                if (OPERSTATE (i4OperIndex) == MASTER_STATE)
                {
                    OPERNEXTSTATE (i4OperIndex) = BACKUP_STATE;
                    VRRP_DBG (VRRP_TRC_EVENTS,
                              "SEM: Next change is BACKUP_STATE\n");
                }
                else
                {                /* continue in BACKUP_STATE */
                    OPERNEXTSTATE (i4OperIndex) = BACKUP_STATE;
                    VRRP_DBG (VRRP_TRC_EVENTS,
                              "SEM: Remain in BACKUP_STATE \n");
                }
            }
        }
        else if (u1Event == SHUTDOWN_EVENT)
        {
            OPERNEXTSTATE (i4OperIndex) = INITIAL_STATE;

        }
        else
            /*Copy the Next State from Sem Table for rest of states and events */
        {
            OPERNEXTSTATE (i4OperIndex) =
                SemTable[OPERSTATE (i4OperIndex) - 1][u1Event].u1NextState;
        }
    }

    OPERRCVDPRIORITY (i4OperIndex) = (UINT1) i4Priority;
    u1CmEvent = (UINT1) (cmEvent[OPERSTATE (i4OperIndex) - 1]
                         [OPERNEXTSTATE (i4OperIndex) - 1]);

    if (u1CmEvent != NO_EVENT)
    {

        VRRP_DBG2 (VRRP_TRC_EVENTS,
                   "SEM: Inform Comp of state change [%d] for index [%d]\n",
                   u1CmEvent, i4OperIndex);
        OPEREVENTRCVD (i4OperIndex) = (UINT1) u1Event;
        VrrpStateChangeRequest (u1CmEvent, i4OperIndex);
        VRRP_DBG2 (VRRP_TRC_EVENTS,
                   "SEM: Event [%d] Index [%d] sent to Comp about state change\n",
                   u1CmEvent, i4OperIndex);
    }
    else
        /* to call the VrrpFuncPtr function for No event cases rest 
         * will be called from VrrpStateChageResp() */
    {
        returnStatus = (((VrrpFuncPtr) SemTable[OPERSTATE (i4OperIndex) -
                                                1][u1Event].FuncPtr)
                        (i4OperIndex, i4Priority));
        if (returnStatus == VRRP_OK)
        {
            OPERSTATE (i4OperIndex) = OPERNEXTSTATE (i4OperIndex);
        }
        else
        {
            PktProcessUpdateStatistics (i4OperIndex, VRRPSEMFAIL);
            VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                      "SEM: -E- Error Occured in Action Routine\n");
            return VRRP_NOT_OK;
        }
    }

    UNUSED_PARAM (u4IpAddrCount);

    return VRRP_OK;
}
