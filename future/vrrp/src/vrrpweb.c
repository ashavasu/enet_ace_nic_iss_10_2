/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: vrrpweb.c,v 1.8 2015/05/27 13:51:28 siva Exp $
 *
 * Description: This file contains web support for
 *              VRRPv3.
 *
 *******************************************************************/

#ifndef _ISS_WEB_C_
#define _ISS_WEB_C_
#ifdef VRRP_WANTED

#ifdef WEBNM_WANTED
#include "fswebnm.h"
#include "vrrpinc.h"

/*********************************************************************
 *  Function Name : VrrpWebConfPageGet 
 *  Description   : This function processes the request coming for the  
 *                  Vrrp Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
VrrpWebConfPageGet (tHttp * pHttp)
{
    tVrrpIPvXAddr       PrimaryIp;
    static UINT1        au1AuthKey[255];
    static tCfaIfInfo   IfInfo;
    static tVrrpOperTable OperTable;
    tSNMP_OCTET_STRING_TYPE AuthKey;
    tSNMP_OCTET_STRING_TYPE CurrIp;
    tUtlIn6Addr         Ipv6;
    tUtlIn6Addr         Ipv6MasterIp;
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1IpAddress[IPVX_IPV6_ADDR_LEN];
    CHR1               *pu1Address = NULL;
    INT4                i4CurrAddrType = 0;
    INT4                i4Version = 0;
    INT4                i4OutCome = 0;
    INT4                i4CurrIndex = 0;
    INT4                i4CurrVrId = 0;
    INT4                i4NextIndex = 0;
    INT4                i4NextVrId = 0;
    INT4                i4NextAddrType = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4Ret = 0;
    UINT1               au1TempAuthKey[16];
    UINT4               u4Priority = 0;
    UINT4               u4AuthType = 0;
    UINT4               u4AdvtInt = 0;
    UINT4               u4PreemptMode = 0;
    UINT4               u4OperState = 0;
    UINT4               u4AdminState = 0;
    UINT4               u4Adv = 0;
    UINT4		u4len=0;

    pu1Address = (CHR1 *) & au1Address[0];

    MEMSET (&OperTable, 0, sizeof (tVrrpOperTable));
    MEMSET (au1IpAddress, 0, IPVX_IPV6_ADDR_LEN);
    MEMSET (&CurrIp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET ((UINT1 *) &IfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (&Ipv6, 0, sizeof (tUtlIn6Addr));
    MEMSET (&Ipv6MasterIp, 0, sizeof (tUtlIn6Addr));
    MEMSET (au1TempAuthKey, 0, sizeof (au1TempAuthKey));

    CurrIp.pu1_OctetList = au1IpAddress;
    pHttp->i4Write = 0;

    IssPrintAvailableIpInterfacesAndIPAddr (pHttp);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    WebnmRegisterLock (pHttp, VrrpLock, VrrpUnLock);
    VRRP_LOCK ();

    nmhGetFsVrrpVersionSupported (&i4Version);
    if (i4Version == VRRP_VERSION_2)
    {
        i4OutCome = nmhGetFirstIndexVrrpOperTable (&i4CurrIndex, &i4CurrVrId);

        i4CurrAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        i4OutCome = nmhGetFirstIndexVrrpv3OperationsTable (&i4CurrIndex,
                                                           &i4CurrVrId,
                                                           &i4CurrAddrType);
    }

    if (i4OutCome == SNMP_FAILURE)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

	u4len = (UINT4)(STRLEN("VRID_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("VRID_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "VRID_KEY", u4len);
        pHttp->au1KeyString[u4len] = '\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (INT4), "%d",
                  i4CurrVrId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

	u4len = (UINT4)(STRLEN("VLAN_ID_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("VLAN_ID_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "VLAN_ID_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaGetIfInfo ((UINT4) i4CurrIndex, &IfInfo);
        STRNCPY (pHttp->au1DataString, IfInfo.au1IfName,
                 (sizeof (IfInfo.au1IfName)));
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

	u4len = (UINT4)(STRLEN("ADDRESS_TYPE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ADDRESS_TYPE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ADDRESS_TYPE_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        /*SNPRINTF appends /0 character at the end of the string, so in all the places where SNPRINTF used size is used as (STRLEN( "string") +1) */
        if (i4CurrAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("ipv4") + 1),
                      "%s", "ipv4");
        }
        else
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("ipv6") + 1),
                      "%s", "ipv6");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        VrrpCmnGetAllOperTable (i4Version, i4CurrIndex, i4CurrVrId,
                                i4CurrAddrType, &OperTable);
        MEMCPY (PrimaryIp.au1Addr, OperTable.PrimaryIpAddr.au1Addr,
                sizeof (PrimaryIp.au1Addr));
        MEMCPY (CurrIp.pu1_OctetList, OperTable.PrimaryIpAddr.au1Addr,
                sizeof (OperTable.PrimaryIpAddr.au1Addr));
	u4len = (UINT4)(STRLEN("PRIMARY_IP_ADDR_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("PRIMARY_IP_ADDR_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "PRIMARY_IP_ADDR_KEY", u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMCPY (Ipv6.u1addr, OperTable.PrimaryIpAddr.au1Addr,
                sizeof (Ipv6.u1addr));
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (IPVX_IPV6_ADDR_LEN + 8), "%s",
                  VRRP_IPVX_PRINT_ADDR (OperTable.PrimaryIpAddr,
                                        i4CurrAddrType));
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4Priority = (UINT4) OperTable.i4Priority;
	u4len = (UINT4) (STRLEN("PRIORITY_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("PRIORITY_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "PRIORITY_KEY", u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4Priority);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4AuthType = (UINT4) OperTable.i4AuthType;
	u4len = (UINT4)(STRLEN("AUTH_TYPE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("AUTH_TYPE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "AUTH_TYPE_KEY", u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4AuthType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

	u4len = (UINT4)(STRLEN("AUTH_KEY_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("AUTH_KEY_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "AUTH_KEY_KEY", u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4AuthType == 2)
        {
            AuthKey.pu1_OctetList = &au1TempAuthKey[0];
            AuthKey.i4_Length = 16;
            nmhGetVrrpOperAuthKey (i4CurrIndex, i4CurrVrId, &AuthKey);
            MEMCPY (au1AuthKey, AuthKey.pu1_OctetList, AuthKey.i4_Length);
            au1AuthKey[AuthKey.i4_Length] = '\0';
            SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (au1AuthKey), "%s",
                      au1AuthKey);
        }
        else
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("-") + 1), "%s",
                      "-");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        if ((OperTable.i4AdvtInterval % 1000) == 0)
        {
            if ((OperTable.i4AdvtInterval / 1000) != ADVERTISEMENTINTL)
            {
                u4AdvtInt = (UINT4) OperTable.i4AdvtInterval / 1000;
            }
        }
        else
        {
            u4AdvtInt = (UINT4) OperTable.i4AdvtInterval;
        }

        if (i4Version == VRRP_VERSION_2)
        {
            u4Adv = (UINT4) OperTable.i4AdvtInterval;
        }
        else
        {
            u4AdvtInt = (UINT4) (OperTable.i4AdvtInterval);
            u4Adv = u4AdvtInt * 10;    /*To convert the advt into msec */
        }
	u4len = (UINT4)(STRLEN("ADV_INT_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ADV_INT_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ADV_INT_KEY",u4len);
        pHttp->au1KeyString[u4len]= '\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (sizeof (UINT4) + 3), "%u",
                  u4Adv);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4PreemptMode = (UINT4) OperTable.i4PreemptMode;
	u4len = (UINT4)(STRLEN("PREEMPT_MODE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("PREEMPT_MODE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "PREEMPT_MODE_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4PreemptMode);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4RetVal = OperTable.u1AcceptMode;
	u4len = (UINT4)(STRLEN("ACCEPT_MODE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ACCEPT_MODE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ACCEPT_MODE_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4RetVal = OperTable.u4TrackGroupId;
	u4len = (UINT4)(STRLEN("TRC_GNO_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("TRC_GNO_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "TRC_GNO_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4RetVal = OperTable.u1DecrementedPriority;
	u4len = (UINT4)(STRLEN("DEC_PRI_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("DEC_PRI_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "DEC_PRI_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (&au1Address, 0, MAX_ADDR_LEN);
	u4len = (UINT4)(STRLEN("VIR_MAC_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("VIR_MAC_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "VIR_MAC_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        CLI_CONVERT_MAC_TO_DOT_STR (OperTable.maVirtualMacAddr,
                                    (UINT1 *) pu1Address);
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (MAX_ADDR_LEN + 1), "%s",
                  pu1Address);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        SNPRINTF ((CHR1 *) pHttp->au1DataString, (IPVX_IPV6_ADDR_LEN + 8), "%s",
                  VRRP_IPVX_PRINT_ADDR (OperTable.MasterIpAddr,
                                        i4CurrAddrType));
	u4len = (UINT4)(STRLEN("MASTER_IP_ADDR_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("MASTER_IP_ADDR_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "MASTER_IP_ADDR_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4OperState = (UINT4) OperTable.i4OperState;
	u4len = (UINT4)(STRLEN("OPER_STATE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("OPER_STATE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "OPER_STATE_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        switch (u4OperState)
        {
            case 1:
                SNPRINTF ((CHR1 *) pHttp->au1DataString,
                          (STRLEN ("Initial") + 1), "%s", "Initial");
                break;
            case 2:
                SNPRINTF ((CHR1 *) pHttp->au1DataString,
                          (STRLEN ("Backup") + 1), "%s", "Backup");
                break;
            case 3:
                SNPRINTF ((CHR1 *) pHttp->au1DataString,
                          (STRLEN ("Master") + 1), "%s", "Master");
                break;
            default:
                break;
        };
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4AdminState = (UINT4) OperTable.i4OperAdminState;
	u4len = (UINT4)(STRLEN("ADMIN_STATE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ADMIN_STATE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ADMIN_STATE_KEY",u4len);
        pHttp->au1KeyString[u4len] = '\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4Version == VRRP_VERSION_2)
        {
            if (u4AdminState == 1)
            {
                SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("Up") + 1),
                          "%s", "Up");
            }
            else
            {
                SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("Down") + 1),
                          "%s", "Down");
            }
        }
        else
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("Up") + 1), "%s",
                      "Up");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        if (i4Version == VRRP_VERSION_2)
        {
            u4Ret = (nmhGetNextIndexVrrpOperTable (i4CurrIndex, &i4NextIndex,
                                                   i4CurrVrId,
                                                   &i4NextVrId) ==
                     SNMP_SUCCESS);
        }
        else
        {
            u4Ret = (nmhGetNextIndexVrrpv3OperationsTable (i4CurrIndex,
                                                           &i4NextIndex,
                                                           i4CurrVrId,
                                                           &i4NextVrId,
                                                           i4CurrAddrType,
                                                           &i4NextAddrType)
                     == SNMP_SUCCESS);
            i4CurrAddrType = i4NextAddrType;
        }
        i4CurrVrId = i4NextVrId;
        i4CurrIndex = i4NextIndex;

    }
    while (u4Ret);

    VRRP_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

    return;
}

/*********************************************************************
 *  Function Name : VrrpWebConfPageSet 
 *  Description   : This function processes the request coming for the  
 *                  Vrrp Configuration page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
VrrpWebConfPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE AuthKey;
    tUtlIn6Addr         ipv6Addr;
    tUtlIn6Addr         ipv6AssocAddr;
    UINT1               au1Ip6AssocAddr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1IpAddr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1Ip6Addr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1AuthKey[16];
    INT4                i4VrId = 0;
    INT4                i4VlanId = 0;
    INT4                i4Pri = 0;
    INT4                i4PreMode = 0;
    INT4                i4AuthType = 0;
    INT4                i4AdvInt = 0;
    INT4                i4AdminState = 0;
    INT4                i4Version = 0;
    INT4                i4AddrType = 0;
    INT4                i4RetVal = 0;
    INT4                i4AcceptMode = 0;
    INT4                i4TimeUnit = 0;
    INT4                i4DecPriority = 0;
    UINT4               u4TrackGrp = 0;
    UINT4               u4IPAddr = 0;
    UINT4               u4Option = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Ret = 0;
    BOOL1               ApplyButtonSet = FALSE;
    BOOL1               DeleteButtonSet = FALSE;
    UINT4		u4len = 0;

    MEMSET (au1IpAddr, 0, sizeof (au1IpAddr));
    MEMSET (au1Ip6Addr, 0, sizeof (au1Ip6Addr));
    MEMSET (au1Ip6AssocAddr, 0, sizeof (au1Ip6AssocAddr));
    MEMSET (&ipv6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&ipv6AssocAddr, 0, sizeof (tUtlIn6Addr));
    MEMSET (au1AuthKey, 0, sizeof (au1AuthKey));

    nmhGetFsVrrpVersionSupported (&i4Version);
    u4len = (UINT4)(STRLEN("VR_ID")<(sizeof(pHttp->au1Name)) ? STRLEN("VR_ID"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "VR_ID",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VrId = ATOI (pHttp->au1Value);
    u4len = (UINT4)(STRLEN("VLAN_ID")<(sizeof(pHttp->au1Name)) ? STRLEN("VLAN_ID"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "VLAN_ID",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = ATOI (pHttp->au1Value);

    WebnmRegisterLock (pHttp, VrrpLock, VrrpUnLock);
    VRRP_LOCK ();

    /*interface addr whilecoming as modifying parameter */

    u4len = (UINT4)(STRLEN("ACTION")<(sizeof(pHttp->au1Name)) ? STRLEN("ACTION"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "ACTION",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRNCMP (pHttp->au1Value, "Delete", STRLEN ("Delete")) == 0)
    {
        DeleteButtonSet = TRUE;
	u4len = (UINT4)(STRLEN("INTERFACE")<(sizeof(pHttp->au1Name)) ? STRLEN("INTERFACE"):(sizeof(pHttp->au1Name)-1));
        STRNCPY (pHttp->au1Name, "INTERFACE",u4len);
        pHttp->au1Name[u4len] ='\0';
	u4len = 0;
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Ret =
            CfaGetInterfaceIndexFromName ((UINT1 *) pHttp->au1Value,
                                          &u4IfIndex);
        if (u4Ret == OSIX_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            return;
        }

        i4VlanId = (INT4) u4IfIndex;
	u4len = (UINT4)(STRLEN("ADDRESS_TYPE")<(sizeof(pHttp->au1Name)) ? STRLEN("ADDRESS_TYPE"):(sizeof(pHttp->au1Name)-1));
        STRNCPY (pHttp->au1Name, "ADDRESS_TYPE",u4len);
        pHttp->au1Name[u4len] ='\0';
	u4len = 0;
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (DeleteButtonSet == TRUE)
        {
            if (STRCMP (pHttp->au1Value, "ipv4") == 0)
            {
                i4AddrType = 1;
            }
            else
            {
                i4AddrType = 2;
            }
        }

        VrrpCmnDeleteVrId (i4Version, i4VlanId, i4VrId, i4AddrType);

        VRRP_UNLOCK ();
        VrrpWebConfPageGet (pHttp);
        WebnmUnRegisterLock (pHttp);

        return;
    }
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
	u4len = (UINT4)(STRLEN("INTERFACE")<(sizeof(pHttp->au1Name)) ? STRLEN("INTERFACE"):(sizeof(pHttp->au1Name)-1));
        STRNCPY (pHttp->au1Name, "INTERFACE",u4len);
        pHttp->au1Name[u4len]='\0';
	u4len = 0;
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4VlanId = ATOI (pHttp->au1Value);
        u4Ret =
            CfaGetInterfaceIndexFromName ((UINT1 *) pHttp->au1Value,
                                          &u4IfIndex);
        if (u4Ret == OSIX_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            return;
        }
        i4VlanId = (INT4) u4IfIndex;
        ApplyButtonSet = TRUE;
    }

    u4len = (UINT4)(STRLEN("ADDRESS_TYPE")<(sizeof(pHttp->au1Name)) ? STRLEN("ADDRESS_TYPE"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "ADDRESS_TYPE",u4len);
    pHttp->au1Name[u4len]= '\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (ApplyButtonSet == TRUE)
    {
        if (STRCMP (pHttp->au1Value, "ipv4") == 0)
        {
            i4AddrType = 1;
        }
        else
        {
            i4AddrType = 2;
        }
    }
    else
    {
        i4AddrType = ATOI (pHttp->au1Value);
    }

    u4len = (UINT4)(STRLEN("PRIMARY_IP_ADDR")<(sizeof(pHttp->au1Name)) ? STRLEN("PRIMARY_IP_ADDR"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "PRIMARY_IP_ADDR",u4len);
    pHttp->au1Name[u4len]='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        u4IPAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }
    else
    {
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &ipv6Addr);
    }

    u4len = (UINT4)(STRLEN("PRIORITY")<(sizeof(pHttp->au1Name)) ? STRLEN("PRIORITY"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "PRIORITY",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Pri = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("AUTH_TYPE")<(sizeof(pHttp->au1Name)) ? STRLEN("AUTH_TYPE"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "AUTH_TYPE",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AuthType = ATOI (pHttp->au1Value);

    if (i4AuthType == 2)
    {
	u4len = (UINT4)(STRLEN("AUTH_KEY")<(sizeof(pHttp->au1Name)) ? STRLEN("AUTH_KEY"):(sizeof(pHttp->au1Name)-1));
        STRNCPY (pHttp->au1Name, "AUTH_KEY",u4len);
        pHttp->au1Name[u4len]='\0';
	u4len = 0;
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        AuthKey.i4_Length = (INT4) STRLEN (pHttp->au1Value);
        AuthKey.pu1_OctetList = &au1AuthKey[0];
        MEMCPY (AuthKey.pu1_OctetList, pHttp->au1Value,
                MEM_MAX_BYTES ((UINT4) AuthKey.i4_Length, sizeof (au1AuthKey)));
    }

    u4len = (UINT4)(STRLEN("TIME_UNIT")<(sizeof(pHttp->au1Name)) ? STRLEN("TIME_UNIT"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "TIME_UNIT",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TimeUnit = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("ADV_INT")<(sizeof(pHttp->au1Name)) ? STRLEN("ADV_INT"):(sizeof(pHttp->au1Name)));
    STRNCPY (pHttp->au1Name, "ADV_INT",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AdvInt = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("PREEMPT_MODE")<(sizeof(pHttp->au1Name)) ? STRLEN("PREEMPT_MODE"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "PREEMPT_MODE",u4len);
    pHttp->au1Name[u4len]='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PreMode = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("ACCEPT_MODE")<(sizeof(pHttp->au1Name)) ? STRLEN("ACCEPT_MODE"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "ACCEPT_MODE",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AcceptMode = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("ADMIN_STATE")<(sizeof(pHttp->au1Name)) ? STRLEN("ADMIN_STATE"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "ADMIN_STATE",u4len);
    pHttp->au1Name[u4len]='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AdminState = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("TRC_GNO")<(sizeof(pHttp->au1Name)) ? STRLEN("TRC_GNO"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "TRC_GNO",u4len);
    pHttp->au1Name[u4len]='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4TrackGrp = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("DEC_PRI")<(sizeof(pHttp->au1Name)) ? STRLEN("DEC_PRI"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "DEC_PRI",u4len);
    pHttp->au1Name[u4len]='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4DecPriority = ATOI (pHttp->au1Value);

    if (i4Version == VRRP_VERSION_2)
    {
        u4Option = 1;
        i4RetVal = VrrpCmnSetV2IpAddr (i4VlanId, i4VrId, i4AddrType, u4IPAddr,
                                       u4Option);
        if (i4RetVal == VRRP_NOT_OK)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *) "Ip fail. Cannot create Row.");
            return;
        }
    }
    else
    {
        u4IPAddr = OSIX_HTONL (u4IPAddr);
        u4Option = 1;
        MEMCPY (au1IpAddr, (UINT1 *) &u4IPAddr, IPVX_IPV4_ADDR_LEN);
        if (i4AddrType == 1)
        {
            i4RetVal =
                VrrpCmnSetV3IpAddr (i4VlanId, i4VrId, i4AddrType, au1IpAddr,
                                    u4Option);
        }
        else
        {

            i4RetVal =
                VrrpCmnSetV3IpAddr (i4VlanId, i4VrId, i4AddrType,
                                    ipv6Addr.u1addr, u4Option);
        }
        if (i4RetVal == VRRP_NOT_OK)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *) "Ip fail. Cannot create Row.");
            return;
        }
    }
    if (VrrpCmnSetPriority (i4VlanId, i4VrId, i4AddrType, i4Pri) == VRRP_NOT_OK)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp,
                      (CONST INT1 *) "Invalid Priority. Cannot create Row.");
        return;
    }
    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhSetVrrpOperAuthType (i4VlanId, i4VrId, i4AuthType) ==
            SNMP_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Auth Type. Cannot set the value.");
            return;
        }

        if (i4AuthType == 2)
        {
            if (nmhTestv2VrrpOperAuthKey
                (&u4ErrorCode, i4VlanId, i4VrId, &AuthKey) == SNMP_FAILURE)
            {
                VRRP_UNLOCK ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Auth Key. Cannot create Row.");
                return;
            }

            if (nmhSetVrrpOperAuthKey (i4VlanId, i4VrId, &AuthKey) ==
                SNMP_FAILURE)
            {
                VRRP_UNLOCK ();
                WebnmUnRegisterLock (pHttp);

                IssSendError (pHttp,
                              (CONST INT1 *)
                              "Invalid Auth Key. Cannot set the value.");
                return;
            }
        }
    }

    if (ApplyButtonSet == (TRUE))
    {
        i4TimeUnit = 2;
        i4RetVal = VrrpCmnSetAdvertisementIntervalInMSec (i4VlanId, i4VrId,
                                                          i4AddrType, i4AdvInt);
    }
    else
    {
        /*configuring advt interval */
        if (i4TimeUnit == 1)    /*if configured in sec */
        {
            i4RetVal =
                VrrpCmnSetAdvertisementInterval (i4VlanId, i4VrId, i4AddrType,
                                                 i4AdvInt);
        }
        else                    /*if configured in msec */
        {
            i4RetVal =
                VrrpCmnSetAdvertisementIntervalInMSec (i4VlanId, i4VrId,
                                                       i4AddrType, i4AdvInt);
        }
    }

    if (i4RetVal == VRRP_NOT_OK)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Adv interval. Cannot set the value.");
        return;
    }
    if (VrrpCmnSetPreemptMode (i4VlanId, i4VrId, i4AddrType, i4PreMode)
        == VRRP_NOT_OK)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Preempt Mode. Cannot set the value.");
        return;
    }
    if (i4Version != VRRP_VERSION_2)
    {
        if (VrrpCmnSetAcceptMode (i4VlanId, i4VrId, i4AddrType, i4AcceptMode)
            == VRRP_NOT_OK)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Accept Mode. Cannot set the value.");
            return;
        }
    }

    if (i4Version == VRRP_VERSION_2)
    {
        i4AdminState = ISS_ACTIVE;
        if (nmhTestv2VrrpOperAdminState (&u4ErrorCode, i4VlanId, i4VrId,
                                         i4AdminState) == SNMP_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Entry. Cannot set Admin State.");
            return;
        }

        if (nmhSetVrrpOperAdminState (i4VlanId, i4VrId, i4AdminState)
            == SNMP_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Invalid Entry. Cannot set Admin State.");
            return;
        }
    }

    if (VrrpCmnSetTrackAndDecPriority (i4VlanId, i4VrId, i4AddrType,
                                       u4TrackGrp,
                                       i4DecPriority) == VRRP_NOT_OK)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp,
                      (CONST INT1 *)
                      "Invalid Entry. Cannot set Track and Dec Priority.");
        return;
    }

    VRRP_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    VrrpWebConfPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : VrrpWebTrackPageGet  
 *  Description   : This function processes the request coming for the
 *                  Vrrp Track page. 
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
VrrpWebTrackPageGet (tHttp * pHttp)
{
    tCfaIfInfo          IfInfo;
    INT4                i4RetVal = 0;
    INT4                i4Version = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    UINT4               u4NumberOfLinks = 0;
    UINT4               u4GroupNo = 0;
    UINT4               u4GroupIndex = 0;
    UINT4               u4NextGroupIndex = 0;
    UINT4               u4Temp = 0;
    UINT4		u4len = 0;

    MEMSET ((UINT1 *) &IfInfo, 0, sizeof (tCfaIfInfo));
    IssPrintAvailableIpInterfacesAndIPAddr (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    WebnmRegisterLock (pHttp, VrrpLock, VrrpUnLock);
    VRRP_LOCK ();

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        return;
    }
    if (i4Version == VRRP_VERSION_2)
    {
        i4RetVal =
            nmhGetFirstIndexFsVrrpOperTrackGroupIfTable (&u4GroupNo,
                                                         &i4IfIndex);
    }
    else
    {
        i4RetVal =
            nmhGetFirstIndexFsVrrpv3OperationsTrackGroupIfTable (&u4GroupNo,
                                                                 &i4IfIndex);
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

	u4len = (UINT4)(STRLEN("GROUP_NO_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("GROUP_NO_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "GROUP_NO_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%d",
                  u4GroupNo);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        if (i4Version == VRRP_VERSION_2)
        {
            nmhGetFsVrrpOperTrackedGroupTrackedLinks (u4GroupNo,
                                                      &u4NumberOfLinks);
        }
        else
        {
            nmhGetFsVrrpv3OperationsTrackedGroupTrackedLinks (u4GroupNo,
                                                              &u4NumberOfLinks);
        }
	u4len = (UINT4)(STRLEN("NO_LINKS_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("NO_LINKS_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "NO_LINKS_KEY",u4len);
        pHttp->au1KeyString[u4len] = '\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (u4NumberOfLinks == 0)
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("-") + 1), "%s",
                      "-");
        }
        else
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%d",
                      u4NumberOfLinks);
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
        CfaGetIfInfo ((UINT4) i4IfIndex, &IfInfo);
	u4len = (UINT4)(STRLEN("INTERFACE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("INTERFACE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "INTERFACE_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        STRNCPY (pHttp->au1DataString, IfInfo.au1IfName, (sizeof (tCfaIfInfo)));
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        if (i4Version == VRRP_VERSION_2)
        {
            i4RetVal = (nmhGetNextIndexFsVrrpOperTrackGroupIfTable (u4GroupNo,
                                                                    &u4NextGroupIndex,
                                                                    i4IfIndex,
                                                                    &i4NextIfIndex)
                        == SNMP_SUCCESS);
        }
        else
        {
            i4RetVal =
                (nmhGetNextIndexFsVrrpv3OperationsTrackGroupIfTable
                 (u4GroupNo, &u4NextGroupIndex, i4IfIndex,
                  &i4NextIfIndex) == SNMP_SUCCESS);
        }
        if (u4GroupIndex == u4NextGroupIndex)
        {
            break;
        }
        u4GroupNo = u4NextGroupIndex;
        i4IfIndex = i4NextIfIndex;
    }
    while (i4RetVal);

    VRRP_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : VrrpWebTrackPageSet 
 *  Description   : This function processes the request coming for the
 *                  Vrrp Track page.
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/
VOID
VrrpWebTrackPageSet (tHttp * pHttp)
{
    UINT4               u4GrpNo = 0;
    INT4                i4NoOfLinks = 0;
    INT4                i4VlanId = 0;
    INT4                i4IfIndex = 0;
    INT4                i4Version = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Ret = 0;
    UINT4		u4len = 0;

    WebnmRegisterLock (pHttp, VrrpLock, VrrpUnLock);
    VRRP_LOCK ();

    nmhGetFsVrrpVersionSupported (&i4Version);

    u4len = (UINT4)(STRLEN("GROUP_NO")<(sizeof(pHttp->au1Name)) ? STRLEN("GROUP_NO"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "GROUP_NO",u4len);
    pHttp->au1Name[u4len] = '\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4GrpNo = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("NO_OF_LINK")<(sizeof(pHttp->au1Name)) ? STRLEN("NO_OF_LINK"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "NO_OF_LINK",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "-") == 0)
    {
        i4NoOfLinks = 0;
    }
    else
    {
        i4NoOfLinks = ATOI (pHttp->au1Value);
    }

    u4len = (UINT4)(STRLEN("VLAN_ID")<(sizeof(pHttp->au1Name)) ? STRLEN("VLAN_ID"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "VLAN_ID",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("ACTION")<(sizeof(pHttp->au1Name)) ? STRLEN("ACTION"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "ACTION",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRNCMP (pHttp->au1Value, "Delete", STRLEN ("Delete")) == 0)
    {

	u4len = (UINT4)(STRLEN("INTERFACE")<(sizeof(pHttp->au1Name)) ? STRLEN("INTERFACE"):(sizeof(pHttp->au1Name)-1));
        STRNCPY (pHttp->au1Name, "INTERFACE",u4len);
        pHttp->au1Name[u4len] ='\0';
	u4len = 0;
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Ret =
            CfaGetInterfaceIndexFromName ((UINT1 *) pHttp->au1Value,
                                          &u4IfIndex);
        if (u4Ret == OSIX_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            return;
        }
        i4IfIndex = (INT4) u4IfIndex;
        if (i4NoOfLinks != 0)
        {
            if (VrrpCmnSetTrackLinks (u4GrpNo, 0) == VRRP_NOT_OK)
            {
                return;
            }
        }

        if (VrrpCmnDeleteTrackGroupIf (u4GrpNo, i4IfIndex) == VRRP_NOT_OK)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Last Tracked Group Interface cannot be deleted with more Virtual Router Entries attached");
            return;
        }

        if (VrrpCmnSetTrackRowStatus (i4Version, u4GrpNo, DESTROY)
            == VRRP_NOT_OK)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);
            IssSendError (pHttp,
                          (CONST INT1 *)
                          "Last Tracked Group Interface cannot be deleted with more Virtual Router Entries attached");

            return;

        }

        VRRP_UNLOCK ();
        VrrpWebTrackPageGet (pHttp);
        WebnmUnRegisterLock (pHttp);

        return;
    }

    if (VrrpCmnCreateTrackGroupIf (u4GrpNo, i4VlanId) == VRRP_NOT_OK)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to create an TrackIf Entry");
        return;
    }
    if (VrrpCmnSetTrackLinks (u4GrpNo, i4NoOfLinks) == VRRP_NOT_OK)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp,
                      (CONST INT1 *) "Unable to set track links an Entry");
        return;
    }

    VRRP_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    VrrpWebTrackPageGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : VrrpBasicSet                                                                        *  Description   : This function processes the request coming for the
 *                  Vrrp Basic Settings page.                                                           *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.                                                                               *  Return Values : None
 *********************************************************************/

VOID
VrrpWebBasicSet (tHttp * pHttp)
{

    INT4                i4Version = 0;
    INT4                i4Status = 0;
    INT4                i4AuthDep = 0;
    INT4                i4NtnCtl = 0;
    UINT4               u4Err = 0;
    UINT4		u4len = 0;

    WebnmRegisterLock (pHttp, VrrpLock, VrrpUnLock);
    VRRP_LOCK ();

    u4len = (UINT4)(STRLEN("VRRP_VERSION")<(sizeof(pHttp->au1Name)) ? STRLEN("VRRP_VERSION"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "VRRP_VERSION",u4len);
    pHttp->au1Name[u4len] = '\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Version = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("VRRP_STATUS")<(sizeof(pHttp->au1Name)) ? STRLEN("VRRP_STATUS"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "VRRP_STATUS",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Status = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("AUTH_DEPRECATE")<(sizeof(pHttp->au1Name)) ? STRLEN("AUTH_DEPRECATE"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "AUTH_DEPRECATE",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4AuthDep = ATOI (pHttp->au1Value);

    u4len = (UINT4)(STRLEN("NOTIF_CNTL")<(sizeof(pHttp->au1Name)) ? STRLEN("NOTIF_CNTL"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "NOTIF_CNTL",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4NtnCtl = ATOI (pHttp->au1Value);

    if (nmhTestv2FsVrrpVersionSupported (&u4Err, i4Version) == SNMP_SUCCESS)
    {
        if (nmhSetFsVrrpVersionSupported (i4Version) == SNMP_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            return;
        }
    }
    else
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSendError (pHttp, (CONST INT1 *) "Unable to set Version");
        return;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhSetFsVrrpStatus (i4Status) == SNMP_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            return;
        }
    }
    else
    {
        if (nmhSetFsVrrpv3Status (i4Status) == SNMP_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            return;
        }
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhSetFsVrrpAuthDeprecate (i4AuthDep) == SNMP_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            return;
        }
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhSetVrrpNotificationCntl (i4NtnCtl) == SNMP_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            return;
        }
    }
    else
    {
        if (nmhSetFsVrrpv3NotificationCntl (i4NtnCtl) == SNMP_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            return;
        }
    }

    VRRP_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    VrrpWebBasicGet (pHttp);
    return;
}

/*********************************************************************
 *  Function Name : VrrpWebStatsPage                                                                    *  Description   : This function processes the request coming for the
 *                  Vrrp version2 stat  page.                                                           *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.                                                                               *  Return Values : None
 *********************************************************************/

VOID
VrrpWebStatsPage (tHttp * pHttp)
{
    tCfaIfInfo          IfInfo;
    tSNMP_COUNTER64_TYPE Val;
    tVrrpStatsTable     Stats;
    CHR1                ac1Val[VRRP_U8_STR_LENGTH];
    FS_UINT8            u8Val;
    CHR1               *pac1LastProtoError[] = {
        "No Error\0",
        "Incorrect IP TTL\0",
        "Incorrect Version\0",
        "Incorrect Checksum\0",
        "Invalid VrId\0",
        "Advertisement Interval Mismatch\0",
        "Invalid VRRP Packet Type\0",
        "Address List Mismatch\0",
        "Incorrect Packet Length\0"
    };
    CHR1               *pac1NewMasterReason[] = {
        "Not Master",
        "Priority",
        "Preempted",
        "Master No Response"
    };
    INT4                i4OutCome = 0;
    INT4                i4CurrIndex = 0;
    INT4                i4CurrVrId = 0;
    INT4                i4NextIndex = 0;
    INT4                i4NextVrrpId = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4RetVal = 0;
    INT4                i4Version = 0;
    UINT4               u4Val = 0;
    UINT4               u4RetVal = 0;
    UINT4               u4Temp = 0;
    UINT4               u4Ret = 0;
    UINT4		u4len = 0;

    MEMSET (&Stats, 0, sizeof (tVrrpStatsTable));
    Val.msn = 0;
    Val.lsn = 0;
    u8Val.u4Hi = 0;
    u8Val.u4Lo = 0;
    MEMSET ((UINT1 *) ac1Val, 0, VRRP_U8_STR_LENGTH);
    pHttp->i4Write = 0;

    WebnmRegisterLock (pHttp, VrrpLock, VrrpUnLock);
    VRRP_LOCK ();

    nmhGetFsVrrpVersionSupported (&i4Version);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    /* Global Statistics. */

    if (i4Version == VRRP_VERSION_2)
    {
        nmhGetVrrpRouterChecksumErrors (&u4Val);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%d", u4Val);
    }
    else
    {
        nmhGetVrrpv3RouterChecksumErrors (&Val);

        u8Val.u4Hi = Val.msn;
        u8Val.u4Lo = Val.lsn;

        FSAP_U8_2STR (&u8Val, ac1Val);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
    }
    u4len = (UINT4)(STRLEN("CSUM_ERR_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("CSUM_ERR_KEY"):(sizeof(pHttp->au1KeyString)-1));
    STRNCPY (pHttp->au1KeyString, "CSUM_ERR_KEY",u4len);
    pHttp->au1KeyString[u4len]='\0';
    u4len = 0;
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    Val.msn = 0;
    Val.lsn = 0;
    u8Val.u4Hi = 0;
    u8Val.u4Lo = 0;
    MEMSET ((UINT1 *) ac1Val, 0, VRRP_U8_STR_LENGTH);

    if (i4Version == VRRP_VERSION_2)
    {
        u4Val = 0;
        nmhGetVrrpRouterVersionErrors (&u4Val);

        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%d", u4Val);
    }
    else
    {
        nmhGetVrrpv3RouterVersionErrors (&Val);

        u8Val.u4Hi = Val.msn;
        u8Val.u4Lo = Val.lsn;

        FSAP_U8_2STR (&u8Val, ac1Val);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
    }
    u4len = (UINT4)(STRLEN("VERSION_ERR_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("VERSION_ERR_KEY"):(sizeof(pHttp->au1KeyString)-1));
    STRNCPY (pHttp->au1KeyString, "VERSION_ERR_KEY",u4len);
    pHttp->au1KeyString[u4len] ='\0';
    u4len = 0;
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    Val.msn = 0;
    Val.lsn = 0;
    u8Val.u4Hi = 0;
    u8Val.u4Lo = 0;
    MEMSET ((UINT1 *) ac1Val, 0, VRRP_U8_STR_LENGTH);
    if (i4Version == VRRP_VERSION_2)
    {
        u4Val = 0;
        nmhGetVrrpRouterVrIdErrors (&u4Val);

        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%d", u4Val);
    }
    else
    {

        nmhGetVrrpv3RouterVrIdErrors (&Val);

        u8Val.u4Hi = Val.msn;
        u8Val.u4Lo = Val.lsn;

        FSAP_U8_2STR (&u8Val, ac1Val);

        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
    }
    u4len = (UINT4)(STRLEN("VRID_ERR_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("VRID_ERR_KEY"):(sizeof(pHttp->au1KeyString)-1));
    STRNCPY (pHttp->au1KeyString, "VRID_ERR_KEY",u4len);
    pHttp->au1KeyString[u4len] ='\0';
    u4len = 0;
    WebnmSendString (pHttp, pHttp->au1KeyString);
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    /* Per VRID Statistics. */
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    if (i4Version == VRRP_VERSION_2)
    {
        u4Val = 0;
        i4OutCome =
            nmhGetFirstIndexVrrpRouterStatsTable (&i4CurrIndex, &i4CurrVrId);
        i4AddrType = IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        i4OutCome = (INT4) (nmhGetFirstIndexVrrpv3StatisticsTable (&i4CurrIndex,
                                                                   &i4CurrVrId,
                                                                   &i4AddrType));
    }
    if (i4OutCome == SNMP_FAILURE)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    u4Temp = (UINT4) pHttp->i4Write;

    do
    {
        pHttp->i4Write = (INT4) u4Temp;
	u4len = (UINT4)(STRLEN("VRID_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("VRID_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "VRID_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (INT4), "%d",
                  i4CurrVrId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

	u4len = (UINT4)(STRLEN("INTERFACE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("INTERFACE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "INTERFACE_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaGetIfInfo ((UINT4) i4CurrIndex, &IfInfo);
        STRNCPY (pHttp->au1DataString, IfInfo.au1IfName,
                 (sizeof (IfInfo.au1IfName)));
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

	u4len = (UINT4)(STRLEN("ADDR_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ADDR_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ADDR_KEY",u4len);
        pHttp->au1KeyString[u4len]='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("ipv4") + 1),
                      "%s", "ipv4");
        }
        else
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("ipv6") + 1),
                      "%s", "ipv6");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        if (VrrpCmnGetAllStatsTable (i4Version, i4CurrIndex, i4CurrVrId,
                                     i4AddrType, &Stats) == VRRP_NOT_OK)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp, (CONST INT1 *) "Unable to get Stats Table");
            return;
        }
        u4RetVal = Stats.u4BecomeMaster;
	u4len = (UINT4)(STRLEN("MASTER_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("MASTER_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "MASTER_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);
	u4len = (UINT4)(STRLEN("NEW_MASTER_REASON_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("NEW_MASTER_REASON_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "NEW_MASTER_REASON_KEY", u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,
                  (STRLEN (pac1NewMasterReason[Stats.u1NewMasterReason]) + 1),
                  "%s", pac1NewMasterReason[Stats.u1NewMasterReason]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        FSAP_U8_2STR (&(Stats.u8RcvdAdvertisements), ac1Val);
	u4len = (UINT4)(STRLEN("ADRX_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ADRX_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ADRX_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);
        FSAP_U8_2STR (&(Stats.u8TxedAdvertisments), ac1Val);

	u4len = (UINT4)(STRLEN("ADTX_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ADTX_KEY"):(sizeof(pHttp->au1KeyString)-1));	
        STRNCPY (pHttp->au1KeyString, "ADTX_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

        FSAP_U8_2STR (&(Stats.u8TxedV2Advertisments), ac1Val);

	u4len = (UINT4)(STRLEN("ADTXV2_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ADTXV2_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ADTXV2_KEY",u4len);
        pHttp->au1KeyString[u4len] = '\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);
        FSAP_U8_2STR (&(Stats.u8V2AdvtIgnored), ac1Val);
	u4len = (UINT4)(STRLEN("V2_IGNORED")<(sizeof(pHttp->au1KeyString)) ? STRLEN("V2_IGNORED"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "V2_IGNORED",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, VRRP_U8_STR_LENGTH, "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsVrrpv3StatisticsSkewTime (i4CurrIndex, i4CurrVrId, i4AddrType,
                                          &i4RetVal);
        i4RetVal = i4RetVal * 10;
	u4len = (UINT4)(STRLEN("SKEW_TIME_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("SKEW_TIME_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "SKEW_TIME_KEY",u4len);
        pHttp->au1KeyString[u4len]='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (INT4), "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsVrrpv3StatisticsMasterDownInterval (i4CurrIndex, i4CurrVrId,
                                                    i4AddrType, &i4RetVal);
        i4RetVal = (i4RetVal * 10);
	u4len = (UINT4)(STRLEN("MAS_DOWN_INT_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("MAS_DOWN_INT_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "MAS_DOWN_INT_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (sizeof (INT4) + 1), "%d",
                  i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        nmhGetFsVrrpv3StatisticsMasterAdverInterval (i4CurrIndex, i4CurrVrId,
                                                     i4AddrType, &i4RetVal);
        i4RetVal = (i4RetVal * 10);
	u4len = (UINT4)(STRLEN("RECVD_MASTER_INT_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("RECVD_MASTER_INT_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "RECVD_MASTER_INT_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (sizeof (INT4) + 1), "%d",
                  i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

        FSAP_U8_2STR (&(Stats.u8AdvtIntervalErrors), ac1Val);

	u4len = (UINT4)(STRLEN("ADINT_ERR_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ADINT_ERR_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ADINT_ERR_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4RetVal = 0;
        if (i4Version == VRRP_VERSION_2)
        {
            nmhGetVrrpStatsAuthFailures (i4CurrIndex, i4CurrVrId, &u4RetVal);
        }
	u4len = (UINT4)(STRLEN("AUTH_FAIL_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("AUTH_FAIL_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "AUTH_FAIL_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);
        FSAP_U8_2STR (&(Stats.u8IpTtlErrors), ac1Val);
	u4len = (UINT4)(STRLEN("IP_TTL_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("IP_TTL_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "IP_TTL_KEY",u4len);
        pHttp->au1KeyString[u4len] = '\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

        FSAP_U8_2STR (&(Stats.u8RcvdPriZeroPackets), ac1Val);

	u4len = (UINT4)(STRLEN("PKT0_PRI_RX_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("PKT0_PRI_RX_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "PKT0_PRI_RX_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

        FSAP_U8_2STR (&(Stats.u8SentPriZeroPackets), ac1Val);

	u4len = (UINT4)(STRLEN("PKT0_PRI_TX_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("PKT0_PRI_TX_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "PKT0_PRI_TX_KEY",u4len);
        pHttp->au1KeyString[u4len]  ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

        FSAP_U8_2STR (&(Stats.u8RcvdInvalidTypePackets), ac1Val);

	u4len = (UINT4)(STRLEN("INVALID_PKT_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("INVALID_PKT_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "INVALID_PKT_KEY", u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

        FSAP_U8_2STR (&(Stats.u8AddressListErrors), ac1Val);

	u4len = (UINT4)(STRLEN("ADDR_LIST_ERR_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ADDR_LIST_ERR_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ADDR_LIST_ERR_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len =0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        u4RetVal = 0;
        if (i4Version == VRRP_VERSION_2)
        {
            nmhGetVrrpStatsInvalidAuthType (i4CurrIndex, i4CurrVrId, &u4RetVal);
        }
	u4len = (UINT4)(STRLEN("INVALID_AUTH_TYPE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("INVALID_AUTH_TYPE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "INVALID_AUTH_TYPE_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4RetVal = 0;
        if (i4Version == VRRP_VERSION_2)
        {
            nmhGetVrrpStatsAuthTypeMismatch (i4CurrIndex, i4CurrVrId,
                                             &u4RetVal);
        }
	u4len = (UINT4)(STRLEN("MIS_AUTH_TYPE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("MIS_AUTH_TYPE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "MIS_AUTH_TYPE_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (UINT4), "%u",
                  u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);
        FSAP_U8_2STR (&(Stats.u8PacketLengthErrors), ac1Val);
	u4len = (UINT4)(STRLEN("PKT_LEN_ERR_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("PKT_LEN_ERR_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "PKT_LEN_ERR_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (VRRP_U8_STR_LENGTH + 1), "%s",
                  ac1Val);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

	u4len = (UINT4)(STRLEN("PROTO_ERR_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("PROTO_ERR_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "PROTO_ERR_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString,
                  (STRLEN (pac1LastProtoError[Stats.u1ProtoErrReason]) + 1),
                  "%s", pac1LastProtoError[Stats.u1ProtoErrReason]);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));

        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        u4Ret =
            (UINT4) nmhGetNextIndexVrrpv3StatisticsTable (i4CurrIndex,
                                                          &i4NextIndex,
                                                          i4CurrVrId,
                                                          &i4NextVrrpId,
                                                          i4AddrType,
                                                          &i4NextAddrType);

        if (i4CurrIndex == i4NextIndex && i4CurrVrId == i4NextVrrpId
            && i4AddrType == i4NextAddrType)
        {
            break;
        }
        i4CurrIndex = i4NextIndex;
        i4CurrVrId = i4NextVrrpId;
        i4AddrType = i4NextAddrType;
    }
    while (u4Ret);

    VRRP_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : VrrpWebAssocPageGet                                                                 *  Description   : This function processes the request coming for the
 *                  Vrrp Associated Ip Page .                                                           *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.                                                                               *  Return Values : None
 *********************************************************************/
VOID
VrrpWebAssocPageGet (tHttp * pHttp)
{
    tCfaIfInfo          IfInfo;
    tSNMP_OCTET_STRING_TYPE NextAssocIp;
    tSNMP_OCTET_STRING_TYPE AssocIp;
    tVrrpIPvXAddr       AssoIpAddr;
    UINT1               au1IpAddress[IPVX_IPV6_ADDR_LEN];
    UINT1               au1NextIpAddress[IPVX_IPV6_ADDR_LEN];
    UINT1              *pu1String = NULL;
    INT4                i4Version = 0;
    INT4                i4CurrIndex = 0;
    INT4                i4CurrVrId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4OutCome = 0;
    INT4                i4NextIndex = 0;
    INT4                i4NextVrId = 0;
    INT4                i4NextAddrType = 0;
    UINT4               u4AssocIp = 0;
    UINT4               u4Temp = 0;
    UINT4               u4Ret = 0;
    UINT4               u4NextAssocIp = 0;
    UINT4               u4PrimIp = 0;
    UINT4		u4len = 0;

    MEMSET (au1IpAddress, 0, IPVX_IPV6_ADDR_LEN);
    MEMSET ((UINT1 *) &IfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (au1NextIpAddress, 0, IPVX_IPV6_ADDR_LEN);
    AssocIp.pu1_OctetList = au1IpAddress;
    AssocIp.i4_Length = IPVX_IPV6_ADDR_LEN;
    NextAssocIp.pu1_OctetList = au1NextIpAddress;

    pHttp->i4Write = 0;

    IssPrintAvailableIpInterfacesAndIPAddr (pHttp);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4Temp = (UINT4) pHttp->i4Write;

    WebnmRegisterLock (pHttp, VrrpLock, VrrpUnLock);
    VRRP_LOCK ();

    nmhGetFsVrrpVersionSupported (&i4Version);
    if (i4Version == VRRP_VERSION_2)
    {
        i4OutCome =
            nmhGetFirstIndexVrrpAssoIpAddrTable (&i4CurrIndex, &i4CurrVrId,
                                                 &u4AssocIp);

        i4CurrAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        i4OutCome = nmhGetFirstIndexVrrpv3AssociatedIpAddrTable (&i4CurrIndex,
                                                                 &i4CurrVrId,
                                                                 &i4CurrAddrType,
                                                                 &AssocIp);
    }

    if (i4OutCome == SNMP_FAILURE)
    {
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        return;
    }

    do
    {

        pHttp->i4Write = (INT4) u4Temp;

	u4len = (UINT4)(STRLEN("VRID_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("VRID_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "VRID_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SNPRINTF ((CHR1 *) pHttp->au1DataString, sizeof (INT4), "%d",
                  i4CurrVrId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

	u4len = (UINT4)(STRLEN("VLAN_ID_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("VLAN_ID_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "VLAN_ID_KEY",u4len);
        pHttp->au1KeyString[u4len] = '\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        CfaGetIfInfo ((UINT4) i4CurrIndex, &IfInfo);
        STRNCPY (pHttp->au1DataString, IfInfo.au1IfName,
                 (sizeof (IfInfo.au1IfName)));
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

	u4len = (UINT4)(STRLEN("ADDRESS_TYPE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ADDRESS_TYPE_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ADDRESS_TYPE_KEY",u4len);
        pHttp->au1KeyString[u4len]='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4CurrAddrType == IPVX_ADDR_FMLY_IPV4)
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("ipv4") + 1),
                      "%s", "ipv4");
        }
        else
        {
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("ipv6") + 1),
                      "%s", "ipv6");
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

	u4len = (UINT4)(STRLEN("ASSOC_IP_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("ASSOC_IP_KEY"):(sizeof(pHttp->au1KeyString)-1));
        STRNCPY (pHttp->au1KeyString, "ASSOC_IP_KEY",u4len);
        pHttp->au1KeyString[u4len] ='\0';
	u4len = 0;
        WebnmSendString (pHttp, pHttp->au1KeyString);

        if (i4Version == VRRP_VERSION_2)
        {
            nmhGetVrrpOperPrimaryIpAddr (i4CurrIndex, i4CurrVrId, &u4PrimIp);
            if (u4AssocIp == u4PrimIp)
            {

                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4AssocIp);
                SNPRINTF ((CHR1 *) pHttp->au1DataString,
                          (IPVX_IPV6_ADDR_LEN + 8), "%s", pu1String);
            }
            else
            {
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4AssocIp);
                SNPRINTF ((CHR1 *) pHttp->au1DataString,
                          (IPVX_IPV6_ADDR_LEN + 8), "%s", pu1String);
            }
        }
        else
        {

            VRRP_IPVX_ADDR_INIT_IPVX (AssoIpAddr, (UINT1) i4CurrAddrType,
                                      AssocIp.pu1_OctetList);
            SNPRINTF ((CHR1 *) pHttp->au1DataString, (IPVX_IPV6_ADDR_LEN + 8),
                      "%s", VRRP_IPVX_PRINT_ADDR (AssoIpAddr, i4CurrAddrType));
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        if (i4Version == VRRP_VERSION_2)
        {
            u4Ret = (nmhGetNextIndexVrrpAssoIpAddrTable (i4CurrIndex,
                                                         &i4NextIndex,
                                                         i4CurrVrId,
                                                         &i4NextVrId,
                                                         u4AssocIp,
                                                         &u4NextAssocIp)
                     == SNMP_SUCCESS);
            i4NextAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
            i4CurrIndex = i4NextIndex;
            i4CurrVrId = i4NextVrId;
            i4CurrAddrType = i4NextAddrType;
            u4AssocIp = u4NextAssocIp;
        }
        else
        {
            u4Ret = (nmhGetNextIndexVrrpv3AssociatedIpAddrTable (i4CurrIndex,
                                                                 &i4NextIndex,
                                                                 i4CurrVrId,
                                                                 &i4NextVrId,
                                                                 i4CurrAddrType,
                                                                 &i4NextAddrType,
                                                                 &AssocIp,
                                                                 &NextAssocIp)
                     == SNMP_SUCCESS);

            i4CurrIndex = i4NextIndex;
            i4CurrVrId = i4NextVrId;
            i4CurrAddrType = i4NextAddrType;
            MEMCPY (&AssocIp, &NextAssocIp, sizeof (tSNMP_OCTET_STRING_TYPE));
        }

    }
    while (u4Ret);

    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    VRRP_UNLOCK ();

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    WebnmUnRegisterLock (pHttp);
    return;

}

/*********************************************************************
 *  Function Name : VrrpWebBasicGet                                                                     *  Description   : This function processes the request coming for the
 *                  Vrrp Global Settings Page.                                                          *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.                                                                               *  Return Values : None
 *********************************************************************/
VOID
VrrpWebBasicGet (tHttp * pHttp)
{
    INT4                i4Version = 0;
    INT4                i4Status = 0;
    INT4                i4AuthDep = 0;
    INT4                i4NtnCtl = 0;
    UINT4		u4len = 0;

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    WebnmRegisterLock (pHttp, VrrpLock, VrrpUnLock);
    VRRP_LOCK ();

    nmhGetFsVrrpVersionSupported (&i4Version);
    u4len = (UINT4)(STRLEN("VRRP_VERSION_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("VRRP_VERSION_KEY"):(sizeof(pHttp->au1KeyString)-1));
    STRNCPY (pHttp->au1KeyString, "VRRP_VERSION_KEY",u4len);
    pHttp->au1KeyString[u4len] ='\0';
    u4len = 0;
    WebnmSendString (pHttp, pHttp->au1KeyString);

    if (i4Version == VRRP_VERSION_2)
    {
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("v2") + 1), "%s",
                  "v2");
    }
    else if (i4Version == VRRP_VERSION_3)
    {
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("v3") + 1), "%s",
                  "v3");
    }
    else
    {
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("v2-v3") + 1), "%s",
                  "v2-v3");

    }
    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    if (i4Version == VRRP_VERSION_2)
    {
        nmhGetFsVrrpStatus (&i4Status);
    }
    else
    {
        nmhGetFsVrrpv3Status (&i4Status);
    }
    u4len = (UINT4)(STRLEN("VRRP_STATUS_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("VRRP_STATUS_KEY"):(sizeof(pHttp->au1KeyString)-1));
    STRNCPY (pHttp->au1KeyString, "VRRP_STATUS_KEY",u4len);
    pHttp->au1KeyString[u4len] ='\0';
    u4len = 0;
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (i4Status == 1)
    {
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("enabled") + 1), "%s",
                  "enabled");
    }
    else
    {
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("disabled") + 1),
                  "%s", "disabled");
    }

    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    if (i4Version == VRRP_VERSION_2)
    {
        nmhGetFsVrrpAuthDeprecate (&i4AuthDep);
    }
    else
    {
        i4AuthDep = 2;
    }

    u4len = (UINT4)(STRLEN("AUTH_DEPRECATE_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("AUTH_DEPRECATE_KEY"):(sizeof(pHttp->au1KeyString)-1));
    STRNCPY (pHttp->au1KeyString, "AUTH_DEPRECATE_KEY",u4len);
    pHttp->au1KeyString[u4len] ='\0';
    u4len = 0;
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (i4AuthDep == 1)
    {
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("enabled") + 1), "%s",
                  "enabled");
    }
    else
    {
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("disabled") + 1),
                  "%s", "disabled");
    }

    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

    if (i4Version == VRRP_VERSION_2)
    {
        nmhGetVrrpNotificationCntl (&i4NtnCtl);
    }
    else
    {
        nmhGetFsVrrpv3NotificationCntl (&i4NtnCtl);
    }
    u4len = (UINT4)(STRLEN("NOTIF_CNTL_KEY")<(sizeof(pHttp->au1KeyString)) ? STRLEN("NOTIF_CNTL_KEY"):(sizeof(pHttp->au1KeyString)-1));
    STRNCPY (pHttp->au1KeyString, "NOTIF_CNTL_KEY",u4len);
    pHttp->au1KeyString[u4len] ='\0';
    u4len = 0;
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (i4NtnCtl == 1)
    {
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("enabled") + 1), "%s",
                  "enabled");
    }
    else
    {
        SNPRINTF ((CHR1 *) pHttp->au1DataString, (STRLEN ("disabled") + 1),
                  "%s", "disabled");
    }

    WebnmSockWrite (pHttp, pHttp->au1DataString,
                    (INT4) STRLEN (pHttp->au1DataString));
    WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

    VRRP_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;
}

/*********************************************************************
 *  Function Name : VrrpWebAssocPageSet                                                                 *  Description   : This function processes the request coming for the
 *                  Vrrp Associated Ip Page Set.                                                        *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.                                                                               *  Return Values : None
 *********************************************************************/

VOID
VrrpWebAssocPageSet (tHttp * pHttp)
{
    tUtlIn6Addr         ipv6AssocAddr;
    UINT1               au1IpAddr[IPVX_IPV6_ADDR_LEN];
    INT4                i4Version = 0;
    INT4                i4CurrIndex = 0;
    INT4                i4CurrVrId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4VlanId = 0;
    INT4                i4RetVal = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Ret = 0;
    UINT4               u4Option = 0;
    UINT4               u4SecIPAddr = 0;
    UINT4		u4len = 0;

    MEMSET (&ipv6AssocAddr, 0, sizeof (tUtlIn6Addr));
    MEMSET (au1IpAddr, 0, sizeof (au1IpAddr));

    nmhGetFsVrrpVersionSupported (&i4Version);
    u4len = (UINT4)(STRLEN("VR_ID")<(sizeof(pHttp->au1Name)) ? STRLEN("VR_ID"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "VR_ID",u4len);
    pHttp->au1Name[u4len]='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CurrVrId = ATOI (pHttp->au1Value);
    u4len = (UINT4)(STRLEN("VLAN_ID")<(sizeof(pHttp->au1Name)) ? STRLEN("VLAN_ID"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "VLAN_ID",u4len);
    pHttp->au1Name[u4len]='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = ATOI (pHttp->au1Value);

    WebnmRegisterLock (pHttp, VrrpLock, VrrpUnLock);
    VRRP_LOCK ();
    u4len = (UINT4)(STRLEN("ACTION")<(sizeof(pHttp->au1Name)) ? STRLEN("ACTION"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "ACTION",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRNCMP (pHttp->au1Value, "Delete", STRLEN ("Delete")) == 0)
    {
	u4len = (UINT4)(STRLEN("INTERFACE")<(sizeof(pHttp->au1Name)) ? STRLEN("INTERFACE"):(sizeof(pHttp->au1Name)-1));
        STRNCPY (pHttp->au1Name, "INTERFACE",u4len);
        pHttp->au1Name[u4len] ='\0';
	u4len = 0;
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Ret =
            CfaGetInterfaceIndexFromName ((UINT1 *) pHttp->au1Value,
                                          &u4IfIndex);
        if (u4Ret == OSIX_FAILURE)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            return;
        }

        i4VlanId = (INT4) u4IfIndex;

        i4CurrIndex = (INT4) u4IfIndex;
	u4len = (UINT4)(STRLEN("AddressType")<(sizeof(pHttp->au1Name)) ? STRLEN("AddressType"):(sizeof(pHttp->au1Name)-1));
        STRNCPY (pHttp->au1Name, "AddressType",u4len);
        pHttp->au1Name[u4len] ='\0';
	u4len = 0;
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRCMP (pHttp->au1Value, "ipv4") == 0)
        {
            i4CurrAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
        }
        else
        {
            i4CurrAddrType = VRRP_IPVX_ADDR_FMLY_IPV6;
        }
	u4len = (UINT4)(STRLEN("SECONDARY_IP_ADDR")<(sizeof(pHttp->au1Name)) ? STRLEN("SECONDARY_IP_ADDR"):(sizeof(pHttp->au1Name)-1));
        STRNCPY (pHttp->au1Name, "SECONDARY_IP_ADDR",u4len);
        pHttp->au1Name[u4len] ='\0';
	u4len = 0;
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (i4CurrAddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
        {
            u4SecIPAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
        }
        else
        {
            issDecodeSpecialChar (pHttp->au1Value);
            INET_ATON6 (pHttp->au1Value, &ipv6AssocAddr);

        }

        if (i4Version == VRRP_VERSION_2)
        {
            if (VrrpCmnDelV2IpAddr (i4CurrIndex, i4CurrVrId, i4CurrAddrType,
                                    u4SecIPAddr, 2) == VRRP_NOT_OK)
            {
                if (VrrpCmnDelV2IpAddr (i4CurrIndex, i4CurrVrId, i4CurrAddrType,
                                        u4SecIPAddr, 1) == VRRP_NOT_OK)
                {
                    VRRP_UNLOCK ();
                    WebnmUnRegisterLock (pHttp);

                    IssSendError (pHttp,
                                  (CONST INT1 *)
                                  "error in deleting primary ip ");
                    return;

                }
            }
        }
        else
        {
            if (i4CurrAddrType == 1)
            {
                u4SecIPAddr = OSIX_HTONL (u4SecIPAddr);
                MEMCPY (au1IpAddr, (UINT1 *) &u4SecIPAddr, IPVX_IPV4_ADDR_LEN);

                if (VrrpCmnDelV3IpAddr (i4CurrIndex, i4CurrVrId, i4CurrAddrType,
                                        au1IpAddr, 2) == VRRP_NOT_OK)
                {
                    if (VrrpCmnDelV3IpAddr
                        (i4CurrIndex, i4CurrVrId, i4CurrAddrType, au1IpAddr,
                         1) == VRRP_NOT_OK)
                    {
                        VRRP_UNLOCK ();
                        WebnmUnRegisterLock (pHttp);

                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "error in deleting primary ip ");
                        return;
                    }
                }
            }

            else
            {
                if (VrrpCmnDelV3IpAddr (i4CurrIndex, i4CurrVrId, i4CurrAddrType,
                                        ipv6AssocAddr.u1addr, 2) == VRRP_NOT_OK)
                {
                    if (VrrpCmnDelV3IpAddr
                        (i4CurrIndex, i4CurrVrId, i4CurrAddrType,
                         ipv6AssocAddr.u1addr, 1) == VRRP_NOT_OK)
                    {
                        VRRP_UNLOCK ();
                        WebnmUnRegisterLock (pHttp);

                        IssSendError (pHttp,
                                      (CONST INT1 *)
                                      "error in deleting primary ip");
                        return;
                    }
                }
            }

        }
        VRRP_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        VrrpWebAssocPageGet (pHttp);

        return;

    }
    u4len = (UINT4)(STRLEN("ADDRESS_TYPE")<(sizeof(pHttp->au1Name)) ? STRLEN("ADDRESS_TYPE"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "ADDRESS_TYPE",u4len);
    pHttp->au1Name[u4len] ='\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CurrAddrType = ATOI (pHttp->au1Value);
    u4len = (UINT4)(STRLEN("SECONDARY_IP_ADDR")<(sizeof(pHttp->au1Name)) ? STRLEN("SECONDARY_IP_ADDR"):(sizeof(pHttp->au1Name)-1));
    STRNCPY (pHttp->au1Name, "SECONDARY_IP_ADDR",u4len);
    pHttp->au1Name[u4len] = '\0';
    u4len = 0;
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (i4CurrAddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        u4SecIPAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }
    else
    {
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &ipv6AssocAddr);

    }

    if (i4Version == VRRP_VERSION_2)
    {
        u4Option = 2;
        i4RetVal =
            VrrpCmnSetV2IpAddr (i4VlanId, i4CurrVrId, i4CurrAddrType,
                                u4SecIPAddr, u4Option);
        if (i4RetVal == VRRP_NOT_OK)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *) "Sec Ip fail. Cannot create Row.");
            return;
        }
    }
    else
    {
        u4SecIPAddr = OSIX_HTONL (u4SecIPAddr);
        u4Option = 2;
        MEMCPY (au1IpAddr, (UINT1 *) &u4SecIPAddr, IPVX_IPV4_ADDR_LEN);
        if (i4CurrAddrType == 1)
        {
            i4RetVal =
                VrrpCmnSetV3IpAddr (i4VlanId, i4CurrVrId, i4CurrAddrType,
                                    au1IpAddr, u4Option);
        }
        else
        {
            i4RetVal =
                VrrpCmnSetV3IpAddr (i4VlanId, i4CurrVrId, i4CurrAddrType,
                                    ipv6AssocAddr.u1addr, u4Option);

        }
        if (i4RetVal == VRRP_NOT_OK)
        {
            VRRP_UNLOCK ();
            WebnmUnRegisterLock (pHttp);

            IssSendError (pHttp,
                          (CONST INT1 *) "Sec Ip fail. Cannot create Row.");
            return;
        }
    }

    VRRP_UNLOCK ();
    WebnmUnRegisterLock (pHttp);

    VrrpWebAssocPageGet (pHttp);
    return;
}

#endif
#endif
#endif
