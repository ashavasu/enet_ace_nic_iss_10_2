/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: vrrpcmn.c,v 1.7 2017/12/16 11:57:11 siva Exp $
 *
 * Description: This file contains Get All routines for all tables
 *              of VRRP.
 *
 *******************************************************************/
#include "vrrpinc.h"
#include "cli.h"
#include "vrrpcli.h"
#include "stdvrrcli.h"
#include "fsvrrpcli.h"
#include "vrrp3cli.h"
#include "fsvr3cli.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetVrrpStatus                             */
/*                                                                           */
/*    Description         : This function enables / disables VRRP on the     */
/*                          router with based on the version.                */
/*                                                                           */
/*    Input(s)            : i4Status     - Enable or Disable Status          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetVrrpStatus (INT4 i4Status)
{
    INT4                i4Version = 0;
    INT4                i4RetVal = 0;
    UINT4               u4Error = 0;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return VRRP_NOT_OK;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhTestv2FsVrrpStatus (&u4Error, i4Status) != SNMP_SUCCESS)
        {
            return VRRP_NOT_OK;
        }

        i4RetVal = nmhSetFsVrrpStatus (i4Status);
    }
    else
    {
        if (nmhTestv2FsVrrpv3Status (&u4Error, i4Status) != SNMP_SUCCESS)
        {
            return VRRP_NOT_OK;
        }

        i4RetVal = nmhSetFsVrrpv3Status (i4Status);
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetV3AssocIpRowStatus                     */
/*                                                                           */
/*    Description         : This function sets Assoc Ip Row Status for V3    */
/*                                                                           */
/*    Input(s)            : i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          pIpAddr      - IP Address                        */
/*                          i4RowStatus  - Row Status Value                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetV3AssocIpRowStatus (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                              tSNMP_OCTET_STRING_TYPE * pIpAddr,
                              INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Vrrpv3AssociatedIpAddrRowStatus (&u4ErrorCode, i4IfIndex,
                                                  i4VrId, i4AddrType,
                                                  pIpAddr, i4RowStatus)
        == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    if (nmhSetVrrpv3AssociatedIpAddrRowStatus (i4IfIndex, i4VrId, i4AddrType,
                                               pIpAddr, i4RowStatus)
        == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetV2AssocIpRowStatus                     */
/*                                                                           */
/*    Description         : This function sets Associated IP Address Row     */
/*                          Status for V2                                    */
/*                                                                           */
/*    Input(s)            : i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          u4IpAddr     - IP Address                        */
/*                          i4RowStatus  - RowStatus                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS or CLI_FAILURE                       */
/*****************************************************************************/
INT4
VrrpCmnSetV2AssocIpRowStatus (INT4 i4IfIndex, INT4 i4VrId, UINT4 u4IpAddr,
                              INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2VrrpAssoIpAddrRowStatus (&u4ErrorCode, i4IfIndex,
                                          i4VrId, u4IpAddr, i4RowStatus)
        == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    if (nmhSetVrrpAssoIpAddrRowStatus (i4IfIndex, i4VrId, u4IpAddr, i4RowStatus)
        == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetOperRowStatus                          */
/*                                                                           */
/*    Description         : This function sets V2 or V3 Oper Row Status      */
/*                          based on Version.                                */
/*                                                                           */
/*    Input(s)            : i4Version    - Version                           */
/*                          i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          i4RowStatus  - Row Status                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS or CLI_FAILURE                       */
/*****************************************************************************/
INT4
VrrpCmnSetOperRowStatus (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                         INT4 i4AddrType, INT4 i4RowStatus)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4ErrorCode = 0;

    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhTestv2VrrpOperRowStatus (&u4ErrorCode, i4IfIndex, i4VrId,
                                        i4RowStatus) == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        i1RetVal = nmhSetVrrpOperRowStatus (i4IfIndex, i4VrId, i4RowStatus);
    }
    else
    {
        if (nmhTestv2Vrrpv3OperationsRowStatus (&u4ErrorCode, i4IfIndex, i4VrId,
                                                i4AddrType, i4RowStatus)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        i1RetVal =
            nmhSetVrrpv3OperationsRowStatus (i4IfIndex, i4VrId, i4AddrType,
                                             i4RowStatus);
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetAdminState                             */
/*                                                                           */
/*    Description         : This function sets Admin State Object for V2     */
/*                                                                           */
/*    Input(s)            : i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AdminState - Admin State                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
VrrpCmnSetAdminState (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AdminState)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2VrrpOperAdminState (&u4ErrorCode, i4IfIndex, i4VrId,
                                     i4AdminState) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhSetVrrpOperAdminState (i4IfIndex, i4VrId, i4AdminState) ==
        SNMP_FAILURE)
    {
        return;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetV2PrimaryIpAddr                        */
/*                                                                           */
/*    Description         : This function sets Primary Address for V2.       */
/*                                                                           */
/*    Input(s)            : i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          u4IpAddr     - IP Address                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetV2PrimaryIpAddr (INT4 i4IfIndex, INT4 i4VrId, UINT4 u4IpAddr)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2VrrpOperPrimaryIpAddr (&u4ErrorCode, i4IfIndex, i4VrId,
                                        u4IpAddr) == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    if (nmhSetVrrpOperPrimaryIpAddr (i4IfIndex, i4VrId, u4IpAddr)
        == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetV3PrimaryIpAddr                        */
/*                                                                           */
/*    Description         : This function sets Primary for V3                */
/*                                                                           */
/*    Input(s)            : i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          pIpAddr      - IP Address                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetV3PrimaryIpAddr (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                           tSNMP_OCTET_STRING_TYPE * pIpAddr)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Vrrpv3OperationsPrimaryIpAddr (&u4ErrorCode, i4IfIndex,
                                                i4VrId, i4AddrType, pIpAddr)
        == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    if (nmhSetVrrpv3OperationsPrimaryIpAddr (i4IfIndex, i4VrId, i4AddrType,
                                             pIpAddr) == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetV2IpAddr                               */
/*                                                                           */
/*    Description         : This function sets V2 Ipv4 Primary Or Secondary  */
/*                          Address.                                         */
/*    Input(s)            : i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          u4IpAddr     - IP Address                        */
/*                          u4Option     - Primary or Secondary              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetV2IpAddr (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                    UINT4 u4IpAddr, UINT4 u4Option)
{
    tIPvXAddr           PrevIpAddr;
    tIPvXAddr           CurIpAddr;
    UINT4               u4PrimaryIpAddr = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4OperIndex = VRRP_NOT_OK;
    BOOL1               bOperEntryPresent = TRUE;

    VRRP_IPVX_ADDR_CLEAR (PrevIpAddr);
    VRRP_IPVX_ADDR_CLEAR (CurIpAddr);

    if (nmhGetVrrpOperPrimaryIpAddr (i4IfIndex, i4VrId, &u4PrimaryIpAddr)
        == SNMP_FAILURE)
    {
        /* Ignore Failure */
    }

    if (nmhGetVrrpAssoIpAddrRowStatus (i4IfIndex, i4VrId,
                                       u4IpAddr, &i4RowStatus) == SNMP_SUCCESS)
    {
        if ((u4Option == VRRP_CLI_PRIMARY) && (u4IpAddr != u4PrimaryIpAddr))
        {
            CLI_SET_ERR (CLI_VRRP_DUPLICATE_SECONDARY_IP);
            return VRRP_NOT_OK;
        }

        if ((u4Option == VRRP_CLI_SECONDARY) && (u4PrimaryIpAddr != 0) &&
            (u4IpAddr == u4PrimaryIpAddr))
        {
            CLI_SET_ERR (CLI_VRRP_DUPLICATE_PRIMARY_IP);
            return VRRP_NOT_OK;
        }

        return VRRP_OK;
    }

    if (nmhGetVrrpOperRowStatus (i4IfIndex, i4VrId, &i4RowStatus) ==
        SNMP_FAILURE)
    {
        if (u4Option == VRRP_CLI_SECONDARY)
        {
            CLI_SET_ERR (CLI_VRRP_SET_VRIP);
            return VRRP_NOT_OK;
        }

        bOperEntryPresent = FALSE;

        if (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId,
                                     i4AddrType, CREATE_AND_WAIT)
            == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    if (u4Option == VRRP_CLI_PRIMARY)
    {
        if (u4PrimaryIpAddr == u4IpAddr)
        {
            return VRRP_OK;
        }

        VRRP_IPVX_COPY_TO_IPVX (CurIpAddr, u4IpAddr);
        VRRP_IPVX_COPY_TO_IPVX (PrevIpAddr, u4PrimaryIpAddr);

        i4OperIndex = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

        if (i4OperIndex == VRRP_NOT_OK)
        {
            CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);
            return VRRP_NOT_OK;
        }

        if (VrrpValidateDecPriority (i4OperIndex,
                                     VRRP_VAL_PRIO_OPER_IP_MODIFY,
                                     PrevIpAddr, CurIpAddr,
                                     0, OPERDECPRIO (i4OperIndex))
            == VRRP_NOT_OK)
        {
            if ((bOperEntryPresent == FALSE) &&
                (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId,
                                          i4AddrType, DESTROY) == VRRP_NOT_OK))
            {
                /* Ignore Failure */
            }

            CLI_SET_ERR (CLI_VRRP_DEC_PRIO_NO_IP_MODIFY);

            return VRRP_NOT_OK;
        }

        OPERISTESTNOTREQD (i4OperIndex) = TRUE;
    }

    if ((u4Option == VRRP_CLI_SECONDARY) && (u4PrimaryIpAddr == 0))
    {
        CLI_SET_ERR (CLI_VRRP_PRIMARY_IP_DOWN);
        return VRRP_NOT_OK;
    }

    if ((bOperEntryPresent == TRUE) && (u4Option == VRRP_CLI_PRIMARY))
    {
        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINDOWN);

        if (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId,
                                     i4AddrType, NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    if ((u4Option == VRRP_CLI_PRIMARY) &&
        (nmhTestv2VrrpOperPrimaryIpAddr (&u4ErrorCode, i4IfIndex, i4VrId,
                                         u4IpAddr) == SNMP_FAILURE))
    {
        if ((bOperEntryPresent == FALSE) &&
            (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId,
                                      i4AddrType, DESTROY) == VRRP_NOT_OK))
        {
            /* Ignore Failure */
        }
        else if (bOperEntryPresent == TRUE)
        {
            if (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId,
                                         i4AddrType, ACTIVE) == VRRP_NOT_OK)
            {
                /* Ignore Failure */
            }

            VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINUP);
        }

        return VRRP_NOT_OK;
    }

    if (VrrpCmnSetV2AssocIpRowStatus (i4IfIndex, i4VrId, u4IpAddr,
                                      CREATE_AND_GO) == VRRP_NOT_OK)
    {
        if (bOperEntryPresent == TRUE)
        {
            if (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId,
                                         i4AddrType, ACTIVE) == VRRP_NOT_OK)
            {
                /* Ignore Failure */
            }

            VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINUP);
        }
        else
        {
            if (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId,
                                         i4AddrType, DESTROY) == VRRP_NOT_OK)
            {
                /* Ignore Failure */
            }
        }

        return VRRP_NOT_OK;
    }

    if ((u4Option == VRRP_CLI_PRIMARY) && (u4PrimaryIpAddr != 0))
    {
        if (VrrpCmnSetV2AssocIpRowStatus (i4IfIndex, i4VrId, u4PrimaryIpAddr,
                                          DESTROY) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    if ((u4Option == VRRP_CLI_PRIMARY) &&
        (VrrpCmnSetV2PrimaryIpAddr (i4IfIndex, i4VrId, u4IpAddr) ==
         VRRP_NOT_OK))
    {
        return VRRP_NOT_OK;
    }

    if (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId, i4AddrType,
                                 ACTIVE) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINUP);

    if (i4OperIndex != VRRP_NOT_OK)
    {
        OPERISTESTNOTREQD (i4OperIndex) = FALSE;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetV3IpAddr                               */
/*                                                                           */
/*    Description         : This function sets V3 IPv4 or IPv6 Primary Or    */
/*                          Secondary Option.                                */
/*    Input(s)            : i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          u4IpAddr     - IP Address                        */
/*                          u4Option     - Primary or Secondary              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetV3IpAddr (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                    UINT1 *pu1IpAddr, UINT4 u4Option)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tSNMP_OCTET_STRING_TYPE PrimaryIpAddr;
    tIPvXAddr           PrevIpAddr;
    tIPvXAddr           CurIpAddr;
    UINT1               au1ZeroIpAddr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1IpAddress[IPVX_IPV6_ADDR_LEN];
    INT4                i4RowStatus = 0;
    INT4                i4OperIndex = VRRP_NOT_OK;
    BOOL1               bOperEntryPresent = TRUE;
    UINT4               u4ErrorCode = 0;

    VRRP_IPVX_ADDR_CLEAR (PrevIpAddr);
    VRRP_IPVX_ADDR_CLEAR (CurIpAddr);
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrimaryIpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1IpAddress, 0, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1ZeroIpAddr, 0, IPVX_IPV6_ADDR_LEN);

    IpAddr.pu1_OctetList = pu1IpAddr;
    PrimaryIpAddr.pu1_OctetList = au1IpAddress;

    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        IpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
        PrimaryIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        IpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
        PrimaryIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    if (nmhGetVrrpv3OperationsPrimaryIpAddr (i4IfIndex, i4VrId, i4AddrType,
                                             &PrimaryIpAddr) == SNMP_FAILURE)
    {
        /* Ignore Failure */
    }

    if (nmhGetVrrpv3AssociatedIpAddrRowStatus (i4IfIndex, i4VrId, i4AddrType,
                                               &IpAddr, &i4RowStatus)
        == SNMP_SUCCESS)
    {
        if ((u4Option == VRRP_CLI_PRIMARY) &&
            (nmhTestv2Vrrpv3OperationsPrimaryIpAddr (&u4ErrorCode,
                                                     i4IfIndex, i4VrId,
                                                     i4AddrType,
                                                     &IpAddr) == SNMP_FAILURE))
        {
            return VRRP_NOT_OK;
        }

        if ((u4Option == VRRP_CLI_PRIMARY) &&
            (MEMCMP (PrimaryIpAddr.pu1_OctetList, IpAddr.pu1_OctetList,
                     IpAddr.i4_Length) != 0))
        {
            CLI_SET_ERR (CLI_VRRP_DUPLICATE_SECONDARY_IP);
            return VRRP_NOT_OK;
        }

        if ((u4Option == VRRP_CLI_SECONDARY) &&
            (MEMCMP (PrimaryIpAddr.pu1_OctetList, IpAddr.pu1_OctetList,
                     IpAddr.i4_Length) == 0))
        {
            CLI_SET_ERR (CLI_VRRP_DUPLICATE_PRIMARY_IP);
            return VRRP_NOT_OK;
        }

        return VRRP_OK;
    }

    if (nmhGetVrrpv3OperationsRowStatus (i4IfIndex, i4VrId, i4AddrType,
                                         &i4RowStatus) == SNMP_FAILURE)
    {
        if (u4Option == VRRP_CLI_SECONDARY)
        {
            CLI_SET_ERR (CLI_VRRP_SET_VRIP);
            return VRRP_NOT_OK;
        }

        bOperEntryPresent = FALSE;

        if (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                     i4AddrType, CREATE_AND_WAIT)
            == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    if (u4Option == VRRP_CLI_PRIMARY)
    {
        if (MEMCMP (PrimaryIpAddr.pu1_OctetList, IpAddr.pu1_OctetList,
                    IpAddr.i4_Length) == 0)
        {
            return VRRP_OK;
        }

        VRRP_IPVX_ADDR_INIT_IPVX (CurIpAddr, (UINT1) i4AddrType,
                                  IpAddr.pu1_OctetList);
        VRRP_IPVX_ADDR_INIT_IPVX (PrevIpAddr, (UINT1) i4AddrType,
                                  PrimaryIpAddr.pu1_OctetList);

        i4OperIndex = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

        if (i4OperIndex == VRRP_NOT_OK)
        {
            CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);
            return VRRP_NOT_OK;
        }

        if (VrrpValidateDecPriority (i4OperIndex,
                                     VRRP_VAL_PRIO_OPER_IP_MODIFY,
                                     PrevIpAddr, CurIpAddr,
                                     0, OPERDECPRIO (i4OperIndex))
            == VRRP_NOT_OK)
        {
            if ((bOperEntryPresent == FALSE) &&
                (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                          i4AddrType, DESTROY) == VRRP_NOT_OK))
            {
                /* Ignore Failure */
            }

            CLI_SET_ERR (CLI_VRRP_DEC_PRIO_NO_IP_MODIFY);

            return VRRP_NOT_OK;
        }

        OPERISTESTNOTREQD (i4OperIndex) = TRUE;
    }

    if ((u4Option == VRRP_CLI_PRIMARY) &&
        (nmhTestv2Vrrpv3OperationsPrimaryIpAddr (&u4ErrorCode,
                                                 i4IfIndex, i4VrId,
                                                 i4AddrType,
                                                 &IpAddr) == SNMP_FAILURE))
    {
        if ((bOperEntryPresent == FALSE) &&
            (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                      i4AddrType, DESTROY) == VRRP_NOT_OK))
        {
            /* Ignore Failure */
        }

        return VRRP_NOT_OK;
    }

    if ((u4Option == VRRP_CLI_SECONDARY) &&
        (MEMCMP (PrimaryIpAddr.pu1_OctetList, au1ZeroIpAddr,
                 PrimaryIpAddr.i4_Length) == 0))
    {
        CLI_SET_ERR (CLI_VRRP_PRIMARY_IP_DOWN);
        return VRRP_NOT_OK;
    }

    if ((bOperEntryPresent == TRUE) && (u4Option == VRRP_CLI_PRIMARY))
    {
        if (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                     i4AddrType, NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    if (VrrpCmnSetV3AssocIpRowStatus (i4IfIndex, i4VrId, i4AddrType,
                                      &IpAddr, CREATE_AND_GO) == VRRP_NOT_OK)
    {
        if ((bOperEntryPresent == FALSE) &&
            (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                      i4AddrType, DESTROY) == VRRP_NOT_OK))
        {
            /* Ignore Failure */
        }
        else if ((bOperEntryPresent == TRUE) &&
                 (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                           i4AddrType, ACTIVE) == VRRP_NOT_OK))
        {
            /* Ignore Failure */
        }

        return VRRP_NOT_OK;
    }

    if ((u4Option == VRRP_CLI_PRIMARY) &&
        (MEMCMP (PrimaryIpAddr.pu1_OctetList, au1ZeroIpAddr,
                 PrimaryIpAddr.i4_Length) != 0))
    {
        if (VrrpCmnSetV3AssocIpRowStatus (i4IfIndex, i4VrId, i4AddrType,
                                          &PrimaryIpAddr, DESTROY)
            == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    if ((u4Option == VRRP_CLI_PRIMARY) &&
        (VrrpCmnSetV3PrimaryIpAddr (i4IfIndex, i4VrId, i4AddrType, &IpAddr)
         == VRRP_NOT_OK))
    {
        if (bOperEntryPresent == FALSE)
        {
            if (VrrpCmnSetV3AssocIpRowStatus (i4IfIndex, i4VrId, i4AddrType,
                                              &IpAddr, DESTROY) == VRRP_NOT_OK)
            {
                /* Ignore Failure */
            }

            if (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                         i4AddrType, DESTROY) == VRRP_NOT_OK)
            {
                /* Ignore Failure */
            }
        }

        return VRRP_NOT_OK;
    }

    if (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                 i4AddrType, ACTIVE) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    if (i4OperIndex != VRRP_NOT_OK)
    {
        OPERISTESTNOTREQD (i4OperIndex) = FALSE;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnDelV2IpAddr                               */
/*                                                                           */
/*    Description         : This function deletes V2 Ipv4 Primary Or         */
/*                          Secondary Address.                               */
/*    Input(s)            : i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          u4IpAddr     - IP Address                        */
/*                          u4Option     - Primary or Secondary              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnDelV2IpAddr (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                    UINT4 u4IpAddr, UINT4 u4Option)
{
    UINT4               u4PrimaryIpAddr = 0;
    UINT4               u4ZeroIpAddr = 0;
    INT4                i4Count = 0;
    INT4                i4RowStatus = 0;
    INT4                i4Count1 = 0;

    if (nmhGetVrrpOperPrimaryIpAddr (i4IfIndex, i4VrId, &u4PrimaryIpAddr)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return VRRP_NOT_OK;
    }

    if (nmhGetVrrpOperIpAddrCount (i4IfIndex, i4VrId, &i4Count) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return VRRP_NOT_OK;
    }

    if (nmhGetVrrpAssoIpAddrRowStatus (i4IfIndex, i4VrId,
                                       u4IpAddr, &i4RowStatus) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_IP_NOT_FOUND);

        return VRRP_NOT_OK;
    }

    if ((u4Option == VRRP_CLI_PRIMARY) && (u4PrimaryIpAddr != u4IpAddr))
    {
        CLI_SET_ERR (CLI_VRRP_DEL_SEC_THRO_PRIMARY);

        return VRRP_NOT_OK;
    }
    else if ((u4Option == VRRP_CLI_SECONDARY) && (u4PrimaryIpAddr == u4IpAddr))
    {
        CLI_SET_ERR (CLI_VRRP_DEL_PRIMARY_THRO_SEC);

        return VRRP_NOT_OK;
    }

    if (i4Count == VRRP_ONE)
    {
        if (VrrpCmnSetTrackAndDecPriority (i4IfIndex, i4VrId, i4AddrType, 0, 0)
            == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINDOWN);

        if (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId,
                                     i4AddrType, NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    if (VrrpCmnSetV2AssocIpRowStatus (i4IfIndex, i4VrId, u4IpAddr,
                                      DESTROY) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    if (nmhGetVrrpOperIpAddrCount (i4IfIndex, i4VrId, &i4Count1) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return VRRP_NOT_OK;
    }

    if ((i4Count1 >= VRRP_ONE) && (u4Option == VRRP_CLI_PRIMARY))
    {
        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINDOWN);

        if (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId,
                                     i4AddrType, NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetV2PrimaryIpAddr (i4IfIndex, i4VrId, u4ZeroIpAddr)
            == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    if ((i4Count == VRRP_ONE) &&
        (VrrpCmnSetOperRowStatus (VRRP_VERSION_2, i4IfIndex, i4VrId,
                                  i4AddrType, DESTROY) == VRRP_NOT_OK))
    {
        return VRRP_NOT_OK;
    }

    if ((u4Option == VRRP_CLI_PRIMARY) && (i4Count > VRRP_ONE))
    {
        CLI_SET_ERR (CLI_VRRP_DEL_PRIMARY_IP);
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnDelV3IpAddr                               */
/*                                                                           */
/*    Description         : This function deletes V3 IPv4 or IPv6 Primary Or */
/*                          Secondary Option.                                */
/*    Input(s)            : i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          u4IpAddr     - IP Address                        */
/*                          u4Option     - Primary or Secondary              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnDelV3IpAddr (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                    UINT1 *pu1IpAddr, UINT4 u4Option)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tSNMP_OCTET_STRING_TYPE PrimaryIpAddr;
    tSNMP_OCTET_STRING_TYPE ZeroIpAddr;
    UINT1               au1IpAddress[IPVX_IPV6_ADDR_LEN];
    UINT1               au1ZeroIpAddr[IPVX_IPV6_ADDR_LEN];
    INT4                i4Count = 0;
    INT4                i4Count1 = 0;
    INT4                i4RowStatus = 0;

    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrimaryIpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ZeroIpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1IpAddress, 0, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1ZeroIpAddr, 0, IPVX_IPV6_ADDR_LEN);

    IpAddr.pu1_OctetList = pu1IpAddr;
    PrimaryIpAddr.pu1_OctetList = au1IpAddress;
    ZeroIpAddr.pu1_OctetList = au1ZeroIpAddr;

    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        IpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
        PrimaryIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
        ZeroIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        IpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
        PrimaryIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
        ZeroIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    if (nmhGetVrrpv3OperationsPrimaryIpAddr (i4IfIndex, i4VrId, i4AddrType,
                                             &PrimaryIpAddr) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return VRRP_NOT_OK;
    }

    if (nmhGetVrrpv3OperationsAddrCount (i4IfIndex, i4VrId, i4AddrType,
                                         &i4Count) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return VRRP_NOT_OK;
    }

    if (nmhGetVrrpv3AssociatedIpAddrRowStatus (i4IfIndex, i4VrId, i4AddrType,
                                               &IpAddr, &i4RowStatus)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_IP_NOT_FOUND);

        return VRRP_NOT_OK;
    }

    if ((u4Option == VRRP_CLI_PRIMARY) &&
        (MEMCMP (PrimaryIpAddr.pu1_OctetList, IpAddr.pu1_OctetList,
                 IpAddr.i4_Length) != 0))
    {
        CLI_SET_ERR (CLI_VRRP_DEL_SEC_THRO_PRIMARY);

        return VRRP_NOT_OK;
    }
    else if ((u4Option == VRRP_CLI_SECONDARY) &&
             (MEMCMP (PrimaryIpAddr.pu1_OctetList, IpAddr.pu1_OctetList,
                      IpAddr.i4_Length) == 0))
    {
        CLI_SET_ERR (CLI_VRRP_DEL_PRIMARY_THRO_SEC);
        return VRRP_NOT_OK;
    }

    if (i4Count == VRRP_ONE)
    {
        if (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                     i4AddrType, NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetTrackAndDecPriority (i4IfIndex, i4VrId, i4AddrType, 0, 0)
            == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    if (VrrpCmnSetV3AssocIpRowStatus (i4IfIndex, i4VrId, i4AddrType, &IpAddr,
                                      DESTROY) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    if (nmhGetVrrpv3OperationsAddrCount (i4IfIndex, i4VrId, i4AddrType,
                                         &i4Count1) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return VRRP_NOT_OK;
    }

    if ((i4Count1 >= VRRP_ONE) && (u4Option == VRRP_CLI_PRIMARY))
    {
        if (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                     i4AddrType, NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetV3PrimaryIpAddr
            (i4IfIndex, i4VrId, i4AddrType, &ZeroIpAddr) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    if ((i4Count == VRRP_ONE) &&
        (VrrpCmnSetOperRowStatus (VRRP_VERSION_3, i4IfIndex, i4VrId,
                                  i4AddrType, DESTROY) == VRRP_NOT_OK))
    {
        return VRRP_NOT_OK;
    }

    if ((u4Option == VRRP_CLI_PRIMARY) && (i4Count > VRRP_ONE))
    {
        CLI_SET_ERR (CLI_VRRP_DEL_PRIMARY_IP);
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetPriority                               */
/*                                                                           */
/*    Description         : This function sets Priority Object based on      */
/*                          version supported for both IPv4 and IPv6.        */
/*                                                                           */
/*    Input(s)            : i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          i4Priority   - Priority                          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS or CLI_FAILURE                       */
/*****************************************************************************/
INT4
VrrpCmnSetPriority (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                    INT4 i4Priority)
{
    INT4                i4Version = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return VRRP_NOT_OK;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV6)
        {
            CLI_SET_ERR (CLI_VRRP_IPV6_NO_SUPPORT);
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINDOWN);

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        if (nmhTestv2VrrpOperPriority (&u4ErrorCode, i4IfIndex, i4VrId,
                                       i4Priority) == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetVrrpOperPriority (i4IfIndex, i4VrId, i4Priority) ==
            SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     ACTIVE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINUP);
    }
    else
    {
        if (nmhTestv2Vrrpv3OperationsPriority (&u4ErrorCode,
                                               i4IfIndex, i4VrId,
                                               i4AddrType,
                                               (UINT4) i4Priority)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetVrrpv3OperationsPriority (i4IfIndex, i4VrId,
                                            i4AddrType,
                                            (UINT4) i4Priority) == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetPreemptMode                            */
/*                                                                           */
/*    Description         : This function enables/Disables the preemption    */
/*                          Mode based on version supported                  */
/*                                                                           */
/*    Input(s)            : i4IfIndex      - Interface Index                 */
/*                          i4VrId         - Virtual Router Identifier       */
/*                          i4AddrType     - Address Type                    */
/*                          i4PreemptMode  - Preempt Value                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetPreemptMode (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                       INT4 i4PreemptMode)
{
    INT4                i4Version = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return VRRP_NOT_OK;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV6)
        {
            CLI_SET_ERR (CLI_VRRP_IPV6_NO_SUPPORT);
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        if (nmhTestv2VrrpOperPreemptMode (&u4ErrorCode, i4IfIndex, i4VrId,
                                          i4PreemptMode) == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetVrrpOperPreemptMode (i4IfIndex, i4VrId, i4PreemptMode) ==
            SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     ACTIVE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

    }
    else
    {
        if (nmhTestv2Vrrpv3OperationsPreemptMode (&u4ErrorCode,
                                                  i4IfIndex, i4VrId,
                                                  i4AddrType,
                                                  i4PreemptMode)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetVrrpv3OperationsPreemptMode (i4IfIndex, i4VrId,
                                               i4AddrType,
                                               i4PreemptMode) == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetAdvertisementInterval                  */
/*                                                                           */
/*    Description         : This function Sets the Advertisement Interval    */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : i4IfIndex       - Interface Index                */
/*                          i4VrId          - Virtual Router Identifier      */
/*                          i4AddrType      - Address Type                   */
/*                          i4AdvInterval   - Advertisement Interval         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS or CLI_FAILURE                       */
/*****************************************************************************/
INT4
VrrpCmnSetAdvertisementInterval (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                                 INT4 i4AdvInterval)
{
    INT4                i4Version = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return VRRP_NOT_OK;
    }

    if (i4AdvInterval == 0)
    {
        i4AdvInterval = ADVERTISEMENTINTL;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV6)
        {
            CLI_SET_ERR (CLI_VRRP_IPV6_NO_SUPPORT);
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINDOWN);

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        if (nmhTestv2VrrpOperAdvertisementInterval (&u4ErrorCode, i4IfIndex,
                                                    i4VrId, i4AdvInterval)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetVrrpOperAdvertisementInterval (i4IfIndex, i4VrId,
                                                 i4AdvInterval) == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     ACTIVE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINUP);
    }
    else
    {
        /* For version 3 conversion from seconds to centiseconds should be
         * done */
        i4AdvInterval = (i4AdvInterval * 100);

        if (nmhTestv2Vrrpv3OperationsAdvInterval (&u4ErrorCode, i4IfIndex,
                                                  i4VrId, i4AddrType,
                                                  i4AdvInterval)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetVrrpv3OperationsAdvInterval (i4IfIndex, i4VrId,
                                               i4AddrType, i4AdvInterval)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*    Function Name       : VrrpCmnSetAdvertisementIntervalInMSec            */
/*                                                                           */
/*    Description         : This function Sets the Advertisement Interval    */
/*                          in milli-seconds                                 */
/*                                                                           */
/*    Input(s)            : i4IfIndex     - Interface Index                  */
/*                          i4VrId        - Virtual Router Identifier        */
/*                          i4AddrType    - Address Type                     */
/*                          i4AdvInterval - Advertisement Interval in Msec   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : CLI_SUCCESS or CLI_FAILURE                       */
/*****************************************************************************/
INT4
VrrpCmnSetAdvertisementIntervalInMSec (INT4 i4IfIndex, INT4 i4VrId,
                                       INT4 i4AddrType, INT4 i4AdvInterval)
{
    INT4                i4Version = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return VRRP_NOT_OK;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV6)
        {
            CLI_SET_ERR (CLI_VRRP_IPV6_NO_SUPPORT);
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINDOWN);

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        if (nmhTestv2FsVrrpOperAdvertisementIntervalInMsec (&u4ErrorCode,
                                                            i4IfIndex, i4VrId,
                                                            i4AdvInterval)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetFsVrrpOperAdvertisementIntervalInMsec (i4IfIndex, i4VrId,
                                                         i4AdvInterval)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     ACTIVE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINUP);
    }
    else
    {
        /* For version 3 conversion from milli-seconds to centiseconds should be
         * done */
        i4AdvInterval = (i4AdvInterval / 10);

        if (nmhTestv2Vrrpv3OperationsAdvInterval (&u4ErrorCode, i4IfIndex,
                                                  i4VrId, i4AddrType,
                                                  i4AdvInterval)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetVrrpv3OperationsAdvInterval (i4IfIndex, i4VrId,
                                               i4AddrType, i4AdvInterval)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetAcceptMode                             */
/*                                                                           */
/*    Description         : This function enables/Disables the accept        */
/*                          Mode based on version supported                  */
/*                                                                           */
/*    Input(s)            : i4IfIndex      - Interface Index                 */
/*                          i4VrId         - Virtual Router Identifier       */
/*                          i4AddrType     - Address Type                    */
/*                          i4AcceptMode   - Accept Mode                     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetAcceptMode (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                      INT4 i4AcceptMode)
{
    INT4                i4Version = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return VRRP_NOT_OK;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV6)
        {
            CLI_SET_ERR (CLI_VRRP_IPV6_NO_SUPPORT);
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINDOWN);

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        if (nmhTestv2FsVrrpAcceptMode (&u4ErrorCode,
                                       i4IfIndex, i4VrId,
                                       i4AcceptMode) == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetFsVrrpAcceptMode (i4IfIndex, i4VrId,
                                    i4AcceptMode) == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     ACTIVE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINUP);

    }
    else
    {
        if (nmhTestv2Vrrpv3OperationsAcceptMode (&u4ErrorCode,
                                                 i4IfIndex, i4VrId,
                                                 i4AddrType,
                                                 i4AcceptMode) == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetVrrpv3OperationsAcceptMode (i4IfIndex, i4VrId,
                                              i4AddrType, i4AcceptMode)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetTrackAndDecPriority                    */
/*                                                                           */
/*    Description         : This function sets tracking group and priority   */
/*                                                                           */
/*    Input(s)            : i4IfIndex         - Interface Index              */
/*                          i4VrId            - Virtual Router Identifier    */
/*                          i4AddrType        - Address Type                 */
/*                          u4GroupIndex      - Group Index                  */
/*                          i4Priority        - Decrement Priority           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetTrackAndDecPriority (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                               UINT4 u4GroupIndex, INT4 i4Priority)
{
    INT4                i4Version = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4PrevGroupIndex = 0;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return VRRP_NOT_OK;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV6)
        {
            CLI_SET_ERR (CLI_VRRP_IPV6_NO_SUPPORT);
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINDOWN);

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     NOT_IN_SERVICE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        if ((u4GroupIndex != 0) &&
            (nmhTestv2FsVrrpOperTrackGroupId (&u4ErrorCode, i4IfIndex, i4VrId,
                                              u4GroupIndex) == SNMP_FAILURE))
        {
            return VRRP_NOT_OK;
        }

        if (nmhSetFsVrrpOperTrackGroupId (i4IfIndex, i4VrId, u4GroupIndex) ==
            SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhGetFsVrrpOperTrackGroupId (i4IfIndex, i4VrId, &u4PrevGroupIndex)
            == SNMP_FAILURE)
        {
            /* Ignore Failure */
        }

        if ((i4Priority != 0) &&
            (nmhTestv2FsVrrpOperDecrementPriority (&u4ErrorCode, i4IfIndex,
                                                   i4VrId,
                                                   (UINT4) i4Priority)
             == SNMP_FAILURE))
        {
            if (nmhSetFsVrrpOperTrackGroupId
                (i4IfIndex, i4VrId, u4PrevGroupIndex) == SNMP_FAILURE)
            {
                /* Ignore Failure */
            }

            return VRRP_NOT_OK;
        }

        if (nmhSetFsVrrpOperDecrementPriority (i4IfIndex, i4VrId,
                                               (UINT4) i4Priority) ==
            SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                     ACTIVE) == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }

        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINUP);
    }
    else
    {
        if ((u4GroupIndex != 0) &&
            (nmhTestv2FsVrrpv3OperationsTrackGroupId (&u4ErrorCode,
                                                      i4IfIndex,
                                                      i4VrId, i4AddrType,
                                                      u4GroupIndex))
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if (nmhGetFsVrrpv3OperationsTrackGroupId (i4IfIndex, i4VrId,
                                                  i4AddrType, &u4PrevGroupIndex)
            == SNMP_FAILURE)
        {
            /* Ignore Failure */
        }

        if (nmhSetFsVrrpv3OperationsTrackGroupId (i4IfIndex, i4VrId,
                                                  i4AddrType, u4GroupIndex)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        if ((i4Priority != 0) &&
            (nmhTestv2FsVrrpv3OperationsDecrementPriority (&u4ErrorCode,
                                                           i4IfIndex,
                                                           i4VrId,
                                                           i4AddrType,
                                                           (UINT4)
                                                           i4Priority)
             == SNMP_FAILURE))
        {
            if (nmhSetFsVrrpv3OperationsTrackGroupId (i4IfIndex, i4VrId,
                                                      i4AddrType,
                                                      u4PrevGroupIndex) ==
                SNMP_FAILURE)
            {
                /* Ignore Failure */
            }

            return VRRP_NOT_OK;
        }

        if (nmhSetFsVrrpv3OperationsDecrementPriority (i4IfIndex, i4VrId,
                                                       i4AddrType,
                                                       (UINT4) i4Priority)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetTrackRowStatus                         */
/*                                                                           */
/*    Description         : This function sets track group row status based  */
/*                          on version.                                      */
/*                                                                           */
/*    Input(s)            : i4Version         - Version                      */
/*                          u4GroupIndex      - Group Index                  */
/*                          i4RowStatus       - Row Status                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetTrackRowStatus (INT4 i4Version, UINT4 u4GroupIndex, INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;

    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhTestv2FsVrrpOperTrackRowStatus (&u4ErrorCode,
                                               u4GroupIndex, i4RowStatus)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        i4RetVal = nmhSetFsVrrpOperTrackRowStatus (u4GroupIndex, i4RowStatus);
    }
    else
    {
        if (nmhTestv2FsVrrpv3OperationsTrackRowStatus (&u4ErrorCode,
                                                       u4GroupIndex,
                                                       i4RowStatus)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        i4RetVal = nmhSetFsVrrpv3OperationsTrackRowStatus (u4GroupIndex,
                                                           i4RowStatus);
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetTrackIfRowStatus                       */
/*                                                                           */
/*    Description         : This function sets track if row status based     */
/*                          on version.                                      */
/*                                                                           */
/*    Input(s)            : i4Version         - Version                      */
/*                          u4GroupIndex      - Group Index                  */
/*                          i4IfIndex         - Interface Index              */
/*                          i4RowStatus       - Row Status                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetTrackIfRowStatus (INT4 i4Version, UINT4 u4GroupIndex,
                            INT4 i4IfIndex, INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = 0;

    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhTestv2FsVrrpOperTrackGroupIfRowStatus
            (&u4ErrorCode, u4GroupIndex, i4IfIndex,
             i4RowStatus) == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        i4RetVal = nmhSetFsVrrpOperTrackGroupIfRowStatus (u4GroupIndex,
                                                          i4IfIndex,
                                                          i4RowStatus);
    }
    else
    {
        if (nmhTestv2FsVrrpv3OperationsTrackGroupIfRowStatus (&u4ErrorCode,
                                                              u4GroupIndex,
                                                              i4IfIndex,
                                                              i4RowStatus)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        i4RetVal = nmhSetFsVrrpv3OperationsTrackGroupIfRowStatus (u4GroupIndex,
                                                                  i4IfIndex,
                                                                  i4RowStatus);
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnCreateTrackGroupIf                        */
/*                                                                           */
/*    Description         : This function creates track group and interface  */
/*                                                                           */
/*    Input(s)            : u4GroupIndex      - Group Index                  */
/*                          i4IfIndex         - Interface Index              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnCreateTrackGroupIf (UINT4 u4GroupIndex, INT4 i4IfIndex)
{
    INT4                i4Version = 0;
    INT4                i4RowStatus = 0;
    BOOL1               bEntryFound = TRUE;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return VRRP_NOT_OK;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhGetFsVrrpOperTrackGroupIfRowStatus (u4GroupIndex, i4IfIndex,
                                                   &i4RowStatus) ==
            SNMP_SUCCESS)
        {
            return VRRP_OK;
        }

        if (nmhGetFsVrrpOperTrackRowStatus (u4GroupIndex, &i4RowStatus)
            == SNMP_FAILURE)
        {
            bEntryFound = FALSE;
        }
    }
    else
    {
        if (nmhGetFsVrrpv3OperationsTrackGroupIfRowStatus
            (u4GroupIndex, i4IfIndex, &i4RowStatus) == SNMP_SUCCESS)
        {
            return VRRP_OK;
        }

        if (nmhGetFsVrrpv3OperationsTrackRowStatus (u4GroupIndex, &i4RowStatus)
            == SNMP_FAILURE)
        {
            bEntryFound = FALSE;
        }
    }

    if ((bEntryFound == FALSE) &&
        (VrrpCmnSetTrackRowStatus (i4Version, u4GroupIndex, CREATE_AND_WAIT)
         == VRRP_NOT_OK))
    {
        return VRRP_NOT_OK;
    }

    if (VrrpCmnSetTrackIfRowStatus (i4Version, u4GroupIndex, i4IfIndex,
                                    CREATE_AND_GO) == VRRP_NOT_OK)
    {
        if ((bEntryFound == FALSE) &&
            (VrrpCmnSetTrackRowStatus (i4Version, u4GroupIndex, DESTROY)
             == VRRP_NOT_OK))
        {
            /* Ignore Failure */
        }

        return VRRP_NOT_OK;
    }

    if (VrrpCmnSetTrackRowStatus (i4Version, u4GroupIndex, ACTIVE) ==
        VRRP_NOT_OK)
    {
        if ((bEntryFound == FALSE) &&
            (VrrpCmnSetTrackRowStatus (i4Version, u4GroupIndex, DESTROY)
             == VRRP_NOT_OK))
        {
            /* Ignore Failure */
        }

        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnDeleteTrackGroupIf                        */
/*                                                                           */
/*    Description         : This function delete track group and interface   */
/*                                                                           */
/*    Input(s)            : u4SetGroupIndex   - Group Index                  */
/*                          i4SetIfIndex      - Interface Index              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnDeleteTrackGroupIf (UINT4 u4SetGroupIndex, INT4 i4SetIfIndex)
{
    INT4                i4Version = 0;
    UINT4               u4GroupIndex = u4SetGroupIndex;
    UINT4               u4NextGroupIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4RetVal = SNMP_SUCCESS;
    BOOL1               b1GroupEntryFound = FALSE;
    BOOL1               b1GetNextGroupReqd = FALSE;
    BOOL1               b1MoreGroupIfFound = FALSE;
    BOOL1               b1GroupIfEntryFound = FALSE;
    BOOL1               b1GroupIfEntryReqd = FALSE;
    BOOL1               b1DeleteSuccess = FALSE;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return VRRP_NOT_OK;
    }

    if ((i4SetIfIndex == 0) &&
        (VrrpCompGetTrackOperCount (u4SetGroupIndex) > 0))
    {
        CLI_SET_ERR (CLI_VRRP_DEL_TRACK_WITH_MORE_VR);
        return VRRP_NOT_OK;
    }

    while (i4RetVal == SNMP_SUCCESS)
    {
        if (i4Version == VRRP_VERSION_2)
        {
            i4RetVal
                = nmhGetNextIndexFsVrrpOperTrackGroupIfTable (u4GroupIndex,
                                                              &u4NextGroupIndex,
                                                              i4IfIndex,
                                                              &i4NextIfIndex);
        }
        else
        {
            i4RetVal
                = nmhGetNextIndexFsVrrpv3OperationsTrackGroupIfTable
                (u4GroupIndex, &u4NextGroupIndex, i4IfIndex, &i4NextIfIndex);
        }

        if (i4RetVal == SNMP_FAILURE)
        {
            break;
        }

        if (u4NextGroupIndex != u4SetGroupIndex)
        {
            break;
        }

        if (b1GetNextGroupReqd == TRUE)
        {
            b1MoreGroupIfFound = TRUE;
            break;
        }

        b1GroupEntryFound = TRUE;
        u4GroupIndex = u4NextGroupIndex;

        if (i4SetIfIndex != 0)
        {
            b1GroupIfEntryReqd = TRUE;

            if (i4NextIfIndex != i4SetIfIndex)
            {
                i4IfIndex = i4NextIfIndex;
                continue;
            }

            b1GroupIfEntryFound = TRUE;
        }

        if (VrrpCmnSetTrackIfRowStatus (i4Version, u4GroupIndex,
                                        i4NextIfIndex, DESTROY) == VRRP_NOT_OK)
        {
            i4RetVal = VRRP_NOT_OK;
            break;
        }

        b1DeleteSuccess = TRUE;

        if ((b1GroupIfEntryReqd == TRUE) && (b1GroupIfEntryFound == TRUE))
        {
            i4IfIndex = 0;
            b1GetNextGroupReqd = TRUE;
        }
    }

    if (b1GroupEntryFound == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_NO_TRACK_GROUP);

        return VRRP_NOT_OK;
    }

    if ((i4SetIfIndex != 0) && (b1GroupIfEntryFound == FALSE))
    {
        CLI_SET_ERR (CLI_VRRP_NO_TRACK_IF);

        return VRRP_NOT_OK;
    }

    if ((i4SetIfIndex == 0) || (b1MoreGroupIfFound == FALSE))
    {
        if (b1DeleteSuccess == FALSE)
        {
            return VRRP_NOT_OK;
        }

        if (VrrpCmnSetTrackRowStatus (i4Version, u4GroupIndex, DESTROY)
            == VRRP_NOT_OK)
        {
            return VRRP_NOT_OK;
        }
    }

    return VRRP_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpCmnSetTrackLinks                             */
/*                                                                           */
/*    Description         : This function sets track links                   */
/*                                                                           */
/*    Input(s)            : u4GroupIndex      - Group Index                  */
/*                          i4Links           - Links                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : VRRP_OK or VRRP_NOT_OK                           */
/*****************************************************************************/
INT4
VrrpCmnSetTrackLinks (UINT4 u4GroupIndex, INT4 i4Links)
{
    INT4                i4Version = 0;
    UINT4               u4ErrorCode = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return VRRP_NOT_OK;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhTestv2FsVrrpOperTrackedGroupTrackedLinks (&u4ErrorCode,
                                                         u4GroupIndex,
                                                         (UINT4) i4Links)
            == SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        i1RetVal = nmhSetFsVrrpOperTrackedGroupTrackedLinks (u4GroupIndex,
                                                             (UINT4) i4Links);
    }
    else
    {
        if (nmhTestv2FsVrrpv3OperationsTrackedGroupTrackedLinks (&u4ErrorCode,
                                                                 u4GroupIndex,
                                                                 (UINT4)
                                                                 i4Links) ==
            SNMP_FAILURE)
        {
            return VRRP_NOT_OK;
        }

        i1RetVal =
            nmhSetFsVrrpv3OperationsTrackedGroupTrackedLinks (u4GroupIndex,
                                                              (UINT4) i4Links);
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/******************************************************************************
* Function Name :  VrrpCmnDeleteVrId                                      
*                                                                         
* Description   :  This function deletes VRID and its associated IP
* Input (s)     :  i4Version     - Version
*                  i4DelIfIndex  - Interface Index
*                  i4DelVrId     - Virtual Router ID
*                  i4DelAddrType - Address Type
* Output (s)    :  None
* Returns       :  VRRP_OK or VRRP_NOT_OK
*******************************************************************************/
INT4
VrrpCmnDeleteVrId (INT4 i4Version, INT4 i4DelIfIndex, INT4 i4DelVrId,
                   INT4 i4DelAddrType)
{
    tSNMP_OCTET_STRING_TYPE NextIpAddr;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1NextIpAddr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1IpAddr[IPVX_IPV6_ADDR_LEN];
    UINT4               u4IpAddr = 0;
    UINT4               u4NextIpAddr = 0;
    INT4                i4IfIndex = i4DelIfIndex;
    INT4                i4VrId = i4DelVrId;
    INT4                i4AddrType = i4DelAddrType;
    INT4                i4NextIfIndex = 0;
    INT4                i4NextVrId = 0;
    INT4                i4NextAddrType = 0;
    BOOL1               bEntryFound = FALSE;
    INT1                i1RetVal = SNMP_SUCCESS;
    INT4                i4RetVal = VRRP_OK;

    MEMSET (&NextIpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1IpAddr, 0, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1NextIpAddr, 0, IPVX_IPV6_ADDR_LEN);

    IpAddr.pu1_OctetList = au1IpAddr;
    NextIpAddr.pu1_OctetList = au1NextIpAddr;

    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        IpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
        NextIpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        IpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
        NextIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINDOWN);
    }

    if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                 NOT_IN_SERVICE) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    while (i1RetVal == SNMP_SUCCESS)
    {
        if (i4Version == VRRP_VERSION_2)
        {
            i1RetVal = nmhGetNextIndexVrrpAssoIpAddrTable (i4IfIndex,
                                                           &i4NextIfIndex,
                                                           i4VrId,
                                                           &i4NextVrId,
                                                           u4IpAddr,
                                                           &u4NextIpAddr);

            i4NextAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
        }
        else
        {
            i1RetVal = nmhGetNextIndexVrrpv3AssociatedIpAddrTable (i4IfIndex,
                                                                   &i4NextIfIndex,
                                                                   i4VrId,
                                                                   &i4NextVrId,
                                                                   i4AddrType,
                                                                   &i4NextAddrType,
                                                                   &IpAddr,
                                                                   &NextIpAddr);
        }

        if ((i1RetVal == SNMP_FAILURE) || (i4NextIfIndex != i4DelIfIndex)
            || (i4NextVrId != i4DelVrId) || (i4NextAddrType != i4DelAddrType))
        {
            break;
        }

        bEntryFound = TRUE;

        if (i4Version == VRRP_VERSION_2)
        {
            if (VrrpCmnSetV2AssocIpRowStatus (i4NextIfIndex, i4NextVrId,
                                              u4NextIpAddr,
                                              DESTROY) == VRRP_NOT_OK)
            {
                return VRRP_NOT_OK;
            }

            u4IpAddr = u4NextIpAddr;
        }
        else
        {
            if (VrrpCmnSetV3AssocIpRowStatus (i4NextIfIndex, i4NextVrId,
                                              i4NextAddrType, &NextIpAddr,
                                              DESTROY) == VRRP_NOT_OK)
            {
                return VRRP_NOT_OK;
            }

            MEMCPY (&IpAddr, &NextIpAddr, sizeof (tSNMP_OCTET_STRING_TYPE));
        }

        i4IfIndex = i4NextIfIndex;
        i4VrId = i4NextVrId;
        i4AddrType = i4NextAddrType;
    }

    if (bEntryFound == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return VRRP_NOT_OK;
    }

    i4RetVal = VrrpCmnSetOperRowStatus (i4Version, i4DelIfIndex, i4DelVrId,
                                        i4DelAddrType, DESTROY);

    return i4RetVal;
}

/******************************************************************************
* Function Name :  VrrpCmnGetAllOperTable
*                                                                         
* Description   :  This function gets all information about Oper Table based
*                  on version.
* Input (s)     :  i4Version     - Version
*                  i4IfIndex     - Interface Index
*                  i4VrId        - Virtual Router ID
*                  i4AddrType    - Address Type
* Output (s)    :  None
* Returns       :  VRRP_OK or VRRP_NOT_OK
*******************************************************************************/
INT4
VrrpCmnGetAllOperTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                        INT4 i4AddrType, tVrrpOperTable * pOperTable)
{
    tSNMP_OCTET_STRING_TYPE PrimaryIpAddr;
    tSNMP_OCTET_STRING_TYPE MasterIpAddr;
    UINT1               au1PrimaryIpAddr[IPVX_IPV6_ADDR_LEN];
    UINT1               au1MasterIpAddr[IPVX_IPV6_ADDR_LEN];
    UINT4               u4IpAddr = 0;
    UINT4               u4MasterIpAddr = 0;
    UINT4               u4Val = 0;
    INT4                i4Val = 0;
    INT4                i4OperIndex = 0;

    MEMSET (&PrimaryIpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&MasterIpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PrimaryIpAddr, 0, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1MasterIpAddr, 0, IPVX_IPV6_ADDR_LEN);

    PrimaryIpAddr.pu1_OctetList = au1PrimaryIpAddr;
    MasterIpAddr.pu1_OctetList = au1MasterIpAddr;

    PrimaryIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
    MasterIpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;

    i4OperIndex = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

    if (i4OperIndex == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    pOperTable->b1IsIpvXOwner = OPERISOWNER (i4OperIndex);
    pOperTable->u1TrackedIfStatus = OPERTRACKIFSTATUS (i4OperIndex);

    if (i4Version == VRRP_VERSION_2)
    {
        nmhGetVrrpOperState (i4IfIndex, i4VrId, &(pOperTable->i4OperState));

        nmhGetVrrpOperPrimaryIpAddr (i4IfIndex, i4VrId, &u4IpAddr);

        VRRP_IPVX_COPY_TO_IPVX (pOperTable->PrimaryIpAddr, u4IpAddr);

        nmhGetVrrpOperVirtualMacAddr (i4IfIndex, i4VrId,
                                      &(pOperTable->maVirtualMacAddr));

        nmhGetVrrpOperMasterIpAddr (i4IfIndex, i4VrId, &u4MasterIpAddr);

        VRRP_IPVX_COPY_TO_IPVX (pOperTable->MasterIpAddr, u4MasterIpAddr);

        nmhGetFsVrrpOperAdvertisementIntervalInMsec (i4IfIndex, i4VrId,
                                                     &(pOperTable->
                                                       i4AdvtInterval));

        nmhGetVrrpOperPriority (i4IfIndex, i4VrId, &(pOperTable->i4Priority));

        i4Val = 0;
        nmhGetFsVrrpAcceptMode (i4IfIndex, i4VrId, &i4Val);
        pOperTable->u1AcceptMode = (UINT1) i4Val;

        nmhGetVrrpOperPreemptMode (i4IfIndex, i4VrId,
                                   &(pOperTable->i4PreemptMode));

        nmhGetVrrpOperAuthType (i4IfIndex, i4VrId, &(pOperTable->i4AuthType));

        nmhGetVrrpOperVirtualRouterUpTime (i4IfIndex, i4VrId,
                                           &(pOperTable->
                                             u4VirtualRouterUpTime));

        nmhGetFsVrrpOperTrackGroupId (i4IfIndex, i4VrId,
                                      &(pOperTable->u4TrackGroupId));

        nmhGetFsVrrpOperDecrementPriority (i4IfIndex, i4VrId, &u4Val);
        pOperTable->u1DecrementedPriority = (UINT1) u4Val;
        u4Val = 0;
    }
    else
    {
        nmhGetVrrpv3OperationsStatus (i4IfIndex, i4VrId, i4AddrType,
                                      &(pOperTable->i4OperState));

        nmhGetVrrpv3OperationsPrimaryIpAddr (i4IfIndex, i4VrId, i4AddrType,
                                             &PrimaryIpAddr);

        VRRP_IPVX_ADDR_INIT_IPVX (pOperTable->PrimaryIpAddr, (UINT1) i4AddrType,
                                  PrimaryIpAddr.pu1_OctetList);

        nmhGetVrrpv3OperationsVirtualMacAddr (i4IfIndex, i4VrId, i4AddrType,
                                              &(pOperTable->maVirtualMacAddr));

        nmhGetVrrpv3OperationsMasterIpAddr (i4IfIndex, i4VrId, i4AddrType,
                                            &MasterIpAddr);

        VRRP_IPVX_ADDR_INIT_IPVX (pOperTable->MasterIpAddr, (UINT1) i4AddrType,
                                  MasterIpAddr.pu1_OctetList);

        nmhGetVrrpv3OperationsAdvInterval (i4IfIndex, i4VrId, i4AddrType,
                                           &(pOperTable->i4AdvtInterval));

        nmhGetVrrpv3OperationsPriority (i4IfIndex, i4VrId, i4AddrType, &u4Val);
        pOperTable->i4Priority = (INT4) u4Val;
        u4Val = 0;

        nmhGetVrrpv3OperationsPreemptMode (i4IfIndex, i4VrId, i4AddrType,
                                           &(pOperTable->i4PreemptMode));
        if (i4Version == VRRP_VERSION_2_3)
        {
            nmhGetVrrpOperAuthType (i4IfIndex, i4VrId,
                                    &(pOperTable->i4AuthType));
            nmhGetFsVrrpOperAdvertisementIntervalInMsec (i4IfIndex, i4VrId,
                                                         &
                                                         (pOperTable->
                                                          i4AdvtInterval));
        }

        nmhGetVrrpv3OperationsAcceptMode (i4IfIndex, i4VrId, i4AddrType,
                                          &i4Val);
        pOperTable->u1AcceptMode = (UINT1) i4Val;
        i4Val = 0;

        nmhGetVrrpv3OperationsUpTime (i4IfIndex, i4VrId, i4AddrType,
                                      &(pOperTable->u4VirtualRouterUpTime));

        nmhGetFsVrrpv3OperationsTrackGroupId (i4IfIndex, i4VrId, i4AddrType,
                                              &u4Val);
        pOperTable->u4TrackGroupId = u4Val;
        u4Val = 0;

        nmhGetFsVrrpv3OperationsDecrementPriority (i4IfIndex, i4VrId,
                                                   i4AddrType, &u4Val);
        pOperTable->u1DecrementedPriority = (UINT1) u4Val;
        u4Val = 0;

        nmhGetFsVrrpv3StatisticsMasterAdverInterval (i4IfIndex, i4VrId,
                                                     i4AddrType,
                                                     &(pOperTable->
                                                       i4RcvdMasterAdvtInterval));

        nmhGetFsVrrpv3StatisticsSkewTime (i4IfIndex, i4VrId, i4AddrType,
                                          &(pOperTable->i4SkewTime));

        nmhGetFsVrrpv3StatisticsMasterDownInterval (i4IfIndex, i4VrId,
                                                    i4AddrType,
                                                    &(pOperTable->
                                                      i4MasterDownInterval));
    }

    return VRRP_OK;
}

/******************************************************************************
* Function Name :  VrrpCmnGetAllStatsTable
*                                                                         
* Description   :  This function gets all information about Stats Table based
*                  on version.
* Input (s)     :  i4Version     - Version
*                  i4IfIndex     - Interface Index
*                  i4VrId        - Virtual Router ID
*                  i4AddrType    - Address Type
* Output (s)    :  None
* Returns       :  VRRP_OK or VRRP_NOT_OK
*******************************************************************************/
INT4
VrrpCmnGetAllStatsTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                         INT4 i4AddrType, tVrrpStatsTable * pStats)
{
    tSNMP_COUNTER64_TYPE Counter;
    UINT4               u4Counter = 0;
    INT4                i4Val = 0;

    Counter.msn = 0;
    Counter.lsn = 0;

    if (i4Version == VRRP_VERSION_2)
    {
        VrrpStatsVrrpSemFail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4VrrpSemFail = u4Counter;
        u4Counter = 0;

        VrrpStatsVrrpSendNDNeighborAdvtFail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4VrrpSendNDNeighborAdvtFail = u4Counter;
        u4Counter = 0;

        VrrpStatsVrrpSendPacketToIpv4Fail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4VrrpSendPacketToIpv4Fail = u4Counter;
        u4Counter = 0;

        VrrpStatsVrrpGetIpv4PortFromOperIndexFail (i4IfIndex, i4VrId,
                                                   &u4Counter);

        pStats->u4VrrpGetIpv4PortFromOperIndexFail = u4Counter;
        u4Counter = 0;

        VrrpStatsVrrpSendGratArpFail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4VrrpSendGratArpFail = u4Counter;
        u4Counter = 0;

        VrrpStatsEthRegisterMacAddrFail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4EthRegisterMacAddrFail = u4Counter;
        u4Counter = 0;

        VrrpStatsVrrpNetIpv4DeleteIntfFail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4VrrpNetIpv4DeleteIntfFail = u4Counter;
        u4Counter = 0;

        VrrpStatsVrrpIpifJoinMcastGroupFail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4VrrpIpifJoinMcastGroupFail = u4Counter;
        u4Counter = 0;

        VrrpStatsVrrpIpifLeaveMcastGroupFail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4VrrpIpifLeaveMcastGroupFail = u4Counter;
        u4Counter = 0;

        VrrpStatsVrrpSocketFail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4VrrpSocketFail = u4Counter;
        u4Counter = 0;

        VrrpStatsPktProcessValidateAdverPacketFail (i4IfIndex, i4VrId,
                                                    &u4Counter);

        pStats->u4PktProcessValidateAdverPacketFail = u4Counter;
        u4Counter = 0;

        VrrpStatsPktProcessValidateAdverV2PacketFail (i4IfIndex, i4VrId,
                                                      &u4Counter);

        pStats->u4PktProcessValidateAdverV2PacketFail = u4Counter;
        u4Counter = 0;

        VrrpStatsPktProcessValidateAdverV3PacketFail (i4IfIndex, i4VrId,
                                                      &u4Counter);

        pStats->u4PktProcessValidateAdverV3PacketFail = u4Counter;
        u4Counter = 0;

        VrrpStatsPktProcessTransmitV2PacketFail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4PktProcessTransmitV2PacketFail = u4Counter;
        u4Counter = 0;

        VrrpStatsPktProcessTransmitV3PacketFail (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4PktProcessTransmitV3PacketFail = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsBecomeMaster (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4BecomeMaster = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsAdvertiseRcvd (i4IfIndex, i4VrId, &u4Counter);

        pStats->u8RcvdAdvertisements.u4Lo = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsAdvertiseIntervalErrors (i4IfIndex, i4VrId, &u4Counter);

        pStats->u8AdvtIntervalErrors.u4Lo = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsAuthFailures (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4AuthFailures = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsIpTtlErrors (i4IfIndex, i4VrId, &u4Counter);

        pStats->u8IpTtlErrors.u4Lo = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsPriorityZeroPktsRcvd (i4IfIndex, i4VrId, &u4Counter);

        pStats->u8RcvdPriZeroPackets.u4Lo = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsPriorityZeroPktsSent (i4IfIndex, i4VrId, &u4Counter);

        pStats->u8SentPriZeroPackets.u4Lo = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsInvalidTypePktsRcvd (i4IfIndex, i4VrId, &u4Counter);

        pStats->u8RcvdInvalidTypePackets.u4Lo = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsAddressListErrors (i4IfIndex, i4VrId, &u4Counter);

        pStats->u8AddressListErrors.u4Lo = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsInvalidAuthType (i4IfIndex, i4VrId, &u4Counter);

        pStats->u8RcvdInvalidTypePackets.u4Lo = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsAuthTypeMismatch (i4IfIndex, i4VrId, &u4Counter);

        pStats->u4AuthTypeMismatch = u4Counter;
        u4Counter = 0;

        nmhGetVrrpStatsPacketLengthErrors (i4IfIndex, i4VrId, &u4Counter);

        pStats->u8PacketLengthErrors.u4Lo = u4Counter;
        u4Counter = 0;
    }
    else
    {
        Vrrpv3StatsVrrpSemFail (i4IfIndex, i4VrId, i4AddrType, &u4Counter);

        pStats->u4VrrpSemFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsVrrpSendNDNeighborAdvtFail (i4IfIndex, i4VrId, i4AddrType,
                                               &u4Counter);

        pStats->u4VrrpSendNDNeighborAdvtFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsVrrpSendPacketToIpv4Fail (i4IfIndex, i4VrId, i4AddrType,
                                             &u4Counter);

        pStats->u4VrrpSendPacketToIpv4Fail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsVrrpSendPacketToIpv6Fail (i4IfIndex, i4VrId, i4AddrType,
                                             &u4Counter);

        pStats->u4VrrpSendPacketToIpv6Fail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsVrrpGetIpv4PortFromOperIndexFail (i4IfIndex, i4VrId,
                                                     i4AddrType, &u4Counter);

        pStats->u4VrrpGetIpv4PortFromOperIndexFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsVrrpGetIpv6PortFromOperIndexFail (i4IfIndex, i4VrId,
                                                     i4AddrType, &u4Counter);

        pStats->u4VrrpGetIpv6PortFromOperIndexFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsVrrpSendGratArpFail (i4IfIndex, i4VrId, i4AddrType,
                                        &u4Counter);

        pStats->u4VrrpSendGratArpFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsEthRegisterMacAddrFail (i4IfIndex, i4VrId, i4AddrType,
                                           &u4Counter);

        pStats->u4EthRegisterMacAddrFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsVrrpNetIpv4DeleteIntfFail (i4IfIndex, i4VrId, i4AddrType,
                                              &u4Counter);

        pStats->u4VrrpNetIpv4DeleteIntfFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsVrrpNetIpv6DeleteIntfFail (i4IfIndex, i4VrId, i4AddrType,
                                              &u4Counter);

        pStats->u4VrrpNetIpv6DeleteIntfFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsVrrpIpifJoinMcastGroupFail (i4IfIndex, i4VrId, i4AddrType,
                                               &u4Counter);

        pStats->u4VrrpIpifJoinMcastGroupFail = u4Counter;
        u4Counter = 0;
        Vrrpv3StatsVrrpIpifLeaveMcastGroupFail (i4IfIndex, i4VrId, i4AddrType,
                                                &u4Counter);

        pStats->u4VrrpIpifLeaveMcastGroupFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsVrrpSocketFail (i4IfIndex, i4VrId, i4AddrType, &u4Counter);

        pStats->u4VrrpSocketFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsPktProcessValidateAdverPacketFail (i4IfIndex, i4VrId,
                                                      i4AddrType, &u4Counter);

        pStats->u4PktProcessValidateAdverPacketFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsPktProcessValidateAdverV2PacketFail (i4IfIndex, i4VrId,
                                                        i4AddrType, &u4Counter);

        pStats->u4PktProcessValidateAdverV2PacketFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsPktProcessValidateAdverV3PacketFail (i4IfIndex, i4VrId,
                                                        i4AddrType, &u4Counter);

        pStats->u4PktProcessValidateAdverV3PacketFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsPktProcessTransmitV2PacketFail (i4IfIndex, i4VrId,
                                                   i4AddrType, &u4Counter);

        pStats->u4PktProcessTransmitV2PacketFail = u4Counter;
        u4Counter = 0;

        Vrrpv3StatsPktProcessTransmitV3PacketFail (i4IfIndex, i4VrId,
                                                   i4AddrType, &u4Counter);

        pStats->u4PktProcessTransmitV3PacketFail = u4Counter;
        u4Counter = 0;
        nmhGetVrrpv3StatisticsMasterTransitions (i4IfIndex, i4VrId, i4AddrType,
                                                 &u4Counter);

        pStats->u4BecomeMaster = u4Counter;
        u4Counter = 0;

        nmhGetVrrpv3StatisticsRcvdAdvertisements (i4IfIndex, i4VrId, i4AddrType,
                                                  &Counter);

        pStats->u8RcvdInvalidTypePackets.u4Hi = Counter.msn;
        pStats->u8RcvdAdvertisements.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetVrrpv3StatisticsAdvIntervalErrors (i4IfIndex, i4VrId, i4AddrType,
                                                 &Counter);

        pStats->u8AdvtIntervalErrors.u4Hi = Counter.msn;
        pStats->u8AdvtIntervalErrors.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetVrrpv3StatisticsIpTtlErrors (i4IfIndex, i4VrId, i4AddrType,
                                           &Counter);

        pStats->u8IpTtlErrors.u4Hi = Counter.msn;
        pStats->u8IpTtlErrors.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetVrrpv3StatisticsRcvdPriZeroPackets (i4IfIndex, i4VrId,
                                                  i4AddrType, &Counter);

        pStats->u8RcvdPriZeroPackets.u4Hi = Counter.msn;
        pStats->u8RcvdPriZeroPackets.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetVrrpv3StatisticsSentPriZeroPackets (i4IfIndex, i4VrId,
                                                  i4AddrType, &Counter);

        pStats->u8SentPriZeroPackets.u4Hi = Counter.msn;
        pStats->u8SentPriZeroPackets.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetVrrpv3StatisticsRcvdInvalidTypePackets (i4IfIndex, i4VrId,
                                                      i4AddrType, &Counter);

        pStats->u8RcvdInvalidTypePackets.u4Hi = Counter.msn;
        pStats->u8RcvdInvalidTypePackets.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetVrrpv3StatisticsAddressListErrors (i4IfIndex, i4VrId, i4AddrType,
                                                 &Counter);

        pStats->u8AddressListErrors.u4Hi = Counter.msn;
        pStats->u8AddressListErrors.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetVrrpv3StatisticsPacketLengthErrors (i4IfIndex, i4VrId, i4AddrType,
                                                  &Counter);

        pStats->u8PacketLengthErrors.u4Hi = Counter.msn;
        pStats->u8PacketLengthErrors.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetFsVrrpv3StatisticsTxedAdvertisements (i4IfIndex, i4VrId,
                                                    i4AddrType, &Counter);

        pStats->u8TxedAdvertisments.u4Hi = Counter.msn;
        pStats->u8TxedAdvertisments.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetFsVrrpv3StatisticsTxedV2Advertisements (i4IfIndex, i4VrId,
                                                      i4AddrType, &Counter);

        pStats->u8TxedV2Advertisments.u4Hi = Counter.msn;
        pStats->u8TxedV2Advertisments.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetFsVrrpv3StatisticsV2AdvertiseIgnored (i4IfIndex, i4VrId,
                                                    i4AddrType, &Counter);

        pStats->u8V2AdvtIgnored.u4Hi = Counter.msn;
        pStats->u8V2AdvtIgnored.u4Lo = Counter.lsn;
        Counter.msn = 0;
        Counter.lsn = 0;

        nmhGetVrrpv3StatisticsNewMasterReason (i4IfIndex, i4VrId, i4AddrType,
                                               &i4Val);

        pStats->u1NewMasterReason = (UINT1) i4Val;
        i4Val = 0;

        nmhGetVrrpv3StatisticsProtoErrReason (i4IfIndex, i4VrId, i4AddrType,
                                              &i4Val);

        pStats->u1ProtoErrReason = (UINT1) i4Val;
        i4Val = 0;

        nmhGetVrrpv3StatisticsRowDiscontinuityTime (i4IfIndex, i4VrId,
                                                    i4AddrType, &u4Counter);

        nmhGetVrrpv3StatisticsRefreshRate (i4IfIndex, i4VrId, i4AddrType,
                                           &u4Counter);
    }

    return VRRP_OK;
}
