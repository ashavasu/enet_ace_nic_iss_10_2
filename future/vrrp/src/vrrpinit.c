/*  $Id: vrrpinit.c,v 1.30 2017/12/16 11:57:11 siva Exp $ */
/********************************************************************
 *                                                                  *
 * $RCSfile: vrrpinit.c,v $
 *                                                                  *
 * $Date: 2017/12/16 11:57:11 $
 *                                                                  *
 * $Revision: 1.30 $
 *                                                                  *
 *******************************************************************/

/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrpinit.c                                   |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Core Protocol.                          |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : Initialisation Module of VRRP Core Protocol.  |
 * | All the data structures are allocated here and the  task waits for       |
 * | different events in this module. This is the main function of Vrrp       |
 * | Protocol.                                                                |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |  Padmanaban. B       |   Base Line Version             |
 * |                 |                      |                                 |
 * |  1.1.0.0        |  Padmanaban. B       |   Changes for IMS 1600          |
 * |                 |                      |                                 |
 * |  1.2.0.0        |  Padmanaban. B       |   Enhancements to IP Address    |
 * |                 |                      |   Change Processing             |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#define    INIT_MODULE
#include "vrrpinc.h"
#ifdef LNXIP4_WANTED
#include "lnxip.h"
#endif

#ifdef L3_SWITCHING_WANTED
#include "ipnp.h"
#endif /* L3_SWITCHING_WANTED */

/*******************************************************************************
 * Function Name    : VrrpUnLock
 * Description      : This Function Releases the VRRP Lock.
 * Global Variables :
 * Inputs           : None
 * Output           : None
 * Returns          : SNMP_SUCCESS
 *                    SNMP_FAILURE
 ******************************************************************************/

INT4
VrrpUnLock ()
{
    if ((OsixSemGive (gVrrpSemId) != OSIX_SUCCESS))
    {
        VRRP_DBG (VRRP_TRC_INIT, "INIT: VrrpSem not released\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*******************************************************************************
 * Function Name    : VrrpLock
 * Description      : This Function creates the VRRP Lock.
 * Global Variables :
 * Inputs           : None
 * Output           : None
 * Returns          : SNMP_SUCCESS
 *                    SNMP_FAILURE
 ******************************************************************************/

INT4
VrrpLock ()
{
    if ((OsixSemTake (gVrrpSemId) != OSIX_SUCCESS))
    {
        VRRP_DBG (VRRP_TRC_INIT, "INIT: VrrpSem not acquired\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*******************************************************************************
 * Function Name    : VrrpCreateTask
 * Description      : This Function creates the VRRP task.
 * Global Variables : gVrrpTaskId
 * Inputs           : None
 * Output           : None
 * Returns          : OSIX_SUCCESS
 *                    OSIX_FAILURE
 ******************************************************************************/
UINT4
VrrpCreateTask ()
{
    /* Create the VRRP task */
    if (OsixTskCrt (VRRP_TASK_NAME, VRRP_TASK_PRIORITY,
                    VRRP_TASK_STACK_SIZE, (OsixTskEntry) VrrpMain,
                    0, &gVrrpTaskId) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    return OSIX_FAILURE;
}

/*******************************************************************************
 * Function Name    : VrrpMain
 * Description      : This Function is the entry point into VRRP Protocol.
 *                    It initializes VRRP Data Structures and waits for
 *                    events intended for this task
 * Global Variables : VRRP OPER TABLE
 *                    VRRP IP ADDRESS TABLE
 *                    VRRP GLOBAL TABLE
 * Inputs           : None
 * Output           : None
 * Returns          : None
 ******************************************************************************/
VOID
VrrpMain (INT1 *i1Param)
{
    tVrrpTxIpv4BufSize *pTxIpv4BufSize = NULL;
    tVrrpTxIpv6BufSize *pTxIpv6BufSize = NULL;
    UINT4               u4Events = 0;
    UINT4               u4InitiaCount = 1;
    UINT4               u4MaxAssocIpPerVr = 0;
    UINT4               u4MaxTrackIfPerGroup = 0;
    UINT4               u4MaxOperEntry = 0;
    UINT4               u4MaxGroupEntry = 0;

    UNUSED_PARAM (i1Param);
    UNUSED_PARAM (pTxIpv4BufSize);
    UNUSED_PARAM (pTxIpv6BufSize);

    if (OsixTskIdSelf (&gVrrpTaskId) == OSIX_FAILURE)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "INIT: VRRP Task Id get failed\n");
        VRRP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    u4MaxAssocIpPerVr = IssSzGetSizingMacroValue ("MAX_ASSO_ADDR_ENTRY");

    if (u4MaxAssocIpPerVr == 0)
    {
        u4MaxAssocIpPerVr = MAX_ASSO_ADDR_ENTRY;
    }

    u4MaxTrackIfPerGroup = IssSzGetSizingMacroValue ("MAX_TRACK_IF_ENTRY");

    if (u4MaxTrackIfPerGroup == 0)
    {
        u4MaxTrackIfPerGroup = MAX_TRACK_IF_ENTRY;
    }

    u4MaxOperEntry = FsVRRPSizingParams[MAX_VRRP_OPER_ENTRY_SIZING_ID].
        u4PreAllocatedUnits;

    gu4VrrpMaxOperEntries = u4MaxOperEntry;

    /* One contiguous block of memory with size (tVrrpOperTable) *
     * MAX Oper Entries required is created for VrrpOperTable and 
     * accessed as array. */
    FsVRRPSizingParams[MAX_VRRP_OPER_ENTRY_SIZING_ID].u4StructSize
        = (sizeof (tVrrpOperTable) * u4MaxOperEntry);
    FsVRRPSizingParams[MAX_VRRP_OPER_ENTRY_SIZING_ID].u4PreAllocatedUnits
        = VRRP_ONE;

    FsVRRPSizingParams[MAX_VRRP_ASSO_IP_ENTRY_SIZING_ID].u4PreAllocatedUnits
        = (u4MaxOperEntry * u4MaxAssocIpPerVr);

    /* Struct Size is calculated based on Associated Ip Addresses 
     * supported per Vrrp Oper Entry */
    FsVRRPSizingParams[MAX_VRRP_TX_IPV4_BUF_SIZING_ID].u4StructSize
        = ((u4MaxAssocIpPerVr * IPVX_IPV4_ADDR_LEN) +
           VRRP_HEADER_LEN + TEXT_AUTHKEY_SIZE);
    FsVRRPSizingParams[MAX_VRRP_TX_IPV4_BUF_SIZING_ID].u4PreAllocatedUnits
        = u4MaxOperEntry;

    FsVRRPSizingParams[MAX_VRRP_TX_IPV6_BUF_SIZING_ID].u4StructSize
        = ((u4MaxAssocIpPerVr * IPVX_IPV6_ADDR_LEN) + VRRP_HEADER_LEN);
    FsVRRPSizingParams[MAX_VRRP_TX_IPV6_BUF_SIZING_ID].u4PreAllocatedUnits
        = u4MaxOperEntry;

    FsVRRPSizingParams[MAX_VRRP_TX_RX_IPV4_PKT_SIZING_ID].u4StructSize
        = (IP_HDR_LEN + (u4MaxAssocIpPerVr * IPVX_IPV4_ADDR_LEN) +
           VRRP_HEADER_LEN + TEXT_AUTHKEY_SIZE);

    /* For IPv6, VRRP do not construct IPv6 Header or expect IPv6 header
     * when packet is received. */
    FsVRRPSizingParams[MAX_VRRP_TX_RX_IPV6_PKT_SIZING_ID].u4StructSize
        = ((u4MaxAssocIpPerVr * IPVX_IPV6_ADDR_LEN) + VRRP_HEADER_LEN);

    u4MaxGroupEntry
        =
        FsVRRPSizingParams[MAX_VRRP_TRACK_GROUP_SIZING_ID].u4PreAllocatedUnits;

    FsVRRPSizingParams[MAX_VRRP_TRACK_IF_GROUP_SIZING_ID].u4PreAllocatedUnits
        = (u4MaxGroupEntry * u4MaxTrackIfPerGroup);

    if (VrrpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "INIT: Memory Allocation of OperTable Failed\n");
        VRRP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if ((gpVrrpOperTable =
         (tVrrpOperTable *) (MemAllocMemBlk (VRRP_OPER_MEMPOOL_ID))) == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for OPER Table\r\n");

        /* return failure */
        VRRP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    MEMSET (gpVrrpOperTable, 0, ((sizeof (tVrrpOperTable) * u4MaxOperEntry)));

    if (OsixCreateSem ((CONST UINT1 *) VRRP_SEM_NAME, u4InitiaCount,
                       OSIX_DEFAULT_SEM_MODE, &gVrrpSemId) != OSIX_SUCCESS)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "INIT: Creation of Semaphore Failure\n");
        VRRP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (TmrIfCreateTimer () == VRRP_NOT_OK)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "INIT: Initialization of Timer Failure\n");
        VRRP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    else
    {
        VRRP_DBG (VRRP_TRC_INIT, "INIT: TIMER IF Created\n");
    }

    gi4VrrpSysLogId = SYS_LOG_REGISTER ((UINT1 *) VRRP_TASK_NAME,
                                        SYSLOG_CRITICAL_LEVEL);

    VrrpConfig ();                /* Configuring the VRRP for default values */

    VRRP_DBG (VRRP_TRC_INIT, "INIT: Configuration of Oper Table Successful\n");

    if (OsixQueCrt (IP_VRRP_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    MAX_VRRP_QUEUE_DEPTH, &gIpVrrpQId) != OSIX_SUCCESS)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "INIT: Queue Creation Failed\n");
        VRRP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (VrrpRedInitGlobalInfo () == OSIX_FAILURE)
    {
        VRRP_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    VrrpRedDynDataDescInit ();

    VrrpRedDescrTblInit ();

    VRRP_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    RegisterSTDVRR ();
    RegisterFSVRRP ();
    RegisterVRRP3 ();
    RegisterFSVR3 ();
#endif
    while (VRRP_TRUE)
    {
        OsixEvtRecv (gVrrpTaskId, (VRRP_TMR_EXP_EVENT | VRRP_PKT_RXED_EVENT |
                                   VRRP_IPV6_PKT_RXED_EVENT |
                                   VRRP_SHUTDOWN_EVENT |
                                   VRRP_IP_ADDR_CHG_EVENT |
                                   VRRP_LINK_STATE_CHG_EVENT |
                                   VRRP_RM_PKT_EVENT |
                                   VRRP_STARTUP_EVENT),
                     (OSIX_WAIT | OSIX_EV_ANY), &u4Events);

        VRRP_LOCK ();
        if (u4Events & VRRP_TMR_EXP_EVENT)
        {
            VRRP_DBG (VRRP_TRC_EVENTS, "INIT: Timer expiry Event Received\n");

            TmrIfGetExpiredTimers ();
        }

        if (u4Events & VRRP_PKT_RXED_EVENT)
        {
            VRRP_DBG (VRRP_TRC_EVENTS,
                      "INIT: Received vrrp packet received Event\n");

            VrrpReceivePacket ();

            VRRP_DBG (VRRP_TRC_INIT, "INIT: Adver Packet Processed \n");
        }

        if (u4Events & VRRP_IPV6_PKT_RXED_EVENT)
        {
            VRRP_DBG (VRRP_TRC_EVENTS,
                      "INIT: Received VRRP IPv6 Packet received Event\n");

            VrrpReceiveIpv6Packet ();

            VRRP_DBG (VRRP_TRC_INIT,
                      "INIT: VRRP IPv6 Advertisement Packet Processed\n");
        }

        if ((u4Events & VRRP_LINK_STATE_CHG_EVENT) ||
            (u4Events & VRRP_IP_ADDR_CHG_EVENT))
        {
            VRRP_DBG (VRRP_TRC_EVENTS, "INIT: Received PktrcvdEvt Event\n");

            VrrpDequePacket ();

            VRRP_DBG (VRRP_TRC_INIT, "INIT: Link status change Processed \n");

        }

        if (u4Events & VRRP_SHUTDOWN_EVENT)
        {
            VRRP_DBG (VRRP_TRC_EVENTS, "INIT: Shutdown Event Received\n");

            VrrpShutDown ();

            VRRP_DBG (VRRP_TRC_INIT, "INIT: Shutdown Event Processed\n");
        }

        if (u4Events & VRRP_STARTUP_EVENT)    /* Added for enable */
        {
            VRRP_DBG (VRRP_TRC_EVENTS, "INIT: Startup Event Received\n");

            VrrpEnable ();
            VRRP_DBG (VRRP_TRC_INIT, "INIT: Startup  Event Processed\n");
        }

        if (u4Events & VRRP_RM_PKT_EVENT)
        {
            VRRP_DBG (VRRP_TRC_EVENTS, "INIT: RM Event Received\n");

            VrrpRedHandleRmEvents ();
            VRRP_DBG (VRRP_TRC_INIT, "INIT: RM  Event Processed\n");
        }
        VRRP_UNLOCK ();
    }
}

/* Added VrrpEnable() */
/*******************************************************************************
 * Function Name    : VrrpEnable
 * Description      : Processes the Startup Event; This is called only for
 *                    Re-Enabling the Disabled VRRP Instances.
 * Global Variables :
 *
 *
 * Inputs           : None
 * Output           : None
 * Returns          : None
 ******************************************************************************/
VOID
VrrpEnable ()
{
    INT4                i4count;

    for (i4count = 0; i4count < (INT4) gu4VrrpMaxOperEntries; i4count++)
    {
        if (OPERROWSTATUS (i4count) == ACTIVE)
        {
            /* Re-joining the multicast group */
            VrrpIpifJoinMcastGroup (i4count);

            VrrpDbActivateOperEntry (i4count);

            VRRP_DBG (VRRP_TRC_INIT,
                      " INIT : Re Enabling the Disabled VRRP Instances");

            VrrpRedDbNodeInit (&OPERDBNODE (i4count));
        }                        /* For Loop */

        VRRP_DBG1 (VRRP_TRC_INIT,
                   "INIT : Startup Event processed by SEM for OperIndex  %d\n",
                   i4count);
    }
}

/*******************************************************************************
 * Function Name    : VrrpLinkDown
 * Description      : Processes the Linkdown Event.
 * Inputs           : u4PortNo     - IP Port Number
 *                    i4AddrType   - Address Type IPv4 or IPv6
 * Output           : None
 * Returns          : None
 ******************************************************************************/
VOID
VrrpLinkDown (UINT4 u4PortNo, INT4 i4AddrType)
{
    UINT4               u4IfIndex = u4PortNo;
    INT4                i4count = 0;
    CHR1                ac1AddrType[5];
    MEMSET (ac1AddrType, 0, 5);

    VRRP_DBG2 (VRRP_TRC_EVENTS,
               "Link Down Event to be processed for If Index %d "
               "and Address Type %d\n\r", u4IfIndex, i4AddrType);

    VrrpHandleLinkDownForTrackGroup (u4IfIndex);

    VRRP_ADDRTYPE_TO_STRING (ac1AddrType, i4AddrType);

    for (i4count = 0; i4count < (INT4) gu4VrrpMaxOperEntries; i4count++)
    {
        if ((OPERIFINDEX (i4count) == (INT4) u4IfIndex) &&
            (OPERADDRTYPE (i4count) == (UINT1) i4AddrType) &&
            (OPERROWSTATUS (i4count) == ACTIVE))
        {
            VRRP_DBG4 (VRRP_TRC_EVENTS,
                       "Sem for VR %d %d to be shutdown due to link down "
                       "event on interface %d and address type %s\n\r",
                       OPERVRID (i4count), OPERADDRTYPE (i4count),
                       u4IfIndex, ac1AddrType);
            VrrpDbDeactivateOperEntry (i4count);
        }
    }                            /* End of For Loop */

    return;
}                                /* Check for Link Down Event */

/*******************************************************************************
 * Function Name    : VrrpLinkUp
 * Description      : Processes the LinkUp Event.
 * Inputs           : u4PortNo     - IP Port Number
 *                    i4AddrType   - Address Type IPv4 or IPv6
 * Output           : None
 * Returns          : None
 ******************************************************************************/
VOID
VrrpLinkUp (UINT4 u4PortNo, INT4 i4AddrType)
{
    UINT4               u4IfIndex = u4PortNo;
    INT4                i4count = 0;
    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        VRRP_DBG2 (VRRP_TRC_EVENTS,
                   "Link Up Event to be processed for Interface Index %d "
                   "and Address Type %s\n\r", u4IfIndex, "IPv4");
    }
    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV6)
    {
        VRRP_DBG2 (VRRP_TRC_EVENTS,
                   "Link Up Event to be processed for Interface Index %d "
                   "and Address Type %s\n\r", u4IfIndex, "IPv6");
    }

    VrrpHandleLinkUpForTrackGroup (u4IfIndex);

    for (i4count = 0; i4count < (INT4) gu4VrrpMaxOperEntries; i4count++)
    {
        if ((OPERIFINDEX (i4count) == (INT4) u4IfIndex) &&
            (OPERADDRTYPE (i4count) == (UINT1) i4AddrType))
        {

            VRRP_DBG4 (VRRP_TRC_EVENTS,
                       "Sem for VR %d %d to be activated due to link up "
                       "event on interface %d and address type %d\n\r",
                       OPERVRID (i4count), OPERADDRTYPE (i4count),
                       u4IfIndex, i4AddrType);

            VrrpDbActivateOperEntry (i4count);
        }
    }                            /* End of For Loop */

    return;
}

/*******************************************************************************
 * Function Name    : VrrpIpChange
 * Description      : Processes the Ip Address Change Event.
 * Inputs           : u4PortNo     - IP Port Number
 *                    i4AddrType   - Address Type IPv4 or IPv6
 *                    u4Type       - Ipv6 Addr Type LLOCAL or Global
 * Output           : None
 * Returns          : None
 ******************************************************************************/
VOID
VrrpIpChange (UINT4 u4PortNo, INT4 i4AddrType, tVrrpIPvXAddr IpAddr,
              UINT4 u4Type)
{
    UINT4               u4IfIndex = u4PortNo;
    INT4                i4count = 0;

    VRRP_DBG2 (VRRP_TRC_EVENTS,
               "Ip Address %s Change Event Processed for If Index %d\n\r",
               VRRP_IPVX_PRINT_ADDR (IpAddr, (UINT1) i4AddrType), u4IfIndex);

    if (VrrpCompIsOurIp ((UINT1) i4AddrType, (INT4) u4IfIndex, IpAddr) ==
        VRRP_NOT_OK)
    {
        VRRP_DBG2 (VRRP_TRC_EVENTS,
                   "Ip Address %s is not ours Ip Change event for interface %d "
                   "not processed %d\r\n",
                   VRRP_IPVX_PRINT_ADDR (IpAddr, (UINT1) i4AddrType),
                   u4IfIndex);
        return;
    }

    for (i4count = 0; i4count < (INT4) gu4VrrpMaxOperEntries; i4count++)
    {
        if ((OPERIFINDEX (i4count) == (INT4) u4IfIndex) &&
            (OPERADDRTYPE (i4count) == (UINT1) i4AddrType) &&
            (OPERROWSTATUS (i4count) == ACTIVE) &&
            (OPERSTATE (i4count) != INITIAL_STATE))
        {
            if ((u4Type == ADDR6_LLOCAL) &&
                (VRRP_IPVX_COMPARE (INTERFACEIPADDR (i4count), IpAddr) != 0))
            {
                VRRP_DBG4 (VRRP_TRC_EVENTS,
                           "Sem for VR %d %d to be restarted since "
                           "IP is changed and new IP interface %d is %s\r\n",
                           OPERVRID (i4count), OPERADDRTYPE (i4count),
                           u4IfIndex,
                           VRRP_IPVX_PRINT_ADDR (IpAddr, (UINT1) i4AddrType));

                VrrpSem (SHUTDOWN_EVENT, i4count, ZERO, ZERO);

                VrrpFillIfIpAddr (i4count);

                VrrpCompSetIpvXOwner (i4count);

                VrrpSem (STARTUP_EVENT, i4count, ZERO, ZERO);
            }
        }
    }                            /* End of For Loop */

    return;
}                                /* End of IP_ADDR_CHG_EVENT processing */

/*******************************************************************************
 * Function Name    : VrrpIfaceDelete
 * Description      : Processes the vlan Interface deletion Event.
 * Inputs           : u4PortNo     - IP Port Number
 *                    i4AddrType   - Address Type IPv4 or IPv6
 * Output           : None
 * Returns          : None
 ******************************************************************************/
VOID
VrrpIfaceDelete (UINT4 u4PortNo, INT4 i4AddrType)
{
    INT4                i4count = 0;
    UINT4               u4IfIndex = u4PortNo;

    VRRP_DBG2 (VRRP_TRC_EVENTS,
               "Interface %d with Address Type %d deleted\n",
               u4IfIndex, i4AddrType);

    for (i4count = 0; i4count < (INT4) gu4VrrpMaxOperEntries; i4count++)
    {
        if ((OPERIFINDEX (i4count) == (INT4) u4IfIndex) &&
            (OPERADDRTYPE (i4count) == (UINT1) i4AddrType))
        {
            if (OPERADMINSTATE (i4count) != ADMINDOWN)
            {
                /*We have to shut down VRRP on this interface. 
                 *Perform related operations before shutdown*/
                VrrpSem (SHUTDOWN_EVENT, i4count, ZERO, ZERO);
            }

            VrrpDbDeleteOperEntry (i4count);
        }
    }

    return;
}

/*******************************************************************************
 * Function Name    : VrrpHandleIfDelete
 * Description      : Processes the vlan Interface deletion Event.
 * Inputs           : u4IfIndex     - IP Interface Index
 * Output           : None
 * Returns          : VRRP_NOT_OK or VRRP_OK
 ******************************************************************************/
INT4
VrrpHandleIfDelete (UINT4 u4IfIndex)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < (INT4) gu4VrrpMaxOperEntries; i4Index++)
    {
        if (OPERIFINDEX (i4Index) != (INT4) u4IfIndex)
        {
            continue;
        }

        if (OPERSTATE (i4Index) != MASTER_STATE)
        {
            continue;
        }

        if (EthDeRegisterMacAddr (OPERMASTERIPADDR (i4Index),
                                  OPERMACADDR (i4Index), i4Index)
            == VRRP_NOT_OK)
        {
            continue;
        }
    }

    return VRRP_OK;
}

/*******************************************************************************
 * Function Name    : VrrpShutdown
 * Description      : Processes the Shutdown Event.
 * Global Variables : VRRP OPER TABLE
 *                    VRRP IP ADDRESS TABLE
 *                    VRRP GLOBAL TABLE
 * Inputs           : None
 * Output           : None
 * Returns          : None
 ******************************************************************************/
VOID
VrrpShutDown ()
{
    INT4                i4count = 0;
    INT1                i1RetVal1 = VRRP_NOT_OK;
    INT1                i1RetVal2 = VRRP_NOT_OK;

    for (i4count = 0; i4count < (INT4) gu4VrrpMaxOperEntries; i4count++)
    {
        if (OPERROWSTATUS (i4count) == ACTIVE)
        {
            VrrpSem (SHUTDOWN_EVENT, i4count, ZERO, ZERO);

            /* Leaving multicast group */
            VrrpIpifLeaveMcastGroup (i4count);

            VRRP_DBG1 (VRRP_TRC_INIT,
                       " INIT : Going to Shutdown the SEM %d\n", i4count);

            /* Clear Statistics */
            VrrpClearStats (i4count);
        }                        /* Check Row Status */

        VRRP_DBG1 (VRRP_TRC_INIT,
                   "INIT : Shutdown Event processed by SEM for OperIndex  %d\n",
                   i4count);

    }                            /* For Loop */

    i1RetVal1 = (INT1) SelRemoveFd (gi4VrrpIpv4Socket);

    i1RetVal2 = (INT1) SelRemoveFd (gi4VrrpIpv6Socket);

    if ((i1RetVal1 == VRRP_NOT_OK) || (i1RetVal2 == VRRP_NOT_OK))
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "VRRP SelRemoveFd failed\n");
        return;
    }

    i1RetVal1 = VrrpCloseSocket (&gi4VrrpIpv4Socket);

    i1RetVal2 = VrrpCloseSocket (&gi4VrrpIpv6Socket);

    if ((i1RetVal1 == VRRP_NOT_OK) || (i1RetVal2 == VRRP_NOT_OK))
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "VRRP: Closing Socket FAILED\n");

        return;
    }
}

/*******************************************************************************
 * Function Name    : VrrpGetOperPriority
 * Description      : This function gets the priority being assigned by the 
 *                    Vrrp protocol.
 * Global Variables : VRRP OPER TABLE
 *                    VRRP IP ADDRESS TABLE
 *                    VRRP GLOBAL TABLE
 * Inputs           : None
 * Output           : None
 * Returns          : None
 ******************************************************************************/
INT4
VrrpGetOperPriority (INT4 i4OperIndex)
{
    INT4                i4Priority = 0;

    if (OPERISOWNER (i4OperIndex) == TRUE)
    {
        i4Priority = VRRP_IPOWNER_PRIORITY;
    }
    else
    {
        i4Priority = OPERADMINPRIORITY (i4OperIndex);
    }

    if (OPERTRACKIFSTATUS (i4OperIndex) == VRRP_TRACK_IF_DOWN)
    {
        i4Priority = (i4Priority - OPERDECPRIO (i4OperIndex));
    }

    return i4Priority;
}
