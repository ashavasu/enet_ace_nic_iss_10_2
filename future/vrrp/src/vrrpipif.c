/********************************************************************
 * $Id: vrrpipif.c,v 1.61 2017/12/16 11:57:11 siva Exp $          *
 *                                    *
 * $RCSfile: vrrpipif.c,v $
 *                                                                  *
 * $Date: 2017/12/16 11:57:11 $
 *                                                                  *
 * $Revision: 1.61 $
 *                                                                  *
 *******************************************************************/

/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrpipif.c                                   |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Interface Module.                       |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : IP     Interface Module of VRRP Core Protocol.|
 * | The calls in this module would be mapped on to the target system calls.  |
 * +--------------------------------------------------------------------------+
 *
 *  CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |  Padmanaban. B       |   Base Line Version             |
 * |                 |                      |                                 |
 * |  1.1.0.0        |  Padmanaban. B       |   Changes for IMS 1600          |
 * |                 |                      |                                 |
 * |  1.2.0.0        |  Padmanaban. B       |   Enhancements to make          |
 * |                 |                      |   compatible  to IP Ver 1.10    |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#ifndef _IP_IF_C_
#define _IP_IF_C_

#include  "vrrpinc.h"
#include  "ip.h"
#include  "vcm.h"

#ifdef L3_SWITCHING_WANTED
#include "ipnp.h"
#endif /* L3_SWITCHING_WANTED */

extern UINT4        gu4IsIvrEnabled;
extern VOID         VrrpDeleteInterface (INT4, INT4);
/*******************************************************************************
 * Function Name   : VrrpSendGratArp
 * Description     : Sends gratuitous ARP Request for all
 *                   the IP Addresses it owns
 * Global Varibles : VRRP OPER TABLE.
 * Inputs          : i4OperIndex
 * Output          : None.
 * Returns         : VRRP_OK
 *                   VRRP_NOT_OK
 ******************************************************************************/
EXPORT INT1
VrrpSendGratArp (INT4 i4OperIndex)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;
    UINT4               u4AssoIpAddr = 0;
    UINT4               u4Port = 0;
    t_ARP_PKT           ArpPkt;

    if (VRRP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return VRRP_OK;
    }

    if (OPERADDRTYPE (i4OperIndex) != VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        return VRRP_OK;
    }

    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    VRRP_MEMSET (&ArpPkt, 0, sizeof (t_ARP_PKT));
    VRRP_MEMCPY (ArpPkt.i1Shwaddr, &OPERMACADDR (i4OperIndex),
                 CFA_ENET_ADDR_LEN);
    VRRP_MEMSET (ArpPkt.i1Thwaddr, 0xff, CFA_ENET_ADDR_LEN);
    ArpPkt.i2Opcode = ARP_REQUEST;

    UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                         pTempIpAddrEntry, tIpAddrTable *)
    {
        VRRP_IPVX_COPY_TO_IPV4 (u4AssoIpAddr, pIpAddrEntry->AssoIpAddr);

        if (u4AssoIpAddr != 0)
        {
            ArpPkt.u4Tproto_addr = u4AssoIpAddr;
            ArpPkt.u4Sproto_addr = u4AssoIpAddr;

            if (ArpSendReqOrResp ((UINT2) u4Port,
                                  CFA_ENCAP_ENETV2, &ArpPkt) == ARP_FAILURE)
            {
                VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                          "IpIf: -E- Failed to Send Gratituous Arp\n");
                PktProcessUpdateStatistics (i4OperIndex, VRRPSENDGRATARPFAIL);
                return VRRP_NOT_OK;
            }
            VRRP_DBG (VRRP_TRC_PKT, "IpIf: Successfully Sent Gratituous Arp\n");
        }
    }

    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : VrrpSendNDNeighborAdvtToAllAssocIp
 * Description     : Sends Neighbor Advertisements for all IP Addresses it owns.
 * Global Varibles : VRRP OPER TABLE.
 * Inputs          : i4OperIndex
 * Output          : None.
 * Returns         : VRRP_OK
 *                   VRRP_NOT_OK
 ******************************************************************************/
EXPORT INT1
VrrpSendNDNeighborAdvtToAllAssocIp (INT4 i4OperIndex)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;
    UINT4               u4Port = 0;

    if (VRRP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return VRRP_OK;
    }

    if (OPERADDRTYPE (i4OperIndex) != VRRP_IPVX_ADDR_FMLY_IPV6)
    {
        return VRRP_OK;
    }

    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                         pTempIpAddrEntry, tIpAddrTable *)
    {
        if (VrrpSendNDNeighborAdvt (i4OperIndex, u4Port,
                                    pIpAddrEntry->AssoIpAddr) == VRRP_NOT_OK)
        {
            /* Failure Not handled */
        }
    }

    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : VrrpSendNDNeighborAdvt
 * Description     : Sends Neighbor Advertisements for all IP Addresses it owns.
 * Global Varibles : VRRP OPER TABLE.
 * Inputs          : i4OperIndex
 * Output          : None.
 * Returns         : VRRP_OK
 *                   VRRP_NOT_OK
 ******************************************************************************/
EXPORT INT1
VrrpSendNDNeighborAdvt (INT4 i4OperIndex, UINT4 u4Port, tVrrpIPvXAddr IpAddr)
{
    tNd6AddrExt         Nd6Addr;
    tNd6NeighAdv        Nd6NeighAdv;
    tIp6Addr            Ip6Src;
    tIp6Addr            Ip6Dest;
    UINT1               au1TxBuf[sizeof (tNd6NeighAdv) + sizeof (tNd6AddrExt)];
    UINT1               au1Cmsg[(sizeof (struct cmsghdr) * 2) +
                                sizeof (struct in6_pktinfo)];
    UINT1               au1DestIpv6Addr[IPVX_IPV6_ADDR_LEN] =
        { 0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01
    };
    struct sockaddr_in6 DestAddr;
    struct msghdr       MsgHdr;
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo = NULL;
    INT4                i4Size = 0;
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif

    MEMSET ((UINT1 *) &DestAddr, 0, sizeof (DestAddr));
    MEMSET (au1TxBuf, 0, sizeof (au1TxBuf));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET ((UINT1 *) &MsgHdr, 0, sizeof (MsgHdr));
    MEMSET ((UINT1 *) &Nd6NeighAdv, 0, sizeof (tNd6NeighAdv));
    MEMSET ((UINT1 *) &Nd6Addr, 0, sizeof (tNd6AddrExt));
    MEMSET ((UINT1 *) &Ip6Src, 0, sizeof (tIp6Addr));
    MEMSET ((UINT1 *) &Ip6Dest, 0, sizeof (tIp6Addr));

    i4Size = sizeof (tNd6NeighAdv) + sizeof (tNd6AddrExt);

    Nd6Addr.u1Type = ND6_TARG_LLA_EXT;
    Nd6Addr.u1Len = ND6_LLA_EXT_LEN;
    MEMCPY (&Nd6Addr.lladdr, OPERMACADDR (i4OperIndex), MAC_ADDR_LEN);

    Nd6NeighAdv.icmp6Hdr.u1Type = ND6_NEIGHBOR_ADVERTISEMENT;
    Nd6NeighAdv.icmp6Hdr.u1Code = ND6_RSVD_CODE;
    Nd6NeighAdv.icmp6Hdr.u2Chksum = 0;
    Nd6NeighAdv.u4AdvFlag = (ND6_DEFAULT_ROUTER + ND6_OVERRIDE_FLAG);
    Nd6NeighAdv.u4AdvFlag = OSIX_HTONL (Nd6NeighAdv.u4AdvFlag);
    MEMCPY (Nd6NeighAdv.targAddr6.u1_addr, IpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);

    MEMCPY (Ip6Src.u1_addr, INTERFACEIPADDR (i4OperIndex).au1Addr,
            IPVX_IPV6_ADDR_LEN);

    MEMCPY (Ip6Dest.u1_addr, au1DestIpv6Addr, IPVX_IPV6_ADDR_LEN);

    MEMCPY (au1TxBuf, (UINT1 *) &Nd6NeighAdv, sizeof (tNd6NeighAdv));

    MEMCPY (&au1TxBuf[sizeof (tNd6NeighAdv)], (UINT1 *) &Nd6Addr,
            sizeof (tNd6AddrExt));

    Nd6NeighAdv.icmp6Hdr.u2Chksum =
        UtlIp6LinChecksum (&Ip6Src, &Ip6Dest, (UINT4) i4Size, IPPROTO_ICMPV6,
                           au1TxBuf);
#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = au1TxBuf;
    Iov.iov_len = (size_t) i4Size;
    MsgHdr.msg_iov = &Iov;
    MsgHdr.msg_iovlen = 1;
#endif

    DestAddr.sin6_family = AF_INET6;
    MEMCPY (DestAddr.sin6_addr.s6_addr, au1DestIpv6Addr, IPVX_IPV6_ADDR_LEN);

    MsgHdr.msg_name = (VOID *) &DestAddr;
    MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    MsgHdr.msg_control = (VOID *) au1Cmsg;
    MsgHdr.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&MsgHdr);
    pCmsgInfo->cmsg_level = SOL_RAW;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&MsgHdr));

    pIpPktInfo->ipi6_ifindex = (INT4) u4Port;
    MEMCPY (pIpPktInfo->ipi6_addr.s6_addr,
            INTERFACEIPADDR (i4OperIndex).au1Addr, IPVX_IPV6_ADDR_LEN);

    gi4VrrpNDSocket = VrrpOpenNdSocket ();

    if (gi4VrrpNDSocket == VRRP_NOT_OK)
    {
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDNDNEIGHBORADVTFAIL);
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Invalid socket to send ND advertisment message for "
                   "VRID %d\r\n", OPERVRID (i4OperIndex));

        return VRRP_NOT_OK;
    }

    if (setsockopt (gi4VrrpNDSocket, IPPROTO_IPV6, IPV6_MULTICAST_IF,
                    (void *) &u4Port, sizeof (UINT4)) < 0)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Setting Outgoing Interface for VRRP ND NA Packet Failed "
                   "for VRID %d\n", OPERVRID (i4OperIndex));

        if (VrrpCloseSocket (&gi4VrrpNDSocket) == VRRP_NOT_OK)
        {
            /* Ignore Failure */
        }
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDNDNEIGHBORADVTFAIL);
        return VRRP_NOT_OK;
    }

    if (sendmsg (gi4VrrpNDSocket, &MsgHdr, 0) < 0)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Sending Message to SLI for VRRP ND NA Packet Failed for "
                   "VRID %d\n", OPERVRID (i4OperIndex));

        if (VrrpCloseSocket (&gi4VrrpNDSocket) == VRRP_NOT_OK)
        {
            /* Ignore Failure */
        }
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDNDNEIGHBORADVTFAIL);
        return VRRP_NOT_OK;
    }

#ifndef BSDCOMP_SLI_WANTED
    if (sendto (gi4VrrpNDSocket, au1TxBuf, i4Size, 0,
                (struct sockaddr *) &DestAddr, sizeof (struct sockaddr_in6))
        < 0)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Sending Packet to SLI for VRRP ND NA Packet failed for "
                   "VRID %d\n", OPERVRID (i4OperIndex));

        if (VrrpCloseSocket (&gi4VrrpNDSocket) == VRRP_NOT_OK)
        {
            /* Ignore Failure */
        }
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDNDNEIGHBORADVTFAIL);
        return VRRP_NOT_OK;
    }
#endif

    VRRP_DBG1 (VRRP_TRC_PKT,
               "VRRP ND NA Packet Sent to IPv6 for VRID %d\n",
               OPERVRID (i4OperIndex));

    if (VrrpCloseSocket (&gi4VrrpNDSocket) == VRRP_NOT_OK)
    {
        /* Ignore Failure */
    }

    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : VrrpJoinSolicitedMulticastGroup
 * Description     : This function computes and joins solicited node multicast
 *                   group.
 * Global Varibles : VRRP OPER TABLE.
 * Inputs          : i4OperIndex      - Oper Index
 *                   b1IsAcceptConf   - Flag to indicate if this function is
 *                                      called through Accept Mode Configuration
 * Output          : None.
 * Returns         : VRRP_OK
 *                   VRRP_NOT_OK
 ******************************************************************************/
EXPORT INT1
VrrpJoinSolicitedMulticastGroup (INT4 i4OperIndex, BOOL1 b1IsAcceptConf)
{
    INT1                i1RetVal = VRRP_NOT_OK;

    if ((OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4) ||
        (OPERACCEPTMODE (i4OperIndex) == VRRP_ENABLE))
    {
        VRRP_DBG (VRRP_TRC_ALL,
                  "Joining Solicited Multicast Group not required for "
                  "IPv4\r\n");

        return VRRP_OK;
    }

    i1RetVal = VrrpCreateSecIpOrJoinSoliMCast (i4OperIndex, gVrrpZeroIpAddr,
                                               b1IsAcceptConf);

    return i1RetVal;
}

/*******************************************************************************
 * Function Name   : VrrpCreateSecIpOrJoinSoliMCast
 * Description     : This function computes & join solicited multicast group or
 *                   creates a secondary IPv6 address based on the accept mode
 *                   configuration.
 * Global Varibles : VRRP OPER TABLE.
 * Inputs          : i4OperIndex      - Oper Index
 *                   IpAddr           - IP Address
 *                   b1IsAcceptConf   - Flag to indicate if this function is
 *                                      called through Accept Mode Configuration
 * Output          : None.
 * Returns         : VRRP_OK
 *                   VRRP_NOT_OK
 ******************************************************************************/
EXPORT INT1
VrrpCreateSecIpOrJoinSoliMCast (INT4 i4OperIndex, tVrrpIPvXAddr IpAddr,
                                BOOL1 b1IsAcceptConf)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;
    tVrrpNwIntf         VrrpNwIntf;
    UINT4               u4Port = 0;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);
    MEMSET (&VrrpNwIntf, 0, sizeof (tVrrpNwIntf));

    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);
    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        VRRP_DBG3 (VRRP_TRC_ALL_FAILURES,
                   "Fetching IP Port from If Index failed during "
                   "Secondary IP Addition or Joining MCast Group for "
                   "IfIndex: %d VRID %d Addr Type %s\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);

        return VRRP_NOT_OK;
    }

    VrrpNwIntf.u4IfIndex = (UINT4) OPERIFINDEX (i4OperIndex);
    VrrpNwIntf.u4Port = u4Port;
    VRRP_IPVX_COPY (VrrpNwIntf.IntfAddr, INTERFACEIPADDR (i4OperIndex));
    MEMCPY (VrrpNwIntf.au1MacAddr, OPERMACADDR (i4OperIndex), MAC_ADDR_LEN);
    VrrpNwIntf.u4VrId = (UINT4) OPERVRID (i4OperIndex);
    VrrpNwIntf.u1AddrType = OPERADDRTYPE (i4OperIndex);
    VrrpNwIntf.b1IsIpvXOwner = OPERISOWNER (i4OperIndex);

    if (OPERACCEPTMODE (i4OperIndex) == VRRP_DISABLE)
    {
        VrrpNwIntf.u1Action = VRRP_NW_INTF_MCAST_CREATE;
        VrrpNwIntf.u1AcceptMode = FALSE;
    }
    else
    {
        VrrpNwIntf.u1Action = VRRP_NW_INTF_SECONDARY_CREATE;
        VrrpNwIntf.u1AcceptMode = TRUE;
    }

    VrrpNwIntf.b1IsAcceptConf = b1IsAcceptConf;
    VrrpNwIntf.b1IsChgNwActionReqd = TRUE;
    VrrpNwIntf.b1IsNwActionChgd = FALSE;
    if (VrrpNetIpGetWr (&VrrpNwIntf, &VrrpNwIntf) == VRRP_NOT_OK)
    {
        /* Ignore Failure */
    }
    VrrpNwIntf.b1IsChgNwActionReqd = FALSE;

    if ((OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4) &&
        (VrrpNwIntf.u1Action == VRRP_NW_INTF_MCAST_CREATE))
    {
        VRRP_DBG3 (VRRP_TRC_EVENTS,
                   "Joining MCast Group not required for IPv4 for "
                   "IfIndex %d VRID %d AddrType %d\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   OPERADDRTYPE (i4OperIndex));

        return VRRP_OK;
    }

    if ((VrrpNwIntf.b1IsNwActionChgd == FALSE) &&
        (VrrpNwIntf.u1Action == VRRP_NW_INTF_SECONDARY_CREATE) &&
        (OPERACCEPTMODE (i4OperIndex) == VRRP_DISABLE))
    {
        VRRP_DBG3 (VRRP_TRC_EVENTS,
                   "Secondary IP creation not done since accept mode "
                   "is disabled for IfIndex %d VRID %d AddrType %d\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   OPERADDRTYPE (i4OperIndex));

        return VRRP_OK;
    }

    VrrpNwIntf.b1IsNwActionChgd = FALSE;

    UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                         pTempIpAddrEntry, tIpAddrTable *)
    {
        if ((VRRP_IPVX_COMPARE (IpAddr, gVrrpZeroIpAddr) != 0) &&
            (VRRP_IPVX_COMPARE (IpAddr, pIpAddrEntry->AssoIpAddr) != 0))
        {
            continue;
        }

        if (VrrpCompIsOurIp ((UINT1) OPERADDRTYPE (i4OperIndex),
                             OPERIFINDEX (i4OperIndex),
                             pIpAddrEntry->AssoIpAddr) == VRRP_OK)
        {
            VRRP_DBG5 (VRRP_TRC_EVENTS,
                       "Skipping Action %d for IP %s in NetIp Layer for "
                       "IfIndex %d VRID %d AddrType %s as it is already "
                       "Our Ip\r\n",
                       VrrpNwIntf.u1Action,
                       VRRP_IPVX_PRINT_ADDR (pIpAddrEntry->AssoIpAddr,
                                             (UINT1)
                                             OPERADDRTYPE (i4OperIndex)),
                       OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                       ac1AddrType);
            continue;
        }

        VRRP_IPVX_COPY (VrrpNwIntf.IpvXAddr, pIpAddrEntry->AssoIpAddr);
        VrrpNwIntf.u1SecIndex = pIpAddrEntry->u1SecIndex;

        VRRP_IPVX_COPY (VrrpNwIntf.CheckIp, OPERPRIMARYIPADDR (i4OperIndex));
        VrrpNwIntf.b1IsCheckIpReqd = TRUE;
        if (VrrpNetIpGetWr (&VrrpNwIntf, &VrrpNwIntf) == VRRP_NOT_OK)
        {
            continue;
        }
        VrrpNwIntf.b1IsCheckIpReqd = FALSE;

        if (VrrpNetIpCreateWr (i4OperIndex, &VrrpNwIntf) == VRRP_NOT_OK)
        {
            VRRP_DBG5 (VRRP_TRC_EVENTS,
                       "Adding IP %s with Action %d in NetIp Layer failed "
                       " for IfIndex %d VRID %d AddrType %s\r\n",
                       VRRP_IPVX_PRINT_ADDR (pIpAddrEntry->AssoIpAddr,
                                             (UINT1)
                                             OPERADDRTYPE (i4OperIndex)),
                       VrrpNwIntf.u1Action, OPERIFINDEX (i4OperIndex),
                       OPERVRID (i4OperIndex), ac1AddrType);
            continue;
        }

        VRRP_DBG5 (VRRP_TRC_EVENTS,
                   "IP %s with Action %d in NetIp layer for IfIndex %d VRID %d "
                   "Address Type %s Added\r\n",
                   VRRP_IPVX_PRINT_ADDR (pIpAddrEntry->AssoIpAddr,
                                         (UINT1) OPERADDRTYPE (i4OperIndex)),
                   VrrpNwIntf.u1Action,
                   OPERIFINDEX (i4OperIndex),
                   OPERVRID (i4OperIndex), ac1AddrType);
        pIpAddrEntry->u1NwStatus = TRUE;
    }

    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : VrrpLeaveSolicitedMulticastGroup
 * Description     : This function leaves solicited node multicast
 *                   group.
 * Global Varibles : VRRP OPER TABLE.
 * Inputs          : i4OperIndex      - Oper Index
 *                   b1IsAcceptConf   - Flag to indicate if this function is
 *                                      called through Accept Mode Configuration
 * Output          : None.
 * Returns         : VRRP_OK
 *                   VRRP_NOT_OK
 ******************************************************************************/
EXPORT INT1
VrrpLeaveSolicitedMulticastGroup (INT4 i4OperIndex, BOOL1 b1IsAcceptConf)
{
    INT1                i1RetVal = VRRP_NOT_OK;

    if ((OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4) ||
        (OPERACCEPTMODE (i4OperIndex) == VRRP_ENABLE))
    {
        VRRP_DBG (VRRP_TRC_ALL,
                  "Leaving Solicited Multicast Group not required for "
                  "IPv4\r\n");

        return VRRP_OK;
    }

    i1RetVal = VrrpDeleteSecIpOrLeaveSoliMCast (i4OperIndex, gVrrpZeroIpAddr,
                                                b1IsAcceptConf);

    return i1RetVal;
}

/*******************************************************************************
 * Function Name   : VrrpDeleteSecIpOrLeaveSoliMCast
 * Description     : This function leaves solicited multicast group or
 *                   deletes a secondary IPv6 address based on the accept mode
 *                   configuration.
 * Global Varibles : VRRP OPER TABLE.
 * Inputs          : i4OperIndex      - Oper Index
 *                   IpAddr           - IP Address
 *                   b1IsAcceptConf   - Flag to indicate if this function is
 *                                      called through Accept Mode Configuration
 * Output          : None.
 * Returns         : VRRP_OK
 *                   VRRP_NOT_OK
 ******************************************************************************/
EXPORT INT1
VrrpDeleteSecIpOrLeaveSoliMCast (INT4 i4OperIndex, tVrrpIPvXAddr IpAddr,
                                 BOOL1 b1IsAcceptConf)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;
    tVrrpNwIntf         VrrpNwIntf;
    UINT4               u4Port = 0;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);

    MEMSET (&VrrpNwIntf, 0, sizeof (tVrrpNwIntf));
    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);
    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        VRRP_DBG3 (VRRP_TRC_ALL_FAILURES,
                   "Fetching IP Port from If Index failed during "
                   "Secondary IP Deletion or Leaving MCast Group for "
                   "IfIndex: %d VRID %d Addr Type %s\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        return VRRP_NOT_OK;
    }

    VrrpNwIntf.u4IfIndex = (UINT4) OPERIFINDEX (i4OperIndex);
    VrrpNwIntf.u4Port = u4Port;
    VRRP_IPVX_COPY (VrrpNwIntf.IntfAddr, INTERFACEIPADDR (i4OperIndex));
    MEMCPY (VrrpNwIntf.au1MacAddr, OPERMACADDR (i4OperIndex), MAC_ADDR_LEN);
    VrrpNwIntf.u4VrId = (UINT4) OPERVRID (i4OperIndex);
    VrrpNwIntf.u1AddrType = OPERADDRTYPE (i4OperIndex);

    if (OPERACCEPTMODE (i4OperIndex) == VRRP_DISABLE)
    {
        VrrpNwIntf.u1Action = VRRP_NW_INTF_MCAST_DELETE;
        VrrpNwIntf.u1AcceptMode = FALSE;
    }
    else
    {
        VrrpNwIntf.u1Action = VRRP_NW_INTF_SECONDARY_DELETE;
        VrrpNwIntf.u1AcceptMode = TRUE;
    }

    VrrpNwIntf.b1IsAcceptConf = b1IsAcceptConf;
    VrrpNwIntf.b1IsChgNwActionReqd = TRUE;
    VrrpNwIntf.b1IsNwActionChgd = FALSE;
    if (VrrpNetIpGetWr (&VrrpNwIntf, &VrrpNwIntf) == VRRP_NOT_OK)
    {
        /* Ignore Failure */
    }
    VrrpNwIntf.b1IsChgNwActionReqd = FALSE;

    if ((OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4) &&
        (VrrpNwIntf.u1Action == VRRP_NW_INTF_MCAST_DELETE))
    {
        VRRP_DBG3 (VRRP_TRC_EVENTS,
                   "Leaving MCast Group not required for IPv4 for "
                   "IfIndex %d VRID %d AddrType %s\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        return VRRP_OK;
    }

    if ((VrrpNwIntf.b1IsNwActionChgd == FALSE) &&
        (VrrpNwIntf.u1Action == VRRP_NW_INTF_SECONDARY_CREATE) &&
        (OPERACCEPTMODE (i4OperIndex) == VRRP_DISABLE))
    {
        VRRP_DBG3 (VRRP_TRC_EVENTS,
                   "Secondary IP deletion not done since accept mode "
                   "is disabled for IfIndex %d VRID %d AddrType %s\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);

        return VRRP_OK;
    }

    VrrpNwIntf.b1IsNwActionChgd = FALSE;

    UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                         pTempIpAddrEntry, tIpAddrTable *)
    {
        if ((VRRP_IPVX_COMPARE (IpAddr, gVrrpZeroIpAddr) != 0) &&
            (VRRP_IPVX_COMPARE (IpAddr, pIpAddrEntry->AssoIpAddr) != 0))
        {
            continue;
        }

        if (pIpAddrEntry->u1NwStatus == FALSE)
        {
            VRRP_DBG5 (VRRP_TRC_EVENTS,
                       "Skipping Action %d for IP %s in NetIp Layer for "
                       "IfIndex %d VRID %d AddrType %s as it is already "
                       "not added\r\n",
                       VrrpNwIntf.u1Action,
                       VRRP_IPVX_PRINT_ADDR (pIpAddrEntry->AssoIpAddr,
                                             (UINT1)
                                             OPERADDRTYPE (i4OperIndex)),
                       OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                       ac1AddrType);
            continue;
        }

        VRRP_IPVX_COPY (VrrpNwIntf.IpvXAddr, pIpAddrEntry->AssoIpAddr);
        VrrpNwIntf.u1SecIndex = pIpAddrEntry->u1SecIndex;

        VRRP_IPVX_COPY (VrrpNwIntf.CheckIp, OPERPRIMARYIPADDR (i4OperIndex));
        VrrpNwIntf.b1IsCheckIpReqd = TRUE;
        if (VrrpNetIpGetWr (&VrrpNwIntf, &VrrpNwIntf) == VRRP_NOT_OK)
        {
            VRRP_DBG5 (VRRP_TRC_EVENTS,
                       "Skipping Action %d for IP %s in NetIp Layer for "
                       "IfIndex %d VRID %d AddrType %s as it is already "
                       "not added\r\n",
                       VrrpNwIntf.u1Action,
                       VRRP_IPVX_PRINT_ADDR (pIpAddrEntry->AssoIpAddr,
                                             (UINT1)
                                             OPERADDRTYPE (i4OperIndex)),
                       OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                       ac1AddrType);
            continue;
        }
        VrrpNwIntf.b1IsCheckIpReqd = FALSE;

        if (VrrpNetIpDeleteWr (i4OperIndex, &VrrpNwIntf) == VRRP_NOT_OK)
        {
            VRRP_DBG5 (VRRP_TRC_EVENTS,
                       "Deleting IP %s with Action %d in NetIp Layer failed "
                       " for IfIndex %d VRID %d AddrType %s\r\n",
                       VRRP_IPVX_PRINT_ADDR (pIpAddrEntry->AssoIpAddr,
                                             (UINT1)
                                             OPERADDRTYPE (i4OperIndex)),
                       VrrpNwIntf.u1Action, OPERIFINDEX (i4OperIndex),
                       OPERVRID (i4OperIndex), ac1AddrType);
            continue;
        }

        VRRP_DBG5 (VRRP_TRC_EVENTS,
                   "IP %s with Action %d Deleted in NetIp layer for IfIndex %d VRID %d "
                   "Address Type %s\r\n",
                   VRRP_IPVX_PRINT_ADDR (pIpAddrEntry->AssoIpAddr,
                                         (UINT1) OPERADDRTYPE (i4OperIndex)),
                   VrrpNwIntf.u1Action,
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        pIpAddrEntry->u1NwStatus = FALSE;
    }

    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : VrrpFillIfIpAddr.
 * Description     : Gets the IP Address for the given interface based on IPv4
 *                   or IPv6.
 * Global Varibles : None.
 * Inputs          : i4OperIndex - Oper Index.
 * Output          : None.
 * Returns         : None.
******************************************************************************/
EXPORT VOID
VrrpFillIfIpAddr (INT4 i4OperIndex)
{
    if (OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        VrrpFillV4IfIpAddr (i4OperIndex);
    }
    else
    {
        VrrpFillV6IfIpAddr (i4OperIndex);
    }

    return;
}

/*******************************************************************************
 * Function Name   : VrrpFillV4IfIpAddr.
 * Description     : Gets the IPv4 Address for the given interface.
 * Global Varibles : None.
 * Inputs          : i4OperIndex - Oper Index.
 * Output          : None.
 * Returns         : None.
 ******************************************************************************/

EXPORT VOID
VrrpFillV4IfIpAddr (INT4 i4OperIndex)
{
    tNetIpv4IfInfo      IfInfo;
    UINT4               u4Port = 0;
    INT4                i4RetVal = 0;

    MEMSET (&IfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (NetIpv4GetPortFromIfIndex ((UINT4) OPERIFINDEX (i4OperIndex),
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return;
    }

    i4RetVal = NetIpv4GetIfInfo (u4Port, &IfInfo);

    UNUSED_PARAM (i4RetVal);

    VRRP_IPVX_COPY_TO_IPVX (INTERFACEIPADDR (i4OperIndex), IfInfo.u4Addr);

    return;
}

/*******************************************************************************
 * Function Name   : VrrpFillV6IfIpAddr.
 * Description     : Gets the IPv6 Address for the given interface.
 * Global Varibles : None.
 * Inputs          : i4OperIndex - Oper Index.
 * Output          : None.
 * Returns         : None.
 ******************************************************************************/

EXPORT VOID
VrrpFillV6IfIpAddr (INT4 i4OperIndex)
{
    tNetIpv6IfInfo      IfInfo;
#ifdef IP6_WANTED
    INT4                i4Ret = 0;
#endif

    MEMSET (&IfInfo, 0, sizeof (tNetIpv6IfInfo));

#ifdef IP6_WANTED
    i4Ret = NetIpv6GetIfInfo ((UINT4) OPERIFINDEX (i4OperIndex), &IfInfo);
    if (i4Ret == NETIPV6_FAILURE)
    {
        return;
    }
#endif
    /* This Init Macro actually copies V6 Address into IPVX format */
    VRRP_IPVX_ADDR_INIT_IPV6 (INTERFACEIPADDR (i4OperIndex),
                              IfInfo.Ip6Addr.u1_addr);

    return;
}

/*******************************************************************************
 * Function Name   : VrrpRegisterwithIp.
 * Description     : Registers the VRRP Module with IP.
 * Global Varibles : None.
 * Inputs          : u1ModuleId       -  protocol ID
 *                   pLinkStatusFunc  -  call back function to indicate link
 *                   status.
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
EXPORT INT1
VrrpRegisterwithIp (UINT1 u1ModuleId, VrrpFuncPtr pLinkStatusFunc)
{
    tNetIpRegInfo       RegnInfo;
    UINT4               u4Mask = 0;

    MEMSET ((UINT1 *) &RegnInfo, 0, sizeof (tNetIpRegInfo));

    RegnInfo.u1ProtoId = u1ModuleId;
    RegnInfo.pIfStChng = (VOID *) pLinkStatusFunc;
    RegnInfo.pRtChng = NULL;
    RegnInfo.pProtoPktRecv = NULL;
    RegnInfo.u2InfoMask = NETIPV4_IFCHG_REQ;
    RegnInfo.u4ContextId = VCM_DEFAULT_CONTEXT;

    if (NetIpv4RegisterHigherLayerProtocol (&RegnInfo) == NETIPV4_FAILURE)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "Registration with IPv4 Failed\n");

        return VRRP_NOT_OK;
    }

    u4Mask = (NETIPV6_ADDRESS_CHANGE | NETIPV6_INTERFACE_PARAMETER_CHANGE);

#ifdef IP6_WANTED
    if (NetIpv6RegisterHigherLayerProtocol ((UINT4) u1ModuleId, u4Mask,
                                            VrrpIp6RcvNotification)
        == NETIPV6_FAILURE)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "Registration with IPv6 Failed\n");

        return VRRP_NOT_OK;
    }
#endif

    VRRP_DBG (VRRP_TRC_INIT, "Registere with both IPv4 and IPv6\n");

    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : VrrpDeRegisterwithIp.
 * Description     : Deregisters the VRRP protocol.
 * Global Varibles : None.
 * Inputs          : u1ModuleId
 * Output          : None.
 * Returns         : VRRP_OK
 *                   VRRP_NOT_OK
 ******************************************************************************/
EXPORT INT1
VrrpDeRegisterwithIp (UINT1 u1ModuleId)
{
    if (NetIpv4DeRegisterHigherLayerProtocol (u1ModuleId) == NETIPV4_FAILURE)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "Deregistration with IPv4 failed\n");

        return VRRP_NOT_OK;
    }

#ifdef IP6_WANTED
    if (NetIpv6DeRegisterHigherLayerProtocol (u1ModuleId) == NETIPV4_FAILURE)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "Deregistration with Ipv6 failed\n");

        return VRRP_NOT_OK;
    }
#endif
    VRRP_DBG (VRRP_TRC_INIT, "Deregistered with both IPv4 and IPv6\n");

    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : VrrpSendPacket.
 * Description     : Function to Send an IP Packet.
 * Global Varibles : None.
 * Inputs          : i4Size.
 *                   i4OperIndex.
 * Output          : None.
 * Returns         : VRRP_NOT_OK.
 *                   VRRP_OK.
 ******************************************************************************/
EXPORT INT1
VrrpSendPacket (INT4 i4Size, INT4 i4OperIndex)
{
    INT1                i1RetVal = VRRP_NOT_OK;

    if (OPERADDRTYPE (i4OperIndex) == (UINT1) VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        i1RetVal = VrrpSendPacketToIpv4 (i4Size, i4OperIndex);
    }
    else
    {
        i1RetVal = VrrpSendPacketToIpv6 (i4Size, i4OperIndex);
    }

    return i1RetVal;
}

/*******************************************************************************
 * Function Name   : VrrpSendPacketToIpv4
 * Description     : Function to Send an IPv4 Packet.
 * Global Varibles : None.
 * Inputs          : i4Size.
 *                   i4OperIndex.
 * Output          : None.
 * Returns         : VRRP_NOT_OK.
 *                   VRRP_OK.
 ******************************************************************************/
EXPORT INT1
VrrpSendPacketToIpv4 (INT4 i4Size, INT4 i4OperIndex)
{
    UINT1               u1Proto = VRRPMODULE;
    UINT1               u1Tos = 0;
    UINT1               u1Ttl = VRRP_TTL;
    UINT1               u1Df = 0;
    UINT4               u4Port = 0;
    UINT2               u2Id = 0;
    UINT2               u2TotLen;
    UINT4               u4Dest = VRRP_MCAST_ADDR;    /*Vrrp multicast address */
    UINT1               au1Cmsg[((sizeof (struct cmsghdr)) * 2)];
    t_IP_HEADER         IpHeader;
    struct sockaddr_in  DestAddr;
    struct msghdr       MsgHdr;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo = NULL;
    struct ip_mreqn     Mreqn;
    UINT1              *pu1TxBuf = NULL;
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif

    MEMSET (&IpHeader, 0, sizeof (t_IP_HEADER));
    MEMSET ((UINT1 *) &DestAddr, 0, sizeof (DestAddr));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET ((UINT1 *) &MsgHdr, 0, sizeof (MsgHdr));
    MEMSET (&Mreqn, 0, sizeof (Mreqn));

    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    if (gi4VrrpIpv4Socket == VRRP_NOT_OK)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Invalid socket to send VRRP Advertisement message for "
                   "VRID %d\r\n", OPERVRID (i4OperIndex));
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDPACKETTOIPV4FAIL);
        return VRRP_NOT_OK;
    }

    pu1TxBuf = (UINT1 *) MemAllocMemBlk (VRRP_IPV4_PKT_MEMPOOL_ID);

    if (pu1TxBuf == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for IPv4 Tx Pkt Buffer\r\n");
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDPACKETTOIPV4FAIL);
        return VRRP_NOT_OK;
    }

    MEMSET (pu1TxBuf, 0,
            (FsVRRPSizingParams[MAX_VRRP_TX_RX_IPV4_PKT_SIZING_ID].
             u4StructSize));

    MEMSET (&IpHeader, 0, IP_HDR_LEN);
    IpHeader.u1Ver_hdrlen = IP_VERS_AND_HLEN (IP_VERSION_4, 0);
    IpHeader.u1Tos = u1Tos;
    u2TotLen = (UINT2) (i4Size + IP_HDR_LEN);
    IpHeader.u2Totlen = (UINT2) OSIX_HTONS (u2TotLen);
    IpHeader.u2Id = u2Id;
    IpHeader.u2Fl_offs = u1Df;
    IpHeader.u1Ttl = u1Ttl;
    IpHeader.u1Proto = u1Proto;
    VRRP_IPVX_COPY_TO_IPV4 (IpHeader.u4Src, INTERFACEIPADDR (i4OperIndex));
    IpHeader.u4Src = OSIX_HTONL (IpHeader.u4Src);
    IpHeader.u4Dest = OSIX_HTONL (u4Dest);

    IpHeader.u2Cksum = 0;
    IpHeader.u2Cksum
        = OSIX_HTONS (UtlIpCSumLinBuf ((const INT1 *) &IpHeader, IP_HDR_LEN));

    /* Copy the IPv4 Header */
    MEMCPY (pu1TxBuf, (UINT1 *) &IpHeader, IP_HDR_LEN);

    /* Copy the VRRP Info */
    MEMCPY ((pu1TxBuf + IP_HDR_LEN), TRANSMITLNBUF (i4OperIndex), i4Size);

#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = pu1TxBuf;
    Iov.iov_len = (size_t) (i4Size + IP_HDR_LEN);
    MsgHdr.msg_iov = &Iov;
    MsgHdr.msg_iovlen = 1;
#endif

    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = IpHeader.u4Dest;

    MsgHdr.msg_name = (VOID *) &DestAddr;
    MsgHdr.msg_namelen = sizeof (struct sockaddr_in);
    MsgHdr.msg_control = (VOID *) au1Cmsg;
    MsgHdr.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&MsgHdr);
    pCmsgInfo->cmsg_level = SOL_RAW;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo =
        (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&MsgHdr));

#ifndef BSDCOMP_SLI_WANTED
    pIpPktInfo->ipi_ifindex = (INT4) u4Port;
    Mreqn.imr_ifindex = (INT4) u4Port;
#else
    pIpPktInfo->ipi_ifindex = OPERVRPORTNUM (i4OperIndex);
    Mreqn.imr_ifindex = OPERVRPORTNUM (i4OperIndex);
#endif

    pIpPktInfo->ipi_addr.s_addr = DestAddr.sin_addr.s_addr;
    pIpPktInfo->ipi_spec_dst.s_addr = DestAddr.sin_addr.s_addr;

    if (setsockopt (gi4VrrpIpv4Socket, IPPROTO_IP, IP_MULTICAST_IF,
                    (void *) &Mreqn, sizeof (Mreqn)) < 0)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Setting Outgoing Interface for VRRP IPv4 Packet Failed for "
                   "VRID %d\n", OPERVRID (i4OperIndex));

        MemReleaseMemBlock (VRRP_IPV4_PKT_MEMPOOL_ID, pu1TxBuf);
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDPACKETTOIPV4FAIL);
        return VRRP_NOT_OK;
    }

    if (sendmsg (gi4VrrpIpv4Socket, &MsgHdr, 0) < 0)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Sending Message to SLI for IPv4 Packet Failed for VRID %d\n",
                   OPERVRID (i4OperIndex));

        MemReleaseMemBlock (VRRP_IPV4_PKT_MEMPOOL_ID, pu1TxBuf);
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDPACKETTOIPV4FAIL);
        return VRRP_NOT_OK;
    }

#ifndef BSDCOMP_SLI_WANTED
    if (sendto (gi4VrrpIpv4Socket, pu1TxBuf, (i4Size + IP_HDR_LEN), 0,
                (struct sockaddr *) &DestAddr, sizeof (struct sockaddr_in)) < 0)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Sending Packet to SLI for VRRP IPv4 Packet failed for "
                   "VRID %d\n", OPERVRID (i4OperIndex));

        MemReleaseMemBlock (VRRP_IPV4_PKT_MEMPOOL_ID, pu1TxBuf);
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDPACKETTOIPV4FAIL);
        return VRRP_NOT_OK;
    }
#endif

    MemReleaseMemBlock (VRRP_IPV4_PKT_MEMPOOL_ID, pu1TxBuf);

    VRRP_DBG1 (VRRP_TRC_PKT,
               "VRRP Packet Sent to Ipv4 for VRID %d\n",
               OPERVRID (i4OperIndex));

    return (VRRP_OK);
}

/*******************************************************************************
 * Function Name   : VrrpSendPacketToIpv6
 * Description     : Function to Send an IPv6 Packet.
 * Global Varibles : None.
 * Inputs          : i4Size.
 *                   i4OperIndex.
 * Output          : None.
 * Returns         : VRRP_NOT_OK.
 *                   VRRP_OK.
 ******************************************************************************/
EXPORT INT1
VrrpSendPacketToIpv6 (INT4 i4Size, INT4 i4OperIndex)
{
    UINT4               u4Port = 0;
    UINT1               au1Cmsg[(sizeof (struct cmsghdr) * 2) +
                                sizeof (struct in6_pktinfo)];
    struct sockaddr_in6 DestAddr;
    struct msghdr       MsgHdr;
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1              *pu1TxBuf = NULL;
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif

    MEMSET ((UINT1 *) &DestAddr, 0, sizeof (DestAddr));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET ((UINT1 *) &MsgHdr, 0, sizeof (MsgHdr));

    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    if (OPERVRPORTNUM (i4OperIndex) != 0)
    {
        u4Port = OPERVRPORTNUM (i4OperIndex);
    }

    if (gi4VrrpIpv6Socket == VRRP_NOT_OK)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Invalid socket to send VRRP Advertisement message for "
                   "VRID %d\r\n", OPERVRID (i4OperIndex));
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDPACKETTOIPV6FAIL);
        return VRRP_NOT_OK;
    }

    pu1TxBuf = TRANSMITLNBUF (i4OperIndex);

#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = pu1TxBuf;
    Iov.iov_len = (size_t) i4Size;
    MsgHdr.msg_iov = &Iov;
    MsgHdr.msg_iovlen = 1;
#endif

    DestAddr.sin6_family = AF_INET6;
    MEMCPY (DestAddr.sin6_addr.s6_addr, gVrrpV6DestMCastAddr.au1Addr,
            IPVX_IPV6_ADDR_LEN);

    MsgHdr.msg_name = (VOID *) &DestAddr;
    MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    MsgHdr.msg_control = (VOID *) au1Cmsg;
    MsgHdr.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&MsgHdr);
    pCmsgInfo->cmsg_level = SOL_RAW;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&MsgHdr));

    pIpPktInfo->ipi6_ifindex = (INT4) u4Port;
    MEMCPY (pIpPktInfo->ipi6_addr.s6_addr,
            INTERFACEIPADDR (i4OperIndex).au1Addr, IPVX_IPV6_ADDR_LEN);

    if (setsockopt (gi4VrrpIpv6Socket, IPPROTO_IPV6, IPV6_MULTICAST_IF,
                    (void *) &u4Port, sizeof (UINT4)) < 0)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Setting Outgoing Interface for VRRP IPv6 Packet Failed for "
                   "VRID %d\n", OPERVRID (i4OperIndex));
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDPACKETTOIPV6FAIL);
        return VRRP_NOT_OK;
    }

    if (sendmsg (gi4VrrpIpv6Socket, &MsgHdr, 0) < 0)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Sending Message to SLI for IPv6 Packet Failed for VRID %d\n",
                   OPERVRID (i4OperIndex));
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDPACKETTOIPV6FAIL);
        return VRRP_NOT_OK;
    }

#ifndef BSDCOMP_SLI_WANTED
    if (sendto (gi4VrrpIpv6Socket, pu1TxBuf, i4Size, 0,
                (struct sockaddr *) &DestAddr, sizeof (struct sockaddr_in6))
        < 0)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Sending Packet to SLI for VRRP IPv6 Packet failed for "
                   "VRID %d\n", OPERVRID (i4OperIndex));
        PktProcessUpdateStatistics (i4OperIndex, VRRPSENDPACKETTOIPV6FAIL);
        return VRRP_NOT_OK;
    }
#endif

    VRRP_DBG1 (VRRP_TRC_PKT,
               "VRRP Packet Sent to Ipv6 for VRID %d\n",
               OPERVRID (i4OperIndex));

    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : VrrpDequePacket.
 * Description     : Function to Dequeue a packet
 * Global Varibles :
 * Inputs          : None.
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
EXPORT INT1
VrrpDequePacket ()
{
    tVrrpIpMsg         *pVrrpMsg = NULL;

    while (OsixQueRecv (gIpVrrpQId, (UINT1 *) &pVrrpMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pVrrpMsg == NULL)
        {
            return (VRRP_NOT_OK);
        }

        switch (pVrrpMsg->u4Cmd)
        {
            case VRRP_LINKDOWN_PKT:
                VrrpLinkDown (pVrrpMsg->u4IfIndex, pVrrpMsg->i4AddrType);
                break;
            case VRRP_LINKUP_PKT:
                VrrpLinkUp (pVrrpMsg->u4IfIndex, pVrrpMsg->i4AddrType);
                break;
            case VRRP_STATECHG_PKT:
                VrrpIpChange (pVrrpMsg->u4IfIndex, pVrrpMsg->i4AddrType,
                              pVrrpMsg->IpAddr, pVrrpMsg->u4Type);
                break;
            case VRRP_IFACE_DEL_PKT:
                VrrpIfaceDelete (pVrrpMsg->u4IfIndex, pVrrpMsg->i4AddrType);
                break;
            default:
                break;
        }
        MemReleaseMemBlock (VRRP_PKTQ_MEMPOOL_ID, (UINT1 *) pVrrpMsg);
    }
    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : VrrpReceivePacket.
 * Description     : Function to receive a packet
 * Global Varibles :
 * Inputs          : None.
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
EXPORT INT1
VrrpReceivePacket ()
{
    UINT1               u1Ttl = 0;
    INT4                i4DataToRead = 0;
    INT4                i4DataSize = 0;
    INT4                i4AddrLen = 0;

    UINT1               au1Cmsg[(sizeof (struct cmsghdr) * 2)];
    struct sockaddr_in  PeerAddr;
    struct msghdr       MsgHdr;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1              *pu1RxBuf = NULL;
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
    UNUSED_PARAM (i4AddrLen);
#endif

    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET ((UINT1 *) &MsgHdr, 0, sizeof (MsgHdr));

    if (VRRP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return VRRP_OK;
    }

    pu1RxBuf = (UINT1 *) MemAllocMemBlk (VRRP_IPV4_PKT_MEMPOOL_ID);

    if (pu1RxBuf == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for IPv4 Rx Pkt Buffer\r\n");
        return VRRP_OK;
    }

    i4DataToRead =
        (INT4) ((FsVRRPSizingParams[MAX_VRRP_TX_RX_IPV4_PKT_SIZING_ID].
                 u4StructSize));

    MEMSET (pu1RxBuf, 0, i4DataToRead);

#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = pu1RxBuf;
    Iov.iov_len = (size_t) i4DataToRead;
    MsgHdr.msg_iov = &Iov;
    MsgHdr.msg_iovlen = 1;
#endif

    PeerAddr.sin_family = AF_INET;

    MsgHdr.msg_name = (VOID *) &PeerAddr;
    MsgHdr.msg_namelen = sizeof (struct sockaddr_in);
    MsgHdr.msg_control = (VOID *) au1Cmsg;
    MsgHdr.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&MsgHdr);
    pCmsgInfo->cmsg_level = SOL_RAW;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

#ifndef BSDCOMP_SLI_WANTED
    /* Our IP / SLI implementation require recvfrom() to receive packets
     * from RAW Sockets and recvmsg() to receive ancillary data like 
     * interface index in which packet is received. For IPv4, complete
     * IPv4 Header + VRRP Packet is recevied from socket.
     *
     * To compute checksum at VRRP level for version 3, the following
     * information are needed to construct "Pseudo Header" 
     * used for checksum computation.
     *
     * 1. Source IP Address in IPv4 Header -> Received from IPv4 Header.
     * 2. Destination IP Address in IPv4 Header -> Received from IPv4 Header.
     * 3. TTL in IPv6 Header -> Received from recvmsg() -> Received from IPv4
     *    header itself.
     * 4. VRRP Packet size -> Received and calculated recvfrom() output. The
     *    output of recvfrom() contains IP Header size + VRRP Packet size.
     *
     * In addition to this, the interface index in which the packet is received
     * is retrieved from recvmsg().
     */
    while ((i4DataSize =
            recvfrom (gi4VrrpIpv4Socket, pu1RxBuf, i4DataToRead, 0,
                      (struct sockaddr *) &PeerAddr, &i4AddrLen)) > 0)
    {
        /* Get the interface index on which the packet is received */
        recvmsg (gi4VrrpIpv4Socket, &MsgHdr, 0);

        pIpPktInfo = (struct in_pktinfo *) (VOID *)
            CMSG_DATA (CMSG_FIRSTHDR (&MsgHdr));

        VRRP_IPVX_ADDR_CLEAR (gSenderIpAddr);
        gSenderIpAddr.u1Afi = VRRP_IPVX_ADDR_FMLY_IPV4;
        gSenderIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
        MEMCPY (gSenderIpAddr.au1Addr,
                pu1RxBuf + SRC_IP_OFFSET, IPVX_IPV4_ADDR_LEN);

        VRRP_IPVX_ADDR_CLEAR (gReceiverIpAddr);
        gReceiverIpAddr.u1Afi = VRRP_IPVX_ADDR_FMLY_IPV4;
        gReceiverIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
        MEMCPY (gReceiverIpAddr.au1Addr,
                pu1RxBuf + DST_IP_OFFSET, IPVX_IPV4_ADDR_LEN);

        u1Ttl = pu1RxBuf[TTL_OFFSET];

        PktProcessReceivedPacket (pIpPktInfo->ipi_ifindex,
                                  pu1RxBuf + IP_HDR_LEN,
                                  (UINT4) (i4DataSize - IP_HDR_LEN),
                                  VRRP_IPVX_ADDR_FMLY_IPV4, u1Ttl);

        MEMSET (pu1RxBuf, 0, i4DataToRead);
    }
#else /*BSDCOMP_SLI_WANTED */
    /* Standard Linux IP implementation just require recvmsg() to receive 
     * packets from RAW Sockets and ancillary data like interface index in
     * which packet is received. For IPv4, complete IPv4 Header + VRRP Packet
     * is recevied from socket.
     *
     * To compute checksum at VRRP level for version 3, the following
     * information are needed to construct "Pseudo Header" 
     * used for checksum computation.
     *
     * 1. Source IP Address in IPv4 Header -> Received from IPv4 Header.
     * 2. Destination IP Address in IPv4 Header -> Received from IPv4 Header.
     * 3. TTL in IPv6 Header -> Received from recvmsg() -> Received from IPv4
     *    header itself.
     * 4. VRRP Packet size -> Received and calculated recvmsg() output. The
     *    output of recvfrom() contains IP Header size + VRRP Packet size.
     *
     * In addition to this, the interface index in which the packet is received
     * is retrieved from recvmsg().
     */
    while ((i4DataSize = recvmsg (gi4VrrpIpv4Socket, &MsgHdr, 0)) > 0)
    {
        pIpPktInfo = (struct in_pktinfo *) (VOID *)
            CMSG_DATA (CMSG_FIRSTHDR (&MsgHdr));

        VRRP_IPVX_ADDR_CLEAR (gSenderIpAddr);
        gSenderIpAddr.u1Afi = VRRP_IPVX_ADDR_FMLY_IPV4;
        gSenderIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
        MEMCPY (gSenderIpAddr.au1Addr,
                pu1RxBuf + SRC_IP_OFFSET, IPVX_IPV4_ADDR_LEN);

        VRRP_IPVX_ADDR_CLEAR (gReceiverIpAddr);
        gReceiverIpAddr.u1Afi = VRRP_IPVX_ADDR_FMLY_IPV4;
        gReceiverIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
        MEMCPY (gReceiverIpAddr.au1Addr,
                pu1RxBuf + DST_IP_OFFSET, IPVX_IPV4_ADDR_LEN);

        u1Ttl = pu1RxBuf[TTL_OFFSET];

        PktProcessReceivedPacket (pIpPktInfo->ipi_ifindex,
                                  pu1RxBuf + IP_HDR_LEN,
                                  (UINT4) (i4DataSize - IP_HDR_LEN),
                                  VRRP_IPVX_ADDR_FMLY_IPV4, u1Ttl);

        MEMSET (pu1RxBuf, 0, i4DataToRead);
    }
#endif /* BSDCOMP_SLI_WANTED */

    /* Add the Socket descriptor to Select utility for Packet Reception */
    if (SelAddFd (gi4VrrpIpv4Socket, VrrpPktInSocket) != OSIX_SUCCESS)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Failed to initiate next VRRP IPv4 packet reception\n");

        MemReleaseMemBlock (VRRP_IPV4_PKT_MEMPOOL_ID, pu1RxBuf);

        return VRRP_NOT_OK;
    }

    MemReleaseMemBlock (VRRP_IPV4_PKT_MEMPOOL_ID, pu1RxBuf);

    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : VrrpReceiveIpv6Packet
 * Description     : Function to receive a VRRP IPV6 packet
 * Global Varibles :
 * Inputs          : None.
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
EXPORT INT1
VrrpReceiveIpv6Packet ()
{
    UINT1               u1Ttl = 0;
    INT4                i4DataToRead = 0;
    INT4                i4DataSize = 0;
    INT4                i4AddrLen = 0;

    UINT1               au1Cmsg[(sizeof (struct cmsghdr) * 2) +
                                sizeof (struct in6_pktinfo)];
    struct sockaddr_in6 PeerAddr;
    struct msghdr       MsgHdr;
    struct in6_pktinfo *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo = NULL;
    UINT1              *pu1RxBuf = NULL;
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
    UNUSED_PARAM (i4AddrLen);
#endif

    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in6));
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    MEMSET ((UINT1 *) &MsgHdr, 0, sizeof (MsgHdr));

    if (VRRP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return VRRP_OK;
    }

    pu1RxBuf = (UINT1 *) MemAllocMemBlk (VRRP_IPV6_PKT_MEMPOOL_ID);

    if (pu1RxBuf == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for IPv6 Rx Pkt Buffer\r\n");
        return VRRP_OK;
    }

    i4DataToRead =
        (INT4) ((FsVRRPSizingParams[MAX_VRRP_TX_RX_IPV6_PKT_SIZING_ID].
                 u4StructSize));

    MEMSET (pu1RxBuf, 0, i4DataToRead);

#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = pu1RxBuf;
    Iov.iov_len = (size_t) i4DataToRead;
    MsgHdr.msg_iov = &Iov;
    MsgHdr.msg_iovlen = 1;
#endif

    PeerAddr.sin6_family = AF_INET6;

    MsgHdr.msg_name = (VOID *) &PeerAddr;
    MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    MsgHdr.msg_control = (VOID *) au1Cmsg;
    MsgHdr.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&MsgHdr);
    pCmsgInfo->cmsg_level = SOL_RAW;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

#ifndef BSDCOMP_SLI_WANTED
    /* Our IP / SLI implementation require recvfrom() to receive packets
     * from RAW Sockets and recvmsg() to receive ancillary data like 
     * (Destination IP Address and Hop Limit) present in IPv6 Header and
     * Interface Index in which packet is received. Only VRRP Packet
     * is received from IPv6 Socket for VRRP.
     *
     * To compute checksum at VRRP level, the following information are
     * needed to construct "Pseudo Header" used for checksum computation.
     *
     * 1. Source Ip Address in IPv6 Header -> Received from PeerAddr field
     *    of recvfrom().
     * 2. Destination IP Address in IPv6 Header -> Received from recvmsg().
     * 3. Hop Limit in IPv6 Header -> Received from recvmsg().
     * 4. VRRP Packet size -> Received from recvfrom() output.
     *
     * In addition to this Interface Index in which packet is received is
     * retrieved from recvmsg().
     */
    while ((i4DataSize =
            recvfrom (gi4VrrpIpv6Socket, pu1RxBuf, i4DataToRead, 0,
                      (struct sockaddr *) &PeerAddr, &i4AddrLen)) > 0)
    {
        /* Get the interface index on which the packet is received */
        recvmsg (gi4VrrpIpv6Socket, &MsgHdr, 0);

        pIpPktInfo = (struct in6_pktinfo *) (VOID *)
            CMSG_DATA (CMSG_FIRSTHDR (&MsgHdr));

        /* Save Sender Address (Source Address) and Receiver Address
         * (Destination address from IPv6 Header. This will be used for
         * Checksum computation at VRRP level. */
        VRRP_IPVX_ADDR_CLEAR (gSenderIpAddr);
        gSenderIpAddr.u1Afi = VRRP_IPVX_ADDR_FMLY_IPV6;
        gSenderIpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
        MEMCPY (gSenderIpAddr.au1Addr, PeerAddr.sin6_addr.s6_addr,
                IPVX_IPV6_ADDR_LEN);

        VRRP_IPVX_ADDR_CLEAR (gReceiverIpAddr);
        gReceiverIpAddr.u1Afi = VRRP_IPVX_ADDR_FMLY_IPV6;
        gReceiverIpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
        MEMCPY (gReceiverIpAddr.au1Addr, pIpPktInfo->ipi6_addr.s6_addr,
                IPVX_IPV6_ADDR_LEN);

        u1Ttl = (UINT1) pIpPktInfo->in6_hoplimit;

        PktProcessReceivedPacket (pIpPktInfo->ipi6_ifindex,
                                  pu1RxBuf, (UINT4) i4DataSize,
                                  VRRP_IPVX_ADDR_FMLY_IPV6, u1Ttl);

        MEMSET (pu1RxBuf, 0, i4DataToRead);
    }
#else /*BSDCOMP_SLI_WANTED */
    /* Standard Linux IP implementation just require recvmsg() to receive
     * packets from RAW Sockets and ancillary data like 
     * (Destination IP Address and Hop Limit) present in IPv6 Header and
     * Interface Index in which the packet is received. Only VRRP Packet is
     * received from IPv6 socket for VRRP.
     *
     * To compute checksum at VRRP level, the following information are
     * needed to construct "Pseudo Header" used for checksum computation.
     * All of them are received from recvmsg().
     *
     * 1. Source Ip Address in IPv6 Header.
     * 2. Destination IP Address in IPv6 Header.
     * 3. Hop Limit in IPv6 Header.
     * 4. VRRP Packet size.
     *
     * In addition to this, Interface Index from which packet is received is
     * also retrieved from recvmsg().
     */
    while ((i4DataSize = recvmsg (gi4VrrpIpv6Socket, &MsgHdr, 0)) > 0)
    {
        pIpPktInfo = (struct in6_pktinfo *) (VOID *)
            CMSG_DATA (CMSG_FIRSTHDR (&MsgHdr));

        /* Save Sender Address (Source Address) and Receiver Address
         * (Destination address from IPv6 Header. This will be used for
         * Checksum computation at VRRP level. */
        VRRP_IPVX_ADDR_CLEAR (gSenderIpAddr);
        gSenderIpAddr.u1Afi = VRRP_IPVX_ADDR_FMLY_IPV6;
        gSenderIpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
        MEMCPY (gSenderIpAddr.au1Addr, PeerAddr.sin6_addr.s6_addr,
                IPVX_IPV6_ADDR_LEN);

        VRRP_IPVX_ADDR_CLEAR (gReceiverIpAddr);
        gReceiverIpAddr.u1Afi = VRRP_IPVX_ADDR_FMLY_IPV6;
        gReceiverIpAddr.u1AddrLen = IPVX_IPV6_ADDR_LEN;
        MEMCPY (gReceiverIpAddr.au1Addr, pIpPktInfo->ipi6_addr.s6_addr,
                IPVX_IPV6_ADDR_LEN);

        /* As per RFC 3542 Section 6.2,
         * In the cmsghdr structure containing this ancillary data, the
         * cmsg_level member will be IPPROTO_IPV6, the cmsg_type member will be
         * IPV6_HOPLIMIT, and the first byte of cmsg_data[] will be the first
         * byte of the integer hop limit. au1Cmsg[12] points to the data */
        u1Ttl = (UINT1) au1Cmsg[(sizeof (struct cmsghdr))];

        PktProcessReceivedPacket (pIpPktInfo->ipi6_ifindex,
                                  pu1RxBuf, (UINT4) i4DataSize,
                                  VRRP_IPVX_ADDR_FMLY_IPV6, u1Ttl);

        MEMSET (pu1RxBuf, 0, i4DataToRead);
    }
#endif /* BSDCOMP_SLI_WANTED */

    /* Add the Socket descriptor to Select utility for Packet Reception */
    if (SelAddFd (gi4VrrpIpv6Socket, VrrpPktInIpv6Socket) != OSIX_SUCCESS)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Failed to initiate next VRRP IPv6 packet reception\n");

        MemReleaseMemBlock (VRRP_IPV6_PKT_MEMPOOL_ID, pu1RxBuf);

        return VRRP_NOT_OK;
    }

    MemReleaseMemBlock (VRRP_IPV6_PKT_MEMPOOL_ID, pu1RxBuf);

    return VRRP_OK;
}

/******************************************************************************* 
 * Function Name   : EthDeRegisterMacAddr
 *
 * Description     : Function to deregister MAC address with Ethernet
 * Global Varibles :
 * Inputs          : MasterIpAddress      - Master IP Address
 *                   pu1VirtualMacAddr    - Virtual MAC Address
 *                   i4OperIndex          - Oper Index
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
EthDeRegisterMacAddr (tVrrpIPvXAddr MasterIpAddress, UINT1 *pu1VirtualMacAddr,
                      INT4 i4OperIndex)
{
    tVrrpNwIntf         VrrpNwIntf;
    UINT4               u4Port = 0;
    INT4                i4RetVal = VRRP_NOT_OK;
    UINT2               u2VlanId = 0;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);

    MEMSET (&VrrpNwIntf, 0, sizeof (tVrrpNwIntf));
    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);

    if (VRRP_IS_NP_PROGRAMMING_ALLOWED () != VRRP_OK)
    {
        VRRP_DBG3 (VRRP_TRC_EVENTS,
                   "NP Programming not required for IfIndex %d VRID %d "
                   "AddrType %s\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        return VRRP_OK;
    }

    if (OPERNWENTRYSTATUS (i4OperIndex) == FALSE)
    {
        VRRP_DBG3 (VRRP_TRC_EVENTS,
                   "Entry IfIndex %d VRID %d AddrType %s already not present "
                   "Network Layer\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);

        return VRRP_OK;
    }

    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        VRRP_DBG3 (VRRP_TRC_ALL_FAILURES,
                   "Fetching IP Port from If Index failed during "
                   "Network Layer deletion for "
                   "IfIndex %d VRID %d Addr Type %s\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);

        return VRRP_NOT_OK;
    }

    VrrpNwIntf.u4IfIndex = (UINT4) OPERIFINDEX (i4OperIndex);
    VrrpNwIntf.u4Port = u4Port;
    VrrpNwIntf.u4VrId = (UINT4) OPERVRID (i4OperIndex);
    VrrpNwIntf.u1AddrType = OPERADDRTYPE (i4OperIndex);

    VRRP_IPVX_COPY (VrrpNwIntf.IntfAddr, INTERFACEIPADDR (i4OperIndex));
    VRRP_IPVX_COPY (VrrpNwIntf.IpvXAddr, MasterIpAddress);
    VRRP_IPVX_COPY (VrrpNwIntf.VirtualIp, OPERPRIMARYIPADDR (i4OperIndex));
    VRRP_MEMCPY (VrrpNwIntf.au1MacAddr, pu1VirtualMacAddr, MAC_ADDR_LEN);
    VrrpNwIntf.u1Action = VRRP_NW_INTF_DELETE;
    VrrpNwIntf.b1IsIpvXOwner = OPERISOWNER (i4OperIndex);
    VrrpNwIntf.b1IsOwnerPresent =
        VrrpIsOwnerPresentInThisIfIndex (OPERIFINDEX (i4OperIndex),
                                         OPERVRID (i4OperIndex),
                                         OPERADDRTYPE (i4OperIndex));

    VRRPOPERHWSTATUS (i4OperIndex) = NP_NOT_UPDATED;

    if (OPERACCEPTMODE (i4OperIndex) == VRRP_DISABLE)
    {
        VrrpNwIntf.u1AcceptMode = FALSE;
    }
    else
    {
        VrrpNwIntf.u1AcceptMode = TRUE;
    }

    VrrpRedDbUtilAddTblNode (&gVrrpDynInfoList, &OPERDBNODE (i4OperIndex));
    VrrpRedSyncDynInfo ();

    VrrpNwIntf.u4LnxIpPortNum = OPERVRPORTNUM (i4OperIndex);

    if (CfaGetVlanId ((UINT4) OPERIFINDEX (i4OperIndex), &u2VlanId)
        == CFA_SUCCESS)
    {
        VrrpNwIntf.u2VlanId = u2VlanId;

        i4RetVal = VrrpNetIpDeleteWr (i4OperIndex, &VrrpNwIntf);
    }
    else
    {
        i4RetVal = VrrpNetIpDeleteWr (i4OperIndex, &VrrpNwIntf);
    }

    if (i4RetVal == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    if ((OPERVRPORTNUM (i4OperIndex) != 0) &&
        (VrrpIpifLeaveMcastGroup (i4OperIndex) == VRRP_NOT_OK))
    {
        return VRRP_NOT_OK;
    }

    OPERVRPORTNUM (i4OperIndex) = 0;

    VRRPOPERHWSTATUS (i4OperIndex) = NP_UPDATED;
    VrrpRedDbUtilAddTblNode (&gVrrpDynInfoList, &OPERDBNODE (i4OperIndex));
    VrrpRedSyncDynInfo ();

    if (VrrpDeleteSecIpOrLeaveSoliMCast (i4OperIndex, gVrrpZeroIpAddr,
                                         FALSE) == VRRP_NOT_OK)
    {
        VRRP_DBG3 (VRRP_TRC_ALL_FAILURES,
                   "Secondary IP deletion or leaving Mcast Group failed for "
                   "IfIndex %d VRID %d Addr Type %s\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        return VRRP_NOT_OK;
    }

    OPERNWENTRYSTATUS (i4OperIndex) = FALSE;

    VRRP_DBG3 (VRRP_TRC_EVENTS,
               "Secondary IP deleted or Left Mcast Group for "
               "IfIndex %d VRID %d Addr Type %s\r\n",
               OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex), ac1AddrType);
    return (VRRP_OK);
}

/******************************************************************************* 
 * Function Name   : EthRegisterMacAddr
 *
 * Description     : Function to register MAC address with Ethernet
 * Global Varibles :
 * Inputs          : MasterIpAddress      - Master IP Address
 *                   pu1VirtualMacAddr    - Virtual Mac
 *                   i4OperIndex          - Oper Index
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
EthRegisterMacAddr (tVrrpIPvXAddr MasterIpAddress, UINT1 *pu1VirtualMacAddr,
                    INT4 i4OperIndex)
{
    tVrrpNwIntf         VrrpNwIntf;
    UINT4               u4Port = 0;
    INT4                i4RetVal = VRRP_NOT_OK;
    UINT2               u2VlanId = 0;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);
    MEMSET (&VrrpNwIntf, 0, sizeof (tVrrpNwIntf));
    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);

    if (VRRP_IS_NP_PROGRAMMING_ALLOWED () != VRRP_OK)
    {
        VRRP_DBG3 (VRRP_TRC_EVENTS,
                   "NP Programming not required for IfIndex %d VRID %d "
                   "AddrType %s\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        return VRRP_OK;
    }

    if (OPERNWENTRYSTATUS (i4OperIndex) == TRUE)
    {
        VRRP_DBG3 (VRRP_TRC_EVENTS,
                   "Entry IfIndex %d VRID %d AddrType %s already present "
                   "Network Layer\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        return VRRP_OK;
    }

    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        VRRP_DBG3 (VRRP_TRC_ALL_FAILURES,
                   "Fetching IP Port from If Index failed during "
                   "Network Layer Addition for "
                   "IfIndex %d VRID %d Addr Type %s\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        return VRRP_NOT_OK;
    }

    VrrpNwIntf.u4IfIndex = (UINT4) OPERIFINDEX (i4OperIndex);
    VrrpNwIntf.u4Port = u4Port;
    VrrpNwIntf.u4VrId = (UINT4) OPERVRID (i4OperIndex);
    VrrpNwIntf.u1AddrType = OPERADDRTYPE (i4OperIndex);

    VRRP_IPVX_COPY (VrrpNwIntf.IntfAddr, INTERFACEIPADDR (i4OperIndex));
    VRRP_IPVX_COPY (VrrpNwIntf.IpvXAddr, MasterIpAddress);
    VRRP_IPVX_COPY (VrrpNwIntf.VirtualIp, OPERPRIMARYIPADDR (i4OperIndex));
    VRRP_MEMCPY (VrrpNwIntf.au1MacAddr, pu1VirtualMacAddr, MAC_ADDR_LEN);
    VrrpNwIntf.u1Action = VRRP_NW_INTF_CREATE;
    VrrpNwIntf.b1IsIpvXOwner = OPERISOWNER (i4OperIndex);
    VrrpNwIntf.b1IsOwnerPresent =
        VrrpIsOwnerPresentInThisIfIndex (OPERIFINDEX (i4OperIndex),
                                         OPERVRID (i4OperIndex),
                                         OPERADDRTYPE (i4OperIndex));

    if (OPERACCEPTMODE (i4OperIndex) == VRRP_DISABLE)
    {
        VrrpNwIntf.u1AcceptMode = FALSE;
    }
    else
    {
        VrrpNwIntf.u1AcceptMode = TRUE;
    }

    VRRPOPERHWSTATUS (i4OperIndex) = NP_NOT_UPDATED;
    VrrpRedDbUtilAddTblNode (&gVrrpDynInfoList, &OPERDBNODE (i4OperIndex));
    VrrpRedSyncDynInfo ();

    if (CfaGetVlanId ((UINT4) OPERIFINDEX (i4OperIndex), &u2VlanId)
        == CFA_SUCCESS)
    {
        VrrpNwIntf.u2VlanId = u2VlanId;

        VcmGetIfMapVcId ((UINT4) OPERIFINDEX (i4OperIndex),
                         &VrrpNwIntf.u4VcNum);
        i4RetVal = VrrpNetIpCreateWr (i4OperIndex, &VrrpNwIntf);

        /*Delete the L2 Entry for VRRP mac from VLAN FDB table, if any */
        /* This entry might have learned from the previous VRRP master */

        /*Context ID is default context since mutiple instance is not 
         *supported for VRRP*/
        VlanRemoveFdbEntry (L2IWF_DEFAULT_CONTEXT, u2VlanId, pu1VirtualMacAddr);
    }
    else
    {
        VcmGetIfMapVcId ((UINT4) OPERIFINDEX (i4OperIndex),
                         &VrrpNwIntf.u4VcNum);
        i4RetVal = VrrpNetIpCreateWr (i4OperIndex, &VrrpNwIntf);
    }

    if (i4RetVal == VRRP_NOT_OK)
    {
        PktProcessUpdateStatistics (i4OperIndex, VRRPETHREGISTERMACADDRFAIL);
        return VRRP_NOT_OK;
    }

    OPERVRPORTNUM (i4OperIndex) = VrrpNwIntf.u4LnxIpPortNum;

    if ((OPERVRPORTNUM (i4OperIndex) != 0) &&
        (VrrpIpifJoinMcastGroup (i4OperIndex) == VRRP_NOT_OK))
    {
        return VRRP_NOT_OK;
    }

    VRRP_DBG3 (VRRP_TRC_EVENTS,
               "Entry IfIndex %d VRID %d Addr Type %s added in "
               "network layer\r\n",
               OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex), ac1AddrType);
    VRRPOPERHWSTATUS (i4OperIndex) = NP_UPDATED;
    VrrpRedDbUtilAddTblNode (&gVrrpDynInfoList, &OPERDBNODE (i4OperIndex));
    VrrpRedSyncDynInfo ();

    if (VrrpCreateSecIpOrJoinSoliMCast (i4OperIndex, gVrrpZeroIpAddr,
                                        TRUE) == VRRP_NOT_OK)
    {
        VRRP_DBG3 (VRRP_TRC_ALL_FAILURES,
                   "Secondary IP Addition or Joining Mcast Group failed for "
                   "IfIndex %d VRID %d Addr Type %s\r\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        return VRRP_NOT_OK;
    }

    OPERNWENTRYSTATUS (i4OperIndex) = TRUE;

    VRRP_DBG3 (VRRP_TRC_EVENTS,
               "Secondary IP added or Joined Mcast Group added for "
               "IfIndex %d VRID %d Addr Type %s\r\n",
               OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex), ac1AddrType);

    return (VRRP_OK);
}

/******************************************************************************* 
 * Function Name   : VrrpNetIpCreateWr
 *
 * Description     : Wrapper function to call IPv4 or IPv6 Create API.
 * Global Varibles :
 * Inputs          : i4OperIndex          - Oper Index
 *                   pVrrpNwIntf          - Pointer to Vrrp Nw Interface
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT4
VrrpNetIpCreateWr (INT4 i4OperIndex, tVrrpNwIntf * pVrrpNwIntf)
{
    tIp6Addr            Ip6Addr;
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;
    UINT4               u4IpAddr = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    if (OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4)
    {

        UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                             pTempIpAddrEntry, tIpAddrTable *)
        {
            VRRP_IPVX_COPY_TO_IPV4 (u4IpAddr, pIpAddrEntry->AssoIpAddr);
#ifdef ARP_WANTED
            ArpModifyWithIndex ((UINT4) OPERIFINDEX (i4OperIndex), u4IpAddr,
                                ARP_INVALID);
#endif
        }

        if (NetIpv4CreateVrrpInterface (pVrrpNwIntf) == NETIPV4_FAILURE)
        {
            return VRRP_NOT_OK;
        }
    }
    else
    {
#ifdef IP6_WANTED
        UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                             pTempIpAddrEntry, tIpAddrTable *)
        {
            MEMCPY (&Ip6Addr, pIpAddrEntry->AssoIpAddr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);

            NetIpv6UpdateNDCache (pVrrpNwIntf->u4Port, &Ip6Addr,
                                  &OPERMACADDR (i4OperIndex),
                                  ND6_CACHE_ENTRY_INCOMPLETE, 0);

        }
        if (NetIpv6CreateVrrpInterface (pVrrpNwIntf) == NETIPV6_FAILURE)
        {
            return VRRP_NOT_OK;
        }
#endif
    }

    return VRRP_OK;
}

/******************************************************************************* 
 * Function Name   : VrrpNetIpDeleteWr
 *
 * Description     : Wrapper function to call IPv4 or IPv6 Delete API.
 * Global Varibles :
 * Inputs          : i4OperIndex          - Oper Index
 *                   pVrrpNwIntf          - Pointer to Vrrp Nw Interface
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT4
VrrpNetIpDeleteWr (INT4 i4OperIndex, tVrrpNwIntf * pVrrpNwIntf)
{
    if (OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        if (NetIpv4DeleteVrrpInterface (pVrrpNwIntf) == NETIPV4_FAILURE)
        {
            PktProcessUpdateStatistics (i4OperIndex, VRRPNETIP4DELINTFFAIL);
            return VRRP_NOT_OK;
        }
    }
    else
    {
#ifdef IP6_WANTED
        if (NetIpv6DeleteVrrpInterface (pVrrpNwIntf) == NETIPV6_FAILURE)
        {
            PktProcessUpdateStatistics (i4OperIndex, VRRPNETIP4DELINTFFAIL);
            return VRRP_NOT_OK;
        }
#endif
    }

    return VRRP_OK;
}

/******************************************************************************* 
 * Function Name   : VrrpNetIpGetWr
 *
 * Description     : Wrapper function to call IPv4 or IPv6 Delete API.
 * Global Varibles :
 * Inputs          : pVrrpNwInIntf        - Pointer to Vrrp Nw Interface
 * Output          : pVrrpNwOutIntf       - Pointer to Vrrp Nw Out Interface
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT4
VrrpNetIpGetWr (tVrrpNwIntf * pVrrpNwInIntf, tVrrpNwIntf * pVrrpNwOutIntf)
{
    if (pVrrpNwInIntf->u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        if (NetIpv4GetVrrpInterface (pVrrpNwInIntf, pVrrpNwOutIntf)
            == NETIPV4_FAILURE)
        {
            return VRRP_NOT_OK;
        }
    }
    else
    {
#ifdef IP6_WANTED
        if (NetIpv6GetVrrpInterface (pVrrpNwInIntf, pVrrpNwOutIntf)
            == NETIPV6_FAILURE)
        {
            return VRRP_NOT_OK;
        }
#endif
    }

    return VRRP_OK;
}

/******************************************************************************* 
 * Function Name   : VrrpGetPortFromOperIndex
 *
 * Description     : This function gets IP Port Number from CFA If Index
 *                   present in Oper table based on address type
 * Global Varibles :
 * Inputs          : i4OperIndex          - Oper Index
 * Output          : u4Port               - IP Port Number
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT4
VrrpGetPortFromOperIndex (INT4 i4OperIndex, UINT4 *pu4Port)
{
    if (OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        if (NetIpv4GetPortFromIfIndex ((UINT4) OPERIFINDEX (i4OperIndex),
                                       pu4Port) == NETIPV4_FAILURE)
        {
            PktProcessUpdateStatistics (i4OperIndex,
                                        VRRPGETIPV4PORTFROMOPERINDEXFAIL);
            return VRRP_NOT_OK;
        }
    }
    else
    {
#ifdef IP6_WANTED
        if (NetIpv6GetPortFromCfaIfIndex ((UINT4) OPERIFINDEX (i4OperIndex),
                                          pu4Port) == NETIPV6_FAILURE)
        {

            PktProcessUpdateStatistics (i4OperIndex,
                                        VRRPGETIPV4PORTFROMOPERINDEXFAIL);
            return VRRP_NOT_OK;
        }
#endif
    }

    return VRRP_OK;
}

/******************************************************************************* 
 * Function Name   : VrrpGetCfaIfIndexFromVrPortNum
 *
 * Description     : This function gets CFA If Index from IP Port Number
 *                   stored in Virtual router entries
 * Global Varibles :
 * Inputs          : u4Port        - IP Port Number
 * Output          : u4IfIndex     - CFA If Index
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT4
VrrpGetCfaIfIndexFromVrPortNum (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    INT4                i4count = 0;

    for (i4count = 0; i4count < (INT4) gu4VrrpMaxOperEntries; i4count++)
    {
        if (OPERVRPORTNUM (i4count) == u4Port)
        {
            *pu4IfIndex = OPERIFINDEX (i4count);
            return VRRP_OK;
        }
    }

    return VRRP_NOT_OK;
}

/******************************************************************************* 
 * Function Name   : VrrpGetCfaIfIndexFromPort
 *
 * Description     : This function gets CFA If Index from IP Port Number
 *                   based on address type
 * Global Varibles :
 * Inputs          : u4Port        - IP Port Number
 *                   i4AddrType    - Address Type Ipv4 or IPv6
 * Output          : u4IfIndex     - CFA If Index
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT4
VrrpGetCfaIfIndexFromPort (UINT4 u4Port, INT4 i4AddrType, UINT4 *pu4IfIndex)
{
    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        if ((NetIpv4GetCfaIfIndexFromPort (u4Port, pu4IfIndex)
             == NETIPV4_FAILURE) &&
            (VrrpGetCfaIfIndexFromVrPortNum (u4Port, pu4IfIndex)
             == VRRP_NOT_OK))
        {
            return VRRP_NOT_OK;
        }
    }
#ifdef IP6_WANTED
    else
    {
        if ((NetIpv6GetCfaIfIndexFromPort (u4Port, pu4IfIndex)
             == NETIPV6_FAILURE) &&
            (VrrpGetCfaIfIndexFromVrPortNum (u4Port, pu4IfIndex)
             == VRRP_NOT_OK))
        {
            return VRRP_NOT_OK;
        }
    }
#endif
    return VRRP_OK;
}

/******************************************************************************* 
 * Function Name   : VrrpIsOwnerPresentInThisIfIndex
 *
 * Description     : This function checks if owner is present in this If Index.
 * Global Varibles :
 * Inputs          : i4IfIndex     - CFA If Index
 *                   i4VrId        - VR Id
 *                   u1AddrType    - u1AddrType
 * Output          : None
 * Returns         : TRUE or FALSE
 ******************************************************************************/
BOOL1
VrrpIsOwnerPresentInThisIfIndex (INT4 i4IfIndex, INT4 i4VrId, UINT1 u1AddrType)
{
    INT4                i4count = 0;

    for (i4count = 0; i4count < (INT4) gu4VrrpMaxOperEntries; i4count++)
    {
        if ((OPERIFINDEX (i4count) == i4IfIndex) &&
            (OPERVRID (i4count) != i4VrId) &&
            (OPERADDRTYPE (i4count) == u1AddrType) &&
            (OPERISOWNER (i4count) == TRUE))
        {
            return TRUE;
        }
    }

    return FALSE;
}

/******************************************************************************* 
 * Function Name   : EthGetIfOperStatus
 * Description     : Function to the ethernet interface Oper status
 * Global Varibles :
 * Inputs          : i4IfIndex  - Interface Index
 *                   u1AddrType - Address Type
 *
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
EthGetIfOperStatus (INT4 i4IfIndex, UINT1 u1AddrType)
{
    tNetIpv4IfInfo      Ipv4Info;
    tNetIpv6IfInfo      Ipv6Info;
    UINT4               u4Port = 0;
    INT1                i1Status = VRRP_NOT_OK;

    MEMSET (&Ipv4Info, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&Ipv6Info, 0, sizeof (tNetIpv6IfInfo));

    if (u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port)
            == NETIPV4_FAILURE)
        {
            return (VRRP_NOT_OK);
        }

        if (NetIpv4GetIfInfo (u4Port, &Ipv4Info) == NETIPV4_FAILURE)
        {
            return (VRRP_NOT_OK);
        }

        i1Status = (INT1) Ipv4Info.u4Oper;
    }
    else
    {
#ifdef IP6_WANTED
        if (NetIpv6GetIfInfo ((UINT4) i4IfIndex, &Ipv6Info) == NETIPV6_FAILURE)
        {
            return (VRRP_NOT_OK);
        }
#endif

        i1Status = (INT1) Ipv6Info.u4Oper;
    }

    return (i1Status);
}

/*******************************************************************************
 * Function Name   : VrrpRcvLinkStatus
 * Description     : Function to receive the VRRP link status from IPv4
 * Global Varibles :
 * Inputs          : u1PortNo
 *                   u1Bitmap

                    ADMIN_STATE         0x00000002
                    IP_ADDR             0x00000004
                    SUBNET_MASK         0x00000008
                    LOCAL_BCAST_ADDR    0x00000010
                    IFACE_DELETED       0x00000020
                    OPER_STATE          0x00000001
 * Output          : None.
 * Returns         : None
 ******************************************************************************/
VOID
VrrpRcvLinkStatus (tNetIpv4IfInfo * pIpIfInfo, UINT1 u1Bitmap)
{
    UINT4               u4Event = 0;
    tVrrpIpMsg         *pVrrpMsg = NULL;

    pVrrpMsg = (tVrrpIpMsg *) MemAllocMemBlk (VRRP_PKTQ_MEMPOOL_ID);

    if (pVrrpMsg == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for IP Status " "Notification\r\n");
        return;
    }

    MEMSET (pVrrpMsg, 0, sizeof (tVrrpIpMsg));
    pVrrpMsg->pBuf = NULL;
    pVrrpMsg->i4AddrType = VRRP_IPVX_ADDR_FMLY_IPV4;

    pVrrpMsg->u4IfIndex = pIpIfInfo->u4CfaIfIndex;

    if ((u1Bitmap & OPER_STATE) || (u1Bitmap & IP_ADDR))
    {
        u4Event = VRRP_LINK_STATE_CHG_EVENT;

        if (pIpIfInfo->u4Oper == IPIF_OPER_DISABLE)
        {
            pVrrpMsg->u4Cmd = VRRP_LINKDOWN_PKT;
        }
        else if (pIpIfInfo->u4Oper == IPIF_OPER_ENABLE)
        {
            pVrrpMsg->u4Cmd = VRRP_LINKUP_PKT;
        }
    }

    if (u1Bitmap & IFACE_DELETED)
    {
        u4Event = VRRP_LINK_STATE_CHG_EVENT;
        pVrrpMsg->u4Cmd = VRRP_IFACE_DEL_PKT;
    }

    if (u4Event == 0)
    {
        VRRP_DBG (VRRP_TRC_EVENTS,
                  "Not a valid event to process in VRRP. Message not posted "
                  "to VRRP\n");

        MemReleaseMemBlock (VRRP_PKTQ_MEMPOOL_ID, (UINT1 *) pVrrpMsg);

        return;
    }

    if (OsixQueSend (gIpVrrpQId, (UINT1 *) &pVrrpMsg, OSIX_DEF_MSG_LEN) !=
        OSIX_SUCCESS)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Failed to enqueue IPv4 Link Status Event to VRRP\n");

        MemReleaseMemBlock (VRRP_PKTQ_MEMPOOL_ID, (UINT1 *) pVrrpMsg);

        return;
    }

    OsixEvtSend (gVrrpTaskId, u4Event);

    return;
}

/*******************************************************************************
 * Function Name   : VrrpIp6RcvNotification
 * Description     : Function to receive the VRRP link status from IPv6
 * Global Varibles :
 * Inputs          : pIp6HliParams - Pointer to HliParams
 * Output          : None.
 * Returns         : None
 ******************************************************************************/
VOID
VrrpIp6RcvNotification (tNetIpv6HliParams * pIp6HliParams)
{
    UINT4               u4Event = 0;
    tVrrpIpMsg         *pVrrpMsg = NULL;

    if (pIp6HliParams->u4Command == NETIPV6_ADDRESS_CHANGE)
    {
        if ((IS_ADDR_LINK_SCOPE_MULTI
             (pIp6HliParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr))
            ||
            (IS_ADDR_MULTI
             (pIp6HliParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr)))
        {
            VRRP_DBG (VRRP_TRC_EVENTS,
                      "IPv6 Multicast or Link Local or Link Local Multicast "
                      "Addresses are not handled for address changed\n");

            return;
        }
    }

    pVrrpMsg = (tVrrpIpMsg *) MemAllocMemBlk (VRRP_PKTQ_MEMPOOL_ID);

    if (pVrrpMsg == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for IPv6 Status Notification\r\n");
        return;
    }

    MEMSET (pVrrpMsg, 0, sizeof (tVrrpIpMsg));
    pVrrpMsg->pBuf = NULL;
    pVrrpMsg->i4AddrType = VRRP_IPVX_ADDR_FMLY_IPV6;

    if (pIp6HliParams->u4Command == NETIPV6_INTERFACE_PARAMETER_CHANGE)
    {
        u4Event = VRRP_LINK_STATE_CHG_EVENT;

        pVrrpMsg->u4IfIndex
            = pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4Index;

        if (pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4IfStat ==
            NETIPV6_IF_DELETE)
        {
            pVrrpMsg->u4Cmd = VRRP_IFACE_DEL_PKT;
        }
        else if (pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4IfStat ==
                 NETIPV6_IF_DOWN)
        {
            pVrrpMsg->u4Cmd = VRRP_LINKDOWN_PKT;
        }
        else if (pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4IfStat ==
                 NETIPV6_IF_UP)
        {
            pVrrpMsg->u4Cmd = VRRP_LINKUP_PKT;
        }
        else
        {
            VRRP_DBG (VRRP_TRC_EVENTS, "IPv6 IF Create Event Not handled\n");

            MemReleaseMemBlock (VRRP_PKTQ_MEMPOOL_ID, (UINT1 *) pVrrpMsg);

            return;
        }
    }
    else if (pIp6HliParams->u4Command & NETIPV6_ADDRESS_CHANGE)
    {
        u4Event = VRRP_IP_ADDR_CHG_EVENT;

        pVrrpMsg->u4IfIndex = pIp6HliParams->unIpv6HlCmdType.AddrChange.u4Index;

        VRRP_IPVX_ADDR_INIT_IPVX (pVrrpMsg->IpAddr,
                                  VRRP_IPVX_ADDR_FMLY_IPV6,
                                  pIp6HliParams->unIpv6HlCmdType.AddrChange.
                                  Ipv6AddrInfo.Ip6Addr.u1_addr);

        pVrrpMsg->u4Cmd = VRRP_STATECHG_PKT;
        pVrrpMsg->u4Type
            = pIp6HliParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type;
    }
    else
    {
        VRRP_DBG (VRRP_TRC_EVENTS,
                  "Invalid event to be processed by VRRP. Posting to VRRP "
                  "is not done\n");

        MemReleaseMemBlock (VRRP_PKTQ_MEMPOOL_ID, (UINT1 *) pVrrpMsg);

        return;
    }

    if (OsixQueSend (gIpVrrpQId, (UINT1 *) &pVrrpMsg, OSIX_DEF_MSG_LEN) !=
        OSIX_SUCCESS)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "IpIf: Failed to enqueue link down event to VRRP");
        MemReleaseMemBlock (VRRP_PKTQ_MEMPOOL_ID, (UINT1 *) pVrrpMsg);
    }

    OsixEvtSend (gVrrpTaskId, u4Event);

    return;
}

/*******************************************************************************
 * Function Name   : VrrpEnabledOnInterface
 * Description     : Function to check whether VRRP is enabled on a an interface
 * Global Varibles :
 * Inputs          : i4IfIndex - cfa ifindex
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
VrrpEnabledOnInterface (INT4 i4IfIndex)
{
    INT4                i4Count;

    for (i4Count = 0; i4Count < (INT4) gu4VrrpMaxOperEntries; i4Count++)
    {
        if ((OPERIFINDEX (i4Count) == i4IfIndex) &&
            (OPERROWSTATUS (i4Count) == ACTIVE))
        {
            return VRRP_OK;
        }
    }

    return VRRP_NOT_OK;
}

/******************************************************************************* *
 * Function Name   : VrrpGetMacAddress
 * Description     : Function for getting the MacAddress
 * Global Varibles :
 * Inputs          : u4IfIndex   - CfaIfIndex
 *                   i4VrId      - Virtual IP
 *                   i4AddrType  - Address Type
 * Output          : pu1MacAddress.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
VrrpGetMacAddress (UINT4 u4IfIndex, INT4 i4VrId,
                   INT4 i4AddrType, UINT1 *pu1MacAddress)
{
    INT4                i4OperIndex = VRRP_OK;

    i4OperIndex = VrrpDbGetOperEntry ((INT4) u4IfIndex, i4VrId, i4AddrType);

    if (i4OperIndex == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    VRRP_MEMCPY (pu1MacAddress, &OPERMACADDR (i4OperIndex), MAC_ADDR_LEN);

    return VRRP_OK;
}

/******************************************************************************
 * Function Name   : IsVrrpMasterIpAddress
 * Description     : Function for checking masters ip address 
 * Global Varibles :
 * Inputs          : u4IfIndex - Interface Index
 *                 : u4IpAddr  - Ip Address
 * Output          : i4VrId - Virtual Id
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
IsVrrpMasterIpAddress (UINT4 u4IfIndex, UINT4 u4IpAddr, INT4 *i4VrId)
{
    INT4                i4Count = 0;
    UINT4               u4TempIpAddr = 0;

    for (i4Count = 0; i4Count < (INT4) gu4VrrpMaxOperEntries; i4Count++)
    {
        if (((UINT4) OPERIFINDEX (i4Count) == u4IfIndex) &&
            (OPERSTATE (i4Count) == MASTER_STATE))
        {
            VRRP_IPVX_COPY_TO_IPV4 (u4TempIpAddr, OPERMASTERIPADDR (i4Count));

            if (u4TempIpAddr == u4IpAddr)
            {
                *i4VrId = OPERVRID (i4Count);
                return VRRP_OK;
            }
        }
    }
    *i4VrId = 0;
    return VRRP_NOT_OK;
}

/******************************************************************************
 * Function Name   : VrrpGetVridFromAssocIpAddress
 * Description     : Checks whether given IP is an associated IP of a master
 *                   or not. If yes, returns corresponding vrid 
 * Global Varibles :
 * Inputs          : u4IfIndex           - Interface Index
 *                 : IpAddr              - IPv4 or IPv6 Address
 * Output          : pi4VrId - Virtual Id
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
VrrpGetVridFromAssocIpAddress (UINT4 u4IfIndex, tIPvXAddr IpAddr, INT4 *pi4VrId)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;
    INT4                i4Count = 0;
    UINT1               u1IsMclagEnabled = OSIX_FALSE;

    if (gi4VrrpStatus == VRRP_DISABLE)
    {
        *pi4VrId = VRRP_OK;
        return VRRP_OK;
    }

    for (i4Count = 0; i4Count < (INT4) gu4VrrpMaxOperEntries; i4Count++)
    {
        if (OPERIFINDEX (i4Count) != (INT4) u4IfIndex)
        {
            continue;
        }

        if (OPERROWSTATUS (i4Count) != ACTIVE)
        {
            continue;
        }

        UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4Count), pIpAddrEntry,
                             pTempIpAddrEntry, tIpAddrTable *)
        {
            if (pIpAddrEntry->u1AssoIpAddrRowStatus != (UINT1) ACTIVE)
            {
                continue;
            }

            if (VRRP_IPVX_COMPARE (pIpAddrEntry->AssoIpAddr, IpAddr) != 0)
            {
                continue;
            }

            VrrpIsMCLAGEnabled (u4IfIndex, &u1IsMclagEnabled);

            if (u1IsMclagEnabled == OSIX_TRUE)
            {
                *pi4VrId = OPERVRID (i4Count);
                return VRRP_OK;
            }

            if (OPERSTATE (i4Count) != MASTER_STATE)
            {
                *pi4VrId = 0;
                return VRRP_NOT_OK;
            }
            else
            {
                *pi4VrId = OPERVRID (i4Count);
                return VRRP_OK;
            }
        }
    }

    *pi4VrId = 0;

    return VRRP_OK;
}

/******************************************************************************
 * Function Name   : VrrpGetVrIdFromMacAddr
 * Description     : Function to get the VirId from Mac Address 
 * Global Varibles :
 * Inputs          : u1MacAddr - Mac Address 
 * Output          : i4VrId - Virtual Id
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
VrrpGetVrIdFromMacAddr (UINT1 *u1MacAddr, INT4 *i4VrId)
{
    UINT1               u1VirMac1[5] = { 0x0, 0x0, 0x5e, 0x0, 0x1 };
    UINT1               u1VirMac2[5] = { 0x0, 0x0, 0x5e, 0x0, 0x2 };

    if (MEMCMP (u1MacAddr, u1VirMac1, 5) == 0)
    {
        *i4VrId = (INT4) u1MacAddr[5];
        return VRRP_OK;
    }

    if (MEMCMP (u1MacAddr, u1VirMac2, 5) == 0)
    {
        *i4VrId = (INT4) u1MacAddr[5];
        return VRRP_OK;
    }

    *i4VrId = 0;
    return VRRP_NOT_OK;
}

/******************************************************************************
 * Function Name   : VrrpGetState
 * Description     : Function to get the current state of the Virtual Router 
 * Global Varibles :
 * Inputs          : u4IfIndex   - Cfa Interface Index
 *                 : i4VrId      - Virtual Id 
 *                   i4AddrType  - Address Type
 * Output          : 
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
VrrpGetState (UINT4 u4IfIndex, INT4 i4VrId, INT4 i4AddrType)
{
    INT4                i4OperIndex = VRRP_NOT_OK;

    i4OperIndex = VrrpDbGetOperEntry ((INT4) u4IfIndex, i4VrId, i4AddrType);

    if (i4OperIndex == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    return ((INT1) OPERSTATE (i4OperIndex));
}

/******************************************************************************
 * Function Name   : VrrpOpenSocket
 * Description     : Opens a IPv4 or IPv6 socket for sending and 
 *                   receiving VRRP packets.
 * Inputs          : None
 * Output          : None
 * Returns         : Valid Socket Descriptor if Socket is opened correctly
 *                   VRRP_NOT_OK - If Socket is not opened.
 ******************************************************************************/
INT4
VrrpOpenSocket (UINT1 u1AddrType)
{
    INT4                i4MCloopVal = VRRP_FALSE;    /* Reset multicast looping */
    INT4                i4HdrVal = VRRP_TRUE;    /* Set header include option */
    INT4                i4HopLimit = (INT4) VRRP_TTL;
    INT4                i4Socket = VRRP_NOT_OK;
    INT4                i4Family = VRRP_OK;
    INT4                i4SockOptLevel = VRRP_NOT_OK;
    INT4                i4SockOptName1 = VRRP_NOT_OK;
    INT4                i4SockOptName2 = VRRP_NOT_OK;
    INT4                i4RetVal = VRRP_NOT_OK;
    UINT1               u1OptVal = VRRP_TRUE;    /* Set IP Packet info option */
    UINT1               u1OptLen1 = 0;

    if (u1AddrType == (UINT1) VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        i4Family = AF_INET;
        i4SockOptLevel = IPPROTO_IP;
        i4SockOptName1 = IP_PKTINFO;
        u1OptLen1 = sizeof (UINT1);
#ifdef BSDCOMP_SLI_WANTED
        i4SockOptName2 = IP_MULTICAST_LOOP;
#endif
    }
    else
    {
        i4Family = AF_INET6;
        i4SockOptLevel = IPPROTO_IPV6;
#ifndef BSDCOMP_SLI_WANTED
        i4SockOptName1 = IP_RECVIF;
        u1OptLen1 = sizeof (UINT1);
#else
        i4SockOptName1 = IPV6_PKTINFO;
        u1OptLen1 = sizeof (struct in6_pktinfo);
#endif
        i4SockOptName2 = IPV6_MULTICAST_LOOP;
    }

    i4Socket = socket (i4Family, SOCK_RAW, VRRPMODULE);
    if (i4Socket < 0)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "Failed to open VRRP Socket\n");

        return VRRP_NOT_OK;
    }

    if ((fcntl (i4Socket, F_SETFL, O_NONBLOCK)) < 0)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP Socket Option to NONBLOCK failed\n");

        VrrpCloseSocket (&i4Socket);
        return VRRP_NOT_OK;
    }

    /* i4SockOptName1 is IP_PKTINFO for IPv4 and IPV6_PKTINFO for IPv6
     * */
    if (setsockopt (i4Socket, i4SockOptLevel, i4SockOptName1,
                    &u1OptVal, u1OptLen1) < 0)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP Socket Option IP_PKTINFO failed\n");

        VrrpCloseSocket (&i4Socket);
        return VRRP_NOT_OK;
    }

    /* Set the option to include the header.
     *
     * This is required only for IPv4. */
    if ((u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV4) &&
        (setsockopt (i4Socket, i4SockOptLevel, IP_HDRINCL,
                     &i4HdrVal, sizeof (INT4)) < 0))
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP Socket Option IP_HDRINCL failed\n");

        VrrpCloseSocket (&i4Socket);
        return VRRP_NOT_OK;
    }

    /* Set the option not to receive packet on the interface on which 
     * it is sent.
     *
     * i4SockOptName2 is IP_MULTICAST_LOOP for IPv4 and IPV6_MULTICAST_LOOP
     * for IPv6. */
    if (setsockopt (i4Socket, i4SockOptLevel, i4SockOptName2,
                    &i4MCloopVal, sizeof (INT4)) < 0)
    {
        /* Support is not present in FSIP, so, if failure happens
         * ignore it. */
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP Socket Option MULTICAST LOOP failed\n");
    }

    if ((u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV6) &&
        (setsockopt (i4Socket, i4SockOptLevel, IPV6_RECVHOPLIMIT,
                     &i4HdrVal, sizeof (INT4)) < 0))
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP Socket Option RECVHOPLIMIT failed\n");

        VrrpCloseSocket (&i4Socket);
        return VRRP_NOT_OK;
    }

#ifdef BSDCOMP_SLI_WANTED
    if ((u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV6) &&
        (setsockopt (i4Socket, i4SockOptLevel, IPV6_RECVPKTINFO,
                     &i4HdrVal, sizeof (INT4)) < 0))
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP Socket Option RECVHOPLIMIT failed\n");

        VrrpCloseSocket (&i4Socket);
        return VRRP_NOT_OK;
    }
#endif

    if ((u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV6) &&
        (setsockopt (i4Socket, i4SockOptLevel, IPV6_MULTICAST_HOPS,
                     &i4HopLimit, sizeof (INT4)) < 0))
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP Socket Option MULTICAST_HOPS failed\n");

        VrrpCloseSocket (&i4Socket);
        return VRRP_NOT_OK;
    }

    if (u1AddrType == (UINT1) VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        i4RetVal = SelAddFd (i4Socket, VrrpPktInSocket);
    }
    else
    {
        i4RetVal = SelAddFd (i4Socket, VrrpPktInIpv6Socket);
    }

    if (i4RetVal == VRRP_NOT_OK)
    {
        VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                   "Adding Callback function to Socket %d failed\n", i4Socket);

        VrrpCloseSocket (&i4Socket);

        return VRRP_NOT_OK;
    }

    if (u1AddrType == (UINT1) VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        VRRP_DBG1 (VRRP_TRC_INIT,
                   "Socket for VRRP IPv4 packet %d Opened\n", i4Socket);
    }
    else
    {
        VRRP_DBG1 (VRRP_TRC_INIT,
                   "Socket for VRRP IPv6 packet %d Opened\n", i4Socket);
    }

    return i4Socket;
}

/******************************************************************************
 * Function Name   : VrrpCloseSocket
 * Description     : Function for closing the socket for VRRP packets 
 * Inputs          : i4Socket   - Socket Descriptor
 * Output          : Closes socket
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
VrrpCloseSocket (INT4 *pi4Socket)
{
    INT4                i4RetVal = 0;
    INT4                i4Socket = *pi4Socket;

    if (i4Socket != VRRP_NOT_OK)
    {
        /*Close the socket */
        i4RetVal = close (i4Socket);

        if (i4RetVal < 0)
        {
            VRRP_DBG1 (VRRP_TRC_ALL_FAILURES,
                       "VRRP socket %d close failed\n", i4Socket);
            return VRRP_NOT_OK;
        }
    }

    VRRP_DBG1 (VRRP_TRC_INIT, "VRRP Socket %d closed\n", i4Socket);

    *pi4Socket = VRRP_NOT_OK;

    return VRRP_OK;
}

/******************************************************************************
 * Function Name   : VrrpOpenNdSocket
 * Description     : Opens a Socket for sending IPv6 ND Neighbor Advertisement
 *                   packet.
 * Inputs          : None
 * Output          : None
 * Returns         : Valid Socket Descriptor if Socket is opened correctly
 *                   VRRP_NOT_OK - If Socket is not opened.
 ******************************************************************************/
INT4
VrrpOpenNdSocket (VOID)
{
    INT4                i4MCloopVal = VRRP_FALSE;    /* Reset multicast looping */
    INT4                i4HdrVal = VRRP_TRUE;    /* Set header include option */
    INT4                i4Socket = VRRP_NOT_OK;
    INT4                i4HopLimit = (INT4) VRRP_TTL;
#ifndef BSDCOMP_SLI_WANTED
    UINT1               u1OptVal = VRRP_TRUE;    /* Set IP Packet info option */
#endif

    i4Socket = socket (AF_INET6, SOCK_RAW, IPPROTO_ICMPV6);

    if (i4Socket < 0)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES, "Failed to open VRRP ND Socket\n");

        return VRRP_NOT_OK;
    }

    if ((fcntl (i4Socket, F_SETFL, O_NONBLOCK)) < 0)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP ND Socket Option to NONBLOCK failed\n");

        VrrpCloseSocket (&i4Socket);
        return VRRP_NOT_OK;
    }

#ifndef BSDCOMP_SLI_WANTED
    if (setsockopt (i4Socket, IPPROTO_IPV6, IP_RECVIF,
                    &u1OptVal, sizeof (UINT1)) < 0)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP ND Socket Option PKTINFO failed\n");

        VrrpCloseSocket (&i4Socket);
        return VRRP_NOT_OK;
    }
#endif

    if (setsockopt (i4Socket, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
                    &i4MCloopVal, sizeof (INT4)) < 0)
    {
        /* Support is not present in FSIP, so, if failure happens
         * ignore it. */
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP ND Socket Option MULTICAST LOOP failed\n");
    }

    if (setsockopt (i4Socket, IPPROTO_IPV6, IPV6_RECVHOPLIMIT,
                    &i4HdrVal, sizeof (INT4)) < 0)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP ND Socket Option RECVHOPLIMIT failed\n");

        VrrpCloseSocket (&i4Socket);
        return VRRP_NOT_OK;
    }

    if (setsockopt (i4Socket, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                    &i4HopLimit, sizeof (INT4)) < 0)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "Setting VRRP ND Socket Option MULTICAST_HOPS failed\n");

        VrrpCloseSocket (&i4Socket);
        return VRRP_NOT_OK;
    }

    VRRP_DBG1 (VRRP_TRC_INIT,
               "Socket for VRRP IPv6 ND packet %d Opened\n", i4Socket);

    return i4Socket;
}

/******************************************************************************
 * Function Name   : VrrpPktInSocket
 * Description     : Call back function from SelAddFd(), when a VRRP IPv4 packet
 *                   is received 
 * Inputs          : i4SockFd
 * Output          : Sends an event to VRRP task
 * Returns         : None.
 ******************************************************************************/
VOID
VrrpPktInSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    OsixEvtSend (gVrrpTaskId, VRRP_PKT_RXED_EVENT);

    return;
}

/******************************************************************************
 * Function Name   : VrrpPktInIpv6Socket
 * Description     : Call back function from SelAddFd(), when VRRP IPv6 packet
 *                   is received 
 * Global Varibles : gVrrpTaskId
 * Inputs          : i4SockFd
 * Output          : Sends an event to VRRP task
 * Returns         : None.
 ******************************************************************************/
VOID
VrrpPktInIpv6Socket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);

    OsixEvtSend (gVrrpTaskId, VRRP_IPV6_PKT_RXED_EVENT);

    return;
}

/******************************************************************************
 * Function Name   : VrrpIpifJoinMcastGroup
 * Description     : Function for an interface to join VRRP multicat group 
 * Inputs          : i4OperIndex  - Oper Index
 * Output          : Joins Vrrp multicast group.
 * Returns         : VRRP_OK - on success
 *                   VRRP_NOT_OK - on failure.
 ******************************************************************************/
INT4
VrrpIpifJoinMcastGroup (INT4 i4OperIndex)
{
    struct ip_mreqn     Mreqn;
#ifndef BSDCOMP_SLI_WANTED
    tSliIpv6Mreq        Ipv6Mreqn;
#else
    struct ipv6_mreq    Ipv6Mreqn;
#endif
    VOID               *pMreqn = NULL;
    UINT4               u4Port = 0;
    UINT4               u4Mcastaddr = 0;
    UINT4               u4VrrpMcastAddr = VRRP_MCAST_ADDR;
    INT4                i4SockOptLevel = VRRP_NOT_OK;
    INT4                i4SockOptName = VRRP_NOT_OK;
    INT4                i4Size = VRRP_NOT_OK;
    INT4                i4Socket = VRRP_NOT_OK;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (&Mreqn, 0, sizeof (Mreqn));
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&Ipv6Mreqn, 0, sizeof (Ipv6Mreqn));

    if (VRRP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return VRRP_OK;
    }

    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    if (OPERVRPORTNUM (i4OperIndex) != 0)
    {
        u4Port = OPERVRPORTNUM (i4OperIndex);
    }

    if (OPERADDRTYPE (i4OperIndex) == (UINT1) VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        i4SockOptLevel = IPPROTO_IP;
        i4SockOptName = IP_ADD_MEMBERSHIP;

        u4Mcastaddr = OSIX_HTONL (u4VrrpMcastAddr);
        Mreqn.imr_multiaddr.s_addr = u4Mcastaddr;
        Mreqn.imr_ifindex = (INT4) u4Port;

        pMreqn = (VOID *) &Mreqn;
        i4Size = sizeof (Mreqn);
        i4Socket = gi4VrrpIpv4Socket;
    }
    else
    {
        i4SockOptLevel = IPPROTO_IPV6;
        i4SockOptName = IPV6_ADD_MEMBERSHIP;

#ifndef BSDCOMP_SLI_WANTED
        MEMCPY (Ipv6Mreqn.ipv6mr_multiaddr.s6_addr,
                gVrrpV6DestMCastAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        Ipv6Mreqn.ipv6mr_ifindex = (INT4) u4Port;
#else
        MEMCPY (Ipv6Mreqn.ipv6mr_multiaddr.s6_addr,
                gVrrpV6DestMCastAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        Ipv6Mreqn.ipv6mr_interface = (INT4) u4Port;
#endif
        pMreqn = (VOID *) &Ipv6Mreqn;
        i4Size = sizeof (Ipv6Mreqn);

        i4Socket = gi4VrrpIpv6Socket;
    }

    if (i4Socket == VRRP_NOT_OK)
    {
        PktProcessUpdateStatistics (i4OperIndex, VRRPSOCKETFAIL);
        return VRRP_NOT_OK;
    }

    if (setsockopt (i4Socket, i4SockOptLevel, i4SockOptName, pMreqn, i4Size) <
        0)
    {
        /* Ignore failure */
    }

    return VRRP_OK;
}

/******************************************************************************
 * Function Name   : VrrpIpifLeaveMcastGroup
 * Description     : Function for an interface to join VRRP multicat group 
 * Inputs          : i4OperIndex - Oper Index
 * Output          : Leaves Vrrp multicast group.
 * Returns         : VRRP_OK - on success
 *                   VRRP_NOT_OK - on failure.
 ******************************************************************************/
INT4
VrrpIpifLeaveMcastGroup (INT4 i4OperIndex)
{
    struct ip_mreqn     Mreqn;
#ifndef BSDCOMP_SLI_WANTED
    tSliIpv6Mreq        Ipv6Mreqn;
#else
    struct ipv6_mreq    Ipv6Mreqn;
#endif
    VOID               *pMreqn = NULL;
    UINT4               u4Port = 0;
    UINT4               u4Mcastaddr = 0;
    UINT4               u4VrrpMcastAddr = VRRP_MCAST_ADDR;
    INT4                i4SockOptLevel = VRRP_NOT_OK;
    INT4                i4SockOptName = VRRP_NOT_OK;
    INT4                i4Size = VRRP_NOT_OK;
    INT4                i4Socket = VRRP_NOT_OK;

    MEMSET (&Mreqn, 0, sizeof (Mreqn));
    MEMSET (&Ipv6Mreqn, 0, sizeof (Ipv6Mreqn));

    if (VRRP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return VRRP_OK;
    }

    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    if (OPERVRPORTNUM (i4OperIndex) != 0)
    {
        u4Port = OPERVRPORTNUM (i4OperIndex);
    }

    if (OPERADDRTYPE (i4OperIndex) == (UINT1) VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        i4SockOptLevel = IPPROTO_IP;
        i4SockOptName = IP_DROP_MEMBERSHIP;

        u4Mcastaddr = OSIX_HTONL (u4VrrpMcastAddr);
        Mreqn.imr_multiaddr.s_addr = u4Mcastaddr;
        Mreqn.imr_ifindex = (INT4) u4Port;

        pMreqn = (VOID *) &Mreqn;
        i4Size = sizeof (Mreqn);
        i4Socket = gi4VrrpIpv4Socket;
    }
    else
    {
        i4SockOptLevel = IPPROTO_IPV6;
        i4SockOptName = IPV6_DROP_MEMBERSHIP;

#ifndef BSDCOMP_SLI_WANTED
        MEMCPY (Ipv6Mreqn.ipv6mr_multiaddr.s6_addr,
                gVrrpV6DestMCastAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        Ipv6Mreqn.ipv6mr_ifindex = (INT4) u4Port;
#else
        MEMCPY (Ipv6Mreqn.ipv6mr_multiaddr.s6_addr,
                gVrrpV6DestMCastAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        Ipv6Mreqn.ipv6mr_interface = (INT4) u4Port;
#endif
        pMreqn = (VOID *) &Ipv6Mreqn;
        i4Size = sizeof (Ipv6Mreqn);

        i4Socket = gi4VrrpIpv6Socket;
    }

    if (i4Socket == VRRP_NOT_OK)
    {
        PktProcessUpdateStatistics (i4OperIndex, VRRPSOCKETFAIL);
        return VRRP_NOT_OK;
    }

    if (setsockopt (i4Socket, i4SockOptLevel, i4SockOptName, pMreqn, i4Size) <
        0)
    {
        /* Ignore failure */
    }

    return VRRP_OK;
}

/******************************************************************************
 * Function Name   : GetVrrpOperAuthKey
 * Description     : Fetches the value of vrrpOperAuthKey from the 
 *                   database. 
 * Global Varibles : None
 * Inputs          : u4IfIndex 
                   : i4VrrpOperVrId
 * Output          : pOctetAuthKeyValue 
 * Returns         : VRRP_OK - on success
 *                   VRRP_NOT_OK - on failure.
 ******************************************************************************/
INT4
GetVrrpOperAuthKey (UINT4 u4IfIndex, INT4 i4VrrpOperVrId,
                    tSNMP_OCTET_STRING_TYPE * pOctetAuthKeyValue)
{
    INT4                i4count;

    if ((i4count = VrrpDbGetOperEntry ((INT4) u4IfIndex, i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4)) >= ZERO)
    {

        pOctetAuthKeyValue->i4_Length =
            (INT4) STRLEN (OPERAUTHKEY (i4count).pu1_OctetList);
        MEMCPY (pOctetAuthKeyValue->pu1_OctetList,
                OPERAUTHKEY (i4count).pu1_OctetList,
                pOctetAuthKeyValue->i4_Length);
        return VRRP_OK;
    }
    return VRRP_NOT_OK;

}

/******************************************************************************
* Function Name :  VrrpApiValIfTrackIfExists
*
* Description   :  This function calls VrrpValIfTrackIfExists inside
*                   semaphore protection
* Input (s)     :  i4RecvdIfIndex     - Interface Index
* Output (s)    :  None
* Returns       :  i4Ret,which is VRRP_OK or VRRP_NOT_OK
*******************************************************************************/
INT4
VrrpApiValIfTrackIfExists (INT4 i4RecvdIfIndex)
{
    INT4                i4Ret = 0;

    VRRP_LOCK ();

    i4Ret = VrrpValIfTrackIfExists (i4RecvdIfIndex);

    VRRP_UNLOCK ();

    return i4Ret;
}

/******************************************************************************
 * Function Name   : VrrpApiValIfIpChange
 * Description     : This function calls VrrpValIfIpChange with semaphore
 *                   protection
 * Inputs          : u4IfIndex - interface address to be added
 *                   u4IpAddr  - IP address(Current interface address)
 *                   u4IpSubnetMask - Subnet mask of current interface address
 * Returns         : i4Ret VRRP_OK - on success
 *                   VRRP_NOT_OK - on failure.
 ******************************************************************************/
INT4
VrrpApiValIfIpChange (UINT4 u4IfIndex, tIPvXAddr CurIpAddr,
                      UINT4 u4IpSubnetMask)
{
    INT4                i4Ret = 0;

    VRRP_LOCK ();

    i4Ret = VrrpValIfIpChange (u4IfIndex, CurIpAddr, u4IpSubnetMask);

    VRRP_UNLOCK ();

    return i4Ret;
}

/******************************************************************************
 * Function Name   : VrrpApiValDecPriority
 * Description     : This function calls VrrpExtValDecPriority with semaphore
 *                   protection
 * Inputs          : u4IfIndex     - interface address to be added
 *                   u1Operation   - Operation performed
 *                   PrevIpAddr    - Prev Ip Addr
 *                   CurIpAddr     - Current Ip Addr
 * Returns         : i4Ret VRRP_OK - on success
 *                   VRRP_NOT_OK - on failure.
 ******************************************************************************/
INT4
VrrpApiValDecPriority (UINT4 u4IfIndex, UINT1 u1Operation,
                       tIPvXAddr PrevIpAddr, tIPvXAddr CurIpAddr)
{
    INT4                i4Ret = 0;

    VRRP_LOCK ();

    i4Ret = VrrpExtValDecPriority (u4IfIndex, u1Operation, PrevIpAddr,
                                   CurIpAddr);

    VRRP_UNLOCK ();

    return i4Ret;
}

/******************************************************************************
 * Function Name   : VrrpIpIfHandleCardAttach
 * Description     : Calls the VrrpHandleCardAttach function after acquiring 
 *                   VrrpLock()
 * Global Varibles : None
 * Inputs          : pSlotInfo   - Mbsm Slot Inforamtion.
                   : i4AddrType  - Address Type.
 * Output          : Calls the FsMbsmVrrpHwProgram
 * Returns         : VRRP_OK - on success
 *                   VRRP_NOT_OK - on failure.
 ******************************************************************************/
INT4
VrrpIpIfHandleCardAttach (tMbsmSlotInfo * pSlotInfo, INT4 i4AddrType)
{

    INT4                i4RetVal = 0;

    VrrpLock ();

    i4RetVal = VrrpHandleCardAttach (pSlotInfo, i4AddrType);

    VrrpUnLock ();

    return i4RetVal;

}

/******************************************************************************
 * Function Name   : VrrpHandleCardAttach
 * Description     : Calls the VrrpIpIfMbsmHwWr for every Master
 * Global Varibles : None
 * Inputs          : pSlotInfo   - Mbsm Slot Inforamtion.
                   : i4AddrType  - Address Type.
 * Output          : Calls the FsMbsmVrrpHwProgram
 * Returns         : VRRP_OK - on success
 *                   VRRP_NOT_OK - on failure.
 ******************************************************************************/
INT4
VrrpHandleCardAttach (tMbsmSlotInfo * pSlotInfo, INT4 i4AddrType)
{

    INT4                i4count = 0;

    for (i4count = 0; i4count < (INT4) gu4VrrpMaxOperEntries; i4count++)
    {
        if ((OPERADDRTYPE (i4count) == (UINT1) i4AddrType) &&
            (OPERSTATE (i4count) == MASTER_STATE))
        {
            if (VrrpIpIfMbsmHwWr (pSlotInfo, i4count) == VRRP_NOT_OK)
            {
                return VRRP_NOT_OK;
            }
        }
    }

    return VRRP_OK;

}

/******************************************************************************
 * Function Name   : VrrpIpIfMbsmHwWr
 * Description     : Fills the VRRP Network Interface structure based on
 *                   OperIndex
 *
 * Global Varibles : None
 * Inputs          : pSlotInfo   - Mbsm Slot Inforamtion
                   : i4OperIndex - Oper Index
 * Output          :
 * Returns         : VRRP_OK - on success
 *                   VRRP_NOT_OK - on failure.
 ******************************************************************************/
INT4
VrrpIpIfMbsmHwWr (tMbsmSlotInfo * pSlotInfo, INT4 i4OperIndex)
{

    tVrrpNwIntf         VrrpNwIntf;
    UINT4               u4Port = 0;

    MEMSET (&VrrpNwIntf, 0, sizeof (tVrrpNwIntf));

    if (VrrpGetPortFromOperIndex (i4OperIndex, &u4Port) == VRRP_NOT_OK)
    {
        return VRRP_NOT_OK;
    }

    if (VRRP_IS_NP_PROGRAMMING_ALLOWED () != VRRP_OK)
    {
        return VRRP_NOT_OK;
    }

    VrrpNwIntf.u4IfIndex = (UINT4) OPERIFINDEX (i4OperIndex);
    VrrpNwIntf.u4Port = u4Port;
    VrrpNwIntf.u4VrId = (UINT4) OPERVRID (i4OperIndex);
    VrrpNwIntf.u1AddrType = OPERADDRTYPE (i4OperIndex);
    VRRP_IPVX_COPY (VrrpNwIntf.IntfAddr, INTERFACEIPADDR (i4OperIndex));
    VRRP_IPVX_COPY (VrrpNwIntf.VirtualIp, OPERPRIMARYIPADDR (i4OperIndex));
    VrrpNwIntf.u1Action = VRRP_NW_INTF_CREATE;
    VrrpNwIntf.b1IsIpvXOwner = OPERISOWNER (i4OperIndex);
    VrrpNwIntf.b1IsOwnerPresent =
        VrrpIsOwnerPresentInThisIfIndex (OPERIFINDEX (i4OperIndex),
                                         OPERVRID (i4OperIndex),
                                         OPERADDRTYPE (i4OperIndex));

    if (OPERACCEPTMODE (i4OperIndex) == VRRP_DISABLE)
    {
        VrrpNwIntf.u1AcceptMode = FALSE;
    }
    else
    {
        VrrpNwIntf.u1AcceptMode = TRUE;
    }

    VRRPOPERHWSTATUS (i4OperIndex) = NP_NOT_UPDATED;

#ifdef MBSM_WANTED
    if (FsMbsmVrrpHwProgram (pSlotInfo, (tVrrpNwIntf *) & VrrpNwIntf) ==
        FNP_FAILURE)
    {
        return VRRP_NOT_OK;
    }
#else
    UNUSED_PARAM (pSlotInfo);
#endif /* MBSM_WANTED */

    return VRRP_OK;
}

/*******************************************************************************
* Function Name   : VrrpAcceptModeEnabledOnInterface
* Description     : Function to check whether VRRP Accept mode enabled on a an interface
* Global Varibles :
* Inputs          : i4IfIndex - cfa ifindex
* Output          : None.
* Returns         : VRRP_OK.
*                   VRRP_NOT_OK.
******************************************************************************/
INT1
VrrpAcceptModeEnabledOnInterface (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                                  tIPvXAddr IpAddr)
{
    /*vrrp lock is not required here as we access RB Tree only.
     * RB tree will have separate LOCK and also gvrrpOperTable variable
     * is unchanged once its initialised
     */
    INT4                i4Index = VRRP_NOT_OK;
    i4Index = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

    if ((i4Index == VRRP_NOT_OK))
    {
        return VRRP_NOT_OK;
    }

    /*checking whether the received ip is one for the interface ip or not,
     * if the received address is one for the interface address
     * we should not drop that packet*/
    if (VrrpCompIsOurIp (OPERADDRTYPE (i4Index),
                         OPERIFINDEX (i4Index), IpAddr) == VRRP_OK)
    {
        return VRRP_OK;
    }
    /*checking wheteher the received ip is present in the associated ip address
     * table, if not present it means that ip is for forwarding*/
    if (VrrpValIfIpChange ((UINT4) i4IfIndex, IpAddr, 0) == VRRP_NOT_OK)
    {
        return VRRP_OK;
    }

    if ((OPERACCEPTMODE (i4Index) == VRRP_ENABLE))
    {
        return VRRP_OK;
    }

    return VRRP_NOT_OK;
}

/*******************************************************************************
 * Function Name   : VrrpSetLocalAffinity 
 * Description     : Function to deregister/register  MAC address with 
 *          Ethernet in VRRP Backup node
 * Global Varibles : None
 * Inputs          : i4IfIndex - cfa ifindex
 * Output          : None.
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
VOID
VrrpSetLocalAffinity (UINT1 u1LocalAffinity)
{
    tVrrpIPvXAddr       MasterIpAddress;
    INT4                i4Version = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4VrId = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4RetVal = 0;
    INT4                i4NextVrId = 0;
    INT4                i4OperIndex = 0;
    UINT1               u1IsMclagEnabled = OSIX_FALSE;
    UINT1               au1VMacAddr[MACADDRSIZE];

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        return;
    }
    if (i4Version == VRRP_VERSION_2)
    {
        i4RetVal = nmhGetFirstIndexVrrpOperTable (&i4IfIndex, &i4VrId);

        i4AddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        i4RetVal = nmhGetFirstIndexVrrpv3OperationsTable (&i4IfIndex,
                                                          &i4VrId, &i4AddrType);

    }
    while (i4RetVal == SNMP_SUCCESS)
    {
        VrrpIsMCLAGEnabled ((UINT4) i4IfIndex, &u1IsMclagEnabled);
        if ((u1IsMclagEnabled == OSIX_TRUE) &&
            (OPERSTATE (i4OperIndex) == BACKUP_STATE))
        {
            for (i4OperIndex = 0; i4OperIndex < (INT4) gu4VrrpMaxOperEntries;
                 i4OperIndex++)
            {
                if (OPERIFINDEX (i4OperIndex) == i4IfIndex)
                {
                    break;
                }
            }
            VRRP_IPVX_ADDR_CLEAR (MasterIpAddress);
            VRRP_IPVX_COPY (MasterIpAddress, OPERMASTERIPADDR (i4OperIndex));
            VRRP_MEMCPY (au1VMacAddr, &OPERMACADDR (i4OperIndex), MACADDRSIZE);
            if (u1LocalAffinity == OSIX_FALSE)
            {
                EthDeRegisterMacAddr (MasterIpAddress, au1VMacAddr,
                                      i4OperIndex);
            }
            else
            {
                EthRegisterMacAddr (MasterIpAddress, au1VMacAddr, i4OperIndex);
            }
        }
        if (i4Version == VRRP_VERSION_2)
        {
            i4RetVal = nmhGetNextIndexVrrpOperTable (i4IfIndex, &i4NextIfIndex,
                                                     i4VrId, &i4NextVrId);

            i4NextAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
        }
        else
        {
            i4RetVal =
                nmhGetNextIndexVrrpv3OperationsTable (i4IfIndex, &i4NextIfIndex,
                                                      i4VrId, &i4NextVrId,
                                                      i4AddrType,
                                                      &i4NextAddrType);
        }

        i4IfIndex = i4NextIfIndex;
        i4VrId = i4NextVrId;
        i4AddrType = i4NextAddrType;
    }
}

/******************************************************************************
 * Function Name   : IsVrrpVirtualIpAddress
 * Description     : Function for checkin if Virtual IP is given as input
 * Global Varibles :
 * Inputs          : u4IfIndex - Interface Index
 *                 : u4IpAddr  - Ip Address
 * Output          : i4VrId - Virtual Id
 * Returns         : VRRP_OK.
 *                   VRRP_NOT_OK.
 ******************************************************************************/
INT1
IsVrrpVirtualIpAddress (UINT4 u4IfIndex, UINT4 u4IpAddr, INT4 *i4VrId)
{
    INT4                i4Count = 0;
    UINT4               u4TempIpAddr = 0;
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;

    for (i4Count = 0; i4Count < (INT4) gu4VrrpMaxOperEntries; i4Count++)
    {
        if (((UINT4) OPERIFINDEX (i4Count) == u4IfIndex) &&
            (OPERSTATE (i4Count) == MASTER_STATE))
        {

            UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4Count), pIpAddrEntry,
                                 pTempIpAddrEntry, tIpAddrTable *)
            {
                if (pIpAddrEntry->u1AssoIpAddrRowStatus != (UINT1) ACTIVE)
                {
                    continue;
                }
                VRRP_IPVX_COPY_TO_IPV4 (u4TempIpAddr, pIpAddrEntry->AssoIpAddr);
                if (u4TempIpAddr != u4IpAddr)
                {
                    continue;
                }
                *i4VrId = OPERVRID (i4Count);
                return VRRP_OK;
            }
        }

    }
    *i4VrId = 0;
    return VRRP_NOT_OK;
}
#endif /* _IP_IF_C */
