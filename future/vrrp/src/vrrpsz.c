/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vrrpsz.c,v 1.6 2014/03/11 14:22:22 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _VRRPSZ_C
#include "vrrpinc.h"
INT4
VrrpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < VRRP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsVRRPSizingParams[i4SizingId].
                                     u4StructSize,
                                     FsVRRPSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(VRRPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            VrrpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
VrrpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsVRRPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, VRRPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
VrrpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < VRRP_MAX_SIZING_ID; i4SizingId++)
    {
        if (VRRPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (VRRPMemPoolIds[i4SizingId]);
            VRRPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
