
/*  $Id: vrrplow.c,v 1.43 2017/12/16 11:57:11 siva Exp $ */
#define  MIDLEVEL
#include "vrrpinc.h"
#include "vrrpcli.h"
#include "fsvrrpcli.h"

/* LOW LEVEL Routines for Table : VrrpOperTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVrrpOperTable
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhValidateIndexInstanceVrrpOperTable (INT4 i4IfIndex, INT4 i4VrrpOperVrId)
{
    if ((i4IfIndex <= 0) ||
        (i4VrrpOperVrId < VRRP_MIN_VRID) || (i4VrrpOperVrId > VRRP_MAX_VRID))
    {
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVrrpOperTable
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVrrpOperTable (INT4 *pi4IfIndex, INT4 *pi4VrrpOperVrId)
{
    return (nmhGetNextIndexVrrpOperTable (0, pi4IfIndex, 0, pi4VrrpOperVrId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexVrrpOperTable
 Input       :  The Indices
                i4IfIndex
                nexti4IfIndex
                VrrpOperVrId
                nextVrrpOperVrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVrrpOperTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                              INT4 i4VrrpOperVrId, INT4 *pi4NextVrrpOperVrId)
{
    BOOL1               b1VersionMatched = FALSE;

    b1VersionMatched = VrrpCompCheckVersion (VRRP_VERSION_2);

    if (b1VersionMatched == FALSE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    if (VrrpDbGetNextV2OperEntry (i4IfIndex, i4VrrpOperVrId, pi4NextIfIndex,
                                  pi4NextVrrpOperVrId) == VRRP_NOT_OK)
    {
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetVrrpOperVirtualMacAddr
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperVirtualMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperVirtualMacAddr (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                              tMacAddr * pRetValVrrpOperVirtualMacAddr)
{
    tSNMP_OCTET_STRING_TYPE MacAddr;
    tSNMP_MULTI_DATA_TYPE Data;
    UINT1               au1MacAddr[MAC_ADDR_LEN];
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    MEMSET (au1MacAddr, 0, MAC_ADDR_LEN);
    MacAddr.pu1_OctetList = au1MacAddr;
    MacAddr.i4_Length = MAC_ADDR_LEN;

    Data.pOctetStrValue = &MacAddr;

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_VIRT_MAC, &Data);

    MEMCPY (pRetValVrrpOperVirtualMacAddr, Data.pOctetStrValue->pu1_OctetList,
            MAC_ADDR_LEN);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperState
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperState (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                     INT4 *pi4RetValVrrpOperState)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_STATUS, &Data);

    *pi4RetValVrrpOperState = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperAdminState
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperAdminState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperAdminState (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                          INT4 *pi4RetValVrrpOperAdminState)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ADMIN_STATE, &Data);

    *pi4RetValVrrpOperAdminState = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperPriority
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperPriority (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                        INT4 *pi4RetValVrrpOperPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_PRIORITY, &Data);

    *pi4RetValVrrpOperPriority = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperIpAddrCount
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperIpAddrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperIpAddrCount (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                           INT4 *pi4RetValVrrpOperIpAddrCount)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ADDR_COUNT, &Data);

    *pi4RetValVrrpOperIpAddrCount = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperMasterIpAddr
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperMasterIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperMasterIpAddr (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                            UINT4 *pu4RetValVrrpOperMasterIpAddr)
{
    tSNMP_MULTI_DATA_TYPE Data;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1Ipv4Addr[IPVX_IPV4_ADDR_LEN];
    INT4                i4RetValue = SNMP_FAILURE;
    UINT4               u4IpAddr = 0;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Ipv4Addr, 0, IPVX_IPV4_ADDR_LEN);

    IpAddr.pu1_OctetList = au1Ipv4Addr;
    IpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    Data.pOctetStrValue = &IpAddr;

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_MASTER_IP, &Data);

    MEMCPY (&u4IpAddr, Data.pOctetStrValue->pu1_OctetList, IPVX_IPV4_ADDR_LEN);

    *pu4RetValVrrpOperMasterIpAddr = OSIX_NTOHL (u4IpAddr);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperPrimaryIpAddr
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperPrimaryIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperPrimaryIpAddr (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                             UINT4 *pu4RetValVrrpOperPrimaryIpAddr)
{
    tSNMP_MULTI_DATA_TYPE Data;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1Ipv4Addr[IPVX_IPV4_ADDR_LEN];
    INT4                i4RetValue = SNMP_FAILURE;
    UINT4               u4IpAddr = 0;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Ipv4Addr, 0, IPVX_IPV4_ADDR_LEN);

    IpAddr.pu1_OctetList = au1Ipv4Addr;
    IpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    Data.pOctetStrValue = &IpAddr;

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_PRIM_IP, &Data);

    MEMCPY (&u4IpAddr, Data.pOctetStrValue->pu1_OctetList, IPVX_IPV4_ADDR_LEN);

    *pu4RetValVrrpOperPrimaryIpAddr = OSIX_NTOHL (u4IpAddr);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperAuthType
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperAuthType (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                        INT4 *pi4RetValVrrpOperAuthType)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_AUTH_TYPE, &Data);

    *pi4RetValVrrpOperAuthType = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperAuthKey
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperAuthKey (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                       tSNMP_OCTET_STRING_TYPE * pRetValVrrpOperAuthKey)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    Data.pOctetStrValue = pRetValVrrpOperAuthKey;

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_AUTH_KEY, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperAdvertisementInterval
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperAdvertisementInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperAdvertisementInterval (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                     INT4
                                     *pi4RetValVrrpOperAdvertisementInterval)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ADVT_INT, &Data);

    *pi4RetValVrrpOperAdvertisementInterval
        = (Data.i4_SLongValue / V2_ADVERTISEMENTINTLINMSEC);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperPreemptMode
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperPreemptMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperPreemptMode (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                           INT4 *pi4RetValVrrpOperPreemptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_PREMP_MODE, &Data);

    *pi4RetValVrrpOperPreemptMode = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperVirtualRouterUpTime
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperVirtualRouterUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperVirtualRouterUpTime (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                   UINT4 *pu4RetValVrrpOperVirtualRouterUpTime)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_UP_TIME, &Data);

    *pu4RetValVrrpOperVirtualRouterUpTime = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperProtocol
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperProtocol (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                        INT4 *pi4RetValVrrpOperProtocol)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_PROTOCOL, &Data);

    *pi4RetValVrrpOperProtocol = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpOperRowStatus
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpOperRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpOperRowStatus (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                         INT4 *pi4RetValVrrpOperRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ROW_STATUS, &Data);

    *pi4RetValVrrpOperRowStatus = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVrrpOperAdminState
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                setValVrrpOperAdminState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpOperAdminState (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                          INT4 i4SetValVrrpOperAdminState)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValVrrpOperAdminState;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ADMIN_STATE, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpOperPriority
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                setValVrrpOperPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpOperPriority (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                        INT4 i4SetValVrrpOperPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValVrrpOperPriority;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_PRIORITY, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpOperPrimaryIpAddr
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                setValVrrpOperPrimaryIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpOperPrimaryIpAddr (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                             UINT4 u4SetValVrrpOperPrimaryIpAddr)
{
    tSNMP_MULTI_DATA_TYPE Data;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1Ipv4Addr[IPVX_IPV4_ADDR_LEN];
    INT4                i4RetValue = SNMP_FAILURE;
    UINT4               u4IpAddr = 0;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Ipv4Addr, 0, IPVX_IPV4_ADDR_LEN);

    IpAddr.pu1_OctetList = au1Ipv4Addr;
    IpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    Data.pOctetStrValue = &IpAddr;

    u4IpAddr = OSIX_HTONL (u4SetValVrrpOperPrimaryIpAddr);
    MEMCPY (Data.pOctetStrValue->pu1_OctetList, &u4IpAddr, IPVX_IPV4_ADDR_LEN);

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_PRIM_IP, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpOperAuthType
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                setValVrrpOperAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpOperAuthType (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                        INT4 i4SetValVrrpOperAuthType)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValVrrpOperAuthType;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_AUTH_TYPE, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpOperAuthKey
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                setValVrrpOperAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpOperAuthKey (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                       tSNMP_OCTET_STRING_TYPE * pSetValVrrpOperAuthKey)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.pOctetStrValue = pSetValVrrpOperAuthKey;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_AUTH_KEY, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpOperAdvertisementInterval
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                setValVrrpOperAdvertisementInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpOperAdvertisementInterval (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                     INT4 i4SetValVrrpOperAdvertisementInterval)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue
        = (i4SetValVrrpOperAdvertisementInterval * V2_ADVERTISEMENTINTLINMSEC);

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ADVT_INT, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpOperPreemptMode
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                setValVrrpOperPreemptMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpOperPreemptMode (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                           INT4 i4SetValVrrpOperPreemptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValVrrpOperPreemptMode;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_PREMP_MODE, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpOperProtocol
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                setValVrrpOperProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpOperProtocol (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                        INT4 i4SetValVrrpOperProtocol)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValVrrpOperProtocol;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_PROTOCOL, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpOperRowStatus
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                setValVrrpOperRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpOperRowStatus (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                         INT4 i4SetValVrrpOperRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValVrrpOperRowStatus;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ROW_STATUS, &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VrrpOperAdminState
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                testValVrrpOperAdminState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpOperAdminState (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                             INT4 i4VrrpOperVrId,
                             INT4 i4TestValVrrpOperAdminState)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValVrrpOperAdminState;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_ADMIN_STATE, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2VrrpOperPriority
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                testValVrrpOperPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpOperPriority (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                           INT4 i4VrrpOperVrId, INT4 i4TestValVrrpOperPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValVrrpOperPriority;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_PRIORITY, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2VrrpOperPrimaryIpAddr
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                testValVrrpOperPrimaryIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpOperPrimaryIpAddr (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                INT4 i4VrrpOperVrId,
                                UINT4 u4TestValVrrpOperPrimaryIpAddr)
{
    tSNMP_MULTI_DATA_TYPE Data;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1Ipv4Addr[IPVX_IPV4_ADDR_LEN];
    INT4                i4RetValue = SNMP_FAILURE;
    UINT4               u4IpAddr = 0;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    MEMSET (&IpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Ipv4Addr, 0, IPVX_IPV4_ADDR_LEN);

    IpAddr.pu1_OctetList = au1Ipv4Addr;
    IpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    Data.pOctetStrValue = &IpAddr;

    u4IpAddr = OSIX_HTONL (u4TestValVrrpOperPrimaryIpAddr);
    MEMCPY (Data.pOctetStrValue->pu1_OctetList, &u4IpAddr, IPVX_IPV4_ADDR_LEN);

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_PRIM_IP, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2VrrpOperAuthType
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                testValVrrpOperAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpOperAuthType (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                           INT4 i4VrrpOperVrId, INT4 i4TestValVrrpOperAuthType)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValVrrpOperAuthType;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_AUTH_TYPE, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2VrrpOperAuthKey
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                testValVrrpOperAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpOperAuthKey (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                          INT4 i4VrrpOperVrId,
                          tSNMP_OCTET_STRING_TYPE * pTestValVrrpOperAuthKey)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.pOctetStrValue = pTestValVrrpOperAuthKey;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_AUTH_KEY, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2VrrpOperAdvertisementInterval
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                testValVrrpOperAdvertisementInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpOperAdvertisementInterval (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        INT4 i4VrrpOperVrId,
                                        INT4
                                        i4TestValVrrpOperAdvertisementInterval)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue
        = (i4TestValVrrpOperAdvertisementInterval * V2_ADVERTISEMENTINTLINMSEC);

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_ADVT_INT, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2VrrpOperPreemptMode
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                testValVrrpOperPreemptMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpOperPreemptMode (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              INT4 i4VrrpOperVrId,
                              INT4 i4TestValVrrpOperPreemptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValVrrpOperPreemptMode;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_PREMP_MODE, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2VrrpOperProtocol
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                testValVrrpOperProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpOperProtocol (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                           INT4 i4VrrpOperVrId, INT4 i4TestValVrrpOperProtocol)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValVrrpOperProtocol;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_PROTOCOL, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2VrrpOperRowStatus
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                testValVrrpOperRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpOperRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                            INT4 i4VrrpOperVrId,
                            INT4 i4TestValVrrpOperRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValVrrpOperRowStatus;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_ROW_STATUS, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VrrpOperTable
 Input       :  The Indices
                IfIndex
                VrrpOperVrId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VrrpOperTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : VrrpAssoIpAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVrrpAssoIpAddrTable
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId
                VrrpAssoIpAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceVrrpAssoIpAddrTable (INT4 i4IfIndex,
                                             INT4 i4VrrpOperVrId,
                                             UINT4 u4VrrpAssoIpAddr)
{
    if (nmhValidateIndexInstanceVrrpOperTable (i4IfIndex, i4VrrpOperVrId)
        == SNMP_FAILURE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    if (!((IP_IS_ADDR_CLASS_A (u4VrrpAssoIpAddr)) ||
          (IP_IS_ADDR_CLASS_B (u4VrrpAssoIpAddr)) ||
          (IP_IS_ADDR_CLASS_C (u4VrrpAssoIpAddr))))
    {
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVrrpAssoIpAddrTable
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId
                VrrpAssoIpAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVrrpAssoIpAddrTable (INT4 *pi4IfIndex, INT4 *pi4VrrpOperVrId,
                                     UINT4 *pu4VrrpAssoIpAddr)
{
    return (nmhGetNextIndexVrrpAssoIpAddrTable (0, pi4IfIndex,
                                                0, pi4VrrpOperVrId,
                                                0, pu4VrrpAssoIpAddr));
}

/****************************************************************************
 Function    :  nmhGetNextIndexVrrpAssoIpAddrTable
 Input       :  The Indices
                i4IfIndex
                nexti4IfIndex
                VrrpOperVrId
                nextVrrpOperVrId
                VrrpAssoIpAddr
                nextVrrpAssoIpAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVrrpAssoIpAddrTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                    INT4 i4VrrpOperVrId,
                                    INT4 *pi4NextVrrpOperVrId,
                                    UINT4 u4VrrpAssoIpAddr,
                                    UINT4 *pu4NextVrrpAssoIpAddr)
{
    tIPvXAddr           IpAddr;
    tIpAddrTable       *pIpAddrEntry = NULL;
    INT4                i4OperIndex = VRRP_NOT_OK;
    BOOL1               b1VersionMatched = FALSE;

    VRRP_IPVX_ADDR_CLEAR (IpAddr);
    VRRP_IPVX_COPY_TO_IPVX (IpAddr, u4VrrpAssoIpAddr);

    b1VersionMatched = VrrpCompCheckVersion (VRRP_VERSION_2);

    if (b1VersionMatched == FALSE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    i4OperIndex = VrrpDbGetOperEntry (i4IfIndex, i4VrrpOperVrId,
                                      (INT4) VRRP_IPVX_ADDR_FMLY_IPV4);

    if (i4OperIndex > VRRP_NOT_OK)
    {
        pIpAddrEntry = VrrpDbOperGetNextAssocIpEntry (i4OperIndex, IpAddr);

        if (pIpAddrEntry != NULL)
        {
            *pi4NextIfIndex = i4IfIndex;
            *pi4NextVrrpOperVrId = i4VrrpOperVrId;
            VRRP_IPVX_COPY_TO_IPV4 (*pu4NextVrrpAssoIpAddr,
                                    pIpAddrEntry->AssoIpAddr);

            return ((INT1) SNMP_SUCCESS);
        }
    }

    if (VrrpDbGetNextV2OperEntry (i4IfIndex, i4VrrpOperVrId, pi4NextIfIndex,
                                  pi4NextVrrpOperVrId) == VRRP_NOT_OK)
    {
        return ((INT1) SNMP_FAILURE);
    }

    i4OperIndex = VrrpDbGetOperEntry (*pi4NextIfIndex, *pi4NextVrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4);

    if (i4OperIndex == VRRP_NOT_OK)
    {
        return ((INT1) SNMP_FAILURE);
    }

    VRRP_IPVX_ADDR_CLEAR (IpAddr);

    pIpAddrEntry = VrrpDbOperGetNextAssocIpEntry (i4OperIndex, IpAddr);

    if (pIpAddrEntry == NULL)
    {
        return ((INT1) SNMP_FAILURE);
    }

    VRRP_IPVX_COPY_TO_IPV4 (*pu4NextVrrpAssoIpAddr, pIpAddrEntry->AssoIpAddr);

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVrrpAssoIpAddrRowStatus
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId
                VrrpAssoIpAddr

                The Object
                retValVrrpAssoIpAddrRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpAssoIpAddrRowStatus (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                               UINT4 u4VrrpAssoIpAddr,
                               INT4 *pi4RetValVrrpAssoIpAddrRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1Ipv4Addr[IPVX_IPV4_ADDR_LEN];
    UINT4               u4IpAddr = 0;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    MEMSET (au1Ipv4Addr, 0, IPVX_IPV4_ADDR_LEN);

    IpAddr.pu1_OctetList = au1Ipv4Addr;
    IpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = OSIX_HTONL (u4VrrpAssoIpAddr);
    MEMCPY (IpAddr.pu1_OctetList, &u4IpAddr, IPVX_IPV4_ADDR_LEN);

    i4RetValue = VrrpGetAllAssocIpTable (VRRP_VERSION_2, i4IfIndex,
                                         i4VrrpOperVrId,
                                         VRRP_IPVX_ADDR_FMLY_IPV4, &IpAddr,
                                         VRRP_MIB_ASSO_ROW_STATUS, &Data);

    *pi4RetValVrrpAssoIpAddrRowStatus = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVrrpAssoIpAddrRowStatus
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId
                VrrpAssoIpAddr

                The Object
                setValVrrpAssoIpAddrRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpAssoIpAddrRowStatus (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                               UINT4 u4VrrpAssoIpAddr,
                               INT4 i4SetValVrrpAssoIpAddrRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1Ipv4Addr[IPVX_IPV4_ADDR_LEN];
    UINT4               u4IpAddr = 0;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    MEMSET (au1Ipv4Addr, 0, IPVX_IPV4_ADDR_LEN);

    IpAddr.pu1_OctetList = au1Ipv4Addr;
    IpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = OSIX_HTONL (u4VrrpAssoIpAddr);
    MEMCPY (IpAddr.pu1_OctetList, &u4IpAddr, IPVX_IPV4_ADDR_LEN);

    Data.i4_SLongValue = i4SetValVrrpAssoIpAddrRowStatus;

    i4RetValue = VrrpSetAllAssocIpTable (VRRP_VERSION_2, i4IfIndex,
                                         i4VrrpOperVrId,
                                         VRRP_IPVX_ADDR_FMLY_IPV4, &IpAddr,
                                         VRRP_MIB_ASSO_ROW_STATUS, &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VrrpAssoIpAddrRowStatus
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId
                VrrpAssoIpAddr

                The Object
                testValVrrpAssoIpAddrRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpAssoIpAddrRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                  INT4 i4VrrpOperVrId, UINT4 u4VrrpAssoIpAddr,
                                  INT4 i4TestValVrrpAssoIpAddrRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT1               au1Ipv4Addr[IPVX_IPV4_ADDR_LEN];
    UINT4               u4IpAddr = 0;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    MEMSET (au1Ipv4Addr, 0, IPVX_IPV4_ADDR_LEN);

    IpAddr.pu1_OctetList = au1Ipv4Addr;
    IpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;

    u4IpAddr = OSIX_HTONL (u4VrrpAssoIpAddr);
    MEMCPY (IpAddr.pu1_OctetList, &u4IpAddr, IPVX_IPV4_ADDR_LEN);

    Data.i4_SLongValue = i4TestValVrrpAssoIpAddrRowStatus;

    i4RetValue = VrrpTestAllAssocIpTable (VRRP_VERSION_2, i4IfIndex,
                                          i4VrrpOperVrId,
                                          VRRP_IPVX_ADDR_FMLY_IPV4,
                                          &IpAddr, VRRP_MIB_ASSO_ROW_STATUS,
                                          &Data, pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VrrpAssoIpAddrTable
 Input       :  The Indices
                IfIndex
                VrrpOperVrId
                VrrpAssoIpAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VrrpAssoIpAddrTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : VrrpRouterStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVrrpRouterStatsTable
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVrrpRouterStatsTable (INT4 i4IfIndex,
                                              INT4 i4VrrpOperVrId)
{
    return (nmhValidateIndexInstanceVrrpOperTable (i4IfIndex, i4VrrpOperVrId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVrrpRouterStatsTable
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVrrpRouterStatsTable (INT4 *pi4IfIndex, INT4 *pi4VrrpOperVrId)
{
    return (nmhGetFirstIndexVrrpOperTable (pi4IfIndex, pi4VrrpOperVrId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexVrrpRouterStatsTable
 Input       :  The Indices
                i4IfIndex
                nexti4IfIndex
                VrrpOperVrId
                nextVrrpOperVrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVrrpRouterStatsTable (INT4 i4IfIndex, INT4 *pi4Nexti4IfIndex,
                                     INT4 i4VrrpOperVrId,
                                     INT4 *pi4NextVrrpOperVrId)
{
    return (nmhGetNextIndexVrrpOperTable (i4IfIndex, pi4Nexti4IfIndex,
                                          i4VrrpOperVrId, pi4NextVrrpOperVrId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVrrpStatsBecomeMaster
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsBecomeMaster
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsBecomeMaster (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                             UINT4 *pu4RetValVrrpStatsBecomeMaster)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_MASTER_TRANS, &Data);

    *pu4RetValVrrpStatsBecomeMaster = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsVrrpSemFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId, UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_VRRPSEMFAIL, &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsVrrpSendNDNeighborAdvtFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                     UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_VRRPSENDNDNEIGHBORADVTFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsVrrpSendPacketToIpv4Fail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                   UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_VRRPSENDPACKETTOIPV4FAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsVrrpGetIpv4PortFromOperIndexFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                           UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_VRRPGETIPV4PORTFROMOPERINDEXFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsVrrpSendGratArpFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                              UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_VRRPSENDGRATARPFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsEthRegisterMacAddrFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                 UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_VRRPETHREGISTERMACADDRFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsVrrpNetIpv4DeleteIntfFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                    UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_VRRPNETIP4DELINTFFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsVrrpIpifJoinMcastGroupFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                     UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_VRRPIPIFJOINMCASTGROUPFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsVrrpIpifLeaveMcastGroupFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                      UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_VRRPIPIFLEAVEMCASTGROUPFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsVrrpSocketFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId, UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_VRRPSOCKETFAIL, &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsPktProcessValidateAdverPacketFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                            UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERPACKETFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsPktProcessValidateAdverV2PacketFail (INT4 i4IfIndex,
                                              INT4 i4VrrpOperVrId,
                                              UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERV2PACKETFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsPktProcessValidateAdverV3PacketFail (INT4 i4IfIndex,
                                              INT4 i4VrrpOperVrId,
                                              UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERV3PACKETFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsPktProcessTransmitV2PacketFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                         UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_PKTPROCESSTRANSMITV2PACKETFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
VrrpStatsPktProcessTransmitV3PacketFail (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                         UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_PKTPROCESSTRANSMITV3PACKETFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsAdvertiseRcvd
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsAdvertiseRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsAdvertiseRcvd (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                              UINT4 *pu4RetValVrrpStatsAdvertiseRcvd)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_RCVD_ADVT, &Data);

    *pu4RetValVrrpStatsAdvertiseRcvd = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsAdvertiseIntervalErrors
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsAdvertiseIntervalErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsAdvertiseIntervalErrors (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                        UINT4
                                        *pu4RetValVrrpStatsAdvertiseIntervalErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_ADVT_INT_ERR, &Data);

    *pu4RetValVrrpStatsAdvertiseIntervalErrors = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsAuthFailures
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsAuthFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetVrrpStatsAuthFailures ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetVrrpStatsAuthFailures (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                             UINT4 *pu4RetValVrrpStatsAuthFailures)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_AUTH_FAILURES, &Data);

    *pu4RetValVrrpStatsAuthFailures = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsIpTtlErrors
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsIpTtlErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsIpTtlErrors (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                            UINT4 *pu4RetValVrrpStatsIpTtlErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_IP_TTL_ERR, &Data);

    *pu4RetValVrrpStatsIpTtlErrors = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsPriorityZeroPktsRcvd
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsPriorityZeroPktsRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsPriorityZeroPktsRcvd (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                     UINT4
                                     *pu4RetValVrrpStatsPriorityZeroPktsRcvd)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_RCVD_ZERO_PRI, &Data);

    *pu4RetValVrrpStatsPriorityZeroPktsRcvd = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsPriorityZeroPktsSent
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsPriorityZeroPktsSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsPriorityZeroPktsSent (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                     UINT4
                                     *pu4RetValVrrpStatsPriorityZeroPktsSent)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_SENT_ZERO_PRI, &Data);

    *pu4RetValVrrpStatsPriorityZeroPktsSent = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsInvalidTypePktsRcvd
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsInvalidTypePktsRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsInvalidTypePktsRcvd (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                    UINT4
                                    *pu4RetValVrrpStatsInvalidTypePktsRcvd)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_RCVD_INVD_TYPE, &Data);

    *pu4RetValVrrpStatsInvalidTypePktsRcvd = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsAddressListErrors
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsAddressListErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsAddressListErrors (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                  UINT4 *pu4RetValVrrpStatsAddressListErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_ADDR_LIST_ERR, &Data);

    *pu4RetValVrrpStatsAddressListErrors = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsInvalidAuthType
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsInvalidAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsInvalidAuthType (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                UINT4 *pu4RetValVrrpStatsInvalidAuthType)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_INVALID_AUTH, &Data);

    *pu4RetValVrrpStatsInvalidAuthType = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsAuthTypeMismatch
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsAuthTypeMismatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsAuthTypeMismatch (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                 UINT4 *pu4RetValVrrpStatsAuthTypeMismatch)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_AUTH_TYPE_MISMATCH, &Data);

    *pu4RetValVrrpStatsAuthTypeMismatch = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpStatsPacketLengthErrors
 Input       :  The Indices
                i4IfIndex
                VrrpOperVrId

                The Object
                retValVrrpStatsPacketLengthErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpStatsPacketLengthErrors (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                   UINT4 *pu4RetValVrrpStatsPacketLengthErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_STAT_PKT_LEN_ERR, &Data);

    *pu4RetValVrrpStatsPacketLengthErrors = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVrrpNodeVersion
 Input       :  The Indices

                The Object
                retValVrrpNodeVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpNodeVersion (INT4 *pi4RetValVrrpNodeVersion)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_2, VRRP_MIB_VERSION, &Data);

    *pi4RetValVrrpNodeVersion = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetVrrpNotificationCntl
 Input       :  The Indices

                The Object
                retValVrrpNotificationCntl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpNotificationCntl (INT4 *pi4RetValVrrpNotificationCntl)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_2, VRRP_MIB_NOTIF_CTRL, &Data);

    *pi4RetValVrrpNotificationCntl = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVrrpNotificationCntl
 Input       :  The Indices

                The Object
                setValVrrpNotificationCntl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpNotificationCntl (INT4 i4SetValVrrpNotificationCntl)
{
    gi4VrrpNotificationCntl = (UINT1) i4SetValVrrpNotificationCntl;
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2VrrpNotificationCntl
 Input       :  The Indices

                The Object
                testValVrrpNotificationCntl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2VrrpNotificationCntl (UINT4 *pu4ErrorCode,
                               INT4 i4TestValVrrpNotificationCntl)
{
    if ((i4TestValVrrpNotificationCntl == VRRP_ENABLE) ||
        (i4TestValVrrpNotificationCntl == VRRP_DISABLE))
    {
        return (SNMP_SUCCESS);
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_VRRP_INVALID_VALUE);
    return (SNMP_FAILURE);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2VrrpNotificationCntl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2VrrpNotificationCntl (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVrrpRouterChecksumErrors
 Input       :  The Indices

                The Object
                retValVrrpRouterChecksumErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpRouterChecksumErrors (UINT4 *pu4RetValVrrpRouterChecksumErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_2, VRRP_MIB_CHECKSUM_ERRORS, &Data);

    *pu4RetValVrrpRouterChecksumErrors = Data.u4_ULongValue;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetVrrpRouterVersionErrors
 Input       :  The Indices

                The Object
                retValVrrpRouterVersionErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpRouterVersionErrors (UINT4 *pu4RetValVrrpRouterVersionErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_2, VRRP_MIB_VERSION_ERRORS, &Data);

    *pu4RetValVrrpRouterVersionErrors = Data.u4_ULongValue;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetVrrpRouterVrIdErrors
 Input       :  The Indices

                The Object
                retValVrrpRouterVrIdErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpRouterVrIdErrors (UINT4 *pu4RetValVrrpRouterVrIdErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_2, VRRP_MIB_VRID_ERRORS, &Data);

    *pu4RetValVrrpRouterVrIdErrors = Data.u4_ULongValue;

    return ((INT1) SNMP_SUCCESS);
}
