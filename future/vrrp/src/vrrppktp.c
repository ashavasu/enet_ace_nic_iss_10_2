/********************************************************************
 * $Id: vrrppktp.c,v 1.27 2017/12/16 11:57:12 siva Exp $          *
 *                                                                  *
 * $RCSfile: vrrppktp.c,v $
 *                                                                  *
 * $Date: 2017/12/16 11:57:12 $
 *                                                                  *
 * $Revision: 1.27 $
 *                                                                  *
 *******************************************************************/

/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrppktp.c                                   |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Core Protocol.                          |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : PktProcessing Module of VRRP Core Protocol.   |
 * | PreProcesing of Vrrp Packets received from the Lower Interface is done   |
 * | here. It also includes processes packets before transmission to Lower    |
 * | Layer. Update of Protocol Statistics is done here.                       |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#define PKTPROCESS_MODULE
#include "vrrpinc.h"
#include "snmputil.h"

/*******************************************************************************
*Function Name   :   PktProcessValidateAdverPacket                        
*Description     :   This Funtion is called from the VRRPReceivePacket Module
*                    This function Validates the VRRP Adver 
*                    Packet inaccordance with 
*                    draft-04.  It also  updates the Statistic MIB 
*Global Varibles :  
*                    None         
*Inputs          :   i4IfIndex       - Interface Index over which packet is
*                                      received.
*                    pVrrpAdverPkt   - Pointer to the VRRP Adver packet.
*                    u4DataSize      - Size of VRRP IP Packet
*                    i4AddrType      - Address Type
*                    u1Ttl           - Time to Live or Hop Limit
*Output          :                                                              
*                    None
*Returns         :   Valid Oper Index - If validation is successfull
*                    VRRP_NOT_OK      - If validation is not sucessfull
*******************************************************************************/
INT4
PktProcessValidateAdverPacket (INT4 i4IfIndex, tVrrpAdverPkt * pVrrpAdverPkt,
                               UINT4 u4DataSize, INT4 i4AddrType, UINT1 u1Ttl)
{
    tVrrpAdverHeader   *pVrrpAdverHeader = NULL;
    FS_UINT8            u8ZeroVal;
    INT4                i4VrId = 0;
    INT4                i4OperIndex = VRRP_NOT_OK;
    UINT2               u2Checksum = 0;
    UINT2               u2RcvdChecksum = 0;
    UINT1               u1Version = 0;
    UINT1               u1Type = 0;
    INT1                i1RetVal = VRRP_NOT_OK;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];
    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);

    FSAP_U8_CLR (&u8ZeroVal);

    pVrrpAdverHeader = pVrrpAdverPkt->pHeader;
    u1Version = (UINT1) ((pVrrpAdverHeader->u1VersionAndType & 0xF0) >> 4);
    u1Type = pVrrpAdverHeader->u1VersionAndType & 0x0F;

    i4VrId = (INT4) pVrrpAdverHeader->u1VrId;

    i4OperIndex = PktProcessGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);

    if (i4OperIndex == VRRP_NOT_OK)
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                    VRRP_TRC_VERSION_3),
                   "Oper Entry for IfIndex %d VrId %d Address Type %s not "
                   "found\n", i4IfIndex, i4VrId, ac1AddrType);

        PktProcessUpdateV2V3Statistics (0, (UINT1) VRRPROUTVRIDERRORS,
                                        (UINT1) VRRP_VRID_ERROR);

        PktProcessUpdateV2V3Statistics (0, PKTPROCESSVALIDATEADVERPACKETFAIL,
                                        0);
        return VRRP_NOT_OK;
    }

    /* Value VRRP_VERSION_2_3 refers to VRRP Version 2 */
    if (u1Version == VRRP_VERSION_2_3)
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2),
                   "Received V2 Advertisement Packet for IfIndex %d"
                   " VrId %d Address Type %s\n", i4IfIndex, i4VrId,
                   ac1AddrType);
        if (gi4VrrpNodeVersion == VRRP_VERSION_3)
        {
            VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                        VRRP_TRC_VERSION_3),
                       "Supported Version is 3. V2 Packet for IfIndex %d"
                       " VrId %d Address Type %s dropped\n",
                       i4IfIndex, i4VrId, ac1AddrType);
            gi4VrrpTxRxVersion = VRRP_VERSION_3;
            PktProcessUpdateStatistics (i4OperIndex, VRRPROUTVERSIONERRORS);
            PktProcessSetAndNotifyStats (i4OperIndex, VRRPSTATSPROTOERRREASON,
                                         (UINT1) VRRP_VERSION_ERROR);
            PktProcessUpdateStatistics (i4OperIndex,
                                        PKTPROCESSVALIDATEADVERPACKETFAIL);

            return VRRP_NOT_OK;
        }
        else if (OPERV3ADVTRCVD (i4OperIndex) == TRUE)
        {
            VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                        VRRP_TRC_VERSION_3),
                       "V3 Advertisement also received for for IfIndex %d"
                       " VrId %d Address Type %s. V2 Packet Ignored\n",
                       i4IfIndex, i4VrId, ac1AddrType);

            /* If V2 Packets are received from the router in which V3 packets 
             * are also received, ignore V2 packets. This is as per the 
             * RFC 5798 Section 8.4.2. */
            gi4VrrpTxRxVersion = VRRP_VERSION_3;
            PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSV2ADVTIGNORED);
            PktProcessUpdateStatistics (i4OperIndex,
                                        PKTPROCESSVALIDATEADVERPACKETFAIL);

            return VRRP_NOT_OK;
        }
        else
        {
            gi4VrrpTxRxVersion = VRRP_VERSION_2_3;
        }
    }
    else if (u1Version == VRRP_VERSION_3)
    {
        VRRP_DBG3 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_3),
                   "Received V3 Advertisement Packet for IfIndex %d"
                   " VrId %d Address Type %s\n", i4IfIndex, i4VrId,
                   ac1AddrType);
        if (gi4VrrpNodeVersion == VRRP_VERSION_2)
        {
            VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                        VRRP_VERSION_3),
                       "Supported Version is 2. V3 Packet for IfIndex %d"
                       " VrId %d Address Type %s dropped\n",
                       i4IfIndex, i4VrId, ac1AddrType);

            gi4VrrpTxRxVersion = VRRP_VERSION_2_3;
            PktProcessUpdateStatistics (i4OperIndex, VRRPROUTVERSIONERRORS);

            PktProcessSetAndNotifyStats (i4OperIndex, VRRPSTATSPROTOERRREASON,
                                         (UINT1) VRRP_VERSION_ERROR);
            PktProcessUpdateStatistics (i4OperIndex,
                                        PKTPROCESSVALIDATEADVERPACKETFAIL);

            return VRRP_NOT_OK;
        }
        else
        {
            gi4VrrpTxRxVersion = VRRP_VERSION_3;
        }
    }
    else
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                    VRRP_TRC_VERSION_3),
                   "Received Advertisement Packet with unknown version for "
                   "IfIndex %d VrId %d Address Type %s - Packet Dropped\n",
                   i4IfIndex, i4VrId, ac1AddrType);

        PktProcessUpdateV2V3Statistics (i4OperIndex,
                                        (UINT1) VRRPROUTVERSIONERRORS,
                                        (UINT1) VRRP_VERSION_ERROR);

        PktProcessSetAndNotifyStats (i4OperIndex, VRRPSTATSPROTOERRREASON,
                                     (UINT1) VRRP_VERSION_ERROR);
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSVALIDATEADVERPACKETFAIL);

        return VRRP_NOT_OK;
    }

    if (u1Ttl != VRRP_TTL)
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                    VRRP_TRC_VERSION_3),
                   "TTL incorrect in received packet for "
                   "IfIndex %d VrId %d Address Type %s - Packet Dropped\n",
                   i4IfIndex, i4VrId, ac1AddrType);

        PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSIPTTLERRORS);

        PktProcessSetAndNotifyStats (i4OperIndex,
                                     (UINT1) VRRPSTATSPROTOERRREASON,
                                     (UINT1) VRRP_TTL_ERROR);
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSVALIDATEADVERPACKETFAIL);

        return VRRP_NOT_OK;
    }

    if (u1Type != VRRP_ADVERTISEMENT)
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                    VRRP_TRC_VERSION_3),
                   "Invalid Advertisement Type Received for "
                   "IfIndex %d VrId %d Address Type %s - Packet Dropped\n",
                   i4IfIndex, i4VrId, ac1AddrType);
        PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSINVALIDTYPEPKTSRCVD);

        PktProcessSetAndNotifyStats (i4OperIndex,
                                     (UINT1) VRRPSTATSPROTOERRREASON,
                                     (UINT1) VRRP_INVALID_TYPE_ERROR);
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSVALIDATEADVERPACKETFAIL);

        return VRRP_NOT_OK;
    }

    u2Checksum = pVrrpAdverHeader->u2CheckSum;
    pVrrpAdverHeader->u2CheckSum = 0;

    if (gi4VrrpTxRxVersion == VRRP_VERSION_2_3)
    {
        u2RcvdChecksum =
            PktProcessCalculateV2CheckSum (pVrrpAdverPkt, (UINT1) i4AddrType);
    }
    else
    {
        u2RcvdChecksum =
            PktProcessCalculateV3CheckSum (pVrrpAdverPkt, (UINT1) i4AddrType,
                                           gSenderIpAddr);
    }

    if (u2Checksum != u2RcvdChecksum)
    {
        VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2 | VRRP_TRC_VERSION_3),
                   "Checksum in Packet: %#x, Computed Checksum: %#x\n",
                   u2Checksum, u2RcvdChecksum);

        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                    VRRP_TRC_VERSION_3),
                   "Checksum incorrect in received packet for "
                   "IfIndex %d VrId %d Address Type %s - Packet Dropped\n",
                   i4IfIndex, i4VrId, ac1AddrType);
        PktProcessUpdateStatistics (i4OperIndex, VRRPROUTCHECKSUMERRORS);

        PktProcessSetAndNotifyStats (i4OperIndex,
                                     (UINT1) VRRPSTATSPROTOERRREASON,
                                     (UINT1) VRRP_CHECKSUM_ERROR);
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSVALIDATEADVERPACKETFAIL);
        return VRRP_NOT_OK;
    }

    if ((OPERISOWNER (i4OperIndex) == TRUE) &&
        (OPERTRACKIFSTATUS (i4OperIndex) != VRRP_TRACK_IF_DOWN))
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                    VRRP_TRC_VERSION_3),
                   "Advertisement Packet received by Owner for "
                   "IfIndex %d VrId %d Address Type %s - Packet Dropped\n",
                   i4IfIndex, i4VrId, ac1AddrType);

        PktProcessUpdateStatistics (i4OperIndex, VRRPROUTVRIDERRORS);

        PktProcessSetAndNotifyStats (i4OperIndex,
                                     (UINT1) VRRPSTATSPROTOERRREASON,
                                     (UINT1) VRRP_VRID_ERROR);
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSVALIDATEADVERPACKETFAIL);

        return VRRP_NOT_OK;
    }

    if (gi4VrrpTxRxVersion == VRRP_VERSION_2_3)
    {
        i1RetVal = PktProcessValidateAdverV2Packet (i4OperIndex, pVrrpAdverPkt,
                                                    u4DataSize);
    }
    else
    {
        i1RetVal = PktProcessValidateAdverV3Packet (i4OperIndex, pVrrpAdverPkt,
                                                    u4DataSize);
    }

    if (i1RetVal == VRRP_NOT_OK)
    {
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSVALIDATEADVERPACKETFAIL);
        return VRRP_NOT_OK;
    }

    if (PktProcessValidateIpAddrs (i4OperIndex, pVrrpAdverPkt) == VRRP_NOT_OK)
    {
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSVALIDATEADVERPACKETFAIL);
        return VRRP_NOT_OK;
    }

    if (pVrrpAdverHeader->u1Priority == ZERO)
    {
        VRRP_DBG3 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2 | VRRP_TRC_VERSION_3),
                   "Priority Zero received packet for "
                   "IfIndex %d VrId %d Address Type %s\n",
                   i4IfIndex, i4VrId, ac1AddrType);
        PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSPRIORITYZEROPKTSRCVD);
    }

    PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSADVERTISERCVD);

    VRRP_DBG3 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2 | VRRP_TRC_VERSION_3),
               "No Errors observed in received packet for "
               "IfIndex %d VrId %d Address Type %s\n",
               i4IfIndex, i4VrId, ac1AddrType);

    return i4OperIndex;
}

/*******************************************************************************
*Function Name   :   PktProcessValidateAdverV2Packet                        
*Description     :   This Funtion is called from the VRRPReceivePacket Module
*                    This function Validates the VRRP Advertisement in 
*                    VRRP version 2 format as mentioned in RFC 3768.
*
*Inputs          :   i4OperIndex     - Oper Index
*                    pVrrpAdverPkt   - Pointer to the VRRP Adver packet.
*                    u4DataSize      - Data Size
*
*Output          :   None
*Returns         :                                                              
*                    VRRP_OK                                                 
*                    VRRP_NOT_OK                                
*******************************************************************************/
INT1
PktProcessValidateAdverV2Packet (INT4 i4OperIndex,
                                 tVrrpAdverPkt * pVrrpAdverPkt,
                                 UINT4 u4DataSize)
{
    tVrrpAdverHeader   *pVrrpAdverHeader = NULL;
    UINT4               u4Time = 0;
    INT4                i4AdvtInterval = 0;
    UINT1               u1AuthType = 0;
    UINT1               u1AdvtInterval = 0;
    UINT4               u4MaxAuthKeyLen = 0;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);

    pVrrpAdverHeader = pVrrpAdverPkt->pHeader;

    pVrrpAdverHeader->u2AuthTypeOrAdverInt
        = OSIX_NTOHS (pVrrpAdverHeader->u2AuthTypeOrAdverInt);

    u1AuthType
        = (UINT1) ((pVrrpAdverHeader->u2AuthTypeOrAdverInt & 0xFF00) >> 8);
    u1AdvtInterval = (UINT1) (pVrrpAdverHeader->u2AuthTypeOrAdverInt & 0x00FF);

    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);

    /* Check the Packet Size */
    if (u4DataSize < MIN_ADVERPACKET_SIZE)
    {
        VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2),
                   "Received Data Size: %d Supported Size: %d\n",
                   u4DataSize, MIN_ADVERPACKET_SIZE);

        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2),
                   "Packet Length mismatch in received packet for "
                   "IfIndex %d VrId %d Address Type %s - Packet Dropped\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSPACKETLENGTHERRORS);

        PktProcessSetAndNotifyStats (i4OperIndex,
                                     (UINT1) VRRPSTATSPROTOERRREASON,
                                     (UINT1) VRRP_PACKET_LENGTH_ERROR);
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSVALIDATEADVERV2PACKETFAIL);

        return VRRP_NOT_OK;
    }

    if (u1AuthType > HMAC_IP_AUTHENTICATION_PROT_VAL)
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2),
                   "Invalid Authentication Type in received packet for "
                   "IfIndex %d VrId %d Address Type %s - Packet Dropped\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);

        PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSINVALIDAUTHTYPE);

        PktProcessSetAndNotifyStats (i4OperIndex,
                                     (UINT1) VRRPSTATSAUTHERRORTYPE,
                                     (UINT1) VRRP_INVALID_AUTH_TYPE_ERROR);
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSVALIDATEADVERV2PACKETFAIL);

        return VRRP_NOT_OK;
    }

    /* Convert received auth type into MIB value and then do validation */
    if ((u1AuthType + 1) != OPERAUTHTYPE (i4OperIndex))
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2),
                   "Authentication mismatch in received packet for "
                   "IfIndex %d VrId %d Address Type %s - Packet Dropped\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);

        PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSAUTHTYPEMISMATCH);

        PktProcessSetAndNotifyStats (i4OperIndex,
                                     (UINT1) VRRPSTATSAUTHERRORTYPE,
                                     (UINT1) VRRP_AUTH_TYPE_MISMATCH_ERROR);
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSVALIDATEADVERV2PACKETFAIL);

        return VRRP_NOT_OK;
    }

    if (u1AuthType == SIMPLE_TEXT_AUTHENTICATION_PROT_VAL)
    {
        if (OPERAUTHKEY (i4OperIndex).i4_Length >=
            (INT4) STRLEN (pVrrpAdverPkt->pAuthenticationData))
        {
            u4MaxAuthKeyLen = (UINT4) OPERAUTHKEY (i4OperIndex).i4_Length;
        }
        else
        {
            u4MaxAuthKeyLen = STRLEN (pVrrpAdverPkt->pAuthenticationData);
        }

        /* Changed the Memcmp params to vlaidate the key */
        if (VRRP_MEMCMP (pVrrpAdverPkt->pAuthenticationData,
                         (OPERAUTHKEY (i4OperIndex).pu1_OctetList),
                         u4MaxAuthKeyLen) != ZERO)
        {
            VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2),
                       "Authentication Key mismatch in received packet for "
                       "IfIndex %d VrId %d Address Type %s - Packet Dropped\n",
                       OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                       ac1AddrType);
            PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSAUTHFAILURES);

            PktProcessSetAndNotifyStats (i4OperIndex,
                                         (UINT1) VRRPSTATSAUTHERRORTYPE,
                                         (UINT1) VRRP_AUTH_FAILURE_ERROR);
            PktProcessUpdateStatistics (i4OperIndex,
                                        PKTPROCESSVALIDATEADVERV2PACKETFAIL);

            return VRRP_NOT_OK;
        }
    }

    if (OPERADVERINTL (i4OperIndex) < V2_ADVERTISEMENTINTLINMSEC)
    {
        u4Time = 1;
    }
    else
    {
        u4Time
            =
            (UINT4) (OPERADVERINTL (i4OperIndex) / V2_ADVERTISEMENTINTLINMSEC);
    }

    if (u1AdvtInterval != (UINT1) u4Time)
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2),
                   "Advertisement Interval mismatch in received packet for "
                   "IfIndex %d VrId %d Address Type %s\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);

        PktProcessUpdateStatistics (i4OperIndex,
                                    VRRPSTATSADVERTISEINTERVALERRORS);

        PktProcessSetAndNotifyStats (i4OperIndex,
                                     (UINT1) VRRPSTATSPROTOERRREASON,
                                     (UINT1) VRRP_ADVT_INT_ERROR);

        /* This packet should be dropped only if version 2 alone is supported.
         *
         * If version 3 is also supported, store the received advertisement
         * interval in v3's format i.e., convert value received in seconds to
         * centiseconds (here, we are converting in milliseconds as required by
         * our internal calculation. */
        if (gi4VrrpNodeVersion == VRRP_VERSION_2)
        {
            VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2),
                       "Supported Version is only V2. Advertisement Interval "
                       "mismatch packet for IfIndex %d VrId %d Address Type %s "
                       "Dropped\n",
                       OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                       ac1AddrType);
            PktProcessUpdateStatistics (i4OperIndex,
                                        PKTPROCESSVALIDATEADVERV2PACKETFAIL);
            return VRRP_NOT_OK;
        }
    }

    i4AdvtInterval = (INT4) (u1AdvtInterval * V2_ADVERTISEMENTINTLINMSEC);

    if ((gi4VrrpNodeVersion != VRRP_VERSION_2) &&
        (i4AdvtInterval != OPERRCVDMASTERADVTINT (i4OperIndex)))
    {
        OPERRCVDMASTERADVTINT (i4OperIndex) = i4AdvtInterval;

        /* Received Master Advertisement Interval changed. Sync this
         * with Standby. */
        VrrpRedDbUtilAddTblNode (&gVrrpDynInfoList, &OPERDBNODE (i4OperIndex));
        VrrpRedSyncDynInfo ();
    }

    return VRRP_OK;
}

/*******************************************************************************
*Function Name   :   PktProcessValidateAdverV3Packet                        
*Description     :   This Funtion is called from the VRRPReceivePacket Module
*                    This function Validates the VRRP Advertisement in 
*                    VRRP version 3 format as mentioned in RFC 5798.
*
*Inputs          :   i4OperIndex     - Oper Index
*                    pVrrpAdverPkt   - Pointer to the VRRP Adver packet.
*                    u4DataSize      - Data Size
*
*Output          :   None
*Returns         :                                                              
*                    VRRP_OK                                                 
*                    VRRP_NOT_OK                                
*******************************************************************************/
INT1
PktProcessValidateAdverV3Packet (INT4 i4OperIndex,
                                 tVrrpAdverPkt * pVrrpAdverPkt,
                                 UINT4 u4DataSize)
{
    tVrrpAdverHeader   *pVrrpAdverHeader = NULL;
    UINT4               u4Time = 0;
    INT4                i4AdvtInterval = 0;
    UINT1               u1Rsvd = 0;
    UINT2               u2AdvtInterval = 0;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);

    pVrrpAdverHeader = pVrrpAdverPkt->pHeader;

    pVrrpAdverHeader->u2AuthTypeOrAdverInt
        = OSIX_NTOHS (pVrrpAdverHeader->u2AuthTypeOrAdverInt);

    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);

    /* Check the Packet Size */
    if (OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        if (u4DataSize < MIN_V3_IPV4_ADVERPACKET_SIZE)
        {
            VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_3),
                       "Received Data Size: %d Supported Size: %d\n",
                       u4DataSize, MIN_V3_IPV4_ADVERPACKET_SIZE);

            VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_3),
                       "Packet Length mismatch in received packet for "
                       "IfIndex %d VrId %d Address Type %s\n",
                       OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                       ac1AddrType);

            PktProcessUpdateStatistics (i4OperIndex,
                                        VRRPSTATSPACKETLENGTHERRORS);

            PktProcessSetAndNotifyStats (i4OperIndex,
                                         (UINT1) VRRPSTATSPROTOERRREASON,
                                         (UINT1) VRRP_PACKET_LENGTH_ERROR);
            PktProcessUpdateStatistics (i4OperIndex,
                                        PKTPROCESSVALIDATEADVERV3PACKETFAIL);
            return VRRP_NOT_OK;
        }
    }
    else
    {
        if (u4DataSize < MIN_V3_IPV6_ADVERPACKET_SIZE)
        {
            VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_3),
                       "Received Data Size: %d Supported Size: %d\n",
                       u4DataSize, MIN_V3_IPV6_ADVERPACKET_SIZE);

            VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_3),
                       "Packet Length mismatch in received packet for "
                       "IfIndex %d VrId %d Address Type %s\n",
                       OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                       ac1AddrType);

            PktProcessUpdateStatistics (i4OperIndex,
                                        VRRPSTATSPACKETLENGTHERRORS);

            PktProcessSetAndNotifyStats (i4OperIndex,
                                         (UINT1) VRRPSTATSPROTOERRREASON,
                                         (UINT1) VRRP_PACKET_LENGTH_ERROR);
            PktProcessUpdateStatistics (i4OperIndex,
                                        PKTPROCESSVALIDATEADVERV3PACKETFAIL);
            return VRRP_NOT_OK;
        }
    }

    u1Rsvd = (UINT1) ((pVrrpAdverHeader->u2AuthTypeOrAdverInt & 0xF000) >> 12);

    VRRP_DBG4 ((VRRP_TRC_VERSION_3),
               "Rsvd field %d is ignored for IfIndex %d VrId %d Addr Type "
               "%s\r\n",
               u1Rsvd, OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
               ac1AddrType);
    /* Ignore Reserved fields on Reception */

    u2AdvtInterval = (UINT2) (pVrrpAdverHeader->u2AuthTypeOrAdverInt & 0x0FFF);

    u4Time = (UINT4) (OPERADVERINTL (i4OperIndex) / V3_ADVERTISEMENTINTLINMSEC);

    if (u2AdvtInterval != (UINT2) u4Time)
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_3),
                   "Advertisement Interval mismatch in received packet for "
                   "IfIndex %d VrId %d Address Type %s - Error Ignored\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        PktProcessUpdateStatistics (i4OperIndex,
                                    VRRPSTATSADVERTISEINTERVALERRORS);

        PktProcessSetAndNotifyStats (i4OperIndex,
                                     (UINT1) VRRPSTATSPROTOERRREASON,
                                     (UINT1) VRRP_ADVT_INT_ERROR);

        /* No need to throw error if advertisement interval mismatch is seen
         * for VRRP Version 3 packet. Instead just log the error and proceed
         * to process the packet. */
    }

    /* Store the Advertisement interval received in the packet temporarily.
     *
     * This value may be changed as zero later in the SEM if this router is
     * the actual master */
    i4AdvtInterval = (INT4) (u2AdvtInterval * V3_ADVERTISEMENTINTLINMSEC);

    if (OPERRCVDMASTERADVTINT (i4OperIndex) != i4AdvtInterval)
    {
        OPERRCVDMASTERADVTINT (i4OperIndex) = i4AdvtInterval;

        /* Received Master Advertisement Interval changed. Sync this
         * with Standby. */
        VrrpRedDbUtilAddTblNode (&gVrrpDynInfoList, &OPERDBNODE (i4OperIndex));
        VrrpRedSyncDynInfo ();
    }

    return VRRP_OK;
}

/*******************************************************************************
*Function Name     :   PktProcessEncodeV2AdverHeader                         
*Description       :   This function encodes VRRP Advertisement Header in 
*                      Version 2 format in accordance with RFC 3768.    
*
*Inputs            :   i4OperIndex        - Oper Index
*                      b1IsPriorityZero   - Indicates if Priority Zero Packet or
*                                           Not
*
*Output            :   pVrrpAdverPkt  - VRRP Packet with Appropriate Fields   
*Returns           :   None                                                         
*******************************************************************************/
VOID
PktProcessEncodeV2AdverHeader (INT4 i4OperIndex, BOOL1 b1IsPriorityZero,
                               tVrrpAdverPkt * pVrrpAdverPkt)
{
    UINT4               u4Time = 0;
    tVrrpAdverHeader   *pVrrpAdverHeader = NULL;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);
    pVrrpAdverHeader = pVrrpAdverPkt->pHeader;

    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);

    /* Value VRRP_VERSION_2_3 indicates version 2 */
    pVrrpAdverHeader->u1VersionAndType = VRRP_VERSION_2_3;

    pVrrpAdverHeader->u1VersionAndType =
        (UINT1) (pVrrpAdverHeader->u1VersionAndType << 4);

    pVrrpAdverHeader->u1VersionAndType =
        (UINT1) (pVrrpAdverHeader->u1VersionAndType + VRRP_ADVERTISEMENT);

    pVrrpAdverHeader->u1VrId = (UINT1) OPERVRID (i4OperIndex);

    if (b1IsPriorityZero == VRRP_TRUE)
    {
        pVrrpAdverHeader->u1Priority = ZERO;
    }
    else
    {
        pVrrpAdverHeader->u1Priority = (UINT1) OPERPRIORITY (i4OperIndex);
    }

    pVrrpAdverHeader->u1CountIpAddrs = (UINT1) OPERIPADDRCOUNT (i4OperIndex);

    pVrrpAdverHeader->u2AuthTypeOrAdverInt
        = ((UINT1) (OPERAUTHTYPE (i4OperIndex) - 1));

    pVrrpAdverHeader->u2AuthTypeOrAdverInt =
        (UINT2) (pVrrpAdverHeader->u2AuthTypeOrAdverInt << 8);

    /* Minimum Advertisement Interval to be filled in V2 Packet is 1 Second.
     *
     * There are 2 MIB objects in V2, 
     *   1. Taking input in Seconds (1-255) seconds
     *   2. Taking input in milliseconds (100-255000) milliseconds.
     *
     * In data structure, value is maintained only in milliseconds.
     * For the first one conversion to packet format with precision is easy.
     *
     * For the second one conversion to packet format with precision is done
     * as (mib_value / 1000) -> Only Quotient is passed to the packet,
     *
     * If (mib_value is 1600 millisecond, value in packet 1 second (1000
     * milliseconds), this is ok for V2 since Master Down Interval is not
     * calculated based on received value.
     */
    if (OPERADVERINTL (i4OperIndex) < V2_ADVERTISEMENTINTLINMSEC)
    {
        u4Time = 1;
    }
    else
    {
        u4Time
            =
            (UINT4) (OPERADVERINTL (i4OperIndex) / V2_ADVERTISEMENTINTLINMSEC);
    }

    pVrrpAdverHeader->u2AuthTypeOrAdverInt =
        (UINT2) (pVrrpAdverHeader->u2AuthTypeOrAdverInt + u4Time);

    pVrrpAdverHeader->u2AuthTypeOrAdverInt
        = OSIX_HTONS (pVrrpAdverHeader->u2AuthTypeOrAdverInt);

    PktFillAssociatedIpAddresses (i4OperIndex, pVrrpAdverPkt);

    VRRP_MEMSET (pVrrpAdverPkt->pAuthenticationData, 0, TEXT_AUTHKEY_SIZE);
    if (OPERAUTHTYPE (i4OperIndex) == SIMPLE_TEXT_AUTHENTICATION_MIB_VAL)
    {
        VRRP_MEMCPY (pVrrpAdverPkt->pAuthenticationData,
                     (OPERAUTHKEY (i4OperIndex)).pu1_OctetList,
                     (OPERAUTHKEY (i4OperIndex)).i4_Length);
    }

    pVrrpAdverHeader->u2CheckSum = 0;
    pVrrpAdverHeader->u2CheckSum
        = PktProcessCalculateV2CheckSum (pVrrpAdverPkt,
                                         OPERADDRTYPE (i4OperIndex));

    VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2),
               "V2 Advertisement Packet Encoded Successfully for VrId %d "
               "Address Type %sn", OPERVRID (i4OperIndex), ac1AddrType);

    return;
}

/*******************************************************************************
*Function Name     :   PktProcessEncodeV3AdverHeader                         
*Description       :   This function encodes VRRP Advertisement Header in 
*                      Version 3 format in accordance with RFC 5798.    
*
*Inputs            :   i4OperIndex        - Oper Index
*                      b1IsPriorityZero   - Indicates if Priority Zero Packet or
*                                           Not
*
*Output            :   pVrrpAdverPkt  - VRRP Packet with Appropriate Fields   
*Returns           :   None                                                         
*******************************************************************************/
VOID
PktProcessEncodeV3AdverHeader (INT4 i4OperIndex, BOOL1 b1IsPriorityZero,
                               tVrrpAdverPkt * pVrrpAdverPkt)
{
    UINT4               u4Time = 0;
    tVrrpAdverHeader   *pVrrpAdverHeader = NULL;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];
    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);

    pVrrpAdverHeader = pVrrpAdverPkt->pHeader;

    /* Value VRRP_VERSION_3 indicates version 3 */
    pVrrpAdverHeader->u1VersionAndType = VRRP_VERSION_3;

    pVrrpAdverHeader->u1VersionAndType =
        (UINT1) (pVrrpAdverHeader->u1VersionAndType << 4);

    pVrrpAdverHeader->u1VersionAndType =
        (UINT1) (pVrrpAdverHeader->u1VersionAndType + VRRP_ADVERTISEMENT);

    pVrrpAdverHeader->u1VrId = (UINT1) OPERVRID (i4OperIndex);

    if (b1IsPriorityZero == VRRP_TRUE)
    {
        pVrrpAdverHeader->u1Priority = ZERO;
    }
    else
    {
        pVrrpAdverHeader->u1Priority = (UINT1) OPERPRIORITY (i4OperIndex);
    }

    pVrrpAdverHeader->u1CountIpAddrs = (UINT1) OPERIPADDRCOUNT (i4OperIndex);

    /* Fill Rsvd as Zero */
    pVrrpAdverHeader->u2AuthTypeOrAdverInt = 0;

    /* Shift 12 bits to fill Advertisement Interval */
    pVrrpAdverHeader->u2AuthTypeOrAdverInt =
        (UINT2) (pVrrpAdverHeader->u2AuthTypeOrAdverInt << 12);

    /* Minimum Advertisement Interval to be filled in V3 Packet is 1 centisecond.
     *
     * There is only one MIB object in V3, 
     *   1. Taking input in centi seconds (1-4095) centi seconds
     *
     * In data structure, value is maintained only in milliseconds.
     *
     * So, conversion is (mib_value / 10)
     */
    u4Time = (UINT4) (OPERADVERINTL (i4OperIndex) / V3_ADVERTISEMENTINTLINMSEC);

    pVrrpAdverHeader->u2AuthTypeOrAdverInt =
        (UINT2) (pVrrpAdverHeader->u2AuthTypeOrAdverInt + u4Time);

    pVrrpAdverHeader->u2AuthTypeOrAdverInt
        = OSIX_HTONS (pVrrpAdverHeader->u2AuthTypeOrAdverInt);

    PktFillAssociatedIpAddresses (i4OperIndex, pVrrpAdverPkt);

    pVrrpAdverHeader->u2CheckSum = 0;
    pVrrpAdverHeader->u2CheckSum
        = PktProcessCalculateV3CheckSum (pVrrpAdverPkt,
                                         OPERADDRTYPE (i4OperIndex),
                                         INTERFACEIPADDR (i4OperIndex));
    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);
    VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_3),
               "V3 Advertisement Packet Encoded Successfully for VrId %d "
               "Address Type %s\n", OPERVRID (i4OperIndex), ac1AddrType);
    return;
}

/*******************************************************************************
* Function Name     :   PktFillAssociatedIpAddresses
* Description       :   This function fills Associated IP Addresses in VRRP
*                       Packet.
*
* Inputs            :   i4OperIndex        - Oper Index
*
* Output            :   pVrrpAdverPkt  - VRRP Packet with Associated Ip Addresses
*                                        filled
* Returns           :   None                                                         
*******************************************************************************/
VOID
PktFillAssociatedIpAddresses (INT4 i4OperIndex, tVrrpAdverPkt * pVrrpAdverPkt)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;

    UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                         pTempIpAddrEntry, tIpAddrTable *)
    {
        if (pIpAddrEntry->u1AssoIpAddrRowStatus != (UINT1) ACTIVE)
        {
            continue;
        }

        if (OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4)
        {
            VRRP_MEMCPY (pVrrpAdverPkt->pIpAddress,
                         pIpAddrEntry->AssoIpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
            pVrrpAdverPkt->pIpAddress += IPVX_IPV4_ADDR_LEN;
        }
        else
        {
            VRRP_MEMCPY (pVrrpAdverPkt->pIpAddress,
                         pIpAddrEntry->AssoIpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            pVrrpAdverPkt->pIpAddress += IPVX_IPV6_ADDR_LEN;
        }
    }

    return;
}

/*******************************************************************************
*Function Name      :   PktProcessValidateIpAddrs                         
*Description        :   This Funtion is called to Validate the IP Address in the
*                       Adver Pkt.            
*Global Varibles    :   None.                                                   
*Inputs             :                                                           
*                       i4OperIndex      -    Index of OperEntryTable
*                       pVrrpAdverPkt    -     Pointer to VRRP Adver Packet
* 
*Output             :                                                           
*                       None                                                    
*Returns            :                                                           
*                       VALID                                                   
*                       INVALID                                                 
*******************************************************************************/
INT1
PktProcessValidateIpAddrs (INT4 i4OperIndex, tVrrpAdverPkt * pVrrpAdverPkt)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;
    UINT1               u1MibIpCount = 0;
    UINT1               u1AdverIpCount = 0;
    UINT1               u1Counter1 = 0;
    UINT1               u1Counter2 = 0;
    tVrrpIPvXAddr       IpAddr;
    UINT1              *pIpAddress = NULL;
    BOOL1               b1IsErrorObserved = FALSE;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);

    pIpAddress = pVrrpAdverPkt->pIpAddress;
    VRRP_IPVX_ADDR_CLEAR (IpAddr);
    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);

    /* includes Master and Associate Ip Address  */

    u1AdverIpCount = pVrrpAdverPkt->pHeader->u1CountIpAddrs;

    /*Only Asso Ip Addr count */

    u1MibIpCount = (UINT1) OPERIPADDRCOUNT (i4OperIndex);

    /*
     * To validate if the Number of Ip Address in the Adver Packet
     * equal those configured
     * in the Router.
     */

    if (u1MibIpCount != u1AdverIpCount)
    {
        VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                    VRRP_TRC_VERSION_3),
                   "IP Address Count Mismatch in received packet for "
                   "IfIndex %d VrId %d Address Type %s - Error Ignored\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        b1IsErrorObserved = TRUE;
        /* No need to discard the packet */
    }

    for (u1Counter1 = 0; u1Counter1 < u1AdverIpCount; u1Counter1++)
    {
        u1Counter2 = 0;

        UTL_DLL_OFFSET_SCAN (&OPERASSOCIPLIST (i4OperIndex), pIpAddrEntry,
                             pTempIpAddrEntry, tIpAddrTable *)
        {
            u1Counter2++;

            VRRP_IPVX_ADDR_CLEAR (IpAddr);
            VRRP_IPVX_ADDR_INIT_IPV4 (IpAddr, pIpAddress);

            if ((pIpAddrEntry->u1AssoIpAddrRowStatus == (UINT1) ACTIVE) &&
                (VRRP_IPVX_COMPARE (IpAddr, pIpAddrEntry->AssoIpAddr) == 0))
            {
                break;
            }
        }

        if (u1Counter2 <= u1MibIpCount)
        {                        /* Address Match found in Mib */
            pIpAddress++;
            continue;
        }
        else
        {                        /*  Address Match NOT found in Mib   */

            VRRP_DBG3 ((VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_2 |
                        VRRP_TRC_VERSION_3),
                       "IP Address in received packet for "
                       "IfIndex %d VrId %d Address Type %s not present "
                       "- Error Ignored\n",
                       OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                       ac1AddrType);
            b1IsErrorObserved = TRUE;
            /* No need to discard the packet */
        }
    }

    if (b1IsErrorObserved == TRUE)
    {
        PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSADDRESSLISTERRORS);

        PktProcessSetAndNotifyStats (i4OperIndex, VRRPSTATSPROTOERRREASON,
                                     (UINT1) VRRP_ADDRESS_LIST_ERROR);
    }

    return VRRP_OK;
}

/*******************************************************************************
* Function Name   :   PktProcessCalculateV2CheckSum
* Description     :   This Funtion is called to Compute the checksum for VRRP v2
* Global Varibles :   None.
* Inputs          :   pVrrpAdverPkt - Pointer to Advertisement Packet.
*                     u1AddrType - Address Type
* Output          :   None
* Returns         :   CheckSum
*******************************************************************************/
UINT2
PktProcessCalculateV2CheckSum (tVrrpAdverPkt * pVrrpAdverPkt, UINT1 u1AddrType)
{
    UINT4               u4IpAddrLen = 0;
    UINT2               u2PktSize = 0;
    UINT2               u2CheckSum = 0;
    UINT2               u2Counter;
    UINT4               u4Scrape = 0;
    UINT2              *pu2Buf = NULL;
    UINT2               u2Temp = 0;

    u4IpAddrLen = ((UINT4) (pVrrpAdverPkt->pHeader->u1CountIpAddrs)) *
        IPVX_IPV4_ADDR_LEN;
    u2PktSize =
        (UINT2) (sizeof (tVrrpAdverHeader) + u4IpAddrLen + TEXT_AUTHKEY_SIZE);

    /* no Odd no of Pkts in VRRP protocol */
    u2PktSize = (UINT2) (u2PktSize / (UINT2) VRRP_TWO);

    /*
     * The checksum is the 16-bit one's complement of the one's complement
     * sum of the entire VRRP message starting with the version field.For
     * computing the checksum, the checksum field is set to zero.Overflow
     * is taken care by folding the higher 16 bits with the lower 16. 
     */
    pu2Buf = (UINT2 *) (VOID *) pVrrpAdverPkt->pHeader;

    for (u2Counter = 0; u2Counter < u2PktSize; u2Counter++)
    {
        u2Temp = pu2Buf[u2Counter];
        u4Scrape += u2Temp;
    }

    while (u4Scrape >> 16)
    {
        u4Scrape = (u4Scrape & 0xffff) + (u4Scrape >> 16);
    }

    u2CheckSum = (UINT2) (~(u4Scrape));

    VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2),
               "Computed Checksum of V2 IPv4 Packet for VrId %d is %x\r\n",
               pVrrpAdverPkt->pHeader->u1VrId, u2CheckSum);

    UNUSED_PARAM (u1AddrType);

    return (u2CheckSum);
}

/*******************************************************************************
* Function Name   :   PktProcessCalculateV3CheckSum
* Description     :   This Funtion is called to Compute the checksum for VRRP v3
* Global Varibles :   None.
* Inputs          :   pVrrpAdverPkt - Pointer to Advertisement Packet.
*                     u1AddressType - Address Type
*                     SourceIp      - Source IP Address in IP header
* Output          :   None
* Returns         :   CheckSum
*******************************************************************************/
UINT2
PktProcessCalculateV3CheckSum (tVrrpAdverPkt * pVrrpAdverPkt, UINT1 u1AddrType,
                               tVrrpIPvXAddr SourceIp)
{
    tVrrpIPvXAddr       DestIp;
    UINT4               u4IpAddrLen = 0;
    UINT2               u2PktSize = 0;
    UINT2               u2CheckSum = 0;
    UINT2               u2Proto = VRRPMODULE;
    UINT2               u2Counter = 0;
    UINT2               u2Counter1 = 0;
    UINT4               u4Scrape = 0;
    UINT2              *pu2Buf = NULL;
    UINT2               u2Temp = 0;
    UINT2               u2Len = 0;

    VRRP_IPVX_ADDR_CLEAR (DestIp);

#ifdef VRRPV3_INTEROP_WANTED
    /* This compilation switch is not defined by default. RFC 5798 do not have
     * clarify on how checksum needs to be done for VRRP V3 Advertisement message
     * for IPv4. Some implementations use Pseudo Header to compute check sum and
     * some others do not. Our implementation uses pseudo header to compute check
     * sum.
     *
     * To provide interoperability support with third party
     * vendor, this compilation switch should be enabled. When this compilation
     * switch is enabled, pseudo header is not used to compute checksum. */
    if (u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        return (PktProcessCalculateV2CheckSum (pVrrpAdverPkt, u1AddrType));
    }
#endif

    if (u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        VRRP_IPVX_COPY (DestIp, gVrrpV4DestMCastAddr);

        u4IpAddrLen = ((UINT4) (pVrrpAdverPkt->pHeader->u1CountIpAddrs)) *
            IPVX_IPV4_ADDR_LEN;
        u2Len = (UINT2) IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        VRRP_IPVX_COPY (DestIp, gVrrpV6DestMCastAddr);

        u4IpAddrLen = ((UINT4) (pVrrpAdverPkt->pHeader->u1CountIpAddrs)) *
            IPVX_IPV6_ADDR_LEN;
        u2Len = (UINT2) IPVX_IPV6_ADDR_LEN;
    }

    u2PktSize = (UINT2) (sizeof (tVrrpAdverHeader) + u4IpAddrLen);

    /* Checksum computation for VRRP packets for IPv6 is done as follows.
     *
     * Create a Pseudo Header by including following fields.
     *     1. Source IP Address of IP Header
     *     2. Destination IP Address of IP Header
     *     3. Length of VRRP Advertisement Packet.
     *     4. Next Header set to VRRP Protocol Field
     *     5. VRRP Advertisement Packet itself.
     *
     * Checksum is then computed as 16-bit one's complement of the one's
     * complement of this header.
     */

    for (u2Counter = 0; u2Counter < ((UINT2) (u2Len / VRRP_TWO)); u2Counter++)
    {
        u2Temp = 0;
        MEMCPY ((UINT1 *) &u2Temp,
                (UINT1 *) &SourceIp.au1Addr[u2Counter1], VRRP_TWO);

        /* NTOHS is not required for u2Temp as Checksum computation is done
         * in network order */
        u4Scrape += (u2Temp);
        u2Counter1 = (UINT2) (u2Counter1 + VRRP_TWO);
    }

    u2Counter1 = 0;
    for (u2Counter = 0; u2Counter < ((UINT2) (u2Len / VRRP_TWO)); u2Counter++)
    {
        u2Temp = 0;
        MEMCPY ((UINT1 *) &u2Temp,
                (UINT1 *) &DestIp.au1Addr[u2Counter1], VRRP_TWO);

        /* NTOHS is not required for u2Temp as Checksum computation is done
         * in network order */
        u4Scrape += (u2Temp);
        u2Counter1 = (UINT2) (u2Counter1 + VRRP_TWO);
    }

    /* For Pktsize and Protocol field, HTONS is required before Checksum
     * computation is done on network order. */
    u4Scrape += (OSIX_HTONS (u2PktSize));
    u4Scrape += (OSIX_HTONS (u2Proto));

    /* no Odd no of Pkts in VRRP protocol */
    u2PktSize = (UINT2) (u2PktSize / (UINT2) VRRP_TWO);

    pu2Buf = (UINT2 *) (VOID *) pVrrpAdverPkt->pHeader;

    for (u2Counter = 0; u2Counter < u2PktSize; u2Counter++)
    {
        u2Temp = pu2Buf[u2Counter];
        u4Scrape += u2Temp;
    }

    while (u4Scrape >> 16)
    {
        u4Scrape = (u4Scrape & 0xffff) + (u4Scrape >> 16);
    }

    u2CheckSum = (UINT2) (~(u4Scrape));

    if (u1AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_3),
                   "Computed Checksum of V3 IPv4 Packet for VrId %d is %x\n",
                   pVrrpAdverPkt->pHeader->u1VrId, u2CheckSum);
    }
    else
    {
        VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_3),
                   "Computed Checksum of V3 IPv6 Packet for VrId %d is %x\n",
                   pVrrpAdverPkt->pHeader->u1VrId, u2CheckSum);
    }

    return (u2CheckSum);
}

/*******************************************************************************
*Function Name   :   PktProcessGetOperEntry                     
*Description     :   This Funtion is called to get the VRRP Oper Index for
                     received If Index, Vrid and Address Type
*Inputs          :   i4IfIndex       - Interface Index
*                    i4VrrpOperVrid  - VRID
*                    i4AddrType      - Address Type
*Output          :                                                              
*                                                                    
*Returns         :                                                              
*                    OperIndex     - OperEntryIndex
*******************************************************************************/
EXPORT INT4
PktProcessGetOperEntry (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType)
{
    tVrrpOperTable      VrrpOperEntry;
    tVrrpOperTable     *pVrrpOperEntry = NULL;

    MEMSET (&VrrpOperEntry, 0, sizeof (tVrrpOperTable));

    VrrpOperEntry.i4IfIndex = i4IfIndex;
    VrrpOperEntry.i4VrId = i4VrId;
    VrrpOperEntry.u1AddressType = (UINT1) i4AddrType;

    pVrrpOperEntry
        = (tVrrpOperTable *) RBTreeGet (gVrrpOperTable, &VrrpOperEntry);

    if (pVrrpOperEntry == NULL)
    {
        return VRRP_NOT_OK;
    }

    return (pVrrpOperEntry->i4OperIndex);
}

/*******************************************************************************
*Function Name   :   PktProcessReceivedPacket 
*Description     :   This Funtion is used to process the Advertisment Packet.
                     This function first type casts the received buffer to the
                     VRRP Header format, validates the packet and if validation
                     is successful gives it to SEM or Election Module.
*Inputs          :   i4Port         - IP Port Number
*                    pBuf           - VRRP IP Packet received
*                    u4DataSize     - Data Size received
*                    i4AddrType     - Address Type
*                    u1Ttl          - TTL or Hop Limit
*Output          :                                                              
*                                                                    
*Returns         :   None
*******************************************************************************/
VOID
PktProcessReceivedPacket (INT4 i4Port, UINT1 *pBuf, UINT4 u4DataSize,
                          INT4 i4AddrType, UINT1 u1Ttl)
{
    INT4                i4OperIndex = VRRP_NOT_OK;
    UINT4               u4IfIndex = 0;
    INT4                i4IfIndex = 0;
    tVrrpAdverPkt       VrrpAdverPkt;
    tVrrpAdverHeader   *pVrrpAdverHeader = NULL;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];
    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);

    if (VrrpGetCfaIfIndexFromPort ((UINT4) i4Port, i4AddrType,
                                   &u4IfIndex) == VRRP_NOT_OK)
    {
        return;
    }

    MEMSET (&VrrpAdverPkt, 0, sizeof (tVrrpAdverPkt));

    VrrpAdverPkt.pHeader = pVrrpAdverHeader = (tVrrpAdverHeader *) (VOID *)
        pBuf;
    VrrpAdverPkt.pIpAddress = (UINT1 *) (VOID *)
        &pBuf[sizeof (tVrrpAdverHeader)];

    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        VrrpAdverPkt.pAuthenticationData =
            (UINT1 *) &pBuf[sizeof (tVrrpAdverHeader) +
                            (UINT1) (((pVrrpAdverHeader->u1CountIpAddrs) *
                                      IPVX_IPV4_ADDR_LEN))];
    }

    i4IfIndex = (INT4) u4IfIndex;

    i4OperIndex = PktProcessValidateAdverPacket (i4IfIndex, &VrrpAdverPkt,
                                                 u4DataSize, i4AddrType, u1Ttl);
    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);
    if (i4OperIndex == VRRP_NOT_OK)
    {
        VRRP_DBG4 (VRRP_TRC_ALL_FAILURES,
                   "Invalid Advertisement Packet received on If %d with size %d"
                   ", Address Type %s, Ttl %d\n",
                   i4IfIndex, u4DataSize, ac1AddrType, u1Ttl);
        return;
    }

    if ((VrrpSem (ADVERPKT_EVENT, i4OperIndex, pVrrpAdverHeader->u1Priority,
                  (UINT4) pBuf[sizeof (tVrrpAdverHeader)])) == VRRP_NOT_OK)
    {
        VRRP_DBG ((VRRP_TRC_EVENTS | VRRP_TRC_ALL_FAILURES),
                  "Advertisement Packet Event Handling failed\n");
    }

    VRRP_DBG4 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2 | VRRP_TRC_VERSION_3),
               "Advertisement Packet processed successfully on If %d with "
               "size %d, Address Type %s, Ttl %d\n",
               i4IfIndex, u4DataSize, ac1AddrType, u1Ttl);
    return;
}

/*******************************************************************************
*Function Name   :   PktProcessTransmitPacket                               
*Description     :   This Funtion  is called from the Election Module to     
*                    Enqueue the packet to the VRRP_IP Queue. 
*                    This function would allocate a buffer,   
*                    fill the header value and update the MIB statistics.      
*Global Varibles:                                                              
*                    None             
*Inputs        :                                                               
*                    i4OperIndex      - Index of the OperEntryTable
*                    b1IsPriorityZero - Flag to indicate if the Priority is Zero
*Output        :                                                               
*                    Packet Enqueued to the VRRP-IP Queue                    
*Returns       :                                                               
*                   VRRP_OK                                                 
*                   VRRP_NOT_OK                     
*******************************************************************************/
INT1
PktProcessTransmitPacket (INT4 i4OperIndex, BOOL1 b1IsPriorityZero)
{
    INT1                i1V2RetVal = VRRP_OK;
    INT1                i1V3RetVal = VRRP_OK;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);

    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);
    if (VRRP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return VRRP_OK;
    }

    if (b1IsPriorityZero == VRRP_TRUE)
    {
        VRRP_DBG3 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2 | VRRP_TRC_VERSION_3),
                   "Priority Zero Packet to be Sent for IfIndex %d VrId %d "
                   "Address Type %s\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        PktProcessUpdateStatistics (i4OperIndex, VRRPSTATSPRIORITYZEROPKTSSENT);
    }
    else
    {
        VRRP_DBG3 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2 | VRRP_TRC_VERSION_3),
                   "Non-Zero Priority Packet to be Sent for IfIndex %d VrId %d "
                   "Address Type %ds\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
    }

    PktClearTxBuf (i4OperIndex);

    if ((gi4VrrpNodeVersion == VRRP_VERSION_2) ||
        (gi4VrrpNodeVersion == VRRP_VERSION_2_3))
    {
        i1V2RetVal = PktProcessTransmitV2Packet (i4OperIndex, b1IsPriorityZero);
    }

    PktClearTxBuf (i4OperIndex);

    if ((gi4VrrpNodeVersion == VRRP_VERSION_3) ||
        (gi4VrrpNodeVersion == VRRP_VERSION_2_3))
    {
        i1V3RetVal = PktProcessTransmitV3Packet (i4OperIndex, b1IsPriorityZero);
    }

    if ((i1V2RetVal == VRRP_NOT_OK) || (i1V3RetVal == VRRP_NOT_OK))
    {
        VRRP_DBG3 ((VRRP_TRC_PKT | VRRP_TRC_ALL_FAILURES |
                    VRRP_TRC_VERSION_2 | VRRP_TRC_VERSION_3),
                   "Advertisement Packet sending failed for IfIndex %d VrId %d "
                   "Address Type %s\n",
                   OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex),
                   ac1AddrType);
        return VRRP_NOT_OK;
    }

    VRRP_DBG3 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2 | VRRP_TRC_VERSION_3),
               "Advertisement Packet Sent for IfIndex %d VrId %d "
               "Address Type %s\n",
               OPERIFINDEX (i4OperIndex), OPERVRID (i4OperIndex), ac1AddrType);
    return VRRP_OK;
}

/*******************************************************************************
*Function Name   :   PktProcessTransmitV2Packet                               
*Description     :   This Funtion  is called from the Election Module to     
*                    Enqueue the packet to the VRRP_IP Queue. 
*                    This function would allocate a buffer,   
*                    fill the header value in V2 format and update the 
*                    MIB statistics.      
*Global Varibles:                                                              
*                    None             
*Inputs        :                                                               
*                    i4OperIndex      - Index of the OperEntryTable
*                    b1IsPriorityZero - Flag to indicate if the Priority is Zero
*Output        :                                                               
*                    Packet Enqueued to the VRRP-IP Queue                    
*Returns       :                                                               
*                   VRRP_OK                                                 
*                   VRRP_NOT_OK                     
*******************************************************************************/
INT1
PktProcessTransmitV2Packet (INT4 i4OperIndex, BOOL1 b1IsPriorityZero)
{
    tVrrpAdverPkt       VrrpAdverPkt;
    tVrrpAdverHeader   *pVrrpAdverHeader = NULL;
    UINT4               u4IpAddressCount = 0;
    INT4                i4Size = 0;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);
    MEMSET (&VrrpAdverPkt, 0, sizeof (tVrrpAdverPkt));

    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);
    u4IpAddressCount = (UINT4) OPERIPADDRCOUNT (i4OperIndex);

    pVrrpAdverHeader
        = (tVrrpAdverHeader *) (VOID *) TRANSMITLNBUF (i4OperIndex);

    VrrpAdverPkt.pHeader = pVrrpAdverHeader;

    VrrpAdverPkt.pIpAddress =
        (UINT1 *) (TRANSMITLNBUF (i4OperIndex) + sizeof (tVrrpAdverHeader));

    /* VRRP Version 2 is applicable only for IPv4. Hence, authentication data
     * base pointer is calculated based IPv4 Address count only */
    VrrpAdverPkt.pAuthenticationData
        = (UINT1 *) (TRANSMITLNBUF (i4OperIndex) + sizeof (tVrrpAdverHeader) +
                     (u4IpAddressCount * IPVX_IPV4_ADDR_LEN));

    PktProcessEncodeV2AdverHeader (i4OperIndex, b1IsPriorityZero,
                                   &VrrpAdverPkt);

    /* VRRP Version 2 is applicable only for IPv4. Hence, size of the packet
     * is calculated only including IPv4 Associated Addresses. */
    i4Size = (INT4) (sizeof (tVrrpAdverHeader) +
                     (u4IpAddressCount * IPVX_IPV4_ADDR_LEN) +
                     TEXT_AUTHKEY_SIZE);

    if (VrrpSendPacket (i4Size, i4OperIndex) == VRRP_NOT_OK)
    {
        VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_ALL_FAILURES |
                    VRRP_TRC_VERSION_2),
                   "Failed to Transmit V2 Advertisement Packet for "
                   "VrId %d Address Type %s\r\n",
                   OPERVRID (i4OperIndex), ac1AddrType);
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSTRANSMITV2PACKETFAIL);

        return VRRP_NOT_OK;
    }

    VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_2),
               "V2 Advertisement Packet for VrId %d Address Type %s"
               "transmitted successfully\n",
               OPERVRID (i4OperIndex), ac1AddrType);
    PktProcessUpdateV3Statistics (i4OperIndex, VRRPSTATSV2ADVERTISETXED);

    return VRRP_OK;
}

/*******************************************************************************
*Function Name   :   PktProcessTransmitV3Packet
*Description     :   This Funtion  is called from the Election Module to     
*                    Enqueue the packet to the VRRP_IP Queue. 
*                    This function would allocate a buffer,   
*                    fill the header value and update the MIB statistics.      
*Global Varibles:                                                              
*                    None             
*Inputs        :                                                               
*                    i4OperIndex        - Index of the OperEntryTable
*                    b1IsPriorityZero   - Flag to indicate if the Priority is Zero
*Output        :                                                               
*                    Packet Enqueued to the VRRP-IP Queue                    
*Returns       :                                                               
*                   VRRP_OK                                                 
*                   VRRP_NOT_OK                     
*******************************************************************************/
INT1
PktProcessTransmitV3Packet (INT4 i4OperIndex, BOOL1 b1IsPriorityZero)
{
    tVrrpAdverPkt       VrrpAdverPkt;
    tVrrpAdverHeader   *pVrrpAdverHeader = NULL;
    UINT4               u4IpAddressCount = 0;
    INT4                i4Size = 0;
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];

    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);

    MEMSET (&VrrpAdverPkt, 0, sizeof (tVrrpAdverPkt));

    u4IpAddressCount = (UINT4) OPERIPADDRCOUNT (i4OperIndex);

    pVrrpAdverHeader = (tVrrpAdverHeader *) (VOID *)
        TRANSMITLNBUF (i4OperIndex);

    VrrpAdverPkt.pHeader = pVrrpAdverHeader;

    VrrpAdverPkt.pIpAddress =
        (UINT1 *) (TRANSMITLNBUF (i4OperIndex) + sizeof (tVrrpAdverHeader));

    PktProcessEncodeV3AdverHeader (i4OperIndex, b1IsPriorityZero,
                                   &VrrpAdverPkt);
    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);

    /* Calculate size of the packet based on IPv4 or IPv6 */
    if (OPERADDRTYPE (i4OperIndex) == (UINT1) VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        i4Size = (INT4) (sizeof (tVrrpAdverHeader) +
                         (u4IpAddressCount * IPVX_IPV4_ADDR_LEN));
    }
    else
    {
        i4Size = (INT4) (sizeof (tVrrpAdverHeader) +
                         (u4IpAddressCount * IPVX_IPV6_ADDR_LEN));
    }

    if (VrrpSendPacket (i4Size, i4OperIndex) == VRRP_NOT_OK)
    {
        VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_ALL_FAILURES | VRRP_TRC_VERSION_3),
                   "Failed to Transmit V3 Advertisement Packet for "
                   "VrId %d Address Type %s\r\n",
                   OPERVRID (i4OperIndex), ac1AddrType);
        PktProcessUpdateStatistics (i4OperIndex,
                                    PKTPROCESSTRANSMITV3PACKETFAIL);

        return VRRP_NOT_OK;
    }

    VRRP_DBG2 ((VRRP_TRC_PKT | VRRP_TRC_VERSION_3),
               "V3 Advertisement Packet for VrId %d Address Type %s "
               "transmitted successfully\n",
               OPERVRID (i4OperIndex), ac1AddrType);
    PktProcessUpdateV3Statistics (i4OperIndex, VRRPSTATSADVERTISETXED);

    return VRRP_OK;
}

/*******************************************************************************
*Function Name   :   PktClearTxBuf
*Description     :   This function clears Tx Buffer based on Address Type
*
*Inputs          :   i4OperIndex        - Index of the OperEntryTable
*Output          :   None
*Returns         :   None
*******************************************************************************/
VOID
PktClearTxBuf (INT4 i4OperIndex)
{
    UINT4               u4StructSize = 0;

    if (OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        u4StructSize =
            FsVRRPSizingParams[MAX_VRRP_TX_IPV4_BUF_SIZING_ID].u4StructSize;
    }
    else
    {
        u4StructSize =
            FsVRRPSizingParams[MAX_VRRP_TX_IPV6_BUF_SIZING_ID].u4StructSize;
    }

    if (TRANSMITLNBUF (i4OperIndex) != NULL)
    {
        MEMSET (TRANSMITLNBUF (i4OperIndex), 0, u4StructSize);
    }

    return;
}

/*******************************************************************************
*Function Name   :   PktProcessUpdateStatistics                               
*Description     :   This Funtion  is called to increment the apropriate 
*                    MIB Variable.                                     
*Global Varibles :                                                              
*                    None                                                    
*Inputs          : 
*                    i4OperIndex     -   Index of the OperEntryTable         
*                    u1MibVariable   -   MIM Variable to be Updated   
*Output          :                                                      
*                    None
*Returns         :                                                              
*                    None
*******************************************************************************/
VOID
PktProcessUpdateStatistics (INT4 i4OperIndex, UINT1 u1MibVariable)
{
    if (gi4VrrpTxRxVersion == VRRP_VERSION_2_3)
    {
        PktProcessUpdateV2Statistics (i4OperIndex, u1MibVariable);
    }
    else
    {
        PktProcessUpdateV3Statistics (i4OperIndex, u1MibVariable);
    }

    return;
}

/*******************************************************************************
*Function Name   :   PktProcessUpdateV2V3Statistics                               
*Description     :   This Funtion  is called to increment the statistics of both
*                    V2 as well as V3 Statistics variable and notify logging and 
*                    trap.
*Global Varibles :                                                              
*                    None                                                    
*Inputs          : 
*                    i4OperIndex     -   Index of the OperEntryTable         
*                    u1MibVariable   -   MIB Variable to be Updated   
*                    u1MibValue      -   MIB Value to be notified
*Output          :                                                      
*                    None
*Returns         :                                                              
*                    None
*******************************************************************************/
VOID
PktProcessUpdateV2V3Statistics (INT4 i4OperIndex, UINT1 u1MibVariable,
                                UINT1 u1MibValue)
{
    if ((gi4VrrpNodeVersion == VRRP_VERSION_2) ||
        (gi4VrrpNodeVersion == VRRP_VERSION_2_3))
    {
        gi4VrrpTxRxVersion = VRRP_VERSION_2_3;
        PktProcessUpdateStatistics (i4OperIndex, u1MibVariable);
    }

    if ((gi4VrrpNodeVersion == VRRP_VERSION_3) ||
        (gi4VrrpNodeVersion == VRRP_VERSION_2_3))
    {
        gi4VrrpTxRxVersion = VRRP_VERSION_3;
        PktProcessUpdateStatistics (i4OperIndex, u1MibVariable);
    }

    UNUSED_PARAM (u1MibValue);

    return;
}

/*******************************************************************************
*Function Name   :   PktProcessUpdateV2Statistics                               
*Description     :   This Funtion  is called to increment the statistics of
*                    appropriate V2 MIB Variable.
*Global Varibles :                                                              
*                    None                                                    
*Inputs          : 
*                    i4OperIndex     -   Index of the OperEntryTable         
*                    u1MibVariable   -   MIB Variable to be Updated   
*Output          :                                                      
*                    None
*Returns         :                                                              
*                    None
*******************************************************************************/
VOID
PktProcessUpdateV2Statistics (INT4 i4OperIndex, UINT1 u1MibVariable)
{
    tVrrpStatsTable    *pVrrpStatsEntry;

    pVrrpStatsEntry = &(VRRPSTATS (i4OperIndex));

    switch (u1MibVariable)
    {
        case VRRPSEMFAIL:
            pVrrpStatsEntry->u4VrrpSemFail++;
            break;

        case VRRPSENDNDNEIGHBORADVTFAIL:
            pVrrpStatsEntry->u4VrrpSendNDNeighborAdvtFail++;
            break;

        case VRRPSENDPACKETTOIPV4FAIL:
            pVrrpStatsEntry->u4VrrpSendPacketToIpv4Fail++;
            break;

        case VRRPGETIPV4PORTFROMOPERINDEXFAIL:
            pVrrpStatsEntry->u4VrrpGetIpv4PortFromOperIndexFail++;
            break;

        case VRRPSENDGRATARPFAIL:
            pVrrpStatsEntry->u4VrrpSendGratArpFail++;
            break;

        case VRRPETHREGISTERMACADDRFAIL:
            pVrrpStatsEntry->u4EthRegisterMacAddrFail++;
            break;

        case VRRPNETIP4DELINTFFAIL:
            pVrrpStatsEntry->u4VrrpNetIpv4DeleteIntfFail++;
            break;

        case VRRPIPIFJOINMCASTGROUPFAIL:
            pVrrpStatsEntry->u4VrrpIpifJoinMcastGroupFail++;
            break;

        case VRRPIPIFLEAVEMCASTGROUPFAIL:
            pVrrpStatsEntry->u4VrrpIpifLeaveMcastGroupFail++;
            break;

        case VRRPSOCKETFAIL:
            pVrrpStatsEntry->u4VrrpSocketFail++;
            break;

        case PKTPROCESSVALIDATEADVERPACKETFAIL:
            pVrrpStatsEntry->u4PktProcessValidateAdverPacketFail++;
            break;

        case PKTPROCESSVALIDATEADVERV2PACKETFAIL:
            pVrrpStatsEntry->u4PktProcessValidateAdverV2PacketFail++;
            break;

        case PKTPROCESSTRANSMITV2PACKETFAIL:
            pVrrpStatsEntry->u4PktProcessTransmitV2PacketFail++;
            break;

        case VRRPSTATSBECOMEMASTER:
            pVrrpStatsEntry->u4BecomeMaster++;
            break;

        case VRRPSTATSADVERTISERCVD:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8RcvdAdvertisements));
            break;

        case VRRPSTATSADVERTISEINTERVALERRORS:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8AdvtIntervalErrors));
            break;

        case VRRPSTATSAUTHFAILURES:
            pVrrpStatsEntry->u4AuthFailures++;
            break;

        case VRRPSTATSIPTTLERRORS:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8IpTtlErrors));
            break;

        case VRRPSTATSPRIORITYZEROPKTSRCVD:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8RcvdPriZeroPackets));
            break;

        case VRRPSTATSPRIORITYZEROPKTSSENT:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8SentPriZeroPackets));
            break;

        case VRRPSTATSINVALIDTYPEPKTSRCVD:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8RcvdInvalidTypePackets));
            break;

        case VRRPSTATSPACKETLENGTHERRORS:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8PacketLengthErrors));
            break;

        case VRRPSTATSADDRESSLISTERRORS:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8AddressListErrors));
            break;

        case VRRPSTATSINVALIDAUTHTYPE:
            pVrrpStatsEntry->u4InvalidAuthType++;
            break;

        case VRRPSTATSAUTHTYPEMISMATCH:
            pVrrpStatsEntry->u4AuthTypeMismatch++;
            break;

        case VRRPROUTCHECKSUMERRORS:
            FSAP_U8_INC (&(STATSCHECKSUMERRORS64));
            break;

        case VRRPROUTVERSIONERRORS:
            gu4RouterVersionErrors++;
            break;

        case VRRPROUTVRIDERRORS:
            gu4RouterVrIdErrors++;
            break;

        default:
            break;
    }                            /* End of Switch */

    return;
}

/*******************************************************************************
*Function Name   :   PktProcessUpdateV3Statistics                               
*Description     :   This Funtion  is called to increment the statistics count
*                    of appropriate V3 MIB variable
*Global Varibles :                                                              
*                    None                                                    
*Inputs          : 
*                    i4OperIndex     -   Index of the OperEntryTable         
*                    u1MibVariable   -   MIB Variable to be Updated   
*Output          :                                                      
*                    None
*Returns         :                                                              
*                    None
*******************************************************************************/
VOID
PktProcessUpdateV3Statistics (INT4 i4OperIndex, UINT1 u1MibVariable)
{
    tVrrpStatsTable    *pVrrpStatsEntry;

    pVrrpStatsEntry = &(VRRPSTATS (i4OperIndex));

    switch (u1MibVariable)
    {
        case VRRPSTATSBECOMEMASTER:
            pVrrpStatsEntry->u4BecomeMaster++;

        case VRRPSEMFAIL:
            pVrrpStatsEntry->u4VrrpSemFail++;
            break;

        case VRRPSENDNDNEIGHBORADVTFAIL:
            pVrrpStatsEntry->u4VrrpSendNDNeighborAdvtFail++;
            break;

        case VRRPSENDPACKETTOIPV4FAIL:
            pVrrpStatsEntry->u4VrrpSendPacketToIpv4Fail++;
            break;

        case VRRPSENDPACKETTOIPV6FAIL:
            pVrrpStatsEntry->u4VrrpSendPacketToIpv6Fail++;
            break;

        case VRRPGETIPV4PORTFROMOPERINDEXFAIL:
            pVrrpStatsEntry->u4VrrpGetIpv4PortFromOperIndexFail++;
            break;

        case VRRPGETIPV6PORTFROMOPERINDEXFAIL:
            pVrrpStatsEntry->u4VrrpGetIpv6PortFromOperIndexFail++;
            break;

        case VRRPSENDGRATARPFAIL:
            pVrrpStatsEntry->u4VrrpSendGratArpFail++;
            break;

        case VRRPETHREGISTERMACADDRFAIL:
            pVrrpStatsEntry->u4EthRegisterMacAddrFail++;
            break;

        case VRRPNETIP4DELINTFFAIL:
            pVrrpStatsEntry->u4VrrpNetIpv4DeleteIntfFail++;
            break;
        case VRRPNETIP6DELINTFFAIL:
            pVrrpStatsEntry->u4VrrpNetIpv6DeleteIntfFail++;
            break;
        case VRRPIPIFJOINMCASTGROUPFAIL:
            pVrrpStatsEntry->u4VrrpIpifJoinMcastGroupFail++;
            break;

        case VRRPIPIFLEAVEMCASTGROUPFAIL:
            pVrrpStatsEntry->u4VrrpIpifLeaveMcastGroupFail++;
            break;

        case VRRPSOCKETFAIL:
            pVrrpStatsEntry->u4VrrpSocketFail++;
            break;

        case PKTPROCESSVALIDATEADVERPACKETFAIL:
            pVrrpStatsEntry->u4PktProcessValidateAdverPacketFail++;
            break;

        case PKTPROCESSVALIDATEADVERV2PACKETFAIL:
            pVrrpStatsEntry->u4PktProcessValidateAdverV2PacketFail++;
            break;

        case PKTPROCESSVALIDATEADVERV3PACKETFAIL:
            pVrrpStatsEntry->u4PktProcessValidateAdverV3PacketFail++;
            break;

        case PKTPROCESSTRANSMITV2PACKETFAIL:
            pVrrpStatsEntry->u4PktProcessTransmitV2PacketFail++;
            break;

        case PKTPROCESSTRANSMITV3PACKETFAIL:
            pVrrpStatsEntry->u4PktProcessTransmitV3PacketFail++;
            break;

        case VRRPSTATSADVERTISERCVD:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8RcvdAdvertisements));
            OPERV3ADVTRCVD (i4OperIndex) = TRUE;
            break;

        case VRRPSTATSADVERTISEINTERVALERRORS:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8AdvtIntervalErrors));
            break;

        case VRRPSTATSIPTTLERRORS:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8IpTtlErrors));
            break;

        case VRRPSTATSPRIORITYZEROPKTSRCVD:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8RcvdPriZeroPackets));
            break;

        case VRRPSTATSPRIORITYZEROPKTSSENT:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8SentPriZeroPackets));
            break;

        case VRRPSTATSINVALIDTYPEPKTSRCVD:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8RcvdInvalidTypePackets));
            break;

        case VRRPSTATSPACKETLENGTHERRORS:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8PacketLengthErrors));
            break;

        case VRRPSTATSADDRESSLISTERRORS:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8AddressListErrors));
            break;

        case VRRPROUTCHECKSUMERRORS:
            FSAP_U8_INC (&(gu8RouterChecksumErrors));
            break;

        case VRRPROUTVERSIONERRORS:
            FSAP_U8_INC (&(gu8RouterVersionErrors));
            break;

        case VRRPROUTVRIDERRORS:
            FSAP_U8_INC (&(gu8RouterVrIdErrors));
            break;

        case VRRPSTATSADVERTISETXED:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8TxedAdvertisments));
            break;

        case VRRPSTATSV2ADVERTISETXED:
            FSAP_U8_INC (&(pVrrpStatsEntry->u8TxedV2Advertisments));
            break;

        case VRRPSTATSV2ADVTIGNORED:
            FSAP_U8_INC ((&pVrrpStatsEntry->u8V2AdvtIgnored));
            break;

        default:
            break;
    }                            /* End of Switch */

    return;
}

/*******************************************************************************
*Function Name   :   PktProcessSetAndNotifyStats
*Description     :   This Funtion is called to set Protocol Error reason variable
*                    and New Master reason variable and notify logging and SNMP
*                    Manager.
*Global Varibles :                                                              
*                    None                                                    
*Inputs          : 
*                    i4OperIndex     -   Index of the OperEntryTable         
*                    u1MibVariable   -   MIB Variable to be Updated   
*                    u1MibValue      -   MIB Value to be notified
*Output          :                                                      
*                    None
*Returns         :                                                              
*                    None
*******************************************************************************/
VOID
PktProcessSetAndNotifyStats (INT4 i4OperIndex, UINT1 u1MibVariable,
                             UINT1 u1MibValue)
{
    PktProcessSetReason (i4OperIndex, u1MibVariable, u1MibValue);

    PktNotifyLogging (i4OperIndex, u1MibVariable, u1MibValue);

    PktNotifyStatsToSnmpManager (i4OperIndex, u1MibVariable, u1MibValue);

    return;
}

/*******************************************************************************
*Function Name   :   PktProcessSetReason
*Description     :   This Funtion is called to set Protocol Error reason variable
*                    and New Master reason variable.
*Global Varibles :                                                              
*                    None                                                    
*Inputs          : 
*                    i4OperIndex     -   Index of the OperEntryTable         
*                    u1MibVariable   -   MIB Variable to be Updated   
*                    u1MibValue      -   MIB Value to be notified
*Output          :                                                      
*                    None
*Returns         :                                                              
*                    None
*******************************************************************************/
VOID
PktProcessSetReason (INT4 i4OperIndex, UINT1 u1MibVariable, UINT1 u1MibValue)
{
    tVrrpStatsTable    *pVrrpStatsEntry = NULL;

    pVrrpStatsEntry = &(VRRPSTATS (i4OperIndex));

    if (u1MibVariable == VRRPSTATSPROTOERRREASON)
    {
        if (u1MibValue <= VRRP_VRID_ERROR)
        {
            pVrrpStatsEntry->u1ProtoErrReason = u1MibValue;
        }
    }
    else if (u1MibVariable == VRRPSTATSNEWMASTERREASON)
    {
        pVrrpStatsEntry->u1NewMasterReason = u1MibValue;
    }

    return;
}

/*******************************************************************************
*Function Name   :   PktNotifyLogging
*Description     :   This Funtion is called to notify logging module.
*
*Global Varibles :                                                              
*                    None                                                    
*Inputs          : 
*                    i4OperIndex     -   Index of the OperEntryTable         
*                    u1MibVariable   -   MIB Variable to be Updated   
*                    u1MibValue      -   MIB Value to be notified
*Output          :                                                      
*                    None
*Returns         :                                                              
*                    None
*******************************************************************************/
VOID
PktNotifyLogging (INT4 i4OperIndex, UINT1 u1MibVariable, UINT1 u1MibValue)
{
    CHR1                ac1AddrType[IPVX_IPV4_ADDR_LEN + 1];
    CHR1                ac1VrrpSysLogMsg[SNMP_MAX_OCTETSTRING_SIZE];
    CHR1               *pac1LastProtoError[] = {
        "No Error",
        "Incorrect IP TTL",
        "Incorrect Version",
        "Incorrect Checksum",
        "Invalid VrId",
        "Advertisement Interval Mismatch",
        "Invalid VRRP Packet Type",
        "Address List Mismatch",
        "Incorrect Packet Length"
    };
    CHR1               *pac1NewMasterReason[] = {
        "Not Master",
        "Priority",
        "Preempted",
        "Master No Response"
    };
    CHR1               *pac1AuthError[] = {
        "No Error",
        "Invalid Authentication Type",
        "Authentication Type Mismatch",
        "Authentication Failure"
    };
    MEMSET (ac1AddrType, 0, IPVX_IPV4_ADDR_LEN + 1);
    MEMSET (ac1VrrpSysLogMsg, 0, sizeof (ac1VrrpSysLogMsg));

    VRRP_ADDRTYPE_TO_STR (ac1AddrType, i4OperIndex);

    if (u1MibVariable == VRRPSTATSPROTOERRREASON)
    {
        SNPRINTF (ac1VrrpSysLogMsg, sizeof (ac1VrrpSysLogMsg),
                  "Error Observed for VrId %d Address Type %s. "
                  "Reason: %s",
                  OPERVRID (i4OperIndex), ac1AddrType,
                  pac1LastProtoError[u1MibValue]);

        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT4) gi4VrrpSysLogId, "%s",
                      ac1VrrpSysLogMsg));
    }
    else if (u1MibVariable == VRRPSTATSNEWMASTERREASON)
    {
        if (u1MibValue != VRRP_MASTER_REASON_NOT_MASTER)
        {
            if ((OPERNEXTSTATE (i4OperIndex) != BACKUP_STATE))

            {
                SNPRINTF (ac1VrrpSysLogMsg, sizeof (ac1VrrpSysLogMsg),
                          "NEW MASTER %s Elected for VRID %d Address Type %s. Reason: %s",
                          VRRP_IPVX_PRINT_ADDR (OPERMASTERIPADDR (i4OperIndex),
                                                OPERADDRTYPE (i4OperIndex)),
                          OPERVRID (i4OperIndex), ac1AddrType,
                          pac1NewMasterReason[u1MibValue]);
            }
            else
            {
                SNPRINTF (ac1VrrpSysLogMsg, sizeof (ac1VrrpSysLogMsg),
                          "NEW BACKUP %s Elected for VRID %d Address Type %s. Reason: %s",
                          VRRP_IPVX_PRINT_ADDR (OPERMASTERIPADDR (i4OperIndex),
                                                OPERADDRTYPE (i4OperIndex)),
                          OPERVRID (i4OperIndex), ac1AddrType,
                          pac1NewMasterReason[u1MibValue]);

            }
        }
        else
        {
            SNPRINTF (ac1VrrpSysLogMsg, sizeof (ac1VrrpSysLogMsg),
                      "Not Master for VRID %d Address Type %s",
                      OPERVRID (i4OperIndex), ac1AddrType);

        }

        SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, (UINT4) gi4VrrpSysLogId, "%s",
                      ac1VrrpSysLogMsg));
    }
    else if (u1MibVariable == VRRPSTATSAUTHERRORTYPE)
    {
        SNPRINTF (ac1VrrpSysLogMsg, sizeof (ac1VrrpSysLogMsg),
                  "Error Observed for VrId %d Address Type %s. "
                  "Reason: %s",
                  OPERVRID (i4OperIndex), ac1AddrType,
                  pac1AuthError[u1MibValue]);

        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, (UINT4) gi4VrrpSysLogId, "%s",
                      ac1VrrpSysLogMsg));
    }

    return;
}

/*******************************************************************************
*Function Name   :   PktNotifyStatsToSnmpManager
*Description     :   This Funtion is called to notify Stats to SNMP Manager
*                    based on V2 or V3
*
*Global Varibles :                                                              
*                    None                                                    
*Inputs          : 
*                    i4OperIndex     -   Index of the OperEntryTable         
*                    u1MibVariable   -   MIB Variable to be Updated   
*                    u1MibValue      -   MIB Value to be notified
*Output          :                                                      
*                    None
*Returns         :                                                              
*                    None
*******************************************************************************/
VOID
PktNotifyStatsToSnmpManager (INT4 i4OperIndex, UINT1 u1MibVariable,
                             UINT1 u1MibValue)
{
    if (u1MibVariable == VRRPSTATSNEWMASTERREASON)
    {
        /* Based on the version supported send trap accordingly */
        if (gi4VrrpNodeVersion == VRRP_VERSION_2)
        {
            PktNotifyV2StatsToSnmpManager (i4OperIndex, u1MibVariable,
                                           u1MibValue);
        }
        else
        {
            PktNotifyV3StatsToSnmpManager (i4OperIndex, u1MibVariable,
                                           u1MibValue);
        }
    }
    else if (u1MibVariable == VRRPSTATSPROTOERRREASON)
    {
        /* Protocol Error Reason Trap is applicable when packet received with
         * VRRP Version 3. */
        if (gi4VrrpTxRxVersion == VRRP_VERSION_2_3)
        {
            return;
        }

        PktNotifyV3StatsToSnmpManager (i4OperIndex, u1MibVariable, u1MibValue);
    }
    else if (u1MibVariable == VRRPSTATSAUTHERRORTYPE)
    {
        /* Auth Error Trap is applicable when packet received with
         * VRRP Version 2. */
        if (gi4VrrpTxRxVersion == VRRP_VERSION_3)
        {
            return;
        }

        PktNotifyV2StatsToSnmpManager (i4OperIndex, u1MibVariable, u1MibValue);
    }

    return;
}

/*******************************************************************************
*Function Name   :   PktNotifyV2StatsToSnmpManager
*Description     :   This Funtion is called to notify V2 Stats SNMP Manager
*
*Global Varibles :                                                              
*                    None                                                    
*Inputs          : 
*                    i4OperIndex     -   Index of the OperEntryTable         
*                    u1MibVariable   -   MIB Variable to be Updated   
*                    u1MibValue      -   MIB Value to be notified
*Output          :                                                      
*                    None
*Returns         :                                                              
*                    None
*******************************************************************************/
VOID
PktNotifyV2StatsToSnmpManager (INT4 i4OperIndex, UINT1 u1MibVariable,
                               UINT1 u1MibValue)
{
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OCTET_STRING_TYPE *pOString = NULL;
    UINT4               u4Len = 0;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

    UINT4               au4VrrpNotification[] = { 1, 3, 6, 1, 2, 1, 68, 0, 0 };

    UINT4               au4VrrpMasterIpAddr[] =
        { 1, 3, 6, 1, 2, 1, 68, 1, 3, 1, 7, 0, 0 };

    UINT4               au4VrrpAuthPktSrc[] = { 1, 3, 6, 1, 2, 1, 68, 1, 5 };

    UINT4               au4VrrpAuthErrorType[] = { 1, 3, 6, 1, 2, 1, 68, 1, 6 };

    UNUSED_PARAM (u1MibValue);

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        return;
    }

    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4VrrpNotification) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        free_oid (pOid);

        return;
    }

    u4Len = sizeof (au4VrrpNotification) / sizeof (UINT4);

    if (u1MibVariable == VRRPSTATSNEWMASTERREASON)
    {
        au4VrrpNotification[u4Len - 1] = VRRP_ONE;
    }
    else if (u1MibVariable == VRRPSTATSAUTHERRORTYPE)
    {
        au4VrrpNotification[u4Len - 1] = VRRP_TWO;
    }

    MEMCPY (pOidValue->pu4_OidList, au4VrrpNotification,
            sizeof (au4VrrpNotification));
    pOidValue->u4_Length = u4Len;

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);

        return;
    }

    pStartVb = pVbList;

    if (u1MibVariable == VRRPSTATSNEWMASTERREASON)
    {
        u4Len = sizeof (au4VrrpMasterIpAddr) / sizeof (UINT4);

        /* Value of the Trap  -> vrrpv3NewMaster */
        pOid = alloc_oid ((INT4) u4Len);

        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        au4VrrpMasterIpAddr[u4Len - 2] = (UINT4) OPERIFINDEX (i4OperIndex);
        au4VrrpMasterIpAddr[u4Len - 1] = (UINT4) OPERVRID (i4OperIndex);

        MEMCPY (pOid->pu4_OidList, au4VrrpMasterIpAddr,
                sizeof (au4VrrpMasterIpAddr));
        pOid->u4_Length = u4Len;

        pOString
            = SNMP_AGT_FormOctetString (OPERMASTERIPADDR (i4OperIndex).au1Addr,
                                        OPERMASTERIPADDR (i4OperIndex).
                                        u1AddrLen);

        if (pOString == NULL)
        {
            free_oid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        pVbList->pNextVarBind =
            ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_IP_ADDR_PRIM,
                                                      0, 0,
                                                      pOString, NULL,
                                                      SnmpCnt64Type));

        if (pVbList->pNextVarBind == NULL)
        {
            free_oid (pOid);
            free_octetstring (pOString);
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;
    }
    else if (u1MibVariable == VRRPSTATSAUTHERRORTYPE)
    {
        /* Value of the Trap  -> vrrpAuthError Trap */
        u4Len = (sizeof (au4VrrpAuthPktSrc) / sizeof (UINT4));

        pOid = alloc_oid ((INT4) u4Len);

        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        MEMCPY (pOid->pu4_OidList, au4VrrpAuthPktSrc,
                sizeof (au4VrrpAuthPktSrc));
        pOid->u4_Length = u4Len;

        pOString
            = SNMP_AGT_FormOctetString (gSenderIpAddr.au1Addr,
                                        gSenderIpAddr.u1AddrLen);

        if (pOString == NULL)
        {
            free_oid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        pVbList->pNextVarBind =
            ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_IP_ADDR_PRIM,
                                                      0, 0,
                                                      pOString, NULL,
                                                      SnmpCnt64Type));

        if (pVbList->pNextVarBind == NULL)
        {
            free_oid (pOid);
            free_octetstring (pOString);
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        pVbList = pVbList->pNextVarBind;

        u4Len = (sizeof (au4VrrpAuthErrorType) / sizeof (UINT4));

        pOid = alloc_oid ((INT4) u4Len);

        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        MEMCPY (pOid->pu4_OidList, au4VrrpAuthErrorType,
                sizeof (au4VrrpAuthErrorType));
        pOid->u4_Length = u4Len;

        pVbList->pNextVarBind =
            ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER32,
                                                      0, (INT4) u1MibValue,
                                                      NULL, NULL,
                                                      SnmpCnt64Type));

        if (pVbList->pNextVarBind == NULL)
        {
            free_oid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;
    }

#ifdef SNMP_3_WANTED
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    SNMP_AGT_FreeVarBindList (pStartVb);
#endif

    return;
}

/*******************************************************************************
*Function Name   :   PktNotifyV3StatsToSnmpManager
*Description     :   This Funtion is called to notify V3 Stats SNMP Manager
*
*Global Varibles :                                                              
*                    None                                                    
*Inputs          : 
*                    i4OperIndex     -   Index of the OperEntryTable         
*                    u1MibVariable   -   MIB Variable to be Updated   
*                    u1MibValue      -   MIB Value to be notified
*Output          :                                                      
*                    None
*Returns         :                                                              
*                    None
*******************************************************************************/
VOID
PktNotifyV3StatsToSnmpManager (INT4 i4OperIndex, UINT1 u1MibVariable,
                               UINT1 u1MibValue)
{
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;

    tSNMP_OCTET_STRING_TYPE *pOString = NULL;
    UINT4               u4Len = 0;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

    UINT4               au4VrrpNotification[] = { 1, 3, 6, 1, 2, 1, 207, 0, 0 };

    UINT4               au4VrrpMasterIpAddr[] =
        { 1, 3, 6, 1, 2, 1, 207, 1, 1, 1, 1, 3, 0, 0, 0 };

    UINT4               au4VrrpNewMasterReason[] =
        { 1, 3, 6, 1, 2, 1, 207, 1, 2, 5, 1, 2, 0, 0, 0 };

    UINT4               au4VrrpProtoErrReason[] =
        { 1, 3, 6, 1, 2, 1, 207, 1, 2, 5, 1, 6, 0, 0, 0 };

    UINT4               u4Val = 0;

    MEMSET (&SnmpCnt64Type, 0, sizeof (tSNMP_COUNTER64_TYPE));

    if (u1MibVariable == VRRPSTATSNEWMASTERREASON)
    {
        u4Val = VRRP_ONE;
    }
    else if (u1MibVariable == VRRPSTATSPROTOERRREASON)
    {
        if (u1MibValue > VRRP_VRID_ERROR)
        {
            return;
        }

        u4Val = VRRP_TWO;
    }

    u4Len = (sizeof (au4snmpTrapOid) / sizeof (UINT4));

    pOid = alloc_oid ((INT4) u4Len);
    if (pOid == NULL)
    {
        return;
    }

    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = u4Len;

    u4Len = (sizeof (au4VrrpNotification) / sizeof (UINT4));

    pOidValue = alloc_oid ((INT4) u4Len);
    if (pOidValue == NULL)
    {
        free_oid (pOid);

        return;
    }

    au4VrrpNotification[u4Len - 1] = u4Val;

    MEMCPY (pOidValue->pu4_OidList, au4VrrpNotification,
            sizeof (au4VrrpNotification));
    pOidValue->u4_Length = u4Len;

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);

        return;
    }

    pStartVb = pVbList;

    if (u1MibVariable == VRRPSTATSNEWMASTERREASON)
    {
        u4Len = (sizeof (au4VrrpMasterIpAddr) / sizeof (UINT4));

        /* Value of the Trap  -> vrrpv3NewMaster */
        pOid = alloc_oid ((INT4) u4Len);

        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        au4VrrpMasterIpAddr[u4Len - 3] = (UINT4) OPERIFINDEX (i4OperIndex);
        au4VrrpMasterIpAddr[u4Len - 2] = (UINT4) OPERVRID (i4OperIndex);
        au4VrrpMasterIpAddr[u4Len - 1] = (UINT4) OPERADDRTYPE (i4OperIndex);

        MEMCPY (pOid->pu4_OidList, au4VrrpMasterIpAddr,
                sizeof (au4VrrpMasterIpAddr));
        pOid->u4_Length = u4Len;

        pOString =
            SNMP_AGT_FormOctetString (OPERMASTERIPADDR (i4OperIndex).au1Addr,
                                      (INT4)
                                      OPERMASTERIPADDR (i4OperIndex).u1AddrLen);

        if (pOString == NULL)
        {
            free_oid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        pVbList->pNextVarBind =
            ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_OCTET_PRIM,
                                                      0, 0,
                                                      pOString, NULL,
                                                      SnmpCnt64Type));

        if (pVbList->pNextVarBind == NULL)
        {
            free_oid (pOid);
            free_octetstring (pOString);
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        pVbList = pVbList->pNextVarBind;

        u4Len = (sizeof (au4VrrpNewMasterReason) / sizeof (UINT4));

        pOid = alloc_oid ((INT4) u4Len);

        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        au4VrrpNewMasterReason[u4Len - 3] = (UINT4) OPERIFINDEX (i4OperIndex);
        au4VrrpNewMasterReason[u4Len - 2] = (UINT4) OPERVRID (i4OperIndex);
        au4VrrpNewMasterReason[u4Len - 1] = (UINT4) OPERADDRTYPE (i4OperIndex);

        MEMCPY (pOid->pu4_OidList, au4VrrpNewMasterReason,
                sizeof (au4VrrpNewMasterReason));
        pOid->u4_Length = u4Len;

        pVbList->pNextVarBind =
            ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER32,
                                                      0,
                                                      (INT4)
                                                      STATSMASTERREASON
                                                      (i4OperIndex), NULL, NULL,
                                                      SnmpCnt64Type));

        if (pVbList->pNextVarBind == NULL)
        {
            free_oid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;
    }
    else if (u1MibVariable == VRRPSTATSPROTOERRREASON)
    {
        /* Value of the Trap  -> vrrpv3ProtoError */
        u4Len = (sizeof (au4VrrpProtoErrReason) / sizeof (UINT4));

        pOid = alloc_oid ((INT4) u4Len);

        if (pOid == NULL)
        {
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        au4VrrpProtoErrReason[u4Len - 3] = (UINT4) OPERIFINDEX (i4OperIndex);
        au4VrrpProtoErrReason[u4Len - 2] = (UINT4) OPERVRID (i4OperIndex);
        au4VrrpProtoErrReason[u4Len - 1] = (UINT4) OPERADDRTYPE (i4OperIndex);

        MEMCPY (pOid->pu4_OidList, au4VrrpProtoErrReason,
                sizeof (au4VrrpProtoErrReason));
        pOid->u4_Length = u4Len;

        pVbList->pNextVarBind =
            ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                      SNMP_DATA_TYPE_INTEGER32,
                                                      0,
                                                      (INT4)
                                                      STATSPROTOERRREASON
                                                      (i4OperIndex), NULL, NULL,
                                                      SnmpCnt64Type));

        if (pVbList->pNextVarBind == NULL)
        {
            free_oid (pOid);
            SNMP_free_snmp_vb_list (pStartVb);

            return;
        }

        pVbList = pVbList->pNextVarBind;
        pVbList->pNextVarBind = NULL;
    }

#ifdef SNMP_3_WANTED
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    SNMP_AGT_FreeVarBindList (pStartVb);
#endif

    return;
}
