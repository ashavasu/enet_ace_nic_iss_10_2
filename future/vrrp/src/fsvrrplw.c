/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvrrplw.c,v 1.13 2014/07/20 11:09:51 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "vrrpinc.h"
#include "vrrpcli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVrrpStatus
 Input       :  The Indices

                The Object 
                retValFsVrrpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpStatus (INT4 *pi4RetValFsVrrpStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_2, VRRP_MIB_GBL_STATUS, &Data);

    *pi4RetValFsVrrpStatus = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpMaxOperEntries
 Input       :  The Indices

                The Object 
                retValFsVrrpMaxOperEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpMaxOperEntries (INT4 *pi4RetValFsVrrpMaxOperEntries)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_2, VRRP_MIB_MAX_OPER_ENTRIES, &Data);

    *pi4RetValFsVrrpMaxOperEntries = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVrrpStatus
 Input       :  The Indices

                The Object 
                setValFsVrrpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpStatus (INT4 i4SetValFsVrrpStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpStatus;

    i4RetValue = VrrpSetAllScalars (VRRP_VERSION_2, VRRP_MIB_GBL_STATUS, &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVrrpStatus
 Input       :  The Indices

                The Object 
                testValFsVrrpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsVrrpStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpStatus;

    i4RetValue = VrrpTestAllScalars (VRRP_VERSION_2, VRRP_MIB_GBL_STATUS,
                                     &Data, pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVrrpStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVrrpStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsVrrpOperTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVrrpOperTable
 Input       :  The Indices
                IfIndex
                VrrpOperVrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVrrpOperTable (INT4 i4IfIndex, INT4 i4VrrpOperVrId)
{
    return (nmhValidateIndexInstanceVrrpOperTable (i4IfIndex, i4VrrpOperVrId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVrrpOperTable
 Input       :  The Indices
                IfIndex
                VrrpOperVrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVrrpOperTable (INT4 *pi4IfIndex, INT4 *pi4VrrpOperVrId)
{
    return (nmhGetFirstIndexVrrpOperTable (pi4IfIndex, pi4VrrpOperVrId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVrrpOperTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                VrrpOperVrId
                nextVrrpOperVrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVrrpOperTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                INT4 i4VrrpOperVrId, INT4 *pi4NextVrrpOperVrId)
{
    return (nmhGetNextIndexVrrpOperTable (i4IfIndex, pi4NextIfIndex,
                                          i4VrrpOperVrId, pi4NextVrrpOperVrId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVrrpAdminPriority
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                retValFsVrrpAdminPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpAdminPriority (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                           INT4 *pi4RetValFsVrrpAdminPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ADMIN_PRIO, &Data);

    *pi4RetValFsVrrpAdminPriority = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 * Function    :  nmhGetFsVrrpOperAdvertisementIntervalInMsec
 * Input       :  The Indices
 *                IfIndex
 *                VrrpOperVrId
 *                The Object
 *                retValFsVrrpOperAdvertisementIntervalInMsec
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsVrrpOperAdvertisementIntervalInMsec (INT4 i4IfIndex,
                                             INT4 i4VrrpOperVrId,
                                             INT4
                                             *pi4RetValFsVrrpOperAdvertisementIntervalInMsec)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ADVT_INT_MSEC, &Data);

    *pi4RetValFsVrrpOperAdvertisementIntervalInMsec = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpOperTrackGroupId
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                retValFsVrrpOperTrackGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpOperTrackGroupId (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                              UINT4 *pu4RetValFsVrrpOperTrackGroupId)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_TRACK_GRP_ID, &Data);

    *pu4RetValFsVrrpOperTrackGroupId = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpOperDecrementPriority
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                retValFsVrrpOperDecrementPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpOperDecrementPriority (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                   UINT4 *pu4RetValFsVrrpOperDecrementPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_DEC_PRIORITY, &Data);

    *pu4RetValFsVrrpOperDecrementPriority = Data.u4_ULongValue;

    return ((INT1) i4RetValue);

}

/****************************************************************************
 Function    :  nmhGetFsVrrpAcceptMode
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                retValFsVrrpAcceptMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpAcceptMode (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                        INT4 *pi4RetValFsVrrpAcceptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ACCEPT_MODE, &Data);

    *pi4RetValFsVrrpAcceptMode = Data.i4_SLongValue;

    return ((INT1) i4RetValue);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVrrpAdminPriority
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                setValFsVrrpAdminPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpAdminPriority (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                           INT4 i4SetValFsVrrpAdminPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpAdminPriority;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ADMIN_PRIO, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 * Function    :  nmhSetFsVrrpOperAdvertisementIntervalInMsec
 * Input       :  The Indices
 *                IfIndex
 *                VrrpOperVrId
 *                The Object
 *                setValFsVrrpOperAdvertisementIntervalInMsec
 * Output      :  The Set Low Lev Routine Take the Indices &
 *                Sets the Value accordingly.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFsVrrpOperAdvertisementIntervalInMsec (INT4 i4IfIndex,
                                             INT4 i4VrrpOperVrId,
                                             INT4
                                             i4SetValFsVrrpOperAdvertisementIntervalInMsec)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpOperAdvertisementIntervalInMsec;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ADVT_INT_MSEC, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetFsVrrpOperTrackGroupId
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                setValFsVrrpOperTrackGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpOperTrackGroupId (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                              UINT4 u4SetValFsVrrpOperTrackGroupId)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4SetValFsVrrpOperTrackGroupId;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_TRACK_GRP_ID, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetFsVrrpOperDecrementPriority
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                setValFsVrrpOperDecrementPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpOperDecrementPriority (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                   UINT4 u4SetValFsVrrpOperDecrementPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4SetValFsVrrpOperDecrementPriority;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_DEC_PRIORITY, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetFsVrrpAcceptMode
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object
                setValFsVrrpAcceptMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpAcceptMode (INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                        INT4 i4SetValFsVrrpAcceptMode)
{

    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpAcceptMode;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                      i4VrrpOperVrId,
                                      VRRP_IPVX_ADDR_FMLY_IPV4,
                                      VRRP_MIB_OPER_ACCEPT_MODE, &Data);
    return ((INT1) i4RetValue);

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVrrpAdminPriority
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                testValFsVrrpAdminPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpAdminPriority (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                              INT4 i4VrrpOperVrId,
                              INT4 i4TestValFsVrrpAdminPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpAdminPriority;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_ADMIN_PRIO, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 * Function    :  nmhTestv2FsVrrpOperAdvertisementIntervalInMsec
 * Input       :  The Indices
 *                IfIndex
 *                VrrpOperVrId
 *                The Object
 *                testValFsVrrpOperAdvertisementIntervalInMsec
 * Output      :  The Test Low Lev Routine Take the Indices &
 *                Test whether that Value is Valid Input for Set.
 *                Stores the value of error code in the Return val
 * Error Codes :  The following error codes are to be returned
 *                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FsVrrpOperAdvertisementIntervalInMsec (UINT4 *pu4ErrorCode,
                                                INT4 i4IfIndex,
                                                INT4 i4VrrpOperVrId,
                                                INT4
                                                i4TestValFsVrrpOperAdvertisementIntervalInMsec)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpOperAdvertisementIntervalInMsec;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_ADVT_INT_MSEC, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsVrrpOperTrackGroupId
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                testValFsVrrpOperTrackGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpOperTrackGroupId (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                 INT4 i4VrrpOperVrId,
                                 UINT4 u4TestValFsVrrpOperTrackGroupId)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4TestValFsVrrpOperTrackGroupId;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_TRACK_GRP_ID, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsVrrpOperDecrementPriority
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                testValFsVrrpOperDecrementPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpOperDecrementPriority (UINT4 *pu4ErrorCode,
                                      INT4 i4IfIndex,
                                      INT4 i4VrrpOperVrId,
                                      UINT4
                                      u4TestValFsVrrpOperDecrementPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4TestValFsVrrpOperDecrementPriority;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_DEC_PRIORITY, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsVrrpAcceptMode
 Input       :  The Indices
                IfIndex
                VrrpOperVrId

                The Object 
                testValFsVrrpAcceptMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpAcceptMode (UINT4 *pu4ErrorCode,
                           INT4 i4IfIndex,
                           INT4 i4VrrpOperVrId, INT4 i4TestValFsVrrpAcceptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpAcceptMode;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_2, i4IfIndex,
                                       i4VrrpOperVrId,
                                       VRRP_IPVX_ADDR_FMLY_IPV4,
                                       VRRP_MIB_OPER_ACCEPT_MODE, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVrrpOperTable
 Input       :  The Indices
                IfIndex
                VrrpOperVrId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVrrpOperTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVrrpAuthDeprecate
 Input       :  The Indices

                The Object 
                retValFsVrrpAuthDeprecate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpAuthDeprecate (INT4 *pi4RetValFsVrrpAuthDeprecate)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_2, VRRP_MIB_AUTH_DEPRECATE, &Data);

    *pi4RetValFsVrrpAuthDeprecate = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 *Function    :  nmhGetFsVrrpTraceOption
 *Input       :  The Indices
 *
 *               The Object
 *               retValFsVrrpTraceOption
 *Output      :  The Get Low Lev Routine Take the Indices &
 *               store the Value requested in the Return val.
 *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsVrrpTraceOption (INT4 *pi4RetValFsVrrpTraceOption)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_2, VRRP_MIB_TRACE_OPTION, &Data);

    *pi4RetValFsVrrpTraceOption = Data.i4_SLongValue;

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVrrpAuthDeprecate
 Input       :  The Indices

                The Object 
                setValFsVrrpAuthDeprecate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpAuthDeprecate (INT4 i4SetValFsVrrpAuthDeprecate)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpAuthDeprecate;

    i4RetValue = VrrpSetAllScalars (VRRP_VERSION_2, VRRP_MIB_AUTH_DEPRECATE,
                                    &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 *Function    :  nmhSetFsVrrpTraceOption
 *Input       :  The Indices
 *               The Object
 *               setValFsVrrpTraceOption
 *Output      :  The Set Low Lev Routine Take the Indices &
 *               Sets the Value accordingly.
 *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFsVrrpTraceOption (INT4 i4SetValFsVrrpTraceOption)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpTraceOption;

    i4RetValue = VrrpSetAllScalars (VRRP_VERSION_2, VRRP_MIB_TRACE_OPTION,
                                    &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVrrpAuthDeprecate
 Input       :  The Indices

                The Object 
                testValFsVrrpAuthDeprecate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpAuthDeprecate (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsVrrpAuthDeprecate)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpAuthDeprecate;

    i4RetValue = VrrpTestAllScalars (VRRP_VERSION_2, VRRP_MIB_AUTH_DEPRECATE,
                                     &Data, pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 *Function    :  nmhTestv2FsVrrpTraceOption
 *Input       :  The Indices
 *
 *               The Object
 *               testValFsVrrpTraceOption
 *Output      :  The Test Low Lev Routine Take the Indices &
 *               Test whether that Value is Valid Input for Set.
 *               Stores the value of error code in the Return val
 *Error Codes :  The following error codes are to be returned
 *               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FsVrrpTraceOption (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsVrrpTraceOption)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpTraceOption;

    i4RetValue = VrrpTestAllScalars (VRRP_VERSION_2, VRRP_MIB_TRACE_OPTION,
                                     &Data, pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVrrpAuthDeprecate
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVrrpAuthDeprecate (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 *Function    :  nmhDepv2FsVrrpTraceOption
 *Output      :  The Dependency Low Lev Routine Take the Indices &
 *               check whether dependency is met or not.
 *               Stores the value of error code in the Return val
 *Error Codes :  The following error codes are to be returned
 *               SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *               SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *               SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *               SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *               SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhDepv2FsVrrpTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsVrrpOperTrackGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVrrpOperTrackGroupTable
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVrrpOperTrackGroupTable (UINT4
                                                   u4FsVrrpOperTrackGroupIndex)
{
    UNUSED_PARAM (u4FsVrrpOperTrackGroupIndex);

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVrrpOperTrackGroupTable
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVrrpOperTrackGroupTable (UINT4 *pu4FsVrrpOperTrackGroupIndex)
{
    return (nmhGetNextIndexFsVrrpOperTrackGroupTable (0,
                                                      pu4FsVrrpOperTrackGroupIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVrrpOperTrackGroupTable
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
                nextFsVrrpOperTrackGroupIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVrrpOperTrackGroupTable (UINT4 u4FsVrrpOperTrackGroupIndex,
                                          UINT4
                                          *pu4NextFsVrrpOperTrackGroupIndex)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    BOOL1               b1VersionMatched = FALSE;

    b1VersionMatched = VrrpCompCheckVersion (VRRP_VERSION_2);

    if (b1VersionMatched == FALSE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    pTrackGroupEntry
        = VrrpDbGetNextTrackGroupEntry (u4FsVrrpOperTrackGroupIndex);

    if (pTrackGroupEntry == NULL)
    {
        return ((INT1) SNMP_FAILURE);
    }

    *pu4NextFsVrrpOperTrackGroupIndex = pTrackGroupEntry->u4GroupIndex;

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVrrpOperTrackedGroupTrackedLinks
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex

                The Object 
                retValFsVrrpOperTrackedGroupTrackedLinks
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpOperTrackedGroupTrackedLinks (UINT4 u4FsVrrpOperTrackGroupIndex,
                                          UINT4
                                          *pu4RetValFsVrrpOperTrackedGroupTrackedLinks)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllTrackGroupTable (VRRP_VERSION_2,
                                            u4FsVrrpOperTrackGroupIndex,
                                            VRRP_MIB_TRACK_GROUP_LINKS, &Data);

    *pu4RetValFsVrrpOperTrackedGroupTrackedLinks = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetFsVrrpOperTrackRowStatus
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex

                The Object 
                retValFsVrrpOperTrackRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpOperTrackRowStatus (UINT4 u4FsVrrpOperTrackGroupIndex,
                                INT4 *pi4RetValFsVrrpOperTrackRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllTrackGroupTable (VRRP_VERSION_2,
                                            u4FsVrrpOperTrackGroupIndex,
                                            VRRP_MIB_TRACK_ROW_STATUS, &Data);

    *pi4RetValFsVrrpOperTrackRowStatus = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVrrpOperTrackedGroupTrackedLinks
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex

                The Object 
                setValFsVrrpOperTrackedGroupTrackedLinks
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpOperTrackedGroupTrackedLinks (UINT4 u4FsVrrpOperTrackGroupIndex,
                                          UINT4
                                          u4SetValFsVrrpOperTrackedGroupTrackedLinks)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4SetValFsVrrpOperTrackedGroupTrackedLinks;

    i4RetValue = VrrpSetAllTrackGroupTable (VRRP_VERSION_2,
                                            u4FsVrrpOperTrackGroupIndex,
                                            VRRP_MIB_TRACK_GROUP_LINKS, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetFsVrrpOperTrackRowStatus
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex

                The Object 
                setValFsVrrpOperTrackRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpOperTrackRowStatus (UINT4 u4FsVrrpOperTrackGroupIndex,
                                INT4 i4SetValFsVrrpOperTrackRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpOperTrackRowStatus;

    i4RetValue = VrrpSetAllTrackGroupTable (VRRP_VERSION_2,
                                            u4FsVrrpOperTrackGroupIndex,
                                            VRRP_MIB_TRACK_ROW_STATUS, &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVrrpOperTrackedGroupTrackedLinks
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex

                The Object 
                testValFsVrrpOperTrackedGroupTrackedLinks
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpOperTrackedGroupTrackedLinks (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsVrrpOperTrackGroupIndex,
                                             UINT4
                                             u4TestValFsVrrpOperTrackedGroupTrackedLinks)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.u4_ULongValue = u4TestValFsVrrpOperTrackedGroupTrackedLinks;

    i4RetValue = VrrpTestAllTrackGroupTable (VRRP_VERSION_2,
                                             u4FsVrrpOperTrackGroupIndex,
                                             VRRP_MIB_TRACK_GROUP_LINKS, &Data,
                                             pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2FsVrrpOperTrackRowStatus
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex

                The Object 
                testValFsVrrpOperTrackRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpOperTrackRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsVrrpOperTrackGroupIndex,
                                   INT4 i4TestValFsVrrpOperTrackRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpOperTrackRowStatus;

    i4RetValue = VrrpTestAllTrackGroupTable (VRRP_VERSION_2,
                                             u4FsVrrpOperTrackGroupIndex,
                                             VRRP_MIB_TRACK_ROW_STATUS, &Data,
                                             pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVrrpOperTrackGroupTable
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVrrpOperTrackGroupTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsVrrpOperTrackGroupIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVrrpOperTrackGroupIfTable
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
                FsVrrpOperTrackGroupIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVrrpOperTrackGroupIfTable (UINT4
                                                     u4FsVrrpOperTrackGroupIndex,
                                                     INT4
                                                     i4FsVrrpOperTrackGroupIfIndex)
{
    UNUSED_PARAM (u4FsVrrpOperTrackGroupIndex);
    UNUSED_PARAM (i4FsVrrpOperTrackGroupIfIndex);

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVrrpOperTrackGroupIfTable
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
                FsVrrpOperTrackGroupIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVrrpOperTrackGroupIfTable (UINT4
                                             *pu4FsVrrpOperTrackGroupIndex,
                                             INT4
                                             *pi4FsVrrpOperTrackGroupIfIndex)
{
    return (nmhGetNextIndexFsVrrpOperTrackGroupIfTable (0,
                                                        pu4FsVrrpOperTrackGroupIndex,
                                                        0,
                                                        pi4FsVrrpOperTrackGroupIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVrrpOperTrackGroupIfTable
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
                nextFsVrrpOperTrackGroupIndex
                FsVrrpOperTrackGroupIfIndex
                nextFsVrrpOperTrackGroupIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVrrpOperTrackGroupIfTable (UINT4 u4FsVrrpOperTrackGroupIndex,
                                            UINT4
                                            *pu4NextFsVrrpOperTrackGroupIndex,
                                            INT4 i4FsVrrpOperTrackGroupIfIndex,
                                            INT4
                                            *pi4NextFsVrrpOperTrackGroupIfIndex)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;
    BOOL1               b1VersionMatched = FALSE;

    b1VersionMatched = VrrpCompCheckVersion (VRRP_VERSION_2);

    if (b1VersionMatched == FALSE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    pTrackGroupEntry = VrrpDbGetTrackGroupEntry (u4FsVrrpOperTrackGroupIndex);

    if (pTrackGroupEntry != NULL)
    {
        pTrackGroupIfEntry
            = VrrpDbGetNextTrackGroupIfEntry (pTrackGroupEntry,
                                              i4FsVrrpOperTrackGroupIfIndex);

        if (pTrackGroupIfEntry != NULL)
        {
            *pu4NextFsVrrpOperTrackGroupIndex = u4FsVrrpOperTrackGroupIndex;
            *pi4NextFsVrrpOperTrackGroupIfIndex = pTrackGroupIfEntry->i4IfIndex;

            return ((INT1) SNMP_SUCCESS);
        }
    }

    pTrackGroupEntry
        = VrrpDbGetNextTrackGroupEntry (u4FsVrrpOperTrackGroupIndex);

    if (pTrackGroupEntry == NULL)
    {
        return ((INT1) SNMP_FAILURE);
    }

    pTrackGroupIfEntry = VrrpDbGetNextTrackGroupIfEntry (pTrackGroupEntry, 0);

    if (pTrackGroupIfEntry == NULL)
    {
        return ((INT1) SNMP_FAILURE);
    }

    *pu4NextFsVrrpOperTrackGroupIndex = pTrackGroupEntry->u4GroupIndex;
    *pi4NextFsVrrpOperTrackGroupIfIndex = pTrackGroupIfEntry->i4IfIndex;

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVrrpOperTrackGroupIfRowStatus
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
                FsVrrpOperTrackGroupIfIndex

                The Object 
                retValFsVrrpOperTrackGroupIfRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVrrpOperTrackGroupIfRowStatus (UINT4 u4FsVrrpOperTrackGroupIndex,
                                       INT4 i4FsVrrpOperTrackGroupIfIndex,
                                       INT4
                                       *pi4RetValFsVrrpOperTrackGroupIfRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllTrackGroupIfTable (VRRP_VERSION_2,
                                              u4FsVrrpOperTrackGroupIndex,
                                              i4FsVrrpOperTrackGroupIfIndex,
                                              VRRP_MIB_TRACK_IF_ROW_STATUS,
                                              &Data);

    *pi4RetValFsVrrpOperTrackGroupIfRowStatus = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVrrpOperTrackGroupIfRowStatus
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
                FsVrrpOperTrackGroupIfIndex

                The Object 
                setValFsVrrpOperTrackGroupIfRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVrrpOperTrackGroupIfRowStatus (UINT4 u4FsVrrpOperTrackGroupIndex,
                                       INT4 i4FsVrrpOperTrackGroupIfIndex,
                                       INT4
                                       i4SetValFsVrrpOperTrackGroupIfRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValFsVrrpOperTrackGroupIfRowStatus;

    i4RetValue = VrrpSetAllTrackGroupIfTable (VRRP_VERSION_2,
                                              u4FsVrrpOperTrackGroupIndex,
                                              i4FsVrrpOperTrackGroupIfIndex,
                                              VRRP_MIB_TRACK_IF_ROW_STATUS,
                                              &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVrrpOperTrackGroupIfRowStatus
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
                FsVrrpOperTrackGroupIfIndex

                The Object 
                testValFsVrrpOperTrackGroupIfRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVrrpOperTrackGroupIfRowStatus (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsVrrpOperTrackGroupIndex,
                                          INT4 i4FsVrrpOperTrackGroupIfIndex,
                                          INT4
                                          i4TestValFsVrrpOperTrackGroupIfRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValFsVrrpOperTrackGroupIfRowStatus;

    i4RetValue = VrrpTestAllTrackGroupIfTable (VRRP_VERSION_2,
                                               u4FsVrrpOperTrackGroupIndex,
                                               i4FsVrrpOperTrackGroupIfIndex,
                                               VRRP_MIB_TRACK_IF_ROW_STATUS,
                                               &Data, pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVrrpOperTrackGroupIfTable
 Input       :  The Indices
                FsVrrpOperTrackGroupIndex
                FsVrrpOperTrackGroupIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVrrpOperTrackGroupIfTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}
