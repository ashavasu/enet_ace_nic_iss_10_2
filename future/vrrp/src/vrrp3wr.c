/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vrrp3wr.c,
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "lr.h" 
#include  "fssnmp.h" 
#include  "vrrp3lw.h"
#include  "vrrp3wr.h"
#include  "vrrp3db.h"
#include  "ip.h"
#include  "vrrp.h"

INT4 GetNextIndexVrrpv3OperationsTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexVrrpv3OperationsTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexVrrpv3OperationsTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}


VOID RegisterVRRP3 ()
{
	SNMPRegisterMibWithLock (&vrrp3OID, &vrrp3Entry, 
                             VrrpLock, VrrpUnLock, SNMP_MSR_TGR_TRUE);

	SNMPAddSysorEntry (&vrrp3OID, (const UINT1 *) "stdvrrp3");
}



VOID UnRegisterVRRP3 ()
{
	SNMPUnRegisterMib (&vrrp3OID, &vrrp3Entry);
	SNMPDelSysorEntry (&vrrp3OID, (const UINT1 *) "stdvrrp3");
}

INT4 Vrrpv3OperationsMasterIpAddrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3OperationsMasterIpAddr(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 Vrrpv3OperationsPrimaryIpAddrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3OperationsPrimaryIpAddr(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 Vrrpv3OperationsVirtualMacAddrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	pMultiData->pOctetStrValue->i4_Length = 6;
	return(nmhGetVrrpv3OperationsVirtualMacAddr(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		(tMacAddr *)pMultiData->pOctetStrValue->pu1_OctetList));

}
INT4 Vrrpv3OperationsStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3OperationsStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Vrrpv3OperationsPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3OperationsPriority(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Vrrpv3OperationsAddrCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3OperationsAddrCount(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Vrrpv3OperationsAdvIntervalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3OperationsAdvInterval(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Vrrpv3OperationsPreemptModeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3OperationsPreemptMode(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Vrrpv3OperationsAcceptModeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3OperationsAcceptMode(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Vrrpv3OperationsUpTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3OperationsUpTime(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Vrrpv3OperationsRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3OperationsRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Vrrpv3OperationsPrimaryIpAddrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVrrpv3OperationsPrimaryIpAddr(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 Vrrpv3OperationsPrioritySet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVrrpv3OperationsPriority(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 Vrrpv3OperationsAdvIntervalSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVrrpv3OperationsAdvInterval(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Vrrpv3OperationsPreemptModeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVrrpv3OperationsPreemptMode(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Vrrpv3OperationsAcceptModeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVrrpv3OperationsAcceptMode(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Vrrpv3OperationsRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVrrpv3OperationsRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Vrrpv3OperationsPrimaryIpAddrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Vrrpv3OperationsPrimaryIpAddr(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 Vrrpv3OperationsPriorityTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Vrrpv3OperationsPriority(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 Vrrpv3OperationsAdvIntervalTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Vrrpv3OperationsAdvInterval(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Vrrpv3OperationsPreemptModeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Vrrpv3OperationsPreemptMode(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Vrrpv3OperationsAcceptModeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Vrrpv3OperationsAcceptMode(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Vrrpv3OperationsRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Vrrpv3OperationsRowStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 Vrrpv3OperationsTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Vrrpv3OperationsTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexVrrpv3AssociatedIpAddrTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexVrrpv3AssociatedIpAddrTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexVrrpv3AssociatedIpAddrTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue),
			pFirstMultiIndex->pIndex[3].pOctetStrValue,
			pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 Vrrpv3AssociatedIpAddrRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3AssociatedIpAddrTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3AssociatedIpAddrRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Vrrpv3AssociatedIpAddrRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetVrrpv3AssociatedIpAddrRowStatus(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 Vrrpv3AssociatedIpAddrRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2Vrrpv3AssociatedIpAddrRowStatus(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiIndex->pIndex[3].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 Vrrpv3AssociatedIpAddrTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2Vrrpv3AssociatedIpAddrTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 Vrrpv3RouterChecksumErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetVrrpv3RouterChecksumErrors(&(pMultiData->u8_Counter64Value)));
}
INT4 Vrrpv3RouterVersionErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetVrrpv3RouterVersionErrors(&(pMultiData->u8_Counter64Value)));
}
INT4 Vrrpv3RouterVrIdErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetVrrpv3RouterVrIdErrors(&(pMultiData->u8_Counter64Value)));
}
INT4 Vrrpv3GlobalStatisticsDiscontinuityTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetVrrpv3GlobalStatisticsDiscontinuityTime(&(pMultiData->u4_ULongValue)));
}

INT4 GetNextIndexVrrpv3StatisticsTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexVrrpv3StatisticsTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexVrrpv3StatisticsTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 Vrrpv3StatisticsMasterTransitionsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsMasterTransitions(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Vrrpv3StatisticsNewMasterReasonGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsNewMasterReason(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Vrrpv3StatisticsRcvdAdvertisementsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsRcvdAdvertisements(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 Vrrpv3StatisticsAdvIntervalErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsAdvIntervalErrors(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 Vrrpv3StatisticsIpTtlErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsIpTtlErrors(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 Vrrpv3StatisticsProtoErrReasonGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsProtoErrReason(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 Vrrpv3StatisticsRcvdPriZeroPacketsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsRcvdPriZeroPackets(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 Vrrpv3StatisticsSentPriZeroPacketsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsSentPriZeroPackets(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 Vrrpv3StatisticsRcvdInvalidTypePacketsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsRcvdInvalidTypePackets(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 Vrrpv3StatisticsAddressListErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsAddressListErrors(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 Vrrpv3StatisticsPacketLengthErrorsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsPacketLengthErrors(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 Vrrpv3StatisticsRowDiscontinuityTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsRowDiscontinuityTime(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 Vrrpv3StatisticsRefreshRateGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetVrrpv3StatisticsRefreshRate(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
