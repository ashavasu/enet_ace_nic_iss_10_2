/* $Id: vrrpconf.c,v 1.15 2014/03/15 14:22:32 siva Exp $*/
/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrpconf.c                                   |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Core Protocol.                          |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : Configuration Module of VRRP Core Protocol.   |
 * | All the SNMP MIB Variables are set to default values in this module      |
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |                                                                          |
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |  Padmanaban. B       |   Base Line Version             |
 * |                 |                      |                                 |
 * |  1.1.0.0        |  Padmanaban. B       |   Changes for IMS 1600          |
 * |                 |                      |                                 |
 * |  1.2.0.0        |  Padmanaban. B       |   VRRP Statistic table also     |
 * |                 |                      |   initialised.                  |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#include "vrrpinc.h"

/*******************************************************************************
 * Function Name    : VRRPConfig                              
 * Description      : This Function configures VRRP for effective
 *                    operation
 * Global Variables : VRRP OPER TABLE
 *                    VRRP IP ADDRESS TABLE
 *                    VRRP GLOBAL TABLE
 * Inputs           : None                                 
 * Output           : None
 * Returns          : None.     
 ******************************************************************************/
VOID
VrrpConfig ()
{
    UINT4               u4DestIp = VRRP_MCAST_ADDR;
    UINT1               au1Ip6Addr[IPVX_IPV6_ADDR_LEN] =
        { 0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x12
    };

    /* Set Destination IP Address of VRRP Advertisement Packet */
    VRRP_IPVX_COPY_TO_IPVX (gVrrpV4DestMCastAddr, u4DestIp);

    VRRP_IPVX_ADDR_INIT_IPVX (gVrrpV6DestMCastAddr,
                              (UINT1) VRRP_IPVX_ADDR_FMLY_IPV6, au1Ip6Addr);

    /* Configuring the Global Data Structures to their 
     * Default Values */

    STATSVERSIONERRORS = 0;
    STATSVIDERRORS = 0;

    FSAP_U8_CLR (&STATSCHECKSUMERRORS64);
    FSAP_U8_CLR (&STATSVERSIONERRORS64);
    FSAP_U8_CLR (&STATSVIDERRORS64);

    /* Oper Entry RB Tree */
    VrrpDbCreateOperTable ();

    /* Initialize Track Group List */
    UTL_DLL_INIT (&gTrackGroupHead,
                  FSAP_OFFSETOF (tVrrpTrackGroupTable, TrackGroupNode));

    /* Used in snmp-low routines. Flag to indicate operTable is set */
    gu1MaxOperEntries = VRRP_SET;

    return;
}

/*******************************************************************************
 * Function Name    : VrrpDefaultConfig                              
 * Description      : This Function configures given VRRP OperEntry for 
 *                     Default Value
 * Global Variables : VRRP OPER TABLE
 *                    VRRP IP ADDRESS TABLE
 *                    VRRP GLOBAL TABLE
 * Inputs           : i4Index - VrrpOperTable Index                             
 * Output           : None
 * Returns          : None.     
 ******************************************************************************/

VOID
VrrpDefaultConfig (INT4 i4Index)
{
    UINT1               au1IpAddr[IPVX_IPV6_ADDR_LEN];

    MEMSET (au1IpAddr, 0, IPVX_IPV6_ADDR_LEN);

    /* Initialise Oper Table */
    MEMSET (OPERMACADDR (i4Index), 0, MAC_ADDR_LEN);

    OPERRMVERSION (i4Index) = VRRP_HA_VERSION;

    OPERSTATE (i4Index) = INITIAL_STATE;

    OPERADMINSTATE (i4Index) = ADMINDOWN;

    VRRP_IPVX_ADDR_INIT_IPVX (OPERPRIMARYIPADDR (i4Index),
                              OPERADDRTYPE (i4Index), au1IpAddr);
    VRRP_IPVX_ADDR_INIT_IPVX (OPERMASTERIPADDR (i4Index),
                              OPERADDRTYPE (i4Index), au1IpAddr);
    VRRP_IPVX_ADDR_INIT_IPVX (INTERFACEIPADDR (i4Index),
                              OPERADDRTYPE (i4Index), au1IpAddr);

    OPERADMINPRIORITY (i4Index) = DEFAULT_PRIORITY;
    OPERAUTHTYPE (i4Index) = NO_AUTHENTICATION_MIB_VAL;

    OPERADVERINTL (i4Index) = V2_ADVERTISEMENTINTLINMSEC;

    OPERPREEMPTMODE (i4Index) = VRRP_ENABLE;
    OPERACCEPTMODE (i4Index) = VRRP_DISABLE;

    OPERPROTOCOL (i4Index) = DEFAULT_PROTOCOL;
    OPERROWSTATUS (i4Index) = ROW_NOT_CREATED;
    OPERIPADDRCOUNT (i4Index) = 0;
    OPERROUTERUPTIME (i4Index) = 0;

    /* Initialize AuthKey */
    OPERAUTHKEY (i4Index).i4_Length = 0;
    OPERAUTHKEY (i4Index).pu1_OctetList = NULL;

    OPERNEXTSTATE (i4Index) = 0;
    OPEREVENTRCVD (i4Index) = 0;
    OPERRCVDPRIORITY (i4Index) = 0;
    OPERRCVDMASTERADVTINT (i4Index) = 0;
    OPERSKEWTIME (i4Index) = 0;
    OPERMASTERDOWNINT (i4Index) = 0;
    OPERTRACKGRPID (i4Index) = 0;
    OPERDECPRIO (i4Index) = 0;
    OPERTRACKIFSTATUS (i4Index) = 0;

    VRRPOPERHWSTATUS (i4Index) = NP_NOT_UPDATED;

    /* Initialise Stats Table */
    VrrpClearStats (i4Index);

    /* Initialise Asso Table */
    UTL_DLL_INIT (&(OPERASSOCIPLIST (i4Index)),
                  FSAP_OFFSETOF (tIpAddrTable, AssocIpAddrNode));

    /* Initialize other DLL Nodes */
    TMO_DLL_Init_Node (&(OPERTRACKOPERNODE (i4Index)));

    return;
}

/*******************************************************************************
 * Function Name    : VrrpClearStats
 * Description      : This Function clears statistics table
 * Inputs           : i4Index - VrrpOperTable Index                             
 * Output           : None
 * Returns          : None.     
 ******************************************************************************/
VOID
VrrpClearStats (INT4 i4Index)
{
    STATSBECOMEMASTER (i4Index) = 0;
    STATSAUTHFAILURES (i4Index) = 0;
    STATSINVALIDAUTHTYPE (i4Index) = 0;
    STATSAUTHTYPEMISMATCH (i4Index) = 0;

    STATSMASTERREASON (i4Index) = 0;
    FSAP_U8_CLR (&STATSADVERTISERCVD64 (i4Index));
    FSAP_U8_CLR (&STATSADINTLERRORS64 (i4Index));
    FSAP_U8_CLR (&STATSIPTTLERRORS64 (i4Index));
    FSAP_U8_CLR (&STATSPTYZERORCVD64 (i4Index));
    FSAP_U8_CLR (&STATSPTYZEROSENT64 (i4Index));
    STATSPROTOERRREASON (i4Index) = 0;
    FSAP_U8_CLR (&STATSINVALIDTYPE64 (i4Index));
    FSAP_U8_CLR (&STATSADDRLISTERROR64 (i4Index));
    FSAP_U8_CLR (&STATSPACKETLENERROR64 (i4Index));
    STATSDISCTIME (i4Index) = 0;
    FSAP_U8_CLR (&STATSTXEDADVT (i4Index));
    FSAP_U8_CLR (&STATSTXEDADVT64 (i4Index));
    FSAP_U8_CLR (&STATSV2ADVTIGNORED (i4Index));

    return;
}
