/***************************************************************************
 * Copyright (C)2010 Aricent Inc . All Rights Reserved
 *
 * $Id: vrrpdb.c,v 1.6 2015/10/01 06:08:56 siva Exp $
 * Description:  This file contains the data structure utility functions for
 *               VRRP.
 * ************************************************************************/

#include "vrrpinc.h"

PRIVATE INT4 VrrpDbOperTableCmp PROTO ((tRBElem *, tRBElem *));

/***************************************************************************
 * FUNCTION NAME    : VrrpDbCreateOperTable
 *
 * DESCRIPTION      : This function creats the VRRP Oper Table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VRRP_SUCCESS / VRRP_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
VrrpDbCreateOperTable (VOID)
{
    gVrrpOperTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tVrrpOperTable, VrrpOperNode),
                              VrrpDbOperTableCmp);

    if (gVrrpOperTable == NULL)
    {
        VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                  "VRRP Init - Oper Table Creation Failed\r\n");
        return VRRP_NOT_OK;
    }

    VRRP_DBG (VRRP_TRC_INIT, "VRRP Init - Oper Table Created\r\n");
    return VRRP_OK;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeleteOperTable
 *
 * DESCRIPTION      : This function Deletes the VRRP Oper Table
 *                    
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeleteOperTable (VOID)
{
    if (gVrrpOperTable != NULL)
    {
        RBTreeDestroy (gVrrpOperTable, NULL, 0);
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbOperTableCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the VRRP Oper 
 *                    table. 
 *                    Indices of this table are
 *
 *                    1. If Index
 *                    2. VRID
 *                    3. Address Type
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparison
 *                    pRBElem2 - Pointer to the node2 for comparison
 *
 * OUTPUT           : None
 *
 * RETURNS          : VRRP_RB_EQUAL    - if all the keys matched for both
 *                                       the nodes
 *                    VRRP_RB_LESSER   - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    VRRP_RB_GREATER  - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
VrrpDbOperTableCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tVrrpOperTable     *pEntryA = NULL;
    tVrrpOperTable     *pEntryB = NULL;

    pEntryA = (tVrrpOperTable *) pRBElem1;
    pEntryB = (tVrrpOperTable *) pRBElem2;

    if (pEntryA->i4IfIndex < pEntryB->i4IfIndex)
    {
        return VRRP_RB_LESSER;
    }
    else if (pEntryA->i4IfIndex > pEntryB->i4IfIndex)
    {
        return VRRP_RB_GREATER;
    }

    if (pEntryA->i4VrId < pEntryB->i4VrId)
    {
        return VRRP_RB_LESSER;
    }
    else if (pEntryA->i4VrId > pEntryB->i4VrId)
    {
        return VRRP_RB_GREATER;
    }

    if (pEntryA->u1AddressType < pEntryB->u1AddressType)
    {
        return VRRP_RB_LESSER;
    }
    else if (pEntryA->u1AddressType > pEntryB->u1AddressType)
    {
        return VRRP_RB_GREATER;
    }

    return VRRP_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbAddOperNode
 *
 * DESCRIPTION      : This function add a node to the VRRP Oper Node
 * 
 * INPUT            : pVrrpOperEntry - Pointer to VRRP Oper Entry
 * 
 * OUTPUT           : None
 *
 * RETURNS          : VRRP_OK or VRRP_NOT_OK
 *
 **************************************************************************/
PUBLIC INT4
VrrpDbAddOperNode (tVrrpOperTable * pVrrpOperEntry)
{
    if (RBTreeAdd (gVrrpOperTable, pVrrpOperEntry) != RB_SUCCESS)
    {
        return VRRP_NOT_OK;
    }

    return VRRP_OK;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeleteOperNode
 *
 * DESCRIPTION      : This function delete a VRRP Oper Entry from the
 *                    VRRP Oper Table.
 *
 * INPUT            : pVrrpOperEntry - Pointer to VRRP Oper Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeleteOperNode (tVrrpOperTable * pVrrpOperEntry)
{
    RBTreeRem (gVrrpOperTable, pVrrpOperEntry);

    MEMSET (pVrrpOperEntry, 0, sizeof (tVrrpOperTable));

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbCreateOperEntry
 *
 * DESCRIPTION      : This function creates VRRP Oper Entry
 * 
 * INPUT            : i4Index       - Oper Index
 *                    i4IfIndex     - Interface Index
 *                    i4VrId        - Virtual Router Identifier
 *                    i4AddrType    - Address Type
 * 
 * OUTPUT           : None
 *
 * RETURNS          : VRRP_NOT_OK or VRRP_OK
 *
 **************************************************************************/
PUBLIC INT4
VrrpDbCreateOperEntry (INT4 i4OperIndex, INT4 i4IfIndex, INT4 i4VrId,
                       INT4 i4AddrType)
{
    tMacAddr            maVirtualMac1 = { 0, 0, 0x5E, 0, 0x01, 0 };
    tMacAddr            maVirtualMac2 = { 0, 0, 0x5E, 0, 0x02, 0 };
    tVrrpOperTable     *pVrrpOperEntry = NULL;
    tMemPoolId          PoolId = 0;

    pVrrpOperEntry = &gpVrrpOperTable[i4OperIndex];

    pVrrpOperEntry->i4IfIndex = i4IfIndex;
    pVrrpOperEntry->i4VrId = i4VrId;
    pVrrpOperEntry->u1AddressType = (UINT1) i4AddrType;
    pVrrpOperEntry->i4OperIndex = i4OperIndex;

    VrrpDefaultConfig (i4OperIndex);

    pVrrpOperEntry->stAuthKey.pu1_OctetList
        = (UINT1 *) MemAllocMemBlk (VRRP_AUTHKEY_MEMPOOL_ID);

    if (pVrrpOperEntry->stAuthKey.pu1_OctetList == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for OPER Auth Key\r\n");

        CLI_SET_ERR (CLI_VRRP_MAX_AUTH);

        return VRRP_NOT_OK;
    }

    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        PoolId = VRRP_IPV4_BUF_MEMPOOL_ID;
    }
    else
    {
        PoolId = VRRP_IPV6_BUF_MEMPOOL_ID;
    }

    pVrrpOperEntry->pu1TxBuf = (UINT1 *) MemAllocMemBlk (PoolId);

    if (pVrrpOperEntry->pu1TxBuf == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for OPER Tx Buffer\r\n");

        CLI_SET_ERR (CLI_VRRP_MAX_TX_BUF);

        MemReleaseMemBlock (VRRP_AUTHKEY_MEMPOOL_ID,
                            (UINT1 *) pVrrpOperEntry->stAuthKey.pu1_OctetList);
        return VRRP_NOT_OK;
    }

    if (VrrpDbAddOperNode (pVrrpOperEntry) == VRRP_NOT_OK)
    {
        CLI_SET_ERR (CLI_VRRP_ADD_OPER);

        MemReleaseMemBlock (VRRP_AUTHKEY_MEMPOOL_ID,
                            (UINT1 *) pVrrpOperEntry->stAuthKey.pu1_OctetList);

        MemReleaseMemBlock (PoolId, (UINT1 *) pVrrpOperEntry->pu1TxBuf);

        return VRRP_NOT_OK;
    }

    if (VrrpIpifJoinMcastGroup (i4OperIndex) == VRRP_NOT_OK)
    {
        CLI_SET_ERR (CLI_VRRP_JOIN_MCAST);

        MemReleaseMemBlock (VRRP_AUTHKEY_MEMPOOL_ID,
                            (UINT1 *) pVrrpOperEntry->stAuthKey.pu1_OctetList);

        MemReleaseMemBlock (PoolId, (UINT1 *) pVrrpOperEntry->pu1TxBuf);

        VrrpDbDeleteOperNode (pVrrpOperEntry);
        return VRRP_NOT_OK;
    }

    pVrrpOperEntry->i4OperRowStatus = NOT_READY;

    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        maVirtualMac1[VRID_LOC_IN_MAC] = (UINT1) (OPERVRID (i4OperIndex));
        VRRP_MEMCPY (&OPERMACADDR (i4OperIndex), maVirtualMac1, MAC_ADDR_LEN);
    }
    else
    {
        maVirtualMac2[VRID_LOC_IN_MAC] = (UINT1) (OPERVRID (i4OperIndex));
        VRRP_MEMCPY (&OPERMACADDR (i4OperIndex), maVirtualMac2, MAC_ADDR_LEN);
    }

    return VRRP_OK;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeleteOperEntry
 *
 * DESCRIPTION      : This function deletes VRRP Oper Entry
 * 
 * INPUT            : i4Index       - Oper Index
 * 
 * OUTPUT           : None
 *
 * RETURNS          : VRRP_NOT_OK or VRRP_OK
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeleteOperEntry (INT4 i4OperIndex)
{
    tVrrpOperTable     *pVrrpOperEntry = NULL;
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tMemPoolId          PoolId = 0;
    INT4                i4IfIndex = OPERIFINDEX (i4OperIndex);
    INT4                i4NextIfIndex = 0;
    INT4                i4NextVrId = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4AddrType = (INT4) OPERADDRTYPE (i4OperIndex);

    pVrrpOperEntry = &gpVrrpOperTable[i4OperIndex];

    pTrackGroupEntry
        = VrrpDbGetTrackGroupEntry (pVrrpOperEntry->u4TrackGroupId);

    if (pTrackGroupEntry != NULL)
    {
        VrrpDbTrackDeleteOperNode (pTrackGroupEntry, i4OperIndex);
    }

    VrrpDbDeleteAllAssocIpEntry (i4OperIndex);

    if (pVrrpOperEntry->stAuthKey.pu1_OctetList != NULL)
    {
        MemReleaseMemBlock (VRRP_AUTHKEY_MEMPOOL_ID,
                            (UINT1 *) pVrrpOperEntry->stAuthKey.pu1_OctetList);
    }

    if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
    {
        PoolId = VRRP_IPV4_BUF_MEMPOOL_ID;
    }
    else
    {
        PoolId = VRRP_IPV6_BUF_MEMPOOL_ID;
    }

    if (pVrrpOperEntry->pu1TxBuf != NULL)
    {
        MemReleaseMemBlock (PoolId, (UINT1 *) pVrrpOperEntry->pu1TxBuf);
    }

    VrrpDbDeleteOperNode (pVrrpOperEntry);

    if ((VrrpDbGetNextOperEntry (i4IfIndex, 0, 0, &i4NextIfIndex, &i4NextVrId,
                                 &i4NextAddrType) == VRRP_OK) &&
        (i4IfIndex == i4NextIfIndex))
    {
        return;
    }

    if (VrrpIpifLeaveMcastGroup (i4OperIndex) == VRRP_NOT_OK)
    {
        return;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbGetOperEntry
 *
 * DESCRIPTION      : This function gets the Oper Index corresponding to
 *                    If Index, VrId and Address Type.
 *
 * INPUT            : i4IfIndex      - CFA Interface Index
 *                    i4VrId         - Virtual Router Identifier
 *                    i4AddressType  - Address Type 
 *
 * OUTPUT           : None
 *
 * RETURNS          : Valid Oper Index if entry found
 *                    (-1) if entry not found
 *
 **************************************************************************/
PUBLIC INT4
VrrpDbGetOperEntry (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddressType)
{
    tVrrpOperTable      VrrpOperEntry;
    tVrrpOperTable     *pVrrpOperEntry = NULL;

    MEMSET (&VrrpOperEntry, 0, sizeof (tVrrpOperTable));

    VrrpOperEntry.i4IfIndex = i4IfIndex;
    VrrpOperEntry.i4VrId = i4VrId;
    VrrpOperEntry.u1AddressType = (UINT1) i4AddressType;

    pVrrpOperEntry
        = (tVrrpOperTable *) RBTreeGet (gVrrpOperTable, &VrrpOperEntry);

    if (pVrrpOperEntry == NULL)
    {
        return VRRP_NOT_OK;
    }

    return (pVrrpOperEntry->i4OperIndex);
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbGetNextOperEntry
 *
 * DESCRIPTION      : This function gets the Next Oper Entry Indices 
 *                    corresponding to If Index, VrId and Address Type.
 *
 * INPUT            : i4IfIndex      - CFA Interface Index
 *                    i4VrId         - Virtual Router Identifier
 *                    i4AddressType  - Address Type 
 *
 * OUTPUT           : *pi4IfIndex     - Next CFA Interface Index
 *                    *pi4VrId        - Next VRID
 *                    *pi4AddressType - Next Address Type
 *
 * RETURNS          : Valid Oper Index if entry found
 *                    (-1) if entry not found
 *
 **************************************************************************/
PUBLIC INT4
VrrpDbGetNextOperEntry (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddressType,
                        INT4 *pi4IfIndex, INT4 *pi4VrId, INT4 *pi4AddressType)
{
    tVrrpOperTable      VrrpOperEntry;
    tVrrpOperTable     *pVrrpOperEntry = NULL;

    MEMSET (&VrrpOperEntry, 0, sizeof (tVrrpOperTable));

    VrrpOperEntry.i4IfIndex = i4IfIndex;
    VrrpOperEntry.i4VrId = i4VrId;
    VrrpOperEntry.u1AddressType = (UINT1) i4AddressType;

    pVrrpOperEntry
        =
        (tVrrpOperTable *) RBTreeGetNext (gVrrpOperTable, &VrrpOperEntry, NULL);

    if (pVrrpOperEntry == NULL)
    {
        return VRRP_NOT_OK;
    }

    *pi4IfIndex = pVrrpOperEntry->i4IfIndex;
    *pi4VrId = pVrrpOperEntry->i4VrId;
    *pi4AddressType = (INT4) pVrrpOperEntry->u1AddressType;

    return VRRP_OK;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbGetNextV2OperEntry
 *
 * DESCRIPTION      : This function gets the Next VRRP Version 2 Oper Entry
 *                    Indices corresponding to If Index and VrId.
 *
 *                    For Version 2, address type is only IPv4 and IPv6 is not
 *                    applicable.
 *
 * INPUT            : i4IfIndex      - CFA Interface Index
 *                    i4VrId         - Virtual Router Identifier
 *                    i4AddressType  - Address Type 
 *
 * OUTPUT           : *pi4IfIndex     - Next CFA Interface Index
 *                    *pi4VrId        - Next VRID
 *
 * RETURNS          : Valid Oper Index if entry found
 *                    (-1) if entry not found
 *
 **************************************************************************/
PUBLIC INT4
VrrpDbGetNextV2OperEntry (INT4 i4IfIndex, INT4 i4VrId, INT4 *pi4IfIndex,
                          INT4 *pi4VrId)
{
    INT4                i4NextIfIndex = i4IfIndex;
    INT4                i4NextVrId = i4VrId;
    INT4                i4AddressType = (INT4) VRRP_IPVX_ADDR_FMLY_IPV4;

    while (VrrpDbGetNextOperEntry (i4NextIfIndex, i4NextVrId, i4AddressType,
                                   pi4IfIndex, pi4VrId, &i4AddressType)
           == VRRP_OK)
    {
        if (i4AddressType == VRRP_IPVX_ADDR_FMLY_IPV4)
        {
            return VRRP_OK;
        }

        i4NextIfIndex = *pi4IfIndex;
        i4NextVrId = *pi4VrId;
    }

    return VRRP_NOT_OK;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbGetFreeOperIndex
 *
 * DESCRIPTION      : This function gets the Next Free Oper Index
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : Next Free Oper Index if entry available
 *                    (-1) if No Free Oper Index available
 *
 **************************************************************************/
PUBLIC INT4
VrrpDbGetFreeOperIndex ()
{
    INT4                i4Index = VRRP_NOT_OK;

    for (i4Index = 0; i4Index < (INT4) gu4VrrpMaxOperEntries; i4Index++)
    {
        /* Return the first index in which Row Status is not set */
        if (OPERROWSTATUS (i4Index) == 0)
        {
            break;
        }
    }

    if (i4Index == (INT4) gu4VrrpMaxOperEntries)
    {
        return (VRRP_NOT_OK);
    }

    return (i4Index);
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbActivateOperEntry
 *
 * DESCRIPTION      : This function activates Oper Entry and initiates SEM.
 *
 * INPUT            : i4OperIndex       - Oper Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbActivateOperEntry (INT4 i4OperIndex)
{
    tIp6Addr            Ip6Addr;
    UINT1               u1AddressType = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    if (OPERROWSTATUS (i4OperIndex) != ACTIVE)
    {
        return;
    }

    if (gi4VrrpNodeVersion == VRRP_VERSION_2)
    {
        if (OPERADMINSTATE (i4OperIndex) == ADMINDOWN)
        {
            return;
        }
    }

    if (OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (Ip6Addr.u1_addr, OPERPRIMARYIPADDR (i4OperIndex).au1Addr,
                IPVX_IPV6_ADDR_LEN);

        u1AddressType = (UINT1) Ip6AddrType (&Ip6Addr);

        if (u1AddressType != ADDR6_LLOCAL)
        {
            return;
        }
    }

    VrrpSem (STARTUP_EVENT, i4OperIndex, ZERO, ZERO);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeactivateOperEntry
 *
 * DESCRIPTION      : This function deactivates Oper Entry and shutdown SEM
 *
 * INPUT            : i4OperIndex       - Oper Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeactivateOperEntry (INT4 i4OperIndex)
{
    if (gi4VrrpNodeVersion == VRRP_VERSION_2)
    {
        if (OPERROWSTATUS (i4OperIndex) == NOT_IN_SERVICE)
        {
            return;
        }
        else if (OPERROWSTATUS (i4OperIndex) == DESTROY)
        {
            if (OPERADMINSTATE (i4OperIndex) == ADMINDOWN)
            {
                return;
            }
            else
            {
                OPERADMINSTATE (i4OperIndex) = ADMINDOWN;
            }
        }
    }

    VrrpSem (SHUTDOWN_EVENT, i4OperIndex, ZERO, ZERO);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbCreateAssocIpEntry
 *
 * DESCRIPTION      : This function creates Associated IP Address Entry
 *                    and adds to Associated IP Address List
 *
 * INPUT            : i4OperIndex       - Oper Index
 *                    AssocIpAddr       - Associated Ip Address
 *
 * OUTPUT           : None
 *
 * RETURNS          : VRRP_OK     - If Entry creation is success
 *                    VRRP_NOT_OK - If Entry creation is failure
 *
 **************************************************************************/
PUBLIC tIpAddrTable *
VrrpDbCreateAssocIpEntry (INT4 i4OperIndex, tIPvXAddr AssocIpAddr)
{
    tIpAddrTable       *pIpAddrEntry = NULL;

    pIpAddrEntry = (tIpAddrTable *) MemAllocMemBlk (VRRP_ASSOC_IP_MEMPOOL_ID);

    if (pIpAddrEntry == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for Associated IP Addresses\r\n");

        CLI_SET_ERR (CLI_VRRP_MAX_ASSOC_IP);

        return NULL;
    }

    MEMSET (pIpAddrEntry, 0, sizeof (tIpAddrTable));

    VRRP_IPVX_COPY (pIpAddrEntry->AssoIpAddr, AssocIpAddr);

    pIpAddrEntry->u1NwStatus = FALSE;

    /* Copy previous IpvX Owner value */
    OPERISPREVOWNER (i4OperIndex) = OPERISOWNER (i4OperIndex);

    VrrpDbOperAddAssocIpNode (i4OperIndex, pIpAddrEntry);

    KW_FALSEPOSITIVE_FIX (pIpAddrEntry);

    return pIpAddrEntry;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeleteAssocIpEntry
 *
 * DESCRIPTION      : This function deletes Associated IP Address Entry
 *
 * INPUT            : i4OperIndex       - Oper Index
 *                    pIpAddrEntry      - Associated IP Address Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeleteAssocIpEntry (INT4 i4OperIndex, tIpAddrTable * pIpAddrEntry)
{
    BOOL1               b1IsPrevOwner = FALSE;
    BOOL1               b1IsCurrentOwner = FALSE;

    VrrpDbOperDeleteAssocIpNode (i4OperIndex, pIpAddrEntry);

    MemReleaseMemBlock (VRRP_ASSOC_IP_MEMPOOL_ID, (UINT1 *) pIpAddrEntry);

    OPERIPADDRCOUNT (i4OperIndex) = (OPERIPADDRCOUNT (i4OperIndex) - 1);

    if (OPERIPADDRCOUNT (i4OperIndex) <= 0)
    {
        VrrpDbDeactivateOperEntry (i4OperIndex);
        OPERADMINSTATE (i4OperIndex) = ADMINDOWN;
        OPERROWSTATUS (i4OperIndex) = NOT_READY;

        return;
    }

    b1IsPrevOwner = OPERISPREVOWNER (i4OperIndex);

    VrrpCompSetIpvXOwner (i4OperIndex);

    b1IsCurrentOwner = OPERISOWNER (i4OperIndex);

    if (b1IsCurrentOwner != b1IsPrevOwner)
    {
        VrrpDbDeactivateOperEntry (i4OperIndex);

        VrrpDbActivateOperEntry (i4OperIndex);
    }

    OPERISPREVOWNER (i4OperIndex) = OPERISOWNER (i4OperIndex);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbActivateAssocIpEntry
 *
 * DESCRIPTION      : This function activates Associated IP Addres and adds
 *                    it to network interface.
 *
 * INPUT            : i4OperIndex       - Oper Index
 *                    pIpAddrEntry      - Associated IP Address Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbActivateAssocIpEntry (INT4 i4OperIndex, tIpAddrTable * pIpAddrEntry)
{
    BOOL1               b1IsPrevOwner = FALSE;
    BOOL1               b1IsCurrentOwner = FALSE;

    b1IsPrevOwner = OPERISPREVOWNER (i4OperIndex);

    VrrpCompSetIpvXOwner (i4OperIndex);
    if (VrrpCompIsOurIp (OPERADDRTYPE (i4OperIndex), OPERIFINDEX (i4OperIndex), 
                             pIpAddrEntry->AssoIpAddr) == VRRP_NOT_OK) 
    { 
            pIpAddrEntry->b1IsIpvXOwner = FALSE; 
    } 
    else 
    { 
            pIpAddrEntry->b1IsIpvXOwner = TRUE; 
    } 

    b1IsCurrentOwner = OPERISOWNER (i4OperIndex);

    if (b1IsCurrentOwner != b1IsPrevOwner)
    {
        VrrpDbDeactivateOperEntry (i4OperIndex);

        VrrpDbActivateOperEntry (i4OperIndex);
    }

    OPERISPREVOWNER (i4OperIndex) = OPERISOWNER (i4OperIndex);

    if (OPERSTATE (i4OperIndex) != MASTER_STATE)
    {
        return;
    }

    if (VrrpCreateSecIpOrJoinSoliMCast (i4OperIndex, pIpAddrEntry->AssoIpAddr,
                                        FALSE) == VRRP_NOT_OK)
    {
        return;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeactivateAssocIpEntry
 *
 * DESCRIPTION      : This function deactivates Associated IP Addres and 
 *                    removes it from network interface.
 *
 * INPUT            : i4OperIndex       - Oper Index
 *                    pIpAddrEntry      - Associated IP Address Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeactivateAssocIpEntry (INT4 i4OperIndex, tIpAddrTable * pIpAddrEntry)
{
    if (OPERSTATE (i4OperIndex) != MASTER_STATE)
    {
        return;
    }

    if (VrrpDeleteSecIpOrLeaveSoliMCast (i4OperIndex, pIpAddrEntry->AssoIpAddr,
                                         FALSE) == VRRP_NOT_OK)
    {
        return;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbOperAddAssocIpNode
 *
 * DESCRIPTION      : This function adds Associated IP Address to Associated 
 *                    IP Address List in Oper Entry in Sorted manner
 *
 * INPUT            : i4OperIndex       - Oper Index
 *                    pInIpAddrEntry    - Pointer to Associated IP Address
 *                                        Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbOperAddAssocIpNode (INT4 i4OperIndex, tIpAddrTable * pInIpAddrEntry)
{
    tVrrpOperTable     *pVrrpOperEntry = NULL;
    tIpAddrTable       *pIpAddrEntry = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tTMO_DLL_NODE      *pPrevNode = NULL;

    pVrrpOperEntry = (tVrrpOperTable *) & gpVrrpOperTable[i4OperIndex];

    TMO_DLL_Scan (&(pVrrpOperEntry->AssocIpAddrHead), pLstNode, tTMO_DLL_NODE *)
    {
        pIpAddrEntry = VRRP_GET_BASE_PTR (tIpAddrTable, AssocIpAddrNode,
                                          pLstNode);

        if (VRRP_IPVX_COMPARE (pInIpAddrEntry->AssoIpAddr,
                               pIpAddrEntry->AssoIpAddr) < 0)
        {
            break;
        }

        pPrevNode = pLstNode;
    }

    TMO_DLL_Init_Node (&(pInIpAddrEntry->AssocIpAddrNode));
    TMO_DLL_Insert (&(pVrrpOperEntry->AssocIpAddrHead), pPrevNode,
                    &(pInIpAddrEntry->AssocIpAddrNode));

    OPERIPADDRCOUNT (i4OperIndex) = (OPERIPADDRCOUNT (i4OperIndex) + 1);

    pInIpAddrEntry->u1SecIndex = (UINT1) OPERIPADDRCOUNT (i4OperIndex);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbOperDeleteAssocIpNode
 *
 * DESCRIPTION      : This function deletes Associated IP Address from
 *                    Associated IP Address List in Oper Entry
 *
 * INPUT            : i4OperIndex       - Oper Index
 *                    pInIpAddrEntry    - Pointer to Associated IP Address
 *                                        Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbOperDeleteAssocIpNode (INT4 i4OperIndex, tIpAddrTable * pInIpAddrEntry)
{
    tVrrpOperTable     *pVrrpOperEntry = NULL;

    pVrrpOperEntry = (tVrrpOperTable *) & gpVrrpOperTable[i4OperIndex];

    TMO_DLL_Delete (&(pVrrpOperEntry->AssocIpAddrHead),
                    &(pInIpAddrEntry->AssocIpAddrNode));

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbOperGetAssocIpEntry
 *
 * DESCRIPTION      : This function gets Associated IP Address Entry for the
 *                    passed IP Address
 *
 * INPUT            : i4OperIndex       - Oper Index
 *                    AssocIpAddr       - Associated IP Address
 *
 * OUTPUT           : None
 *
 * RETURNS          : pIpAddrEntry      - Pointer to Associated IP Address
 *                                        Entry
 *
 **************************************************************************/
PUBLIC tIpAddrTable *
VrrpDbOperGetAssocIpEntry (INT4 i4OperIndex, tIPvXAddr AssocIpAddr)
{
    tVrrpOperTable     *pVrrpOperEntry = NULL;
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;

    pVrrpOperEntry = (tVrrpOperTable *) & gpVrrpOperTable[i4OperIndex];

    UTL_DLL_OFFSET_SCAN (&(pVrrpOperEntry->AssocIpAddrHead), pIpAddrEntry,
                         pTempIpAddrEntry, tIpAddrTable *)
    {
        if (VRRP_IPVX_COMPARE (pIpAddrEntry->AssoIpAddr, AssocIpAddr) == 0)
        {
            break;
        }
    }

    return (pIpAddrEntry);
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbOperGetNextAssocIpEntry
 *
 * DESCRIPTION      : This function gets Associated IP Address Entry for the
 *                    passed IP Address
 *
 * INPUT            : i4OperIndex       - Oper Index
 *                    AssocIpAddr       - Associated IP Address
 *
 * OUTPUT           : None
 *
 * RETURNS          : pIpAddrEntry      - Pointer to Next Associated IP Address
 *                                        Entry
 *
 **************************************************************************/
PUBLIC tIpAddrTable *
VrrpDbOperGetNextAssocIpEntry (INT4 i4OperIndex, tIPvXAddr AssocIpAddr)
{
    tVrrpOperTable     *pVrrpOperEntry = NULL;
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;

    pVrrpOperEntry = (tVrrpOperTable *) & gpVrrpOperTable[i4OperIndex];

    UTL_DLL_OFFSET_SCAN (&(pVrrpOperEntry->AssocIpAddrHead), pIpAddrEntry,
                         pTempIpAddrEntry, tIpAddrTable *)
    {
        if (VRRP_IPVX_COMPARE (pIpAddrEntry->AssoIpAddr, AssocIpAddr) > 0)
        {
            break;
        }
    }

    return (pIpAddrEntry);
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeleteAllAssocIpEntry
 *
 * DESCRIPTION      : This function deletes all associated IP Entries
 *                    in this Oper Index
 *
 * INPUT            : i4OperIndex       - Oper Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeleteAllAssocIpEntry (INT4 i4OperIndex)
{
    tVrrpOperTable     *pVrrpOperEntry = NULL;
    tIpAddrTable       *pIpAddrEntry = NULL;
    tIpAddrTable       *pTempIpAddrEntry = NULL;

    pVrrpOperEntry = (tVrrpOperTable *) & gpVrrpOperTable[i4OperIndex];

    UTL_DLL_OFFSET_SCAN (&(pVrrpOperEntry->AssocIpAddrHead), pIpAddrEntry,
                         pTempIpAddrEntry, tIpAddrTable *)
    {
        VrrpDbDeactivateAssocIpEntry (i4OperIndex, pIpAddrEntry);

        VrrpDbDeleteAssocIpEntry (i4OperIndex, pIpAddrEntry);
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbCreateTrackGroupEntry
 *
 * DESCRIPTION      : This function creates Track Group Entry
 *                    and adds to Track Group List
 *
 * INPUT            : u4GroupIndex      - Track Group Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : VRRP_OK     - If Entry creation is success
 *                    VRRP_NOT_OK - If Entry creation is failure
 *
 **************************************************************************/
PUBLIC INT4
VrrpDbCreateTrackGroupEntry (UINT4 u4GroupIndex)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;

    pTrackGroupEntry
        = (tVrrpTrackGroupTable *) MemAllocMemBlk (VRRP_TRACK_MEMPOOL_ID);

    if (pTrackGroupEntry == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for Track Group Entry\r\n");

        CLI_SET_ERR (CLI_VRRP_MAX_TRACK_GROUP);

        return VRRP_NOT_OK;
    }

    MEMSET (pTrackGroupEntry, 0, sizeof (tVrrpTrackGroupTable));

    pTrackGroupEntry->u4GroupIndex = u4GroupIndex;
    pTrackGroupEntry->u1RowStatus = NOT_READY;

    UTL_DLL_INIT (&(pTrackGroupEntry->VrrpOperHead),
                  FSAP_OFFSETOF (tVrrpOperTable, TrackGroupOperNode));

    UTL_DLL_INIT (&(pTrackGroupEntry->TrackGroupIfHead),
                  FSAP_OFFSETOF (tVrrpTrackGroupIfTable, TrackGroupIfNode));

    VrrpDbAddTrackGroupNode (pTrackGroupEntry);

    KW_FALSEPOSITIVE_FIX (pTrackGroupEntry);

    return VRRP_OK;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeleteTrackGroupEntry
 *
 * DESCRIPTION      : This function deletes Track Group Entry
 *
 * INPUT            : pTrackGroupEntry   - Pointer to Track Group Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeleteTrackGroupEntry (tVrrpTrackGroupTable * pTrackGroupEntry)
{
    VrrpDbDeleteTrackGroupNode (pTrackGroupEntry);

    MemReleaseMemBlock (VRRP_TRACK_MEMPOOL_ID, (UINT1 *) pTrackGroupEntry);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbAddTrackGroupNode
 *
 * DESCRIPTION      : This function adds an entry to Track Group List in
 *                    Sorted Manner
 *
 * INPUT            : pInTrackGroupEntry  - Pointer to Track Group Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbAddTrackGroupNode (tVrrpTrackGroupTable * pInTrackGroupEntry)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tTMO_DLL_NODE      *pPrevNode = NULL;

    TMO_DLL_Scan (&(gTrackGroupHead), pLstNode, tTMO_DLL_NODE *)
    {
        pTrackGroupEntry = VRRP_GET_BASE_PTR (tVrrpTrackGroupTable,
                                              TrackGroupNode, pLstNode);

        if (pInTrackGroupEntry->u4GroupIndex < pTrackGroupEntry->u4GroupIndex)
        {
            break;
        }

        pPrevNode = pLstNode;
    }

    TMO_DLL_Init_Node (&(pInTrackGroupEntry->TrackGroupNode));
    TMO_DLL_Insert (&(gTrackGroupHead), pPrevNode,
                    &(pInTrackGroupEntry->TrackGroupNode));

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeleteTrackGroupNode
 *
 * DESCRIPTION      : This function deletes track group node
 *
 * INPUT            : pInTrackGroupEntry    - Pointer to Track Group Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeleteTrackGroupNode (tVrrpTrackGroupTable * pInTrackGroupEntry)
{
    TMO_DLL_Delete (&(gTrackGroupHead), &(pInTrackGroupEntry->TrackGroupNode));

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbGetTrackGroupEntry
 *
 * DESCRIPTION      : This function gets track group entry
 *
 * INPUT            : u4GroupIndex     - Track Group Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : pTrackGroupEntry - Pointer to Track Group Entry
 *
 **************************************************************************/
PUBLIC tVrrpTrackGroupTable *
VrrpDbGetTrackGroupEntry (UINT4 u4GroupIndex)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tVrrpTrackGroupTable *pTempTrackGroupEntry = NULL;

    UTL_DLL_OFFSET_SCAN (&(gTrackGroupHead), pTrackGroupEntry,
                         pTempTrackGroupEntry, tVrrpTrackGroupTable *)
    {
        if (pTrackGroupEntry->u4GroupIndex == u4GroupIndex)
        {
            break;
        }
    }

    return (pTrackGroupEntry);
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbGetNextTrackGroupEntry
 *
 * DESCRIPTION      : This function gets next track group entry
 *
 * INPUT            : u4GroupIndex     - Track Group Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : pTrackGroupEntry - Pointer to Next Track Group Entry
 *
 **************************************************************************/
PUBLIC tVrrpTrackGroupTable *
VrrpDbGetNextTrackGroupEntry (UINT4 u4GroupIndex)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tVrrpTrackGroupTable *pTempTrackGroupEntry = NULL;

    UTL_DLL_OFFSET_SCAN (&(gTrackGroupHead), pTrackGroupEntry,
                         pTempTrackGroupEntry, tVrrpTrackGroupTable *)
    {
        if (pTrackGroupEntry->u4GroupIndex > u4GroupIndex)
        {
            break;
        }
    }

    return (pTrackGroupEntry);
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbCreateTrackGroupIfEntry
 *
 * DESCRIPTION      : This function creates Track Group If Entry
 *                    and adds to Track Group List
 *
 * INPUT            : pTrackGroupEntry   - Pointer to Track Group Entry
 *                    i4IfIndex          - Interface Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : VRRP_OK     - If Entry creation is success
 *                    VRRP_NOT_OK - If Entry creation is failure
 *
 **************************************************************************/
PUBLIC INT4
VrrpDbCreateTrackGroupIfEntry (tVrrpTrackGroupTable * pTrackGroupEntry,
                               INT4 i4IfIndex)
{
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;

    pTrackGroupIfEntry
        = (tVrrpTrackGroupIfTable *) MemAllocMemBlk (VRRP_TRACK_IF_MEMPOOL_ID);

    if (pTrackGroupIfEntry == NULL)
    {
        VRRP_DBG (VRRP_TRC_MEMORY,
                  "Memory Allocation failed for Track Group If Entry\r\n");

        CLI_SET_ERR (CLI_VRRP_MAX_TRACK_IF);

        return VRRP_NOT_OK;
    }

    MEMSET (pTrackGroupIfEntry, 0, sizeof (tVrrpTrackGroupIfTable));

    pTrackGroupIfEntry->i4IfIndex = i4IfIndex;
    pTrackGroupIfEntry->u1RowStatus = (UINT1) ACTIVE;
    pTrackGroupIfEntry->u1IfStatus = (UINT1) VRRP_TRACK_IF_UP;

    VrrpDbAddTrackIfToGroup (pTrackGroupEntry, pTrackGroupIfEntry);

    KW_FALSEPOSITIVE_FIX (pTrackGroupIfEntry);

    return VRRP_OK;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeleteTrackGroupIfEntry
 *
 * DESCRIPTION      : This function deletes Track Group Entry
 *
 * INPUT            : pTrackGroupEntry    - Pointer to Track Group Entry
 *                    pTrackGroupIfEntry  - Pointer to Track Group If Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeleteTrackGroupIfEntry (tVrrpTrackGroupTable * pTrackGroupEntry,
                               tVrrpTrackGroupIfTable * pTrackGroupIfEntry)
{
    VrrpDbRemoveTrackIfFromGroup (pTrackGroupEntry, pTrackGroupIfEntry);

    MemReleaseMemBlock (VRRP_TRACK_IF_MEMPOOL_ID, (UINT1 *) pTrackGroupIfEntry);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbAddTrackToGroup
 *
 * DESCRIPTION      : This function adds an entry to Track Group List in
 *                    Sorted Manner
 *
 * INPUT            : pInTrackGroupEntry   - Pointer to Track Group Entry
 *                    pInTrackGroupIfEntry - Pointer to Track Group If Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbAddTrackIfToGroup (tVrrpTrackGroupTable * pInTrackGroupEntry,
                         tVrrpTrackGroupIfTable * pInTrackGroupIfEntry)
{
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tTMO_DLL_NODE      *pPrevNode = NULL;

    TMO_DLL_Scan (&(pInTrackGroupEntry->TrackGroupIfHead), pLstNode,
                  tTMO_DLL_NODE *)
    {
        pTrackGroupIfEntry = VRRP_GET_BASE_PTR (tVrrpTrackGroupIfTable,
                                                TrackGroupIfNode, pLstNode);

        if (pInTrackGroupIfEntry->i4IfIndex < pTrackGroupIfEntry->i4IfIndex)
        {
            break;
        }

        pPrevNode = pLstNode;
    }

    TMO_DLL_Init_Node (&(pInTrackGroupIfEntry->TrackGroupIfNode));

    TMO_DLL_Insert (&(pInTrackGroupEntry->TrackGroupIfHead), pPrevNode,
                    &(pInTrackGroupIfEntry->TrackGroupIfNode));

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbRemoveTrackIfFromGroup
 *
 * DESCRIPTION      : This function deletes track group If node
 *
 * INPUT            : pInTrackGroupEntry    - Pointer to Track Group Entry
 *                    pInTrackGroupIfEntry  - Pointer to Track Group If Entry
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbRemoveTrackIfFromGroup (tVrrpTrackGroupTable * pInTrackGroupEntry,
                              tVrrpTrackGroupIfTable * pInTrackGroupIfEntry)
{
    TMO_DLL_Delete (&(pInTrackGroupEntry->TrackGroupIfHead),
                    &(pInTrackGroupIfEntry->TrackGroupIfNode));

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbGetTrackGroupIfEntry
 *
 * DESCRIPTION      : This function gets track group If entry
 *
 * INPUT            : pTrackGroupEntry - Pointer to Track Group Entry
 *                    i4IfIndex        - Interface Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : pTrackGroupIfEntry - Pointer to Track Group If Entry
 *
 **************************************************************************/
PUBLIC tVrrpTrackGroupIfTable *
VrrpDbGetTrackGroupIfEntry (tVrrpTrackGroupTable * pTrackGroupEntry,
                            INT4 i4IfIndex)
{
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;
    tVrrpTrackGroupIfTable *pTempTrackGroupIfEntry = NULL;

    UTL_DLL_OFFSET_SCAN (&(pTrackGroupEntry->TrackGroupIfHead),
                         pTrackGroupIfEntry, pTempTrackGroupIfEntry,
                         tVrrpTrackGroupIfTable *)
    {
        if (pTrackGroupIfEntry->i4IfIndex == i4IfIndex)
        {
            break;
        }
    }

    return (pTrackGroupIfEntry);
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbGetNextTrackGroupIfEntry
 *
 * DESCRIPTION      : This function gets next track group If entry
 *
 * INPUT            : pTrackGroupEntry - Pointer to Track Group Entry
 *                    i4IfIndex        - Interface Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : pTrackGroupIfEntry - Pointer to Track Group If Entry
 *
 **************************************************************************/
PUBLIC tVrrpTrackGroupIfTable *
VrrpDbGetNextTrackGroupIfEntry (tVrrpTrackGroupTable * pTrackGroupEntry,
                                INT4 i4IfIndex)
{
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;
    tVrrpTrackGroupIfTable *pTempTrackGroupIfEntry = NULL;

    UTL_DLL_OFFSET_SCAN (&(pTrackGroupEntry->TrackGroupIfHead),
                         pTrackGroupIfEntry, pTempTrackGroupIfEntry,
                         tVrrpTrackGroupIfTable *)
    {
        if (pTrackGroupIfEntry->i4IfIndex > i4IfIndex)
        {
            break;
        }
    }

    return (pTrackGroupIfEntry);
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbTrackAddOperNode
 *
 * DESCRIPTION      : This function adds an Oper Node entry to Track Group List
 *
 * INPUT            : pInTrackGroupEntry    - Pointer to Track Group Entry
 *                    i4OperIndex           - Oper Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC INT4
VrrpDbTrackAddOperNode (tVrrpTrackGroupTable * pInTrackGroupEntry,
                        INT4 i4OperIndex)
{
    tVrrpOperTable     *pVrrpOperEntry = NULL;

    pVrrpOperEntry = &(gpVrrpOperTable[i4OperIndex]);

    TMO_DLL_Init_Node (&(pVrrpOperEntry->TrackGroupOperNode));
    TMO_DLL_Add (&(pInTrackGroupEntry->VrrpOperHead),
                 &(pVrrpOperEntry->TrackGroupOperNode));

    return VRRP_OK;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbTrackDeleteOperNode
 *
 * DESCRIPTION      : This function deletes the Oper Node from Track Group
 *                    List
 *
 * INPUT            : pInTrackGroupEntry    - Pointer to Track Group Entry
 *                    i4OperIndex           - Oper Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbTrackDeleteOperNode (tVrrpTrackGroupTable * pInTrackGroupEntry,
                           INT4 i4OperIndex)
{
    tVrrpOperTable     *pVrrpOperEntry = NULL;

    pVrrpOperEntry = &(gpVrrpOperTable[i4OperIndex]);

    TMO_DLL_Delete (&(pInTrackGroupEntry->VrrpOperHead),
                    &(pVrrpOperEntry->TrackGroupOperNode));

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbActivateAcceptMode
 *
 * DESCRIPTION      : This function activates Accept Mode configuration. 
 *                    If the VRRP Oper Entry is in Master state and not the
 *                    owner, secondary IP address is added.
 *
 * INPUT            : i4OperIndex           - Oper Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbActivateAcceptMode (INT4 i4OperIndex)
{
    UINT1 u1IsMclagEnabled = OSIX_FALSE;

    VrrpIsMCLAGEnabled ((UINT4)OPERIFINDEX (i4OperIndex), &u1IsMclagEnabled);

    /* If Mclag is enabled, create secondary interface in both 
       Master and slave node
     */
    if (u1IsMclagEnabled == OSIX_FALSE)
    {
        if (OPERSTATE (i4OperIndex) != MASTER_STATE)
        {
            OPERACCEPTMODE (i4OperIndex) = (UINT1) VRRP_ENABLE;

            return;
        }
    }

    /* For IPv6, if Accept mode was disabled earlier, associated IPv6
     * addresses were joined in solicited muliticast group. Now,
     * to create secondary IP Address first leave the multicast group
     * and while creating secondary IP address it will be automatically
     * added to multicast group. */

    if (VrrpLeaveSolicitedMulticastGroup (i4OperIndex, TRUE) == VRRP_NOT_OK)
    {
        /* Failure Not handled */
    }

    /* Before creating secondary address, accept mode should be
     * enabled. */
    OPERACCEPTMODE (i4OperIndex) = (UINT1) VRRP_ENABLE;

    if (VrrpCreateSecIpOrJoinSoliMCast (i4OperIndex, gVrrpZeroIpAddr, TRUE)
        == VRRP_NOT_OK)
    {
        return;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbDeactivateAcceptMode
 *
 * DESCRIPTION      : This function deactivates Accept Mode configuration. 
 *                    If the VRRP Oper Entry is in Master state and not the
 *                    owner, secondary IP address is deleted.
 *
 * INPUT            : i4OperIndex           - Oper Index
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbDeactivateAcceptMode (INT4 i4OperIndex)
{
    UINT1 u1IsMclagEnabled = OSIX_FALSE;

    VrrpIsMCLAGEnabled ((UINT4)OPERIFINDEX (i4OperIndex), &u1IsMclagEnabled);

    /* If Mclag is enabled, delete secondary interface in both
       Master and slave node
     */
    if (u1IsMclagEnabled == OSIX_FALSE)
    {
        if (OPERSTATE (i4OperIndex) != MASTER_STATE)
        {
            OPERACCEPTMODE (i4OperIndex) = (UINT1) VRRP_DISABLE;

            return;
        }
    }

    if (VrrpDeleteSecIpOrLeaveSoliMCast (i4OperIndex, gVrrpZeroIpAddr, TRUE)
        == VRRP_NOT_OK)
    {
        return;
    }

    /* Accept mode can be disabled after secondary address is deleted. */
    OPERACCEPTMODE (i4OperIndex) = (UINT1) VRRP_DISABLE;

    /* For IPv6 addres case, join the IPv6 address to solicited multicast
     * group because when accept mode was disabled earlier this address was
     * part of that and left when accept mode was enabled. */

    if (VrrpJoinSolicitedMulticastGroup (i4OperIndex, TRUE) == VRRP_NOT_OK)
    {
        /* Failure not handled */
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : VrrpDbSetAdvtInterval
 *
 * DESCRIPTION      : This function set advertisement interval
 *
 * INPUT            : i4OperIndex     - Oper Index
 *                    i4AdvtInterval  - Advertisement Interval
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
VrrpDbSetAdvtInterval (INT4 i4OperIndex, INT4 i4AdvtInterval)
{
    tVrrpTimerRef      *pTmrRef = NULL;

    OPERADVERINTL (i4OperIndex) = i4AdvtInterval;

    pTmrRef = &OPERTMR (i4OperIndex);

    if (OPERSTATE (i4OperIndex) == MASTER_STATE)
    {
        TmrIfStopTimer (pTmrRef);

        VrrpSem (ADVERTIMER_EVENT, i4OperIndex, OPERPRIORITY (i4OperIndex),
                 ZERO);
    }
    else if (OPERSTATE (i4OperIndex) == BACKUP_STATE)
    {
        VrrpSem (SHUTDOWN_EVENT, i4OperIndex, 0, 0);

        VrrpSem (STARTUP_EVENT, i4OperIndex, 0, 0);
    }

    return;
}
