/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: vrrpset.c,v 1.5 2017/12/16 11:57:12 siva Exp $
 *
 * Description: This file contains Get All routines for all tables
 *              of VRRP.
 *           
 *******************************************************************/
#include "vrrpinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpSetAllOperTable                              */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Oper Table and sets the value of requested       */
/*                          Object.                                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          u4ObjectId   - Object Identifier                 */
/*                          pData        - Pointer to Data to be set         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpSetAllOperTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                     INT4 i4AddrType, UINT4 u4ObjectId,
                     tSNMP_MULTI_DATA_TYPE * pData)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    BOOL1               b1VersionMatched = FALSE;
    INT4                i4Index = VRRP_NOT_OK;

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return SNMP_FAILURE;
    }

    i4Index = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

    if ((u4ObjectId != VRRP_MIB_OPER_ROW_STATUS) && (i4Index == VRRP_NOT_OK))
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_OPER_PRIM_IP:
            VRRP_IPVX_ADDR_INIT_IPVX (OPERPRIMARYIPADDR (i4Index),
                                      (UINT1) i4AddrType,
                                      pData->pOctetStrValue->pu1_OctetList);
            break;

        case VRRP_MIB_OPER_ADMIN_PRIO:
        case VRRP_MIB_OPER_PRIORITY:
            OPERADMINPRIORITY (i4Index) = pData->i4_SLongValue;
            break;

        case VRRP_MIB_OPER_ADVT_INT_MSEC:
        case VRRP_MIB_OPER_ADVT_INT:
            if (OPERADVERINTL (i4Index) == pData->i4_SLongValue)
            {
                return SNMP_SUCCESS;
            }

            VrrpDbSetAdvtInterval (i4Index, pData->i4_SLongValue);
            break;

        case VRRP_MIB_OPER_PREMP_MODE:
            OPERPREEMPTMODE (i4Index) = pData->i4_SLongValue;
            break;

        case VRRP_MIB_OPER_ACCEPT_MODE:
            if (OPERACCEPTMODE (i4Index) == (UINT1) pData->i4_SLongValue)
            {
                return SNMP_SUCCESS;
            }

            if (pData->i4_SLongValue == (INT4) VRRP_ENABLE)
            {
                VrrpDbActivateAcceptMode (i4Index);
            }
            else
            {
                VrrpDbDeactivateAcceptMode (i4Index);
            }
            break;

        case VRRP_MIB_OPER_ADMIN_STATE:
            if (OPERADMINSTATE (i4Index) == pData->i4_SLongValue)
            {
                return SNMP_SUCCESS;
            }

            OPERADMINSTATE (i4Index) = pData->i4_SLongValue;

            if (OPERADMINSTATE (i4Index) == ADMINUP)
            {
                VrrpDbActivateOperEntry (i4Index);
            }
            else
            {
                VrrpDbDeactivateOperEntry (i4Index);
            }
            break;

        case VRRP_MIB_OPER_AUTH_TYPE:
            OPERAUTHTYPE (i4Index) = pData->i4_SLongValue;
            break;

        case VRRP_MIB_OPER_AUTH_KEY:
            VRRP_MEMSET (OPERAUTHKEY (i4Index).pu1_OctetList, 0,
                         AUTHKEYSIZE + 1);
            (OPERAUTHKEY (i4Index)).i4_Length =
                pData->pOctetStrValue->i4_Length;
            VRRP_MEMCPY ((OPERAUTHKEY (i4Index)).pu1_OctetList,
                         pData->pOctetStrValue->pu1_OctetList,
                         pData->pOctetStrValue->i4_Length);
            (OPERAUTHKEY (i4Index)).
                pu1_OctetList[OPERAUTHKEY (i4Index).i4_Length] = '\0';
            break;

        case VRRP_MIB_OPER_PROTOCOL:
            OPERPROTOCOL (i4Index) = pData->i4_SLongValue;
            break;

        case VRRP_MIB_OPER_TRACK_GRP_ID:
            if (OPERTRACKGRPID (i4Index) == pData->u4_ULongValue)
            {
                return SNMP_SUCCESS;
            }

            pTrackGroupEntry
                = VrrpDbGetTrackGroupEntry (OPERTRACKGRPID (i4Index));

            if (pTrackGroupEntry != NULL)
            {
                VrrpDbTrackDeleteOperNode (pTrackGroupEntry, i4Index);
                VrrpCompCheckTrackAndActOnOper (pTrackGroupEntry, i4Index,
                                                TRUE);
            }

            OPERTRACKGRPID (i4Index) = pData->u4_ULongValue;

            pTrackGroupEntry
                = VrrpDbGetTrackGroupEntry (OPERTRACKGRPID (i4Index));

            if (pTrackGroupEntry != NULL)
            {
                VrrpDbTrackAddOperNode (pTrackGroupEntry, i4Index);
                VrrpCompCheckTrackAndActOnOper (pTrackGroupEntry, i4Index,
                                                FALSE);
            }

            break;

        case VRRP_MIB_OPER_DEC_PRIORITY:
            OPERDECPRIO (i4Index) = (UINT1) pData->u4_ULongValue;
            break;

        case VRRP_MIB_OPER_ROW_STATUS:

            if ((pData->i4_SLongValue == CREATE_AND_WAIT) ||
                (pData->i4_SLongValue == CREATE_AND_GO))
            {
                i4Index = VrrpDbGetFreeOperIndex ();

                if (i4Index == VRRP_NOT_OK)
                {
                    CLI_SET_ERR (CLI_VRRP_MAX_VR);

                    return SNMP_FAILURE;
                }

                if (VrrpDbCreateOperEntry (i4Index, i4IfIndex, i4VrId,
                                           i4AddrType) == VRRP_NOT_OK)
                {
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if (i4Index == VRRP_NOT_OK)
                {
                    CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

                    return SNMP_FAILURE;
                }

                if (pData->i4_SLongValue == OPERROWSTATUS (i4Index))
                {
                    return SNMP_SUCCESS;
                }

                OPERROWSTATUS (i4Index) = pData->i4_SLongValue;

                if (pData->i4_SLongValue == ACTIVE)
                {
                    VrrpDbActivateOperEntry (i4Index);
                }
                else if (pData->i4_SLongValue == NOT_IN_SERVICE)
                {
                    VrrpDbDeactivateOperEntry (i4Index);
                }
                else
                {
                    /* This is Destroy case */

                    VrrpDbDeactivateOperEntry (i4Index);

                    VrrpDbDeleteOperEntry (i4Index);
                }
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpSetAllAssocIpTable                           */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Associated IP Address Table and sets  value of   */
/*                          requested Object.                                */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          i4IfIndex    - Interface Index                   */
/*                          i4VrId       - Virtual Router Identifier         */
/*                          i4AddrType   - Address Type                      */
/*                          pAssocIpAddr - Associated IP Address             */
/*                          u4ObjectId   - Object Identifier                 */
/*                          pData        - Pointer to Data to be set         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpSetAllAssocIpTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                        INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE * pAssocIpAddr,
                        UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE * pData)
{
    tIpAddrTable       *pIpAddrEntry = NULL;
    BOOL1               b1VersionMatched = FALSE;
    INT4                i4Index = VRRP_NOT_OK;
    tIPvXAddr           IpAddr;

    VRRP_IPVX_ADDR_CLEAR (IpAddr);
    VRRP_IPVX_ADDR_INIT_IPVX (IpAddr, (UINT1) i4AddrType,
                              pAssocIpAddr->pu1_OctetList);

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return SNMP_FAILURE;
    }

    i4Index = VrrpDbGetOperEntry (i4IfIndex, i4VrId, i4AddrType);

    if (i4Index == VRRP_NOT_OK)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_ENTRY);

        return SNMP_FAILURE;
    }

    pIpAddrEntry = VrrpDbOperGetAssocIpEntry (i4Index, IpAddr);

    switch (u4ObjectId)
    {
        case VRRP_MIB_ASSO_ROW_STATUS:
            if ((pData->i4_SLongValue == CREATE_AND_WAIT) ||
                (pData->i4_SLongValue == CREATE_AND_GO))
            {
                pIpAddrEntry = VrrpDbCreateAssocIpEntry (i4Index, IpAddr);

                if (pIpAddrEntry == NULL)
                {
                    return SNMP_FAILURE;
                }

                if (pData->i4_SLongValue == CREATE_AND_GO)
                {
                    pIpAddrEntry->u1AssoIpAddrRowStatus = (UINT1) ACTIVE;

                    VrrpDbActivateAssocIpEntry (i4Index, pIpAddrEntry);
                }
            }
            else
            {
                if (pIpAddrEntry == NULL)
                {
                    CLI_SET_ERR (CLI_VRRP_NO_ASSOC_ENTRY);

                    return SNMP_FAILURE;
                }

                if (pIpAddrEntry->u1AssoIpAddrRowStatus ==
                    (UINT1) pData->i4_SLongValue)
                {
                    return SNMP_SUCCESS;
                }

                if (pData->i4_SLongValue == ACTIVE)
                {
                    pIpAddrEntry->u1AssoIpAddrRowStatus = (UINT1) ACTIVE;

                    VrrpDbActivateAssocIpEntry (i4Index, pIpAddrEntry);
                }
                else
                {
                    /* This is Destroy case */
                    VrrpDbDeactivateAssocIpEntry (i4Index, pIpAddrEntry);

                    VrrpDbDeleteAssocIpEntry (i4Index, pIpAddrEntry);
                }
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpSetAllTrackGroupTable                        */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Track Group Table and sets value of requested    */
/*                          Object.                                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          u4GroupIndex - Group Index                       */
/*                          u4ObjectId   - Object Identifier                 */
/*                          pData        - Pointer to Data to be set         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpSetAllTrackGroupTable (INT4 i4Version, UINT4 u4GroupIndex,
                           UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE * pData)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    BOOL1               b1VersionMatched = FALSE;
    INT4                i4RetVal = VRRP_NOT_OK;

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return SNMP_FAILURE;
    }

    pTrackGroupEntry = VrrpDbGetTrackGroupEntry (u4GroupIndex);

    if ((u4ObjectId != VRRP_MIB_TRACK_ROW_STATUS) && (pTrackGroupEntry == NULL))
    {
        CLI_SET_ERR (CLI_VRRP_NO_TRACK_GROUP);

        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_TRACK_GROUP_LINKS:
            if (pTrackGroupEntry->u1TrackedLinks ==
                (UINT1) pData->u4_ULongValue)
            {
                break;
            }

            pTrackGroupEntry->u1TrackedLinks = (UINT1) pData->u4_ULongValue;

            VrrpCompCalcTrackGroupStatusAndAct (pTrackGroupEntry, NULL);

            break;

        case VRRP_MIB_TRACK_ROW_STATUS:
            if ((pData->i4_SLongValue == CREATE_AND_WAIT) ||
                (pData->i4_SLongValue == CREATE_AND_GO))
            {
                i4RetVal = VrrpDbCreateTrackGroupEntry (u4GroupIndex);

                if (i4RetVal == VRRP_NOT_OK)
                {
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if (pTrackGroupEntry == NULL)
                {
                    CLI_SET_ERR (CLI_VRRP_NO_TRACK_GROUP);

                    return SNMP_FAILURE;
                }

                if (pData->i4_SLongValue ==
                    (INT4) pTrackGroupEntry->u1RowStatus)
                {
                    break;
                }

                if (pData->i4_SLongValue == DESTROY)
                {
                    VrrpDbDeleteTrackGroupEntry (pTrackGroupEntry);
                }
                else
                {
                    /* This case should be ACTIVE case */
                    pTrackGroupEntry->u1RowStatus = (UINT1) ACTIVE;
                }
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpSetAllTrackGroupIfTable                      */
/*                                                                           */
/*    Description         : This function validates VRRP version, VRRP       */
/*                          Track Group If Table and sets value of requested */
/*                          Object.                                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          u4GroupIndex - Group Index                       */
/*                          i4IfIndex    - Interface Index                   */
/*                          u4ObjectId   - Object Identifier                 */
/*                          pData        - Pointer to Data to be set         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNMP_SUCCESS or SNMP_FAILURE                     */
/*****************************************************************************/
INT4
VrrpSetAllTrackGroupIfTable (INT4 i4Version, UINT4 u4GroupIndex,
                             INT4 i4IfIndex, UINT4 u4ObjectId,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    tVrrpTrackGroupTable *pTrackGroupEntry = NULL;
    tVrrpTrackGroupIfTable *pTrackGroupIfEntry = NULL;
    BOOL1               b1VersionMatched = FALSE;
    INT4                i4RetVal = VRRP_NOT_OK;

    b1VersionMatched = VrrpCompCheckVersion (i4Version);

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return SNMP_FAILURE;
    }

    pTrackGroupEntry = VrrpDbGetTrackGroupEntry (u4GroupIndex);

    if (pTrackGroupEntry == NULL)
    {
        CLI_SET_ERR (CLI_VRRP_NO_TRACK_GROUP);

        return SNMP_FAILURE;
    }

    pTrackGroupIfEntry = VrrpDbGetTrackGroupIfEntry (pTrackGroupEntry,
                                                     i4IfIndex);

    switch (u4ObjectId)
    {
        case VRRP_MIB_TRACK_IF_ROW_STATUS:
            if (pData->i4_SLongValue == CREATE_AND_GO)
            {
                i4RetVal = VrrpDbCreateTrackGroupIfEntry (pTrackGroupEntry,
                                                          i4IfIndex);

                if (i4RetVal == VRRP_NOT_OK)
                {
                    return SNMP_FAILURE;
                }

                pTrackGroupIfEntry
                    = VrrpDbGetTrackGroupIfEntry (pTrackGroupEntry, i4IfIndex);

                VrrpCompCalcTrackGroupStatusAndAct (pTrackGroupEntry,
                                                    pTrackGroupIfEntry);
            }
            else
            {
                if (pTrackGroupIfEntry == NULL)
                {
                    CLI_SET_ERR (CLI_VRRP_NO_TRACK_IF);

                    return SNMP_FAILURE;
                }

                VrrpDbDeleteTrackGroupIfEntry (pTrackGroupEntry,
                                               pTrackGroupIfEntry);

                VrrpCompCalcTrackGroupStatusAndAct (pTrackGroupEntry, NULL);
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : VrrpSetAllScalars                                */
/*                                                                           */
/*    Description         : This function validates VRRP version and if      */
/*                          validation is successful, sets the value         */
/*                          else errors is returned                          */
/*                                                                           */
/*    Input(s)            : i4Version    - Version of MIB from which this    */
/*                                         function is called.               */
/*                          u4ObjectId   - Object Identifier                 */
/*                          pData        - Pointer to Data to be set         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
INT4
VrrpSetAllScalars (INT4 i4Version, UINT4 u4ObjectId,
                   tSNMP_MULTI_DATA_TYPE * pData)
{
    BOOL1               b1VersionMatched = TRUE;
    INT4                i4count = 0;

    if (u4ObjectId != VRRP_MIB_VERSION)
    {
        b1VersionMatched = VrrpCompCheckVersion (i4Version);
    }

    if (b1VersionMatched == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case VRRP_MIB_VERSION:
            if (pData->i4_SLongValue == VRRP_VERSION_2 &&
                gi4VrrpNodeVersion == VRRP_VERSION_2_3)
            {
                /* Reset the V3 Advertisement received status
                 * for all instances. otherwise incoming
                 * v2 packets will not be accepted because of
                 * already seen v3 advertisement status
                 */
                for (i4count = 0; i4count < (INT4) gu4VrrpMaxOperEntries;
                     i4count++)
                {
                    if (OPERROWSTATUS (i4count) == ACTIVE)
                    {
                        OPERV3ADVTRCVD (i4count) = FALSE;
                    }
                }
            }

            gi4VrrpNodeVersion = pData->i4_SLongValue;
            break;

        case VRRP_MIB_NOTIF_CTRL:
            gi4VrrpNotificationCntl = pData->i4_SLongValue;
            break;

        case VRRP_MIB_GBL_STATUS:
            if (pData->i4_SLongValue == gi4VrrpStatus)
            {
                break;
            }

            gi4VrrpStatus = pData->i4_SLongValue;

            if (gi4VrrpStatus == VRRP_ENABLE)
            {

                gi4VrrpIpv4Socket =
                    VrrpOpenSocket ((UINT1) VRRP_IPVX_ADDR_FMLY_IPV4);
                gi4VrrpIpv6Socket =
                    VrrpOpenSocket ((UINT1) VRRP_IPVX_ADDR_FMLY_IPV6);

                if ((gi4VrrpIpv4Socket == VRRP_NOT_OK) ||
                    (gi4VrrpIpv6Socket == VRRP_NOT_OK))

                {
                    VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                              "VRRP Socket opening FAILED\n");
                    return SNMP_FAILURE;
                }

#ifdef IP6_WANTED
                else
                {
                    VRRP_DBG (VRRP_TRC_INIT,
                              "VRRP Sucessfully Opened the sockets\n");
                }
#endif /* IP6_WANTED */

                VRRP_DBG (VRRP_TRC_INIT,
                          "VRRP Sucessfully Opened the sockets\n");

                if (VrrpRegisterwithIp (VRRPMODULE,
                                        (VrrpFuncPtr) VrrpRcvLinkStatus)
                    != VRRP_OK)
                {
                    CLI_SET_ERR (CLI_VRRP_REG_WITH_IP_ERROR);

                    VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                              "VRRP Registration with IP FAILED\n");

                    return SNMP_FAILURE;
                }

                VRRP_DBG (VRRP_TRC_INIT,
                          "VRRP Sucessfully Registered with IP\n");

                OsixEvtSend (gVrrpTaskId, VRRP_STARTUP_EVENT);
            }
            else
            {
                if (VrrpDeRegisterwithIp (VRRPMODULE) != VRRP_OK)
                {
                    CLI_SET_ERR (CLI_VRRP_DEREG_WITH_IP_ERROR);

                    VRRP_DBG (VRRP_TRC_ALL_FAILURES,
                              "VRRP DeRegisteration with IP FAILED\n");
                    return SNMP_FAILURE;

                }

                VRRP_DBG (VRRP_TRC_INIT,
                          "VRRP Sucessfully DeRegistered with IP\n");

                OsixEvtSend (gVrrpTaskId, VRRP_SHUTDOWN_EVENT);
            }
            break;

        case VRRP_MIB_TRACE_OPTION:
            gi4DbugFlag = pData->i4_SLongValue;
            break;

        case VRRP_MIB_AUTH_DEPRECATE:
            gi4VrrpAuthDeprecate = pData->i4_SLongValue;
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}
