/*  $Id: vrrptmrif.c,v 1.10 2014/03/11 14:22:22 siva Exp $ */
/********************************************************************
 *                                                                  *
 * $RCSfile: vrrptmrif.c,v $
 *                                                                  *
 * $Date: 2014/03/11 14:22:22 $
 *                                                                  *
 * $Revision: 1.10 $
 *                                                                  *
 *******************************************************************/

/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrptmrif.c                                  |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Interface Module.                       |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : Timer  Interface Module of VRRP Core Protocol.|
 * | The calls in this module would be mapped on to the target system calls.  |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#include "vrrpinc.h"

tTimerListId        gpVrrpTmrList;

/*******************************************************************************
 * Function Name   : TmrIfCreateTimer                                     
 * Description     : This Funtion Creates the Timer Blocks
 * Global Varibles : Pointer to Active Timer List - gpVrrpTmrList
 *                   Pointer to Free Timer List - pFreeTmrList
 * Inputs          : None
 * Output          : None
 * Returns         : VRRP_OK                                                 
 *                   VRRP_NOT_OK                     
 ******************************************************************************/
EXPORT INT1
TmrIfCreateTimer (VOID)
{

    if (TmrCreateTimerList (VRRP_TASK_NAME,
                            VRRP_TMR_EXP_EVENT,
                            NULL, &gpVrrpTmrList) != TMR_SUCCESS)
    {
        return (VRRP_NOT_OK);
    }

    return (VRRP_OK);
}

/*******************************************************************************
 * Function Name   : TmrIfStartTimer
 * Description     : This Funtion starts a timer
 * Global Varibles : Pointer to Active Timer List - gpVrrpTmrList
 *                   Pointer to Free Timer List - pFreeTmrList
 * Inputs          : Pointer to the Timer Structure
 *                   Timer Duration
 * Output          : None
 * Returns         : VRRP_OK
 *                   VRRP_NOT_OK
 ******************************************************************************/
EXPORT INT1
TmrIfStartTimer (VOID *pTimer, FLT4 fDuration)
{
    UINT4               u4Ticks = 0;
    FLT4                f4Ticks = 0;

    if (VRRP_GET_NODE_STATUS () != RM_ACTIVE)
    {
        return VRRP_OK;
    }

    f4Ticks = ((FLT4) SYS_NUM_OF_TIME_UNITS_IN_A_SEC) * fDuration / 1000;
    u4Ticks = (UINT4) f4Ticks;

    if (TmrStartTimer (gpVrrpTmrList,
                       (tTmrAppTimer *) (pTimer), u4Ticks) == TMR_FAILURE)
    {
        return (VRRP_NOT_OK);
    }

    return (VRRP_OK);
}

/*******************************************************************************
 * Function Name   : TmrIfStopTimer
 * Description     : This Funtion stops a timer
 * Global Varibles : Pointer to Active Timer List - gpVrrpTmrList
 *                   Pointer to Free Timer List - pFreeTmrList
 * Inputs          : Timer Reference
 * Output          : None
 * Returns         : VRRP_OK
 *                   VRRP_NOT_OK
 ******************************************************************************/
EXPORT INT1
TmrIfStopTimer (VOID *pTimer)
{
    TmrStopTimer (gpVrrpTmrList, (tTmrAppTimer *) (pTimer));

    VRRP_DBG (VRRP_TRC_TIMERS, "TmrIf: Successfully stoped the timer\n");
    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : TmrIfDeleteTimer
 * Description     : This Funtion Deletes the Timer Blocks
 * Global Varibles : Pointer to Active Timer List - gpVrrpTmrList
 *                   Pointer to Free Timer List - pFreeTmrList
 * Inputs          : None
 * Output          : None
 * Returns         : None
 ******************************************************************************/
EXPORT INT1
TmrIfDeleteTimer (VOID)
{

    TmrDeleteTimerList (gpVrrpTmrList);
    return VRRP_OK;
}

/*******************************************************************************
 * Function Name   : TmrIfGetExpiredTimers
 * Description     : This Funtion gets the Expired Timers
 * Global Varibles : Pointer to Active Timer List - gpVrrpTmrList
 *                   Pointer to Free Timer List - pFreeTmrList
 * Inputs          : None  
 * Output          : None
 * Returns         : None                                                 
 ******************************************************************************/
VOID
TmrIfGetExpiredTimers (VOID)
{
    tTmrAppTimer       *pVrrpAppTmr;
    tVrrpTimerRef      *pTimerRef;
    INT4                i4OperIndex;
    UINT1               u1TimerType;

    VRRP_DBG (VRRP_TRC_TIMERS, "TmrIf: Entered Tmrif after Timer Expiry\n");

    TmrGetExpiredTimers (gpVrrpTmrList, &pVrrpAppTmr);

    while (pVrrpAppTmr != NULL)
    {
        pTimerRef = (tVrrpTimerRef *) (pVrrpAppTmr);
        i4OperIndex = pTimerRef->i4OperIndex;

        /* Call SEM to process timer expiry */
        u1TimerType = pTimerRef->u1TimerType;

        VrrpSem (u1TimerType, i4OperIndex, OPERPRIORITY (i4OperIndex), ZERO);

        pVrrpAppTmr = TmrGetNextExpiredTimer (gpVrrpTmrList);
    }
}
