
/*  $Id: vrrpcli.c,v 1.48 2017/12/16 11:57:11 siva Exp $ */
#ifndef __VRRPCLI_C__
#define __VRRPCLI_C__
#include "vrrpinc.h"
#include "cli.h"
#include "vrrpcli.h"
#include "vrrpdef.h"
#include "fsvrrpwr.h"
#include "vrrpwr.h"
#include "stdvrrcli.h"
#include "fsvrrpcli.h"
#include "vrrp3cli.h"
#include "fsvr3cli.h"
#include "msr.h"

/**************************************************************************/
/*  Function Name   : cli_process_vrrp_cmd                                */
/*                                                                        */
/*  Description     : Protocol CLI message handler function               */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    u4Command - Command identifier                      */
/*                    ... -Variable command argument list                 */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
cli_process_vrrp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    tCfaIfInfo          CfaIfInfo;
    va_list             ap;
    UINT4              *args[VRRP_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4IfaceIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4version = 0;
    UINT1              *pu1Inst = NULL;
    tIp6Addr            Ip6Addr;
    INT4                i4Version = 0;
    UINT1               au1AutoLlAddr[IPVX_IPV6_ADDR_LEN] =
        { 0xfe, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x5e, 0x0, 0x0, 0x0,
0x2, 0x0, 0x0 };
    UINT1               au1VrMac[MAC_ADDR_LEN] =
        { 0x0, 0x0, 0x5e, 0x0, 0x2, 0x0 };
    UINT1               au1Ip6IfId[IP6_EUI_ADDRESS_LEN];

    MEMSET (au1Ip6IfId, 0, IP6_EUI_ADDRESS_LEN);

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);

    UNUSED_PARAM (pu1Inst);

    /* Walk through the rest of the arguements and store in args array. 
     * Store VRRP_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == VRRP_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, VrrpLock, VrrpUnLock);
    VRRP_LOCK ();

    switch (u4Command)
    {
        case VRRP_CLI_START:
            i4RetStatus = VrrpCliSetVrrpStatus (CliHandle, (INT4) VRRP_ENABLE);
            break;

        case VRRP_CLI_NO_START:
            i4RetStatus = VrrpCliSetVrrpStatus (CliHandle, (INT4) VRRP_DISABLE);
            break;

        case VRRP_CLI_VRRP_IF:
            /* args[0] - Interface Index */
            if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (i4Version == VRRP_VERSION_2_3)
            {
                CLI_SET_ERR (CLI_VRRP_VERSION_V2_V3_ERR);
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (CfaGetIfInfo ((UINT4) CLI_PTR_TO_I4 (args[0]),
                              &CfaIfInfo) == CFA_FAILURE)
            {
                CLI_SET_ERR (CLI_VRRP_INVALID_IFINDEX);

                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (!((CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
                  ((CfaIfInfo.u1IfType == CFA_ENET) &&
                   (CfaIfInfo.u1BridgedIface == CFA_DISABLED))))
            {
                CLI_SET_ERR (CLI_VRRP_INCORRECT_IFINDEX);

                i4RetStatus = CLI_FAILURE;
                break;
            }

            CLI_SET_IFINDEX (CLI_PTR_TO_I4 (args[0]));
            CliChangePath (CLI_VRRP_IF);

            break;

        case VRRP_CLI_NO_VRRP_IF:
            /* args[0] - Interface Index */
            i4RetStatus =
                VrrpCliSetNoVrrpInterface (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case VRRP_CLI_IP:
            /* args[0] - Virtual Router ID */
            /* args[1] - Primary IP Address */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetIpv4Address (CliHandle, i4IfaceIndex,
                                                 *(INT4 *) args[0],
                                                 *(INT4 *) args[1],
                                                 CLI_PTR_TO_U4 (args[2]));
            break;

        case VRRP_CLI_NO_IP:
            /* args[0] - Virtual Router ID */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliDelIpv4Address (CliHandle, i4IfaceIndex,
                                                 *(INT4 *) args[0],
                                                 *(INT4 *) args[1],
                                                 CLI_PTR_TO_U4 (args[2]));

            break;

        case VRRP_CLI_IPV6:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            if (VRRP_CLI_AUTOCONFIG == CLI_PTR_TO_I4 (args[1]))
            {
                au1VrMac[5] = (*(UINT1 *) args[0]);
                GET_EUI_64_IF_ID (au1VrMac, au1Ip6IfId);
                MEMCPY (au1AutoLlAddr + IP6_EUI_ADDRESS_LEN, au1Ip6IfId,
                        IP6_EUI_ADDRESS_LEN);
                MEMCPY (&Ip6Addr.ip6_addr_u.u1ByteAddr[0], au1AutoLlAddr,
                        IPVX_IPV6_ADDR_LEN);
            }
            else
            {
                if (INET_ATON6 (args[1], &Ip6Addr) == 0)
                {
                    CliPrintf (CliHandle,
                               "\r%% Incorrect Ipv6 Address Format\n");
                    break;
                }

                if (IS_ADDR_UNSPECIFIED (Ip6Addr))
                {
                    CliPrintf (CliHandle,
                               "\r%% Incorrect Ipv6 Address format\n");
                    break;
                }
            }
            i4RetStatus = VrrpCliSetIpv6Address (CliHandle, i4IfaceIndex,
                                                 *(INT4 *) args[0],
                                                 Ip6Addr.u1_addr,
                                                 CLI_PTR_TO_U4 (args[2]));
            break;

        case VRRP_CLI_NO_IPV6:
            i4IfaceIndex = CLI_GET_IFINDEX ();

            if (VRRP_CLI_AUTOCONFIG == CLI_PTR_TO_I4 (args[1]))
            {
                au1VrMac[5] = (*(UINT1 *) args[0]);
                GET_EUI_64_IF_ID (au1VrMac, au1Ip6IfId);
                MEMCPY (au1AutoLlAddr + IP6_EUI_ADDRESS_LEN, au1Ip6IfId,
                        IP6_EUI_ADDRESS_LEN);
                MEMCPY (&Ip6Addr.ip6_addr_u.u1ByteAddr[0], au1AutoLlAddr,
                        IPVX_IPV6_ADDR_LEN);
            }
            else
            {
                if ((args[1] != NULL) && (INET_ATON6 (args[1], &Ip6Addr) == 0))
                {
                    CliPrintf (CliHandle,
                               "\r%% Incorrect Ipv6 Address Format\n");
                    break;
                }

                if ((args[1] != NULL) && (IS_ADDR_UNSPECIFIED (Ip6Addr)))
                {
                    CliPrintf (CliHandle,
                               "\r%% Incorrect Ipv6 Address format\n");
                    break;
                }
            }
            i4RetStatus = VrrpCliDelIpv6Address (CliHandle, i4IfaceIndex,
                                                 *(INT4 *) args[0],
                                                 Ip6Addr.u1_addr,
                                                 CLI_PTR_TO_U4 (args[2]));
            break;

        case VRRP_CLI_PRIORITY:
            /* args[0] - Virtual Router ID */
            /* args[1] - Virtual Router Priority */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetPriority (CliHandle, i4IfaceIndex,
                                              *(INT4 *) args[0],
                                              CLI_PTR_TO_I4 (args[2]),
                                              *(INT4 *) args[1]);
            break;

        case VRRP_CLI_NO_PRIORITY:
            /* args[0] - Virtual Router ID */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetPriority (CliHandle, i4IfaceIndex,
                                              *(INT4 *) args[0],
                                              CLI_PTR_TO_I4 (args[1]),
                                              DEFAULT_PRIORITY);
            break;

        case VRRP_CLI_ACCEPT:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetAcceptMode (CliHandle, i4IfaceIndex,
                                                *(INT4 *) args[0],
                                                CLI_PTR_TO_I4 (args[2]),
                                                CLI_PTR_TO_I4 (args[1]));
            break;

        case VRRP_CLI_PREEMPT:
            /* args[0] - Virtual Router ID */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetPreemptMode (CliHandle, i4IfaceIndex,
                                                 *(INT4 *) args[0],
                                                 CLI_PTR_TO_I4 (args[1]),
                                                 VRRP_ENABLE);
            break;

        case VRRP_CLI_NO_PREEMPT:
            /* args[0] - Virtual Router ID */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetPreemptMode (CliHandle, i4IfaceIndex,
                                                 *(INT4 *) args[0],
                                                 CLI_PTR_TO_I4 (args[1]),
                                                 VRRP_DISABLE);
            break;

        case VRRP_CLI_AUTH:
            /* args[0] - Virtual Router ID */
            /* args[1] - Authentication key */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetVrrpAuthType (CliHandle, i4IfaceIndex,
                                                  *(INT4 *) args[0],
                                                  (UINT1 *) args[1]);
            break;

        case VRRP_CLI_NO_AUTH:
            /* args[0] - Virtual Router ID */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetVrrpAuthType (CliHandle, i4IfaceIndex,
                                                  *(INT4 *) args[0], NULL);
            break;

        case VRRP_CLI_AUTH_DEPRECATE:
            /* args[0]= Authentication Deprecated flag */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetVrrpAuthDeprecate (CliHandle,
                                                       CLI_PTR_TO_I4 (args[0]));
            break;

        case VRRP_CLI_TIMER:
            /* args[0] - Virtual Router ID */
            /* args[1] - Advertisement time Interval */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetVrrpAdvertiseInterval (CliHandle,
                                                           i4IfaceIndex,
                                                           *(INT4 *) args[0],
                                                           CLI_PTR_TO_I4
                                                           (args[2]),
                                                           *(INT4 *) args[1]);
            break;

        case VRRP_CLI_NO_TIMER:
            /* args[0] - Virtual Router ID */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetVrrpAdvertiseInterval (CliHandle,
                                                           i4IfaceIndex,
                                                           *(INT4 *) args[0],
                                                           CLI_PTR_TO_I4
                                                           (args[1]),
                                                           (INT4) VRRP_ZERO);
            break;

        case VRRP_CLI_MSEC_TIMER:
            /* args[0] - Virtual Router ID */
            /* args[1] - Advertisement time Interval */
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetVrrpAdvertiseIntervalInMsec (CliHandle,
                                                                 i4IfaceIndex,
                                                                 *(INT4 *)
                                                                 args[0],
                                                                 CLI_PTR_TO_I4
                                                                 (args[2]),
                                                                 *(INT4 *)
                                                                 args[1]);
            break;

        case VRRP_CLI_TRACK_GROUP:
            i4RetStatus = VrrpCliCreateTrackGroupIf (CliHandle,
                                                     *(UINT4 *) args[0],
                                                     CLI_PTR_TO_I4 (args[1]));
            break;

        case VRRP_CLI_NO_TRACK_GROUP:
            if (args[1] != NULL)
            {
                i4IfaceIndex = CLI_PTR_TO_I4 (args[1]);
            }
            i4RetStatus = VrrpCliDeleteTrackGroupIf (CliHandle,
                                                     *(UINT4 *) args[0],
                                                     i4IfaceIndex);
            break;

        case VRRP_CLI_TRACK_LINKS:
            i4RetStatus = VrrpCliSetTrackLinks (CliHandle,
                                                *(UINT4 *) args[0],
                                                *(INT4 *) args[1]);
            break;

        case VRRP_CLI_NO_TRACK_LINKS:
            i4RetStatus = VrrpCliSetTrackLinks (CliHandle,
                                                *(UINT4 *) args[0], 0);
            break;

        case VRRP_CLI_OPER_TRACK:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetGroupIdAndDecPrio (CliHandle, i4IfaceIndex,
                                                       *(INT4 *) args[0],
                                                       CLI_PTR_TO_I4 (args[3]),
                                                       *(UINT4 *) args[1],
                                                       *(INT4 *) args[2]);
            break;

        case VRRP_CLI_NO_OPER_TRACK:
            i4IfaceIndex = CLI_GET_IFINDEX ();
            i4RetStatus = VrrpCliSetGroupIdAndDecPrio (CliHandle, i4IfaceIndex,
                                                       *(INT4 *) args[0],
                                                       CLI_PTR_TO_I4 (args[1]),
                                                       0, 0);
            break;

        case VRRP_CLI_SET_VERSION:
            u4version = CLI_PTR_TO_U4 (args[0]);
            i4RetStatus = VrrpCliSetVersion (CliHandle, u4version);
            break;

        case VRRP_CLI_SHOW:
            /* args[0] - Display Hint */
            /* args[1] - Interface Index */
            /* args[2] - Virtual Router ID */
            VrrpCliShowVrrp (CliHandle, CLI_PTR_TO_U4 (args[0]),
                             CLI_PTR_TO_I4 (args[1]), CLI_PTR_TO_I4 (args[2]));
            break;

        case VRRP_CLI_SHOW_TRACK:
            VrrpCliShowTrack (CliHandle);
            break;

        case VRRP_CLI_TRACE:
            /*
             * args[0] - debug level to set
             */
            i4RetStatus =
                VrrpSetTraceLevel (CliHandle, CLI_ENABLE,
                                   CLI_PTR_TO_U4 (args[0]));
            break;

        case VRRP_CLI_NO_TRACE:
            /* 
             * args[0] - debug level to set
             */
            i4RetStatus =
                VrrpSetTraceLevel (CliHandle, CLI_DISABLE,
                                   CLI_PTR_TO_U4 (args[0]));
            break;

        default:
            i4RetStatus = CLI_FAILURE;
            break;
    }

    VRRP_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    if ((i4RetStatus == (INT4) CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_VRRP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", VrrpCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    return i4RetStatus;
}

/**************************************************************************
* Function Name :  VrrpCliSetVrrpStatus                                   *
*                                                                         *
* Description   :  This routine processes all the VRRP Cli commands.      *
*                                                                         *
* Input (s)     :  u4Status  - Vrrp Status                                *
*                                                                         *
* Output (s)    :  ppRespMsg - Formated output message.                   *
*                                                                         *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetVrrpStatus (tCliHandle CliHandle, INT4 i4Status)
{
    UNUSED_PARAM (CliHandle);

    if (VrrpCmnSetVrrpStatus (i4Status) == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    if (i4Status == (INT4) VRRP_ENABLE)
    {
        CliChangePath (CLI_VRRP);
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliSetPriority                                     *
*                                                                         *
* Description   :  This function  sets the priority for the Associated    *
*                  IP Address.                                            *
*                                                                         *
* Input (s)     :  i4IfIndex   - The interface over which Vrrp is to be   *
*                                enabled/disabled.                        *
*                  i4VrId      - Virtual Router Id.                       *
*                  i4AddrType  - Address Type                             *
*                  i4Priority  - Priority.                                *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetPriority (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4VrId,
                    INT4 i4AddrType, INT4 i4Priority)
{
    UNUSED_PARAM (CliHandle);

    if (VrrpCmnSetPriority (i4IfIndex, i4VrId, i4AddrType,
                            i4Priority) == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliSetVrrpAuthType                                 *
*                                                                         *
* Description   :  This function sets the Authentication Type and         *
*                  Authentication Key.                                    *
*                                                                         *
* Input (s)     :  i4IfIndex    - The interface index.                    *
*                  i4VrId       - Virtual router Identification           *
*                  au1AuthKey   _ Authentication Key.                     *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetVrrpAuthType (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4VrId,
                        UINT1 *au1AuthKey)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE AuthKey;
    INT4                i4AuthType = 0;
    UINT1               au1AuthKeyName[AUTHKEYSIZE + 1];
    INT4                i4Version = 0;
    INT4                i4AddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (CliHandle);

    MEMSET (au1AuthKeyName, 0, sizeof (au1AuthKeyName));

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return CLI_FAILURE;
    }

    if (i4Version == VRRP_VERSION_3)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return CLI_FAILURE;
    }

    VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINDOWN);

    if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId,
                                 i4AddrType, NOT_IN_SERVICE) == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    if (au1AuthKey != NULL)
    {
        i4AuthType = SIMPLE_TEXT_AUTHENTICATION_MIB_VAL;
    }
    else
    {
        i4AuthType = NO_AUTHENTICATION_MIB_VAL;
    }

    do
    {
        if (nmhTestv2VrrpOperAuthType
            (&u4ErrorCode, i4IfIndex, i4VrId, i4AuthType) != SNMP_SUCCESS)
        {
            nmhSetVrrpOperAuthType (i4IfIndex, i4VrId,
                                    NO_AUTHENTICATION_MIB_VAL);
            i4RetVal = CLI_FAILURE;

            break;
        }

        if (nmhSetVrrpOperAuthType (i4IfIndex, i4VrId, i4AuthType)
            == SNMP_FAILURE)
        {
            i4RetVal = CLI_FAILURE;

            break;
        }

        if (i4AuthType != NO_AUTHENTICATION_MIB_VAL)
        {
            if (NULL != au1AuthKey)
            {
                AuthKey.i4_Length = VRRP_STRLEN ((const char *) au1AuthKey);
                AuthKey.pu1_OctetList = au1AuthKeyName;
                if ((NULL != AuthKey.pu1_OctetList) &&
                    (AuthKey.i4_Length > ZERO) &&
                    (AuthKey.i4_Length <= AUTHKEYSIZE))
                {
                    /* Copy AuthKey only if it is of valid length */
                    VRRP_MEMCPY (AuthKey.pu1_OctetList, au1AuthKey,
                                 AuthKey.i4_Length);
                }
            }

            if (nmhTestv2VrrpOperAuthKey
                (&u4ErrorCode, i4IfIndex, i4VrId, &AuthKey) != SNMP_SUCCESS)
            {
                i4RetVal = CLI_FAILURE;

                break;
            }

            if (nmhSetVrrpOperAuthKey (i4IfIndex, i4VrId, &AuthKey) ==
                SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;

                break;
            }
        }
    }
    while (0);

    if (VrrpCmnSetOperRowStatus (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                 ACTIVE) == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    VrrpCmnSetAdminState (i4IfIndex, i4VrId, ADMINUP);

    return i4RetVal;
}

/**************************************************************************
 *  Function Name :  VrrpCliSetVrrpAuthDeprecate                           
 *                                                                          
 *  Description   :  This function sets the Authentication Type and         
 *                   Authentication Key.                                    
 *                                                                          
 *   Input (s)     :  i4IfIndex    - The interface index.                   
 *                    i4VrId       - Virtual router Identification           
 *                    au1AuthKey   _ Authentication Key.                     
 *                                                                           
 *  Output (s)    :  None                                                   
 *                                                                           
 *  Returns       :  None.                                                 
 * ***************************************************************************/
INT4
VrrpCliSetVrrpAuthDeprecate (tCliHandle CliHandle, INT4 i4AuthDeprecate)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Version = 0;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return CLI_FAILURE;
    }

    if (i4Version == VRRP_VERSION_3)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_VERSION);

        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsVrrpAuthDeprecate (&u4ErrorCode, i4AuthDeprecate)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVrrpAuthDeprecate (i4AuthDeprecate) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliSetVrrpAdvertiseInterval                        *
*                                                                         *
* Description   :  This function sets the Advertisement Interval.         *
*                                                                         *
* Input (s)     :  i4IfIndex     - The interface index.                   *
*                  i4VrId        - Virtual router Identification          *
*                  i4AddrType    - Address Type                           *
*                  i4AdvInterval - Advertisement Interval.                *
*                                                                         *
* Output (s)    :  None                                                   *
*                                                                         *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetVrrpAdvertiseInterval (tCliHandle CliHandle, INT4 i4IfIndex,
                                 INT4 i4VrId, INT4 i4AddrType,
                                 INT4 i4AdvInterval)
{
    UNUSED_PARAM (CliHandle);

    if (VrrpCmnSetAdvertisementInterval (i4IfIndex, i4VrId, i4AddrType,
                                         i4AdvInterval) == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
 * Function Name :  VrrpCliSetVrrpAdvertiseIntervalInMsec                  *
 *                                                                         *
 * Description   :  This function sets the Advertisement Interval.         *
 *                                                                         *
 * Input (s)     :  i4IfIndex     - The interface index.                   *
 *                  i4VrId        - Virtual router Identification          *
 *                  i4AddrType    - Address Type                           *
 *                  i4AdvInterval - Advertisement Interval.                *
 *                                                                         *
 * Output (s)    :  None                                                   *
 *                                                                         *
 * Returns       :  None.                                                  *
 ***************************************************************************/
INT4
VrrpCliSetVrrpAdvertiseIntervalInMsec (tCliHandle CliHandle, INT4 i4IfIndex,
                                       INT4 i4VrId, INT4 i4AddrType,
                                       INT4 i4AdvInterval)
{
    UNUSED_PARAM (CliHandle);

    if (VrrpCmnSetAdvertisementIntervalInMSec (i4IfIndex, i4VrId, i4AddrType,
                                               i4AdvInterval) == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliSetPreemptMode                                  *
*                                                                         *
* Description   :  This function  sets the preempt mode to enable or      *
*                  disable.                                               *
*                                                                         *
* Input (s)     :  i4IfIndex     - The interface index.                   *
*                  i4VrId        - Virtual router Identification          *
*                  i4AddrType    - Address Type                           *
*                  i4Preempt     - VRRP_ENABLE/VRRP_DISABLE               *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetPreemptMode (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4VrId,
                       INT4 i4AddrType, INT4 i4Preempt)
{
    UNUSED_PARAM (CliHandle);

    if (VrrpCmnSetPreemptMode (i4IfIndex, i4VrId, i4AddrType, i4Preempt)
        == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliSetAcceptMode                                   *
*                                                                         *
* Description   :  This function  sets the accept mode to enable or       *
*                  disable.                                               *
*                                                                         *
* Input (s)     :  i4IfIndex     - The interface index.                   *
*                  i4VrId        - Virtual router Identification          *
*                  i4AddrType    - Address Type                           *
*                  i4AcceptMode  - Accept Mode Enable / Disable           *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetAcceptMode (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4VrId,
                      INT4 i4AddrType, INT4 i4AcceptMode)
{
    UNUSED_PARAM (CliHandle);

    if (VrrpCmnSetAcceptMode (i4IfIndex, i4VrId, i4AddrType, i4AcceptMode)
        == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliSetGroupIdAndDecPrio                            *
*                                                                         *
* Description   :  This function  sets the Group Id and Decrement         *
*                  Priority.                                              *
*                                                                         *
* Input (s)     :  i4IfIndex     - The interface index.                   *
*                  i4VrId        - Virtual router Identification          *
*                  i4AddrType    - Address Type                           *
*                  u4GroupIndex  - Group Index                            *
*                  i4Priority    - Decrement Priority                     *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetGroupIdAndDecPrio (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4VrId,
                             INT4 i4AddrType, UINT4 u4GroupIndex,
                             INT4 i4Priority)
{
    UNUSED_PARAM (CliHandle);

    if (VrrpCmnSetTrackAndDecPriority (i4IfIndex, i4VrId, i4AddrType,
                                       u4GroupIndex, i4Priority) == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliCreateTrackGroupIf                              *
*                                                                         *
* Description   :  This function creates track group and interface.       *
*                                                                         *
* Input (s)     :  u4GroupIndex  - Group Index                            *
*                  i4IfIndex     - Interface Index                        *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliCreateTrackGroupIf (tCliHandle CliHandle, UINT4 u4GroupIndex,
                           INT4 i4IfIndex)
{
    UNUSED_PARAM (CliHandle);

    if (VrrpCmnCreateTrackGroupIf (u4GroupIndex, i4IfIndex) == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliDeleteTrackGroupIf                              *
*                                                                         *
* Description   :  This function deletes track group and interface.       *
*                                                                         *
* Input (s)     :  u4GroupIndex  - Group Index                            *
*                  i4IfIndex     - Interface Index                        *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliDeleteTrackGroupIf (tCliHandle CliHandle, UINT4 u4GroupIndex,
                           INT4 i4IfIndex)
{
    UNUSED_PARAM (CliHandle);

    if (VrrpCmnDeleteTrackGroupIf (u4GroupIndex, i4IfIndex) == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliSetTrackLinks                                   *
*                                                                         *
* Description   :  This function sets tracked interface links             *
*                                                                         *
* Input (s)     :  u4GroupIndex  - Group Index                            *
*                  i4Links       - Tracked Links                          *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetTrackLinks (tCliHandle CliHandle, UINT4 u4GroupIndex, INT4 i4Links)
{
    UNUSED_PARAM (CliHandle);

    if (VrrpCmnSetTrackLinks (u4GroupIndex, i4Links) == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliSetNoVrrpInterface                              *
*                                                                         *
* Description   :  This function  deletes the VR Entries on the Interface *
*                                                                         *
* Input (s)     :  i4DelIfIndex     - The interface index.                *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetNoVrrpInterface (tCliHandle CliHandle, INT4 i4DelIfIndex)
{
    INT4                i4IfIndex = i4DelIfIndex;
    INT4                i4NextIfIndex = 0;
    INT4                i4VrId = 0;
    INT4                i4NextVrId = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4Version = 0;
    INT4                i4RetVal = VRRP_OK;
    INT1                i1RetVal = SNMP_SUCCESS;
    BOOL1               bEntryFound = FALSE;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return CLI_FAILURE;
    }

    while (i1RetVal == SNMP_SUCCESS)
    {
        if (i4Version == VRRP_VERSION_2)
        {
            i1RetVal = nmhGetNextIndexVrrpOperTable (i4IfIndex, &i4NextIfIndex,
                                                     i4VrId, &i4NextVrId);

            i4NextAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
        }
        else
        {
            i1RetVal = nmhGetNextIndexVrrpv3OperationsTable (i4IfIndex,
                                                             &i4NextIfIndex,
                                                             i4VrId,
                                                             &i4NextVrId,
                                                             i4AddrType,
                                                             &i4NextAddrType);
        }

        if ((i1RetVal == SNMP_FAILURE) || (i4NextIfIndex != i4DelIfIndex))
        {
            break;
        }

        bEntryFound = TRUE;

        if (VrrpCmnDeleteVrId (i4Version, i4NextIfIndex, i4NextVrId,
                               i4NextAddrType) == VRRP_NOT_OK)
        {
            i4RetVal = VRRP_NOT_OK;
            break;
        }

        i4IfIndex = i4NextIfIndex;
        i4VrId = i4NextVrId;
        i4AddrType = i4NextAddrType;
    }

    UNUSED_PARAM (CliHandle);

    if (bEntryFound == FALSE)
    {
        CLI_SET_ERR (CLI_VRRP_NO_VR_IN_IF);

        return CLI_FAILURE;
    }

    if (i4RetVal == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliShowVrrp                                        *
*                                                                         *
* Description   :  This function is invoked to get the global VRRP        *
*                  configuratiion and store in the output buffer.         *
*                                                                         *
* Input (s)     :  None.                                                  *
*                                                                         *
* Output (s)    :  Formated output message.                               *
*                                                                         *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliShowVrrp (tCliHandle CliHandle, UINT4 u4Val, INT4 i4IfIndexVal,
                 INT4 i4VrIdVal)
{
    UINT1               u1DispFlag = TRUE;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4VrId = 0;
    INT4                i4NextVrId = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4Version = 0;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    /* This routine handles three possible combinations 
     * show vrrp [ brief | stats | detail ]
     * show vrrp interface Vlan X [ brief | stats | detail ]
     * show vrrp interface Vlan x vrid [brief | stats | detail ]
     */

    if (i4Version == VRRP_VERSION_2)
    {
        i4RetVal = nmhGetFirstIndexVrrpOperTable (&i4IfIndex, &i4VrId);

        i4AddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        i4RetVal = nmhGetFirstIndexVrrpv3OperationsTable (&i4IfIndex,
                                                          &i4VrId, &i4AddrType);

    }

    CliPrintf (CliHandle, "\r\n");
    if ((u4Val == VRRP_CLI_BRIEF) && (i4RetVal == SNMP_SUCCESS))
    {
        /* VRRP brief requires a header to be printed. Hence this is 
         * handled here. 
         */

        CliPrintf (CliHandle, "\rP indicates configured to preempt\r\n");

        CliPrintf (CliHandle,
                   "\r\nInterface vrID Prio P State  Master Address            "
                   "VRouter Address          \r\n");
        CliPrintf (CliHandle,
                   "--------- ---- ---- - -----  ------------------------- "
                   "-------------------------\r\n");
    }

    if ((u4Val == VRRP_CLI_STAT) && (i4IfIndexVal == 0) && (i4VrIdVal == 0))
    {
        /* Display the global statistics only for show vrrp stats */

        VrrpCliShowGlobalStats (CliHandle, i4Version);
    }

    /* Scan the VRRP oper table and display the entries based on user input.
     * If no user input is given all entries will be displayed. If the user has
     * given a specific VLAN details related all VRID for that interface will
     * be displayed. If user has provided VLAN and VRID only information related
     * to the specific entry will be displayed.
     */

    while (i4RetVal == SNMP_SUCCESS)
    {
        u1DispFlag = TRUE;
        if (i4IfIndexVal != 0)
        {
            if (i4VrIdVal != 0)
            {
                if ((i4IfIndex != i4IfIndexVal) || (i4VrIdVal != i4VrId))
                {
                    u1DispFlag = FALSE;
                }
            }
            else
            {
                if (i4IfIndex != i4IfIndexVal)
                {
                    u1DispFlag = FALSE;
                }
            }
        }

        if ((u4Val == VRRP_CLI_DETAIL) && (u1DispFlag == TRUE))
        {
            VrrpCliShowVrrpDetail (CliHandle, i4Version, i4IfIndex, i4VrId,
                                   i4AddrType);
        }

        if ((u4Val == VRRP_CLI_BRIEF) && (u1DispFlag == TRUE))
        {
            VrrpCliShowVrrpBrief (CliHandle, i4Version, i4IfIndex, i4VrId,
                                  i4AddrType);
        }

        if ((u4Val == VRRP_CLI_STAT) && (u1DispFlag == TRUE))
        {
            VrrpCliShowVrrpStats (CliHandle, i4Version, i4IfIndex, i4VrId,
                                  i4AddrType);
        }

        if (i4Version == VRRP_VERSION_2)
        {
            i4RetVal = nmhGetNextIndexVrrpOperTable (i4IfIndex, &i4NextIfIndex,
                                                     i4VrId, &i4NextVrId);

            i4NextAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
        }
        else
        {
            i4RetVal =
                nmhGetNextIndexVrrpv3OperationsTable (i4IfIndex, &i4NextIfIndex,
                                                      i4VrId, &i4NextVrId,
                                                      i4AddrType,
                                                      &i4NextAddrType);
        }

        i4IfIndex = i4NextIfIndex;
        i4VrId = i4NextVrId;
        i4AddrType = i4NextAddrType;
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliShowVrrpBrief                                   *
*                                                                         *
* Description   :  This function displays Brief information for a given   *
*                  Interface Index and VrID                               *
* Input (s)     :  i4IfIndex - Interface Index                            *
*                  i4VrId    - Virtual Router ID                          * 
* Output (s)    :  Formated output message.                               *
*                                                                         *
* Returns       :  None.                                                  *
**************************************************************************/
VOID
VrrpCliShowVrrpBrief (tCliHandle CliHandle, INT4 i4Version,
                      INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType)
{
    UINT4               u4Port = 0;
    INT4                i4Priority = 0;
    tNetIpv4IfInfo      IfInfo;
    tVrrpOperTable      OperTable;

    MEMSET (&OperTable, 0, sizeof (tVrrpOperTable));
    MEMSET (&IfInfo, 0, sizeof (tNetIpv4IfInfo));

    if (VrrpCmnGetAllOperTable (i4Version, i4IfIndex, i4VrId,
                                i4AddrType, &OperTable) == VRRP_NOT_OK)
    {
        return;
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return;
    }

    if (NetIpv4GetIfInfo (u4Port, &IfInfo) == NETIPV4_FAILURE)
    {
        return;
    }

    CliPrintf (CliHandle, "%-9s ", (INT1 *) IfInfo.au1IfName);

    CliPrintf (CliHandle, "%-4d ", i4VrId);

    if (OperTable.b1IsIpvXOwner == TRUE)
    {
        i4Priority = VRRP_IPOWNER_PRIORITY;
    }
    else
    {
        i4Priority = OperTable.i4Priority;
    }

    if (OperTable.u1TrackedIfStatus == VRRP_TRACK_IF_DOWN)
    {
        i4Priority = (i4Priority - OperTable.u1DecrementedPriority);
    }

    CliPrintf (CliHandle, "%-4d ", i4Priority);

    if (OperTable.i4PreemptMode == VRRP_ENABLE)
    {
        CliPrintf (CliHandle, "%-1s ", "P");
    }
    else
    {
        CliPrintf (CliHandle, "%-1s ", " ");
    }

    if (OperTable.i4OperState == INITIAL_STATE)
    {
        CliPrintf (CliHandle, "%-6s ", "Init");
    }
    else if (OperTable.i4OperState == BACKUP_STATE)
    {
        CliPrintf (CliHandle, "%-6s ", "Backup");
    }
    else
    {
        CliPrintf (CliHandle, "%-6s ", "Master");
    }

    if (VRRP_IPVX_COMPARE (OperTable.MasterIpAddr, gVrrpZeroIpAddr) != 0)
    {
        CliPrintf (CliHandle, "%-25s ",
                   VRRP_IPVX_PRINT_ADDR (OperTable.MasterIpAddr, i4AddrType));
    }
    else
    {
        CliPrintf (CliHandle, "%-25s ", "-");
    }

    if (VRRP_IPVX_COMPARE (OperTable.PrimaryIpAddr, gVrrpZeroIpAddr) != 0)
    {
        CliPrintf (CliHandle, "%-25s\r\n",
                   VRRP_IPVX_PRINT_ADDR (OperTable.PrimaryIpAddr, i4AddrType));
    }
    else
    {
        CliPrintf (CliHandle, "%-25s\r\n", "-");
    }

    return;
}

/**************************************************************************
* Function Name :  VrrpCliShowVrrpDetail                                  *
*                                                                         *
* Description   :  This function displays detailed information for a given*
*                  Interface Index and VrID                               *
*                                                                         *
* Input (s)     :  CliHandle      - CLI Handle                            *
*                  i4Version      - VRRP Version                          *
*                  i4IfIndex      - Interface Index                       *
*                  i4VrId         - Virtual Router ID                     *
*                  i4AddrType     - Address Type                          *
*                                                                         *
* Output (s)    :  Formated output message.                               *
*                                                                         *
* Returns       :  None.                                                  *
**************************************************************************/
VOID
VrrpCliShowVrrpDetail (tCliHandle CliHandle, INT4 i4Version,
                       INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType)
{
    tVrrpOperTable      OperTable;
    tVrrpIPvXAddr       AssoIpAddr;
    tSNMP_OCTET_STRING_TYPE AssoIp;
    INT4                i4NextVrId = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4Priority = 0;
    UINT4               u4Port = 0;
    UINT4               u4NextIp = 0;
    UINT4               u4Time = 0;
    CHR1               *pu1Address = NULL;
    INT1                ai1Time[VRRP_CLI_TIME_STRING];
    UINT1               au1Address[MAX_ADDR_LEN];
    UINT1               au1AssoIp[IPVX_IPV6_ADDR_LEN];
    tNetIpv4IfInfo      IfInfo;

    pu1Address = (CHR1 *) & au1Address[0];

    MEMSET (&OperTable, 0, sizeof (tVrrpOperTable));
    MEMSET (ai1Time, 0, VRRP_CLI_TIME_STRING);
    MEMSET (&IfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (au1Address, 0, MAX_ADDR_LEN);
    MEMSET (au1AssoIp, 0, IPVX_IPV4_ADDR_LEN);
    MEMSET (&AssoIp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    AssoIp.pu1_OctetList = au1AssoIp;
    AssoIp.i4_Length = IPVX_IPV6_ADDR_LEN;

    if (VrrpCmnGetAllOperTable (i4Version, i4IfIndex, i4VrId, i4AddrType,
                                &OperTable) == VRRP_NOT_OK)
    {
        return;
    }

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return;
    }

    if (NetIpv4GetIfInfo (u4Port, &IfInfo) == NETIPV4_FAILURE)
    {
        return;
    }

    CliPrintf (CliHandle, "%s  -", IfInfo.au1IfName);

    CliPrintf (CliHandle, " vrID %d\r\n", i4VrId);
    CliPrintf (CliHandle, "------------------\r\n");

    if (OperTable.i4OperState == INITIAL_STATE)
    {
        CliPrintf (CliHandle, "  State is Init\r\n");
    }
    else if (OperTable.i4OperState == BACKUP_STATE)
    {
        CliPrintf (CliHandle, "  State is Backup\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "  State is Master\r\n");
    }

    if (VRRP_IPVX_COMPARE (OperTable.PrimaryIpAddr, gVrrpZeroIpAddr) != 0)
    {
        CliPrintf (CliHandle,
                   "  Virtual IP address is %s\r\n",
                   VRRP_IPVX_PRINT_ADDR (OperTable.PrimaryIpAddr, i4AddrType));
    }
    else
    {
        CliPrintf (CliHandle, "  Virtual IP address is Unknown\r\n");
    }

    MEMSET (au1Address, 0, MAX_ADDR_LEN);
    CLI_CONVERT_MAC_TO_DOT_STR (OperTable.maVirtualMacAddr,
                                (UINT1 *) pu1Address);

    CliPrintf (CliHandle, "  Virtual MAC address is %s\r\n", pu1Address);

    if (VRRP_IPVX_COMPARE (OperTable.MasterIpAddr, gVrrpZeroIpAddr) != 0)
    {
        CliPrintf (CliHandle,
                   "  Master router is %s \r\n",
                   VRRP_IPVX_PRINT_ADDR (OperTable.MasterIpAddr, i4AddrType));
    }
    else
    {
        CliPrintf (CliHandle, "  Master router is Unknown\r\n");
    }

    CliPrintf (CliHandle, "  Associated IpAddresses : \r\n");

    CliPrintf (CliHandle, "  ----------------------    \r\n");

    if (i4Version == VRRP_VERSION_2)
    {
        i4RetVal = nmhGetNextIndexVrrpAssoIpAddrTable (i4IfIndex,
                                                       &i4NextIfIndex, i4VrId,
                                                       &i4NextVrId, 0,
                                                       &u4NextIp);

        VRRP_IPVX_COPY_TO_IPVX (AssoIpAddr, u4NextIp);
        i4NextAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        i4RetVal = nmhGetNextIndexVrrpv3AssociatedIpAddrTable (i4IfIndex,
                                                               &i4NextIfIndex,
                                                               i4VrId,
                                                               &i4NextVrId,
                                                               i4AddrType,
                                                               &i4NextAddrType,
                                                               &AssoIp,
                                                               &AssoIp);

        VRRP_IPVX_ADDR_INIT_IPVX (AssoIpAddr, (UINT1) i4NextAddrType,
                                  AssoIp.pu1_OctetList);
    }

    while ((i4RetVal == SNMP_SUCCESS) &&
           (i4IfIndex == i4NextIfIndex) && (i4VrId == i4NextVrId) &&
           (i4AddrType == i4NextAddrType))
    {
        CliPrintf (CliHandle, "  %s\r\n",
                   VRRP_IPVX_PRINT_ADDR (AssoIpAddr, i4AddrType));

        if (i4Version == VRRP_VERSION_2)
        {
            i4RetVal = nmhGetNextIndexVrrpAssoIpAddrTable (i4NextIfIndex,
                                                           &i4NextIfIndex,
                                                           i4NextVrId,
                                                           &i4NextVrId,
                                                           u4NextIp, &u4NextIp);

            VRRP_IPVX_COPY_TO_IPVX (AssoIpAddr, u4NextIp);
            i4NextAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
        }
        else
        {
            i4RetVal =
                nmhGetNextIndexVrrpv3AssociatedIpAddrTable (i4NextIfIndex,
                                                            &i4NextIfIndex,
                                                            i4NextVrId,
                                                            &i4NextVrId,
                                                            i4NextAddrType,
                                                            &i4NextAddrType,
                                                            &AssoIp, &AssoIp);

            VRRP_IPVX_ADDR_INIT_IPVX (AssoIpAddr, (UINT1) i4NextAddrType,
                                      AssoIp.pu1_OctetList);
        }
    }

    if ((i4Version == VRRP_VERSION_2_3) || (i4Version == VRRP_VERSION_3))
    {
        OperTable.i4AdvtInterval = (OperTable.i4AdvtInterval * 10);

        OperTable.i4RcvdMasterAdvtInterval
            = (OperTable.i4RcvdMasterAdvtInterval * 10);

        OperTable.i4SkewTime = (OperTable.i4SkewTime * 10);

        OperTable.i4MasterDownInterval = (OperTable.i4MasterDownInterval * 10);
    }

    CliPrintf (CliHandle,
               "  Advertise time is %d milli secs\r\n",
               OperTable.i4AdvtInterval);

    if (OperTable.b1IsIpvXOwner == TRUE)
    {
        i4Priority = VRRP_IPOWNER_PRIORITY;
    }
    else
    {
        i4Priority = OperTable.i4Priority;
    }

    if (OperTable.u1TrackedIfStatus == VRRP_TRACK_IF_DOWN)
    {
        i4Priority = (i4Priority - OperTable.u1DecrementedPriority);
    }

    CliPrintf (CliHandle, "  Current priority is %d\r\n", i4Priority);

    CliPrintf (CliHandle, "  Configured priority is %d", OperTable.i4Priority);

    if (OperTable.i4PreemptMode == VRRP_ENABLE)
    {
        CliPrintf (CliHandle, ", may preempt\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
    }

    if ((i4Version == VRRP_VERSION_2) || (i4Version == VRRP_VERSION_2_3))
    {
        if (OperTable.i4AuthType != NO_AUTHENTICATION_MIB_VAL)
        {
            CliPrintf (CliHandle,
                       "  Configured Authentication key of vrrp is invisible\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  No Authentication \r\n");
        }
    }

    if (OperTable.u4TrackGroupId != 0)
    {
        CliPrintf (CliHandle,
                   "  Tracked Group is %u, ", OperTable.u4TrackGroupId);

        if (OperTable.u1DecrementedPriority != 0)
        {
            CliPrintf (CliHandle,
                       "Decrement Priority is %d\r\n",
                       OperTable.u1DecrementedPriority);
        }
        else
        {
            CliPrintf (CliHandle, "Decrement Priority is not configured\r\n");
        }

        if (OperTable.u1TrackedIfStatus == VRRP_TRACK_IF_DOWN)
        {
            CliPrintf (CliHandle, "  Tracked Group is DOWN\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  Tracked Group is UP\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "  Tracked Group is not configured\r\n");
    }

    if (OperTable.u1AcceptMode == VRRP_ENABLE)
    {
        CliPrintf (CliHandle, "  Accept Mode is Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "  Accept Mode is Disabled\r\n");
    }

    if ((i4Version == VRRP_VERSION_2_3) || (i4Version == VRRP_VERSION_3))
    {

        if (OperTable.i4RcvdMasterAdvtInterval != 0)
        {
            CliPrintf (CliHandle,
                       "  Received Master Advertisement Interval is %d "
                       "milli secs\r\n", OperTable.i4RcvdMasterAdvtInterval);
        }

        if (OperTable.i4SkewTime != 0)
        {
            CliPrintf (CliHandle,
                       "  Computed Skew Time is %d milli secs\r\n",
                       OperTable.i4SkewTime);
        }

        if (OperTable.i4MasterDownInterval != 0)
        {
            CliPrintf (CliHandle,
                       "  Computed Master Down Interval is %d "
                       "milli secs\r\n", OperTable.i4MasterDownInterval);
        }
    }

    OsixGetSysTime (&u4Time);
    VrrpCliConvertTimetoString ((u4Time - OperTable.u4VirtualRouterUpTime),
                                ai1Time);

    CliPrintf (CliHandle,
               "  Time Since Virtual Router is UP is %s\r\n", ai1Time);

    CliPrintf (CliHandle, "\r\n");

    return;
}

/**************************************************************************
* Function Name :  VrrpCliShowGlobalStats                                 *
*                                                                         *
* Description   :  This function is invoked to get the global VRRP        *
*                  statistics and store in the output buffer.             *
*                                                                         *
* Input (s)     :  CliHandle    - CLI Handle                              *
*                  i4Version    - VRRP Version                            *
*                                                                         *
* Output (s)    :  Formated output message.                               *
*                                                                         *
* Returns       :  None.                                                  *
***************************************************************************/
VOID
VrrpCliShowGlobalStats (tCliHandle CliHandle, INT4 i4Version)
{
    CHR1                ac1Val[VRRP_U8_STR_LENGTH];
    tSNMP_COUNTER64_TYPE Val;
    FS_UINT8            u8Val;
    UINT4               u4Val;

    MEMSET ((UINT1 *) ac1Val, 0, VRRP_U8_STR_LENGTH);
    Val.msn = 0;
    Val.lsn = 0;
    u8Val.u4Hi = 0;
    u8Val.u4Lo = 0;

    CliPrintf (CliHandle, "VRRP Statistics\r\n");

    CliPrintf (CliHandle, "---------------\r\n");

    if (i4Version == VRRP_VERSION_2)
    {
        nmhGetVrrpRouterChecksumErrors (&u4Val);

        CliPrintf (CliHandle,
                   " Router Checksum Errors          : %d\r\n", u4Val);

        nmhGetVrrpRouterVersionErrors (&u4Val);

        CliPrintf (CliHandle,
                   " Router Version Errors           : %d\r\n", u4Val);

        nmhGetVrrpRouterVrIdErrors (&u4Val);

        CliPrintf (CliHandle,
                   " Router VrId Errors              : %d\r\n", u4Val);
    }
    else
    {
        nmhGetVrrpv3RouterChecksumErrors (&Val);

        u8Val.u4Hi = Val.msn;
        u8Val.u4Lo = Val.lsn;

        FSAP_U8_2STR (&u8Val, ac1Val);

        CliPrintf (CliHandle,
                   " Router Checksum Errors          : %s\r\n", ac1Val);

        Val.msn = 0;
        Val.lsn = 0;
        u8Val.u4Hi = 0;
        u8Val.u4Lo = 0;
        MEMSET ((UINT1 *) ac1Val, 0, VRRP_U8_STR_LENGTH);

        nmhGetVrrpv3RouterVersionErrors (&Val);

        u8Val.u4Hi = Val.msn;
        u8Val.u4Lo = Val.lsn;

        FSAP_U8_2STR (&u8Val, ac1Val);

        CliPrintf (CliHandle,
                   " Router Version Errors           : %s\r\n", ac1Val);

        Val.msn = 0;
        Val.lsn = 0;
        u8Val.u4Hi = 0;
        u8Val.u4Lo = 0;
        MEMSET ((UINT1 *) ac1Val, 0, VRRP_U8_STR_LENGTH);

        nmhGetVrrpv3RouterVrIdErrors (&Val);

        u8Val.u4Hi = Val.msn;
        u8Val.u4Lo = Val.lsn;

        FSAP_U8_2STR (&u8Val, ac1Val);

        CliPrintf (CliHandle,
                   " Router VrId Errors              : %s\r\n", ac1Val);

        nmhGetVrrpv3GlobalStatisticsDiscontinuityTime (&Val.msn);
    }

    CliPrintf (CliHandle, "\r\n");

    return;
}

/**************************************************************************
* Function Name :  VrrpCliShowVrrpStats                                   *
*                                                                         *
* Description   :  This function is invoked to get the VRRP               *
*                  statistics and store in the output buffer.             *
*                                                                         *
* Input (s)     :  i4IfIndex  - Interface Index                           *
*                  i4VrrpOperVrId - VRID on the Interface                 *
*                                                                         *
* Output (s)    :  ppRespMsg - Formated output message.                   *
*                                                                         *
* Returns       :  None.                                                  *
***************************************************************************/
VOID
VrrpCliShowVrrpStats (tCliHandle CliHandle, INT4 i4Version,
                      INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType)
{

    UINT4               u4Port = 0;
    tNetIpv4IfInfo      IfInfo;
    tVrrpStatsTable     Stats;
    CHR1                ac1Val[VRRP_U8_STR_LENGTH];
    CHR1               *pac1LastProtoError[] = {
        "No Error",
        "Incorrect IP TTL",
        "Incorrect Version",
        "Incorrect Checksum",
        "Invalid VrId",
        "Advertisement Interval Mismatch",
        "Invalid VRRP Packet Type",
        "Address List Mismatch",
        "Incorrect Packet Length"
    };
    CHR1               *pac1NewMasterReason[] = {
        "Not Master",
        "Priority",
        "Preempted",
        "Master No Response"
    };

    MEMSET (&IfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&Stats, 0, sizeof (tVrrpStatsTable));
    MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex,
                                   &u4Port) == NETIPV4_FAILURE)
    {
        return;
    }

    if (NetIpv4GetIfInfo (u4Port, &IfInfo) == NETIPV4_FAILURE)
    {
        return;
    }

    if (VrrpCmnGetAllStatsTable (i4Version, i4IfIndex, i4VrId,
                                 i4AddrType, &Stats) == VRRP_NOT_OK)
    {
        return;
    }

    CliPrintf (CliHandle, "%s  -", IfInfo.au1IfName);

    CliPrintf (CliHandle, " vrID %d\r\n", i4VrId);

    CliPrintf (CliHandle, "------------------\r\n");

    CliPrintf (CliHandle, " Transitions to Master          : %d\r\n",
               Stats.u4BecomeMaster);

    FSAP_U8_2STR (&(Stats.u8RcvdAdvertisements), ac1Val);

    CliPrintf (CliHandle, " Advertisements Received        : %s\r\n", ac1Val);

    MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

    FSAP_U8_2STR (&(Stats.u8AdvtIntervalErrors), ac1Val);

    CliPrintf (CliHandle, " Advertise Interval Errors      : %s\r\n", ac1Val);

    MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

    if (i4Version == VRRP_VERSION_2)
    {
        CliPrintf (CliHandle,
                   " Authentication Failures        : %d\r\n",
                   Stats.u4AuthFailures);
    }

    FSAP_U8_2STR (&(Stats.u8IpTtlErrors), ac1Val);

    CliPrintf (CliHandle, " TTL Errors                     : %s\r\n", ac1Val);

    MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

    FSAP_U8_2STR (&(Stats.u8RcvdPriZeroPackets), ac1Val);

    CliPrintf (CliHandle, " Zero Priority Packets Received : %s\r\n", ac1Val);

    MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

    FSAP_U8_2STR (&(Stats.u8SentPriZeroPackets), ac1Val);

    CliPrintf (CliHandle, " Zero Priority Packets Sent     : %s\r\n", ac1Val);

    MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

    FSAP_U8_2STR (&(Stats.u8RcvdInvalidTypePackets), ac1Val);

    CliPrintf (CliHandle, " Invalid Type Packets Received  : %s\r\n", ac1Val);

    MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

    FSAP_U8_2STR (&(Stats.u8AddressListErrors), ac1Val);

    CliPrintf (CliHandle, " Address List Errors            : %s\r\n", ac1Val);

    MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

    if (i4Version == VRRP_VERSION_2)
    {
        CliPrintf (CliHandle,
                   " Invalid Authentication Type    : %d\r\n",
                   Stats.u4InvalidAuthType);
        CliPrintf (CliHandle,
                   " Authentication Type Mismatch   : %d\r\n",
                   Stats.u4AuthTypeMismatch);
    }

    FSAP_U8_2STR (&(Stats.u8PacketLengthErrors), ac1Val);

    CliPrintf (CliHandle, " Packet Length Errors           : %s\r\n", ac1Val);

    MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

    if ((i4Version == VRRP_VERSION_2_3) || (i4Version == VRRP_VERSION_3))
    {
        FSAP_U8_2STR (&(Stats.u8TxedAdvertisments), ac1Val);

        CliPrintf (CliHandle,
                   " V3 Advertisements Sent         : %s\r\n", ac1Val);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

        FSAP_U8_2STR (&(Stats.u8TxedV2Advertisments), ac1Val);

        CliPrintf (CliHandle,
                   " V2 Advertisements Sent         : %s\r\n", ac1Val);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

        FSAP_U8_2STR (&(Stats.u8V2AdvtIgnored), ac1Val);

        CliPrintf (CliHandle,
                   " V2 Advertisements Ignored      : %s\r\n", ac1Val);

        MEMSET ((UINT1 *) &ac1Val, 0, VRRP_U8_STR_LENGTH);

        CliPrintf (CliHandle,
                   " New Master Reason              : %s\r\n",
                   pac1NewMasterReason[Stats.u1NewMasterReason]);

        CliPrintf (CliHandle,
                   " Last Protocol Error            : %s\r\n",
                   pac1LastProtoError[Stats.u1ProtoErrReason]);
    }

    CliPrintf (CliHandle,
               " VrrpSemFailCount                               : %d\r\n",
               Stats.u4VrrpSemFail);
    CliPrintf (CliHandle,
               " VrrpSendNDNeighborAdvtFailCount                : %d\r\n",
               Stats.u4VrrpSendNDNeighborAdvtFail);
    CliPrintf (CliHandle,
               " VrrpSendPacketToIpv4FailCount                  : %d\r\n",
               Stats.u4VrrpSendPacketToIpv4Fail);
    CliPrintf (CliHandle,
               " VrrpSendPacketToIpv6FailCount                  : %d\r\n",
               Stats.u4VrrpSendPacketToIpv6Fail);
    CliPrintf (CliHandle,
               " VrrpGetIpv4PortFromOperIndexFailCount          : %d\r\n",
               Stats.u4VrrpGetIpv4PortFromOperIndexFail);
    CliPrintf (CliHandle,
               " VrrpGetIpv6PortFromOperIndexFailCount          : %d\r\n",
               Stats.u4VrrpGetIpv6PortFromOperIndexFail);
    CliPrintf (CliHandle,
               " VrrpSendGratArpFailCount                       : %d\r\n",
               Stats.u4VrrpSendGratArpFail);
    CliPrintf (CliHandle,
               " EthRegisterMacAddrFailCount                    : %d\r\n",
               Stats.u4EthRegisterMacAddrFail);
    CliPrintf (CliHandle,
               " VrrpNetIpv4DeleteIntfFailCount                 : %d\r\n",
               Stats.u4VrrpNetIpv4DeleteIntfFail);
    CliPrintf (CliHandle,
               " VrrpNetIpv6DeleteIntfFailCount                 : %d\r\n",
               Stats.u4VrrpNetIpv6DeleteIntfFail);
    CliPrintf (CliHandle,
               " VrrpIpifJoinMcastGroupFailCount                : %d\r\n",
               Stats.u4VrrpIpifJoinMcastGroupFail);
    CliPrintf (CliHandle,
               " VrrpIpifLeaveMcastGroupFailCount               : %d\r\n",
               Stats.u4VrrpIpifLeaveMcastGroupFail);
    CliPrintf (CliHandle,
               " VrrpSocketFailCount                            : %d\r\n",
               Stats.u4VrrpSocketFail);
    CliPrintf (CliHandle,
               " PktProcessValidateAdverPacketFailCount         : %d\r\n",
               Stats.u4PktProcessValidateAdverPacketFail);
    CliPrintf (CliHandle,
               " PktProcessValidateAdverV2PacketFailCount       : %d\r\n",
               Stats.u4PktProcessValidateAdverV2PacketFail);
    CliPrintf (CliHandle,
               " PktProcessValidateAdverV3PacketFailCount       : %d\r\n",
               Stats.u4PktProcessValidateAdverV3PacketFail);
    CliPrintf (CliHandle,
               " PktProcessTransmitV2PacketFailCount            : %d\r\n",
               Stats.u4PktProcessTransmitV2PacketFail);
    CliPrintf (CliHandle,
               " PktProcessTransmitV3PacketFailCount            : %d\r\n",
               Stats.u4PktProcessTransmitV3PacketFail);

    CliPrintf (CliHandle, "\r\n");

    return;
}

/**************************************************************************
* Function Name :  VrrpCliShowTrack                                       *
*                                                                         *
* Description   :  This function displays track information.              *
*                                                                         *
* Input (s)     :  CliHandle   - CLI Handle                               *
*                                                                         *
* Output (s)    :  Formated output message.                               *
*                                                                         *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliShowTrack (tCliHandle CliHandle)
{
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4GroupIndex = 0;
    UINT4               u4NextGroupIndex = 0;
    UINT4               u4Val = 0;
    UINT4               u4LastGroupIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4Version = 0;
    INT4                i4RetVal = 0;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    while (VRRP_ONE)
    {
        if (i4Version == VRRP_VERSION_2)
        {
            i4RetVal = nmhGetNextIndexFsVrrpOperTrackGroupIfTable (u4GroupIndex,
                                                                   &u4NextGroupIndex,
                                                                   i4IfIndex,
                                                                   &i4NextIfIndex);
        }
        else
        {
            i4RetVal =
                nmhGetNextIndexFsVrrpv3OperationsTrackGroupIfTable
                (u4GroupIndex, &u4NextGroupIndex, i4IfIndex, &i4NextIfIndex);
        }

        if (i4RetVal == SNMP_FAILURE)
        {
            break;
        }

        u4GroupIndex = u4NextGroupIndex;
        i4IfIndex = i4NextIfIndex;

        if (u4LastGroupIndex != u4GroupIndex)
        {
            u4LastGroupIndex = u4GroupIndex;

            CliPrintf (CliHandle,
                       "Track Group Information for Group %u\r\n",
                       u4GroupIndex);

            CliPrintf (CliHandle, "------------------------------------\r\n");

            if (i4Version == VRRP_VERSION_2)
            {
                nmhGetFsVrrpOperTrackedGroupTrackedLinks (u4GroupIndex, &u4Val);
            }
            else
            {
                nmhGetFsVrrpv3OperationsTrackedGroupTrackedLinks (u4GroupIndex,
                                                                  &u4Val);
            }

            CliPrintf (CliHandle,
                       "Number of Links Required to go down for state transition: %d\r\n",
                       u4Val);

            CliPrintf (CliHandle, "Interfaces Tracked are\r\n");

            CliPrintf (CliHandle, "----------------------\r\n");
        }

        CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) &au1IfName[0]);

        CliPrintf (CliHandle, "%s\r\n", au1IfName);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VrrpShowRunningConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function shows the current configuration of   */
/*                        Vrrp module.                                       */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
VrrpShowRunningConfig (tCliHandle CliHandle)
{
    tVrrpOperTable      OperTable;
    tSNMP_OCTET_STRING_TYPE AssoIp;
    tVrrpIPvXAddr       AssoIpAddr;
    UINT1               au1AssoIp[IPVX_IPV6_ADDR_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4GroupIndex = 0;
    UINT4               u4NextGroupIndex = 0;
    UINT4               u4Val = 0;
    UINT4               u4NextIp = 0;
    INT4                i4Val = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4VrId = 0;
    INT4                i4NextVrId = 0;
    INT4                i4NextIndex = 0;
    INT4                i4AddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4Version = 0;
    INT4                i4TempIfIndex = 0;
    INT4                i4TempVrId = 0;
    INT4                i4TempAddrType = 0;
    INT4                i4RetVal = 0;
    INT4                i4RetVal1 = 0;
    BOOL1               bLoopVal = FALSE;

    MEMSET (&OperTable, 0, sizeof (tVrrpOperTable));
    MEMSET (&AssoIp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1AssoIp, 0, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    AssoIp.pu1_OctetList = au1AssoIp;
    AssoIp.i4_Length = IPVX_IPV6_ADDR_LEN;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "!\r\n");

    while (VRRP_ONE)
    {
        if (i4Version == VRRP_VERSION_2)
        {
            i4RetVal = nmhGetNextIndexFsVrrpOperTrackGroupIfTable (u4GroupIndex,
                                                                   &u4NextGroupIndex,
                                                                   i4IfIndex,
                                                                   &i4NextIfIndex);
        }
        else
        {
            i4RetVal =
                nmhGetNextIndexFsVrrpv3OperationsTrackGroupIfTable
                (u4GroupIndex, &u4NextGroupIndex, i4IfIndex, &i4NextIfIndex);
        }

        if (i4RetVal == SNMP_FAILURE)
        {
            break;
        }

        u4GroupIndex = u4NextGroupIndex;
        i4IfIndex = i4NextIfIndex;

        if (CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) &au1IfName[0])
            == CFA_FAILURE)
        {
            /*ignore failure */
        }
        else
        {
            CliPrintf (CliHandle,
                       "track %u interface %s\r\n", u4GroupIndex, au1IfName);
        }
    }

    u4GroupIndex = 0;
    u4NextGroupIndex = 0;
    i4IfIndex = 0;
    i4NextIfIndex = 0;

    while (VRRP_ONE)
    {
        if (i4Version == VRRP_VERSION_2)
        {
            i4RetVal = nmhGetNextIndexFsVrrpOperTrackGroupTable (u4GroupIndex,
                                                                 &u4NextGroupIndex);
        }
        else
        {
            i4RetVal =
                nmhGetNextIndexFsVrrpv3OperationsTrackGroupTable (u4GroupIndex,
                                                                  &u4NextGroupIndex);
        }

        if (i4RetVal == SNMP_FAILURE)
        {
            break;
        }

        u4GroupIndex = u4NextGroupIndex;

        if (i4Version == VRRP_VERSION_2)
        {
            nmhGetFsVrrpOperTrackedGroupTrackedLinks (u4GroupIndex, &u4Val);
        }
        else
        {
            nmhGetFsVrrpv3OperationsTrackedGroupTrackedLinks (u4GroupIndex,
                                                              &u4Val);
        }

        if (u4Val != 0)
        {
            CliPrintf (CliHandle, "track %u links %d\r\n", u4GroupIndex, u4Val);
        }
    }

    if (i4Version == VRRP_VERSION_2)
    {
        nmhGetFsVrrpStatus (&i4Val);
    }
    else
    {
        nmhGetFsVrrpv3Status (&i4Val);
    }

    if (i4Val == VRRP_DISABLE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "router vrrp\r\n");

    if (i4Version == VRRP_VERSION_2)
    {
        nmhGetFsVrrpAuthDeprecate (&i4Val);

        if (i4Val != VRRP_AUTHDEPRECATE_ENABLE)
        {
            CliPrintf (CliHandle, " auth-deprecate disable\r\n");
        }
    }
    else if (i4Version == VRRP_VERSION_2_3)
    {
        CliPrintf (CliHandle, " vrrp version v2-v3\r\n");
    }
    else if (i4Version == VRRP_VERSION_3)
    {
        CliPrintf (CliHandle, " vrrp version v3\r\n");
    }

    if (i4Version == VRRP_VERSION_2)
    {
        i4RetVal = nmhGetFirstIndexVrrpOperTable (&i4IfIndex, &i4VrId);

        i4AddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        i4RetVal = nmhGetFirstIndexVrrpv3OperationsTable (&i4IfIndex,
                                                          &i4VrId, &i4AddrType);
    }

    while (i4RetVal == SNMP_SUCCESS)
    {
        if (VrrpCmnGetAllOperTable (i4Version, i4IfIndex, i4VrId,
                                    i4AddrType, &OperTable) == VRRP_NOT_OK)
        {
            break;
        }

        MEMSET (&au1IfName[0], 0, CFA_MAX_PORT_NAME_LENGTH);

        CfaCliConfGetIfName ((UINT4) i4IfIndex, (INT1 *) &au1IfName[0]);

        if ((bLoopVal == TRUE) && (i4IfIndex != i4NextIndex))
        {
            CliPrintf (CliHandle, " exit\r\n");
        }

        if (i4IfIndex != i4NextIndex)
        {
            CliPrintf (CliHandle, " interface %s\r\n", au1IfName);
            i4NextIndex = i4IfIndex;
        }

        bLoopVal = TRUE;

        if (VRRP_IPVX_COMPARE (OperTable.PrimaryIpAddr, gVrrpZeroIpAddr) != 0)
        {
            if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
            {
                CliPrintf (CliHandle, "  vrrp %d ipv4 %s\r\n",
                           i4VrId,
                           VRRP_IPVX_PRINT_ADDR (OperTable.PrimaryIpAddr,
                                                 i4AddrType));
            }
            else
            {
                CliPrintf (CliHandle, "  vrrp %d ipv6 %s\r\n",
                           i4VrId,
                           VRRP_IPVX_PRINT_ADDR (OperTable.PrimaryIpAddr,
                                                 i4AddrType));
            }
        }

        if ((i4Version == VRRP_VERSION_2) || (i4Version == VRRP_VERSION_2_3))
        {
            i4RetVal1 = nmhGetNextIndexVrrpAssoIpAddrTable (i4IfIndex,
                                                            &i4NextIfIndex,
                                                            i4VrId,
                                                            &i4NextVrId, 0,
                                                            &u4NextIp);

            VRRP_IPVX_COPY_TO_IPVX (AssoIpAddr, u4NextIp);
            i4NextAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
        }
        else
        {
            i4RetVal1 = nmhGetNextIndexVrrpv3AssociatedIpAddrTable (i4IfIndex,
                                                                    &i4NextIfIndex,
                                                                    i4VrId,
                                                                    &i4NextVrId,
                                                                    i4AddrType,
                                                                    &i4NextAddrType,
                                                                    &AssoIp,
                                                                    &AssoIp);

            VRRP_IPVX_ADDR_INIT_IPVX (AssoIpAddr, (UINT1) i4NextAddrType,
                                      AssoIp.pu1_OctetList);

            OperTable.i4AdvtInterval = (OperTable.i4AdvtInterval * 10);
        }

        i4TempIfIndex = i4IfIndex;
        i4TempVrId = i4VrId;
        i4TempAddrType = i4AddrType;

        while ((i4RetVal1 == SNMP_SUCCESS) &&
               (i4IfIndex == i4NextIfIndex) && (i4VrId == i4NextVrId) &&
               (i4AddrType == i4NextAddrType))
        {
            if (VRRP_IPVX_COMPARE (OperTable.PrimaryIpAddr, AssoIpAddr) != 0)
            {
                if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
                {
                    CliPrintf (CliHandle, "  vrrp %d ipv4 %s secondary\r\n",
                               i4VrId,
                               VRRP_IPVX_PRINT_ADDR (AssoIpAddr, i4AddrType));
                }
                else
                {
                    CliPrintf (CliHandle, "  vrrp %d ipv6 %s secondary\r\n",
                               i4VrId,
                               VRRP_IPVX_PRINT_ADDR (AssoIpAddr, i4AddrType));
                }
            }

            if (i4Version == VRRP_VERSION_2)
            {
                i4RetVal1 = nmhGetNextIndexVrrpAssoIpAddrTable (i4NextIfIndex,
                                                                &i4NextIfIndex,
                                                                i4NextVrId,
                                                                &i4NextVrId,
                                                                u4NextIp,
                                                                &u4NextIp);

                VRRP_IPVX_COPY_TO_IPVX (AssoIpAddr, u4NextIp);
                i4NextAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
            }
            else
            {
                i4RetVal1 =
                    nmhGetNextIndexVrrpv3AssociatedIpAddrTable (i4NextIfIndex,
                                                                &i4NextIfIndex,
                                                                i4NextVrId,
                                                                &i4NextVrId,
                                                                i4NextAddrType,
                                                                &i4NextAddrType,
                                                                &AssoIp,
                                                                &AssoIp);

                VRRP_IPVX_ADDR_INIT_IPVX (AssoIpAddr, (UINT1) i4NextAddrType,
                                          AssoIp.pu1_OctetList);
            }
        }

        i4IfIndex = i4TempIfIndex;
        i4VrId = i4TempVrId;
        i4AddrType = i4TempAddrType;

        MEMSET (AssoIp.pu1_OctetList, 0, IPVX_IPV6_ADDR_LEN);

        if (i4AddrType == VRRP_IPVX_ADDR_FMLY_IPV4)
        {
            if (OperTable.i4Priority != DEFAULT_PRIORITY)
            {
                CliPrintf (CliHandle, "  vrrp %d priority %d\r\n",
                           i4VrId, OperTable.i4Priority);
            }

            if (OperTable.i4PreemptMode != VRRP_ENABLE)
            {
                CliPrintf (CliHandle, "  no vrrp %d preempt\r\n", i4VrId);
            }

            if ((OperTable.i4AdvtInterval % 1000) == 0)
            {
                if ((OperTable.i4AdvtInterval / 1000) != ADVERTISEMENTINTL)
                {
                    CliPrintf (CliHandle,
                               "  vrrp %d timer %d\r\n",
                               i4VrId, OperTable.i4AdvtInterval / 1000);
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "  vrrp %d timer msec %d\r\n",
                           i4VrId, OperTable.i4AdvtInterval);
            }

            if (OperTable.u1AcceptMode != VRRP_DISABLE)
            {
                CliPrintf (CliHandle,
                           "  vrrp %d accept-mode enable\r\n", i4VrId);
            }

            if (OperTable.u4TrackGroupId != 0)
            {
                CliPrintf (CliHandle,
                           "  vrrp %d track %u decrement %d\r\n",
                           i4VrId, OperTable.u4TrackGroupId,
                           OperTable.u1DecrementedPriority);
            }
        }
        else
        {
            if (OperTable.i4Priority != DEFAULT_PRIORITY)
            {
                CliPrintf (CliHandle, "  vrrp %d ipv6 priority %d\r\n",
                           i4VrId, OperTable.i4Priority);
            }

            if (OperTable.i4PreemptMode != VRRP_ENABLE)
            {
                CliPrintf (CliHandle, "  no vrrp %d ipv6 preempt\r\n", i4VrId);
            }

            if ((OperTable.i4AdvtInterval % 1000) == 0)
            {
                if ((OperTable.i4AdvtInterval / 1000) != ADVERTISEMENTINTL)
                {
                    CliPrintf (CliHandle,
                               "  vrrp %d ipv6 timer %d\r\n",
                               i4VrId, OperTable.i4AdvtInterval / 1000);
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "  vrrp %d ipv6 timer msec %d\r\n",
                           i4VrId, OperTable.i4AdvtInterval);
            }

            if (OperTable.u1AcceptMode != VRRP_DISABLE)
            {
                CliPrintf (CliHandle,
                           "  vrrp %d ipv6 accept-mode enable\r\n", i4VrId);
            }

            if (OperTable.u4TrackGroupId != 0)
            {
                CliPrintf (CliHandle,
                           "  vrrp %d ipv6 track %u decrement %d\r\n",
                           i4VrId, OperTable.u4TrackGroupId,
                           OperTable.u1DecrementedPriority);
            }
        }

        if ((OperTable.i4AuthType == SIMPLE_TEXT_AUTHENTICATION_MIB_VAL) &&
            ((i4Version == VRRP_VERSION_2) || (i4Version == VRRP_VERSION_2_3)))
        {
            CliPrintf (CliHandle,
                       "  vrrp %d text-authentication\r\n",
                       i4VrId, OperTable.i4AuthType);
        }

        if (i4Version == VRRP_VERSION_2)
        {
            i4RetVal = nmhGetNextIndexVrrpOperTable (i4IfIndex, &i4NextIfIndex,
                                                     i4VrId, &i4NextVrId);

            i4NextAddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
        }
        else
        {
            i4RetVal =
                nmhGetNextIndexVrrpv3OperationsTable (i4IfIndex, &i4NextIfIndex,
                                                      i4VrId, &i4NextVrId,
                                                      i4AddrType,
                                                      &i4NextAddrType);
        }

        i4IfIndex = i4NextIfIndex;
        i4VrId = i4NextVrId;
        i4AddrType = i4NextAddrType;
    }

    CliPrintf (CliHandle, "end\r\n");
    CliPrintf (CliHandle, "!\r\n");

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : VrrpGetVrrpIfCfgPrompt                                 */
/*                                                                            */
/* Description       : This function validates the given pi1ModeName          */
/*                     and returns the prompt in pi1DispStr if valid,         */
/*                                                                            */
/* Input Parameters  : pi1ModeName - Mode Name to validate                    */
/*                     pi1DispStr - Display string to be returned.            */
/*                                                                            */
/* Output Parameters : pi1DispStr - Display string.                           */
/*                                                                            */
/* Return Value      : TRUE/FALSE                                             */
/******************************************************************************/
INT1
VrrpGetVrrpIfCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;

    if (!(pi1ModeName) || !(pi1DispStr))
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_VRRP_IF);
    if (STRNCMP (pi1ModeName, CLI_VRRP_IF, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;
    STRNCPY (pi1DispStr, "(config-vrrp-if)#", STRLEN ("(config-vrrp-if)#"));
    pi1DispStr[STRLEN ("(config-vrrp-if)#")] = '\0';

    return TRUE;
}

/******************************************************************************/
/* Function Name     : VrrpGetVrrpCfgPrompt                                   */
/*                                                                            */
/* Description       : This function validates the given pi1ModeName          */
/*                     and returns the prompt in pi1DispStr if valid,         */
/*                                                                            */
/* Input Parameters  : pi1ModeName - Mode Name to validate                    */
/*                     pi1DispStr - Display string to be returned.            */
/*                                                                            */
/* Output Parameters : pi1DispStr - Display string.                           */
/*                                                                            */
/* Return Value      : TRUE/FALSE                                             */
/******************************************************************************/
INT1
VrrpGetVrrpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;

    if (!(pi1ModeName) || !(pi1DispStr))
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_VRRP_IF);
    if (STRNCMP (pi1ModeName, CLI_VRRP, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    STRNCPY (pi1DispStr, "(config-vrrp)#", STRLEN ("(config-vrrp)#"));
    pi1DispStr[STRLEN ("(config-vrrp)#")] = '\0';

    return TRUE;
}

/*****************************************************************************/
/* Function Name : VrrpSetTraceValue                                          */
/* Description   : Sets the Trace Value                                      */
/* Input(s)      : i4TrapLevel - TrapLevel                                   */
/*                 u1LoggingCmd - Set or Reset                               */
/* Return Values : CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/

INT4
VrrpSetTraceValue (INT4 i4TrapLevel, UINT1 u1LoggingCmd)
{
    UINT4               u4Trace = 0;
    if (i4TrapLevel == SYSLOG_CRITICAL_LEVEL)
    {
        u4Trace = VRRP_CLI_TRC_STATE | VRRP_TRC_ALL_FAILURES;
    }
    else if (i4TrapLevel == SYSLOG_INFO_LEVEL)
    {
        u4Trace = VRRP_CLI_TRC_ALL;
    }
    if (VrrpSetTraceLevel (1, u1LoggingCmd, (INT4) u4Trace) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function Name : VrrpSetTraceLevel                                     */
/*                                                                         */
/*   Description   :To Set VRRP Trace level                                */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                    2. CLI_ENABLE / CLI_DISABLE - to set / reset the     */
/*                       trace level                                       */
/*                    3. u4TraceLevel - trace level                        */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
INT4
VrrpSetTraceLevel (tCliHandle CliHandle, UINT1 u1TraceFlag, INT4 i4TraceLevel)
{
    UINT4               u4ErrCode = 0;
    INT4                i4VrrpTrace = 0;
    INT4                i4Version = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% No Version Configured\r\n");

        return CLI_FAILURE;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        i1RetVal = nmhGetFsVrrpTraceOption (&i4VrrpTrace);
    }
    else
    {
        i1RetVal = nmhGetFsVrrpv3TraceOption (&i4VrrpTrace);
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Incompatible Version\r\n");

        return CLI_FAILURE;
    }

    if (u1TraceFlag == CLI_DISABLE)
    {
        /* for reset of all trace command i4TraceLevel will have all bits sets
         * and doing a complement will reset all bit positions.
         */
        i4TraceLevel = i4VrrpTrace & (~i4TraceLevel);
    }
    else
    {
        i4TraceLevel = i4VrrpTrace | i4TraceLevel;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        if (nmhTestv2FsVrrpTraceOption (&u4ErrCode, i4TraceLevel) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Wrong Trace Option\r\n");

            return CLI_FAILURE;
        }

        if (nmhSetFsVrrpTraceOption (i4TraceLevel) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Wrong Trace Option\r\n");

            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsVrrpv3TraceOption (&u4ErrCode, i4TraceLevel) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Wrong Trace Option\r\n");

            return CLI_FAILURE;
        }

        if (nmhSetFsVrrpv3TraceOption (i4TraceLevel) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Wrong Trace Option\r\n");

            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************/
/*   Function Name : IssVrrpShowDebugging                                  */
/*                                                                         */
/*   Description   : This function prints the DHCP relay  debug level      */
/*                                                                         */
/*   Input(s)      : CliHandle - CLI context ID                            */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/
VOID
IssVrrpShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = 0;

    nmhGetFsVrrpTraceOption (&i4DbgLevel);

    if (i4DbgLevel != 0)
    {
        CliPrintf (CliHandle, "\rVRRP :");

        if ((i4DbgLevel & VRRP_TRC_ALL) == VRRP_TRC_ALL)
        {
            CliPrintf (CliHandle, "\r\n  Vrrp all debugging is on");
            CliPrintf (CliHandle, "\r\n");
            return;
        }
        if ((i4DbgLevel & VRRP_TRC_PKT) != 0)
        {
            CliPrintf (CliHandle, "\r\n  Vrrp pkt debugging is on");
        }
        if ((i4DbgLevel & VRRP_TRC_EVENTS) != 0)
        {
            CliPrintf (CliHandle, "\r\n  Vrrp events debugging is on");
        }
        if ((i4DbgLevel & VRRP_TRC_INIT) != 0)
        {
            CliPrintf (CliHandle, "\r\n  Vrrp init debugging is on");
        }
        if ((i4DbgLevel & VRRP_TRC_TIMERS) != 0)
        {
            CliPrintf (CliHandle, "\r\n  Vrrp timers debugging is on");
        }
        if ((i4DbgLevel & VRRP_TRC_MEMORY) != 0)
        {
            CliPrintf (CliHandle, "\r\n Vrrp Memory debugging is on");
        }
        if ((i4DbgLevel & VRRP_TRC_BUFFER) != 0)
        {
            CliPrintf (CliHandle, "\r\n Vrrp Buffer debugging is on");
        }
        if ((i4DbgLevel & VRRP_TRC_VERSION_2) != 0)
        {
            CliPrintf (CliHandle, "\r\n Vrrp Version 2 debugging is on");
        }
        if ((i4DbgLevel & VRRP_TRC_VERSION_3) != 0)
        {
            CliPrintf (CliHandle, "\r\n Vrrp Version 3 debugging is on");
        }
        if ((i4DbgLevel & VRRP_TRC_ALL_FAILURES) != 0)
        {
            CliPrintf (CliHandle, "\r\n  Vrrp failures debugging is on");
        }
        CliPrintf (CliHandle, "\r\n");
    }

    return;

}

/***************************************************************************
 * Function Name :  VrrpCliSetVersion
 * Description   :  This function sets the version of VRRP through CLI
 * Input (s)     :  CliHandle - CLI Handle
 *                  u4Version - Version to be set
 * Output (s)    :  None
 * Returns       :  CLI_FAILURE / CLI_SUCCESS
 ***************************************************************************/
INT4
VrrpCliSetVersion (tCliHandle CliHandle, UINT4 u4version)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsVrrpVersionSupported (&u4ErrorCode,
                                         (INT4) u4version) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVrrpVersionSupported (u4version) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliSetIpv4Address                                  *
*                                                                         *
* Description   :  This function sets the Primary IP Address              *
*                                                                         *
* Input (s)     :  i4IfIndex    - The interface index.                    *
*                  i4VrId       - Virtual router Identification           *
*                  u4IpAddr     - Primary IP Address.                     *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetIpv4Address (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4VrId,
                       UINT4 u4IpAddr, UINT4 u4Option)
{
    UINT1               au1IpAddr[IPVX_IPV4_ADDR_LEN];
    UINT4               u4TempIpAddr = 0;
    INT4                i4AddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
    INT4                i4Version = 0;
    INT4                i4RetVal = 0;

    MEMSET (au1IpAddr, 0, IPVX_IPV4_ADDR_LEN);

    if (u4IpAddr == 0)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_IP);

        return CLI_FAILURE;
    }

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return CLI_FAILURE;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        i4RetVal = VrrpCmnSetV2IpAddr (i4IfIndex, i4VrId, i4AddrType, u4IpAddr,
                                       u4Option);
    }
    else
    {
        u4TempIpAddr = OSIX_HTONL (u4IpAddr);
        MEMCPY (au1IpAddr, (UINT1 *) &u4TempIpAddr, IPVX_IPV4_ADDR_LEN);

        i4RetVal = VrrpCmnSetV3IpAddr (i4IfIndex, i4VrId, i4AddrType, au1IpAddr,
                                       u4Option);
    }

    if (i4RetVal == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliDelIpv4Address                                  *
*                                                                         *
* Description   :  This function deletes the Ipv4 Address                 *
*                                                                         *
* Input (s)     :  i4IfIndex    - The interface index.                    *
*                  i4VrId       - Virtual router Identification           *
*                  u4IpAddr     - IPv4 Address.                           *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliDelIpv4Address (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4VrId,
                       UINT4 u4IpAddr, UINT4 u4Option)
{
    UINT1               au1IpAddr[IPVX_IPV4_ADDR_LEN];
    UINT4               u4TempIpAddr = 0;
    INT4                i4AddrType = VRRP_IPVX_ADDR_FMLY_IPV4;
    INT4                i4Version = 0;
    INT4                i4RetVal = 0;

    MEMSET (au1IpAddr, 0, IPVX_IPV4_ADDR_LEN);

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return CLI_FAILURE;
    }

    if (u4Option == VRRP_CLI_DELETE_VRID)
    {
        if (VrrpCmnDeleteVrId (i4Version, i4IfIndex, i4VrId, i4AddrType)
            == VRRP_NOT_OK)
        {
            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    if (u4IpAddr == 0)
    {
        CLI_SET_ERR (CLI_VRRP_INVALID_IP);

        return CLI_FAILURE;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        i4RetVal = VrrpCmnDelV2IpAddr (i4IfIndex, i4VrId, i4AddrType, u4IpAddr,
                                       u4Option);
    }
    else
    {
        u4TempIpAddr = OSIX_HTONL (u4IpAddr);
        MEMCPY (au1IpAddr, (UINT1 *) &u4TempIpAddr, IPVX_IPV4_ADDR_LEN);

        i4RetVal = VrrpCmnDelV3IpAddr (i4IfIndex, i4VrId, i4AddrType, au1IpAddr,
                                       u4Option);
    }

    if (i4RetVal == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliSetIpv6Address                                  *
*                                                                         *
* Description   :  This function sets the IPv6 Address                    *
*                                                                         *
* Input (s)     :  i4IfIndex    - The interface index.                    *
*                  i4VrId       - Virtual router Identification           *
*                  pu1IpAddr    - IPv6 Address.                           *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliSetIpv6Address (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4VrId,
                       UINT1 *pu1Addr, UINT4 u4Option)
{
    INT4                i4Version = 0;
    INT4                i4AddrType = VRRP_IPVX_ADDR_FMLY_IPV6;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return CLI_FAILURE;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        CLI_SET_ERR (CLI_VRRP_IPV6_NO_SUPPORT);

        return CLI_FAILURE;
    }

    if (VrrpCmnSetV3IpAddr (i4IfIndex, i4VrId, i4AddrType, pu1Addr, u4Option)
        == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/**************************************************************************
* Function Name :  VrrpCliDelIpv6Address                                  *
*                                                                         *
* Description   :  This function deletes the IPv6 Address                 *
*                                                                         *
* Input (s)     :  i4IfIndex    - The interface index.                    *
*                  i4VrId       - Virtual router Identification           *
*                  pu1IpAddr    - IPv6 Address.                           *
* Returns       :  None.                                                  *
***************************************************************************/
INT4
VrrpCliDelIpv6Address (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4VrId,
                       UINT1 *pu1Addr, UINT4 u4Option)
{
    INT4                i4Version = 0;
    INT4                i4AddrType = VRRP_IPVX_ADDR_FMLY_IPV6;

    if (nmhGetFsVrrpVersionSupported (&i4Version) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VRRP_VERSION_FETCH_ERROR);

        return CLI_FAILURE;
    }

    if (i4Version == VRRP_VERSION_2)
    {
        CLI_SET_ERR (CLI_VRRP_IPV6_NO_SUPPORT);

        return CLI_FAILURE;
    }

    if (u4Option == VRRP_CLI_DELETE_VRID)
    {
        if (VrrpCmnDeleteVrId (i4Version, i4IfIndex, i4VrId, i4AddrType)
            == VRRP_NOT_OK)
        {
            return CLI_FAILURE;
        }

        return CLI_SUCCESS;
    }

    if (VrrpCmnDelV3IpAddr (i4IfIndex, i4VrId, i4AddrType, pu1Addr, u4Option)
        == VRRP_NOT_OK)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : VrrpCliConvertTimetoString
 * Description   : This routine will display the time string in 00:00:00:00
 *                 format.
 * Input(s)      :  u4Time               - Time 
 * Output(s)     :  pi1Time              - Display Time String
 * Return(s)     : True /False
 *******************************************************************************/
VOID
VrrpCliConvertTimetoString (UINT4 u4Time, INT1 *pi1Time)
{
    UINT4               u4Hrs;
    UINT4               u4Mins;
    UINT4               u4Secs;

    MEMSET (pi1Time, 0, VRRP_CLI_TIME_STRING);

    if (u4Time == 0)
    {

        SNPRINTF ((CHR1 *) pi1Time, VRRP_CLI_TIME_STRING, "00:00:00");
        return;
    }

    u4Time = u4Time / 100;
    u4Secs = u4Time % 60;
    u4Time = u4Time / 60;
    u4Mins = u4Time % 60;
    u4Time = u4Time / 60;
    u4Hrs = u4Time % 24;

    SNPRINTF ((CHR1 *) pi1Time, VRRP_CLI_TIME_STRING,
              "%02d:%02d:%02d", u4Hrs, u4Mins, u4Secs);
    return;
}

#endif
