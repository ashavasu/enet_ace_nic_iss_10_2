/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvr3wr.c,v 1.2 2014/03/08 13:40:06 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "lr.h" 
#include  "fssnmp.h" 
#include  "fsvr3lw.h"
#include  "fsvr3wr.h"
#include  "fsvr3db.h"
#include  "ip.h"
#include  "vrrp.h"


VOID RegisterFSVR3 ()
{
	SNMPRegisterMibWithLock (&fsvr3OID, &fsvr3Entry,
                             VrrpLock, VrrpUnLock, SNMP_MSR_TGR_TRUE);

	SNMPAddSysorEntry (&fsvr3OID, (const UINT1 *) "fsvrrp3");
}

VOID UnRegisterFSVR3 ()
{
	SNMPUnRegisterMib (&fsvr3OID, &fsvr3Entry);
	SNMPDelSysorEntry (&fsvr3OID, (const UINT1 *) "fsvrrp3");
}

INT4 FsVrrpVersionSupportedGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsVrrpVersionSupported(&(pMultiData->i4_SLongValue)));
}
INT4 FsVrrpv3TraceOptionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsVrrpv3TraceOption(&(pMultiData->i4_SLongValue)));
}
INT4 FsVrrpv3NotificationCntlGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsVrrpv3NotificationCntl(&(pMultiData->i4_SLongValue)));
}
INT4 FsVrrpv3StatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsVrrpv3Status(&(pMultiData->i4_SLongValue)));
}
INT4 FsVrrpv3MaxOperEntriesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsVrrpv3MaxOperEntries(&(pMultiData->i4_SLongValue)));
}
INT4 FsVrrpv3MaxAssociatedIpEntriesGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsVrrpv3MaxAssociatedIpEntries(&(pMultiData->i4_SLongValue)));
}
INT4 FsVrrpVersionSupportedSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsVrrpVersionSupported(pMultiData->i4_SLongValue));
}


INT4 FsVrrpv3TraceOptionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsVrrpv3TraceOption(pMultiData->i4_SLongValue));
}


INT4 FsVrrpv3NotificationCntlSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsVrrpv3NotificationCntl(pMultiData->i4_SLongValue));
}


INT4 FsVrrpv3StatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsVrrpv3Status(pMultiData->i4_SLongValue));
}


INT4 FsVrrpVersionSupportedTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsVrrpVersionSupported(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsVrrpv3TraceOptionTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsVrrpv3TraceOption(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsVrrpv3NotificationCntlTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsVrrpv3NotificationCntl(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsVrrpv3StatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsVrrpv3Status(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsVrrpVersionSupportedDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsVrrpVersionSupported(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsVrrpv3TraceOptionDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsVrrpv3TraceOption(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsVrrpv3NotificationCntlDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsVrrpv3NotificationCntl(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsVrrpv3StatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsVrrpv3Status(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsVrrpv3OperationsTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsVrrpv3OperationsTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsVrrpv3OperationsTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsVrrpv3OperationsTrackGroupIdGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3OperationsTrackGroupId(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsVrrpv3OperationsDecrementPriorityGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3OperationsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3OperationsDecrementPriority(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsVrrpv3OperationsTrackGroupIdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsVrrpv3OperationsTrackGroupId(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsVrrpv3OperationsDecrementPrioritySet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsVrrpv3OperationsDecrementPriority(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsVrrpv3OperationsTrackGroupIdTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsVrrpv3OperationsTrackGroupId(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsVrrpv3OperationsDecrementPriorityTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsVrrpv3OperationsDecrementPriority(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsVrrpv3OperationsTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsVrrpv3OperationsTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsVrrpv3OperationsTrackGroupTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsVrrpv3OperationsTrackGroupTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsVrrpv3OperationsTrackGroupTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsVrrpv3OperationsTrackedGroupTrackedLinksGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3OperationsTrackGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3OperationsTrackedGroupTrackedLinks(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsVrrpv3OperationsTrackRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3OperationsTrackGroupTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3OperationsTrackRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsVrrpv3OperationsTrackedGroupTrackedLinksSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsVrrpv3OperationsTrackedGroupTrackedLinks(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsVrrpv3OperationsTrackRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsVrrpv3OperationsTrackRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsVrrpv3OperationsTrackedGroupTrackedLinksTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsVrrpv3OperationsTrackedGroupTrackedLinks(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsVrrpv3OperationsTrackRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsVrrpv3OperationsTrackRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsVrrpv3OperationsTrackGroupTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsVrrpv3OperationsTrackGroupTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsVrrpv3OperationsTrackGroupIfTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsVrrpv3OperationsTrackGroupIfTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsVrrpv3OperationsTrackGroupIfTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsVrrpv3OperationsTrackGroupIfRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3OperationsTrackGroupIfTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3OperationsTrackGroupIfRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsVrrpv3OperationsTrackGroupIfRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsVrrpv3OperationsTrackGroupIfRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsVrrpv3OperationsTrackGroupIfRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsVrrpv3OperationsTrackGroupIfRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsVrrpv3OperationsTrackGroupIfTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsVrrpv3OperationsTrackGroupIfTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsVrrpv3StatisticsTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsVrrpv3StatisticsTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsVrrpv3StatisticsTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue),
			pFirstMultiIndex->pIndex[1].i4_SLongValue,
			&(pNextMultiIndex->pIndex[1].i4_SLongValue),
			pFirstMultiIndex->pIndex[2].i4_SLongValue,
			&(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsVrrpv3StatisticsTxedAdvertisementsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3StatisticsTxedAdvertisements(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 FsVrrpv3StatisticsTxedV2AdvertisementsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3StatisticsTxedV2Advertisements(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 FsVrrpv3StatisticsV2AdvertiseIgnoredGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3StatisticsV2AdvertiseIgnored(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->u8_Counter64Value)));

}
INT4 FsVrrpv3StatisticsMasterAdverIntervalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3StatisticsMasterAdverInterval(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsVrrpv3StatisticsSkewTimeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3StatisticsSkewTime(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsVrrpv3StatisticsMasterDownIntervalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsVrrpv3StatisticsTable(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsVrrpv3StatisticsMasterDownInterval(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiIndex->pIndex[1].i4_SLongValue,
		pMultiIndex->pIndex[2].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
