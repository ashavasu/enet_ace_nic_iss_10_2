/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vrrp3lw.c,v 1.3 2017/12/16 11:57:11 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "lr.h"
#include  "fssnmp.h"
#include  "vrrpinc.h"

/* LOW LEVEL Routines for Table : Vrrpv3OperationsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVrrpv3OperationsTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVrrpv3OperationsTable (INT4 i4IfIndex,
                                               INT4 i4Vrrpv3OperationsVrId,
                                               INT4
                                               i4Vrrpv3OperationsInetAddrType)
{
    if ((i4IfIndex <= 0) ||
        (i4Vrrpv3OperationsVrId < VRRP_MIN_VRID) ||
        (i4Vrrpv3OperationsVrId > VRRP_MAX_VRID) ||
        (i4Vrrpv3OperationsInetAddrType < VRRP_IPVX_ADDR_FMLY_IPV4) ||
        (i4Vrrpv3OperationsInetAddrType > VRRP_IPVX_ADDR_FMLY_IPV6))
    {
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVrrpv3OperationsTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVrrpv3OperationsTable (INT4 *pi4IfIndex,
                                       INT4 *pi4Vrrpv3OperationsVrId,
                                       INT4 *pi4Vrrpv3OperationsInetAddrType)
{
    return (nmhGetNextIndexVrrpv3OperationsTable (0, pi4IfIndex,
                                                  0, pi4Vrrpv3OperationsVrId,
                                                  0,
                                                  pi4Vrrpv3OperationsInetAddrType));
}

/****************************************************************************
 Function    :  nmhGetNextIndexVrrpv3OperationsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                Vrrpv3OperationsVrId
                nextVrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                nextVrrpv3OperationsInetAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVrrpv3OperationsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                      INT4 i4Vrrpv3OperationsVrId,
                                      INT4 *pi4NextVrrpv3OperationsVrId,
                                      INT4 i4Vrrpv3OperationsInetAddrType,
                                      INT4 *pi4NextVrrpv3OperationsInetAddrType)
{
    BOOL1               b1VersionMatched = FALSE;

    b1VersionMatched = VrrpCompCheckVersion (VRRP_VERSION_3);

    if (b1VersionMatched == FALSE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    if (VrrpDbGetNextOperEntry (i4IfIndex, i4Vrrpv3OperationsVrId,
                                i4Vrrpv3OperationsInetAddrType, pi4NextIfIndex,
                                pi4NextVrrpv3OperationsVrId,
                                pi4NextVrrpv3OperationsInetAddrType)
        == VRRP_NOT_OK)
    {
        return ((INT1) SNMP_FAILURE);
    }

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsMasterIpAddr
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsMasterIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsMasterIpAddr (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                    INT4 i4Vrrpv3OperationsInetAddrType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValVrrpv3OperationsMasterIpAddr)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    Data.pOctetStrValue = pRetValVrrpv3OperationsMasterIpAddr;

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_MASTER_IP, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsPrimaryIpAddr
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsPrimaryIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsPrimaryIpAddr (INT4 i4IfIndex,
                                     INT4 i4Vrrpv3OperationsVrId,
                                     INT4 i4Vrrpv3OperationsInetAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValVrrpv3OperationsPrimaryIpAddr)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    Data.pOctetStrValue = pRetValVrrpv3OperationsPrimaryIpAddr;

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_PRIM_IP, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsVirtualMacAddr
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsVirtualMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsVirtualMacAddr (INT4 i4IfIndex,
                                      INT4 i4Vrrpv3OperationsVrId,
                                      INT4 i4Vrrpv3OperationsInetAddrType,
                                      tMacAddr *
                                      pRetValVrrpv3OperationsVirtualMacAddr)
{
    tSNMP_OCTET_STRING_TYPE MacAddr;
    tSNMP_MULTI_DATA_TYPE Data;
    UINT1               au1MacAddr[MAC_ADDR_LEN];
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    MEMSET (au1MacAddr, 0, MAC_ADDR_LEN);
    MacAddr.pu1_OctetList = au1MacAddr;
    MacAddr.i4_Length = MAC_ADDR_LEN;

    Data.pOctetStrValue = &MacAddr;

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_VIRT_MAC, &Data);

    MEMCPY (pRetValVrrpv3OperationsVirtualMacAddr,
            Data.pOctetStrValue->pu1_OctetList, MAC_ADDR_LEN);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsStatus
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsStatus (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                              INT4 i4Vrrpv3OperationsInetAddrType,
                              INT4 *pi4RetValVrrpv3OperationsStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_STATUS, &Data);

    *pi4RetValVrrpv3OperationsStatus = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsPriority
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsPriority (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                INT4 i4Vrrpv3OperationsInetAddrType,
                                UINT4 *pu4RetValVrrpv3OperationsPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_PRIORITY, &Data);

    *pu4RetValVrrpv3OperationsPriority = (UINT4) Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsAddrCount
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsAddrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsAddrCount (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                 INT4 i4Vrrpv3OperationsInetAddrType,
                                 INT4 *pi4RetValVrrpv3OperationsAddrCount)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_ADDR_COUNT, &Data);

    *pi4RetValVrrpv3OperationsAddrCount = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsAdvInterval
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsAdvInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsAdvInterval (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                   INT4 i4Vrrpv3OperationsInetAddrType,
                                   INT4 *pi4RetValVrrpv3OperationsAdvInterval)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_ADVT_INT, &Data);

    *pi4RetValVrrpv3OperationsAdvInterval
        = (Data.i4_SLongValue / V3_ADVERTISEMENTINTLINMSEC);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsPreemptMode
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsPreemptMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsPreemptMode (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                   INT4 i4Vrrpv3OperationsInetAddrType,
                                   INT4 *pi4RetValVrrpv3OperationsPreemptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_PREMP_MODE, &Data);

    *pi4RetValVrrpv3OperationsPreemptMode = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsAcceptMode
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsAcceptMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsAcceptMode (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                  INT4 i4Vrrpv3OperationsInetAddrType,
                                  INT4 *pi4RetValVrrpv3OperationsAcceptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_ACCEPT_MODE, &Data);

    *pi4RetValVrrpv3OperationsAcceptMode = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsUpTime
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsUpTime (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                              INT4 i4Vrrpv3OperationsInetAddrType,
                              UINT4 *pu4RetValVrrpv3OperationsUpTime)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_UP_TIME, &Data);

    *pu4RetValVrrpv3OperationsUpTime = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3OperationsRowStatus
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3OperationsRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3OperationsRowStatus (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                 INT4 i4Vrrpv3OperationsInetAddrType,
                                 INT4 *pi4RetValVrrpv3OperationsRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_ROW_STATUS, &Data);

    *pi4RetValVrrpv3OperationsRowStatus = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVrrpv3OperationsPrimaryIpAddr
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                setValVrrpv3OperationsPrimaryIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpv3OperationsPrimaryIpAddr (INT4 i4IfIndex,
                                     INT4 i4Vrrpv3OperationsVrId,
                                     INT4 i4Vrrpv3OperationsInetAddrType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValVrrpv3OperationsPrimaryIpAddr)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.pOctetStrValue = pSetValVrrpv3OperationsPrimaryIpAddr;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_PRIM_IP, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpv3OperationsPriority
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                setValVrrpv3OperationsPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpv3OperationsPriority (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                INT4 i4Vrrpv3OperationsInetAddrType,
                                UINT4 u4SetValVrrpv3OperationsPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = (INT4) u4SetValVrrpv3OperationsPriority;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_PRIORITY, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpv3OperationsAdvInterval
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                setValVrrpv3OperationsAdvInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpv3OperationsAdvInterval (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                   INT4 i4Vrrpv3OperationsInetAddrType,
                                   INT4 i4SetValVrrpv3OperationsAdvInterval)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue
        = (i4SetValVrrpv3OperationsAdvInterval * V3_ADVERTISEMENTINTLINMSEC);

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_ADVT_INT, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpv3OperationsPreemptMode
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                setValVrrpv3OperationsPreemptMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpv3OperationsPreemptMode (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                   INT4 i4Vrrpv3OperationsInetAddrType,
                                   INT4 i4SetValVrrpv3OperationsPreemptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValVrrpv3OperationsPreemptMode;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_PREMP_MODE, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpv3OperationsAcceptMode
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                setValVrrpv3OperationsAcceptMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpv3OperationsAcceptMode (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                  INT4 i4Vrrpv3OperationsInetAddrType,
                                  INT4 i4SetValVrrpv3OperationsAcceptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValVrrpv3OperationsAcceptMode;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_ACCEPT_MODE, &Data);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhSetVrrpv3OperationsRowStatus
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                setValVrrpv3OperationsRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpv3OperationsRowStatus (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                 INT4 i4Vrrpv3OperationsInetAddrType,
                                 INT4 i4SetValVrrpv3OperationsRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValVrrpv3OperationsRowStatus;

    i4RetValue = VrrpSetAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                      i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType,
                                      VRRP_MIB_OPER_ROW_STATUS, &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Vrrpv3OperationsPrimaryIpAddr
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                testValVrrpv3OperationsPrimaryIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Vrrpv3OperationsPrimaryIpAddr (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                        INT4 i4Vrrpv3OperationsVrId,
                                        INT4 i4Vrrpv3OperationsInetAddrType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValVrrpv3OperationsPrimaryIpAddr)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.pOctetStrValue = pTestValVrrpv3OperationsPrimaryIpAddr;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_OPER_PRIM_IP, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2Vrrpv3OperationsPriority
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                testValVrrpv3OperationsPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Vrrpv3OperationsPriority (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                   INT4 i4Vrrpv3OperationsVrId,
                                   INT4 i4Vrrpv3OperationsInetAddrType,
                                   UINT4 u4TestValVrrpv3OperationsPriority)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = (INT4) u4TestValVrrpv3OperationsPriority;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_OPER_PRIORITY, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2Vrrpv3OperationsAdvInterval
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                testValVrrpv3OperationsAdvInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Vrrpv3OperationsAdvInterval (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                      INT4 i4Vrrpv3OperationsVrId,
                                      INT4 i4Vrrpv3OperationsInetAddrType,
                                      INT4 i4TestValVrrpv3OperationsAdvInterval)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue
        = (i4TestValVrrpv3OperationsAdvInterval * V3_ADVERTISEMENTINTLINMSEC);

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_OPER_ADVT_INT, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2Vrrpv3OperationsPreemptMode
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                testValVrrpv3OperationsPreemptMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Vrrpv3OperationsPreemptMode (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                      INT4 i4Vrrpv3OperationsVrId,
                                      INT4 i4Vrrpv3OperationsInetAddrType,
                                      INT4 i4TestValVrrpv3OperationsPreemptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValVrrpv3OperationsPreemptMode;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_OPER_PREMP_MODE, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2Vrrpv3OperationsAcceptMode
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                testValVrrpv3OperationsAcceptMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Vrrpv3OperationsAcceptMode (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                     INT4 i4Vrrpv3OperationsVrId,
                                     INT4 i4Vrrpv3OperationsInetAddrType,
                                     INT4 i4TestValVrrpv3OperationsAcceptMode)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValVrrpv3OperationsAcceptMode;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_OPER_ACCEPT_MODE, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhTestv2Vrrpv3OperationsRowStatus
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                testValVrrpv3OperationsRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Vrrpv3OperationsRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                    INT4 i4Vrrpv3OperationsVrId,
                                    INT4 i4Vrrpv3OperationsInetAddrType,
                                    INT4 i4TestValVrrpv3OperationsRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValVrrpv3OperationsRowStatus;

    i4RetValue = VrrpTestAllOperTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_OPER_ROW_STATUS, &Data,
                                       pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Vrrpv3OperationsTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Vrrpv3OperationsTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (*pu4ErrorCode);
    UNUSED_PARAM (*pSnmpIndexList);
    UNUSED_PARAM (*pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : Vrrpv3AssociatedIpAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVrrpv3AssociatedIpAddrTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                Vrrpv3AssociatedIpAddrAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVrrpv3AssociatedIpAddrTable (INT4 i4IfIndex,
                                                     INT4
                                                     i4Vrrpv3OperationsVrId,
                                                     INT4
                                                     i4Vrrpv3OperationsInetAddrType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pVrrpv3AssociatedIpAddrAddress)
{
    if (nmhValidateIndexInstanceVrrpv3OperationsTable (i4IfIndex,
                                                       i4Vrrpv3OperationsVrId,
                                                       i4Vrrpv3OperationsInetAddrType)
        == SNMP_FAILURE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    UNUSED_PARAM (pVrrpv3AssociatedIpAddrAddress);
    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVrrpv3AssociatedIpAddrTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                Vrrpv3AssociatedIpAddrAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVrrpv3AssociatedIpAddrTable (INT4 *pi4IfIndex,
                                             INT4 *pi4Vrrpv3OperationsVrId,
                                             INT4
                                             *pi4Vrrpv3OperationsInetAddrType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pVrrpv3AssociatedIpAddrAddress)
{
    return (nmhGetNextIndexVrrpv3AssociatedIpAddrTable (0, pi4IfIndex, 0,
                                                        pi4Vrrpv3OperationsVrId,
                                                        0,
                                                        pi4Vrrpv3OperationsInetAddrType,
                                                        0,
                                                        pVrrpv3AssociatedIpAddrAddress));
}

/****************************************************************************
 Function    :  nmhGetNextIndexVrrpv3AssociatedIpAddrTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                Vrrpv3OperationsVrId
                nextVrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                nextVrrpv3OperationsInetAddrType
                Vrrpv3AssociatedIpAddrAddress
                nextVrrpv3AssociatedIpAddrAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVrrpv3AssociatedIpAddrTable (INT4 i4IfIndex,
                                            INT4 *pi4NextIfIndex,
                                            INT4 i4Vrrpv3OperationsVrId,
                                            INT4 *pi4NextVrrpv3OperationsVrId,
                                            INT4 i4Vrrpv3OperationsInetAddrType,
                                            INT4
                                            *pi4NextVrrpv3OperationsInetAddrType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pVrrpv3AssociatedIpAddrAddress,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextVrrpv3AssociatedIpAddrAddress)
{
    tIPvXAddr           IpAddr;
    tIpAddrTable       *pIpAddrEntry = NULL;
    INT4                i4OperIndex = VRRP_NOT_OK;
    BOOL1               b1VersionMatched = FALSE;

    VRRP_IPVX_ADDR_CLEAR (IpAddr);
    VRRP_IPVX_ADDR_INIT_IPVX (IpAddr, i4Vrrpv3OperationsInetAddrType,
                              pVrrpv3AssociatedIpAddrAddress->pu1_OctetList);

    b1VersionMatched = VrrpCompCheckVersion (VRRP_VERSION_3);

    if (b1VersionMatched == FALSE)
    {
        return ((INT1) SNMP_FAILURE);
    }

    i4OperIndex = VrrpDbGetOperEntry (i4IfIndex, i4Vrrpv3OperationsVrId,
                                      i4Vrrpv3OperationsInetAddrType);

    if (i4OperIndex > VRRP_NOT_OK)
    {
        pIpAddrEntry = VrrpDbOperGetNextAssocIpEntry (i4OperIndex, IpAddr);

        if (pIpAddrEntry != NULL)
        {
            *pi4NextIfIndex = i4IfIndex;
            *pi4NextVrrpv3OperationsVrId = i4Vrrpv3OperationsVrId;
            *pi4NextVrrpv3OperationsInetAddrType
                = i4Vrrpv3OperationsInetAddrType;
            VRRP_IPVX_COPY_TO_OCTET_STR (pIpAddrEntry->AssoIpAddr,
                                         pNextVrrpv3AssociatedIpAddrAddress);

            return ((INT1) SNMP_SUCCESS);
        }
    }

    if (VrrpDbGetNextOperEntry (i4IfIndex, i4Vrrpv3OperationsVrId,
                                i4Vrrpv3OperationsInetAddrType, pi4NextIfIndex,
                                pi4NextVrrpv3OperationsVrId,
                                pi4NextVrrpv3OperationsInetAddrType)
        == VRRP_NOT_OK)
    {
        return ((INT1) SNMP_FAILURE);
    }

    i4OperIndex =
        VrrpDbGetOperEntry (*pi4NextIfIndex, *pi4NextVrrpv3OperationsVrId,
                            *pi4NextVrrpv3OperationsInetAddrType);

    if (i4OperIndex == VRRP_NOT_OK)
    {
        return ((INT1) SNMP_FAILURE);
    }

    VRRP_IPVX_ADDR_CLEAR (IpAddr);

    pIpAddrEntry = VrrpDbOperGetNextAssocIpEntry (i4OperIndex, IpAddr);

    if (pIpAddrEntry == NULL)
    {
        return ((INT1) SNMP_FAILURE);
    }

    VRRP_IPVX_COPY_TO_OCTET_STR (pIpAddrEntry->AssoIpAddr,
                                 pNextVrrpv3AssociatedIpAddrAddress);

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVrrpv3AssociatedIpAddrRowStatus
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                Vrrpv3AssociatedIpAddrAddress

                The Object 
                retValVrrpv3AssociatedIpAddrRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3AssociatedIpAddrRowStatus (INT4 i4IfIndex,
                                       INT4 i4Vrrpv3OperationsVrId,
                                       INT4 i4Vrrpv3OperationsInetAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pVrrpv3AssociatedIpAddrAddress,
                                       INT4
                                       *pi4RetValVrrpv3AssociatedIpAddrRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllAssocIpTable ((UINT1) VRRP_VERSION_3, i4IfIndex,
                                         i4Vrrpv3OperationsVrId,
                                         i4Vrrpv3OperationsInetAddrType,
                                         pVrrpv3AssociatedIpAddrAddress,
                                         VRRP_MIB_ASSO_ROW_STATUS, &Data);

    *pi4RetValVrrpv3AssociatedIpAddrRowStatus = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetVrrpv3AssociatedIpAddrRowStatus
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                Vrrpv3AssociatedIpAddrAddress

                The Object 
                setValVrrpv3AssociatedIpAddrRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetVrrpv3AssociatedIpAddrRowStatus (INT4 i4IfIndex,
                                       INT4 i4Vrrpv3OperationsVrId,
                                       INT4 i4Vrrpv3OperationsInetAddrType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pVrrpv3AssociatedIpAddrAddress,
                                       INT4
                                       i4SetValVrrpv3AssociatedIpAddrRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4SetValVrrpv3AssociatedIpAddrRowStatus;

    i4RetValue = VrrpSetAllAssocIpTable (VRRP_VERSION_3, i4IfIndex,
                                         i4Vrrpv3OperationsVrId,
                                         i4Vrrpv3OperationsInetAddrType,
                                         pVrrpv3AssociatedIpAddrAddress,
                                         VRRP_MIB_ASSO_ROW_STATUS, &Data);

    return ((INT1) i4RetValue);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Vrrpv3AssociatedIpAddrRowStatus
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                Vrrpv3AssociatedIpAddrAddress

                The Object 
                testValVrrpv3AssociatedIpAddrRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Vrrpv3AssociatedIpAddrRowStatus (UINT4 *pu4ErrorCode, INT4 i4IfIndex,
                                          INT4 i4Vrrpv3OperationsVrId,
                                          INT4 i4Vrrpv3OperationsInetAddrType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pVrrpv3AssociatedIpAddrAddress,
                                          INT4
                                          i4TestValVrrpv3AssociatedIpAddrRowStatus)
{
    tSNMP_MULTI_DATA_TYPE Data;
    tIPvXAddr           IpAddr;
    INT4                i4RetValue = SNMP_FAILURE;

    VRRP_IPVX_ADDR_CLEAR (IpAddr);
    VRRP_IPVX_ADDR_INIT_IPVX (IpAddr, (UINT1) i4Vrrpv3OperationsInetAddrType,
                              pVrrpv3AssociatedIpAddrAddress->pu1_OctetList);

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));
    Data.i4_SLongValue = i4TestValVrrpv3AssociatedIpAddrRowStatus;

    i4RetValue = VrrpTestAllAssocIpTable (VRRP_VERSION_3, i4IfIndex,
                                          i4Vrrpv3OperationsVrId,
                                          i4Vrrpv3OperationsInetAddrType,
                                          pVrrpv3AssociatedIpAddrAddress,
                                          VRRP_MIB_ASSO_ROW_STATUS, &Data,
                                          pu4ErrorCode);

    return ((INT1) i4RetValue);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Vrrpv3AssociatedIpAddrTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                Vrrpv3AssociatedIpAddrAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Vrrpv3AssociatedIpAddrTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return ((INT1) SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVrrpv3RouterChecksumErrors
 Input       :  The Indices

                The Object 
                retValVrrpv3RouterChecksumErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3RouterChecksumErrors (tSNMP_COUNTER64_TYPE *
                                  pu8RetValVrrpv3RouterChecksumErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_3, VRRP_MIB_CHECKSUM_ERRORS, &Data);

    pu8RetValVrrpv3RouterChecksumErrors->msn = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3RouterChecksumErrors->lsn = Data.u8_Counter64Value.lsn;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3RouterVersionErrors
 Input       :  The Indices

                The Object 
                retValVrrpv3RouterVersionErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3RouterVersionErrors (tSNMP_COUNTER64_TYPE *
                                 pu8RetValVrrpv3RouterVersionErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_3, VRRP_MIB_VERSION_ERRORS, &Data);

    pu8RetValVrrpv3RouterVersionErrors->msn = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3RouterVersionErrors->lsn = Data.u8_Counter64Value.lsn;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3RouterVrIdErrors
 Input       :  The Indices

                The Object 
                retValVrrpv3RouterVrIdErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3RouterVrIdErrors (tSNMP_COUNTER64_TYPE *
                              pu8RetValVrrpv3RouterVrIdErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_3, VRRP_MIB_VRID_ERRORS, &Data);

    pu8RetValVrrpv3RouterVrIdErrors->msn = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3RouterVrIdErrors->lsn = Data.u8_Counter64Value.lsn;

    return ((INT1) SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3GlobalStatisticsDiscontinuityTime
 Input       :  The Indices

                The Object 
                retValVrrpv3GlobalStatisticsDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3GlobalStatisticsDiscontinuityTime (UINT4
                                               *pu4RetValVrrpv3GlobalStatisticsDiscontinuityTime)
{
    tSNMP_MULTI_DATA_TYPE Data;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    VrrpGetAllScalars (VRRP_VERSION_3, VRRP_MIB_GBL_DISC_TIME, &Data);

    *pu4RetValVrrpv3GlobalStatisticsDiscontinuityTime = Data.u4_ULongValue;

    return ((INT1) SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : Vrrpv3StatisticsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceVrrpv3StatisticsTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceVrrpv3StatisticsTable (INT4 i4IfIndex,
                                               INT4 i4Vrrpv3OperationsVrId,
                                               INT4
                                               i4Vrrpv3OperationsInetAddrType)
{
    return (nmhValidateIndexInstanceVrrpv3OperationsTable (i4IfIndex,
                                                           i4Vrrpv3OperationsVrId,
                                                           i4Vrrpv3OperationsInetAddrType));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexVrrpv3StatisticsTable
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexVrrpv3StatisticsTable (INT4 *pi4IfIndex,
                                       INT4 *pi4Vrrpv3OperationsVrId,
                                       INT4 *pi4Vrrpv3OperationsInetAddrType)
{
    return (nmhGetFirstIndexVrrpv3OperationsTable (pi4IfIndex,
                                                   pi4Vrrpv3OperationsVrId,
                                                   pi4Vrrpv3OperationsInetAddrType));
}

/****************************************************************************
 Function    :  nmhGetNextIndexVrrpv3StatisticsTable
 Input       :  The Indices
                IfIndex
                nextIfIndex
                Vrrpv3OperationsVrId
                nextVrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType
                nextVrrpv3OperationsInetAddrType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexVrrpv3StatisticsTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                      INT4 i4Vrrpv3OperationsVrId,
                                      INT4 *pi4NextVrrpv3OperationsVrId,
                                      INT4 i4Vrrpv3OperationsInetAddrType,
                                      INT4 *pi4NextVrrpv3OperationsInetAddrType)
{
    return (nmhGetNextIndexVrrpv3OperationsTable (i4IfIndex, pi4NextIfIndex,
                                                  i4Vrrpv3OperationsVrId,
                                                  pi4NextVrrpv3OperationsVrId,
                                                  i4Vrrpv3OperationsInetAddrType,
                                                  pi4NextVrrpv3OperationsInetAddrType));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsMasterTransitions
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsMasterTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsMasterTransitions (INT4 i4IfIndex,
                                         INT4 i4Vrrpv3OperationsVrId,
                                         INT4 i4Vrrpv3OperationsInetAddrType,
                                         UINT4
                                         *pu4RetValVrrpv3StatisticsMasterTransitions)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_MASTER_TRANS, &Data);

    *pu4RetValVrrpv3StatisticsMasterTransitions = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpSemFail (INT4 i4IfIndex,
                        INT4 i4Vrrpv3OperationsVrId,
                        INT4 i4Vrrpv3OperationsInetAddrType, UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPSEMFAIL, &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpSendNDNeighborAdvtFail (INT4 i4IfIndex,
                                       INT4 i4Vrrpv3OperationsVrId,
                                       INT4 i4Vrrpv3OperationsInetAddrType,
                                       UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPSENDNDNEIGHBORADVTFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpSendPacketToIpv4Fail (INT4 i4IfIndex,
                                     INT4 i4Vrrpv3OperationsVrId,
                                     INT4 i4Vrrpv3OperationsInetAddrType,
                                     UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPSENDPACKETTOIPV4FAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpSendPacketToIpv6Fail (INT4 i4IfIndex,
                                     INT4 i4Vrrpv3OperationsVrId,
                                     INT4 i4Vrrpv3OperationsInetAddrType,
                                     UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPSENDPACKETTOIPV6FAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpGetIpv4PortFromOperIndexFail (INT4 i4IfIndex,
                                             INT4 i4Vrrpv3OperationsVrId,
                                             INT4
                                             i4Vrrpv3OperationsInetAddrType,
                                             UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPGETIPV4PORTFROMOPERINDEXFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpGetIpv6PortFromOperIndexFail (INT4 i4IfIndex,
                                             INT4 i4Vrrpv3OperationsVrId,
                                             INT4
                                             i4Vrrpv3OperationsInetAddrType,
                                             UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPGETIPV6PORTFROMOPERINDEXFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpSendGratArpFail (INT4 i4IfIndex,
                                INT4 i4Vrrpv3OperationsVrId,
                                INT4 i4Vrrpv3OperationsInetAddrType,
                                UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPSENDGRATARPFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsEthRegisterMacAddrFail (INT4 i4IfIndex,
                                   INT4 i4Vrrpv3OperationsVrId,
                                   INT4 i4Vrrpv3OperationsInetAddrType,
                                   UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPETHREGISTERMACADDRFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpNetIpv4DeleteIntfFail (INT4 i4IfIndex,
                                      INT4 i4Vrrpv3OperationsVrId,
                                      INT4 i4Vrrpv3OperationsInetAddrType,
                                      UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPNETIP4DELINTFFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpNetIpv6DeleteIntfFail (INT4 i4IfIndex,
                                      INT4 i4Vrrpv3OperationsVrId,
                                      INT4 i4Vrrpv3OperationsInetAddrType,
                                      UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPNETIP6DELINTFFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpIpifJoinMcastGroupFail (INT4 i4IfIndex,
                                       INT4 i4Vrrpv3OperationsVrId,
                                       INT4 i4Vrrpv3OperationsInetAddrType,
                                       UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPIPIFJOINMCASTGROUPFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpIpifLeaveMcastGroupFail (INT4 i4IfIndex,
                                        INT4 i4Vrrpv3OperationsVrId,
                                        INT4 i4Vrrpv3OperationsInetAddrType,
                                        UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPIPIFLEAVEMCASTGROUPFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsVrrpSocketFail (INT4 i4IfIndex,
                           INT4 i4Vrrpv3OperationsVrId,
                           INT4 i4Vrrpv3OperationsInetAddrType,
                           UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_VRRPSOCKETFAIL, &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsPktProcessValidateAdverPacketFail (INT4 i4IfIndex,
                                              INT4 i4Vrrpv3OperationsVrId,
                                              INT4
                                              i4Vrrpv3OperationsInetAddrType,
                                              UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERPACKETFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsPktProcessValidateAdverV2PacketFail (INT4 i4IfIndex,
                                                INT4 i4Vrrpv3OperationsVrId,
                                                INT4
                                                i4Vrrpv3OperationsInetAddrType,
                                                UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERV2PACKETFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsPktProcessValidateAdverV3PacketFail (INT4 i4IfIndex,
                                                INT4 i4Vrrpv3OperationsVrId,
                                                INT4
                                                i4Vrrpv3OperationsInetAddrType,
                                                UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERV3PACKETFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsPktProcessTransmitV2PacketFail (INT4 i4IfIndex,
                                           INT4 i4Vrrpv3OperationsVrId,
                                           INT4 i4Vrrpv3OperationsInetAddrType,
                                           UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_PKTPROCESSTRANSMITV2PACKETFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

INT1
Vrrpv3StatsPktProcessTransmitV3PacketFail (INT4 i4IfIndex,
                                           INT4 i4Vrrpv3OperationsVrId,
                                           INT4 i4Vrrpv3OperationsInetAddrType,
                                           UINT4 *pu4RetVal)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_PKTPROCESSTRANSMITV3PACKETFAIL,
                                       &Data);

    *pu4RetVal = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsNewMasterReason
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsNewMasterReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsNewMasterReason (INT4 i4IfIndex,
                                       INT4 i4Vrrpv3OperationsVrId,
                                       INT4 i4Vrrpv3OperationsInetAddrType,
                                       INT4
                                       *pi4RetValVrrpv3StatisticsNewMasterReason)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_MASTER_REASON, &Data);

    *pi4RetValVrrpv3StatisticsNewMasterReason = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsRcvdAdvertisements
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsRcvdAdvertisements
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsRcvdAdvertisements (INT4 i4IfIndex,
                                          INT4 i4Vrrpv3OperationsVrId,
                                          INT4 i4Vrrpv3OperationsInetAddrType,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValVrrpv3StatisticsRcvdAdvertisements)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_RCVD_ADVT, &Data);

    pu8RetValVrrpv3StatisticsRcvdAdvertisements->msn
        = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3StatisticsRcvdAdvertisements->lsn
        = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsAdvIntervalErrors
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsAdvIntervalErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsAdvIntervalErrors (INT4 i4IfIndex,
                                         INT4 i4Vrrpv3OperationsVrId,
                                         INT4 i4Vrrpv3OperationsInetAddrType,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValVrrpv3StatisticsAdvIntervalErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_ADVT_INT_ERR, &Data);

    pu8RetValVrrpv3StatisticsAdvIntervalErrors->msn
        = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3StatisticsAdvIntervalErrors->lsn
        = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsIpTtlErrors
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsIpTtlErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsIpTtlErrors (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                   INT4 i4Vrrpv3OperationsInetAddrType,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValVrrpv3StatisticsIpTtlErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_IP_TTL_ERR, &Data);

    pu8RetValVrrpv3StatisticsIpTtlErrors->msn = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3StatisticsIpTtlErrors->lsn = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsProtoErrReason
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsProtoErrReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsProtoErrReason (INT4 i4IfIndex,
                                      INT4 i4Vrrpv3OperationsVrId,
                                      INT4 i4Vrrpv3OperationsInetAddrType,
                                      INT4
                                      *pi4RetValVrrpv3StatisticsProtoErrReason)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_PROTO_ERR_REASON, &Data);

    *pi4RetValVrrpv3StatisticsProtoErrReason = Data.i4_SLongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsRcvdPriZeroPackets
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsRcvdPriZeroPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsRcvdPriZeroPackets (INT4 i4IfIndex,
                                          INT4 i4Vrrpv3OperationsVrId,
                                          INT4 i4Vrrpv3OperationsInetAddrType,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValVrrpv3StatisticsRcvdPriZeroPackets)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_RCVD_ZERO_PRI, &Data);

    pu8RetValVrrpv3StatisticsRcvdPriZeroPackets->msn
        = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3StatisticsRcvdPriZeroPackets->lsn
        = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsSentPriZeroPackets
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsSentPriZeroPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsSentPriZeroPackets (INT4 i4IfIndex,
                                          INT4 i4Vrrpv3OperationsVrId,
                                          INT4 i4Vrrpv3OperationsInetAddrType,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValVrrpv3StatisticsSentPriZeroPackets)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_SENT_ZERO_PRI, &Data);

    pu8RetValVrrpv3StatisticsSentPriZeroPackets->msn
        = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3StatisticsSentPriZeroPackets->lsn
        = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsRcvdInvalidTypePackets
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsRcvdInvalidTypePackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsRcvdInvalidTypePackets (INT4 i4IfIndex,
                                              INT4 i4Vrrpv3OperationsVrId,
                                              INT4
                                              i4Vrrpv3OperationsInetAddrType,
                                              tSNMP_COUNTER64_TYPE *
                                              pu8RetValVrrpv3StatisticsRcvdInvalidTypePackets)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_RCVD_INVD_TYPE, &Data);

    pu8RetValVrrpv3StatisticsRcvdInvalidTypePackets->msn
        = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3StatisticsRcvdInvalidTypePackets->lsn
        = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsAddressListErrors
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsAddressListErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsAddressListErrors (INT4 i4IfIndex,
                                         INT4 i4Vrrpv3OperationsVrId,
                                         INT4 i4Vrrpv3OperationsInetAddrType,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValVrrpv3StatisticsAddressListErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_ADDR_LIST_ERR, &Data);

    pu8RetValVrrpv3StatisticsAddressListErrors->msn
        = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3StatisticsAddressListErrors->lsn
        = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsPacketLengthErrors
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsPacketLengthErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsPacketLengthErrors (INT4 i4IfIndex,
                                          INT4 i4Vrrpv3OperationsVrId,
                                          INT4 i4Vrrpv3OperationsInetAddrType,
                                          tSNMP_COUNTER64_TYPE *
                                          pu8RetValVrrpv3StatisticsPacketLengthErrors)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_PKT_LEN_ERR, &Data);

    pu8RetValVrrpv3StatisticsPacketLengthErrors->msn
        = Data.u8_Counter64Value.msn;
    pu8RetValVrrpv3StatisticsPacketLengthErrors->lsn
        = Data.u8_Counter64Value.lsn;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsRowDiscontinuityTime
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsRowDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsRowDiscontinuityTime (INT4 i4IfIndex,
                                            INT4 i4Vrrpv3OperationsVrId,
                                            INT4 i4Vrrpv3OperationsInetAddrType,
                                            UINT4
                                            *pu4RetValVrrpv3StatisticsRowDiscontinuityTime)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_ROW_DISC_TIME, &Data);

    *pu4RetValVrrpv3StatisticsRowDiscontinuityTime = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}

/****************************************************************************
 Function    :  nmhGetVrrpv3StatisticsRefreshRate
 Input       :  The Indices
                IfIndex
                Vrrpv3OperationsVrId
                Vrrpv3OperationsInetAddrType

                The Object 
                retValVrrpv3StatisticsRefreshRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetVrrpv3StatisticsRefreshRate (INT4 i4IfIndex, INT4 i4Vrrpv3OperationsVrId,
                                   INT4 i4Vrrpv3OperationsInetAddrType,
                                   UINT4 *pu4RetValVrrpv3StatisticsRefreshRate)
{
    tSNMP_MULTI_DATA_TYPE Data;
    INT4                i4RetValue = SNMP_FAILURE;

    MEMSET (&Data, 0, sizeof (tSNMP_MULTI_DATA_TYPE));

    i4RetValue = VrrpGetAllStatsTable (VRRP_VERSION_3, i4IfIndex,
                                       i4Vrrpv3OperationsVrId,
                                       i4Vrrpv3OperationsInetAddrType,
                                       VRRP_MIB_STAT_REFRESH_RATE, &Data);

    *pu4RetValVrrpv3StatisticsRefreshRate = Data.u4_ULongValue;

    return ((INT1) i4RetValue);
}
