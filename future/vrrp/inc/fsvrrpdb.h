/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvrrpdb.h,v 1.10 2014/07/20 11:09:51 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSVRRPDB_H
#define _FSVRRPDB_H

UINT1 FsVrrpOperTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsVrrpOperTrackGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsVrrpOperTrackGroupIfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsvrrp [] ={1,3,6,1,4,1,2076,153};
tSNMP_OID_TYPE fsvrrpOID = {8, fsvrrp};


UINT4 FsVrrpStatus [ ] ={1,3,6,1,4,1,2076,153,1,1};
UINT4 FsVrrpMaxOperEntries [ ] ={1,3,6,1,4,1,2076,153,1,2};
UINT4 FsVrrpAdminPriority [ ] ={1,3,6,1,4,1,2076,153,1,3,1,1};
UINT4 FsVrrpOperAdvertisementIntervalInMsec [ ] ={1,3,6,1,4,1,2076,153,1,3,1,2};
UINT4 FsVrrpOperTrackGroupId [ ] ={1,3,6,1,4,1,2076,153,1,3,1,3};
UINT4 FsVrrpOperDecrementPriority [ ] ={1,3,6,1,4,1,2076,153,1,3,1,4};
UINT4 FsVrrpAcceptMode [ ] ={1,3,6,1,4,1,2076,153,1,3,1,5};
UINT4 FsVrrpAuthDeprecate [ ] ={1,3,6,1,4,1,2076,153,1,4};
UINT4 FsVrrpTraceOption [ ] ={1,3,6,1,4,1,2076,153,1,5};
UINT4 FsVrrpOperTrackGroupIndex [ ] ={1,3,6,1,4,1,2076,153,1,6,1,1};
UINT4 FsVrrpOperTrackedGroupTrackedLinks [ ] ={1,3,6,1,4,1,2076,153,1,6,1,2};
UINT4 FsVrrpOperTrackRowStatus [ ] ={1,3,6,1,4,1,2076,153,1,6,1,3};
UINT4 FsVrrpOperTrackGroupIfIndex [ ] ={1,3,6,1,4,1,2076,153,1,7,1,1};
UINT4 FsVrrpOperTrackGroupIfRowStatus [ ] ={1,3,6,1,4,1,2076,153,1,7,1,2};




tMbDbEntry fsvrrpMibEntry[]= {

{{10,FsVrrpStatus}, NULL, FsVrrpStatusGet, FsVrrpStatusSet, FsVrrpStatusTest, FsVrrpStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsVrrpMaxOperEntries}, NULL, FsVrrpMaxOperEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsVrrpAdminPriority}, GetNextIndexFsVrrpOperTable, FsVrrpAdminPriorityGet, FsVrrpAdminPrioritySet, FsVrrpAdminPriorityTest, FsVrrpOperTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsVrrpOperTableINDEX, 2, 0, 0, "100"},

{{12,FsVrrpOperAdvertisementIntervalInMsec}, GetNextIndexFsVrrpOperTable, FsVrrpOperAdvertisementIntervalInMsecGet, FsVrrpOperAdvertisementIntervalInMsecSet, FsVrrpOperAdvertisementIntervalInMsecTest, FsVrrpOperTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsVrrpOperTableINDEX, 2, 0, 0, "1000"},

{{12,FsVrrpOperTrackGroupId}, GetNextIndexFsVrrpOperTable, FsVrrpOperTrackGroupIdGet, FsVrrpOperTrackGroupIdSet, FsVrrpOperTrackGroupIdTest, FsVrrpOperTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVrrpOperTableINDEX, 2, 0, 0, "0"},

{{12,FsVrrpOperDecrementPriority}, GetNextIndexFsVrrpOperTable, FsVrrpOperDecrementPriorityGet, FsVrrpOperDecrementPrioritySet, FsVrrpOperDecrementPriorityTest, FsVrrpOperTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVrrpOperTableINDEX, 2, 0, 0, "0"},

{{12,FsVrrpAcceptMode}, GetNextIndexFsVrrpOperTable, FsVrrpAcceptModeGet, FsVrrpAcceptModeSet, FsVrrpAcceptModeTest, FsVrrpOperTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVrrpOperTableINDEX, 2, 0, 0, "2"},

{{10,FsVrrpAuthDeprecate}, NULL, FsVrrpAuthDeprecateGet, FsVrrpAuthDeprecateSet, FsVrrpAuthDeprecateTest, FsVrrpAuthDeprecateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsVrrpTraceOption}, NULL, FsVrrpTraceOptionGet, FsVrrpTraceOptionSet, FsVrrpTraceOptionTest, FsVrrpTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsVrrpOperTrackGroupIndex}, GetNextIndexFsVrrpOperTrackGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsVrrpOperTrackGroupTableINDEX, 1, 0, 0, NULL},

{{12,FsVrrpOperTrackedGroupTrackedLinks}, GetNextIndexFsVrrpOperTrackGroupTable, FsVrrpOperTrackedGroupTrackedLinksGet, FsVrrpOperTrackedGroupTrackedLinksSet, FsVrrpOperTrackedGroupTrackedLinksTest, FsVrrpOperTrackGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVrrpOperTrackGroupTableINDEX, 1, 0, 0, "0"},

{{12,FsVrrpOperTrackRowStatus}, GetNextIndexFsVrrpOperTrackGroupTable, FsVrrpOperTrackRowStatusGet, FsVrrpOperTrackRowStatusSet, FsVrrpOperTrackRowStatusTest, FsVrrpOperTrackGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVrrpOperTrackGroupTableINDEX, 1, 0, 1, NULL},

{{12,FsVrrpOperTrackGroupIfIndex}, GetNextIndexFsVrrpOperTrackGroupIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVrrpOperTrackGroupIfTableINDEX, 2, 0, 0, NULL},

{{12,FsVrrpOperTrackGroupIfRowStatus}, GetNextIndexFsVrrpOperTrackGroupIfTable, FsVrrpOperTrackGroupIfRowStatusGet, FsVrrpOperTrackGroupIfRowStatusSet, FsVrrpOperTrackGroupIfRowStatusTest, FsVrrpOperTrackGroupIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVrrpOperTrackGroupIfTableINDEX, 2, 0, 1, NULL},
};
tMibData fsvrrpEntry = { 14, fsvrrpMibEntry };

#endif /* _FSVRRPDB_H */

