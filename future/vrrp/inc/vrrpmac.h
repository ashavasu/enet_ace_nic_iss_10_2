/*  $Id: vrrpmac.h,v 1.17 2017/12/16 11:57:11 siva Exp $ */
/********************************************************************
 *                                                                  *
 * $RCSfile: vrrpmac.h,v $
 *                                                                  *
 * $Date: 2017/12/16 11:57:11 $
 *                                                                  *
 * $Revision: 1.17 $
 *                                                                  *
 *******************************************************************/

/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrpmac.h                                    |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricnet Inc.                                 |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Core Protocol.                          |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : Macro definitions of VRRP Core Protocol.      |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#ifdef VRRP_DEBUG
#define   VRRP_DEFAULT_DEBUG_LEVEL   VRRP_TRC_ALL     /* full debug mode */
#else /* VRRP_DEBUG mode */
#define   VRRP_DEFAULT_DEBUG_LEVEL   VRRP_TRC_NONE     /* no debug-trace */
#endif /* no VRRP_DEBUG mode */

#define VRRP_MASK                 gi4DbugFlag 
#define MODULE_NAME    "VRRP"

/* flags for the VRRP trace call  */
#define   VRRP_TRC_NONE                 (0x00000000)
#define   VRRP_TRC_ALL                  (0x0000ffff)
#define   VRRP_TRC_PKT                  (0x00000001)
#define   VRRP_TRC_EVENTS               (0x00000002)
#define   VRRP_TRC_INIT                 (0x00000004)
#define   VRRP_TRC_TIMERS               (0x00000008)
#define   VRRP_TRC_ALL_FAILURES         (0x00000010)
#define   VRRP_TRC_MEMORY               (0x00000020)
#define   VRRP_TRC_BUFFER               (0x00000040)
#define   VRRP_TRC_VERSION_2            (0x00000080)
#define   VRRP_TRC_VERSION_3            (0x00000100)

#ifdef TRACE_WANTED
/* MACROS for Debug-Trace Mechanism */
/* to enable the trace VRRP_TRC_NONE to be replaced by
   VRRP_TRC_ALL_TRACK*/
#define   VRRP_DBG5(Flag, Fmt, Arg1,Arg2,Arg3,Arg4,Arg5) \
{\
 if (((Flag) & VRRP_TRC_ALL_FAILURES) || (Flag & VRRP_CLI_TRC_STATE))\
 {\
  UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, (UINT4)VRRP_MASK, Flag, MODULE_NAME, MODULE_NAME, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5);\
 }\
 else\
 {\
  UtlTrcLog((UINT4)VRRP_MASK, Flag, MODULE_NAME,  Fmt, Arg1, Arg2, Arg3, Arg4, Arg5);\
 }\
}

#define   VRRP_DBG4(Flag, Fmt, Arg1,Arg2,Arg3,Arg4) \
{\
 if (((Flag) & VRRP_TRC_ALL_FAILURES) || (Flag & VRRP_CLI_TRC_STATE))\
 {\
  UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, (UINT4)VRRP_MASK, Flag, MODULE_NAME, MODULE_NAME, Fmt, Arg1, Arg2, Arg3, Arg4);\
 }\
 else\
 {\
  UtlTrcLog((UINT4)VRRP_MASK, Flag, MODULE_NAME,  Fmt, Arg1, Arg2, Arg3, Arg4);\
 }\
}

#define   VRRP_DBG3(Flag, Fmt, Arg1,Arg2,Arg3) \
{\
 if (((Flag) & VRRP_TRC_ALL_FAILURES) || (Flag & VRRP_CLI_TRC_STATE))\
 {\
  UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, (UINT4)VRRP_MASK, Flag, MODULE_NAME, MODULE_NAME, Fmt, Arg1, Arg2, Arg3);\
 }\
 else\
 {\
  UtlTrcLog((UINT4)VRRP_MASK, Flag, MODULE_NAME,  Fmt, Arg1, Arg2, Arg3);\
 }\
}

#define   VRRP_DBG2(Flag, Fmt, Arg1,Arg2) \
{\
 if (((Flag) & VRRP_TRC_ALL_FAILURES) || (Flag & VRRP_CLI_TRC_STATE))\
 {\
  UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, (UINT4)VRRP_MASK, Flag, MODULE_NAME, MODULE_NAME, Fmt, Arg1, Arg2);\
 }\
 else\
 {\
  UtlTrcLog((UINT4)VRRP_MASK, Flag, MODULE_NAME,  Fmt, Arg1, Arg2);\
 }\
}

#define   VRRP_DBG1(Flag, Fmt, Arg) \
{\
 if (((Flag) & VRRP_TRC_ALL_FAILURES) || (Flag & VRRP_CLI_TRC_STATE))\
 {\
  UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, (UINT4)VRRP_MASK, Flag, MODULE_NAME, MODULE_NAME, Fmt, Arg);\
 }\
 else\
 {\
  UtlTrcLog((UINT4)VRRP_MASK, Flag, MODULE_NAME,  Fmt, Arg);\
 }\
}

#define   VRRP_DBG(Flag,Fmt) \
{\
 if (((Flag) & VRRP_TRC_ALL_FAILURES) || (Flag & VRRP_CLI_TRC_STATE))\
 {\
  UtlSysTrcLog (SYSLOG_CRITICAL_LEVEL, (UINT4)VRRP_MASK, Flag, MODULE_NAME, MODULE_NAME, Fmt);\
 }\
 else\
 {\
  UtlTrcLog((UINT4)VRRP_MASK, Flag, MODULE_NAME,  Fmt);\
 }\
}

#else
#define   VRRP_DBG(Flag,Fmt) UNUSED_PARAM(Flag)

#define   VRRP_DBG1(Flag,Fmt, Arg) UNUSED_PARAM(Flag)

#define   VRRP_DBG2(Flag,Fmt, Arg1, Arg2) UNUSED_PARAM(Flag)

#define   VRRP_DBG3(Flag,Fmt, Arg1, Arg2, Arg3) UNUSED_PARAM(Flag)

#define   VRRP_DBG4(Flag,Fmt, Arg1, Arg2, Arg3, Arg4) UNUSED_PARAM(Flag)

#define   VRRP_DBG5(Flag,Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) UNUSED_PARAM(Flag)

#endif



#define SKEW_TIME(a)                    (float)(256 - a)/256
#define MASTERDOWN_INTERVAL(a,b)        ((3*(a))+(SKEW_TIME(b)))

/* Macros used for accessing OperTable */

#define OPERMACADDR(x)            gpVrrpOperTable[x].maVirtualMacAddr
#define OPERIFINDEX(x)            gpVrrpOperTable[x].i4IfIndex
#define OPERVRID(x)               gpVrrpOperTable[x].i4VrId
#define OPERADDRTYPE(x)           gpVrrpOperTable[x].u1AddressType
#define OPERSTATE(x)              gpVrrpOperTable[x].i4OperState
#define OPERADMINSTATE(x)         gpVrrpOperTable[x].i4OperAdminState
#define OPERADMINPRIORITY(x)      gpVrrpOperTable[x].i4Priority
#define OPERPRIORITY(x)           VrrpGetOperPriority(x)
#define OPERIPADDRCOUNT(x)        gpVrrpOperTable[x].i4IpAddrCount
#define OPERMASTERIPADDR(x)       gpVrrpOperTable[x].MasterIpAddr
#define OPERPRIMARYIPADDR(x)      gpVrrpOperTable[x].PrimaryIpAddr
#define OPERAUTHTYPE(x)           gpVrrpOperTable[x].i4AuthType
#define OPERAUTHKEY(x)            gpVrrpOperTable[x].stAuthKey
#define OPERADVERINTL(x)          gpVrrpOperTable[x].i4AdvtInterval
#define OPERPREEMPTMODE(x)        gpVrrpOperTable[x].i4PreemptMode
#define OPERPROTOCOL(x)           gpVrrpOperTable[x].i4Protocol
#define OPERROWSTATUS(x)          gpVrrpOperTable[x].i4OperRowStatus
#define OPERROUTERUPTIME(x)       gpVrrpOperTable[x].u4VirtualRouterUpTime
#define INTERFACEIPADDR(x)        gpVrrpOperTable[x].InterfaceIpAddr
#define VRRPSTATS(x)              gpVrrpOperTable[x].VrrpStats
#define OPERTMR(x)                gpVrrpOperTable[x].TmrRef
#define TRANSMITLNBUF(x)          gpVrrpOperTable[x].pu1TxBuf
#define OPERNEXTSTATE(x)          gpVrrpOperTable[x].u1NextState
#define OPEREVENTRCVD(x)          gpVrrpOperTable[x].u1EventRcvd
#define OPERRCVDPRIORITY(x)       gpVrrpOperTable[x].u1RcvdPriority
#define OPERDBNODE(x)             gpVrrpOperTable[x].OperDbNode
#define VRRPOPERHWSTATUS(x)       gpVrrpOperTable[x].u1HwStatus
#define OPERNWENTRYSTATUS(x)      gpVrrpOperTable[x].u1NwEntryStatus

#define OPERVRPORTNUM(x)     gpVrrpOperTable[x].u4VrIfIndex

#define OPERACCEPTMODE(x)         gpVrrpOperTable[x].u1AcceptMode
#define OPERTRACKGRPID(x)         gpVrrpOperTable[x].u4TrackGroupId
#define OPERDECPRIO(x)            gpVrrpOperTable[x].u1DecrementedPriority
#define OPERTRACKIFSTATUS(x)      gpVrrpOperTable[x].u1TrackedIfStatus
#define OPERV3ADVTRCVD(x)         gpVrrpOperTable[x].b1V3AdvtRcvd
#define OPERRCVDMASTERADVTINT(x)  gpVrrpOperTable[x].i4RcvdMasterAdvtInterval
#define OPERSKEWTIME(x)           gpVrrpOperTable[x].i4SkewTime
#define OPERMASTERDOWNINT(x)      gpVrrpOperTable[x].i4MasterDownInterval
#define OPERISOWNER(x)            gpVrrpOperTable[x].b1IsIpvXOwner
#define OPERISPREVOWNER(x)        gpVrrpOperTable[x].b1IsPrevIpvXOwner
#define OPERISTESTNOTREQD(x)      gpVrrpOperTable[x].b1IsTestValNotReqd

#define OPERASSOCIPLIST(x)        gpVrrpOperTable[x].AssocIpAddrHead
#define OPERTRACKOPERNODE(x)      gpVrrpOperTable[x].TrackGroupOperNode

#define OPERRMVERSION(x)          gpVrrpOperTable[x].u1Version
#define OPERRMPAD(x)              gpVrrpOperTable[x].au1Pad
 
/* Macros used for accessing ASSO IPADDR Table */

#define ASSOIPADDRESS(OperIndex, AssoIndex) \
      gpVrrpOperTable[OperIndex].IpAssoAddrTable[AssoIndex].AssoIpAddr

#define ASSOROWSTATUS(OperIndex, AssoIndex) \
     gpVrrpOperTable[OperIndex].IpAssoAddrTable[AssoIndex].u1AssoIpAddrRowStatus

/* Macros used for accessing STATS  Table */

#define STATSVERSIONERRORS         gu4RouterVersionErrors
#define STATSVIDERRORS             gu4RouterVrIdErrors

#define STATSCHECKSUMERRORS64      gu8RouterChecksumErrors
#define STATSVERSIONERRORS64       gu8RouterVersionErrors
#define STATSVIDERRORS64           gu8RouterVrIdErrors

#define STATSBECOMEMASTER(x)       gpVrrpOperTable[x].VrrpStats.u4BecomeMaster
#define STATSAUTHFAILURES(x)       gpVrrpOperTable[x].VrrpStats.u4AuthFailures
#define STATSINVALIDAUTHTYPE(x)    gpVrrpOperTable[x].VrrpStats.u4InvalidAuthType
#define STATSAUTHTYPEMISMATCH(x)   gpVrrpOperTable[x].VrrpStats.u4AuthTypeMismatch

#define STATSMASTERREASON(x)       gpVrrpOperTable[x].VrrpStats.u1NewMasterReason
#define STATSADVERTISERCVD64(x)    gpVrrpOperTable[x].VrrpStats.u8RcvdAdvertisements
#define STATSADINTLERRORS64(x)     gpVrrpOperTable[x].VrrpStats.u8AdvtIntervalErrors
#define STATSIPTTLERRORS64(x)      gpVrrpOperTable[x].VrrpStats.u8IpTtlErrors
#define STATSPTYZERORCVD64(x)      gpVrrpOperTable[x].VrrpStats.u8RcvdPriZeroPackets
#define STATSPTYZEROSENT64(x)      gpVrrpOperTable[x].VrrpStats.u8SentPriZeroPackets
#define STATSPROTOERRREASON(x)     gpVrrpOperTable[x].VrrpStats.u1ProtoErrReason
#define STATSINVALIDTYPE64(x)      gpVrrpOperTable[x].VrrpStats.u8RcvdInvalidTypePackets
#define STATSADDRLISTERROR64(x)    gpVrrpOperTable[x].VrrpStats.u8AddressListErrors
#define STATSPACKETLENERROR64(x)   gpVrrpOperTable[x].VrrpStats.u8PacketLengthErrors
#define STATSDISCTIME(x)           gpVrrpOperTable[x].VrrpStats.DiscontinuityTime
#define STATSTXEDADVT(x)           gpVrrpOperTable[x].VrrpStats.u8TxedV2Advertisments
#define STATSTXEDADVT64(x)         gpVrrpOperTable[x].VrrpStats.u8TxedAdvertisments
#define STATSV2ADVTIGNORED(x)      gpVrrpOperTable[x].VrrpStats.u8V2AdvtIgnored
#define OPERFIRSTPKTRECVFROMNP(x)  gpVrrpOperTable[x].u1FirstPktReceived
#define STATVRRPSEMFAIL(x)                              gpVrrpOperTable[x].VrrpStats.u4VrrpSemFail
#define STATVRRPSENDNDNEIGHBORADVTFAIL(x)               gpVrrpOperTable[x].VrrpStats.u4VrrpSendNDNeighborAdvtFail
#define STATVRRPSENDPACKETTOIPV4FAIL(x)                 gpVrrpOperTable[x].VrrpStats.u4VrrpSendPacketToIpv4Fail
#define STATVRRPSENDPACKETTOIPV6FAIL(x)                 gpVrrpOperTable[x].VrrpStats.u4VrrpSendPacketToIpv6Fail
#define STATVRRPGETIPV4PORTFROMOPERINDEXFAIL(x)         gpVrrpOperTable[x].VrrpStats.u4VrrpGetIpv4PortFromOperIndexFail
#define STATVRRPGETIPV6PORTFROMOPERINDEXFAIL(x)         gpVrrpOperTable[x].VrrpStats.u4VrrpGetIpv6PortFromOperIndexFail
#define STATVRRPSENDGRATARPFAIL(x)                      gpVrrpOperTable[x].VrrpStats.u4VrrpSendGratArpFail
#define STATVRRPETHREGISTERMACADDRFAIL(x)               gpVrrpOperTable[x].VrrpStats.u4EthRegisterMacAddrFail
#define STATVRRPNETIP4DELINTFFAIL(x)                    gpVrrpOperTable[x].VrrpStats.u4VrrpNetIpv4DeleteIntfFail
#define STATVRRPNETIP6DELINTFFAIL(x)                    gpVrrpOperTable[x].VrrpStats.u4VrrpNetIpv6DeleteIntfFail
#define STATVRRPIPIFJOINMCASTGROUPFAIL(x)               gpVrrpOperTable[x].VrrpStats.u4VrrpIpifJoinMcastGroupFail
#define STATVRRPIPIFLEAVEMCASTGROUPFAIL(x)              gpVrrpOperTable[x].VrrpStats.u4VrrpIpifLeaveMcastGroupFail
#define STATVRRPSOCKETFAIL(x)                           gpVrrpOperTable[x].VrrpStats.u4VrrpSocketFail
#define STATPKTPROCESSVALIDATEADVERPACKETFAIL(x)        gpVrrpOperTable[x].VrrpStats.u4PktProcessValidateAdverPacketFail
#define STATPKTPROCESSVALIDATEADVERV2PACKETFAIL(x)      gpVrrpOperTable[x].VrrpStats.u4PktProcessValidateAdverV2PacketFail
#define STATPKTPROCESSVALIDATEADVERV3PACKETFAIL(x)      gpVrrpOperTable[x].VrrpStats.u4PktProcessValidateAdverV3PacketFail
#define STATPKTPROCESSTRANSMITV2PACKETFAIL(x)           gpVrrpOperTable[x].VrrpStats.u4PktProcessTransmitV2PacketFail
#define STATPKTPROCESSTRANSMITV3PACKETFAIL(x)           gpVrrpOperTable[x].VrrpStats.u4PktProcessTransmitV3PacketFail

#define VRRP_RM_PKT_Q_ID gVrrpRmPktQId
/* Macros used in SNMP Low Level Routines */

#define BITSET(Mask,SetBit) Mask = Mask | (0x01 << SetBit);

# define  COPYTOSTRING(px,py)\
               memcpy(py,((tSNMP_OCTET_STRING_TYPE *)px)->pu1_OctetList,\
                       ((tSNMP_OCTET_STRING_TYPE *)px)->i4_Length);


# define  COPYFROMSTRING(px,py,size)\
         memcpy(((tSNMP_OCTET_STRING_TYPE *)px)->pu1_OctetList,py,size);\
           ((tSNMP_OCTET_STRING_TYPE *)px)->i4_Length = size;


/* MACROS for BUFIF Start Here */
#define   EXTRACT_IF_INDEX(x) CRU_BUF_Get_Interface_Num(CRU_BUF_Get_InterfaceId((tCRU_BUF_CHAIN_DESC *)&x))

#define  CRU_FORW_RT_TASK_INPUT_TQUEUE   TMO_TASK_QUE_01
#define  CRU_IP_ROUTING_VRRP_MODULE      112

/****************************************************************************/


#define ALIGN_BUFFER_TO_VRRP_PKT(pBuf) \
      { \
        INT1 Ver4Hlen4; \
        CRU_BMC_GET_1_BYTE (pBuf,ZERO,Ver4Hlen4);\
        CRU_BUF_MOVE_VALID_OFFSET (pBuf, ((Ver4Hlen4 & 0x0f) << 2));\
       }

#define VRRP_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#define VRRP_MAX_AUTH_KEY_SIZE  17

/* Get Base Pointer */
#define VRRP_GET_BASE_PTR(type, memberName, pMember) \
    (type *) ((FS_ULONG)(pMember) - ((FS_ULONG) &((type *)0)->memberName))

#define VRRP_THREE 3
