

#ifdef  NM_WANTED 
#include "nm.h"  

 t_MMI_INDEX_DESC   vrrp_index_list[] = 
{

 /* vrrpNotificationCntl  */  
{1, "enabled"},
{2, "disabled"},
{-1 ,  NULL  },
 /* vrrpStatus  */  
{1, "enabled"},
{2, "disabled"},
{-1 ,  NULL  },
 /* vrrpOperState  */  
{1, "initialize"},
{2, "backup"},
{3, "master"},
{-1 ,  NULL  },
 /* vrrpOperAdminState  */  
{1, "up"},
{2, "down"},
{-1 ,  NULL  },
 /* vrrpOperAuthType  */  
{1, "noAuthentication"},
{2, "simpleTextPassword"},
{3, "ipAuthenticationHeader"},
{-1 ,  NULL  },
 /* vrrpOperProtocol  */  
{1, "ip"},
{2, "bridge"},
{3, "decnet"},
{4, "other"},
{-1 ,  NULL  },
 /* vrrpTrapAuthErrorType  */  
{1, "invalidAuthType"},
{2, "authTypeMismatch"},
{3, "authFailure"},
{-1 ,  NULL  }

};

/* End_Enum */ 


 static t_MB_DB_Range vrrpRange[] = { 

 { 0 , "vrrpMIB", 1 , 255 , RANGE },
 { 1 , "vrrpMIB", 1 , 255 , RANGE },
 { 2 , "vrrpOperPriority", 0 , 255 , RANGE },
 { 3 , "vrrpOperIpAddrCount", 0 , 255 , RANGE },
 { 4 , "vrrpOperAuthKey", 0, 16 , SIZE },
 { 5 , "vrrpOperAdvertisementInterval", 1 , 255 , RANGE },
 { 6 , "vrrpMIBGroups", 1 , 255 , RANGE },
 {0 ,0 ,0 ,0 ,0 } 

 };

/* End_Range */ 


 t_MIB_DB vrrp[]= {
 /*  0 */ { "vrrpMIB", 1, 0, BRANCH, MIB_READ_ONLY,"1.3.6.1.2.1.68" ,&vrrpRange[ 0 ],NULL},
 /*  1 */ { "vrrpOperations", 7, 2, BRANCH, MIB_READ_ONLY,"1.3.6.1.2.1.68.1" ,NULL,NULL},
 /*  2 */ { "vrrpStatistics", 33, 3, BRANCH, MIB_READ_ONLY,"1.3.6.1.2.1.68.2" ,NULL,NULL},
 /*  3 */ { "vrrpConformance", 5, 4, BRANCH, MIB_READ_ONLY,"1.3.6.1.2.1.68.3" ,NULL,NULL},
 /*  4 */ { "vrrpNotifications", 0, 0, BRANCH, MIB_READ_ONLY,"1.3.6.1.2.1.68.0" ,NULL,NULL},
 /*  5 */ { "vrrpMIBCompliances", 0, 6, BRANCH, MIB_READ_ONLY,"1.3.6.1.2.1.68.3.1" ,NULL,NULL},
 /*  6 */ { "vrrpMIBGroups", 0, 0, BRANCH, MIB_READ_ONLY,"1.3.6.1.2.1.68.3.2" ,&vrrpRange[ 6 ],NULL},
 /*  7 */ { "vrrpNodeVersion", 0, 8, INTEGER, MIB_READ_ONLY,"1.3.6.1.2.1.68.1.1" ,NULL,NULL},
 /*  8 */ { "vrrpNotificationCntl", 0, 9, INTEGER, MIB_READ_WRITE,"1.3.6.1.2.1.68.1.2" ,NULL,&vrrp_index_list[ 0]},
 /*  9 */ { "vrrpStatus", 0, 10, INTEGER, MIB_READ_WRITE,"1.3.6.1.2.1.68.1.3" ,NULL,&vrrp_index_list[ 3]},
 /* 10 */ { "vrrpMaxOperEntries", 0, 11, INTEGER, MIB_READ_WRITE,"1.3.6.1.2.1.68.1.4" ,NULL,NULL},
 /* 11 */ { "vrrpOperTable", 12, 29, TABLE, MIB_NOT_ACCESSIBLE,"1.3.6.1.2.1.68.1.5" ,NULL,NULL},
 /* 12 */ { "vrrpOperEntry", 13, 0, TABLE, MIB_NOT_ACCESSIBLE,"1.3.6.1.2.1.68.1.5.1" ,NULL,NULL},
 /* 13 */ { "vrrpIfIndex", 0, 14, INTEGER, MIB_READ_ONLY,"1.3.6.1.2.1.68.1.5.1.1" ,NULL,NULL},
 /* 14 */ { "vrrpOperVrId", 0, 15, INTEGER, MIB_READ_ONLY,"1.3.6.1.2.1.68.1.5.1.2" ,NULL,NULL},
 /* 15 */ { "vrrpOperVirtualMacAddr", 0, 16, OCTET_STRING, MIB_READ_ONLY,"1.3.6.1.2.1.68.1.5.1.3" ,NULL,NULL},
 /* 16 */ { "vrrpOperState", 0, 17, INTEGER, MIB_READ_ONLY,"1.3.6.1.2.1.68.1.5.1.4" ,NULL,&vrrp_index_list[ 6]},
 /* 17 */ { "vrrpOperAdminState", 0, 18, INTEGER, MIB_READ_CREATE,"1.3.6.1.2.1.68.1.5.1.5" ,NULL,&vrrp_index_list[ 10]},
 /* 18 */ { "vrrpOperPriority", 0, 19, INTEGER, MIB_READ_CREATE,"1.3.6.1.2.1.68.1.5.1.6" ,&vrrpRange[ 2 ],NULL},
 /* 19 */ { "vrrpOperIpAddrCount", 0, 20, INTEGER, MIB_READ_ONLY,"1.3.6.1.2.1.68.1.5.1.7" ,&vrrpRange[ 3 ],NULL},
 /* 20 */ { "vrrpOperMasterIpAddr", 0, 21, IP_ADDRESS, MIB_READ_ONLY,"1.3.6.1.2.1.68.1.5.1.8" ,NULL,NULL},
 /* 21 */ { "vrrpOperPrimaryIpAddr", 0, 22, IP_ADDRESS, MIB_READ_CREATE,"1.3.6.1.2.1.68.1.5.1.9" ,NULL,NULL},
 /* 22 */ { "vrrpOperAuthType", 0, 23, INTEGER, MIB_READ_CREATE,"1.3.6.1.2.1.68.1.5.1.10" ,NULL,&vrrp_index_list[ 13]},
 /* 23 */ { "vrrpOperAuthKey", 0, 24, DISPLAY_STRING, MIB_READ_CREATE,"1.3.6.1.2.1.68.1.5.1.11" ,&vrrpRange[ 4 ],NULL},
 /* 24 */ { "vrrpOperAdvertisementInterval", 0, 25, INTEGER, MIB_READ_CREATE,"1.3.6.1.2.1.68.1.5.1.12" ,&vrrpRange[ 5 ],NULL},
 /* 25 */ { "vrrpOperPreemptMode", 0, 26, INTEGER, MIB_READ_CREATE,"1.3.6.1.2.1.68.1.5.1.13" ,NULL,NULL},
 /* 26 */ { "vrrpOperVirtualRouterUpTime", 0, 27, TIME_TICKS, MIB_READ_ONLY,"1.3.6.1.2.1.68.1.5.1.14" ,NULL,NULL},
 /* 27 */ { "vrrpOperProtocol", 0, 28, INTEGER, MIB_READ_CREATE,"1.3.6.1.2.1.68.1.5.1.15" ,NULL,&vrrp_index_list[ 17]},
 /* 28 */ { "vrrpOperRowStatus", 0, 0, INTEGER, MIB_READ_CREATE,"1.3.6.1.2.1.68.1.5.1.16" ,NULL,NULL},
 /* 29 */ { "vrrpAssoIpAddrTable", 30, 50, TABLE, MIB_NOT_ACCESSIBLE,"1.3.6.1.2.1.68.1.6" ,NULL,NULL},
 /* 30 */ { "vrrpAssoIpAddrEntry", 31, 0, TABLE, MIB_NOT_ACCESSIBLE,"1.3.6.1.2.1.68.1.6.1" ,NULL,NULL},
 /* 31 */ { "vrrpAssoIpAddr", 0, 32, IP_ADDRESS, MIB_NOT_ACCESSIBLE,"1.3.6.1.2.1.68.1.6.1.1" ,NULL,NULL},
 /* 32 */ { "vrrpAssoIpAddrRowStatus", 0, 0, INTEGER, MIB_READ_CREATE,"1.3.6.1.2.1.68.1.6.1.2" ,NULL,NULL},
 /* 33 */ { "vrrpRouterChecksumErrors", 0, 34, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.1" ,NULL,NULL},
 /* 34 */ { "vrrpRouterVersionErrors", 0, 35, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.2" ,NULL,NULL},
 /* 35 */ { "vrrpRouterVrIdErrors", 0, 36, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.3" ,NULL,NULL},
 /* 36 */ { "vrrpRouterStatsTable", 37, 0, TABLE, MIB_NOT_ACCESSIBLE,"1.3.6.1.2.1.68.2.4" ,NULL,NULL},
 /* 37 */ { "vrrpRouterStatsEntry", 38, 0, TABLE, MIB_NOT_ACCESSIBLE,"1.3.6.1.2.1.68.2.4.1" ,NULL,NULL},
 /* 38 */ { "vrrpStatsBecomeMaster", 0, 39, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.1" ,NULL,NULL},
 /* 39 */ { "vrrpStatsAdvertiseRcvd", 0, 40, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.2" ,NULL,NULL},
 /* 40 */ { "vrrpStatsAdvertiseIntervalErrors", 0, 41, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.3" ,NULL,NULL},
 /* 41 */ { "vrrpStatsAuthFailures", 0, 42, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.4" ,NULL,NULL},
 /* 42 */ { "vrrpStatsIpTtlErrors", 0, 43, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.5" ,NULL,NULL},
 /* 43 */ { "vrrpStatsPriorityZeroPktsRcvd", 0, 44, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.6" ,NULL,NULL},
 /* 44 */ { "vrrpStatsPriorityZeroPktsSent", 0, 45, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.7" ,NULL,NULL},
 /* 45 */ { "vrrpStatsInvalidTypePktsRcvd", 0, 46, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.8" ,NULL,NULL},
 /* 46 */ { "vrrpStatsAddressListErrors", 0, 47, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.9" ,NULL,NULL},
 /* 47 */ { "vrrpStatsInvalidAuthType", 0, 48, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.10" ,NULL,NULL},
 /* 48 */ { "vrrpStatsAuthTypeMismatch", 0, 49, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.11" ,NULL,NULL},
 /* 49 */ { "vrrpStatsPacketLengthErrors", 0, 0, COUNTER, MIB_READ_ONLY,"1.3.6.1.2.1.68.2.4.1.12" ,NULL,NULL},
 /* 50 */ { "vrrpTrapPacketSrc", 0, 51, IP_ADDRESS, MIB_ACCESSIBLE_FOR_NOTIFY,"1.3.6.1.2.1.68.1.7" ,NULL,NULL},
 /* 51 */ { "vrrpTrapAuthErrorType", 0, 0, INTEGER, MIB_ACCESSIBLE_FOR_NOTIFY,"1.3.6.1.2.1.68.1.8" ,NULL,&vrrp_index_list[ 22]},
{ "NULL",0,0,0,0,"NULL",NULL,NULL}
};


#endif

