/*<!-- $Id: vrrpcon.h,v 1.3 2017/12/16 11:57:11 siva Exp $-->*/

# ifndef vrrp_OCON_H
# define vrrp_OCON_H
/*
 *  The Constant Declarations for
 *  vrrpOperTable
 */
# define VRRPIFINDEX               (1)
# define VRRPOPERVRID               (2)
# define VRRPOPERVIRTUALMACADDR               (3)
# define VRRPOPERSTATE               (4)
# define VRRPOPERADMINSTATE               (5)
# define VRRPOPERPRIORITY               (6)
# define VRRPOPERIPADDRCOUNT               (7)
# define VRRPOPERMASTERIPADDR               (8)
# define VRRPOPERPRIMARYIPADDR               (9)
# define VRRPOPERAUTHTYPE               (10)
# define VRRPOPERAUTHKEY               (11)
# define VRRPOPERADVERTISEMENTINTERVAL               (12)
# define VRRPOPERPREEMPTMODE               (13)
# define VRRPOPERVIRTUALROUTERUPTIME               (14)
# define VRRPOPERPROTOCOL               (15)
# define VRRPOPERROWSTATUS               (16)

/*
 *  The Constant Declarations for
 *  vrrpAssoIpAddrTable
 */
# define VRRPASSOIPADDR               (1)
# define VRRPASSOIPADDRROWSTATUS               (2)

/*
 *  The Constant Declarations for
 *  vrrpRouterStatsTable
 */
#define VRRPSTATSBECOMEMASTER               (1)
#define VRRPSTATSADVERTISERCVD              (2)
#define VRRPSTATSADVERTISEINTERVALERRORS    (3)
#define VRRPSTATSAUTHFAILURES               (4)
#define VRRPSTATSIPTTLERRORS                (5)
#define VRRPSTATSPRIORITYZEROPKTSRCVD       (6)
#define VRRPSTATSPRIORITYZEROPKTSSENT       (7)
#define VRRPSTATSINVALIDTYPEPKTSRCVD        (8)
#define VRRPSTATSADDRESSLISTERRORS          (9)
#define VRRPSTATSINVALIDAUTHTYPE            (10)
#define VRRPSTATSAUTHTYPEMISMATCH           (11)
#define VRRPSTATSPACKETLENGTHERRORS         (12)
#define VRRPSTATSNEWMASTERREASON            (13)
#define VRRPSTATSPROTOERRREASON             (14)
#define VRRPSTATSADVERTISETXED              (15)
#define VRRPSTATSV2ADVTIGNORED              (16)
#define VRRPSTATSAUTHERRORTYPE              (17)
#define VRRPSTATSV2ADVERTISETXED            (18)

#define VRRPSEMFAIL                         (19)
#define VRRPSENDNDNEIGHBORADVTFAIL          (20)
#define VRRPSENDPACKETTOIPV4FAIL            (24)
#define VRRPSENDPACKETTOIPV6FAIL            (25)
#define VRRPGETIPV4PORTFROMOPERINDEXFAIL    (26)
#define VRRPGETIPV6PORTFROMOPERINDEXFAIL    (27)
#define VRRPSENDGRATARPFAIL                 (28)
#define VRRPETHREGISTERMACADDRFAIL          (29)
#define VRRPNETIP4DELINTFFAIL               (30)
#define VRRPNETIP6DELINTFFAIL               (31)
#define VRRPIPIFJOINMCASTGROUPFAIL          (32)
#define VRRPIPIFLEAVEMCASTGROUPFAIL         (33)
#define VRRPSOCKETFAIL                      (34)
#define PKTPROCESSVALIDATEADVERPACKETFAIL   (35)
#define PKTPROCESSVALIDATEADVERV2PACKETFAIL (36)
#define PKTPROCESSVALIDATEADVERV3PACKETFAIL (37)
#define PKTPROCESSTRANSMITV2PACKETFAIL      (38)
#define PKTPROCESSTRANSMITV3PACKETFAIL      (39)

/*
 *  The Constant Declarations for
 *  vrrpOperations
 */
# define VRRPNODEVERSION                    (1)
# define VRRPNOTIFICATIONCNTL               (2)
# define VRRPSTATUS                         (3)
# define VRRPMAXOPERENTRIES                 (4)
# define VRRPTRAPPACKETSRC                  (7)
# define VRRPTRAPAUTHERRORTYPE              (8)

/*
 *  The Constant Declarations for
 *  vrrpStatistics
 */
# define VRRPROUTERCHECKSUMERRORS               (1)
# define VRRPROUTERVERSIONERRORS                (2)
# define VRRPROUTERVRIDERRORS                   (3)

#endif /*  vrrp_OCON_H  */
