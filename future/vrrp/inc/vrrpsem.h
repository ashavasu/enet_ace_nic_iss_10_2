/********************************************************************
 *                                                                  *
 * $RCSfile: vrrpsem.h,v $
 *                                                                  *
 * $Date: 2007/02/01 15:09:11 $
 *                                                                  *
 * $Revision: 1.2 $
 *                                                                  *
 *******************************************************************/

/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  sem.h                                       |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Core Protocol.                          |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : Initialisation of the Sem Table.              |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +-------------------------------------------------------------------------- *   |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

/* Initialisation of the State Event Table */

#ifdef ELECTION_MODULE 

/* State Event Table Description */
tSemTransitNode SemTable[MAX_STATES][MAX_EVENTS]={
/*EventDesciption       ActionRoutines                          NextState*/
{
                         /* INITIAL_STATE */
{/*STARTUP_EVENT*/     (VrrpFuncPtr)ElectRcvStartupInInitialState, BACKUP_STATE,0 ,0
                                                            /* MASTER_STATE */},
{/*ADVERPKT_EVENT*/    (VrrpFuncPtr)ElectRcvdInvalidEvent,        INITIAL_STATE,0,0},
{/*MASTERDOWN_EVENT*/   (VrrpFuncPtr)ElectRcvdInvalidEvent,       INITIAL_STATE,0,0},
{/*ADVERTIMER_EVENT*/   (VrrpFuncPtr)ElectRcvdInvalidEvent,       INITIAL_STATE,0,0},
{/*SHUTDOWN_EVENT*/     (VrrpFuncPtr)ElectRcvdInvalidEvent,       INITIAL_STATE,0,0}
},
                          /* BACKUP_STATE */
{
{/*STARTUP_EVENT*/      (VrrpFuncPtr)ElectRcvdInvalidEvent,       BACKUP_STATE,0,0},
{/*ADVERPKT_EVENT*/     (VrrpFuncPtr)ElectRcvAdverPktInBackUpState, BACKUP_STATE,0,0},
{/*MASTERDOWN_EVENT*/   (VrrpFuncPtr)ElectRcvMstrDwnTmrInBackUpState, MASTER_STATE,0,0},
{/*ADVERTIMER_EVENT*/   (VrrpFuncPtr)ElectRcvdInvalidEvent,        BACKUP_STATE,0,0},

{/*SHUTDOWN_EVENT*/     (VrrpFuncPtr)ElectRcvShutDownInBackState,  INITIAL_STATE,0,0}
},

                                 /* MASTER_STATE */
{
{/*STARTUP_EVENT*/      (VrrpFuncPtr)ElectRcvdInvalidEvent,        MASTER_STATE,0,0},
{/*ADVERPKT_EVENT*/     (VrrpFuncPtr)ElectRcvAdverPktInMasterState,BACKUP_STATE,0,0
                                                            /* MASTER_STATE */},
{/*MASTERDOWN_EVENT*/   (VrrpFuncPtr)ElectRcvdInvalidEvent,        MASTER_STATE,0,0},
{/*ADVERTIMER_EVENT*/   (VrrpFuncPtr)ElectRcvAdverTmrInMasterState,MASTER_STATE,0,0},
{/*SHUTDOWN_EVENT*/     (VrrpFuncPtr)ElectRcvShutDownInmasterState,INITIAL_STATE,0,0}
}

};

#else 

extern tSemTransitNode SemTable[MAX_STATES][MAX_EVENTS];


#endif
