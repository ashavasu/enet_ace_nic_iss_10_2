/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvr3db.h,v 1.2 2014/03/08 13:40:02 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSVR3DB_H
#define _FSVR3DB_H

UINT1 FsVrrpv3OperationsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsVrrpv3OperationsTrackGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsVrrpv3OperationsTrackGroupIfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsVrrpv3StatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 fsvr3 [] ={1,3,6,1,4,1,29601,2,85};
tSNMP_OID_TYPE fsvr3OID = {9, fsvr3};


UINT4 FsVrrpVersionSupported [ ] ={1,3,6,1,4,1,29601,2,85,1,1,1};
UINT4 FsVrrpv3TraceOption [ ] ={1,3,6,1,4,1,29601,2,85,1,1,2};
UINT4 FsVrrpv3NotificationCntl [ ] ={1,3,6,1,4,1,29601,2,85,1,1,3};
UINT4 FsVrrpv3Status [ ] ={1,3,6,1,4,1,29601,2,85,1,1,4};
UINT4 FsVrrpv3MaxOperEntries [ ] ={1,3,6,1,4,1,29601,2,85,1,1,5};
UINT4 FsVrrpv3MaxAssociatedIpEntries [ ] ={1,3,6,1,4,1,29601,2,85,1,1,6};
UINT4 FsVrrpv3OperationsTrackGroupId [ ] ={1,3,6,1,4,1,29601,2,85,1,2,1,1,1};
UINT4 FsVrrpv3OperationsDecrementPriority [ ] ={1,3,6,1,4,1,29601,2,85,1,2,1,1,2};
UINT4 FsVrrpv3OperationsTrackGroupIndex [ ] ={1,3,6,1,4,1,29601,2,85,1,2,2,1,1};
UINT4 FsVrrpv3OperationsTrackedGroupTrackedLinks [ ] ={1,3,6,1,4,1,29601,2,85,1,2,2,1,2};
UINT4 FsVrrpv3OperationsTrackRowStatus [ ] ={1,3,6,1,4,1,29601,2,85,1,2,2,1,3};
UINT4 FsVrrpv3OperationsTrackGroupIfIndex [ ] ={1,3,6,1,4,1,29601,2,85,1,2,3,1,1};
UINT4 FsVrrpv3OperationsTrackGroupIfRowStatus [ ] ={1,3,6,1,4,1,29601,2,85,1,2,3,1,2};
UINT4 FsVrrpv3StatisticsTxedAdvertisements [ ] ={1,3,6,1,4,1,29601,2,85,1,3,1,1,1};
UINT4 FsVrrpv3StatisticsTxedV2Advertisements [ ] ={1,3,6,1,4,1,29601,2,85,1,3,1,1,2};
UINT4 FsVrrpv3StatisticsV2AdvertiseIgnored [ ] ={1,3,6,1,4,1,29601,2,85,1,3,1,1,3};
UINT4 FsVrrpv3StatisticsMasterAdverInterval [ ] ={1,3,6,1,4,1,29601,2,85,1,3,1,1,4};
UINT4 FsVrrpv3StatisticsSkewTime [ ] ={1,3,6,1,4,1,29601,2,85,1,3,1,1,5};
UINT4 FsVrrpv3StatisticsMasterDownInterval [ ] ={1,3,6,1,4,1,29601,2,85,1,3,1,1,6};




tMbDbEntry fsvr3MibEntry[]= {

{{12,FsVrrpVersionSupported}, NULL, FsVrrpVersionSupportedGet, FsVrrpVersionSupportedSet, FsVrrpVersionSupportedTest, FsVrrpVersionSupportedDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{12,FsVrrpv3TraceOption}, NULL, FsVrrpv3TraceOptionGet, FsVrrpv3TraceOptionSet, FsVrrpv3TraceOptionTest, FsVrrpv3TraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FsVrrpv3NotificationCntl}, NULL, FsVrrpv3NotificationCntlGet, FsVrrpv3NotificationCntlSet, FsVrrpv3NotificationCntlTest, FsVrrpv3NotificationCntlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsVrrpv3Status}, NULL, FsVrrpv3StatusGet, FsVrrpv3StatusSet, FsVrrpv3StatusTest, FsVrrpv3StatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{12,FsVrrpv3MaxOperEntries}, NULL, FsVrrpv3MaxOperEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsVrrpv3MaxAssociatedIpEntries}, NULL, FsVrrpv3MaxAssociatedIpEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,FsVrrpv3OperationsTrackGroupId}, GetNextIndexFsVrrpv3OperationsTable, FsVrrpv3OperationsTrackGroupIdGet, FsVrrpv3OperationsTrackGroupIdSet, FsVrrpv3OperationsTrackGroupIdTest, FsVrrpv3OperationsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVrrpv3OperationsTableINDEX, 3, 0, 0, "0"},

{{14,FsVrrpv3OperationsDecrementPriority}, GetNextIndexFsVrrpv3OperationsTable, FsVrrpv3OperationsDecrementPriorityGet, FsVrrpv3OperationsDecrementPrioritySet, FsVrrpv3OperationsDecrementPriorityTest, FsVrrpv3OperationsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVrrpv3OperationsTableINDEX, 3, 0, 0, "0"},

{{14,FsVrrpv3OperationsTrackGroupIndex}, GetNextIndexFsVrrpv3OperationsTrackGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsVrrpv3OperationsTrackGroupTableINDEX, 1, 0, 0, NULL},

{{14,FsVrrpv3OperationsTrackedGroupTrackedLinks}, GetNextIndexFsVrrpv3OperationsTrackGroupTable, FsVrrpv3OperationsTrackedGroupTrackedLinksGet, FsVrrpv3OperationsTrackedGroupTrackedLinksSet, FsVrrpv3OperationsTrackedGroupTrackedLinksTest, FsVrrpv3OperationsTrackGroupTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVrrpv3OperationsTrackGroupTableINDEX, 1, 0, 0, NULL},

{{14,FsVrrpv3OperationsTrackRowStatus}, GetNextIndexFsVrrpv3OperationsTrackGroupTable, FsVrrpv3OperationsTrackRowStatusGet, FsVrrpv3OperationsTrackRowStatusSet, FsVrrpv3OperationsTrackRowStatusTest, FsVrrpv3OperationsTrackGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVrrpv3OperationsTrackGroupTableINDEX, 1, 0, 1, NULL},

{{14,FsVrrpv3OperationsTrackGroupIfIndex}, GetNextIndexFsVrrpv3OperationsTrackGroupIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVrrpv3OperationsTrackGroupIfTableINDEX, 2, 0, 0, NULL},

{{14,FsVrrpv3OperationsTrackGroupIfRowStatus}, GetNextIndexFsVrrpv3OperationsTrackGroupIfTable, FsVrrpv3OperationsTrackGroupIfRowStatusGet, FsVrrpv3OperationsTrackGroupIfRowStatusSet, FsVrrpv3OperationsTrackGroupIfRowStatusTest, FsVrrpv3OperationsTrackGroupIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVrrpv3OperationsTrackGroupIfTableINDEX, 2, 0, 1, NULL},

{{14,FsVrrpv3StatisticsTxedAdvertisements}, GetNextIndexFsVrrpv3StatisticsTable, FsVrrpv3StatisticsTxedAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsVrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{14,FsVrrpv3StatisticsTxedV2Advertisements}, GetNextIndexFsVrrpv3StatisticsTable, FsVrrpv3StatisticsTxedV2AdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsVrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{14,FsVrrpv3StatisticsV2AdvertiseIgnored}, GetNextIndexFsVrrpv3StatisticsTable, FsVrrpv3StatisticsV2AdvertiseIgnoredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, FsVrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{14,FsVrrpv3StatisticsMasterAdverInterval}, GetNextIndexFsVrrpv3StatisticsTable, FsVrrpv3StatisticsMasterAdverIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{14,FsVrrpv3StatisticsSkewTime}, GetNextIndexFsVrrpv3StatisticsTable, FsVrrpv3StatisticsSkewTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{14,FsVrrpv3StatisticsMasterDownInterval}, GetNextIndexFsVrrpv3StatisticsTable, FsVrrpv3StatisticsMasterDownIntervalGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsVrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},
};
tMibData fsvr3Entry = { 19, fsvr3MibEntry };

#endif /* _FSVR3DB_H */

