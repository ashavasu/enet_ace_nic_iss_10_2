
# ifndef vrrp_OGP_H
# define vrrp_OGP_H

 /* The Definitions of the OGP Index Constants.  */
# define SNMP_OGP_INDEX_VRRPOPERTABLE               (0)
# define SNMP_OGP_INDEX_VRRPASSOIPADDRTABLE               (1)
# define SNMP_OGP_INDEX_VRRPROUTERSTATSTABLE               (2)
# define SNMP_OGP_INDEX_VRRPOPERATIONS               (3)
# define SNMP_OGP_INDEX_VRRPSTATISTICS               (4)

#endif /*  vrrp_OGP_H  */
