
/*  $Id: vrrpdef.h,v 1.27 2017/12/16 11:57:11 siva Exp $ */
/********************************************************************
 *                                                                  *
 * $RCSfile: vrrpdef.h,v $
 *                                                                  *
 * $Date: 2017/12/16 11:57:11 $
 *                                                                  *
 * $Revision: 1.27 $
 *                                                                  *
 *******************************************************************/

/* 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrpdef.h                                    |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.                                 |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Core Protocol.                          |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : Constants used in VRRP Core Protocol.         |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#ifndef _VRRP_DEF_H_
#define _VRRP_DEF_H_

/* Constants used throughout the Protocol */
#define   ROW_NOT_CREATED        0
#define VRRP_MAX_PRFX_LEN     128
#define VRRP_PRIMARY          1
#define VRRP_SECONDARY        2

# define VRRP_ENABLE                                1
# define VRRP_DISABLE                               2
#ifndef NULL
#define NULL    (0)
#endif

# define VRRP_AUTHDEPRECATE_ENABLE             1
# define VRRP_AUTHDEPRECATE_DISABLE            2

# define VRRP_SEM_NAME                        "Vrrp"

# define ADMINUP                                1
# define ADMINDOWN                                2

# define VRRP_OR                               TMO_OR 
# define VRRP_AND                              TMO_AND 

# define VRRP_MCAST_ADDR                       0xE0000012 /*224.0.0.18*/

#define  VRRP_KERN_ADDIP                       1
#define  VRRP_KERN_DELIP                       2


#define VRRP_PKT_RXED_EVENT                      TMO_TASK_QUE_11_ENQ_EVENT
#define VRRP_LINK_STATE_CHG_EVENT                TMO_TASK_QUE_12_ENQ_EVENT
#define VRRP_IP_ADDR_CHG_EVENT                   TMO_TASK_QUE_13_ENQ_EVENT
#define VRRP_IPV6_PKT_RXED_EVENT                 TMO_TASK_QUE_14_ENQ_EVENT
#define VRRP_TMR_EXP_EVENT                       TIMER_EVENT
#define VRRP_STARTUP_EVENT                       0x00000010 
#define VRRP_SHUTDOWN_EVENT                      0x00000100 
#define VRRP_RM_PKT_EVENT                        0x00000008
#define VRRP_TMR                                 TIMER_EVENT

#define VRRP_ENET                               1 

/* Constants in SNMP Low Level */

# define MINPRIORITY                            1  
# define MAXPRIORITY                            254  
# define MACADDRSIZE                            6
# define HMAKEYSIZE                             16

# define AUTHKEYSIZE                            16 /*Maximum possible size
                                                    *of authentication key*/

#define VRRP_OPER_MEMPOOL_ID     VRRPMemPoolIds[MAX_VRRP_OPER_ENTRY_SIZING_ID]
#define VRRP_TIMER_MEMPOOL_ID    VRRPMemPoolIds[MAX_VRRP_TIMER_ENTRY_SIZING_ID]
#define VRRP_PKTQ_MEMPOOL_ID     VRRPMemPoolIds[MAX_VRRP_QUEUE_DEPTH_SIZING_ID]
#define VRRP_AUTHKEY_MEMPOOL_ID  VRRPMemPoolIds[MAX_VRRP_AUTHKEY_ENTRY_SIZING_ID]
#define VRRP_RM_MSG_MEMPOOL_ID   VRRPMemPoolIds[MAX_VRRP_RM_QUE_DEPTH_SIZING_ID]
#define VRRP_DYN_MSG_MEMPOOL_ID  VRRPMemPoolIds[MAX_VRRP_DYN_MSG_SIZE_SIZING_ID]
#define VRRP_ASSOC_IP_MEMPOOL_ID VRRPMemPoolIds[MAX_VRRP_ASSO_IP_ENTRY_SIZING_ID]
#define VRRP_IPV4_BUF_MEMPOOL_ID VRRPMemPoolIds[MAX_VRRP_TX_IPV4_BUF_SIZING_ID]
#define VRRP_IPV6_BUF_MEMPOOL_ID VRRPMemPoolIds[MAX_VRRP_TX_IPV6_BUF_SIZING_ID]
#define VRRP_IPV4_PKT_MEMPOOL_ID VRRPMemPoolIds[MAX_VRRP_TX_RX_IPV4_PKT_SIZING_ID]
#define VRRP_IPV6_PKT_MEMPOOL_ID VRRPMemPoolIds[MAX_VRRP_TX_RX_IPV6_PKT_SIZING_ID]
#define VRRP_TRACK_MEMPOOL_ID    VRRPMemPoolIds[MAX_VRRP_TRACK_GROUP_SIZING_ID]
#define VRRP_TRACK_IF_MEMPOOL_ID VRRPMemPoolIds[MAX_VRRP_TRACK_IF_GROUP_SIZING_ID]

# define DEFAULT_PRIORITY         100
# define PRIORITYBIT              0x02
# define AUTHKEYBIT               0x04
# define DEFAULT_PROTOCOL         1

#define ADVERTISEMENTINTL                   1 
#define MAX_ADVERTISEMENTINTL             255 

#define ADVERTISEMENTINTLINMSEC           100
#define MAX_ADVERTISEMENTINTLINMSEC    255000

#define V2_ADVERTISEMENTINTLINMSEC       1000
#define V2_MAX_ADVERTISEMENTINTLINMSEC 255000

#define V3_ADVERTISEMENTINTLINCS            1
#define V3_MAX_ADVERTISEMENTINTLINCS     4095

#define V3_ADVERTISEMENTINTLINMSEC         10
#define V3_MAX_ADVERTISEMENTINTLINMSEC  40950

#define VRRP_GLOB_STATS_SIZE 1000
#define VRRP_GLOB_CONFIG_SIZE 1000
#define VRRP_MAX_TABLE_SIZE 20000
#define VRRP_ASSOIP_TABLE_SIZE 10000
#define VRRP_MAX_OPERATIONAL_TABLE_SIZE     20000

/* Constants in Election Start Here */

# define MAX_EVENTS              5
# define MAX_STATES              3

/* Possible Event of  the State Event Machine */
# define         STARTUP_EVENT                   0
# define         ADVERPKT_EVENT                  1
# define         MASTERDOWN_EVENT                2
# define         ADVERTIMER_EVENT                3
# define         SHUTDOWN_EVENT                  4

/* Timers Used */

# define         ADVER_TIMER                     3
# define         MASTERDOWN_TIMER                2

# define         VRRP_TRUE               1
# define         VRRP_FALSE              0
# define         ERROR_OCCURED           0

/* Constants in Packet Processing Start Here */

# define    VRRP_ADVERTISEMENT           1
# define    VRRP_IPOWNER_PRIORITY         255

#define    VRRP_ADVERTISEMENT_PKT       0x45 /*Means IP header with VRRP Pkt*/
#define    VRRP_STATECHG_PKT            2
#define    VRRP_LINKDOWN_PKT            3
#define    VRRP_LINKUP_PKT              4
#define    VRRP_IFACE_DEL_PKT           5

# define    NO_AUTHENTICATION_MIB_VAL            1
# define    SIMPLE_TEXT_AUTHENTICATION_MIB_VAL   2
# define    HMAC_IP_AUTHENTICATION_MIB_VAL       3
# define    NO_AUTHENTICATION_PROT_VAL            0
# define    SIMPLE_TEXT_AUTHENTICATION_PROT_VAL   1
# define    HMAC_IP_AUTHENTICATION_PROT_VAL       2
# define    IPADDRESS_COUNT_OFFSET       3
# define    VRID_OFFSET                  1
# define    CHECKSUM_OFFSET              6

#define   MIN_ADVERPACKET_SIZE          (VRRP_HEADER_LEN + IPADDRESS_SIZE + \
                                          TEXT_AUTHKEY_SIZE)
#define    MIN_V2_ADVERPACKET_SIZE        MIN_ADVERPACKET_SIZE
#define    MIN_V3_IPV4_ADVERPACKET_SIZE   (VRRP_HEADER_LEN + IPVX_IPV4_ADDR_LEN)
#define    MIN_V3_IPV6_ADVERPACKET_SIZE   (VRRP_HEADER_LEN + IPVX_IPV6_ADDR_LEN)
#define   MAX_ADVERPACKET_SIZE        (VRRP_HEADER_LEN + \
                                        MAX_ASSO_ADDR_ENTRY * IPADDRESS_SIZE +\
                                        TEXT_AUTHKEY_SIZE)
# define   ONE                          1
# define   VRRP_SET                          1 

/* Indexes Of MIB Variables */

#define   STATS_BECOMEMASTER                   1
#define   STATS_ADVERTISEMENT_RCVD             2
#define   STATS_VRRPCHECKSUM_ERRORS            3
#define   STATS_VERSION_ERRORS                 4
#define   STATS_VRID_ERRORS                    5
#define   STATS_ADVERTISEINTERVAL_ERRORS       6
#define   STATS_SECURITYVIOLATIONS             7
#define   STATS_IPTTL_ERRORS                   8
#define   STATS_PRIORITYZEROPKTS_RCVD          9
#define   STATS_PRIORITYZEROPKTS_SENT          10
#define   STATS_INVALIDTYPEPKTS_RCVD           11
#define   STATS_MINSIZE_ERRORS                 12
#define   STATS_ADDRESS_LIST_ERRORS            13
#define   STATS_UNKNOWN_AUTH_TYPE              14
#define   STATS_AUTH_TYPE_ERRORS               15

#define   IP_VRRP_QUEUE            (UINT1 *) "IpVrrp"

#define   VRRP_QUEUE_DEPTH                     IP_DEV_MAX_L3VLAN_INTF * 4

#define   VRRP_RM_PKT_QUEUE                   "VRRPRMQ"
#define   VRRP_RM_QUE_DEPTH                   10

/* Timer Constants Start Here */
#define   TIMER_EVENT                          0x00000001
#define   SRC_IP_OFFSET                        12
#define   DST_IP_OFFSET                        16
#define   TTL_OFFSET                           8

#define   SRC_IPV6_OFFSET                      8
#define   DST_IPV6_OFFSET                     24
#define   IPV6_TTL_OFFSET                      7

#define   TTL_SIZE                             1
#define   VRRP_TTL                             255

#define   VRRP_STATUS_INVLD                       99
#define   VRRP_INVLD_IP_ADDR_1            0x00000000
#define   VRRP_INVLD_IP_ADDR_2            0xffffffff

#define VRRPROUTCHECKSUMERRORS           (21)
#define VRRPROUTVERSIONERRORS           (22)
#define VRRPROUTVRIDERRORS          (23)

#define VRRP_ALLOC  MEM_MALLOC
#define VRRP_FREE   MEM_FREE
#define VRRP_MEMSET MEMSET
#define VRRP_MEMCMP MEMCMP
#define VRRP_MEMCPY MEMCPY
#define VRRP_MALLOC malloc

#define VRRP_STRLEN                      strlen

#define VRRP_TASK_NAME          (UINT1 *) "VRRP"
#define VRRP_TASK_PRIORITY      20
#define VRRP_TASK_MODE          OSIX_DEFAULT_TASK_MODE
#define VRRP_TASK_STACK_SIZE    OSIX_DEFAULT_STACK_SIZE
#define VRRP_MAX_HW_ADDR_LEN        6
#define VRRP_IP_ADDR_LEN            4
/*********************************************************************/

#define TMO_TASK_QUE_00         0
#define TMO_TASK_QUE_01         1
#define TMO_TASK_QUE_02         2
#define TMO_TASK_QUE_03         3
#define TMO_TASK_QUE_04         4
#define TMO_TASK_QUE_05         5
#define TMO_TASK_QUE_06         6
#define TMO_TASK_QUE_07         7
#define TMO_TASK_QUE_08         8
#define TMO_TASK_QUE_09         9
#define TMO_TASK_QUE_10        10
#define TMO_TASK_QUE_11        11
#define TMO_TASK_QUE_12        12
#define TMO_TASK_QUE_13        13
#define TMO_TASK_QUE_14        14
#define TMO_TASK_QUE_15        15

#define TMO_ANY_EVENT             (~0)
#define TMO_TASK_QUE_00_ENQ_EVENT (0x80000000)
#define TMO_TASK_QUE_01_ENQ_EVENT (0x40000000)
#define TMO_TASK_QUE_02_ENQ_EVENT (0x20000000)
#define TMO_TASK_QUE_03_ENQ_EVENT (0x10000000)
#define TMO_TASK_QUE_04_ENQ_EVENT (0x08000000)
#define TMO_TASK_QUE_05_ENQ_EVENT (0x04000000)
#define TMO_TASK_QUE_06_ENQ_EVENT (0x02000000)
#define TMO_TASK_QUE_07_ENQ_EVENT (0x01000000)
#define TMO_TASK_QUE_08_ENQ_EVENT (0x00800000)
#define TMO_TASK_QUE_09_ENQ_EVENT (0x00400000)
#define TMO_TASK_QUE_10_ENQ_EVENT (0x00200000)
#define TMO_TASK_QUE_11_ENQ_EVENT (0x00100000)
#define TMO_TASK_QUE_12_ENQ_EVENT (0x00080000)
#define TMO_TASK_QUE_13_ENQ_EVENT (0x00040000)
#define TMO_TASK_QUE_14_ENQ_EVENT (0x00020000)
#define TMO_TASK_QUE_15_ENQ_EVENT (0x00010000)
#define TMO_TIMER_00_EXP_EVENT    (0x00008000)
#define TMO_TIMER_01_EXP_EVENT    (0x00004000)
#define TMO_TIMER_02_EXP_EVENT    (0x00002000)
#define TMO_TIMER_03_EXP_EVENT    (0x00001000)

#define VRRP_RB_GREATER           1
#define VRRP_RB_LESSER           -1
#define VRRP_RB_EQUAL             0

#define   NP_UPDATED            1
#define   NP_NOT_UPDATED        2

#define VRRP_MIN_VRID           1
#define VRRP_MAX_VRID           255

#define VRRP_TRACK_IF_NOT_KNOWN      0
#define VRRP_TRACK_IF_UP             1
#define VRRP_TRACK_IF_DOWN           2

#define VRRP_GROUP_NOT_KNOWN         0
#define VRRP_GROUP_UP                1
#define VRRP_GROUP_DOWN              2

#ifdef L2RED_WANTED
#define VRRP_GET_NODE_STATUS()  gVrrpRedGblInfo.u1NodeStatus
#else
#define VRRP_GET_NODE_STATUS()  RM_ACTIVE
#endif

#ifdef L2RED_WANTED
#define VRRP_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define VRRP_GET_RMNODE_STATUS()  RM_ACTIVE
#endif

#ifdef RM_WANTED
#define   VRRP_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? VRRP_OK : VRRP_NOT_OK)
#else
#define   VRRP_IS_NP_PROGRAMMING_ALLOWED() VRRP_OK
#endif

/* Porting ipv4 to ipvx */
#define VRRP_IPVX_ADDR_FMLY_IPV4                       IPVX_ADDR_FMLY_IPV4
#define VRRP_IPVX_ADDR_FMLY_IPV6                       IPVX_ADDR_FMLY_IPV6
#define VRRP_IPVX_ADDR_CLEAR(Addr)                     IPVX_ADDR_CLEAR(&Addr)
#define VRRP_IPVX_ADDR_INIT_IPVX(Addr,u1Afi,pu1Addr)   IPVX_ADDR_INIT(Addr,u1Afi,pu1Addr)
#define VRRP_IPVX_COPY(DestAddr, SrcAddr)              IPVX_ADDR_COPY(&DestAddr,&SrcAddr)
#define VRRP_IPVX_COMPARE(Addr1,Addr2)                 IPVX_ADDR_COMPARE(Addr1,Addr2)
#define VRRP_IPVX_ADDR_INIT_IPV4(Addr1,pu1Addr)        IPVX_ADDR_INIT_IPV4(Addr1,IPVX_ADDR_FMLY_IPV4,pu1Addr)
#define VRRP_IPVX_ADDR_INIT_IPV6(Addr1,pu1Addr)        IPVX_ADDR_INIT_IPV6(Addr1,IPVX_ADDR_FMLY_IPV6,pu1Addr)
#define VRRP_IPVX_COPY_TO_IPVX(ADDR1,u4Addr)\
{\
    u4Addr = OSIX_HTONL (u4Addr);\
    IPVX_ADDR_INIT_FROMV4(ADDR1, u4Addr);\
    u4Addr = OSIX_NTOHL (u4Addr);\
}
#define VRRP_IPVX_COPY_TO_IPV4(u4DestAddr,SrcAddr)\
{\
    MEMCPY (&u4DestAddr, SrcAddr.au1Addr, sizeof (UINT4));\
    u4DestAddr = OSIX_NTOHL (u4DestAddr);\
}
#define VRRP_IPVX_COPY_TO_OCTET_STR(Addr,pOctetStr)    IPVX_IPVX_ADDR_TO_OCT_STR(&Addr,pOctetStr)

#define VRRP_IPV6_THIRD_WORD_OFFSET          12

#define VRRP_IPVX_PRINT_ADDR(Addr,u1Afi)\
    ((u1Afi == VRRP_IPVX_ADDR_FMLY_IPV4) ? VrrpPrintIpv4Address(Addr) : VrrpPrintIpv6Address(Addr))

#define VRRP_ADDRTYPE_TO_STR(ac1AddrType, i4OperIndex) \
{\
    if(OPERADDRTYPE (i4OperIndex) == VRRP_IPVX_ADDR_FMLY_IPV4)\
    {\
         STRNCPY (ac1AddrType, "IPV4",sizeof(ac1AddrType));\
    }\
    else\
    {\
       STRNCPY (ac1AddrType, "IPV6",sizeof(ac1AddrType));\
    }\
\
}

#define VRRP_ADDRTYPE_TO_STRING(ac1AddrType, i4AddrType) \
{\
    if(i4AddrType==1)\
    {\
         STRNCPY (ac1AddrType, "IPV4",sizeof(ac1AddrType));\
    }\
    else\
    {\
       STRNCPY (ac1AddrType, "IPV6",sizeof(ac1AddrType));\
    }\
\
}

/* MACRO for all the objects in VRRP table */
#define VRRP_MIB_OPER_IF_INDEX               1
#define VRRP_MIB_OPER_VRID                   2
#define VRRP_MIB_OPER_ADDR_TYPE              3
#define VRRP_MIB_OPER_MASTER_IP              4
#define VRRP_MIB_OPER_PRIM_IP                5
#define VRRP_MIB_OPER_VIRT_MAC               6
#define VRRP_MIB_OPER_STATUS                 7
#define VRRP_MIB_OPER_PRIORITY               8
#define VRRP_MIB_OPER_ADDR_COUNT             9
#define VRRP_MIB_OPER_ADVT_INT               10
#define VRRP_MIB_OPER_PREMP_MODE             11
#define VRRP_MIB_OPER_ACCEPT_MODE            12
#define VRRP_MIB_OPER_UP_TIME                13
#define VRRP_MIB_OPER_ROW_STATUS             14
#define VRRP_MIB_OPER_ADMIN_STATE            15
#define VRRP_MIB_OPER_AUTH_TYPE              16
#define VRRP_MIB_OPER_AUTH_KEY               17
#define VRRP_MIB_OPER_PROTOCOL               18
#define VRRP_MIB_OPER_TRACK_GRP_ID           19
#define VRRP_MIB_OPER_DEC_PRIORITY           20
#define VRRP_MIB_OPER_ADMIN_PRIO             21
#define VRRP_MIB_OPER_ADVT_INT_MSEC          22

/* Associated IP address table */
#define VRRP_MIB_ASSO_IP_ADDR                23
#define VRRP_MIB_ASSO_ROW_STATUS             24

/* Statistics entry*/
#define VRRP_MIB_STAT_VRRPSEMFAIL                          70
#define VRRP_MIB_STAT_VRRPSENDNDNEIGHBORADVTFAIL           71
#define VRRP_MIB_STAT_VRRPSENDPACKETTOIPV4FAIL             72
#define VRRP_MIB_STAT_VRRPGETIPV4PORTFROMOPERINDEXFAIL     73
#define VRRP_MIB_STAT_VRRPSENDGRATARPFAIL                  74
#define VRRP_MIB_STAT_VRRPETHREGISTERMACADDRFAIL           75
#define VRRP_MIB_STAT_VRRPNETIP4DELINTFFAIL                76
#define VRRP_MIB_STAT_VRRPIPIFJOINMCASTGROUPFAIL           77
#define VRRP_MIB_STAT_VRRPIPIFLEAVEMCASTGROUPFAIL          78
#define VRRP_MIB_STAT_VRRPSOCKETFAIL                       79
#define VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERPACKETFAIL    80
#define VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERV2PACKETFAIL  81
#define VRRP_MIB_STAT_PKTPROCESSVALIDATEADVERV3PACKETFAIL  82
#define VRRP_MIB_STAT_PKTPROCESSTRANSMITV2PACKETFAIL       83
#define VRRP_MIB_STAT_PKTPROCESSTRANSMITV3PACKETFAIL       84
#define VRRP_MIB_STAT_VRRPSENDPACKETTOIPV6FAIL             85
#define VRRP_MIB_STAT_VRRPGETIPV6PORTFROMOPERINDEXFAIL     86
#define VRRP_MIB_STAT_VRRPNETIP6DELINTFFAIL                87

#define VRRP_MIB_STAT_MASTER_TRANS           25
#define VRRP_MIB_STAT_MASTER_REASON          26
#define VRRP_MIB_STAT_RCVD_ADVT              27
#define VRRP_MIB_STAT_ADVT_INT_ERR           28
#define VRRP_MIB_STAT_IP_TTL_ERR             29
#define VRRP_MIB_STAT_PROTO_ERR_REASON       30
#define VRRP_MIB_STAT_RCVD_ZERO_PRI          31
#define VRRP_MIB_STAT_SENT_ZERO_PRI          32
#define VRRP_MIB_STAT_RCVD_INVD_TYPE         33
#define VRRP_MIB_STAT_ADDR_LIST_ERR          34
#define VRRP_MIB_STAT_PKT_LEN_ERR            35
#define VRRP_MIB_STAT_ROW_DISC_TIME          36
#define VRRP_MIB_STAT_REFRESH_RATE           37
#define VRRP_MIB_STAT_TXED_ADVT              38
#define VRRP_MIB_STAT_MASTER_ADVT_INT        39
#define VRRP_MIB_STAT_SKEW_TIME              40
#define VRRP_MIB_STAT_MASTER_DOWN_INT        41
#define VRRP_MIB_STAT_AUTH_FAILURES          42
#define VRRP_MIB_STAT_INVALID_AUTH           43
#define VRRP_MIB_STAT_AUTH_TYPE_MISMATCH     44
#define VRRP_MIB_STAT_V2_ADVT_IGNORED        45
#define VRRP_MIB_STAT_V3_TXED_ADVT           46

/* Macros for Track Group Table */
#define VRRP_MIB_TRACK_GROUP_INDEX           47
#define VRRP_MIB_TRACK_GROUP_LINKS           48
#define VRRP_MIB_TRACK_ROW_STATUS            49

/* Macros for Track Group If Table */
#define VRRP_MIB_TRACK_IF_ROW_STATUS         50

/* Macros for Scalars */
#define VRRP_MIB_VERSION                     51
#define VRRP_MIB_NOTIF_CTRL                  52
#define VRRP_MIB_GBL_STATUS                  53
#define VRRP_MIB_MAX_OPER_ENTRIES            54
#define VRRP_MIB_MAX_ASSOC_ENTRIES           55
#define VRRP_MIB_TRACE_OPTION                56
#define VRRP_MIB_AUTH_DEPRECATE              57
#define VRRP_MIB_CHECKSUM_ERRORS             58
#define VRRP_MIB_VERSION_ERRORS              59
#define VRRP_MIB_VRID_ERRORS                 60
#define VRRP_MIB_GBL_DISC_TIME               61

/* Events for Track Group */
#define VRRP_TRACK_DELETE_EVENT               1
#define VRRP_TRACK_NIS_EVENT                  2 
#define VRRP_TRACK_LINK_STATE_EVENT           3
#define VRRP_TRACK_IF_CHG_EVENT               4

/* Virtual Mac Location */
#define VRID_LOC_IN_MAC                       5

/* Macros for Proto Error Reason MIB Variable */
#define VRRP_NO_ERROR                         0
#define VRRP_TTL_ERROR                        1
#define VRRP_VERSION_ERROR                    2
#define VRRP_CHECKSUM_ERROR                   3
#define VRRP_VRID_ERROR                       4

#define VRRP_ADVT_INT_ERROR                   5
#define VRRP_INVALID_TYPE_ERROR               6
#define VRRP_ADDRESS_LIST_ERROR               7
#define VRRP_PACKET_LENGTH_ERROR              8

#define VRRP_ZERO                             0
#define VRRP_ONE                              1
#define VRRP_TWO                              2

/* New Master Reasons */
#define VRRP_MASTER_REASON_NOT_MASTER         0
#define VRRP_MASTER_REASON_PRIORITY           1
#define VRRP_MASTER_REASON_PREEMPTED          2
#define VRRP_MASTER_REASON_NO_RESPONSE        3

/* Auth Error Type */
#define VRRP_AUTH_NO_ERROR                    0
#define VRRP_INVALID_AUTH_TYPE_ERROR          1
#define VRRP_AUTH_TYPE_MISMATCH_ERROR         2
#define VRRP_AUTH_FAILURE_ERROR               3

/* VRRP High Availability Version Supported */
#define VRRP_HA_VERSION                       1

#define VRRP_U8_STR_LENGTH                    21

#define VRRP_CLI_TIME_STRING                   9

#endif
