/*  $Id: vrrpsz.h,v 1.7 2014/03/11 14:22:19 siva Exp $ */
enum {
    MAX_VRRP_ASSO_IP_ENTRY_SIZING_ID,
    MAX_VRRP_AUTHKEY_ENTRY_SIZING_ID,
    MAX_VRRP_DYN_MSG_SIZE_SIZING_ID,
    MAX_VRRP_OPER_ENTRY_SIZING_ID,
    MAX_VRRP_QUEUE_DEPTH_SIZING_ID,
    MAX_VRRP_RM_QUE_DEPTH_SIZING_ID,
    MAX_VRRP_TIMER_ENTRY_SIZING_ID,
    MAX_VRRP_TRACK_GROUP_SIZING_ID,
    MAX_VRRP_TRACK_IF_GROUP_SIZING_ID,
    MAX_VRRP_TX_IPV4_BUF_SIZING_ID,
    MAX_VRRP_TX_IPV6_BUF_SIZING_ID,
    MAX_VRRP_TX_RX_IPV4_PKT_SIZING_ID,
    MAX_VRRP_TX_RX_IPV6_PKT_SIZING_ID,
    VRRP_MAX_SIZING_ID
};


#ifdef  _VRRPSZ_C
tMemPoolId VRRPMemPoolIds[ VRRP_MAX_SIZING_ID];
INT4  VrrpSizingMemCreateMemPools(VOID);
VOID  VrrpSizingMemDeleteMemPools(VOID);
INT4  VrrpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _VRRPSZ_C  */
extern tMemPoolId VRRPMemPoolIds[ ];
extern INT4  VrrpSizingMemCreateMemPools(VOID);
extern VOID  VrrpSizingMemDeleteMemPools(VOID);
extern INT4  VrrpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#endif /*  _VRRPSZ_C  */


#ifdef  _VRRPSZ_C
tFsModSizingParams FsVRRPSizingParams [] = {
{ "tIpAddrTable", "MAX_VRRP_ASSO_IP_ENTRY", sizeof(tIpAddrTable),MAX_VRRP_ASSO_IP_ENTRY, MAX_VRRP_ASSO_IP_ENTRY,0 },
{ "UINT1[VRRP_MAX_AUTH_KEY_SIZE]", "MAX_VRRP_AUTHKEY_ENTRY", sizeof(UINT1[VRRP_MAX_AUTH_KEY_SIZE]),MAX_VRRP_AUTHKEY_ENTRY, MAX_VRRP_AUTHKEY_ENTRY,0 },
{ "tVrrpRedTable", "MAX_VRRP_DYN_MSG_SIZE", sizeof(tVrrpRedTable),MAX_VRRP_DYN_MSG_SIZE, MAX_VRRP_DYN_MSG_SIZE,0 },
{ "tVrrpOperTable", "MAX_VRRP_OPER_ENTRY", sizeof(tVrrpOperTable),MAX_VRRP_OPER_ENTRY, MAX_VRRP_OPER_ENTRY,0 },
{ "tVrrpIpMsg", "MAX_VRRP_QUEUE_DEPTH", sizeof(tVrrpIpMsg),MAX_VRRP_QUEUE_DEPTH, MAX_VRRP_QUEUE_DEPTH,0 },
{ "tVrrpRmMsg", "MAX_VRRP_RM_QUE_DEPTH", sizeof(tVrrpRmMsg),MAX_VRRP_RM_QUE_DEPTH, MAX_VRRP_RM_QUE_DEPTH,0 },
{ "tTmrAppTimer", "MAX_VRRP_TIMER_ENTRY", sizeof(tTmrAppTimer),MAX_VRRP_TIMER_ENTRY, MAX_VRRP_TIMER_ENTRY,0 },
{ "tVrrpTrackGroupTable", "MAX_VRRP_TRACK_GROUP", sizeof(tVrrpTrackGroupTable),MAX_VRRP_TRACK_GROUP, MAX_VRRP_TRACK_GROUP,0 },
{ "tVrrpTrackGroupIfTable", "MAX_VRRP_TRACK_IF_GROUP", sizeof(tVrrpTrackGroupIfTable),MAX_VRRP_TRACK_IF_GROUP, MAX_VRRP_TRACK_IF_GROUP,0 },
{ "tVrrpTxIpv4BufSize", "MAX_VRRP_TX_IPV4_BUF", sizeof(tVrrpTxIpv4BufSize),MAX_VRRP_TX_IPV4_BUF, MAX_VRRP_TX_IPV4_BUF,0 },
{ "tVrrpTxIpv6BufSize", "MAX_VRRP_TX_IPV6_BUF", sizeof(tVrrpTxIpv6BufSize),MAX_VRRP_TX_IPV6_BUF, MAX_VRRP_TX_IPV6_BUF,0 },
{ "tVrrpTxRxIpv4PktSize", "MAX_VRRP_TX_RX_IPV4_PKT", sizeof(tVrrpTxRxIpv4PktSize),MAX_VRRP_TX_RX_IPV4_PKT, MAX_VRRP_TX_RX_IPV4_PKT,0 },
{ "tVrrpTxRxIpv6PktSize", "MAX_VRRP_TX_RX_IPV6_PKT", sizeof(tVrrpTxRxIpv6PktSize),MAX_VRRP_TX_RX_IPV6_PKT, MAX_VRRP_TX_RX_IPV6_PKT,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _VRRPSZ_C  */
extern tFsModSizingParams FsVRRPSizingParams [];
#endif /*  _VRRPSZ_C  */


