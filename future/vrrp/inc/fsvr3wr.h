/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvr3wr.h,v 1.2 2014/03/08 13:40:02 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

#ifndef _FSVR3WR_H
#define _FSVR3WR_H

VOID RegisterFSVR3(VOID);

VOID UnRegisterFSVR3(VOID);
INT4 FsVrrpVersionSupportedGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3TraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3NotificationCntlGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3StatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3MaxOperEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3MaxAssociatedIpEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpVersionSupportedSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3TraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3NotificationCntlSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3StatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpVersionSupportedTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3TraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3NotificationCntlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3StatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpVersionSupportedDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsVrrpv3TraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsVrrpv3NotificationCntlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsVrrpv3StatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsVrrpv3OperationsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVrrpv3OperationsTrackGroupIdGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsDecrementPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackGroupIdSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsDecrementPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackGroupIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsDecrementPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsVrrpv3OperationsTrackGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVrrpv3OperationsTrackedGroupTrackedLinksGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackedGroupTrackedLinksSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackedGroupTrackedLinksTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsVrrpv3OperationsTrackGroupIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVrrpv3OperationsTrackGroupIfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackGroupIfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackGroupIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3OperationsTrackGroupIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsVrrpv3StatisticsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVrrpv3StatisticsTxedAdvertisementsGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3StatisticsTxedV2AdvertisementsGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3StatisticsV2AdvertiseIgnoredGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3StatisticsMasterAdverIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3StatisticsSkewTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpv3StatisticsMasterDownIntervalGet(tSnmpIndex *, tRetVal *);
#endif /* _FSVR3WR_H */
