/*  $Id: vrrpglob.h,v 1.9 2014/03/11 14:22:19 siva Exp $ */

/********************************************************************
 *                                                                  *
 * $RCSfile: vrrpglob.h,v $
 *                                                                  *
 * $Date: 2014/03/11 14:22:19 $
 *                                                                  *
 * $Revision: 1.9 $
 *                                                                  *
 *******************************************************************/

/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrpglobal.h                                 |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Core Protocol.                          |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : Global definitions of VRRP Core Protocol.     |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

/* Global definitions Start Here */

#ifndef _VRRP_GLOBAL_H_
#define _VRRP_GLOBAL_H_
#ifdef  INIT_MODULE  

   tVrrpIPvXAddr gVrrpZeroIpAddr;
   tVrrpIPvXAddr gVrrpV4DestMCastAddr;
   tVrrpIPvXAddr gVrrpV6DestMCastAddr;
   tVrrpOperTable *gpVrrpOperTable;                     /*  Vrrp Global Oper Table */
  
   INT4  gi4VrrpIpv4Socket;                             /*   IPv4 Socket Descriptor  */
   INT4  gi4VrrpIpv6Socket;                             /*   IPv6 Socket Descriptor */
   INT4  gi4VrrpNDSocket;                               /*   IPv6 ND Socket Descriptor */
   UINT4 gu4VrrpMaxOperEntries = MAX_VRRP_OPER_ENTRY;   /* Max Entries in the Oper Table */
   UINT4 gu4VrrpMaxAssoEntries = MAX_VRRP_ASSO_IP_ENTRY;
   UINT1 gu1MaxOperEntries = VRRP_SET;
   UINT1 gu1StandardMask = 0xFF;
   tRBTree gVrrpOperTable;
   INT4 gi4VrrpStatus= VRRP_DISABLE;
   INT4 gi4VrrpNotificationCntl= VRRP_ENABLE;
   INT4 gi4VrrpNodeVersion= VRRP_VERSION_2;
   INT4 gi4VrrpAuthDeprecate= VRRP_AUTHDEPRECATE_ENABLE;
   UINT4 gu4RouterVersionErrors;   
   UINT4 gu4RouterVrIdErrors;       
   FILE  *LogFile;
   INT4    gi4DbugFlag;
   UINT4 gu4VrrpTrapPacketSrc;
   UINT1 gu1VrrpTrapConfigErrorType;
   tOsixTaskId gVrrpTaskId;
   tVrrpIPvXAddr gSenderIpAddr;
   tVrrpIPvXAddr gReceiverIpAddr;
   tOsixQId gIpVrrpQId;
   tOsixSemId  gVrrpSemId;
   tOsixQId gVrrpRmPktQId;
   INT4  gi4VrrpSysLogId;
   tDbTblDescriptor gVrrpDynInfoList;
   tDbDataDescInfo  gaVrrpDynDataDescList[VRRP_MAX_DYN_INFO_TYPE];
   tDbOffsetTemplate gaVrrpOffsetTbl[] =
   {{(FSAP_OFFSETOF(tVrrpOperTable, u1Version) - 
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)), 1},

    {(FSAP_OFFSETOF(tVrrpOperTable, au1Pad) -
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)), 3},

     {(FSAP_OFFSETOF(tVrrpOperTable, i4IfIndex) -
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)), 4},

    {(FSAP_OFFSETOF(tVrrpOperTable, i4VrId) -
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)), 4},

    {(FSAP_OFFSETOF(tVrrpOperTable, i4OperState) -
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)), 4},

    {(FSAP_OFFSETOF(tVrrpOperTable, MasterIpAddr) -
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)),
     sizeof (tVrrpIPvXAddr)},

    {(FSAP_OFFSETOF(tVrrpOperTable, u1HwStatus) -
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)), 1},

    {(FSAP_OFFSETOF(tVrrpOperTable, u1AddressType) -
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)), 1},

    {(FSAP_OFFSETOF(tVrrpOperTable, u1TrackedIfStatus) -
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)), 1},

    {(FSAP_OFFSETOF(tVrrpOperTable, b1V3AdvtRcvd) -
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)), 1},

    {(FSAP_OFFSETOF(tVrrpOperTable, i4RcvdMasterAdvtInterval) -
      FSAP_OFFSETOF(tVrrpOperTable, OperDbNode) - sizeof(tDbTblNode)), 4},

    {-1, 0}};
   tVrrpRedGblInfo   gVrrpRedGblInfo;
   tRBTree gVrrpRedTable;
   FS_UINT8 gu8RouterVersionErrors;
   FS_UINT8 gu8RouterChecksumErrors;
   FS_UINT8 gu8RouterVrIdErrors;
   tOsixSysTime gGblDiscTime;
   tTMO_DLL gTrackGroupHead;
   INT4 gi4VrrpTxRxVersion = VRRP_VERSION_2_3;
   
#else

  extern tVrrpIPvXAddr gVrrpZeroIpAddr;
  extern tVrrpIPvXAddr gVrrpV4DestMCastAddr;
  extern tVrrpIPvXAddr gVrrpV6DestMCastAddr;
  extern tVrrpOperTable *gpVrrpOperTable; 
  extern INT4  gi4VrrpIpv4Socket;                  /* IPv4 Socket Descriptor  */
  extern INT4  gi4VrrpIpv6Socket;                  /* IPv6 Socket Descriptor  */
  extern INT4  gi4VrrpNDSocket;                    /*   IPv6 ND Socket Descriptor */
  extern UINT4 gu4VrrpMaxOperEntries;    /* Max Entries in the Oper Table */
  extern UINT4 gu4VrrpMaxAssoEntries;
  extern UINT1 gu1MaxOperEntries;
  extern UINT1 gu1StandardMask;
  extern tRBTree gVrrpOperTable;
  extern INT4 gi4VrrpStatus;
  extern INT4 gi4VrrpNotificationCntl;
  extern INT4 gi4VrrpNodeVersion;
  extern INT4 gi4VrrpAuthDeprecate;
  extern UINT4 gu4RouterVersionErrors;
  extern UINT4 gu4RouterVrIdErrors;        
  extern FILE  *LogFile;
  extern INT4    gi4DbugFlag;
  extern UINT4 gu4VrrpTrapPacketSrc;
  extern UINT1 gu1VrrpTrapConfigErrorType;
  extern tOsixTaskId gVrrpTaskId;
  extern tVrrpIPvXAddr gSenderIpAddr;
  extern tVrrpIPvXAddr gReceiverIpAddr;
  extern UINT1 gu1Ttl;
  extern tOsixQId gIpVrrpQId;
  extern tOsixSemId  gVrrpSemId;
  extern tOsixQId gVrrpRmPktQId;
  extern INT4  gi4VrrpSysLogId;
  extern tDbTblDescriptor gVrrpDynInfoList;
  extern tDbDataDescInfo  gaVrrpDynDataDescList[VRRP_MAX_DYN_INFO_TYPE];
  extern tDbOffsetTemplate gaVrrpOffsetTbl[];
  extern tVrrpRedGblInfo   gVrrpRedGblInfo;
  extern tRBTree gVrrpRedTable;
  extern FS_UINT8 gu8RouterVersionErrors;
  extern FS_UINT8 gu8RouterChecksumErrors;
  extern FS_UINT8 gu8RouterVrIdErrors;
  extern tOsixSysTime gGblDiscTime;
  extern tTMO_DLL gTrackGroupHead;
  extern INT4 gi4VrrpTxRxVersion;

#endif

#endif
