/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vrrp3db.h,
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _VRRP3DB_H
#define _VRRP3DB_H

UINT1 Vrrpv3OperationsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 Vrrpv3AssociatedIpAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 Vrrpv3StatisticsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};

UINT4 vrrp3 [] ={1,3,6,1,2,1,207};
tSNMP_OID_TYPE vrrp3OID = {7, vrrp3};


UINT4 Vrrpv3OperationsVrId [ ] ={1,3,6,1,2,1,207,1,1,1,1,1};
UINT4 Vrrpv3OperationsInetAddrType [ ] ={1,3,6,1,2,1,207,1,1,1,1,2};
UINT4 Vrrpv3OperationsMasterIpAddr [ ] ={1,3,6,1,2,1,207,1,1,1,1,3};
UINT4 Vrrpv3OperationsPrimaryIpAddr [ ] ={1,3,6,1,2,1,207,1,1,1,1,4};
UINT4 Vrrpv3OperationsVirtualMacAddr [ ] ={1,3,6,1,2,1,207,1,1,1,1,5};
UINT4 Vrrpv3OperationsStatus [ ] ={1,3,6,1,2,1,207,1,1,1,1,6};
UINT4 Vrrpv3OperationsPriority [ ] ={1,3,6,1,2,1,207,1,1,1,1,7};
UINT4 Vrrpv3OperationsAddrCount [ ] ={1,3,6,1,2,1,207,1,1,1,1,8};
UINT4 Vrrpv3OperationsAdvInterval [ ] ={1,3,6,1,2,1,207,1,1,1,1,9};
UINT4 Vrrpv3OperationsPreemptMode [ ] ={1,3,6,1,2,1,207,1,1,1,1,10};
UINT4 Vrrpv3OperationsAcceptMode [ ] ={1,3,6,1,2,1,207,1,1,1,1,11};
UINT4 Vrrpv3OperationsUpTime [ ] ={1,3,6,1,2,1,207,1,1,1,1,12};
UINT4 Vrrpv3OperationsRowStatus [ ] ={1,3,6,1,2,1,207,1,1,1,1,13};
UINT4 Vrrpv3AssociatedIpAddrAddress [ ] ={1,3,6,1,2,1,207,1,1,2,1,1};
UINT4 Vrrpv3AssociatedIpAddrRowStatus [ ] ={1,3,6,1,2,1,207,1,1,2,1,2};
UINT4 Vrrpv3RouterChecksumErrors [ ] ={1,3,6,1,2,1,207,1,2,1};
UINT4 Vrrpv3RouterVersionErrors [ ] ={1,3,6,1,2,1,207,1,2,2};
UINT4 Vrrpv3RouterVrIdErrors [ ] ={1,3,6,1,2,1,207,1,2,3};
UINT4 Vrrpv3GlobalStatisticsDiscontinuityTime [ ] ={1,3,6,1,2,1,207,1,2,4};
UINT4 Vrrpv3StatisticsMasterTransitions [ ] ={1,3,6,1,2,1,207,1,2,5,1,1};
UINT4 Vrrpv3StatisticsNewMasterReason [ ] ={1,3,6,1,2,1,207,1,2,5,1,2};
UINT4 Vrrpv3StatisticsRcvdAdvertisements [ ] ={1,3,6,1,2,1,207,1,2,5,1,3};
UINT4 Vrrpv3StatisticsAdvIntervalErrors [ ] ={1,3,6,1,2,1,207,1,2,5,1,4};
UINT4 Vrrpv3StatisticsIpTtlErrors [ ] ={1,3,6,1,2,1,207,1,2,5,1,5};
UINT4 Vrrpv3StatisticsProtoErrReason [ ] ={1,3,6,1,2,1,207,1,2,5,1,6};
UINT4 Vrrpv3StatisticsRcvdPriZeroPackets [ ] ={1,3,6,1,2,1,207,1,2,5,1,7};
UINT4 Vrrpv3StatisticsSentPriZeroPackets [ ] ={1,3,6,1,2,1,207,1,2,5,1,8};
UINT4 Vrrpv3StatisticsRcvdInvalidTypePackets [ ] ={1,3,6,1,2,1,207,1,2,5,1,9};
UINT4 Vrrpv3StatisticsAddressListErrors [ ] ={1,3,6,1,2,1,207,1,2,5,1,10};
UINT4 Vrrpv3StatisticsPacketLengthErrors [ ] ={1,3,6,1,2,1,207,1,2,5,1,11};
UINT4 Vrrpv3StatisticsRowDiscontinuityTime [ ] ={1,3,6,1,2,1,207,1,2,5,1,12};
UINT4 Vrrpv3StatisticsRefreshRate [ ] ={1,3,6,1,2,1,207,1,2,5,1,13};




tMbDbEntry vrrp3MibEntry[]= {

{{12,Vrrpv3OperationsVrId}, GetNextIndexVrrpv3OperationsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Vrrpv3OperationsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3OperationsInetAddrType}, GetNextIndexVrrpv3OperationsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Vrrpv3OperationsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3OperationsMasterIpAddr}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsMasterIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Vrrpv3OperationsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3OperationsPrimaryIpAddr}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsPrimaryIpAddrGet, Vrrpv3OperationsPrimaryIpAddrSet, Vrrpv3OperationsPrimaryIpAddrTest, Vrrpv3OperationsTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Vrrpv3OperationsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3OperationsVirtualMacAddr}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsVirtualMacAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Vrrpv3OperationsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3OperationsStatus}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Vrrpv3OperationsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3OperationsPriority}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsPriorityGet, Vrrpv3OperationsPrioritySet, Vrrpv3OperationsPriorityTest, Vrrpv3OperationsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Vrrpv3OperationsTableINDEX, 3, 0, 0, "100"},

{{12,Vrrpv3OperationsAddrCount}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsAddrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Vrrpv3OperationsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3OperationsAdvInterval}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsAdvIntervalGet, Vrrpv3OperationsAdvIntervalSet, Vrrpv3OperationsAdvIntervalTest, Vrrpv3OperationsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Vrrpv3OperationsTableINDEX, 3, 0, 0, "100"},

{{12,Vrrpv3OperationsPreemptMode}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsPreemptModeGet, Vrrpv3OperationsPreemptModeSet, Vrrpv3OperationsPreemptModeTest, Vrrpv3OperationsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Vrrpv3OperationsTableINDEX, 3, 0, 0, "1"},

{{12,Vrrpv3OperationsAcceptMode}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsAcceptModeGet, Vrrpv3OperationsAcceptModeSet, Vrrpv3OperationsAcceptModeTest, Vrrpv3OperationsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Vrrpv3OperationsTableINDEX, 3, 0, 0, "2"},

{{12,Vrrpv3OperationsUpTime}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Vrrpv3OperationsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3OperationsRowStatus}, GetNextIndexVrrpv3OperationsTable, Vrrpv3OperationsRowStatusGet, Vrrpv3OperationsRowStatusSet, Vrrpv3OperationsRowStatusTest, Vrrpv3OperationsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Vrrpv3OperationsTableINDEX, 3, 0, 1, NULL},

{{12,Vrrpv3AssociatedIpAddrAddress}, GetNextIndexVrrpv3AssociatedIpAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Vrrpv3AssociatedIpAddrTableINDEX, 4, 0, 0, NULL},

{{12,Vrrpv3AssociatedIpAddrRowStatus}, GetNextIndexVrrpv3AssociatedIpAddrTable, Vrrpv3AssociatedIpAddrRowStatusGet, Vrrpv3AssociatedIpAddrRowStatusSet, Vrrpv3AssociatedIpAddrRowStatusTest, Vrrpv3AssociatedIpAddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Vrrpv3AssociatedIpAddrTableINDEX, 4, 0, 1, NULL},

{{10,Vrrpv3RouterChecksumErrors}, NULL, Vrrpv3RouterChecksumErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Vrrpv3RouterVersionErrors}, NULL, Vrrpv3RouterVersionErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Vrrpv3RouterVrIdErrors}, NULL, Vrrpv3RouterVrIdErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Vrrpv3GlobalStatisticsDiscontinuityTime}, NULL, Vrrpv3GlobalStatisticsDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,Vrrpv3StatisticsMasterTransitions}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsMasterTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsNewMasterReason}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsNewMasterReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsRcvdAdvertisements}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsRcvdAdvertisementsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsAdvIntervalErrors}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsAdvIntervalErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsIpTtlErrors}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsIpTtlErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsProtoErrReason}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsProtoErrReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsRcvdPriZeroPackets}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsRcvdPriZeroPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsSentPriZeroPackets}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsSentPriZeroPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsRcvdInvalidTypePackets}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsRcvdInvalidTypePacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsAddressListErrors}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsAddressListErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsPacketLengthErrors}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsPacketLengthErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsRowDiscontinuityTime}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsRowDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},

{{12,Vrrpv3StatisticsRefreshRate}, GetNextIndexVrrpv3StatisticsTable, Vrrpv3StatisticsRefreshRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Vrrpv3StatisticsTableINDEX, 3, 0, 0, NULL},
};
tMibData vrrp3Entry = { 32, vrrp3MibEntry };

#endif /* _VRRP3DB_H */

