/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vrrplow.h,v 1.9 2014/03/11 14:22:19 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VrrpNotificationCntl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for VrrpOperTable. */
INT1
nmhValidateIndexInstanceVrrpOperTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for VrrpOperTable  */

INT1
nmhGetFirstIndexVrrpOperTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVrrpOperTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVrrpOperVirtualMacAddr ARG_LIST((INT4  , INT4 ,tMacAddr  *));

INT1
nmhGetVrrpOperState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpOperAdminState ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpOperPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpOperIpAddrCount ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpOperMasterIpAddr ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpOperPrimaryIpAddr ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpOperAuthType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpOperAuthKey ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVrrpOperAdvertisementInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpOperPreemptMode ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpOperVirtualRouterUpTime ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpOperProtocol ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpOperRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVrrpOperAdminState ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetVrrpOperPriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetVrrpOperPrimaryIpAddr ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetVrrpOperAuthType ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetVrrpOperAuthKey ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVrrpOperAdvertisementInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetVrrpOperPreemptMode ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetVrrpOperProtocol ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetVrrpOperRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VrrpOperAdminState ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2VrrpOperPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2VrrpOperPrimaryIpAddr ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2VrrpOperAuthType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2VrrpOperAuthKey ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2VrrpOperAdvertisementInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2VrrpOperPreemptMode ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2VrrpOperProtocol ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2VrrpOperRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VrrpOperTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for VrrpAssoIpAddrTable. */
INT1
nmhValidateIndexInstanceVrrpAssoIpAddrTable ARG_LIST((INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for VrrpAssoIpAddrTable  */

INT1
nmhGetFirstIndexVrrpAssoIpAddrTable ARG_LIST((INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVrrpAssoIpAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVrrpAssoIpAddrRowStatus ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVrrpAssoIpAddrRowStatus ARG_LIST((INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2VrrpAssoIpAddrRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2VrrpAssoIpAddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for VrrpRouterStatsTable. */
INT1
nmhValidateIndexInstanceVrrpRouterStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for VrrpRouterStatsTable  */

INT1
nmhGetFirstIndexVrrpRouterStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVrrpRouterStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVrrpStatsBecomeMaster ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsAdvertiseRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsAdvertiseIntervalErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsAuthFailures ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsIpTtlErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsPriorityZeroPktsRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsPriorityZeroPktsSent ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsInvalidTypePktsRcvd ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsAddressListErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsInvalidAuthType ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsAuthTypeMismatch ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpStatsPacketLengthErrors ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVrrpNodeVersion ARG_LIST((INT4 *));

INT1
nmhGetVrrpNotificationCntl ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVrrpNotificationCntl ARG_LIST((INT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2VrrpNotificationCntl ARG_LIST((UINT4 *  ,INT4 ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVrrpRouterChecksumErrors ARG_LIST((UINT4 *));

INT1
nmhGetVrrpRouterVersionErrors ARG_LIST((UINT4 *));

INT1
nmhGetVrrpRouterVrIdErrors ARG_LIST((UINT4 *));
