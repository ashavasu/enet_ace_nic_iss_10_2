/********************************************************************
 *                                                                  *
 * $Id: vrrpinc.h,v 1.13 2014/03/11 14:22:19 siva Exp $
 *
 * $RCSfile: vrrpinc.h,v $
 *                                                                  *
 * $Date: 2014/03/11 14:22:19 $
 *                                                                  *
 * $Revision: 1.13 $
 *                                                                  *
 *******************************************************************/

/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrpinc.h                                    |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  Protocol                                     |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            :  All include files for the Interface Modules. |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */
 
#ifndef   _VRRP_INC_H
#define   _VRRP_INC_H

/* General INCLUDE file definitions Start Here */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <stdarg.h>

/* Protocol INCLUDE file definitions Start Here */
# include  "include.h" 
# include  "extern.h"
# include  "midconst.h"
#include   "fssyslog.h"

#include "dbutil.h"

#include "iss.h"
#include "fssocket.h"
#include "ip.h"
#include  "vrrp.h"
#include  "cfa.h"
#include  "arp.h"
#include  "fsvlan.h"
#include  "l2iwf.h"
#include  "sizereg.h"
#include  "vrrpred.h"
#include  "vrrpdef.h"
#include  "vrrptypes.h"
#include  "vrrpmac.h"
#include  "vrrpglob.h"
#include  "vrrpcon.h"

#include "vrrplow.h"
#include "fsvrrplw.h"
#include "vrrp3lw.h"
#include "fsvr3lw.h"

#include "vrrpwr.h"
#include "fsvrrpwr.h"
#include "vrrp3wr.h"
#include "fsvr3wr.h"

/* Function  Prototype   definitions Start Here */
#include  "vrrpproto.h"

#include  "vrrpsem.h"

#include  "vrrpsz.h"

#ifdef L3_SWITCHING_WANTED
#include "ipnp.h"
#endif

#include "ip6util.h"

#include "vrrpcli.h"

#ifdef NPAPI_WANTED
#include "vrrpnpwr.h"
#endif /* NPAPI_WANTED */
#endif  /* #ifdef ALLINCS Checking */
