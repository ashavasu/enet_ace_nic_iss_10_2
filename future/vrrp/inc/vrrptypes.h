/*  $Id: vrrptypes.h,v 1.15 2017/12/16 11:57:11 siva Exp $ */
/********************************************************************
 *                                                                  *
 * $RCSfile: vrrptypes.h,v $
 *                                                                  *
 * $Date: 2017/12/16 11:57:11 $
 *                                                                  *
 * $Revision: 1.15 $
 *                                                                  *
 *******************************************************************/

/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrptypes.h                                  |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.                                 |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Core Protocol.                          |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : Type definitions of VRRP Core Protocol.       |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#ifndef _VRRP_TYPES_H_
#define _VRRP_TYPES_H_

typedef enum 
{
   NO_EVENT = 0,
   INIT_MSTR_IND,
   INIT_BKUP_IND,
   BKUP_MSTR_IND,
   BKUP_INIT_IND,
   MSTR_BKUP_IND,
   MSTR_INIT_IND,
   IFACE_INFO_IND
} VRRPCM_EVENT_T;

typedef struct
{
   UINT4  u4Src;
   UINT4  u4Dest;
   UINT2  u2Id;
   UINT2  u2Olen;
   UINT1  u1Proto;
   UINT1  u1Tos;
   UINT1  u1Ttl;
   UINT1  u1Df;
   UINT2  u2Port;
   UINT2  u2Padding;/*Allignmnet*/
} t_IP_SEND_PARMS1;

typedef struct _TxIpv4Buf
{
    UINT1 au1TxIpv4Buf[VRRP_MAX_TX_IPV4_BUF_SIZE];
} tVrrpTxIpv4BufSize;

typedef struct _TxIpv6Buf
{
    UINT1 au1TxIpv6Buf[VRRP_MAX_TX_IPV6_BUF_SIZE];
} tVrrpTxIpv6BufSize;

typedef struct _TxRxIpv4Pkt
{
    UINT1 au1TxRxIpv4Pkt[VRRP_MAX_TX_RX_IPV4_PKT_SIZE];
} tVrrpTxRxIpv4PktSize;

typedef struct _TxRxIpv6Pkt
{
    UINT1 au1TxRxIpv6Pkt[VRRP_MAX_TX_RX_IPV6_PKT_SIZE];
} tVrrpTxRxIpv6PktSize;

typedef tIPvXAddr tVrrpIPvXAddr;
/* Timer type definitions Start Here */

typedef struct VrrpTimerRef
{
    /* tTmrAppTimer should be the first member of this struct */
    tTmrAppTimer RefTimer;           /* Timer  to be started/Stopped */            
    UINT1        u1TimerType;        /* Timer Type Adver or MasterDown */   
    UINT1        u1Reserved[3];      /* For Padding */
    INT4         i4OperIndex;        /* Oper Entry Index */
} tVrrpTimerRef;
 
/* Structure to process IP message */
typedef struct VrrpIpMsg 
{
    tCRU_BUF_CHAIN_HEADER   *pBuf;
    tVrrpIPvXAddr           IpAddr;
    UINT4                   u4IfIndex;
    UINT4                   u4Cmd;
    UINT4                   u4Type;
    INT4                    i4AddrType;
} tVrrpIpMsg;

/* Common type definitions Start Here */

typedef struct _tIPADDRTABLE 
{
    /* Next Associated IP Address Node */
    tTMO_DLL_NODE    AssocIpAddrNode;

    /* Associated IP Address */
    tVrrpIPvXAddr    AssoIpAddr;
    
    /* Row Status Variable */
    UINT1             u1AssoIpAddrRowStatus;

    /* Network Status variable for accept mode support */
    UINT1             u1NwStatus;
    /* Is IPvX Owner */
    BOOL1             b1IsIpvXOwner;
    /* Secondary Index */
    UINT1             u1SecIndex;
} tIpAddrTable;

typedef struct _tSTATSTABLE 
{
    /* No. of times it was Master
     * This variable is applicable for both V2 and V3. */
    UINT4           u4BecomeMaster;

    /* No. of V2 Advertisements received that don't pass authentication check */
    UINT4           u4AuthFailures; 
 
    /* No. of V2 Advertisments recevied with unknown Auth Type */
    UINT4           u4InvalidAuthType;

    /* No. of V2 Advertisements received with 'Auth Type' != to the locally configure 
     * Auth method. */
    UINT4           u4AuthTypeMismatch;

    /* No. of V2 Advertisements ignored since V3 Advertisements are received
     * and processed. */
    /* VRRP Failure Counters*/
    UINT4           u4VrrpSemFail;
    UINT4           u4VrrpSendNDNeighborAdvtFail;
    UINT4           u4VrrpSendPacketToIpv4Fail;
    UINT4           u4VrrpSendPacketToIpv6Fail;
    UINT4           u4VrrpGetIpv4PortFromOperIndexFail;
    UINT4           u4VrrpGetIpv6PortFromOperIndexFail ;
    UINT4           u4VrrpSendGratArpFail;
    UINT4           u4EthRegisterMacAddrFail;
    UINT4           u4VrrpNetIpv4DeleteIntfFail;
    UINT4           u4VrrpNetIpv6DeleteIntfFail;
    UINT4           u4VrrpIpifJoinMcastGroupFail;
    UINT4           u4VrrpIpifLeaveMcastGroupFail;
    UINT4           u4VrrpSocketFail;
    UINT4           u4PktProcessValidateAdverPacketFail;
    UINT4           u4PktProcessValidateAdverV2PacketFail;
    UINT4           u4PktProcessValidateAdverV3PacketFail;
    UINT4           u4PktProcessTransmitV2PacketFail;
    UINT4           u4PktProcessTransmitV3PacketFail;

    FS_UINT8        u8V2AdvtIgnored;

    /* No. of V2 Advertisements Transmitted */
    FS_UINT8        u8TxedV2Advertisments;

    /* No. of V2 or V3 Advertisements received */
    FS_UINT8        u8RcvdAdvertisements;

    /* No. of V2 or V3 Advertisements dropped due to Interval Errors */
    FS_UINT8        u8AdvtIntervalErrors;

    /* No. of V2 or V3 Advertisements dropped due to IP TTL Errors */
    FS_UINT8        u8IpTtlErrors;

    /* No. of V2 or V3 Advertisements recevied with Zero Priority */
    FS_UINT8        u8RcvdPriZeroPackets;

    /* No. of V2 or V3 Advertisements sent with Zero Priority */
    FS_UINT8        u8SentPriZeroPackets;

    /* No. of V2 or V3 Advertisements dropped due to Invalid Type */
    FS_UINT8        u8RcvdInvalidTypePackets;

    /* No. of V2 or V3 Advertisements dropped due to address list errors */
    FS_UINT8        u8AddressListErrors;

    /* No. of V2 or V3 Advertisements dropped due to Packet Length Errors */
    FS_UINT8        u8PacketLengthErrors;

    /* No. of V3 Advertsements transmitted */
    FS_UINT8        u8TxedAdvertisments;

    /* Discontinuity Time for this statistics entry */
    tOsixSysTime    DiscontinuityTime;

    /* Reason for election of new Master Router 
     *     = Not Master         - 0
     *     = Priority           - 1
     *     = Preempted          - 2
     *     = Master No Response - 3
     */
    UINT1           u1NewMasterReason;

    /* Reason for Protocol Error 
     *     = No Error           - 0
     *     = IP TTL Error       - 1
     *     = Version Error      - 2
     *     = Checksum Error     - 3
     *     = VRID Error         - 4
     */
    UINT1           u1ProtoErrReason;

    /* Added for Padding */
    UINT1           au1Pad[2];
} tVrrpStatsTable;

typedef struct _tVrrpOperTable 
{
    /* Head pointer of Associated IP Address */
    tTMO_DLL                AssocIpAddrHead;

    /* Next Oper Entry in Track Group List */
    tTMO_DLL_NODE           TrackGroupOperNode;
    
    /* Linear Transmission buffer for this Virtual Router Entry */
    UINT1                   *pu1TxBuf;

    /* Statistics table for this Virtual Router Entry */
    tVrrpStatsTable         VrrpStats;

    /* Timer Reference for this Virtual Router Entry 
     *    - Advertisment Timer
     *    - Master Down Timer
     */
    tVrrpTimerRef           TmrRef;

    /* Authentication Key */
    tSNMP_OCTET_STRING_TYPE stAuthKey;

    /* Next VRRP Oper Entry */
    tRBNodeEmbd             VrrpOperNode;

    /* Admin State of this Virtual Router Entry 
     *    = Up   - 1
     *    = Down - 2
     */
    INT4                    i4OperAdminState;

    /* Priority of this Virtual Router Entry
     *    - Range is 0 - 255
     */
    INT4                    i4Priority;

    /* Count of Associated IP Addresses */
    INT4                    i4IpAddrCount;

    /* The Address which becomes the master when the backup router transitions
     * to become master and has many IP Addresses */
    tVrrpIPvXAddr           PrimaryIpAddr;

    /* Authentication Type Used 
     *    = No Authentication        - 1
     *    = Simple Text Password     - 2
     *    = IP Authentication Header - 3
     */
    INT4                    i4AuthType;
    
    /* Adverisement Interval
     *    -> Stored in Milli Seconds
     *    For version 2, value received is in seconds.
     *    For version 3, value received is in centi-seconds.
     */
    INT4                    i4AdvtInterval;

    /* Skew Time */
    INT4                    i4SkewTime;

    /* Master Down Interval */
    INT4                    i4MasterDownInterval;

    /* Preemption Mode
     *    = True    - 1
     *    = False   - 2
     */
    INT4                    i4PreemptMode;

    /* Time since this virtual router has last transitioned out of 
     * Initialize state */
    UINT4                   u4VirtualRouterUpTime;
    
    UINT4             u4VrIfIndex;      /* Virtual VRRP Interface PortNum */
    /* The particular protocol controlled by this virtual router
     *    = IP        - 1
     *    = Bridge    - 2
     *    = Decnet    - 3
     *    = Other     - 4
     */    
    INT4                    i4Protocol;
    
    /* Row Status of this entry 
     *    = Active          - 1
     *    = Not In Service  - 2
     *    = Not Ready       - 3
     *    = Create And Go   - 4
     *    = Create And Wait - 5
     *    = Destroy         - 6
     */
    INT4                    i4OperRowStatus;

    /* Interface IP Adress */
    tVrrpIPvXAddr           InterfaceIpAddr;

    /* Virtual MAC Address */
    tMacAddr                maVirtualMacAddr;

    /* Next Oper State of this Virtual Router Entry
     *    -> Used for temporary purpose as input to SEM
     *    = Initialize    - 1
     *    = Backup        - 2
     *    = Master        - 3
     */
    UINT1                   u1NextState;

    /* Last Event Received by the SEM */
    UINT1                   u1EventRcvd;

    /* Tracking Group Index - Used for link tracking feature
     *    - Range (1 - 10)
     */
    UINT4                   u4TrackGroupId;

    /* Oper Index */
    INT4                    i4OperIndex;

    /* Priority value received in last advertisement packet */
    UINT1                   u1RcvdPriority;

    /* Accept Mode Support
     *    = Enable   - 1
     *    = Disable  - 2
     */
    UINT1                   u1AcceptMode;

    /* Decremented Priority value - Used for link tracking feature
     *    - Range - 0 - 254
     */
    UINT1                   u1DecrementedPriority;

    /* Is IPvX Owner */
    BOOL1                   b1IsIpvXOwner;

    /* Network Interface Entry Status */
    UINT1                   u1NwEntryStatus;

    /* Boolean variable to indicate Previous Owner */
    BOOL1                   b1IsPrevIpvXOwner;

    /* Boolean variable to indicate if test validtion required or not */
    BOOL1                   b1IsTestValNotReqd;

    /* For Padding */
    UINT1                   au1Reserved[1];

    /* --------------------------------------------------------------------------*/

    /* The dynamic entries that are to be synced should be added after the
     * OperDbNode. And both the halves should be padded accordingly */

    /* Data Base node to sync vrrp dynamic information.*/
    tDbTblNode              OperDbNode;

    /* RM VRRP Header Version Field */
    UINT1                   u1Version;

    /* RM VRRP Header Reserved Field */
    UINT1                   au1Pad[3];

    /* Interface Identifier - First Index to this Virtual Router Entry */
    INT4                    i4IfIndex;

    /* Virtual Router Identifier - Second Index to this Virtual Router Entry
     *      Range is 0 - 255
     */
    INT4                    i4VrId;

    /* Oper State of this Virtual Router Entry
     *    = Initialize    - 1
     *    = Backup        - 2
     *    = Master        - 3
     */
    INT4                    i4OperState;

    /* Master Router's IP Address - This is the value used to display
     * in CLI, WEB and SNMP */
    tVrrpIPvXAddr           MasterIpAddr;

    /* Hardware status of the entry. Indicates whether present in the hardware
     * or not.
     *    = NP_UPDATED      - 1
     *    = NP_NOT_UPDATED  - 2
     */
    UINT1                   u1HwStatus;

    /* Address Type - Third Index to Index Virtual Router Entry */
    UINT1                   u1AddressType;

    /* Indicates if the the tracked interface is down or not
     *    = Not Tracked    - 0
     *    = UP             - 1
     *    = DOWN           - 2
     */
    UINT1                   u1TrackedIfStatus;

    /* Boolean variable to indicate if V3 Advertisment is received along 
     * with V2. This variable is synchronize with standby that V3 
     * Advertisements is also received along with V2, so that when standby 
     * becomes active and if it receives V2 advertisements before V3, it will
     * drop V3 Advertisments. */
    BOOL1                   b1V3AdvtRcvd;

    /* Received Master Advertisement Interval */
    INT4                    i4RcvdMasterAdvtInterval;
    /*indication that first pkt is received from NP*/
    UINT1                   u1FirstPktReceived;
    UINT1                   au1Padding [3];
}tVrrpOperTable;

typedef struct _TrackGroupTable
{
    /* Next Track Group Entry */
    tTMO_DLL_NODE      TrackGroupNode;

    /* Head Pointer to VRRP Oper Entry */
    tTMO_DLL           VrrpOperHead;

    /* Head Pointer to VRRP Track Group If Entry */
    tTMO_DLL           TrackGroupIfHead;

    /* Group Index */
    UINT4              u4GroupIndex;

    /* Row Status */
    UINT1              u1RowStatus;

    /* Tracked links to go down before notifying VRRP Oper Table for 
     * transition from Master to Backup. */
    UINT1              u1TrackedLinks;

    /* Current number of links that are UP. When this number goes below
     * u1TrackedLinks, all VRRP Oper Entries in this list are transitioned
     * from Master to Backup. */
    UINT1              u1CurActiveLinks;

    /* Group Status */
    UINT1              u1GroupStatus;
}tVrrpTrackGroupTable;

typedef struct _TrackGroupIfTable
{
    /* Next Track Group If Table */
    tTMO_DLL_NODE      TrackGroupIfNode;

    /* Tracked Interface Index */
    INT4               i4IfIndex;

    /* Row Status */
    UINT1              u1RowStatus;

    /* If Status */
    UINT1              u1IfStatus;

    /* Padding */
    UINT1              au1Pad[2];
}tVrrpTrackGroupIfTable;

/* Election typedefinitions Start Here */
#ifdef VrrpFuncPtr
#undef VrrpFuncPtr
#endif


typedef struct _tSemTransition
{
        VrrpFuncPtr  FuncPtr      ;    /* Pointer to the Action Routine */
        UINT1    u1NextState  ;        /* Next State in the State Event Machine */
        UINT1    u1Padding  ;          /* Added for Allignment*/ 
        UINT2    u2Padding  ;          /* Added for Allignment*/
} tSemTransitNode;

/* Packet Processing typedefinitions Start Here */

typedef struct _tVrrpAdverHeader 
{
   UINT1      u1VersionAndType      ;  /* Version Number and Packet Type      */
   UINT1      u1VrId                ;  /* Virtual Identifier                  */
   UINT1      u1Priority            ;  /* Priority                            */ 
   UINT1      u1CountIpAddrs        ;  /* Number of IP Address                */
   UINT2      u2AuthTypeOrAdverInt  ;  /* Auth Type or Advertisement Interval */
   UINT2      u2CheckSum            ;  /* Check Sum                           */
} tVrrpAdverHeader;

typedef struct _tVrrpAdverPkt 
{
   tVrrpAdverHeader    *pHeader             ;  /* Header of the Adver Packet */
   UINT1               *pIpAddress         ;  /* Base Pointer to an Array of 
                                                  IPAddress */
   UINT1               *pAuthenticationData ;  /* Pointer to Authentication 
                                                  String*/
} tVrrpAdverPkt;

/***************************************************************/

struct watch_dog_timer {
        struct watch_dog_timer *p_next;
        struct watch_dog_timer *p_prev;
        int priority;
        int delay;
        int count;
        VrrpFuncPtr p_func;
        int param;
};

typedef struct watch_dog_timer WDOG_TIMER;
typedef struct watch_dog_timer *T_WDOG_ID;

/**************************************************************/

/* Redundancy related definitions start here */

typedef struct _VrrpRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tVrrpRmCtrlMsg;

typedef struct _VrrpRedTable {
    tRBNodeEmbd          RbNode;             /* RbNode for the entry */
    INT4                 i4IfIndex;          /* Interface index */
    INT4                 i4VrId;             /* VRRP ID */
    INT4                 i4OperState;        /* Current State of this VRRP Router */
    tVrrpIPvXAddr        MasterIpAddr;       /* The Master router's IP address */
    UINT1                u1AddressType;      /* Address Type */
    UINT1                au1Pad[3];          /* For Padding */
}tVrrpRedTable;

typedef struct _VrrpRmMsg
{
     tVrrpRmCtrlMsg RmCtrlMsg; /* Control message from RM */
} tVrrpRmMsg;

typedef struct _sVrrpRedGblInfo{
    UINT1 u1BulkUpdStatus; /* bulk update status
                            * VRRP_HA_UPD_NOT_STARTED 0
                            * VRRP_HA_UPD_COMPLETED 1
                            * VRRP_HA_UPD_IN_PROGRESS 2,
                            * VRRP_HA_UPD_ABORTED 3 */
    UINT1 u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1 u1NumPeersUp;     /* Indicates number of standby nodes
                               that are up. */
    UINT1 bBulkReqRcvd;     /* To check whether bulk request recieved
                                 from standby before RM_STANDBY_UP event. */
}tVrrpRedGblInfo;

#endif
