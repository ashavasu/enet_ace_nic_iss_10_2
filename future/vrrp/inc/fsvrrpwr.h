/*  $Id: fsvrrpwr.h,v 1.9 2014/07/20 11:09:51 siva Exp $ */

#ifndef _FSVRRPWR_H
#define _FSVRRPWR_H

VOID RegisterFSVRRP(VOID);

VOID UnRegisterFSVRRP(VOID);
INT4 FsVrrpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpMaxOperEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsVrrpOperTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVrrpAdminPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperAdvertisementIntervalInMsecGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackGroupIdGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperDecrementPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpAcceptModeGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpAdminPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperAdvertisementIntervalInMsecSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackGroupIdSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperDecrementPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpAcceptModeSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpAdminPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperAdvertisementIntervalInMsecTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackGroupIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperDecrementPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpAcceptModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsVrrpAuthDeprecateGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpAuthDeprecateSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpAuthDeprecateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpAuthDeprecateDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsVrrpTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsVrrpOperTrackGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVrrpOperTrackedGroupTrackedLinksGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackedGroupTrackedLinksSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackedGroupTrackedLinksTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackGroupTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsVrrpOperTrackGroupIfTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsVrrpOperTrackGroupIfRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackGroupIfRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackGroupIfRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsVrrpOperTrackGroupIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSVRRPWR_H */
