
/*  Prototype for Get Test & Set for vrrpOperTable.  */
tSNMP_VAR_BIND*
vrrpOperEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
vrrpOperEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
vrrpOperEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for vrrpAssoIpAddrTable.  */
tSNMP_VAR_BIND*
vrrpAssoIpAddrEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
vrrpAssoIpAddrEntrySet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
vrrpAssoIpAddrEntryTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for vrrpRouterStatsTable.  */
tSNMP_VAR_BIND*
vrrpRouterStatsEntryGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));


/*  Prototype for Get Test & Set for vrrpOperations.  */
tSNMP_VAR_BIND*
vrrpOperationsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));
INT4
vrrpOperationsSet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));
INT4
vrrpOperationsTest ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, tSNMP_MULTI_DATA_TYPE*));


/*  Prototype for Get Test & Set for vrrpStatistics.  */
tSNMP_VAR_BIND*
vrrpStatisticsGet ARG_LIST((tSNMP_OID_TYPE*, tSNMP_OID_TYPE*, UINT1, UINT1));

