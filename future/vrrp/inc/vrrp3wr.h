/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vrrp3wr.h,
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
#ifndef _VRRP3WR_H
#define _VRRP3WR_H
INT4 GetNextIndexVrrpv3OperationsTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterVRRP3(VOID);

VOID UnRegisterVRRP3(VOID);
INT4 Vrrpv3OperationsMasterIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsPrimaryIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsVirtualMacAddrGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsStatusGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsAddrCountGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsAdvIntervalGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsPreemptModeGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsAcceptModeGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsPrimaryIpAddrSet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsAdvIntervalSet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsPreemptModeSet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsAcceptModeSet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsPrimaryIpAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsAdvIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsPreemptModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsAcceptModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Vrrpv3OperationsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexVrrpv3AssociatedIpAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 Vrrpv3AssociatedIpAddrRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3AssociatedIpAddrRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3AssociatedIpAddrRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Vrrpv3AssociatedIpAddrTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 Vrrpv3RouterChecksumErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3RouterVersionErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3RouterVrIdErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3GlobalStatisticsDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexVrrpv3StatisticsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Vrrpv3StatisticsMasterTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsNewMasterReasonGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsRcvdAdvertisementsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsAdvIntervalErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsIpTtlErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsProtoErrReasonGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsRcvdPriZeroPacketsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsSentPriZeroPacketsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsRcvdInvalidTypePacketsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsAddressListErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsPacketLengthErrorsGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsRowDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 Vrrpv3StatisticsRefreshRateGet(tSnmpIndex *, tRetVal *);
#endif /* _VRRP3WR_H */
