/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vrrp3lw.h,
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Vrrpv3OperationsTable. */
INT1
nmhValidateIndexInstanceVrrpv3OperationsTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Vrrpv3OperationsTable  */

INT1
nmhGetFirstIndexVrrpv3OperationsTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVrrpv3OperationsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVrrpv3OperationsMasterIpAddr ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVrrpv3OperationsPrimaryIpAddr ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetVrrpv3OperationsVirtualMacAddr ARG_LIST((INT4  , INT4  , INT4 ,tMacAddr * ));

INT1
nmhGetVrrpv3OperationsStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpv3OperationsPriority ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpv3OperationsAddrCount ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpv3OperationsAdvInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpv3OperationsPreemptMode ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpv3OperationsAcceptMode ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpv3OperationsUpTime ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpv3OperationsRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVrrpv3OperationsPrimaryIpAddr ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetVrrpv3OperationsPriority ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetVrrpv3OperationsAdvInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetVrrpv3OperationsPreemptMode ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetVrrpv3OperationsAcceptMode ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetVrrpv3OperationsRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Vrrpv3OperationsPrimaryIpAddr ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Vrrpv3OperationsPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2Vrrpv3OperationsAdvInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Vrrpv3OperationsPreemptMode ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Vrrpv3OperationsAcceptMode ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Vrrpv3OperationsRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Vrrpv3OperationsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Vrrpv3AssociatedIpAddrTable. */
INT1
nmhValidateIndexInstanceVrrpv3AssociatedIpAddrTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Vrrpv3AssociatedIpAddrTable  */

INT1
nmhGetFirstIndexVrrpv3AssociatedIpAddrTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVrrpv3AssociatedIpAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVrrpv3AssociatedIpAddrRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetVrrpv3AssociatedIpAddrRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Vrrpv3AssociatedIpAddrRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Vrrpv3AssociatedIpAddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVrrpv3RouterChecksumErrors ARG_LIST((tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3RouterVersionErrors ARG_LIST((tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3RouterVrIdErrors ARG_LIST((tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3GlobalStatisticsDiscontinuityTime ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for Vrrpv3StatisticsTable. */
INT1
nmhValidateIndexInstanceVrrpv3StatisticsTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Vrrpv3StatisticsTable  */

INT1
nmhGetFirstIndexVrrpv3StatisticsTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexVrrpv3StatisticsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetVrrpv3StatisticsMasterTransitions ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpv3StatisticsNewMasterReason ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpv3StatisticsRcvdAdvertisements ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3StatisticsAdvIntervalErrors ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3StatisticsIpTtlErrors ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3StatisticsProtoErrReason ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetVrrpv3StatisticsRcvdPriZeroPackets ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3StatisticsSentPriZeroPackets ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3StatisticsRcvdInvalidTypePackets ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3StatisticsAddressListErrors ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3StatisticsPacketLengthErrors ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetVrrpv3StatisticsRowDiscontinuityTime ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetVrrpv3StatisticsRefreshRate ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));
