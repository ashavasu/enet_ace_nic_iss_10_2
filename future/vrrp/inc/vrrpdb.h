/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: vrrpdb.h,v 1.8 2011/08/26 07:26:47 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDVRRDB_H
#define _STDVRRDB_H

UINT1 VrrpOperTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 VrrpAssoIpAddrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 VrrpRouterStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdvrr [] ={1,3,6,1,2,1,68};
tSNMP_OID_TYPE stdvrrOID = {7, stdvrr};


UINT4 VrrpNodeVersion [ ] ={1,3,6,1,2,1,68,1,1};
UINT4 VrrpNotificationCntl [ ] ={1,3,6,1,2,1,68,1,2};
UINT4 VrrpOperVrId [ ] ={1,3,6,1,2,1,68,1,3,1,1};
UINT4 VrrpOperVirtualMacAddr [ ] ={1,3,6,1,2,1,68,1,3,1,2};
UINT4 VrrpOperState [ ] ={1,3,6,1,2,1,68,1,3,1,3};
UINT4 VrrpOperAdminState [ ] ={1,3,6,1,2,1,68,1,3,1,4};
UINT4 VrrpOperPriority [ ] ={1,3,6,1,2,1,68,1,3,1,5};
UINT4 VrrpOperIpAddrCount [ ] ={1,3,6,1,2,1,68,1,3,1,6};
UINT4 VrrpOperMasterIpAddr [ ] ={1,3,6,1,2,1,68,1,3,1,7};
UINT4 VrrpOperPrimaryIpAddr [ ] ={1,3,6,1,2,1,68,1,3,1,8};
UINT4 VrrpOperAuthType [ ] ={1,3,6,1,2,1,68,1,3,1,9};
UINT4 VrrpOperAuthKey [ ] ={1,3,6,1,2,1,68,1,3,1,10};
UINT4 VrrpOperAdvertisementInterval [ ] ={1,3,6,1,2,1,68,1,3,1,11};
UINT4 VrrpOperPreemptMode [ ] ={1,3,6,1,2,1,68,1,3,1,12};
UINT4 VrrpOperVirtualRouterUpTime [ ] ={1,3,6,1,2,1,68,1,3,1,13};
UINT4 VrrpOperProtocol [ ] ={1,3,6,1,2,1,68,1,3,1,14};
UINT4 VrrpOperRowStatus [ ] ={1,3,6,1,2,1,68,1,3,1,15};
UINT4 VrrpAssoIpAddr [ ] ={1,3,6,1,2,1,68,1,4,1,1};
UINT4 VrrpAssoIpAddrRowStatus [ ] ={1,3,6,1,2,1,68,1,4,1,2};
UINT4 VrrpRouterChecksumErrors [ ] ={1,3,6,1,2,1,68,2,1};
UINT4 VrrpRouterVersionErrors [ ] ={1,3,6,1,2,1,68,2,2};
UINT4 VrrpRouterVrIdErrors [ ] ={1,3,6,1,2,1,68,2,3};
UINT4 VrrpStatsBecomeMaster [ ] ={1,3,6,1,2,1,68,2,4,1,1};
UINT4 VrrpStatsAdvertiseRcvd [ ] ={1,3,6,1,2,1,68,2,4,1,2};
UINT4 VrrpStatsAdvertiseIntervalErrors [ ] ={1,3,6,1,2,1,68,2,4,1,3};
UINT4 VrrpStatsAuthFailures [ ] ={1,3,6,1,2,1,68,2,4,1,4};
UINT4 VrrpStatsIpTtlErrors [ ] ={1,3,6,1,2,1,68,2,4,1,5};
UINT4 VrrpStatsPriorityZeroPktsRcvd [ ] ={1,3,6,1,2,1,68,2,4,1,6};
UINT4 VrrpStatsPriorityZeroPktsSent [ ] ={1,3,6,1,2,1,68,2,4,1,7};
UINT4 VrrpStatsInvalidTypePktsRcvd [ ] ={1,3,6,1,2,1,68,2,4,1,8};
UINT4 VrrpStatsAddressListErrors [ ] ={1,3,6,1,2,1,68,2,4,1,9};
UINT4 VrrpStatsInvalidAuthType [ ] ={1,3,6,1,2,1,68,2,4,1,10};
UINT4 VrrpStatsAuthTypeMismatch [ ] ={1,3,6,1,2,1,68,2,4,1,11};
UINT4 VrrpStatsPacketLengthErrors [ ] ={1,3,6,1,2,1,68,2,4,1,12};
UINT4 VrrpTrapPacketSrc [ ] ={1,3,6,1,2,1,68,1,5};
UINT4 VrrpTrapAuthErrorType [ ] ={1,3,6,1,2,1,68,1,6};




tMbDbEntry stdvrrMibEntry[]= {

{{9,VrrpNodeVersion}, NULL, VrrpNodeVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,VrrpNotificationCntl}, NULL, VrrpNotificationCntlGet, VrrpNotificationCntlSet, VrrpNotificationCntlTest, VrrpNotificationCntlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,VrrpOperVrId}, GetNextIndexVrrpOperTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, VrrpOperTableINDEX, 2, 0, 0, NULL},

{{11,VrrpOperVirtualMacAddr}, GetNextIndexVrrpOperTable, VrrpOperVirtualMacAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, VrrpOperTableINDEX, 2, 0, 0, NULL},

{{11,VrrpOperState}, GetNextIndexVrrpOperTable, VrrpOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, VrrpOperTableINDEX, 2, 0, 0, NULL},

{{11,VrrpOperAdminState}, GetNextIndexVrrpOperTable, VrrpOperAdminStateGet, VrrpOperAdminStateSet, VrrpOperAdminStateTest, VrrpOperTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VrrpOperTableINDEX, 2, 0, 0, "2"},

{{11,VrrpOperPriority}, GetNextIndexVrrpOperTable, VrrpOperPriorityGet, VrrpOperPrioritySet, VrrpOperPriorityTest, VrrpOperTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, VrrpOperTableINDEX, 2, 0, 0, "100"},

{{11,VrrpOperIpAddrCount}, GetNextIndexVrrpOperTable, VrrpOperIpAddrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, VrrpOperTableINDEX, 2, 0, 0, NULL},

{{11,VrrpOperMasterIpAddr}, GetNextIndexVrrpOperTable, VrrpOperMasterIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, VrrpOperTableINDEX, 2, 0, 0, NULL},

{{11,VrrpOperPrimaryIpAddr}, GetNextIndexVrrpOperTable, VrrpOperPrimaryIpAddrGet, VrrpOperPrimaryIpAddrSet, VrrpOperPrimaryIpAddrTest, VrrpOperTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, VrrpOperTableINDEX, 2, 0, 0, "0"},

{{11,VrrpOperAuthType}, GetNextIndexVrrpOperTable, VrrpOperAuthTypeGet, VrrpOperAuthTypeSet, VrrpOperAuthTypeTest, VrrpOperTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VrrpOperTableINDEX, 2, 0, 0, "1"},

{{11,VrrpOperAuthKey}, GetNextIndexVrrpOperTable, VrrpOperAuthKeyGet, VrrpOperAuthKeySet, VrrpOperAuthKeyTest, VrrpOperTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, VrrpOperTableINDEX, 2, 0, 0, NULL},

{{11,VrrpOperAdvertisementInterval}, GetNextIndexVrrpOperTable, VrrpOperAdvertisementIntervalGet, VrrpOperAdvertisementIntervalSet, VrrpOperAdvertisementIntervalTest, VrrpOperTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, VrrpOperTableINDEX, 2, 0, 0, "1"},

{{11,VrrpOperPreemptMode}, GetNextIndexVrrpOperTable, VrrpOperPreemptModeGet, VrrpOperPreemptModeSet, VrrpOperPreemptModeTest, VrrpOperTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VrrpOperTableINDEX, 2, 0, 0, "1"},

{{11,VrrpOperVirtualRouterUpTime}, GetNextIndexVrrpOperTable, VrrpOperVirtualRouterUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, VrrpOperTableINDEX, 2, 0, 0, NULL},

{{11,VrrpOperProtocol}, GetNextIndexVrrpOperTable, VrrpOperProtocolGet, VrrpOperProtocolSet, VrrpOperProtocolTest, VrrpOperTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VrrpOperTableINDEX, 2, 0, 0, "1"},

{{11,VrrpOperRowStatus}, GetNextIndexVrrpOperTable, VrrpOperRowStatusGet, VrrpOperRowStatusSet, VrrpOperRowStatusTest, VrrpOperTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VrrpOperTableINDEX, 2, 0, 1, NULL},

{{11,VrrpAssoIpAddr}, GetNextIndexVrrpAssoIpAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, VrrpAssoIpAddrTableINDEX, 3, 0, 0, NULL},

{{11,VrrpAssoIpAddrRowStatus}, GetNextIndexVrrpAssoIpAddrTable, VrrpAssoIpAddrRowStatusGet, VrrpAssoIpAddrRowStatusSet, VrrpAssoIpAddrRowStatusTest, VrrpAssoIpAddrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, VrrpAssoIpAddrTableINDEX, 3, 0, 1, NULL},

{{9,VrrpTrapPacketSrc}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{9,VrrpTrapAuthErrorType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{9,VrrpRouterChecksumErrors}, NULL, VrrpRouterChecksumErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,VrrpRouterVersionErrors}, NULL, VrrpRouterVersionErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,VrrpRouterVrIdErrors}, NULL, VrrpRouterVrIdErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,VrrpStatsBecomeMaster}, GetNextIndexVrrpRouterStatsTable, VrrpStatsBecomeMasterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsAdvertiseRcvd}, GetNextIndexVrrpRouterStatsTable, VrrpStatsAdvertiseRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsAdvertiseIntervalErrors}, GetNextIndexVrrpRouterStatsTable, VrrpStatsAdvertiseIntervalErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsAuthFailures}, GetNextIndexVrrpRouterStatsTable, VrrpStatsAuthFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsIpTtlErrors}, GetNextIndexVrrpRouterStatsTable, VrrpStatsIpTtlErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsPriorityZeroPktsRcvd}, GetNextIndexVrrpRouterStatsTable, VrrpStatsPriorityZeroPktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsPriorityZeroPktsSent}, GetNextIndexVrrpRouterStatsTable, VrrpStatsPriorityZeroPktsSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsInvalidTypePktsRcvd}, GetNextIndexVrrpRouterStatsTable, VrrpStatsInvalidTypePktsRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsAddressListErrors}, GetNextIndexVrrpRouterStatsTable, VrrpStatsAddressListErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsInvalidAuthType}, GetNextIndexVrrpRouterStatsTable, VrrpStatsInvalidAuthTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsAuthTypeMismatch}, GetNextIndexVrrpRouterStatsTable, VrrpStatsAuthTypeMismatchGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},

{{11,VrrpStatsPacketLengthErrors}, GetNextIndexVrrpRouterStatsTable, VrrpStatsPacketLengthErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, VrrpRouterStatsTableINDEX, 2, 0, 0, NULL},
};
tMibData stdvrrEntry = { 36, stdvrrMibEntry };

#endif /* _STDVRRDB_H */

