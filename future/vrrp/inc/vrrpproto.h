/*  $Id: vrrpproto.h,v 1.27 2018/01/02 09:36:39 siva Exp $ */
/********************************************************************
 *                                                                  *
 * $RCSfile: vrrpproto.h,v $
 *                                                                  *
 * $Date: 2018/01/02 09:36:39 $
 *                                                                  *
 * $Revision: 1.27 $
 *                                                                  *
 *******************************************************************/

 /* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * +--------------------------------------------------------------------------+
 * |                                                                          |
 * |   FILE  NAME             :  vrrpproto.p                                  |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.               |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  VRRP                                         |
 * |                                                                          |
 * |   MODULE NAME            :  VRRP Core Protocol.                          |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            : Type definitions of VRRP Core Protocol.       |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 *
 *     CHANGE RECORD :
 * +--------------------------------------------------------------------------+
 * |   VERSION       |        AUTHOR/       |         DESCRIPTION OF          |
 * |                 |        DATE          |            CHANGE               |
 * +-----------------|----------------------|---------------------------------+
 * |  1.0.0.0        |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * |                 |                      |                                 |
 * +--------------------------------------------------------------------------+
 */

#ifndef _VRRP_PROTO_P_
#define _VRRP_PROTO_P_

VOID PrintLog ARG_LIST((char a, char* b,...));

INT1 ETHGetIfStatus ARG_LIST((UINT1 u1IfNum));


/* Init Funtion Prototypes */
VOID VrrpDefaultConfig (INT4 i4Index);


VOID VrrpLinkDown ARG_LIST((UINT4  u4Port, INT4 i4AddrType));

VOID VrrpLinkUp ARG_LIST((UINT4 u4Port, INT4 i4AddrType));

VOID VrrpIpChange ARG_LIST((UINT4 u4Port, INT4 i4AddrType,
                            tVrrpIPvXAddr IpAddr, UINT4 u4Type));

VOID VrrpIfaceDelete ARG_LIST((UINT4  u4Port, INT4 i4AddrType));

VOID VrrpEnable ARG_LIST((VOID));

INT4 VrrpGetOperPriority(INT4 i4OperIndex);

/* SNMP Config Function Prototypes */

VOID VrrpConfig ARG_LIST((VOID));

/* Election Function Prototypes  GLOBAL FUNCS */

EXPORT INT1 VrrpSem ARG_LIST((UINT4 u1Event, INT4 i4OperIndex, INT4 i4Priority,
                              UINT4 u4IpAddrCount));

/* Election Function Prototypes  STATIC FUNCS */

INT1 ElectRcvStartupInInitialState ARG_LIST((INT4 i4OperIndex,
                                             INT4 i4Priority));

INT1 ElectRcvAdverPktInBackUpState ARG_LIST((INT4 i4OperIndex,
                                             INT4 i4Priority ));

INT1 ElectElectionCancelMasterDownTimer ARG_LIST((INT4 i4OperIndex,
                                                  INT4 i4Priority ));

INT1 ElectRcvAdverPktInMasterState ARG_LIST((INT4 i4OperIndex,
                                             INT4 i4Priority ));

INT1 ElectRcvAdverTmrInMasterState ARG_LIST((INT4 i4OperIndex,
                                             INT4 i4Priority ));

INT1 ElectRcvShutDownInMasterState ARG_LIST((INT4 i4OperIndex,
                                             INT4 i4Priority ));

INT1 ElectRcvdInvalidEvent ARG_LIST((INT4 i4OperIndex,
                                     INT4 i4Priority ));

INT1 ElectRcvShutDownInBackState ARG_LIST((INT4 i4OperIndex,
                                           INT4 i4Priority ));

INT1 ElectRcvShutDownInmasterState ARG_LIST((INT4 i4OperIndex,
                                             INT4 i4Priority ));

INT1 ElectRcvMstrDwnTmrInBackUpState ARG_LIST((INT4 i4OperIndex,
                                               INT4 i4Priority ));

INT1 ElectRcvStartMstrInInitState ARG_LIST((INT4 i4OperIndex,
                                            INT4 i4Priority ));

INT1 ElectRcvStartBkUpInInitState ARG_LIST((INT4 i4OperIndex,
                                            INT4 i4Priority ));

INT1 ElectRcvAdverLPInMasterState ARG_LIST((INT4 i4OperIndex,
                                            INT4 i4Priority ));

INT1 ElectRcvAdverHPInMasterState ARG_LIST((INT4 i4OperIndex,
                                            INT4 i4Priority ));

/* PktProcessing Function Prototypes GLOBAL_FUNCS */

EXPORT VOID PktProcessReceivedPacket ARG_LIST((INT4 i4Port,
                                               UINT1 *pBuf, UINT4 u4DataSize,
                                               INT4 i4AddrType,
                                               UINT1 u1Ttl));

EXPORT INT1 PktProcessTransmitPacket  ARG_LIST((INT4 i4OperIndex,
                                                BOOL1 b1IsPriorityZero));

EXPORT INT1 PktProcessTransmitV2Packet  ARG_LIST((INT4 i4OperIndex,
                                                  BOOL1 b1IsPriorityZero));

EXPORT INT1 PktProcessTransmitV3Packet  ARG_LIST((INT4 i4OperIndex,
                                                  BOOL1 b1IsPriorityZero));

EXPORT VOID PktProcessUpdateStatistics ARG_LIST((INT4 i4OperIndex,
                                                 UINT1 u1MibVariable));

EXPORT VOID PktProcessUpdateV2V3Statistics ARG_LIST((INT4 i4OperIndex,
                                                     UINT1 u1MibVariable,
                                                     UINT1 u1MibValue));

EXPORT VOID PktProcessUpdateV2Statistics ARG_LIST((INT4 i4OperIndex,
                                                   UINT1 u1MibVariable));

EXPORT VOID PktProcessUpdateV3Statistics ARG_LIST((INT4 i4OperIndex,
                                                   UINT1 u1MibVariable));

EXPORT INT4 PktProcessGetOperEntry ARG_LIST((INT4 i4IfIndex, INT4 i4VrrpOperVrId,
                                             INT4 i4AddrType));

/* PktProcessing Function Prototypes STATIC_FUNCS */

#ifdef PKTPROCESS_MODULE

INT4 PktProcessValidateAdverPacket  ARG_LIST((INT4 i4IfIndex,
                                              tVrrpAdverPkt *pVrrpAdverPkt,
                                              UINT4 u4DataSize, INT4 i4AddrType,
                                              UINT1 u1Ttl));

INT1 PktProcessValidateAdverV2Packet ARG_LIST((INT4 i4OperIndex,
                                               tVrrpAdverPkt * pVrrpAdverPkt,
                                               UINT4 u4DataSize));

INT1 PktProcessValidateAdverV3Packet ARG_LIST((INT4 i4OperIndex,
                                               tVrrpAdverPkt * pVrrpAdverPkt,
                                               UINT4 u4DataSize));

VOID PktProcessEncodeAdverHeader  ARG_LIST((INT4 i4OperIndex,
                                            tVrrpAdverPkt *pVrrpAdverPkt));

VOID PktProcessEncodeV2AdverHeader ARG_LIST((INT4 i4OperIndex,
                                             BOOL1 b1IsPriorityZero,
                                             tVrrpAdverPkt *pVrrpAdverPkt));

VOID PktProcessEncodeV3AdverHeader ARG_LIST((INT4 i4OperIndex,
                                             BOOL1 b1IsPriorityZero,
                                             tVrrpAdverPkt *pVrrpAdverPkt));

INT1 PktProcessValidateIpAddrs  ARG_LIST((INT4 i4OperIndex,
                                            tVrrpAdverPkt *pVrrpAdverPkt));

UINT2 PktProcessCalculateV2CheckSum ARG_LIST((tVrrpAdverPkt * pVrrpAdverPkt,
                                              UINT1 u1AddrType));

UINT2 PktProcessCalculateV3CheckSum ARG_LIST((tVrrpAdverPkt * pVrrpAdverPkt,
                                              UINT1 u1AddrType,
                                              tVrrpIPvXAddr SourceIp));

UINT2 PktProcessCalculateCSForIpv4 ARG_LIST((tVrrpAdverPkt * pVrrpAdverPkt,
                                             UINT2 u2PktSize));

#endif        /* PKTPROCESS_MODULE */

INT1  VrrpStateChangeRequest ARG_LIST((UINT1 u1Event, INT4 i4OperIndex));

VOID  VrrpMasterIpAddressSelection ARG_LIST((INT4 i4OperIndex,INT4 i4State));

INT1   BufIfConvertToLinearBuf ARG_LIST((VOID *pChainBuf, VOID *pLinearBuf,
                                         UINT4 u4Offsize, UINT4 u4DataSize));

/* High availability related functions */
INT4  VrrpRBTreeRedEntryCmp PROTO ((tRBElem *, tRBElem *));

INT4  VrrpRedInitGlobalInfo PROTO ((VOID));

INT4  VrrpRedRmRegisterProtocols PROTO ((tRmRegParams *pRmReg));

INT4  VrrpRedRmDeRegisterProtocols PROTO ((VOID));

INT4  VrrpRedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));

VOID  VrrpRedHandleRmEvents PROTO ((VOID));

VOID  VrrpRedHandleGoActive PROTO ((VOID));

VOID  VrrpRedHandleGoStandby PROTO ((tVrrpRmMsg * pMsg));

VOID  VrrpRedHandleStandbyToActive PROTO ((VOID));

VOID  VrrpRedHandleActiveToStandby PROTO ((VOID));

VOID  VrrpRedHandleIdleToActive PROTO ((VOID));

VOID  VrrpRedHandleIdleToStandby PROTO ((VOID));

VOID  VrrpRedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));

VOID  VrrpRedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));

UINT4 VrrpRedRmHandleProtocolEvent PROTO ((tRmProtoEvt *pEvt));

INT4  VrrpRedRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));

INT4  VrrpRedSendMsgToRm PROTO ((tRmMsg * pMsg , UINT2 u2Length ));

VOID  VrrpRedSendBulkReqMsg PROTO ((VOID));

INT4  VrrpRedDeInitGlobalInfo PROTO ((VOID));

VOID  VrrpRedSendBulkUpdTailMsg PROTO ((VOID));

VOID  VrrpRedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));

VOID  VrrpRedSendBulkUpdMsg PROTO ((VOID));

VOID  VrrpRedProcessBulkInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);

VOID  VrrpRedProcessDynamicInfo (tRmMsg * pMsg, UINT2 *pu2OffSet);

VOID  VrrpRedHwAudit PROTO ((VOID));

INT4  VrrpRedSendBulkCacheInfo (UINT1 *pu1BulkUpdPendFlg);

VOID  VrrpRedDynDataDescInit (VOID);

VOID  VrrpRedDescrTblInit (VOID);

VOID  VrrpRedDbNodeInit (tDbTblNode * pVrrpDbNode);

VOID  VrrpRedDbUtilAddTblNode (tDbTblDescriptor * pVrrpDataDesc, 
                               tDbTblNode * pVrrpDbNode);

VOID  VrrpRedSyncDynInfo (VOID);

INT4 VrrpStartTimers (VOID);

VOID VrrpRedAddAllNodeInDbTbl (VOID);

INT4 VrrpOperTableModification (INT4 i4IfIndex, INT4 i4VrId);

EXPORT INT1   BufIfConvertToChainBuf ARG_LIST((VOID *pChainBuf, VOID *pLinearBuf, UINT4 u4Size));

EXPORT INT1   BufIfReleaseChainBuf  ARG_LIST((VOID *pChainBuf));

EXPORT INT1   BufIfResetLinearBuf ARG_LIST((VOID *pLinearBuf));

EXPORT INT1   BufIfReleaseLinearBuf ARG_LIST((VOID *pLinearBuf));

EXPORT UINT4  BufIfGetChainBufSize  ARG_LIST((void *pChainBuf));

EXPORT VOID  *BufIfAllocateChainBuf ARG_LIST((UINT4 u4DataSize));

EXPORT VOID  *BufIfAllocateLinearBuf ARG_LIST((UINT4 u4Size));

EXPORT INT1   BufIfCreateBufferPool ARG_LIST((VOID));

EXPORT INT1   BufIfDeleteBufferPool ARG_LIST((VOID));

EXPORT VOID  *BufIfDuplicateChainBuf ARG_LIST((VOID *pChainBuf));

/* Timer Interface Funtion Prototypes GLOBAL FUNCS */

EXPORT INT1 TmrIfCreateTimer ARG_LIST((VOID ));

EXPORT INT1 TmrIfStopTimer ARG_LIST((VOID *pTimer));

EXPORT INT1 TmrIfStartTimer ARG_LIST((VOID *pTimer, FLT4 fDuration));

EXPORT INT1 TmrIfDeleteTimer ARG_LIST((VOID));

EXPORT VOID TmrIfGetExpiredTimers ARG_LIST((VOID));

/* Ip Interface Function Prototypes */
EXPORT INT1 VrrpSendGratArp ARG_LIST((INT4 i4OperIndex));

EXPORT INT1 VrrpSendNDNeighborAdvt ARG_LIST((INT4 i4OperIndex,
                                             UINT4 u4Port,
                                             tVrrpIPvXAddr IpAddr));

EXPORT INT1 VrrpSendNDNeighborAdvtToAllAssocIp ARG_LIST((INT4 i4OperIndex));

EXPORT INT1 VrrpJoinSolicitedMulticastGroup ARG_LIST((INT4 i4OperIndex,
                                                      BOOL1 b1IsAcceptConf));

EXPORT INT1 VrrpCreateSecIpOrJoinSoliMCast ARG_LIST((INT4 i4OperIndex,
                                                     tVrrpIPvXAddr IpAddr,
                                                     BOOL1 b1IsAcceptConf));

EXPORT INT1 VrrpLeaveSolicitedMulticastGroup ARG_LIST((INT4 i4OperIndex,
                                                       BOOL1 b1IsAcceptConf));

EXPORT INT1 VrrpDeleteSecIpOrLeaveSoliMCast ARG_LIST((INT4 i4OperIndex,
                                                      tVrrpIPvXAddr IpAddr,
                                                      BOOL1 b1IsAcceptConf));

EXPORT INT1 VrrpDeRegisterwithIp ARG_LIST((UINT1 u1ModuleId));

EXPORT INT4 VrrpAddToNwIntf ARG_LIST((INT4 i4OperIndex));

EXPORT INT4 VrrpRemoveFromNwIntf ARG_LIST((INT4 i4OperIndex));

EXPORT INT4 VrrpNetIpCreateWr ARG_LIST((INT4 i4OperIndex,
                                        tVrrpNwIntf *pVrrpNwIntf));

EXPORT INT4 VrrpNetIpDeleteWr ARG_LIST((INT4 i4OperIndex,
                                        tVrrpNwIntf *pVrrpNwIntf));

EXPORT INT4 VrrpNetIpGetWr ARG_LIST ((tVrrpNwIntf *pVrrpNwInIntf,
                                      tVrrpNwIntf *pVrrpNwOutIntf));

EXPORT INT4 VrrpGetPortFromOperIndex ARG_LIST((INT4 i4OperIndex,
                                               UINT4 *pu4Port));

EXPORT INT4 VrrpGetCfaIfIndexFromPort ARG_LIST((UINT4 u4Port,
                                                INT4 i4AddrType,
                                                UINT4 *pu4IfIndex));
EXPORT INT4 VrrpGetCfaIfIndexFromVrPortNum ARG_LIST ((UINT4 u4Port,
                                                      UINT4 *pu4IfIndex));

EXPORT INT1 VrrpSendPacket  ARG_LIST(( INT4 i4Size, INT4 i4IfIndex));

EXPORT INT1 VrrpSendPacketToIpv4 ARG_LIST((INT4 i4Size, INT4 i4OperIndex));
EXPORT INT1 VrrpSendPacketToIpv6 ARG_LIST((INT4 i4Size, INT4 i4OperIndex));

EXPORT INT1 VrrpDequePacket ARG_LIST((VOID));
EXPORT INT1 VrrpReceivePacket  ARG_LIST((VOID));
EXPORT INT1 VrrpReceiveIpv6Packet ARG_LIST((VOID));
EXPORT INT4 VrrpIpifJoinMcastGroup ARG_LIST((INT4));
EXPORT INT4 VrrpIpifLeaveMcastGroup ARG_LIST((INT4));
EXPORT INT4 GetVrrpOperAuthKey ARG_LIST ((UINT4 ,INT4 , tSNMP_OCTET_STRING_TYPE *));

EXPORT INT1 EthDeRegisterMacAddr  ARG_LIST((tVrrpIPvXAddr MasterIpAddress, UINT1 *u1VirtualMacAddr,INT4 i4IfIndex));
EXPORT INT1 EthRegisterMacAddr  ARG_LIST((tVrrpIPvXAddr MasterIpAddress, UINT1 *u1VirtualMacAddr,INT4 i4OperIndex));
EXPORT INT1 EthGetIfOperStatus ARG_LIST((INT4 i4IfIndex, UINT1 u1AddrType));


/* Actual Target Function Prototypes */
EXPORT VOID VrrpRcvLinkStatus(tNetIpv4IfInfo *pIpIfInfo, UINT1 u1Bitmap);
EXPORT VOID VrrpIp6RcvNotification (tNetIpv6HliParams *pIp6HliParams);
EXPORT INT4 VrrpOpenSocket (UINT1);
EXPORT INT1 VrrpCloseSocket (INT4 *pi4Socket);
EXPORT INT4 VrrpOpenNdSocket (VOID);
EXPORT VOID VrrpPktInSocket (INT4);
EXPORT VOID VrrpPktInIpv6Socket (INT4);

EXPORT BOOL1 VrrpCompCheckVersion (INT4 i4Version);
EXPORT VOID VrrpCompActivateTrackGroup (tVrrpTrackGroupTable *pTrackGroupEntry);
EXPORT VOID VrrpCompDeactivateTrackGroup (tVrrpTrackGroupTable *pTrackGroupEntry);
EXPORT VOID VrrpHandleLinkDownForTrackGroup (UINT4 u4IfIndex);
EXPORT VOID VrrpHandleLinkUpForTrackGroup (UINT4 u4IfIndex);

EXPORT INT4 VrrpGetAllOperTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                 INT4 i4AddrType, UINT4 u4ObjectId,
                                 tSNMP_MULTI_DATA_TYPE *pData);
EXPORT INT4 VrrpGetAllAssocIpTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                    INT4 i4AddrType,
                                    tSNMP_OCTET_STRING_TYPE *pAssocIpAddr,
                                    UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE *pData);
EXPORT INT4 VrrpGetAllStatsTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                  INT4 i4AddrType, UINT4 u4ObjectId,
                                  tSNMP_MULTI_DATA_TYPE *pData);
EXPORT INT4 VrrpGetAllTrackGroupTable (INT4 i4Version, UINT4 u4GroupIndex,
                                       UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE *pData);
EXPORT INT4 VrrpGetAllTrackGroupIfTable (INT4 i4Version, UINT4 u4GroupIndex,
                                         INT4 i4IfIndex, UINT4 u4ObjectId,
                                         tSNMP_MULTI_DATA_TYPE *pData);
EXPORT VOID VrrpGetAllScalars (INT4 i4Version, UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE *pData);

EXPORT INT4 VrrpSetAllOperTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                 INT4 i4AddrType, UINT4 u4ObjectId,
                                 tSNMP_MULTI_DATA_TYPE *pData);
EXPORT INT4 VrrpSetAllAssocIpTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                    INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE *pAssocIpAddr,
                                    UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE *pData);
EXPORT INT4 VrrpSetAllStatsTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                  INT4 i4AddrType, UINT4 u4ObjectId,
                                  tSNMP_MULTI_DATA_TYPE *pData);
EXPORT INT4 VrrpSetAllTrackGroupTable (INT4 i4Version, UINT4 u4GroupIndex,
                                       UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE *pData);
EXPORT INT4 VrrpSetAllTrackGroupIfTable (INT4 i4Version, UINT4 u4GroupIndex,
                                         INT4 i4IfIndex, UINT4 u4ObjectId, 
                                         tSNMP_MULTI_DATA_TYPE *pData);
EXPORT INT4 VrrpSetAllScalars (INT4 i4Version, UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE *pData);

EXPORT INT4 VrrpTestAllOperTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                 INT4 i4AddrType, UINT4 u4ObjectId,
                                 tSNMP_MULTI_DATA_TYPE *pData, UINT4 *pu4ErrorCode);
EXPORT INT4 VrrpTestAllAssocIpTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                    INT4 i4AddrType, tSNMP_OCTET_STRING_TYPE *pAssocIpAddr,
                                    UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE *pData,
                                    UINT4 *pu4ErrorCode);
EXPORT INT4 VrrpTestAllStatsTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                  INT4 i4AddrType, UINT4 u4ObjectId,
                                  tSNMP_MULTI_DATA_TYPE *pData, UINT4 *pu4ErrorCode);
EXPORT INT4 VrrpTestAllTrackGroupTable (INT4 i4Version, UINT4 u4GroupIndex,
                                       UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE *pData,
                                       UINT4 *pu4ErrorCode);
EXPORT INT4 VrrpTestAllTrackGroupIfTable (INT4 i4Version, UINT4 u4GroupIndex,
                                          INT4 i4IfIndex, UINT4 u4ObjectId,
                                          tSNMP_MULTI_DATA_TYPE *pData,
                                          UINT4 *pu4ErrorCode);
EXPORT INT4 VrrpTestAllScalars (INT4 i4Version, UINT4 u4ObjectId, tSNMP_MULTI_DATA_TYPE *pData,
                                UINT4 *pu4ErrorCode);

EXPORT INT4 VrrpDbCreateOperTable (VOID);
EXPORT VOID VrrpDbDeleteOperTable (VOID);
EXPORT INT4 VrrpDbAddOperNode (tVrrpOperTable *pVrrpOperEntry);
EXPORT VOID VrrpDbDeleteOperNode (tVrrpOperTable *pVrrpOperEntry);
EXPORT INT4 VrrpDbCreateOperEntry (INT4 i4OperIndex, INT4 i4IfIndex,
                                   INT4 i4VrId, INT4 i4AddrType);
EXPORT VOID VrrpDbDeleteOperEntry (INT4 i4OperIndex);
EXPORT INT4 VrrpDbGetOperEntry (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddressType);
EXPORT INT4 VrrpDbGetNextOperEntry (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddressType, 
                                    INT4 *pi4IfIndex, INT4 *pi4VrId, INT4 *pi4AddressType);
EXPORT INT4 VrrpDbGetNextV2OperEntry (INT4 i4IfIndex, INT4 i4VrId, INT4 *pi4IfIndex, 
                                      INT4 *pi4VrId);
EXPORT INT4 VrrpDbGetFreeOperIndex (VOID);
EXPORT VOID VrrpDbActivateOperEntry (INT4 i4OperIndex);
EXPORT VOID VrrpDbDeactivateOperEntry (INT4 i4OperIndex);
EXPORT tIpAddrTable * VrrpDbCreateAssocIpEntry (INT4 i4OperIndex, tIPvXAddr AssocIpAddr);
EXPORT VOID VrrpDbDeleteAssocIpEntry (INT4 i4OperIndex, tIpAddrTable *pIpAddrEntry);
EXPORT VOID VrrpDbActivateAssocIpEntry (INT4 i4OperIndex, tIpAddrTable *pIpAddrEntry);
EXPORT VOID VrrpDbDeactivateAssocIpEntry (INT4 i4OperIndex, tIpAddrTable *pIpAddrEntry);
EXPORT VOID VrrpDbOperAddAssocIpNode (INT4 i4OperIndex, tIpAddrTable *pInIpAddrEntry);
EXPORT VOID VrrpDbOperDeleteAssocIpNode (INT4 i4OperIndex, tIpAddrTable *pInIpAddrEntry);
EXPORT tIpAddrTable * VrrpDbOperGetAssocIpEntry (INT4 i4OperIndex, tIPvXAddr AssocIpAddr);
EXPORT tIpAddrTable * VrrpDbOperGetNextAssocIpEntry (INT4 i4OperIndex, tIPvXAddr AssocIpAddr);
EXPORT VOID VrrpDbDeleteAllAssocIpEntry (INT4 i4OperIndex);
EXPORT INT4 VrrpDbCreateTrackGroupEntry (UINT4 u4GroupIndex);
EXPORT VOID VrrpDbDeleteTrackGroupEntry (tVrrpTrackGroupTable *pTrackGroupEntry);
EXPORT VOID VrrpDbAddTrackGroupNode (tVrrpTrackGroupTable *pInTrackGroupEntry);
EXPORT VOID VrrpDbDeleteTrackGroupNode (tVrrpTrackGroupTable *pInTrackGroupEntry);
EXPORT tVrrpTrackGroupTable * VrrpDbGetTrackGroupEntry (UINT4 u4GroupIndex);
EXPORT tVrrpTrackGroupTable * VrrpDbGetNextTrackGroupEntry (UINT4 u4GroupIndex);

EXPORT INT4 VrrpDbCreateTrackGroupIfEntry (tVrrpTrackGroupTable *pTrackGroupEntry,
                                           INT4 i4IfIndex);

EXPORT VOID VrrpDbDeleteTrackGroupIfEntry (tVrrpTrackGroupTable *pTrackGroupEntry,
                                           tVrrpTrackGroupIfTable *pTrackGroupIfEntry);

EXPORT VOID VrrpDbAddTrackIfToGroup (tVrrpTrackGroupTable *pInTrackGroupEntry,
                                     tVrrpTrackGroupIfTable *pInTrackGroupIfEntry);

EXPORT VOID VrrpDbRemoveTrackIfFromGroup (tVrrpTrackGroupTable *pInTrackGroupEntry,
                                          tVrrpTrackGroupIfTable *pInTrackGroupIfEntry);

EXPORT tVrrpTrackGroupIfTable * VrrpDbGetTrackGroupIfEntry (tVrrpTrackGroupTable *pTrackGroupEntry,
                                                            INT4 i4IfIndex);

EXPORT tVrrpTrackGroupIfTable * VrrpDbGetNextTrackGroupIfEntry (tVrrpTrackGroupTable *pTrackGroupEntry,
                                                                INT4 i4IfIndex);

EXPORT INT4 VrrpDbTrackAddOperNode (tVrrpTrackGroupTable *pInTrackGroupEntry, INT4 i4OperIndex);
EXPORT VOID VrrpDbTrackDeleteOperNode (tVrrpTrackGroupTable *pInTrackGroupEntry, INT4 i4OperIndex);

EXPORT VOID VrrpDbActivateAcceptMode (INT4 i4OperIndex);

EXPORT VOID VrrpDbDeactivateAcceptMode (INT4 i4OperIndex);

EXPORT VOID VrrpDbSetAdvtInterval (INT4 i4OperIndex, INT4 i4AdvtInterval);

EXPORT VOID VrrpClearStats (INT4 i4Index);

EXPORT VOID PktProcessSetAndNotifyStats (INT4 i4OperIndex, UINT1 u1MibVariable,
                                         UINT1 u1MibValue);

EXPORT VOID PktProcessSetReason (INT4 i4OperIndex, UINT1 u1MibVariable, 
                                 UINT1 u1MibValue);

EXPORT VOID PktNotifyLogging (INT4 i4OperIndex, UINT1 u1MibVariable,
                              UINT1 u1MibValue);

EXPORT VOID PktNotifyStatsToSnmpManager (INT4 i4OperIndex,
                                         UINT1 u1MibVariable,
                                         UINT1 u1MibValue);

EXPORT VOID PktNotifyV3StatsToSnmpManager (INT4 i4OperIndex,
                                           UINT1 u1MibVariable,
                                           UINT1 u1MibValue);

EXPORT VOID PktNotifyV2StatsToSnmpManager (INT4 i4OperIndex,
                                           UINT1 u1MibVariable,
                                           UINT1 u1MibValue);


EXPORT VOID PktFillAssociatedIpAddresses (INT4 i4OperIndex,
                                          tVrrpAdverPkt *pVrrpAdverPkt);

EXPORT VOID VrrpFillIfIpAddr (INT4 i4OperIndex);

EXPORT VOID VrrpFillV4IfIpAddr (INT4 i4OperIndex);

EXPORT VOID VrrpFillV6IfIpAddr (INT4 i4OperIndex);

EXPORT BOOL1 VrrpIsOwnerPresentInThisIfIndex (INT4 i4IfIndex, INT4 i4VrId,
                                              UINT1 u1AddrType);
   
EXPORT INT1 VrrpCompIpv4IsOurIp (INT4 i4IfIndex, tIPvXAddr IpAddr);
EXPORT INT1 VrrpCompIpv6IsOurIp (INT4 i4IfIndex, tIPvXAddr IpAddr);
EXPORT INT1 VrrpCompIsOurIp (UINT1 u1AddrType, INT4 i4IfIndex, tIPvXAddr IpAddr);
EXPORT VOID VrrpCompSetIpvXOwner (INT4 i4OperIndex);
EXPORT FLT4 VrrpCompCalcSkewTime (INT4 i4OperIndex);
EXPORT FLT4 VrrpCompCalcMasterDownInterval (INT4 i4OperIndex);

EXPORT UINT1 * VrrpPrintIpv4Address (tVrrpIPvXAddr Addr);
EXPORT UINT1 * VrrpPrintIpv6Address (tVrrpIPvXAddr Addr);

EXPORT VOID PktClearTxBuf (INT4 i4OperIndex);

EXPORT INT4 VrrpCmnSetVrrpStatus (INT4 i4Status);

EXPORT INT4 VrrpCmnSetV3AssocIpRowStatus (INT4 i4IfIndex, INT4 i4VrId,
                                          INT4 i4AddrType, 
                                          tSNMP_OCTET_STRING_TYPE *pIpAddr,
                                          INT4 i4RowStatus);

EXPORT INT4 VrrpCmnSetV2AssocIpRowStatus (INT4 i4IfIndex, INT4 i4VrId,
                                          UINT4 u4IpAddr, INT4 i4RowStatus);

EXPORT INT4 VrrpCmnSetOperRowStatus (INT4 i4Version, INT4 i4IfIndex, 
                                     INT4 i4VrId, INT4 i4AddrType,
                                     INT4 i4RowStatus);

EXPORT VOID VrrpCmnSetAdminState (INT4 i4IfIndex, INT4 i4VrId,
                                  INT4 i4AdminState);

EXPORT INT4 VrrpCmnSetV2PrimaryIpAddr (INT4 i4IfIndex, INT4 i4VrId,
                                       UINT4 u4IpAddr);

EXPORT INT4 VrrpCmnSetV3PrimaryIpAddr (INT4 i4IfIndex, INT4 i4VrId,
                                       INT4 i4AddrType,
                                       tSNMP_OCTET_STRING_TYPE *pIpAddr);

EXPORT INT4 VrrpCmnSetV2IpAddr (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                                UINT4 u4IpAddr, UINT4 u4Option);

EXPORT INT4 VrrpCmnSetV3IpAddr (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                                UINT1 *pu1IpAddr, UINT4 u4Option);

EXPORT INT4 VrrpCmnDelV2IpAddr (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                                UINT4 u4IpAddr, UINT4 u4Option);

EXPORT INT4 VrrpCmnDelV3IpAddr (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                                UINT1 *pu1IpAddr, UINT4 u4Option);

EXPORT INT4 VrrpCmnSetPriority (INT4 i4IfIndex, INT4 i4VrId,
                                INT4 i4AddrType,
                                INT4 i4Priority);

EXPORT INT4 VrrpCmnSetPreemptMode (INT4 i4IfIndex, INT4 i4VrId,
                                   INT4 i4AddrType,
                                   INT4 i4PreemptMode);

EXPORT INT4 VrrpCmnSetAdvertisementInterval(INT4 i4IfIndex, INT4 i4VrId,
                                            INT4 i4AddrType, 
                                            INT4 i4AdvInterval);

EXPORT INT4 VrrpCmnSetAdvertisementIntervalInMSec (INT4 i4IfIndex, INT4 i4VrId,
                                                   INT4 i4AddrType,
                                                   INT4 i4AdvInterval);

EXPORT INT4 VrrpCmnSetAcceptMode (INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType,
                                  INT4 i4AcceptMode);

EXPORT INT4 VrrpCmnSetTrackAndDecPriority (INT4 i4IfIndex, INT4 i4VrId,
                                           INT4 i4AddrType,
                                           UINT4 u4GroupIndex, INT4 i4Priority);

EXPORT INT4 VrrpCmnSetTrackRowStatus (INT4 i4Version, UINT4 u4GroupIndex,
                                      INT4 i4RowStatus);

EXPORT INT4 VrrpCmnSetTrackIfRowStatus (INT4 i4Version, UINT4 u4GroupIndex,
                                        INT4 i4IfIndex, INT4 i4RowStatus);

EXPORT INT4 VrrpCmnCreateTrackGroupIf (UINT4 u4GroupIndex, INT4 i4IfIndex);

EXPORT INT4 VrrpCmnDeleteTrackGroupIf (UINT4 u4GroupIndex, INT4 i4IfIndex);

EXPORT INT4 VrrpCmnSetTrackLinks (UINT4 u4GroupIndex, INT4 i4Links);

EXPORT INT4 VrrpCmnDeleteVrId (INT4 i4Version, INT4 i4DelIfIndex, INT4 i4DelVrId,
                               INT4 i4DelAddrType);

EXPORT INT4 VrrpCmnGetAllOperTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                    INT4 i4AddrType, tVrrpOperTable *pOperTable);

EXPORT INT4 VrrpCmnGetAllStatsTable (INT4 i4Version, INT4 i4IfIndex, INT4 i4VrId,
                                     INT4 i4AddrType, tVrrpStatsTable *pStats);

EXPORT INT4 VrrpCliSetVrrpStatus (tCliHandle CliHandle, INT4 i4Status);

EXPORT INT4 VrrpCliSetPriority (tCliHandle CliHandle, INT4 i4IfIndex,
                                INT4 i4VrId,  INT4 i4AddrType,
                                INT4 i4Priority);

EXPORT INT4 VrrpCliSetVrrpAuthType (tCliHandle CliHandle, INT4 i4IfIndex,
                                    INT4 i4VrId, UINT1 *au1AuthKey);

EXPORT INT4 VrrpCliSetVrrpAuthDeprecate (tCliHandle CliHandle,
                                         INT4 i4AuthDeprecate);

EXPORT INT4 VrrpCliSetVrrpAdvertiseInterval (tCliHandle CliHandle,
                                             INT4 i4IfIndex,
                                             INT4 i4VrId,
                                             INT4 i4AddrType,
                                             INT4 i4AdvInterval);
EXPORT INT4 VrrpCliSetVrrpAdvertiseIntervalInMsec (tCliHandle CliHandle,
                                                   INT4 i4IfIndex,
                                                   INT4 i4VrId,
                                                   INT4 i4AddrType,
                                                   INT4 i4AdvInterval);

EXPORT INT4 VrrpCliSetPreemptMode (tCliHandle CliHandle, INT4 i4IfIndex,
                                   INT4 i4VrId, INT4 i4AddrType,
                                   INT4 i4Preempt);

EXPORT INT4 VrrpCliSetAcceptMode (tCliHandle CliHandle, INT4 i4IfIndex,
                                  INT4 i4VrId, INT4 i4AddrType,
                                  INT4 i4AcceptMode);

EXPORT INT4 VrrpCliSetGroupIdAndDecPrio (tCliHandle CliHandle, INT4 i4IfIndex,
                                         INT4 i4VrId, INT4 i4AddrType,
                                         UINT4 u4GroupIndex,
                                         INT4 i4Priority);

EXPORT INT4 VrrpCliCreateTrackGroupIf (tCliHandle CliHandle, UINT4 u4GroupIndex,
                                       INT4 i4IfIndex);

EXPORT INT4 VrrpCliDeleteTrackGroupIf (tCliHandle CliHandle, UINT4 u4GroupIndex,
                                       INT4 i4IfIndex);

EXPORT INT4 VrrpCliSetTrackLinks (tCliHandle CliHandle, UINT4 u4GroupIndex,
                                  INT4 i4Links);

EXPORT INT4 VrrpCliSetNoVrrpInterface (tCliHandle CliHandle, INT4 i4DelIfIndex);

EXPORT INT4 VrrpCliShowTrack (tCliHandle CliHandle);

EXPORT INT4 VrrpCliShowVrrp (tCliHandle CliHandle, UINT4 u4Val,
                             INT4 i4IfIndexVal, INT4 i4VrIdVal);

EXPORT VOID VrrpCliShowVrrpBrief (tCliHandle CliHandle, INT4 i4Version, 
                                  INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType);

EXPORT VOID VrrpCliShowVrrpDetail (tCliHandle CliHandle, INT4 i4Version, 
                                   INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType);

EXPORT VOID VrrpCliShowGlobalStats (tCliHandle CliHandle, INT4 i4Version);

EXPORT VOID VrrpCliShowVrrpStats (tCliHandle CliHandle, INT4 i4Version,
                                  INT4 i4IfIndex, INT4 i4VrId, INT4 i4AddrType);

EXPORT INT4 VrrpSetTraceLevel (tCliHandle CliHandle, UINT1 u1TraceFlag,
                               INT4 i4TraceLevel);

EXPORT INT4 VrrpSetTraceValue (INT4 i4TrapLevel,UINT1 u1LoggingCmd);

EXPORT INT4 VrrpCliSetVersion (tCliHandle CliHandle , UINT4 u4version);

EXPORT INT4 VrrpCliSetIpv4Address (tCliHandle CliHandle,INT4 i4IfIndex,
                                   INT4 i4VrId, UINT4 u4IpAddr, UINT4 u4Option);

EXPORT INT4 VrrpCliDelIpv4Address (tCliHandle CliHandle,INT4 i4IfIndex,
                                   INT4 i4VrId, UINT4 u4IpAddr,
                                   UINT4 u4Option);

EXPORT INT4 VrrpCliSetIpv6Address (tCliHandle CliHandle, INT4 i4IfIndex,
                                   INT4 i4VrId, UINT1 *pu1Addr,
                                   UINT4 u4Option);

EXPORT INT4 VrrpCliDelIpv6Address (tCliHandle CliHandle, INT4 i4IfIndex,
                                   INT4 i4VrId, UINT1 *pu1Addr, UINT4 u4Option);

EXPORT VOID VrrpCliConvertTimetoString (UINT4 u4Time, INT1 *pi1Time);

EXPORT INT4 VrrpValIfIpChange (UINT4 u4IfIndex,tIPvXAddr CurIpAddr,UINT4 u4IpSubnetMask);

EXPORT INT4 VrrpValIfTrackIfExists (INT4 i4RecvdIfIndex);

EXPORT INT1 VrrpValidateDecPriority (INT4 i4OperIndex, UINT1 u1Operation,
                                     tIPvXAddr PrevIpAddr, tIPvXAddr CurIpAddr,
                                     INT4 i4CurPriority, INT4 i4DecPriority);

EXPORT BOOL1 VrrpCompGetIPvXOwner (INT4 i4OperIndex, tIPvXAddr IpAddr);

EXPORT INT1 VrrpExtValDecPriority (UINT4 u4IfIndex, UINT1 u1Operation,
                                   tIPvXAddr PrevIpAddr, tIPvXAddr CurIpAddr);

EXPORT UINT4 VrrpCompGetTrackOperCount (UINT4 u4GroupIndex);

EXPORT VOID VrrpCompCalcTrackGroupStatusAndAct (tVrrpTrackGroupTable *pTrackGroupEntry,
                                                tVrrpTrackGroupIfTable *pTrackGroupIfEntry);

EXPORT VOID VrrpCompActivateOperTrack (INT4 i4OperIndex);
 
EXPORT VOID VrrpCompDeactivateOperTrack (INT4 i4OperIndex);

EXPORT VOID VrrpCompCheckTrackAndActOnOper (tVrrpTrackGroupTable *pTrackGroupEntry,
                                            INT4 i4OperIndex, BOOL1 b1IsResetTrack);

EXPORT VOID VrrpIsMCLAGEnabled (UINT4 u4IfIndex, UINT1 *pu1IsMclagEnabled);

#endif
