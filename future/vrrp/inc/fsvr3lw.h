/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvr3lw.h,v 1.2 2014/03/08 13:40:02 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVrrpVersionSupported ARG_LIST((INT4 *));

INT1
nmhGetFsVrrpv3TraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsVrrpv3NotificationCntl ARG_LIST((INT4 *));

INT1
nmhGetFsVrrpv3Status ARG_LIST((INT4 *));

INT1
nmhGetFsVrrpv3MaxOperEntries ARG_LIST((INT4 *));

INT1
nmhGetFsVrrpv3MaxAssociatedIpEntries ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVrrpVersionSupported ARG_LIST((INT4 ));

INT1
nmhSetFsVrrpv3TraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsVrrpv3NotificationCntl ARG_LIST((INT4 ));

INT1
nmhSetFsVrrpv3Status ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVrrpVersionSupported ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsVrrpv3TraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsVrrpv3NotificationCntl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsVrrpv3Status ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVrrpVersionSupported ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVrrpv3TraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVrrpv3NotificationCntl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVrrpv3Status ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVrrpv3OperationsTable. */
INT1
nmhValidateIndexInstanceFsVrrpv3OperationsTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVrrpv3OperationsTable  */

INT1
nmhGetFirstIndexFsVrrpv3OperationsTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVrrpv3OperationsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVrrpv3OperationsTrackGroupId ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsVrrpv3OperationsDecrementPriority ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVrrpv3OperationsTrackGroupId ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsVrrpv3OperationsDecrementPriority ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVrrpv3OperationsTrackGroupId ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsVrrpv3OperationsDecrementPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVrrpv3OperationsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVrrpv3OperationsTrackGroupTable. */
INT1
nmhValidateIndexInstanceFsVrrpv3OperationsTrackGroupTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVrrpv3OperationsTrackGroupTable  */

INT1
nmhGetFirstIndexFsVrrpv3OperationsTrackGroupTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVrrpv3OperationsTrackGroupTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVrrpv3OperationsTrackedGroupTrackedLinks ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsVrrpv3OperationsTrackRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVrrpv3OperationsTrackedGroupTrackedLinks ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsVrrpv3OperationsTrackRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVrrpv3OperationsTrackedGroupTrackedLinks ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsVrrpv3OperationsTrackRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVrrpv3OperationsTrackGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVrrpv3OperationsTrackGroupIfTable. */
INT1
nmhValidateIndexInstanceFsVrrpv3OperationsTrackGroupIfTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVrrpv3OperationsTrackGroupIfTable  */

INT1
nmhGetFirstIndexFsVrrpv3OperationsTrackGroupIfTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVrrpv3OperationsTrackGroupIfTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVrrpv3OperationsTrackGroupIfRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVrrpv3OperationsTrackGroupIfRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVrrpv3OperationsTrackGroupIfRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVrrpv3OperationsTrackGroupIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVrrpv3StatisticsTable. */
INT1
nmhValidateIndexInstanceFsVrrpv3StatisticsTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVrrpv3StatisticsTable  */

INT1
nmhGetFirstIndexFsVrrpv3StatisticsTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVrrpv3StatisticsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVrrpv3StatisticsTxedAdvertisements ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsVrrpv3StatisticsTxedV2Advertisements ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsVrrpv3StatisticsV2AdvertiseIgnored ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsVrrpv3StatisticsMasterAdverInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsVrrpv3StatisticsSkewTime ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsVrrpv3StatisticsMasterDownInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));
