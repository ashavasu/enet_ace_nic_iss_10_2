/********************************************************************
* Copyright (C) 2004-2018 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: vrrpcmd.def,v 1.26.2.1 2018/03/13 13:38:53 siva Exp $                                                         
*                                                                    
*********************************************************************/

DEFINE GROUP: VRRPGBL_CONFIG_CMDS 

COMMAND  : router vrrp
ACTION   : cli_process_vrrp_cmd (CliHandle, VRRP_CLI_START, NULL);
SYNTAX   : router vrrp
PRVID    : 15
HELP     : Enable VRRP in the router.
CXT_HELP : router Configures router related information | 
           vrrp VRRP related configuration | 
           <CR> Enable VRRP in the router

COMMAND  : no router vrrp
ACTION   : cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_START, NULL);
SYNTAX   : no router vrrp
PRVID    : 15
HELP     : Disable VRRP in the router.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           router Router related configuration | 
           vrrp VRRP related configuration | 
           <CR> Disable VRRP in the router

COMMAND  : track <integer> interface { vlan <vlan_vfi_id> | <iftype> <ifnum> }
                                 
ACTION   :
{
    UINT4 u4IfIndex = 0;
    UINT4 u4Ret = CLI_SUCCESS;
    
    if ($3 != NULL)
    {
        u4Ret = CfaCliGetIfIndex ($3, $4, &u4IfIndex);
    }
    else if ($5 != NULL)
    {
        u4Ret = CfaCliGetIfIndex ($5, $6, &u4IfIndex);
    }
    
    if (u4Ret != CLI_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface Index\r\n");
        return CLI_FAILURE;
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_TRACK_GROUP, NULL, $1, u4IfIndex);
    }
}
SYNTAX   : track <group-index> interface { vlan <vlan_vfi_id> | <iftype> <ifnum> }
PRVID    : 15
HELP     : Creates Track Group and adds an interface to it.
CXT_HELP : track Track related Configuration |
           <1-4294967295> Group Index Value|
           interface Configures interface related information | 
           vlan VLAN related configuration | 
           vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI | 
           DYNiftype |
           DYNifnum |
           <CR> Create a Track Group and add an interface to it.

COMMAND  : no track <integer> [ interface { vlan <vlan_vfi_id> | <iftype> <ifnum> } ]
ACTION   :
{
    UINT4 u4IfIndex = 0;
    UINT4 u4Ret = CLI_SUCCESS;
    
    if ($4 != NULL)
    {
        u4Ret = CfaCliGetIfIndex ($4, $5, &u4IfIndex);
    }
    else if ($6 != NULL)
    {
        u4Ret = CfaCliGetIfIndex ($6, $7, &u4IfIndex);
    }
   
    if (u4Ret != CLI_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface Index\r\n");
        return CLI_FAILURE;
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_TRACK_GROUP, NULL, $2, u4IfIndex);
    }
}
SYNTAX   : no track <group-index> interface { vlan <vlan_vfi_id> | <iftype> <ifnum> }
PRVID    : 15
HELP     : Deletes a Track Group and / or removes an interface from it.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
           track Track related Configuration |
           <1-4294967295> Group Index Value |
           interface Configures interface related information | 
           vlan VLAN related configuration | 
           vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI | 
           DYNiftype |
           DYNifnum |
           <CR> Delete a Track Group and / or remove an interface from it.
 
COMMAND  : track <integer> links <integer(1-255)>
ACTION   : cli_process_vrrp_cmd (CliHandle, VRRP_CLI_TRACK_LINKS, NULL, $1, $3);
SYNTAX   : track <group-index> links <links-to-track(1-255)>
PRVID    : 15
HELP     : Configure Number of links to track.
CXT_HELP : track Track related Configuration |
           <1-4294967295> Group Index Value |
           links Links related Configuration |
           <1-255> Tracked Links Value |
           <CR> Configure Number of Links to track.

COMMAND  : no track <integer> links
ACTION   : cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_TRACK_LINKS, NULL, $2);
SYNTAX   : no track <group-index> links
PRVID    : 15
HELP     : Resets the number of links to track.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value |
           track Track related Configuration |
           <1-4294967295> Group Index Value |
           links Links related Configuration |
           <CR> Resets the number of links to track.

END GROUP 
 
DEFINE GROUP: VRRP_CONFIG_CMDS
COMMAND : auth-deprecate { enable | disable }
ACTION :
{
    if ($1 != NULL)
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_AUTH_DEPRECATE,
                              NULL, VRRP_CLI_AUTHDEPRECATE_ENABLE);
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_AUTH_DEPRECATE,
                              NULL, VRRP_CLI_AUTHDEPRECATE_DISABLE);
    }
}
SYNTAX : auth-deprecate { enable | disable }
PRVID : 15
HELP : Enables/Disables VRRP Auth Deprecation
CXT_HELP : auth-deprecate VRRP auth deprecation flag |
           enable Enables the AuthDeprecation flag   |
           disable Disables the AuthDeprecation flag |
           <CR> Enables/Disables AuthDeprecation flag in VRRP

COMMAND  : vrrp version { v2 | v2-v3 | v3 }
ACTION   :
{
    if ($2 != NULL)
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_SET_VERSION, NULL, 
                              VRRP_CLI_VERSION_2);
    }
    else if ($3 != NULL)
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_SET_VERSION, NULL,
                              VRRP_CLI_VERSION_2_3);
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_SET_VERSION, NULL,
                              VRRP_CLI_VERSION_3);
    }
}
SYNTAX   : vrrp version { v2 | v2-v3 | v3 }
PRVID    : 15
HELP     : Set VRRP Version in the router.
CXT_HELP : vrrp VRRP configuration|
           version Version related configuration|
           v2 Enables VRRP Version 2|
           v2-v3 Enables both VRRP Version 2 and Version 3|
           v3 Enables VRRP Version 3|
           <CR> Enables VRRP version in the router
END GROUP

DEFINE GROUP: VRRPIF_CMDS 

COMMAND : interface { vlan <vlan_vfi_id> | <iftype> <ifnum> | <ipiftype> <ifnum>}
ACTION  :
{ 
    UINT4 u4IfIndex = 0;
    UINT4 u4Ret = CLI_SUCCESS;
    
    if ($1 != NULL)
    {
        u4Ret = CfaCliGetIfIndex ($1, $2, &u4IfIndex);
    }
    else if ($3 != NULL)
    {
        u4Ret = CfaCliGetIfIndex ($3, $4, &u4IfIndex);
    }
    else if ($5 != NULL)
    {
        u4Ret = CfaCliGetIfIndex ($5, $6, &u4IfIndex);
    }
    
    if (u4Ret != CLI_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface Index\r\n");
        return CLI_FAILURE;
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_VRRP_IF, NULL, u4IfIndex);
    }
}
SYNTAX  : interface { vlan <vlan-id/vfi-id> | <interface-type> <interface-id> |
                      <IP-interface-type> <IP-interface-number>}
PRVID   : 15
HELP    : Select an interface to configure.
CXT_HELP : interface Configures interface related information | 
           vlan VLAN related configuration | 
           vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI | 
           DYNiftype |
           DYNifnum |
           <ipiftype> IP Interface type |
           <ipifnum> IP Interface number |
           <CR> Select an interface to configure

COMMAND : no interface { vlan <vlan_vfi_id> | <iftype> <ifnum> | <ipiftype> <ifnum>}
ACTION  :
{ 
    UINT4 u4IfIndex = 0;
    UINT4 u4Ret = CLI_SUCCESS;
    
    if ($2 != NULL)
    {
        u4Ret = CfaCliGetIfIndex ($2, $3, &u4IfIndex);
    }
    else if ($4 != NULL)
    {
        u4Ret = CfaCliGetIfIndex ($4, $5, &u4IfIndex);
    }
    else if ($6 != NULL)
    {
        u4Ret = CfaCliGetIfIndex ($6, $7, &u4IfIndex);
    }
    
    if (u4Ret != CLI_SUCCESS)
    {
        CliPrintf(CliHandle, "\r%% Invalid Interface Index\r\n");
        return CLI_FAILURE;
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_VRRP_IF, NULL, u4IfIndex);
    }
}
SYNTAX  : no interface { Vlan <vlan-id/vfi-id> | <interface-type> <interface-id> |
                         <IP-interface-type> <IP-interface-number>}
PRVID   : 15
HELP    : Deletes the Virtual Router Entries on the given Interface.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           interface Interface related configuration | 
           vlan VLAN related configuration | 
           vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI | 
           DYNiftype| 
           DYNifnum| 
           <ipiftype> IP Interface type |
           <ipifnum> IP Interface number |
           <CR> Deletes the Virtual Router Entries on the given Interface

COMMAND : exit
ACTION  : {
              CliChangePath ("..");
          }
SYNTAX  : exit
PRVID   : 15
HELP    : exit to config mode.
CXT_HELP : exit Exit to the global configuration (config)# mode | 
           <CR> Exit to config mode

END GROUP 

DEFINE GROUP: IF_VRRP_CONFIG_CMDS 

COMMAND  : vrrp <short(1-255)> ipv4 <ip_addr> [secondary]
ACTION   :
{
    if ($4 != NULL)
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_IP, NULL, $1, $3,
                              VRRP_CLI_SECONDARY);
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_IP, NULL, $1, $3,
                              VRRP_CLI_PRIMARY);
    }
}
SYNTAX   : vrrp <vrid(1-255)> ipv4 <ip_addr> [secondary]
PRVID    : 15
HELP     : Sets the IP address for the virtual router.
CXT_HELP : vrrp Configures VRRP related information|
           (1-255) Virtual router ID|
           ipv4 IPv4 related configuration|
           A.B.C.D IPv4 address|
           secondary Address is set as secondary IP address|
           <CR> Sets the IP address for the virtual router


COMMAND  : vrrp <short(1-255)> ipv6 <ip6_addr> [secondary]
ACTION   :
{
    if ($4 != NULL)
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_IPV6, NULL, $1, $3,
                              VRRP_CLI_SECONDARY);
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_IPV6, NULL, $1, $3,
                              VRRP_CLI_PRIMARY);
    }
}
SYNTAX   : vrrp <vrid(1-255)> ipv6 <ip6_addr> [secondary]
PRVID    : 15
HELP     : Sets the IP address for the virtual router.
CXT_HELP : vrrp Configures VRRP related information|
           (1-255) Virtual router ID|
           ipv6 IPv6 related configuration|
           AAAA::BBBB IPv6 address|
           secondary Address is set as secondary IP address|
           <CR> Sets the IP address for the virtual router

COMMAND  : no vrrp <short(1-255)> ipv4 [ <ip_addr> [secondary] ]
ACTION   :
{
    UINT4 u4IpAddr = 0;

    if ($4 != NULL)
    {
        if ($5 != NULL)
        {
            cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_IP, NULL,
                                  $2, $4, VRRP_CLI_SECONDARY);
        }
        else
        {
            cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_IP, NULL,
                                  $2, $4, VRRP_CLI_PRIMARY);
        }
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_IP, NULL,
                              $2, &u4IpAddr, VRRP_CLI_DELETE_VRID);
    }
}
SYNTAX   : no vrrp <vrid(1-255)> ipv4 [ <ip_addr> [secondary] ]
PRVID    : 15
HELP     : Deletes the IP address for the virtual router.
CXT_HELP : no Deletes the Configured IP Address|
           vrrp Configures VRRP related information|
           (1-255) Virtual router ID|
           ipv4 IPv4 related configuration|
           A.B.C.D IPv4 address|
           secondary Address is set as secondary IP address|
           <CR> Deletes the IPv4 address for the virtual router.

COMMAND  : no vrrp <short(1-255)> ipv6 [ <ip6_addr> [secondary] ]
ACTION   :
{
    if ($4 != NULL)
    {
        if ($5 != NULL)
        {
            cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_IPV6, NULL,
                                  $2, $4, VRRP_CLI_SECONDARY);
        }
        else
        {
            cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_IPV6, NULL,
                                  $2, $4, VRRP_CLI_PRIMARY);
        }
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_IPV6, NULL,
                              $2, NULL, VRRP_CLI_DELETE_VRID);
    }
}
SYNTAX   : no vrrp <vrid(1-255)> ipv6 [ <ip6_addr> [secondary] ]
PRVID    : 15
HELP     : Deletes the IP address for the virtual router.
CXT_HELP : no Deletes the Configured IP Address|
           vrrp Configures VRRP related information|
           (1-255) Virtual router ID|
           ipv6 IPv6 related configuration|
           AAAA::BBBB IPv6 address|
           secondary Address is set as secondary IP address|
           <CR> Deletes the IPv6 address for the virtual router.

COMMAND  : vrrp <short(1-255)> [ ipv4 ] priority <short(1-254)>
ACTION   : 
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV4;

    cli_process_vrrp_cmd(CliHandle, VRRP_CLI_PRIORITY, NULL, $1, $4, i4AddrType);
}
SYNTAX   : vrrp <vrid(1-255)> [ ipv4 ] priority <priority(1-254)>
PRVID    : 15
HELP     : Sets the priority for the virtual router.
CXT_HELP : vrrp Configures VRRP related information | 
           (1-255) Virtual router ID |
           ipv4 IPv4 related configuration |
           priority Priority related configuration | 
           (1-254) Priority used for the virtual router master election process | 
           <CR> Sets the priority for the virtual router

COMMAND  : vrrp <short(1-255)> ipv6 priority <short(1-254)>
ACTION   : 
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV6;

    cli_process_vrrp_cmd(CliHandle, VRRP_CLI_PRIORITY, NULL, $1, $4, i4AddrType);
}
SYNTAX   : vrrp <vrid(1-255)> ipv6 priority <priority(1-254)>
PRVID    : 15
HELP     : Sets the priority for the virtual router.
CXT_HELP : vrrp Configures VRRP related information | 
           (1-255) Virtual router ID |
           ipv6 IPv6 related configuration |
           priority Priority related configuration | 
           (1-254) Priority used for the virtual router master election process | 
           <CR> Sets the priority for the virtual router

COMMAND  : no vrrp <short(1-255)> [ ipv4 ] priority
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV4;

    cli_process_vrrp_cmd(CliHandle, VRRP_CLI_NO_PRIORITY, NULL, $2, i4AddrType);
}
SYNTAX   : no vrrp <vrid(1-255)> [ ipv4 ] priority
PRVID    : 15
HELP     : Sets the priority for the virtual router to default value.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           vrrp VRRP related configuration |
           (1-255) Virtual router ID |
           ipv4 IPv4 related configuration |
           priority Priority related configuration | 
           <CR> Sets the priority for the virtual router to default value

COMMAND  : no vrrp <short(1-255)> ipv6 priority
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV6;

    cli_process_vrrp_cmd(CliHandle, VRRP_CLI_NO_PRIORITY, NULL, $2, i4AddrType);
}
SYNTAX   : no vrrp <vrid(1-255)> ipv6 priority
PRVID    : 15
HELP     : Sets the priority for the virtual router to default value.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           vrrp VRRP related configuration |
           (1-255) Virtual router ID |
           ipv6 IPv6 related configuration |
           priority Priority related configuration | 
           <CR> Sets the priority for the virtual router to default value

COMMAND  : vrrp <short(1-255)> [ ipv4 ] preempt [delay minimum <integer(0-30)>]
ACTION :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV4;

    if ($6 != NULL)
    {
        CliPrintf (CliHandle,
                   "Delay minimum is not supported\r\n");
    }

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_PREEMPT, NULL, $1, i4AddrType);
}
SYNTAX   : vrrp <vrid(1-255)> [ ipv4 ] preempt 
           [ delay minimum <value(0-30)> ]
PRVID    : 15
HELP     : Enables preempt mode.
CXT_HELP : vrrp Configures VRRP related information | 
           (1-255) Virtual router ID |
           ipv4 IPv4 related configuration |
           preempt Preempt mode related configuration | 
           delay Delay related configuration | 
           minimum Minimum value configuration | 
           (0-30) Number of seconds that the router will delay before issuing an advertisement claiming master ownership | 
           <CR> Enables preempt mode

COMMAND  : vrrp <short(1-255)> ipv6 preempt [delay minimum <integer(0-30)>]
ACTION :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV6;

    if ($6 != NULL)
    {
        CliPrintf (CliHandle,
                   "Delay minimum is not supported\r\n");
    }

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_PREEMPT, NULL, $1, i4AddrType);
}
SYNTAX   : vrrp <vrid(1-255)> ipv6 preempt 
           [ delay minimum <value(0-30)> ]
PRVID    : 15
HELP     : Enables preempt mode.
CXT_HELP : vrrp Configures VRRP related information | 
           (1-255) Virtual router ID |
           ipv6 IPv6 related configuration |
           preempt Preempt mode related configuration | 
           delay Delay related configuration | 
           minimum Minimum value configuration | 
           (0-30) Number of seconds that the router will delay before issuing an advertisement claiming master ownership | 
           <CR> Enables preempt mode

COMMAND  : no vrrp <short(1-255)> [ ipv4 ] preempt
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV4;

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_PREEMPT, NULL, $2, i4AddrType);
}
SYNTAX   : no vrrp <vrid(1-255)> [ ipv4 ] preempt 
PRVID    : 15
HELP     : Disables preempt mode
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           vrrp VRRP related configuration | 
           (1-255) Virtual router ID |
           ipv4 IPv4 related configuration |
           preempt Pre-empt mode related configuration | 
           <CR> Disables preempt mode

COMMAND  : no vrrp <short(1-255)> ipv6 preempt
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV6;

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_PREEMPT, NULL, $2, i4AddrType);
}
SYNTAX   : no vrrp <vrid(1-255)> ipv6 preempt 
PRVID    : 15
HELP     : Disables preempt mode
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           vrrp VRRP related configuration | 
           (1-255) Virtual router ID |
           ipv6 IPv6 related configuration |
           preempt Pre-empt mode related configuration | 
           <CR> Disables preempt mode

COMMAND  : vrrp <short(1-255)> text-authentication <random_str>
ACTION   : cli_process_vrrp_cmd(CliHandle,VRRP_CLI_AUTH,NULL,$1,$3);
SYNTAX   : vrrp <vrid(1-255)> text-authentication <password>
PRVID    : 15
HELP     : Sets the authentication type for the virtual router to simple password.
CXT_HELP : vrrp Configures VRRP related information | 
           (1-255) Virtual router ID | 
           text-authentication Simple password authentication related configuration | 
           <random_str> Authentication password used to validate the incoming VRRP packets | 
           <CR> Sets the authentication type for the virtual router to simple password

COMMAND  : no vrrp <short(1-255)> text-authentication
ACTION   : cli_process_vrrp_cmd(CliHandle,VRRP_CLI_NO_AUTH,NULL,$2);
SYNTAX   : no vrrp <vrid(1-255)> text-authentication
PRVID    : 15
HELP     : Sets the authentication type for the virtual router to none.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           vrrp VRRP related configuration | 
           (1-255) Virtual router ID | 
           text-authentication Simple password authentication related configuration | 
           <CR> Sets the authentication type for the virtual router to none

COMMAND  : vrrp <short(1-255)> [ ipv4 ] timer [msec] <integer(1-255000)>
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV4;
    
    if ($4 != NULL)
    {
        if (((*(UINT4 *) $5) % 10) != 0)
        {
            CliPrintf (CliHandle,
                       "\r%% Enter values in 10, 20, 30 .....msec only\r\n");
            return (CLI_FAILURE);
        }
        
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_MSEC_TIMER, NULL, $1, $5, i4AddrType);
     }
     else
     {
         cli_process_vrrp_cmd (CliHandle, VRRP_CLI_TIMER, NULL, $1, $5, i4AddrType);
     }
}
SYNTAX   : vrrp <vrid(1-255)> [ ipv4 ] timer [msec] <interval(1-255)secs>
PRVID    : 15
HELP     : Sets the advertisement timer for a virtual router
CXT_HELP : vrrp Configures VRRP related information |
           (1-255) Virtual router ID |
           ipv4 IPv4 related configuration |
           timer Timer related configuration |
           msec Unit is changed to milli-seconds |
           (1-255secs)/(10-255000msecs) Acceptable range for version 2 is (1-255secs)/(100-255000msecs) / Acceptable range for version 2-3 and version 3 is (1-40secs)/(10-40950msecs) |
           <CR> Sets the advertisement timer for a virtual router

COMMAND  : vrrp <short(1-255)> ipv6 timer [msec] <integer(1-255000)>
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV6;
    
    if ($4 != NULL)
    {
        if (((*(UINT4 *) $5) % 10) != 0)
        {
            CliPrintf (CliHandle,
                       "\r%% Enter values in 10, 20, 30 .....msec only\r\n");
            return (CLI_FAILURE);
        }
        
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_MSEC_TIMER, NULL, $1, $5, i4AddrType);
     }
     else
     {
         cli_process_vrrp_cmd (CliHandle, VRRP_CLI_TIMER, NULL, $1, $5, i4AddrType);
     }
}
SYNTAX   : vrrp <vrid(1-255)> ipv6 timer [msec] <interval(1-255)secs>
PRVID    : 15
HELP     : Sets the advertisement timer for a virtual router
CXT_HELP : vrrp Configures VRRP related information |
           (1-255) Virtual router ID |
           ipv6 IPv6 related configuration |
           timer Timer related configuration |
           msec Unit is changed to milli-seconds |
           (1-255secs)/(10-255000msecs) Time interval, in seconds/milli seconds, between successive advertisement messages |
           <CR> Sets the advertisement timer for a virtual router

COMMAND  : no vrrp <short(1-255)> [ ipv4 ] timer
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV4;

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_TIMER, NULL, $2, i4AddrType);
}
SYNTAX   : no vrrp <vrid(1-255)> [ ipv4 ] timer 
PRVID    : 15
HELP     : Sets the advertisement timer for a virtual router to default value.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           vrrp VRRP related configuration | 
           (1-255) Virtual router ID |
           ipv4 IPv4 related configuration |
           timer Timer related configuration | 
           <CR> Sets the advertisement timer for a virtual router to default value

COMMAND  : no vrrp <short(1-255)> ipv6 timer
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV6;

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_TIMER, NULL, $2, i4AddrType);
}
SYNTAX   : no vrrp <vrid(1-255)> ipv6 timer 
PRVID    : 15
HELP     : Sets the advertisement timer for a virtual router to default value.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           vrrp VRRP related configuration | 
           (1-255) Virtual router ID |
           ipv6 IPv6 related configuration |
           timer Timer related configuration | 
           <CR> Sets the advertisement timer for a virtual router to default value

COMMAND  : exit
ACTION   : 
{
    CLI_SET_IFINDEX (-1);
    CliChangePath ("..");
}
SYNTAX   : exit
PRVID    : 15
HELP     : exit to vrrp mode.
CXT_HELP : exit Exits to the VRRP router configuration (config-vrrp)# mode | 
           <CR> Exit to vrrp mode

COMMAND  : vrrp <short(1-255)> [ ipv4 ] accept-mode { enable | disable }     
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV4;
    
    if ($4 != NULL)
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_ACCEPT,
                              NULL, $1, VRRP_CLI_ACCEPT_ENABLE, i4AddrType);
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_ACCEPT, NULL, $1,
                              VRRP_CLI_ACCEPT_DISABLE, i4AddrType);
    }
}
SYNTAX   : vrrp <vrid(1-255)> [ ipv4 ] accept-mode { enable | disable }     
PRVID    : 15     
HELP     : Enables or disables accept mode.  
CXT_HELP : vrrp VRRP related configuration|     
           (1-255) Virtual router ID|
           ipv4 IPv4 related configuration|
           accept-mode Accept-mode related configuration|
           enable Enables Accept Mode|
           disable Disables Accept Mode|
           <CR> Enables or disables accept mode

COMMAND  : vrrp <short(1-255)> ipv6 accept-mode { enable | disable }     
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV6;
    
    if ($4 != NULL)
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_ACCEPT,
                              NULL, $1, VRRP_CLI_ACCEPT_ENABLE, i4AddrType);
    }
    else
    {
        cli_process_vrrp_cmd (CliHandle, VRRP_CLI_ACCEPT, NULL, $1,
                              VRRP_CLI_ACCEPT_DISABLE, i4AddrType);
    }
}
SYNTAX   : vrrp <vrid(1-255)> ipv6 accept-mode { enable | disable }     
PRVID    : 15     
HELP     : Enables or disables accept mode.  
CXT_HELP : vrrp VRRP related configuration|     
           (1-255) Virtual router ID|
           ipv6 IPv6 related configuration|
           accept-mode Accept-mode related configuration|
           enable Enables Accept Mode|
           disable Disables Accept Mode|
           <CR> Enables or disables accept mode

COMMAND  : vrrp <short(1-255)> [ ipv4 ] track <integer> decrement
           <integer(1-254)>
ACTION   : 
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV4;

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_OPER_TRACK, NULL, $1, $4, $6,
                          i4AddrType);
}
SYNTAX   : vrrp <vrid(1-255)> [ ipv4 ] track <group-index>
           decrement <integer(1-254)>
PRVID    : 15
HELP     : Configures the tracking group and decrement priority.
CXT_HELP : vrrp VRRP related configuration|
           (1-255) Virtual router ID |
           ipv4 IPv4 related configuration |
           track Track related configuration |
           <1-4294967295> Group Index Value |
           decrement Decrement Priority Configuration |
           (1-254) Priority Value |
           <CR> Configures the tracking group and decrement priority.

COMMAND  : vrrp <short(1-255)> ipv6 track <integer> decrement
           <integer(1-254)>
ACTION   : 
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV6;

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_OPER_TRACK, NULL, $1, $4, $6,
                          i4AddrType);
}
SYNTAX   : vrrp <vrid(1-255)> ipv6 track <group-index>
           decrement <integer(1-254)>
PRVID    : 15
HELP     : Configures the tracking group and decrement priority.
CXT_HELP : vrrp VRRP related configuration|
           (1-255) Virtual router ID |
           ipv6 IPv6 related configuration |
           track Track related configuration |
           <1-4294967295> Group Index Value |
           decrement Decrement Priority Configuration |
           (1-254) Priority Value |
           <CR> Configures the tracking group and decrement priority.

COMMAND  : no vrrp <short(1-255)> [ ipv4 ] track
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV4;

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_OPER_TRACK, NULL, $2, i4AddrType);
}
SYNTAX   : no vrrp <vrid(1-255)> [ ipv4 ] track
PRVID    : 15
HELP     : Resets the tracking group and decrement priority.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
           vrrp VRRP related configuration |
           (1-255) Virtual router ID |
           ipv4 IPv4 related configuration |
           track Track related configuration |
           <CR> Resets the tracking group and decrement priority.

COMMAND  : no vrrp <short(1-255)> ipv6 track
ACTION   :
{
    INT4 i4AddrType = VRRP_CLI_FMLY_IPV6;

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_OPER_TRACK, NULL, $2, i4AddrType);
}
SYNTAX   : no vrrp <vrid(1-255)> ipv6 track
PRVID    : 15
HELP     : Resets the tracking group and decrement priority.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
           vrrp VRRP related configuration |
           (1-255) Virtual router ID |
           ipv6 IPv6 related configuration |
           track Track related configuration |
           <CR> Resets the tracking group and decrement priority.

END GROUP

DEFINE GROUP: VRRP_DISPLAY_CMDS

COMMAND  : show vrrp [interface  { vlan <vlan_vfi_id> | <iftype> <ifnum> |
           <ipiftype> <ifnum> } <short(1-255)>] [{brief | detail |statistics}]
ACTION   :
{
    UINT4  u4Val = VRRP_CLI_BRIEF;
    INT4   i4VrId = 0;
    UINT4  u4IfIndex = 0;
    
    if ($2 != NULL)
    {
        if ((CfaCliGetIfIndex ($3, $4, &u4IfIndex) == CLI_FAILURE) &&
            (CfaCliGetIfIndex ($5, $6, &u4IfIndex) == CLI_FAILURE) &&
            (CfaCliGetIfIndex ($7, $8, &u4IfIndex) == CLI_FAILURE))
        {
            CliPrintf(CliHandle, "\r%%Invalid Interface Index \r\n");
            return CLI_FAILURE;
        }
    
        if ($9 != NULL)
        {
            i4VrId = *(INT4 *)$9;
        }
    }
    
    if ($10 != NULL)
    {
        u4Val = VRRP_CLI_BRIEF;
    }
    else if ($11 != NULL)
    {
        u4Val = VRRP_CLI_DETAIL;
    }
    else if ($12 != NULL)
    {
        u4Val = VRRP_CLI_STAT;
    }
    
    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_SHOW, NULL, u4Val, u4IfIndex,
                          i4VrId);
}
SYNTAX   : show vrrp [interface  { vlan <VlanId/vfi-id> | <interface-type>
           <interface-id> | <IP-interface-type> <IP-interface-number>}
           <VrId(1-255)>] [ { brief | detail | statistics } ]
PRVID    : 15
HELP     : Display VRRP status information.
CXT_HELP : show Displays the configuration / statistics / general information | 
           vrrp VRRP related configuration | 
           interface Interface related configuration | 
           vlan VLAN related configuration | 
           vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
           DYNiftype| 
           DYNifnum| 
           <ipiftype> IP Interface type |
           <ipifnum> IP Interface number | 
           (1-255) Virtual router ID | 
           brief Brief information | 
           detail Detailed information | 
           statistics Statistics related information | 
           <CR> Display VRRP status information

COMMAND  : show vrrp interface [ { vlan <vlan_vfi_id> | <iftype> <ifnum> |
           <ipiftype> <ifnum> } ] [ { brief | detail |statistics } ]
ACTION   :
{
    UINT4  u4Val = VRRP_CLI_BRIEF;
    UINT4  u4IfIndex = 0;
    
    if ($4 != NULL)
    {
        if (CfaCliGetIfIndex ($3, $4, &u4IfIndex) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,"\r%%Invalid Interface Index \r\n");
            return CLI_FAILURE;
        }
    }
    else if ($6 != NULL)
    {
        if (CfaCliGetIfIndex ($5, $6, &u4IfIndex) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,"\r%%Invalid Interface Index \r\n");
            return CLI_FAILURE;
        }
    }
    else if ($7 != NULL)
    {
        if (CfaCliGetIfIndex ($7, $8, &u4IfIndex) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Invalid Interface Index \r\n");
            return CLI_FAILURE;
        }
    }
    
    if ($9 != NULL)
    {
        u4Val = VRRP_CLI_BRIEF;
    }
    else if ($10 != NULL)
    {
        u4Val = VRRP_CLI_DETAIL;
    }
    else if ($11 != NULL)
    {
        u4Val = VRRP_CLI_STAT;
    }
    
    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_SHOW, NULL, u4Val, u4IfIndex, 0);
}
SYNTAX   : show vrrp interface [ { vlan <VlanId/vfi-id> | <interface-type>
           <interface-id> | <IP-interface-type> <IP-interface-number>}]
           [ { brief | detail | statistics } ]
PRVID    : 15
HELP     : Display VRRP status information.
CXT_HELP : show Displays the configuration / statistics / general information | 
           vrrp VRRP related configuration | 
           interface Interface related configuration | 
           vlan VLAN related configuration | 
           vlan_vfi_id The range (1-4094) is for VLAN ID and the range (4096 - 65535) is for VFI |
           DYNiftype| 
           DYNifnum| 
           <ipiftype> IP Interface type |
           <ipifnum> IP Interface number | 
           brief Brief information | 
           detail Detailed information | 
           statistics Statistics related information | 
           <CR> Display VRRP status information

COMMAND  : show track
ACTION   : cli_process_vrrp_cmd (CliHandle, VRRP_CLI_SHOW_TRACK, NULL);
SYNTAX   : show track
PRVID    : 15
HELP     : Display VRRP Track Group Information.
CXT_HELP : show Displays the configuration / statistics / general information |
           track Track related configuration |
           <CR> Display VRRP Track Group Information.

COMMAND  : debug ip vrrp { all | init | pkt | timers | events | failures |
                           memory | buffer | version2 | version3 | critical }
ACTION   : 
{
    UINT4 u4Value = 0;

    if ($3 != NULL)
    {
        u4Value = VRRP_CLI_TRC_ALL;
    }
    else if ($4 != NULL)
    {
        u4Value = VRRP_CLI_TRC_INIT;
    }
    else if ($5 != NULL)
    {
        u4Value = VRRP_CLI_TRC_PKT;
    }
    else if ($6 != NULL)
    {
        u4Value = VRRP_CLI_TRC_TIMERS;
    }
    else if ($7 != NULL)
    {
        u4Value = VRRP_CLI_TRC_EVENTS;
    }
    else if ($8 != NULL)
    {
        u4Value = VRRP_CLI_TRC_ALL_FAILURES;
    }
    else if ($9 != NULL)
    {
        u4Value = VRRP_CLI_TRC_MEMORY;
    }
    else if ($10 != NULL)
    {
        u4Value = VRRP_CLI_TRC_BUFFER;
    }
    else if ($11 != NULL)
    {
        u4Value = VRRP_CLI_TRC_VERSION_2;
    }
    else if ($12 != NULL)
    {
        u4Value = VRRP_CLI_TRC_VERSION_3;
    }
    else
    {
        u4Value = VRRP_CLI_TRC_STATE | VRRP_CLI_TRC_ALL_FAILURES;
    }

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_TRACE,
                          NULL, u4Value);
}
SYNTAX   : debug ip vrrp { all | init | pkt | timers | events | failures |
                           memory | buffer | version2 | version3 | state}
PRVID    : 15
HELP     : Enables debugging for all types of traces in the vrrp module 
CXT_HELP : debug Configures trace for the protocol|
              ip IP related configuration|
              vrrp VRRP related configuration|
              all All trace messages|
              init Initialization and Shutdown messages|
              pkt Packet dump traces|
              timers Timer related traces|
              events Event related traces|
              failures All failure traces |
              memory Memory related traces |
              buffer Buffer allocation / release traces |
              version2 Version 2 related traces |
              version3 Version 3 related traces |
              vrrp critical messages  |
              <CR> Enables debugging for all types of traces in the vrrp module

COMMAND  : no debug ip vrrp { all | init | pkt | timers | events | failures |
                              memory | buffer | version2 | version3 | critical }
ACTION   : 
{
    UINT4 u4Value = 0;

    if ($4 != NULL)
    {
        u4Value = VRRP_CLI_TRC_ALL;
    }
    else if ($5 != NULL)
    {
        u4Value = VRRP_CLI_TRC_INIT;
    }
    else if ($6 != NULL)
    {
        u4Value = VRRP_CLI_TRC_PKT;
    }
    else if ($7 != NULL)
    {
        u4Value = VRRP_CLI_TRC_TIMERS;
    }
    else if ($8 != NULL)
    {
        u4Value = VRRP_CLI_TRC_EVENTS;
    }
    else if ($9 != NULL)
    {
        u4Value = VRRP_CLI_TRC_ALL_FAILURES;
    }
    else if ($10 != NULL)
    {
        u4Value = VRRP_CLI_TRC_MEMORY;
    }
    else if ($11 != NULL)
    {
        u4Value = VRRP_CLI_TRC_BUFFER;
    }
    else if ($12 != NULL)
    {
        u4Value = VRRP_CLI_TRC_VERSION_2;
    }
    else if ($13 != NULL)
    {
        u4Value = VRRP_CLI_TRC_VERSION_3;
    }
    else
    {
        u4Value = VRRP_CLI_TRC_STATE | VRRP_CLI_TRC_ALL_FAILURES;
    }

    cli_process_vrrp_cmd (CliHandle, VRRP_CLI_NO_TRACE,
                          NULL, u4Value);
}
SYNTAX   : no debug ip vrrp { all | init | pkt | timers | events | failures |
                              memory | buffer | version2 | version3 | state }
PRVID    : 15
HELP     : Reset the debug level for VRRP module 
CXT_HELP : no Disables the configuration/deletes the entry/resets to default value|
	      debug Configures trace for the protocol|
              ip IP related configuration|
              vrrp VRRP related configuration|
              all All resources|
              init Initialization and Shutdown messages|
              pkt Packet Dump messages|
              timers Timers messages|
              events Events messages|
              failures All failure messages |
              memory Memory related messages |
              buffer Buffer related messages |
              version2 Version 2 related messages |
              version3 Version 3 related messages |
              vrrp critical messages |
              <CR> Set the debug level for VRRP module

#ifdef  VRRP_TEST_WANTED
COMMAND : execute vrrp ut { all | [case <integer> in] file { vrrpred | vrrpv3 }}
ACTION  :
    {
        UINT4 u4FileNumber = 0;
        if( $8 != NULL )
        {
            u4FileNumber = 1;
        }
        else if ($9 != NULL)
        {
            u4FileNumber = 2;
        }
        cli_process_vrrp_test_cmd (CliHandle, NULL,
                                  $3, &u4FileNumber, $5);
    }
SYNTAX  : execute vrrp ut { all | [case <integer> in] file { vrrpred | vrrpv3 }}
PRVID   : 1
HELP    : VRRP Unit Testing.
CXT_HELP : execute Executes a framework |
           vrrp vrrp related configuration |
           ut Unit Testing related configuration |
           all All test cases |
           case Specifies a test case |
           (1-x) Test case number in file (x is the max number of cases in a file) |
           in In
           file File name
           vrrpred Execute Redundancy related UT cases|
           vrrpv3 Execute VRRP V3 related UT cases|
           <CR> Executes the specified test cases
#endif

END GROUP
