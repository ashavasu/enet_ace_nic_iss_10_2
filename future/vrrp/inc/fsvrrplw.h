/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvrrplw.h,v 1.10 2014/07/20 11:09:51 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVrrpStatus ARG_LIST((INT4 *));

INT1
nmhGetFsVrrpMaxOperEntries ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVrrpStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVrrpStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVrrpStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVrrpOperTable. */
INT1
nmhValidateIndexInstanceFsVrrpOperTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVrrpOperTable  */

INT1
nmhGetFirstIndexFsVrrpOperTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVrrpOperTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVrrpAdminPriority ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsVrrpOperAdvertisementIntervalInMsec ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsVrrpOperTrackGroupId ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsVrrpOperDecrementPriority ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsVrrpAcceptMode ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVrrpAdminPriority ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsVrrpOperAdvertisementIntervalInMsec ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsVrrpOperTrackGroupId ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsVrrpOperDecrementPriority ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsVrrpAcceptMode ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVrrpAdminPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsVrrpOperAdvertisementIntervalInMsec ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsVrrpOperTrackGroupId ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsVrrpOperDecrementPriority ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsVrrpAcceptMode ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVrrpOperTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVrrpAuthDeprecate ARG_LIST((INT4 *));

INT1
nmhGetFsVrrpTraceOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVrrpAuthDeprecate ARG_LIST((INT4 ));

INT1
nmhSetFsVrrpTraceOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVrrpAuthDeprecate ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsVrrpTraceOption ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVrrpAuthDeprecate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsVrrpTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVrrpOperTrackGroupTable. */
INT1
nmhValidateIndexInstanceFsVrrpOperTrackGroupTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVrrpOperTrackGroupTable  */

INT1
nmhGetFirstIndexFsVrrpOperTrackGroupTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVrrpOperTrackGroupTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVrrpOperTrackedGroupTrackedLinks ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsVrrpOperTrackRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVrrpOperTrackedGroupTrackedLinks ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsVrrpOperTrackRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVrrpOperTrackedGroupTrackedLinks ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsVrrpOperTrackRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVrrpOperTrackGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsVrrpOperTrackGroupIfTable. */
INT1
nmhValidateIndexInstanceFsVrrpOperTrackGroupIfTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsVrrpOperTrackGroupIfTable  */

INT1
nmhGetFirstIndexFsVrrpOperTrackGroupIfTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsVrrpOperTrackGroupIfTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsVrrpOperTrackGroupIfRowStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsVrrpOperTrackGroupIfRowStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsVrrpOperTrackGroupIfRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsVrrpOperTrackGroupIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
