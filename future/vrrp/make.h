#
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# $Id: make.h,v 1.7 2013/01/23 11:34:15 siva Exp $
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 13 march 2000                                 |
# |                                                                          |
# |   DESCRIPTION            : This file contains all the warning options    |
# |                            that are used for building this module        |
# +--------------------------------------------------------------------------+

#include the LR make.h and make.rule
include ../LR/make.h
include ../LR/make.rule

#Project base directories
PROJECT_NAME   = FutureVrrp
PROJECT_BASE_DIR  = ${BASE_DIR}/vrrp
PROJECT_SOURCE_DIR = $(PROJECT_BASE_DIR)/src
PROJECT_INCLUDE_DIR = $(PROJECT_BASE_DIR)/inc
PROJECT_OBJECT_DIR = $(PROJECT_BASE_DIR)/obj

SNMP_INC_DIR            = ${BASE_DIR}/inc/snmp
COMN_INC_DIR            = ${BASE_DIR}/inc
FSAP_INC_DIR            = ${BASE_DIR}/fsap2/tmo

#Project compilation switches
PROJECT_COMPILATION_SWITCHES =

#Project include files
PROJECT_INCLUDE_FILES = $(PROJECT_INCLUDE_DIR)/vrrp_final_db.h \
                        $(PROJECT_INCLUDE_DIR)/vrrp_snmpmbdb.h \
                        $(PROJECT_INCLUDE_DIR)/vrrpcon.h \
                        $(PROJECT_INCLUDE_DIR)/vrrptypes.h \
                        $(PROJECT_INCLUDE_DIR)/vrrpglob.h \
                        $(PROJECT_INCLUDE_DIR)/vrrplow.h \
                        $(PROJECT_INCLUDE_DIR)/vrrpmid.h \
                        $(PROJECT_INCLUDE_DIR)/vrrpogi.h \
                        $(PROJECT_INCLUDE_DIR)/vrrpmac.h \
                        $(PROJECT_INCLUDE_DIR)/vrrpsem.h \
                        $(PROJECT_INCLUDE_DIR)/vrrpdef.h \
                        $(PROJECT_INCLUDE_DIR)/vrrpinc.h \
                        $(PROJECT_INCLUDE_DIR)/vrrpred.h

PROJECT_FINAL_INCLUDES_DIRS     = -I$(PROJECT_INCLUDE_DIR) \
                                        $(COMMON_INCLUDE_DIRS) \
                                        -I$(FSAP_INC_DIR)

#Project final include files
PROJECT_FINAL_INCLUDE_FILES   = $(PROJECT_INCLUDE_FILES)

#dependencies
PROJECT_DEPENDENCIES        =   $(COMMON_DEPENDENCIES) \
           $(PROJECT_FINAL_INCLUDE_FILES) \
           $(PROJECT_BASE_DIR)/Makefile \
           $(PROJECT_BASE_DIR)/make.h
