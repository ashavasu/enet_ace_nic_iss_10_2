#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :                                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 25th May 2000                                 |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

IF_COUNT_OPNS = -DCSW_MAX_ETH_IFS=16 -DCSW_MAX_V35_IFS=0 \
                -DCSW_MAX_T1E1_IFS=0 -DCSW_MAX_PER_IF_VCS=0

BRIDGE_SWITCHES = ${IF_COUNT_OPNS}


TOTAL_OPNS = ${BRIDGE_SWITCHES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

CFA_BASE_DIR   = ${BASE_DIR}/cfa2

BRIDGE_BASE_DIR = ${BASE_DIR}/bridge
BRIDGE_SRC_DIR  = ${BRIDGE_BASE_DIR}/src
BRIDGE_INC_DIR  = ${BRIDGE_BASE_DIR}/inc
BRIDGE_OBJ_DIR  = ${BRIDGE_BASE_DIR}/obj
STAP_SRC_DIR    = ${BRIDGE_SRC_DIR}
STAP_INC_DIR    = ${BRIDGE_INC_DIR}
STAP_OBJ_DIR    = ${BRIDGE_OBJ_DIR}
BASE_INC_DIR    = $(BASE_DIR)/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${BRIDGE_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
