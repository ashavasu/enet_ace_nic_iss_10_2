/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbrilow.h,v 1.3 2007/02/01 14:44:35 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dBridgeSystemControl ARG_LIST((INT4 *));

INT1
nmhGetDot1dBaseBridgeStatus ARG_LIST((INT4 *));

INT1
nmhGetDot1dBaseBridgeCRCStatus ARG_LIST((INT4 *));

INT1
nmhGetDot1dBaseBridgeDebug ARG_LIST((INT4 *));

INT1
nmhGetDot1dBaseBridgeTrace ARG_LIST((INT4 *));

INT1
nmhGetDot1dBaseBridgeMaxFwdDbEntries ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dBridgeSystemControl ARG_LIST((INT4 ));

INT1
nmhSetDot1dBaseBridgeStatus ARG_LIST((INT4 ));

INT1
nmhSetDot1dBaseBridgeCRCStatus ARG_LIST((INT4 ));

INT1
nmhSetDot1dBaseBridgeDebug ARG_LIST((INT4 ));

INT1
nmhSetDot1dBaseBridgeTrace ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dBridgeSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1dBaseBridgeStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1dBaseBridgeCRCStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1dBaseBridgeDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Dot1dBaseBridgeTrace ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for Dot1dFutureBasePortTable. */
INT1
nmhValidateIndexInstanceDot1dFutureBasePortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dFutureBasePortTable  */

INT1
nmhGetFirstIndexDot1dFutureBasePortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dFutureBasePortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dBasePortAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dBasePortOperStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dBasePortBcastStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dBasePortFilterNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dBasePortMcastNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dBasePortBcastOutFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1dBasePortMcastOutFrames ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dBasePortAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1dBasePortBcastStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1dBasePortFilterNumber ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDot1dBasePortMcastNumber ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dBasePortAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1dBasePortBcastStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1dBasePortFilterNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Dot1dBasePortMcastNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for Dot1dFutureTpPortTable. */
INT1
nmhValidateIndexInstanceDot1dFutureTpPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dFutureTpPortTable  */

INT1
nmhGetFirstIndexDot1dFutureTpPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dFutureTpPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dTpPortInProtoDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1dTpPortInFilterDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1dTpPortProtocolFilterMask ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dTpPortProtocolFilterMask ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dTpPortProtocolFilterMask ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for Dot1dFilterTable. */
INT1
nmhValidateIndexInstanceDot1dFilterTable ARG_LIST((INT4  , tMacAddr  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for Dot1dFilterTable  */

INT1
nmhGetFirstIndexDot1dFilterTable ARG_LIST((INT4 * , tMacAddr *  , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dFilterTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr *  , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dFilterSrcMask ARG_LIST((INT4  , tMacAddr  , tMacAddr ,tMacAddr * ));

INT1
nmhGetDot1dFilterDstMask ARG_LIST((INT4  , tMacAddr  , tMacAddr ,tMacAddr * ));

INT1
nmhGetDot1dFilterPermiss ARG_LIST((INT4  , tMacAddr  , tMacAddr ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dFilterSrcMask ARG_LIST((INT4  , tMacAddr  , tMacAddr ,tMacAddr ));

INT1
nmhSetDot1dFilterDstMask ARG_LIST((INT4  , tMacAddr  , tMacAddr ,tMacAddr ));

INT1
nmhSetDot1dFilterPermiss ARG_LIST((INT4  , tMacAddr  , tMacAddr ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dFilterSrcMask ARG_LIST((UINT4 *  ,INT4  , tMacAddr  , tMacAddr ,tMacAddr ));

INT1
nmhTestv2Dot1dFilterDstMask ARG_LIST((UINT4 *  ,INT4  , tMacAddr  , tMacAddr ,tMacAddr ));

INT1
nmhTestv2Dot1dFilterPermiss ARG_LIST((UINT4 *  ,INT4  , tMacAddr  , tMacAddr ,INT4 ));

/* Proto Validate Index Instance for Dot1dMcastTable. */
INT1
nmhValidateIndexInstanceDot1dMcastTable ARG_LIST((tMacAddr  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dMcastTable  */

INT1
nmhGetFirstIndexDot1dMcastTable ARG_LIST((tMacAddr *  , INT4 * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dMcastTable ARG_LIST((tMacAddr , tMacAddr *  , INT4 , INT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dMcastPermiss ARG_LIST((tMacAddr  , INT4  , INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dMcastPermiss ARG_LIST((tMacAddr  , INT4  , INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dMcastPermiss ARG_LIST((UINT4 *  ,tMacAddr  , INT4  , INT4 ));
