/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbriwr.h,v 1.3 2011/11/03 07:16:59 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/

#ifndef _STDBRIWR_H
#define _STDBRIWR_H

VOID RegisterSTDBRI(VOID);
INT4 Dot1dBaseBridgeAddressGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dBaseNumPortsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dBaseTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1dBasePortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dBasePortGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dBasePortIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dBasePortCircuitGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dBasePortDelayExceededDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dBasePortMtuExceededDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpProtocolSpecificationGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpTimeSinceTopologyChangeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpTopChangesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpRootCostGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpRootPortGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpBridgeMaxAgeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpBridgeHelloTimeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpBridgeForwardDelayGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpBridgeMaxAgeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpBridgeHelloTimeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpBridgeForwardDelaySet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dStpBridgeMaxAgeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dStpBridgeHelloTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dStpBridgeForwardDelayTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPriorityDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1dStpBridgeMaxAgeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1dStpBridgeHelloTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 Dot1dStpBridgeForwardDelayDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexDot1dStpPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dStpPortGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortPriorityGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortStateGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortEnableGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortPathCostGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortDesignatedRootGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortDesignatedCostGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortDesignatedBridgeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortDesignatedPortGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortForwardTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortPathCost32Get(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortPrioritySet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortEnableSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortPathCostSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortPathCost32Set(tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortPathCostTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortPathCost32Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dStpPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 Dot1dTpLearnedEntryDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpAgingTimeGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpAgingTimeSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpAgingTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dTpAgingTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexDot1dTpFdbTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dTpFdbAddressGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpFdbPortGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpFdbStatusGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1dTpPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dTpPortGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpPortMaxInfoGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpPortInFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpPortOutFramesGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dTpPortInDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexDot1dStaticTable(tSnmpIndex *, tSnmpIndex *);
INT4 Dot1dStaticAddressGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStaticReceivePortGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStaticAllowedToGoToGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStaticStatusGet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStaticAllowedToGoToSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStaticStatusSet(tSnmpIndex *, tRetVal *);
INT4 Dot1dStaticAllowedToGoToTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dStaticStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Dot1dStaticTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STDBRIWR_H */
