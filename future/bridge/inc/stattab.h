/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stattab.h,v 1.2 2007/02/01 14:44:35 iss Exp $
 *
 * Description:This file contains the typedef of the
 *             static table 
 *
 *******************************************************************/
#ifndef _STATIC_TAB_H
#define _STATIC_TAB_H

#define   MAX_STATICTABLE_ENTRIES    200
#define   STATIC_TABLE_ENTRY_AGEOUT_TIME    1000
#define   STATIC_OTHER                         1
#define   STATIC_INVALID                       2
#define   STATIC_PERMANENT                     3
#define   STATIC_DELETEONRESET                 4
#define   STATIC_DELETEONTIMEOUT               5



/*
 * This data definition is for the single node of staticTable list
 */

typedef struct STATICTABLE_ENTRY {
    tTMO_SLL_NODE  link;                                
    tTIMER         timeout;                             /* deleteontimeout */
    UINT1          u1StaticAllowedToGo[MAX_NO_BYTES];  
    /* dot1dStaticAllowedToGo */
    tMacAddr       staticAddress;                       /* dot1dStaticAddress */
    UINT1          u1StaticReceivePort;                
    /* dot1dStaticReceivePort */
    UINT1          u1StaticStatus;                     /* dot1dStaticStatus */
    UINT1          au1Reserved[3];
} tSTATICTABLE_ENTRY;

typedef struct STATICTABLE_LIST
{
    tTMO_SLL  entry;
} tSTATICTABLE_LIST;

#endif/* _STATIC_TAB_H */
