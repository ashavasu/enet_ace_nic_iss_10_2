/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bmport.h,v 1.4 2007/02/01 14:44:35 iss Exp $
 *
 * Description:System specific calls and other parameter
 *             to alter the operation of the bridge
 *
 *******************************************************************/
#ifndef _BMPORT_H 
#define _BMPORT_H

#define   FILL(tar,cnt,byte)   memset(tar,byte,cnt) 

#define   GET_IFACEID_FROM_BUF_HEADER(x)  \
          ((tTMO_INTERFACE *)&((x)->u4_interface_id))

#define   BRG_MEM_SET(inf1,data,cnt)  MEMSET(inf1,data,cnt)
#define   BRG_MEM_CPY(inf1,inf2,cnt)  MEMCPY(inf1,inf2,cnt)
#define   BRG_MEM_CMP(inf1,inf2,cnt)  MEMCMP(inf1,inf2,cnt)
#define   MEM_CPY(inf1,inf2,cnt)      MEMCPY(inf1,inf2,cnt)

/* 
**  trap related defines
**
*/ 

#define   BRG_TRAP_PROTOCOL_STAP         4                            
#define   CRU_TRAP_PROTOCOL_STAP         BRG_TRAP_PROTOCOL_STAP       
#define   BRG_MEMORY_ALLOCATION_FAILED   3                            
#define   CRU_MEMORY_ALLOCATION_FAILED   BRG_MEMORY_ALLOCATION_FAILED 
#define   BRG_NO_MORE_BUFFERS            103                          
#define   CRU_NO_MORE_BUFFERS            BRG_NO_MORE_BUFFERS          
#define   BRG_CHAIN_BUFFERS_EXHAUSTED    1                            
#define   CRU_CHAIN_BUFFERS_EXHAUSTED    BRG_CHAIN_BUFFERS_EXHAUSTED  
#define   BRG_TRAP_PROTOCOL_BRIDGING     3                            
#define   CRU_TRAP_PROTOCOL_BRIDGING     BRG_TRAP_PROTOCOL_BRIDGING   
#define   BRG_PROTOCOL_UP                100                          
#define   CRU_PROTOCOL_UP                BRG_PROTOCOL_UP              
#define   BRG_PROTOCOL_DOWN              101                          
#define   CRU_PROTOCOL_DOWN              BRG_PROTOCOL_DOWN            
#define   BRG_NEW_ROOT                   106                          
#define   CRU_NEW_ROOT                   BRG_NEW_ROOT                 
#define   BRG_TOPOLOGY_CHANGE_TRAP       105                          
#define   CRU_TOPOLOGY_CHANGE_TRAP       BRG_TOPOLOGY_CHANGE_TRAP     

/*
** CRU related protocol type constants 
**
*/

#define   BRG_IP                          0x0004                        
#define   BRG_IPX                         0x0008                        
#define   CRU_IPX                         BRG_IPX                       
#define   BRG_XNS                         0x0010                        
#define   CRU_XNS                         BRG_XNS                       
#define   BRG_DECNET                      0x0020                        
#define   CRU_DECNET                      BRG_DECNET                    
#define   BRG_ATALK                       0x0080                        
#define   CRU_ATALK                       BRG_ATALK                     
#define   BRG_PPPOE                       0x0100
#define   BRG_PROTOCOL_UNKNOWN            0x0000                        
#define   CRU_PROTOCOL_UNKNOWN            BRG_PROTOCOL_UNKNOWN          
#define   BRG_BRIDGE_WITHOUT_CRC_MODULE   0x0E                          
#define   CRU_BRIDGE_WITHOUT_CRC_MODULE   BRG_BRIDGE_WITHOUT_CRC_MODULE 

#define   CRU_MAX_ENET_INTERFACES         CSW_MAX_ETH_IFS               
#define   CRU_MAX_T1E1_INTERFACES         CSW_MAX_T1E1_IFS              
#define   CRU_MAX_V35_INTERFACES          CSW_MAX_V35_IFS               

#define   CRU_MAX_WAN_INTERFACES         \
          (CRU_MAX_V35_INTERFACES+CRU_MAX_T1E1_INTERFACES)

#define   BRG_BRIDGING                          0x0001                     
#define   CRU_BRIDGING                          BRG_BRIDGING               
#define   BRG_STAP                              0x0002                     
#define   CRU_STAP                              BRG_STAP                   
#define   CRU_OUG_ENET_HEADER_OFFSET            (0)                        

#define   tTMO_BUF_DATA_DESC                    tCRU_BUF_DATA_DESC         
#define   tTMO_INTERFACE                        tCRU_INTERFACE             
#define   BRG_BUF_CHAIN_DESC                    tCRU_BUF_CHAIN_DESC        
#define   TMO_TIMER_01_EXP_EVENT                STAP_TIMER_EXP_EVENT       
#define   BRG_SUCCESS                           OSIX_SUCCESS               
#define   BRG_FAILURE                           OSIX_FAILURE               
#define   BRG_MEM_SUCCESS                       MEM_SUCCESS                
#define   BRG_MEM_FAILURE                       MEM_FAILURE                
#define   BRG_OSIX_MSG_NORMAL                   OSIX_MSG_NORMAL

	
#define   BRG_WITHOUT_CRC_MODULE                0x0E                       
#define   CRU_BRIDGE_WITHOUT_CRCMODULE          BRG_WITHOUT_CRC_MODULE     

#define   BRG_COMMON_FORWARD_MODULE             0x08                       
#define   CRU_COMMON_FORWARD_MODULE             BRG_COMMON_FORWARD_MODULE  

#define   BRG_BRIDGE_STAP_MODULE                0x10                       
#define   CRU_BRIDGE_STAP_MODULE                BRG_BRIDGE_STAP_MODULE     

#define   BRG_OUG_STAP_HEADER_OFFSET            17                         
#define   CRU_OUG_STAP_HEADER_OFFSET            BRG_OUG_STAP_HEADER_OFFSET 
#define   CRU_OUG_STAP_OVER_PPP_HEADER_OFFSET   4                          


#define   BRG_NTOHL(addr)            OSIX_NTOHL(addr)                           
#define   BRG_NTOHS(addr)            OSIX_NTOHS(addr)                           

#define   BRG_HTONL(lanaddr,value)   *((UINT4 *)(lanaddr)) = OSIX_HTONL(value); 
#define   BRG_HTONS(lanaddr,value)   *((UINT2 *)(lanaddr)) = OSIX_HTONS(value); 



#define  BRG_TIMER_SUCCESS  TMR_SUCCESS
#define  BRG_TIMER_FAILURE  TMR_FAILURE

#define BRG_GET_NEXT_EXPIRED_TIMER TmrGetNextExpiredTimer 
#define BRG_SEND_EVENT  OSS_SEND_EVENT	
#define BRG_SEND_TO_QUEUE OsixSendToQ

/* Osix Related Macros  */

#define  OSS_SPAWN_TASK(task_name,priority,stack_size,task,args,mode,task_id)  \
         OsixCreateTask(task_name,priority,stack_size,task,args,mode,task_id)  

#define  BRG_DELETE_TASK(nodeId, task_name)  \
         OsixDeleteTask(nodeId, task_name)  

#define  OSS_WAIT_EVENT(event_mask,flags,timeout,event_recd) \
         OsixReceiveEvent(event_mask,flags,timeout,event_recd) 

#define  OSS_SEND_EVENT(self,task_name,event) \
         OsixSendEvent(self,task_name,event)

#define  ADD_TO_CFA_QUEUE(nodeid,qname,msg,msgpri)  \
         OsixSendToQ(nodeid,qname,msg,msgpri)     

#define  BRIDGE_CREATE_TMR_LIST(taskname,event,clbkfn,tmrlist) \
         TmrCreateTimerList(taskname,event,clbkfn,tmrlist) 

#define  BRG_DELETE_TMR_LIST(tmrlist) TmrDeleteTimerList(tmrlist)

#define  BRIDGE_START_TMR(tlistid,brgref,time) \
         TmrStartTimer(tlistid,brgref,time)

#define  BRIDGE_STOP_TMR(tlistid,brgref) TmrStopTimer(tlistid,brgref)

#define  BRIDGE_GET_SYS_TIME(pSysTime) \
         OsixGetSysTime((tOsixSysTime *)pSysTime)

#define  BRG_CREATE_QUEUE(qname,qdepth,mod,id) \
	 OsixCreateQ (qname,qdepth,mod,id)
#define  BRG_DELETE_QUEUE(qid, qname)      OsixDeleteQ (qid, qname)

#define  BRG_DEQUEUE_MESSAGES(qname,wait,ppmsg)\
	             OsixReceiveFromQ(SELF, (qname),(wait),0,((tOsixMsg **)(ppmsg)))

#define  BRG_NO_WAIT    OSIX_NO_WAIT
	
		
/*
 * Memory allocation macros
 */

#define  BRG_ALLOC_MEM_BLOCK(poolid,ppnode) \
         MemAllocateMemBlock((poolid), (UINT1 **)(ppnode))
	
#define  BRG_RELEASE_MEM_BLOCK(poolid, pNode) \
         MemReleaseMemBlock((poolid), (UINT1 *)(pNode)) 

#define  STAP_CREATE_MEM_POOL(size, maxmsg, defmem, msgplid)\
         MemCreateMemPool(size, maxmsg, defmem, msgplid) 

#define  BRG_DELETE_MEM_POOL(msgPlId) MemDeleteMemPool (msgPlId)

#define  BRG_DELETE_HASH_TABLE  TMO_HASH_Delete_Table 

#define  BRG_BUF_ALLOCATE_CHAIN(size,offset)  \
         CRU_BUF_Allocate_MsgBufChain(size,offset)

#define  STAP_BUF_RELEASE_CHAIN(pBufChain,FALSE) \
         CRU_BUF_Release_MsgBufChain(pBufChain,FALSE)
 
#define  ALLOC_MCAST_ENTRY(mcastmsgid,ptr)\
         MemAllocateMemBlock(mcastmsgid,(UINT1 **)ptr) 

#define  FREE_MCAST_ENTRY(mcastid,ptr) \
         MemReleaseMemBlock(mcastid,(UINT1*)ptr)

#define  ALLOC_STATICTABLE_ENTRY(staticTabid,ptr)\
         MemAllocateMemBlock(staticTabid,(UINT1 **)ptr)

#define  FREE_STATICTABLE_ENTRY(staticTabid,ptr)  \
         MemReleaseMemBlock(staticTabid,(UINT1*)ptr)

#define  ALLOC_FWD_ENTRY(fwdPoolId,ptr)\
         MemAllocateMemBlock(fwdPoolId,(UINT1 **)ptr)

#define  FREE_FWD_ENTRY(fwdPoolId,ptr)  \
         MemReleaseMemBlock(fwdPoolId,(UINT1*)ptr)

#define  ALLOC_FILTER_ENTRY(fltmsgid,ptr) \
         MemAllocateMemBlock(fltmsgid,(UINT1 **)ptr) 

#define  FREE_FILTER_ENTRY(filtid,ptr) \
         MemReleaseMemBlock(filtid,(UINT1*)ptr)

#define  ALLOC_STAP_NODE(stmsgid,ptr) \
         MemAllocateMemBlock(stmsgid,(UINT1 **)ptr)
 
#define  FREE_STAP_NODE(stapid,ptr) \
         MemReleaseMemBlock(stapid,(UINT1*)ptr)

#define  BRG_BUF_GET_CHAIN_VALIDBYTECOUNT(ptr)   \
         CRU_BUF_Get_ChainValidByteCount(ptr)    

#define  BRG_BUF_DELETE_BUF_CHAINATEND(pMsg,size )  \
         CRU_BUF_Delete_BufChainAtEnd(pMsg,size )

#define  BRG_BUF_SET_SOURCEMODULEID(pMsg,srcid )\
         CRU_BUF_Set_SourceModuleId(pMsg,srcid )

#define  BRG_BUF_SET_DESTINMODULEID(pMsg, destinid)  \
         CRU_BUF_Set_DestinModuleId(pMsg, destinid)
    
#define  BRG_BUF_SET_INTERFACEID(pMsg,ifaceid) \
         CRU_BUF_Set_InterfaceId(pMsg,ifaceid) 
    
#define  BRG_BUF_MOVE_VALID_OFFSET(pMsg,size) \
         CRU_BUF_Move_ValidOffset(pMsg,size) 
    
#define  BRG_BUF_COPY_FROM_BUFCHAIN(pMsg,dst_ptr,offset,size) \
         CRU_BUF_Copy_FromBufChain(pMsg,(UINT1*)dst_ptr,offset,size) 
    
#define  BRG_BUF_COPY_OVER_CHAIN(pChainhdr,p_data,offset,size)  \
         CRU_BUF_Copy_OverBufChain(pChainhdr,p_data,offset,size) 

#define  BRG_BUF_GET_INTERFACEID(ptr)        CRU_BUF_Get_InterfaceId(ptr)      
#define  BRG_BUF_GET_INTERFACE_TYPE(iface)   CRU_BUF_Get_Interface_Type(iface) 


/* Following macros are added for FSAP2 conversion 28Sept 1999 */
#define  BRG_BMC_GET_DATAPOINTER(pMsg) \
        (((tCRU_BUF_CHAIN_DESC*)pMsg)->pFirstValidDataDesc->pu1_FirstValidByte)

#define  BRG_BMC_ASSIGN_1_BYTE(pBufChain,u4_Offset,u1_Value) \
{\
   UINT1 au1_Buf_Byte;\
   CRU_BUF_Copy_OverBufChain(pBufChain,(au1_Buf_Byte = (u1_Value),&au1_Buf_Byte),u4_Offset,1);\
}

#define BRG_BMC_ASSIGN_2_BYTE(pBufChain,u4_Offset,u2_Value) \
{\
   UINT1 au1_Buf_Word_Arr[2];\
   BRG_HTONS(au1_Buf_Word_Arr,(u2_Value)) \
   CRU_BUF_Copy_OverBufChain(pBufChain,au1_Buf_Word_Arr,u4_Offset,2);\
}

#define BRG_BMC_ASSIGN_4_BYTE(pBufChain,u4_Offset,u4_Value) \
{\
   UINT1 au1_Buf_Dword_Arr[4];\
   BRG_HTONL(au1_Buf_Dword_Arr,(u4_Value)) \
   CRU_BUF_Copy_OverBufChain(pBufChain,au1_Buf_Dword_Arr,u4_Offset,4);\
}

#define BRG_BUF_SET_CHAIN_BYTE(pChainHdr,u4_Offset,u1_ByteValue)\
        BRG_BMC_ASSIGN_1_BYTE(pChainHdr,u4_Offset,u1_ByteValue)

#define BRG_BUF_SET_CHAIN_WORD(pChainHdr,u4_Offset,u2_WordValue)\
        BRG_BMC_ASSIGN_2_BYTE(pChainHdr,u4_Offset,u2_WordValue)

#define BRG_BUF_SET_CHAIN_DWORD(pChainHdr,u4_Offset,u4_DWordValue)\
        BRG_BMC_ASSIGN_4_BYTE(pChainHdr,u4_Offset,u4_DWordValue)
 


/*
 * LOCAL STAP message processing macros
 */

#define   ADD_TO_STAP_QUEUE(ptr, size)   \
          (TMO_SLL_Add (&stapQueue, (tTMO_SLL_NODE *) ptr))
#define   GET_FROM_STAP_QUEUE()            TMO_SLL_Get (&stapQueue) 
#define   REMOVE_FROM_STAP_QUEUE(pNode)   \
          TMO_SLL_Delete (&stapQueue, (tTMO_SLL_NODE*)pNode)

/*Queue related Variable*/
typedef tOsixQId     tBrgQId; 
typedef  tMemPoolId  tBrgMemPoolId;  
typedef  tOsixTaskId tBrgTaskId;  



#define   PRT_PREPARE_FLOOD_BUFFER_OK(pMsg,iface,crc,flag, ret_code) 
#define   UTL_UPDATE_MSG_TO_THIS_IFACE(pMsg, iface)     \
          CRU_BMC_Set_InterfaceId (pMsg, CONV_UINT4 (iface));


#define BRG_BMC_WTOPDU(pu1PduAddr,u2Value) \
          *((UINT2 *)(pu1PduAddr)) = (UINT2)OSIX_HTONS(u2Value);

/* Transit time handling */
#define   MAX_BRG_TRANSIT_DELAY      1
#define   BRG_TRANSIT_DELAY_TICKS    MAX_BRG_TRANSIT_DELAY *     \
                                     SYS_TIME_TICKS_IN_A_SEC

/* Timer Task Related */
#define BRIDGE_TMR_TASK_PRIORITY 35
#define TIME_QUANTUM               1                             
#define BRG_TIME_DURATION          (1 * SYS_TIME_TICKS_IN_A_SEC) 
                                     
/* BRG_TIMER_EXP_EVENT            0x0001 defined in bridge.h */ 
#define BRG_START_EVENT           0x0002 
#define BRG_STOP_EVENT            0x0004
/* IGS_TMR_EXPIRY_EVENT           0x0100 defined in igs.h */

#define BRG_INIT_COMPLETE(u4Status)	lrInitComplete(u4Status)

#endif/* _BMPORT_H */
