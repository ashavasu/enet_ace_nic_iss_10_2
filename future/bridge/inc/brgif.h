/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brgif.h,v 1.2 2007/02/01 14:44:35 iss Exp $
 *
 * Description: Contains Bridge interface layer constants.
 *
 *******************************************************************/
#ifndef _BRG_IF_H
#define _BRG_IF_H

#define BRG_GMRP_ADDR          0x0180c2000020
#define BRG_GVRP_ADDR          0x0180c2000021

#define BRG_HIGH_SPEED                650000000

#define BRG_INVALID_PKT               3


#define BRG_ENET_LLC_HEADER_SIZE      17
#define BRG_ENET_HDR_WITHOUTLEN_SIZE  12
#define BRG_BPDU_OFFSET_FROM_ENETLEN  5



#endif /* _BRG_IF_H */
