/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mlist.h,v 1.2 2007/02/01 14:44:35 iss Exp $
 *
 * Description:This file contains the typedef of the
 *             filtering and the forwarding databases
 *
 *******************************************************************/
#ifndef _MLIST_H
#define _MLIST_H


#define   MAX_MULTILIST_NUMBER    100
#define   MAX_MCAST_ENTRIES       100
#define   MCAST_DISCARD    1
#define   MCAST_FORWARD    2
#define   MCAST_INVALID    3

/*
 * This data definition is for the single node of multicast list
 */
typedef struct MULTICAST_ENTRY
{
    tTMO_SLL_NODE  link;
    tMacAddr    multicastAddr;
    UINT1          u1Permiss;
    UINT1          reserved1;
} tMULTICAST_ENTRY;



/*
 * The data structure for each single linked list
 */
typedef struct MULTICAST_LIST 
{
    tTMO_SLL  entry;
} tMULTICAST_LIST;


#endif /*_MLIST_H */
