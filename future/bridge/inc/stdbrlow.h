/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbrlow.h,v 1.3 2007/02/01 14:44:35 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dBaseBridgeAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetDot1dBaseNumPorts ARG_LIST((INT4 *));

INT1
nmhGetDot1dBaseType ARG_LIST((INT4 *));

/* Proto Validate Index Instance for Dot1dBasePortTable. */
INT1
nmhValidateIndexInstanceDot1dBasePortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dBasePortTable  */

INT1
nmhGetFirstIndexDot1dBasePortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dBasePortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dBasePortIfIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dBasePortCircuit ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDot1dBasePortDelayExceededDiscards ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1dBasePortMtuExceededDiscards ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetDot1dStpProtocolSpecification ARG_LIST((INT4 *));

extern INT1
nmhGetDot1dStpPriority ARG_LIST((INT4 *));

extern INT1
nmhGetDot1dStpTimeSinceTopologyChange ARG_LIST((UINT4 *));

extern INT1
nmhGetDot1dStpTopChanges ARG_LIST((UINT4 *));

extern INT1
nmhGetDot1dStpDesignatedRoot ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetDot1dStpRootCost ARG_LIST((INT4 *));

extern INT1
nmhGetDot1dStpRootPort ARG_LIST((INT4 *));

extern INT1
nmhGetDot1dStpMaxAge ARG_LIST((INT4 *));

extern INT1
nmhGetDot1dStpHelloTime ARG_LIST((INT4 *));

extern INT1
nmhGetDot1dStpHoldTime ARG_LIST((INT4 *));

extern INT1
nmhGetDot1dStpForwardDelay ARG_LIST((INT4 *));

extern INT1
nmhGetDot1dStpBridgeMaxAge ARG_LIST((INT4 *));

extern INT1
nmhGetDot1dStpBridgeHelloTime ARG_LIST((INT4 *));

extern INT1
nmhGetDot1dStpBridgeForwardDelay ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetDot1dStpPriority ARG_LIST((INT4 ));

extern INT1
nmhSetDot1dStpBridgeMaxAge ARG_LIST((INT4 ));

extern INT1
nmhSetDot1dStpBridgeHelloTime ARG_LIST((INT4 ));

extern INT1
nmhSetDot1dStpBridgeForwardDelay ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2Dot1dStpPriority ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2Dot1dStpBridgeMaxAge ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2Dot1dStpBridgeHelloTime ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2Dot1dStpBridgeForwardDelay ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for Dot1dStpPortTable. */
extern INT1
nmhValidateIndexInstanceDot1dStpPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dStpPortTable  */

extern INT1
nmhGetFirstIndexDot1dStpPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexDot1dStpPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetDot1dStpPortPriority ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDot1dStpPortState ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDot1dStpPortEnable ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDot1dStpPortPathCost ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDot1dStpPortDesignatedRoot ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetDot1dStpPortDesignatedCost ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetDot1dStpPortDesignatedBridge ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetDot1dStpPortDesignatedPort ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetDot1dStpPortForwardTransitions ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetDot1dStpPortPriority ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetDot1dStpPortEnable ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetDot1dStpPortPathCost ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2Dot1dStpPortPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Dot1dStpPortEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Dot1dStpPortPathCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dTpLearnedEntryDiscards ARG_LIST((UINT4 *));

INT1
nmhGetDot1dTpAgingTime ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dTpAgingTime ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dTpAgingTime ARG_LIST((UINT4 *  ,INT4 ));

/* Proto Validate Index Instance for Dot1dTpFdbTable. */
INT1
nmhValidateIndexInstanceDot1dTpFdbTable ARG_LIST((tMacAddr));

/* Proto Type for Low Level GET FIRST fn for Dot1dTpFdbTable  */

INT1
nmhGetFirstIndexDot1dTpFdbTable ARG_LIST((tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dTpFdbTable ARG_LIST((tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dTpFdbPort ARG_LIST((tMacAddr  ,INT4 *));

INT1
nmhGetDot1dTpFdbStatus ARG_LIST((tMacAddr  ,INT4 *));

/* Proto Validate Index Instance for Dot1dTpPortTable. */
INT1
nmhValidateIndexInstanceDot1dTpPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dTpPortTable  */

INT1
nmhGetFirstIndexDot1dTpPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dTpPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dTpPortMaxInfo ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDot1dTpPortInFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1dTpPortOutFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1dTpPortInDiscards ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for Dot1dStaticTable. */
INT1
nmhValidateIndexInstanceDot1dStaticTable ARG_LIST((tMacAddr  , INT4  ));

/* Proto Type for Low Level GET FIRST fn for Dot1dStaticTable  */

INT1
nmhGetFirstIndexDot1dStaticTable ARG_LIST((tMacAddr *  , INT4 * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dStaticTable ARG_LIST((tMacAddr , tMacAddr *  , INT4 , INT4 * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dStaticAllowedToGoTo ARG_LIST((tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDot1dStaticStatus ARG_LIST((tMacAddr  , INT4  ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDot1dStaticAllowedToGoTo ARG_LIST((tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDot1dStaticStatus ARG_LIST((tMacAddr  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Dot1dStaticAllowedToGoTo ARG_LIST((UINT4 *  ,tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Dot1dStaticStatus ARG_LIST((UINT4 *  ,tMacAddr  , INT4  ,INT4 ));
