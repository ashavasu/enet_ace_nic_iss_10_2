/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: typdfs.h,v 1.2 2007/02/01 14:44:35 iss Exp $
 *
 * Description: Data structures and Interface related macros used in 
 *              Bridge
 *
 *******************************************************************/
#ifndef __TYPDFS_H__
#define __TYPDFS_H__

#define MAX_NO_BYTES BRG_PORT_LIST_SIZE

#define   CONV_UINT4(x)   (*((UINT4 *)&(x)))

#define   BRG_SYSTEM_CONTROL base.u1SystemControl
#define   BRG_MODULE_STATUS  base.u1BridgeStatus
#define   BRG_FILT_MEM_POOLID filtid
#define   BRG_FDB_MEM_POOLID fwdPoolId
#define   BRG_MCAST_MEM_POOLID mcastid
#define   BRG_STATIC_MEM_POOLID staticTabid

#define   BRG_FDB_HASH_TBL  pSHtab 
#define   BRG_IS_ENABLED()    \
           ((BRG_SYSTEM_CONTROL == (UINT1)BRG_START) && \
            (BRG_MODULE_STATUS == (UINT1)BRG_ENABLED)) 

#define   BOOLEAN         tBOOLEAN
#define   BRG_EQUAL         0
#define   LESSER           -1
#define   GREATER           1

#define   BRG_GREATER      GREATER   
#define   BRG_LESSER       LESSER   

#define   IFACE_ID_SIZE     4
#define   PORT_ID_SIZE      2

#define BRIDGE_OK 0
#define BRIDGE_NOT_OK -1	

/*
 * The following is the length of the key formed for filtering 
 * database
 */
#define   KEY_LENGTH            14
#define   BM_IS_VALID_SERIAL_INTERFACE(ifno)  \
          (((ifno) > CRU_MAX_ENET_INTERFACES) \
          && ((ifno) <= (CRU_MAX_ENET_INTERFACES + CRU_MAX_WAN_INTERFACES)))

/*
 * This Macro Depends On The Host To Net Byte Ordering
 */
#define   BM_GET_PTR_TO_1ST_BYTE_OF_MACADDR(x)    (&((UINT1 *)&(x))[3])

/* To Check whether the destn. address is a reserved group address 
 * Reserved Group MAC Address : Higher 4 bytes should be 0x0180C200(MSB)   
 * And Lower 2 bytes should be less than or equal to 0x000F(LSB)   */

#define IS_RESERVED_GROUP_ADDR(addr) \
	(BRG_MEM_CMP(addr,brgrpAddr,5) == BRG_EQUAL ) \
	&& (addr[5] <= 0x0F)
   
#define IS_VALID_MULTICAST_ADDR(port,addr) \
        (BRG_MEM_CMP(addr,bcastAddr,ETHERNET_ADDR_SIZE) == BRG_EQUAL)\
        || (BdIsMcastAddr (                      \
        base.portEntry[(port)].u1MlistNo,(tMacAddr *)&(addr)) == TRUE)


#define BM_IS_MAC_MULTICAST(p) (p & 0x01)

#define BM_IS_BCAST_ADDRESS(addr)\
              (BRG_MEM_CMP(addr,bcastAddr,ETHERNET_ADDR_SIZE) == BRG_EQUAL)

#define BM_IS_ENET_INTERFACE(iface) \
        (BRG_BUF_GET_INTERFACE_TYPE(iface) == CRU_ENET_PHYS_INTERFACE_TYPE)

#define BM_GET_PHYS_INTERFACE_NO(iface)         \
        (BM_IS_ENET_INTERFACE (iface)            \
        ? (iface.u1_InterfaceNum + 1)          \
        : (iface.u1_InterfaceNum + CRU_MAX_ENET_INTERFACES + 1))

#define SN_CONVERT_IFNO_TO_PHYS_NO(type, Ifno)                    \
        ((type == CRU_ENET_PHYS_INTERFACE_TYPE) ?         \
        Ifno + 1:                                         \
        Ifno + (CRU_MAX_ENET_INTERFACES + 1))

/* Currently direct conversion to logical port is done */
#define CONV_TO_LOGICAL_PORT(type,Ifno)  Ifno

#define IS_VALID_PROTOCOL_FILTER_MASK(u4ProtoFiltMask)    \
        (u4ProtoFiltMask == 0x0000) ||                    \
        (u4ProtoFiltMask == 0x0004) ||                    \
        (u4ProtoFiltMask == 0x0008) ||                    \
        (u4ProtoFiltMask == 0x0010) ||                    \
        (u4ProtoFiltMask == 0x0020) ||                    \
        (u4ProtoFiltMask == 0x0040) ||                    \
        (u4ProtoFiltMask == 0x0080) ||                  \
        (u4ProtoFiltMask == 0x0100)                     \

/* For the IP Data traffic to be bridged */
#define BRG_IP_PROTOCOL   0x0800

#define BRG_PPPOE_DISC_PROTOCOL   0x8864
#define BRG_PPPOE_SESSION_PROTOCOL   0x8863


#define IS_BRGIF_OPERADMIN_UP(u4Port)  \
    (base.portEntry[u4Port].u1AdminStatus == UP) &&   \
       (base.portEntry[u4Port].u1OperStatus == UP) 

#define IS_BRGIF_ADMIN_UP(u4Port)  \
(base.portEntry[u4Port].u1AdminStatus == UP)   

#define IS_BRGIF_OPER_UP(u4Port)  \
    (base.portEntry[u4Port].u1AdminStatus != BRIDGE_INVALID) &&  \
       (base.portEntry[u4Port].u1OperStatus == UP) 

#define  BRG_INCR_PORT_FILTER_IN_DISCARDS(u4PortNo)    \
       tpInfo.portEntry[u4PortNo].u4NoFilterInDiscard++

#define  BRG_INCR_PORT_MCAST_DISCARDS(u4PortNo)    \
       tpInfo.portEntry[u4PortNo].u4NoFilterInDiscard++

#define IS_BRG_PROTO_FILTERED(u2protocol,u4PortNo)    \
       ((u2protocol) & (tpInfo.portEntry[u4PortNo].u2ProtocolFilterMask))

#define BRG_INCR_PORT_PROTOCOL_FILTER_DISCARDS(u4PortNo) \
               tpInfo.portEntry[u4PortNo].u4NoProtoInDiscard++

#ifdef  SYS_HC_WANTED
#define BRG_IN_FRAMES_OVERFLOW(u4PortNo)                              \
   ( (base.portEntry[u4PortNo].u4IfSpeed >  BRG_HIGH_SPEED) &&        \
     ((tpInfo.portEntry[u4PortNo].u4TpIn & 0xffffffff) ==             \
                                           0xffffffff) )


#define BRG_OUT_FRAMES_OVERFLOW(u4PortNo)                             \
   ( (base.portEntry[u4PortNo].u4IfSpeed >  BRG_HIGH_SPEED) &&        \
     ((tpInfo.portEntry[u4PortNo].u4TpOut & 0xffffffff) ==            \
                                           0xffffffff) )

#define BRG_FILTER_IN_DISCARD_OVERFLOW(u4PortNo)                      \
   ( (base.portEntry[u4PortNo].u4IfSpeed >  BRG_HIGH_SPEED) &&        \
     ((tpInfo.portEntry[u4PortNo].u4NoFilterInDiscard &               \
       0xffffffff) ==  0xffffffff) )

#define BRG_PROTO_IN_DISCARD_OVERFLOW(u4PortNo)                       \
   ( (base.portEntry[u4PortNo].u4IfSpeed >  BRG_HIGH_SPEED) &&        \
     ((tpInfo.portEntry[u4PortNo].u4NoProtoInDiscard &                \
       0xffffffff) ==  0xffffffff) )

#define BRG_INCR_HC_OR_TP_PORT_OUT_FRAMES(u4PortNo)                   \
   if (BRG_OUT_FRAMES_OVERFLOW(u4PortNo) == TRUE ) {                  \
      tpInfo.portEntry[u4PortNo].u4TpOutOverflow++;                   \
   }                                                                  \
   else {                                                             \
      tpInfo.portEntry[u4PortNo].u4TpOut++;                           \
   }                                                                 
                                                                     
#define BRG_INCR_HC_OR_TP_PORT_IN_FRAMES(u4PortNo)                    \
   if (BRG_IN_FRAMES_OVERFLOW(u4PortNo) == TRUE) {                    \
      tpInfo.portEntry[u4PortNo].u4TpInOverflow++;                    \
   }                                                                  \
   else {                                                             \
      tpInfo.portEntry[u4PortNo].u4TpIn++;                            \
   }

#define BRG_INCR_HC_OR_TP_FILTER_IN_DISCARDS(u4PortNo)                \
   if (BRG_FILTER_IN_DISCARD_OVERFLOW(u4PortNo) == TRUE ) {           \
      tpInfo.portEntry[u4PortNo].u4FilterInDiscardOverflow++;         \
   }                                                                  \
   else {                                                             \
      tpInfo.portEntry[u4PortNo].u4NoFilterInDiscard++;               \
   }                                                                  
                                                                      
#define BRG_INCR_HC_OR_TP_PROTOCOL_FILTER_DISCARDS(u4PortNo)          \
   if (BRG_PROTO_IN_DISCARD_OVERFLOW(u4PortNo) == TRUE ) {            \
      tpInfo.portEntry[u4PortNo].u4ProtoInDiscardOverflow++;          \
   }                                                                  \
   else {                                                             \
      tpInfo.portEntry[u4PortNo].u4NoProtoInDiscard++;                \
   }                                                                 
#endif /* SYS_HC_WANTED */
#define BRG_GET_NEXT_PORT_INDEX(u2PortIndex) \
                  base.portEntry[u2PortIndex].u2NextPortInd

#endif /* __TYPDFS_H__   */
