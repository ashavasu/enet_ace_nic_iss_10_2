/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpdfs.h,v 1.3 2011/11/03 07:16:59 siva Exp $
 *
 * Description: Contains datastructure for the tranparent bridge.
 *
 *******************************************************************/

#ifndef TYPDFS_H
#define TYPDFS_H

typedef struct TP_PORT_ENTRY
{
    UINT4  u4TpIn;                 /* dot1dTpPortInFrames */           
    UINT4  u4TpOut;                /* dot1dTpPortOutFrames */
    UINT2  u2Maxinfo;               /* dot1dTpPortMaxinfo */
    UINT2  u2ProtocolFilterMask;  /* dot1dTpPortProtocolFilterMask */
    UINT4  u4NoFilterInDiscard;
    UINT4  u4NoProtoInDiscard;
    UINT4  u4TpInOverflow;            /* dot1dTpPortInOverflowFrames */
    UINT4  u4TpOutOverflow;           /* dot1dTpPortOutOverflowFrames */
    UINT4  u4FilterInDiscardOverflow; /* dot1dTpPortInOverflowDiscards
                                       * sum of below two
                                       */
    UINT4  u4ProtoInDiscardOverflow;
} tTP_PORT_ENTRY;

typedef struct TP_INFO
{
    UINT4           u4NoLearnedEntryDiscard; /*dot1dTpLearnedEntryDiscards*/ 
    UINT4           u4FdbAgeingTime;          /* dot1dTpAgingTime */
    tTP_PORT_ENTRY  *portEntry;
} tTP_INFO;

typedef struct sBridgeSystemSize {
    UINT4  u4BridgeMaxFdbEntries;
    UINT4  u4BridgeMaxFilterEntries;
}tBridgeSystemSize;
#endif         /* ifndef TYPDFS.H  */
