/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brggroup.h,v 1.3 2007/02/01 14:44:35 iss Exp $
 *
 * Description:This file contains the data structure for the 
 *             group detail table and other group related datas.
 *
 *******************************************************************/
/*
 * This entry specifies each row in the GROUP DETAIL TABLE 
 * Index : memberId
 */
typedef struct bridge_group_entry {
    tTMO_INTERFACE  memberId;   /* 
                                  * interface id of the member
                                  */
   struct {
       UINT4  u4MtuExcDiscards;
       UINT4  u4ProtoInDiscard;
       UINT4  u4FilterInDiscard;
       UINT4  u4InDataFrames;
       UINT4  u4OutDataFrames;
       UINT4  u4NoBcastFramesTrxed;
       UINT4  u4NoMcastFramesTrxed;
       UINT2  u2Maxinfo;
       UINT2  u2ProtocolFilterMask;
       UINT1  u1AdminStatus;
       UINT1  u1OperStatus;
       UINT1  u1BcastStatus;
       UINT1  u1FilterNo;
       UINT1  u1McastNo;
       UINT1  au1Reserved[3];
   } memberData;
    UINT2  u2NextMember;       /* Protocol specific Info */
    UINT1  u1IftabIndex;       /* 
                                  * Specifies the index in  if-table
                                  * (i.e the group in which it belongs
                                  *  to).
                                  */
    UINT1 u1Reserved;

} tBRIDGE_GROUP_ENTRY;


typedef struct iftab_group_info {
   UINT2    u2IndexOfFirstMember;/*
                                      * This field contains the index of the 
                                      * first member ofd the group in the GDT.
                                      */
   UINT1    u1IsMcastVcExist;    /*
                                      * This entry specifies whether a multicast
                                      * address is available for the group.
                                      */
   UINT1    u1Reserved;
} tIFTAB_GROUP_INFO;


typedef struct BrgQMesg {
   UINT4       u4MesgType;
#ifdef MBSM_WANTED
   struct MbsmIndication {
       tMbsmProtoMsg *pMbsmProtoMsg;
   }MbsmCardUpdate;
#endif
} tBrgQMesg;
/*
typedef INT4 (*tSET_GEN_FUN )();
*/

#define   NO_MCAST_VC_EXIST       0
#define   MCAST_VC_EXIST          1
#define   NOT_GROUP               0

#define   MAX_GROUP_ENTRIES    1000

#define   NO_FREE_GDT_ENTRY   MAX_GROUP_ENTRIES 
#define   NULL_GDT_INDEX      0xffff            

#define   IF_IS_GROUP(port_no)                                               \
              (base.portEntry[port_no].grpInfo.u2IndexOfFirstMember  \
              != NOT_GROUP)


#define   IF_GET_FIRST_MEMBER_INDEX(port_no)                                 \
          (base.portEntry[port_no].grpInfo.u2IndexOfFirstMember) 

#define   GT_GET_LOGICAL_PORT_OF_THIS_MEMBER(mem_index)                      \
            (grp_tab[mem_index].u1IftabIndex)

#define   GT_FOR_ALL_MEMBERS_OF_GROUP(ind, iface)                            \
          for (iface = ((ind != NULL_GDT_INDEX) ? (grp_tab[ind].memberId) : (iface));ind != NULL_GDT_INDEX;          \
   (((ind = grp_tab[ind].u2NextMember) != NULL_GDT_INDEX) ? (iface = grp_tab[ind].memberId) : iface))

#define   GT_IS_THIS_IFACE(iface, mem_index)                 \
          ((grp_tab[mem_index].memberData.u1AdminStatus != BRIDGE_INVALID)  && \
          (grp_tab[mem_index].memberId.u1_InterfaceType       \
          == (iface).u1_InterfaceType)  && \
          (grp_tab[mem_index].memberId.u1_InterfaceNum        \
          == (iface).u1_InterfaceNum) &&               \
          (grp_tab[mem_index].memberId.u2_SubReferenceNum     \
          == (iface).u2_SubReferenceNum))                             

#define   GT_GET_INDIVIDUAL_CONN_IFACE(ind)                  \
          (grp_tab[ind].memberId)

#define   GT_IS_STATUS_OK(ind)     \
          (grp_tab[ind].memberData.u1AdminStatus == UP) \
          && (grp_tab[ind].memberData.u1OperStatus == UP)

#define   GT_IS_THIS_PROTOCOL_ALLOWED(protocol, mem_index)  \
          (!(grp_tab[mem_index].memberData.u2ProtocolFilterMask & protocol))

#define   GT_IS_NOT_FILTERED(destAddr ,srcAddr, mem_index)               \
          (BdIsfiltered ( grp_tab[mem_index].memberData.u1FilterNo,\
          &destAddr, &srcAddr) != TRUE)

#define   GT_IS_VALID_MCAST_ADDR(destAddr, mem_index)                     \
        ((BrgMacAddrMemCmp(destAddr,bcastAddr,ETHERNET_ADDR_SIZE) == BRG_EQUAL)\
          || (BdIsMcastAddr (                                    \
          grp_tab[mem_index].memberData.u1McastNo, &destAddr) == TRUE))

#define   GT_IS_SIZE_OK(size, mem_index)                                   \
          (grp_tab[mem_index].memberData.u2Maxinfo >= size)

#define   GT_IS_BCAST_STATUS_OK(mem_index)                                 \
          (grp_tab[mem_index].memberData.u1BcastStatus == UP)

#define   GT_INCR_MTU_DISCARD(mem_index)                                   \
          (grp_tab[mem_index].memberData.u4MtuExcDiscards++)

#define   GT_INCR_PROTO_IN_DISCARD(mem_index)                           \
          (grp_tab[mem_index].memberData.u4ProtoInDiscard++)

#define   GT_INCR_FILTER_IN_DISCARD(mem_index)                            \
          (grp_tab[mem_index].memberData.u4FilterInDiscard++)

#define   GT_RECD_DATA_ON_THIS_CONN(ind)                                   \
          (grp_tab[ind].memberData.u4InDataFrames++)

#define   GT_SEND_DATA_ON_THIS_CONN(ind)                                   \
          (grp_tab[ind].memberData.u4OutDataFrames++)

#define   IF_ASSIGN_FIRST_MEM_INDEX(port,ind)                              \
          (base.portEntry[port].grpInfo.u4_first_member_index = ind)

#define   GT_IS_FREE_GDT_ENTRY(ind)                                        \
          (grp_tab[ind].memberData.u1AdminStatus == BRIDGE_INVALID)

#define   GT_GET_FREE_GDT_INDEX(ind)                                       \
          for (ind = 1;                                         \
          ind < MAX_GROUP_ENTRIES && !GT_IS_FREE_GDT_ENTRY(ind); ind ++);

/*
 * This Macros operate on the FDBID
 */
#define   GET_LOGICAL_PORT_FROM_FDBID(fdbid)   (fdbid / 0x1000000)             
#define   GET_MEM_INDEX_FROM_FDBID(fdbid)      (fdbid & 0x0000ffff)            
#define   GET_TYPE_FROM_FDBID(fdbid)           ((fdbid & 0x00ff0000)/ 0x10000) 

#define   CODE_LOGICAL_PORT_IN_FDBID(fdbid, port)                            \
          (fdbid = ((fdbid & 0x00ffffff) | (port * 0x1000000)));

#define   CODE_MEM_INDEX_IN_FDBID(fdbid, index)                              \
          (fdbid = ((fdbid & 0xffff0000) | index))

#define   CODE_TYPE_IN_FDBID(fdbid, code)                                    \
          (fdbid = (fdbid & 0xff00ffff) | code)

#define   CODE_IF_NO_IN_FDBID(fdbid, ifno)                                   \
          (fdbid = (fdbid & 0xff00ffff) | (ifno * 0x10000))

#define   SINGLE_MEMBER_FDB   0x00fe0000 
#define   ALL_MEMBER_FDB      0x00ff0000 
#define   FLAT_ENTRY_FDB      0x00fd0000 

#define   IS_FDBID_OF_THIS_TYPE(fdbid, type)                               \
          (((fdbid) & 0x00ff0000) == type)

#define   IS_UNASSIGNED_FDB(fdbid)  \
          ((((fdbid) & 0x00ff0000) != SINGLE_MEMBER_FDB) \
          && (((fdbid) & 0x00ff0000) != ALL_MEMBER_FDB)  \
          && (((fdbid) & 0x00ff0000) != FLAT_ENTRY_FDB))

#define   IF_INIT_GRP_INFO(ind, status)                  \
          base.portEntry[ind].grpInfo.u2IndexOfFirstMember = status;

#define   GIF_INCR_MTU_EXCEED_DISCARD(p_msg_attr)                    \
  base.portEntry[p_msg_attr->u2LogicalPortNo].u4NoMtuExceedDiscard++; \
  if (p_msg_attr->bMember == TRUE) {                            \
      grp_tab[p_msg_attr->u2Index].memberData.u4MtuExcDiscards++;        \
  }

#define GIF_INCR_PROTO_IN_DISCARD(p_msg_attr)                       \
  tpInfo.portEntry[p_msg_attr->u2LogicalPortNo].u4NoProtoInDiscard++; \
  if (p_msg_attr->bMember == TRUE) {                              \
      grp_tab[p_msg_attr->u2Index].memberData.u4ProtoInDiscard++;     \
  }

#define GIF_INCR_FILTER_IN_DISCARD(p_msg_attr)                        \
 tpInfo.portEntry[p_msg_attr->u2LogicalPortNo].u4NoFilterInDiscard++;\
 if (p_msg_attr->bMember == TRUE) {                              \
    grp_tab[p_msg_attr->u2Index].memberData.u4FilterInDiscard++;  \
 }

#define   GT_INCR_NO_BCAST_FRAMES_TRXED(index)                   \
          grp_tab[index].memberData.u4NoBcastFramesTrxed++;           

#define   GT_INCR_NO_MCAST_FRAMES_TRXED(index)                   \
          grp_tab[index].memberData.u4NoMcastFramesTrxed++;

#define   IF_INCR_NO_BCAST_FRAMES_TRXED(index)                   \
          base.portEntry[index].u4NoBcastFramesTrxed++;

#define   IF_INCR_NO_MCAST_FRAMES_TRXED(index)                    \
          base.portEntry[index].u4NoMcastFramesTrxed++;
