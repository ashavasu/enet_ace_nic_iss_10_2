/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: basedfs.h,v 1.3 2007/02/01 14:44:35 iss Exp $
 *
 * Description:This file contains datastructures for the
 *             basic bridging part.
 *
 *******************************************************************/
#ifndef _BASEDFS_H
#define _BASEDFS_H



typedef struct BM_BASE_PORT_ENTRY 
{
   tTMO_INTERFACE     portId;            /*
                                          * Comprises of dot1dBasePortIfIndex 
                                          * and dot1dBasePortCircuit.
                                          */
    tIFTAB_GROUP_INFO  grpInfo;
    UINT4              u4NoMtuExceedDiscard;  
    UINT4              u4NoBcastFramesTrxed;
    UINT4              u4NoMcastFramesTrxed;
    UINT4              u4NoTransitDelayDiscard;
                                       /* dot1dBasePortDelayExceededDiscards */
#ifdef SYS_HC_WANTED
    UINT4              u4IfSpeed;      /* Stores IfSpeed of interface
                                        * Added for HC_PORT.
                                        */
#endif
    UINT2              u2NextPortInd;
    UINT1              u1AdminStatus;           /* dot1dBasePortAdminStatus */
    UINT1              u1OperStatus;            /* dot1dBasePortOperStatus */ 
    UINT1              u1FilterNo;              /* dot1dFilterAssignNumber */
    UINT1              u1MlistNo;
    UINT1              u1BroadcastStatus;       /* dot1dBasePortBcastStatus */
    UINT1              u1Reserved;         /* Byte Alignment */
} tBM_BASE_PORT_ENTRY;

 
typedef struct BM_BASE
{
    tBM_BASE_PORT_ENTRY  *portEntry;             /* dot1dBasePortTable     */
    UINT2        u2NoOfPorts;   /* dot1dBaseNumPorts */
    tMacAddr     brgMacAddr;    /* dot1dBaseBridgeAddress */
    UINT1        u1BridgeStatus; /* dot1dBaseBridgeStatus */
    UINT1        u1CrcStatus;
    UINT1        u1SystemControl; /* dot1dBridgeSystemControl */
    UINT1        au1Reserv[1];  /* For Byte alignmnet */ 
} tBM_BASE;

#endif/*  _BASEDFS_H */
