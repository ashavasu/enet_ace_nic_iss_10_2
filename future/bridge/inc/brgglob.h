/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brgglob.h,v 1.3 2007/02/01 14:44:35 iss Exp $
 *
 * Description: Contains global definitions.
 *
 *******************************************************************/
#ifndef _BRGGLOB_H
#define _BRGGLOB_H

/* Transparent Bridging related variables */

tMacAddr gBrgGmrpAddr = {0x01, 0x80, 0xC2, 0x00, 0x00, 0x20}; 
tMacAddr gBrgGvrpAddr = {0x01, 0x80, 0xC2, 0x00, 0x00, 0x21}; 
tMacAddr gBrgStapMacAddr = {0x01, 0x80, 0xC2, 0x00, 0x00, 0x00};

tBM_BASE                     base;
tBrgGlb                      gBrgGlb;
tBrgTaskId                   gBrgTaskId;
tTP_INFO                     tpInfo; 
tTP_PORT_ENTRY               tpPortEntry[BRG_MAX_PHY_PLUS_LOG_PORTS+2] ;
tBM_BASE_PORT_ENTRY          bmBasePortEntry[BRG_MAX_PHY_PLUS_LOG_PORTS+2];
tTMO_HASH_TABLE             *pSHtab;
tFORWARDING_ENTRY           *bdFdbEntries;
tBrgMemPoolId                filtid;
tBrgMemPoolId                fwdPoolId;
tFILTER_LOOKUPTABLE_ENTRY    bdFilter[MAX_FILTER_NUMBER];
tBrgMemPoolId                mcastid;
tMULTICAST_LIST              bdMlist[MAX_MULTILIST_NUMBER];
tBrgMemPoolId                staticTabid;
tSTATICTABLE_LIST            bdStaticTable[BRG_MAX_PHY_PLUS_LOG_PORTS+1];
tMacAddr              bcastAddr;       
/* Timer related variables */

/*Queue related Variable*/
tBrgQId                      gBrgQId; 
tBrgMemPoolId                gBrgPoolId; 
tbrglist                     tlistid;
UINT4                        bridgeTimer;
UINT4 MAX_FDB_ENTRIES;/* Maximum FDB entries */
UINT4 MAX_FILTER_ENTRIES;/* Maximum Filter Entries */
UINT4 u4MaxFdbEntryToHndl;/* Used for handling the FDB aging */
UINT1                        brgrpAddr[ETHERNET_ADDR_SIZE];
/*---------Global Declarations--------*/
UINT2 gu2BrgStartPortInd = BRG_INVALID_PORT_INDEX;
UINT2 gu2BrgLastPortInd  = BRG_INVALID_PORT_INDEX;
#endif
