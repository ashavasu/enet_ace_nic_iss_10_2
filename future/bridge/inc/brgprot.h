/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brgprot.h,v 1.6 2007/02/01 14:44:35 iss Exp $
 *
 * Description: All prototypes for functions done here
 *
 *******************************************************************/
#ifndef _BRGPROTOS_H
#define _BRGPROTOS_H

/* General TRANSPARENT BRIDGING  related functions */

UINT1 IsBridgeEnabled(UINT2 );  
VOID  TpProcessDataFrame  (tMSG_ATTR *,tCRU_BUF_CHAIN_HEADER  *,UINT4 );
VOID  TpBroadcastFrame  (tCRU_BUF_CHAIN_HEADER *,tMSG_ATTR *,tBOOLEAN );
tBOOLEAN TpIsPortOkForForwarding  (UINT4  ,tMSG_ATTR  *,tBOOLEAN   );
UINT2   GenerateProtoFilterMask (UINT2 );

#ifdef IGS_WANTED
VOID TpSendMcastFrameOverSpecificPorts(tCRU_BUF_CHAIN_HEADER *,
                                       tMSG_ATTR *,
                                       tPortList );
#endif /* IGS_WANTED */

/* General operations on data */

INT4  BuCompareMacaddr(tMacAddr, tMacAddr);

UINT4 BuGetNoBitsSet (UINT1 *, UINT4 );
INT4  BuCompareIface  (tTMO_INTERFACE   *,tTMO_INTERFACE   *);

UINT2  BuGetProtocolType  (UINT2  )  ;
INT4  BuPrepareFloodBuffer(tCRU_BUF_CHAIN_HEADER *,tTMO_INTERFACE ,tBOOLEAN);
INT1  BrgCheckFilterEntry(tMacAddr ,tMacAddr ,tMacAddr );

VOID StInitBaseStap (VOID);



/* DYNAMIC FORWARDING related functions */

INT4   BdFdbMemoryInit (VOID );
INT4   BdFdbInit ( VOID  );
INT1   BdDbInit ( VOID  );

tFORWARDING_ENTRY *BdGetNewFdbEntry (VOID);
INT4  BdFreeFdbEntry  (tFORWARDING_ENTRY *);
tBOOLEAN      BdIsFdbEntryFree ( VOID  );
UINT4        HashFn (tMacAddr *);
UINT4 InsertFn  (tTMO_HASH_NODE *,UINT1       *);
 VOID  DeleteNodeFn  (tTMO_HASH_NODE  *);
tFORWARDING_ENTRY *HashSearchNode  (tTMO_HASH_TABLE *,tMacAddr *);
INT4   BdLearnFdbEntry(tMacAddr *, UINT4, UINT1);
UINT4  BdGetPortFromDest  (tMacAddr *);
VOID  BdRemoveFdbEntry ( UINT4 );
VOID  BdAssignAllUnassignedFdb  (tTMO_INTERFACE *)  ;
INT4 BdCodeFdbid (tTMO_INTERFACE *,UINT4 *);
UINT4   TpGetPortIfCanReceiveMsg  (tTMO_INTERFACE  *,UINT4 *,tMSG_ATTR *);

INT4
BridgeIngressFilter (tCRU_BUF_CHAIN_HEADER * pMsg,
                     UINT2 u2ifIndex, UINT2 u2Protocol,
                     UINT4 *pu4FdbId, tMSG_ATTR  *pMsgAttr, UINT1 u1FrameType,
                     INT4  i4IsReservAddr);
/* INTERFACES related functions */

INT4 BridgeFwdReceiveCircuitOperStatus (tTMO_INTERFACE, UINT1);
UINT4 GetPortFromIfaceId  (tTMO_INTERFACE  *);
INT4 SnIsIfaceIdOk  (tTMO_INTERFACE  *)  ;
INT4 SnCanCreateThisIf  (tTMO_INTERFACE  *)   ;
INT4 InAllocMemoryForTables ( VOID  )    ;
INT4 InEnablePort (tTMO_INTERFACE );
INT4 InInvalidPort(tTMO_INTERFACE );
INT4 InMakeAllInterfacesDown ( VOID );
VOID  InResetBasicVars (UINT4 );
INT4 InSetLogicalPortOperStatus(tTMO_INTERFACE, UINT1 );
VOID  InInitTpPort (UINT4 );
VOID  InInitTp (VOID );
VOID  InInitMaxinfo(tTMO_INTERFACE);
INT4 BdDeleteStaticEntries  (tTMO_INTERFACE   *);

/* MULTICAST TABLE based filtering functions */

INT4 BdMlistInit ( VOID  )  ;
tBOOLEAN BdIsMcastAddr  (UINT1  ,tMacAddr  *) ;
VOID  BdSetMlistNo  (UINT1   ,UINT1   )   ;


/* SOURCE DESTINATION pair based filtering functions */

INT4    BdFilterInit ( VOID  );
tBOOLEAN  BdIsfiltered  (UINT1  ,tMacAddr *,tMacAddr *);
INT4 BdAddFilterEntry(UINT1, tMacAddr , tMacAddr  , tMacAddr , tMacAddr , UINT1);
INT4 BdDeleteFilterEntry (UINT1, tMacAddr  ,tMacAddr  );
tFILTERING_ENTRY  *BdGetFilterEntryFromIndex  (UINT1 ,tMacAddr ,tMacAddr );
VOID SnMakeKey( UINT1 * ,UINT1 ,tMacAddr ,tMacAddr );

/* STATIC TABLE related functions */

INT4 BdStaticTableInit  ( VOID  );
VOID  BdFilterFrameBasedOnStaticTable(tSTATICTABLE_ENTRY*,tCRU_BUF_CHAIN_HEADER*,tMSG_ATTR *);
INT4 BuCompareStaticaddrAndReceiveport(tMacAddr ,INT4,tMacAddr ,INT4);
tSTATICTABLE_ENTRY *BdIsMacaddrPresentOnStaticTable(tMacAddr,UINT4);
tSTATICTABLE_ENTRY *BrgFindStaticEntryInTbl(tMacAddr ,UINT4 );
INT4 StaticCompareMacaddrAndPort (tMacAddr,INT4,tMacAddr,INT4);
INT4 BdAddStaticEntry(UINT1 , tMacAddr , UINT1 pu1AllowedStaticPorts[], UINT1 );

tSTATICTABLE_ENTRY *BdIsMacAddrPresentOnCommonStaticTable (tMacAddr );
INT1 BdRemoveStaticEntryFromList (INT4 ,tSTATICTABLE_ENTRY *);
INT1 BdRemoveStatictableEntry( tSTATICTABLE_ENTRY *);
VOID  BdDeleteStaticEntry (UINT4 ) ;



/* SPANNING TREE PROTOCOL MODULE related functions */
VOID BrgTmrTask (INT1 *);

/* TIMER related functions */

VOID  TmStartTimer  (tTIMER *);
VOID  TmStopTimer  (tTIMER *);
VOID  TmFdbTimerTick ( VOID  )  ;
VOID  TmStatictableTimerTick ( VOID  ) ;

/***** brgif.c prototypes *********/
VOID BridgeFillInInfo(tMSG_ATTR  *, tBrgIf   *);

UINT4 BrgGetAgeoutTime (VOID);

/*     START and SHUT */
INT1 BrgDbDeInit (VOID);
INT1 BrgFdbDeInit (VOID);
INT1 BrgFilterDeInit(VOID);
INT1 BrgMlistDeInit(VOID);
INT1 BrgStaticDeInit (VOID);
INT1 BridgeModuleInit (VOID);
INT1 BridgeModuleShutdown (VOID);
VOID BridgeModuleRestart (VOID);
VOID BrgResetAllScalarValues(VOID);
INT4 BridgeTmrTaskInit (VOID);
INT4 BrgGetCfaPortOperStatus(UINT2 u2PortNo, tCfaIfInfo  *pIfInfo);
UINT4 BrgTmrExpiryHandler (VOID);
INT4 BrgGetLLPortOperStatus (UINT2 u2PortNo, UINT1* pu1PortStatus);
#ifdef MBSM_WANTED
INT4 BridgeMbsmUpdateCardInsertion ( tMbsmPortInfo *, tMbsmSlotInfo *);
#endif
#endif /* ifndef _BRGPROTOS_H */
