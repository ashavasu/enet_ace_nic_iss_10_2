/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tim.h,v 1.2 2007/02/01 14:44:35 iss Exp $
 *
 * Description: Timer related definitions and data structures
 *
 *******************************************************************/
#ifndef __TIM_H__
#define __TIM_H__

typedef tTimerListId    tbrglist;
typedef tTmrAppTimer    tbrgtmr;
typedef struct BRGTIMER
{
    tBOOLEAN  bActive;      /* Indicate whether the timer is active or not */
    UINT4     u4TickCount; /* Indicate number of ticks expired only if 
                              * bActive is TRUE.
                              */
} tTIMER;

#define   IS_TIMER_EXPIRED(timer,incr,limit)   \
          (((((timer).bActive == TRUE) && \
          ((timer).u4TickCount += (incr)) >= (limit))) ? 1 :0)
#endif /* __TIM_H__ */
