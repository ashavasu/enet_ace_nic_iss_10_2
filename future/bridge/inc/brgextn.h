/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brgextn.h,v 1.7 2007/02/01 14:44:35 iss Exp $
 *
 * Description: Contains extern definitions.
 *
 *******************************************************************/
#ifndef _BRGEXTERN_H
#define _BRGEXTERN_H

/* *********** Extern Variables ************************* */
/* Transparent Bridging related variables */

/* MAC_ADDR_CHANGE - Start */
extern tMacAddr gBrgGmrpAddr; 
extern tMacAddr gBrgGvrpAddr; 
extern tMacAddr gBrgStapMacAddr;
/* MAC_ADDR_CHANGE - End */

extern tBM_BASE                     base;
extern tBrgGlb                      gBrgGlb;
extern tTP_INFO                     tpInfo; 
extern tTP_PORT_ENTRY               tpPortEntry[BRG_MAX_PHY_PLUS_LOG_PORTS+2] ;
extern tBM_BASE_PORT_ENTRY          bmBasePortEntry[BRG_MAX_PHY_PLUS_LOG_PORTS+2];
extern tTMO_HASH_TABLE             *pSHtab;
extern tFORWARDING_ENTRY           *bdFdbEntries;
extern tBrgMemPoolId                filtid;
extern tBrgMemPoolId                fwdPoolId;
extern tFILTER_LOOKUPTABLE_ENTRY    bdFilter[MAX_FILTER_NUMBER];
extern tBrgMemPoolId                mcastid;
extern tMULTICAST_LIST              bdMlist[MAX_MULTILIST_NUMBER];
extern tBrgMemPoolId                staticTabid;
extern tSTATICTABLE_LIST            bdStaticTable[BRG_MAX_PHY_PLUS_LOG_PORTS+1];
extern UINT4 MAX_FDB_ENTRIES;/* Maximum FDB entries */
extern UINT4 MAX_FILTER_ENTRIES;/* Maximum Filter Entries */
extern tMacAddr              bcastAddr;       


/* Spanning Tree Protocol related variables */
extern UINT1                        brgrpAddr[ETHERNET_ADDR_SIZE];
/* Timer related variables */
extern tbrglist                     tlistid;
extern UINT4                        bridgeTimer;

/*Queue related Variable*/
extern tBrgQId                      gBrgQId; 
extern tBrgMemPoolId                gBrgPoolId; 

extern UINT2 gu2BrgStartPortInd;
extern UINT2 gu2BrgLastPortInd;

/* ******************** END of Variable ******************************** */

/* ******************** External Functions **************** */
/* Used By CFA Module */

extern INT4 BridgeInitializeProtocol( VOID );
extern UINT1 GetBridgeStatus( VOID ) ;
extern INT4 BridgeDeletePort (UINT2 ) ;
extern INT4 BridgeCreatePort (UINT2 , UINT2 , UINT4)  ;

/* General TRANSPARENT BRIDGING  related functions */

extern UINT1 IsBridgeEnabled(UINT2 );  
extern VOID TpProcessDataFrame  (tMSG_ATTR *,tCRU_BUF_CHAIN_HEADER *,UINT4 );
extern VOID TpBroadcastFrame  (tCRU_BUF_CHAIN_HEADER *,tMSG_ATTR *,tBOOLEAN );
extern tBOOLEAN TpIsPortOkForForwarding  (UINT4  ,tMSG_ATTR  *,tBOOLEAN );
extern UINT2        GenerateProtoFilterMask (UINT2 );

/* General operations on data */

extern INT4  BuCompareMacaddr(tMacAddr, tMacAddr);
extern UINT4 BuGetNoBitsSet (UINT1 *, UINT4 );
extern INT4  BuCompareIface  (tTMO_INTERFACE   *,tTMO_INTERFACE   *);
extern UINT2  BuGetProtocolType  (UINT2  )  ;
extern INT4  BuPrepareFloodBuffer (tCRU_BUF_CHAIN_HEADER *,tTMO_INTERFACE,tBOOLEAN);
extern INT1  BrgCheckFilterEntry(tMacAddr ,tMacAddr ,tMacAddr );


/* DYNAMIC FORWARDING related functions */

extern INT4   BdFdbMemoryInit (VOID );
extern INT4   BdFdbInit ( VOID  );
extern INT1   BdDbInit ( VOID  );


extern tFORWARDING_ENTRY *BdGetNewFdbEntry (VOID);
extern INT4  BdFreeFdbEntry  (tFORWARDING_ENTRY *);
extern tBOOLEAN  BdIsFdbEntryFree ( VOID  );
extern UINT4    HashFn (tMacAddr *);
extern UINT4 InsertFn  (tTMO_HASH_NODE *,UINT1       *);
extern  VOID  DeleteNodeFn  (tTMO_HASH_NODE  *);
extern tFORWARDING_ENTRY *HashSearchNode  (tTMO_HASH_TABLE *,tMacAddr *);
extern INT4   BdLearnFdbEntry(tMacAddr *, UINT4, UINT1);
extern UINT4  BdGetPortFromDest  (tMacAddr *);
extern VOID  BdRemoveFdbEntry ( UINT4 );
extern VOID  BdAssignAllUnassignedFdb  (tTMO_INTERFACE *)  ;
extern INT4 BdCodeFdbid (tTMO_INTERFACE *,UINT4 *);
extern UINT4   TpGetPortIfCanReceiveMsg  (tTMO_INTERFACE  *,UINT4 *,tMSG_ATTR *);


/* INTERFACES related functions */

extern INT4 BridgeFwdReceiveCircuitOperStatus (tTMO_INTERFACE, UINT1);
extern UINT4 GetPortFromIfaceId  (tTMO_INTERFACE  *);
extern INT4 SnIsIfaceIdOk  (tTMO_INTERFACE  *)  ;
extern INT4 SnCanCreateThisIf  (tTMO_INTERFACE  *)   ;
extern INT4 InAllocMemoryForTables ( VOID  )    ;
extern INT4 InEnablePort (tTMO_INTERFACE );
extern INT4 InInvalidPort(tTMO_INTERFACE );
extern INT4 InMakeAllInterfacesDown ( VOID );
extern VOID  InResetBasicVars (UINT4 );
extern INT4 InSetLogicalPortOperStatus(tTMO_INTERFACE, UINT1 );
extern VOID  InInitTpPort (UINT4 );
extern VOID  InInitTp (VOID );
extern VOID  InInitMaxinfo(tTMO_INTERFACE);
extern INT4 BdDeleteStaticEntries  (tTMO_INTERFACE   *);
extern VOID BrgInsertInIfList(UINT2);
extern VOID BrgDeleteFromIfList(UINT2);

/* MULTICAST TABLE based filtering functions */

extern INT4 BdMlistInit ( VOID  )  ;
extern tBOOLEAN BdIsMcastAddr  (UINT1  ,tMacAddr  *) ;
extern VOID  BdSetMlistNo  (UINT1   ,UINT1   )   ;

/* SOURCE DESTINATION pair based filtering functions */

extern INT4    BdFilterInit ( VOID  );
extern tBOOLEAN  BdIsfiltered  (UINT1  ,tMacAddr *,tMacAddr *);
extern INT4 BdAddFilterEntry(UINT1, tMacAddr , tMacAddr  , tMacAddr , tMacAddr , UINT1);
extern INT4 BdDeleteFilterEntry (UINT1, tMacAddr ,tMacAddr  );
tFILTERING_ENTRY  *BdGetFilterEntryFromIndex  (UINT1 ,tMacAddr ,tMacAddr );
extern VOID SnMakeKey( UINT1 * ,UINT1 ,tMacAddr  ,tMacAddr );

/* STATIC TABLE related functions */

extern INT4 BdStaticTableInit  ( VOID  );
extern VOID  BdFilterFrameBasedOnStaticTable(tSTATICTABLE_ENTRY*,tCRU_BUF_CHAIN_HEADER*,tMSG_ATTR *);
extern INT4 BuCompareStaticaddrAndReceiveport(tMacAddr ,INT4,tMacAddr ,INT4);
extern tSTATICTABLE_ENTRY *BdIsMacaddrPresentOnStaticTable(tMacAddr,UINT4);
extern INT4 StaticCompareMacaddrAndPort (tMacAddr,INT4,tMacAddr,INT4);
extern INT4 BdAddStaticEntry(UINT1 , tMacAddr , UINT1 pu1AllowedStaticPorts[], UINT1 );

extern tSTATICTABLE_ENTRY *BdIsMacAddrPresentOnCommonStaticTable (tMacAddr );
extern INT1 BdRemoveStaticEntryFromList (INT4 ,tSTATICTABLE_ENTRY *);
extern INT1 BdRemoveStatictableEntry( tSTATICTABLE_ENTRY *);
extern VOID  BdDeleteStaticEntry (UINT4 ) ;


/* TIMER related functions */

extern VOID  TmStartTimer  (tTIMER *);
extern VOID  TmStopTimer  (tTIMER *);
extern VOID  TmFdbTimerTick ( VOID  )  ;
extern VOID  TmStatictableTimerTick ( VOID  ) ;

extern tSNMP_VAR_BIND *
SNMP_AGT_FormVarBind ARG_LIST((tSNMP_OID_TYPE * , INT2 , UINT4 , INT4,
                               tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,
                               tSNMP_COUNTER64_TYPE));

extern tSNMP_OCTET_STRING_TYPE *SNMP_AGT_FormOctetString
ARG_LIST ((UINT1 *string, INT4 length));

extern tSNMP_OID_TYPE     *SNMP_AGT_GetOidFromString PROTO ((INT1 *pi1_str));

/* Snmp Registration functions */
#ifdef MSTP_WANTED
extern VOID RegisterFSMST(VOID);
#endif

#if defined(IGS_WANTED) || defined(MLDS_WANTED)
extern VOID RegisterFSSNP(VOID);
#endif

#endif /* #ifndef _BRGEXTERN_H */
