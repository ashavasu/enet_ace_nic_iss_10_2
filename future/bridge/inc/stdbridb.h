/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdbridb.h,v 1.4 2008/08/20 13:28:01 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDBRIDB_H
#define _STDBRIDB_H

UINT1 Dot1dBasePortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dStpPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dTpFdbTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot1dTpPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dStaticTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdbri [] ={1,3,6,1,2,1,17};
tSNMP_OID_TYPE stdbriOID = {7, stdbri};


UINT4 Dot1dBaseBridgeAddress [ ] ={1,3,6,1,2,1,17,1,1};
UINT4 Dot1dBaseNumPorts [ ] ={1,3,6,1,2,1,17,1,2};
UINT4 Dot1dBaseType [ ] ={1,3,6,1,2,1,17,1,3};
UINT4 Dot1dBasePort [ ] ={1,3,6,1,2,1,17,1,4,1,1};
UINT4 Dot1dBasePortIfIndex [ ] ={1,3,6,1,2,1,17,1,4,1,2};
UINT4 Dot1dBasePortCircuit [ ] ={1,3,6,1,2,1,17,1,4,1,3};
UINT4 Dot1dBasePortDelayExceededDiscards [ ] ={1,3,6,1,2,1,17,1,4,1,4};
UINT4 Dot1dBasePortMtuExceededDiscards [ ] ={1,3,6,1,2,1,17,1,4,1,5};
UINT4 Dot1dStpProtocolSpecification [ ] ={1,3,6,1,2,1,17,2,1};
UINT4 Dot1dStpPriority [ ] ={1,3,6,1,2,1,17,2,2};
UINT4 Dot1dStpTimeSinceTopologyChange [ ] ={1,3,6,1,2,1,17,2,3};
UINT4 Dot1dStpTopChanges [ ] ={1,3,6,1,2,1,17,2,4};
UINT4 Dot1dStpDesignatedRoot [ ] ={1,3,6,1,2,1,17,2,5};
UINT4 Dot1dStpRootCost [ ] ={1,3,6,1,2,1,17,2,6};
UINT4 Dot1dStpRootPort [ ] ={1,3,6,1,2,1,17,2,7};
UINT4 Dot1dStpMaxAge [ ] ={1,3,6,1,2,1,17,2,8};
UINT4 Dot1dStpHelloTime [ ] ={1,3,6,1,2,1,17,2,9};
UINT4 Dot1dStpHoldTime [ ] ={1,3,6,1,2,1,17,2,10};
UINT4 Dot1dStpForwardDelay [ ] ={1,3,6,1,2,1,17,2,11};
UINT4 Dot1dStpBridgeMaxAge [ ] ={1,3,6,1,2,1,17,2,12};
UINT4 Dot1dStpBridgeHelloTime [ ] ={1,3,6,1,2,1,17,2,13};
UINT4 Dot1dStpBridgeForwardDelay [ ] ={1,3,6,1,2,1,17,2,14};
UINT4 Dot1dStpPort [ ] ={1,3,6,1,2,1,17,2,15,1,1};
UINT4 Dot1dStpPortPriority [ ] ={1,3,6,1,2,1,17,2,15,1,2};
UINT4 Dot1dStpPortState [ ] ={1,3,6,1,2,1,17,2,15,1,3};
UINT4 Dot1dStpPortEnable [ ] ={1,3,6,1,2,1,17,2,15,1,4};
UINT4 Dot1dStpPortPathCost [ ] ={1,3,6,1,2,1,17,2,15,1,5};
UINT4 Dot1dStpPortDesignatedRoot [ ] ={1,3,6,1,2,1,17,2,15,1,6};
UINT4 Dot1dStpPortDesignatedCost [ ] ={1,3,6,1,2,1,17,2,15,1,7};
UINT4 Dot1dStpPortDesignatedBridge [ ] ={1,3,6,1,2,1,17,2,15,1,8};
UINT4 Dot1dStpPortDesignatedPort [ ] ={1,3,6,1,2,1,17,2,15,1,9};
UINT4 Dot1dStpPortForwardTransitions [ ] ={1,3,6,1,2,1,17,2,15,1,10};
UINT4 Dot1dStpPortPathCost32 [ ] ={1,3,6,1,2,1,17,2,15,1,11};
UINT4 Dot1dTpLearnedEntryDiscards [ ] ={1,3,6,1,2,1,17,4,1};
UINT4 Dot1dTpAgingTime [ ] ={1,3,6,1,2,1,17,4,2};
UINT4 Dot1dTpFdbAddress [ ] ={1,3,6,1,2,1,17,4,3,1,1};
UINT4 Dot1dTpFdbPort [ ] ={1,3,6,1,2,1,17,4,3,1,2};
UINT4 Dot1dTpFdbStatus [ ] ={1,3,6,1,2,1,17,4,3,1,3};
UINT4 Dot1dTpPort [ ] ={1,3,6,1,2,1,17,4,4,1,1};
UINT4 Dot1dTpPortMaxInfo [ ] ={1,3,6,1,2,1,17,4,4,1,2};
UINT4 Dot1dTpPortInFrames [ ] ={1,3,6,1,2,1,17,4,4,1,3};
UINT4 Dot1dTpPortOutFrames [ ] ={1,3,6,1,2,1,17,4,4,1,4};
UINT4 Dot1dTpPortInDiscards [ ] ={1,3,6,1,2,1,17,4,4,1,5};
UINT4 Dot1dStaticAddress [ ] ={1,3,6,1,2,1,17,5,1,1,1};
UINT4 Dot1dStaticReceivePort [ ] ={1,3,6,1,2,1,17,5,1,1,2};
UINT4 Dot1dStaticAllowedToGoTo [ ] ={1,3,6,1,2,1,17,5,1,1,3};
UINT4 Dot1dStaticStatus [ ] ={1,3,6,1,2,1,17,5,1,1,4};


tMbDbEntry stdbriMibEntry[]= {

{{9,Dot1dBaseBridgeAddress}, NULL, Dot1dBaseBridgeAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dBaseNumPorts}, NULL, Dot1dBaseNumPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dBaseType}, NULL, Dot1dBaseTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,Dot1dBasePort}, GetNextIndexDot1dBasePortTable, Dot1dBasePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dBasePortIfIndex}, GetNextIndexDot1dBasePortTable, Dot1dBasePortIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dBasePortCircuit}, GetNextIndexDot1dBasePortTable, Dot1dBasePortCircuitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, Dot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dBasePortDelayExceededDiscards}, GetNextIndexDot1dBasePortTable, Dot1dBasePortDelayExceededDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dBasePortMtuExceededDiscards}, GetNextIndexDot1dBasePortTable, Dot1dBasePortMtuExceededDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dBasePortTableINDEX, 1, 0, 0, NULL},

{{9,Dot1dStpProtocolSpecification}, NULL, Dot1dStpProtocolSpecificationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpPriority}, NULL, Dot1dStpPriorityGet, Dot1dStpPrioritySet, Dot1dStpPriorityTest, Dot1dStpPriorityDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpTimeSinceTopologyChange}, NULL, Dot1dStpTimeSinceTopologyChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpTopChanges}, NULL, Dot1dStpTopChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpDesignatedRoot}, NULL, Dot1dStpDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpRootCost}, NULL, Dot1dStpRootCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpRootPort}, NULL, Dot1dStpRootPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpMaxAge}, NULL, Dot1dStpMaxAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpHelloTime}, NULL, Dot1dStpHelloTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpHoldTime}, NULL, Dot1dStpHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpForwardDelay}, NULL, Dot1dStpForwardDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpBridgeMaxAge}, NULL, Dot1dStpBridgeMaxAgeGet, Dot1dStpBridgeMaxAgeSet, Dot1dStpBridgeMaxAgeTest, Dot1dStpBridgeMaxAgeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpBridgeHelloTime}, NULL, Dot1dStpBridgeHelloTimeGet, Dot1dStpBridgeHelloTimeSet, Dot1dStpBridgeHelloTimeTest, Dot1dStpBridgeHelloTimeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Dot1dStpBridgeForwardDelay}, NULL, Dot1dStpBridgeForwardDelayGet, Dot1dStpBridgeForwardDelaySet, Dot1dStpBridgeForwardDelayTest, Dot1dStpBridgeForwardDelayDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Dot1dStpPort}, GetNextIndexDot1dStpPortTable, Dot1dStpPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortPriority}, GetNextIndexDot1dStpPortTable, Dot1dStpPortPriorityGet, Dot1dStpPortPrioritySet, Dot1dStpPortPriorityTest, Dot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortState}, GetNextIndexDot1dStpPortTable, Dot1dStpPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortEnable}, GetNextIndexDot1dStpPortTable, Dot1dStpPortEnableGet, Dot1dStpPortEnableSet, Dot1dStpPortEnableTest, Dot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortPathCost}, GetNextIndexDot1dStpPortTable, Dot1dStpPortPathCostGet, Dot1dStpPortPathCostSet, Dot1dStpPortPathCostTest, Dot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortDesignatedRoot}, GetNextIndexDot1dStpPortTable, Dot1dStpPortDesignatedRootGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortDesignatedCost}, GetNextIndexDot1dStpPortTable, Dot1dStpPortDesignatedCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortDesignatedBridge}, GetNextIndexDot1dStpPortTable, Dot1dStpPortDesignatedBridgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortDesignatedPort}, GetNextIndexDot1dStpPortTable, Dot1dStpPortDesignatedPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortForwardTransitions}, GetNextIndexDot1dStpPortTable, Dot1dStpPortForwardTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStpPortPathCost32}, GetNextIndexDot1dStpPortTable, Dot1dStpPortPathCost32Get, Dot1dStpPortPathCost32Set, Dot1dStpPortPathCost32Test, Dot1dStpPortTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dStpPortTableINDEX, 1, 0, 0, NULL},

{{9,Dot1dTpLearnedEntryDiscards}, NULL, Dot1dTpLearnedEntryDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Dot1dTpAgingTime}, NULL, Dot1dTpAgingTimeGet, Dot1dTpAgingTimeSet, Dot1dTpAgingTimeTest, Dot1dTpAgingTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Dot1dTpFdbAddress}, GetNextIndexDot1dTpFdbTable, Dot1dTpFdbAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1dTpFdbTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpFdbPort}, GetNextIndexDot1dTpFdbTable, Dot1dTpFdbPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dTpFdbTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpFdbStatus}, GetNextIndexDot1dTpFdbTable, Dot1dTpFdbStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1dTpFdbTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPort}, GetNextIndexDot1dTpPortTable, Dot1dTpPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortMaxInfo}, GetNextIndexDot1dTpPortTable, Dot1dTpPortMaxInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortInFrames}, GetNextIndexDot1dTpPortTable, Dot1dTpPortInFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortOutFrames}, GetNextIndexDot1dTpPortTable, Dot1dTpPortOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dTpPortInDiscards}, GetNextIndexDot1dTpPortTable, Dot1dTpPortInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dTpPortTableINDEX, 1, 0, 0, NULL},

{{11,Dot1dStaticAddress}, GetNextIndexDot1dStaticTable, Dot1dStaticAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, Dot1dStaticTableINDEX, 2, 0, 0, NULL},

{{11,Dot1dStaticReceivePort}, GetNextIndexDot1dStaticTable, Dot1dStaticReceivePortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Dot1dStaticTableINDEX, 2, 0, 0, NULL},

{{11,Dot1dStaticAllowedToGoTo}, GetNextIndexDot1dStaticTable, Dot1dStaticAllowedToGoToGet, Dot1dStaticAllowedToGoToSet, Dot1dStaticAllowedToGoToTest, Dot1dStaticTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Dot1dStaticTableINDEX, 2, 0, 0, NULL},

{{11,Dot1dStaticStatus}, GetNextIndexDot1dStaticTable, Dot1dStaticStatusGet, Dot1dStaticStatusSet, Dot1dStaticStatusTest, Dot1dStaticTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dStaticTableINDEX, 2, 0, 0, NULL},
};
tMibData stdbriEntry = { 47, stdbriMibEntry };
#endif /* _STDBRIDB_H */

