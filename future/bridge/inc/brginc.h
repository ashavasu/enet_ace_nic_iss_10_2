/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brginc.h,v 1.9 2012/08/08 10:32:26 siva Exp $
 *
 * Description:This file contains FUTURE Bridge     
 *              include files.                   
 *
 *******************************************************************/
#ifndef  BRGINC_H
#define  BRGINC_H

#include "lr.h"
#include "ip.h"
#include "cfa.h"

#include "snmccons.h"
#include "snmcdefn.h"
#include "snmctdfs.h"

#include "la.h"
#include "bridge.h"
#include "fsvlan.h"
#include "pnac.h"
#include "snp.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
#include "rstp.h"
#include "mstp.h"

#include "l2iwf.h"

#include "bmport.h"
#include "bmcons.h"
#include "typdfs.h"
#include "tim.h"
#include "brggroup.h"
#include "basedfs.h"
#include "tpdfs.h"
#include "filfor.h"
#include "stattab.h"
#include "mlist.h"
#include "brgtrace.h"
#include "brgprot.h"
#include "brgextn.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "cfanp.h"
#include "brgnp.h"
#include "cfanpwr.h"
#endif

#include "stdbrlow.h"
#include "fsbrilow.h"
#include "tphclow.h"
#include "brgif.h"
#endif   
