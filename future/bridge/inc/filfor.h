/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: filfor.h,v 1.2 2007/02/01 14:44:35 iss Exp $
 *
 * Description:This file contains the typedef of the
 *             filtering and the forwarding databases
 *
 *******************************************************************/
#define   HASH_LIST_SIZE    0x20/*
                                 * Number of buckets in the forwarding
                                 * table
                                 */

/*************************************************************
 * The Time interval between the encounter of same FDB entry**
 *************************************************************/
#define   TIME_OFFSET    1    /* Not used in the code */

/**************************************
 * values used  for local processing***
 **************************************/
#define   BRIDGE_ITSELF         4
#define   STATIC_ENTRY_EXIST    5
#define   STORED                1

/*********************************************
 * Possible values taken by dot1dTpFdbStatus.*
 *********************************************/
#define   FDB_OTHER          1
#define   FDB_INVALID        2
#define   FDB_LEARNED        3
#define   FDB_SELF           4
#define   FDB_MGMT_STATIC    5

typedef struct FORWARDING_ENTRY {
    tTMO_HASH_NODE  link;
    tTIMER          ageout;
    UINT4           u4Fdbid;
    tMacAddr        destAddr;  /* dot1dTpFdbAddress */
    UINT1           u1Status;  /* dot1dTpFdbStatus */
    UINT1           u1Reserved;
} tFORWARDING_ENTRY;


typedef struct FORWARD_LOOKUPTABLE_ENTRY
{
    tFORWARDING_ENTRY  *ptr;
} tFORWARD_LOOKUPTABLE_ENTRY;

#define   NO_FDB_EXIST   0xffffffff 
#define   REMOVE_FDB_ENTRY(pFwdEntry)   \
          TMO_HASH_Delete_Node (pSHtab,(tTMO_HASH_NODE *)&(pFwdEntry->link), HashFn (&(pFwdEntry->destAddr)));

/*****************************************************************************/
/*                         Filter Information                                */
/*****************************************************************************/ 

#define   MAX_FILTER_NUMBER    100
                               /* The  range  of  filter  number  is from
                                * 0-99 but to ease checking and declaration
                                * it is declared as 100.
                                */


/*************************************************************************
 * The status of the filtering database entries. i.e The values taken by**
 * dot1dFilterPermiss.                                                  **
 *************************************************************************/

#define   MACF_DISCARD    1    /* Discard the frame   */
#define   MACF_FORWARD    2    /* Forward if possible */
#define   MACF_INVALID    3    /* Empty slot          */


/*************************************************************************
 * The data structure of each filtering entry node in the filtering list.*
 *************************************************************************/
typedef struct FILTERING_ENTRY
{
    tTMO_SLL_NODE  link;
    tMacAddr    sourceAddr;  /* dot1dFilterSrcAddress */
    tMacAddr    sourceMask;  /* dot1dFilterSrcMask */
    tMacAddr    destAddr;    /* dot1dFilterDstAddress */
    tMacAddr    destMask;    /* dot1dFilterDstMask */
    UINT1          u1Status;    /* dot1dFilterPermiss */
    UINT1          reserved1;    /* padding */
    UINT1          reserved2;    /* padding */
    UINT1          reserved3;    /* padding */
} tFILTERING_ENTRY;


/*************************************************** 
 * Data structure to maintain a single filter list**
 ***************************************************/
typedef struct FILTER_LOOKUPTABLE_ENTRY 
{
    tTMO_SLL  entry;
} tFILTER_LOOKUPTABLE_ENTRY;


