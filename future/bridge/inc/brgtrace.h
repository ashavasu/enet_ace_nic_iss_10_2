/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brgtrace.h,v 1.2 2007/02/01 14:44:35 iss Exp $
 *
 * Description: Provides general purpose macros for tracing
 *
 *******************************************************************/
#ifndef _BRG_TRACE_H_
#define _BRG_TRACE_H_

typedef struct BrgGlb { 
    UINT4  u4BrgDbg;
    UINT4  u4BrgTrc;
} tBrgGlb;


/* Trace and debug flags */
#define   BRG_TRC_FLAG            gBrgGlb.u4BrgTrc
#define   BRG_TRC_DBG             gBrgGlb.u4BrgDbg


#define BRG_MIN_TRACE_VAL             0
#define BRG_MAX_TRACE_VAL             255

/* Module names */
#define   BRG_MOD_NAME                       ((const char *)"BRG")


/* Trace definitions */
#define   BRG_TRC(BRG_TRC_FLAG, TraceType, BRG_MOD_NAME, Str)                \
        MOD_TRC(BRG_TRC_FLAG, TraceType, BRG_MOD_NAME, (const char *)Str)

#define   BRG_TRC_ARG1(BRG_TRC_FLAG, TraceType, BRG_MOD_NAME, Str, Arg1)      \
        MOD_TRC_ARG1(BRG_TRC_FLAG, TraceType, BRG_MOD_NAME, (const char *)Str, Arg1)

#define   BRG_TRC_ARG2(BRG_TRC_FLAG, TraceType, BRG_MOD_NAME, Str, Arg1, Arg2)  \
        MOD_TRC_ARG2(BRG_TRC_FLAG, TraceType, BRG_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define   BRG_TRC_ARG3(BRG_TRC_FLAG, TraceType, BRG_MOD_NAME, Str, Arg1, Arg2, Arg3) \
        MOD_TRC_ARG3(BRG_TRC_FLAG, TraceType, BRG_MOD_NAME, (const char *)Str, Arg1, Arg2, Arg3)


/* Pl undefine this switch in common makefile to exclude this code */
#ifdef TRACE_WANTED

 /* Common bitmasks for various events. This has to be implemented in all the 
  * modules in the same fashion
  */

#define   BRG_INIT_SHUT_TRC      INIT_SHUT_TRC     
#define   BRG_MGMT_TRC           MGMT_TRC          
#define   BRG_DATA_PATH_TRC      DATA_PATH_TRC     
#define   BRG_CONTROL_PATH_TRC   CONTROL_PLANE_TRC 
#define   BRG_DUMP_TRC           DUMP_TRC          
#define   BRG_OS_RESOURCE_TRC    OS_RESOURCE_TRC   
#define   BRG_ALL_FAILURE_TRC    ALL_FAILURE_TRC   
#define   BRG_BUFFER_TRC         BUFFER_TRC        
#endif /* _TRACE_H_ */
#endif/* BRG_TRACE_H_ */
