/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsbriddb.h,v 1.5 2011/11/03 07:16:59 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSBRIDDB_H
#define _FSBRIDDB_H

UINT1 Dot1dFutureBasePortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dFutureTpPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Dot1dFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_FIXED_LENGTH_OCTET_STRING ,6};
UINT1 Dot1dMcastTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,6 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsbrid [] ={1,3,6,1,4,1,2076,26};
tSNMP_OID_TYPE fsbridOID = {8, fsbrid};


UINT4 Dot1dBridgeSystemControl [ ] ={1,3,6,1,4,1,2076,26,1,1,1};
UINT4 Dot1dBaseBridgeStatus [ ] ={1,3,6,1,4,1,2076,26,1,1,2};
UINT4 Dot1dBaseBridgeCRCStatus [ ] ={1,3,6,1,4,1,2076,26,1,1,3};
UINT4 Dot1dBaseBridgeDebug [ ] ={1,3,6,1,4,1,2076,26,1,1,4};
UINT4 Dot1dBaseBridgeTrace [ ] ={1,3,6,1,4,1,2076,26,1,1,5};
UINT4 Dot1dBaseBridgeMaxFwdDbEntries [ ] ={1,3,6,1,4,1,2076,26,1,1,6};
UINT4 Dot1dFutureBasePort [ ] ={1,3,6,1,4,1,2076,26,1,1,7,1,1};
UINT4 Dot1dBasePortAdminStatus [ ] ={1,3,6,1,4,1,2076,26,1,1,7,1,2};
UINT4 Dot1dBasePortOperStatus [ ] ={1,3,6,1,4,1,2076,26,1,1,7,1,3};
UINT4 Dot1dBasePortBcastStatus [ ] ={1,3,6,1,4,1,2076,26,1,1,7,1,4};
UINT4 Dot1dBasePortFilterNumber [ ] ={1,3,6,1,4,1,2076,26,1,1,7,1,5};
UINT4 Dot1dBasePortMcastNumber [ ] ={1,3,6,1,4,1,2076,26,1,1,7,1,6};
UINT4 Dot1dBasePortBcastOutFrames [ ] ={1,3,6,1,4,1,2076,26,1,1,7,1,7};
UINT4 Dot1dBasePortMcastOutFrames [ ] ={1,3,6,1,4,1,2076,26,1,1,7,1,8};
UINT4 Dot1dFutureTpPort [ ] ={1,3,6,1,4,1,2076,26,1,2,1,1,1};
UINT4 Dot1dTpPortInProtoDiscards [ ] ={1,3,6,1,4,1,2076,26,1,2,1,1,2};
UINT4 Dot1dTpPortInFilterDiscards [ ] ={1,3,6,1,4,1,2076,26,1,2,1,1,3};
UINT4 Dot1dTpPortProtocolFilterMask [ ] ={1,3,6,1,4,1,2076,26,1,2,1,1,4};
UINT4 Dot1dFilterNumber [ ] ={1,3,6,1,4,1,2076,26,1,3,1,1,1};
UINT4 Dot1dFilterSrcAddress [ ] ={1,3,6,1,4,1,2076,26,1,3,1,1,2};
UINT4 Dot1dFilterSrcMask [ ] ={1,3,6,1,4,1,2076,26,1,3,1,1,3};
UINT4 Dot1dFilterDstAddress [ ] ={1,3,6,1,4,1,2076,26,1,3,1,1,4};
UINT4 Dot1dFilterDstMask [ ] ={1,3,6,1,4,1,2076,26,1,3,1,1,5};
UINT4 Dot1dFilterPermiss [ ] ={1,3,6,1,4,1,2076,26,1,3,1,1,6};
UINT4 Dot1dMlistNumber [ ] ={1,3,6,1,4,1,2076,26,1,4,1,1,1};
UINT4 Dot1dMcastMacaddress [ ] ={1,3,6,1,4,1,2076,26,1,4,1,1,2};
UINT4 Dot1dMcastPermiss [ ] ={1,3,6,1,4,1,2076,26,1,4,1,1,3};


tMbDbEntry fsbridMibEntry[]= {

{{11,Dot1dBridgeSystemControl}, NULL, Dot1dBridgeSystemControlGet, Dot1dBridgeSystemControlSet, Dot1dBridgeSystemControlTest, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,Dot1dBaseBridgeStatus}, NULL, Dot1dBaseBridgeStatusGet, Dot1dBaseBridgeStatusSet, Dot1dBaseBridgeStatusTest, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,Dot1dBaseBridgeCRCStatus}, NULL, Dot1dBaseBridgeCRCStatusGet, Dot1dBaseBridgeCRCStatusSet, Dot1dBaseBridgeCRCStatusTest, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,Dot1dBaseBridgeDebug}, NULL, Dot1dBaseBridgeDebugGet, Dot1dBaseBridgeDebugSet, Dot1dBaseBridgeDebugTest, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Dot1dBaseBridgeTrace}, NULL, Dot1dBaseBridgeTraceGet, Dot1dBaseBridgeTraceSet, Dot1dBaseBridgeTraceTest, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,Dot1dBaseBridgeMaxFwdDbEntries}, NULL, Dot1dBaseBridgeMaxFwdDbEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,Dot1dFutureBasePort}, GetNextIndexDot1dFutureBasePortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1dFutureBasePortTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dBasePortAdminStatus}, GetNextIndexDot1dFutureBasePortTable, Dot1dBasePortAdminStatusGet, Dot1dBasePortAdminStatusSet, Dot1dBasePortAdminStatusTest, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dFutureBasePortTableINDEX, 1, 0, 0, "1"},

{{13,Dot1dBasePortOperStatus}, GetNextIndexDot1dFutureBasePortTable, Dot1dBasePortOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Dot1dFutureBasePortTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dBasePortBcastStatus}, GetNextIndexDot1dFutureBasePortTable, Dot1dBasePortBcastStatusGet, Dot1dBasePortBcastStatusSet, Dot1dBasePortBcastStatusTest, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dFutureBasePortTableINDEX, 1, 0, 0, "1"},

{{13,Dot1dBasePortFilterNumber}, GetNextIndexDot1dFutureBasePortTable, Dot1dBasePortFilterNumberGet, Dot1dBasePortFilterNumberSet, Dot1dBasePortFilterNumberTest, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dFutureBasePortTableINDEX, 1, 0, 0, "0"},

{{13,Dot1dBasePortMcastNumber}, GetNextIndexDot1dFutureBasePortTable, Dot1dBasePortMcastNumberGet, Dot1dBasePortMcastNumberSet, Dot1dBasePortMcastNumberTest, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dFutureBasePortTableINDEX, 1, 0, 0, "0"},

{{13,Dot1dBasePortBcastOutFrames}, GetNextIndexDot1dFutureBasePortTable, Dot1dBasePortBcastOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dFutureBasePortTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dBasePortMcastOutFrames}, GetNextIndexDot1dFutureBasePortTable, Dot1dBasePortMcastOutFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dFutureBasePortTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dFutureTpPort}, GetNextIndexDot1dFutureTpPortTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1dFutureTpPortTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dTpPortInProtoDiscards}, GetNextIndexDot1dFutureTpPortTable, Dot1dTpPortInProtoDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dFutureTpPortTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dTpPortInFilterDiscards}, GetNextIndexDot1dFutureTpPortTable, Dot1dTpPortInFilterDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Dot1dFutureTpPortTableINDEX, 1, 0, 0, NULL},

{{13,Dot1dTpPortProtocolFilterMask}, GetNextIndexDot1dFutureTpPortTable, Dot1dTpPortProtocolFilterMaskGet, Dot1dTpPortProtocolFilterMaskSet, Dot1dTpPortProtocolFilterMaskTest, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Dot1dFutureTpPortTableINDEX, 1, 0, 0, "0"},

{{13,Dot1dFilterNumber}, GetNextIndexDot1dFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1dFilterTableINDEX, 3, 0, 0, NULL},

{{13,Dot1dFilterSrcAddress}, GetNextIndexDot1dFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot1dFilterTableINDEX, 3, 0, 0, NULL},

{{13,Dot1dFilterSrcMask}, GetNextIndexDot1dFilterTable, Dot1dFilterSrcMaskGet, Dot1dFilterSrcMaskSet, Dot1dFilterSrcMaskTest, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot1dFilterTableINDEX, 3, 0, 0, "FFFFFFFFFFFF"},

{{13,Dot1dFilterDstAddress}, GetNextIndexDot1dFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot1dFilterTableINDEX, 3, 0, 0, NULL},

{{13,Dot1dFilterDstMask}, GetNextIndexDot1dFilterTable, Dot1dFilterDstMaskGet, Dot1dFilterDstMaskSet, Dot1dFilterDstMaskTest, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, Dot1dFilterTableINDEX, 3, 0, 0, "FFFFFFFFFFFF"},

{{13,Dot1dFilterPermiss}, GetNextIndexDot1dFilterTable, Dot1dFilterPermissGet, Dot1dFilterPermissSet, Dot1dFilterPermissTest, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dFilterTableINDEX, 3, 0, 0, "2"},

{{13,Dot1dMlistNumber}, GetNextIndexDot1dMcastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Dot1dMcastTableINDEX, 2, 0, 0, NULL},

{{13,Dot1dMcastMacaddress}, GetNextIndexDot1dMcastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_NOACCESS, Dot1dMcastTableINDEX, 2, 0, 0, NULL},

{{13,Dot1dMcastPermiss}, GetNextIndexDot1dMcastTable, Dot1dMcastPermissGet, Dot1dMcastPermissSet, Dot1dMcastPermissTest, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Dot1dMcastTableINDEX, 2, 0, 0, "2"},
};
tMibData fsbridEntry = { 27, fsbridMibEntry };
#endif /* _FSBRIDDB_H */

