#include "brginc.h"

/* Proto Validate Index Instance for Dot1dTpHCPortTable. */
INT1
nmhValidateIndexInstanceDot1dTpHCPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dTpHCPortTable  */

INT1
nmhGetFirstIndexDot1dTpHCPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dTpHCPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dTpHCPortInFrames ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot1dTpHCPortOutFrames ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDot1dTpHCPortInDiscards ARG_LIST((INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for Dot1dTpPortOverflowTable. */
INT1
nmhValidateIndexInstanceDot1dTpPortOverflowTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Dot1dTpPortOverflowTable  */

INT1
nmhGetFirstIndexDot1dTpPortOverflowTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDot1dTpPortOverflowTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDot1dTpPortInOverflowFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1dTpPortOutOverflowFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDot1dTpPortInOverflowDiscards ARG_LIST((INT4 ,UINT4 *));

INT1 BrgSnmpGetNextPortEntry(INT4 ,INT4 *);


