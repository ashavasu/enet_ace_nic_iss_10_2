/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: bmcons.h,v 1.3 2007/02/01 14:44:35 iss Exp $
 *
 * Description:This file contains constants that are used
 *             in the STAP.
 *
 *******************************************************************/

/******************************************************************/
/*  The below mentioned constants are the recommended values      */
/*  specified in ISO/IEC 10038 ANSI/IEEE Std 802.1D.              */
/******************************************************************/

#ifndef _BMCONS_H
#define _BMCONS_H

/*
 * The possible values that dot1dBaseType can take
 */
#define  UNKNOWN_BRIDGE      1 
#define  TRANSPARENT_BRIDGE  2     /* Only this value is used */
#define  SOURCE_ROUTE        3 

/*
 * CRC Status Values
 */
#define   BRG_WITH_CRC       1
#define   BRG_WITHOUT_CRC    2

#define   MAX_BRG_DIAMETER           7
#define   MAX_FRAME_LIFE_TIME        7
#define   MAX_MEDIUM_ACCESS_DELAY    1

#define  BROADCAST_UP               1             
#define  BROADCAST_DOWN             2             

#define  ETHERNET_COST              (1000 / 10)   
#define  SERIAL_COST                (1000 / 2)    
#define  PPP_COST                   ETHERNET_COST 
#define   ATM_COST        ETHERNET_COST

#define  PORT_PRIORITY              0x80          
#define  BRIDGE_PRIORITY            0x8000        

#define  DEF_PRO_MASK               0x0000        
#define  DEF_MAXINFO                512           

#define   SEC_TO_STAP(x)           ((x) / 100 )
#define   SHORT_AGEOUT_TIME        FORWARD_DELAY
#define   NORMAL_AGEOUT_TIME       SEC_TO_STAP(30000)

#define   FDB_TIME_TO_MGMT(x)      (x)
#define   MGMT_TO_FDB_TIME(x)      (x)

#define  BRG_MIN_AGEOUT_TIME        10            
#define  BRG_MAX_AGEOUT_TIME        1000000       


#define  BRIDGING                   0x0001        
#define  STAP                       0x0002        

#define  ENABLE                     1             
#define  DISABLE                    2             

/* SNMP START and SHUTDOWN services */
#define BRG_SNMP_START              1
#define BRG_SNMP_SHUTDOWN           2

#define  BRG_SNMP_TRUE               1
#define  BRG_SNMP_FALSE              2

#define  BRG_ENABLED                1             
#define  BRG_DISABLED               2             

#define  BRG_PPP_MAX_INFO           1500          
#define  BRG_ATMVC_MAX_INFO           9188

#define BRIDGE_GROUP_ADDRESS    { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x00}

#define BRG_STATIC_ALL_PORT 0 
#define BRG_MIN_PORT_NO      1

 /* Should be changed if FORWARD_DELAY is changed */
#define  BRG_SHORT_AGING_TIME 15				     

/* Related to FWD  and Filter entries constants */
#define   BRG_MAX_FWD_ENTRIES       65536  
#define   BRG_MAX_FILTER_ENTRIES    65536 
#define   BRG_INIT_VAL              0  

#define BRG_INVALID_PORT_INDEX  (BRG_MAX_PHY_PLUS_LOG_PORTS + 1)
#endif /* _BMCONS_H */
