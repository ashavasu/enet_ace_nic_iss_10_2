/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtfdbt.c,v 1.2 2007/02/01 14:44:36 iss Exp $
 *
 * Description:Routines for maintaining the forwarding        
 *             database table     
 *
 *******************************************************************/
#include "brginc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdDbInit                                   */
/*                                                                           */
/*    Description               : Initialises all the database in the Bridge */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_SUCCESS: On Success.                */
/*                                BRIDGE_FAILURE: On Failure.                */
/*                                                                           */
/*****************************************************************************/
INT1
BdDbInit (VOID)
{

    INT4                i4RetSts = BRIDGE_SUCCESS;

    i4RetSts = BdFdbInit ();
    if (i4RetSts == BRIDGE_FAILURE)
    {
        BrgDbDeInit ();
        return BRIDGE_FAILURE;
    }
    BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
             " Forward Db initialized successfully \n");

    i4RetSts = BdFilterInit ();
    if (i4RetSts == BRIDGE_FAILURE)
    {
        BrgDbDeInit ();
        return BRIDGE_FAILURE;
    }
    BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
             " Filter Db initialized successfully \n");

    i4RetSts = BdMlistInit ();
    if (i4RetSts == BRIDGE_FAILURE)
    {
        BrgDbDeInit ();
        return BRIDGE_FAILURE;
    }
    BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
             " Multi-cast list Db initialized successfully \n");

    i4RetSts = BdStaticTableInit ();
    if (i4RetSts == BRIDGE_FAILURE)
    {
        BrgDbDeInit ();
        return BRIDGE_FAILURE;
    }
    BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
             " Static Db initialized successfully \n");

    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BrgDbDeInit                                   */
/*                                                                           */
/*    Description               : De-Initialises all the database in the Bridge */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_SUCCESS: On Success.                */
/*                                                                           */
/*                                BRIDGE_FAILURE: On Failure                 */
/*****************************************************************************/
INT1
BrgDbDeInit (VOID)
{
    INT1                i1RetSts = BRIDGE_SUCCESS;

    if (BRG_FDB_HASH_TBL != NULL)
    {
        i1RetSts = BrgFdbDeInit ();
        if (i1RetSts == BRIDGE_FAILURE)
        {
            BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
                     " Forward Db De-initialization Failed!  \n");
        }
    }

    if (BRG_FILT_MEM_POOLID != BRG_INIT_VAL)
    {
        i1RetSts = BrgFilterDeInit ();
        if (i1RetSts == BRIDGE_FAILURE)
        {
            BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
                     " Filter Db De-initialization Failed! \n");
        }
    }

    if (BRG_MCAST_MEM_POOLID != BRG_INIT_VAL)
    {

        i1RetSts = BrgMlistDeInit ();
        if (i1RetSts == BRIDGE_FAILURE)
        {
            BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
                     " Multi-cast list De-initialization Failed! \n");
        }
    }

    if (BRG_STATIC_MEM_POOLID != BRG_INIT_VAL)
    {

        i1RetSts = BrgStaticDeInit ();
        if (i1RetSts == BRIDGE_FAILURE)
        {
            BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
                     " Static Db De-initialization Failed! \n");
        }
    }
    return i1RetSts;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BrgFdbDeInit */
/*                                                                           */
/*    Description               : De-Initialises the hash table and call routine*/
/*                                to release memory.               */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : Hash table.                                */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_SUCCESS: On Success.                */
/*                                                                           */
/*                                BRIDGE_FAILURE: On Failure                 */
/*****************************************************************************/
INT1
BrgFdbDeInit (VOID)
{
    UINT4               u4RetVal = BRG_MEM_SUCCESS;

    /* Delete the hash table BRG_FDB_HASH_TBL  */
    if (BRG_FDB_HASH_TBL != NULL)
    {
        BRG_DELETE_HASH_TABLE (BRG_FDB_HASH_TBL, DeleteNodeFn);
        BRG_FDB_HASH_TBL = NULL;
    }

    u4RetVal = BRG_DELETE_MEM_POOL (BRG_FDB_MEM_POOLID);
    if (u4RetVal == BRG_MEM_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to delete memory pool for fdb Tbl entries  \n");
        return BRIDGE_FAILURE;
    }
    BRG_FDB_MEM_POOLID = 0;

    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BrgFilterDeInit                            */
/*                                                                           */
/*    Description               : Release memory for the filter              */
/*                                and initialises all the filter list        */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_SUCCESS: On Success.                */
/*                                BRIDGE_FAILURE: On Failure.                */
/*                                                                           */
/*****************************************************************************/

INT1
BrgFilterDeInit (VOID)
{
    UINT4               u4RetVal = BRG_MEM_SUCCESS;

    u4RetVal = BRG_DELETE_MEM_POOL (BRG_FILT_MEM_POOLID);

    if (u4RetVal == BRG_MEM_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to delete memory pool for filering Tbl entries  \n");
        return BRIDGE_FAILURE;
    }
    BRG_FILT_MEM_POOLID = 0;

    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BrgMlistDeInit */
/*                                                                           */
/*    Description               : Release memory for the multicast Tbl       */
/*                                and de-initialises all the multi-filter list */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_SUCCESS: On Success.                */
/*                                BRIDGE_FAILURE: On Failure.                */
/*                                                                           */
/*****************************************************************************/
INT1
BrgMlistDeInit (VOID)
{
    UINT4               u4RetVal = BRG_MEM_SUCCESS;

    u4RetVal = BRG_DELETE_MEM_POOL (BRG_MCAST_MEM_POOLID);

    if (u4RetVal == BRG_MEM_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to delete memory for filering Table entries  \n");
        return BRIDGE_FAILURE;
    }
    BRG_MCAST_MEM_POOLID = 0;

    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BrgStaticDeInit */
/*                                                                           */
/*    Description               : De-Initialises Static  */
/*                                release the memory for static data base    */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : Hash table.                                */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_SUCCESS : On Success.               */
/*                                BRIDGE_FAILURE : On Failure.               */
/*                                                                           */
/*****************************************************************************/
INT1
BrgStaticDeInit (VOID)
{

    UINT4               u4RetVal = BRG_MEM_SUCCESS;

    u4RetVal = BRG_DELETE_MEM_POOL (BRG_STATIC_MEM_POOLID);
    if (u4RetVal == BRG_MEM_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to delete memory pool for static Table entries  \n");
        return BRIDGE_FAILURE;
    }
    BRG_STATIC_MEM_POOLID = 0;
    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdFdbInit                                  */
/*                                                                           */
/*    Description               : Initialises the hash table and call routine*/
/*                                to allocate memory for heap.               */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : Hash table.                                */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_SUCCESS: On Success.                */
/*                                BRIDGE_FAILURE: On Failure.                */
/*                                                                           */
/*****************************************************************************/

INT4
BdFdbInit (void)
{
    if ((pSHtab = TMO_HASH_Create_Table (HASH_LIST_SIZE,
                                         InsertFn, TRUE)) == NULL)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to create Hash Table for the Forwarding Table \n");
        return BRIDGE_FAILURE;
    }
    if (BdFdbMemoryInit () == BRIDGE_NOT_OK)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to initialize the memory for forwarding table \n");
        return BRIDGE_FAILURE;
    }

    return BRIDGE_SUCCESS;
}

/*******************************************************************************/
/*                                                                             */
/*    Function Name             : BdLearnFdbEntry                              */
/*                                                                             */
/*    Description               : Routine stores the corresponding address     */
/*                                and the FDB Id in the Hash table.            */
/*                                                                             */
/*    Input(s)                  : pMacAddr  - The address to be learned.       */
/*                                u4Fdbid   - The FDBID corresponding to the   */
/*                                            Interface Id.                    */
/*                                u1Status  - The Status of the Entry.         */
/*                                                                             */
/*    Output(s)                 : None                                         */
/*                                                                             */
/*    Global Variables Referred : None                                         */
/*                                                                             */
/*    Global Variables Modified : Hash table.                                  */
/*                                                                             */
/*    Exceptions or Operating                                                  */
/*    System Error Handling     : None.                                        */
/*                                                                             */
/*    Use of Recursion          : None.                                        */
/*                                                                             */
/*    Returns                   : BRIDGE_OK     : If the entry already exist and      */
/*                                         the status specified is FDB_INVALID */
/*                                         and deletion is made successfully.  */
/*                                BRIDGE_NOT_OK : If no memory can be allocated       */
/*                                         from heap.                          */
/*                                                                             */
/*                                STATIC_ENTRY_EXIST: Static entry with same   */
/*                                                    address is already       */
/*                                                    present                  */
/*                                BRIDGE_ITSELF     : The specified address is */
/*                                                    the address of this      */
/*                                                    bridge itself.           */
/*                                                                             */
/*                                STORED            : If an entry is made      */
/*                                                    new or overwritten.      */
/*                                                                             */
/*******************************************************************************/

INT4
BdLearnFdbEntry (tMacAddr * pMacaddr, UINT4 u4Fdbid, UINT1 u1Status)
{
    tFORWARDING_ENTRY  *pFdb;

    if ((pFdb = (tFORWARDING_ENTRY *) HashSearchNode (pSHtab,
                                                      pMacaddr)) == NULL)
    {

        if (u1Status == FDB_INVALID)
        {

            BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC, BRG_MOD_NAME,
                     " FDB Entry is invalid, while learning   \n");
            return BRIDGE_OK;
        }

        if ((pFdb = (tFORWARDING_ENTRY *) BdGetNewFdbEntry ()) == NULL)
        {
            BRG_TRC (BRG_TRC_FLAG,
                     BRG_DATA_PATH_TRC | BRG_OS_RESOURCE_TRC |
                     BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                     " Memory Allocation Failure, "
                     "during the creation of new FDB entry   \n");
            return BRIDGE_NOT_OK;
        }

        BRG_MEM_CPY (pFdb->destAddr, (UINT1 *) *pMacaddr, ETHERNET_ADDR_SIZE);
        pFdb->u4Fdbid = u4Fdbid;
        pFdb->u1Status = u1Status;

        TMO_HASH_Add_Node (pSHtab, (tTMO_HASH_NODE *) & (pFdb->link),
                           HashFn (pMacaddr), (UINT1 *) pMacaddr);
#ifdef NPAPI_WANTED
        FsBrgAddMacEntry (GET_LOGICAL_PORT_FROM_FDBID (u4Fdbid),
                          (UINT1 *) *pMacaddr);
#endif

        switch (u1Status)
        {
            case FDB_LEARNED:
                TmStartTimer (&pFdb->ageout);
                break;

            case FDB_OTHER:    /* FALL THROUGH */

            case FDB_INVALID:    /* FALL THROUGH */

            case FDB_SELF:        /* ,,     ,,    */

            case FDB_MGMT_STATIC:
                TmStopTimer (&pFdb->ageout);
                break;
        }
    }
    else
    {
        if (u1Status == FDB_LEARNED)
        {
            switch (pFdb->u1Status)
            {
                case FDB_SELF:
                    return BRIDGE_ITSELF;

                case FDB_MGMT_STATIC:
                    return STATIC_ENTRY_EXIST;

                case FDB_LEARNED:
                    pFdb->u4Fdbid = u4Fdbid;
                    TmStartTimer ((tTIMER *) & pFdb->ageout);
                    return STORED;

                default:
                    return -1;
            }
        }
        else
        {
            switch (u1Status)
            {
                case FDB_INVALID:
                    TmStopTimer (&pFdb->ageout);
                    REMOVE_FDB_ENTRY (pFdb);

#ifdef NPAPI_WANTED
                    FsBrgDelMacEntry (GET_LOGICAL_PORT_FROM_FDBID
                                      (pFdb->u4Fdbid), pFdb->destAddr);
#endif
                    BdFreeFdbEntry (pFdb);
                    return BRIDGE_OK;

                default:
                    pFdb->u4Fdbid = u4Fdbid;
                    pFdb->u1Status = u1Status;
                    TmStopTimer ((tTIMER *) & pFdb->ageout);
            }
        }
    }
    BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
             BRG_MOD_NAME,
             "Source Address LEARNING for data frames SUCCESSFUL\n");
    return STORED;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdGetPortFromDest                          */
/*                                                                           */
/*    Description               : This routine returns the FDBID stored for  */
/*                                the corresponding MacAddress.              */
/*                                                                           */
/*    Input(s)                  : pMacaddr - MacAddress whose FDBID is needed*/
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : Hash table.                                */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : FDBID  : If matching found                 */
/*                                NO_FDB_EXIST: If matching not found.       */
/*                                                                           */
/*****************************************************************************/
UINT4
BdGetPortFromDest (tMacAddr * pMacaddr)
{
    tFORWARDING_ENTRY  *pFdb;

    if ((pFdb = (tFORWARDING_ENTRY *) HashSearchNode (pSHtab,
                                                      pMacaddr)) != NULL)
    {
        if (!IS_UNASSIGNED_FDB (pFdb->u4Fdbid))
        {
            return (unsigned) pFdb->u4Fdbid;
        }
    }
    return NO_FDB_EXIST;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdDeleteStaticEntry                        */
/*                                                                           */
/*    Description               : This routine removes the static            */
/*                                forwarding entries.                        */
/*                                                                           */
/*    Input(s)                  : u4EntryNo  - Index of the Entry            */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/

void
BdDeleteStaticEntry (UINT4 u4PortNo)
{
    tFORWARDING_ENTRY  *pFwdEntry;
    UINT4               u4Hindex;
    UINT1               u1PortNo;

    if (BRG_FDB_HASH_TBL != NULL)
    {
        for (u4Hindex = 0; u4Hindex < HASH_LIST_SIZE; u4Hindex++)
        {
            TMO_HASH_Scan_Bucket (pSHtab, u4Hindex, pFwdEntry,
                                  tFORWARDING_ENTRY *)
            {
                u1PortNo =
                    (UINT1) GET_LOGICAL_PORT_FROM_FDBID (pFwdEntry->u4Fdbid);
                if ((pFwdEntry->u1Status == FDB_MGMT_STATIC)
                    && (u1PortNo == (UINT1) u4PortNo))
                {
                    REMOVE_FDB_ENTRY (pFwdEntry);
                    BdFreeFdbEntry (pFwdEntry);
                    pFwdEntry = NULL;

                }
            }
        }
    }
}

/*************************** END OF FILE(dtfdbt.c) **********************/
