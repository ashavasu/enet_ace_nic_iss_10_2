/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tptxdt.c,v 1.9 2007/02/01 14:44:36 iss Exp $
 *
 * Description:This file has routines through which the transp
 *             part can access the Group and related datas.     
 *
 *******************************************************************/
#include "brginc.h"

/**********************************************************************
 * Functionality:-
 *      This routine receives the validated frame and learns the source 
 *    address if possible.
 *    Finds a cached forwarding database entry if one exists or 
 *    broadcast if the address is not cached.
 *    Performs necessary checking before transmitting the frame.
 *    updates statistics.
 *
 * Formal Parameters:-
 *    1.msgAttr : this specifies all the attributes (i.e size, 
 *                 destination addr, source address, protocol etc) of 
 *                 the received msg.
 *    2.pMsg    : the data buffer.
 *    3.u1InputPort: the logical port from which this frame was 
 *                     received.
 *    4.fdb_id   : the coded representation of the input interface_id .
 *
 * Global Variables Used:-
 *    base bridge datastructure (base) and tranparent bridge data 
 *    structure (tpInfo).
 *
 * Return Values:-
 *    None.
 *********************************************************************/

void
TpProcessDataFrame (tMSG_ATTR * msgAttr, tCRU_BUF_CHAIN_HEADER * pMsg,
                    UINT4 u4Fdbid)
{
    tBOOLEAN            bPortStatus = FALSE;
    UINT1               u1InputPort;
    INT2                i2FdbStatus;
    UINT4               u4PortNo, u4FdbidOut;
    tSTATICTABLE_ENTRY *pPtr = NULL;
#if defined IGS_WANTED || defined MLDS_WANTED
    tBrgIf              BrgIf;
    tPortList           PortList;
    UINT2               u2Result = 0;
    INT4                i4RetVal;
#endif /* IGS_WANTED */
    UINT2               u2PortState = 0xff;
#ifndef NPAPI_WANTED
    UINT4               u4CurSysTime = 0;
    UINT4               u4DataLength;
#endif

    /*
     * This portion will check whether the input frame is received
     * from a valid port or not ,the validity of the input port is
     * reflected in port_status field
     */

    u1InputPort = (UINT1) msgAttr->u2LogicalPortNo;

    u2PortState = (UINT2) L2IwfGetInstPortState (RST_DEFAULT_INSTANCE,
                                                 (UINT2) u1InputPort);

    switch (u2PortState)
    {
        case AST_PORT_STATE_LEARNING:
            BRG_TRC_ARG1 (BRG_TRC_FLAG, BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                          BRG_MOD_NAME,
                          "Processing data frame, "
                          "port in LEARNING State\n", u1InputPort);
            if ((i2FdbStatus = (INT2) BdLearnFdbEntry ((tMacAddr *) &
                                                       msgAttr->srcAddr,
                                                       u4Fdbid,
                                                       (UINT1) FDB_LEARNED))
                == BRIDGE_NOT_OK)
            {
                tpInfo.u4NoLearnedEntryDiscard++;
            }
            break;

        case AST_PORT_STATE_FORWARDING:
            BRG_TRC_ARG1 (BRG_TRC_FLAG, BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                          BRG_MOD_NAME,
                          "Processing data frame, "
                          "port in FORWARDING State  \n", u1InputPort);
            if ((i2FdbStatus = (INT2) BdLearnFdbEntry ((tMacAddr *) &
                                                       msgAttr->srcAddr,
                                                       u4Fdbid,
                                                       (UINT1) FDB_LEARNED))
                == BRIDGE_NOT_OK)
            {
                tpInfo.u4NoLearnedEntryDiscard++;
            }
            else
            {
                /****************************************************
                 *** If the Source address is Our own address the ***
                 *** this frame was originated by this Router     ***
                 *** Hence discard the Frame.  When RSTP/MSTP is UP    ***
                 *** This condition rarely occurs if there is a   ***
                 *** Topology Change.                             ***
                 ****************************************************/

                if (i2FdbStatus == FDB_SELF)
                {
                    bPortStatus = FALSE;
                }
                else
                {
                    bPortStatus = TRUE;
                }
            }
            break;

        default:
            break;
    }

    if (bPortStatus == TRUE)
    {
        /*  filter the frame based on Static  Table  */
        if ((pPtr = BrgFindStaticEntryInTbl (msgAttr->destAddr,
                                             u1InputPort)) != NULL)
        {
#ifndef NPAPI_WANTED
            /* Packet is not forwarded by H/W */
            BdFilterFrameBasedOnStaticTable (pPtr, pMsg, msgAttr);
#endif

            STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);
            BRG_TRC (BRG_TRC_FLAG,
                     BRG_DATA_PATH_TRC | BRG_BUFFER_TRC | BRG_ALL_FAILURE_TRC,
                     BRG_MOD_NAME,
                     "After static Tbl fwding, relxd orginal buffer "
                     "after giving the packet to CFA module \n  ");

            return;
        }

        /**************************************************************
         *** If The Multicast Bit is Set then broadcast this frame  ***
         **************************************************************/

        if (BM_IS_MAC_MULTICAST (msgAttr->destAddr[0]))
        {
            if (BM_IS_BCAST_ADDRESS (msgAttr->destAddr))
            {
                BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC, BRG_MOD_NAME,
                         "Broad cast pkt is rcvd \n");
#ifndef NPAPI_WANTED
                TpBroadcastFrame (pMsg, msgAttr, FALSE);
#else
                STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);
                return;
#endif
            }
            else
            {
                BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC, BRG_MOD_NAME,
                         "Multi-cast pkt is rcvd \n ");

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
                if ((SnoopIsIgmpSnoopingEnabled (SNOOP_INSTANCE_ID) ==
                     SNOOP_SUCCESS)
                    || (SnoopIsMldSnoopingEnabled (SNOOP_INSTANCE_ID) ==
                        SNOOP_SUCCESS))
                {

                    BRG_MEM_SET (&BrgIf, 0, sizeof (tBrgIf));
                    BRG_MEM_SET (&PortList, 0, sizeof (tPortList));

                    /* Fill the BrgIf InInfo */
                    BridgeFillInInfo (msgAttr, &BrgIf);

                    i4RetVal =
                        SnoopProcessMultiCastFrame (pMsg, &BrgIf, 0, &u2Result,
                                                    PortList);

                    /* Return value of failure form IGMP module is in following 
                     * conditions
                     * 1) Not able to extract IP header for identifying the pkt
                     * In these cases McastFwdTable lookup can't be performed and 
                     * the Mcast pkt should be broadcast
                     */
                    if (i4RetVal == SNOOP_FAILURE)
                    {
                        /* duplicated frame will be sent out. 
                         * Original frame will be released by this function
                         */
                        TpBroadcastFrame (pMsg, msgAttr, TRUE);
                        return;
                    }

                    /* IGMP modules is enabled & able to identify the pkt type */
                    if (u2Result == SNOOP_LOOKUP_MCAST_FWD_TBL)
                    {

                        /* It is a Mcast Data Pkt & bridge should fwd the pkt 
                         * based on the Multicast Forwarding Table 
                         * If lookup fails, mcast data will be broadcast by this 
                         * routine itself
                         */

                        /* This routine will be replaced by bridge's Mcast 
                         * Data processing routine while merging
                         */

                        /* Buffer will be released by this function */
                        SnoopProcessMcastDataPkt (pMsg, msgAttr);
                    }
                    else if (u2Result == SNOOP_FORWARD_ALL)
                    {

                        /* IGMP router control pkts / report / leave message */
                        TpBroadcastFrame (pMsg, msgAttr, TRUE);
                    }
                    else if (u2Result == SNOOP_NO_FORWARD)
                    {
                        /* release the buffer  */
                        STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);
                    }
                    else if (u2Result == SNOOP_FORWARD_SPECIFIC)
                    {

                        /* Buffer will be released by this function */
                        TpSendMcastFrameOverSpecificPorts (pMsg, msgAttr,
                                                           PortList);
                    }            /* forward specific */
                }
                else
                {                /* IGMP SNOOPING disabled */
#ifndef NPAPI_WANTED
                    /* Packet is not forwarded by H/W */
                    TpBroadcastFrame (pMsg, msgAttr, TRUE);
#else
                    STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);
                    return;
#endif
                }
#else /* IGS_WANTED */
#ifndef NPAPI_WANTED
                /* Packet is not forwarded by H/W */
                TpBroadcastFrame (pMsg, msgAttr, TRUE);
#else
                STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);
                return;
#endif
#endif /* IGS_WANTED */

            }
        }
        else
        {

            /**************************************************************
             ***Destination address is not a Multicast/Broadcast Address***
             *************************************************************/

            if ((u4FdbidOut = BdGetPortFromDest (&msgAttr->destAddr)) ==
                (UINT4) NO_FDB_EXIST)
            {
                /*******************************************************
                 *** Destination address is not cached in FDB        ***
                 *******************************************************/
#ifndef NPAPI_WANTED
                /* Packet is not forwarded by H/W */
                BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                         BRG_MOD_NAME,
                         "Destination address not cached in FDB Tbl, "
                         "so pkt is broadcasted\n");

                TpBroadcastFrame (pMsg, msgAttr, FALSE);
#else
                STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);
#endif

                return;

            }
            else
            {

                /***************************************************
                 *** Destination address cached in FDB           ***
                 ***************************************************/
#ifdef  NPAPI_WANTED
                /* find the port number where the destination host exist */
                FsBrgFindFdbMacEntry (msgAttr->destAddr, &u4PortNo);
                /* Unicast packet packet is forwarded by H/W */
                STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);
                return;
#else

                u4PortNo = GET_LOGICAL_PORT_FROM_FDBID (u4FdbidOut);

                /*****************************************************
                 *** Filter frames if destination address is cached***
                 *** for the port in which the frame was received. ***
                 *** Check whether the Frame can be forwarded thru ***
                 *** this port.                                    ***
                 *****************************************************/

                if ((u4PortNo != u1InputPort) &&
                    (TpIsPortOkForForwarding (u4PortNo, msgAttr,
                                              FALSE) == TRUE))
                {

                    BRIDGE_GET_SYS_TIME (&u4CurSysTime);

                    /* discard the frame if the tansit delay is more than
                     * the Maximum defined bridge  transit delay
                     * Send Out the packet otherwise
                     */
                    if ((u4CurSysTime - msgAttr->u4TimeStamp) >
                        BRG_TRANSIT_DELAY_TICKS)
                    {

                        /* discard the frame */
                        base.portEntry[u4PortNo].u4NoTransitDelayDiscard++;

                        STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);

                        BRG_TRC (BRG_TRC_FLAG,
                                 BRG_DATA_PATH_TRC | BRG_BUFFER_TRC |
                                 BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                                 "Unicast fwding, relxd orginal buffer "
                                 "because of transit delay  \n  ");
                    }

                    /*******************************************************
                     *** Frame is Ready to forward at the Logical port   ***
                     *** Level.                                          ***
                     *******************************************************/

#ifdef SYS_HC_WANTED
                    BRG_INCR_HC_OR_TP_PORT_OUT_FRAMES (u4PortNo);
#else /* SYS_HC_WANTED */
                    tpInfo.portEntry[u4PortNo].u4TpOut++;
#endif /* SYS_HC_WANTED */

                    u4DataLength = CRU_BUF_Get_ChainValidByteCount (pMsg);

                    /* BCPCHANGES - 17/10 - start */
                    switch (base.portEntry[u4PortNo].portId.u1_InterfaceType)
                    {
                        case CFA_ENET:
                            BRG_TRC_ARG1 (BRG_TRC_FLAG,
                                          BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                                          BRG_MOD_NAME,
                                          "Data Frame given to CFA "
                                          " - Ethernet "
                                          "interface %d\n", u4PortNo);

                            BridgeHandleOutFrame (pMsg, (UINT2) u4PortNo,
                                                  u4DataLength,
                                                  0, CFA_ENCAP_NONE);

                            break;

                        default:

                            /* Counter incremented for invalid interface type  */
                            BRG_INCR_PORT_FILTER_IN_DISCARDS (u1InputPort);
                            STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);
                            BRG_TRC (BRG_TRC_FLAG,
                                     BRG_DATA_PATH_TRC | BRG_BUFFER_TRC |
                                     BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                                     "Unicast fwding, relxd orginal buffer "
                                     "for invalid interface Type \n  ");
                            break;
                    }
                    /*End - 17/10 */
                }
                else
                {

                    /*******************************************************
                     *** Learned Port is Not OK for forwarding this frame***
                     *** Release Buffer                                  ***
                     *******************************************************/

                    STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);
                }
#endif
            }
        }
    }
    else
    {

        /***************************************************
         *** The Received Port/Source Address is not OK  ***
         ***************************************************/
        /* Counter incremented for port or Source Address not Ok */
        BRG_INCR_PORT_FILTER_IN_DISCARDS (u1InputPort);
        STAP_BUF_RELEASE_CHAIN (pMsg, FALSE);
    }
}

#if (defined IGS_WANTED || defined MLDS_WANTED)
/**********************************************************************
 * Functionality:-
 *    This routine duplicates the passed buffer & sends out over the
 *    list of port in the PortList. 
 *    Finally releases the original buffer
 *
 * Formal Parameters:-
 *    1.pMsg    : the data buffer.
 *    2.msgAttr : this specifies all the attributes (i.e size, 
 *                 destination addr, source address, protocol etc) of 
 *                 the received msg.
 *    3.PortList: List of ports over which pkt is to be sent out
 *
 * Global Variables Used:-
 *    None
 *
 * Return Values:-
 *    None.
 *********************************************************************/

VOID
TpSendMcastFrameOverSpecificPorts (tCRU_BUF_CHAIN_HEADER * pBuf,
                                   tMSG_ATTR * pMsgAttr, tPortList PortList)
{
    UINT4               u4FrameSize;
    UINT2               u2ByteInd;
    UINT2               u2BitIndex;
    UINT2               u2Port;
    UINT1               u1PortFlag;
    tCRU_BUF_CHAIN_HEADER *pMsgDum;

    u4FrameSize = BRG_BUF_GET_CHAIN_VALIDBYTECOUNT (pBuf);

    for (u2ByteInd = 0; u2ByteInd < BRG_PORT_LIST_SIZE; u2ByteInd++)
    {

        if (PortList[u2ByteInd] != 0)
        {

            u1PortFlag = SNOOP_BIT8;

            for (u2BitIndex = 0; u2BitIndex < BRG_PORTS_PER_BYTE; u2BitIndex++)
            {

                if ((PortList[u2ByteInd] & u1PortFlag) != 0)
                {

                    u2Port =
                        (UINT2) ((u2ByteInd * BRG_PORTS_PER_BYTE) + u2BitIndex +
                                 1);

                    if (TpIsPortOkForForwarding (u2Port,
                                                 pMsgAttr, FALSE) == FALSE)
                    {
                        continue;
                    }

                    /* Duplicate the buffer to handle out the frame */
                    pMsgDum
                        =
                        (tCRU_BUF_CHAIN_HEADER *)
                        CRU_BUF_Duplicate_BufChain (pBuf);

                    if (pMsgDum != NULL)
                    {

                        BRG_TRC (BRG_TRC_FLAG,
                                 BRG_DATA_PATH_TRC | BRG_BUFFER_TRC |
                                 BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                                 "Pkt Duplicated & sent out!");

                        BridgeForwardMcastFrame (pMsgDum, u4FrameSize, u2Port,
                                                 pMsgAttr->u4TimeStamp);
                    }
                    else
                    {
                        BRG_TRC (BRG_TRC_FLAG,
                                 BRG_DATA_PATH_TRC | BRG_BUFFER_TRC |
                                 BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                                 "Duplication of Mcast Bufer Fails!");
                    }

                }

                u1PortFlag = (UINT1) (u1PortFlag >> 1);
            }
        }
    }
    /* release the buffer  */
    STAP_BUF_RELEASE_CHAIN (pBuf, FALSE);
}
#endif /* IGS_WANTED */

/******************************************************************/
/*
 * Functionality:-
 *      This routine broadcasts the frame to all the valid ports 
 *      except the port from which it is recieved.
 *
 * Formal Parameters:-
 *      1.pMsg : data buffer.
 *      2.u1InputPort:The received port.
 *      3.msgAttr:
 *
 * Global Variables Used:-
 *      base and tpInfo.
 *
 * Return Values:-
 *      None.
 */

void
TpBroadcastFrame (tCRU_BUF_CHAIN_HEADER * pMsg, tMSG_ATTR * msgAttr,
                  tBOOLEAN bMulticast)
{                                /* (K) Indicating This procedure */
    UINT4               u4Port;    /* is called as a result of multicast */
    UINT1               u1InputPort;
    UINT4               u4DataLength;
    tCRU_BUF_CHAIN_HEADER *pMsgDum = NULL;

    u1InputPort = (UINT1) msgAttr->u2LogicalPortNo;
    for (u4Port = (UINT4) gu2BrgStartPortInd;
         u4Port != BRG_INVALID_PORT_INDEX;
         u4Port = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4Port))
    {

      /************************************************************
       *** Filter frames from flooding in to the port from which***
       *** the frame was received and check whether this port   ***
       *** is OK for transmitting this frame.                   ***
       ************************************************************/

        if ((u4Port != u1InputPort) &&
            (TpIsPortOkForForwarding (u4Port, msgAttr, TRUE)))
        {

         /**********************************************************
          *** If Destination address is Multicast Address then   ***
          *** Check whether the address is valid for this port   ***
          **********************************************************/

            if (!BM_IS_MAC_MULTICAST (msgAttr->destAddr[0])
                || (IS_VALID_MULTICAST_ADDR (u4Port, msgAttr->destAddr)))
            {
                if (bMulticast == TRUE)
                {
                    IF_INCR_NO_MCAST_FRAMES_TRXED (u4Port);
                }
                else
                {
                    IF_INCR_NO_BCAST_FRAMES_TRXED (u4Port);
                }

            /*******************************************************
             *** This Port is Not a Group                        ***
             *******************************************************/

#ifdef SYS_HC_WANTED
                BRG_INCR_HC_OR_TP_PORT_OUT_FRAMES (u4Port);
#else /* SYS_HC_WANTED */
                tpInfo.portEntry[u4Port].u4TpOut++;
#endif /* SYS_HC_WANTED */

                if ((pMsgDum =
                     (tCRU_BUF_CHAIN_HEADER *)
                     CRU_BUF_Duplicate_BufChain (pMsg)) != NULL)
                {
                    pMsgDum->ModuleData = pMsg->ModuleData;
                    u4DataLength = CRU_BUF_Get_ChainValidByteCount (pMsgDum);
                    /* BCPCHANGES - 17/10 - start */
                    switch (base.portEntry[u4Port].portId.u1_InterfaceType)
                    {
                        case CFA_ENET:
                            /* Changed the protocol from 0 to CFA_PROT_BPDU */
                            BRG_TRC_ARG1 (BRG_TRC_FLAG,
                                          BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                                          BRG_MOD_NAME,
                                          "Broadcasting Data"
                                          "Frame given to CFA "
                                          "for Tx in Ethernet interface "
                                          "%d\n", u4Port);

                            BridgeHandleOutFrame (pMsgDum, (UINT2) u4Port,
                                                  u4DataLength,
                                                  CFA_PROT_BPDU,
                                                  CFA_ENCAP_NONE);

                            break;

                        default:
                            BRG_TRC (BRG_TRC_FLAG,
                                     BRG_DATA_PATH_TRC | BRG_BUFFER_TRC |
                                     BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                                     "In Broadcasting Data Frame, "
                                     "Releasing the Buffer "
                                     " - Invalid Interface type\n");

                            /* Counter incremented for invalid interface type */
                            BRG_INCR_PORT_FILTER_IN_DISCARDS (u1InputPort);
                            CRU_BUF_Release_MsgBufChain (pMsgDum, FALSE);

                            break;
                    }
/*End - 17/10 */
                }
            }
            else
            {

                if (BdIsMcastAddr (base.portEntry[u4Port].u1MlistNo,
                                   &(msgAttr->destAddr)) == FALSE)
                {

                    BRG_INCR_PORT_MCAST_DISCARDS (u4Port);
                }
            }
        }
    }
    BRG_TRC (BRG_TRC_FLAG,
             BRG_DATA_PATH_TRC | BRG_BUFFER_TRC | BRG_ALL_FAILURE_TRC,
             BRG_MOD_NAME,
             "In Broadcasting Data Frame, rlxd Buffer "
             " after giving pkt to CFA module \n  ");
    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);

}

/******************************************************************/
/*
 * Functionality:-
 *     This routine check whether a frame can be received or transmitted
 *     Thru a particular port.
 *     If bBroadcast is TRUE then the broadcast status of the port is 
 *     also  checked otherwise not checked
 *
 * Formal Parameters:-
 *     1. u4Port :The port which the validation is required.
 *     2. msgAttr:Attribute about the message.
 *     3. bBroadcast:This boolean value indicates whether the calling 
 *                    procedure does this validation during broadcasting
 *                    of the frame or for tranmission to the already 
 *                    learned port.
 *
 * Global Variables Used:-
 *     base and tpInfo.
 *
 * Return Values:-
 *     TRUE : If the port is OK for tranmitting this message .
 *     FALSE: otherwise.
 */

tBOOLEAN
TpIsPortOkForForwarding (UINT4 u4Port, tMSG_ATTR * msgAttr, tBOOLEAN bBroadcast)
{
    /*************************************************************
     *** Checks whether the Port is OK for transmitting this   ***
     *** Frame. This checking is done at the logical port level***
     *************************************************************/

    if (IS_BRGIF_OPERADMIN_UP (u4Port) &&
        ((bBroadcast == TRUE &&
          base.portEntry[u4Port].u1BroadcastStatus == BROADCAST_UP) ||
         (bBroadcast == FALSE)))
    {

        if ((BdIsfiltered (base.portEntry[u4Port].u1FilterNo,
                           &msgAttr->destAddr, &msgAttr->srcAddr) != TRUE))
        {

            /*************************************************************
             *** The Frame is not filtered by the filter list assigned ***
             *** for this Port                                         ***
             *************************************************************/
            if (!(msgAttr->u2Protocol
                  & tpInfo.portEntry[u4Port].u2ProtocolFilterMask))
            {

                /******************************************************
                 *** This Protocol is Not filtered by this Port     ***
                 ******************************************************/

                /******************************************************
                 *** If RSTP/MSTP status Is UP check whether the Port is ***
                 *** in Forwarding State.                           ***
                 ******************************************************/

                /* If RSTP/MSTP is not present consider the port state
                 * as always FORWARDING.
                 */
                if (L2IwfGetInstPortState (RST_DEFAULT_INSTANCE,
                                           (UINT2) u4Port)
                    == (UINT1) AST_PORT_STATE_FORWARDING)
                {
                    if ((base.portEntry[u4Port].portId.u1_InterfaceType
                         == CRU_ENET_PHYS_INTERFACE_TYPE) ||
                        (base.portEntry[u4Port].portId.u1_InterfaceType ==
                         CFA_LAGG))
                    {
                        if (tpInfo.portEntry[u4Port].u2Maxinfo
                            >= msgAttr->u2Length)
                        {
                            return TRUE;
                        }
                    }
                    else
                    {
                        if (base.u1CrcStatus == BRG_WITH_CRC)
                        {
                            if (tpInfo.portEntry[u4Port].u2Maxinfo
                                >= (msgAttr->u2Length
                                    + CRU_ENET_HEADER_SIZE + 4))
                            {
                                return TRUE;
                            }
                        }
                        else
                        {
                            if (tpInfo.portEntry[u4Port].u2Maxinfo
                                >= (msgAttr->u2Length + CRU_ENET_HEADER_SIZE))
                            {
                                return TRUE;
                            }
                        }
                    }
                    base.portEntry[u4Port].u4NoMtuExceedDiscard++;
                    BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC, BRG_MOD_NAME,
                             "Data frame filtered for MTU exceeded  \n ");
                }
            }
            else
            {
                BRG_INCR_PORT_PROTOCOL_FILTER_DISCARDS (u4Port);
                BRG_INCR_PORT_FILTER_IN_DISCARDS (u4Port);
            }
        }
        else
        {
            BRG_INCR_PORT_FILTER_IN_DISCARDS (u4Port);
        }
    }
    return FALSE;
}

/*************************** END OF FILE(tptxdt.c) ***********************/
