/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mgfilt.c,v 1.2 2007/02/01 14:44:36 iss Exp $
 *
 * Description:This file contains the SNMP low level          
 *             Routines for dot1dFilter Group          
 *
 *******************************************************************/
#include "brginc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : SnMakeKey                                  */
/*                                                                           */
/*    Description               : This Routine creates a key combining       */
/*                                filternumber,source macaddr,Destination    */
/*                                macaddr.This is used for comparing the     */
/*                                indices of filter table.                   */
/*                                                                           */
/*    Input(s)                  : key                                        */
/*                                FilterNum                                  */
/*                                srcAddr                                    */
/*                                destAddr                                   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
SnMakeKey (UINT1 *Key, UINT1 u1FilterNum, tMacAddr srcAddr, tMacAddr destAddr)
{

    Key[0] = u1FilterNum;
    BRG_MEM_CPY (Key + 1, srcAddr, ETHERNET_ADDR_SIZE);
    BRG_MEM_CPY (Key + 7, destAddr, ETHERNET_ADDR_SIZE);
}

/*************************** END OF FILE(mgfilt.c) ***************************/
