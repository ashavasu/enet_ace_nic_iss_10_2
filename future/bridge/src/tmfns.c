/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tmfns.c,v 1.2 2007/02/01 14:44:36 iss Exp $
 *
 * Description:This file contains routines for timer          
 *             management.                             
 *
 *******************************************************************/
#include "brginc.h"

/******************************************************************/
/*
 * Timer Controlling Routines
 */
/******************************************************************/

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : TmStartTimer                               */
/*                                                                           */
/*    Description               : This function starts the timer             */
/*                                                                           */
/*    Input(s)                  : pTmr                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
void
TmStartTimer (tTIMER * pTmr)
{
    pTmr->bActive = TRUE;
    pTmr->u4TickCount = 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : TmStopTimer                                */
/*                                                                           */
/*    Description               : This function stops the  timer             */
/*                                                                           */
/*    Input(s)                  : pTmr                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/

/******************************************************************/

void
TmStopTimer (tTIMER * pTmr)
{
    pTmr->bActive = FALSE;

}

/******************************************************************/
/*
 * Functionality         : Checks all FDB entries for the ageout.
 *                         Remove the ageout entries from the FDB.
 * 
 * Formal Parameters     : None.
 * 
 * Global Variables Used : bdFdbEntries ( List of FDB entries)
 * 
 * Return Values         : None.
 *
 */

void
TmFdbTimerTick ()
{
    tFORWARDING_ENTRY  *pFwdEntry;
    UINT4               u4Hindex;
    UINT4               u4AgeingTime = 0;

    u4AgeingTime = tpInfo.u4FdbAgeingTime;

    for (u4Hindex = 0; u4Hindex < HASH_LIST_SIZE; u4Hindex++)
    {
        if (BRG_FDB_HASH_TBL != NULL)
        {
            TMO_HASH_Scan_Bucket (pSHtab, u4Hindex, pFwdEntry,
                                  tFORWARDING_ENTRY *)
            {
                if (IS_TIMER_EXPIRED (pFwdEntry->ageout, 1, u4AgeingTime))
                {
                    REMOVE_FDB_ENTRY (pFwdEntry);
#ifdef NPAPI_WANTED
                    /*
                     * 1. If FDB is maintained in the S/W alone, following should 
                     * be a dummy call
                     * 2. If FDB is maintained in H/W alone, this thread will not come
                     * 3. If a copy of FDB is maintained in S/W also, which is 
                     * triggered from H/W, timer should be run either in S/W or in H/W
                     * This should be taken care in the below function
                     */
                    FsBrgDelMacEntry (GET_LOGICAL_PORT_FROM_FDBID
                                      (pFwdEntry->u4Fdbid),
                                      pFwdEntry->destAddr);
#endif
                    BdFreeFdbEntry (pFwdEntry);
                    pFwdEntry = NULL;

                }
            }
        }
    }
}

/******************************************************************/
/*
 * Functionality         : Checks all StaticTable entries for the ageout.
 *                         Remove the ageout entries from the StaticTable.
 *
 * Formal Parameters     : None.
 *
 * Global Variables Used : bd_bd_staticTable ( List of StaticTable entries)
 *
 * Return Values         : None.
 *
 */

void
TmStatictableTimerTick (void)
{
    tSTATICTABLE_ENTRY *pPtr;
    UINT4               u4AgeingTime;
    INT4                i4StaticPort;

    u4AgeingTime = STATIC_TABLE_ENTRY_AGEOUT_TIME;

    for (i4StaticPort = 0; i4StaticPort <= BRG_MAX_PHY_PLUS_LOG_PORTS;
         i4StaticPort++)
    {

        if (TMO_SLL_Count (&bdStaticTable[i4StaticPort].entry) != 0)
        {
            TMO_SLL_Scan (&bdStaticTable[i4StaticPort].entry, pPtr,
                          tSTATICTABLE_ENTRY *)
            {
                if (pPtr->u1StaticStatus == STATIC_DELETEONTIMEOUT)
                {
                    if (IS_TIMER_EXPIRED (pPtr->timeout, 1, u4AgeingTime))
                    {
                        BdRemoveStaticEntryFromList (i4StaticPort, pPtr);
                    }
                }
            }
        }
    }

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BrgTmrExpiryHandler                        */
/*                                                                           */
/*    Description               : This function handles the timer expiry     */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRG_SUCCESS On Success.                    */
/*                                BRG_FAILURE On Failure.                    */
/*                                                                           */
/*****************************************************************************/

UINT4
BrgTmrExpiryHandler (VOID)
{
    UINT4               u4Val = BRG_SUCCESS;
    tbrgtmr            *pExpiredTmr = NULL;

    while ((pExpiredTmr = (tbrgtmr *)
            BRG_GET_NEXT_EXPIRED_TIMER (tlistid)) != NULL)
    {
        bridgeTimer++;
        TmFdbTimerTick ();
        TmStatictableTimerTick ();

        u4Val = BRIDGE_START_TMR (tlistid, pExpiredTmr, BRG_TIME_DURATION);
        if (u4Val == BRG_FAILURE)
        {
            BRG_TRC (BRG_TRC_FLAG, BRG_ALL_FAILURE_TRC |
                     BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
                     " Restarting Timer Failed... \n ");
            return BRG_FAILURE;
        }
    }
    return BRG_SUCCESS;
}

/*************************** END OF FILE(tmfns.c) ***********************/
