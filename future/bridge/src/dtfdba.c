/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtfdba.c,v 1.3 2007/02/01 14:44:36 iss Exp $
 *
 * Description:This file contains middle level routines       
 *             for the FDB management.                    
 *
 *******************************************************************/
#include "brginc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdGetNewFdbEntry                           */
/*                                                                           */
/*    Description               : This routine returns a free FDB Entry, if  */
/*                                one available else return NULL.            */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : ForwardingEntry : if available             */
/*                                NULL            : if not available         */
/*                                                                           */
/*****************************************************************************/

tFORWARDING_ENTRY  *
BdGetNewFdbEntry (void)
{
    tFORWARDING_ENTRY  *pNewEntry;
    UINT4               u4Retval;
    if ((u4Retval = ALLOC_FWD_ENTRY (fwdPoolId, (UINT1 *) &pNewEntry))
        == BRG_MEM_SUCCESS)
    {
        BRG_TRC (BRG_TRC_FLAG,
                 BRG_MGMT_TRC | BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 "Memory allocation success, "
                 "in allocation of entry in the forwarding table\n ");
        return ((tFORWARDING_ENTRY *) pNewEntry);
    }

    BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
             BRG_MOD_NAME, " No Free FDB Entry \n");
    return (NULL);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdFreeFdbEntry                             */
/*                                                                           */
/*    Description               : This routine makes the specified FDB Entry */
/*                                free.                                      */
/*                                                                           */
/*    Input(s)                  : pFdbEntry - Pointer to the FDB Entry.      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK     : On Success                 */
/*                                BRIDGE_NOT_OK : On Failure                 */
/*                                                                           */
/*****************************************************************************/

INT4
BdFreeFdbEntry (tFORWARDING_ENTRY * pFdbEntry)
{
    UINT4               u4RetVal;

    u4RetVal = FREE_FWD_ENTRY (fwdPoolId, pFdbEntry);
    if (u4RetVal == BRG_MEM_FAILURE)
    {
        return BRIDGE_NOT_OK;
    }
    return BRIDGE_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : HashFn                                     */
/*                                                                           */
/*    Description               : Accessory Routines for Hash Library.       */
/*                                                                           */
/*    Input(s)                  : pKey - MacAddress .                        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : HashIndex.                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
HashFn (tMacAddr * pKey)
{
    return (pKey[0][5] % HASH_LIST_SIZE);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : InsertFn                                   */
/*                                                                           */
/*    Description               : Inserts the given value in to the Node.    */
/*                                                                           */
/*    Input(s)                  : pNode    - FDB Entry Node.                 */
/*                                pu1Param - Value to be Inserted.           */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : TRUE  : On Success                         */
/*                                FALSE : On Failure.                        */
/*                                                                           */
/*****************************************************************************/
UINT4
InsertFn (tTMO_HASH_NODE * pNode, UINT1 *pU1Param)
{
    tMacAddr           *pMac;
    tFORWARDING_ENTRY  *pFdb;
    INT2                i2I;

    pMac = (tMacAddr *) pU1Param;
    pFdb = (tFORWARDING_ENTRY *) pNode;
    i2I = (INT2) BuCompareMacaddr ((UINT1 *) *pMac, pFdb->destAddr);

    if (i2I == BRG_EQUAL || i2I == LESSER)
        return (UINT4) TRUE;
    else
        return (UINT4) FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : DeleteNodeFn                               */
/*                                                                           */
/*    Description               : Deletes the given node from FDB Entry.     */
/*                                                                           */
/*    Input(s)                  : pNode    - FDB Entry Node.                 */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
void
DeleteNodeFn (tTMO_HASH_NODE * pNode)
{
    BdFreeFdbEntry ((tFORWARDING_ENTRY *) pNode);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : HashSearchNode                             */
/*                                                                           */
/*    Description               : This function search the hash table for    */
/*                                the given value.                           */
/*                                                                           */
/*    Input(s)                  : pHtab    - Pointer to the Hash Table.      */
/*                                pMacaddr - Macaddress to be searched.      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : ForwardingEntry - If Entry Found.          */
/*                                NULL            - If Entry not Found.      */
/*                                                                           */
/*****************************************************************************/

tFORWARDING_ENTRY  *
HashSearchNode (tTMO_HASH_TABLE * pHtab, tMacAddr * pMacaddr)
{
    tFORWARDING_ENTRY  *ptr;
    UINT4               u4Hindex;
    tMacAddr            fMacaddr;

    u4Hindex = HashFn (pMacaddr);
    if (BRG_FDB_HASH_TBL != NULL)
    {
        TMO_HASH_Scan_Bucket (pHtab, u4Hindex, ptr, tFORWARDING_ENTRY *)
        {
            BRG_MEM_CPY (fMacaddr, ptr->destAddr, ETHERNET_ADDR_SIZE);
            if (BuCompareMacaddr (fMacaddr, (UINT1 *) *pMacaddr) == BRG_EQUAL)
            {
                return ptr;
            }
        }
    }
    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdFdbMemoryInit                            */
/*                                                                           */
/*    Description               : This function initialises the Forwarding   */
/*                                table.                                     */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK : On Success.                    */
/*                                BRIDGE_NOT_OK : On Failure                 */
/*                                                                           */
/*****************************************************************************/

INT4
BdFdbMemoryInit (void)
{
    UINT4               u4RetVal;

    if ((u4RetVal = STAP_CREATE_MEM_POOL (sizeof (tFORWARDING_ENTRY),
                                          MAX_FDB_ENTRIES,
                                          MEM_DEFAULT_MEMORY_TYPE,
                                          &fwdPoolId)) == BRG_MEM_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to create memory for forwarding Table entries  \n");
        return BRIDGE_NOT_OK;
    }
    return BRIDGE_OK;
}

/*************************** END OF FILE(dtfdba.c) **********************/
