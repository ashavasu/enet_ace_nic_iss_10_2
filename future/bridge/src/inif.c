/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: inif.c,v 1.14 2013/12/16 15:26:28 siva Exp $
 *
 * Description:This file contains initialization routine      
 *             and CFA interface routines.                 
 *
 *******************************************************************/
#include "brginc.h"
#include "brgglob.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BridgeFwdReceiveCircuitOperStatus          */
/*                                                                           */
/*    Description               : Checks the Operation status of the given   */
/*                                Interface Id.                              */
/*                                                                           */
/*    Input(s)                  : ifaceId   - InterfaceId                    */
/*                                u1Status  - Status                         */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : Return Status else                         */
/*                                Return 0.                                  */
/*                                                                           */
/*****************************************************************************/

INT4
BridgeFwdReceiveCircuitOperStatus (tTMO_INTERFACE ifaceId, UINT1 u1Status)
{
    UINT4               u4PortNo = 0;

    if ((u4PortNo = GetPortFromIfaceId (&ifaceId)) != 0xffff)
    {

        return InSetLogicalPortOperStatus (ifaceId, u1Status);
    }
    return 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : InSetLogicalPortOperStatus                 */
/*                                                                           */
/*    Description               : Routine sets the Operational status of     */
/*                                the Port.                                  */
/*                                                                           */
/*    Input(s)                  : ifaceId   - InterfaceId                    */
/*                                u1Status  - Status to be set               */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : tpInfo                                     */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK      : On Success.                      */
/*                                BRIDGE_NOT_OK  : On Failure.                      */
/*                                                                           */
/*****************************************************************************/

INT4
InSetLogicalPortOperStatus (tTMO_INTERFACE ifaceId, UINT1 u1Status)
{
    UINT4               u4PortNo = 0;

    if (ifaceId.u2_SubReferenceNum != 0xffff)
    {
        if ((u4PortNo = GetPortFromIfaceId (&ifaceId)) == 0xffff)
        {
            return BRIDGE_NOT_OK;
        }

        if (base.portEntry[u4PortNo].u1AdminStatus == (UINT1) UP)
        {
            if (base.portEntry[u4PortNo].u1OperStatus != u1Status)
            {
#ifdef RSTP_WANTED
                (VOID) AstUpdatePortStatus ((UINT2) u4PortNo, u1Status);
#endif /* RSTP_WANTED */
                base.portEntry[u4PortNo].u1OperStatus = u1Status;
            }
        }
    }
    else
    {
        for (u4PortNo = 1; u4PortNo <= BRG_MAX_PHY_PLUS_LOG_PORTS; u4PortNo++)
        {

            if ((base.portEntry[u4PortNo].u1AdminStatus == (UINT1) UP)
                && (base.portEntry[u4PortNo].portId.u1_InterfaceNum
                    == ifaceId.u1_InterfaceNum)
                && (base.portEntry[u4PortNo].portId.u1_InterfaceType
                    == ifaceId.u1_InterfaceType))
            {

                if (base.portEntry[u4PortNo].u1OperStatus != u1Status)
                {
#ifdef RSTP_WANTED
                    (VOID) AstUpdatePortStatus ((UINT2) u4PortNo, u1Status);
#endif /* RSTP_WANTED */
                    base.portEntry[u4PortNo].u1OperStatus = u1Status;
                }
            }
        }
    }
    return BRIDGE_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : InEnablePort                               */
/*                                                                           */
/*    Description               : Routine enables the Adminstration status  */
/*                                of the Port.                               */
/*                                                                           */
/*    Input(s)                  : ifaceId   - InterfaceId                    */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : base, grp_tab .                            */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK      : On Success.                      */
/*                                BRIDGE_NOT_OK  : On Failure.                      */
/*                                                                           */
/*****************************************************************************/

INT4
InEnablePort (tTMO_INTERFACE ifaceId)
{

    UINT4               u4PortNo = 0xffff;

    if (ifaceId.u2_SubReferenceNum != (UINT2) 0xffff)
    {
        /* For a valid interface */
        u4PortNo = GetPortFromIfaceId (&ifaceId);
        if (u4PortNo == (UINT4) 0xffff)
        {
            /* First time creation of interfaces */

            u4PortNo = ifaceId.u1_InterfaceNum;
            base.portEntry[u4PortNo].portId = ifaceId;
            InResetBasicVars (u4PortNo);
            InInitTpPort (u4PortNo);
        }
        /* port DOWN or BRIDGE_INVALID */
        if (base.portEntry[u4PortNo].u1AdminStatus == (UINT1) BRIDGE_INVALID)
        {
            /*
             * This below procedure checks only for all valid entries   
             * hence the admin status is set before this.            
             */
            base.portEntry[u4PortNo].u1AdminStatus = (UINT1) UP;
            BdAssignAllUnassignedFdb (&ifaceId);
        }
        /* Bridge port will be created on the PPP interfaces only with the 
         * BCP comes up. So Oper status can be madeup for the Bridge interface
         * with the ifType is PPP also */
        /* Added the setting of the operational status as UP */

        base.portEntry[u4PortNo].u1AdminStatus = (UINT1) UP;

        /* Whether LA is enabled or not operstatus is made up ,
         * oper up is indicated through operstatus indication
         */
        base.portEntry[u4PortNo].u1OperStatus = (UINT1) DOWN;
        InInitMaxinfo (ifaceId);
    }
    else
        return BRIDGE_NOT_OK;    /* Invalid circuit */

    return BRIDGE_OK;
}

/******************************************************************/
/*
 * Functionality:-
 *     This routine invalidate the Administration status of the port
 *     This routine is accessory routine for 
 *     BRIDGE SNMP Low level routine
 *
 * Formal Parameters:-
 *     ifaceId : The interface-id.
 *
 * Global Variables Used:-
 *     base, grp_tab.
 *
 * Return Values:-
 *     1. BRIDGE_OK    : On success.
 *     2. BRIDGE_NOT_OK: Otherwise.
 */

INT4
InInvalidPort (tTMO_INTERFACE ifaceId)
{

    UINT4               u4PortNo = 0;
    UINT4               u4Count = 0;

    /* Check whether the cirucit is valid */
    if (ifaceId.u2_SubReferenceNum != 0xffff)
    {
        for (u4Count = 1; u4Count <= (UINT4) BRG_MAX_PHY_PLUS_LOG_PORTS;
             u4Count++)
        {
            if ((base.portEntry[u4Count].portId.u1_InterfaceType
                 == ifaceId.u1_InterfaceType)
                &&
                (base.portEntry[u4Count].portId.u1_InterfaceNum
                 == ifaceId.u1_InterfaceNum)
                &&
                (base.portEntry[u4Count].portId.u2_SubReferenceNum
                 == ifaceId.u2_SubReferenceNum))
            {

                u4PortNo = u4Count;
                break;
            }
            if (u4Count == (UINT4) BRG_MAX_PHY_PLUS_LOG_PORTS)
            {
                u4PortNo = (UINT4) 0xffff;
                break;
            }
        }

        /* Valid Circuit number */
        if (u4PortNo != (UINT4) 0xffff)
        {
            /* Check whether the port status is already invalid */
            if (base.portEntry[u4PortNo].u1AdminStatus ==
                (UINT1) BRIDGE_INVALID)
                return BRIDGE_NOT_OK;

            if (u4PortNo > (UINT4) BRG_MAX_PHY_PLUS_LOG_PORTS)
            {
                return BRIDGE_NOT_OK;
            }

            base.portEntry[u4PortNo].portId = ifaceId;

        }
        else
        {                        /* port number is not a valid number */
            return BRIDGE_NOT_OK;
        }
        /*
         *Since this port is invalidated the FDB entries that are added
         *Thru management is no more valid, Learned entries are any how
         *will be timed out so they are not explicitly deleted
         */
        BdDeleteStaticEntries (&ifaceId);
        BdAssignAllUnassignedFdb (&ifaceId);
        /* Invalidate the base port admin */
        base.portEntry[u4PortNo].u1AdminStatus = (UINT1) BRIDGE_INVALID;
        return BRIDGE_OK;
    }
    else
        return BRIDGE_NOT_OK;
}

/*****************************************************************************/
/* Function Name      : BridgeInitializeProtocol                             */
/*                                                                           */
/* Description        :                                                      */
/*     This routine is the initialization for the entire bridge              */
/*     operation. Initialize variables with default values and allocates     */
/*     memory for tables.                                                    */
/*                                                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : BRIDGE_SUCCESS or BRIDGE_FAILURE                     */
/*****************************************************************************/
INT4
BridgeInitializeProtocol (VOID)
{
    UINT4               u2PortNo = BRG_MIN_PORT_NO;
    INT4                i4RetSts = BRIDGE_SUCCESS;

    /* Enables the failure trace */
#ifdef TRACE_WANTED
    gBrgGlb.u4BrgTrc = BRG_ALL_FAILURE_TRC;
#else
    gBrgGlb.u4BrgTrc = 0;
#endif

/* Initialize the TB module 
* By default bridge module is initialzied 
* Module status is START */
    bridgeTimer = 0;            /* Global Bridge base timer */

    i4RetSts = InAllocMemoryForTables ();
    if (i4RetSts == BRIDGE_FAILURE)
    {

        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to allocate memory for all tables in FutureTB/STP\n");
        return BRIDGE_FAILURE;
    }
    for (u2PortNo = BRG_MIN_PORT_NO; u2PortNo <= BRG_MAX_PHY_PLUS_LOG_PORTS;
         u2PortNo++)
    {
        FILL (&base.portEntry[u2PortNo].portId, sizeof (tTMO_INTERFACE), 0);
        base.portEntry[u2PortNo].u1AdminStatus = (UINT1) BRIDGE_INVALID;
        BRG_GET_NEXT_PORT_INDEX (u2PortNo) = BRG_INVALID_PORT_INDEX;
    }
    i4RetSts = BridgeModuleInit ();
    if (i4RetSts == BRIDGE_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME, "Failed to Initialize TB MODULE ! \n ");
        return BRIDGE_FAILURE;
    }

    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BridgeTmrTaskInit */
/*                                                                           */
/* Description        :                                                      */
/*     This routine is used initialize the Timer Task for TB module         */
/*      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : BRIDGE_SUCCESS or BRIDGE_FAILURE                     */
/*****************************************************************************/
INT4
BridgeTmrTaskInit (VOID)
{
    UINT4               u4RetVal = 0;

    bridgeTimer = 0;            /* Global Bridge base timer */
    u4RetVal = OSS_SPAWN_TASK (BRIDGE_TMR_TASK_NAME,
                               BRIDGE_TMR_TASK_PRIORITY,
                               OSIX_DEFAULT_STACK_SIZE,
                               BrgTmrTask,
                               0, OSIX_DEFAULT_TASK_MODE, &gBrgTaskId);

    if (u4RetVal == BRG_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME, "Failed to Spawn a Bridge Timer Task \n ");
        return BRIDGE_FAILURE;
    }

    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BridgeModuleRestart                                  */
/*                                                                           */
/* Description        :                                                      */
/*     This routine is the called whenever Transparent bridge module is      */
/*     restarted. This routine creates all ports and updates the port status */
/*     if the port is up.                                                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
BridgeModuleRestart (VOID)
{
    tCfaIfInfo          IfInfo;
    UINT2               u2PortNo = BRG_MIN_PORT_NO;
    UINT4               u4IfSpeed = 0;
    UINT2               u2IfType = 0;
    UINT1               u1PortState;

    for (u2PortNo = BRG_MIN_PORT_NO; u2PortNo <= BRG_MAX_PHY_PLUS_LOG_PORTS;
         u2PortNo++)
    {
        CfaGetIfInfo (u2PortNo, &IfInfo);
        if (IfInfo.u1BridgedIface == CFA_DISABLED)
        {
            continue;
        }
        if (IfInfo.u1IfOperStatus == CFA_IF_UP)
        {
            u2IfType = IfInfo.u1IfType;
            u4IfSpeed = IfInfo.u4IfSpeed;
            /* Create the port and give the indication as UP
             * only for link which is UP */
            BridgeCreatePort (u2PortNo, u2IfType, u4IfSpeed);
            if (IfInfo.u1IfOperStatus == UP)
            {
                /* Get the port oper status from the lower layers */
                if (BrgGetLLPortOperStatus (u2PortNo, &u1PortState)
                    == BRIDGE_SUCCESS)
                {
                    if (u1PortState == CFA_IF_DOWN)
                    {
                        continue;
                    }
                }

                BridgePortOperStatusIndication (u2PortNo, UP);
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : BridgeModuleInit */
/*                                                                           */
/* Description        :                                                      */
/*     This routine is the initialization for the Transparent bridge         */
/*     module. Initialize variables with default values and allocates        */
/*     memory for tables.                                                    */
/*                                                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : BRIDGE_SUCCESS or BRIDGE_FAILURE                     */
/*****************************************************************************/
INT1
BridgeModuleInit (VOID)
{
    tBridgeSystemSize   brgSystemSize;
    INT4                i4RetSts = BRIDGE_SUCCESS;

    /* Initializing the Bridge system sizing information */
    GetBridgeSizingParams (&brgSystemSize);

    MAX_FDB_ENTRIES = brgSystemSize.u4BridgeMaxFdbEntries;
    MAX_FILTER_ENTRIES = brgSystemSize.u4BridgeMaxFilterEntries;

    i4RetSts = InAllocMemoryForTables ();
    if (i4RetSts == BRIDGE_FAILURE)
    {

        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to allocate memory for all tables in FutureTB/STP\n");
        return BRIDGE_FAILURE;
    }

    i4RetSts = BdDbInit ();
    if (i4RetSts == BRIDGE_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 "Failed to initialize Forwarding or "
                 "Filtering or Multi-cast or the Static table \n");
        return BRIDGE_FAILURE;
    }

    InInitTp ();

    /* Reset the global variables of TB */
    BrgResetAllScalarValues ();

    /* BRIDGE_GROUP_ADDRESS  { 0x01, 0x80, 0xc2, 0x00, 0x00, 0x00} */
    brgrpAddr[0] = 0x01;
    brgrpAddr[1] = 0x80;
    brgrpAddr[2] = 0xc2;
    brgrpAddr[3] = 0x00;
    brgrpAddr[4] = 0x00;
    brgrpAddr[5] = 0x00;

    /* Initializing the broadcast address */
    BRG_MEM_SET (bcastAddr, 0xFF, ETHERNET_ADDR_SIZE);

    CfaGetSysMacAddress (base.brgMacAddr);

    BRG_MODULE_STATUS = (UINT1) BRG_DISABLED;
    /* Bridge Module is started */
    BRG_SYSTEM_CONTROL = (UINT1) BRG_START;
    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BridgeModuleShutdown                                 */
/*                                                                           */
/* Description        :                                                      */
/*     This routine is the de-initialization for the Transparent bridge      */
/*     module and release memory for tables.                                 */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : BRIDGE_SUCCESS or BRIDGE_FAILURE                     */
/*****************************************************************************/
INT1
BridgeModuleShutdown (VOID)
{
    UINT2               u2PortNo = BRG_MIN_PORT_NO;
    INT4                i4RetSts = BRIDGE_SUCCESS;

    /* Resetting the base  port interface info */
    for (u2PortNo = gu2BrgStartPortInd;
         u2PortNo != BRG_INVALID_PORT_INDEX;
         u2PortNo = BRG_GET_NEXT_PORT_INDEX (u2PortNo))
    {
        /* Give the indication to bridge/vlan as 
         * delete */
        BridgeDeletePort (u2PortNo);
    }

    gu2BrgStartPortInd = BRG_INVALID_PORT_INDEX;
    gu2BrgLastPortInd = BRG_INVALID_PORT_INDEX;

    for (u2PortNo = BRG_MIN_PORT_NO; u2PortNo <= BRG_MAX_PHY_PLUS_LOG_PORTS;
         u2PortNo++)
    {
        FILL (&base.portEntry[u2PortNo].portId, sizeof (tTMO_INTERFACE), 0);
        base.portEntry[u2PortNo].u1AdminStatus = (UINT1) BRIDGE_INVALID;
        BRG_GET_NEXT_PORT_INDEX (u2PortNo) = BRG_INVALID_PORT_INDEX;
    }

    i4RetSts = BrgDbDeInit ();
    if (i4RetSts == BRIDGE_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 "Failed to initialize Forwarding or "
                 "Filtering or Multi-cast or the Static table \n");
        return BRIDGE_FAILURE;
    }
    InInitTp ();
    /* Reset the global variables of TB */
    BrgResetAllScalarValues ();

    BRG_MODULE_STATUS = (UINT1) BRG_DISABLED;
    /* Bridge Module is started */
    BRG_SYSTEM_CONTROL = (UINT1) BRG_SHUTDOWN;
    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BrgResetAllScalarValues                              */
/*                                                                           */
/* Description        :This routine resets the scalar values for             */
/*      the Transparent bridge                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : base - global data                                   */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None   */
/*****************************************************************************/

VOID
BrgResetAllScalarValues (VOID)
{
    base.u1BridgeStatus = (UINT1) DOWN;
    base.u1CrcStatus = BRG_WITHOUT_CRC;
    gBrgGlb.u4BrgTrc = 0;
    gBrgGlb.u4BrgDbg = 0;

    return;
}

/******************************************************************/
/*
 * Functionality:-
 *     This routine makes the admin status of all ports to DOWN, and 
 *     sends notification to ASTP module to disable all ports.
 *
 * Formal Parameters:-
 *     None.
 *
 * Global Variables Used:-
 *     Base.
 *
 * Return Values:-
 *     BRIDGE_OK     :On success.
 *     BRIDGE_NOT_OK :Otherwise.
 */

INT4
InMakeAllInterfacesDown (VOID)
{
    UINT4               u4PortNo = 0;

    for (u4PortNo = 1; u4PortNo <= (UINT4) BRG_MAX_PHY_PLUS_LOG_PORTS;
         u4PortNo++)
    {
        if (base.portEntry[u4PortNo].u1AdminStatus == (UINT1) UP)
        {
            base.portEntry[u4PortNo].u1AdminStatus = (UINT1) DOWN;
            /* Removed the setting of the interfaces as DOWN */
            /* Give indications to VLAN */
#ifdef VLAN_WANTED
            VlanPortOperInd ((UINT2) u4PortNo, CFA_IF_DOWN);
#endif /* VLAN_WANTED */
        }
    }
#ifdef RSTP_WANTED
    if (AstAllPortDisable () != (INT4) RST_SUCCESS)
    {
        return BRIDGE_NOT_OK;
    }
#endif /* RSTP_WANTED */
    return BRIDGE_OK;
}

/******************************************************************/
/*
 * Functionality:-
 *       performs the initialization for the port.
 *
 * Formal Parameters:-
 *      None.
 *
 * Global Variables Used:-
 *      base.
 *
 * Return Values:-
 *     None.
 */

void
InResetBasicVars (UINT4 u4PortNo)
{

    base.portEntry[u4PortNo].u1FilterNo = (UINT1) 0;
    base.portEntry[u4PortNo].u1MlistNo = (UINT1) 0;
#ifdef NPAPI_WANTED
    /* Resetting the multi-list no */
    FsBrgSetMcGrp2Port (u4PortNo, (INT4) 0);
#endif
    base.portEntry[u4PortNo].u1BroadcastStatus = (UINT1) BROADCAST_UP;
    base.portEntry[u4PortNo].u4NoMtuExceedDiscard = (UINT4) 0;
    base.portEntry[u4PortNo].u4NoBcastFramesTrxed = (UINT4) 0;
    base.portEntry[u4PortNo].u4NoMcastFramesTrxed = (UINT4) 0;
    base.portEntry[u4PortNo].u4NoTransitDelayDiscard = (UINT4) 0;
#ifdef SYS_HC_WANTED
    base.portEntry[u4PortNo].u4IfSpeed = (UINT4) 0;
#endif

}

/******************************************************************/
/*
 * Functionality:-
 *      Returns the status of the BRIDGE. 
 *
 * Formal Parameters:-
 *      None.
 *
 * Global Variables Used:-
 *      base.
 *
 * Return Values:-
 *    status of BRIDGE. 
 */

UINT1
GetBridgeStatus (VOID)
{
    return (base.u1BridgeStatus);
}

UINT1
GetBridgeSystemControl (VOID)
{
    return BRG_SYSTEM_CONTROL;
}

/******************************************************************/
/*
 * Functionality:-
 *      This routine is called from Bridge Interface Layer when
 *      Link Aggregation is disabled
 *      --Creates port entry in bridge
 *
 * Formal Parameters:-
 *      u2IfIndex - Port which is to be created
 *      u2IfType  - Interface Type of Port
 *      u4IfSpeed - Speed of the interface
 *
 * Global Variables Used:-
 *     base.portEntry[u2IfIndex]
 *
 * Return Values:-
 *     BRIDGE_SUCCESS / BRIDGE_FAILURE
 */

INT4
BridgeCreatePort (UINT2 u2IfIndex, UINT2 u2IfType, UINT4 u4IfSpeed)
{
    tTMO_INTERFACE      ifaceId;
    INT4                i4RetValue = 0;

    /* Checking for the valid port number between
     * 1 and maximum number of port
     */
    if ((u2IfIndex < BRG_MIN_PORT_NO)
        || (u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return BRIDGE_FAILURE;
    }
    /* Checking for the validity of the port, determined by
     * the u1AdminStatus.
     * u1AdminStatus - UP or DOWN when the port is valid
     * otherwise it will be invalid
     */
    if (base.portEntry[u2IfIndex].u1AdminStatus != BRIDGE_INVALID)
    {
        return BRIDGE_SUCCESS;
    }
    base.portEntry[u2IfIndex].portId.u1_InterfaceNum = (UINT1) u2IfIndex;
    base.portEntry[u2IfIndex].portId.u1_InterfaceType = (UINT1) u2IfType;
    /* Handled for ethernet and PPP */
    base.portEntry[u2IfIndex].portId.u2_SubReferenceNum = 0;
    /* Added for supporting HC_PORTS */
#ifdef SYS_HC_WANTED
    base.portEntry[u2IfIndex].u4IfSpeed = u4IfSpeed;
#endif

    ifaceId.u1_InterfaceNum = (UINT1) u2IfIndex;
    ifaceId.u2_SubReferenceNum = 0;
    ifaceId.u1_InterfaceType = (UINT1) u2IfType;

    InEnablePort (ifaceId);

    /* Added the verification for IfType */
    if (base.u1BridgeStatus == (UINT1) UP && u2IfType == (UINT2) CFA_ENET)
    {

        i4RetValue = CfaGddConfigPort (u2IfIndex, CFA_ENET_EN_PROMISC, NULL);

        if (i4RetValue == (INT4) BRIDGE_NOT_OK)
        {
            return BRIDGE_FAILURE;
        }
    }

    BrgInsertInIfList (u2IfIndex);

    /* Bridge operstatus is to be down when the port is created.
     * Operation Status up is indicated in Operstatus indication.
     */
#ifndef SYS_HC_WANTED
    UNUSED_PARAM (u4IfSpeed);
#endif

    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BridgeDeletePort                           */
/*                                                                           */
/*    Description               : This function informs the bridge about     */
/*                                deletion of a port so that bridging can    */
/*                                be disabled on that port                   */
/*                                                                           */
/*    Input(s)                  : u2IfIndex   - port Index                   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK      On Success.                 */
/*                                BRIDGE_NOT_OK  On Failure.                 */
/*                                                                           */
/*****************************************************************************/
INT4
BridgeDeletePort (UINT2 u2IfIndex)
{
    tTMO_INTERFACE      ifaceId;

    /* Checking for the valid port number between
     * 1 and maximum number of port
     */
    if ((u2IfIndex < BRG_MIN_PORT_NO)
        || (u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return BRIDGE_NOT_OK;
    }

    /* Check whether the Port is already deleted, if it is already 
     * deleted return as BRIDGE_SUCCESS 
     */
    if (base.portEntry[u2IfIndex].u1AdminStatus == BRIDGE_INVALID)
    {
        return BRIDGE_OK;
    }
    ifaceId.u1_InterfaceNum = (UINT1) u2IfIndex;
    ifaceId.u2_SubReferenceNum = (UINT2) 0;
    ifaceId.u1_InterfaceType =
        base.portEntry[u2IfIndex].portId.u1_InterfaceType;

    base.portEntry[u2IfIndex].u1OperStatus = (UINT1) DOWN;

    InInvalidPort (ifaceId);
/* Not Disabling Promiscuous Mode of the physical Interface 
 * here. Otherwise we cannot receive any packets in Linux Environment.
 */

    BrgDeleteFromIfList (u2IfIndex);

    return BRIDGE_OK;
}

/*****************************************************************************/
/* Function Name      : BridgePortOperStatusIndication                       */
/*                                                                           */
/* Description        : This function passes port operstatus indication  to  */
/*                      -->Directly to Bridge                                */
/*                      This function will be called from CFA when ethernet  */
/*                      interface's oper status changes                      */
/*                                                                           */
/*                      This function will be called from CFA for all type   */
/*                      of interfaces like ENET/ATM/PPP etc.                 */
/*                                                                           */
/* Input(s)           :  u2IfIndex  - Interface index (as far as LA/PNAC and */
/*                                    bridge are considered, interface       */
/*                                    number is same as port number)         */
/*                                                                           */
/*                       u1Status   - Oper status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
BridgePortOperStatusIndication (UINT2 u2IfIndex, UINT1 u1Status)
{
    /* Checking for the valid port number between
     * 1 and maximum number of port
     */
    if ((u2IfIndex < BRG_MIN_PORT_NO)
        || (u2IfIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return;
    }

    if (base.portEntry[u2IfIndex].u1OperStatus == (UINT1) u1Status)
    {
        return;
    }

    base.portEntry[u2IfIndex].u1OperStatus = u1Status;

    if (base.portEntry[u2IfIndex].u1AdminStatus == (UINT1) DOWN)
    {
        return;
    }

    L2IwfBrgHLPortOperIndication (u2IfIndex, u1Status);

    return;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : InInitMaxInfo                              */
/*                                                                           */
/*    Description               : This function is to find out the max info  */
/*                                                                           */
/*    Input(s)                  : ifaceId   - port Index                     */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
void
InInitMaxinfo (tTMO_INTERFACE ifaceId)
{
    tCfaIfInfo          IfInfo;
    /* initialise maxinfo for ports */
    if (ifaceId.u1_InterfaceType == (UINT1) CFA_ENET)
    {
        /* MTU of ENET */
        CfaGetIfInfo (ifaceId.u1_InterfaceNum, &IfInfo);
        tpInfo.portEntry[ifaceId.u1_InterfaceNum].u2Maxinfo =
            (UINT2) IfInfo.u4IfMtu + 14;
    }

}

/**********************************************************************/
/*
 *  Functionality : -
 *     Updates the configuration obtained from the BCP negotiation.
 *     
 *  Formal Parameters :-
 *    Interface Index
 *    Local MAC address
 *    Peer MAC address     
 *    Spanning Tree Protocol
 *    
 *  Global Variables Used:-
 *    base
 *  
 *  Return Values:-
 *   None.
 *********************************************************************/

VOID
BridgeConfigInfoUpdate (UINT2 u2IfIndex,
                        UINT1 LocalMacAddress[6],
                        UINT1 PeerMacAddress[6], UINT1 u1Stp)
{
    /* Update the configuration information for Bridge.
       Currently the default configuration is used */
    UNUSED_PARAM (u2IfIndex);
    UNUSED_PARAM (LocalMacAddress);
    UNUSED_PARAM (PeerMacAddress);
    UNUSED_PARAM (u1Stp);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function name             : BrgInsertInIfList                          */
/*                                                                           */
/*                                                                           */
/*    Description               : This function maintains the created ports  */
/*                                in a singly linked list fashion.           */
/*                                                                           */
/*    Input(s)                  : u2PortIndex - port number                  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*****************************************************************************/
VOID
BrgInsertInIfList (UINT2 u2IfIndex)
{
    UINT2               u2CurIndex = 0;
    UINT2               u2PrevIndex = 0;

    BRG_GET_NEXT_PORT_INDEX (u2IfIndex) = BRG_INVALID_PORT_INDEX;

    if (gu2BrgStartPortInd == BRG_INVALID_PORT_INDEX)
    {
        /* This is the first port to be created. Set the start & End *
         * port index to this port */
        gu2BrgStartPortInd = u2IfIndex;
        gu2BrgLastPortInd = u2IfIndex;
        return;
    }

    u2CurIndex = gu2BrgStartPortInd;
    while (u2CurIndex < u2IfIndex)
    {
        u2PrevIndex = u2CurIndex;
        u2CurIndex = BRG_GET_NEXT_PORT_INDEX (u2CurIndex);
    }

    BRG_GET_NEXT_PORT_INDEX (u2IfIndex) = u2CurIndex;
    BRG_GET_NEXT_PORT_INDEX (u2PrevIndex) = u2IfIndex;

    gu2BrgStartPortInd = (gu2BrgStartPortInd > u2IfIndex)
        ? u2IfIndex : gu2BrgStartPortInd;
    gu2BrgLastPortInd = (gu2BrgLastPortInd > u2IfIndex)
        ? gu2BrgLastPortInd : u2IfIndex;
}

/*****************************************************************************/
/*                                                                           */
/*    Function name             : BrgDeleteFromIfList                        */
/*                                                                           */
/*    Description               : This function deletes the port numbers     */
/*                                from the list                              */
/*                                                                           */
/*    Input(s)                  : u2PortIndex - port number                  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*****************************************************************************/
VOID
BrgDeleteFromIfList (UINT2 u2IfIndex)
{
    UINT2               u2PortIndex = 0;
    UINT2               u2PrePortIndex = BRG_INVALID_PORT_INDEX;

    if (u2IfIndex == gu2BrgStartPortInd)
    {
        gu2BrgStartPortInd = BRG_GET_NEXT_PORT_INDEX (u2IfIndex);
        if (u2IfIndex == gu2BrgLastPortInd)
        {
            gu2BrgLastPortInd = gu2BrgStartPortInd;
        }
    }
    else
    {
        /* This port is either in the last or in the middle */
        for (u2PortIndex = gu2BrgStartPortInd;
             u2PortIndex != u2IfIndex;
             u2PortIndex = BRG_GET_NEXT_PORT_INDEX (u2PortIndex))
        {
            u2PrePortIndex = u2PortIndex;
        }
        BRG_GET_NEXT_PORT_INDEX (u2PrePortIndex) =
            BRG_GET_NEXT_PORT_INDEX (u2IfIndex);
        if (u2IfIndex == gu2BrgLastPortInd)
        {
            gu2BrgLastPortInd = u2PrePortIndex;
        }
    }
}

/*****************************************************************************/
/* Function Name      : BrgGetLLPortOperStatus                               */
/*                                                                           */
/* Description        : This used to get the port oper state from LA or PNAC */
/*                      module.                                              */
/*                                                                           */
/* Input(s)           : u2PortNo - port Number                               */
/*                                                                           */
/* Output(s)          : pu1PortStatus - port oper status                     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : BRIDGE_SUCCESS or  BRIDGE_FAILURE                    */
/*****************************************************************************/
INT4
BrgGetLLPortOperStatus (UINT2 u2PortNo, UINT1 *pu1PortStatus)
{
    if (L2IwfGetPortOperStatus (BRIDGE_MODULE, u2PortNo, pu1PortStatus)
        == L2IWF_FAILURE)
    {
        return BRIDGE_FAILURE;
    }

    return BRIDGE_SUCCESS;
}

/*************************** END OF FILE(inif.c) ***************************/
