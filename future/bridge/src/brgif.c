/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brgif.c,v 1.6 2007/02/01 14:44:36 iss Exp $
 *
 * Description:This file contains bridge interface layer routines.      
 *
 *******************************************************************/

#include "brginc.h"

/*****************************************************************************/
/* Function Name      : BridgeHandOverInFrame                                */
/*                                                                           */
/* Description        : This function performs ingress filtering and         */
/*                      handovers the incoming  frames to                    */
/*                      Bridge module if Vlan is not enabled                 */
/*                                                                           */
/*                      This function handovers incoming frames  to          */
/*                      -->Bridge module if Vlan is disabled                 */
/*                                                                           */
/*                                                                           */
/* Input(s)           :  pBuf       - incoming frame                         */
/*                       u4IfIndex  - Interface index (as far as LA/PNAC and */
/*                                    bridge are considered, interface       */
/*                                    number is same as port number)         */
/*                       u2Protocol - protocol type of the frame             */
/*                       u1FrameType- BRG_DATA_FRAME  / BRG_CTL_FRAME        */
/*                       i4IsReservAddr - Reserved address                   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : BRIDGE_SUCCESS/BRIDGE_FAILURE/BRIDGE_IGNORE          */
/*****************************************************************************/
INT1
BridgeHandOverInFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                       UINT2 u2Protocol, UINT1 u1FrameType, INT4 i4IsReservAddr)
{
    UINT4               u4FdbId = 0;
    tMSG_ATTR           MsgAttr;
    tBrgIf              BrgIf;

    if (GetBridgeStatus () != BRG_UP)
    {
        return BRIDGE_SUCCESS;
    }
    BRG_MEM_SET (&MsgAttr, 0, sizeof (tMSG_ATTR));
    BRG_MEM_SET (&BrgIf, 0, sizeof (tBrgIf));

   /************************************************************
    *** Extract Destinatin and Source MAC Addresses           ***
    ************************************************************/

    BRG_BUF_COPY_FROM_BUFCHAIN (pBuf, MsgAttr.destAddr, 0, ETHERNET_ADDR_SIZE);
    BRG_BUF_COPY_FROM_BUFCHAIN (pBuf, MsgAttr.srcAddr, ETHERNET_ADDR_SIZE,
                                ETHERNET_ADDR_SIZE);
    if (BridgeIngressFilter (pBuf, (UINT2) u4IfIndex, u2Protocol,
                             &u4FdbId, &MsgAttr, u1FrameType,
                             i4IsReservAddr) != BRIDGE_SUCCESS)
    {

        STAP_BUF_RELEASE_CHAIN (pBuf, FALSE);
        BRG_TRC_ARG1 (BRG_TRC_FLAG, BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                      BRG_MOD_NAME,
                      "Released the buffer,"
                      "which is protocol filtered in the Input Port \n",
                      u4IfIndex);
        return BRIDGE_FAILURE;
    }

    BRIDGE_GET_SYS_TIME (&MsgAttr.u4TimeStamp);

    TpProcessDataFrame (&MsgAttr, pBuf, u4FdbId);
    return BRIDGE_IGNORE;
}

/*****************************************************************************/
/**** Functions to be called FROM BRIDGE (To SEND OUT FRAMES )     ***********/
/*****************************************************************************/

/*****************************************************************************/
/* Function Name      : BridgeForwardMcastFrame                              */
/*                                                                           */
/* Description        : This function is called from  BRIDGE /               */
/*                      IGMP snooping modules for forwarding a Mcast frame   */
/*                      through Ethernet interface                           */
/*                      ( It may be Mcast data frame / control frame that    */
/*                        is to be forwarded out )                           */
/*                                                                           */
/* Input(s)           :  pBuf        - Outgoing  frame                       */
/*                       u4FrameSize - SIze of the frame                     */
/*                       u2IfIndex   - Interface index                       */
/*                       u4InTime    - Time when the pkt was received        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : Base Port , Tp Port                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
BridgeForwardMcastFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                         UINT4 u4FrameSize, UINT2 u2IfIndex, UINT4 u4InTime)
{
    UINT4               u4CurSysTime;

    BRIDGE_GET_SYS_TIME (&u4CurSysTime);

    if ((u4CurSysTime - u4InTime) > BRG_TRANSIT_DELAY_TICKS)
    {

        /* discard the frame */
        base.portEntry[u2IfIndex].u4NoTransitDelayDiscard++;

        STAP_BUF_RELEASE_CHAIN (pBuf, FALSE);

        BRG_TRC (BRG_TRC_FLAG,
                 BRG_DATA_PATH_TRC | BRG_BUFFER_TRC |
                 BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Released Mcast PktFrom IGS "
                 "because of transit delay  \n  ");
        return;
    }

    /* For multicast data, increment the corresponding counter */

    IF_INCR_NO_MCAST_FRAMES_TRXED (u2IfIndex);

#ifdef SYS_HC_WANTED

    BRG_INCR_HC_OR_TP_PORT_OUT_FRAMES (u2IfIndex);

#else /* SYS_HC_WANTED */

    tpInfo.portEntry[u2IfIndex].u4TpOut++;

#endif /* SYS_HC_WANTED */

    BridgeHandleOutFrame (pBuf, u2IfIndex, u4FrameSize, 0, CFA_ENCAP_NONE);
}

/*****************************************************************************/
/* Function Name      : BridgeHandleOutFrame                                 */
/*                                                                           */
/* Description        : This function is called from  BRIDGE /  STAP         */
/*                      IGMP snooping modules for sending out a frame        */
/*                      through Ethernet interface                           */
/*                      This function handovers the packets to               */
/*                      -->LA module if LA is enabled                        */
/*                      -->PNAC module if LA is disabled                     */
/*                      -->Directly to CFA if PNAC & LA are disabled         */
/*                                                                           */
/* Input(s)           :  pBuf       - Outgoing  frame                        */
/*                       u2IfIndex  - Interface index                        */
/*                       u4FrameSize - SIze of the frame                     */
/*                       u2Protocol  - protocol type of the frame            */
/*                       u1EncapType - encaps type                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
BridgeHandleOutFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                      UINT2 u2IfIndex, UINT4 u4FrameSize,
                      UINT2 u2Protocol, UINT1 u1EncapType)
{
    L2IwfHandleOutgoingPktOnPort (pBuf, u2IfIndex, u4FrameSize,
                                  u2Protocol, u1EncapType);
    return;
}

/************************ Forwarding Table Flushing API's *********************/

/*****************************************************************************/
/* Function Name      : BrgDeleteFwdEntryForPort                             */
/*                                                                           */
/* Description        : This function deletes the forwarding table enties    */
/*                      learnt via this port                                 */
/*                                                                           */
/* Input(s)           : u2Port  - Port  number                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
BrgDeleteFwdEntryForPort (UINT2 u2Port)
{

#ifndef NPAPI_WANTED
    tFORWARDING_ENTRY  *pFwdEntry;
    UINT4               u4Hindex;
    UINT4               u4FdbId;
    UINT4               u4Mask;
#endif

    BRG_TRC_ARG1 (BRG_TRC_FLAG, BRG_DATA_PATH_TRC, BRG_MOD_NAME,
                  "Delete Fwd Entries for interface %d \n", u2Port);
#ifdef NPAPI_WANTED
    if (FsBrgFlushPort ((INT4) u2Port) == FNP_FAILURE)
    {
        /* If HW flushing failed donot flush the SW entries */
        /* To avoid synchronization in SW Table, 
         * assumption
         * here that HW will recover for failure and 
         * either H/W or S/W learning is done */
    }

#else

    BRG_MEM_SET (&u4FdbId, 0, sizeof (UINT4));
    BRG_MEM_SET (&u4Mask, 0, sizeof (UINT4));

    /* Delete the static entries from the forwarding Table */
    if (base.portEntry[u2Port].u1AdminStatus == BRIDGE_INVALID)
    {
        return;
    }

    if (IF_IS_GROUP (u2Port))
    {

        CODE_TYPE_IN_FDBID (u4FdbId, ALL_MEMBER_FDB);
    }
    else
    {

        CODE_TYPE_IN_FDBID (u4FdbId, FLAT_ENTRY_FDB);
    }

    CODE_LOGICAL_PORT_IN_FDBID (u4FdbId, u2Port);

    if (IS_FDBID_OF_THIS_TYPE (u4FdbId, ALL_MEMBER_FDB)
        || IS_FDBID_OF_THIS_TYPE (u4FdbId, FLAT_ENTRY_FDB))
        u4Mask = 0xff000000;
    else
        u4Mask = 0xff00ffff;

    if (BRG_FDB_HASH_TBL != NULL)
    {
        for (u4Hindex = 0; u4Hindex < HASH_LIST_SIZE; u4Hindex++)
        {
            TMO_HASH_Scan_Bucket (pSHtab, u4Hindex, pFwdEntry,
                                  tFORWARDING_ENTRY *)
            {
                if ((pFwdEntry->u1Status == FDB_LEARNED) &&
                    (((pFwdEntry->u4Fdbid) & u4Mask) == (u4FdbId & u4Mask)) &&
                    (!(IS_UNASSIGNED_FDB (pFwdEntry->u4Fdbid))))
                {

                    REMOVE_FDB_ENTRY (pFwdEntry);
                    BdFreeFdbEntry (pFwdEntry);
                    pFwdEntry = NULL;
                }

            }
        }
    }
#endif
    return;

}

/*****************************************************************************/
/* Function Name      : BrgDeleteFwdEntryForMac                              */
/*                                                                           */
/* Description        : This function deletes the forwarding table enties    */
/*                      matching the given MAC Address & port number         */
/*                                                                           */
/* Input(s)           : pDestAddr - mac address                              */
/*                      u2Port    - Port  number                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
BrgDeleteFwdEntryForMac (tMacAddr * pDestAddr, UINT2 u2Port)
{
    tFORWARDING_ENTRY  *pFdb;

    if ((pFdb = (tFORWARDING_ENTRY *) HashSearchNode (pSHtab,
                                                      pDestAddr)) != NULL)
    {
        TmStopTimer (&pFdb->ageout);
        REMOVE_FDB_ENTRY (pFdb);
        BdFreeFdbEntry (pFdb);
        pFdb = NULL;
    }

    UNUSED_PARAM (u2Port);

    return;

}

/************************ API's For IGMP SNOOPING MODULE    ******************/

/*****************************************************************************/
/* Function Name      : BrgIncrStatsAtMcastTx                                */
/*                                                                           */
/* Description        : This function increments the base port entry's mcast */
/*                      counter & Tp port entry's Tx pkts count              */
/*                                                                           */
/*                      This function is called from IGMP snooping module    */
/*                                                                           */
/* Input(s)           : u2Port - port number                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : base , tpInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
BrgIncrStatsAtMcastTx (UINT2 u2Port)
{
    IF_INCR_NO_MCAST_FRAMES_TRXED (u2Port);

#ifdef SYS_HC_WANTED
    BRG_INCR_HC_OR_TP_PORT_OUT_FRAMES (u2Port);
#else /* SYS_HC_WANTED */
    tpInfo.portEntry[u2Port].u4TpOut++;
#endif /* SYS_HC_WANTED */

}

/************************ API's For Vlan   ************************************/

/*****************************************************************************/
/* Function Name      : BrgGetAgeoutTime                                     */
/*                                                                           */
/* Description        : This function returns the AgeoutTime to be used      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : tpInfo                                               */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : AgeoutTime                                           */
/*****************************************************************************/
UINT4
BrgGetAgeoutTime (VOID)
{
    return (tpInfo.u4FdbAgeingTime);
}

/*****************************************************************************/
/* Function Name      : BrgIncrFilterInDiscards                              */
/*                                                                           */
/* Description        : This function increments the Filter Indiscard        */
/*                                                                           */
/* Input(s)           : u2Port - port number                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : base , tpInfo                                        */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
BrgIncrFilterInDiscards (UINT2 u2Port)
{
#ifdef SYS_HC_WANTED
    BRG_INCR_HC_OR_TP_FILTER_IN_DISCARDS (u2Port);
#else /* SYS_HC_WANTED */
    BRG_INCR_PORT_FILTER_IN_DISCARDS (u2Port);
#endif /* SYS_HC_WANTED */

}

/*****************************************************************************/
/* Function Name      : BrgCanForward                                        */
/*                                                                           */
/* Description        : This function will be called from VLAN when vlan     */
/*                      enabled from disable state                           */
/*                                                                           */
/*                      This function checks whether the pkt is to be        */
/*                      filtered or allowed to be sent out.                  */
/*                                                                           */
/*                      Returns BRG_FORWARD if pkt can be sent out .         */
/*                      Returns BRG_NO_FORWARD if pkt can't be sent out      */
/*                                                                           */
/* Input(s)           : pBuf - Pkt which needs to be checked whether can be  */
/*                             forwarded or not                              */
/*                      u2Port - port number over which the pkt is supposed  */
/*                             to be forwared out                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : base                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : BRG_FORWARD /BRG_NO_FORWARD                          */
/*****************************************************************************/

INT4
BrgCanForward (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Port)
{
    tMacAddr            SrcMacAddr;
    tMacAddr            DstMacAddr;

    BRG_MEM_SET (SrcMacAddr, 0, ETHERNET_ADDR_SIZE);
    BRG_MEM_SET (DstMacAddr, 0, ETHERNET_ADDR_SIZE);

    BRG_BUF_COPY_FROM_BUFCHAIN (pBuf, DstMacAddr, 0, ETHERNET_ADDR_SIZE);
    BRG_BUF_COPY_FROM_BUFCHAIN (pBuf, SrcMacAddr,
                                ETHERNET_ADDR_SIZE, ETHERNET_ADDR_SIZE);

    /* check if pkt is to be filtered */
    if ((BdIsfiltered (base.portEntry[u2Port].u1FilterNo,
                       &DstMacAddr, &SrcMacAddr) == TRUE))
    {

        return BRG_NO_FORWARD;
    }

    /* Check if mcast pkt is to be filtered */
    if (BM_IS_MAC_MULTICAST (DstMacAddr[0]))
    {

        if (BdIsMcastAddr (base.portEntry[u2Port].u1MlistNo, &DstMacAddr)
            == FALSE)
        {
            return BRG_NO_FORWARD;
        }
    }

    return BRG_FORWARD;
}

/************************** Utility Functions of BrgIf ***********************/

/*****************************************************************************/
/* Function Name      : BridgeFillInInfo                                     */
/*                                                                           */
/* Description        : This function Fills the BrgIfInInfo using the        */
/*                      message Attribute structure                          */
/*                                                                           */
/* Input(s)           :  pMsgAttr   - Msg attribute                          */
/*                                                                           */
/* Output(s)          :  pBrgIf     - Bridge Interface Structure             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
BridgeFillInInfo (tMSG_ATTR * pMsgAttr, tBrgIf * pBrgIf)
{
    tBrgIfInInfo       *pInInfo;

    pInInfo = &pBrgIf->uInfo.InInfo;

    BRG_MEM_CPY (&pInInfo->SrcAddr, pMsgAttr->srcAddr, ETHERNET_ADDR_SIZE);
    BRG_MEM_CPY (&pInInfo->DestAddr, pMsgAttr->destAddr, ETHERNET_ADDR_SIZE);

    pInInfo->u2Protocol = pMsgAttr->u2Protocol;
    pInInfo->u2InPort = pMsgAttr->u2LogicalPortNo;
    pInInfo->u2Length = pMsgAttr->u2Length;

    BRIDGE_GET_SYS_TIME (&pBrgIf->u4TimeStamp);

    return;
}
