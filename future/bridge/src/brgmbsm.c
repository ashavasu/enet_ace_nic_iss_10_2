/*****************************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2004-2005                         */
/*****************************************************************************/
/*    FILE  NAME            : brgmbsm.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Bridge module                                  */
/*    MODULE NAME           : Bridge module Card Updation                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 21 Jan 2005                                    */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Card Insertion functions for*/
/*                            the Bridge module.                             */
/*---------------------------------------------------------------------------*/

#include "brginc.h"
/*****************************************************************************/
/* Function Name      : BridgeMbsmUpdateCardInsertion                        */
/*                                                                           */
/* Description        : This function programs the HW with the Bridge        */
/*                      software configuration.                              */
/*                                                                           */
/*                      This function will be called from the Bridge module  */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           :  tMbsmPortInfo - Structure containing the PortList,  */
/*                                       Starting Index and the port count.  */
/*                       tMbsmSlotInfo - Structure containing the SlotId     */
/*                                       info.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCCESS/MBSM_FAILURE                            */
/*****************************************************************************/

INT4
BridgeMbsmUpdateCardInsertion (tMbsmPortInfo * pPortInfo,
                               tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4AgingTime = 0;

    /* Get the Bridge Aging time from the SW and program the same in the HW. */
    u4AgingTime = BrgGetAgeoutTime ();

    if (FsBrgMbsmSetAgingTime (u4AgingTime, pSlotInfo) == FNP_FAILURE)
    {
        return MBSM_FAILURE;
    }

    UNUSED_PARAM (pPortInfo);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : BrgMbsmProcessUpdateMessage                          */
/*                                                                           */
/* Description        : This function constructs the Bridge Q Mesg and calls */
/*                      the Bridge packet arrival event to process this Mesg.*/
/*                                                                           */
/*                      This function will be called from the MBSM module    */
/*                      when an indication for the Card Insertion is         */
/*                      received.                                            */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
BridgeMbsmProcessUpdateMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tBrgQMesg          *pMesg = NULL;

    if (pProtoMsg == NULL)
        return MBSM_FAILURE;

    if (BRG_ALLOC_MEM_BLOCK (gBrgPoolId, &pMesg) != BRG_MEM_SUCCESS)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 " Failed to Allocate Memory for Bridge Message  !!! \n");

        return MBSM_FAILURE;
    }

    if (!(pMesg->MbsmCardUpdate.pMbsmProtoMsg =
          MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)))
    {
        BRG_RELEASE_MEM_BLOCK (gBrgPoolId, pMesg);
        return MBSM_FAILURE;
    }
    pMesg->u4MesgType = (UINT4) i4Event;
    MEMCPY (pMesg->MbsmCardUpdate.pMbsmProtoMsg, pProtoMsg,
            sizeof (tMbsmProtoMsg));

    if (BRG_SEND_TO_QUEUE (SELF, BRG_QUEUE_NAME, (tOsixMsg *) pMesg,
                           BRG_OSIX_MSG_NORMAL) == BRG_SUCCESS)
    {
        if (BRG_SEND_EVENT (SELF, BRIDGE_TMR_TASK_NAME, BRG_PKT_ARVL_EVENT)
            != BRG_SUCCESS)
        {
            BRG_TRC (BRG_TRC_FLAG, BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                     " Failed to Send event to Bridge Task  !!! \n");
        }
        return MBSM_SUCCESS;
    }

    BRG_TRC (BRG_TRC_FLAG, BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
             " INF: Send To Q failed !!!\n");

    MEM_FREE (pMesg->MbsmCardUpdate.pMbsmProtoMsg);
    BRG_RELEASE_MEM_BLOCK (gBrgPoolId, pMesg);

    return MBSM_FAILURE;
}
