# include  "lr.h"
# include  "fssnmp.h"
# include  "stdbriwr.h"
# include  "stdbrlow.h"
# include  "stdbridb.h"
# include "brginc.h"
# include  "rstp.h"

VOID
RegisterSTDBRI ()
{
    SNMPRegisterMib (&stdbriOID, &stdbriEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdbriOID, (const UINT1 *) "dot1dBridge");
}

INT4
Dot1dBaseBridgeAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    pMultiData->pOctetStrValue->i4_Length = 6;
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dBaseBridgeAddress
            ((tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));
}

INT4
Dot1dBaseNumPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dBaseNumPorts (&(pMultiData->i4_SLongValue)));
}

INT4
Dot1dBaseTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dBaseType (&(pMultiData->i4_SLongValue)));
}

INT4
GetNextIndexDot1dBasePortTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dBasePortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dBasePortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1dBasePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
Dot1dBasePortIfIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortIfIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dBasePortCircuitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortCircuit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOidValue));

}

INT4
Dot1dBasePortDelayExceededDiscardsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortDelayExceededDiscards
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1dBasePortMtuExceededDiscardsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortMtuExceededDiscards
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1dStpProtocolSpecificationGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal =
        nmhGetDot1dStpProtocolSpecification (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpPriority (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpTimeSinceTopologyChangeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpTimeSinceTopologyChange
        (&(pMultiData->u4_ULongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpTopChangesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpTopChanges (&(pMultiData->u4_ULongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpDesignatedRootGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpDesignatedRoot (pMultiData->pOctetStrValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpRootCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpRootCost (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpRootPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpRootPort (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpMaxAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpMaxAge (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpHelloTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpHelloTime (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpHoldTime (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpForwardDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpForwardDelay (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpBridgeMaxAgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpBridgeMaxAge (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpBridgeHelloTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpBridgeHelloTime (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpBridgeForwardDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhGetDot1dStpBridgeForwardDelay (&(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhSetDot1dStpPriority (pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpBridgeMaxAgeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhSetDot1dStpBridgeMaxAge (pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpBridgeHelloTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhSetDot1dStpBridgeHelloTime (pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpBridgeForwardDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhSetDot1dStpBridgeForwardDelay (pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhTestv2Dot1dStpPriority (pu4Error, pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpBridgeMaxAgeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhTestv2Dot1dStpBridgeMaxAge
        (pu4Error, pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpBridgeHelloTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhTestv2Dot1dStpBridgeHelloTime
        (pu4Error, pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpBridgeForwardDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal;

    UNUSED_PARAM (pMultiIndex);

    AST_LOCK ();

    i4RetVal = nmhTestv2Dot1dStpBridgeForwardDelay
        (pu4Error, pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
GetNextIndexDot1dStpPortTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Dot1dStpPort;

    AST_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dStpPortTable (&i4Dot1dStpPort) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dStpPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4Dot1dStpPort) == SNMP_FAILURE)
        {
            AST_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].i4_SLongValue = i4Dot1dStpPort;
    AST_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
Dot1dStpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    AST_LOCK ();

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    AST_UNLOCK ();
    return SNMP_SUCCESS;

}

INT4
Dot1dStpPortPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortState (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortPathCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortPathCost (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortDesignatedRootGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortDesignatedRoot
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortDesignatedCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortDesignatedCost
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->i4_SLongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortDesignatedBridgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortDesignatedBridge
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortDesignatedPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortDesignatedPort
        (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortForwardTransitionsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    if (nmhValidateIndexInstanceDot1dStpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        AST_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4RetVal = nmhGetDot1dStpPortForwardTransitions
        (pMultiIndex->pIndex[0].i4_SLongValue, &(pMultiData->u4_ULongValue));

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhSetDot1dStpPortPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhSetDot1dStpPortEnable (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortPathCostSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhSetDot1dStpPortPathCost (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhTestv2Dot1dStpPortPriority (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhTestv2Dot1dStpPortEnable (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dStpPortPathCostTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4RetVal;

    AST_LOCK ();

    i4RetVal = nmhTestv2Dot1dStpPortPathCost (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue);

    AST_UNLOCK ();
    return i4RetVal;
}

INT4
Dot1dTpLearnedEntryDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dTpLearnedEntryDiscards (&(pMultiData->u4_ULongValue)));
}

INT4
Dot1dTpAgingTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dTpAgingTime (&(pMultiData->i4_SLongValue)));
}

INT4
Dot1dTpAgingTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDot1dTpAgingTime (pMultiData->i4_SLongValue));
}

INT4
Dot1dTpAgingTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Dot1dTpAgingTime (pu4Error, pMultiData->i4_SLongValue));
}

INT4
GetNextIndexDot1dTpFdbTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dTpFdbTable ((tMacAddr *) pNextMultiIndex->
                                             pIndex[0].pOctetStrValue->
                                             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dTpFdbTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Dot1dTpFdbAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dTpFdbTable ((*(tMacAddr *) pMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
Dot1dTpFdbPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dTpFdbTable ((*(tMacAddr *) pMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dTpFdbPort ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                   pOctetStrValue->pu1_OctetList),
                                  &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dTpFdbStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dTpFdbTable ((*(tMacAddr *) pMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dTpFdbStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                     pOctetStrValue->pu1_OctetList),
                                    &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexDot1dTpPortTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dTpPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dTpPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1dTpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
Dot1dTpPortMaxInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dTpPortMaxInfo (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dTpPortInFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dTpPortInFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
Dot1dTpPortOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dTpPortOutFrames (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
Dot1dTpPortInDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dTpPortInDiscards (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexDot1dStaticTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dStaticTable ((tMacAddr *) pNextMultiIndex->
                                              pIndex[0].pOctetStrValue->
                                              pu1_OctetList,
                                              &(pNextMultiIndex->pIndex[1].
                                                i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dStaticTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList, pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Dot1dStaticAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dStaticTable ((*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
Dot1dStaticReceivePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dStaticTable ((*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
Dot1dStaticAllowedToGoToGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dStaticTable ((*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dStaticAllowedToGoTo ((*(tMacAddr *) pMultiIndex->
                                             pIndex[0].pOctetStrValue->
                                             pu1_OctetList),
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Dot1dStaticStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dStaticTable ((*(tMacAddr *) pMultiIndex->
                                                   pIndex[0].pOctetStrValue->
                                                   pu1_OctetList),
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dStaticStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                      pOctetStrValue->pu1_OctetList),
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dStaticAllowedToGoToSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1dStaticAllowedToGoTo ((*(tMacAddr *) pMultiIndex->
                                             pIndex[0].pOctetStrValue->
                                             pu1_OctetList),
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Dot1dStaticStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1dStaticStatus ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                      pOctetStrValue->pu1_OctetList),
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
Dot1dStaticAllowedToGoToTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dStaticAllowedToGoTo (pu4Error,
                                               (*(tMacAddr *) pMultiIndex->
                                                pIndex[0].pOctetStrValue->
                                                pu1_OctetList),
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));
}

INT4
Dot1dStaticStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dStaticStatus (pu4Error,
                                        (*(tMacAddr *) pMultiIndex->pIndex[0].
                                         pOctetStrValue->pu1_OctetList),
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}
