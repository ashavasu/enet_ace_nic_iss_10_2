/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brgutil.c,v 1.2 2007/02/01 14:44:36 iss Exp $
 *
 * Description:This file contains the common routines that    
 *             are used by the bridge.                        
 *
 *******************************************************************/

/******************************************************************/

#include "brginc.h"
/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BuCompareMacaddr                           */
/*                                                                           */
/*    Description               : This function comapares the mac addresses  */
/*                                                                           */
/*    Input(s)                  : addr1                                      */
/*                                addr2                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : LESSER                                     */
/*                                BRG_EQUAL                                  */
/*                                GREATER                                    */
/*****************************************************************************/
INT4
BuCompareMacaddr (tMacAddr addr1, tMacAddr addr2)
{
    INT4                i4Val = BRG_EQUAL;

    if (BRG_MEM_CMP (addr1, addr2, ETHERNET_ADDR_SIZE) < 0)
        i4Val = LESSER;
    else if (BRG_MEM_CMP (addr1, addr2, ETHERNET_ADDR_SIZE) > 0)
        i4Val = GREATER;
    else
        i4Val = BRG_EQUAL;

    return i4Val;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BuGetNoBitsSet                             */
/*                                                                           */
/*    Description               : This routine is used to make the KEY,this  */
/*                                counts the number of ones in the array of  */
/*                                bytes                                      */
/*                                                                           */
/*    Input(s)                  : pStr                                       */
/*                                u4Len                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : u4Count                                    */
/*****************************************************************************/
UINT4
BuGetNoBitsSet (UINT1 *pStr, UINT4 u4Len)
{
    UINT4               u4I, u4J, u4Count = 0;

    for (u4I = 0; u4I < u4Len; u4I++)
    {
        for (u4J = 0; u4J < 8; u4J++)
        {
            if (pStr[u4I] & (1 << u4J))
            {
                u4Count++;
            }
        }
    }
    return u4Count;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BuCompareIface                             */
/*                                                                           */
/*    Description               : This routine is used to compare interfaces */
/*                                                                           */
/*    Input(s)                  : pIf1                                       */
/*                                pIf2                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : LESSER                                     */
/*                                BRG_EQUAL                                  */
/*                                GREATER                                    */
/*****************************************************************************/
INT4
BuCompareIface (tTMO_INTERFACE * pIf1, tTMO_INTERFACE * pIf2)
{
    UINT4               u4Id1 = 0x10000000, u4Id2 = 0x10000000;
    if ((pIf1->u1_InterfaceType == CRU_ENET_PHYS_INTERFACE_TYPE) ||
        (pIf1->u1_InterfaceType == CFA_LAGG))
        u4Id1 = 0x0000000;

    if ((pIf2->u1_InterfaceType == CRU_ENET_PHYS_INTERFACE_TYPE) ||
        (pIf2->u1_InterfaceType == CFA_LAGG))
        u4Id2 = 0x0000000;

    u4Id1 = u4Id1 + pIf1->u1_InterfaceNum * 0x10000 + pIf1->u2_SubReferenceNum;

    u4Id2 = u4Id2 + pIf2->u1_InterfaceNum * 0x10000 + pIf2->u2_SubReferenceNum;
    if (u4Id1 > u4Id2)
        return GREATER;
    else if (u4Id1 < u4Id2)
        return LESSER;
    else
        return BRG_EQUAL;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BuPrepareFloodBuffer                       */
/*                                                                           */
/*    Description               :                                            */
/*                                                                           */
/*    Input(s)                  : pMsg                                       */
/*                                iface                                      */
/*                                bFlag                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK                                         */
/*                                BRIDGE_NOT_OK                                     */
/*****************************************************************************/
INT4
BuPrepareFloodBuffer (tCRU_BUF_CHAIN_HEADER * pMsg, tTMO_INTERFACE iface,
                      tBOOLEAN bFlag)
{
    tCRU_BUF_CHAIN_HEADER *pMsgDum;

    if (bFlag == FALSE)
    {

        BRG_BUF_SET_INTERFACEID (pMsg, iface);
        if ((iface.u1_InterfaceType == CRU_ENET_PHYS_INTERFACE_TYPE) ||
            (iface.u1_InterfaceType == CFA_LAGG))
            BRG_BUF_SET_DESTINMODULEID (pMsg, CRU_COMMON_FORWARD_MODULE);
        BRG_BUF_SET_SOURCEMODULEID (pMsg, CRU_BRIDGE_WITHOUT_CRC_MODULE);
    }
    else if ((pMsgDum =
              (tCRU_BUF_CHAIN_HEADER *) CRU_BUF_Duplicate_BufChain (pMsg)) !=
             NULL)
    {
        pMsgDum->ModuleData = pMsg->ModuleData;
        if ((iface.u1_InterfaceType == CRU_ENET_PHYS_INTERFACE_TYPE) ||
            (iface.u1_InterfaceType == CFA_LAGG))
            BRG_BUF_SET_DESTINMODULEID (pMsgDum, CRU_COMMON_FORWARD_MODULE);
        BRG_BUF_SET_INTERFACEID (pMsgDum, iface);
        CRU_BUF_Link_BufChains (pMsg, pMsgDum);

    }
    else
    {
        return BRIDGE_NOT_OK;
    }

    return BRIDGE_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BuGetProtocolType                          */
/*                                                                           */
/*    Description               : This function gets the protocol type       */
/*                                                                           */
/*    Input(s)                  : u2EnetProtocol                             */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : CRU_IP                                     */
/*                                CRU_IPX                                    */
/*                                CRU_XNS                                    */
/*                                CRU_DECNET                                 */
/*                                CRU_ATALK                                  */
/*                                CRU_PROTOCOL_UNKNOWN                       */
/*****************************************************************************/
UINT2
BuGetProtocolType (UINT2 u2EnetProtocol)
{
    switch (u2EnetProtocol)
    {
        case CRU_ENET_IP_TYPE:
            return CRU_IP;

        case CRU_ENET_IPX_TYPE:
            return CRU_IPX;

        case CRU_ENET_XNS_TYPE:
            return CRU_XNS;

        case CRU_ENET_DECNET_TYPE:
            return CRU_DECNET;

        case CRU_ENET_ATALK_TYPE:
            return CRU_ATALK;

        default:
            return CRU_PROTOCOL_UNKNOWN;
    }
}

/*******************************************************************************
* BrgCheckFilterEntry
*
* DESCRIPTION:
* This utility function checks whether the masked address matches with the 
* entry 
*
* INPUTS:
* pFilMask - mask address 
* pFilAddr - address in the filter entry
* addr -address to be matched
* OUTPUTS:
* None.
*
* RETURNS:
* TRUE - if matched 
* FALSE - if not matched
*
* COMMENTS:
*  None
*
*******************************************************************************/
INT1
BrgCheckFilterEntry (tMacAddr pFilMask, tMacAddr pFilAddr, tMacAddr addr)
{
    INT1                i1Index;
    for (i1Index = 0; i1Index < ETHERNET_ADDR_SIZE; i1Index++)
    {
        if ((pFilMask[i1Index] & pFilAddr[i1Index])
            != (pFilMask[i1Index] & addr[i1Index]))
        {
            return (FALSE);
        }
    }
    return (TRUE);
}

/*************************** END OF FILE(brgutil.c) ***********************/
