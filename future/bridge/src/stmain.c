/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stmain.c,v 1.9 2007/03/29 13:21:04 iss Exp $
 *
 * Description:This file contains main routine of the         
 *             STAP module.                               
 *
 *******************************************************************/
#include "brginc.h"

#ifdef SNMP_2_WANTED
#include "fsbridwr.h"
#include "stdbriwr.h"
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BrgTmrTask                                 */
/*                                                                           */
/*    Description               : Entry Point/main for Bridge Timer Task.    */
/*                                All Activities of the Timer are Received by*/
/*                                this routine as different Events.          */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified :  bridgeTimer.                          */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/

VOID
BrgTmrTask (INT1 *pi1InParam)
{
    UINT4               u4retval;
    UINT4               u4Events;
    tbrgtmr             brgRef;
    tBrgQMesg          *pMesg = NULL;
    UINT4               u4Val = BRG_SUCCESS;
#ifdef MBSM_WANTED
    INT4                i4ProtoId = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    INT4                i4RetVal = 0;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
#endif /* MBSM_WANTED */

    UNUSED_PARAM (pi1InParam);

    BRG_TRC (BRG_TRC_FLAG, BRG_INIT_SHUT_TRC | BRG_OS_RESOURCE_TRC,
             BRG_MOD_NAME, " Started Spawning the Bridge Timer Task \n ");

    /* Initializations */
    if (BridgeInitializeProtocol () != BRG_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        BRG_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Create a Queue for bridge packets */
    if (BRG_CREATE_QUEUE (BRG_QUEUE_NAME, BRG_QUEUE_DEPTH,
                          0, &gBrgQId) != BRG_SUCCESS)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_INIT_SHUT_TRC |
                 BRG_OS_RESOURCE_TRC |
                 BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME, " Failed to create Queue for Bridge !!! \n");
        /* Indicate the status of initialization to the main routine */
        BRG_INIT_COMPLETE (OSIX_FAILURE);
        return;

    }
    /* Create Queue Mesg buffer pool */
    if (STAP_CREATE_MEM_POOL (sizeof (tBrgQMesg), BRG_QUEUE_DEPTH,
                              MEM_DEFAULT_MEMORY_TYPE, &gBrgPoolId) !=
        BRG_MEM_SUCCESS)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_INIT_SHUT_TRC |
                 BRG_OS_RESOURCE_TRC |
                 BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME, " Failed to create Mem pool for Bridge  !!! \n");
        if (BRG_DELETE_QUEUE (gBrgQId, BRG_QUEUE_NAME) != BRG_SUCCESS)
        {

            BRG_TRC (BRG_TRC_FLAG, BRG_INIT_SHUT_TRC |
                     BRG_OS_RESOURCE_TRC |
                     BRG_ALL_FAILURE_TRC,
                     BRG_MOD_NAME, " Failed to delete Queue of Bridge  !!! \n");
        }
        /* Indicate the status of initialization to the main routine */
        BRG_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    u4Val = BRIDGE_CREATE_TMR_LIST (BRIDGE_TMR_TASK_NAME,
                                    BRG_TIMER_EXP_EVENT, NULL, &tlistid);
    if (u4Val == BRG_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_INIT_SHUT_TRC |
                 BRG_OS_RESOURCE_TRC |
                 BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to create the Timer List for Bridge Timer !!! \n");
        /* Indicate the status of initialization to the main routine */
        BRG_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Start the Timer service */
    brgRef.u4Data = 0;
    u4Val = BRIDGE_START_TMR (tlistid, &brgRef, BRG_TIME_DURATION);
    if (u4Val == BRG_FAILURE)

    {
        BRG_TRC (BRG_TRC_FLAG, BRG_INIT_SHUT_TRC |
                 BRG_OS_RESOURCE_TRC |
                 BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME, "Failed to start the Bridge Timer !!! \n ");
        /* Indicate the status of initialization to the main routine */
        BRG_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    BRG_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterSTDBRI ();
    RegisterFSBRID ();
#endif

    while (1)
    {
        if ((u4retval = OSS_WAIT_EVENT (BRG_TIMER_EXP_EVENT |
                                        BRG_START_EVENT | BRG_STOP_EVENT |
                                        BRG_PKT_ARVL_EVENT,
                                        OSIX_WAIT,
                                        0, &u4Events)) == BRG_SUCCESS)
        {

         /****************************************************/
            /*              BRG_START_EVENT                     */
         /****************************************************/
            if (u4Events & BRG_START_EVENT)
            {

                /* Starting Bridge Module */
                if (BridgeModuleInit () == BRIDGE_FAILURE)
                {
                    BRG_TRC (BRG_TRC_FLAG,
                             (BRG_INIT_SHUT_TRC
                              | BRG_OS_RESOURCE_TRC
                              | BRG_ALL_FAILURE_TRC), BRG_MOD_NAME,
                             " Starting Bridge Module Failed \n ");
                }
                else
                {
                    BridgeModuleRestart ();

                    BRG_TRC (BRG_TRC_FLAG,
                             (BRG_INIT_SHUT_TRC
                              | BRG_OS_RESOURCE_TRC
                              | BRG_ALL_FAILURE_TRC), BRG_MOD_NAME,
                             " Started Bridge Module Successfully... \n ");
                }
            }

         /****************************************************/
            /*                BRG_STOP_EVENT                    */
         /****************************************************/
            if (u4Events & BRG_STOP_EVENT)
            {
                /* Shutting Down Bridge Module */
                if (BridgeModuleShutdown () == BRIDGE_FAILURE)
                {
                    BRG_TRC (BRG_TRC_FLAG,
                             (BRG_INIT_SHUT_TRC
                              | BRG_OS_RESOURCE_TRC
                              | BRG_ALL_FAILURE_TRC), BRG_MOD_NAME,
                             " Bridge Module Shutdown Failed... \n ");
                }
                else
                {

                    BRG_TRC (BRG_TRC_FLAG,
                             (BRG_INIT_SHUT_TRC
                              | BRG_OS_RESOURCE_TRC
                              | BRG_ALL_FAILURE_TRC), BRG_MOD_NAME,
                             " Bridge Module Shutdown Successfully...\n");
                }
            }
            if (u4Events & BRG_TIMER_EXP_EVENT)
            {
            /************************************* 
            *** Timer Expiry Event            ***
            *************************************/
                if (BrgTmrExpiryHandler () != BRG_SUCCESS)
                {
                    BRG_TRC (BRG_TRC_FLAG,
                             (BRG_OS_RESOURCE_TRC
                              | BRG_ALL_FAILURE_TRC), BRG_MOD_NAME,
                             " Brg Timer Expiry handler failed... \n ");
                }

            }
            if (u4Events & BRG_PKT_ARVL_EVENT)
            {

                while (BRG_DEQUEUE_MESSAGES
                       (BRG_QUEUE_NAME, BRG_NO_WAIT, &pMesg) == BRG_SUCCESS)
                {

                    if (pMesg == NULL)
                        continue;

                    switch (pMesg->u4MesgType)
                    {
#ifdef MBSM_WANTED

                        case MBSM_MSG_CARD_INSERT:
                            i4ProtoId =
                                pMesg->MbsmCardUpdate.pMbsmProtoMsg->
                                i4ProtoCookie;
                            pSlotInfo =
                                &(pMesg->MbsmCardUpdate.pMbsmProtoMsg->
                                  MbsmSlotInfo);
                            pPortInfo =
                                &(pMesg->MbsmCardUpdate.pMbsmProtoMsg->
                                  MbsmPortInfo);
                            i4RetVal =
                                BridgeMbsmUpdateCardInsertion (pPortInfo,
                                                               pSlotInfo);
                            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                            MbsmProtoAckMsg.i4SlotId =
                                MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                            MbsmProtoAckMsg.i4RetStatus = i4RetVal;

                            MbsmSendAckFromProto (&MbsmProtoAckMsg);

                            MEM_FREE (pMesg->MbsmCardUpdate.pMbsmProtoMsg);

                            break;
                        case MBSM_MSG_CARD_REMOVE:
                            i4ProtoId =
                                pMesg->MbsmCardUpdate.pMbsmProtoMsg->
                                i4ProtoCookie;
                            pSlotInfo =
                                &(pMesg->MbsmCardUpdate.pMbsmProtoMsg->
                                  MbsmSlotInfo);
                            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                            MbsmProtoAckMsg.i4SlotId =
                                MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                            MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;

                            MbsmSendAckFromProto (&MbsmProtoAckMsg);

                            MEM_FREE (pMesg->MbsmCardUpdate.pMbsmProtoMsg);

                            break;
#endif
                        default:
                            break;
                    }

                    BRG_RELEASE_MEM_BLOCK (gBrgPoolId, pMesg);
                }
            }
        }
    }
}

/*************************** END OF FILE(stmain.c) ***********************/
