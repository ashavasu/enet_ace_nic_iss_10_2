# include  "lr.h"
# include  "fssnmp.h"
# include  "fsbrilow.h"
# include  "fsbridwr.h"
# include  "fsbriddb.h"

VOID
RegisterFSBRID ()
{
    SNMPRegisterMib (&fsbridOID, &fsbridEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsbridOID, (const UINT1 *) "futureBridgeMIB");
}

INT4
Dot1dBridgeSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dBridgeSystemControl (&(pMultiData->i4_SLongValue)));
}

INT4
Dot1dBaseBridgeStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dBaseBridgeStatus (&(pMultiData->i4_SLongValue)));
}

INT4
Dot1dBaseBridgeCRCStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dBaseBridgeCRCStatus (&(pMultiData->i4_SLongValue)));
}

INT4
Dot1dBaseBridgeDebugGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dBaseBridgeDebug (&(pMultiData->i4_SLongValue)));
}

INT4
Dot1dBaseBridgeTraceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dBaseBridgeTrace (&(pMultiData->i4_SLongValue)));
}

INT4
Dot1dBaseBridgeMaxFwdDbEntriesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetDot1dBaseBridgeMaxFwdDbEntries
            (&(pMultiData->i4_SLongValue)));
}

INT4
Dot1dBridgeSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDot1dBridgeSystemControl (pMultiData->i4_SLongValue));
}

INT4
Dot1dBaseBridgeStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDot1dBaseBridgeStatus (pMultiData->i4_SLongValue));
}

INT4
Dot1dBaseBridgeCRCStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDot1dBaseBridgeCRCStatus (pMultiData->i4_SLongValue));
}

INT4
Dot1dBaseBridgeDebugSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDot1dBaseBridgeDebug (pMultiData->i4_SLongValue));
}

INT4
Dot1dBaseBridgeTraceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetDot1dBaseBridgeTrace (pMultiData->i4_SLongValue));
}

INT4
Dot1dBridgeSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Dot1dBridgeSystemControl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Dot1dBaseBridgeStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Dot1dBaseBridgeStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Dot1dBaseBridgeCRCStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Dot1dBaseBridgeCRCStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Dot1dBaseBridgeDebugTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Dot1dBaseBridgeDebug
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
Dot1dBaseBridgeTraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2Dot1dBaseBridgeTrace
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
GetNextIndexDot1dFutureBasePortTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dFutureBasePortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dFutureBasePortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1dFutureBasePortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
Dot1dBasePortAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dBasePortOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortOperStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dBasePortBcastStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortBcastStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dBasePortFilterNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortFilterNumber
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dBasePortMcastNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortMcastNumber
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dBasePortBcastOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortBcastOutFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1dBasePortMcastOutFramesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureBasePortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dBasePortMcastOutFrames
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1dBasePortAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1dBasePortAdminStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1dBasePortBcastStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1dBasePortBcastStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1dBasePortFilterNumberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1dBasePortFilterNumber
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1dBasePortMcastNumberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1dBasePortMcastNumber
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1dBasePortAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dBasePortAdminStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Dot1dBasePortBcastStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dBasePortBcastStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
Dot1dBasePortFilterNumberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dBasePortFilterNumber (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
Dot1dBasePortMcastNumberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dBasePortMcastNumber (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
GetNextIndexDot1dFutureTpPortTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dFutureTpPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dFutureTpPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Dot1dFutureTpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
Dot1dTpPortInProtoDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dTpPortInProtoDiscards
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1dTpPortInFilterDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dTpPortInFilterDiscards
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Dot1dTpPortProtocolFilterMaskGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFutureTpPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dTpPortProtocolFilterMask
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dTpPortProtocolFilterMaskSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetDot1dTpPortProtocolFilterMask
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
Dot1dTpPortProtocolFilterMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dTpPortProtocolFilterMask (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
GetNextIndexDot1dFilterTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dFilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dFilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             *(tMacAddr *) pFirstMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[1].pOctetStrValue->
             pu1_OctetList,
             *(tMacAddr *) pFirstMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[2].pOctetStrValue->
             pu1_OctetList) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[1].pOctetStrValue->i4_Length = 6;
    pNextMultiIndex->pIndex[2].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Dot1dFilterNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
Dot1dFilterSrcAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[1].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
Dot1dFilterSrcMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetDot1dFilterSrcMask (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      (*(tMacAddr *) pMultiIndex->pIndex[2].
                                       pOctetStrValue->pu1_OctetList),
                                      (tMacAddr *) pMultiData->pOctetStrValue->
                                      pu1_OctetList));

}

INT4
Dot1dFilterDstAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[2].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
Dot1dFilterDstMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetDot1dFilterDstMask (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      (*(tMacAddr *) pMultiIndex->pIndex[2].
                                       pOctetStrValue->pu1_OctetList),
                                      (tMacAddr *) pMultiData->pOctetStrValue->
                                      pu1_OctetList));

}

INT4
Dot1dFilterPermissGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         (*(tMacAddr *) pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList),
         (*(tMacAddr *) pMultiIndex->pIndex[2].pOctetStrValue->
          pu1_OctetList)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dFilterPermiss (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      (*(tMacAddr *) pMultiIndex->pIndex[2].
                                       pOctetStrValue->pu1_OctetList),
                                      &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dFilterSrcMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1dFilterSrcMask (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      (*(tMacAddr *) pMultiIndex->pIndex[2].
                                       pOctetStrValue->pu1_OctetList),
                                      (*(tMacAddr *) pMultiData->
                                       pOctetStrValue->pu1_OctetList)));

}

INT4
Dot1dFilterDstMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1dFilterDstMask (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      (*(tMacAddr *) pMultiIndex->pIndex[2].
                                       pOctetStrValue->pu1_OctetList),
                                      (*(tMacAddr *) pMultiData->
                                       pOctetStrValue->pu1_OctetList)));

}

INT4
Dot1dFilterPermissSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1dFilterPermiss (pMultiIndex->pIndex[0].i4_SLongValue,
                                      (*(tMacAddr *) pMultiIndex->pIndex[1].
                                       pOctetStrValue->pu1_OctetList),
                                      (*(tMacAddr *) pMultiIndex->pIndex[2].
                                       pOctetStrValue->pu1_OctetList),
                                      pMultiData->i4_SLongValue));

}

INT4
Dot1dFilterSrcMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dFilterSrcMask (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[1].
                                          pOctetStrValue->pu1_OctetList),
                                         (*(tMacAddr *) pMultiIndex->pIndex[2].
                                          pOctetStrValue->pu1_OctetList),
                                         (*(tMacAddr *) pMultiData->
                                          pOctetStrValue->pu1_OctetList)));

}

INT4
Dot1dFilterDstMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dFilterDstMask (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[1].
                                          pOctetStrValue->pu1_OctetList),
                                         (*(tMacAddr *) pMultiIndex->pIndex[2].
                                          pOctetStrValue->pu1_OctetList),
                                         (*(tMacAddr *) pMultiData->
                                          pOctetStrValue->pu1_OctetList)));

}

INT4
Dot1dFilterPermissTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dFilterPermiss (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiIndex->pIndex[1].
                                          pOctetStrValue->pu1_OctetList),
                                         (*(tMacAddr *) pMultiIndex->pIndex[2].
                                          pOctetStrValue->pu1_OctetList),
                                         pMultiData->i4_SLongValue));

}

INT4
GetNextIndexDot1dMcastTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexDot1dMcastTable ((tMacAddr *) pNextMultiIndex->
                                             pIndex[0].pOctetStrValue->
                                             pu1_OctetList,
                                             &(pNextMultiIndex->pIndex[1].
                                               i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexDot1dMcastTable
            (*(tMacAddr *) pFirstMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList,
             (tMacAddr *) pNextMultiIndex->pIndex[0].pOctetStrValue->
             pu1_OctetList, pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pNextMultiIndex->pIndex[0].pOctetStrValue->i4_Length = 6;
    return SNMP_SUCCESS;
}

INT4
Dot1dMlistNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dMcastTable ((*(tMacAddr *) pMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList),
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[1].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
Dot1dMcastMacaddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dMcastTable ((*(tMacAddr *) pMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList),
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
Dot1dMcastPermissGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceDot1dMcastTable ((*(tMacAddr *) pMultiIndex->
                                                  pIndex[0].pOctetStrValue->
                                                  pu1_OctetList),
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetDot1dMcastPermiss ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                      pOctetStrValue->pu1_OctetList),
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
Dot1dMcastPermissSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetDot1dMcastPermiss ((*(tMacAddr *) pMultiIndex->pIndex[0].
                                      pOctetStrValue->pu1_OctetList),
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
Dot1dMcastPermissTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2Dot1dMcastPermiss (pu4Error,
                                        (*(tMacAddr *) pMultiIndex->pIndex[0].
                                         pOctetStrValue->pu1_OctetList),
                                        pMultiIndex->pIndex[1].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}
