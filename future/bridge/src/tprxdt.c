/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tprxdt.c,v 1.2 2007/02/01 14:44:36 iss Exp $
 *
 * Description:This file contains Routines to to handle       
 *             the data frame received.                   
 *
 *******************************************************************/
#include "brginc.h"
/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BridgeIngressFilter                        */
/*                                                                           */
/*    Description               : This function does the Ingress filtering   */
/*                                                                           */
/*    Input(s)                  : pMsg                                       */
/*                                u2IfIndex                                  */
/*                                u2protocol                                 */
/*                                pu4FdbId                                   */
/*                                pMsgAttr                                   */
/*                                u1FrameType                                */
/*                                i4IsReservAddr                             */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_SUCCESS                             */
/*                                BRIDGE_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
INT4
BridgeIngressFilter (tCRU_BUF_CHAIN_HEADER * pMsg, UINT2 u2ifIndex,
                     UINT2 u2Protocol, UINT4 *pu4FdbId, tMSG_ATTR * pMsgAttr,
                     UINT1 u1FrameType, INT4 i4IsReservAddr)
{
    tTMO_INTERFACE      ifaceId;
    UINT4               u4PortNo;

    UNUSED_PARAM (u1FrameType);

    BRG_TRC_ARG1 (BRG_TRC_FLAG, BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                  BRG_MOD_NAME,
                  "Check For Ingress Filtering in interface %d \n", u2ifIndex);

    /* To generate the mask for the protocol */
    pMsgAttr->u2Protocol = GenerateProtoFilterMask (u2Protocol);

   /************************************
    *** Extraction of interface ID   ***
    ************************************/

    ifaceId.u1_InterfaceNum = (UINT1) u2ifIndex;
    ifaceId.u2_SubReferenceNum = 0;
    ifaceId.u1_InterfaceType =
        base.portEntry[u2ifIndex].portId.u1_InterfaceType;

   /********************************************************
    *** If RECD_WITH_CRC then the same CRC can be used   ***
    *** while transmitting the frame                     ***
    *** otherwise the CRC is computed while forwarding   ***
    ********************************************************/
    pMsgAttr->u2Length = (UINT2) BRG_BUF_GET_CHAIN_VALIDBYTECOUNT (pMsg);

    if ((ifaceId.u1_InterfaceType == CRU_ENET_PHYS_INTERFACE_TYPE) ||
        (ifaceId.u1_InterfaceType == CFA_LAGG))
    {
      /************************************************************************** 
       * The crc bytes should be removed only if the ethernet driver passes     *
       * the frame to the bridge module with crc. This is not the case with     *
       * the recent ethernet cards, hence this is kept under compilation switch,*
       * Check the driver specifications, define this switch during compilation,*
       * if required                                                            *
       **************************************************************************/

    }
    else
    {
      /****************************************************
       *** Rxed From Serial Interface.                  ***
       *** Calculate the Length by ignoring the Enet    ***
       *** Header                                       ***
       ****************************************************/
        pMsgAttr->u2Length =
            (UINT2) (pMsgAttr->u2Length - CRU_ENET_HEADER_SIZE);

        if (base.u1CrcStatus == BRG_WITH_CRC)
        {
            pMsgAttr->u2Length = (UINT2) (pMsgAttr->u2Length - 4);
        }
    }

    u4PortNo = TpGetPortIfCanReceiveMsg (&ifaceId, pu4FdbId, pMsgAttr);

    if (u4PortNo != 0xffff)
    {

        if (IS_BRGIF_OPERADMIN_UP (u4PortNo))
        {

#ifdef SYS_HC_WANTED
            /* set the overflow counter if the port is HC_PORT and LSB counter
             * has overflown 
             * Otherwise increment the LSB counter itself
             */
            BRG_INCR_HC_OR_TP_PORT_IN_FRAMES (u4PortNo);
#else /* SYS_HC_WANTED */
            tpInfo.portEntry[u4PortNo].u4TpIn++;
#endif /* SYS_HC_WANTED */

            if (pMsgAttr->u2Length > tpInfo.portEntry[u4PortNo].u2Maxinfo)
            {
                base.portEntry[u4PortNo].u4NoMtuExceedDiscard++;

                /* Counter incremented for filtering due to frame exceeded size */
#ifdef SYS_HC_WANTED
                BRG_INCR_HC_OR_TP_FILTER_IN_DISCARDS (u4PortNo);
#else /* SYS_HC_WANTED */
                BRG_INCR_PORT_FILTER_IN_DISCARDS (u4PortNo);
#endif /* SYS_HC_WANTED */

                BRG_TRC (BRG_TRC_FLAG,
                         BRG_DATA_PATH_TRC | BRG_BUFFER_TRC |
                         BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                         "Frame size exceeded allowed Maximum Info \n");

                return BRIDGE_FAILURE;
            }                    /* MTU exceeded check */

            if (!((pMsgAttr->u2Protocol) &
                  (tpInfo.portEntry[u4PortNo].u2ProtocolFilterMask))
                && (i4IsReservAddr == CFA_FAILURE))
            {

                BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                         BRG_MOD_NAME, "Incoming Pkt is NOT filtered \n");

                return BRIDGE_SUCCESS;

            }
            else
            {
            /****************************************************
             *** This Protocol is filtered in this interface  ***
             *** Release buffer.                              ***
             ****************************************************/
#ifdef  SYS_HC_WANTED

                /* Check whether the protocol filtering and increment counter */
                if (IS_BRG_PROTO_FILTERED (pMsgAttr->u2Protocol, u4PortNo))
                {

                    BRG_INCR_HC_OR_TP_PROTOCOL_FILTER_DISCARDS (u4PortNo);
                    BRG_INCR_HC_OR_TP_FILTER_IN_DISCARDS (u4PortNo);

                }
                else
                {                /* in disc */
                    /* Counter incremented for filtering due to 
                     * reserved group address
                     */
                    BRG_INCR_HC_OR_TP_FILTER_IN_DISCARDS (u4PortNo);
                }
#else /* SYS_HC_WANTED */

                /* Check whether the protocol filtering and increment counter */
                if (IS_BRG_PROTO_FILTERED (pMsgAttr->u2Protocol, u4PortNo))
                {

                    BRG_INCR_PORT_PROTOCOL_FILTER_DISCARDS (u4PortNo);
                    BRG_INCR_PORT_FILTER_IN_DISCARDS (u4PortNo);
                }
                else
                {

                    /* Counter incremented for filtering due to 
                     * reserved group address
                     */

                    BRG_INCR_PORT_FILTER_IN_DISCARDS (u4PortNo);
                }
#endif /* SYS_HC_WANTED */

                BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                         BRG_MOD_NAME, "Incoming Pkt  is filtered \n");

                return BRIDGE_FAILURE;

            }
        }
        else
        {

            /* Counter incremented for filtering due to port down */
#ifdef SYS_HC_WANTED

            BRG_INCR_HC_OR_TP_FILTER_IN_DISCARDS (u4PortNo);

#else /* SYS_HC_WANTED */

            BRG_INCR_PORT_FILTER_IN_DISCARDS (u4PortNo);

#endif /* SYS_HC_WANTED */

            BRG_TRC (BRG_TRC_FLAG,
                     BRG_DATA_PATH_TRC | BRG_BUFFER_TRC | BRG_ALL_FAILURE_TRC,
                     BRG_MOD_NAME, "Port is DOWN! \n");

            return BRIDGE_FAILURE;
        }
    }
    else
    {

        /* Counter incremented for filtering due to invalid port */

#ifdef SYS_HC_WANTED
        BRG_INCR_HC_OR_TP_FILTER_IN_DISCARDS (u2ifIndex);
#else /* SYS_HC_WANTED */
        BRG_INCR_PORT_FILTER_IN_DISCARDS (u2ifIndex);
#endif /* SYS_HC_WANTED */

        BRG_TRC (BRG_TRC_FLAG, BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME, "Port is invalid \n");

        return BRIDGE_FAILURE;
    }

}

/******************************************************************/
/*
 * Functionality:-
 *     This routine checks whether a data frame of certain size and 
 *     protocol  can be received thru a particular interface.
 *     It also codes the FDB_ID (p_u4_fdb_id).
 *
 * Formal Parameters:-
 *     1. The interface id from which the frame is received.
 *     2. pointer to the fdb_id
 *     3. Attributes
 *
 * Global Variables Used:-
 *     base, tpInfo, grp_tab.
 *
 * Return Values:-
 *    1. The logical port number on success.
 *       pU4Fdbid will contain a FDB code for the interface.
 *    2. 0xffff otherwise.
 */

UINT4
TpGetPortIfCanReceiveMsg (tTMO_INTERFACE * pIfaceId, UINT4 *u4Fdbid,
                          tMSG_ATTR * msgAttr)
{
    UINT4               u4Port;

    *u4Fdbid = 0x00;

    msgAttr->bMember = FALSE;
    msgAttr->u2Index = 0;

    if ((u4Port = GetPortFromIfaceId (pIfaceId)) == 0xffff)
    {
        return 0XFFFF;
    }
    else
    {
        CODE_TYPE_IN_FDBID (*u4Fdbid, FLAT_ENTRY_FDB);
        CODE_LOGICAL_PORT_IN_FDBID (*u4Fdbid, u4Port);
        msgAttr->u2LogicalPortNo = (UINT2) u4Port;
        return u4Port;
    }
}

/******************************************************************/
/*
 * Functionality:-
 *     This routine generates the protocol filter mask for the
 * given protocol based on the values indicated in the MIB. Only for the
 * IP protocol is supported.
 * Formal Parameters:-
 *     1. Protocol value given by the CFA
 *
 *
 * Return Values:-
 *    1. Protocol Filter Mask 
 */
UINT2
GenerateProtoFilterMask (UINT2 u2Protocol)
{
    UINT2               u2ProtocolMask;

    /* To generate the mask for the protocol */
    switch (u2Protocol)
    {
        case BRG_IP_PROTOCOL:
            u2ProtocolMask = 0x0004;
            break;

        case BRG_PPPOE_DISC_PROTOCOL:
        case BRG_PPPOE_SESSION_PROTOCOL:
            u2ProtocolMask = BRG_PPPOE;
            break;

        default:
            u2ProtocolMask = 0x0000;
            break;
    }
    return (u2ProtocolMask);
}

/*************************** END OF FILE(tprxdt.c) ***********************/
