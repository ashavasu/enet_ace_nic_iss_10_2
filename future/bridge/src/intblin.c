/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: intblin.c,v 1.2 2007/02/01 14:44:36 iss Exp $
 *
 * Description:this file contains the initialization routines
 *             for tp bridging                               
 *
 *******************************************************************/
#include "brginc.h"

/*********************************************************************/
/*
 * Functionality         : Initializes the tpInfo.portEntry
 * 
 * Formal Parameters     : Port Number
 * 
 * Global Variables Used : tpInfo
 * 
 * Return Values         : None
 *
 */

void
InInitTpPort (UINT4 u4PortNo)
{
    tpInfo.portEntry[u4PortNo].u4TpIn = 0;
    tpInfo.portEntry[u4PortNo].u4TpOut = 0;
    tpInfo.portEntry[u4PortNo].u4NoProtoInDiscard = 0;
    tpInfo.portEntry[u4PortNo].u4NoFilterInDiscard = 0;
    /* Used for HC_PORTS */
    tpInfo.portEntry[u4PortNo].u4TpInOverflow = 0;
    tpInfo.portEntry[u4PortNo].u4TpOutOverflow = 0;
    tpInfo.portEntry[u4PortNo].u4FilterInDiscardOverflow = 0;
    tpInfo.portEntry[u4PortNo].u4ProtoInDiscardOverflow = 0;

    tpInfo.portEntry[u4PortNo].u2ProtocolFilterMask = DEF_PRO_MASK;
    BRG_TRC_ARG1 (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
                  "Initialization of Transparent Bridge port information "
                  "for port %d is completed \n ", u4PortNo);
}

/*********************************************************************/
/*
 * Functionality         : Initializes the Transparent Bridge data
 *                         Structures.
 * 
 * Formal Parameters     : None
 * 
 * Global Variables Used : tpInfo
 * 
 * Return Values         : None
 *
 */
void
InInitTp (void)
{
    UINT4               u4PortNo;

    BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
             " Starting the Initialization of the transparent "
             "Bridging Port Table \n");

    for (u4PortNo = 1; u4PortNo <= BRG_MAX_PHY_PLUS_LOG_PORTS; u4PortNo++)
    {
        InInitTpPort (u4PortNo);
    }
    tpInfo.u4NoLearnedEntryDiscard = 0;
    tpInfo.u4FdbAgeingTime = NORMAL_AGEOUT_TIME;
#ifdef NPAPI_WANTED
    FsBrgSetAgingTime (NORMAL_AGEOUT_TIME);
#endif
    BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
             " Initializing for the transparent Bridging Port Table "
             "info. completed  \n");

}

/*****************************************************************************/
/*   Function name              : InAllocMemoryForTables                     */
/*                                                                           */
/*                                                                           */
/*    Description               : This function allocates strucures          */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRG_SUCCESS on success                     */
/*                                                                           */
/*****************************************************************************/
INT4
InAllocMemoryForTables (void)
{
    base.portEntry = (tBM_BASE_PORT_ENTRY *) & bmBasePortEntry[0];
    tpInfo.portEntry = (tTP_PORT_ENTRY *) & tpPortEntry[0];
    return BRIDGE_SUCCESS;
}

/*****************************************************************************/
/*   Function name              : StInitBaseStap                             */
/*                                                                           */
/*    Description               : This function initializes all base port    */
/*                                Information                                */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
void
StInitBaseStap (void)
{
    UINT4               u4Index;
    BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
             " Starting initialization of all base port information "
             "of the FutureTB/STP \n ");

    for (u4Index = 1; u4Index <= BRG_MAX_PHY_PLUS_LOG_PORTS; u4Index++)
    {
        base.portEntry[u4Index].u1FilterNo = 0;
        base.portEntry[u4Index].portId.u1_InterfaceNum = (UINT1) u4Index;
/* Handled for ethernet and PPP */
        base.portEntry[u4Index].portId.u2_SubReferenceNum = 0;
        base.portEntry[u4Index].u1BroadcastStatus = BROADCAST_UP;
        tpInfo.portEntry[u4Index].u2ProtocolFilterMask = 0x0000;

        InInitTpPort (u4Index);
    }
    BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC, BRG_MOD_NAME,
             " Initialization of all base port information "
             "of the FutureTB/STP completed \n ");

    return;
}

/*************************** END OF FILE(intblin.c) ***************************/
