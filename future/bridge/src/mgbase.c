/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mgbase.c,v 1.2 2007/02/01 14:44:36 iss Exp $
 *
 * Description: SNMP low level routines for dot1dbaseGroup  
 *
 *******************************************************************/
#include "brginc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : GetPortFromIfaceId                         */
/*                                                                           */
/*    Description               : Get Port number from the interface ID.     */
/*                                                                           */
/*    Input(s)                  :  pIfaceId - Interface ID                   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   :  Valid port number or 0xFFFF -             */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

UINT4
GetPortFromIfaceId (tTMO_INTERFACE * pIfaceId)
{
    UINT4               u4I;

    for (u4I = 1; u4I <= BRG_MAX_PHY_PLUS_LOG_PORTS; u4I++)
    {
        if ((base.portEntry[u4I].portId.u1_InterfaceType
             == pIfaceId->u1_InterfaceType)
            &&
            (base.portEntry[u4I].portId.u1_InterfaceNum
             == pIfaceId->u1_InterfaceNum)
            &&
            (base.portEntry[u4I].portId.u2_SubReferenceNum
             == pIfaceId->u2_SubReferenceNum)
            && (base.portEntry[u4I].u1OperStatus == UP))
        {

            return u4I;
        }
    }
    return 0xffff;
}

/*****************************************************************************/
/*                                                                           */
/*    Function name             : SnIsIfaceIdOk                              */
/*                                                                           */
/*    Description               :  This function checks if Interface Id      */
/*                                 is correct.                               */
/*                                                                           */
/*    Input(s)                  :  pIfaceId                                  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK                                         */
/*                                BRIDGE_NOT_OK                                     */
/*                                                                           */
/*****************************************************************************/
INT4
SnIsIfaceIdOk (tTMO_INTERFACE * pIfaceId)
{
    INT2                i2RetVal = BRIDGE_NOT_OK;
    UINT4               u4Port = 0;

    switch (pIfaceId->u1_InterfaceType)
    {
        case CRU_ENET_PHYS_INTERFACE_TYPE:
        case CFA_LAGG:
        case CRU_PPP_PHYS_INTERFACE_TYPE:
            u4Port = GetPortFromIfaceId (pIfaceId);
            if (u4Port != 0xffff)
            {
                if (u4Port <= BRG_MAX_PHY_PLUS_LOG_PORTS)
                    i2RetVal = BRIDGE_OK;
                else
                    i2RetVal = BRIDGE_NOT_OK;
            }
            break;

        case CRU_FRMRL_PHYS_INTERFACE_TYPE:
            /* FALL THROUGH */

        case CRU_X25_PHYS_INTERFACE_TYPE:
            if (GetPortFromIfaceId (pIfaceId) == 0xffff)
            {
                if ((pIfaceId->u2_SubReferenceNum == 0xffff)
                    && BM_IS_VALID_SERIAL_INTERFACE (pIfaceId->u1_InterfaceNum))
                {
                    i2RetVal = BRIDGE_NOT_OK;
                }
            }
            else
            {
                i2RetVal = BRIDGE_NOT_OK;
            }
            break;
    }
    return i2RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : SnCanCreateThisIf                          */
/*                                                                           */
/*    Description               : Check whether the interface can be created */
/*                                                                           */
/*    Input(s)                  :  pIfaceId - Interface Id                   */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK                                         */
/*                                BRIDGE_NOT_OK                                     */
/*                                                                           */
/*****************************************************************************/

INT4
SnCanCreateThisIf (tTMO_INTERFACE * pIfaceId)
{
    INT2                i2RetVal = BRIDGE_NOT_OK;

    switch (pIfaceId->u1_InterfaceType)
    {
        case CRU_ENET_PHYS_INTERFACE_TYPE:
        case CFA_LAGG:
            if ((pIfaceId->u1_InterfaceNum < CRU_MAX_ENET_INTERFACES)
                && (pIfaceId->u2_SubReferenceNum == 0))
                i2RetVal = BRIDGE_OK;
            break;

        case CRU_PPP_PHYS_INTERFACE_TYPE:
            /* FALL THROUGH */

        case CRU_FRMRL_PHYS_INTERFACE_TYPE:
            /* FALL THROUGH */

        case CRU_X25_PHYS_INTERFACE_TYPE:
            if (pIfaceId->u1_InterfaceNum <= (UINT1) CRU_MAX_WAN_INTERFACES)
                i2RetVal = BRIDGE_OK;
            break;
    }
    return i2RetVal;
}

/*************************** END OF FILE(mgbase.c) ***************************/
