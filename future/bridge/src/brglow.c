/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: brglow.c,v 1.7 2012/08/08 10:32:25 siva Exp $
 *
 * Description: Definitions of SNMP low level functions
 *
 *******************************************************************/
#include "brginc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dBridgeSystemControl
 Input       :  The Indices

                The Object 
                retValDot1dBridgeSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBridgeSystemControl (INT4 *pi4RetValDot1dBridgeSystemControl)
{

    if (BRG_SYSTEM_CONTROL == (UINT1) BRG_START)
    {
        *pi4RetValDot1dBridgeSystemControl = (INT4) BRG_SNMP_START;
    }
    else
    {
        *pi4RetValDot1dBridgeSystemControl = (INT4) BRG_SNMP_SHUTDOWN;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBaseBridgeAddress
 Input       :  The Indices

                The Object 
                retValDot1dBaseBridgeAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseBridgeAddress (tMacAddr * pRetValDot1dBaseBridgeAddress)
{

    BRG_MEM_CPY ((UINT1 *) *pRetValDot1dBaseBridgeAddress,
                 base.brgMacAddr, ETHERNET_ADDR_SIZE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBaseNumPorts
 Input       :  The Indices

                The Object 
                retValDot1dBaseNumPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseNumPorts (INT4 *pi4RetValDot1dBaseNumPorts)
{
    *pi4RetValDot1dBaseNumPorts = BRG_MAX_PHY_PLUS_LOG_PORTS;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBaseType
 Input       :  The Indices

                The Object 
                retValDot1dBaseType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseType (INT4 *pi4RetValDot1dBaseType)
{

    *pi4RetValDot1dBaseType = TRANSPARENT_BRIDGE;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1dBasePortTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dBasePortTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dBasePortTable (INT4 i4Dot1dBasePort)
{

    if (((i4Dot1dBasePort >= 1) &&
         (i4Dot1dBasePort <= BRG_MAX_PHY_PLUS_LOG_PORTS)) &&
        (base.portEntry[i4Dot1dBasePort].u1AdminStatus != BRIDGE_INVALID))
        return SNMP_SUCCESS;
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dBasePortTable
 Input       :  The Indices
                Dot1dBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDot1dBasePortTable (INT4 *pi4Dot1dBasePort)
{
    tBOOLEAN            bFound = FALSE;
    UINT4               u4PortNo;
    UINT1               u1InterfaceNum = 0xff;

    for (u4PortNo = (UINT4) gu2BrgStartPortInd;
         u4PortNo != BRG_INVALID_PORT_INDEX;
         u4PortNo = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4PortNo))
    {
        if (u1InterfaceNum > base.portEntry[u4PortNo].portId.u1_InterfaceNum)
        {
            bFound = TRUE;
            *pi4Dot1dBasePort = base.portEntry[u4PortNo].portId.u1_InterfaceNum;

            u1InterfaceNum = base.portEntry[u4PortNo].portId.u1_InterfaceNum;
        }
    }

    if (bFound == TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dBasePortTable
 Input       :  The Indices
                Dot1dBasePort
                nextDot1dBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dBasePortTable (INT4 i4Dot1dBasePort,
                                   INT4 *pi4NextDot1dBasePort)
{
    tBOOLEAN            bFound = FALSE;
    UINT1               u1InterfaceNum = 0xff;
    UINT4               u4PortNo;

    if (i4Dot1dBasePort < 0)
    {
        return SNMP_FAILURE;
    }
    for (u4PortNo = (UINT4) gu2BrgStartPortInd;
         u4PortNo != BRG_INVALID_PORT_INDEX;
         u4PortNo = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4PortNo))
    {
        if ((base.portEntry[u4PortNo].portId.u1_InterfaceNum >
             i4Dot1dBasePort)
            && (u1InterfaceNum >
                base.portEntry[u4PortNo].portId.u1_InterfaceNum))
        {
            bFound = TRUE;
            u1InterfaceNum = base.portEntry[u4PortNo].portId.u1_InterfaceNum;
        }
    }

    *pi4NextDot1dBasePort = u1InterfaceNum;

    if (bFound == TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dBasePortIfIndex
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dBasePortIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortIfIndex (INT4 i4Dot1dBasePort,
                            INT4 *pi4RetValDot1dBasePortIfIndex)
{
    *pi4RetValDot1dBasePortIfIndex =
        base.portEntry[i4Dot1dBasePort].portId.u1_InterfaceNum;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortCircuit
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dBasePortCircuit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortCircuit (INT4 i4Dot1dBasePort,
                            tSNMP_OID_TYPE * pRetValDot1dBasePortCircuit)
{
    /*
     * Currently only Ethernet interfaces are supported and hence returning
     * the OID {0, 0}.
     */

    UNUSED_PARAM (i4Dot1dBasePort);

    pRetValDot1dBasePortCircuit->u4_Length = 2;
    MEMSET (pRetValDot1dBasePortCircuit->pu4_OidList, 0, 2 * sizeof (UINT4));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortDelayExceededDiscards
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dBasePortDelayExceededDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortDelayExceededDiscards (INT4 i4Dot1dBasePort,
                                          UINT4
                                          *pu4RetValDot1dBasePortDelayExceededDiscards)
{
#ifdef NPAPI_WANTED
/* Counter for frames discarded due to delay exceeded */
    if (CfaFsHwGetStat
        (i4Dot1dBasePort, NP_STAT_DOT1D_BASE_PORT_DLY_EXCEEDED_DISCARDS,
         pu4RetValDot1dBasePortDelayExceededDiscards) == FNP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
#else /* NPAPI_WANTED */

    *pu4RetValDot1dBasePortDelayExceededDiscards =
        base.portEntry[i4Dot1dBasePort].u4NoTransitDelayDiscard;

    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortMtuExceededDiscards
 Input       :  The Indices
                Dot1dBasePort

                The Object 
                retValDot1dBasePortMtuExceededDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortMtuExceededDiscards (INT4 i4Dot1dBasePort,
                                        UINT4
                                        *pu4RetValDot1dBasePortMtuExceededDiscards)
{
#ifdef NPAPI_WANTED
/* Counter for frames discarded due to MTU exceeded */
    if (CfaFsHwGetStat
        (i4Dot1dBasePort, NP_STAT_DOT1D_BASE_PORT_MTU_EXCEEDED_DISCARDS,
         pu4RetValDot1dBasePortMtuExceededDiscards) == FNP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }

#else
    *pu4RetValDot1dBasePortMtuExceededDiscards =
        base.portEntry[i4Dot1dBasePort].u4NoMtuExceedDiscard;
    return SNMP_SUCCESS;
#endif
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dTpLearnedEntryDiscards
 Input       :  The Indices

                The Object 
                retValDot1dTpLearnedEntryDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpLearnedEntryDiscards (UINT4 *pu4RetValDot1dTpLearnedEntryDiscards)
{
#ifdef NPAPI_WANTED
    if (FsBrgGetLearnedEntryDisc (pu4RetValDot1dTpLearnedEntryDiscards)
        == FNP_SUCCESS)
        return SNMP_SUCCESS;
    else
        return SNMP_FAILURE;

#else
    *pu4RetValDot1dTpLearnedEntryDiscards = tpInfo.u4NoLearnedEntryDiscard;
    return SNMP_SUCCESS;

#endif
}

/****************************************************************************
 Function    :  nmhGetDot1dTpAgingTime
 Input       :  The Indices

                The Object 
                retValDot1dTpAgingTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpAgingTime (INT4 *pi4RetValDot1dTpAgingTime)
{
    *pi4RetValDot1dTpAgingTime = FDB_TIME_TO_MGMT (tpInfo.u4FdbAgeingTime);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDot1dBridgeSystemControl
 Input       :  The Indices

                The Object 
                setValDot1dBridgeSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dBridgeSystemControl (INT4 i4SetValDot1dBridgeSystemControl)
{

    UINT4               u4RetSts = BRG_SUCCESS;

    if (BRG_SYSTEM_CONTROL == (UINT1) i4SetValDot1dBridgeSystemControl)
    {
        return (INT1) SNMP_SUCCESS;
    }
    if (i4SetValDot1dBridgeSystemControl == (INT4) BRG_SNMP_START)
    {
        u4RetSts = BRG_SEND_EVENT (SELF, BRIDGE_TMR_TASK_NAME, BRG_START_EVENT);
        if (u4RetSts != BRG_SUCCESS)
        {
            BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC,
                     BRG_MOD_NAME,
                     "Sending the BRG_START_EVENT to BrTmrTask Failed !\n ");
            return (INT1) SNMP_FAILURE;
        }
    }
    else
    {
        u4RetSts = BRG_SEND_EVENT (SELF, BRIDGE_TMR_TASK_NAME, BRG_STOP_EVENT);
        if (u4RetSts != BRG_SUCCESS)
        {
            BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC,
                     BRG_MOD_NAME,
                     "Sending the BRG_STOP_EVENT to BrTmrTask Failed !\n ");
            return (INT1) SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dTpAgingTime
 Input       :  The Indices

                The Object 
                setValDot1dTpAgingTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dTpAgingTime (INT4 i4SetValDot1dTpAgingTime)
{
#ifdef NPAPI_WANTED
    /* Sets aging time in hardware */
    if (FsBrgSetAgingTime (MGMT_TO_FDB_TIME (i4SetValDot1dTpAgingTime))
        != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    tpInfo.u4FdbAgeingTime = MGMT_TO_FDB_TIME (i4SetValDot1dTpAgingTime);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Dot1dBridgeSystemControl
 Input       :  The Indices

                The Object 
                testValDot1dBridgeSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dBridgeSystemControl (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValDot1dBridgeSystemControl)
{

    if ((i4TestValDot1dBridgeSystemControl != (INT4) BRG_SNMP_START) &&
        (i4TestValDot1dBridgeSystemControl != (INT4) BRG_SNMP_SHUTDOWN))
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dTpAgingTime
 Input       :  The Indices

                The Object 
                testValDot1dTpAgingTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dTpAgingTime (UINT4 *pu4ErrorCode, INT4 i4TestValDot1dTpAgingTime)
{
    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }
    if ((i4TestValDot1dTpAgingTime >= (INT4) BRG_MIN_AGEOUT_TIME) &&
        (i4TestValDot1dTpAgingTime <= (INT4) BRG_MAX_AGEOUT_TIME))
    {
        return (INT1) SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
}

/* LOW LEVEL Routines for Table : Dot1dTpFdbTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dTpFdbTable
 Input       :  The Indices
                Dot1dTpFdbAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dTpFdbTable (tMacAddr Dot1dTpFdbAddress)
{
    tFORWARDING_ENTRY  *pFdb;

    if ((pFdb =
         (tFORWARDING_ENTRY *) HashSearchNode (pSHtab,
                                               (tMacAddr *) Dot1dTpFdbAddress))
        == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }
    else
    {
        return (INT1) SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dTpFdbTable
 Input       :  The Indices
                Dot1dTpFdbAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDot1dTpFdbTable (tMacAddr * pDot1dTpFdbAddress)
{
    tFORWARDING_ENTRY  *pTmpPtr = NULL;
    tFORWARDING_ENTRY  *pPtr = NULL;
    tMacAddr            mac;
    UINT4               u4Hindex;

    BRG_MEM_SET (mac, 0xff, ETHERNET_ADDR_SIZE);
    if (BRG_FDB_HASH_TBL != NULL)
    {
        for (u4Hindex = 0; u4Hindex < HASH_LIST_SIZE; u4Hindex++)
        {
            TMO_HASH_Scan_Bucket (pSHtab, u4Hindex, pTmpPtr,
                                  tFORWARDING_ENTRY *)
            {
                if (BuCompareMacaddr (mac, pTmpPtr->destAddr) == GREATER)

                {

                    pPtr = pTmpPtr;
                    BRG_MEM_CPY (mac, pTmpPtr->destAddr, ETHERNET_ADDR_SIZE);
                }
            }
        }
    }
    if (pPtr == NULL)
    {
#ifdef NPAPI_WANTED
        /* if FDB is maintained in S/W, the hardware API
         * Can be made to return failure always
         */
        /* Get the MAC address of the first FDB entry */
        if (FsBrgGetFdbEntryFirst (*pDot1dTpFdbAddress) == FNP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }
    else
    {
        BRG_MEM_CPY ((UINT1 *) *pDot1dTpFdbAddress, pPtr->destAddr,
                     ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dTpFdbTable
 Input       :  The Indices
                Dot1dTpFdbAddress
                nextDot1dTpFdbAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dTpFdbTable (tMacAddr Dot1dTpFdbAddress,
                                tMacAddr * pNextDot1dTpFdbAddress)
{
    tFORWARDING_ENTRY  *pFdb = NULL;
    tFORWARDING_ENTRY  *pTmpFdb = NULL;
    tMacAddr            mac;
    UINT4               u4Hindex;

    BRG_MEM_SET (mac, 0xff, ETHERNET_ADDR_SIZE);

    if (BRG_FDB_HASH_TBL != NULL)
    {
        for (u4Hindex = 0; u4Hindex < HASH_LIST_SIZE; u4Hindex++)
        {
            TMO_HASH_Scan_Bucket (pSHtab, u4Hindex, pTmpFdb,
                                  tFORWARDING_ENTRY *)
            {
                if ((BuCompareMacaddr (Dot1dTpFdbAddress,
                                       pTmpFdb->destAddr) == LESSER)
                    && (BuCompareMacaddr (pTmpFdb->destAddr, mac) == LESSER))
                {
                    BRG_MEM_CPY (mac, pTmpFdb->destAddr, ETHERNET_ADDR_SIZE);
                    pFdb = pTmpFdb;
                }
            }
        }
    }

    if (pFdb == NULL)
    {
#ifdef NPAPI_WANTED
        if (FsBrgGetFdbNextEntry (Dot1dTpFdbAddress,
                                  *pNextDot1dTpFdbAddress) == FNP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
#endif
        return SNMP_FAILURE;
    }
    else
    {
        BRG_MEM_CPY ((UINT1 *) *pNextDot1dTpFdbAddress, pFdb->destAddr,
                     ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dTpFdbPort
 Input       :  The Indices
                Dot1dTpFdbAddress

                The Object 
                retValDot1dTpFdbPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpFdbPort (tMacAddr Dot1dTpFdbAddress, INT4 *pi4RetValDot1dTpFdbPort)
{
    tFORWARDING_ENTRY  *pFdb;
    UINT1               u1PortNo;

    if ((pFdb =
         (tFORWARDING_ENTRY *) HashSearchNode (pSHtab,
                                               (tMacAddr *) Dot1dTpFdbAddress))
        != NULL)
    {
        u1PortNo = (UINT1) GET_LOGICAL_PORT_FROM_FDBID (pFdb->u4Fdbid);
        if (IS_UNASSIGNED_FDB (pFdb->u4Fdbid))
        {
      /** This entry corresponds to the interface-id who is not
          assigned with a logical port **/
      /** To get the logical port, the direct way of getting the value is used **/
            *pi4RetValDot1dTpFdbPort =
                CONV_TO_LOGICAL_PORT (GET_TYPE_FROM_FDBID (fdbid),
                                      GET_LOGICAL_PORT_FROM_FDBID (pFdb->
                                                                   u4Fdbid));
            return SNMP_SUCCESS;
        }

        *pi4RetValDot1dTpFdbPort =
            CONV_TO_LOGICAL_PORT (base.portEntry[u1PortNo].portId.
                                  u1_InterfaceType,
                                  base.portEntry[u1PortNo].portId.
                                  u1_InterfaceNum);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpFdbStatus
 Input       :  The Indices
                Dot1dTpFdbAddress

                The Object 
                retValDot1dTpFdbStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpFdbStatus (tMacAddr Dot1dTpFdbAddress,
                        INT4 *pi4RetValDot1dTpFdbStatus)
{
    tFORWARDING_ENTRY  *pFdb;

    if ((pFdb =
         (tFORWARDING_ENTRY *) HashSearchNode (pSHtab,
                                               (tMacAddr *) Dot1dTpFdbAddress))
        != NULL)
    {
        *pi4RetValDot1dTpFdbStatus = pFdb->u1Status;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Dot1dTpPortTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dTpPortTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dTpPortTable (INT4 i4Dot1dTpPort)
{

    if (((i4Dot1dTpPort >= 1) &&
         (i4Dot1dTpPort <= BRG_MAX_PHY_PLUS_LOG_PORTS)) &&
        (base.portEntry[i4Dot1dTpPort].u1AdminStatus != BRIDGE_INVALID))
        return SNMP_SUCCESS;
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dTpPortTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDot1dTpPortTable (INT4 *pi4Dot1dTpPort)
{
    tBOOLEAN            bFound = FALSE;
    UINT4               u4PortNo;
    UINT1               u1_InterfaceNum = 0xff;

    for (u4PortNo = (UINT4) gu2BrgStartPortInd;
         u4PortNo != BRG_INVALID_PORT_INDEX;
         u4PortNo = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4PortNo))
    {
        if (u1_InterfaceNum > base.portEntry[u4PortNo].portId.u1_InterfaceNum)
        {
            bFound = TRUE;
            u1_InterfaceNum = base.portEntry[u4PortNo].portId.u1_InterfaceNum;
        }
    }

    *pi4Dot1dTpPort = u1_InterfaceNum;
    if (bFound == TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dTpPortTable
 Input       :  The Indices
                Dot1dTpPort
                nextDot1dTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dTpPortTable (INT4 i4Dot1dTpPort, INT4 *pi4NextDot1dTpPort)
{
    tBOOLEAN            bFound = FALSE;
    UINT1               u1InterfaceNum = 0xff;
    UINT4               u4PortNo;

    if (i4Dot1dTpPort < 0)
    {
        return SNMP_FAILURE;
    }
    for (u4PortNo = (UINT4) gu2BrgStartPortInd;
         u4PortNo != BRG_INVALID_PORT_INDEX;
         u4PortNo = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4PortNo))
    {
        if ((base.portEntry[u4PortNo].portId.u1_InterfaceNum >
             i4Dot1dTpPort)
            && (u1InterfaceNum >
                base.portEntry[u4PortNo].portId.u1_InterfaceNum))
        {
            bFound = TRUE;
            u1InterfaceNum = base.portEntry[u4PortNo].portId.u1_InterfaceNum;
        }
    }

    *pi4NextDot1dTpPort = u1InterfaceNum;

    if (bFound == TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dTpPortMaxInfo
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortMaxInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortMaxInfo (INT4 i4Dot1dTpPort, INT4 *pi4RetValDot1dTpPortMaxInfo)
{
    tCfaIfInfo          IfInfo;
/* When hardware supports API to get the maximum information supported by the 
 * port, use the function FsBrgGetMaxInfo()*/
    CfaGetIfInfo (i4Dot1dTpPort, &IfInfo);
    *pi4RetValDot1dTpPortMaxInfo = IfInfo.u4IfMtu;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortInFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortInFrames (INT4 i4Dot1dTpPort,
                           UINT4 *pu4RetValDot1dTpPortInFrames)
{
#ifdef NPAPI_WANTED
/* Get the number of data frames received */
    if (CfaFsHwGetStat (i4Dot1dTpPort, NP_STAT_DOT1D_TP_PORT_IN_FRAMES,
                     pu4RetValDot1dTpPortInFrames) == FNP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
#else
    *pu4RetValDot1dTpPortInFrames = tpInfo.portEntry[i4Dot1dTpPort].u4TpIn;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortOutFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortOutFrames (INT4 i4Dot1dTpPort,
                            UINT4 *pu4RetValDot1dTpPortOutFrames)
{
#ifdef NPAPI_WANTED
/* Get the number of data frames  transmitted */
    if (CfaFsHwGetStat (i4Dot1dTpPort, NP_STAT_DOT1D_TP_PORT_OUT_FRAMES,
                     pu4RetValDot1dTpPortOutFrames) == FNP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
#else
    *pu4RetValDot1dTpPortOutFrames = tpInfo.portEntry[i4Dot1dTpPort].u4TpOut;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortInDiscards
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortInDiscards (INT4 i4Dot1dTpPort,
                             UINT4 *pu4RetValDot1dTpPortInDiscards)
{
#ifdef NPAPI_WANTED
/* Get the number of frames discarded in the input port */
    if (CfaFsHwGetStat (i4Dot1dTpPort, NP_STAT_DOT1D_TP_PORT_IN_DISCARDS,
                     pu4RetValDot1dTpPortInDiscards) == FNP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
#else
    *pu4RetValDot1dTpPortInDiscards =
        tpInfo.portEntry[i4Dot1dTpPort].u4NoFilterInDiscard;
    return SNMP_SUCCESS;

#endif
}

/* LOW LEVEL Routines for Table : Dot1dStaticTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dStaticTable
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dStaticTable (tMacAddr Dot1dStaticAddress,
                                          INT4 i4Dot1dStaticReceivePort)
{
    tSTATICTABLE_ENTRY *pPtr;

    if ((i4Dot1dStaticReceivePort < 0) ||
        (i4Dot1dStaticReceivePort > BRG_MAX_PHY_PLUS_LOG_PORTS))
        return SNMP_FAILURE;

    if ((pPtr =
         BdIsMacaddrPresentOnStaticTable (Dot1dStaticAddress,
                                          i4Dot1dStaticReceivePort)) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dStaticTable
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDot1dStaticTable (tMacAddr * pDot1dStaticAddress,
                                  INT4 *pi4Dot1dStaticReceivePort)
{
    tSTATICTABLE_ENTRY *pPtr = NULL;
    INT4                i4Count;

    for (i4Count = 0; i4Count <= BRG_MAX_PHY_PLUS_LOG_PORTS; i4Count++)
    {
        TMO_SLL_Scan (&bdStaticTable[i4Count].entry, pPtr, tSTATICTABLE_ENTRY *)
        {
            if (pPtr != NULL)
            {
                BRG_MEM_CPY ((UINT1 *) *pDot1dStaticAddress,
                             pPtr->staticAddress, ETHERNET_ADDR_SIZE);
                *pi4Dot1dStaticReceivePort = pPtr->u1StaticReceivePort;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dStaticTable
 Input       :  The Indices
                Dot1dStaticAddress
                nextDot1dStaticAddress
                Dot1dStaticReceivePort
                nextDot1dStaticReceivePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dStaticTable (tMacAddr Dot1dStaticAddress,
                                 tMacAddr * pNextDot1dStaticAddress,
                                 INT4 i4Dot1dStaticReceivePort,
                                 INT4 *pi4NextDot1dStaticReceivePort)
{
    tSTATICTABLE_ENTRY *pPtr;
    INT4                i4I;

    if (i4Dot1dStaticReceivePort < 0)
    {
        return SNMP_FAILURE;
    }
    for (i4I = i4Dot1dStaticReceivePort; i4I <= BRG_MAX_PHY_PLUS_LOG_PORTS;
         i4I++)
    {
        TMO_SLL_Scan (&bdStaticTable[i4I].entry, pPtr, tSTATICTABLE_ENTRY *)
        {
            if ((i4I > i4Dot1dStaticReceivePort)
                ||
                (BuCompareMacaddr (pPtr->staticAddress, Dot1dStaticAddress)
                 == GREATER))
            {
                BRG_MEM_CPY ((UINT1 *) *pNextDot1dStaticAddress,
                             pPtr->staticAddress, ETHERNET_ADDR_SIZE);
                *pi4NextDot1dStaticReceivePort = pPtr->u1StaticReceivePort;

                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dStaticAllowedToGoTo
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                retValDot1dStaticAllowedToGoTo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStaticAllowedToGoTo (tMacAddr Dot1dStaticAddress,
                                INT4 i4Dot1dStaticReceivePort,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValDot1dStaticAllowedToGoTo)
{
    tSTATICTABLE_ENTRY *pPtr;
    UINT4               u4I, u4Index;
    if ((pPtr =
         BdIsMacaddrPresentOnStaticTable (Dot1dStaticAddress,
                                          i4Dot1dStaticReceivePort)) != NULL)
    {
        for (u4I = 1, u4Index = 0; u4I <= BRG_MAX_PHY_PLUS_LOG_PORTS;
             u4I += 8, u4Index++)
        {
            pRetValDot1dStaticAllowedToGoTo->pu1_OctetList[u4Index] =
                pPtr->u1StaticAllowedToGo[u4Index];
        }

        pRetValDot1dStaticAllowedToGoTo->i4_Length = u4Index;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetDot1dStaticStatus
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                retValDot1dStaticStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dStaticStatus (tMacAddr Dot1dStaticAddress,
                         INT4 i4Dot1dStaticReceivePort,
                         INT4 *pi4RetValDot1dStaticStatus)
{
    tSTATICTABLE_ENTRY *pPtr;
    if ((pPtr =
         BdIsMacaddrPresentOnStaticTable (Dot1dStaticAddress,
                                          i4Dot1dStaticReceivePort)) != NULL)
    {
        *pi4RetValDot1dStaticStatus = pPtr->u1StaticStatus;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetDot1dStaticAllowedToGoTo
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                setValDot1dStaticAllowedToGoTo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dStaticAllowedToGoTo (tMacAddr Dot1dStaticAddress,
                                INT4 i4Dot1dStaticReceivePort,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValDot1dStaticAllowedToGoTo)
{
    tSTATICTABLE_ENTRY *pPtr;
    INT4                i4I, i;
    UINT1               u1StaticPortsAllowed[MAX_NO_BYTES];

    MEMSET (u1StaticPortsAllowed, 0, sizeof (u1StaticPortsAllowed));
#ifdef NPAPI_WANTED
    /* Set the ports that are allowed for the static address */
    if (FsBrgSetPortIngStFilt (i4Dot1dStaticReceivePort,
                               Dot1dStaticAddress,
                               u1StaticPortsAllowed) != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    if ((pPtr =
         BdIsMacaddrPresentOnStaticTable (Dot1dStaticAddress,
                                          i4Dot1dStaticReceivePort)) !=
        (tSTATICTABLE_ENTRY *) NULL)
    {
        for (i4I = 1; i4I <= pSetValDot1dStaticAllowedToGoTo->i4_Length; i4I++)
        {
            pPtr->u1StaticAllowedToGo[i4I - 1] =
                pSetValDot1dStaticAllowedToGoTo->pu1_OctetList[i4I - 1];
        }
    }
    else
    {
        for (i = 0; i < pSetValDot1dStaticAllowedToGoTo->i4_Length; i++)
        {
            u1StaticPortsAllowed[i] =
                pSetValDot1dStaticAllowedToGoTo->pu1_OctetList[i];
        }
        if (BdAddStaticEntry
            ((UINT1) i4Dot1dStaticReceivePort, Dot1dStaticAddress,
             u1StaticPortsAllowed, STATIC_PERMANENT) == BRIDGE_NOT_OK)
        {
#ifdef NPAPI_WANTED
            /* Sets the status of the Static entry as invalid in the H/W */
            if (FsBrgSetPortIngStFiltFlags
                (i4Dot1dStaticReceivePort, Dot1dStaticAddress,
                 STATIC_INVALID) != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
#endif
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dStaticStatus
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                setValDot1dStaticStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dStaticStatus (tMacAddr Dot1dStaticAddress,
                         INT4 i4Dot1dStaticReceivePort,
                         INT4 i4SetValDot1dStaticStatus)
{
    UINT1               i1ReturnValue;
    tSTATICTABLE_ENTRY *pStatictabEntry;
    tSTATICTABLE_ENTRY *pPtr = NULL;
    tSTATICTABLE_ENTRY *pPrev;

#ifdef NPAPI_WANTED
    /* Sets the status of the Static entry in the H/W */
    if (FsBrgSetPortIngStFiltFlags
        (i4Dot1dStaticReceivePort, Dot1dStaticAddress,
         i4SetValDot1dStaticStatus) != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    if ((pPtr =
         BdIsMacaddrPresentOnStaticTable (Dot1dStaticAddress,
                                          i4Dot1dStaticReceivePort)) != NULL)
    {
        if (i4SetValDot1dStaticStatus == STATIC_INVALID)
        {
            if ((i1ReturnValue =
                 BdRemoveStaticEntryFromList (i4Dot1dStaticReceivePort,
                                              pPtr)) == BRIDGE_OK)
            {
                return SNMP_SUCCESS;
            }
            else
            {
#ifdef NPAPI_WANTED
                /* Create the Static entry in the H/W */
                if (FsBrgSetPortIngStFiltFlags
                    (i4Dot1dStaticReceivePort, Dot1dStaticAddress,
                     STATIC_PERMANENT) != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
#endif
                return SNMP_FAILURE;
            }
        }

        pPtr->u1StaticStatus = (UINT1) i4SetValDot1dStaticStatus;

        if (i4SetValDot1dStaticStatus == STATIC_DELETEONTIMEOUT)
            TmStartTimer (&pPtr->timeout);

        return SNMP_SUCCESS;
    }

/* Entry not found  */
    if (i4SetValDot1dStaticStatus == STATIC_INVALID)
    {
        return SNMP_FAILURE;
    }

    /* Entry not found, create a new entry */

    if ((i1ReturnValue =
         (INT1) ALLOC_STATICTABLE_ENTRY (staticTabid,
                                         (UINT1 *) &pStatictabEntry)) !=
        BRG_MEM_SUCCESS)
    {

#ifdef NPAPI_WANTED
        /* Sets the status of the Static entry as invalid in the H/W */
        if (FsBrgSetPortIngStFiltFlags
            (i4Dot1dStaticReceivePort, Dot1dStaticAddress,
             STATIC_INVALID) != FNP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
#endif
        return SNMP_FAILURE;
    }

    BRG_MEM_CPY (pStatictabEntry->staticAddress,
                 Dot1dStaticAddress, ETHERNET_ADDR_SIZE);
    pStatictabEntry->u1StaticReceivePort = (UINT1) i4Dot1dStaticReceivePort;
    pStatictabEntry->u1StaticStatus = (UINT1) i4SetValDot1dStaticStatus;
    pPrev = NULL;

    TMO_SLL_Scan (&bdStaticTable[i4Dot1dStaticReceivePort].entry, pPtr,
                  tSTATICTABLE_ENTRY *)
    {
        if ((StaticCompareMacaddrAndPort
             (pPtr->staticAddress, pPtr->u1StaticReceivePort,
              Dot1dStaticAddress, i4Dot1dStaticReceivePort)) == GREATER)
        {
            break;
        }
        else
        {
            pPrev = pPtr;
        }
    }
    TMO_SLL_Insert (&bdStaticTable[i4Dot1dStaticReceivePort].entry,
                    (tTMO_SLL_NODE *) pPrev, (tTMO_SLL_NODE *) pStatictabEntry);

    if (i4SetValDot1dStaticStatus == STATIC_DELETEONTIMEOUT)
        TmStartTimer (&pStatictabEntry->timeout);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dStaticAllowedToGoTo
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                testValDot1dStaticAllowedToGoTo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dStaticAllowedToGoTo (UINT4 *pu4ErrorCode,
                                   tMacAddr Dot1dStaticAddress,
                                   INT4 i4Dot1dStaticReceivePort,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValDot1dStaticAllowedToGoTo)
{
    tMacAddr            zeroAddr;

    if ((pTestValDot1dStaticAllowedToGoTo->i4_Length <= 0) ||
        (pTestValDot1dStaticAllowedToGoTo->i4_Length > BRG_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    BRG_MEM_SET (zeroAddr, 0, ETHERNET_ADDR_SIZE);
    if (!BRG_MEM_CMP (Dot1dStaticAddress, zeroAddr, ETHERNET_ADDR_SIZE) ||
        (i4Dot1dStaticReceivePort < 0) ||
        (i4Dot1dStaticReceivePort > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Dot1dStaticStatus
 Input       :  The Indices
                Dot1dStaticAddress
                Dot1dStaticReceivePort

                The Object 
                testValDot1dStaticStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dStaticStatus (UINT4 *pu4ErrorCode, tMacAddr Dot1dStaticAddress,
                            INT4 i4Dot1dStaticReceivePort,
                            INT4 i4TestValDot1dStaticStatus)
{
    tMacAddr            zeroAddr;

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    BRG_MEM_SET (zeroAddr, 0, ETHERNET_ADDR_SIZE);
    if (!BRG_MEM_CMP (Dot1dStaticAddress, zeroAddr, ETHERNET_ADDR_SIZE) ||
        (i4Dot1dStaticReceivePort < 0) ||
        (i4Dot1dStaticReceivePort > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValDot1dStaticStatus)
    {
        case STATIC_OTHER:
            /* FALL THROUGH */
        case STATIC_INVALID:
            /* FALL THROUGH */
        case STATIC_PERMANENT:
            /* FALL THROUGH */
        case STATIC_DELETEONRESET:
            /* FALL THROUGH */
        case STATIC_DELETEONTIMEOUT:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dBaseBridgeStatus
 Input       :  The Indices

                The Object 
                retValDot1dBaseBridgeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseBridgeStatus (INT4 *pi4RetValDot1dBaseBridgeStatus)
{
    if (BRG_SYSTEM_CONTROL == (UINT1) BRG_START)
    {
        *pi4RetValDot1dBaseBridgeStatus = BRG_MODULE_STATUS;
        return (INT1) SNMP_SUCCESS;
    }

    *pi4RetValDot1dBaseBridgeStatus = DOWN;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1dBaseBridgeCRCStatus
 Input       :  The Indices

                The Object 
                retValDot1dBaseBridgeCRCStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseBridgeCRCStatus (INT4 *pi4RetValDot1dBaseBridgeCRCStatus)
{

    *pi4RetValDot1dBaseBridgeCRCStatus = base.u1CrcStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDot1dBaseBridgeStatus
 Input       :  The Indices

                The Object 
                setValDot1dBaseBridgeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dBaseBridgeStatus (INT4 i4SetValDot1dBaseBridgeStatus)
{
    UINT4               u4I;
    INT4                i4RetValue;

    if (base.u1BridgeStatus != i4SetValDot1dBaseBridgeStatus)
    {
        switch (i4SetValDot1dBaseBridgeStatus)
        {
            case UP:

                for (u4I = BRG_MIN_PORT_NO; u4I <= BRG_MAX_PHY_PLUS_LOG_PORTS;
                     u4I++)
                {
                    /* Added the check for IfType - BCPCHANGES - 19/10 */
                    /* The following CfaGddConfigPort() call is for a port
                     * which was created by CFA when the BaseBridgeStatus was 
                     * DOWN 
                     */
                    if ((base.portEntry[u4I].u1AdminStatus == UP)
                        && (base.portEntry[u4I].portId.u1_InterfaceType ==
                            CFA_ENET))
                    {
                        if ((i4RetValue =
                             CfaGddConfigPort ((UINT2) u4I,
                                               (UINT1) CFA_ENET_EN_PROMISC,
                                               NULL)) == BRIDGE_NOT_OK)
                            return SNMP_FAILURE;
                    }
                }
                break;

            case DOWN:
                break;

            case BRIDGE_DOWN_ALL_IFS:
                if (InMakeAllInterfacesDown () != BRIDGE_OK)
                {
                    return SNMP_FAILURE;
                }
                break;

            default:
                return SNMP_FAILURE;
        }
        base.u1BridgeStatus = (UINT1) i4SetValDot1dBaseBridgeStatus;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dBaseBridgeCRCStatus
 Input       :  The Indices

                The Object 
                setValDot1dBaseBridgeCRCStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dBaseBridgeCRCStatus (INT4 i4SetValDot1dBaseBridgeCRCStatus)
{
    base.u1CrcStatus = (UINT1) i4SetValDot1dBaseBridgeCRCStatus;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dBaseBridgeStatus
 Input       :  The Indices

                The Object 
                testValDot1dBaseBridgeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dBaseBridgeStatus (UINT4 *pu4ErrorCode,
                                INT4 i4TestValDot1dBaseBridgeStatus)
{
    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }
    switch (i4TestValDot1dBaseBridgeStatus)
    {
        case UP:
            /* FALL THROUGH */
        case DOWN:
            /* FALL THROUGH */
        case BRIDGE_DOWN_ALL_IFS:
            /* if (VLAN_IS_VLAN_ENABLED () == VLAN_FALSE)
               { */
            return SNMP_SUCCESS;
            /* } */

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

    }
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dBaseBridgeCRCStatus
 Input       :  The Indices

                The Object 
                testValDot1dBaseBridgeCRCStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dBaseBridgeCRCStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValDot1dBaseBridgeCRCStatus)
{

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    switch (i4TestValDot1dBaseBridgeCRCStatus)
    {
        case BRG_WITH_CRC:
            /* FALL THROUGH */
        case BRG_WITHOUT_CRC:
            return SNMP_SUCCESS;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/* LOW LEVEL Routines for Table : Dot1dFutureBasePortTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dFutureBasePortTable
 Input       :  The Indices
                Dot1dFutureBasePort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dFutureBasePortTable (INT4 i4Dot1dFutureBasePort)
{
    if (((i4Dot1dFutureBasePort >= 1) &&
         (i4Dot1dFutureBasePort <= BRG_MAX_PHY_PLUS_LOG_PORTS)) &&
        (base.portEntry[i4Dot1dFutureBasePort].u1AdminStatus != BRIDGE_INVALID))
        return SNMP_SUCCESS;
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dFutureBasePortTable
 Input       :  The Indices
                Dot1dFutureBasePort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDot1dFutureBasePortTable (INT4 *pi4Dot1dFutureBasePort)
{
    tBOOLEAN            bFound = FALSE;
    UINT4               u4PortNo;
    UINT1               u1InterfaceNum = 0xff;

    for (u4PortNo = (UINT4) gu2BrgStartPortInd;
         u4PortNo != BRG_INVALID_PORT_INDEX;
         u4PortNo = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4PortNo))
    {
        if (u1InterfaceNum > base.portEntry[u4PortNo].portId.u1_InterfaceNum)
        {
            bFound = TRUE;
            *pi4Dot1dFutureBasePort =
                base.portEntry[u4PortNo].portId.u1_InterfaceNum;

            u1InterfaceNum = base.portEntry[u4PortNo].portId.u1_InterfaceNum;
        }
    }

    if (bFound == TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dFutureBasePortTable
 Input       :  The Indices
                Dot1dFutureBasePort
                nextDot1dFutureBasePort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dFutureBasePortTable (INT4 i4Dot1dFutureBasePort,
                                         INT4 *pi4NextDot1dFutureBasePort)
{
    tBOOLEAN            bFound = FALSE;
    UINT1               u1InterfaceNum = 0xff;
    UINT4               u4PortNo;

    if (i4Dot1dFutureBasePort < 0)
    {
        return SNMP_FAILURE;
    }
    for (u4PortNo = (UINT4) gu2BrgStartPortInd;
         u4PortNo != BRG_INVALID_PORT_INDEX;
         u4PortNo = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4PortNo))
    {
        if ((base.portEntry[u4PortNo].portId.u1_InterfaceNum >
             i4Dot1dFutureBasePort)
            && (u1InterfaceNum >
                base.portEntry[u4PortNo].portId.u1_InterfaceNum))
        {
            bFound = TRUE;
            u1InterfaceNum = base.portEntry[u4PortNo].portId.u1_InterfaceNum;
        }
    }

    *pi4NextDot1dFutureBasePort = u1InterfaceNum;

    if (bFound == TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dBasePortAdminStatus
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                retValDot1dFutureBasePortAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortAdminStatus (INT4 i4Dot1dFutureBasePort,
                                INT4 *pi4RetValDot1dBasePortAdminStatus)
{
    *pi4RetValDot1dBasePortAdminStatus =
        base.portEntry[i4Dot1dFutureBasePort].u1AdminStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortOperStatus
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                retValDot1dFutureBasePortOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortOperStatus (INT4 i4Dot1dFutureBasePort,
                               INT4 *pi4RetValDot1dBasePortOperStatus)
{
    *pi4RetValDot1dBasePortOperStatus =
        base.portEntry[i4Dot1dFutureBasePort].u1OperStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortBcastStatus
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                retValDot1dFutureBasePortBcastStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortBcastStatus (INT4 i4Dot1dFutureBasePort,
                                INT4 *pi4RetValDot1dBasePortBcastStatus)
{
    *pi4RetValDot1dBasePortBcastStatus =
        base.portEntry[i4Dot1dFutureBasePort].u1BroadcastStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortFilterNumber
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                retValDot1dFutureBasePortFilterNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortFilterNumber (INT4 i4Dot1dFutureBasePort,
                                 INT4 *pi4RetValDot1dBasePortFilterNumber)
{
    *pi4RetValDot1dBasePortFilterNumber =
        base.portEntry[i4Dot1dFutureBasePort].u1FilterNo;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortMcastNumber
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                retValDot1dFutureBasePortMcastNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortMcastNumber (INT4 i4Dot1dFutureBasePort,
                                INT4 *pi4RetValDot1dBasePortMcastNumber)
{
    *pi4RetValDot1dBasePortMcastNumber =
        base.portEntry[i4Dot1dFutureBasePort].u1MlistNo;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortBcastOutFrames
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                retValDot1dFutureBasePortBcastOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortBcastOutFrames (INT4 i4Dot1dFutureBasePort,
                                   UINT4 *pu4RetValDot1dBasePortBcastOutFrames)
{
#ifdef NPAPI_WANTED
/* Getting the counter value for broadcast out frames */
    if (CfaFsHwGetStat (i4Dot1dFutureBasePort,
                     NP_STAT_DOT1D_PORT_BCAST_OUT_PKTS,
                     pu4RetValDot1dBasePortBcastOutFrames) == FNP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
#else
    *pu4RetValDot1dBasePortBcastOutFrames =
        base.portEntry[i4Dot1dFutureBasePort].u4NoBcastFramesTrxed;
    return SNMP_SUCCESS;
#endif

}

/****************************************************************************
 Function    :  nmhGetDot1dBasePortMcastOutFrames
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                retValDot1dFutureBasePortMcastOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBasePortMcastOutFrames (INT4 i4Dot1dFutureBasePort,
                                   UINT4 *pu4RetValDot1dBasePortMcastOutFrames)
{
#ifdef NPAPI_WANTED
/* Getting the counter value for multicast out frames */
    if (CfaFsHwGetStat (i4Dot1dFutureBasePort,
                     NP_STAT_DOT1D_PORT_MCAST_OUT_PKTS,
                     pu4RetValDot1dBasePortMcastOutFrames) == FNP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
#else
    *pu4RetValDot1dBasePortMcastOutFrames =
        base.portEntry[i4Dot1dFutureBasePort].u4NoMcastFramesTrxed;
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDot1dBasePortAdminStatus
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                setValDot1dFutureBasePortAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dBasePortAdminStatus (INT4 i4Dot1dFutureBasePort,
                                INT4 i4SetValDot1dBasePortAdminStatus)
{
    INT2                i2ReturnValue;
    UINT1               u1BrgOperStatus;

    if ((i2ReturnValue = nmhValidateIndexInstanceDot1dFutureBasePortTable
         (i4Dot1dFutureBasePort)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (base.portEntry[i4Dot1dFutureBasePort].u1AdminStatus ==
        (UINT1) i4SetValDot1dBasePortAdminStatus)
    {
        return SNMP_SUCCESS;
    }

    base.portEntry[i4Dot1dFutureBasePort].u1AdminStatus =
        (UINT1) i4SetValDot1dBasePortAdminStatus;

    u1BrgOperStatus = base.portEntry[i4Dot1dFutureBasePort].u1OperStatus;

    switch (i4SetValDot1dBasePortAdminStatus)
    {
        case UP:
            if (u1BrgOperStatus == UP)
            {
                L2IwfBrgHLPortOperIndication ((UINT2) i4Dot1dFutureBasePort,
                                              CFA_IF_UP);
            }
            break;

        case DOWN:
            if (u1BrgOperStatus == UP)
            {
                L2IwfBrgHLPortOperIndication ((UINT2) i4Dot1dFutureBasePort,
                                              CFA_IF_DOWN);
            }
            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dBasePortBcastStatus
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                setValDot1dFutureBasePortBcastStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dBasePortBcastStatus (INT4 i4Dot1dFutureBasePort,
                                INT4 i4SetValDot1dBasePortBcastStatus)
{
    UINT1               i1ReturnValue;

    if ((i1ReturnValue =
         nmhValidateIndexInstanceDot1dFutureBasePortTable
         (i4Dot1dFutureBasePort)) == SNMP_FAILURE)
        return SNMP_FAILURE;

    base.portEntry[i4Dot1dFutureBasePort].u1BroadcastStatus =
        (UINT1) i4SetValDot1dBasePortBcastStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dBasePortFilterNumber
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                setValDot1dFutureBasePortFilterNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dBasePortFilterNumber (INT4 i4Dot1dFutureBasePort,
                                 INT4 i4SetValDot1dBasePortFilterNumber)
{
    UINT1               i1ReturnValue;

    if ((i1ReturnValue =
         nmhValidateIndexInstanceDot1dFutureBasePortTable
         (i4Dot1dFutureBasePort)) == SNMP_FAILURE)
        return SNMP_FAILURE;

    base.portEntry[i4Dot1dFutureBasePort].u1FilterNo =
        (UINT1) i4SetValDot1dBasePortFilterNumber;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dBasePortMcastNumber
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                setValDot1dFutureBasePortMcastNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dBasePortMcastNumber (INT4 i4Dot1dFutureBasePort,
                                INT4 i4SetValDot1dBasePortMcastNumber)
{
    UINT1               i1ReturnValue;

    if ((i1ReturnValue =
         nmhValidateIndexInstanceDot1dFutureBasePortTable
         (i4Dot1dFutureBasePort)) == SNMP_FAILURE)
        return SNMP_FAILURE;
#ifdef NPAPI_WANTED
/* Setting the multi-list number to port */
    if (FsBrgSetMcGrp2Port (i4Dot1dFutureBasePort,
                            i4SetValDot1dBasePortMcastNumber) != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    base.portEntry[i4Dot1dFutureBasePort].u1MlistNo =
        (UINT1) i4SetValDot1dBasePortMcastNumber;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dBasePortAdminStatus
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                testValDot1dBasePortAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dBasePortAdminStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4Dot1dFutureBasePort,
                                   INT4 i4TestValDot1dBasePortAdminStatus)
{
    UINT1               i1ReturnValue;

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    if ((i1ReturnValue =
         nmhValidateIndexInstanceDot1dFutureBasePortTable
         (i4Dot1dFutureBasePort)) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValDot1dBasePortAdminStatus)
    {
        case UP:
            /* FALL THROUGH */
        case DOWN:
            /* FALL THROUGH */
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dBasePortBcastStatus
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                testValDot1dBasePortBcastStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dBasePortBcastStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4Dot1dFutureBasePort,
                                   INT4 i4TestValDot1dBasePortBcastStatus)
{
    UINT1               i1ReturnValue;

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    if ((i1ReturnValue =
         nmhValidateIndexInstanceDot1dFutureBasePortTable
         (i4Dot1dFutureBasePort)) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValDot1dBasePortBcastStatus)
    {
        case BROADCAST_UP:
            /* FALL THROUGH */
        case BROADCAST_DOWN:
            return SNMP_SUCCESS;
        default:

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dBasePortFilterNumber
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                testValDot1dBasePortFilterNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dBasePortFilterNumber (UINT4 *pu4ErrorCode,
                                    INT4 i4Dot1dFutureBasePort,
                                    INT4 i4TestValDot1dBasePortFilterNumber)
{
    UINT1               i1ReturnValue;

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    if ((i1ReturnValue =
         nmhValidateIndexInstanceDot1dBasePortTable (i4Dot1dFutureBasePort)) ==
        SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dBasePortFilterNumber < 0)
        || (i4TestValDot1dBasePortFilterNumber > (MAX_FILTER_NUMBER - 1)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Filter can be applied on a particular port only when there are 
     * entries present for the particular filter.
     */
    if (i4TestValDot1dBasePortFilterNumber != 0)
    {

        if (TMO_SLL_Count (&bdFilter[i4TestValDot1dBasePortFilterNumber].entry)
            != 0)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dBasePortMcastNumber
 Input       :  The Indices
                Dot1dFutureBasePort

                The Object 
                testValDot1dBasePortMcastNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dBasePortMcastNumber (UINT4 *pu4ErrorCode,
                                   INT4 i4Dot1dFutureBasePort,
                                   INT4 i4TestValDot1dBasePortMcastNumber)
{
    UINT1               i1ReturnValue;

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    if ((i1ReturnValue =
         nmhValidateIndexInstanceDot1dBasePortTable (i4Dot1dFutureBasePort)) ==
        SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValDot1dBasePortMcastNumber < 0)
        || (i4TestValDot1dBasePortMcastNumber > (MAX_MULTILIST_NUMBER - 1)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Multicast Filter can be applied on a particular port only when there are 
     * entries present for the particular multicast filter.
     */
    if (i4TestValDot1dBasePortMcastNumber != 0)
    {

        if (TMO_SLL_Count (&bdMlist[i4TestValDot1dBasePortMcastNumber].entry) !=
            0)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : Dot1dFutureTpPortTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dFutureTpPortTable
 Input       :  The Indices
                Dot1dFutureTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dFutureTpPortTable (INT4 i4Dot1dFutureTpPort)
{
    if ((i4Dot1dFutureTpPort >= 1) &&
        (i4Dot1dFutureTpPort <= BRG_MAX_PHY_PLUS_LOG_PORTS) &&
        (base.portEntry[i4Dot1dFutureTpPort].u1AdminStatus != BRIDGE_INVALID))
        return SNMP_SUCCESS;
    else
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dFutureTpPortTable
 Input       :  The Indices
                Dot1dFutureTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDot1dFutureTpPortTable (INT4 *pi4Dot1dFutureTpPort)
{
    tBOOLEAN            bFound = FALSE;
    UINT4               u4PortNo;
    UINT1               u1_InterfaceNum = 0xff;

    for (u4PortNo = (UINT4) gu2BrgStartPortInd;
         u4PortNo != BRG_INVALID_PORT_INDEX;
         u4PortNo = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4PortNo))
    {
        if (u1_InterfaceNum > base.portEntry[u4PortNo].portId.u1_InterfaceNum)
        {
            bFound = TRUE;
            u1_InterfaceNum = base.portEntry[u4PortNo].portId.u1_InterfaceNum;
        }
    }

    *pi4Dot1dFutureTpPort = u1_InterfaceNum;
    if (bFound == TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dFutureTpPortTable
 Input       :  The Indices
                Dot1dFutureTpPort
                nextDot1dFutureTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dFutureTpPortTable (INT4 i4Dot1dFutureTpPort,
                                       INT4 *pi4NextDot1dFutureTpPort)
{
    tBOOLEAN            bFound = FALSE;
    UINT1               u1InterfaceNum = 0xff;
    UINT4               u4PortNo;

    if (i4Dot1dFutureTpPort < 0)
    {
        return SNMP_FAILURE;
    }
    for (u4PortNo = (UINT4) gu2BrgStartPortInd;
         u4PortNo != BRG_INVALID_PORT_INDEX;
         u4PortNo = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4PortNo))
    {
        if ((base.portEntry[u4PortNo].portId.u1_InterfaceNum >
             i4Dot1dFutureTpPort)
            && (u1InterfaceNum >
                base.portEntry[u4PortNo].portId.u1_InterfaceNum))
        {
            bFound = TRUE;
            u1InterfaceNum = base.portEntry[u4PortNo].portId.u1_InterfaceNum;
        }
    }

    *pi4NextDot1dFutureTpPort = u1InterfaceNum;

    if (bFound == TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dTpPortInProtoDiscards
 Input       :  The Indices
                Dot1dFutureTpPort

                The Object 
                retValDot1dFutureTpPortInProtoDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortInProtoDiscards (INT4 i4Dot1dTpPort,
                                  UINT4 *pu4RetValDot1dTpPortInProtoDiscards)
{
    *pu4RetValDot1dTpPortInProtoDiscards =
        tpInfo.portEntry[i4Dot1dTpPort].u4NoProtoInDiscard;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortInFilterDiscards
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortInFilterDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortInFilterDiscards (INT4 i4Dot1dTpPort,
                                   UINT4 *pu4RetValDot1dTpPortInFilterDiscards)
{
    *pu4RetValDot1dTpPortInFilterDiscards =
        tpInfo.portEntry[i4Dot1dTpPort].u4NoFilterInDiscard;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortProtocolFilterMask
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortProtocolFilterMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dTpPortProtocolFilterMask (INT4 i4Dot1dTpPort,
                                     INT4
                                     *pi4RetValDot1dTpPortProtocolFilterMask)
{
    *pi4RetValDot1dTpPortProtocolFilterMask =
        tpInfo.portEntry[i4Dot1dTpPort].u2ProtocolFilterMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBaseBridgeDebug
 Input       :  The Indices

                The Object 
                retValDot1dBaseBridgeDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseBridgeDebug (INT4 *pi4RetValDot1dBaseBridgeDebug)
{
    *pi4RetValDot1dBaseBridgeDebug = gBrgGlb.u4BrgDbg;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDot1dBaseBridgeTrace
 Input       :  The Indices

                The Object 
                retValDot1dBaseBridgeTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseBridgeTrace (INT4 *pi4RetValDot1dBaseBridgeTrace)
{
    *pi4RetValDot1dBaseBridgeTrace = gBrgGlb.u4BrgTrc;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dBaseBridgeMaxFwdDbEntries
 Input       :  The Indices

                The Object 
                retValDot1dBaseBridgeMaxFwdDbEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dBaseBridgeMaxFwdDbEntries (INT4
                                      *pi4RetValDot1dBaseBridgeMaxFwdDbEntries)
{
    *pi4RetValDot1dBaseBridgeMaxFwdDbEntries = VLAN_DEV_MAX_L2_TABLE_SIZE;    /* MAX_FDB_ENTRIES */
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDot1dTpPortProtocolFilterMask
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                setValDot1dTpPortProtocolFilterMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dTpPortProtocolFilterMask (INT4 i4Dot1dTpPort,
                                     INT4 i4SetValDot1dTpPortProtocolFilterMask)
{
    UINT1               iReturnValue;

    if ((iReturnValue =
         nmhValidateIndexInstanceDot1dTpPortTable (i4Dot1dTpPort)) ==
        SNMP_SUCCESS)
    {
        tpInfo.portEntry[i4Dot1dTpPort].u2ProtocolFilterMask =
            (UINT2) i4SetValDot1dTpPortProtocolFilterMask;
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetDot1dBaseBridgeDebug
 Input       :  The Indices

                The Object 
                setValDot1dBaseBridgeDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dBaseBridgeDebug (INT4 i4SetValDot1dBaseBridgeDebug)
{
    gBrgGlb.u4BrgDbg = i4SetValDot1dBaseBridgeDebug;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dBaseBridgeTrace
 Input       :  The Indices

                The Object 
                setValDot1dBaseBridgeTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dBaseBridgeTrace (INT4 i4SetValDot1dBaseBridgeTrace)
{
    gBrgGlb.u4BrgTrc = i4SetValDot1dBaseBridgeTrace;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dTpPortProtocolFilterMask
 Input       :  The Indices
                Dot1dFutureTpPort

                The Object 
                testValDot1dTpPortProtocolFilterMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dTpPortProtocolFilterMask (UINT4 *pu4ErrorCode,
                                        INT4 i4Dot1dFutureTpPort,
                                        INT4
                                        i4TestValDot1dTpPortProtocolFilterMask)
{
    UINT1               iReturnValue;

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    if ((iReturnValue =
         nmhValidateIndexInstanceDot1dTpPortTable (i4Dot1dFutureTpPort)) ==
        SNMP_SUCCESS)
    {

        if (IS_VALID_PROTOCOL_FILTER_MASK
            (i4TestValDot1dTpPortProtocolFilterMask))
        {
            return SNMP_SUCCESS;

        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }
}

/* LOW LEVEL Routines for Table : Dot1dFilterTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dFilterTable
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dFilterTable (INT4 i4Dot1dFilterNumber,
                                          tMacAddr Dot1dFilterSrcAddress,
                                          tMacAddr Dot1dFilterDstAddress)
{
    if ((BdGetFilterEntryFromIndex
         ((UINT1) i4Dot1dFilterNumber, Dot1dFilterDstAddress,
          Dot1dFilterSrcAddress)) == (tFILTERING_ENTRY *) NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dFilterTable
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDot1dFilterTable (INT4 *pi4Dot1dFilterNumber,
                                  tMacAddr * pDot1dFilterSrcAddress,
                                  tMacAddr * pDot1dFilterDstAddress)
{
    tFILTERING_ENTRY   *pPtr;
    tMacAddr            destAddr, srcAddr;
    UINT4               u4I;
    INT4                i4Cmp;

    BRG_MEM_SET (destAddr, 0xff, ETHERNET_ADDR_SIZE);
    BRG_MEM_SET (srcAddr, 0xff, ETHERNET_ADDR_SIZE);

    for (u4I = 0; u4I < MAX_FILTER_NUMBER; u4I++)
    {

        if (TMO_SLL_Count (&bdFilter[u4I].entry) != 0)
        {
            TMO_SLL_Scan (&bdFilter[u4I].entry, pPtr, tFILTERING_ENTRY *)
            {
                i4Cmp = BuCompareMacaddr (pPtr->sourceAddr, srcAddr);
                if ((i4Cmp == LESSER) || ((i4Cmp == BRG_EQUAL)
                                          &&
                                          (BuCompareMacaddr
                                           (pPtr->destAddr,
                                            destAddr) == LESSER)))
                {

                    BRG_MEM_CPY (destAddr, pPtr->destAddr, ETHERNET_ADDR_SIZE);
                    BRG_MEM_CPY (srcAddr, pPtr->sourceAddr, ETHERNET_ADDR_SIZE);
                }
            }
            *pi4Dot1dFilterNumber = u4I;
            BRG_MEM_CPY ((UINT1 *) *pDot1dFilterDstAddress, destAddr,
                         ETHERNET_ADDR_SIZE);
            BRG_MEM_CPY ((UINT1 *) *pDot1dFilterSrcAddress, srcAddr,
                         ETHERNET_ADDR_SIZE);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dFilterTable
 Input       :  The Indices
                Dot1dFilterNumber
                nextDot1dFilterNumber
                Dot1dFilterSrcAddress
                nextDot1dFilterSrcAddress
                Dot1dFilterDstAddress
                nextDot1dFilterDstAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dFilterTable (INT4 i4Dot1dFilterNumber,
                                 INT4 *pi4NextDot1dFilterNumber,
                                 tMacAddr Dot1dFilterSrcAddress,
                                 tMacAddr * pNextDot1dFilterSrcAddress,
                                 tMacAddr Dot1dFilterDstAddress,
                                 tMacAddr * pNextDot1dFilterDstAddress)
{
    tFILTERING_ENTRY   *pPtr;
    UINT1               key1[KEY_LENGTH], key2[KEY_LENGTH], key3[KEY_LENGTH];
    UINT4               u4I, u4Count;
    tBOOLEAN            bFound = FALSE;

    if (i4Dot1dFilterNumber < 0)
    {
        return SNMP_FAILURE;
    }
    SnMakeKey (key2, (UINT1) i4Dot1dFilterNumber, Dot1dFilterSrcAddress,
               Dot1dFilterDstAddress);

    BRG_MEM_SET (key3, 0xff, KEY_LENGTH);

    for (u4I = i4Dot1dFilterNumber; u4I < MAX_FILTER_NUMBER; u4I++)
    {
        if ((u4Count = TMO_SLL_Count (&bdFilter[u4I].entry)) != 0)
        {

            TMO_SLL_Scan (&bdFilter[u4I].entry, pPtr, tFILTERING_ENTRY *)
            {

                SnMakeKey (key1, (UINT1) u4I, pPtr->sourceAddr, pPtr->destAddr);

                if ((BRG_MEM_CMP (key1, key2, 13) > 0)
                    && (BRG_MEM_CMP (key3, key1, 13) > 0))
                {

                    BRG_MEM_CPY (key3, key1, 13);

                    BRG_MEM_CPY ((UINT1 *) *pNextDot1dFilterDstAddress,
                                 pPtr->destAddr, ETHERNET_ADDR_SIZE);
                    BRG_MEM_CPY ((UINT1 *) *pNextDot1dFilterSrcAddress,
                                 pPtr->sourceAddr, ETHERNET_ADDR_SIZE);
                    *pi4NextDot1dFilterNumber = u4I;

                    SnMakeKey (key3, (UINT1) (*pi4NextDot1dFilterNumber),
                               (UINT1 *) *pNextDot1dFilterSrcAddress,
                               (UINT1 *) *pNextDot1dFilterDstAddress);

                    bFound = TRUE;
                }

            }
            if (bFound == TRUE)
            {
                return SNMP_SUCCESS;
            }

        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dFilterSrcMask
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress

                The Object 
                retValDot1dFilterSrcMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dFilterSrcMask (INT4 i4Dot1dFilterNumber,
                          tMacAddr Dot1dFilterSrcAddress,
                          tMacAddr Dot1dFilterDstAddress,
                          tMacAddr * pRetValDot1dFilterSrcMask)
{
    tFILTERING_ENTRY   *pFil;

    pFil =
        (tFILTERING_ENTRY *)
        BdGetFilterEntryFromIndex ((UINT1) i4Dot1dFilterNumber,
                                   Dot1dFilterDstAddress,
                                   Dot1dFilterSrcAddress);

    if (pFil == NULL)
    {
        /* Entry Not Found */
        return SNMP_FAILURE;
    }
    else
    {
        BRG_MEM_CPY ((UINT1 *) *pRetValDot1dFilterSrcMask, pFil->sourceMask,
                     ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetDot1dFilterDstMask
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress

                The Object 
                retValDot1dFilterDstMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dFilterDstMask (INT4 i4Dot1dFilterNumber,
                          tMacAddr Dot1dFilterSrcAddress,
                          tMacAddr Dot1dFilterDstAddress,
                          tMacAddr * pRetValDot1dFilterDstMask)
{
    tFILTERING_ENTRY   *pFil;

    pFil =
        (tFILTERING_ENTRY *)
        BdGetFilterEntryFromIndex ((UINT1) i4Dot1dFilterNumber,
                                   Dot1dFilterDstAddress,
                                   Dot1dFilterSrcAddress);

    if (pFil == NULL)
    {
        /* Entry Not Found */
        return SNMP_FAILURE;
    }

    BRG_MEM_CPY ((UINT1 *) *pRetValDot1dFilterDstMask, pFil->destMask,
                 ETHERNET_ADDR_SIZE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dFilterPermiss
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress

                The Object 
                retValDot1dFilterPermiss
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dFilterPermiss (INT4 i4Dot1dFilterNumber,
                          tMacAddr Dot1dFilterSrcAddress,
                          tMacAddr Dot1dFilterDstAddress,
                          INT4 *pi4RetValDot1dFilterPermiss)
{
    tFILTERING_ENTRY   *pFil;

    pFil =
        (tFILTERING_ENTRY *)
        BdGetFilterEntryFromIndex ((UINT1) i4Dot1dFilterNumber,
                                   Dot1dFilterDstAddress,
                                   Dot1dFilterSrcAddress);

    if (pFil == NULL)
    {
        /* Entry Not Found */
        return SNMP_FAILURE;
    }

    *pi4RetValDot1dFilterPermiss = pFil->u1Status;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDot1dFilterSrcMask
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress

                The Object 
                setValDot1dFilterSrcMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dFilterSrcMask (INT4 i4Dot1dFilterNumber,
                          tMacAddr Dot1dFilterSrcAddress,
                          tMacAddr Dot1dFilterDstAddress,
                          tMacAddr SetValDot1dFilterSrcMask)
{
    tFILTERING_ENTRY   *pFil;
    tMacAddr            dummy;

    BRG_MEM_SET (dummy, 0xFF, ETHERNET_ADDR_SIZE);
    pFil =
        (tFILTERING_ENTRY *)
        BdGetFilterEntryFromIndex ((UINT1) i4Dot1dFilterNumber,
                                   Dot1dFilterDstAddress,
                                   Dot1dFilterSrcAddress);

    if (pFil == NULL)
    {

        /* Entry Not Found */
        if (BdAddFilterEntry
            ((UINT1) i4Dot1dFilterNumber, dummy, Dot1dFilterDstAddress,
             SetValDot1dFilterSrcMask, Dot1dFilterSrcAddress,
             MACF_FORWARD) == BRIDGE_NOT_OK)
        {
            return SNMP_FAILURE;
        }

    }
    else
    {

        BRG_MEM_CPY (pFil->sourceMask, SetValDot1dFilterSrcMask,
                     ETHERNET_ADDR_SIZE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dFilterDstMask
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress

                The Object 
                setValDot1dFilterDstMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dFilterDstMask (INT4 i4Dot1dFilterNumber,
                          tMacAddr Dot1dFilterSrcAddress,
                          tMacAddr Dot1dFilterDstAddress,
                          tMacAddr SetValDot1dFilterDstMask)
{
    tFILTERING_ENTRY   *pFil;
    tMacAddr            dummy;

    BRG_MEM_SET (dummy, 0xFF, ETHERNET_ADDR_SIZE);
    pFil =
        (tFILTERING_ENTRY *)
        BdGetFilterEntryFromIndex ((UINT1) i4Dot1dFilterNumber,
                                   Dot1dFilterDstAddress,
                                   Dot1dFilterSrcAddress);

    if (pFil == NULL)
    {

        /* Entry Not Found */
        if (BdAddFilterEntry
            ((UINT1) i4Dot1dFilterNumber, SetValDot1dFilterDstMask,
             Dot1dFilterDstAddress, dummy, Dot1dFilterSrcAddress,
             MACF_FORWARD) == BRIDGE_NOT_OK)
        {

            return SNMP_FAILURE;
        }

    }
    else
    {
        BRG_MEM_CPY (pFil->destMask, SetValDot1dFilterDstMask,
                     ETHERNET_ADDR_SIZE);

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDot1dFilterPermiss
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress

                The Object 
                setValDot1dFilterPermiss
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dFilterPermiss (INT4 i4Dot1dFilterNumber,
                          tMacAddr Dot1dFilterSrcAddress,
                          tMacAddr Dot1dFilterDstAddress,
                          INT4 i4SetValDot1dFilterPermiss)
{
    tFILTERING_ENTRY   *pFil;
    tMacAddr            dummy;
    INT4                i4Port;
    BRG_MEM_SET (dummy, 0xff, ETHERNET_ADDR_SIZE);
    pFil =
        (tFILTERING_ENTRY *)
        BdGetFilterEntryFromIndex ((UINT1) i4Dot1dFilterNumber,
                                   Dot1dFilterDstAddress,
                                   Dot1dFilterSrcAddress);
    if (pFil == NULL)
    {
        /* Entry Not Found */
        if (i4SetValDot1dFilterPermiss == MACF_INVALID)
        {
            /* Cannot add entry for invalid status */
            return SNMP_FAILURE;
        }

        if (BdAddFilterEntry
            ((UINT1) i4Dot1dFilterNumber, dummy, Dot1dFilterDstAddress, dummy,
             Dot1dFilterSrcAddress,
             (UINT1) i4SetValDot1dFilterPermiss) == BRIDGE_NOT_OK)
        {

            return SNMP_FAILURE;
        }
    }
    else
    {

        if (i4SetValDot1dFilterPermiss == MACF_INVALID)
        {
            BdDeleteFilterEntry ((UINT1) i4Dot1dFilterNumber,
                                 Dot1dFilterDstAddress, Dot1dFilterSrcAddress);
        }
        pFil->u1Status = (UINT1) i4SetValDot1dFilterPermiss;

        /* Refresh the entries, for ports mapping this empty filter lists */
        if (TMO_SLL_Count (&bdFilter[i4Dot1dFilterNumber].entry) == 0)
        {

            /* Check each port and reset filter number mapped to it */
            for (i4Port = 1; i4Port <= BRG_MAX_PHY_PLUS_LOG_PORTS; i4Port++)
            {
                if (base.portEntry[i4Port].u1FilterNo == i4Dot1dFilterNumber)
                    base.portEntry[i4Port].u1FilterNo = 0;
            }

        }

    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dFilterSrcMask
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress

                The Object 
                testValDot1dFilterSrcMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dFilterSrcMask (UINT4 *pu4ErrorCode, INT4 i4Dot1dFilterNumber,
                             tMacAddr Dot1dFilterSrcAddress,
                             tMacAddr Dot1dFilterDstAddress,
                             tMacAddr TestValDot1dFilterSrcMask)
{
    tMacAddr            zeroAddr;

    UNUSED_PARAM (TestValDot1dFilterSrcMask[0]);
    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    BRG_MEM_SET (zeroAddr, 0, 6);

    /* Filter number is being tested to see whether it is less 
     * than or equal to 0 or greater than the maximum number of filters allowed.
     * If so,error message will be returned.
     */
    if ((i4Dot1dFilterNumber <= 0)
        || (i4Dot1dFilterNumber > (MAX_FILTER_NUMBER - 1)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (!BRG_MEM_CMP (Dot1dFilterSrcAddress, zeroAddr, ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!BRG_MEM_CMP (Dot1dFilterDstAddress, zeroAddr, ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* source address should not be broadcast or multicast address */
    if ((BM_IS_MAC_MULTICAST (Dot1dFilterSrcAddress[0])) ||
        (BM_IS_BCAST_ADDRESS (Dot1dFilterSrcAddress)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Source & destination address should not be same */
    if (!BRG_MEM_CMP
        (Dot1dFilterSrcAddress, Dot1dFilterDstAddress, ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dFilterDstMask
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress

                The Object 
                testValDot1dFilterDstMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dFilterDstMask (UINT4 *pu4ErrorCode, INT4 i4Dot1dFilterNumber,
                             tMacAddr Dot1dFilterSrcAddress,
                             tMacAddr Dot1dFilterDstAddress,
                             tMacAddr TestValDot1dFilterDstMask)
{
    tMacAddr            zeroAddr;

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    BRG_MEM_SET (zeroAddr, 0, ETHERNET_ADDR_SIZE);
    UNUSED_PARAM (TestValDot1dFilterDstMask[0]);

    /* Filter number is being tested to see whether it is less 
     * than or equal to 0 or greater than the maximum number of filters allowed.
     * If so, error message will be returned.
     */
    if ((i4Dot1dFilterNumber <= 0)
        || (i4Dot1dFilterNumber > (MAX_FILTER_NUMBER - 1)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (!BRG_MEM_CMP (Dot1dFilterSrcAddress, zeroAddr, ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (!BRG_MEM_CMP (Dot1dFilterDstAddress, zeroAddr, ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* source address should not be broadcast or multicast address */
    if ((BM_IS_MAC_MULTICAST (Dot1dFilterSrcAddress[0])) ||
        (BM_IS_BCAST_ADDRESS (Dot1dFilterSrcAddress)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Source & destination address should not be same */
    if (!BRG_MEM_CMP
        (Dot1dFilterSrcAddress, Dot1dFilterDstAddress, ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dFilterPermiss
 Input       :  The Indices
                Dot1dFilterNumber
                Dot1dFilterSrcAddress
                Dot1dFilterDstAddress

                The Object 
                testValDot1dFilterPermiss
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dFilterPermiss (UINT4 *pu4ErrorCode, INT4 i4Dot1dFilterNumber,
                             tMacAddr Dot1dFilterSrcAddress,
                             tMacAddr Dot1dFilterDstAddress,
                             INT4 i4TestValDot1dFilterPermiss)
{
    tMacAddr            zeroAddr;

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    BRG_MEM_SET (zeroAddr, 0, ETHERNET_ADDR_SIZE);

    /* Filter number is being tested to see whether it is less 
     * than or equal to 0 or greater than the maximum number of filters allowed.
     * If so,error message will be returned.
     */

    if ((i4Dot1dFilterNumber <= 0)
        || (i4Dot1dFilterNumber > (MAX_FILTER_NUMBER - 1)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (!BRG_MEM_CMP (Dot1dFilterSrcAddress, zeroAddr, ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (!BRG_MEM_CMP (Dot1dFilterDstAddress, zeroAddr, ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* source address should not be broadcast or multicast address */
    if ((BM_IS_MAC_MULTICAST (Dot1dFilterSrcAddress[0])) ||
        (BM_IS_BCAST_ADDRESS (Dot1dFilterSrcAddress)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Source & destination address should not be same */
    if (!BRG_MEM_CMP
        (Dot1dFilterSrcAddress, Dot1dFilterDstAddress, ETHERNET_ADDR_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValDot1dFilterPermiss)
    {
        case MACF_DISCARD:
            /* FALL THROUGH */
        case MACF_FORWARD:
            /* FALL THROUGH */
        case MACF_INVALID:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

    }
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dBaseBridgeDebug
 Input       :  The Indices

                The Object 
                testValDot1dBaseBridgeDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dBaseBridgeDebug (UINT4 *pu4ErrorCode,
                               INT4 i4TestValDot1dBaseBridgeDebug)
{

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }
    if ((i4TestValDot1dBaseBridgeDebug < 0) ||
        (i4TestValDot1dBaseBridgeDebug > 255))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Dot1dBaseBridgeTrace
 Input       :  The Indices

                The Object 
                testValDot1dBaseBridgeTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dBaseBridgeTrace (UINT4 *pu4ErrorCode,
                               INT4 i4TestValDot1dBaseBridgeTrace)
{
    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Shutdown; CANNOT set Trace Option\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValDot1dBaseBridgeTrace < BRG_MIN_TRACE_VAL) ||
        (i4TestValDot1dBaseBridgeTrace > BRG_MAX_TRACE_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Dot1dMcastTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dMcastTable
 Input       :  The Indices
                Dot1dMcastMacaddress
                Dot1dMlistNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceDot1dMcastTable (tMacAddr Dot1dMcastMacaddress,
                                         INT4 i4Dot1dMlistNumber)
{

    tMULTICAST_ENTRY   *pPtr;
    INT4                i4Cmp;

    TMO_SLL_Scan (&bdMlist[i4Dot1dMlistNumber].entry, pPtr, tMULTICAST_ENTRY *)
    {
        if ((i4Cmp =
             BuCompareMacaddr (pPtr->multicastAddr,
                               Dot1dMcastMacaddress)) == BRG_EQUAL)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            if (i4Cmp == GREATER)
            {
                break;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dMcastTable
 Input       :  The Indices
                Dot1dMcastMacaddress
                Dot1dMlistNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDot1dMcastTable (tMacAddr * pDot1dMcastMacaddress,
                                 INT4 *pi4Dot1dMlistNumber)
{
    INT4                i4Count;
    tMULTICAST_ENTRY   *pPtr = NULL;

    for (i4Count = 1; i4Count < MAX_MULTILIST_NUMBER; i4Count++)
    {
        TMO_SLL_Scan (&bdMlist[i4Count].entry, pPtr, tMULTICAST_ENTRY *)
        {
            if (pPtr != NULL)
            {
                *pi4Dot1dMlistNumber = i4Count;

                BRG_MEM_CPY ((UINT1 *) *pDot1dMcastMacaddress,
                             pPtr->multicastAddr, ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dMcastTable
 Input       :  The Indices
                Dot1dMcastMacaddress
                nextDot1dMcastMacaddress
                Dot1dMlistNumber
                nextDot1dMlistNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDot1dMcastTable (tMacAddr Dot1dMcastMacaddress,
                                tMacAddr * pNextDot1dMcastMacaddress,
                                INT4 i4Dot1dMlistNumber,
                                INT4 *pi4NextDot1dMlistNumber)
{
    INT4                i4I;
    tMULTICAST_ENTRY   *pPtr;

    if (i4Dot1dMlistNumber < 0)
    {
        return SNMP_FAILURE;
    }
    for (i4I = i4Dot1dMlistNumber; i4I < MAX_MULTILIST_NUMBER; i4I++)
    {
        TMO_SLL_Scan (&bdMlist[i4I].entry, pPtr, tMULTICAST_ENTRY *)
        {
            if ((i4I > i4Dot1dMlistNumber)
                || (BuCompareMacaddr (pPtr->multicastAddr,
                                      Dot1dMcastMacaddress) == GREATER))
            {
                *pi4NextDot1dMlistNumber = i4I;

                BRG_MEM_CPY ((UINT1 *) *pNextDot1dMcastMacaddress,
                             pPtr->multicastAddr, ETHERNET_ADDR_SIZE);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDot1dMcastPermiss
 Input       :  The Indices
                Dot1dMcastMacaddress
                Dot1dMlistNumber

                The Object 
                retValDot1dMcastPermiss
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDot1dMcastPermiss (tMacAddr Dot1dMcastMacaddress,
                         INT4 i4Dot1dMlistNumber,
                         INT4 *pi4RetValDot1dMcastPermiss)
{
    tMULTICAST_ENTRY   *pPtr;
    INT2                i2Cmp;

    TMO_SLL_Scan (&bdMlist[i4Dot1dMlistNumber].entry, pPtr, tMULTICAST_ENTRY *)
    {
        if ((i2Cmp = (INT2) BuCompareMacaddr (pPtr->multicastAddr,
                                              Dot1dMcastMacaddress)) ==
            BRG_EQUAL)
        {
            *pi4RetValDot1dMcastPermiss = pPtr->u1Permiss;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDot1dMcastPermiss
 Input       :  The Indices
                Dot1dMcastMacaddress
                Dot1dMlistNumber

                The Object 
                setValDot1dMcastPermiss
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDot1dMcastPermiss (tMacAddr Dot1dMcastMacaddress,
                         INT4 i4Dot1dMlistNumber,
                         INT4 i4SetValDot1dMcastPermiss)
{
    tMULTICAST_ENTRY   *pMcastEntry, *pPtr, *pPrev;
    INT2                i2Cmp;
    UINT4               u4retval;
    INT4                i4Port;

    TMO_SLL_Scan (&bdMlist[i4Dot1dMlistNumber].entry, pPtr, tMULTICAST_ENTRY *)
    {
        if ((i2Cmp =
             (INT2) BuCompareMacaddr (pPtr->multicastAddr,
                                      Dot1dMcastMacaddress)) == BRG_EQUAL)
        {
            if (i4SetValDot1dMcastPermiss == MCAST_INVALID)
            {

#ifdef NPAPI_WANTED
                /* Delete the multicast entry from the multi list  */
                if (FsBrgMcDeleteEntry (i4Dot1dMlistNumber,
                                        Dot1dMcastMacaddress) != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

#endif
                TMO_SLL_Delete (&bdMlist[i4Dot1dMlistNumber].entry,
                                (tTMO_SLL_NODE *) pPtr);

                if ((u4retval = FREE_MCAST_ENTRY (mcastid, pPtr)) ==
                    BRG_MEM_FAILURE)
                {
#ifdef NPAPI_WANTED
                    /* Create Multicast entry in the H/W */
                    if (FsBrgMcAddEntry
                        (i4Dot1dMlistNumber,
                         Dot1dMcastMacaddress) != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
#endif
                    return SNMP_FAILURE;
                }

                /* Refresh the entries, for ports mapping this empty mcast lists */
                if (TMO_SLL_Count (&bdMlist[i4Dot1dMlistNumber].entry) == 0)
                {

                    /* Check each port and reset mlist number mapped to it */
                    for (i4Port = 1; i4Port <= BRG_MAX_PHY_PLUS_LOG_PORTS;
                         i4Port++)
                    {
                        if (base.portEntry[i4Port].u1MlistNo ==
                            i4Dot1dMlistNumber)
                            base.portEntry[i4Port].u1MlistNo = 0;
                    }
                }

                return SNMP_SUCCESS;
            }

#ifdef NPAPI_WANTED
            /* Enable the status of multicast entry in the multi list  */
            if (FsBrgMcEnAddrInGrp (i4Dot1dMlistNumber, Dot1dMcastMacaddress,
                                    i4SetValDot1dMcastPermiss) != FNP_SUCCESS)
            {
                return SNMP_FAILURE;
            }

#endif
            pPtr->u1Permiss = (UINT1) i4SetValDot1dMcastPermiss;
            return SNMP_SUCCESS;
        }
    }
    if (i4SetValDot1dMcastPermiss == MCAST_INVALID)
    {
        /* Cannot add entry for invalid status */
        return SNMP_FAILURE;
    }

    /*  Entry Not Found  */
#ifdef NPAPI_WANTED
    /* Add multi-cast entry into the multi-cast group ID */
    if (FsBrgMcAddEntry (i4Dot1dMlistNumber, Dot1dMcastMacaddress) !=
        FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((u4retval = ALLOC_MCAST_ENTRY (mcastid, (UINT1 *) &pMcastEntry))
        != BRG_MEM_SUCCESS)
    {
#ifdef NPAPI_WANTED
        /* Add multi-cast entry into the multi-cast group ID */
        if (FsBrgMcDeleteEntry (i4Dot1dMlistNumber, Dot1dMcastMacaddress) !=
            FNP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
#endif
        return SNMP_FAILURE;
    }

    BRG_MEM_CPY (pMcastEntry->multicastAddr,
                 Dot1dMcastMacaddress, ETHERNET_ADDR_SIZE);
    pMcastEntry->u1Permiss = (UINT1) i4SetValDot1dMcastPermiss;
    pPrev = NULL;

    TMO_SLL_Scan (&bdMlist[i4Dot1dMlistNumber].entry, pPtr, tMULTICAST_ENTRY *)
    {
        if (BuCompareMacaddr (pPtr->multicastAddr, Dot1dMcastMacaddress) ==
            GREATER)
        {
            break;
        }
        else
        {
            pPrev = pPtr;
        }
    }
    TMO_SLL_Insert (&bdMlist[i4Dot1dMlistNumber].entry,
                    (tTMO_SLL_NODE *) pPrev, (tTMO_SLL_NODE *) pMcastEntry);

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Dot1dMcastPermiss
 Input       :  The Indices
                Dot1dMcastMacaddress
                Dot1dMlistNumber

                The Object 
                testValDot1dMcastPermiss
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Dot1dMcastPermiss (UINT4 *pu4ErrorCode,
                            tMacAddr Dot1dMcastMacaddress,
                            INT4 i4Dot1dMlistNumber,
                            INT4 i4TestValDot1dMcastPermiss)
{

    if (BRG_SYSTEM_CONTROL != (UINT1) BRG_START)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_MGMT_TRC | BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                 "Bridge System Control should be START before setting this objects !\n ");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }
    /* Check the validity of multicast MAC address */

    if (!(BM_IS_MAC_MULTICAST (Dot1dMcastMacaddress[0])))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Donot  allow to configure a Broadcast address */
    if (BRG_MEM_CMP (Dot1dMcastMacaddress, bcastAddr, ETHERNET_ADDR_SIZE) ==
        BRG_EQUAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Dot1dMlistNumber >= 0) &&
        (i4Dot1dMlistNumber < MAX_MULTILIST_NUMBER))
    {
        switch (i4TestValDot1dMcastPermiss)
        {
            case MCAST_FORWARD:
                /* FALL THROUGH  */

            case MCAST_DISCARD:
                /* FALL THROUGH  */

            case MCAST_INVALID:
                return SNMP_SUCCESS;
            default:
                break;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/*************************** END OF FILE(bridge_low.c) **********************/
