/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtgrpif.c,v 1.2 2007/02/01 14:44:36 iss Exp $
 *
 * Description:Routines for maintaining the forwarding        
 *             database table                            
 *
 *******************************************************************/
#include "brginc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdDeleteStaticEntries                      */
/*                                                                           */
/*    Description               : Routine deletes all static entries (i.e the*/
/*                                entries with status as FDB_MGMT_STATIC)    */
/*                                cached for the particular interface id.    */
/*                                                                           */
/*    Input(s)                  : pIfaceId - InterfaceId for which the       */
/*                                           entries in the FDB are to be    */
/*                                           deleted.                        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK     : On Success                        */
/*                                BRIDGE_NOT_OK : On Failure                        */
/*                                                                           */
/*****************************************************************************/

INT4
BdDeleteStaticEntries (tTMO_INTERFACE * pIfaceId)
{

    tFORWARDING_ENTRY  *pFwdEntry = NULL;
    UINT4               u4Hindex;
    UINT4               u4Fdbid;
    UINT4               u4Mask;

    if (BdCodeFdbid (pIfaceId, &u4Fdbid) == BRIDGE_OK)
    {
        if (IS_FDBID_OF_THIS_TYPE (u4Fdbid, ALL_MEMBER_FDB)
            || IS_FDBID_OF_THIS_TYPE (u4Fdbid, FLAT_ENTRY_FDB))
            u4Mask = 0xff000000;
        else
            u4Mask = 0xff00ffff;

        if (BRG_FDB_HASH_TBL != NULL)
        {
            for (u4Hindex = 0; u4Hindex < HASH_LIST_SIZE; u4Hindex++)
            {
                TMO_HASH_Scan_Bucket (pSHtab, u4Hindex, pFwdEntry,
                                      tFORWARDING_ENTRY *)
                {
                    if ((pFwdEntry->u1Status == FDB_MGMT_STATIC) &&
                        ((pFwdEntry->u4Fdbid &
                          u4Mask) == (u4Fdbid & u4Mask))
                        && (!(IS_UNASSIGNED_FDB (pFwdEntry->u4Fdbid))))
                    {
                        REMOVE_FDB_ENTRY (pFwdEntry);
                        BdFreeFdbEntry (pFwdEntry);
                        pFwdEntry = NULL;
                    }
                }
            }
        }

        return BRIDGE_OK;
    }
    else
    {
        return BRIDGE_NOT_OK;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdCodeFdbid                                */
/*                                                                           */
/*    Description               : Given the InterfaceId then this routine    */
/*                                will code the corresponding FDBID.         */
/*                                                                           */
/*    Input(s)                  : pIfaceId - InterfaceId for which the       */
/*                                           FDB coding should be done.      */
/*                                pu4FdbId - Through which the coded FDBID   */
/*                                           is returned.                    */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : base, grp_tab.                             */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK     : If the coding is successful.      */
/*                                BRIDGE_NOT_OK : Otherwise                         */
/*                                                                           */
/*****************************************************************************/

INT4
BdCodeFdbid (tTMO_INTERFACE * pIfaceId, UINT4 *pU4Fdbid)
{
    UINT4               u4Port;

    *pU4Fdbid = 0x00;

    if ((u4Port = GetPortFromIfaceId (pIfaceId)) == 0xffff)
    {
        return BRIDGE_NOT_OK;
    }
    else
    {
        if (IF_IS_GROUP (u4Port))
        {
            CODE_TYPE_IN_FDBID (*pU4Fdbid, ALL_MEMBER_FDB);
        }
        else
        {
            CODE_TYPE_IN_FDBID (*pU4Fdbid, FLAT_ENTRY_FDB);
        }
        CODE_LOGICAL_PORT_IN_FDBID (*pU4Fdbid, u4Port);
    }
    return BRIDGE_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdAssignAllUnassignedFdb                   */
/*                                                                           */
/*    Description               : Routine make all unassigned FDBIDs         */
/*                                corresponding to the Interface Id take the */
/*                                appropriate FDBID coding.                  */
/*                                                                           */
/*    Input(s)                  : pIfaceId - InterfaceId.                    */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : base, grp_tab.                             */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/

void
BdAssignAllUnassignedFdb (tTMO_INTERFACE * pIfaceId)
{

    UINT4               u4Fdbid;
    UINT1               u1Type, u1IfNo;
    UINT2               u2Subref;
    tFORWARDING_ENTRY  *pFwdEntry;
    UINT4               u4Hindex;

    BdCodeFdbid (pIfaceId, &u4Fdbid);

    if (BRG_FDB_HASH_TBL != NULL)
    {
        for (u4Hindex = 0; u4Hindex < HASH_LIST_SIZE; u4Hindex++)
        {
            TMO_HASH_Scan_Bucket (pSHtab, u4Hindex, pFwdEntry,
                                  tFORWARDING_ENTRY *)
            {
                if (IS_UNASSIGNED_FDB (pFwdEntry->u4Fdbid))
                {
                    u1Type =
                        (UINT1) GET_LOGICAL_PORT_FROM_FDBID (pFwdEntry->
                                                             u4Fdbid);

                    u1IfNo = (UINT1) GET_TYPE_FROM_FDBID (pFwdEntry->u4Fdbid);

                    u2Subref = (UINT2)
                        GET_MEM_INDEX_FROM_FDBID (pFwdEntry->u4Fdbid);

                    if ((pIfaceId->u1_InterfaceType == u1Type)
                        && (pIfaceId->u1_InterfaceNum == u1IfNo)
                        && (pIfaceId->u2_SubReferenceNum == u2Subref))
                    {
                        pFwdEntry->u4Fdbid = u4Fdbid;
                    }
                }
            }
        }
    }
}

/********************** END OF FILE(dtgrpif.c) *******************************/
