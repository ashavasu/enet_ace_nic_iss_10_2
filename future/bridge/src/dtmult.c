/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtmult.c,v 1.2 2007/02/01 14:44:36 iss Exp $
 *
 * Description:Routines to handle Multilist data structure    
 *             for each port                                  
 *
 *******************************************************************/
#include "brginc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdMlistInit                                */
/*                                                                           */
/*    Description               : Creates Buffer Pool for MAX_MCAST_ENTRIES  */
/*                                and initialises all the multicast list     */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdMlist                                    */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK     : On Success                 */
/*                                BRIDGE_NOT_OK : On Failure                 */
/*                                                                           */
/*****************************************************************************/

INT4
BdMlistInit (void)
{
    UINT4               u4I;
    UINT4               u4retval;

    for (u4I = 0; u4I < MAX_MULTILIST_NUMBER; u4I++)
    {
        TMO_SLL_Init (&bdMlist[u4I].entry);
    }
    /* nrmcst - For now, ensure sizeof(tMULTICAST_ENTRY) is 0%4. */
    if ((u4retval = STAP_CREATE_MEM_POOL (sizeof (tMULTICAST_ENTRY),
                                          MAX_MCAST_ENTRIES,
                                          MEM_DEFAULT_MEMORY_TYPE,
                                          &mcastid)) == BRG_MEM_FAILURE)
    {
        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to create memory for multi-cast Table entries  \n");
        return BRIDGE_NOT_OK;
    }

    return BRIDGE_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdIsMcastAddr                              */
/*                                                                           */
/*    Description               : Checks whether the Given address is present*/
/*                                in the specified multicast list of the     */
/*                                multicast table.                           */
/*                                                                           */
/*    Input(s)                  : u1MlistNo - Multicast list number          */
/*                                pMacAddr  - Address to be checked          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdMlist                                    */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : TRUE   : If Address is present in the table*/
/*                                FALSE  : Otherwise                         */
/*                                                                           */
/*****************************************************************************/

tBOOLEAN
BdIsMcastAddr (UINT1 u1MlistNo, tMacAddr * pMacAddr)
{
    tMULTICAST_ENTRY   *pMcastEntry;

    if (u1MlistNo != 0)
    {
        TMO_SLL_Scan (&bdMlist[u1MlistNo].entry, pMcastEntry,
                      tMULTICAST_ENTRY *)
        {
            if (BuCompareMacaddr ((UINT1 *) *pMacAddr,
                                  pMcastEntry->multicastAddr) == BRG_EQUAL)
            {

                if (pMcastEntry->u1Permiss == MCAST_FORWARD)
                {
                    return TRUE;
                }
                else
                {
                    return FALSE;
                }
            }
        }
    }
    else
    {
        return TRUE;
    }
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdSetMlistNo                               */
/*                                                                           */
/*    Description               : Assigns the Multicast List Number for the  */
/*                                Port.                                      */
/*                                                                           */
/*    Input(s)                  : u1PortNo  - Port Number                    */
/*                                u1McastNo - Multicast List Number          */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bd_mcast                                   */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
BdSetMlistNo (UINT1 u1PortNo, UINT1 u1McastNo)
{
    base.portEntry[u1PortNo].u1MlistNo = u1McastNo;
}

/*************************** END OF FILE(dtmult.c) ****************************/
