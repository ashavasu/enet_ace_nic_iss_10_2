/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtfilt.c,v 1.3 2007/02/01 14:44:36 iss Exp $
 *
 * Description:This file consist of routines for the          
 *             filtering mechanism.                    
 *
 *******************************************************************/
#include "brginc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdFilterInit                               */
/*                                                                           */
/*    Description               : Allocates memory for the MAX_FILTER_ENTRIES*/
/*                                and initialises all the filter list        */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK     : On Success.                */
/*                                BRIDGE_NOT_OK : On Failure.                */
/*                                                                           */
/*****************************************************************************/

INT4
BdFilterInit (void)
{
    UINT4               u1I;
    UINT4               retval;

    if ((retval = STAP_CREATE_MEM_POOL (sizeof (tFILTERING_ENTRY),
                                        MAX_FILTER_ENTRIES,
                                        MEM_DEFAULT_MEMORY_TYPE,
                                        &filtid)) == BRG_MEM_FAILURE)
    {

        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to create memory for filering Table entries  \n");
        return BRIDGE_NOT_OK;
    }

    for (u1I = 0; u1I < MAX_FILTER_NUMBER; u1I++)
        TMO_SLL_Init (&bdFilter[u1I].entry);

    return BRIDGE_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdAddFilterEntry                           */
/*                                                                           */
/*    Description               : Routine adds the given entry in to filter  */
/*                                list. If the entry already exists then the */
/*                                status is updated.                         */
/*                                                                           */
/*    Input(s)                  : All Fields in the Filter Entry.            */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdFilter.                                  */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK     : On Success.                */
/*                                BRIDGE_NOT_OK : On Failure.                */
/*                                                                           */
/*****************************************************************************/

INT4
BdAddFilterEntry (UINT1 u1FilterNo, tMacAddr pDestMask,
                  tMacAddr pDestAddr, tMacAddr pSrcMask,
                  tMacAddr pSrcAddr, UINT1 u1Status)
{
    tFILTERING_ENTRY   *pNewEntry;
    UINT4               u4retval;

    if ((u4retval = ALLOC_FILTER_ENTRY (filtid, (UINT1 *) &pNewEntry))
        != BRG_MEM_SUCCESS)
    {
        BRG_TRC (BRG_TRC_FLAG,
                 BRG_MGMT_TRC | BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 "Memory allocation failure, "
                 "during addition of entry to the filter table\n ");
        if (u1Status == MACF_INVALID)
        {
            return BRIDGE_OK;
        }
        return BRIDGE_NOT_OK;
    }
    BRG_MEM_CPY (pNewEntry->destMask, pDestMask, ETHERNET_ADDR_SIZE);
    BRG_MEM_CPY (pNewEntry->destAddr, pDestAddr, ETHERNET_ADDR_SIZE);
    BRG_MEM_CPY (pNewEntry->sourceMask, pSrcMask, ETHERNET_ADDR_SIZE);
    BRG_MEM_CPY (pNewEntry->sourceAddr, pSrcAddr, ETHERNET_ADDR_SIZE);
    pNewEntry->u1Status = u1Status;

    TMO_SLL_Insert (&bdFilter[u1FilterNo].entry,
                    TMO_SLL_Last (&bdFilter[u1FilterNo].entry),
                    (tTMO_SLL_NODE *) pNewEntry);

    return BRIDGE_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdIsfiltered                               */
/*                                                                           */
/*    Description               : checks whether a Given Source address and  */
/*                                Destination address is filtered in the     */
/*                                specified filter list.                     */
/*                                                                           */
/*    Input(s)                  : u1FilterNo - Filter list to be checked.    */
/*                                destAddr   - Destination MacAddress.       */
/*                                srcAddr    - Source MacAddress             */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdFilter.                                  */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : TRUE   : If the specified destination and  */
/*                                         Source address is filtered in the */
/*                                         List specified.                   */
/*                                FALSE  : Otherwise.                        */
/*                                                                           */
/*****************************************************************************/

tBOOLEAN
BdIsfiltered (UINT1 u1FilterNo, tMacAddr * destAddr, tMacAddr * srcAddr)
{
    tFILTERING_ENTRY   *pFil;

    TMO_SLL_Scan (&bdFilter[u1FilterNo].entry, pFil, tFILTERING_ENTRY *)
    {
        /* Source address is checked */
        if (BrgCheckFilterEntry
            (pFil->sourceMask, pFil->sourceAddr, (UINT1 *) *srcAddr))
        {
            /* Destination address is checked */
            if (BrgCheckFilterEntry
                (pFil->destMask, pFil->destAddr, (UINT1 *) *destAddr))
            {
                if (pFil->u1Status == MACF_DISCARD)
                {
                    return TRUE;
                }
                else
                {
                    return FALSE;
                }
            }
        }
    }
    return FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdGetFilterEntryFromIndex                  */
/*                                                                           */
/*    Description               : Search for the specified filter entry in   */
/*                                the filter table.                          */
/*                                                                           */
/*    Input(s)                  : u1FilterNo - Filter list to be checked.    */
/*                                destAddr   - Destination MacAddress.       */
/*                                srcAddr    - Source MacAddress             */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdFilter.                                  */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : FILTERING_ENTRY  - If the Entry found      */
/*                                NULL             - Otherwise.              */
/*                                                                           */
/*****************************************************************************/

tFILTERING_ENTRY   *
BdGetFilterEntryFromIndex (UINT1 u1FilterNo, tMacAddr pDestAddr,
                           tMacAddr pSrcAddr)
{
    tFILTERING_ENTRY   *pFil;

    for (pFil =
         (tFILTERING_ENTRY *) (TMO_SLL_First (&bdFilter[u1FilterNo].entry));
         pFil != NULL;
         pFil =
         (tFILTERING_ENTRY *) TMO_SLL_Next ((&bdFilter[u1FilterNo].entry),
                                            ((tTMO_SLL_NODE *) (pFil))))
    {

        if (BuCompareMacaddr (pFil->destAddr, pDestAddr) == BRG_EQUAL
            && BuCompareMacaddr (pFil->sourceAddr, pSrcAddr) == BRG_EQUAL)
        {
            return pFil;
        }
    }

    return (tFILTERING_ENTRY *) 0;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdDeleteFilterEntry                        */
/*                                                                           */
/*    Description               : Deletes the specified filter entry from    */
/*                                the filter table.                          */
/*                                                                           */
/*    Input(s)                  : u1FilterNo - Filter list to be checked.    */
/*                                destAddr   - Destination MacAddress.       */
/*                                srcAddr    - Source MacAddress             */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdFilter.                                  */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK      : On Success.               */
/*                                BRIDGE_NOT_OK  : On Failure.               */
/*                                                                           */
/*****************************************************************************/

INT4
BdDeleteFilterEntry (UINT1 u1FilterNo, tMacAddr pDestAddr, tMacAddr pSrcAddr)
{
    tFILTERING_ENTRY   *pFil;
    UINT4               u4retval;

    TMO_SLL_Scan (&bdFilter[u1FilterNo].entry, pFil, tFILTERING_ENTRY *)
    {
        if ((BuCompareMacaddr (pFil->destAddr, pDestAddr) == BRG_EQUAL)
            && (BuCompareMacaddr (pFil->sourceAddr, pSrcAddr) == BRG_EQUAL))
            break;
    }

    if (pFil == NULL)
    {                            /* Entry Not Found */
        return BRIDGE_NOT_OK;
    }

    TMO_SLL_Delete (&bdFilter[u1FilterNo].entry, (tTMO_SLL_NODE *) pFil);
    u4retval = FREE_FILTER_ENTRY (filtid, pFil);
    if (u4retval == BRG_MEM_FAILURE)
    {
        return BRIDGE_NOT_OK;
    }
    return BRIDGE_OK;
}

/********************** END OF FILE(dtfilt.c) *******************************/
