/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tphclow.c,v 1.3 2007/02/01 14:44:36 iss Exp $
 *
 * Description:This file contains low-level routines for some of the 
 * Bridge extension MIB objects.      
 *
 *******************************************************************/

#include "brginc.h"

/* LOW LEVEL Routines for Table : Dot1dTpHCPortTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dTpHCPortTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1dTpHCPortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceDot1dTpHCPortTable (INT4 i4Dot1dTpPort)
{
#ifdef SYS_HC_WANTED
    if ((i4Dot1dTpPort >= 1) && (i4Dot1dTpPort <= BRG_MAX_PHY_PLUS_LOG_PORTS))
    {

        if ((base.portEntry[i4Dot1dTpPort].u1AdminStatus != BRIDGE_INVALID) &&
            (base.portEntry[i4Dot1dTpPort].u4IfSpeed > BRG_HIGH_SPEED))
        {

            return SNMP_SUCCESS;

        }
    }

    /* Not a valid index return failure */

    return SNMP_FAILURE;
#else
    /*This object is not supported in case if SYS_HC_WANTED not defined. */
    /* Set to zero and return success, so that walk would not stop. */
    i4Dot1dTpPort = 0;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dTpHCPortTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1dTpHCPortTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexDot1dTpHCPortTable (INT4 *pi4Dot1dTpPort)
{
    if (BrgSnmpGetNextPortEntry (0, pi4Dot1dTpPort) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dTpHCPortTable
 Input       :  The Indices
                Dot1dTpPort
                nextDot1dTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1dTpHCPortTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexDot1dTpHCPortTable (INT4 i4Dot1dTpPort, INT4 *pi4NextDot1dTpPort)
{
    if (i4Dot1dTpPort < 0)
    {
        return SNMP_FAILURE;
    }
    if (BrgSnmpGetNextPortEntry (i4Dot1dTpPort, pi4NextDot1dTpPort) ==
        SNMP_SUCCESS)
    {
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dTpHCPortInFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpHCPortInFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpHCPortInFrames ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1dTpHCPortInFrames (INT4 i4Dot1dTpPort,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValDot1dTpHCPortInFrames)
{
    pu8RetValDot1dTpHCPortInFrames->msn =
        tpInfo.portEntry[i4Dot1dTpPort].u4TpInOverflow;

    pu8RetValDot1dTpHCPortInFrames->lsn =
        tpInfo.portEntry[i4Dot1dTpPort].u4TpIn;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpHCPortOutFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpHCPortOutFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpHCPortOutFrames ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1dTpHCPortOutFrames (INT4 i4Dot1dTpPort,
                              tSNMP_COUNTER64_TYPE *
                              pu8RetValDot1dTpHCPortOutFrames)
{
    pu8RetValDot1dTpHCPortOutFrames->msn =
        tpInfo.portEntry[i4Dot1dTpPort].u4TpOutOverflow;

    pu8RetValDot1dTpHCPortOutFrames->lsn =
        tpInfo.portEntry[i4Dot1dTpPort].u4TpOut;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpHCPortInDiscards
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpHCPortInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpHCPortInDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1dTpHCPortInDiscards (INT4 i4Dot1dTpPort,
                               tSNMP_COUNTER64_TYPE *
                               pu8RetValDot1dTpHCPortInDiscards)
{
    pu8RetValDot1dTpHCPortInDiscards->msn =
        tpInfo.portEntry[i4Dot1dTpPort].u4ProtoInDiscardOverflow +
        tpInfo.portEntry[i4Dot1dTpPort].u4FilterInDiscardOverflow;

    pu8RetValDot1dTpHCPortInDiscards->lsn =
        tpInfo.portEntry[i4Dot1dTpPort].u4NoProtoInDiscard +
        tpInfo.portEntry[i4Dot1dTpPort].u4NoFilterInDiscard;

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Dot1dTpPortOverflowTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDot1dTpPortOverflowTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceDot1dTpPortOverflowTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceDot1dTpPortOverflowTable (INT4 i4Dot1dTpPort)
{
#ifdef SYS_HC_WANTED
    if ((i4Dot1dTpPort >= 1) && (i4Dot1dTpPort <= BRG_MAX_PHY_PLUS_LOG_PORTS))
    {

        if ((base.portEntry[i4Dot1dTpPort].u1AdminStatus != BRIDGE_INVALID) &&
            (base.portEntry[i4Dot1dTpPort].u4IfSpeed > BRG_HIGH_SPEED))
        {

            return SNMP_SUCCESS;
        }
    }
    /* Not a valid index return failure */

    return SNMP_FAILURE;
#else
    /*This object is not supported in case if SYS_HC_WANTED not defined. */
    /* Set to zero and return success, so that walk would not stop. */
    i4Dot1dTpPort = 0;
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDot1dTpPortOverflowTable
 Input       :  The Indices
                Dot1dTpPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstDot1dTpPortOverflowTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexDot1dTpPortOverflowTable (INT4 *pi4Dot1dTpPort)
{
    if (BrgSnmpGetNextPortEntry (0, pi4Dot1dTpPort) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexDot1dTpPortOverflowTable
 Input       :  The Indices
                Dot1dTpPort
                nextDot1dTpPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextDot1dTpPortOverflowTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexDot1dTpPortOverflowTable (INT4 i4Dot1dTpPort,
                                         INT4 *pi4NextDot1dTpPort)
{
    if (i4Dot1dTpPort < 0)
    {
        return SNMP_FAILURE;
    }
    if (BrgSnmpGetNextPortEntry (i4Dot1dTpPort, pi4NextDot1dTpPort) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDot1dTpPortInOverflowFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortInOverflowFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpPortInOverflowFrames ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1dTpPortInOverflowFrames (INT4 i4Dot1dTpPort,
                                   UINT4 *pu4RetValDot1dTpPortInOverflowFrames)
{
    *pu4RetValDot1dTpPortInOverflowFrames =
        tpInfo.portEntry[i4Dot1dTpPort].u4TpInOverflow;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortOutOverflowFrames
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortOutOverflowFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpPortOutOverflowFrames ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1dTpPortOutOverflowFrames (INT4 i4Dot1dTpPort,
                                    UINT4
                                    *pu4RetValDot1dTpPortOutOverflowFrames)
{
    *pu4RetValDot1dTpPortOutOverflowFrames =
        tpInfo.portEntry[i4Dot1dTpPort].u4TpOutOverflow;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDot1dTpPortInOverflowDiscards
 Input       :  The Indices
                Dot1dTpPort

                The Object 
                retValDot1dTpPortInOverflowDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetDot1dTpPortInOverflowDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetDot1dTpPortInOverflowDiscards (INT4 i4Dot1dTpPort,
                                     UINT4
                                     *pu4RetValDot1dTpPortInOverflowDiscards)
{
    *pu4RetValDot1dTpPortInOverflowDiscards =
        tpInfo.portEntry[i4Dot1dTpPort].u4ProtoInDiscardOverflow +
        tpInfo.portEntry[i4Dot1dTpPort].u4FilterInDiscardOverflow;

    return SNMP_SUCCESS;
}

/**************** SNMP Utility Routines ***********************************/

INT1
BrgSnmpGetNextPortEntry (INT4 i4Dot1dTpPort, INT4 *pi4Dot1dTpPort)
{
    tBOOLEAN            bFound = FALSE;
    UINT4               u4PortNo;
    UINT1               u1InterfaceNum = 0xff;

    for (u4PortNo = (UINT4) gu2BrgStartPortInd;
         u4PortNo != BRG_INVALID_PORT_INDEX;
         u4PortNo = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4PortNo))
    {
        if ((base.portEntry[u4PortNo].portId.u1_InterfaceNum >
             i4Dot1dTpPort)
            && (u1InterfaceNum >
                base.portEntry[u4PortNo].portId.u1_InterfaceNum))
        {

            bFound = TRUE;

            u1InterfaceNum = base.portEntry[u4PortNo].portId.u1_InterfaceNum;

            break;
        }
    }

    *pi4Dot1dTpPort = u1InterfaceNum;

    if (bFound == TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}
