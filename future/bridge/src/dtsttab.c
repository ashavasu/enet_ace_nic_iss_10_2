/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dtsttab.c,v 1.5 2007/02/01 14:44:36 iss Exp $
 *
 * Description:Routines to handle Multilist data structure     
 *             for each port                                 
 *
 *******************************************************************/
#include "brginc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdStaticTableInit                          */
/*                                                                           */
/*    Description               : Creates Buffer Pool for                    */
/*                                MAX_STATICTABLE_ENTRIES and initialises    */
/*                                the list                                   */
/*                                                                           */
/*    Input(s)                  : None                                       */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdStaticTable                              */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK     : On Success                        */
/*                                BRIDGE_NOT_OK : On Failure                        */
/*                                                                           */
/*****************************************************************************/

INT4
BdStaticTableInit (void)
{
    UINT4               u4retval;
    UINT2               u2Index;

    /* Fix - related to the creation of memory pool for statictable entry */
    if ((u4retval = STAP_CREATE_MEM_POOL (sizeof (tSTATICTABLE_ENTRY),
                                          MAX_STATICTABLE_ENTRIES,
                                          MEM_DEFAULT_MEMORY_TYPE,
                                          &staticTabid)) == BRG_MEM_FAILURE)
    {

        BRG_TRC (BRG_TRC_FLAG, BRG_OS_RESOURCE_TRC | BRG_ALL_FAILURE_TRC,
                 BRG_MOD_NAME,
                 " Failed to create memory for Static Table entries  \n");
        return BRIDGE_NOT_OK;
    }
    /* Initialize the static list for each port from 0 to 
     * BRG_MAX_PHY_PLUS_LOG_PORTS 
     */
    /* Initialize for port 0, rule applies to all ports */
    /* Also the port number is directly mapped */
    for (u2Index = 0; u2Index <= BRG_MAX_PHY_PLUS_LOG_PORTS; u2Index++)
        TMO_SLL_Init (&bdStaticTable[u2Index].entry);

    return BRIDGE_OK;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdAddStaticEntry                           */
/*                                                                           */
/*    Description               : Allocates new entries into the static      */
/*                                filter table                               */
/*                                                                           */
/*    Input(s)                  : All Static fileds to be added              */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdStaticTable                              */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK     : On Success                        */
/*                                BRIDGE_NOT_OK : On Failure                        */
/*                                                                           */
/*****************************************************************************/
INT4
BdAddStaticEntry (UINT1 u1RecvPortNo, tMacAddr StaticAddress,
                  UINT1 pu1AllowedStaticPorts[MAX_NO_BYTES],
                  UINT1 u1StaticStatus)
{
    tSTATICTABLE_ENTRY *pnewEntry;
    tSTATICTABLE_ENTRY *pPrev;
    tSTATICTABLE_ENTRY *pPtr;
    UINT4               i;
    UINT4               u4retval;
    if ((u4retval =
         ALLOC_STATICTABLE_ENTRY (staticTabid,
                                  (UINT1 *) &pnewEntry)) != BRG_MEM_SUCCESS)
    {
        if (u1StaticStatus == STATIC_INVALID)
        {
            return BRIDGE_OK;
        }
        return BRIDGE_NOT_OK;
    }
    pnewEntry->u1StaticReceivePort = u1RecvPortNo;
    pnewEntry->u1StaticStatus = u1StaticStatus;
    BRG_MEM_CPY (pnewEntry->staticAddress, StaticAddress, ETHERNET_ADDR_SIZE);
    for (i = 0; i < MAX_NO_BYTES; i++)
    {
        pnewEntry->u1StaticAllowedToGo[i] = pu1AllowedStaticPorts[i];
    }
    if (u1StaticStatus == STATIC_DELETEONTIMEOUT)
    {
        pnewEntry->timeout.u4TickCount = STATIC_TABLE_ENTRY_AGEOUT_TIME;
    }

    /* Updating FDB Table here with status as 'FDB_OTHER' and 
     * port number as 'NO_PORT' results in 
     * dropping of the frames sourced from that specific MAC address.
     * Hence, such a FDB Updating is removed from this function.
     */

    pPrev = NULL;
    TMO_SLL_Scan (&bdStaticTable[u1RecvPortNo].entry, pPtr,
                  tSTATICTABLE_ENTRY *)
    {
        if ((StaticCompareMacaddrAndPort
             (pPtr->staticAddress, pPtr->u1StaticReceivePort,
              StaticAddress, u1RecvPortNo)) == GREATER)
        {
            break;
        }
        else
        {
            pPrev = pPtr;
        }
    }
    TMO_SLL_Insert (&bdStaticTable[u1RecvPortNo].entry,
                    (tTMO_SLL_NODE *) pPrev, (tTMO_SLL_NODE *) pnewEntry);
    return BRIDGE_OK;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BuCompareStaticaddrAndReceiveport          */
/*                                                                           */
/*    Description               : Routine compares the Static Address and    */
/*                                received port.                             */
/*                                                                           */
/*    Input(s)                  : addr1                 - Address            */
/*                                i4StaticReceivedPort1 - Received Port      */
/*                                addr2                 - Address            */
/*                                i4StaticReceivedPort2 - Received Port      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : EQUAL : If addr1 equal to addr2 &         */
/*                                         Received Port1 equal to           */
/*                                         Received Port2.                   */
/*                                GREATER: If addr1 greater than addr2 or    */
/*                                         addr1 equal to addr2 & Port1      */
/*                                         greater than port2.               */
/*                                LESSER : If addr1 less than addr2 or       */
/*                                         addr1 equal to addr2 & Port1      */
/*                                         less than Port2.                  */
/*                                                                           */
/*****************************************************************************/

INT4
BuCompareStaticaddrAndReceiveport (tMacAddr addr1,
                                   INT4 i4StaticReceivePort1,
                                   tMacAddr addr2, INT4 i4StaticReceivePort2)
{
    INT4                i4Val = BRG_EQUAL;

    if (BRG_MEM_CMP (addr1, addr2, ETHERNET_ADDR_SIZE) < 0)
        i4Val = LESSER;
    else if (BRG_MEM_CMP (addr1, addr2, ETHERNET_ADDR_SIZE) > 0)
        i4Val = GREATER;

    if (i4Val == BRG_EQUAL)
    {
        if (i4StaticReceivePort1 > i4StaticReceivePort2)
            i4Val = GREATER;
        else if (i4StaticReceivePort1 < i4StaticReceivePort2)
            i4Val = LESSER;
    }

    return i4Val;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdFilterFrameBasedOnStaticTable            */
/*                                                                           */
/*    Description               : Routine filter the frame based on the      */
/*                                static table entries.                      */
/*                                                                           */
/*    Input(s)                  : pPtr    - Pointer to the Table Entry       */
/*                                pMsg    - Pointer to the Message           */
/*                                msgAttr - Pointer to the MessageAttribute  */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None.                                      */
/*                                                                           */
/*****************************************************************************/

void
BdFilterFrameBasedOnStaticTable (tSTATICTABLE_ENTRY * pPtr,
                                 tCRU_BUF_CHAIN_HEADER * pMsg,
                                 tMSG_ATTR * msgAttr)
{
    UINT4               u4PortNo, u4Index = 0;

    UINT1               u1Mask = 0x80, u1StaticAllowedToGo;

    UINT4               u4ReceivePort, u4DataLength;

    tCRU_BUF_CHAIN_HEADER *pMsgDum = NULL;

    u4ReceivePort = msgAttr->u2LogicalPortNo;

    /* get the AllowedTogo ports & do the following for every port */
    for (u4PortNo = (UINT4) gu2BrgStartPortInd;
         u4PortNo != BRG_INVALID_PORT_INDEX;
         u4PortNo = BRG_GET_NEXT_PORT_INDEX ((UINT2) u4PortNo))
    {
        if (u4PortNo > 8)
        {
            if ((u4PortNo % 8) == 1)
                u1Mask = 0x80;

            u4Index = u4PortNo / 8;
        }
        u1StaticAllowedToGo = pPtr->u1StaticAllowedToGo[u4Index];

        if (u1StaticAllowedToGo & u1Mask)
        {
            /* this port allows the frame to be forwarded */
            if ((u4PortNo != u4ReceivePort) &&
                (TpIsPortOkForForwarding (u4PortNo, msgAttr, FALSE) == TRUE))
            {

            /*******************************************************
             *** Frame is Ready to forward at the Logical port   ***
             *** Level.                                          ***
             *******************************************************/
#ifdef SYS_HC_WANTED
                BRG_INCR_HC_OR_TP_PORT_OUT_FRAMES (u4PortNo);
#else /* SYS_HC_WANTED */
                tpInfo.portEntry[u4PortNo].u4TpOut++;
#endif /* SYS_HC_WANTED */

                if ((pMsgDum =
                     (tCRU_BUF_CHAIN_HEADER *)
                     CRU_BUF_Duplicate_BufChain (pMsg)) != NULL)
                {
                    u4DataLength = CRU_BUF_Get_ChainValidByteCount (pMsgDum);

                    /* BCPCHANGES - 17/10 - start */
                    switch (base.portEntry[u4PortNo].portId.u1_InterfaceType)
                    {
                        case CFA_ENET:
                            BRG_TRC (BRG_TRC_FLAG,
                                     BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                                     BRG_MOD_NAME,
                                     "Looking Static Tbl, "
                                     "Data Frame given to CFA \n");
                            BRG_TRC_ARG1 (BRG_TRC_FLAG,
                                          BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                                          BRG_MOD_NAME,
                                          "to handle packet for ethernet "
                                          "interface of port %d\n", u4PortNo);

                            BridgeHandleOutFrame (pMsgDum, (UINT2) u4PortNo,
                                                  u4DataLength, 0,
                                                  CFA_ENCAP_NONE);
                            break;
                        default:

                            STAP_BUF_RELEASE_CHAIN (pMsgDum, FALSE);
                            BRG_TRC (BRG_TRC_FLAG,
                                     BRG_DATA_PATH_TRC | BRG_BUFFER_TRC |
                                     BRG_ALL_FAILURE_TRC, BRG_MOD_NAME,
                                     " Looking Static Tbl,"
                                     " relxd buffer, for "
                                     " invalid interface \n");

                            break;
                    }
                    /*End - 17/10 */
                }
                else
                {
                    /* To be alerted for BUFFER duplication failure */
                    BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                             BRG_MOD_NAME,
                             " Unable to duplicate the packet,"
                             " during the Static Tbl forwarding  \n");
                    return;
                }
            }
            else
            {
            /*******************************************************
             *** Learned Port is Not OK for forwarding this frame *
             *** Release Buffer                                  ***
             *******************************************************/

                BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC | BRG_BUFFER_TRC,
                         BRG_MOD_NAME,
                         "During static Tbl fwding, "
                         "Output port not in FORWARDING State  \n");
            }
        }
        u1Mask = (UINT1) (u1Mask >> 1);
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BrgFindStaticEntryInTbl                    */
/*                                                                           */
/*    Description               : Routine checks whether the MacAddress      */
/*                                present on the Static table.               */
/*                                Called by data forwarding module */
/*                                                                           */
/*    Input(s)                  : StaticAddress  - MacAddress to be checked  */
/*                                i4StaticReceivePort - Received Port        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : STATICTABLEENTRY : If Entry found.         */
/*                                NULL             : If Entry not found.     */
/*                                                                           */
/*****************************************************************************/
tSTATICTABLE_ENTRY *
BrgFindStaticEntryInTbl (tMacAddr StaticAddress, UINT4 i4StaticReceivePort)
{
    tSTATICTABLE_ENTRY *pPtr = NULL;
    INT4                i4Cmp;
    /* Fix -related to traversing the static list */
    TMO_SLL_Scan (&bdStaticTable[i4StaticReceivePort].entry, pPtr,
                  tSTATICTABLE_ENTRY *)
    {
        if ((i4Cmp =
             BuCompareStaticaddrAndReceiveport (pPtr->staticAddress,
                                                pPtr->u1StaticReceivePort,
                                                StaticAddress,
                                                i4StaticReceivePort)) ==
            BRG_EQUAL)
        {
            BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC, BRG_MOD_NAME,
                     "Static Entry available, "
                     "for the Destination Address in the input port\n");

            return pPtr;
        }
    }
    /* Check whether the static address is available in common Static table */
    if ((pPtr = BdIsMacAddrPresentOnCommonStaticTable (StaticAddress))
        != (tSTATICTABLE_ENTRY *) NULL)
        return pPtr;
    return NULL;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdIsMacaddrPresentOnStaticTable            */
/*                                                                           */
/*    Description               : Routine checks whether the MacAddress      */
/*                                present on the Static table.               */
/*                                                                           */
/*    Input(s)                  : StaticAddress  - MacAddress to be checked  */
/*                                i4StaticReceivePort - Received Port        */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : STATICTABLEENTRY : If Entry found.         */
/*                                NULL             : If Entry not found.     */
/*                                                                           */
/*****************************************************************************/
tSTATICTABLE_ENTRY *
BdIsMacaddrPresentOnStaticTable (tMacAddr StaticAddress,
                                 UINT4 i4StaticReceivePort)
{
    tSTATICTABLE_ENTRY *pPtr = NULL;
    INT4                i4Cmp;

    if (i4StaticReceivePort != BRG_STATIC_ALL_PORT)
    {
        TMO_SLL_Scan (&bdStaticTable[i4StaticReceivePort].entry, pPtr,
                      tSTATICTABLE_ENTRY *)
        {
            if ((i4Cmp =
                 BuCompareStaticaddrAndReceiveport (pPtr->staticAddress,
                                                    pPtr->u1StaticReceivePort,
                                                    StaticAddress,
                                                    i4StaticReceivePort)) ==
                BRG_EQUAL)
            {
                BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC, BRG_MOD_NAME,
                         "Static Entry available, "
                         "for the Destination Address in the input port\n");

                return pPtr;
            }
        }
    }
    else
    {
        /* Check whether the static address is available in common Static table */
        if ((pPtr = BdIsMacAddrPresentOnCommonStaticTable (StaticAddress))
            != (tSTATICTABLE_ENTRY *) NULL)
            return pPtr;
    }
    return NULL;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : StaticCompareMacaddrAndPort                */
/*                                                                           */
/*    Description               : Routine compares the given two mac address */
/*                                and given two received ports.              */
/*                                                                           */
/*    Input(s)                  : addr1                - Address1            */
/*                                i4StaticReceivePort1 - Received Port1      */
/*                                addr2                - Address2            */
/*                                i4StaticReceivedPort2- Received Port2      */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : EUQLAL : If addr1 equal to addr2 &         */
/*                                         Received Port1 equal to           */
/*                                         Received Port2.                   */
/*                                GREATER: If addr1 greater than addr2 or    */
/*                                         addr1 equal to addr2 & Port1      */
/*                                         greater than port2.               */
/*                                LESSER : If addr1 less than addr2 or       */
/*                                         addr1 equal to addr2 & Port1      */
/*                                         less than Port2.                  */
/*                                                                           */
/*****************************************************************************/

INT4
StaticCompareMacaddrAndPort (tMacAddr addr1, INT4 i4StaticReceivePort1,
                             tMacAddr addr2, INT4 i4StaticReceivePort2)
{
    INT4                i4Val = BRG_EQUAL;

    if (BRG_MEM_CMP (addr1, addr2, ETHERNET_ADDR_SIZE) < 0)
        i4Val = LESSER;
    else if (BRG_MEM_CMP (addr1, addr2, ETHERNET_ADDR_SIZE) > 0)
        i4Val = GREATER;

    if (i4Val == BRG_EQUAL)
    {
        if (i4StaticReceivePort1 > i4StaticReceivePort2)
            i4Val = GREATER;
        else if (i4StaticReceivePort1 < i4StaticReceivePort2)
            i4Val = LESSER;
    }
    return i4Val;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdRemoveStatictableEntry                   */
/*                                                                           */
/*    Description               : Remove the static entry from the common    */
/*                                static list maintained for the port.       */
/*                                                                           */
/*    Input(s)                  : i4Port   - Port Number                     */
/*                                pPtr     - Pointer to the Static           */
/*                                           table entry.                    */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdStaticTable                              */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK     : If Available.                     */
/*                                BRIDGE_NOT_OK : If Not Available            .     */
/*                                                                           */
/*****************************************************************************/

INT1
BdRemoveStatictableEntry (tSTATICTABLE_ENTRY * pPtr)
{
    UINT4               u4ReturnValue;
    TMO_SLL_Delete (&bdStaticTable[0].entry, (tTMO_SLL_NODE *) pPtr);
    if ((u4ReturnValue = FREE_STATICTABLE_ENTRY (staticTabid, pPtr)) ==
        BRG_MEM_FAILURE)
        return BRIDGE_NOT_OK;
    return BRIDGE_OK;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdRemoveStaticEntryFromList                */
/*                                                                           */
/*    Description               : Remove the static entry from the           */
/*                                static list maintained for the port.       */
/*                                                                           */
/*    Input(s)                  : i4Port   - Port Number                     */
/*                                pPtr     - Pointer to the Static           */
/*                                           table entry.                    */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdStaticTable                              */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : BRIDGE_OK     : If Available.                     */
/*                                BRIDGE_NOT_OK : If Not Available                  */
/*                                                                           */
/*****************************************************************************/

INT1
BdRemoveStaticEntryFromList (INT4 i4Port, tSTATICTABLE_ENTRY * pPtr)
{
    UINT4               u4ReturnValue;
    tFORWARDING_ENTRY  *pFdb = NULL;
    /* Check whether static address already exist in FDB Tbl and delete it */
    if ((pFdb = (tFORWARDING_ENTRY *) HashSearchNode (pSHtab,
                                                      (tMacAddr *) pPtr->
                                                      staticAddress)) != NULL)
    {
        TmStopTimer (&pFdb->ageout);
        REMOVE_FDB_ENTRY (pFdb);
        BdFreeFdbEntry (pFdb);
        BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC, BRG_MOD_NAME,
                 " Deleted static entry from FDB Tbl \n");

    }
    TMO_SLL_Delete (&bdStaticTable[i4Port].entry, (tTMO_SLL_NODE *) pPtr);
    if ((u4ReturnValue = FREE_STATICTABLE_ENTRY (staticTabid, pPtr)) ==
        BRG_MEM_FAILURE)
        return BRIDGE_NOT_OK;
    return BRIDGE_OK;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name             : BdIsMacAddrPresentOnCommonStaticTable      */
/*                                                                           */
/*    Description               : Check whether the entry is available in    */
/*                                the common static list.            .       */
/*                                                                           */
/*    Input(s)                  : StaticAddress - Address to be checked.     */
/*                                                                           */
/*    Output(s)                 : None                                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : bdStaticTable                              */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : STATICTABLEENTRY : If Entry found.         */
/*                                NULL             : If Entry not found.     */
/*                                                                           */
/*****************************************************************************/

tSTATICTABLE_ENTRY *
BdIsMacAddrPresentOnCommonStaticTable (tMacAddr StaticAddress)
{
    tSTATICTABLE_ENTRY *pPtr;
    INT4                i4Cmp;
    /* Check whether the entry is in common - zero port list */
    TMO_SLL_Scan (&bdStaticTable[0].entry, pPtr, tSTATICTABLE_ENTRY *)
    {
        if ((i4Cmp =
             BuCompareMacaddr (pPtr->staticAddress, StaticAddress)) ==
            BRG_EQUAL)
        {
            BRG_TRC (BRG_TRC_FLAG, BRG_DATA_PATH_TRC, BRG_MOD_NAME,
                     " Static Entry is available, " " in zero port list\n");

            return pPtr;
        }
    }

    return NULL;
}

/* ********************** END OF FILE - dtstatictab.c ************** */
