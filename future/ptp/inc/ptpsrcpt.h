
/**********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpsrcpt.h,v 1.3 2016/04/05 13:23:55 siva Exp $
 *
 * Description: This has prototype definitions for PTP SRC submodule
 *
 ***********************************************************************/
#ifndef __PTPSRCPT_H__
#define __PTPSRCPT_H__


/**********************************************************************
                        ptpsrc.c
**********************************************************************/

INT4
PtpSrcScalars PROTO ((tCliHandle CliHandle));

PUBLIC VOID
PtpSrcIfDetails PROTO ((tCliHandle CliHandle, INT4 i4NextContextId,
                                 INT4 i4NextDomainNumber,INT4 i4IfIndex));

VOID
PtpSrcClockDetails PROTO ((tCliHandle CliHandle,INT4 i4ContextId, INT4 i4DomainId));

VOID
PtpSrcInterfaces PROTO ((tCliHandle CliHandle,INT4 i4CurrDomainId));

VOID
PtpSrcClocks PROTO ((tCliHandle CliHandle));

#endif /* __PTPSRCPT_H__ */
