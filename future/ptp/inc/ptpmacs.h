/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpmacs.h,v 1.3 2011/12/13 14:29:04 siva Exp $
 *
 * Description: This file contains MACROS definitions used in PTP Module.
 *
 *****************************************************************************/
#ifndef _PTPMACS_H_
#define _PTPMACS_H_

#define CMSG_NXTHDR(mhdr, cmsg) __cmsg_nxthdr (mhdr, cmsg)

#define PTP_LBUF_GET_1_BYTE(pBuf, u1Value) \
{\
    u1Value = pBuf[0]; \
    pBuf += 1;\
}
#define PTP_LBUF_GET_2_BYTES(pBuf, u2Value) \
{\
    UINT2 u2TemVal = 0; \
    MEMCPY (&u2TemVal, pBuf, 2);\
    u2Value = (UINT2) (OSIX_NTOHS(u2TemVal));\
    pBuf += 2;\
}

#define PTP_LBUF_GET_4_BYTES(pBuf, u4Value) \
{\
    UINT4 u4TemVal = 0; \
    MEMCPY (&u4TemVal, pBuf, 4);\
    u4Value = (UINT4) (OSIX_NTOHL(u4TemVal));\
    pBuf += 4;\
}

#define PTP_LBUF_PUT_1_BYTE(pBuf, u1Value) \
{\
    *pBuf = u1Value; \
    pBuf += 1;\
}

#define PTP_LBUF_PUT_2_BYTES(pBuf, u2Value) \
{\
    UINT2  u2TmpValue = 0;\
    u2TmpValue = (UINT2) OSIX_HTONS (u2Value);\
    MEMCPY (pBuf, &u2TmpValue, 2);\
    pBuf += 2;\
}

#define PTP_LBUF_PUT_4_BYTES(pBuf, u4Value) \
{\
    UINT4  u4TmpValue = 0;\
    u4TmpValue = (UINT4) OSIX_HTONL (u4Value);\
    MEMCPY (pBuf, &u4TmpValue, 4);\
    pBuf += 4;\
}

#define PTP_LBUF_PUT_N_BYTES(pBuf, pValue, u2Len) \
{\
    MEMCPY (pBuf, pValue, u2Len);\
    pBuf += u2Len;\
}

#define PTP_LBUF_GET_N_BYTES(pBuf, pu1Dest, u2Len) \
{\
    MEMCPY (pu1Dest, pBuf, u2Len);\
    pBuf += (UINT1)u2Len;\
}

#define PTP_IS_TIMESCALE_PTP(pPtpDomain, bResult)\
{\
    /* Timescale should be PTP and clock class should be either 6 or 7 or 52*/\
    bResult = OSIX_FALSE;\
    if ((pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.u1TimeSource \
        == PTP_TIME_SRC_PTP) && \
        ((pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class == \
          PTP_CLK_CLASS_6) || \
         (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class == \
          PTP_CLK_CLASS_7) || \
         (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class == \
          PTP_CLK_CLASS_52)))\
    {\
        bResult = OSIX_TRUE;\
    }\
}

#define PTP_IS_TIMESCALE_ARB(pPtpDomain, bResult)\
{\
    bResult = OSIX_FALSE;\
        if ((pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.u1TimeSource \
             == PTP_TIME_SRC_ARB) && \
            ((pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class != \
              PTP_CLK_CLASS_6) && \
             (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class != \
              PTP_CLK_CLASS_7) && \
             (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class != \
              PTP_CLK_CLASS_52)))\
        {\
            bResult = OSIX_TRUE;\
        }\
}

#define PTP_SET_ALT_MST_FLAG(u2FlagField)\
    u2FlagField = u2FlagField | PTP_FLAG_ALT_MST_MASK;

#define PTP_SET_TWO_STEP_FLAG(u2FlagField)\
    u2FlagField = u2FlagField | PTP_FLAG_TWO_STEP_MASK;
    
#define PTP_SET_LEAP61_FIELD(u2FlagField)\
    u2FlagField = u2FlagField | PTP_FLAG_LEAP61_MASK;

#define PTP_SET_LEAP59_FIELD(u2FlagField)\
    u2FlagField = u2FlagField | PTP_FLAG_LEAP59_MASK;

#define PTP_SET_UTC_OFFSET_FIELD(u2FlagField)\
    u2FlagField = u2FlagField | PTP_FLAG_UTC_OFFSET_MASK;

#define PTP_SET_TIME_TRC_FIELD(u2FlagField)\
    u2FlagField = u2FlagField | PTP_FLAG_TIME_TRC_MASK;

#define PTP_SET_FREQ_TRC_FIELD(u2FlagField)\
    u2FlagField = u2FlagField | PTP_FLAG_FREQ_TRC_MASK;

#define PTP_SET_PTP_TIMESCALE_FIELD(u2FlagField)\
    u2FlagField = u2FlagField | PTP_FLAG_PTP_TIMESCALE_MASK;

#define PTP_GET_CONTROL_FIELD_FOR_MSG(u1MsgType,u1ControlField)\
{\
    if (u1MsgType == PTP_SYNC_MSG)\
    {\
        u1ControlField = PTP_CONTROL_FIELD_SYNC;\
    }\
    else if (u1MsgType == PTP_DELAY_REQ_MSG)\
    {\
        u1ControlField = PTP_CONTROL_FIELD_DELAY_REQ;\
    }\
    else if (u1MsgType == PTP_FOLLOW_UP_MSG)\
    {\
        u1ControlField = PTP_CONTROL_FIELD_FOLLOW_UP;\
    }\
    else if (u1MsgType == PTP_DELAY_RESP_MSG)\
    {\
        u1ControlField = PTP_CONTROL_FIELD_DELAY_RESP;\
    }\
    else \
    {\
        u1ControlField = PTP_CONTROL_FIELD_OTHERS;\
    }\
}
        
#define PTP_GET_1_BYTE_FROM_DELAY_REQ(u1Val, pu1RxPdu, i4OffSet)\
{\
    u1Val = pu1RxPdu[i4OffSet];\
}

#define PTP_IS_CLK_CLASS_1_127(pPtpDomain, bResult)\
{\
    bResult = OSIX_FALSE;\
    if ((pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class >= \
         PTP_CLK_CLASS_1) && \
        (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class <= \
         PTP_CLK_CLASS_127))\
    {\
        bResult = OSIX_TRUE;\
    }\
}
        
#define PTP_FORM_DEFAULT_DS(pPtpDomain, PtpDefaultDs)\
{\
    MEMCPY ((PtpDefaultDs.ClkId), \
            (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId),\
            PTP_MAX_CLOCK_ID_LEN);\
    MEMCPY ((PtpDefaultDs.AnnounceMsg.GMClkId), \
            (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId),\
            PTP_MAX_CLOCK_ID_LEN);\
    MEMCPY (&(PtpDefaultDs.AnnounceMsg.GMClkQuality), \
            &(pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality),\
            sizeof(tPtpClkQuality));\
    PtpDefaultDs.AnnounceMsg.u1GMPriority1\
        =  pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority1;\
    PtpDefaultDs.AnnounceMsg.u1GMPriority2\
        =  pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority2;\
}

/* Macro to calculate the dynamic memory created by buddy pool
 * Note : This calculation should be modified if the memory calculation is
 *        modified in MemBuddyCreate
 */
#define PTP_CALCULATE_BUDDY_MEMORY(u2HdrSize,u4MinBlk,u4MaxBlk, \
                                   u4NumBlks,sizingId) \
        u2HdrSize = (UINT2) \
            (4 * (((((u4MaxBlk / u4MinBlk) * 2) - 1) / 32) + 1)); \
        gaFsPtpSizingParams[sizingId].u4PreAllocatedUnits = \
                  ((u4MaxBlk  / u4MinBlk) * sizeof (VOID *)) + \
                  ((u2HdrSize + u4MaxBlk ) * u4NumBlks);

#define PTP_GET_FLAG_VALUE(u2Val, i4Mask)\
    (((u2Val & (UINT2) i4Mask) == 0)?OSIX_FALSE:OSIX_TRUE)

#define PTP_GET_MEDIA_HDR_LEN(pPtpPort)\
    (((pPtpPort->PtpDeviceType == PTP_IFACE_IEEE_802_3) ||\
      (pPtpPort->PtpDeviceType == PTP_IFACE_VLAN))?PTP_LLC_HDR_SIZE:0)

#define PTP_IPADDR_TO_OCT(u4Addr,Oct) \
    Oct[0] = (UINT1) ((u4Addr & 0xFF000000) >> 24); \
    Oct[1] = (UINT1) ((u4Addr & 0x00FF0000) >> 16); \
    Oct[2] = (UINT1) ((u4Addr & 0x0000FF00) >> 8); \
    Oct[3] = (UINT1) ((u4Addr & 0x000000FF))

#define PTP_RESET_TWO_STEP_FLAG(u2Val)\
    u2Val = u2Val & PTP_ZERO_TWO_STEP_FLAG;

#define PTP_U8_DIV(pResult, pReminder, pVal1, pVal2) \
             PtpUtlU8Div (pResult, pReminder, pVal1, pVal2)
#endif /*_PTPMACS_H_*/
