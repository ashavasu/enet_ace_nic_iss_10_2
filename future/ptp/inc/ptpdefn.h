/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $ $
 *
 * Description:This file contains all constants related to
 *             PTP protocol.
 *
 *******************************************************************/
#ifndef _PTPDEFN_H
#define _PTPDEFN_H

/****************************************************************************/
/* General                                                                  */
/****************************************************************************/

extern INT4         CliGetCurModePromptStr (INT1 *);
/* PTP Clock Class */


/* PTP Port Dataset Constants */

