/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *$Id: ptpincs.h,v 1.8 2013/06/23 12:26:20 siva Exp $
 * Description: This file contains all the header files to be
 *              included and used by PTP 
 *
 *******************************************************************/
#ifndef _PTPINC_H
#define _PTPINC_H 

#ifndef OS_VXWORKS
#include <stdint.h>
#endif
#include "snmccons.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "ip.h"
#include "l2iwf.h"
#include "fsvlan.h"
#include "vcm.h"
#include "trace.h"
#include "fssocket.h"
#include "utilipvx.h"

#include "lr.h"
#include "fsutlsz.h"
#include "cfa.h"
#include "fsbuddy.h"
#include "utlsll.h"
#include "ptp.h"
#include "fsclk.h"
#include "cli.h"
#include "fm.h"
#include "iss.h"
#include "fssocket.h"
#include "ipv6.h"
#include "ip6util.h"

#include "ptpcli.h"
#include "ptpconst.h"
#include "ptpmacs.h"
#include "ptpsize.h"
#include "ptptdfs.h"
#ifdef L2RED_WANTED
#include "ptpred.h"
#endif
#include "delaytdfs.h"
#ifdef _PTPMAIN_C_
#include "ptpglob.h"
#else
#include "ptpextn.h"
#endif
#include "ptptrc.h"
#include "ptptmr.h"
#include "fsptplw.h"
#include "fsptpwr.h"
#include "fsG826wr.h"
#include "fsG826lw.h"
#include "ptpprot.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "ptpnp.h"
#endif
#include "ptpsz.h"
#endif /* _PTPINC_H */
