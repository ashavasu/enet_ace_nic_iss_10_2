/********************************************************************
 * Copyright (C) Aricent Inc . All Rights Reserved
 *
 * $Id: ptptrap.h
 *
 * Description: This file contains trap data structures defined for
 *              PTP module.
 *********************************************************************/

#ifndef _PTP_TRAP_H
#define _PTP_TRAP_H

#define PTP_SNMPV2_TRAP_OID_LEN 11

#define PTP_OBJECT_NAME_LEN 256

UINT4               gau4PtpSnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

UINT4               gau4PtpTrapOid[] = { 1, 3, 6, 1, 4, 1, 29601, 2, 45, 2, 0 };

/* Proto types of the functions private to this file only */
PRIVATE 
tSNMP_OID_TYPE *PtpTrapMakeObjIdFrmString PROTO ((INT1 *pi1TextStr,
						  UINT1 *pu1TableName));

PRIVATE INT4
PtpTrapParseSubIdNew PROTO ((UINT1 **ppu1TempPtr, UINT4 *pu4Value));
#endif /* _PTP_TRAP_H */
