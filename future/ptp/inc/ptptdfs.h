/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *$Id: ptptdfs.h,v 1.11 2014/01/03 12:40:03 siva Exp $
 * Description: This file contains all the typedefs used by the PTP 
 *              module.
 *******************************************************************/
#ifndef _PTPTDFS_H
#define _PTPTDFS_H 

typedef UINT1 tPtpPortList [PTP_PORT_LIST_SIZE];

typedef char   tClkId[PTP_MAX_CLOCK_ID_LEN];
typedef char   tSecKey[PTP_MAX_SEC_KEY_LENGTH];

/* Fixed Point Arithmetic */
typedef int32_t FP_INT4;
typedef int64_t FP_INT8;
typedef uint32_t FP_UINT4;
typedef uint64_t FP_UINT8;

typedef enum {
    PTP_SA_TRUSTED   = 1,
    PTP_SA_UNTRUSTED = 2
} eSATrustState;

typedef enum {
    PTP_SA_IDLE        = 0,
    PTP_SA_CHALLENGING = 1
} eSAChallengeState;

typedef enum {
    PTP_SA_STATIC     = 1,
    PTP_SA_DYNAMIC    = 2
} eSAType;

typedef enum {
    PTP_E2E_TRANSPARENT_CLOCK = 1,
    PTP_P2P_TRANSPARENT_CLOCK = 2
} eTransClkType;

typedef enum {
    M1 = 0,
    M2,
    M3,
    S1,
    P1,
    P2
}eDecisionCode;

typedef struct _PtpCxt {
    tRBNodeEmbd         RbNode;
                        /* RB Node */
    UINT4               u4ContextId;
                        /* Context Id */
    UINT2               u2Trace;
                        /* Context Specific Trace Option */
    UINT1               u1PrimaryDomainId;
                        /* Primary Domain Id */
    UINT1               u1CxtType;
                        /* L2/L3/Both context */
    UINT1               u1AdminStatus;                         
                        /* Admin State of this Context */
    UINT1               u1RowStatus;
                        /* Row Status */
    UINT1               au1Pad[2];
}tPtpCxt;

typedef struct _PtpClkQuality {
    UINT2                  u2OffsetScaledLogVariance;           
                           /* clock offset scaled log variance */
    UINT1                  u1Class;                             
                           /* clock class */
    UINT1                  u1Accuracy;                          
                           /* clock accuracy */
}tPtpClkQuality;

typedef struct _PtpClkDs {
    tClkId                 ClkId;     
                           /* Clock Identity */
    tPtpClkQuality         ClkQuality;
                           /* Clock quality of this clock */
    UINT4                  u4NumberOfPorts;   
                           /* Max Number of ports in this clock */
    UINT4                  u4CurrentNoOfPorts;   
                           /* Current Number of ports in this clock */
    UINT2                  u2AccMasterMaxSize;
                           /* Max size of the Acceptable Masters of 
                            * this clock */ 
    UINT2                  u2UnicastMastMaxSize;
                           /* Max size of the Unicast Masters */
    UINT1                  u1ClkPriority1; 
                           /* priority1 */
    UINT1                  u1ClkPriority2; 
                           /* priority2 */
    BOOL1                  bTwoStepFlag;   
                           /* type of clock, it indicates it is one step 
                            * clock or two step clock */
    BOOL1                  bSlaveOnly; 
                           /* It indicates whether this clock is slave only */
}tPtpClkDs;

typedef struct _PtpCurrentDs {
    FS_UINT8               u8OffsetFromMaster;
                           /* This indicates the offset from 
                            * master */ 
    FS_UINT8               u8MeanPathDelay;
                           /* This is the one way delay between master 
                            * and slave */ 
    UINT4                  u4StepsRemoved;
                           /* This indicates the number of boundary clock 
                            * between the clock and the grand master */
    UINT4                  u4CurrentMasterToSlaveDelay;
                            /* This represents the current Master to Slave
                             * Delay in Nano seconds*/
    UINT4                  u4CurrentSlaveToMasterDelay;
                            /* This represents the current Slave to Master
                             * Delay in Nano seconds*/
    INT1                   i1OffsetSign;
                            /* This indicates the direction of the 
                             * u8OffsetFromMaster */
    INT1                   i1DelaySign;
                            /* This indicates the direction of the 
                             * u8MeanPathDelay */
    UINT1                  au1Pad[2];
}tPtpCurrentDs;

typedef struct _PtpParentDs {
    tClkId                ClkId;                                
                          /* Parent Clock Identity */
    tClkId                GMClkId; 
                          /* Grand Master Clock Identity */
    tPtpClkQuality        GMClkQuality;
                          /* Grand Master Clock Quality */      
    UINT4                 u4ObservedClkPhaseChangeRate;
                          /* observed parent clock phase change rate */
    UINT4                 u4PortIndex;
                          /* Parent clock port index */
    UINT4                 u4ObservedDrift;
    UINT2                 u2ObservedOffsetScaledLogVariance;
                          /* offset scaled log variance of the parent clock */ 
    UINT1                 u1GMPriority1;
                          /* Priority1 of the Grand master clock */
    UINT1                 u1GMPriority2;
                          /* Priority2 of the Grand master clock */ 
    BOOL1                 bParentStats;
                          /* This is TRUE if the clock has slave port and the 
                           * u2ObservedOffsetScaledLogVariance and 
                           * u4ObservedClkPhaseChangeRate are statiscally 
                           * valid estimates,otherwise this is FALSE */
    UINT1                 au1Pad[3];
                          /* Padding */  
}tPtpParentDs;

typedef struct _PtpTimeDs {
    UINT2                 u2CurrentUtcOffset;
                          /* */
    UINT1                 u1TimeSource;
                          /* This is the primary time source of the Clock*/
    BOOL1                 bCurrentUtcOffsetValid;
                          /* This is TRUE if u2CurrentUtcOffset
                           * is the valid estimate */
    BOOL1                 bLeap59;
                          /* This is TRUE if the system epoch is PTP 
                           * epoch and the last minute of the current 
                           * UTC day contains 59 seconds */
    BOOL1                 bLeap61;
                          /* This is TRUE if the system epoch is PTP 
                           * epoch and the last minute of the current 
                           * UTC day contains 61 seconds */
    BOOL1                 bTimeTraceable;
                          /* This variable is TRUE if the timescale and value of
                           * u2CurrentUtcOffset is traceable to a primary 
                           * reference */
    BOOL1                 bFreqTraceable;
                          /* This variable is TRUE if the frequecy determining 
                           * the timescale is traceable to a primary 
                           * reference */
    BOOL1                 bPtpTimeScale;
                          /* This variable is TRUE if the clock timescale of
                           * the grandmaster clock is PTP and FALSE 
                           * otherwise */
    UINT1                au1Pad [3];
}tPtpTimeDs;

typedef struct _PtpTransClkDs {
    tClkId                 ClkId;     
                             /* Clock Identity */
    UINT4                  u4NumberOfPorts;   
                             /* Max Number of ports in this clock */
    UINT4                  u4CurrentNoOfPorts;   
                           /* Current Number of ports in this clock */
    eTransClkType          TransClkType;
                             /* Can be either End to End or Peer to Peer
                              * It denotes the operational method for this 
                              * transparent clock */
    UINT1                  u1PrimaryDomainId;
                             /* Primary Domain Id */
    BOOL1                  bTwoStepFlag;
    UINT1                  au1Pad[2]; 
                             /* Padding */
}tPtpTransClkDs;

typedef struct _PtpOcBcClkDs {
    tPtpCurrentDs         CurrentDs;
                          /* Current Data set of this domain */
    tPtpParentDs          ParentDs;
                          /* Parent Data set of this domain */
    tPtpTimeDs            TimeDs;
                          /* Time Data set of this domain */
    tPtpClkDs             ClkDs;
                          /* Default Data Set of this domain */
}tPtpOcBcClkDs;

typedef union _PtpClockDs {
    tPtpOcBcClkDs         PtpOcBcClkDs;
                          /* Data Set applicable for ordinary and
                           * boundary clocks. */
    tPtpTransClkDs        TransClkDs;
                          /* Transparent clock data set of this domain */
}unPtpClockDs;

typedef struct _PtpDomain {
    tRBNodeEmbd           RbNode;
                          /* RB Node */
    tPtpCxt               *pPtpCxt;                              
                          /* Back pointer to Ptp Context */
    unPtpClockDs          ClockDs;
                          /* Ordinary/boundary clock or Transparent Clock
                           * Data Set for this domain */
    eClkMode              ClkMode;                             
                          /* Clock mode of the domain */
    tPtpPortList          DomainPortList;
                          /* Contains the list of ports associated in this 
                           * domain
                           */
    UINT1                 *pu1PathTraceTLV;
                           /* This is Path Trace TLV, contains type, length
                            * and list of clock identities */
    UINT1                 *pu1AltTimeSclTLV;
                           /* This is Alternate Timescale TLV, 
                            * contains type, length  and list of alternate 
                            * timescale.
                            */
    UINT4                  u4NumberOfSA;
                           /* Number of SAs in this clock */
    UINT4                  u4ContextId; 
                           /* Context Identifier */
    UINT2                  u2AltTimeTotLen; 
                           /* Total size of the alternate 
                             timescale tlvs */
    UINT2                  u2GrandMasterMaxSize;
                           /* Max size of the grand master cluster table
                            *  of this domain */ 
    UINT2                  u2GrandMasterActualSize;
                           /* Actual size of the grand master cluster table
                            *  of this domain */ 
    UINT1                 u1DomainId;           
                          /* Domain Id */
    UINT1                 u1RowStatus;
                          /* Row status variable */
    BOOL1                 bPathTraceOption; 
                          /* This indicates path trace option is
                           * enabled or disabled for this clock */
    BOOL1                 bSecurityEnabled;
                          /* This indicates security protocol 
                           * is enabled or disabled for this clock */
    BOOL1                 bGrandMasterEnabled;
                          /* This indicates grand master cluster table   
                           * is enabled or disabled for this clock */
    UINT1                 u1GrandMasterlogQueryInterval;
                          /* This indicates the Grandmaster Cluster 
                           * Log Query interval for this clock. */
    UINT1                 u1InitInProgress;
                          /* This indicates that one of the ports in
                           * this clock is boundary clock and is in
                           * Initializing state */    
    UINT1                 au1Pad[3];
                          /* Padding */
}tPtpDomain;

typedef struct _PtpPortDs {
    tClkId                ClkId;  
                          /* Clock Identity */
    FS_UINT8              u8PeerMeanPathDelay;
                          /* If the u1DelayMechanism is P2P then this contains 
                           * the one way delay of the port */
    FS_UINT8              u8SyncLimit;
                          /* Sync message limit */
    FS_UINT8              u8SyntRatio;
                          /* Syntonization ratio */
    FS_UINT8              u8RatioPerSec;
                          /* Ratio per second.*/
    eDecisionCode         DecisionCode;
                          /* Selected Decision code associated with this
                           * interface */
    UINT4                 u4PtpPortNumber;
                          /* Denotes PTP allocated Port Number */
    UINT4                 u4VersionNumber;
                          /* Version number */
    UINT4                 u4RateAdj;
                          /* Rate Adjustment Factor */
    UINT1                 u1PortState;
                          /* State of the Port */
    UINT1                 u1RecommendedState;
                          /* Recommended State of the port */
    UINT1                 u1MinDelayReqInterval;
                          /* Delay Request Interval */
    UINT1                 u1AnnounceInterval;
                          /* Announce Message interval */
    UINT1                 u1AnnounceReceiptTimeOut;
                          /* Announce Receipt Time out */
    UINT1                 u1DelayMechanism;
                          /* It indicates the mean propagation delay mechanism
                           * it is either E2E or P2P */
    UINT1                 u1MinPdelayReqInterval;
                          /* Peer Delay Request Interval */
    UINT1                 u1PtpEnabledStatus;
                          /* If the value is OSIX_TRUE, it indicates that
                           * PTP is enabled over this interface and if the
                           * value is OSIX_FALSE, if PTP is disabled over
                           * this interface. */
    INT1                  i1SyncInterval;
                          /* Sync message interval */
    INT1                  i1PeerDelaySign;
                            /* This indicates the direction of the 
                             * u8PeerMeanPathDelay */
    INT1                  i1SyntRatioSign;
                            /* This indicates the direction of the 
                             * u8SyntRatio */
    UINT1                 au1Pad[1];
}tPtpPortDs;

typedef struct _PtpPortStats {
    UINT4               u4RcvdAnnounceMsgCnt; 
                          /* Received Announce Msg Count */
    UINT4               u4RcvdSyncMsgCnt; 
                          /* Received Sync Msg Count */
    UINT4               u4RcvdDelayReqMsgCnt; 
                          /* Received Delay_Req Msg Count */
    UINT4               u4RcvdDelayRespMsgCnt; 
                          /* Received Delay_Resp Msg count */
    UINT4               u4TransDelayReqMsgCnt; 
                          /* Transmitted Delay_Req Msg count */
    UINT4               u4DroppedPtpMsgCnt;
                          /* Corresponds to the number of messages that 
                           * have been dropped over the port*/
    UINT4               u4TransAnnounceMsgCnt; 
                          /* Transmitted Announce Msg Count */
    UINT4               u4TransSyncMsgCnt;     
                          /* Transmitted Sync Msg Count */
    UINT4               u4RcvdPeerDelayReqMsgCnt; 
                          /* Received Peer Delay Request Msg Count */
    UINT4               u4TransPeerDelayReqMsgCnt; 
                          /* Transmitted Peer delay Request Msg Count */
    UINT4               u4RcvdPeerDelayRespMsgCnt; 
                          /* Received Peer Delay Response Msg Count */
    UINT4               u4TransPeerDelayRespMsgCnt; 
                          /* Transmitted Peer Delay Response Msg Count */
    UINT4               u4TransDelayRespMsgCnt; 
                          /* Transmitted Delay_Resp Msg Count */
    UINT4               u4TransPeerDelayRespFollowUpMsgCnt; 
                          /* Transmitted Peer Delay Response 
                           * Follow Up Msg Count*/
    UINT4               u4TransFollowUpMsgCnt; 
                          /* Transmitted Sync Follow Up Msg Count */
    UINT4               u4RcvdPeerDelayRespFollowUpMsgCnt; 
                          /* Received Peer Delay Response Follow Up Msg 
                           * count */
    UINT4               u4RcvdFollowUpMsgCnt; 
                          /* Received Sync Follow Up Msg Count */
}tPtpPortStats;

/* Refer Section 7.3.7 of IEEE Std 1588 2008 */
typedef struct _PtpPortSeqIdPool {
    UINT2              u2AnncSeqId; /* Announce Message Seq Id */
    UINT2              u2SyncSeqId; /* Sync Message Seq Id */
    UINT2              u2DelayReqSeqId; /* Delay Req Seq Id */
    UINT2              u2PDelayReqSeqId; /* Peer Delay Req Rx Seq Id */
    UINT2              u2RxPDelayReqSeqId; /* Rx Peer Delay Req sg Rx Seq Id */
    UINT2              au1Pad[2];
}tPtpPortSeqIdPool;

typedef struct _tPtpPortAddress {

    UINT2 u2NetworkProtocol;   /* Network transport protocol */
    UINT2 u2AddrLength;        /* Length of the transport protocol Address */  
    INT1  ai1Addr[PTP_MASTER_MAX_ADDR_LEN]; /* transport protocol Address */

} tPtpPortAddress;

/* This structure used to get/set the Alternate master related Information */
typedef struct _tPtpAltMstInfo {

    UINT1 u1NumberOfAltMaster; /* Alternate master count for the prot */ 
    UINT1 u1AltMCSyncInterval; /* Alrernate master Multi Sync Intreval */
    UINT1 u1Flag;              /* This field used to indicates valid 
                                  members of this PtpAltMstInfo        */ 
    BOOL1 bAltMCSync;          /*  Alrernate master Multi Sync Flag status*/

} tPtpAltMstInfo;

typedef struct _PtpAnnounceMsg {
    tClkId                GMClkId; 
                          /* Grand Master Clock Identity */
    tPtpClkQuality        GMClkQuality;
                          /* Grand Master Clock Quality */      
    UINT4                 u4StepsRemoved;
                           /* This indicates the number of boundary clock 
                            * between the clock and the grand master */
    UINT2                 u2CurrentUtcOffset;
                          /* It is the Offset between TAI and UTC */
    UINT1                 u1GMPriority1;
                          /* Priority1 of the Grand master clock */
    UINT1                 u1GMPriority2;
                          /* Priority2 of the Grand master clock */ 
    UINT1                 u1TimeSource;
                          /* This is the primary time source of the Clock*/
    BOOL1                 bTimeTraceable;
                          /* Time traceable of the clock */
    BOOL1                 bFreqTraceable;
                          /* Frequency traceable of the clock */
    BOOL1                 bPtpTimeScale;
                          /* Time Scale of the clock */
    BOOL1                 bLeap59;
                          /* This is TRUE if the system epoch is PTP 
                           * epoch and the last minute of the current 
                           * UTC day contains 59 seconds */
    BOOL1                 bLeap61;
                          /* This is TRUE if the system epoch is PTP 
                           * epoch and the last minute of the current 
                           * UTC day contains 61 seconds */
    BOOL1                 bCurrentUtcOffsetValid;
                          /* This is TRUE if u2CurrentUtcOffset
                           * is the valid estimate */
    UINT1                 au1Pad[1];
                          /* Padding */

}tPtpAnnounceMsg;

typedef struct _PtpForeignMasterDs {
    tRBNodeEmbd           RbNode;
                          /* RB Node */
    tClkId                ClkId;     
                          /* Foreign Master Clock Identity */
    tPtpAnnounceMsg       AnnounceMsg;
                          /* Announce Msg Received from this foreign Master */  
    struct _PtpPort       *pPtpPort;
                          /* Back Pointer to Received Port structure */  
    UINT4                 u4PortIndex;
                          /* Port index over which this message was received */
    UINT4                 u4ContextId;
                          /* Context Identifier*/
    UINT4                 u4AnnounceMsgsCnt;
                          /* Announce Messages received from foreign master */
    UINT4                 u4SrcPortIndex;
                          /* Port Index of the foreign master */
    UINT1                 u1DomainId;
                          /* Domain Identifier */
    UINT1                 u1AltPri1;     /* Configured Alternate Priority1 
                                           * for alternate master feature. */
    BOOL1                 bIsAltMstAnnc;  /* Is this message added because of 
                                           * Alternate master enabled 
                                           */
    UINT1                 u1BmcCnt; /* Indicates the number of times this record
                                       has been used by BMC
                                       */
    UINT2                 u2RxAnncSeqId;
    UINT1                 u1FornMastRxCnt; /* Indicates the number of times this
                                              Foreign Master Ds has been rcvd
                                              within FOREIGN_MASTER_THERSHOLD
                                              limit 
                                              */
    UINT1                 au1Pad[1];
}tPtpForeignMasterDs;

typedef struct _PtpPort {
    tRBNodeEmbd           RbNode;
                          /* RB Node */
    tPtpPortDs            PortDs;
    tTmrBlk               AnnounceTimer; 
                          /* Announce Message Timer */
    tTmrBlk               SyncTimer; 
                          /* Sync Message Timer */
    tTmrBlk               DelayReqTimer; 
                          /* Delay Request Message Timer */
    tTmrBlk               PdelayReqTimer; 
                          /* Peer Delay Request Message Timer */
    tTmrBlk               AnnounceReciptTimeOutTimer; 
                          /* Announce Receipt Time out Timer */
    tTmrBlk               QualificationExpiryTimer; 
                          /* Qualification expiry Timer */
    tTmrBlk               MulSyncTimer; 
                          /* Multicast Sync Message Timer for Alternate 
                           * Master feature
                           */
    tPtpPortStats         PtpPortStats; 
                            /* Contains Statistics Specific to this port */
    tFsClkTimeVal         NthIngressSyncTimeStamp;
                            /* Contains the value of OriginTimeStamp for the
                             * sync message received at Nth Previous time. */
    tFsClkTimeVal         IngressSyncTimeStamp;
                            /* Contains the value of OriginTimeStamp for the
                             * sync message */
    tFsClkTimeVal         SyncReceptTime;
                            /* Contains time generated by IngressSyncMsgEvent
                             * */
    tFsClkTimeVal         DelayReqOriginTime;
                            /* Contains origin time for recently transmitted
                             * delay request message */
    tFsClkTimeVal         DelayReqIngressTime;
                            /* Contains ingress time for recently received
                             * delay request message */
    tFsClkTimeVal         PeerDelayOriginTime;
                            /* Contains origin time for recently transmitted
                             * peer delay request message */
    tFsClkTimeVal         PeerDelayRespIngressTime;
                            /* Contains ingress time for recently received
                             * peer delay response message */
    FS_UINT8              u8CorrectionField;
                            /* contains the correction field received in sync
                             * messages  */
    FS_UINT8              u8PDelCorrectionField;
                            /* contains the value of received Peer Delay
                             * Resquest Correction field. */
    FS_UINT8              u8NthCorrectedMasterTs;
                            /* Contains the value of correctedMasterTimeStamp
                             * at Nth previous time */
    tPtpDomain            *pPtpDomain;
                          /* Back Pointer to domain */
    tPtpForeignMasterDs   *pPtpErBestMsg;
                          /* Pointer to the best message received on
                           * this port. */
    tTMO_HASH_NODE         nextHashNode;

    tPtpHwSrcIdHash        PtpHwSrcIdHash;
                             /* Source Port Identity hash value */
    UINT1                 *pu1PeerDelayResp;
                            /* Pointer to the transmitted Peer delay resp
                             * message for two step clocks */
    UINT4                 u4PeerDelayRespLen;
                           /* Length of the stored Peer Delay response
                            * message */
    UINT4                 u4ContextId; 
                          /* Context Identifier */
    UINT4                 u4IfIndex;
                          /* Interface index allocated by CFA, if the interface
                           * type is either 802.3 or IVR. If the interface type
                           * is VLAN, then this denotes VLAN identifier
                           * */
    ePtpDeviceType        PtpDeviceType;
                          /* This indicates the type of the underlying iface.
                           * This can be either a Layer 2 VLAN or a physical
                           * port or an IVR interface or a router port.
                           */
    tPtpPortSeqIdPool     PtpPortSeqIdPool;
                            /* Contains Sequence Id Pool */
    UINT2                 u2UnicastMastMaxSize;
                          /* Max size of the Unicast Masters */
    UINT2                 u2UnicastMastActualSize;
                          /* Actual size of the Unicast Masters */
    UINT1                 u1NumberOfAltMaster;
                          /* Number of Alternate masters in this port */
    UINT1                 u1AltMulcastSynInterval;
                          /* Alternate Multicast Sync Interval */
    UINT1                 u1BestAltMstCount; /* No of Best alternate master.*/
    UINT1                 u1RowStatus; /* Row status */
    UINT1                 u1DomainId;
                          /* Domain Identifier */
    UINT1                 u1UniNegAMInterMsgPeriod;
                          /* This indicates the inter message period of the 
                           * announce message can be requested with the unicast 
                           * masters on this port*/
    BOOL1                 unicastNegOption;
                          /* This indicates unicast option is enabled on 
                           * on this port or not */
    BOOL1                 bAltMulcastSync;
                          /* Alternate Multicast Sync is enabled on this 
                           * port or not */
    BOOL1                 bAccMastEnabled;
                          /* Acceptable Master is enabled for this port */
    BOOL1                 bIsEligibleAltMst; /* Is eligible alternate master.*/
    BOOL1                 bPortFaultyFlag;
                          /* This is TRUE if the port is faulty */
    BOOL1                 bIsTranPort;
    BOOL1                 bDelayReqInProgress; /* indicates whether Delay mech
                                                  is in progress in this port
                                                  or not.Only applicable for
                                                  Delay Request & response */
    UINT1                 au1Pad[1];
}tPtpPort;

typedef struct _PtpPktParams {
    UINT1               *pu1RcvdPdu; /* Pointer to the received PDU.
                                        Linear Buffer */
    tPtpPort            *pPtpPort;   /* Pointer to the port structure */
    tClkSysTimeInfo     *pPtpIngressTimeStamp; /*Time stamp for the pkt */
    tPtpPortAddress     *pPtpPortAddress; /* Address received */
    UINT4                u4PktLen; /* Length of the received PDU */
}tPtpPktParams;

typedef INT4 (*tPtpMsgHandleFn) (tPtpPktParams * pPtpPktParams);

/* Application specific Functions */
typedef struct PtpMsgFn {
    tPtpMsgHandleFn   pMsgRxHandler;
} tPtpMsgFn;

typedef struct _PtpAccMasterTable {
    tRBNodeEmbd         RbNode;
                        /* RB Node */
    tPtpPortAddress     AccMstAddr;
                        /* Address of the Grand Master */ 
    tPtpDomain         *pPtpDomain;
                        /* Back Pointer to domain */
    UINT4               u4ContextId; /* Context Identifier.*/
    UINT4               u4PtpPortNumber;
    UINT1               u1DomainId; /* Domain Identifier.*/
    UINT1               u1AltPri1;     /* Configured Alternate Priority1
                                        * for this alternate master.
                                        */
    UINT1               u1RowStatus;
    UINT1               au1Pad[1];

}tPtpAccMasterTable;

typedef struct _PtpGrandMasterTable {
    tRBNodeEmbd         RbNode;
                        /* RB Node */

    tPtpPortAddress     GMAddr;
                        /* Address of the Grand Master */ 

    tPtpAnnounceMsg     AnnounceMsg;     
                        /* Announce Msg Received from 
                         * Grand Master */
    tTmrBlk             GMReqTimer; 
                        /* This timer used to re requst the unicast 
                         * transmission if the response is not updated 
                         * within the GMlogQueryInterval*/
    tPtpDomain         *pPtpDomain;
                        /* Back Pointer to domain */
    UINT4               u4ContextId; /* Context Identifier.*/
    UINT1               u1Status[PTP_UN_MAX_NGE_MSG_TYPE]; 
                        /* Status of the requested Negotiation status */
    UINT1               u1DomainId; /* Domain Identifier.*/
    UINT1               u1RowStatus; 
                        /* Status of this entry, is it valid or not */
    UINT1               au1Pad[2]; /* Padding */
}tPtpGrandMasterTable;


typedef struct _PtpUnicastMasterTable {
    tRBNodeEmbd         RbNode;
    tPtpPortAddress     UMAddr;
    tTmrBlk             UMReqTimer; 
                        /* This timer used to re requst the unicast 
                         * transmission if the response is not updated 
                         * within the UMlogQueryInterval*/
    tPtpPort           *pPtpPort;
    UINT4               u4ContextId; /* Context Identifier.*/
    UINT4               u4PtpPortNumber;
    UINT1               u1Status[PTP_UN_MAX_NGE_MSG_TYPE];
                        /* Status of the requested Negotiation status */
    UINT1               u1DomainId; /* Domain Identifier.*/
    UINT1               u1RowStatus; 
                        /* Status of this entry, is it valid or not */
    UINT1               au1Pad[2]; /* Padding */
}tPtpAccUnicastMasterTable;


typedef struct _PtpSecKeyDs {
    tRBNodeEmbd         RbNode;
    tSecKey             SecKey;     
                        /* Security Key */
    tPtpDomain         *pPtpDomain;
                        /* Back Pointer to domain */
    UINT4               u4ContextId; /* Context Identifier.*/
    UINT4               u4SecKeyId;
                        /* Security Key Id */
    UINT4               u4SecKeyAlgId;
                        /* Security Algorithm Id */
    UINT4               u4SecKeyLength;
                        /* Security Key Length */
    UINT4               u4SecKeyStartTime;
                        /* Security Key Start Time */
    UINT4               u4SecKeyExpiryTime;
                        /* Security Key Expiry Time */
    BOOL1               bSecKeyValid;
                        /* This indicates the security key is 
                         * valid or not */
    UINT1               u1DomainId; /* Domain Identifier.*/
    UINT1               u1RowStatus;
                        /* Row status */
    UINT1               au1Pad[1];
                        /* Padding */  
}tPtpSecKeyDs;

typedef struct _PtpSADs {
    tRBNodeEmbd         RbNode;
                        /* RB Node */
    tPtpPortAddress     SASrcAddr;
                        /* SA Source Addr */
    tPtpPortAddress     SADstAddr;
                        /* SA Destination Addr */
    tTmrBlk             SATrustTimer;
                        /* SA Trust Timer */
    tTmrBlk             SAChallengeTimer;
                        /* SA Challenge Timer */
    tClkId              SASrcClkId;         
                        /* SA Source Clock Id */ 
    tClkId              SADstClkId;         
                        /* SA Destination Clock Id */
    tTMO_SLL_NODE       nextSANode;
    eSATrustState       SATrustState;
                        /* SA Trust State */  
    eSAChallengeState   SAChallengeState;                   
                        /* SA Challenge State */
    eSAType             SAType;
                        /* This indicates the type of SA */
    tPtpDomain           *pPtpDomain;
                         /* Back Pointer to domain */
    UINT4               u4ContextId; /* Context Identifier.*/
    UINT4               u4SAId;
                        /* SA Id */ 
    UINT4               u4SAReplayCntr;
                        /* SA Replay Counter */
    UINT4               u4SALifeTimeId;
                        /* SA Life Time Id */
    UINT4               u4SAKeyId;
                        /* SA Key Id */
    UINT4               u4SANextLifeTimeId;
                        /* SA Next Life Time Id */
    UINT4               u4SANextKeyId;
                        /* SA Next Key Id */
    UINT4               u4SARequestNonce;
                        /* SA Request Nonce */
    UINT4               u4SAResponseNonce;
                        /* SA Response Nonce */ 
    UINT2               u2SATrustTimeOut;
                        /* SA Trust TimeOut */
    UINT2               u2SAChallengeTimeOut;
                        /* SA Challenge Time Out */
    UINT2               u2SASrcPortIndex;
                        /* SA Source Port Index */
    UINT2               u2SADstPortIndex;
                        /* SA Dst Port Index */
    UINT1               u1DomainId; /* Domain Identifier.*/
    UINT1               u1SADirection; 
                        /* This indicates the direction of SA */  
    UINT1               u1RowStatus; 
                        /* Status of this entry, is it valid or not */
    BOOL1               bSAChallengeRequired;
                        /* This indicates SA Challenge Required 
                         * or not */
    BOOL1               bSAResponseRequired; 
                        /* This indicates challenge response required
                         * or not */ 
    UINT1               au1Pad[3]; /* Padding */
}tPtpSADs;

/* Compatability Database Entry */
typedef struct _PtpCompDBEntry {
    tRBNodeEmbd           RbNode; /* RBTree node */
    tTmrBlk               CompDBTmrNode; /* To remove the stale entry */
    UINT4                 u4ContextId; /* This is the context Identifier.*/
    UINT2                 u2PortId; /* This is the PTP Port Identifier.*/
    UINT2                 u2SrcPortId; 
                          /* This is the v1 node PTP Port Identifier.*/
    UINT2                 u2GMSeqId;
                          /* This is the v1 node Grandmaster 
                           * sequence Identifier.*/
    UINT2                 u2EpochNumber;   /* v1 Node  Epoch  Number */
    UINT1                 u1DomainId; /* This is the Domain Identifier.*/
    UINT1                 u1SyncInterval; 
                          /* This is the v1 node Sync interval.*/
    UINT1                u1v1Stratum;     /* v1 Node Stratum value  */
    UINT1                au1Pad[1];
    INT1                 ai1v1SubDomain[PTP_SUBDOMAIN_NAME_LENGTH];
                          /* v1 Node sub domain Identifier */
    INT1                 ai1ClkId[PTP_CODE_STRING_LENGTH]; 
                           /* v1 Node Clock Identifier */
    INT1                 ai1v1SrcUuid[PTP_UUID_LENGTH+2];
                         /* v1 Node UUID Identifier */
} tPtpCompDBEntry;

/* Altrnate Timescale Table Entry */
typedef struct _PtpAltTimescaleEntry {
    tRBNodeEmbd           RbNode; /* RBTree node */
    FS_UINT8              u8TimeOfNextJump;
                          /* Transmitting node's time at the 
                           * time that the next discontinuity.*/  
    UINT1                 au1DisplayName[PTP_ALT_TIME_NAME_MAX_LEN + 2];
                          /* Textual representation of the timescale Name.*/
    UINT4                 u4ContextId; /* This is the context Identifier.*/
    UINT4                 u4CurrentOffset; 
                          /* Current offset from our reference*/
    UINT4                 u4JumpSeconds; 
                          /* Next Discontinuity time of this timescale  */
    UINT1                 u1DomainId; /* This is the Domain Identifier.*/
    UINT1                 u1KeyId;    
                          /* Alternate timescale unique identifier */
    UINT1                 u1Status;  /* Status of this entry */
    UINT1                 au1Pad[1];  /* Padding */
} tPtpAltTimescaleEntry;


/* Unicast negotiation Structures */
typedef struct _PtpUniNegoEntry {
    tPtpPortAddress       UniMasterAddr;
                          /* Address of the Unicast peer node. */
    UINT4                 u4NetworkProtocol;
                          /* Network protocol of the unicast peer port */
    UINT4                 u4ContextId; /* This is the Context Identifier.*/
    UINT2                 u2PortNum;
                          /* This indicates the port number on which the
                           * unicast negotiation should initiated 
                           * or canceled */
    UINT1                 u1DomainId; /* This is the Domain Identifier.*/
    UINT1                 u1AppId;
                          /* Application Id which is requesting the
                           * negotiation */
    UINT1                 u1ReqType;
                          /* This indicates the request type whether it 
                           * is request to initiate the unicast 
                           * negotiation or request to cancel the unicast 
                           * negotiation */ 
    UINT1                 Pad[3];

}tPtpUniNegoEntry; 

typedef struct _PtpUNegGrantorTbl {
    tRBNodeEmbd           RbNode;  
                          /* RB Tree node.*/
    tPtpPortAddress        *pPtpPortAddress; 
                          /* This indicates the port address of the grantor */
    tClkId                GrantorClkId; 
                          /* Peer node clock identifier.*/
    tTmrBlk               AMDurationTmr; 
                          /* This indicates the duration timer of the 
                           * announce message. This can be used as log query 
                           * interval timer if the request is not granted */
    tTmrBlk               SMDurationTmr; 
                          /* This indicates the duration timer of the 
                           * sync message. This can be used as log query 
                           * interval timer if the request is not granted */
    tTmrBlk               DRespDurationTmr; 
                          /* This indicates the duration timer of the 
                           * delay response message. This can be used
                           * as log query 
                           * interval timer if the request is not granted */
    tTmrBlk               PDRespDurationTmr; 
                          /* This indicates the duration timer of the 
                           * peer delay response message. This can be used 
                           * as log query interval timer if the request is 
                           * not granted */
    tTmrBlk               AMCancelTmr; 
                          /* This indicates the cancel timer of the 
                           * announce message.  */
    tTmrBlk               SMCancelTmr; 
                          /* This indicates the cancel timer of the 
                           * sync message.  */
    tTmrBlk               DRCancelTmr; 
                          /* This indicates the cancel timer of the 
                           * delay response message.  */
    tTmrBlk               PDRCancelTmr; 
                          /* This indicates the cancel timer of the 
                           * peer delay response message.  */
    UINT4                 u4ContextId; /* This is the context Identifier.*/
    UINT4                 u4AMGrantedDuration;
                          /* This indicates the duration time granted for 
                           * the announce message by this master */
    UINT4                 u4SMGrantedDuration;
                          /* This indicates the duration time granted for 
                           * the sync message by this master */
    UINT4                 u4DRGrantedDuration;
                          /* This indicates the duration time granted for 
                           * the delay response message by this master */
    UINT4                 u4PDRGrantedDuration;
                          /* This indicates the duration time granted for 
                           * the peer delay response message by this master */
    UINT2                 u2GrantorPortNum; /* PTP port identifier on 
                                     * which the negotiation happened.*/
    UINT2                 u2PortNum; 
                          /* port on which the grantor is negotiated.*/
    UINT1                 u1DomainId; /* This is the Domain Identifier.*/
    UINT1                 u1MsgStatus;
                          /* This indicates the grant status of messages.
                           * 2 bits - Status of Announce message
                           * 2 bits - Status of Sync message  
                           * 2 bits - Status of DR message
                           * 2 bits - Status of PDR message */  
    UINT1                 u1ReqAppl;
                          /* This indicates the applications requested
                           *  unicast negotiation */
    BOOL1                 bToBeDeleted;
                          /* This indicates the entry needs to be deleted 
                           * once you get the acknowledgement for the 
                           * cancel unicast transmission */
} tPtpUNegGrantorTbl;

typedef struct _PtpUNegGranteeTbl {
    tRBNodeEmbd           RbNode;  
                          /* RB Tree node.*/
    tPtpPortAddress        *pPtpPortAddress; 
                          /* This indicates the port address of the grantee */
    tClkId                GranteeClkId; 
                          /* clock identifier of the grantee .*/
    tTmrBlk               AMInterMsgTmr;
                          /* This is timer of the inter message period 
                           * for the announce message */ 
    tTmrBlk               SMInterMsgTmr;
                          /* This is timer of the inter message period 
                           * for the sync message */ 
    tTmrBlk               DRInterMsgTmr;
                          /* This is timer of the inter message period 
                           * for the delay response message */ 
    tTmrBlk               PDRInterMsgTmr;
                          /* This is timer of the inter message period 
                           * for the peer delay response message */ 
    tTmrBlk               AMCancelTmr;
                          /* This indicates the timer for the cancel 
                           * time of the announce message */
    tTmrBlk               SMCancelTmr;
                          /* This indicates the timer for the cancel 
                           * time of the sync message */
    tTmrBlk               DRCancelTmr;
                          /* This indicates the timer for the cancel 
                           * time of the delay response message */
    tTmrBlk               PDRCancelTmr;
                          /* This indicates the timer for the cancel 
                           * time of the peer delay response message */
    UINT4                 u4AMGrantedDuration;
                          /* This indicates the duration time granted to this
                           * grantee for announce message */
    UINT4                 u4SMGrantedDuration;
                          /* This indicates the duration time granted to this
                           * grantee for sync message */
    UINT4                 u4DRGrantedDuration;
                          /* This indicates the duration time granted to this
                           * grantee for delay response message */
    UINT4                 u4PDRGrantedDuration;
                          /* This indicates the duration time granted to this
                           * grantee for peer delay response message */
    UINT2                 u2GranteePortNum;
                          /* This indicates the port id of the grantee */
    UINT1                 u1AMInterMsgPeriod;
                          /* This indicates the inter messages period granted 
                           * to this grantee for announce message */
    UINT1                 u1SMInterMsgPeriod;
                          /* This indicates the inter messages period granted 
                           * to this grantee for sync message */
    UINT1                 u1DRInterMsgPeriod;
                          /* This indicates the inter messages period granted 
                           * to this grantee for delay response message */
    UINT1                 u1PDRInterMsgPeriod;
                          /* This indicates the inter messages period granted 
                           * to this grantee for peer delay response message */
    UINT1                 u1MsgStatus;
                          /* This indicates the grant status of messages.
                           * 2 bits - Status of Announce message
                           * 2 bits - Status of Sync message  
                           * 2 bits - Status of DR message
                           * 2 bits - Status of PDR message */  
    UINT1                Pad[1];
                         /* Padding */
}tPtpUNegGranteeTbl;


/* This structure is used locally by acceptable/unicast/grand master
 * data set since they have the same indices
 */
typedef struct _PtpMasterIndex
{
    UINT1               au1Addr [PTP_MAX_ADDR_LENGTH];
    UINT4               u4ContextId;
    UINT2               u2NetworkProtocol;
    UINT2               u2AddrLength;
    UINT1               u1DomainId;
    UINT1               au1Rsvd [3];
}tPtpMasterIndex;

/* PTP version 1 Time Representation */
typedef struct _tPtpV1TimeRep {
    UINT4 u4Sec;         /* Second field      */
    INT4  i4NanoSec;     /* nano second field */
} tPtpV1TimeRep;

/* PTP version 1 Common Message header */
typedef struct _tPtpv1ComHdr {
    UINT2  u2ver;                                   /* v1 version fields     */
    UINT2  u2verNetwork;                            /* v1 version Network    */
    UINT2  u2SrcPortId;                             /* v1 Source port id     */
    UINT2  u2SeqId;                                 /* v1 message sequence id*/
    UINT1  u1MsgType;                               /* v1 message Type value */
    UINT1  u1SrcComTech;                            /* v1 source communication
                                                     * technology            */
    UINT1  u1Control;                               /* v1 Control Message type*/
    UINT1  au1Pad[1];                               /* Padding               */
    INT1   ai1SubDomain[PTP_SUBDOMAIN_NAME_LENGTH]; /* v1 Ptp Sub domain name*/
    INT1   ai1SourceUuid[PTP_UUID_LENGTH+2];        /* v1 Ptp UUID name      */
    INT1   ai1Flags[PTP_V1_FLAG_FIELD_LEN];         /* v1 Flag fields        */
    UINT1  au1Pad2[2];                              /* Padding               */

} tPtpv1ComHdr;

/*  PTP version 1 Sync or Delay_Req message */
typedef struct _tPtpv1SyncDReqHdr {
    tPtpV1TimeRep  OrigTimestamp;    /* Origin Timestamp field          */
    INT4           i4EstMstDrift;    /* Estimated Master Drift field    */
    UINT2          u2EpochNum;           /* Epoch Number field              */
    UINT2          u2GMPortId;           /* Grandmaster Port Id field       */
    UINT2          u2GMSequenceId;       /* Grandmaster Sequence Id field   */ 
    UINT2          u2LocalStepsRemoved;  /* Local Steps Removed field       */
    UINT2          u2ParentPortField;    /* Parent Port Id field            */
    INT2           i2CurrUTCOffset;      /* Current UTC Offset field        */
    INT2           i2GMClkVar;           /* Grandmaster Clock Variance field*/
    INT2           i2LocalClkVar;        /* Local Clock Variance field      */ 
    INT2           i2EstMstVar;          /* Estimated Master Variance field */
    UINT1          u1GMCommTech;         /* Grandmaster Communication     
                                            Technology field                */
    UINT1          u1GMClkStratum;       /* Grandmaster Clock Stratum field */
    UINT1          u1LocalClkStratum;    /* Local Clock Stratum field       */
    UINT1          u1ParentCommTech;     /* Parent Communication Technology
                                            field                           */
    INT1           i1SyncInterval;       /* Sync Interval field             */
    UINT1          au1Pad1[1];           /* Padding               */    
    INT1           ai1GMClkUuid[PTP_UUID_LENGTH+2]; 
                                     /* Grandmaster Clock Uuid field   */
    INT1           ai1ParentUuid[PTP_UUID_LENGTH+2]; 
                                     /* Parent Clock Uuid field       */
    INT1           ai1GMClkId[PTP_CODE_STRING_LENGTH]; 
                                     /* Grandmaster Clock id field */
    INT1           ai1LocalClkId[PTP_CODE_STRING_LENGTH];
                                     /* Local Clock id field    */

    BOOL1          bGMPreferred;         /* Grandmaster Preferred field     */
    BOOL1          bGMIsBoundaryClk;     /* Grandmaster Is Boundary Clock 
                                            field                           */
    BOOL1          bUtcReasonable;       /* UtcReasonable field             */
    UINT1         au1Pad2[1];             /* Padding                         */
} tPtpv1SyncDReqHdr;

/* PTP version 1 Follow_Up message */
typedef struct _tPtpv1FollowUpHdr {
    tPtpV1TimeRep PreOrigTimestamp; /* Precise Origin Timestamp field */
    UINT2         u2AssoSeqId;      /* Associated Sequence Id field   */
    UINT1         au1Pad[2];        /* Padding                        */
} tPtpv1FollowUpHdr;

/* PTP version 1 Delay_Resp message */
typedef struct _tPtpv1DRespHdr {
  tPtpV1TimeRep  DRecTimestamp;          /* Delay Receipt Timestamp field   */
  UINT2  u2ReqSrcPortId;                 /* Requesting Source Port Id field */
  UINT2  u2ReqSrcSeqId;                  /* Requesting Source Sequence Id
                                          * field                          */
  UINT1  u1ReqSrcCommTech;               /* Requesting Source Communication 
                                          * Technology field               */
  UINT1  au1Pad[3];                      /* Padding                        */
  INT1   ai1ReqSrcUuid[PTP_UUID_LENGTH+2]; /* Requesting Source Uuid field    */
} tPtpv1DRespHdr;

/* PTP version 1 Information which is used in the PTP version Compatability
 * sub module to collect the v1 Info from the received v1 PTP Pdu */
typedef struct _tPtpv1Info {
    tPtpv1ComHdr      ComHdr;        /* V1 Common header Information         */
    tPtpv1SyncDReqHdr SyncDReqHdr;   /* V1 Sync or Delay Request header 
                                      * Information                          */
    tPtpv1FollowUpHdr FollowUpHdr;   /* V1 Follow up header Information      */
    tPtpv1DRespHdr    DRespHdr;      /* V1 Delay Response header Information */
} tPtpv1Info;

/* PTP version 2 Time Stamp */
typedef struct _tPtpTimeStamp {
    FS_UINT8 u8Sec;    /* Second field - should be 6 Byte */
    UINT4    u4NanoSec;   /* nano second field */
} tPtpTimeStamp;

/* PTP version 2 Port Identity  */
typedef struct _tPortIdentity {
    tClkId ClkId;         /* Clock Identity  */ 
    UINT2  u2PortNumber;  /* Port number     */
    UINT1  au1Pad[2];     /* Padding         */
} tPortIdentity;

/* PTP version 2 Common Message header */
typedef struct _tPtpv2ComHdr {
    tPortIdentity  SrcPortId;    /* Source Port Identifier         */
    FS_UINT8       u8CorrectionField; /* Correction field          */
    UINT2          u2MsgLen;     /* Length of the message          */
    UINT2          u2SeqId;      /* Sequence Identifier of the PDU */
    UINT1          u1TranSpc;    /* */
    UINT1          u1MsgType;    /* Message Type                   */
    UINT1          u1PtpVersion; /* Version of the received PTP
                                    Message                         */
    UINT1          u1DomainId;   /* Received Domain Identifier      */
    UINT1          u1ContField;  /* Control field                   */ 
    INT1           ai1FlagField[2]; /* Flag field                   */    
    INT1           i1LogMsgInter;   /* log Message interval         */

} tPtpv2ComHdr;

/* PTP version 2 Announce message */
typedef struct _tPtpv2AnncHdr {
    tPtpTimeStamp  OrigTimestamp;   /* value of origin Timestamp           */
    tPtpClkQuality GMClkQuality;    /* value of grandmaster ClockQuality   */
    tClkId         GMClkId;         /* value of grandmaster Clock Id       */
    UINT2          u2StepsRemoved;  /* value of stepsRemoved field         */
    INT2           i2CurrUTCOffset; /* value of currentUtcOffset           */
    UINT1          u1GMPri1;        /* value of grandmaster Priority1      */
    UINT1          u1GMPri2;        /* value of grandmaster Priority2      */
    UINT1          u1TimeSrc;       /* value of Times Source               */
    UINT1          u1Pad;           /* Padding                             */
} tPtpv2AnncHdr;

/* PTP version 2 Sync message */
typedef struct _tPtpv2SyncHdr {
    tPtpTimeStamp  OrigTimestamp; /* value of the origin Timestamp */
} tPtpv2SyncHdr;

/* PTP version 2 Follow_Up message */
typedef struct _tPtpv2FollowUpHdr {
    tPtpTimeStamp PreOrigTimestamp;  /* value of the precise Origin Timestamp */
} tPtpv2FollowUpHdr;

/* PTP version 2 Delay_Resp message */
typedef struct _tPtpv2DRespHdr {
  tPtpTimeStamp RecTimestamp;  /* value of the receive Timestamp        */
  tPortIdentity ReqPortId;     /* value of the requesting PortI dentity */
} tPtpv2DRespHdr;

/* PTP version 2 Information which is used in the PTP version Compatability
 * sub module to collect the v2 Info from the received v2 PTP Pdu, which will
 * be used to form the v1 message */
typedef struct _tPtpv2Info {
    tPtpv2ComHdr      ComHdr;    /* V2 Common header Information         */
    tPtpv2AnncHdr     AnncHdr;   /* V2 Announce Msg header Information   */
    tPtpv2SyncHdr     SyncDReqHdr;  /* V2 Sync or Delay Request header
                                     * Information                       */
    tPtpv2FollowUpHdr FollowUpHdr; /* V2 Follow up header Information    */
    tPtpv2DRespHdr    DRespHdr;    /* V2 Delay Response header Information*/
    /* The fllowing are the extra field required to form v1 
     * Sync/ Delay Request Message */
    tPtpClkQuality       LocalClkQuality;    /* Local Clock Quality      */
    tPortIdentity        ParentPortId;       /* Parent Port Id           */
    INT4                 i4ObsParentClkPhaseCngRate;
                         /* Observed Parent Clock Phase Change Rate      */
    UINT2                u2ObsParentOffsetScaledLogVar;
                         /* Observed Parent Offset Scaled Log Variance   */
    UINT2                u2GMPortId; /* Grandmaster Port identifier      */
    UINT2                u2GMSeqId;  /* Grandmaster Sequence identifier  */
    UINT1                u1LocalTimeSrc; /* Local Time source value      */
    BOOL1                bCurrUtcOffsetValid; /* Current UTC Offset valid 
                                               field                     */
} tPtpv2Info;

/* This structure will be used in the PTP version Compatability
 * sub module to pass v1 and v2 Packet information with in the Compatability 
 * sub module functions */
typedef struct _tPtpCompPktInfo {
    tPtpPort *pPtpPort;        /* PTP Port Pointer                   */    
    UINT1 *pu1Ptpv1Pdu;        /* PTP version 1 Pdu Message          */
    UINT1 *pu1Ptpv2Pdu;        /* PTP version 2 Pdu Message          */
    UINT4 u4Ptpv1PduLen;       /* PTP version 1 Pdu Length           */
    UINT4 u4Ptpv2PduLen;       /* PTP version 2 Pdu Length           */
} tPtpCompPktInfo;

/* This structure used to send the Packet from the PTP Packet Tx module 
 * to PTP version Compatability sub module */
typedef struct _tPtpCompTxInfo {
    tPtpCompPktInfo Tx;   /* Tx information  */
} tPtpCompTxInfo;

/* This structure used to in the Rx of version 1 PTP Packet Processing in 
 * PTP version Compatability sub module */
typedef struct tPtpCompRxInfo {
    tPtpCompPktInfo  Rx;                 /* Rx information               */
    UINT1            *pu1Ptpv2AnncPdu;   /* Announce Message Pdu         */
    UINT4            u4Ptpv2AnncPduLen;  /* Announce Message Pdu Length  */
} tPtpCompRxInfo; 

typedef struct _PtpGlobalInfo {
    tOsixTaskId         TaskId; /* PTP Task Id */
    tOsixQId            PtpQId; /* Queue used for messages handling */
    tOsixSemId          SemId;  /* PTP Semaphore Id */
    tMemPoolId          QMsgPoolId;
    tMemPoolId          ContextPoolId;
    tMemPoolId          DomainPoolId;
    tMemPoolId          PortPoolId;
    tMemPoolId          ForeignMasterPoolId;
    tMemPoolId          GMClusterPoolId;
    tMemPoolId          AccMasterPoolId;
    tMemPoolId          UnicMasterPoolId;
    tMemPoolId          SAPoolId;
    tMemPoolId          SecKeyPoolId;
    tMemPoolId          CompPoolId; 
                        /* This corresponds to the memory pool identifier 
                         * used for allocating memory whenever a 
                         * new Compatibility Entry is created in PTP.*/
    tMemPoolId          AltTimSclPoolId;
                        /* This corresponds to the memory pool identifier 
                         * used for allocating memory whenever a new 
                         * Alternate Timescale Entry is created in PTP.*/
    tMemPoolId          UNegGrantorTblPoolId; 
                        /* This corresponds to the memory pool identifier 
                         * used for allocating memory whenever a new 
                         * Unicast negotiation grantor table Entry is 
                         * created in PTP. */
    tMemPoolId          UNegGranteeTblPoolId; 
                        /* This corresponds to the memory pool identifier 
                         * used for allocating memory whenever a new 
                         * Unicast Negotiation Grantee table Entry is 
                         * created in PTP. */
    tMemPoolId          UtilityPoolId;
    tTimerListId        PtpTmrListId; /* Timer List Id */
    tRBTree             ContextTree;
                        /*  PTP context */
    tRBTree             PortTree;
                        /*  PTP Ports */
    tRBTree             FMTree;
                        /*  Foreign Master Tree */
    tRBTree             DomainTree;
                        /* List of domains in this context */
    tRBTree             GrandMastClustLst;
                        /* list of grand masters of the grand master
                         * cluster for this clock */
    tRBTree             AccMastLst;
                        /* list of acceptable masters for this clock */
    tRBTree             SATree;
                        /* Security Associations */
    tRBTree             SecKeyLst;
                        /* List Of Security Key List */
    tRBTree             UnicastMastLst;
                        /* list of unicast masters for this port */
    tRBTree             CompTree;
                        /* This corresponds to the RBTree used to store
                         * Compatibility  related information within PTP */
    tRBTree             AltTimSclTree;
                        /* This corresponds to the RBTree used to store
                         * Alternate timescale  related information
                         * within PTP */
    tRBTree             UNegGrantorTblTree;
                        /* This corresponds to the RBTree used to store
                          * Unicast Negotiation grantor table related
                          * information within PTP */
    tRBTree             UNegGranteeTblTree; 
                         /* This corresponds to the RBTree used to store
                          * Unicast Negotiation grantee table related
                          * information within PTP */
    tTMO_HASH_TABLE    *pPortHashTable;
    tTMO_SLL            saList;
                          /* list of security association used for SNMP
                           * operation */
    tPtpMsgFn           PtpMsgFn[PTP_MAX_MSG];
                           /* Contains the set of routines specific to
                            * different type of messages */
    tTmrDesc            aTmrDesc[PTP_MAX_TMR_TYPES];
                           /* Timer data structure that contains function
                            * ptrs for timer handling and offsets to identify
                            * the data structure containing timer block.
                            * */
    tPtpv1Info          Ptpv1Info; /* PTP V1 info for Compatibility sub
                                      module */
    tPtpv2Info          Ptpv2Info; /* PTP V2 info for Compatibility sub
                                      module */
    eTimeStamp          TimeStamp;
                            /* Denotes hardware or software time stamp */
    INT4                i4PktBuddyId;
                           /* Buddy Identifier for Packets */
    INT4                i4PTrcBuddyId; 
                           /* Memory for Path Trace TLV */
    INT4                i4AltTimeBuddyId; 
                           /* Memory for Alternate timescale TLV */
    INT4                i4Ipv4EvtSockId;
                          /* Socket identifier used for receiving Event
                           * Messages */
    INT4                i4Ipv4GnlSockId;
                          /* Socket identifier used for receiving general
                           * messages */
    INT4                i4Ipv6EvtSockId;
                          /* Socket identifier used for receiving Event
                           * Messages */
    INT4                i4Ipv6GnlSockId;
                          /* Socket identifier used for receiving general
                           * messages */
    UINT4               u4PrimaryContextId;
                          /* Primary ContextId */
    UINT2               u2GblTrace;
    UINT2               u2TrapControl;
    UINT1               u1SysCtrlStatus;
                            /* PTP System control status */
    UINT1               u1HardwareTSOpt;
                          /* Provides the time stamping option */
    UINT1               au1pad[2];
}tPtpGlobalInfo;

/* PTP Message Processing Structures */
typedef struct _tPtpHdrInfo {
    FS_UINT8            u8CorrectionField; /* Correction Field */
    tClkId              ClkId; /* clock Identifier */
    UINT2               u2Port; /* Port number */
    UINT2               u2MsgLen; /* Length of the message */
    UINT2               u2Flags; /* Flags in the received PDU */
    UINT2               u2SeqId; /* Sequence Identifier of the PDU */
    UINT1               u1FirstByte; /* Contains two fields, transport
                                        specific and message type */
    UINT1               u1PtpVersion; /* Version of the received PTP
                                         Message */
    UINT1               u1DomainId; /* Received Domain Identifier */
    UINT1               u1MsgType; /* Type of the message */
    UINT1               u1LogMsgInterval; /* Received u1LogMsgInterval */
    UINT1               au1Pad[3]; /* Padding */
}tPtpHdrInfo;

typedef struct _tPtpRmMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
} tPtpRmMsg;



/* This is the main PTP queue message structure */
typedef struct PtpQMsg {
    tClkSysTimeInfo        PtpSysTimeInfo;
    ePtpDeviceType         PtpDeviceType;
    UINT4                  u4ContextId;
    UINT4                  u4MsgType;
    UINT4                  u4Port;
    UINT4                  u4PktLen;

    union {
        tCRU_BUF_CHAIN_DESC  *pPtpPdu;
        VOID                 *pPtpMsg;
        UINT1                 u1MsgType;
#ifdef L2RED_WANTED
        tPtpRmMsg             PtpCtrlRmMsg;
#endif /* L2RED_WANTED */
    }uPtpMsgParam;

    tVlanId                VlanId;
    UINT1                  u1DomainId;
    UINT1                  u1PortType;
    UINT1                  u1PortOperStatus;
    UINT1                  au1Reserved[3];
#ifdef L2RED_WANTED
#define PtpRmMsg    uPtpMsgParam.PtpCtrlRmMsg
#endif /* L2RED_WANTED */
}tPtpQMsg;
/* Structure for trace */

typedef struct _tPtpUtlValidTraces
{
    UINT1   *pu1TraceStrings;
    UINT2    u2MaxTrcTypes;
    UINT1    au1Pad[2];
}tPtpUtlValidTraces;

/* enum for sizing id */
enum
{
    PTP_PKT_SIZING_ID=0,
    PTP_PTRC_SIZING_ID,
    PTP_ALT_TIME_SIZING_ID,
    PTP_NO_SIZING_ID
};

typedef struct PtpNotifyInfo
{
    UINT4 u4ContextId;
    UINT4 u4DomainId;
    UINT4 u4PortId;
    UINT4 u4ContextAdminState;
    UINT4 u4ContextRowState;
    UINT4 u4PortState;
    UINT4 u4PortAdminStatus;
    UINT4 u4ErrType;
}tPtpNotifyInfo;

typedef struct _PtpTxParams
{
    UINT1               *pu1PtpPdu;
    tPtpPort            *pPtpPort;
    tClkSysTimeInfo     *pPeerDelayReqIngressTs;
    tFsClkTimeVal       *pEventTimeStamp;
    UINT4                u4MsgLen;
    UINT1                u1MsgType;
    UINT1                au1Pad[3];
}tPtpTxParams;

typedef struct
{
    FS_UINT8         u8PeerMeanPathDelay;
    UINT4            u4ContextId;
    UINT4            u4IfIndex;
    UINT1            u1DomainId;
    UINT1            au1Pad[3];
} tPtpNpWrPeerDelay;
/* structure to hold the np PtpNpWrConfigHwClk wrapper's argument */
typedef struct _PtpNpWrCfgHwClk
{
    tClkSysTimeInfo     *pPtpSysTimeInfo; 
    FS_UINT8             u8Offset;
    UINT4                u4IfIndex;
    INT1                 i1OffsetSign;
    UINT1                au1Pad[3];
}tPtpNpWrCfgHwClk;

#endif
