/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 * 
 * $Id: ptpsize.h,v 1.3 2014/03/15 14:18:20 siva Exp $
 *
 * Description: This file contains all the sizing related constants
 *              used by the PTP module. 
 *
 *******************************************************************/
#ifndef _PTPSIZE_H_
#define _PTPSIZE_H_

#define PTP_MAX_CONTEXTS               128

#define PTP_MAX_DOMAINS                128

#define PTP_MAX_PORTS                  128

#define EXT_PTP_MAX_PORTS              136

#define PTP_MAX_FOREIGN_MAST           256

#define PTP_MAX_GRAND_MASTERS_CLUSTER  1

#define PTP_MAX_ACCEPTABLE_MASTERS     256

#define PTP_MAX_UNICAST_MASTERS_LIST   1

#define PTP_MAX_SA                     1

#define PTP_MAX_SECURITY_KEY_LIST      1

#define PTP_MAX_COMP_DB                128

#define PTP_MAX_ALT_TIME_SCALE_ENTRY   256

#define PTP_MAX_UNI_TBL_ENTRY          1

#define PTP_MAX_UNI_PEER_TBL_ENTRY     1

#define PTP_MAX_UTIL_ENTRY             10

/* The following MACRO defines the maximum PDP length of the PTP. This
 * will be used for creating the buddy memory. This includes header fields 
 * too
 * */
#define PTP_MAX_PDU_LEN            1500

#define PTP_MAX_TMR_TYPES             8

#define PTP_MAX_PTRC_TLV_BLOCK_SIZE  404
#define PTP_MIN_PTRC_TLV_BLOCK_SIZE  12
#define PTP_MAX_PTRC_BLOCKS          (PTP_MAX_DOMAINS)

/* No of alternate time scale for a domain */
#define PTP_MAX_ALT_TIME_PER_DOMAIN     20  
/* Max size of alternate time scale for a domain */
#define PTP_MAX_ALT_TIME_TLV_BLOCK_SIZE ((PTP_MAX_ALT_TIME_PER_DOMAIN + \
                                          PTP_TMP_ALT_TIME_PER_CONTEXT) *\
                                         PTP_MAX_ALT_TIME_TLV_SIZE)
#define PTP_MIN_ALT_TIME_TLV_BLOCK_SIZE  20
#define PTP_MAX_ALT_TIME_BLOCKS          (PTP_MAX_DOMAINS)

/* The following MACROS will be used to create buddy memory pools
 * */
#define PTP_PKT_BUDDY_DEF_VAL            44
#define PTP_PKT_BUDDY_MAX_VAL            1500 /* Assumed based on the 
                                                 default MTU
                                               */  
#define PTP_MAX_PKT_BUDDY                6 /* One of the alllocated memory will
                                              be used to send Follow up
                                            */
#endif /*_PTPSIZE_H_*/
