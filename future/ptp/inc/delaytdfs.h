/*****************************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: delaytdfs.h,v 1.1 2012/07/26 09:24:21 siva Exp $
*
* Description: This file contains the data structure definition for Delay module
******************************************************************************/
#ifndef _DEL_TDFS_H
#define _DEL_TDFS_H

typedef struct _DelaySystem
{
    tTmrBlk               G8261DelayTimer; 
    UINT4                 u4G8261ModuleStatus;
}tDelaySystem;

typedef struct _DelayParams
{
    UINT4       u4SwitchCount;
    UINT4       u4PacketSize;
    UINT4       u4Load;
    UINT4       u4NwDisturbance;
    UINT4       u4DelayDuration;
    UINT4       u4FlowInterval;
    UINT4       u4Frequency;
    UINT1       u1NwDirection;
    UINT1       u1FlowVariation;
    UINT1       au1Pad[2];

}tDelayParams;

typedef struct _DelayOutput
{
    FS_UINT8   u8MeanVariance;
    FS_UINT8   u8Delay;
    UINT4      u4Offset;

}tDelayOutput;
#endif
