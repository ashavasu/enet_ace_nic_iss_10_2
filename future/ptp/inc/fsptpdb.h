/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsptpdb.h,v 1.5 2014/01/03 12:40:03 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPTPDB_H
#define _FSPTPDB_H

UINT1 FsPtpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpDomainDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpClockDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpCurrentDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpParentDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpTimeDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpPortConfigDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpForeignMasterDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_FIXED_LENGTH_OCTET_STRING ,8 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpTransparentDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpTransparentPortDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpGrandMasterClusterDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPtpUnicastMasterDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPtpAccMasterDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsPtpSecKeyDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpSADataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPtpAltTimeScaleDataSetTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsptp [] ={1,3,6,1,4,1,29601,2,45};
tSNMP_OID_TYPE fsptpOID = {9, fsptp};


UINT4 FsPtpGlobalSysCtrl [ ] ={1,3,6,1,4,1,29601,2,45,1,1,1};
UINT4 FsPtpGblTraceOption [ ] ={1,3,6,1,4,1,29601,2,45,1,1,2};
UINT4 FsPtpPrimaryContext [ ] ={1,3,6,1,4,1,29601,2,45,1,1,3};
UINT4 FsPtpContextId [ ] ={1,3,6,1,4,1,29601,2,45,1,1,4,1,1};
UINT4 FsPtpAdminStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,1,4,1,2};
UINT4 FsPtpTraceOption [ ] ={1,3,6,1,4,1,29601,2,45,1,1,4,1,3};
UINT4 FsPtpContextType [ ] ={1,3,6,1,4,1,29601,2,45,1,1,4,1,4};
UINT4 FsPtpPrimaryDomain [ ] ={1,3,6,1,4,1,29601,2,45,1,1,4,1,5};
UINT4 FsPtpContextRowStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,1,4,1,6};
UINT4 FsPtpDomainNumber [ ] ={1,3,6,1,4,1,29601,2,45,1,2,1,1,1};
UINT4 FsPtpDomainClockMode [ ] ={1,3,6,1,4,1,29601,2,45,1,2,1,1,2};
UINT4 FsPtpDomainClockIdentity [ ] ={1,3,6,1,4,1,29601,2,45,1,2,1,1,3};
UINT4 FsPtpDomainGMClusterQueryInterval [ ] ={1,3,6,1,4,1,29601,2,45,1,2,1,1,4};
UINT4 FsPtpDomainRowStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,2,1,1,5};
UINT4 FsPtpClockIdentity [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,1};
UINT4 FsPtpClockTwoStepFlag [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,2};
UINT4 FsPtpClockNumberPorts [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,3};
UINT4 FsPtpClockClass [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,4};
UINT4 FsPtpClockAccuracy [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,5};
UINT4 FsPtpClockOffsetScaledLogVariance [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,6};
UINT4 FsPtpClockPriority1 [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,7};
UINT4 FsPtpClockPriority2 [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,8};
UINT4 FsPtpClockSlaveOnly [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,9};
UINT4 FsPtpClockPathTraceOption [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,10};
UINT4 FsPtpClockAccMasterMaxSize [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,11};
UINT4 FsPtpClockSecurityEnabled [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,12};
UINT4 FsPtpClockNumOfSA [ ] ={1,3,6,1,4,1,29601,2,45,1,3,1,1,13};
UINT4 FsPtpCurrentStepsRemoved [ ] ={1,3,6,1,4,1,29601,2,45,1,4,1,1,1};
UINT4 FsPtpCurrentOffsetFromMaster [ ] ={1,3,6,1,4,1,29601,2,45,1,4,1,1,2};
UINT4 FsPtpCurrentMeanPathDelay [ ] ={1,3,6,1,4,1,29601,2,45,1,4,1,1,3};
UINT4 FsPtpCurrentMasterToSlaveDelay [ ] ={1,3,6,1,4,1,29601,2,45,1,4,1,1,4};
UINT4 FsPtpCurrentSlaveToMasterDelay [ ] ={1,3,6,1,4,1,29601,2,45,1,4,1,1,5};
UINT4 FsPtpParentClockIdentity [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,1};
UINT4 FsPtpParentPortNumber [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,2};
UINT4 FsPtpParentStats [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,3};
UINT4 FsPtpParentObservedOffsetScaledLogVariance [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,4};
UINT4 FsPtpParentObservedClockPhaseChangeRate [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,5};
UINT4 FsPtpParentGMClockIdentity [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,6};
UINT4 FsPtpParentGMClockClass [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,7};
UINT4 FsPtpParentGMClockAccuracy [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,8};
UINT4 FsPtpParentGMClockOffsetScaledLogVariance [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,9};
UINT4 FsPtpParentGMPriority1 [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,10};
UINT4 FsPtpParentGMPriority2 [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,11};
UINT4 FsPtpParentClockObservedDrift [ ] ={1,3,6,1,4,1,29601,2,45,1,5,1,1,12};
UINT4 FsPtpTimeCurrentUTCOffset [ ] ={1,3,6,1,4,1,29601,2,45,1,6,1,1,1};
UINT4 FsPtpTimeCurrentUTCOffsetValid [ ] ={1,3,6,1,4,1,29601,2,45,1,6,1,1,2};
UINT4 FsPtpTimeLeap59 [ ] ={1,3,6,1,4,1,29601,2,45,1,6,1,1,3};
UINT4 FsPtpTimeLeap61 [ ] ={1,3,6,1,4,1,29601,2,45,1,6,1,1,4};
UINT4 FsPtpTimeTimeTraceable [ ] ={1,3,6,1,4,1,29601,2,45,1,6,1,1,5};
UINT4 FsPtpTimeFrequencyTraceable [ ] ={1,3,6,1,4,1,29601,2,45,1,6,1,1,6};
UINT4 FsPtpTimeTimeSource [ ] ={1,3,6,1,4,1,29601,2,45,1,6,1,1,7};
UINT4 FsPtpPortIndex [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,1};
UINT4 FsPtpPortClockIdentity [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,2};
UINT4 FsPtpPortInterfaceType [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,3};
UINT4 FsPtpPortIfaceNumber [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,4};
UINT4 FsPtpPortState [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,5};
UINT4 FsPtpPortMinDelayReqInterval [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,6};
UINT4 FsPtpPortPeerMeanPathDelay [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,7};
UINT4 FsPtpPortAnnounceInterval [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,8};
UINT4 FsPtpPortAnnounceReceiptTimeout [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,9};
UINT4 FsPtpPortSyncInterval [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,10};
UINT4 FsPtpPortSynclimit [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,11};
UINT4 FsPtpPortDelayMechanism [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,12};
UINT4 FsPtpPortMinPdelayReqInterval [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,13};
UINT4 FsPtpPortVersionNumber [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,14};
UINT4 FsPtpPortUnicastNegOption [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,15};
UINT4 FsPtpPortUnicastMasterMaxSize [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,16};
UINT4 FsPtpPortAccMasterEnabled [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,17};
UINT4 FsPtpPortNumOfAltMaster [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,18};
UINT4 FsPtpPortAltMulcastSync [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,19};
UINT4 FsPtpPortAltMulcastSyncInterval [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,20};
UINT4 FsPtpPortPtpStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,21};
UINT4 FsPtpPortRcvdAnnounceMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,22};
UINT4 FsPtpPortRcvdSyncMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,23};
UINT4 FsPtpPortRcvdDelayReqMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,24};
UINT4 FsPtpPortRcvdDelayRespMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,25};
UINT4 FsPtpPortTransDelayReqMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,26};
UINT4 FsPtpPortDiscardedMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,27};
UINT4 FsPtpPortTransAnnounceMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,28};
UINT4 FsPtpPortTransSyncMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,29};
UINT4 FsPtpPortRcvdPeerDelayReqMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,30};
UINT4 FsPtpPortTransPeerDelayReqMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,31};
UINT4 FsPtpPortRcvdPeerDelayRespMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,32};
UINT4 FsPtpPortTransPeerDelayRespMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,33};
UINT4 FsPtpPortTransDelayRespMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,34};
UINT4 FsPtpPortTransPeerDelayRespFollowUpMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,35};
UINT4 FsPtpPortTransFollowUpMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,36};
UINT4 FsPtpPortRcvdPeerDelayRespFollowUpMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,37};
UINT4 FsPtpPortRcvdFollowUpMsgCnt [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,38};
UINT4 FsPtpPortRowStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,7,1,1,39};
UINT4 FsPtpForeignMasterClockIdentity [ ] ={1,3,6,1,4,1,29601,2,45,1,8,1,1,1};
UINT4 FsPtpForeignMasterPortIndex [ ] ={1,3,6,1,4,1,29601,2,45,1,8,1,1,2};
UINT4 FsPtpForeignMasterAnnounceMsgs [ ] ={1,3,6,1,4,1,29601,2,45,1,8,1,1,3};
UINT4 FsPtpTransparentClockIdentity [ ] ={1,3,6,1,4,1,29601,2,45,1,9,1,1,1};
UINT4 FsPtpTransparentClockTwoStepFlag [ ] ={1,3,6,1,4,1,29601,2,45,1,9,1,1,2};
UINT4 FsPtpTransparentClockNumberPorts [ ] ={1,3,6,1,4,1,29601,2,45,1,9,1,1,3};
UINT4 FsPtpTransparentClockDelaymechanism [ ] ={1,3,6,1,4,1,29601,2,45,1,9,1,1,4};
UINT4 FsPtpTransparentClockPrimaryDomain [ ] ={1,3,6,1,4,1,29601,2,45,1,9,1,1,5};
UINT4 FsPtpTransparentPortIndex [ ] ={1,3,6,1,4,1,29601,2,45,1,10,1,1,1};
UINT4 FsPtpTransparentPortInterfaceType [ ] ={1,3,6,1,4,1,29601,2,45,1,10,1,1,2};
UINT4 FsPtpTransparentPortIfaceNumber [ ] ={1,3,6,1,4,1,29601,2,45,1,10,1,1,3};
UINT4 FsPtpTransparentPortClockIdentity [ ] ={1,3,6,1,4,1,29601,2,45,1,10,1,1,4};
UINT4 FsPtpTransparentPortMinPdelayReqInterval [ ] ={1,3,6,1,4,1,29601,2,45,1,10,1,1,5};
UINT4 FsPtpTransparentPortFaultyFlag [ ] ={1,3,6,1,4,1,29601,2,45,1,10,1,1,6};
UINT4 FsPtpTransparentPortPeerMeanPathDelay [ ] ={1,3,6,1,4,1,29601,2,45,1,10,1,1,7};
UINT4 FsPtpTransparentPortPtpStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,10,1,1,8};
UINT4 FsPtpTransparentPortRowStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,10,1,1,9};
UINT4 FsPtpGrandMasterClusterNetworkProtocol [ ] ={1,3,6,1,4,1,29601,2,45,1,11,1,1,1};
UINT4 FsPtpGrandMasterClusterAddLength [ ] ={1,3,6,1,4,1,29601,2,45,1,11,1,1,2};
UINT4 FsPtpGrandMasterClusterAddr [ ] ={1,3,6,1,4,1,29601,2,45,1,11,1,1,3};
UINT4 FsPtpGrandMasterClusterRowStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,11,1,1,4};
UINT4 FsPtpUnicastMasterNetworkProtocol [ ] ={1,3,6,1,4,1,29601,2,45,1,12,1,1,1};
UINT4 FsPtpUnicastMasterAddLength [ ] ={1,3,6,1,4,1,29601,2,45,1,12,1,1,2};
UINT4 FsPtpUnicastMasterAddr [ ] ={1,3,6,1,4,1,29601,2,45,1,12,1,1,3};
UINT4 FsPtpUnicastMasterRowStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,12,1,1,4};
UINT4 FsPtpAccMasterNetworkProtocol [ ] ={1,3,6,1,4,1,29601,2,45,1,13,1,1,1};
UINT4 FsPtpAccMasterAddLength [ ] ={1,3,6,1,4,1,29601,2,45,1,13,1,1,2};
UINT4 FsPtpAccMasterAddr [ ] ={1,3,6,1,4,1,29601,2,45,1,13,1,1,3};
UINT4 FsPtpAccMasterAlternatePriority [ ] ={1,3,6,1,4,1,29601,2,45,1,13,1,1,4};
UINT4 FsPtpAccMasterRowStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,13,1,1,5};
UINT4 FsPtpSecKeyId [ ] ={1,3,6,1,4,1,29601,2,45,1,14,1,1,1};
UINT4 FsPtpSecKeyAlgorithmId [ ] ={1,3,6,1,4,1,29601,2,45,1,14,1,1,2};
UINT4 FsPtpSecKeyLength [ ] ={1,3,6,1,4,1,29601,2,45,1,14,1,1,3};
UINT4 FsPtpSecKey [ ] ={1,3,6,1,4,1,29601,2,45,1,14,1,1,4};
UINT4 FsPtpSecKeyStartTime [ ] ={1,3,6,1,4,1,29601,2,45,1,14,1,1,5};
UINT4 FsPtpSecKeyExpirationTime [ ] ={1,3,6,1,4,1,29601,2,45,1,14,1,1,6};
UINT4 FsPtpSecKeyValid [ ] ={1,3,6,1,4,1,29601,2,45,1,14,1,1,7};
UINT4 FsPtpSecKeyRowStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,14,1,1,8};
UINT4 FsPtpSAId [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,1};
UINT4 FsPtpSASrcPortNumber [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,2};
UINT4 FsPtpSASrcAddrLength [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,3};
UINT4 FsPtpSASrcAddr [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,4};
UINT4 FsPtpSADstPortNumber [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,5};
UINT4 FsPtpSADstAddrLength [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,6};
UINT4 FsPtpSADstAddr [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,7};
UINT4 FsPtpSASrcClockIdentity [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,8};
UINT4 FsPtpSADstClockIdentity [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,9};
UINT4 FsPtpSAReplayCounter [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,10};
UINT4 FsPtpSALifeTimeId [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,11};
UINT4 FsPtpSAKeyId [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,12};
UINT4 FsPtpSANextLifeTimeId [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,13};
UINT4 FsPtpSANextKeyId [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,14};
UINT4 FsPtpSATrustState [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,15};
UINT4 FsPtpSATrustTimer [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,16};
UINT4 FsPtpSATrustTimeout [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,17};
UINT4 FsPtpSAChallengeState [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,18};
UINT4 FsPtpSAChallengeTimer [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,19};
UINT4 FsPtpSAChallengeTimeOut [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,20};
UINT4 FsPtpSARequestNonce [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,21};
UINT4 FsPtpSAResponseNonce [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,22};
UINT4 FsPtpSAChallengeRequired [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,23};
UINT4 FsPtpSAResponseRequired [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,24};
UINT4 FsPtpSATypeField [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,25};
UINT4 FsPtpSADirection [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,26};
UINT4 FsPtpSARowStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,15,1,1,27};
UINT4 FsPtpAltTimeScaleKeyId [ ] ={1,3,6,1,4,1,29601,2,45,1,16,1,1,1};
UINT4 FsPtpAltTimeScalecurrentOffset [ ] ={1,3,6,1,4,1,29601,2,45,1,16,1,1,2};
UINT4 FsPtpAltTimeScalejumpSeconds [ ] ={1,3,6,1,4,1,29601,2,45,1,16,1,1,3};
UINT4 FsPtpAltTimeScaletimeOfNextJump [ ] ={1,3,6,1,4,1,29601,2,45,1,16,1,1,4};
UINT4 FsPtpAltTimeScaledisplayName [ ] ={1,3,6,1,4,1,29601,2,45,1,16,1,1,5};
UINT4 FsPtpAltTimeScaleRowStatus [ ] ={1,3,6,1,4,1,29601,2,45,1,16,1,1,6};
UINT4 FsPtpTrapContextName [ ] ={1,3,6,1,4,1,29601,2,45,2,1};
UINT4 FsPtpTrapDomainNumber [ ] ={1,3,6,1,4,1,29601,2,45,2,2};
UINT4 FsPtpGlobalErrTrapType [ ] ={1,3,6,1,4,1,29601,2,45,2,3};
UINT4 FsPtpNotification [ ] ={1,3,6,1,4,1,29601,2,45,2,4};




tMbDbEntry fsptpMibEntry[]= {

{{12,FsPtpGlobalSysCtrl}, NULL, FsPtpGlobalSysCtrlGet, FsPtpGlobalSysCtrlSet, FsPtpGlobalSysCtrlTest, FsPtpGlobalSysCtrlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsPtpGblTraceOption}, NULL, FsPtpGblTraceOptionGet, FsPtpGblTraceOptionSet, FsPtpGblTraceOptionTest, FsPtpGblTraceOptionDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "critical"},

{{12,FsPtpPrimaryContext}, NULL, FsPtpPrimaryContextGet, FsPtpPrimaryContextSet, FsPtpPrimaryContextTest, FsPtpPrimaryContextDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{14,FsPtpContextId}, GetNextIndexFsPtpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpTableINDEX, 1, 0, 0, NULL},

{{14,FsPtpAdminStatus}, GetNextIndexFsPtpTable, FsPtpAdminStatusGet, FsPtpAdminStatusSet, FsPtpAdminStatusTest, FsPtpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpTableINDEX, 1, 0, 0, "2"},

{{14,FsPtpTraceOption}, GetNextIndexFsPtpTable, FsPtpTraceOptionGet, FsPtpTraceOptionSet, FsPtpTraceOptionTest, FsPtpTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpTableINDEX, 1, 0, 0, "critical"},

{{14,FsPtpContextType}, GetNextIndexFsPtpTable, FsPtpContextTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpTableINDEX, 1, 0, 0, NULL},

{{14,FsPtpPrimaryDomain}, GetNextIndexFsPtpTable, FsPtpPrimaryDomainGet, FsPtpPrimaryDomainSet, FsPtpPrimaryDomainTest, FsPtpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpTableINDEX, 1, 0, 0, "0"},

{{14,FsPtpContextRowStatus}, GetNextIndexFsPtpTable, FsPtpContextRowStatusGet, FsPtpContextRowStatusSet, FsPtpContextRowStatusTest, FsPtpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpTableINDEX, 1, 0, 1, NULL},

{{14,FsPtpDomainNumber}, GetNextIndexFsPtpDomainDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpDomainDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpDomainClockMode}, GetNextIndexFsPtpDomainDataSetTable, FsPtpDomainClockModeGet, FsPtpDomainClockModeSet, FsPtpDomainClockModeTest, FsPtpDomainDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpDomainDataSetTableINDEX, 2, 0, 0, "4"},

{{14,FsPtpDomainClockIdentity}, GetNextIndexFsPtpDomainDataSetTable, FsPtpDomainClockIdentityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpDomainDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpDomainGMClusterQueryInterval}, GetNextIndexFsPtpDomainDataSetTable, FsPtpDomainGMClusterQueryIntervalGet, FsPtpDomainGMClusterQueryIntervalSet, FsPtpDomainGMClusterQueryIntervalTest, FsPtpDomainDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpDomainDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpDomainRowStatus}, GetNextIndexFsPtpDomainDataSetTable, FsPtpDomainRowStatusGet, FsPtpDomainRowStatusSet, FsPtpDomainRowStatusTest, FsPtpDomainDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpDomainDataSetTableINDEX, 2, 0, 1, NULL},

{{14,FsPtpClockIdentity}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockIdentityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpClockDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpClockTwoStepFlag}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockTwoStepFlagGet, FsPtpClockTwoStepFlagSet, FsPtpClockTwoStepFlagTest, FsPtpClockDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpClockDataSetTableINDEX, 2, 0, 0, "2"},

{{14,FsPtpClockNumberPorts}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockNumberPortsGet, FsPtpClockNumberPortsSet, FsPtpClockNumberPortsTest, FsPtpClockDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpClockDataSetTableINDEX, 2, 0, 0, "1"},

{{14,FsPtpClockClass}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpClockDataSetTableINDEX, 2, 0, 0, "248"},

{{14,FsPtpClockAccuracy}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockAccuracyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpClockDataSetTableINDEX, 2, 0, 0, "254"},

{{14,FsPtpClockOffsetScaledLogVariance}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockOffsetScaledLogVarianceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpClockDataSetTableINDEX, 2, 0, 0, "0"},

{{14,FsPtpClockPriority1}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockPriority1Get, FsPtpClockPriority1Set, FsPtpClockPriority1Test, FsPtpClockDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpClockDataSetTableINDEX, 2, 0, 0, "128"},

{{14,FsPtpClockPriority2}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockPriority2Get, FsPtpClockPriority2Set, FsPtpClockPriority2Test, FsPtpClockDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpClockDataSetTableINDEX, 2, 0, 0, "128"},

{{14,FsPtpClockSlaveOnly}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockSlaveOnlyGet, FsPtpClockSlaveOnlySet, FsPtpClockSlaveOnlyTest, FsPtpClockDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpClockDataSetTableINDEX, 2, 0, 0, "2"},

{{14,FsPtpClockPathTraceOption}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockPathTraceOptionGet, FsPtpClockPathTraceOptionSet, FsPtpClockPathTraceOptionTest, FsPtpClockDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpClockDataSetTableINDEX, 2, 0, 0, "2"},

{{14,FsPtpClockAccMasterMaxSize}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockAccMasterMaxSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpClockDataSetTableINDEX, 2, 0, 0, "0"},

{{14,FsPtpClockSecurityEnabled}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockSecurityEnabledGet, FsPtpClockSecurityEnabledSet, FsPtpClockSecurityEnabledTest, FsPtpClockDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpClockDataSetTableINDEX, 2, 0, 0, "2"},

{{14,FsPtpClockNumOfSA}, GetNextIndexFsPtpClockDataSetTable, FsPtpClockNumOfSAGet, FsPtpClockNumOfSASet, FsPtpClockNumOfSATest, FsPtpClockDataSetTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPtpClockDataSetTableINDEX, 2, 0, 0, "128"},

{{14,FsPtpCurrentStepsRemoved}, GetNextIndexFsPtpCurrentDataSetTable, FsPtpCurrentStepsRemovedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpCurrentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpCurrentOffsetFromMaster}, GetNextIndexFsPtpCurrentDataSetTable, FsPtpCurrentOffsetFromMasterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpCurrentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpCurrentMeanPathDelay}, GetNextIndexFsPtpCurrentDataSetTable, FsPtpCurrentMeanPathDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpCurrentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpCurrentMasterToSlaveDelay}, GetNextIndexFsPtpCurrentDataSetTable, FsPtpCurrentMasterToSlaveDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpCurrentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpCurrentSlaveToMasterDelay}, GetNextIndexFsPtpCurrentDataSetTable, FsPtpCurrentSlaveToMasterDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpCurrentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentClockIdentity}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentClockIdentityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentPortNumber}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentPortNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentStats}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentStatsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, "2"},

{{14,FsPtpParentObservedOffsetScaledLogVariance}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentObservedOffsetScaledLogVarianceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentObservedClockPhaseChangeRate}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentObservedClockPhaseChangeRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentGMClockIdentity}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentGMClockIdentityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentGMClockClass}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentGMClockClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentGMClockAccuracy}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentGMClockAccuracyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentGMClockOffsetScaledLogVariance}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentGMClockOffsetScaledLogVarianceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentGMPriority1}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentGMPriority1Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentGMPriority2}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentGMPriority2Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpParentClockObservedDrift}, GetNextIndexFsPtpParentDataSetTable, FsPtpParentClockObservedDriftGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpParentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpTimeCurrentUTCOffset}, GetNextIndexFsPtpTimeDataSetTable, FsPtpTimeCurrentUTCOffsetGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpTimeDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpTimeCurrentUTCOffsetValid}, GetNextIndexFsPtpTimeDataSetTable, FsPtpTimeCurrentUTCOffsetValidGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpTimeDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpTimeLeap59}, GetNextIndexFsPtpTimeDataSetTable, FsPtpTimeLeap59Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpTimeDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpTimeLeap61}, GetNextIndexFsPtpTimeDataSetTable, FsPtpTimeLeap61Get, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpTimeDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpTimeTimeTraceable}, GetNextIndexFsPtpTimeDataSetTable, FsPtpTimeTimeTraceableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpTimeDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpTimeFrequencyTraceable}, GetNextIndexFsPtpTimeDataSetTable, FsPtpTimeFrequencyTraceableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpTimeDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpTimeTimeSource}, GetNextIndexFsPtpTimeDataSetTable, FsPtpTimeTimeSourceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpTimeDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpPortIndex}, GetNextIndexFsPtpPortConfigDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortClockIdentity}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortClockIdentityGet, FsPtpPortClockIdentitySet, FsPtpPortClockIdentityTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortInterfaceType}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortInterfaceTypeGet, FsPtpPortInterfaceTypeSet, FsPtpPortInterfaceTypeTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortIfaceNumber}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortIfaceNumberGet, FsPtpPortIfaceNumberSet, FsPtpPortIfaceNumberTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortState}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortMinDelayReqInterval}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortMinDelayReqIntervalGet, FsPtpPortMinDelayReqIntervalSet, FsPtpPortMinDelayReqIntervalTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "0"},

{{14,FsPtpPortPeerMeanPathDelay}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortPeerMeanPathDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortAnnounceInterval}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortAnnounceIntervalGet, FsPtpPortAnnounceIntervalSet, FsPtpPortAnnounceIntervalTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "1"},

{{14,FsPtpPortAnnounceReceiptTimeout}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortAnnounceReceiptTimeoutGet, FsPtpPortAnnounceReceiptTimeoutSet, FsPtpPortAnnounceReceiptTimeoutTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "3"},

{{14,FsPtpPortSyncInterval}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortSyncIntervalGet, FsPtpPortSyncIntervalSet, FsPtpPortSyncIntervalTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "0"},

{{14,FsPtpPortSynclimit}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortSynclimitGet, FsPtpPortSynclimitSet, FsPtpPortSynclimitTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "1000000000"},

{{14,FsPtpPortDelayMechanism}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortDelayMechanismGet, FsPtpPortDelayMechanismSet, FsPtpPortDelayMechanismTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "1"},

{{14,FsPtpPortMinPdelayReqInterval}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortMinPdelayReqIntervalGet, FsPtpPortMinPdelayReqIntervalSet, FsPtpPortMinPdelayReqIntervalTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "0"},

{{14,FsPtpPortVersionNumber}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortVersionNumberGet, FsPtpPortVersionNumberSet, FsPtpPortVersionNumberTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "2"},

{{14,FsPtpPortUnicastNegOption}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortUnicastNegOptionGet, FsPtpPortUnicastNegOptionSet, FsPtpPortUnicastNegOptionTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "2"},

{{14,FsPtpPortUnicastMasterMaxSize}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortUnicastMasterMaxSizeGet, FsPtpPortUnicastMasterMaxSizeSet, FsPtpPortUnicastMasterMaxSizeTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "0"},

{{14,FsPtpPortAccMasterEnabled}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortAccMasterEnabledGet, FsPtpPortAccMasterEnabledSet, FsPtpPortAccMasterEnabledTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "2"},

{{14,FsPtpPortNumOfAltMaster}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortNumOfAltMasterGet, FsPtpPortNumOfAltMasterSet, FsPtpPortNumOfAltMasterTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "0"},

{{14,FsPtpPortAltMulcastSync}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortAltMulcastSyncGet, FsPtpPortAltMulcastSyncSet, FsPtpPortAltMulcastSyncTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "2"},

{{14,FsPtpPortAltMulcastSyncInterval}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortAltMulcastSyncIntervalGet, FsPtpPortAltMulcastSyncIntervalSet, FsPtpPortAltMulcastSyncIntervalTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "0"},

{{14,FsPtpPortPtpStatus}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortPtpStatusGet, FsPtpPortPtpStatusSet, FsPtpPortPtpStatusTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, "2"},

{{14,FsPtpPortRcvdAnnounceMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortRcvdAnnounceMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortRcvdSyncMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortRcvdSyncMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortRcvdDelayReqMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortRcvdDelayReqMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortRcvdDelayRespMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortRcvdDelayRespMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortTransDelayReqMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortTransDelayReqMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortDiscardedMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortDiscardedMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortTransAnnounceMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortTransAnnounceMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortTransSyncMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortTransSyncMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortRcvdPeerDelayReqMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortRcvdPeerDelayReqMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortTransPeerDelayReqMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortTransPeerDelayReqMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortRcvdPeerDelayRespMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortRcvdPeerDelayRespMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortTransPeerDelayRespMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortTransPeerDelayRespMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortTransDelayRespMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortTransDelayRespMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortTransPeerDelayRespFollowUpMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortTransPeerDelayRespFollowUpMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortTransFollowUpMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortTransFollowUpMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortRcvdPeerDelayRespFollowUpMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortRcvdPeerDelayRespFollowUpMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortRcvdFollowUpMsgCnt}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortRcvdFollowUpMsgCntGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPtpPortConfigDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpPortRowStatus}, GetNextIndexFsPtpPortConfigDataSetTable, FsPtpPortRowStatusGet, FsPtpPortRowStatusSet, FsPtpPortRowStatusTest, FsPtpPortConfigDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpPortConfigDataSetTableINDEX, 3, 0, 1, NULL},

{{14,FsPtpForeignMasterClockIdentity}, GetNextIndexFsPtpForeignMasterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPtpForeignMasterDataSetTableINDEX, 5, 0, 0, NULL},

{{14,FsPtpForeignMasterPortIndex}, GetNextIndexFsPtpForeignMasterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpForeignMasterDataSetTableINDEX, 5, 0, 0, NULL},

{{14,FsPtpForeignMasterAnnounceMsgs}, GetNextIndexFsPtpForeignMasterDataSetTable, FsPtpForeignMasterAnnounceMsgsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpForeignMasterDataSetTableINDEX, 5, 0, 0, NULL},

{{14,FsPtpTransparentClockIdentity}, GetNextIndexFsPtpTransparentDataSetTable, FsPtpTransparentClockIdentityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpTransparentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpTransparentClockTwoStepFlag}, GetNextIndexFsPtpTransparentDataSetTable, FsPtpTransparentClockTwoStepFlagGet, FsPtpTransparentClockTwoStepFlagSet, FsPtpTransparentClockTwoStepFlagTest, FsPtpTransparentDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpTransparentDataSetTableINDEX, 2, 0, 0, "2"},

{{14,FsPtpTransparentClockNumberPorts}, GetNextIndexFsPtpTransparentDataSetTable, FsPtpTransparentClockNumberPortsGet, FsPtpTransparentClockNumberPortsSet, FsPtpTransparentClockNumberPortsTest, FsPtpTransparentDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpTransparentDataSetTableINDEX, 2, 0, 0, "0"},

{{14,FsPtpTransparentClockDelaymechanism}, GetNextIndexFsPtpTransparentDataSetTable, FsPtpTransparentClockDelaymechanismGet, FsPtpTransparentClockDelaymechanismSet, FsPtpTransparentClockDelaymechanismTest, FsPtpTransparentDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpTransparentDataSetTableINDEX, 2, 0, 0, "1"},

{{14,FsPtpTransparentClockPrimaryDomain}, GetNextIndexFsPtpTransparentDataSetTable, FsPtpTransparentClockPrimaryDomainGet, FsPtpTransparentClockPrimaryDomainSet, FsPtpTransparentClockPrimaryDomainTest, FsPtpTransparentDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpTransparentDataSetTableINDEX, 2, 0, 0, NULL},

{{14,FsPtpTransparentPortIndex}, GetNextIndexFsPtpTransparentPortDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpTransparentPortDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpTransparentPortInterfaceType}, GetNextIndexFsPtpTransparentPortDataSetTable, FsPtpTransparentPortInterfaceTypeGet, FsPtpTransparentPortInterfaceTypeSet, FsPtpTransparentPortInterfaceTypeTest, FsPtpTransparentPortDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpTransparentPortDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpTransparentPortIfaceNumber}, GetNextIndexFsPtpTransparentPortDataSetTable, FsPtpTransparentPortIfaceNumberGet, FsPtpTransparentPortIfaceNumberSet, FsPtpTransparentPortIfaceNumberTest, FsPtpTransparentPortDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpTransparentPortDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpTransparentPortClockIdentity}, GetNextIndexFsPtpTransparentPortDataSetTable, FsPtpTransparentPortClockIdentityGet, FsPtpTransparentPortClockIdentitySet, FsPtpTransparentPortClockIdentityTest, FsPtpTransparentPortDataSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpTransparentPortDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpTransparentPortMinPdelayReqInterval}, GetNextIndexFsPtpTransparentPortDataSetTable, FsPtpTransparentPortMinPdelayReqIntervalGet, FsPtpTransparentPortMinPdelayReqIntervalSet, FsPtpTransparentPortMinPdelayReqIntervalTest, FsPtpTransparentPortDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpTransparentPortDataSetTableINDEX, 3, 0, 0, "1"},

{{14,FsPtpTransparentPortFaultyFlag}, GetNextIndexFsPtpTransparentPortDataSetTable, FsPtpTransparentPortFaultyFlagGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpTransparentPortDataSetTableINDEX, 3, 0, 0, "2"},

{{14,FsPtpTransparentPortPeerMeanPathDelay}, GetNextIndexFsPtpTransparentPortDataSetTable, FsPtpTransparentPortPeerMeanPathDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsPtpTransparentPortDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpTransparentPortPtpStatus}, GetNextIndexFsPtpTransparentPortDataSetTable, FsPtpTransparentPortPtpStatusGet, FsPtpTransparentPortPtpStatusSet, FsPtpTransparentPortPtpStatusTest, FsPtpTransparentPortDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpTransparentPortDataSetTableINDEX, 3, 0, 0, "2"},

{{14,FsPtpTransparentPortRowStatus}, GetNextIndexFsPtpTransparentPortDataSetTable, FsPtpTransparentPortRowStatusGet, FsPtpTransparentPortRowStatusSet, FsPtpTransparentPortRowStatusTest, FsPtpTransparentPortDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpTransparentPortDataSetTableINDEX, 3, 0, 1, NULL},

{{14,FsPtpGrandMasterClusterNetworkProtocol}, GetNextIndexFsPtpGrandMasterClusterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpGrandMasterClusterDataSetTableINDEX, 5, 0, 0, NULL},

{{14,FsPtpGrandMasterClusterAddLength}, GetNextIndexFsPtpGrandMasterClusterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpGrandMasterClusterDataSetTableINDEX, 5, 0, 0, NULL},

{{14,FsPtpGrandMasterClusterAddr}, GetNextIndexFsPtpGrandMasterClusterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPtpGrandMasterClusterDataSetTableINDEX, 5, 0, 0, NULL},

{{14,FsPtpGrandMasterClusterRowStatus}, GetNextIndexFsPtpGrandMasterClusterDataSetTable, FsPtpGrandMasterClusterRowStatusGet, FsPtpGrandMasterClusterRowStatusSet, FsPtpGrandMasterClusterRowStatusTest, FsPtpGrandMasterClusterDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpGrandMasterClusterDataSetTableINDEX, 5, 0, 1, NULL},

{{14,FsPtpUnicastMasterNetworkProtocol}, GetNextIndexFsPtpUnicastMasterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpUnicastMasterDataSetTableINDEX, 6, 0, 0, NULL},

{{14,FsPtpUnicastMasterAddLength}, GetNextIndexFsPtpUnicastMasterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpUnicastMasterDataSetTableINDEX, 6, 0, 0, NULL},

{{14,FsPtpUnicastMasterAddr}, GetNextIndexFsPtpUnicastMasterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPtpUnicastMasterDataSetTableINDEX, 6, 0, 0, NULL},

{{14,FsPtpUnicastMasterRowStatus}, GetNextIndexFsPtpUnicastMasterDataSetTable, FsPtpUnicastMasterRowStatusGet, FsPtpUnicastMasterRowStatusSet, FsPtpUnicastMasterRowStatusTest, FsPtpUnicastMasterDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpUnicastMasterDataSetTableINDEX, 6, 0, 1, NULL},

{{14,FsPtpAccMasterNetworkProtocol}, GetNextIndexFsPtpAccMasterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpAccMasterDataSetTableINDEX, 5, 0, 0, NULL},

{{14,FsPtpAccMasterAddLength}, GetNextIndexFsPtpAccMasterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpAccMasterDataSetTableINDEX, 5, 0, 0, NULL},

{{14,FsPtpAccMasterAddr}, GetNextIndexFsPtpAccMasterDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsPtpAccMasterDataSetTableINDEX, 5, 0, 0, NULL},

{{14,FsPtpAccMasterAlternatePriority}, GetNextIndexFsPtpAccMasterDataSetTable, FsPtpAccMasterAlternatePriorityGet, FsPtpAccMasterAlternatePrioritySet, FsPtpAccMasterAlternatePriorityTest, FsPtpAccMasterDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpAccMasterDataSetTableINDEX, 5, 0, 0, NULL},

{{14,FsPtpAccMasterRowStatus}, GetNextIndexFsPtpAccMasterDataSetTable, FsPtpAccMasterRowStatusGet, FsPtpAccMasterRowStatusSet, FsPtpAccMasterRowStatusTest, FsPtpAccMasterDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpAccMasterDataSetTableINDEX, 5, 0, 1, NULL},

{{14,FsPtpSecKeyId}, GetNextIndexFsPtpSecKeyDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpSecKeyDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSecKeyAlgorithmId}, GetNextIndexFsPtpSecKeyDataSetTable, FsPtpSecKeyAlgorithmIdGet, FsPtpSecKeyAlgorithmIdSet, FsPtpSecKeyAlgorithmIdTest, FsPtpSecKeyDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpSecKeyDataSetTableINDEX, 3, 0, 0, "1"},

{{14,FsPtpSecKeyLength}, GetNextIndexFsPtpSecKeyDataSetTable, FsPtpSecKeyLengthGet, FsPtpSecKeyLengthSet, FsPtpSecKeyLengthTest, FsPtpSecKeyDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpSecKeyDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSecKey}, GetNextIndexFsPtpSecKeyDataSetTable, FsPtpSecKeyGet, FsPtpSecKeySet, FsPtpSecKeyTest, FsPtpSecKeyDataSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpSecKeyDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSecKeyStartTime}, GetNextIndexFsPtpSecKeyDataSetTable, FsPtpSecKeyStartTimeGet, FsPtpSecKeyStartTimeSet, FsPtpSecKeyStartTimeTest, FsPtpSecKeyDataSetTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsPtpSecKeyDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSecKeyExpirationTime}, GetNextIndexFsPtpSecKeyDataSetTable, FsPtpSecKeyExpirationTimeGet, FsPtpSecKeyExpirationTimeSet, FsPtpSecKeyExpirationTimeTest, FsPtpSecKeyDataSetTableDep, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READWRITE, FsPtpSecKeyDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSecKeyValid}, GetNextIndexFsPtpSecKeyDataSetTable, FsPtpSecKeyValidGet, FsPtpSecKeyValidSet, FsPtpSecKeyValidTest, FsPtpSecKeyDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpSecKeyDataSetTableINDEX, 3, 0, 0, "2"},

{{14,FsPtpSecKeyRowStatus}, GetNextIndexFsPtpSecKeyDataSetTable, FsPtpSecKeyRowStatusGet, FsPtpSecKeyRowStatusSet, FsPtpSecKeyRowStatusTest, FsPtpSecKeyDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpSecKeyDataSetTableINDEX, 3, 0, 1, NULL},

{{14,FsPtpSAId}, GetNextIndexFsPtpSADataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSASrcPortNumber}, GetNextIndexFsPtpSADataSetTable, FsPtpSASrcPortNumberGet, FsPtpSASrcPortNumberSet, FsPtpSASrcPortNumberTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSASrcAddrLength}, GetNextIndexFsPtpSADataSetTable, FsPtpSASrcAddrLengthGet, FsPtpSASrcAddrLengthSet, FsPtpSASrcAddrLengthTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSASrcAddr}, GetNextIndexFsPtpSADataSetTable, FsPtpSASrcAddrGet, FsPtpSASrcAddrSet, FsPtpSASrcAddrTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSADstPortNumber}, GetNextIndexFsPtpSADataSetTable, FsPtpSADstPortNumberGet, FsPtpSADstPortNumberSet, FsPtpSADstPortNumberTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSADstAddrLength}, GetNextIndexFsPtpSADataSetTable, FsPtpSADstAddrLengthGet, FsPtpSADstAddrLengthSet, FsPtpSADstAddrLengthTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSADstAddr}, GetNextIndexFsPtpSADataSetTable, FsPtpSADstAddrGet, FsPtpSADstAddrSet, FsPtpSADstAddrTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSASrcClockIdentity}, GetNextIndexFsPtpSADataSetTable, FsPtpSASrcClockIdentityGet, FsPtpSASrcClockIdentitySet, FsPtpSASrcClockIdentityTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSADstClockIdentity}, GetNextIndexFsPtpSADataSetTable, FsPtpSADstClockIdentityGet, FsPtpSADstClockIdentitySet, FsPtpSADstClockIdentityTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSAReplayCounter}, GetNextIndexFsPtpSADataSetTable, FsPtpSAReplayCounterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSALifeTimeId}, GetNextIndexFsPtpSADataSetTable, FsPtpSALifeTimeIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSAKeyId}, GetNextIndexFsPtpSADataSetTable, FsPtpSAKeyIdGet, FsPtpSAKeyIdSet, FsPtpSAKeyIdTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSANextLifeTimeId}, GetNextIndexFsPtpSADataSetTable, FsPtpSANextLifeTimeIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSANextKeyId}, GetNextIndexFsPtpSADataSetTable, FsPtpSANextKeyIdGet, FsPtpSANextKeyIdSet, FsPtpSANextKeyIdTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSATrustState}, GetNextIndexFsPtpSADataSetTable, FsPtpSATrustStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSATrustTimer}, GetNextIndexFsPtpSADataSetTable, FsPtpSATrustTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSATrustTimeout}, GetNextIndexFsPtpSADataSetTable, FsPtpSATrustTimeoutGet, FsPtpSATrustTimeoutSet, FsPtpSATrustTimeoutTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSAChallengeState}, GetNextIndexFsPtpSADataSetTable, FsPtpSAChallengeStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSAChallengeTimer}, GetNextIndexFsPtpSADataSetTable, FsPtpSAChallengeTimerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSAChallengeTimeOut}, GetNextIndexFsPtpSADataSetTable, FsPtpSAChallengeTimeOutGet, FsPtpSAChallengeTimeOutSet, FsPtpSAChallengeTimeOutTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSARequestNonce}, GetNextIndexFsPtpSADataSetTable, FsPtpSARequestNonceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSAResponseNonce}, GetNextIndexFsPtpSADataSetTable, FsPtpSAResponseNonceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSAChallengeRequired}, GetNextIndexFsPtpSADataSetTable, FsPtpSAChallengeRequiredGet, FsPtpSAChallengeRequiredSet, FsPtpSAChallengeRequiredTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSAResponseRequired}, GetNextIndexFsPtpSADataSetTable, FsPtpSAResponseRequiredGet, FsPtpSAResponseRequiredSet, FsPtpSAResponseRequiredTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSATypeField}, GetNextIndexFsPtpSADataSetTable, FsPtpSATypeFieldGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSADirection}, GetNextIndexFsPtpSADataSetTable, FsPtpSADirectionGet, FsPtpSADirectionSet, FsPtpSADirectionTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpSARowStatus}, GetNextIndexFsPtpSADataSetTable, FsPtpSARowStatusGet, FsPtpSARowStatusSet, FsPtpSARowStatusTest, FsPtpSADataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpSADataSetTableINDEX, 3, 0, 1, NULL},

{{14,FsPtpAltTimeScaleKeyId}, GetNextIndexFsPtpAltTimeScaleDataSetTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPtpAltTimeScaleDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpAltTimeScalecurrentOffset}, GetNextIndexFsPtpAltTimeScaleDataSetTable, FsPtpAltTimeScalecurrentOffsetGet, FsPtpAltTimeScalecurrentOffsetSet, FsPtpAltTimeScalecurrentOffsetTest, FsPtpAltTimeScaleDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpAltTimeScaleDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpAltTimeScalejumpSeconds}, GetNextIndexFsPtpAltTimeScaleDataSetTable, FsPtpAltTimeScalejumpSecondsGet, FsPtpAltTimeScalejumpSecondsSet, FsPtpAltTimeScalejumpSecondsTest, FsPtpAltTimeScaleDataSetTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPtpAltTimeScaleDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpAltTimeScaletimeOfNextJump}, GetNextIndexFsPtpAltTimeScaleDataSetTable, FsPtpAltTimeScaletimeOfNextJumpGet, FsPtpAltTimeScaletimeOfNextJumpSet, FsPtpAltTimeScaletimeOfNextJumpTest, FsPtpAltTimeScaleDataSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpAltTimeScaleDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpAltTimeScaledisplayName}, GetNextIndexFsPtpAltTimeScaleDataSetTable, FsPtpAltTimeScaledisplayNameGet, FsPtpAltTimeScaledisplayNameSet, FsPtpAltTimeScaledisplayNameTest, FsPtpAltTimeScaleDataSetTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPtpAltTimeScaleDataSetTableINDEX, 3, 0, 0, NULL},

{{14,FsPtpAltTimeScaleRowStatus}, GetNextIndexFsPtpAltTimeScaleDataSetTable, FsPtpAltTimeScaleRowStatusGet, FsPtpAltTimeScaleRowStatusSet, FsPtpAltTimeScaleRowStatusTest, FsPtpAltTimeScaleDataSetTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPtpAltTimeScaleDataSetTableINDEX, 3, 0, 1, NULL},

{{11,FsPtpTrapContextName}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsPtpTrapDomainNumber}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsPtpGlobalErrTrapType}, NULL, FsPtpGlobalErrTrapTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsPtpNotification}, NULL, FsPtpNotificationGet, FsPtpNotificationSet, FsPtpNotificationTest, FsPtpNotificationDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fsptpEntry = { 165, fsptpMibEntry };

#endif /* _FSPTPDB_H */

