/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpconst.h,v 1.15 2014/03/15 14:18:20 siva Exp $
 *
 * Description: This file contains constant definitions used in PTP Module.
 *
 *****************************************************************************/
#ifndef _PTP_CONST_H
#define _PTP_CONST_H

#define PTP_ALWAYS                 1

#define PTP_TASK                   ((UINT1 *)"PTP")
#define PTP_PDU_QUEUE_NAME         ((UINT1 *)"PTPQ")
#define PTP_MSG_QUEUE_NAME         ((UINT1 *)"PTMQ")
#define PTP_SEM_NAME               ((UINT1 *)"PTPS")

#define PTP_TMR_EXP_EVENT                      0x0001
#define PTP_CONF_MSG_ENQ_EVENT                 0x0002
#define PTP_IPv4_EVENTSOCK_PKT_ARRIVAL_EVENT   0x0004
#define PTP_IPv4_GENERALSOCK_PKT_ARRIVAL_EVENT 0x0008
#define PTP_IPv6_EVENTSOCK_PKT_ARRIVAL_EVENT   0x0010
#define PTP_IPv6_GENERALSOCK_PKT_ARRIVAL_EVENT 0x0020
#define PTP_RM_MSG_EVENT                       0x0040

#define PTP_PDU_Q_DEPTH            PTP_MAX_PORTS 

#define PTP_MSG_LEN_SIZE                           2
#define PTP_DOMAIN_LEN_SIZE                        1
/* Hash Table size */
#define PTP_HASH_TABLE_SIZE                       20

/* Offset Values */
#define PTP_HDR_SIZE                              34
#define PTP_MSG_TYPE_OFFSET                        0
#define PTP_MSG_VERSION_OFFSET                     1
#define PTP_MSG_LEN_OFFSET                         2
#define PTP_MSG_DOMAIN_OFFSET                      4
#define PTP_MSG_FLAGS_OFFSET                       6
#define PTP_MSG_CORRECTION_FIELD_OFFSET            8
#define PTP_MSG_CORRECTION_FIELD_LOWER_BYTE_OFFSET 4
#define PTP_MSG_PORTID_OFFSET                     20
#define PTP_SEQ_OFFSET                            30
#define PTP_CONTROL_FIELD_OFFSET                  32
#define PTP_LOG_MSG_INTERVAL_OFFSET               33
#define PTP_SRC_PORT_ID_OFFSET                    20
#define PTP_REQUESTING_PORT_ID_OFFSET             44 

/* Announce Message Offset - Includes header too*/
#define PTP_MSG_ANNC_GM_ID_OFFSET          53
#define PTP_MSG_ANNC_STEPS_REMOVED_OFFSET  61
#define PTP_MSG_ANNC_UTC_OFFSET            44
#define PTP_MSG_ANNC_GM_PRIO1_OFFSET       47
#define PTP_MSG_ANNC_CLASS_OFFSET          48
#define PTP_MSG_ANNC_ACCURACY_OFFSET       49
#define PTP_MSG_ANNC_VARIANCE_OFFSET       50
#define PTP_MSG_ANNC_GM_PRIO2_OFFSET       52
#define PTP_MSG_ANNC_TIME_SOURCE_OFFSET    63

/* Sync Message Offset - Includes header too */
#define PTP_SYNC_TIME_STAMP_OFFSET         34

/* Peer Delay Offset - Includes header too */
#define PTP_PDELAY_RESP_TIME_STAMP_OFFSET   34 

#define PTP_MSG_TYPE_MASK                  0x0F
#define PTP_TRANS_SPE_MASK                 0xF0
#define PTP_VERSION_MASK                   0x0F

#define PTP_FLAG_ALT_MST_MASK              0x0001
#define PTP_FLAG_TWO_STEP_MASK             0x0002
#define PTP_FLAG_LEAP61_MASK               0x0100
#define PTP_FLAG_LEAP59_MASK               0x0200
#define PTP_FLAG_UTC_OFFSET_MASK           0x0400
#define PTP_FLAG_PTP_TIMESCALE_MASK        0x0800
#define PTP_FLAG_TIME_TRC_MASK             0x1000
#define PTP_FLAG_FREQ_TRC_MASK             0x2000

/*PTP Port List size macro has been increased from 16 to 20 bytes 
 * to address coverity warnings */
#define PTP_PORT_LIST_SIZE       ((EXT_PTP_MAX_PORTS + 31)/32 * 4)

#define PTP_MAX_CLOCK_ID_LEN                  8
#define PTP_CLOCK_ID_STR_LEN                  25
#define PTP_MAX_ADDR_LENGTH                  16
#define PTP_MAX_SEC_KEY_LENGTH               20
#define PTP_UN_MAX_NGE_MSG_TYPE               4
#define PTP_ALT_TIME_NAME_MAX_LEN            10
#define PTP_CLI_MAX_ALIAS_LEN                64

#define PTP_PTRC_LINK_MTU                    1500

/* Masks used in the flags transmitted in header info */
#define PTP_TWO_STEP_CLK_MASK               0x02

#define PTP_TRANS_SPC_BITS_SHIFT            4
#define PTP_UUID_OFFSET_IN_CLK_ID           2
#define PTP_BMC_MAX_STEP_REMOVED            255
#define PTP_ALTS_NAME_PAD_CONST_DIV         2   
#define PTP_PORT_NON_RECOM_ANN_INTERVAL     2   

/* PTP Data Set Comparison Algorithm Constants */
#define PTP_GM_CLK_ID          1
#define PTP_GM_PRIO_1          2
#define PTP_GM_CLASS          3
#define PTP_GM_ACCURACY          4
#define PTP_GM_VARIANCE          5
#define PTP_GM_PRIO_2          6
#define PTP_STEPS_REMOVED         7
#define PTP_RCV_SEND_IDENTITIES         8
#define PTP_B_BETTER                           9
#define PTP_A_BETTER                           10

/* BMC Algorithm Outputs */
#define PTP_GREATER          1
#define PTP_GREATER_BY_TOPOLOGY         2
#define PTP_EQUAL          0
#define PTP_LESSER         -1
#define PTP_LESSER_BY_TOPOLOGY        -2
#define PTP_DS_CMP_ALGO_ERROR1         3
#define PTP_DS_CMP_ALGO_ERROR2         4
#define PTP_DS_BETTER_RECEIVER         5
#define PTP_DS_BETTER_SENDER         6
#define PTP_DS_EQUAL_SENDER_RECEIVER        7

#define PTP_DEF_CLOCK_NUMBER_PORTS             1

#define PTP_CLK_CLASS_1          1
#define PTP_CLK_CLASS_6          6
#define PTP_CLK_CLASS_7          7
#define PTP_CLK_CLASS_52        52
#define PTP_CLK_CLASS_127       127 
#define PTP_SLAVEONLY_CLK_CLASS 255
#define PTP_INVALID_IF_INDEX               0xFFFF

#define PTP_ANNC_MSG_SIZE                64
#define PTP_SYNC_MSG_SIZE                     44
#define PTP_DELAY_REQ_MSG_LEN                 44
#define PTP_MSG_DELAY_RESP_LEN                54
#define PTP_PEER_DELAY_REQ_MSG_LEN            54
#define PTP_RM_MSG_TYPE                       55

#define PTP_SECONDS_LEN                        6
#define PTP_NANO_SECONDS_LEN                   4

#define PTP_MEAN_LINK_DELAY_VALUE              2
/* Correction field factor */
#define PTP_CORRECTION_FIELD_FACTOR            65536
/* This value refers to the transport specific option and is mostly ZERO
 * for all the underlying protocols except for
 * UDP IPv4 interface - 0. Some version1 supported hardwares use this 
 *                         value to decode time stamping differently.
 * Ethernet Interface - Can take value as 1 when support AVB (Audio Video
 *                      Bridging)
 * */                     
#define PTP_TRANSPORT_SPECIFIC_VALUE           0

/* Control field enumeration 
 * Used for message transmission only */
#define PTP_CONTROL_FIELD_SYNC   0x0
#define PTP_CONTROL_FIELD_DELAY_REQ  0x1
#define PTP_CONTROL_FIELD_FOLLOW_UP  0x2
#define PTP_CONTROL_FIELD_DELAY_RESP  0x3
#define PTP_CONTROL_FIELD_MGMT   0X4
#define PTP_CONTROL_FIELD_OTHERS  0X5

#define PTP_SRC_PORT_ID_LEN                      10 

/* Time Source Enumeration. Refer Table 7 of IEEE Std 1588-2008 */
#define PTP_INTERNAL_OSCILLATOR                0xA0  

/* Context Type */
#define PTP_L2_CONTEXT                            1

#define PTP_SHIFT_1BIT              1
#define PTP_MSB_FLAG                0x80
#define PTP_COMN_HED_MSG_LEN_OFFSET 2
/* This is for the local useage No need to change */
#define PTP_TMP_ALT_TIME_PER_CONTEXT     1   
#define PTP_MAX_ALT_TIME_TLV_SIZE        30 

#define  PTP_SHIFT_1BIT              1
#define  PTP_MSB_FLAG                0x80
#define  PTP_COMN_HED_MSG_LEN_OFFSET 2

#define PTP_TLV_TYPE_FIELD_OFFSET   0
#define PTP_TLV_LEN_FIELD_OFFSET    2
#define PTP_TLV_DATA_FIELD_OFFSET   4

#define PTP_ANNC_HDR_TLV_OFFSET     64
#define PTP_SIG_HDR_TLV_OFFSET      44

#define PTP_MAX_AUTH_TLV_SIZE       30

/* 1byte key Id + 4byte CurrentOffset +
 * 4 byte jumbsecond + 6 byte NextJumpSecond fields  + 1 (Name Len)*/
#define  PTP_ALT_TIME_NON_VAR_LEN  (1 + 4 + 4 + 6 + 1)


#define PTP_AM_UPDATE_MST_COUNT         0x01
#define PTP_AM_UPDATE_MUL_SYNC          0x02
#define PTP_AM_UPDATE_MUL_SYNC_INTERVAL 0x04


#define PTP_DEF_CONTEXT 0

#define PTP_IP_HEADER_LEN 24
#define PTP_IP6_MSG_HEADER_LEN  112
/* COMP */
/*#define PTP_V1_SYNC_RETRY_COUNT         3*/
#define PTP_V1_SYNC_RETRY_COUNT         0
/* This time is specified in the std to increment v1's SyncInterval for 
 * V1 to V2 Conversion of Delay Request and Delay Response */
#define PTP_COMP_TRNS_INCR_TIME         5
#define PTP_THREE_BYTE                  3

#define PTP_MESSAGE_VERSION_ONE         1
#define PTP_MESSAGE_VERSION_TWO         2
#define PTP_V1_MSG_NETWORK_VAL          1

#define PTP_V1_CONTROL_FIELD_OFFSET     32

#define PTP_V1_MSG_TYPE_EVENT_MSG    0x01
#define PTP_V1_MSG_TYPE_GENERAL_MSG  0x02

#define PTP_V1_SYNC_MESSAGE             0x00
#define PTP_V1_DELAY_REQ_MESSAGE        0x01
#define PTP_V1_FOLLOWUP_MESSAGE         0x02
#define PTP_V1_DELAY_RESP_MESSAGE       0x03
#define PTP_V1_MANAGEMENT_MESSAGE       0x04
#define PTP_V1_SYNC_MESSAGE_BURST       0x05
#define PTP_V1_DELAY_REQ_MESSAGE_BURST  0x06

#define PTP_V1_TO_V2_MAX_MSG_LEN    64

#define PTP_V2_ANNC_MSG_LEN         64
#define PTP_V2_SYNC_MSG_LEN         44
#define PTP_V2_DELAY_REQ_MSG_LEN    44
#define PTP_V2_FOLLOW_UP_MSG_LEN    44
#define PTP_V2_DELAY_RES_MSG_LEN    54

#define PTP_V2_TO_V1_MAX_MSG_LEN    1500

#define PTP_V1_SYNC_MSG_LEN         138
#define PTP_V1_DELAY_REQ_MSG_LEN    138
#define PTP_V1_FOLLOW_UP_MSG_LEN    66
#define PTP_V1_DELAY_RES_MSG_LEN    74

#define PTP_UUID_LENGTH                     6
#define PTP_CODE_STRING_LENGTH              4
#define PTP_SUBDOMAIN_NAME_LENGTH           16
#define PTP_V1_FLAG_FIELD_LEN               2
#define PTP_V1_CLK_ID_LEN                   4

#define PTP_V2_FLAG_FIELD_LEN               2

#define PTP_V1_COMM_SYNC_HEADER_PAD         4
#define PTP_V1_COMM_D_RES_HEADER_PAD        4
#define PTP_V1_COMM_FOLLOW_UP_HEADER_PAD    6

/* spec defined constants  */
#define PTP_V1_CLK_ID_ATOM   "ATOM"
#define PTP_V1_CLK_ID_GPS    "GPS\0"
#define PTP_V1_CLK_ID_NTP    "NTP\0"
#define PTP_V1_CLK_ID_HAND   "HAND"
#define PTP_V1_CLK_ID_INIT   "INIT"
#define PTP_V1_CLK_ID_DFLT   "DFLT"

#define DEFAULT_PTP_DOMAIN_NAME      "_DFLT\0\0\0\0\0\0\0\0\0\0\0"
#define ALTERNATE_PTP_DOMAIN1_NAME   "_ALT1\0\0\0\0\0\0\0\0\0\0\0"
#define ALTERNATE_PTP_DOMAIN2_NAME   "_ALT2\0\0\0\0\0\0\0\0\0\0\0"
#define ALTERNATE_PTP_DOMAIN3_NAME   "_ALT3\0\0\0\0\0\0\0\0\0\0\0"

#define PTP_V1_FLAG_LI_61                0
#define PTP_V1_FLAG_LI_59                1
#define PTP_V1_FLAG_BOUNDARY_CLOCK       2
#define PTP_V1_FLAG_ASSIST               3
#define PTP_V1_FLAG_EXT_SYNC             4
#define PTP_V1_FLAG_PARENT_STATS         5
#define PTP_V1_FLAG_SYNC_BURST           6

/* Octet[0]*/
#define PTP_V2_FLAG_ALT_MST              0
#define PTP_V2_FLAG_TWO_STEP             1
#define PTP_V2_FLAG_UNICAST              2

/* Octet[1]*/
#define PTP_V2_FLAG_LEAP61               0
#define PTP_V2_FLAG_LEAP59               1
#define PTP_V2_FLAG_CURR_UTC_OFF_SET     2
#define PTP_V2_FLAG_PTP_TIMESCALE        3
#define PTP_V2_FLAG_TIME_TRACEABLE       4
#define PTP_V2_FLAG_FREQ_TRACEABLE       5

#define PTP_TIME_SRC_ATOMIC_CLOCK         0x10
#define PTP_TIME_SRC_GPS                  0x20
#define PTP_TIME_SRC_TERRESTRIAL_RADIO    0x30
#define PTP_TIME_SRC_PTP                  0x40
#define PTP_TIME_SRC_NTP                  0x50
#define PTP_TIME_SRC_HAND_SET             0x60
#define PTP_TIME_SRC_OTHER                0x90
#define PTP_TIME_SRC_INTERNAL_OSCILLATOR  0xA0
#define PTP_TIME_SRC_ARB                          1

#define PTP_V2_DEFAULT_DOMAIN_ID           0
#define PTP_V2_ALTERNATE_DOMAIN1_ID        1
#define PTP_V2_ALTERNATE_DOMAIN2_ID        2
#define PTP_V2_ALTERNATE_DOMAIN3_ID        3

#define PTP_V1_STRATUM_VAL_0               0
#define PTP_V1_STRATUM_VAL_1               1
#define PTP_V1_STRATUM_VAL_2               2
#define PTP_V1_STRATUM_VAL_3               3
#define PTP_V1_STRATUM_VAL_4               4
#define PTP_V1_STRATUM_VAL_255             255


#define PTP_V2_CLK_CLASS_VAL_6             6  
#define PTP_V2_CLK_CLASS_VAL_7             7  
#define PTP_V2_CLK_CLASS_VAL_9             9 
#define PTP_V2_CLK_CLASS_VAL_10            10 
#define PTP_V2_CLK_CLASS_VAL_12            12 
#define PTP_V2_CLK_CLASS_VAL_248           248 
#define PTP_V2_CLK_CLASS_VAL_249           249 
#define PTP_V2_CLK_CLASS_VAL_251           251 
#define PTP_V2_CLK_CLASS_VAL_255           255 


#define PTP_V2_PRIORITY1_VAL_127           127 
#define PTP_V2_PRIORITY1_VAL_128           128 

#define PTP_V2_PRIORITY2_VAL_127           127 
#define PTP_V2_PRIORITY2_VAL_128           128

#define PTP_V1_GM_IS_PREFERRED_FALSE       0
#define PTP_V1_GM_IS_PREFERRED_TRUE        1

#define PTP_V1_IS_BOUNDARY_CLK_FALSE       0
#define PTP_V1_IS_BOUNDARY_CLK_TRUE        1

#define PTP_V2_CLK_ACCURACY_HEX_VAL_19     0x19
#define PTP_V2_CLK_ACCURACY_HEX_VAL_22     0x22
#define PTP_V2_CLK_ACCURACY_HEX_VAL_23     0x23
#define PTP_V2_CLK_ACCURACY_HEX_VAL_2F     0x2F
#define PTP_V2_CLK_ACCURACY_HEX_VAL_30     0x30
#define PTP_V2_CLK_ACCURACY_HEX_VAL_FD     0xFD
#define PTP_V2_CLK_ACCURACY_HEX_VAL_FE     0xFE

#define PTP_CLK_DEF_PHASE_CHANGE_RATE    2147483647
#define PTP_CLK_DEF_LOG_VARIANCE    65535    

#define PTP_CLK_ID_MAP_OCTET_THREE         0xFF
#define PTP_CLK_ID_MAP_OCTET_FOUR          0xFE
#define PTP_FIFTH_BYTE                     5


/* Size of UINT8 string
 * Maximum u8 value = 18446744073709551615
 * strlen ("18446744073709551615") = 20
 * So the max string length should be 21
 */
#define PTP_U8_STR_LEN                  21

#define PTP_V1_COMMUNICATION_TECHNOLOGY    1

#define PTP_V1TOV2_CLK_ID_MAP_OCTET_ONE   0xFF
#define PTP_V1TOV2_CLK_ID_MAP_OCTET_TWO   0xFE
/* Macro for announce interval */
#define PTP_PORT_MIN_ANNOUNCE_INTERVAL  0
#define PTP_PORT_MAX_ANNOUNCE_INTERVAL  4
#define PTP_PORT_DEF_ANNOUNCE_INTERVAL  1

/* Macro for announce reciept interval */
#define PTP_PORT_MIN_ANNOUNCE_RECEIPT_TIMEOUT  2
#define PTP_PORT_MAX_ANNOUNCE_RECEIPT_TIMEOUT  10
#define PTP_PORT_DEF_ANNOUNCE_RECEIPT_TIMEOUT  3

/* Macro for Port sync interval */
#define PTP_PORT_MIN_SYNC_TIMEOUT  -1
#define PTP_PORT_MAX_SYNC_TIMEOUT   1
#define PTP_PORT_DEF_SYNC_TIMEOUT   0

/* Macros for interface number */
#define PTP_PORT_MIN_IFACE_NUM      0
#define PTP_PORT_MAX_IFACE_NUM      65535

/* Macro for port delay mechanism */
#define PTP_PORT_DELAY_MECH_END_TO_END    1
#define PTP_PORT_DELAY_MECH_PEER_TO_PEER  2
#define PTP_PORT_DELAY_MECH_DISABLED      255

/* Macro for Port peer delay request interval */
#define PTP_PORT_MIN_PEER_DELAY_REQ_INT   0
#define PTP_PORT_MAX_PEER_DELAY_REQ_INT   5
#define PTP_PORT_DEF_PEER_DELAY_REQ_INT   0

/* Macro for Port minimum delay request interval */
#define PTP_PORT_MIN_DELAY_REQ_INT   0
#define PTP_PORT_MAX_DELAY_REQ_INT   5
#define PTP_PORT_DEF_DELAY_REQ_INT  (PTP_PORT_DEF_SYNC_TIMEOUT + PTP_DELAY_SYNC_DIFFERENCE)

/* Macros for unicast master max size */
#define PTP_PORT_MIN_MASTER_SIZE      0
#define PTP_PORT_MAX_MASTER_SIZE      65535
#define PTP_PORT_DEF_MASTER_SIZE      0

/* Macro for number of alternate master through port */
#define PTP_PORT_MIN_ALT_MASTER   0
#define PTP_PORT_MAX_ALT_MASTER   255
#define PTP_PORT_DEF_ALT_MASTER   0

/* Macro for UM log query interval */
#define PTP_PORT_MIN_UM_QUERY_INT   1
#define PTP_PORT_MAX_UM_QUERY_INT   255
#define PTP_PORT_DEF_UM_QUERY_INT   1

/* Macro for announce, sync, delay response and peer delay response message */
#define PTP_PORT_MIN_INTER_MSG_PERIOD   1
#define PTP_PORT_MAX_INTER_MSG_PERIOD   255
#define PTP_PORT_DEF_INTER_MSG_PERIOD   1

/* Macro for number of alternate master through port */
#define PTP_PORT_MIN_ALT_MULT_SYNC_INT   0
#define PTP_PORT_MAX_ALT_MULT_SYNC_INT   255

/* Macros for unicast, acceptable and grand masters */
#define PTP_MIN_NW_PROTOCOL         0
#define PTP_MAX_NW_PROTOCOL         255
#define PTP_MASTER_MIN_ADDR_LEN     1
#define PTP_MASTER_MAX_ADDR_LEN     24

/* Macro for security key */
#define PTP_SEC_MIN_KEY_ID          0
#define PTP_SEC_MAX_KEY_ID          255

/* Macro for security association */
#define PTP_SA_MAX_ID          255
#define PTP_DEF_SA             128
#define PTP_SA_IN              0
#define PTP_SA_OUT             1

/* Macro for alternate time scale */
#define PTP_ALT_TIME_DISPLAY_MIN_LEN     1
#define PTP_ALT_TIME_MIN_KEY_ID          0
#define PTP_ALT_TIME_MAX_KEY_ID          254
#define PTP_ALT_TIME_DEF_NAME            ("NTP")

/* Macro for acceptable master table */
#define PTP_ACMST_DEF_ALT_PRI1           0

/* Macros to identify the messages to be requested or granted */
#define PTP_ANNOUNCE_MSG_TYPE                           1
#define PTP_SYNC_MSG_TYPE                               2
#define PTP_ANNOUNCE_SYNC_MSG_TYPE                      3
#define PTP_DELAY_RESP_MSG_TYPE                         4
#define PTP_ANNOUNCE_SYNC_DELAY_RESP_MSG_TYPE           7
#define PTP_PEER_DELAY_RESP_MSG_TYPE                    8
#define PTP_ANNOUNCE_SYNC_DELAY_PEER_DELAYRESP_MSG_TYPE 15

/* Macros for trace objects */
#define PTP_MAX_TRC_STR_COUNT            9
#define PTP_MAX_TRC_STR_LEN              15

/* Macro to identify the structure to be used */
#define PTP_DOMAIN_DATA_SET              1
#define PTP_PORT_CONFIG_RBTREE_DATA_SET  2
#define PTP_FOREIGN_MASTER_DATA_SET      3
#define PTP_TRANS_PORT_DATA_SET          4
#define PTP_UNICAST_MASTER_DATA_SET      5
#define PTP_ACC_MASTER_DATA_SET          6
#define PTP_SEC_KEY_DATA_SET             7
#define PTP_SA_DATA_SET                  8
#define PTP_ALT_TIME_DATA_SET            9
#define PTP_GRAND_MASTER_DATA_SET        10
#define PTP_COMP_DB_DATA_SET             11
#define PTP_PORT_CONFIG_HASH_DATA_SET    12
#define PTP_CONTEXT_DATA_SET             13

/* Macros for system control */
#define PTP_START                        1
#define PTP_SHUTDOWN                     2


/* Macros for clock priority */
#define PTP_MIN_CLK_PRIORITY             0
#define PTP_MAX_CLK_PRIORITY             255
#define PTP_DEF_CLK_PRIORITY             128

/* PTP Constants */
#define PTP_VERSION_ONE                  1
#define PTP_VERSION_TWO                  2

/* Macro for security algorithm id */
#define PTP_PORT_MIN_SEC_ALGO_ID         1
#define PTP_PORT_MAX_SEC_ALGO_ID         2

/* Clock time source */
#define ATOMIC_CLOCK                     16
#define GPS                              32
#define PTP                              64
#define NTP                              80
#define INTERNAL_OSCILLATOR              160

/* Macros for RB Tree comparison */
/* Should decide whether it can be moved to common header file */
#define RB_LESSER                       -1
#define RB_EQUAL                         0
#define RB_GREATER                       1

/* Macro for Message handler function pointer index */

#define  PTP_FP_ANNC_MSG_TYPE               0
#define  PTP_FP_SYNC_MSG_TYPE               1
#define  PTP_FP_DREQ_MSG_TYPE               2
#define  PTP_FP_DRESP_MSG_TYPE              3
#define  PTP_FP_PDREQ_MSG_TYPE              4
#define  PTP_FP_PEER_DELAY_RESP_MSG         5
#define  PTP_FP_PDRESP_MSG_TYPE             6
#define  PTP_FP_FOLLOWUP_MSG_TYPE           7

/* Macro for trace type */
#define  PTP_MAX_TRC_TYPE                   9
#define  PTP_TRC_MAX_SIZE                   255
#define  PTP_TRC_MIN_SIZE                   1

/* Macros for trace */
#define PTP_UTL_TRACE_TOKEN_DELIMITER       ' '    /* space */

#define PTP_UTL_TRACE_ENABLE                1
#define PTP_UTL_TRACE_DISABLE               2
#define PTP_UTL_MAX_TRC_LEN                 15

/* Macro for sync limmit */
#define PTP_DEF_SYNC_LIMIT                 "50000"

/* Min delay request interval should be <
 * PTP_DELAY_SYNC_DIFFERENCE + sync interval
 * Ref 7.7.2.4 of IEEE 1588
 */
#define PTP_DELAY_SYNC_DIFFERENCE           5

/* Length of the Time fields in PTP packet */
#define PTP_TIME_LENGTH                     10

/* Macros for context id */
#define PTP_MIN_CONTEXT_ID                  0
#define PTP_MAX_CONTEXT_ID                  255
#define PTP_INV_CONTEXT_ID                  PTP_MAX_CONTEXT_ID + 1

/* Traps */
#define PTP_MAX_TRAP                        0xFF
/* -------------------------------------------------------------------------*/
/*                     PTP TLV TYPES VALUES AND RANGE                       */
/* -------------------------------------------------------------------------*/
/* Standard TLVs */
#define PTP_TLV_TYPE_MGNT                0x0001
#define PTP_TLV_TYPE_MGNT_ERR_STATUS     0x0002
#define PTP_TLV_TYPE_ORG_EXTENSION       0x0003
/* Optional unicast message negotiation TLVs */
#define PTP_TLV_TYPE_REQ_UNI_TRAN        0x0004
#define PTP_TLV_TYPE_GRANT_UNI_TRAN      0x0005
#define PTP_TLV_TYPE_CANCEL_UNI_TRAN     0x0006
#define PTP_TLV_TYPE_ACK_CANCEL_UNI_TRAN 0x0007 
/* Optional path trace mechanism TLV */
#define PTP_TLV_TYPE_PATH_TRACE          0x0008 
/* Optional alternate timescale TLV */
#define PTP_TLV_TYPE_ALT_TIME_OFFSET_IND 0x0009 
/* Reserved for standard TLVs 000A to 1FFF  */
/* Experimental TLVs 14.2 */
/* Security TLVs Annex K */
#define PTP_TLV_TYPE_AUTH                0x2000 
#define PTP_TLV_TYPE_AUTH_CHALLENGE      0x2001 
#define PTP_TLV_TYPE_SEC_ASSO_UPDATE     0x2002 
/* Cumulative frequency scale factor offset Annex L */
#define PTP_TLV_TYPE_CUM_FRQ_SCL_FAC_OFF 0x2003
/* Reserved for Experimental TLVs 2004 to  3FFF */
/* Reserved 4000 to FFFF */


enum {
    PTP_INIT_EVENT = 0,
    
    PTP_POWERUP_EVENT = 1,
    
    PTP_DESG_ENABLED_EVENT = 2,
    
    PTP_DESG_DISABLED_EVENT = 3,
    
    PTP_FAULT_CLEARED_EVENT = 4,
    
    PTP_FAULT_DETECTED_EVENT = 5,
    
    PTP_STATE_DECISION_EVENT = 6,
    
    PTP_ANNC_RECPT_TIMEOUT_EVENT = 7,
    
    PTP_QUALIFY_TMR_EXP_EVENT = 8,
    
    PTP_BMC_MASTER_EVENT = 9,
    
    PTP_BMC_SLAVE_EVENT = 10,
    
    PTP_BMC_PASSIVE_EVENT = 11,
    
    PTP_MASTER_SELECTED_EVENT = 12,

    PTP_SYNC_FAULT_EVENT = 13,

    PTP_MAX_EVENTS = 14
};

enum {
    PTP_STATE_FAULTY = 0,
    
    PTP_STATE_DISABLED = 1,
    
    PTP_STATE_INITIALIZING = 2,
    
    PTP_STATE_LISTENING = 3,
    
    PTP_STATE_UNCALIBRATED = 4,
    
    PTP_STATE_SLAVE = 5,
    
    PTP_STATE_PREMASTER = 6,
    
    PTP_STATE_MASTER = 7,
    
    PTP_STATE_PASSIVE = 8,

    PTP_STATE_MAX_STATES = 9
};

/* Macros for clock priority */
#define PTP_MIN_CLK_PRIORITY         0
#define PTP_MAX_CLK_PRIORITY         255

/*PTP Domains */
#define PTP_DEFAULT_DOMAIN 0
#define PTP_ALTERNATE_DOMAIN_ONE 1
#define PTP_ALTERNATE_DOMAIN_TWO 2
#define PTP_ALTERNATE_DOMAIN_THREE 3
#define PTP_USER_DEFINED_DOMAIN_MIN 4
#define PTP_USER_DEFINED_DOMAIN_MAX 127

/*Context */
#define PTP_SWITCH_ALIAS_LEN          VCM_ALIAS_MAX_LEN

/* Time Conversion */
#define PTP_NANO_SEC_TO_SEC_CONV_FACTOR         1000000000
#define PTP_MILLI_SEC_TO_NANO_SEC_CONV_FACTOR   1000000
#define PTP_SEC_TO_MIILI_SEC_CONV_FACTOR        1000
#define PTP_LOG_BASE2_DIV_VAL                   2

/* Syntonization Constant */
#define PTP_SYTONIZE_CALCULATION_INTERVAL       10

/* Traps */
#define PTP_TRAP_INVALID                        0x0
#define PTP_TRAP_PORT_STATE_CHANGE              0x1
#define PTP_TRAP_GLOB_ERR                       0x2
#define PTP_TRAP_SYS_ADMIN_CHANGE               0x3
#define PTP_TRAP_SYS_CNTL_CHANGE                0x4
#define PTP_TRAP_UNICAST_STATUS_CHANGE          0x5
#define PTP_TRAP_PORT_ADMIN_CHANGE              0x6
#define PTP_TRAP_SYNC_FAULT                     0x7
#define PTP_TRAP_ACCEPT_MASTER_FAULT            0x8
#define PTP_TRAP_GRAND_MASTER_FAULT             0x9

/* Macros for Global error types */
#define PTP_GLOBAL_ERR_NONE                     0
#define PTP_GLOBAL_ERR_MEM_FAIL                 1
#define PTP_GLOBAL_ERR_BUF_FAIL                 2
#define PTP_GLOBAL_ERR_SYNC_FAULT               3
#define PTP_GLOBAL_ERR_ACC_MASTER_FAULT         4
#define PTP_GLOBAL_ERR_GM_FAULT                 5

/* Trap Octet[0] Mask values */
#define PTP_TRAP_MASK_GLOB_ERR                  0x01
#define PTP_TRAP_MASK_SYS_CTRL_CHNG             0x02
#define PTP_TRAP_MASK_SYS_ADMIN_CHNG            0x04
#define PTP_TRAP_MASK_PORT_STATE_CHNG           0x08
#define PTP_TRAP_MASK_PORT_ADMIN_CHNG           0x10
#define PTP_TRAP_MASK_SYNC_FAULT                0x20
#define PTP_TRAP_MASK_GM_FAULT                  0x40
#define PTP_TRAP_MASK_ACC_MASTER_FAULT          0x80

/* Trap Octet[1] Mask values */
#define PTP_TRAP_MASK_UCAST_ADMIN_CHNG          0x01

/* Length of the Ptp Trap Input */
#define PTP_TRAP_MAX_LEN                        2

/* Trap information string length */
#define PTP_MAX_TRAP_STR_LEN          32

/* TRAP Ids */
#define PTP_TRAP_PORT_ID 1
#define PTP_TRAP_DOMAIN_ID  2
#define PTP_TRAP_SWITCH_FAILURE_ID 3

/* Destination Multicast IP Addresses */
#define PTP_PEER_DELAY_MSG_IP          0xe000006b
#define PTP_MULTICAST_IP               0xe0000181
    
/* Set the TOS feilds of the DSCP value  range is (0-63)*/
#define  PTP_IPV4_DSCP_VAL             0x2E

#define PTP_ANNC_FN_PTR                0
#define PTP_SYNC_FN_PTR                1
#define PTP_DREQ_FN_PTR                2
#define PTP_DRES_FN_PTR                3
#define PTP_PDREQ_FN_PTR               4
#define PTP_PDRES_FN_PTR               5
#define PTP_SIG_FN_PTR                 6
#define PTP_MAX_MSG                    7

#define PTP_MIN_LOG_RANGE              -31
#define PTP_MAX_LOG_RANGE               31

#define PTP_UNICAST_LOG_MSG_INTERVAL    0x7F

#define PTP_MAX_TRC_ARGS                2

#ifndef OPER_STATE_MASK
#define  OPER_STATE_MASK  0x00000001 
#endif

#ifndef IFACE_DELETED_MASK
#define  IFACE_DELETED_MASK  0x00000020 
#endif

#define PTP_MAX_U4_VAL  0xFFFF

/* Hardware Time Stamp Options */
#define PTP_TS_APPEND                1
#define PTP_TS_INSERT                2

/* Clock id generation */
#define PTP_CLKID_FOURTH_BYTE_VAL 0xFF
#define PTP_CLKID_FIFTH_BYTE_VAL  0xFE

#define PTP_VAL_THREE             3
#define PTP_VAL_FOUR              4
#define PTP_VAL_FIVE              5
#define PTP_VAL_SIX               6
#define PTP_VAL_SEVEN             7

#define PTP_3_OCTETS              3

#define PTP_LLC_HDR_SIZE          14

#define PTP_POSITIVE               1
#define PTP_NEGATIVE              -1

#define PTP_DISPLAY_CONSTANT       65536

#define PTP_OFFSET_FROM_MASTER      1
#define PTP_MEAN_PATH_DELAY         2
#define PTP_PEER_MEAN_PATH_DELAY    3

#define PTP_SIGN_BIT_SET            0x80

/* Traps */
#define PTP_TRAP_CONTROL_MAX 0x08

#define PTP_ZERO_TWO_STEP_FLAG             0xFDFF

#define PTP_FOREIGN_MASTER_TIME_WINDOW    4
#define PTP_FOREIGN_MASTER_THRESHOLD      2

#define PTP_TIME_STR_LEN                  24

/* Invalid Context id in CLI */
#define PTP_CLI_INVALID_CONTEXT_ID        0xffffffff
#define G8261TS_MIN_LOAD 0
#define G8261TS_MAX_LOAD 100
#define G8261TS_MIN_NWDIST 0
#define G8261TS_MAX_NWDIST 100
#define G8261TS_MIN_DURATION 0
#define G8261TS_MAX_DURATION 1000
#define G8261TS_MIN_SWITCHCOUNT 1
#define G8261TS_MAX_SWITCHCOUNT 150
#define G8261TS_MIN_PKTSIZE 64
#define G8261TS_MAX_PKTSIZE 1024
#define G8261TS_MIN_FLOWINTERVAL 1
#define G8261TS_MAX_FLOWINTERVAL 60
#define G8261TS_MIN_FREQUENCY 1
#define G8261TS_MAX_FREQUENCY 1024
#define G8261TS_MAX_DIRECTION 2
#define G8261TS_NWDIST_FACTOR 100
#define G8261TS_PERCENT 100
#define G8261TS_LOAD_FACTOR 100
#define G8261TS_PKTSIZE_FACTOR 10
#define G8261TS_REVERSE_FACTOR 2
#define G8261TS_DECREASING_FLOW 2
#define G8261TS_INCREASING_FLOW 5
#define G8261TS_DEF_PKTSIZE 64
enum
{
    G8261TS_FORWARD = 1,
    G8261TS_REVERSE
};
enum
{
    G8261TS_CONSTANT_FLOW = 1,
    G8261TS_INCREMENT,
    G8261TS_DECREMENT
};

/* To support Fixed point arithmetic */
#define PTP_FIXEDPT_BITS 64 /* size of Fixed point numer */
#define PTP_FIXEDPT_WBITS 51 /* size of whole number part */
#define PTP_FIXEDPT_FBITS   (PTP_FIXEDPT_BITS - PTP_FIXEDPT_WBITS) /* size of fractional part */
#define PTP_FIXEDPT_FMASK   (((FP_UINT8)1 << PTP_FIXEDPT_FBITS) - 1) /* Mask used to get the fractional part */

#endif /*_PTP_CONST_H */
