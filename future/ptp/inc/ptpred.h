/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpred.h,v 1.2 2013/06/27 12:35:14 siva Exp $
 *
 * Description: This file contains the definitions and data structures
 *              and prototypes to support High Availability for PTP module.
 *
 ******************************************************************/
#ifndef _PTPRED_H_
#define _PTPRED_H_

typedef struct _tPtpRedGlobalInfo {
    UINT1       u1NodeStatus;          /* Node Status - Idle/Standby/Active */
    UINT1       u1NumOfStandbyNodesUp; /* Number of Standby nodes which are
                                          booted up.If this value is 0, then dynamic
                                          messages are not sent by active node*/

    UINT1         u1BulkUpdNextPort;
    UINT1         u1pad[1];
    tPtpPort      *pNextPtpPort;
}tPtpRedGlobalInfo;


#define PTP_RED_ONE 1
#define PTP_RED_BULK_REQUEST_MSG           RM_BULK_UPDT_REQ_MSG
#define PTP_RED_BULK_UPDATE_MSG            RM_BULK_UPDATE_MSG 
#define PTP_RED_BULK_UPD_TAIL_MESSAGE      RM_BULK_UPDT_TAIL_MSG
#define PTP_RED_BULK_REQ_SIZE              3
#define PTP_RED_RELAY_REQUEST_PKT_MSG      4
#define PTP_RED_L2_ACT_TO_STDBY_RELAY_PKT  5
#define PTP_RED_L2_STDBY_TO_ACT_RELAY_PKT  6
#define PTP_RED_RM_BULK_UPD_TAIL_MESSAGE   7
#define PTP_RED_PORTINFO_BULK_UPD_MSG      8
#define PTP_RED_REQUEST_PKT_SIZE           9
#define PTP_RED_IPv4_PTP_PKT               10
#define PTP_RED_IPv6_PTP_PKT               11
#define PTP_RED_VLAN_PTP_PKT               12
#define PTP_RED_PTP_PKT_LENGTH             13 
#define PTP_RED_IPvX_ACT_TO_STDBY_RELAY_PKT 14
#define PTP_RED_IPvX_STDBY_TO_ACT_RELAY_PKT 15
#define PTP_RED_L2_RELAY_PKT                16
#define PTP_RED_MSG_TYPE_SIZE               sizeof(UINT1)
#define PTP_RED_BULK_UPD_TYPE_SIZE          sizeof(UINT1)
#define PTP_RED_PORT_COUNT_TYPE_SIZE        sizeof(UINT1)
#define PTP_RED_PORT_INDEX_SIZE             sizeof(UINT4)
#define PTP_RED_PORT_DOMAIN_SIZE            sizeof(UINT1)
#define PTP_RED_PORT_IFINDEX_SIZE           sizeof(UINT4)
#define PTP_RED_MSG_LENGTH_SIZE             sizeof(UINT2)
#define PTP_RED_PTP_PKT_SIZE                sizeof(UINT2)
#define PTP_RED_PTP_ENCP_TYPE_SIZE          sizeof(UINT1)
#define PTP_RED_PTP_PROTOCOL_SIZE           sizeof(UINT4)
#define PTP_RED_PTP_CONTEXT_SIZE            sizeof(UINT4)
#define PTP_RED_PTP_DEVICE_TYPE_SIZE        sizeof(UINT4)
#define PTP_RED_PTP_VLAN_ID_SIZE            sizeof(UINT2)
#define PTP_RED_PTP_RESERVED_1_SIZE         1
#define PTP_RED_PTP_VERSION_SIZE            1
#define PTP_RED_PTP_RESERVED_4_SIZE         4  
#define PTP_RED_PTP_FLAG_SIZE               2
#define PTP_RED_PTP_CORRECTION_FILED_SIZE   8
#define PTP_RED_PTP_HALF_MAC_SIZE           3
#define PTP_RED_PTP_FOURTH_BYTE_SIZE        1

#define PTP_RED_BULK_UPD_TAIL_MSG_SIZE 3
#define PTP_RED_MAX_BULK_UPD_SIZE      1500
#define PTP_RED_PORT_INFO_MSG_LEN      ( PTP_RED_MSG_TYPE_SIZE  + \
                                         PTP_RED_MSG_LENGTH_SIZE + \
                                         PTP_RED_BULK_UPD_TYPE_SIZE + \
                                         PTP_RED_PORT_COUNT_TYPE_SIZE + \
                                         PTP_RED_PORT_INDEX_SIZE + \
                                         PTP_RED_PORT_DOMAIN_SIZE + \
                                         PTP_RED_PORT_IFINDEX_SIZE  \
                                        )
#define PTP_RED_MIN_MSG_LEN             3
#define PTP_RED_CLK_ID_STANDBY_FIFTH_VAL 0xFF
#define PTP_RED_CLK_ID_ACTIVE_FIFTH_VAL  0xFE
#define PTP_RED_CLK_ID_FIFTH_LOC        (PTP_LLC_HDR_SIZE + PTP_RED_MSG_TYPE_SIZE + \
                                         PTP_RED_PTP_VERSION_SIZE + \
                                         PTP_RED_MSG_LENGTH_SIZE + PTP_RED_PORT_DOMAIN_SIZE + \
                                         PTP_RED_PTP_RESERVED_1_SIZE + PTP_RED_PTP_FLAG_SIZE + \
                                         PTP_RED_PTP_CORRECTION_FILED_SIZE + PTP_RED_PTP_RESERVED_4_SIZE +\
                                         + PTP_RED_PTP_HALF_MAC_SIZE + PTP_RED_PTP_FOURTH_BYTE_SIZE )

#define PTP_RED_CLK_ID_FIFTH_LOC_CHECK  48
#define PTP_RED_CLK_ID_FIFTH_LOC_CHECK_AT_ACTIVE  62
#define PTP_RED_CLK_IDENTITY_REPLACE(pu1Pdu)    (*(pu1Pdu + PTP_RED_CLK_ID_FIFTH_LOC_CHECK) = \
                                                   PTP_RED_CLK_ID_ACTIVE_FIFTH_VAL)
#define PTP_RED_CHANGE_CLK_IDENTITY(pu1PtpPdu) (*(pu1PtpPdu + PTP_RED_CLK_ID_FIFTH_LOC) = \
                                                  PTP_RED_CLK_ID_STANDBY_FIFTH_VAL) 
#define PTP_RED_CLK_IDENTITY_CHECK(pu1Pdu)    (*(pu1Pdu + PTP_RED_CLK_ID_FIFTH_LOC_CHECK))
#define PTP_NODE_STATUS() gPtpRedGlobalInfo.u1NodeStatus

#define PTP_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
     RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
          *(pu4Offset) += 1;\
}while (0)
#define PTP_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
     RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
          *(pu4Offset) += 2;\
}while (0)
#define PTP_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
     RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
          *(pu4Offset) += 4;\
}while (0)

#define PTP_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
     RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
          *(pu4Offset) +=u4Size; \
}while (0)

#define PTP_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
     RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
          *(pu4Offset) += 1;\
}while (0)
#define PTP_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
     RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
          *(pu4Offset) += 2;\
}while (0)
#define PTP_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
     RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
          *(pu4Offset) += 4;\
}while (0)
#define PTP_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
     RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
          *(pu4Offset) += u4Size; \
}while (0)

PUBLIC VOID PtpRedHandleRmEvents (VOID);
PUBLIC INT4 PtpRedInit (VOID);
PUBLIC INT4 PtpRedDeInit (VOID);
PUBLIC INT4 PtpRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
PUBLIC VOID PtpRedHandleGoStandby (VOID);
PUBLIC VOID PtpRedHandleActiveToStandby (VOID);
PUBLIC VOID PtpRedHandleGoActive (VOID);
PUBLIC VOID PtpRedHandleIdleToActive (VOID);
PUBLIC VOID PtpRedHandleStandByToActive (VOID);
PUBLIC VOID PtpRedHandleIdleToStandby (VOID);
PUBLIC VOID PtpRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen);
PUBLIC VOID PtpRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen);
PUBLIC VOID PtpRedProcessRelayPktAtStandby (tRmMsg * pMsg, UINT4 *pu4OffSet);
PUBLIC VOID PtpRedProcessRelayPktAtActive (tRmMsg * pMsg, UINT4 *pu4OffSet);
PUBLIC VOID PtpRedRecvDynamicPortInfo  (tRmMsg * pRmMsg);
PUBLIC VOID PtpRedFormL2PtpPktForActive  (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                                    UINT4 u4PktLen, UINT2 u2Protocol,
                                    UINT1 u1EncapType,tRmMsg **pMsg);
PUBLIC VOID
PtpRedFormL2PtpPktForStandby (tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4IfIndex,
                              UINT4 u4ContextId, tVlanId VlanId, UINT4 u4PktLen,
                              tRmMsg **pMsg);
PUBLIC VOID
PtpRedFormVlanPtpPktForActive (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                               tVlanId VlanId, UINT4 u4PktLen ,tRmMsg **pMsg);
PUBLIC VOID
PtpRedFormIpvXPtpPktForActive (UINT1 *pu1PtpPdu, tPtpPort * pPtpPort,
                             UINT4 u4PktLen, UINT1 u1MsgType,UINT1 u1IpVersion, tRmMsg ** pMsg);

PUBLIC VOID
PtpRedFormIPvXPtpPktForStandby (tPtpPktParams * pPtpPktParams, tRmMsg ** pMsg);

PUBLIC VOID
PtpRedProcessAndRelayPkt (tRmMsg * pMsg, UINT2 u2DataLen);



#endif /* _PTPRED_H_ */
