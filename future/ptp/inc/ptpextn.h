/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 *$Id: ptpextn.h,v 1.4 2013/06/23 12:26:20 siva Exp $
 *
 * Description: This file contains all the header files to be
 *              included and used by PTP 
 *
 *******************************************************************/

#ifndef _PTPEXTN_H_
#define _PTPEXTN_H_

extern tPtpGlobalInfo      gPtpGlobalInfo;
#ifdef L2RED_WANTED
extern tPtpRedGlobalInfo       gPtpRedGlobalInfo;
#endif
extern tDelaySystem            gDelaySystem;
extern tDelayParams            gDelayParams[G8261TS_MAX_DIRECTION];
extern tDelayOutput            gDelayOutput;
extern UINT1               gau1PtpDestMacAddr[MAC_ADDR_LEN];
extern UINT1               gau1PtpP2PDestMacAddr[MAC_ADDR_LEN];
extern UINT1               gau1PtpTraceTypes[PTP_MAX_TRC_TYPE][PTP_UTL_MAX_TRC_LEN];
extern UINT1               gau1PtpMcastGroupAddr1 [IPVX_IPV4_ADDR_LEN];
extern UINT1               gau1PtpMcastGroupAddr2 [IPVX_IPV4_ADDR_LEN];
extern tFsModSizingParams  gaFsPtpSizingParams [];
extern tFsModSizingInfo    gFsPtpSizingInfo;

extern tIp6Addr            gV6PtpAddr;
extern tIp6Addr            gV6PtpP2PAddr;
#endif /* _PTPEXTN_H_ */
