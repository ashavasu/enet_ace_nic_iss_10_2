#ifndef _FSG826WR_H
#define _FSG826WR_H

VOID RegisterFSG826(VOID);

VOID UnRegisterFSG826(VOID);
INT4 FsG8261TsModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsG8261TsModuleStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsG8261TsParamsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsG8261TsSwitchCountGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsPacketSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsLoadGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsNetworkDisturbanceGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsDurationGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsFlowVariationFactorGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsFlowIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsFrequencyGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsSwitchCountSet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsPacketSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsLoadSet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsNetworkDisturbanceSet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsDurationSet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsFlowVariationFactorSet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsFlowIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsFrequencySet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsSwitchCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsG8261TsPacketSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsG8261TsLoadTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsG8261TsNetworkDisturbanceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsG8261TsDurationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsG8261TsFlowVariationFactorTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsG8261TsFlowIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsG8261TsFrequencyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsG8261TsParamsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsG8261TsMeanVarianceGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsAccuracyGet(tSnmpIndex *, tRetVal *);
INT4 FsG8261TsDelayGet(tSnmpIndex *, tRetVal *);
#endif /* _FSG826WR_H */
