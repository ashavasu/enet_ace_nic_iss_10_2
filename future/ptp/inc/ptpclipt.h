/**********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpclipt.h,v 1.3 2011/10/14 10:20:40 siva Exp $
 *
 * Description: This has prototype definitions for PTP CLI submodule
 *
 ***********************************************************************/
#ifndef __PTPCLIPT_H__
#define __PTPCLIPT_H__

/**********************************************************************
                        ptpcli.c
**********************************************************************/

PRIVATE INT4 
PtpCliSetSystemControl PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                               INT4 i4Status));
PRIVATE INT4
PtpCliSetTwoStepFlag PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                             INT4 i4DomainId
                             , INT4 i4TwoStepFlag));
PRIVATE INT4
PtpCliSetClkMode PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                         INT4 i4DomainId,
                         INT4 i4ClkMode));
PRIVATE INT4
PtpCliSetClkPriority1 PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                              INT4 i4DomainId, 
                              INT4 i4ClkPriority1));
PRIVATE INT4
PtpCliSetClkPriority2 PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                              INT4 i4DomainId,
                              INT4 i4ClkPriority2));
PRIVATE INT4
PtpCliSetClkSlaveOnly PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                              INT4 i4DomainId,
                              INT4 i4SlaveOnly));
PRIVATE INT4
PtpCliSetAnnounceInterval PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                                  INT4 i4DomainId,
                                  INT4 i4PortId,
                                  INT4 i4AnnounceInterval));
PRIVATE INT4
PtpCliSetAnnounceTimeOut PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                                 INT4 i4DomainIdi,
                                 INT4 i4PortId,
                                 INT4 i4AnnounceTimeOut));
PRIVATE INT4
PtpCliSetDelayReqInterval PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                                  INT4 i4DomainId,
                                  INT4 i4PortId,
                                  INT4 DelayReqInterval,
                                  UINT1 u1IsTransPort));
PRIVATE INT4
PtpCliSetSyncInterval PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                              INT4 i4DomainId,
                              INT4 i4PortId,
                              INT4 i4SyncInterval));
PRIVATE INT4
PtpCliSetDelayType PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                           INT4 i4DomainId,
                           INT4 i4PortId,
                           INT4 i4DelayType));
PRIVATE INT4
PtpCliSetVersion PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                         INT4 i4DomainId, INT4 i4PortId,     
                         INT4 i4Version));
PRIVATE INT4
PtpCliConfigSecurity PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                             INT4 i4DomainId,
                             INT4 i4SecStatus));
PRIVATE INT4
PtpCliConfigSetMaxSA PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                             INT4 i4DomainId,
                             INT4 i4MaxSA));
PRIVATE INT4
PtpCliConfigPathTrace PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                              INT4 i4DomainId,
                              INT4 i4Status));
PRIVATE INT4
PtpCliSetDebugs PROTO ((tCliHandle CliHandle, UINT1 *pu1TraceInput));
PRIVATE INT4
PtpCliSetCxtDebugs PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                           UINT1 *pu1TraceInput));
PRIVATE INT4
PtpCliConfigAccMaster PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                              INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen, 
                              CHR1 * u1AccMasterAddr));
PRIVATE INT4
PtpCliConfigMaxUnicastMasters PROTO((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DoaminId,
                                     INT4 i4MaxUcastMasters));
PRIVATE VOID
PtpCliShowClock PROTO ((tCliHandle CliHandle, INT4 i4Context, INT4 i4Domain));

PRIVATE VOID
PtpCliShowForeignMaster PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                                INT4 i4DomainId));

PRIVATE VOID
PtpCliShowGrandMasterRecords PROTO ((tCliHandle CliHandle, INT4 i4Context,
                                     INT4 i4Domain));


PRIVATE VOID
PtpCliShowPort PROTO ((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                       UINT4 u4IfIndex, INT4 i4IfType));

PRIVATE VOID
PtpCliShowTimeProperty PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                               INT4 i4DomainId));

PRIVATE VOID
PtpCliShowCurrentDS PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                            INT4 i4DomainId));

PUBLIC INT4
PtpCliShowDebugging PROTO ((tCliHandle CliHandle, UINT4 u4ContextId));

PRIVATE INT4
PtpCliShowContextInfo PROTO ((tCliHandle CliHandle, INT4 i4ContextId));

PRIVATE INT4
PtpCliShowGlobalInfo PROTO ((tCliHandle CliHandle));

PRIVATE VOID
PtpCliShowUnicastMasterRecords PROTO ((tCliHandle CliHandle,
                                       INT4 i4ContextId, INT4 i4DomainId));
PRIVATE VOID
PtpCliShowAcceptableMstRecords PROTO ((tCliHandle CliHandle,
                                       INT4 i4ContextId, INT4 i4DomainId));
PRIVATE VOID
PtpCliShowSecurityAssociations PROTO ((tCliHandle CliHandle,
                                       INT4 i4ContextId, INT4 i4DomainId));

PRIVATE INT4
PtpCliUpdateAltTimeScaleEntry PROTO((tCliHandle CliHandle, INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4AltTimeScaleKeyId, 
                               INT4 i4RowStatus));
PRIVATE INT4
PtpCliDelAltTimeScale PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                              INT4 i4DomainId, INT4 i4AltTimeScaleKeyId));
PRIVATE VOID
PtpCliShowAlternateTimeScale PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                                                             INT4 i4DomainId));

PRIVATE INT4
PtpCliSetClockNumberPorts PROTO ((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                                  INT4 i4NumberPorts));

PRIVATE INT4
PtpCliMapPortToDomain PROTO ((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                              INT4 i4IfType, INT4 i4IfIndex, INT4 i4PtpPortNo));

PRIVATE INT4
PtpCliConfigAltTimeScale PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                                 INT4 i4DomainId, 
                                 INT4 i4AltTimeScaleKeyId));
PRIVATE INT4
PtpCliSetAltMasterFlag PROTO (( tCliHandle CliHandle,
                                INT4 i4ContextId, INT4 i4DomainId ,
                                INT4 i4PortId, INT4 i4Status));
PRIVATE INT4
PtpCliUpdateAccMasterEntry PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                                   INT4 i4DomainId, INT4 i4NwProto,
                                   INT4 i4AddrLen,
                                   tSNMP_OCTET_STRING_TYPE pAccMasterAddr, 
                                   INT4 i4RowStatus));

PRIVATE INT4
PtpCliSetGlobalSysControl PROTO ((tCliHandle CliHandle, INT4 i4Status));

PRIVATE INT4
PtpCliUpdateClockNumberPorts PROTO((tCliHandle CliHandle, INT4 i4ContextId,
                                    INT4 i4DomainId, 
                                    INT4 i4NumberPorts));

PRIVATE INT4
PtpCliUpdateTransClkNumberPorts PROTO((tCliHandle CliHandle, INT4 i4ContextId,
                                         INT4 i4DomainId,
                                         INT4 i4NumberPorts));
PRIVATE INT4
PtpCliDeletePortFromDomain PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                                   INT4 i4DomainId, INT4 i4PtpPortNo));

PRIVATE INT4
PtpCliMapPortToClk PROTO((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                          INT4 i4IfType, INT4 i4IfIndex, INT4 i4PtpPortNo));

PRIVATE INT4
PtpCliMapPortToTransClk PROTO ((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                                INT4 i4IfType, INT4 i4IfIndex, INT4 i4PtpPortNo));

PRIVATE INT4
PtpCliConfigureContext PROTO ((tCliHandle CliHandle, INT4 i4ContextId, 
                               INT4 i4DomainId));

PRIVATE INT4
PtpCliSetAltMcastSyncInterval PROTO( ( tCliHandle CliHandle, INT4 i4ContextId,
                                       INT4 i4DomainId ,                                                         
                                       INT4 i4PortId, INT4 i4SyncInterval));


PRIVATE INT4
PtpCliGetContextInfoForShowCmd PROTO ((tCliHandle CliHandle,
                                       UINT1 *pu1ContextName,
                                       UINT4 u4CurrContextId, 
                                       UINT4 *pu4ContextId,
                                       UINT4 u4Command));

PRIVATE INT4
PtpCliDeleteDomain PROTO ((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId));

PRIVATE INT4
PtpCliDeleteContext PROTO ((tCliHandle CliHandle, INT4 i4ContextId));

INT4
PtpCliValidateSyncInterval PROTO ((tCliHandle CliHandle, INT1 * pu1SyncInterval,
                            INT4 * i4SyncInterval));

PRIVATE INT4
PtpCliSetPortAdminStatus PROTO((tCliHandle CliHandle, INT4 i4Context,
                                INT4 i4DomainId, UINT4 u4PtpPortNo,INT4 i4Status));

PRIVATE INT4
PtpCliSetPrimaryContext PROTO ((tCliHandle CliHandle, INT4 i4PrimaryContext));


PRIVATE INT4
PtpCliSetPrimaryDomain PROTO ((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4PrimaryDomain));

PRIVATE INT4
PtpCliSetSyncLimit PROTO ((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                           INT4 i4PtpPortNo, UINT1* pu1SyncLimit));


INT4
PtpCliSetNotifyStatus PROTO
((tCliHandle CliHandle, UINT4 u4TrapValue, UINT1 u1TrapFlag));

PUBLIC INT4
PtpCliValidateSyncLimit PROTO ((tCliHandle CliHandle, INT1 *pu1SyncLimit));



/* Optional Feature Functions*/


PRIVATE INT4
PtpCliSetAccMasterFlag PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4PortId, INT4 i4Status));




PRIVATE VOID
PtpCliShowTransClkPort PROTO ((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                               UINT4 u4IfIndex, INT4 i4IfType));


INT4
PtpCliSetAccMasterAltPriority PROTO ((tCliHandle CliHandle,  INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                               UINT1 *u1AccMasterAddr, INT4 i4AlternatePriority));

PRIVATE INT4
PtpCliDelAccMaster PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                    INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                    UINT1 *u1AccMasterAddr, INT4 i4PriResetFlag));

PRIVATE VOID
PtpCliShowPortCounters PROTO ((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId));


PRIVATE VOID
PtpCliShowPortTimers PROTO ((tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId, 
                             INT4 i4NextPortIndex));


/* Optional Feature Functions*/





PRIVATE INT4
PtpCliSetPortNumOfAlternateMasters PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                                    INT4 i4DomainId, INT4 i4PtpPortId,
                                    INT4 i4MaxAltMasters));

PRIVATE INT4
PtpCliSetTransPortAdminStatus PROTO((tCliHandle CliHandle, INT4 i4ContextId,
                                INT4 i4DomainId, UINT4 u4PtpPortNo, INT4 i4Status));

PRIVATE INT4
PtpCliUpdateATSEntryOptParams PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                              INT4 i4DomainId,  INT4 i4KeyIndex,
                              INT4 i4ATSOffset, INT4 i4ATSJmpsecs,
                              UINT1 *au1AltTimeScaleJump));

PRIVATE INT4
PtpCliSetAltTimeDispName PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                              INT4 i4DomainId,  INT4 i4KeyIndex,
                              UINT1 *au1AltTimeDispName));

PRIVATE INT4
PtpCliNoSetAltTimeDispName PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                                   INT4 i4DomainId,  INT4 i4KeyIndex,
                                   UINT1 *au1AltTimeDispName));

extern INT4   CliGetCurModePromptStr (INT1 *);

PRIVATE VOID
PtpCliConverPortAddToOct PROTO ((UINT1 *u1AccMasterAddr, INT4 i4NwProto,
                                 tSNMP_OCTET_STRING_TYPE *pAccMasterAddr));
#endif /* __PTPCLIPT_H__ */

