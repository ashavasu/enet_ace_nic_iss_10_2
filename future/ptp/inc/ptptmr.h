/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *
 *
 * Description: This file contains all the sizing related constants
 *              used by the PTP module. 
 *
 *******************************************************************/
#ifndef _PTPTMR_H_
#define _PTPTMR_H_

enum {
    PTP_ANNC_TMR = 0,
    PTP_SYNC_TMR = 1,
    PTP_DREQ_TMR = 2,
    PTP_PDREQ_TMR = 3,
    PTP_ANNC_RECEIPT_TMR = 4,
    PTP_QULFN_TMR = 5,
    PTP_COMPDB_TMR = 6,
    PTP_MULT_SYNC_TMR = 7
};

/* type definitions */
typedef struct _PtpTimerDesc {
    VOID                (*pTimerExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT1               au1Rsvd[2];
                            /* Included for 4-byte Alignment
                             */
} tPtpTimerDesc;
#endif /* _PTPTMR_H_*/
