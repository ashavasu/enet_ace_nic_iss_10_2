/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *$Id: ptpprot.h,v 1.15 2014/05/29 13:19:53 siva Exp $
 * Description: Contains the prototypes for the functions used by the 
 *              PTP module.
 ********************************************************************/

#ifndef _PTPPROT_H
#define _PTPPROT_H
/************************* ptpacmst.c *********************************/
PUBLIC INT4 
PtpAcMstIsAccMasterEnabled PROTO ((tPtpPort *pPtpPort));

PUBLIC INT4 
PtpAcMstValidateMaster PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, 
                               tPtpPortAddress *pPortAddress, 
                               UINT1 *pu1AltPriority1));

/************************* ptpalmst.c *********************************/
PUBLIC INT4 
PtpAlMstGetAltMstInfo PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, 
                              UINT4 u4PortId, tPtpAltMstInfo *pPtpAltMstInfo));

PUBLIC INT4 
PtpAlMstHandleUpdateConfOpt PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, 
                                    UINT4 u4PortId, 
                                    tPtpAltMstInfo *pPtpAltMstInfo));

/******************************************************************************
 *                             ptpalts.c                                      *
 ******************************************************************************/
PUBLIC INT4 
PtpAlTsHandleUpdate PROTO ((UINT4 u4ContextId, UINT1 u1DomainId));

PUBLIC INT4
PtpAlTsUpdateTlv PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, 
                         UINT1 *pu1ATSclTlv, UINT2 u2TotTlvLen));

PUBLIC INT4 
PtpAlTsAddAltTimeTLV PROTO ((tPtpDomain *pPtpDomain, UINT1 *pu1PtpPdu, 
                             UINT4 *pu4MsgLen));

PUBLIC INT4
PtpAlTsFindATSTlvs PROTO ((UINT1 *pu1PtpPdu, UINT4 u4MsgLen, UINT1 *pu1ATSclTlv,
                           UINT2 *pu2TotTlvLen));

PUBLIC INT4 
PtpAlMstTriggerAltMstMsg (tPtpPort *pPtpPort);
PUBLIC VOID 
PtpAlMstHandlePortStateChange (tPtpPort *pPtpPort);

PUBLIC tPtpAltTimescaleEntry *
PtpAlTsCreate PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, UINT1 u1PtpAlTsKeyId));

PUBLIC VOID
PtpAlTsDelete PROTO ((tPtpAltTimescaleEntry * pPtpAltTimescaleEntry));

PUBLIC VOID
PtpAlTsDeleteAlTsEntriesInDomain PROTO ((tPtpDomain * pPtpDomain));
/******************************************************************************
 *                             ptpannc.c                                      *
 ******************************************************************************/
PUBLIC INT4 PtpAnncHandleRxAnncMsg PROTO ((tPtpPktParams * pPtpPktParams));

PUBLIC INT4
PtpAnncValidateForeignMaster PROTO ((tPtpForeignMasterDs *pPtpForeignMasterDs));

PUBLIC INT4 
PtpAnncTxAnncMsg PROTO ((tPtpPort *pPtpPort));

PUBLIC VOID PtpAnncInitAnncHandler PROTO ((VOID));


/******************************************************************************
 *                             ptpbmc.c                                       *
 ******************************************************************************/
PUBLIC  VOID
PtpBmcTriggerBmcCalc PROTO ((tPtpDomain  *pPtpDomain)); 

/******************************************************************************
 *                             ptpbmcds.c                                     *
 ******************************************************************************/
PUBLIC VOID
PtpBmcDsUpdtDataSetForM1andM2 PROTO ((tPtpPort   *pPtpPort));

PUBLIC VOID
PtpBmcDsUpdtDataSetForM3 PROTO ((tPtpPort   *pPtpPort));

PUBLIC VOID
PtpBmcDsUpdtDataSetForP1AndP2 PROTO ((tPtpPort   *pPtpPort));

PUBLIC VOID
PtpBmcDsUpdtDataSetForS1 PROTO ((tPtpPort *pPtpPort, 
                                 tPtpForeignMasterDs * pPtpEbestMsg));
PUBLIC INT4
PtpBmcDataSetCmpAlgo PROTO ((tPtpForeignMasterDs * pPtpForeignMaster1,
                             tPtpForeignMasterDs * pPtpForeignMaster2));

PUBLIC VOID
PtpBmcDsUpdtDataSets PROTO ((tPtpDomain * pPtpDomain));
/******************************************************************************
 *                             ptpclk.c                                       *
 ******************************************************************************/
PUBLIC INT4 PtpClkGetTimeStamp  PROTO ((tPtpPort *pPtpPort, VOID *pu1Pdu,
                                        tClkSysTimeInfo * pPtpSysTimeInfo, 
                                        UINT4 u4IfIndex, UINT1 u1Mode));

PUBLIC INT4 PtpClkSynchornize PROTO ((tPtpPort * pPtpPort));

PUBLIC INT4 
PtpClkSyntonizeClk PROTO ((tPtpPort * pPtpPort, 
                           FS_UINT8 *pu8FollowupCorrnField));

PUBLIC VOID 
PtpClkUpdtMasterToSlaveDelay PROTO ((tPtpPort * pPtpPort, FS_UINT8 * pu8SecDiff));

PUBLIC VOID 
PtpClkUpdtSlaveToMasterDelay PROTO ((tPtpPort * pPtpPort, FS_UINT8 * pu8SecDiff));

PUBLIC VOID
PtpClkPerf8ByteSubtraction PROTO ((FS_UINT8 * pu8Val1, FS_UINT8 * pu8Val2,
                                   FS_UINT8 * pu8Result, INT1 * pi1Sign));

PUBLIC VOID
PtpClkSubtractTimeStamps PROTO ((tFsClkTimeVal *pTimeStamp1, 
                                 tFsClkTimeVal *pTimeStamp2,
                                 tFsClkTimeVal *pTimeStampOutput,
                                 INT1 *pi1Sign));

PUBLIC VOID
PtpClkCalcScalNanoSecs PROTO ((FS_UINT8 u8NanoSecVal, FS_UINT8 * pu8CorrnField));

PUBLIC VOID
PtpClkExtractCorrnField PROTO ((FS_UINT8 *pu8NanoSec, 
                                FS_UINT8 * pu8CorrnField));
/******************************************************************************
 *                             ptpcmutl.c                                     *
 ******************************************************************************/
PUBLIC INT4
PtpCmUtlv1SubDomainTov2Domain PROTO ((INT1 *pi1v1SubDomain, 
                                      UINT1 *pu1v2DomainId));

PUBLIC INT4
PtpCmUtlv2DomainTov1SubDomain PROTO ((UINT1 u1v2DomainId, 
                                      INT1 *pi1v1SubDomain));

PUBLIC INT4
PtpCmUtlv1StratumTov2ClkCls PROTO ((UINT1 u1v1Stratum, UINT1 *pu1v2ClkCls));

PUBLIC INT4
PtpCmUtlv2ClkClsTov1Stratum PROTO ((UINT1 u1v2ClkCls, UINT1 *pu1v1Stratum));

PUBLIC INT4
PtpCmUtlv2Pri1Tov1PrefClkStratum PROTO ((UINT1 u1v2Pri1, UINT1 u1v2ClkCls,
                                         BOOL1 *pb1v1Pref, 
                                         UINT1 *pu1v1ClkStratum));

PUBLIC INT4
PtpCmUtlv1ClkIdTov2ClkAcTimSrc PROTO ((INT1 *pi1v1ClkId, 
                                       UINT1 *pu1v2ClkAccuracy,
                                       UINT1 *pu1v2TimSrc));

PUBLIC INT4
PtpCmUtlv2ClkAccuracyTov1ClkId PROTO ((UINT1 u1v2ClkAccuracy, 
                                       INT1 *pi1v1ClkId));

PUBLIC INT4
PtpCmUtlv1ContMsgTypeTov2MsgType PROTO ((UINT1 u1v1Cont, UINT1 u1v1MsgType,
                                         UINT1 *pu1v2MsgType));

PUBLIC INT4
PtpCmUtlv2MsgTypeTov1ContMsgType PROTO ((UINT1 u1v2MsgType, UINT1 *pu1v2Cont,
                                         UINT1 *pu1v1MsgType));

PUBLIC INT4
PtpCmUtlv1SrcPortIdTov2SrcPortId PROTO ((UINT1 u1v1SrcComTech, 
                                         INT1 *pi1v1SrcUuid,
                                         UINT2 u2v1SrcPortId,  
                                         tClkId *pClkId,
                                         UINT2 *pu2v2PortId));

PUBLIC VOID
PtpCmUtlv2SrcPortIdTov1SrcPortId PROTO ((tClkId *pClkId, UINT2 u2v2PortId,
                                         UINT1 *pu1v1SrcComTech, 
                                         INT1 *pi1v1SrcUuid,
                                         UINT2 *pu2v1SrcPortId));

PUBLIC VOID
PtpCmUtlv1FlagTov2Flag PROTO ((INT1 *pi1v1Flag, UINT1 u1v1Stratum, 
                               INT1 *pi1v1ClkId,  INT1 *pi1v2Flag));

PUBLIC INT4
PtpCmUtlv2FlagTov1Flag PROTO ((INT1 *pi1v2Flag, INT1 *pi1v1Flag));

PUBLIC INT4
PtpCmUtlv2ClkQualTov1Fields PROTO ((tPtpClkQuality *pv2ClkQuality, 
                                    UINT1 *pu1v1Stratum,
                                    INT1  *pi1v1ClkId,
                                    INT2 *pi2v1Variance));
PUBLIC INT4
PtpCmUtlv1TimStpTov2TimStp PROTO ((tPtpV1TimeRep *pv1Timestamp, 
                                   UINT2 u2v1EpochNum, 
                                   tPtpTimeStamp *pv2Timestamp));
PUBLIC VOID
PtpCmUtlv2TimStpTov1TimStp PROTO ((tPtpTimeStamp *pv2Timestamp, 
                                   tPtpV1TimeRep *pv1Timestamp, 
                                   UINT2 *pu2v1EpochNum));

PUBLIC INT4
PtpCmUtlGetDomainIdFromPTPv1Msg PROTO ((UINT1 *pu1PtpPdu, UINT1 *pu1DomainId));
/******************************************************************************
 *                             ptpcomp.c                                     *
 ******************************************************************************/
PUBLIC INT4
PtpCompUpdateCompDbFromV1Info PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, 
                                      UINT4 u4PortId, 
                                      tPtpv1Info *pPtpv1Info));

PUBLIC INT4
PtpCompUpdateV1InfoFromCompDb PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, 
                                      UINT4 u4PortId, 
                                      tPtpv1Info *pPtpv1Info));

PUBLIC INT4
PtpCompAllocBuddy PROTO ((tPtpCompRxInfo *pPtpCompRxInfo, 
                          UINT1 u1PtpV1Control));

PUBLIC VOID
PtpCompFreeBuddy PROTO ((tPtpCompRxInfo *pPtpCompRxInfo));

PUBLIC INT4 PtpCompDelCompDbEntry (tPtpCompDBEntry *pPtpCompDB);

PUBLIC tPtpCompDBEntry *
PtpCompGetCompDbEntry (tPtpCompDBEntry *pPtpCompDB);

/******************************************************************************
 *                             ptpcomrx.c                                     *
 ******************************************************************************/
PUBLIC INT4 
PtpComRxProcessV1Messages PROTO ((tPtpPort *pPtpPort, 
                                  tPtpPktParams *pPtpPktParams));

/******************************************************************************
 *                             ptpcomtx.c                                     *
 ******************************************************************************/
PUBLIC INT4 
PtpComTxV2ToV1PtpPdu PROTO ((tPtpCompTxInfo *pPtpCompTxInfo));

/******************************************************************************
 *                             ptpcxt.c                                       *
 ******************************************************************************/
PUBLIC tPtpCxt *
PtpCxtCreateContext PROTO ((UINT4 u4ContextId));

PUBLIC VOID PtpCxtDeleteContext PROTO ((UINT4 u4ContextId));

PUBLIC VOID PtpCxtEnable PROTO ((UINT4 u4ContextId));

PUBLIC VOID PtpCxtDisable PROTO ((UINT4 u4ContextId));

PUBLIC VOID PtpCxtDeleteAllCxt PROTO ((VOID));

PUBLIC tPtpCxt * PtpCxtGetNode PROTO ((UINT4 u4ContextId));

PUBLIC BOOL1 PtpCxtIsPrimaryCxtAndDmn PROTO ((tPtpPort  * pPtpPort));

/******************************************************************************
 *                             ptpdb.c                                        *
 ******************************************************************************/
PUBLIC INT4
PtpDbAddNode PROTO ((VOID * pvPtpTable, VOID * pvPtpNode, UINT1 u1PtpDbId));

PUBLIC VOID *
PtpDbDeleteNode PROTO ((VOID * pvPtpTable, VOID * pvPtpNode, UINT1 u1PtpDbId));

PUBLIC VOID *
PtpDbGetNode PROTO ((VOID * pvPtpTable, VOID * pvPtpNode, UINT1 u1PtpDbId));

PUBLIC VOID *
PtpDbGetFirstNode PROTO ((VOID * pvPtpTable, UINT1 u1PtpDbId));

PUBLIC VOID *
PtpDbGetNextNode PROTO ((VOID * pvPtpTable, VOID * pvPtpNode, 
                         UINT1 u1PtpDbId));

PUBLIC VOID
PtpDbDispTimeInterval PROTO ((INT4 i4DataType, VOID * pvPtrVal, 
                              tSNMP_OCTET_STRING_TYPE * pDisplay));

/******************************************************************************
 *                             ptpdmn.c                                       *
 ******************************************************************************/
PUBLIC VOID PtpDmnDelDomain PROTO ((tPtpDomain *pPtpDomain));

PUBLIC VOID PtpDmnDelAllDomainsInCxt PROTO ((UINT4 u4ContextId));

PUBLIC tPtpDomain * PtpDmnCreateDomain PROTO ((UINT4 u4ContextId,
                                               UINT1 u1DomainId));

PUBLIC tPtpDomain *
PtpDmnGetNextDomainEntry PROTO ((UINT4 u4ContextId, UINT1 u1DomainId));

PUBLIC tPtpDomain *
PtpDmnGetDomainEntry PROTO ((UINT4 u4ContextId, UINT1 u1DomainId));

PUBLIC VOID
PtpDmnConfigSlaveOnDmnPorts PROTO ((UINT4 u4ContextId, UINT1 u1DomainId));

PUBLIC VOID
PtpDmnInitDomainEntry PROTO ((tPtpDomain * pPtpDomain));

PUBLIC VOID
PtpDmnInitDomainEntryCurrentDS PROTO ((tPtpDomain * pPtpDomain));

PUBLIC VOID PtpDmnUpdtPrimaryParams PROTO ((tPtpQMsg * pPtpQMsg));

PUBLIC VOID
PtpDmnDisablePorts PROTO ((UINT4 u4ContextId, UINT1 u1DomainId));
/******************************************************************************
 *                             ptpdreq.c                                      *
 ******************************************************************************/
PUBLIC INT4 PtpDreqHandleRxDelayReqMsg PROTO ((tPtpPktParams * pPtpPktParams));

PUBLIC INT4 PtpDreqTxDelayReqMsg PROTO ((tPtpPort *pPtpPort));

PUBLIC VOID PtpDreqInitDreqHandler PROTO ((VOID));

PUBLIC INT4
PtpDreqStartDelayReqAndTxPkt PROTO ((tPtpPort * pPtpPort));
/******************************************************************************
 *                             ptpdres.c                                      *
 ******************************************************************************/
PUBLIC INT4
PtpDrespHandleRxDelayRespMsg PROTO ((tPtpPktParams * pPtpPktParams));

PUBLIC INT4 
PtpDrespTxDelayResp PROTO ((UINT1 * pu1RxPdu, tPtpPort  * pPtpPort));

PUBLIC VOID PtpDresInitDresHandler PROTO ((VOID));

PUBLIC VOID PtpDresCopyHdrFieldFrmDelayReq PROTO ((UINT1 * pu1DelayReq,
         UINT1 * pu1DelayResp,
         tPtpPort * pPtpPort,
         UINT1 u1MsgType));
/******************************************************************************
 *                             ptphdr.c                                       *
 ******************************************************************************/
PUBLIC VOID 
PtpHdrFillPtpHeader PROTO ((UINT1  **ppPtpPdu, tPtpPort *pPtpPort, 
                            UINT1 u1MsgType));

PUBLIC VOID
PtpHdrAddMediaHdrForIntf PROTO ((UINT1 **ppPtpPdu, tPtpPort * pPtpPort, 
                                 UINT1 u1MsgType, UINT4 * pu4MsgLen));

/******************************************************************************
 *                             ptpif.c                                        *
 ******************************************************************************/
PUBLIC VOID PtpIfDelPort PROTO ((tPtpPort * pPtpPort));

PUBLIC VOID PtpIfEnablePort PROTO ((tPtpPort * pPtpPort));

PUBLIC VOID PtpIfDisablePort PROTO ((tPtpPort * pPtpPort));

PUBLIC tPtpPort * PtpIfCreatePort PROTO ((UINT4 u4ContextId,
                                          UINT1 u1DomainId,
                                          UINT4 u4PtpPortNum));

PUBLIC VOID
PtpIfGetNextAvailableIndex PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                                   UINT4 * pu4PortNum));

PUBLIC VOID 
PtpIfHandleOperStatusChg PROTO ((UINT4 u4ContextId, UINT4 u4Port,
                                 ePtpDeviceType PtpDeviceType,
                                 UINT1 u1Status));

PUBLIC VOID 
PtpIfDeleteInterface PROTO ((UINT4 u4ContextId, UINT4 u4Port,
                             ePtpDeviceType PtpDeviceType));

PUBLIC tPtpPort *
PtpIfGetNextPortEntry PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, 
                              UINT4 u4PortId));

PUBLIC tPtpPort *
PtpIfGetPortEntry PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                          UINT4 u4PortId));

PUBLIC INT4
PtpIfFreePortEntry PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

PUBLIC INT4
PtpIfGetPortNumber PROTO ((INT4 i4ContextId, UINT1 u1DomainId, INT4 i4IfType,
                           UINT4 u4IfIndex, INT4 * i4PtpPortNumber));


PUBLIC tPtpPort *
PtpIfGetPortFromIfTypeAndIndex PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                                       UINT4 u4IfIndex,
                                       ePtpDeviceType PtpDeviceType));

PUBLIC VOID 
PtpIfDestroyBestMsgOnPort PROTO ((tPtpPort * pPtpPort));

PUBLIC VOID
PtpIfDelayMechanismChg PROTO ((tPtpDomain * pPtpDomain));
/******************************************************************************
 *                             ptpmain.c                                      *
 ******************************************************************************/

PUBLIC INT4 PtpMainInit PROTO ((VOID));

PUBLIC INT4 PtpMainModuleStart PROTO ((VOID));

PUBLIC INT4 PtpMainDeInit PROTO ((VOID));

PUBLIC VOID PtpMainModuleShutDown PROTO ((VOID));

PUBLIC VOID PtpMainReleaseQueMemory (tPtpQMsg * pMsg);
/******************************************************************************
 *                             ptpmast.c                                      *
 ******************************************************************************/
PUBLIC UINT1 *
PtpMastCreate PROTO ((VOID * pvPtpNode, UINT1 u1MasterDataSet));

PUBLIC VOID
PtpMastDelete PROTO ((VOID * pvPtpNode, UINT1 u1MasterDataSet));

PUBLIC tPtpAccUnicastMasterTable *
PtpMastAccUnicastMasterCreate PROTO ((tPtpAccUnicastMasterTable *
                                      pPtpAccUnicastMasterTable));

PUBLIC VOID
PtpMastAccUnicastMasterDelete PROTO ((tPtpAccUnicastMasterTable *
                                      pPtpAccUnicastMasterTable));

PUBLIC tPtpForeignMasterDs *
PtpMastGetFirstFornMasterOnPort PROTO ((tPtpPort * pPtpPort));

PUBLIC VOID
PtpMastDeleteFMEntriesInPort PROTO ((tPtpPort * pPtpPort));

PUBLIC VOID
PtpMastDeleteAccEntriesInDmn PROTO ((tPtpDomain * pPtpDomain));
/******************************************************************************
 *                             ptppdreq.c                                     *
 ******************************************************************************/
PUBLIC INT4 
PtpPdreqHandlePeerDelayReqMsg PROTO ((tPtpPktParams * pPtpPktParams));

PUBLIC INT4 PtpPdreqTxPeerDelayReqMsg PROTO ((tPtpPort *pPtpPort));

PUBLIC VOID PtpPdreqInitPdreqHandler PROTO ((VOID));
    
/******************************************************************************
 *                             ptppdres.c                                     *
 ******************************************************************************/
PUBLIC INT4 
PtpPdresHandlePeerDelayRespMsg PROTO ((tPtpPktParams * pPtpPktParams));

PUBLIC INT4
PtpPdresTxPeerDelayResp PROTO ((UINT1 *pu1RxPdu, tPtpPort * pPtpPort,
                                tClkSysTimeInfo * pPtpIngressTimeStamp));

PUBLIC VOID PtpPdresInitPeerDelayRespHandler PROTO ((VOID));

PUBLIC INT4 
PtpPdresTransmitFollowUp PROTO ((tPtpPort * pPtpPort, 
                                 tClkSysTimeInfo * pPtpSysTimeInfo));
/******************************************************************************
 *                             ptpport.c                                      *
 ******************************************************************************/
PUBLIC INT4 PtpPortCfaRegLL PROTO ((VOID));

PUBLIC INT4 PtpPortCfaDeRegisterLL PROTO ((UINT2 u2LenOrType));

PUBLIC INT4 PtpPortClkIwfGetClock PROTO ((tClkSysTimeInfo * pPtpSysTimeInfo));

PUBLIC INT4 
PtpPortValidateIfaceTypeAndNum PROTO ((UINT4 u4ContextId, INT4 i4DeviceType,
                                       UINT4 u4IfIndex));

PUBLIC INT4 PtpPortIsVcExists PROTO ((UINT4 u4ContextId));

PUBLIC INT4
PtpPortHandleOutgoingPktOnPort PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                       UINT4 u4IfIndex,
                                       UINT4 u4PktSize, UINT2 u2Protocol,
                                       UINT1 u1EncapType));

PUBLIC INT4
PtpPortCfaGetIfInfo PROTO ((UINT4 u4IfIndex, tCfaIfInfo *pIfInfo));

PUBLIC INT4
PtpPortTransmitPktOverVlan PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                   UINT4 u4ContextId,
                                   tVlanId VlanId, UINT4 u4PktLen));

PUBLIC INT4
PtpPortSetClk PROTO ((tClkSysTimeInfo * pPtpSysTimeInfo, INT4 i4TimeSource));

PUBLIC VOID
PtpPortGetClkParams PROTO ((tClkIwfPrimParams * pClkIwfPrimParams));


PUBLIC INT4
PtpPortVcmIsSwitchExist PROTO ((UINT1 *pu1Alias, UINT4 *pu4ContextId));

PUBLIC INT4
PtpPortGetCfaIfIndexFromPort PROTO ((UINT4 u4Port, UINT4 *pu4IfIndex));

PUBLIC INT4 PtpPortVcmGetAliasName PROTO ((UINT4 u4ContextId, UINT1 *pu1Alias));
PUBLIC VOID
PtpPortFmNotifyFaults PROTO ((tSNMP_OID_TYPE *pEnterpriseOid,
                              UINT4 u4GenTrapType,
                              UINT4 u4SpecTrapType, 
                              tSNMP_VAR_BIND * pTrapMsg,
                              UINT1 *pu1Msg, 
                              tSNMP_OCTET_STRING_TYPE * pContextName));

PUBLIC INT4
PtpPortVcmGetCxtFromIfIndex PROTO ((UINT4 u4IfIndex, UINT4 * pu4ContextId));

PUBLIC VOID PtpPortDeRegisterClock PROTO ((UINT1 u1Protocol));

PUBLIC INT4 PtpPortRegisterClock PROTO ((VOID));

PUBLIC INT4
PtpPortNetIpv4GetIfInfo PROTO ((UINT4 u4IfIndex, 
    tNetIpv4IfInfo * pNetIpIfInfo));

PUBLIC INT4
PtpPortRegisterNetIpv4 PROTO ((VOID));

PUBLIC INT4
PtpPortDeRegisterNetIpv4 PROTO ((UINT1 u1ProtoId));

PUBLIC VOID
PtpPortGetSysMacAddress PROTO ((UINT4 u4ContextId, tMacAddr SwitchMac));

PUBLIC eTimeStamp PtpPortGetTimeStampMethod PROTO ((VOID));

PUBLIC VOID PtpPortUpdtClkId PROTO ((tPtpDomain * pPtpDomain));

PUBLIC INT4
PtpPortNetIpv6GetIfInfo PROTO ((UINT4 u4IfIndex,
                                tNetIpv6IfInfo * pNetIpv6IfInfo));

PUBLIC INT4
PtpPortNetIpv4GetPortFromIfIndex PROTO ((UINT4 u4IfIndex, UINT4 * pu4IpPort));

PUBLIC INT4 PtpPortRegisterNetIpv6 PROTO ((VOID));

PUBLIC INT4 PtpPortDeRegisterNetIpv6 PROTO ((UINT1 u1ProtoId));

PUBLIC INT4
PtpPortNetIpv6McastJoin PROTO ((UINT4 u4IfIndex, tIp6Addr * pPTPMCastAddr));

PUBLIC INT4
PtpPortNetIpv6McastLeave PROTO ((UINT4 u4IfIndex, tIp6Addr * pPTPMCastAddr));

PUBLIC UINT4 PtpPortRmRegisterProtocols (tRmRegParams * pRmReg);

UINT4 PtpPortRmDeRegisterProtocols (VOID);

UINT4 PtpPortRmReleaseMemoryForMsg (UINT1 *pu1Block);

PUBLIC UINT1 PtpPortRmGetStandbyNodeCount (VOID);

PUBLIC VOID PtpPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt);

PUBLIC UINT4
PtpPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen,
                      UINT4 u4SrcEntId, UINT4 u4DestEntId);

PUBLIC UINT4 PtpPortRmGetNodeState (VOID);

PUBLIC INT4 PtpPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck);

VOID
PtpPortRedSendMsgToRm (UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg * pRmMsg);


/******************************************************************************
 *                             ptpptrc.c                                      *
 ******************************************************************************/
PUBLIC BOOL1
PtpPTrcIsPathTraceEnabled PROTO ((tPtpDomain *pPtpDomain));

PUBLIC VOID
PtpPTrcInitPTrcList PROTO ((tPtpDomain *pPtpDomain));

PUBLIC BOOL1
PtpPTrcIsLoopedAnnounceMsg PROTO ((tPtpDomain *pPtpDomain, tClkId *pMsgSrcClkId,
                                   UINT1 *pu1PTrcTlv));
                                   
PUBLIC VOID
PtpPTrcAddPTrcTLV PROTO ((tPtpPort *pPtpPort, UINT1 *pu1PtpPdu, 
                          UINT4 *pu4MsgLen));

/******************************************************************************
 *                             ptpque.c                                       *
 ******************************************************************************/
PUBLIC VOID PtpQueMsgHandler PROTO ((VOID));

PUBLIC INT4 PtpQuePostEventToPtpTask PROTO ((INT4 i4Event));

PUBLIC INT4 PtpQueProcessPtpPdu PROTO ((tPtpPktParams * pPtpPktParams));

PUBLIC VOID PtpQueLoadMsgHandlers PROTO ((VOID));

PUBLIC INT4 PtpQueEnqMsg PROTO ((tPtpQMsg * pPtpQMsg));
/******************************************************************************
 *                             ptprbutl.c                                     *
 ******************************************************************************/
PUBLIC INT4
PtpRbUtlCompCxtDS PROTO ((tRBElem * pRbInput, tRBElem * pRbNode));

PUBLIC INT4
PtpRbUtlCompDomainDS PROTO ((tRBElem * pRbInput, tRBElem * pRbNode));

PUBLIC INT4
PtpRbUtlCompPortDS PROTO ((tRBElem * pRbInput, tRBElem * pRbNode));

PUBLIC INT4
PtpRbUtlCompFgnMasterDS PROTO ((tRBElem * pRbInput, tRBElem * pRbNode));

PUBLIC INT4
PtpRbUtlCompAccMasterDS PROTO ((tRBElem * pRbInput, tRBElem * pRbNode));

PUBLIC INT4
PtpRbUtlCompUnicastMasterDS PROTO ((tRBElem * pRbInput, tRBElem * pRbNode));

PUBLIC INT4
PtpRbUtlCompGrandMasterDS PROTO ((tRBElem * pRbInput, tRBElem * pRbNode));

PUBLIC INT4
PtpRbUtlCompSecKeyDS PROTO ((tRBElem * pRbInput, tRBElem * pRbNode));

PUBLIC INT4
PtpRbUtlCompVersionDb PROTO ((tRBElem * pRbInput, tRBElem * pRbNode));

PUBLIC INT4
PtpRbUtlCompAltTimescaleDS PROTO ((tRBElem * pRbInput, tRBElem * pRbNode));

PUBLIC INT4 PtpRbUtlCreateRbTrees PROTO ((VOID));
PUBLIC VOID PtpRbUtlDestroyRbTrees PROTO ((VOID));
/******************************************************************************
 *                             ptpsecky.c                                     *
 ******************************************************************************/
PUBLIC tPtpSecKeyDs *
PtpSecKyCreate PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4KeyId));

PUBLIC VOID
PtpSecKyDelete PROTO ((tPtpSecKeyDs * pPtpSecKeyDs));

/******************************************************************************
 *                             ptpsecsa.c                                     *
 ******************************************************************************/
PUBLIC tPtpSADs *
PtpSecSACreate PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4SaId));

PUBLIC VOID
PtpSecSADelete PROTO ((tPtpSADs * pPtpSADs));

PUBLIC tPtpSADs*
PtpSecSAGetSAEntry PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4SAId));

PUBLIC tPtpSADs *
PtpSecSAGetNextSAEntry PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                               UINT4 u4SAId));

/******************************************************************************
 *                             ptpsem.c                                       *
 ******************************************************************************/
PUBLIC  VOID
PtpSemPortSemHandler PROTO ((tPtpPort  *pPtpPort, UINT1  u1Event));

PUBLIC VOID PtpSemMakeListening PROTO ((tPtpPort  * pPtpPort));

PUBLIC VOID PtpSemMakePassive PROTO ((tPtpPort *pPtpPort));
/******************************************************************************
 *                             ptpstate.c                                     *
 ******************************************************************************/
PUBLIC VOID 
PtpStateExecDecisionAlgorithm PROTO ((tPtpPort * pPtpPort,
                                      tPtpForeignMasterDs * pPtpEbestMsg)); 

/******************************************************************************
 *                             ptpsync.c                                      *
 ******************************************************************************/
PUBLIC INT4 
PtpSyncHandleRxSyncMsg PROTO ((tPtpPktParams * pPtpPktParams));

PUBLIC INT4 
PtpSyncTxSyncMsg PROTO ((tPtpPort *pPtpPort));

PUBLIC VOID PtpSyncInitSyncHandler PROTO ((VOID));

PUBLIC INT4 
PtpSyncTransmitFollowUp PROTO ((tPtpPort * pPtpPort,
                                tClkSysTimeInfo * pPtpSysTimeInfo));
/******************************************************************************
 *                             ptptmr.c                                       *
 ******************************************************************************/
PUBLIC INT4 PtpTmrInit PROTO ((VOID));

PUBLIC  INT4 PtpTmrDeInit (VOID);

PUBLIC VOID PtpTmrExpHandler PROTO ((VOID));

PUBLIC INT4 
PtpTmrStartTmr PROTO ((tTmrBlk * pTimer, UINT4 u4TimeInterval, UINT1 u1TmrType,
                       UINT1 u1IsExpired));

PUBLIC INT4
PtpTmrStopAllTimersForPort PROTO ((tPtpPort  * pPtpPort));

PUBLIC INT4
PtpTmrStopPdelayReqTimer PROTO ((tPtpPort  * pPtpPort));

PUBLIC INT4 PtpTmrStopTmrsOnStateChg PROTO ((tPtpPort   * pPtpPort));
/******************************************************************************
 *                             ptptx.c                                        *
 ******************************************************************************/
PUBLIC INT4 
PtpTxTransmitPtpMessage PROTO ((tPtpTxParams * pPtpTxParams));

PUBLIC VOID 
PtpUdpTransmitPtpMsg PROTO ((UINT1 * pPtpPdu, tPtpPort * pPtpPort, 
                             UINT4 u4PktLen, UINT1  u1MsgType));

PUBLIC INT4 
PtpTxTriggerFollowUp PROTO ((tPtpPort * pPtpPort,
                             tClkSysTimeInfo * pPtpSysTimeInfo,
                             UINT1 u1MsgType));

PUBLIC INT4
PtpTxForwardPktOnDmnPorts PROTO ((UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                                  UINT4 u4PktLen, UINT1 u1MsgType,
                                  tFsClkTimeVal * pEventTimeStamp));
/******************************************************************************
 *                             ptputil.c                                      *
 ******************************************************************************/
PUBLIC VOID 
PtpUtilExtractHdrFromMsg PROTO ((UINT1 *pu1Pdu, tPtpHdrInfo *pPtpHdrInfo));

PUBLIC INT4 
PtpUtilIsMsgFromCurrentParent PROTO ((tPtpPort * pPtpPort, tClkId RxClkId, 
                                      UINT2 u2Port));

PUBLIC INT4 
PtpUtilValidateRxPkt PROTO ((UINT1 *pu1Pdu, tPtpPort  *pPtpPort));

PUBLIC INT4
PtpUtilIsDelayRespFromDelayReq PROTO ((tPtpPort * pPtpPort,
                                       tPtpHdrInfo * pPtpHdrInfo,
                                       UINT1 * pu1RxDelayResp));

PUBLIC INT4
PtpUtilFindNextTlv PROTO ((UINT1 *pu1PtpPdu, UINT2 u2PduLen, UINT2 *pu2OffSet,
                           UINT1 **ppu1Tlv, UINT2 *pu2TlvType, 
                           UINT2 *pu2TlvLen));

PUBLIC INT4
PtpUtilFindTlv PROTO ((UINT1 *pu1PtpPdu, UINT2 u2PduLen, UINT2 *pu2OffSet,
                       UINT2 u2TlvType, UINT1 **ppu1Tlv, UINT2 *pu2TlvLen));

PUBLIC INT4
PtpUtilIsSlavePortInBoundaryClk PROTO ((tPtpPort  * pPtpPort));

PUBLIC VOID
PtpUtilInitPortsInDmn PROTO ((UINT4 u4ContextId, UINT1 u1DomainId));

PUBLIC BOOL1
PtpUtilGetAltMstFlagStatus PROTO ((tPtpPort *pPtpPort));

PUBLIC VOID
PtpUtilGetV2AnncInfo PROTO ((tPtpPort *pPtpPort, tPtpv2Info *pPtpv2Info));

PUBLIC BOOL1 PtpUtilIsGrandMasterClk PROTO ((tPtpDomain * pPtpDomain));

PUBLIC BOOL1 PtpUtilIsPtpStarted PROTO ((UINT4 u4ContextId));

PUBLIC INT4 
PtpUtilCmpIdentities PROTO ((tClkId ClkIdA, tClkId ClkIdB));

PUBLIC INT4 PtpUdpDetEventForSocket PROTO ((INT4  i4SockId));

PUBLIC UINT4
PtpUtilLogBase2ToMilliSec PROTO ((INT1 i1LogBase2Val));

PUBLIC VOID
PtpUtilGetTraceOptionValue PROTO ((tPtpUtlValidTraces *pPtpUtlValidTraces,
                                   UINT1 *pu1TraceInput, UINT2 u2TrcLen,
                                   UINT4 *pu4TraceOption,
                                   UINT1 *pu1TraceStatus));

PUBLIC VOID
PtpUtilGetTraceOptionString PROTO ((tPtpUtlValidTraces *pPtpUtlValidTraces,
                                    UINT4 u4TrcOption,
                                    UINT1 *pu1TraceString, UINT2 *pu2TrcLen));

PUBLIC UINT4
PtpUtilGetHashValue PROTO ((ePtpDeviceType PtpDeviceType, UINT4 u4IfIndex));

PUBLIC VOID
PtpUtilFindStartIndex PROTO ((UINT4 u4ContextId, UINT4 *pu4IdxContextId,
                              UINT1 u1DomainId, UINT1 *pu1IdxDomainId, 
                              BOOL1 *bIsFirst));
PUBLIC INT1
PtpUtilSecLogBase2 PROTO ((INT1 i1Sec));

PUBLIC INT1
PtpUtilGetDebugLevel PROTO ((UINT4 u4ContextId, UINT4 * u4DbgLevel));

PUBLIC VOID
PtpUtil8BytesSubtraction PROTO ((tPtpCurrentDs *pPtpCurrentDs));

PUBLIC VOID
PtpUtil8BytesAddition PROTO ((FS_UINT8 *pu8Result, FS_UINT8 pu8Val1, FS_UINT8 pu8Val2,
                            INT1 i1Sign1,INT1 i1Sign2, INT1 *i1DelaySign));

PUBLIC VOID
PtpUtilFixedPtAdd (FS_UINT8 *pu8Result, FS_UINT8 *pu8Val1, FS_UINT8 *pu8Val2);

PUBLIC VOID
PtpUtilFixedPtSub (FS_UINT8 *pu8Result, INT1 *pi1Sign, FS_UINT8 *pu8Val1,
                   FS_UINT8 *pu8Val2);

PUBLIC BOOL1
PtpUtilFixedPtMul (FS_UINT8 *pu8Result, FS_UINT8 *pu8Val1, FS_UINT8 *pu8Val2);

PUBLIC BOOL1
PtpUtilFixedPtDiv (FS_UINT8 *pu8Result, FS_UINT8 *pu8Val1, FS_UINT8 *pu8Val2);

PUBLIC BOOL1
PtpConvertToFixedPt (FS_UINT8 *pu8Result, FS_UINT8 *pu8Val1);

PUBLIC VOID
PtpConvertFixedPtToInt (FS_UINT8 *pu8Result, FS_UINT8 *pu8Val1);

PUBLIC tPtpPort *
PtpGetDeviceTypeFromIfIndex (tCRU_BUF_CHAIN_HEADER * pBuf,
                            UINT4 u4ContextId,UINT4 u4IfIndex,
                            tVlanId VlanId,UINT1 * pu1Flag);
#ifdef L2RED_WANTED
INT4
PtpProcessValidPtpPktForRm (tCRU_BUF_CHAIN_HEADER * pBuf);
#endif
/******************************************************************************
 *                             ptpval.c                                       *
 ******************************************************************************/
PUBLIC INT4
PtpValValidateDomainDS PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                               UINT4 * pu4ErrorCode));

PUBLIC INT4
PtpValValidatePortConfigDS PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                                   UINT4 u4PortIndex, UINT1 u1IsTransPort,
                                   UINT4 * pu4ErrorCode));

PUBLIC INT4
PtpValValidateGrandMaster PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                                  UINT4 u4NwProtocol,
                                  tSNMP_OCTET_STRING_TYPE * pFsPtpMasterAddr,
                                  UINT4 * pu4ErrorCode));
                                 
PUBLIC INT4
PtpValValidateUnicastMaster PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                                    UINT4 u4PortNum, UINT4 u4NwProtocol,
                                    tSNMP_OCTET_STRING_TYPE * pFsPtpMasterAddr,
                                    UINT4 * pu4ErrorCode));

PUBLIC INT4
PtpValValidateAccMaster PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                                UINT4 u4NwProtocol,
                                tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr,
                                UINT4 * pu4ErrorCode));

PUBLIC INT4
PtpValValidateSecKeyDS PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                               UINT4 u4SecKeyId, UINT4 * pu4ErrorCode));

PUBLIC INT4
PtpValValidateSADS PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                           UINT4 u4SaId, UINT4 * pu4ErrorCode));

PUBLIC INT4
PtpValValidateAltTimeDS PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                                UINT1 u1AltTimeScaleKeyId,
                                UINT4 *pu4ErrorCode));

/******************************************************************************
 *                             ptptrans.c                                     *
 ******************************************************************************/
PUBLIC VOID 
PtpTransHandleRcvdPtpPkt PROTO ((UINT1 *pu1Pdu, tPtpPort *pPtpPort, 
                                 UINT4 u4PktLen, UINT1 u1MsgType,
                                 tFsClkTimeVal * pEventTimeStamp));

/******************************************************************************
 *                             ptptrc.c                                       *
 ******************************************************************************/
PUBLIC VOID
PtpTrcTrace PROTO ((UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4Flag, 
                    CHR1 * pc1Fmt, ...));

PUBLIC VOID
PtpTrcDisplayBetterParam PROTO ((tPtpForeignMasterDs * pPtpForeignMaster,
     UINT4 u4DiffDataSet));
/******************************************************************************
 *                             ptpudp.c                                       *
 ******************************************************************************/
PUBLIC INT4 PtpUdpProcessPktOnSocket PROTO ((INT4 i4SockId));

PUBLIC INT4 PtpUdpDeInitParams PROTO ((INT4 i4SockId));

PUBLIC INT4 PtpUdpBindWithUdp PROTO ((VOID));

PUBLIC INT4 PtpUdpInitSockParams PROTO ((INT4  i4SockId));

PUBLIC INT4 PtpUdpAddOrRemoveFd PROTO ((UINT1 u1AddFd));

PUBLIC VOID PtpUdpAddMembership PROTO ((UINT4 u4Port));

PUBLIC VOID PtpUdpRemoveMembership PROTO ((UINT4 u4Port));

/******************************************************************************
 *                             ptpnpwr.c                                      *
 ******************************************************************************/
PUBLIC INT4 PtpNpWrConfigPtpStatus PROTO ((tPtpPort * pPtpPort, 
                                           UINT4 u4Status));

PUBLIC INT4 PtpNpWrInitPortParams PROTO ((tPtpPort * pPtpPort));

PUBLIC INT4 PtpNpWrTimeStampCallBack PROTO ((tPtpHwPtpCbParams * pPtpHwPtpCbParams));

PUBLIC INT4
PtpNpWrConfigDomainMode PROTO ((tPtpDomain * pPtpDomain));

PUBLIC INT4
PtpNpWrConfigRateAdj PROTO ((tPtpPort * pPtpPort));

PUBLIC INT4
PtpNpWrConfigHwClk PROTO ((tPtpNpWrCfgHwClk * pPtpNpWrCfgHwClk));

PUBLIC INT4
PtpHwConfigSrcIdHash PROTO ((tPtpHwSrcIdHash * pPtpHwSrcIdHash));

PUBLIC INT4
PtpNpWrInit PROTO ((VOID));

PUBLIC VOID
PtpNpWrDeInit PROTO ((VOID));

PUBLIC INT4
PtpNpWrGetPktLogTime PROTO ((tPtpHwLog *pPtpHwLog));

/* PTP - PEERDELAY */

PUBLIC INT4
PtpNpWrConfigPeerDelay PROTO ((tPtpNpWrPeerDelay * pPtpNpWrPeerDelay));

PUBLIC INT4
PtpNpWrGetHwClk PROTO ((UINT4 u4IfIndex, tClkSysTimeInfo * pPtpSysTimeInfo));
/******************************************************************************
 *                             ptpapi.c                                       *
 ******************************************************************************/
PUBLIC VOID
PtpApiIPIfStatusChgHdlr PROTO ((tNetIpv4IfInfo * pNetIpv4IfInfo, 
                                UINT4 u4BitMap));
PUBLIC INT4
PtpApiG8261GetDelay PROTO ((FS_UINT8 *pu8Delay));
PUBLIC INT1 PtpApiG8261ComputeDelay PROTO ((FS_UINT8 *pu8Delay));
/******************************************************************************
 *                             ptptrap.c                                      *
 ******************************************************************************/
PUBLIC VOID
PtpTrapSendTrapNotifications PROTO ((tPtpNotifyInfo * pNotifyInfo, 
                                     UINT2 u2TrapType));
PUBLIC VOID
PtpTrapSendGlobErrTrap PROTO ((UINT1 u1ErrType));

PUBLIC VOID
PtpTrapSendPreMastNotification PROTO ((tPtpPort * pPtpPort));
/******************************************************************************
 *                             ptpudpv6.c                                     *
 ******************************************************************************/
PUBLIC INT4 PtpUdpV6BindWithUdpV6 PROTO ((VOID));

PUBLIC VOID 
PtpUdpV6TransmitPtpMsg PROTO ((UINT1 * pu1PtpPdu, tPtpPort * pPtpPort, 
                               UINT4 u4PktLen, UINT1  u1MsgType));

PUBLIC INT4 PtpUdpV6ProcessPktOnSocket PROTO ((INT4 i4SockId));

PUBLIC VOID PtpUtlU8Div PROTO 
         ((FS_UINT8 *pu8Result, FS_UINT8 *pu8Reminder, FS_UINT8 *pu8Val1, 
           FS_UINT8 *pu8Val2));

PUBLIC VOID        PtpQueHandleRxPdu (tPtpQMsg * pPtpQMsg);
#endif /* _PTPPROT_H */
/******************************************************************************
 *                             END OF FILE.                                   *
 ******************************************************************************/
