/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ptptrc.h,v 1.3 2014/01/24 12:20:08 siva Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/
#ifndef _PTPTRC_H_
#define _PTPTRC_H_

#define  PTP_TRC_FLAG  /*Global Variable for traces*/ 
#define  PTP_MOD_NAME      "PTP"               

#define PTP_CLR_TRC_OPT       0

#define PTP_INVALID_TRC   0x00000000
#define PTP_CRITICAL_TRC  0x00000100

#define PTP_ALL_TRC (INIT_SHUT_TRC | MGMT_TRC | \
                      CONTROL_PLANE_TRC | DUMP_TRC | OS_RESOURCE_TRC | \
                      ALL_FAILURE_TRC | BUFFER_TRC | PTP_CRITICAL_TRC)



/* The following traces will be used only for testing purposes
 * and there is no object to configure this value
 */
#define PTP_ENTRY_TRC     0x00000200
#define PTP_EXIT_TRC      0x00000400

#define PTP_MIN_TRC_VALUE INIT_SHUT_TRC
#define PTP_MAX_TRC_VALUE PTP_TRC_CRITICAL
#define PTP_GLB_TRC       PTP_INVALID_CONTEXT_ID

#define PTP_MAX_TRC_LEN   1000

#ifdef TRACE_WANTED

#define PTP_TRC(x) PtpTrcTrace x

#define PTP_DUMP_TRC(cxt,buf,length)\
{\
    tPtpCxt            *pCxt = NULL;\
    pCxt = PtpCxtGetNode (cxt);\
    if (pCxt != NULL) \
    {    \
        if (pCxt->u2Trace & DUMP_TRC)\
        {\
            DumpPkt(buf, length);\
        }\
    }\
}
    
#define PTP_FN_ENTRY() MOD_FN_ENTRY (gPtpGlobalInfo.u2GblTrace, PTP_ENTRY_TRC, \
                                     (const CHR1 *) "PTP")

#define PTP_FN_EXIT() MOD_FN_EXIT (gPtpGlobalInfo.u2GblTrace, PTP_EXIT_TRC, \
                                   (const CHR1 *) "PTP")

#else

#define PTP_TRC(x)

#define PTP_DUMP_TRC(cxt,buf,length)

#define PTP_FN_ENTRY()

#define PTP_FN_EXIT()

#endif /* TRACE_WANTED */


#endif /* _PTPTRC_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  ptptrc.h                       */
/*-----------------------------------------------------------------------*/
