
/******************** Functions in the File : ptputil.c *******************/
PUBLIC INT4
PtpUtilFindNextTlv (UINT1 *pu1PtpPdu, UINT2 u2PduLen, UINT2 *pu2OffSet,
                    UINT1 **ppu1Tlv, UINT2 *pu2TlvType, UINT2 *pu2TlvLen);
PUBLIC INT4
PtpUtilFindTlv (UINT1 *pu1PtpPdu, UINT2 u2PduLen, UINT2 *pu2OffSet,
                UINT2 u2TlvType, UINT1 **ppu1Tlv, UINT2 *pu2TlvLen);
PUBLIC BOOL1
PtpUtilGetAltMstFlagStatus (tPtpPort *pPtpPort);

PUBLIC VOID
PtpUtilGetV2AnncInfo (tPtpPort *pPtpPort, tPtpv2Info *pPtpv2Info);

/******************** Functions in the File : ptpptrc.c *******************/
PUBLIC BOOL1
PtpPTrcIsPathTraceEnabled (UINT4 u4ContextId, UINT1 u1DomainId);
PUBLIC VOID 
PtpPTrcInitPTrcList (UINT4 u4ContextId, UINT1 u1DomainId);
PUBLIC BOOL1 
PtpPTrcIsLoopedAnnounceMsg (UINT4 u4ContextId, UINT1 u1DomainId,
                            tClkId *pMsgSrcClkId, UINT1 *pu1PTrcTlv);
PUBLIC VOID 
PtpPTrcAddPTrcTLV (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4PortId,
                   UINT1 *pu1PtpPdu, UINT4 *pu4MsgLen);

/******************** Functions in the File : ptpport.c *******************/
PUBLIC INT4
PtpPortGetVlanMemberPorts (UINT4 u4ContextId, tVlanId VlanId,
                           tPortList EgressPorts);
PUBLIC INT4
PtpPortCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo *pIfInfo);

/******************** Functions in the File : ptpcomtx.c ******************/
PUBLIC INT4 
PtpComTxV2ToV1PtpPdu (tPtpCompTxInfo *pPtpCompTxInfo);

/******************** Functions in the File : ptpcomrx.c ******************/
PUBLIC INT4 
PtpComRxProcessV1Messages (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4PortId,
                           UINT1 *pu1PtpPdu, UINT4 u4MsgLen);

/******************** Functions in the File : ptpcomp.c  ******************/
PUBLIC INT4
PtpCompUpdateCompDbFromV1Info (UINT4 u4ContextId, UINT1 u1DomainId, 
                               UINT4 u4PortId, tPtpv1Info *pPtpv1Info)
PUBLIC INT4
PtpCompUpdateV1InfoFromCompDb (UINT4 u4ContextId, UINT1 u1DomainId, 
                               UINT4 u4PortId, tPtpv1Info *pPtpv1Info);
PUBLIC INT4
PtpCompAllocBuddy (tPtpCompRxInfo *pPtpCompRxInfo, UINT1 u1PtpV1Control);
PUBLIC VOID
PtpCompFreeBuddy (tPtpCompRxInfo *pPtpCompRxInfo);

/******************** Functions in the File : ptpcmutl.c *****************/
PUBLIC INT4
PtpCmUtlv1SubDomainTov2Domain (INT1 *pi1v1SubDomain, UINT1 *pu1v2DomainId);
PUBLIC INT4
PtpCmUtlv2DomainTov1SubDomain (UINT1 u1v2DomainId, INT1 *pi1v1SubDomain);
PUBLIC INT4
PtpCmUtlv1StratumTov2ClkCls (UINT1 u1v1Stratum, UINT1 *pu1v2ClkCls);
PUBLIC INT4
PtpCmUtlv2ClkClsTov1Stratum (UINT1 u1v2ClkCls, UINT1 *pu1v1Stratum);
PUBLIC INT4
PtpCmUtlv1ClkIdTov2ClkAcTimSrc (INT1 *pi1v1ClkId, UINT1 *pu1v2ClkAccuracy,
                                UINT1 *pu1v2TimSrc);
PUBLIC INT4
PtpCmUtlv2ClkAccuracyTov1ClkId (UINT1 u1v2ClkAccuracy, INT1 *pi1v1ClkId);
PUBLIC INT4
PtpCmUtlv2Pri1Tov1PrefClkStratum (UINT1 u1v2Pri1, UINT1 u1v2ClkCls,
                                  UINT1 *pu1v1Pref, UINT1 *pu1v1ClkStratum);
PUBLIC INT4
PtpCmUtlv1ContMsgTypeTov2MsgType (UINT1 u1v1Cont, UINT1 u1v1MsgType,
                                  UINT1 *pu1v2MsgType);
PUBLIC INT4
PtpCmUtlv2MsgTypeTov1ContMsgType (UINT1 u1v2MsgType, UINT1 *pu1v2Cont,
                                  UINT1 *pu1v1MsgType);
PUBLIC VOID
PtpCmUtlv2SrcPortIdTov1SrcPortId (tClkId *pClkId, UINT2 u2v2PortId,
                                  UINT1 *pu1v1SrcComTech, INT1 *pi1v1SrcUuid,
                                  UINT2 *pu2v1SrcPortId);
PUBLIC INT4
PtpCmUtlv1SrcPortIdTov2SrcPortId (UINT1 u1v1SrcComTech, INT1 *pi1v1SrcUuid,
                                  UINT2 u2v1SrcPortId,  INT1 *pi1v2ClkId,
                                  UINT2 *pu2v2PortId);
PUBLIC VOID
PtpCmUtlv1FlagTov2Flag (INT1 *pi1v1Flag, UINT1 u1v1Stratum, 
                        INT1 *pi1v1ClkId,  INT1 *pi1v2Flag);
PUBLIC INT4
PtpCmUtlv2FlagTov1Flag (UINT1 *pi1v2Flag, UINT1 *pi1v1Flag);
PUBLIC INT4
PtpCmUtlv2ClkQualTov1Fields (tPtpClkQuality *pv2ClkQuality, UINT1 *pu1v1Stratum,
                             INT1 *pi1v1ClkId, UINT2 *pu2v1Variance);
PUBLIC INT4
PtpCmUtlv1TimStpTov2TimStp (tPtpV1TimeRep *pv1Timestamp, UINT2 u2v1EpochNum, 
                            tPtpTimeStamp *pv2Timestamp);
PUBLIC VOID
PtpCmUtlv2TimStpTov1TimStp (tPtpTimeStamp *pv2Timestamp, 
                            tPtpV1TimeRep *pv1Timestamp, UINT2 *pu2v1EpochNum);

/******************** Functions in the File : ptpalts.c *****************/
PUBLIC INT4 
PtpAlTsHandleUpdate (UINT4 u4ContextId, UINT1 u1DomainId);
PUBLIC BOOL1 
PtpAlTsIsEnabled (UINT4 u4ContextId, UINT1 u1DomainId);
PUBLIC INT4
PtpAlTsUpdateTlv (UINT4 u4ContextId, UINT1 u1DomainId, 
                  UINT1 *pu1ATSclTlv, UINT2 u2TotTlvLen);
PUBLIC INT4 
PtpAlTsAddAltTimeTLV (UINT4 u4ContextId, UINT1 u1DomainId,
                      UINT1 *pu1PtpPdu, UINT4 *pu4MsgLen);
PUBLIC INT4
PtpAlTsFindATSTlvs (UINT1 *pu1PtpPdu, UINT4 u4MsgLen, UINT1 *pu1ATSclTlv,
                    UINT2 *pu2TotTlvLen);

/******************** Functions in the File : ptpalmst.c *****************/
PUBLIC INT4 
PtpAlMstGetAltMstInfo (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4PortId,
                       tPtpAltMstInfo *pPtpAltMstInfo);
PUBLIC INT4 
PtpAlMstTriggerAltMstMsg (tPtpPort *pPtpPort);
PUBLIC INT4 
PtpAlMstHandleUpdateConfOpt (UINT4 u4ContextId, UINT1 u1DomainId,
                             UINT4 u4PortId, tPtpAltMstInfo *pPtpAltMstInfo);

/******************** Functions in the File : ptpacmst.c *****************/
PUBLIC INT4 
PtpAcMstIsAccMasterEnabled (UINT4 u4ContextId, UINT1 u1DomainId, 
                            UINT4 u4PortId);
PUBLIC INT4 
PtpAcMstValidateMaster (UINT4 u4ContextId, UINT1 u1DomainId, 
                        tPtpPortAddress *pPortAddress, UINT1 *pu1AltPriority1);

/******************** Functions in the File : ptpalts.c *****************/


