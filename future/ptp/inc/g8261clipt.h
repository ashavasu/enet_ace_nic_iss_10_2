/**********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: g8261clipt.h,v 1.1 2012/07/26 09:24:21 siva Exp $
 *
 * Description: This has prototype definitions for G8261 CLI submodule
 *
 ***********************************************************************/
#ifndef __G8261CLIPT_H__
#define __G8261CLIPT_H__
#ifdef _G8261CLI_C_
PRIVATE INT4
PtpCliSetG8261SwitchCount (tCliHandle CliHandle, UINT4 u4FwdSwitchCount, 
                           UINT4 u4ReverseSwitchCount);
PRIVATE INT4
PtpCliSetG8261PacketSize (tCliHandle CliHandle, INT4 i4PacketSize, INT4 i4ReversePktSize);
PRIVATE INT4
PtpCliSetG8261Load (tCliHandle CliHandle, INT4 i4Load, INT4 i4ReverseLoad);
PRIVATE INT4
PtpCliSetG8261NwDisturbance (tCliHandle CliHandle, INT4 i4NwDist, INT4 i4ReverseNwDist);
PRIVATE INT4
PtpCliSetDelayDuration (tCliHandle CliHandle, INT4 i4DelayDuration, INT4 i4ReverseDuration);
PRIVATE INT4
PtpCliSetG8261Frequency (tCliHandle CliHandle, UINT4 u4Frequency, UINT4 u4ReverseFrequency);
PRIVATE INT4
PtpCliSetG8261FlowVariation (tCliHandle CliHandle, INT4 i4FlowVariation, INT4 i4ReverseVariation);
PRIVATE INT4
PtpCliShowG8261DelayOutput (tCliHandle CliHandle);
PRIVATE INT4
PtpCliSetG8261ModuleStatus (tCliHandle CliHandle, INT4 i4ModuleStatus);
PRIVATE INT4
PtpCliSetG8261FlowInterval (tCliHandle CliHandle, UINT4 u4FlowInterval, 
                             UINT4 u4ReverseFlowInterval);
#endif
#endif
