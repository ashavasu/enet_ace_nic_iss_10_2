/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsG826db.h,v 1.2 2014/11/05 13:11:33 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSG826DB_H
#define _FSG826DB_H

UINT1 FsG8261TsParamsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};

UINT4 fsG826 [] ={1,3,6,1,4,1,29601,2,73};
tSNMP_OID_TYPE fsG826OID = {9, fsG826};


UINT4 FsG8261TsModuleStatus [ ] ={1,3,6,1,4,1,29601,2,73,1,1};
UINT4 FsG8261TsDirection [ ] ={1,3,6,1,4,1,29601,2,73,2,1,1,1};
UINT4 FsG8261TsSwitchCount [ ] ={1,3,6,1,4,1,29601,2,73,2,1,1,2};
UINT4 FsG8261TsPacketSize [ ] ={1,3,6,1,4,1,29601,2,73,2,1,1,3};
UINT4 FsG8261TsLoad [ ] ={1,3,6,1,4,1,29601,2,73,2,1,1,4};
UINT4 FsG8261TsNetworkDisturbance [ ] ={1,3,6,1,4,1,29601,2,73,2,1,1,5};
UINT4 FsG8261TsDuration [ ] ={1,3,6,1,4,1,29601,2,73,2,1,1,6};
UINT4 FsG8261TsFlowVariationFactor [ ] ={1,3,6,1,4,1,29601,2,73,2,1,1,7};
UINT4 FsG8261TsFlowInterval [ ] ={1,3,6,1,4,1,29601,2,73,2,1,1,8};
UINT4 FsG8261TsFrequency [ ] ={1,3,6,1,4,1,29601,2,73,2,1,1,9};
UINT4 FsG8261TsMeanVariance [ ] ={1,3,6,1,4,1,29601,2,73,3,1};
UINT4 FsG8261TsAccuracy [ ] ={1,3,6,1,4,1,29601,2,73,3,2};
UINT4 FsG8261TsDelay [ ] ={1,3,6,1,4,1,29601,2,73,3,3};




tMbDbEntry fsG826MibEntry[]= {

{{11,FsG8261TsModuleStatus}, NULL, FsG8261TsModuleStatusGet, FsG8261TsModuleStatusSet, FsG8261TsModuleStatusTest, FsG8261TsModuleStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsG8261TsDirection}, GetNextIndexFsG8261TsParamsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsG8261TsParamsTableINDEX, 1, 0, 0, NULL},

{{13,FsG8261TsSwitchCount}, GetNextIndexFsG8261TsParamsTable, FsG8261TsSwitchCountGet, FsG8261TsSwitchCountSet, FsG8261TsSwitchCountTest, FsG8261TsParamsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsG8261TsParamsTableINDEX, 1, 0, 0, NULL},

{{13,FsG8261TsPacketSize}, GetNextIndexFsG8261TsParamsTable, FsG8261TsPacketSizeGet, FsG8261TsPacketSizeSet, FsG8261TsPacketSizeTest, FsG8261TsParamsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsG8261TsParamsTableINDEX, 1, 0, 0, NULL},

{{13,FsG8261TsLoad}, GetNextIndexFsG8261TsParamsTable, FsG8261TsLoadGet, FsG8261TsLoadSet, FsG8261TsLoadTest, FsG8261TsParamsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsG8261TsParamsTableINDEX, 1, 0, 0, NULL},

{{13,FsG8261TsNetworkDisturbance}, GetNextIndexFsG8261TsParamsTable, FsG8261TsNetworkDisturbanceGet, FsG8261TsNetworkDisturbanceSet, FsG8261TsNetworkDisturbanceTest, FsG8261TsParamsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsG8261TsParamsTableINDEX, 1, 0, 0, NULL},

{{13,FsG8261TsDuration}, GetNextIndexFsG8261TsParamsTable, FsG8261TsDurationGet, FsG8261TsDurationSet, FsG8261TsDurationTest, FsG8261TsParamsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsG8261TsParamsTableINDEX, 1, 0, 0, NULL},

{{13,FsG8261TsFlowVariationFactor}, GetNextIndexFsG8261TsParamsTable, FsG8261TsFlowVariationFactorGet, FsG8261TsFlowVariationFactorSet, FsG8261TsFlowVariationFactorTest, FsG8261TsParamsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsG8261TsParamsTableINDEX, 1, 0, 0, NULL},

{{13,FsG8261TsFlowInterval}, GetNextIndexFsG8261TsParamsTable, FsG8261TsFlowIntervalGet, FsG8261TsFlowIntervalSet, FsG8261TsFlowIntervalTest, FsG8261TsParamsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsG8261TsParamsTableINDEX, 1, 0, 0, NULL},

{{13,FsG8261TsFrequency}, GetNextIndexFsG8261TsParamsTable, FsG8261TsFrequencyGet, FsG8261TsFrequencySet, FsG8261TsFrequencyTest, FsG8261TsParamsTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsG8261TsParamsTableINDEX, 1, 0, 0, NULL},

{{11,FsG8261TsMeanVariance}, NULL, FsG8261TsMeanVarianceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsG8261TsAccuracy}, NULL, FsG8261TsAccuracyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsG8261TsDelay}, NULL, FsG8261TsDelayGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData fsG826Entry = { 13, fsG826MibEntry };

#endif /* _FSG826DB_H */

