/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: fsptpwr.h,v 1.3 2014/01/03 12:40:03 siva Exp $
 * *****************************************************************/
#ifndef _FSPTPWR_H
#define _FSPTPWR_H

VOID RegisterFSPTP(VOID);

VOID UnRegisterFSPTP(VOID);
INT4 FsPtpGlobalSysCtrlGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpGblTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPrimaryContextGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpGlobalSysCtrlSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpGblTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPrimaryContextSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpGlobalSysCtrlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpGblTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPrimaryContextTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpGlobalSysCtrlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPtpGblTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPtpPrimaryContextDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsPtpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpContextTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPrimaryDomainGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpContextRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPrimaryDomainSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpContextRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPrimaryDomainTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpContextRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpDomainDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpDomainClockModeGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpDomainClockIdentityGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpDomainGMClusterQueryIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpDomainRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpDomainClockModeSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpDomainGMClusterQueryIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpDomainRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpDomainClockModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpDomainGMClusterQueryIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpDomainRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpDomainDataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpClockDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpClockIdentityGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockTwoStepFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockNumberPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockClassGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockAccuracyGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockOffsetScaledLogVarianceGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockPriority1Get(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockPriority2Get(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockSlaveOnlyGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockPathTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockAccMasterMaxSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockSecurityEnabledGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockNumOfSAGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockTwoStepFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockNumberPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockPriority1Set(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockPriority2Set(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockSlaveOnlySet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockPathTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockSecurityEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockNumOfSASet(tSnmpIndex *, tRetVal *);
INT4 FsPtpClockTwoStepFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpClockNumberPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpClockPriority1Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpClockPriority2Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpClockSlaveOnlyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpClockPathTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpClockSecurityEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpClockNumOfSATest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpClockDataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpCurrentDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpCurrentStepsRemovedGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpCurrentOffsetFromMasterGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpCurrentMeanPathDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpCurrentMasterToSlaveDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpCurrentSlaveToMasterDelayGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPtpParentDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpParentClockIdentityGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentPortNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentStatsGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentObservedOffsetScaledLogVarianceGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentObservedClockPhaseChangeRateGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentGMClockIdentityGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentGMClockClassGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentGMClockAccuracyGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentGMClockOffsetScaledLogVarianceGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentGMPriority1Get(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentGMPriority2Get(tSnmpIndex *, tRetVal *);
INT4 FsPtpParentClockObservedDriftGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPtpTimeDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpTimeCurrentUTCOffsetGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTimeCurrentUTCOffsetValidGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTimeLeap59Get(tSnmpIndex *, tRetVal *);
INT4 FsPtpTimeLeap61Get(tSnmpIndex *, tRetVal *);
INT4 FsPtpTimeTimeTraceableGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTimeFrequencyTraceableGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTimeTimeSourceGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPtpPortConfigDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpPortClockIdentityGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortInterfaceTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortIfaceNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortMinDelayReqIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortPeerMeanPathDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAnnounceIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAnnounceReceiptTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortSyncIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortSynclimitGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortDelayMechanismGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortMinPdelayReqIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortVersionNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortUnicastNegOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortUnicastMasterMaxSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAccMasterEnabledGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortNumOfAltMasterGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAltMulcastSyncGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAltMulcastSyncIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortPtpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRcvdAnnounceMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRcvdSyncMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRcvdDelayReqMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRcvdDelayRespMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortTransDelayReqMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortDiscardedMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortTransAnnounceMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortTransSyncMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRcvdPeerDelayReqMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortTransPeerDelayReqMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRcvdPeerDelayRespMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortTransPeerDelayRespMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortTransDelayRespMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortTransPeerDelayRespFollowUpMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortTransFollowUpMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRcvdPeerDelayRespFollowUpMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRcvdFollowUpMsgCntGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortClockIdentitySet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortInterfaceTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortIfaceNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortMinDelayReqIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAnnounceIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAnnounceReceiptTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortSyncIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortSynclimitSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortDelayMechanismSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortMinPdelayReqIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortVersionNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortUnicastNegOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortUnicastMasterMaxSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAccMasterEnabledSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortNumOfAltMasterSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAltMulcastSyncSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAltMulcastSyncIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortPtpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpPortClockIdentityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortInterfaceTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortIfaceNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortMinDelayReqIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAnnounceIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAnnounceReceiptTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortSyncIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortSynclimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortDelayMechanismTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortMinPdelayReqIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortVersionNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortUnicastNegOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortUnicastMasterMaxSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAccMasterEnabledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortNumOfAltMasterTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAltMulcastSyncTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortAltMulcastSyncIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortPtpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpPortConfigDataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpForeignMasterDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpForeignMasterAnnounceMsgsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPtpTransparentDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpTransparentClockIdentityGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockTwoStepFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockNumberPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockDelaymechanismGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockPrimaryDomainGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockTwoStepFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockNumberPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockDelaymechanismSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockPrimaryDomainSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockTwoStepFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockNumberPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockDelaymechanismTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentClockPrimaryDomainTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentDataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpTransparentPortDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpTransparentPortInterfaceTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortIfaceNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortClockIdentityGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortMinPdelayReqIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortFaultyFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortPeerMeanPathDelayGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortPtpStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortInterfaceTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortIfaceNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortClockIdentitySet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortMinPdelayReqIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortPtpStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortInterfaceTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortIfaceNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortClockIdentityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortMinPdelayReqIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortPtpStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTransparentPortDataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpGrandMasterClusterDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpGrandMasterClusterRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpGrandMasterClusterRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpGrandMasterClusterRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpGrandMasterClusterDataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpUnicastMasterDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpUnicastMasterRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpUnicastMasterRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpUnicastMasterRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpUnicastMasterDataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpAccMasterDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpAccMasterAlternatePriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAccMasterRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAccMasterAlternatePrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAccMasterRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAccMasterAlternatePriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpAccMasterRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpAccMasterDataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpSecKeyDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpSecKeyAlgorithmIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyLengthGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyStartTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyExpirationTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyValidGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyAlgorithmIdSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyLengthSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeySet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyStartTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyExpirationTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyValidSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyAlgorithmIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyLengthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyStartTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyExpirationTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyValidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSecKeyDataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpSADataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpSASrcPortNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcAddrLengthGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstPortNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstAddrLengthGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcClockIdentityGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstClockIdentityGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAReplayCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSALifeTimeIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSANextLifeTimeIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSANextKeyIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSATrustStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSATrustTimerGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSATrustTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAChallengeStateGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAChallengeTimerGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAChallengeTimeOutGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSARequestNonceGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAResponseNonceGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAChallengeRequiredGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAResponseRequiredGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSATypeFieldGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSADirectionGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSARowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcPortNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcAddrLengthSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstPortNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstAddrLengthSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcClockIdentitySet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstClockIdentitySet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAKeyIdSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSANextKeyIdSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSATrustTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAChallengeTimeOutSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAChallengeRequiredSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSAResponseRequiredSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSADirectionSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSARowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcPortNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcAddrLengthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstPortNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstAddrLengthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSASrcClockIdentityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSADstClockIdentityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSAKeyIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSANextKeyIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSATrustTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSAChallengeTimeOutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSAChallengeRequiredTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSAResponseRequiredTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSADirectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSARowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpSADataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPtpAltTimeScaleDataSetTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPtpAltTimeScalecurrentOffsetGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScalejumpSecondsGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScaletimeOfNextJumpGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScaledisplayNameGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScaleRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScalecurrentOffsetSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScalejumpSecondsSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScaletimeOfNextJumpSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScaledisplayNameSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScaleRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScalecurrentOffsetTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScalejumpSecondsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScaletimeOfNextJumpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScaledisplayNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScaleRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpAltTimeScaleDataSetTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsPtpGlobalErrTrapTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpNotificationGet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTrapContextNameSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTrapDomainNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpNotificationSet(tSnmpIndex *, tRetVal *);
INT4 FsPtpTrapContextNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTrapDomainNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpNotificationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPtpTrapContextNameDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPtpTrapDomainNumberDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPtpNotificationDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSPTPWR_H */
