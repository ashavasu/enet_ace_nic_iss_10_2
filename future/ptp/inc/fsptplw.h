/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsptplw.h,v 1.3 2014/01/03 12:40:03 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpGlobalSysCtrl ARG_LIST((INT4 *));

INT1
nmhGetFsPtpGblTraceOption ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpPrimaryContext ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpGlobalSysCtrl ARG_LIST((INT4 ));

INT1
nmhSetFsPtpGblTraceOption ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpPrimaryContext ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpGlobalSysCtrl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPtpGblTraceOption ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpPrimaryContext ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpGlobalSysCtrl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPtpGblTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPtpPrimaryContext ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpTable. */
INT1
nmhValidateIndexInstanceFsPtpTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpTable  */

INT1
nmhGetFirstIndexFsPtpTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpAdminStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPtpTraceOption ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpContextType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPtpPrimaryDomain ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPtpContextRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpAdminStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPtpTraceOption ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpPrimaryDomain ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPtpContextRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPtpTraceOption ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpPrimaryDomain ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPtpContextRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpDomainDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpDomainDataSetTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpDomainDataSetTable  */

INT1
nmhGetFirstIndexFsPtpDomainDataSetTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpDomainDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpDomainClockMode ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpDomainClockIdentity ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpDomainGMClusterQueryInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpDomainRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpDomainClockMode ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpDomainGMClusterQueryInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpDomainRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpDomainClockMode ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpDomainGMClusterQueryInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpDomainRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpDomainDataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpClockDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpClockDataSetTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpClockDataSetTable  */

INT1
nmhGetFirstIndexFsPtpClockDataSetTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpClockDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpClockIdentity ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpClockTwoStepFlag ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockNumberPorts ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockClass ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockAccuracy ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockOffsetScaledLogVariance ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockPriority1 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockPriority2 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockSlaveOnly ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockPathTraceOption ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockAccMasterMaxSize ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockSecurityEnabled ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpClockNumOfSA ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpClockTwoStepFlag ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpClockNumberPorts ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpClockPriority1 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpClockPriority2 ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpClockSlaveOnly ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpClockPathTraceOption ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpClockSecurityEnabled ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpClockNumOfSA ARG_LIST((INT4  , INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpClockTwoStepFlag ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpClockNumberPorts ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpClockPriority1 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpClockPriority2 ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpClockSlaveOnly ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpClockPathTraceOption ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpClockSecurityEnabled ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpClockNumOfSA ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpClockDataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpCurrentDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpCurrentDataSetTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpCurrentDataSetTable  */

INT1
nmhGetFirstIndexFsPtpCurrentDataSetTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpCurrentDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpCurrentStepsRemoved ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpCurrentOffsetFromMaster ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpCurrentMeanPathDelay ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpCurrentMasterToSlaveDelay ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpCurrentSlaveToMasterDelay ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPtpParentDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpParentDataSetTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpParentDataSetTable  */

INT1
nmhGetFirstIndexFsPtpParentDataSetTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpParentDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpParentClockIdentity ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpParentPortNumber ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpParentStats ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpParentObservedOffsetScaledLogVariance ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpParentObservedClockPhaseChangeRate ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpParentGMClockIdentity ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpParentGMClockClass ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpParentGMClockAccuracy ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpParentGMClockOffsetScaledLogVariance ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpParentGMPriority1 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpParentGMPriority2 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpParentClockObservedDrift ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPtpTimeDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpTimeDataSetTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpTimeDataSetTable  */

INT1
nmhGetFirstIndexFsPtpTimeDataSetTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpTimeDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpTimeCurrentUTCOffset ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTimeCurrentUTCOffsetValid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTimeLeap59 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTimeLeap61 ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTimeTimeTraceable ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTimeFrequencyTraceable ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTimeTimeSource ARG_LIST((INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPtpPortConfigDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpPortConfigDataSetTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpPortConfigDataSetTable  */

INT1
nmhGetFirstIndexFsPtpPortConfigDataSetTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpPortConfigDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpPortClockIdentity ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpPortInterfaceType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortIfaceNumber ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortState ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortMinDelayReqInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortPeerMeanPathDelay ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpPortAnnounceInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortAnnounceReceiptTimeout ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortSyncInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortSynclimit ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpPortDelayMechanism ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortMinPdelayReqInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortVersionNumber ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortUnicastNegOption ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortUnicastMasterMaxSize ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortAccMasterEnabled ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortNumOfAltMaster ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortAltMulcastSync ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortAltMulcastSyncInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortPtpStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpPortRcvdAnnounceMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortRcvdSyncMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortRcvdDelayReqMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortRcvdDelayRespMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortTransDelayReqMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortDiscardedMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortTransAnnounceMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortTransSyncMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortRcvdPeerDelayReqMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortTransPeerDelayReqMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortRcvdPeerDelayRespMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortTransPeerDelayRespMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortTransDelayRespMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortTransPeerDelayRespFollowUpMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortTransFollowUpMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortRcvdPeerDelayRespFollowUpMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortRcvdFollowUpMsgCnt ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpPortRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpPortClockIdentity ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpPortInterfaceType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortIfaceNumber ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortMinDelayReqInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortAnnounceInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortAnnounceReceiptTimeout ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortSyncInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortSynclimit ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpPortDelayMechanism ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortMinPdelayReqInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortVersionNumber ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortUnicastNegOption ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortUnicastMasterMaxSize ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortAccMasterEnabled ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortNumOfAltMaster ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortAltMulcastSync ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortAltMulcastSyncInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortPtpStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpPortRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpPortClockIdentity ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpPortInterfaceType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortIfaceNumber ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortMinDelayReqInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortAnnounceInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortAnnounceReceiptTimeout ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortSyncInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortSynclimit ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpPortDelayMechanism ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortMinPdelayReqInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortVersionNumber ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortUnicastNegOption ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortUnicastMasterMaxSize ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortAccMasterEnabled ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortNumOfAltMaster ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortAltMulcastSync ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortAltMulcastSyncInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortPtpStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpPortRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpPortConfigDataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpForeignMasterDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpForeignMasterDataSetTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpForeignMasterDataSetTable  */

INT1
nmhGetFirstIndexFsPtpForeignMasterDataSetTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpForeignMasterDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpForeignMasterAnnounceMsgs ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsPtpTransparentDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpTransparentDataSetTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpTransparentDataSetTable  */

INT1
nmhGetFirstIndexFsPtpTransparentDataSetTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpTransparentDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpTransparentClockIdentity ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpTransparentClockTwoStepFlag ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTransparentClockNumberPorts ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTransparentClockDelaymechanism ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTransparentClockPrimaryDomain ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpTransparentClockTwoStepFlag ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpTransparentClockNumberPorts ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpTransparentClockDelaymechanism ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpTransparentClockPrimaryDomain ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpTransparentClockTwoStepFlag ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpTransparentClockNumberPorts ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpTransparentClockDelaymechanism ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpTransparentClockPrimaryDomain ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpTransparentDataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpTransparentPortDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpTransparentPortDataSetTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpTransparentPortDataSetTable  */

INT1
nmhGetFirstIndexFsPtpTransparentPortDataSetTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpTransparentPortDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpTransparentPortInterfaceType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTransparentPortIfaceNumber ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTransparentPortClockIdentity ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpTransparentPortMinPdelayReqInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTransparentPortFaultyFlag ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTransparentPortPeerMeanPathDelay ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpTransparentPortPtpStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpTransparentPortRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpTransparentPortInterfaceType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpTransparentPortIfaceNumber ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpTransparentPortClockIdentity ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpTransparentPortMinPdelayReqInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpTransparentPortPtpStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpTransparentPortRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpTransparentPortInterfaceType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpTransparentPortIfaceNumber ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpTransparentPortClockIdentity ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpTransparentPortMinPdelayReqInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpTransparentPortPtpStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpTransparentPortRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpTransparentPortDataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpGrandMasterClusterDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpGrandMasterClusterDataSetTable ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPtpGrandMasterClusterDataSetTable  */

INT1
nmhGetFirstIndexFsPtpGrandMasterClusterDataSetTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpGrandMasterClusterDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpGrandMasterClusterRowStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpGrandMasterClusterRowStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpGrandMasterClusterRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpGrandMasterClusterDataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpUnicastMasterDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpUnicastMasterDataSetTable ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPtpUnicastMasterDataSetTable  */

INT1
nmhGetFirstIndexFsPtpUnicastMasterDataSetTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpUnicastMasterDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpUnicastMasterRowStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpUnicastMasterRowStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpUnicastMasterRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpUnicastMasterDataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpAccMasterDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpAccMasterDataSetTable ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsPtpAccMasterDataSetTable  */

INT1
nmhGetFirstIndexFsPtpAccMasterDataSetTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpAccMasterDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpAccMasterAlternatePriority ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsPtpAccMasterRowStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpAccMasterAlternatePriority ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsPtpAccMasterRowStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpAccMasterAlternatePriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsPtpAccMasterRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpAccMasterDataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpSecKeyDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpSecKeyDataSetTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpSecKeyDataSetTable  */

INT1
nmhGetFirstIndexFsPtpSecKeyDataSetTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpSecKeyDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpSecKeyAlgorithmId ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSecKeyLength ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSecKey ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpSecKeyStartTime ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpSecKeyExpirationTime ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPtpSecKeyValid ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSecKeyRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpSecKeyAlgorithmId ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSecKeyLength ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSecKey ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpSecKeyStartTime ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsPtpSecKeyExpirationTime ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsPtpSecKeyValid ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSecKeyRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpSecKeyAlgorithmId ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSecKeyLength ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSecKey ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpSecKeyStartTime ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsPtpSecKeyExpirationTime ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsPtpSecKeyValid ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSecKeyRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpSecKeyDataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpSADataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpSADataSetTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpSADataSetTable  */

INT1
nmhGetFirstIndexFsPtpSADataSetTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpSADataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpSASrcPortNumber ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSASrcAddrLength ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSASrcAddr ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpSADstPortNumber ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSADstAddrLength ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSADstAddr ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpSASrcClockIdentity ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpSADstClockIdentity ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpSAReplayCounter ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSALifeTimeId ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSAKeyId ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSANextLifeTimeId ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSANextKeyId ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSATrustState ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSATrustTimer ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSATrustTimeout ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSAChallengeState ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSAChallengeTimer ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSAChallengeTimeOut ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSARequestNonce ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSAResponseNonce ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSAChallengeRequired ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSAResponseRequired ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSATypeField ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSADirection ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpSARowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpSASrcPortNumber ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSASrcAddrLength ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSASrcAddr ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpSADstPortNumber ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSADstAddrLength ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSADstAddr ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpSASrcClockIdentity ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpSADstClockIdentity ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpSAKeyId ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSANextKeyId ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSATrustTimeout ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSAChallengeTimeOut ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSAChallengeRequired ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSAResponseRequired ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSADirection ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpSARowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpSASrcPortNumber ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSASrcAddrLength ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSASrcAddr ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpSADstPortNumber ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSADstAddrLength ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSADstAddr ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpSASrcClockIdentity ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpSADstClockIdentity ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpSAKeyId ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSANextKeyId ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSATrustTimeout ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSAChallengeTimeOut ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSAChallengeRequired ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSAResponseRequired ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSADirection ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpSARowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpSADataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPtpAltTimeScaleDataSetTable. */
INT1
nmhValidateIndexInstanceFsPtpAltTimeScaleDataSetTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPtpAltTimeScaleDataSetTable  */

INT1
nmhGetFirstIndexFsPtpAltTimeScaleDataSetTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPtpAltTimeScaleDataSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpAltTimeScalecurrentOffset ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpAltTimeScalejumpSeconds ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPtpAltTimeScaletimeOfNextJump ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpAltTimeScaledisplayName ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPtpAltTimeScaleRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpAltTimeScalecurrentOffset ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpAltTimeScalejumpSeconds ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPtpAltTimeScaletimeOfNextJump ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpAltTimeScaledisplayName ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPtpAltTimeScaleRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpAltTimeScalecurrentOffset ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpAltTimeScalejumpSeconds ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPtpAltTimeScaletimeOfNextJump ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpAltTimeScaledisplayName ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPtpAltTimeScaleRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpAltTimeScaleDataSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPtpGlobalErrTrapType ARG_LIST((INT4 *));

INT1
nmhGetFsPtpNotification ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPtpNotification ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPtpNotification ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPtpNotification ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
