/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsG826lw.h,v 1.1 2012/07/26 09:24:21 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsG8261TsModuleStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsG8261TsModuleStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsG8261TsModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsG8261TsModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsG8261TsParamsTable. */
INT1
nmhValidateIndexInstanceFsG8261TsParamsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsG8261TsParamsTable  */

INT1
nmhGetFirstIndexFsG8261TsParamsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsG8261TsParamsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsG8261TsSwitchCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsG8261TsPacketSize ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsG8261TsLoad ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsG8261TsNetworkDisturbance ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsG8261TsDuration ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsG8261TsFlowVariationFactor ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsG8261TsFlowInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsG8261TsFrequency ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsG8261TsSwitchCount ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsG8261TsPacketSize ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsG8261TsLoad ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsG8261TsNetworkDisturbance ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsG8261TsDuration ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsG8261TsFlowVariationFactor ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsG8261TsFlowInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsG8261TsFrequency ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsG8261TsSwitchCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsG8261TsPacketSize ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsG8261TsLoad ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsG8261TsNetworkDisturbance ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsG8261TsDuration ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsG8261TsFlowVariationFactor ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsG8261TsFlowInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsG8261TsFrequency ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsG8261TsParamsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsG8261TsMeanVariance ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsG8261TsAccuracy ARG_LIST((INT4 *));

INT1
nmhGetFsG8261TsDelay ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
