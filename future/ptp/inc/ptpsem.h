/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains SEM used by the PTP module 
 *
 *****************************************************************************/
#ifndef _PTPSEM_H_
#define _PTPSEM_H_

enum {
    PA0 = 0,
    PA1 = 1,
    PA2 = 2,
    PA3 = 3,
    PA4 = 4,
    PA5 = 5,
    PA6 = 6,
    PA7 = 7,
    PA8 = 8,
    PA9 = 9,
    PA10 = 10,
    PA11 = 11,
    PTP_MAX_PSM_FUNC = 12
};

PRIVATE VOID PtpSemPsmInvalid (tPtpPort  *pPtpPort);
PRIVATE VOID PtpSemMakeDisabled (tPtpPort  *pPtpPort);
PRIVATE VOID PtpSemMakeFaulty (tPtpPort  * pPtpPort);
PRIVATE VOID PtpSemMakeInitialize (tPtpPort  * pPtpPort);
PRIVATE VOID PtpSemProcessStateDecision (tPtpPort  * pPtpPort);
PRIVATE VOID PtpSemProcessAnncRcptTmOut (tPtpPort  * pPtpPort);
PRIVATE VOID PtpSemProcessQlyTmExpiry (tPtpPort  * pPtpPort);
PRIVATE VOID PtpSemMakePreMaster (tPtpPort  * pPtpPort);
PRIVATE VOID PtpSemMakeSlave (tPtpPort *pPtpPort);
PRIVATE VOID PtpSemProcessSyncFault (tPtpPort  * pPtpPort);
/* States & Event definitions
 * --------------------------
 *   STATES                       EVENTS
 *   ======                       ======
 * S1 - FAULTY                 E1 - Initialize
 * S2 - DISABLED               E2 - PowerUp
 * S3 - INITIALIZING           E3 - DesgEnabled
 * S4 - LISTENING              E4 - DesgDisabled
 * S5 - UNCALIBRATED           E5 - FaultCleared
 * S6 - SLAVE                  E6 - FaultDetected
 * S7 - PREMASTER              E7 - StateDecEvent
 * S8 - MASTER                 E8 - AnnounceRcptTmOut
 * S9 - PASSIVE                E9 - QualifyTmrExpiry
 *                             E10 - BMC_Master
 *                             E11 - BMC_Slave
 *                             E12 - BMC_Passive
 *                             E13 - MasterSelected
 *                             E14 - SyncFault
 * */ 
static UINT1  gau1_psm_table[PTP_MAX_EVENTS][PTP_STATE_MAX_STATES]  = { 
/*
____________________________________________________________
                S1   S2    S3   S4   S5   S6   S7   S8   S9
____________________________________________________________

 * E1 */      { PA3, PA3, PA3, PA3, PA3, PA3, PA3, PA3, PA3 },
/*___________________________________________________________
 * E2 */      { PA3, PA3, PA3, PA3, PA3, PA3, PA3, PA3, PA3 },
/* __________________________________________________________
 * E3 */      { PA0, PA3, PA0, PA0, PA0, PA0, PA0, PA0, PA0 },
/* ___________________________________________________________
 * E4 */      { PA0, PA0, PA1, PA1, PA1, PA1, PA1, PA1, PA1 },
/*____________________________________________________________
 * E5 */      { PA3, PA0, PA0, PA0, PA0, PA0, PA0, PA0, PA0 },
/*____________________________________________________________
 * E6 */      { PA2, PA0, PA2, PA2, PA2, PA2, PA2, PA2, PA2 },
/*____________________________________________________________
 * E7 */      { PA0, PA0, PA0, PA4, PA4, PA4, PA4, PA4, PA4 },
/*____________________________________________________________
 * E8 */      { PA0, PA0, PA0, PA5, PA5, PA5, PA0, PA0, PA5 },
/*____________________________________________________________
 * E9 */      { PA0, PA0, PA0, PA0, PA0, PA0, PA6, PA0, PA0 },
/* ____________________________________________________________
 * E10*/      { PA0, PA0, PA0, PA7, PA7, PA7, PA0, PA0, PA7 },
/* ____________________________________________________________
 * E11*/      { PA0, PA0, PA0, PA8, PA8, PA0, PA8, PA8, PA8 },
/* ____________________________________________________________
 * E12*/      { PA0, PA0, PA0, PA9, PA9, PA9, PA9, PA9, PA0 },
/* ____________________________________________________________
 * E13*/      { PA0, PA0, PA0, PA0, PA10,PA0, PA0, PA0, PA0 },
/* ____________________________________________________________
 * E14*/      { PA0, PA0, PA0, PA0, PA0, PA11,PA0, PA0, PA0 }
/*_____________________________________________________________
 * */
};

/* type of the functions which perform the action in PORTSM */
typedef  VOID (*tPsmFunc)(tPtpPort *);

/* Array of functional pointers */
tPsmFunc ga_psm_func[PTP_MAX_PSM_FUNC] = {
                             /* PA0 */   PtpSemPsmInvalid,
                             /* PA1 */   PtpSemMakeDisabled,
                             /* PA2 */   PtpSemMakeFaulty,
                             /* PA3 */   PtpSemMakeInitialize,
                             /* PA4 */   PtpSemProcessStateDecision,
                             /* PA5 */   PtpSemProcessAnncRcptTmOut,
                             /* PA6 */   PtpSemProcessQlyTmExpiry,
                             /* PA7 */   PtpSemMakePreMaster,
                             /* PA8 */   PtpSemMakeSlave,
                             /* PA9 */   PtpSemMakePassive,
                             /* PA10*/   PtpSemMakeSlave,
                             /* PA11*/   PtpSemProcessSyncFault
                            };

const CHR1 *gac1PtpEvtStr[PTP_MAX_EVENTS] = {
    "INITIALIZE",
    "POWERUP",
    "DESIGNATED_ENABLED",
    "DESIGNATED_DISABLED",
    "FAULT_CLEARED",
    "FAULT_DETECTED",
    "STATE_DECISION_EVENT",
    "ANNOUNCE_RECEIPT_TIMEOUT_EXPIRES",
    "QUALIFICATION_TIMEOUT_EXPIRES",
    "BMC_MASTER",
    "BMC_SLAVE",
    "BMC_PASSIVE",
    "MASTER_SELECTED",
    "SYNCHRONIZATION_FAULT"
};

const CHR1 *gac1PtpStateStr[PTP_STATE_MAX_STATES] = {
    "FAULTY",
    "DISABLED",
    "INITIALIZING",
    "LISTENING",
    "UNCALIBRATED",
    "SLAVE",
    "PREMASTER",
    "MASTER",
    "PASSIVE",
};
#endif /*_PTPSEM_H_*/
