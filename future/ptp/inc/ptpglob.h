/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved]
 *$Id: ptpglob.h,v 1.7 2014/05/29 13:19:53 siva Exp $
 *
 * Description: This file contains all the header files to be
 *              included and used by PTP 
 *
 *******************************************************************/

#ifndef _PTPGLOB_H_
#define _PTPGLOB_H_

tPtpGlobalInfo          gPtpGlobalInfo;
#ifdef L2RED_WANTED
tPtpRedGlobalInfo       gPtpRedGlobalInfo;
#endif
tDelaySystem            gDelaySystem;
tDelayParams            gDelayParams[G8261TS_MAX_DIRECTION];
tDelayOutput            gDelayOutput;
/* For non P2P mechanism Messages */
UINT1                   gau1PtpDestMacAddr[MAC_ADDR_LEN] = 
                               {0x01, 0x1B, 0x19, 0x00, 0x00, 0x00};
/* For P2P Mechanism Messages */
UINT1                   gau1PtpP2PDestMacAddr[MAC_ADDR_LEN] = 
                                  {0x01, 0x80, 0xC2, 0x00, 0x00, 0x0E};
    
/* All IPv6 Messages except PDELAY -- FF0X::181 */
tIp6Addr gV6PtpAddr   = {{{0xff,0x0e,0x00,0x00,0x00,0x00,0x00,0x00,
                           0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x81}}};

/* All IPv6 Messages except PDELAY -- FF02::6B */
tIp6Addr gV6PtpP2PAddr   = {{{0xff,0x02,0x00,0x00,0x00,0x00,0x00,0x00,
                              0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x6B}}};

/* Update the trace types supported by PTP */
UINT1                   gau1PtpTraceTypes[PTP_MAX_TRC_TYPE][PTP_UTL_MAX_TRC_LEN]= 
                                                {{"init-shut"},
                                                 {"mgmt"},
                                                 {"datapath"},
                                                 {"ctrl"},
                                                 {"pkt-dump"},
                                                 {"resource"},
                                                 {"all-fail"},
                                                 {"buffer"},
                                                 {"critical"},
                                                };

UINT1                   gau1PtpMcastGroupAddr1[IPVX_IPV4_ADDR_LEN] = { 224, 0, 1, 129 };
UINT1                   gau1PtpMcastGroupAddr2[IPVX_IPV4_ADDR_LEN] = { 224, 0, 0, 107 };

/* Sizing parameters used for dynamic memory allocation in PTP module */

tFsModSizingParams      gaFsPtpSizingParams[] =
{
     /* The following dynamic buddy memory allocation will be known during
      * execution only and this will be upated during run time.
      * Beacause of this, the actual data will be uptained only after task
      * initialization
      */

    {"PTP_PKT", "PTP_PKT_BUDDYPOOL", 1, 0, 0, 0},

    {"PTP_PTRC", "PTP_PTRC_BUDDYPOOL", 1, 0, 0, 0},

    {"PTP_ALT_TIME", "PTP_ALT_TIME_BUDDYPOOL", 1, 0, 0, 0},

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo        gFsPtpSizingInfo;

#endif /* _PTPGLOB_H_ */
