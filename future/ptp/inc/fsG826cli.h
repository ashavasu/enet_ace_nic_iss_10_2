/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsG826cli.h,v 1.1 2012/07/26 09:24:21 siva Exp $
*
* Description: Header file for nmhSet update trigger
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsG8261TsModuleStatus[11];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsG8261TsModuleStatus(i4SetValFsG8261TsModuleStatus)	\
	nmhSetCmn(FsG8261TsModuleStatus, 11, FsG8261TsModuleStatusSet, NULL, NULL, 0, 0, 0, "%i", i4SetValFsG8261TsModuleStatus)

#endif


/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsG8261TsDirection[13];
extern UINT4 FsG8261TsSwitchCount[13];
extern UINT4 FsG8261TsPacketSize[13];
extern UINT4 FsG8261TsLoad[13];
extern UINT4 FsG8261TsNetworkDisturbance[13];
extern UINT4 FsG8261TsDuration[13];
extern UINT4 FsG8261TsFlowVariationFactor[13];
extern UINT4 FsG8261TsFlowInterval[13];
extern UINT4 FsG8261TsFrequency[13];

#if defined(ISS_WANTED) ||  defined(RM_WANTED)
#define nmhSetFsG8261TsSwitchCount(i4FsG8261TsDirection ,u4SetValFsG8261TsSwitchCount)	\
	nmhSetCmn(FsG8261TsSwitchCount, 13, FsG8261TsSwitchCountSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsG8261TsDirection ,u4SetValFsG8261TsSwitchCount)
#define nmhSetFsG8261TsPacketSize(i4FsG8261TsDirection ,u4SetValFsG8261TsPacketSize)	\
	nmhSetCmn(FsG8261TsPacketSize, 13, FsG8261TsPacketSizeSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsG8261TsDirection ,u4SetValFsG8261TsPacketSize)
#define nmhSetFsG8261TsLoad(i4FsG8261TsDirection ,u4SetValFsG8261TsLoad)	\
	nmhSetCmn(FsG8261TsLoad, 13, FsG8261TsLoadSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsG8261TsDirection ,u4SetValFsG8261TsLoad)
#define nmhSetFsG8261TsNetworkDisturbance(i4FsG8261TsDirection ,u4SetValFsG8261TsNetworkDisturbance)	\
	nmhSetCmn(FsG8261TsNetworkDisturbance, 13, FsG8261TsNetworkDisturbanceSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsG8261TsDirection ,u4SetValFsG8261TsNetworkDisturbance)
#define nmhSetFsG8261TsDuration(i4FsG8261TsDirection ,u4SetValFsG8261TsDuration)	\
	nmhSetCmn(FsG8261TsDuration, 13, FsG8261TsDurationSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsG8261TsDirection ,u4SetValFsG8261TsDuration)
#define nmhSetFsG8261TsFlowVariationFactor(i4FsG8261TsDirection ,i4SetValFsG8261TsFlowVariationFactor)	\
	nmhSetCmn(FsG8261TsFlowVariationFactor, 13, FsG8261TsFlowVariationFactorSet, NULL, NULL, 0, 0, 1, "%i %i", i4FsG8261TsDirection ,i4SetValFsG8261TsFlowVariationFactor)
#define nmhSetFsG8261TsFlowInterval(i4FsG8261TsDirection ,u4SetValFsG8261TsFlowInterval)	\
	nmhSetCmn(FsG8261TsFlowInterval, 13, FsG8261TsFlowIntervalSet, NULL, NULL, 0, 0, 1, "%i %u", i4FsG8261TsDirection ,u4SetValFsG8261TsFlowInterval)
#define nmhSetFsG8261TsFrequency(i4FsG8261TsDirection ,u4SetValFsG8261TsFrequency)	\
	nmhSetCmn(FsG8261TsFrequency, 13, FsG8261TsFrequencySet, NULL, NULL, 0, 0, 1, "%i %u", i4FsG8261TsDirection ,u4SetValFsG8261TsFrequency)

#endif
