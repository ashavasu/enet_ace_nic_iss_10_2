/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *  $Id: ptpsync.c,v 1.14 2014/01/24 12:20:09 siva Exp $
 *
 * Description: This file contains PTP task sync message handler related 
 *              routines.
 *********************************************************************/
#ifndef _PTPSYNC_C_
#define _PTPSYNC_C_

#include "ptpincs.h"

PRIVATE INT4        PtpSyncValidateSyncMsg (tPtpPort * pPtpPort);

PRIVATE INT4        PtpSyncHandleSyncMsg (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                                          tClkSysTimeInfo * pIngressSyncTime);

PRIVATE INT4        PtpSyncUpdtSyncTmr (tPtpPort * pPtpPort,
                                        tPtpHdrInfo * pPtpHdrInfo);

/*****************************************************************************/
/* Function                  : PtpSyncHandleRxSyncMsg                        */
/*                                                                           */
/* Description               : This routine will be invoked to handle the    */
/*                             sync     messages. This routine validates and */
/*                             processes the received sync message.          */
/*                                                                           */
/* Input                     : pPtpPktParams - Pointer to received Packet    */
/*                                             Parameters.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpSyncHandleRxSyncMsg (tPtpPktParams * pPtpPktParams)
{
    PTP_FN_ENTRY ();

    PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
              pPtpPktParams->pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Received Sync Message through Port %u\r\n",
              pPtpPktParams->pPtpPort->PortDs.u4PtpPortNumber));

    if (pPtpPktParams->pPtpPort->pPtpDomain->ClkMode
        == PTP_TRANSPARENT_CLOCK_MODE)
    {
        /* Clock is operating in transparent mode. Do not consume the 
         * packet. Give it to transparent clock handler for forwarding
         * Since, sync message is an event message, generate the ingress
         * eventtimestamp for the received message.
         * */
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Clock is operating in Transparent "
                  "mode.\r\n"));

        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "TimeStamping for ingress event "
                  "message.\r\n"));

        /* The timestamping for ingress and egress events along with correction
         * field updation should happen only when software time stamping is 
         * enabled in the system.
         * The packet should be handled at the hardware level itself if hardware
         * time stamping is enabled.
         * */
        if (gPtpGlobalInfo.TimeStamp == PTP_HARDWARE_TRANS_TIMESTAMPING)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }

        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Forwarding the received sync"
                  "message.\r\n"));

        PtpTransHandleRcvdPtpPkt (pPtpPktParams->pu1RcvdPdu,
                                  pPtpPktParams->pPtpPort,
                                  pPtpPktParams->u4PktLen, PTP_SYNC_MSG,
                                  &(pPtpPktParams->pPtpIngressTimeStamp->
                                    FsClkTimeVal));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    pPtpPktParams->pu1RcvdPdu =
        pPtpPktParams->pu1RcvdPdu +
        PTP_GET_MEDIA_HDR_LEN (pPtpPktParams->pPtpPort);

    if (PtpSyncValidateSyncMsg (pPtpPktParams->pPtpPort) != OSIX_SUCCESS)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpSyncHandleRxSyncMsg: "
                  "Sync Message Validation returned Failure!!\r\n"));

        /* Incrementing the dropped message count */
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpSyncHandleSyncMsg (pPtpPktParams->pu1RcvdPdu,
                              pPtpPktParams->pPtpPort,
                              pPtpPktParams->pPtpIngressTimeStamp)
        != OSIX_SUCCESS)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpSyncHandleRxSyncMsg: "
                  "PtpSyncHandleSyncMsg returned Failure!!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpSyncValidateSyncMsg                        */
/*                                                                           */
/* Description               : This routine validates the received PTP       */
/*                             sync messages.                                */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpSyncValidateSyncMsg (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    /* The received announce message should be discarded on any one of the
     * following criterias
     * 1) Port is in INITIALIZING or DISABLED state
     * 2) Port is in FAULTY state
     * 3) Port is not in SLAVE or UNCALIBRATED state
     * */
    if ((pPtpPort->PortDs.u1PortState == PTP_STATE_INITIALIZING) ||
        (pPtpPort->PortDs.u1PortState == PTP_STATE_LISTENING) ||
        (pPtpPort->PortDs.u1PortState == PTP_STATE_FAULTY))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC, "Port %u is in "
                  "INITIALIZING/LISTENING/FAULTY State.\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Discarding the Sync Message\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if ((pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE) &&
        (pPtpPort->PortDs.u1PortState != PTP_STATE_UNCALIBRATED))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Port %u is not in SLAVE/UNCALIBRATED" " State.\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Discarding the Sync Message\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpSyncHandleSyncMsg                          */
/*                                                                           */
/* Description               : This routine handles the received PTP sync    */
/*                             messages.                                     */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                             pIngressSyncTime - Pointer to ingress Sync    */
/*                                                Time.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpSyncHandleSyncMsg (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                      tClkSysTimeInfo * pIngressSyncTime)
{
    UINT1              *pu1Tmp = NULL;
    tPtpHdrInfo         PtpHdrInfo;
    tFsClkTimeVal       OriginTimeStamp;
    tFsClkTimeVal       FsClkDiff;
    FS_UINT8            u8MeanPathDelay;
    FS_UINT8            u8NanoSecValue;
    FS_UINT8            u8TempOffSetfromMaster;
    FS_UINT8            u8Reminder;
    FS_UINT8            u8NsecDiff;
    tPtpCurrentDs      *pPtpCurrentDs = NULL;
    UINT4               u4Val = 0;
    UINT2               u2Val = 0;
    UINT1               u1ClockPgm = OSIX_FALSE;

    PTP_FN_ENTRY ();

    FSAP_U8_CLR (&u8MeanPathDelay);
    FSAP_U8_CLR (&u8NanoSecValue);
    FSAP_U8_CLR (&u8TempOffSetfromMaster);
    FSAP_U8_CLR (&u8Reminder);
    FSAP_U8_CLR (&u8NsecDiff);

    MEMSET (&PtpHdrInfo, 0, sizeof (tPtpHdrInfo));
    MEMSET (&OriginTimeStamp, 0, sizeof (tFsClkTimeVal));
    MEMSET (&FsClkDiff, 0, sizeof (tFsClkTimeVal));

    UINT8_LO (&(u8NanoSecValue)) = PTP_NANO_SEC_TO_SEC_CONV_FACTOR;

    pPtpCurrentDs = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs);

    PtpUtilExtractHdrFromMsg (pu1Pdu, &PtpHdrInfo);

    /* If the PTP packet is not received from the current Master, then it 
     * should be not be processed. */
    if (PtpUtilIsMsgFromCurrentParent (pPtpPort, PtpHdrInfo.ClkId,
                                       PtpHdrInfo.u2Port) != OSIX_TRUE)
    {
        /* Sync message not received from current parent/Master.
         * No action needs to be taken
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Sync Message not received from current "
                  "Master over Port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Discarding the Sync Message\r\n"));

        /* Incrementing the dropped message count */
        pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    /*Updating PTP message counters */
    if (PtpHdrInfo.u1MsgType == PTP_SYNC_MSG)
    {
        /*Updating Sync message count */
        pPtpPort->PtpPortStats.u4RcvdSyncMsgCnt++;
    }
    else if (PtpHdrInfo.u1MsgType == PTP_FOLLOW_UP_MSG)
    {
        /*Updating Sync Follow up message count */
        pPtpPort->PtpPortStats.u4RcvdFollowUpMsgCnt++;
    }

    if ((PtpHdrInfo.u1MsgType & PTP_FOLLOW_UP_MSG) == 0)
    {
        /* Copy the correction field */
        MEMCPY (&(pPtpPort->u8CorrectionField),
                &(PtpHdrInfo.u8CorrectionField), sizeof (FS_UINT8));
    }

    if ((pPtpPort->bDelayReqInProgress == OSIX_FALSE)
        && (PtpHdrInfo.u1MsgType == PTP_SYNC_MSG))
    {
        /* Update the sync ingress time */
        MEMCPY (&(pPtpPort->SyncReceptTime),
                &(pIngressSyncTime->FsClkTimeVal), sizeof (tFsClkTimeVal));
    }

    /* A two step clock transmits a sync message with two step flag set.
     * And for one step clock, it will be false.
     * */
    if ((PtpHdrInfo.u1MsgType == PTP_SYNC_MSG)
        && ((PtpHdrInfo.u2Flags & PTP_TWO_STEP_CLK_MASK) !=
            PTP_TWO_STEP_CLK_MASK))

    {
        /* On receiving a sync message, then local clock should be synchronized 
         * & correctionField should be adjusted for asymmetry. Refer Section 
         * 11.2 of IEEE Std 1588 2008
         *
         * <offsetFromMaster> = <syncEventIngressTimeStamp> - <originTimeStamp>
         *                      - <meanPathDelay> - correctionField
         * */

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Sync message received from "
                  "one step clock.\r\n"));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Calculating Offset From Master...\r\n"));

        /* Obtaining Sync Origin Time stamp */
        pu1Tmp = pu1Pdu + PTP_SYNC_TIME_STAMP_OFFSET;
        PTP_LBUF_GET_2_BYTES (pu1Tmp, u2Val);
        UINT8_HI (&(OriginTimeStamp.u8Sec)) = (UINT4) u2Val;

        PTP_LBUF_GET_4_BYTES (pu1Tmp, u4Val);
        UINT8_LO (&(OriginTimeStamp.u8Sec)) = u4Val;
        u4Val = 0;
        PTP_LBUF_GET_4_BYTES (pu1Tmp, u4Val);
        OriginTimeStamp.u4Nsec = u4Val;

        if (pPtpPort->bDelayReqInProgress == OSIX_FALSE)
        {
            FSAP_U8_ASSIGN (&(pPtpPort->IngressSyncTimeStamp.u8Sec),
                            &(OriginTimeStamp.u8Sec));

            pPtpPort->IngressSyncTimeStamp.u4Nsec = OriginTimeStamp.u4Nsec;

        }

        /* <syncEventIngressTimeStamp> - <originTimeStamp> */
        PtpClkSubtractTimeStamps (&(pPtpPort->SyncReceptTime),
                                  &(OriginTimeStamp),
                                  &(FsClkDiff), &(pPtpCurrentDs->i1OffsetSign));

        /* Converting the Seconds to NanoSeconds */
        FSAP_U8_MUL (&(u8TempOffSetfromMaster),
                     &(FsClkDiff.u8Sec), &(u8NanoSecValue));
        FSAP_U8_ASSIGN_LO (&u8NsecDiff, FsClkDiff.u4Nsec);
        FSAP_U8_ADD (&(u8TempOffSetfromMaster), &(u8TempOffSetfromMaster),
                     &(u8NsecDiff));
        FSAP_U8_ASSIGN (&(pPtpCurrentDs->u8OffsetFromMaster),
                        &u8TempOffSetfromMaster);
        /* Update the Master to Slave delay */
        PtpClkUpdtMasterToSlaveDelay (pPtpPort, &(u8TempOffSetfromMaster));

        /* Subtracting the correction field */
        if (pPtpCurrentDs->i1OffsetSign == PTP_NEGATIVE)
        {
            /* Now both the u8OffsetFromMaster & u8CorrectionField
             * are in nano seconds */
            FSAP_U8_ADD (&(pPtpCurrentDs->u8OffsetFromMaster),
                         &(pPtpCurrentDs->u8OffsetFromMaster),
                         &(PtpHdrInfo.u8CorrectionField));
        }
        else
        {
            PtpClkPerf8ByteSubtraction (&(pPtpCurrentDs->u8OffsetFromMaster),
                                        &(PtpHdrInfo.u8CorrectionField),
                                        &(pPtpCurrentDs->u8OffsetFromMaster),
                                        &(pPtpCurrentDs->i1OffsetSign));
        }

        /* Subtracting the mean path delay */
        if (pPtpPort->PortDs.u1DelayMechanism ==
            PTP_PORT_DELAY_MECH_PEER_TO_PEER)
        {
            /* Clock supports Peer to Peer delay mechanism. Use Peer mean
             * Path delay for calculation.
             * Refer (c) of Section 11.2 of IEEE 1588 2008
             * */
            /* The value of meanPathDelay will be interms of nanosec */
            FSAP_U8_ASSIGN (&(u8MeanPathDelay),
                            &(pPtpPort->PortDs.u8PeerMeanPathDelay));
            FSAP_U8_ASSIGN (&(pPtpCurrentDs->u8MeanPathDelay),
                            &(pPtpPort->PortDs.u8PeerMeanPathDelay));
            pPtpCurrentDs->i1DelaySign = pPtpPort->PortDs.i1PeerDelaySign;

            PtpUtil8BytesSubtraction (pPtpCurrentDs);
        }
        else
        {
            /* Clock supports delay request response mechanism. Use mean
             * Path delay for calculation.
             * Refer (c) of Section 11.2 of IEEE 1588 2008
             * */

            /* The value of meanPathDelay will be interms of nanosec */

            FSAP_U8_ASSIGN (&(u8MeanPathDelay),
                            &(pPtpCurrentDs->u8MeanPathDelay));
            PtpUtil8BytesSubtraction (pPtpCurrentDs);

        }
        /* This is Sync message. Hence there is no need to pass OriginTimestamp 
         * as one of the arguments for synchronizing/Syntonizing the 
         * local clock */
        PtpClkSyntonizeClk (pPtpPort, NULL);

        u1ClockPgm = OSIX_TRUE;
    }
    else if ((PtpHdrInfo.u1MsgType & PTP_FOLLOW_UP_MSG) != PTP_FOLLOW_UP_MSG)
    {
        /* The transmitter is a two step clock and the transmitter has not
         * egressed the follow up message. Donot process this message
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Sync message received from "
                  "two step clock for port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Ignoring the sync message and waiting "
                  "for the follow up message......\r\n"));

    }
    else if ((PtpHdrInfo.u1MsgType & PTP_FOLLOW_UP_MSG) == PTP_FOLLOW_UP_MSG)
    {
        /* Received follow up for a sync message. Process the follow up message
         * Refer Section 11.2 of IEEE Std 1588 2008.
         * <offsetFromMaster> = <syncEventIngressTimeStamp> - 
         *                      <preciseOriginTimeStamp> - <meanPathDelay> -
         *                      <Correction Field of Sync Message>
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Follow up message received from "
                  "two step clock on port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Calculating Offset From Master...\r\n"));

        /* Adding the correction field of the followup message with the 
           correction field of the sync message */
        FSAP_U8_ADD (&(pPtpPort->u8CorrectionField),
                     &PtpHdrInfo.u8CorrectionField,
                     &(pPtpPort->u8CorrectionField));

        /* Obtaining Sync Origin Time stamp */
        pu1Tmp = pu1Pdu + PTP_SYNC_TIME_STAMP_OFFSET;
        PTP_LBUF_GET_2_BYTES (pu1Tmp, u2Val);
        UINT8_HI (&(OriginTimeStamp.u8Sec)) = (UINT4) u2Val;

        PTP_LBUF_GET_4_BYTES (pu1Tmp, u4Val);
        UINT8_LO (&(OriginTimeStamp.u8Sec)) = u4Val;

        u4Val = 0;
        PTP_LBUF_GET_4_BYTES (pu1Tmp, u4Val);
        OriginTimeStamp.u4Nsec = u4Val;

        if (pPtpPort->bDelayReqInProgress == OSIX_FALSE)
        {
            FSAP_U8_ASSIGN (&(pPtpPort->IngressSyncTimeStamp.u8Sec),
                            &(OriginTimeStamp.u8Sec));

            pPtpPort->IngressSyncTimeStamp.u4Nsec = OriginTimeStamp.u4Nsec;

        }

        PtpClkSubtractTimeStamps (&(pPtpPort->SyncReceptTime),
                                  &(OriginTimeStamp),
                                  &(FsClkDiff), &(pPtpCurrentDs->i1OffsetSign));

        /* Converting the Seconds to NanoSeconds */
        FSAP_U8_CLR (&u8TempOffSetfromMaster);
        FSAP_U8_MUL (&(u8TempOffSetfromMaster),
                     &(FsClkDiff.u8Sec), &(u8NanoSecValue));
        FSAP_U8_ASSIGN_LO (&u8NsecDiff, FsClkDiff.u4Nsec);
        FSAP_U8_ADD (&(u8TempOffSetfromMaster), &(u8TempOffSetfromMaster),
                     &(u8NsecDiff));
        /* Update the Master to Slave delay */
        PtpClkUpdtMasterToSlaveDelay (pPtpPort, &(u8TempOffSetfromMaster));
        FSAP_U8_ASSIGN (&(pPtpCurrentDs->u8OffsetFromMaster),
                        &u8TempOffSetfromMaster);

        /* Subtracting the correction field */
        if (pPtpCurrentDs->i1OffsetSign == PTP_NEGATIVE)
        {
            FSAP_U8_ADD (&(pPtpCurrentDs->u8OffsetFromMaster),
                         &(pPtpCurrentDs->u8OffsetFromMaster),
                         &(PtpHdrInfo.u8CorrectionField));
        }
        else
        {
            PtpClkPerf8ByteSubtraction (&(pPtpCurrentDs->u8OffsetFromMaster),
                                        &(PtpHdrInfo.u8CorrectionField),
                                        &(pPtpCurrentDs->u8OffsetFromMaster),
                                        &(pPtpCurrentDs->i1OffsetSign));
        }

        /* Subtracting the mean path delay */
        if (pPtpPort->PortDs.u1DelayMechanism ==
            PTP_PORT_DELAY_MECH_PEER_TO_PEER)
        {
            /* Clock supports Peer to Peer delay mechanism. Use Peer mean
             * Path delay for calculation.
             * Refer (c) of Section 11.2 of IEEE 1588 2008
             * */
            FSAP_U8_ASSIGN (&u8MeanPathDelay,
                            &(pPtpPort->PortDs.u8PeerMeanPathDelay));
            FSAP_U8_ASSIGN (&(pPtpCurrentDs->u8MeanPathDelay),
                            &(pPtpPort->PortDs.u8PeerMeanPathDelay));
            pPtpCurrentDs->i1DelaySign = pPtpPort->PortDs.i1PeerDelaySign;
            PtpUtil8BytesSubtraction (pPtpCurrentDs);
        }
        else
        {
            /* Clock supports delay request response mechanism. Use mean
             * Path delay for calculation.
             * Refer (c) of Section 11.2 of IEEE 1588 2008
             * */
            FSAP_U8_ASSIGN (&u8MeanPathDelay,
                            &(pPtpCurrentDs->u8MeanPathDelay));
            PtpUtil8BytesSubtraction (pPtpCurrentDs);
        }

        /* This is a follow up message. Pass the exact OriginTime stamp
         * as one of the arguments for synchronizing/Syntonizing the 
         * local clock */
        PtpClkSyntonizeClk (pPtpPort, &(pPtpPort->u8CorrectionField));

        u1ClockPgm = OSIX_TRUE;
    }

    /* Update the log Message interval received over message */
    PtpSyncUpdtSyncTmr (pPtpPort, &PtpHdrInfo);
    if (u1ClockPgm != OSIX_TRUE)
    {
        /* Clock should not be programmed */
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }
    /* We have received a sync message and has processed the same. Now, we need 
     * to trigger the delay request timer, as the mean path delay should be used
     * */
    if (pPtpPort->PortDs.u1DelayMechanism != PTP_PORT_DELAY_MECH_PEER_TO_PEER)
    {
        if (PtpDreqStartDelayReqAndTxPkt (pPtpPort) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpSyncHandleSyncMsg: "
                      "PtpSyncUpdtSyncTmr returned Failure!!!\r\n"));
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Unable to update logSyncInterval....\r\n"));

            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
    }
    /* Clock is updated at the last. This is done so because, had we programmed
     * clock earlier, then the tx time stamp of Delay request message will lose
     * its significance in the current time
     * */
    PtpClkSynchornize (pPtpPort);

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpSyncTxSyncMsg                              */
/*                                                                           */
/* Description               : This routine transmits an SYNC message over   */
/*                             the given interface.                          */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure.     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpSyncTxSyncMsg (tPtpPort * pPtpPort)
{
    UINT1              *pu1PtpPdu = NULL;
    UINT1              *pu1TmpPdu = NULL;
    tClkSysTimeInfo     PtpSysTimeInfo;
    tPtpTxParams        PtpTxParams;
    UINT4               u4MsgLen = PTP_SYNC_MSG_SIZE;

    PTP_FN_ENTRY ();

    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    MEMSET (&PtpTxParams, 0, sizeof (tPtpTxParams));

#ifdef L2RED_WANTED
    /*only in Slave mode packet need to send out
     *otherwise no need to process the packet */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        if (pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }
    }
#endif

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Transmitting Sync Message over interface %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    if ((pPtpPort->PortDs.u1PortState != PTP_STATE_MASTER) &&
        ((PtpUtilGetAltMstFlagStatus (pPtpPort)) == OSIX_FALSE))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Sync message cannot be transmitted over"
                  "non-master port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Not transmitting sync"
                  "message through Port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    pu1PtpPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                               PTP_MAX_PDU_LEN);
    if (pu1PtpPdu == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                  "Unable to allocate memory for transmitting pkt over "
                  "port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (pu1PtpPdu, 0, PTP_MAX_PDU_LEN);
    pu1TmpPdu = pu1PtpPdu;

    PtpHdrAddMediaHdrForIntf (&pu1TmpPdu, pPtpPort, PTP_SYNC_MSG, &u4MsgLen);

    /* Refer Table 26 of IEEE 1588-2008 for Sync Message format
     * */
    PtpHdrFillPtpHeader (&pu1TmpPdu, pPtpPort, PTP_SYNC_MSG);

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Filling PTP Header for Sync Message "
              "transmitted over port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PtpTxParams.pu1PtpPdu = pu1PtpPdu;
    PtpTxParams.pPtpPort = pPtpPort;
    PtpTxParams.u4MsgLen = u4MsgLen;
    PtpTxParams.u1MsgType = (UINT1) PTP_SYNC_MSG;

    /* Time stamp will be added in the PTP TX Sub module */
    if (PtpTxTransmitPtpMessage (&PtpTxParams) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpTxTransmitPtpMessage returned "
                  "Failure!!!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpSyncUpdtSyncTmr                            */
/*                                                                           */
/* Description               : This routine updates the value of the sync    */
/*                             timer as provided by the master and           */
/*                             restarts the timer whenever necessary.        */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                             pPtpHdrInfo - Pointer to header structure     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpSyncUpdtSyncTmr (tPtpPort * pPtpPort, tPtpHdrInfo * pPtpHdrInfo)
{
    PTP_FN_ENTRY ();

    /* The log message interval represented in the sync message 
     * corresponds to the value of portDs.logSyncInterval.
     * Refer Table 24 - Values of logMessageInterval field of IEEE Std 
     * 1588 2008.
     * Updating the same.
     * */

    if (pPtpHdrInfo->u1LogMsgInterval == PTP_UNICAST_LOG_MSG_INTERVAL)
    {
        /* For unicast messages, the value transmitted will always be
         * 0x7F. Hence ignore such messages.
         * */
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if (pPtpPort->PortDs.i1SyncInterval == (INT1) pPtpHdrInfo->u1LogMsgInterval)
    {
        /* Already sync timer is running for the same value. No 
         * need to do a restart.
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Sync timer value already in sync "
                  "with Master for port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Hence not updating the "
                  "portlogSyncInterval.....\r\n"));

        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Updating the logMessageInterval\r\n"));

    pPtpPort->PortDs.i1SyncInterval = (INT1) pPtpHdrInfo->u1LogMsgInterval;

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpSyncInitSyncHandler                        */
/*                                                                           */
/* Description               : This routine initialses the sync handler.     */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpSyncInitSyncHandler (VOID)
{
    PTP_FN_ENTRY ();

    /* Initialize the message handler function pointer */
    gPtpGlobalInfo.PtpMsgFn[PTP_SYNC_FN_PTR].pMsgRxHandler =
        PtpSyncHandleRxSyncMsg;
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpSyncTransmitFollowUp                       */
/*                                                                           */
/* Description               : This routine transmits a FOLLOWUP for the sync*/
/*                             message transmitted earlier over the interface*/
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure.     */
/*                             pPtpSysTimeInfo - Pointer to time stamp.      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpSyncTransmitFollowUp (tPtpPort * pPtpPort, tClkSysTimeInfo * pPtpSysTimeInfo)
{
    UINT1              *pu1PtpPdu = NULL;
    UINT1              *pu1TmpPdu = NULL;
    UINT4               u4MsgLen = PTP_SYNC_MSG_SIZE;
    UINT2               u2TwoByteVal = 0;
    tPtpTxParams        PtpTxParams;

    PTP_FN_ENTRY ();

#ifdef L2RED_WANTED
    /*only in Slave mode packet need to send out
     *otherwise no need to process the packet */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        if (pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }
    }
#endif

    MEMSET (&PtpTxParams, 0, sizeof (tPtpTxParams));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Transmitting Follow up for Sync Message over interface %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    /* Donot perform any of the checks. This should not be invoked during
     * invalid conditions. Means, this function would have been called only 
     * when a valid sync message was transmitted.
     * */
    pu1PtpPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                               PTP_MAX_PDU_LEN);
    if (pu1PtpPdu == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                  "Unable to allocate memory for transmitting pkt over "
                  "port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    pu1TmpPdu = pu1PtpPdu;

    /* Refer Table 27 of IEEE 1588-2008 for Follow up Message format
     * */
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Filling PTP Header for Follow up "
              "transmitted over port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PtpHdrAddMediaHdrForIntf (&pu1TmpPdu, pPtpPort, PTP_FOLLOW_UP_MSG,
                              &u4MsgLen);

    PtpHdrFillPtpHeader (&pu1TmpPdu, pPtpPort, PTP_FOLLOW_UP_MSG);

    /* Precise Origin Time stamp needs to be added after header */
    u2TwoByteVal = (UINT2) (UINT8_HI (&(pPtpSysTimeInfo->FsClkTimeVal.u8Sec)));
    PTP_LBUF_PUT_2_BYTES (pu1TmpPdu, u2TwoByteVal);
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu,
                          (UINT8_LO (&(pPtpSysTimeInfo->FsClkTimeVal.u8Sec))));
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, (pPtpSysTimeInfo->FsClkTimeVal.u4Nsec));

    PtpTxParams.pu1PtpPdu = pu1PtpPdu;
    PtpTxParams.pPtpPort = pPtpPort;
    PtpTxParams.u4MsgLen = u4MsgLen;
    PtpTxParams.u1MsgType = (UINT1) PTP_FOLLOW_UP_MSG;

    if (PtpTxTransmitPtpMessage (&PtpTxParams) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpTxTransmitPtpMessage returned "
                  "Failure!!!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif /* _PTPSYNC_C_ */
