/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpcli.c,v 1.15 2016/04/05 13:24:00 siva Exp $
 *
 * Description: This file contains the PTP CLI related routines and 
 * utility functions.
 *****************************************************************************/

#ifndef _PTPCLI_C_
#define _PTPCLI_C_

#include "ptpincs.h"
#include "ptpclipt.h"
#include "fsptpcli.h"
#include "ptpsrcpt.h"

/***************************************************************************
 * FUNCTION NAME             : cli_process_ptp_cmd 
 *
 * DESCRIPTION               : This function is exported to CLI module to 
 *                                handle the PTP cli commands to take the 
 *                                corresponding action. 
 *                             Only SNMP Low level routines are called 
 *                             from CLI.
 *
 * INPUT                     : Variable arguments
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 * 
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
cli_process_ptp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4               u4ErrCode;
    INT4                i4Inst;
    INT4                i4ContextId = 0;
    INT4                i4DomainId = PTP_V2_DEFAULT_DOMAIN_ID;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4PtpSysCtrl = PTP_SHUTDOWN;
    INT4                i4RecvValue = 0;
    UINT4               u4TempValue = 0;
    UINT4               u4PtpPortNo = 0;
    INT4                i4ATSOffset = 0;
    INT4                i4ATSJmpSecs = 0;
    UINT1              *args[PTP_CLI_MAX_ARGS];
    UINT1               au1IpAddr[PTP_MASTER_MAX_ADDR_LEN];
    CHR1               *Pu1Addr = NULL;
    INT1                i1ArgNo = 0;

    MEMSET (au1IpAddr, 0, sizeof (au1IpAddr));
    Pu1Addr = (CHR1 *) au1IpAddr;

    nmhGetFsPtpGlobalSysCtrl (&i4PtpSysCtrl);

    if ((u4Command != CLI_PTP_START) && (i4PtpSysCtrl == PTP_SHUTDOWN))
    {
        CliPrintf (CliHandle, "%% PTP is shutdown\r\n");
        return CLI_SUCCESS;
    }

    CliRegisterLock (CliHandle, PtpApiLock, PtpApiUnLock);

    if (PtpApiLock () == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%%PTP Error : Command not executed\r\n");
        return CLI_FAILURE;
    }

    if ((u4Command != CLI_PTP_VRF_START) && (u4Command != CLI_PTP_VRF_SHUTDOWN))
    {
        i4ContextId = CLI_GET_CXT_ID ();
        i4DomainId = CLI_GET_DOMAIN_ID ();
    }

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */

    i4Inst = va_arg (ap, INT4);

    /* Walk through the rest of the arguements and store in args array. 
     * Store PTP_CLI_MAX_ARGS arguements at the max. 
     */

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[i1ArgNo++] = va_arg (ap, UINT1 *);
        if (i1ArgNo == PTP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_PTP_SHUTDOWN:

            i4RetStatus = PtpCliSetGlobalSysControl (CliHandle, PTP_SHUTDOWN);
            break;

        case CLI_PTP_START:

            i4RetStatus = PtpCliSetGlobalSysControl (CliHandle, PTP_START);
            break;

        case CLI_PTP_PRIMARY_CXT:

            if (args[0] != NULL)
            {
                CliPrintf (CliHandle, "%% VRF is not supported\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            else if (args[2] != NULL)
            {
                if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[2]),
                                             (UINT4 *) &i4ContextId)
                    == OSIX_FALSE)
                {
                    CliPrintf (CliHandle, "%% Invalid Context\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }

            i4RetStatus = PtpCliSetPrimaryContext (CliHandle, i4ContextId);
            break;

        case CLI_PTP_PRIMARY_DOMAIN:
            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));
            i4RetStatus = PtpCliSetPrimaryDomain (CliHandle, i4ContextId,
                                                  i4RecvValue);
            break;

        case CLI_PTP_VRF_START:

            if (CLI_PTR_TO_U4 (args[0]) == CLI_PTP_L3_CONTEXT)
            {
                CliPrintf (CliHandle, "%% VRF is not supported\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if ((args[1] == NULL) && (args[2] == NULL))
            {
                i4RetStatus =
                    PtpCliConfigureContext (CliHandle, PTP_DEF_CONTEXT,
                                            PTP_V2_DEFAULT_DOMAIN_ID);
            }
            else if (args[1] == NULL)
            {
                MEMCPY (&i4DomainId, args[2], sizeof (INT4));
                i4RetStatus =
                    PtpCliConfigureContext (CliHandle, PTP_DEF_CONTEXT,
                                            i4DomainId);
            }
            else if (args[2] == NULL)
            {
                if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[1]),
                                             (UINT4 *) &i4ContextId)
                    == OSIX_FALSE)
                {
                    CliPrintf (CliHandle, "%% Invalid Context\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                i4RetStatus = PtpCliConfigureContext (CliHandle, i4ContextId,
                                                      PTP_V2_DEFAULT_DOMAIN_ID);
            }
            else
            {
                if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[1]),
                                             (UINT4 *) &i4ContextId)
                    == OSIX_FALSE)
                {
                    CliPrintf (CliHandle, "%% Invalid Context\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                MEMCPY (&i4DomainId, args[2], sizeof (INT4));
                i4RetStatus = PtpCliConfigureContext (CliHandle, i4ContextId,
                                                      i4DomainId);
            }

            if (i4RetStatus == CLI_FAILURE)
            {
                break;
            }

            if (CliChangePath ("(config-ptp)#") == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Unable to enter into ptp "
                           "configuration mode\r\n");
                i4RetStatus = CLI_FAILURE;
                break;

            }

            /* If mode is changed Successfully set the domain id and
               context id */

            CLI_SET_CXT_ID ((UINT4) i4ContextId);
            CLI_SET_DOMAIN_ID ((UINT4) i4DomainId);
            i4RetStatus = CLI_SUCCESS;

            break;

        case CLI_PTP_VRF_SHUTDOWN:

            if (CLI_PTR_TO_U4 (args[0]) == CLI_PTP_L3_CONTEXT)
            {
                CliPrintf (CliHandle, "%% VRF is not supported\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }

            /* When both Domain and context are not specified delete the 
             * all entries in default domain */

            if ((args[1] == NULL) && (args[2] == NULL))
            {
                i4RetStatus = PtpCliDeleteContext (CliHandle, PTP_DEF_CONTEXT);
            }

            /* When Only Domain is Given Delete that doamin in Default 
             * context */

            else if (args[1] == NULL)
            {
                MEMCPY (&i4RecvValue, args[2], sizeof (INT4));
                i4RetStatus = PtpCliDeleteDomain (CliHandle, PTP_DEF_CONTEXT,
                                                  i4RecvValue);
            }

            /* When Context is specified and no domain is specified 
             * delete all domians in the context */

            else if (args[2] == NULL)
            {
                if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[1]),
                                             (UINT4 *) &i4ContextId)
                    == OSIX_FALSE)
                {
                    CliPrintf (CliHandle, "%% Invalid Context\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                i4RetStatus = PtpCliDeleteContext (CliHandle, i4ContextId);
            }
            /* When Exact Domain and Context is specified delete 
             * the requested entry */

            else
            {
                if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[1]),
                                             (UINT4 *) &i4ContextId)
                    == OSIX_FALSE)
                {
                    CliPrintf (CliHandle, "%% Invalid Context\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
                MEMCPY (&i4RecvValue, args[2], sizeof (INT4));
                i4RetStatus = PtpCliDeleteDomain (CliHandle,
                                                  i4ContextId, i4RecvValue);
            }

            break;

        case CLI_PTP_VRF_ENABLE:

            if (args[0] != NULL)
            {
                CliPrintf (CliHandle, "%% VRF not supported\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            else if (args[1] != NULL)
            {
                if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[2]),
                                             (UINT4 *) &i4ContextId) ==
                    OSIX_FALSE)
                {
                    CliPrintf (CliHandle, "%% Invalid Context\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

            }
            else
            {
                i4ContextId = PTP_DEF_CONTEXT;
            }

            i4RetStatus = PtpCliSetSystemControl (CliHandle, i4ContextId,
                                                  PTP_ENABLED);

            break;

        case CLI_PTP_VRF_DISABLE:
            if (args[0] != NULL)
            {
                CliPrintf (CliHandle, "%% VRF not supported\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            else if (args[1] != NULL)
            {
                if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[2]),
                                             (UINT4 *) &i4ContextId) ==
                    OSIX_FALSE)
                {
                    CliPrintf (CliHandle, "%% Invalid Context\r\n");
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

            }
            else
            {
                i4ContextId = PTP_DEF_CONTEXT;
            }

            i4RetStatus = PtpCliSetSystemControl (CliHandle, i4ContextId,
                                                  PTP_DISABLED);

            break;

        case CLI_PTP_CLK_PORTS:

            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));
            i4RetStatus = PtpCliSetClockNumberPorts (CliHandle,
                                                     i4ContextId,
                                                     i4DomainId, i4RecvValue);
            break;

        case CLI_PTP_PORT_CREATE:

            if (PtpIfGetPortNumber (i4ContextId, (UINT1) i4DomainId,
                                    (CLI_PTR_TO_I4 (args[0])),
                                    (CLI_PTR_TO_I4 (args[1])),
                                    (INT4 *) &u4PtpPortNo) == OSIX_SUCCESS)
            {
                CliPrintf (CliHandle, "%% Entry already exsists\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            PtpIfGetNextAvailableIndex (i4ContextId, (UINT1) i4DomainId,
                                        &u4PtpPortNo);

            i4RetStatus = PtpCliMapPortToDomain (CliHandle,
                                                 i4ContextId, i4DomainId,
                                                 (CLI_PTR_TO_I4 (args[0])),
                                                 (CLI_PTR_TO_I4 (args[1])),
                                                 u4PtpPortNo);
            break;

        case CLI_PTP_PORT_DELETE:

            if (PtpIfGetPortNumber (i4ContextId, (UINT1) i4DomainId,
                                    (CLI_PTR_TO_I4 (args[0])),
                                    (CLI_PTR_TO_I4 (args[1])),
                                    (INT4 *) &u4PtpPortNo) == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, ALL_FAILURE_TRC,
                          "PtpIfGetPortNumber function fails \r\n"));
            }

            i4RetStatus = PtpCliDeletePortFromDomain (CliHandle,
                                                      i4ContextId, i4DomainId,
                                                      u4PtpPortNo);

            break;

        case CLI_PTP_TWO_STEP_CLK:

            i4RetStatus = PtpCliSetTwoStepFlag (CliHandle,
                                                i4ContextId,
                                                i4DomainId, PTP_ENABLED);
            break;

        case CLI_PTP_ONE_STEP_CLK:

            i4RetStatus = PtpCliSetTwoStepFlag (CliHandle,
                                                i4ContextId,
                                                i4DomainId,
                                                (INT4) PTP_DISABLED);
            break;

        case CLI_PTP_CLK_PRI_ONE:

            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));
            i4RetStatus = PtpCliSetClkPriority1 (CliHandle,
                                                 i4ContextId,
                                                 i4DomainId, i4RecvValue);
            break;

        case CLI_PTP_CLK_PRI_TWO:

            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));
            i4RetStatus = PtpCliSetClkPriority2 (CliHandle,
                                                 i4ContextId,
                                                 i4DomainId, i4RecvValue);
            break;

        case CLI_PTP_CLK_MODE:

            i4RetStatus = PtpCliSetClkMode (CliHandle,
                                            i4ContextId,
                                            i4DomainId,
                                            CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_PTP_CLK_SLAVE_ONLY:

            i4RetStatus = PtpCliSetClkSlaveOnly (CliHandle,
                                                 i4ContextId,
                                                 i4DomainId, PTP_ENABLED);
            break;

        case CLI_PTP_CLK_NO_SLAVE:

            i4RetStatus = PtpCliSetClkSlaveOnly (CliHandle,
                                                 i4ContextId,
                                                 i4DomainId, PTP_DISABLED);
            break;

        case CLI_PTP_SECURITY_ENABLE:

            i4RetStatus = PtpCliConfigSecurity (CliHandle,
                                                i4ContextId,
                                                i4DomainId, PTP_ENABLED);
            break;

        case CLI_PTP_SECURITY_DISABLE:

            i4RetStatus = PtpCliConfigSecurity (CliHandle,
                                                i4ContextId,
                                                i4DomainId, PTP_DISABLED);
            break;

        case CLI_PTP_SECURITY_MAX_SA:

            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));
            i4RetStatus = PtpCliConfigSetMaxSA (CliHandle,
                                                i4ContextId,
                                                i4DomainId, i4RecvValue);
            break;

        case CLI_PTP_PATH_TRC_ENABLE:

            i4RetStatus = PtpCliConfigPathTrace (CliHandle,
                                                 i4ContextId,
                                                 i4DomainId, PTP_ENABLED);
            break;

        case CLI_PTP_PATH_TRC_DISABLE:

            i4RetStatus = PtpCliConfigPathTrace (CliHandle,
                                                 i4ContextId,
                                                 i4DomainId, PTP_DISABLED);
            break;

        case CLI_PTP_ALT_TIME_SCALE_ENABLE:

            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));
            i4RetStatus = PtpCliUpdateAltTimeScaleEntry (CliHandle,
                                                         i4ContextId,
                                                         i4DomainId,
                                                         i4RecvValue, ACTIVE);

            break;

        case CLI_PTP_ALT_TIME_SCALE_DISABLE:

            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));
            i4RetStatus = PtpCliUpdateAltTimeScaleEntry (CliHandle,
                                                         i4ContextId,
                                                         i4DomainId,
                                                         i4RecvValue,
                                                         NOT_IN_SERVICE);
            break;

        case CLI_PTP_ALT_TIME_SET_NAME:

            MEMCPY (&u4TempValue, args[0], sizeof (UINT4));
            i4RetStatus = PtpCliConfigAltTimeScale (CliHandle,
                                                    i4ContextId,
                                                    i4DomainId, u4TempValue);
            /* If Display Name has to be Configured */

            MEMCPY (&u4TempValue, args[0], sizeof (UINT4));
            PtpCliSetAltTimeDispName (CliHandle, i4ContextId,
                                      i4DomainId,
                                      u4TempValue, (UINT1 *) (args[1]));
            break;

        case CLI_PTP_ALT_TIME_NO_SET_NAME:

            MEMCPY (&u4TempValue, args[0], sizeof (UINT4));
            /* Set the Default Name */
            i4RetStatus =
                PtpCliNoSetAltTimeDispName (CliHandle, i4ContextId,
                                            i4DomainId,
                                            u4TempValue,
                                            (UINT1 *) PTP_ALT_TIME_DEF_NAME);
            break;

        case CLI_PTP_UCAST_MAX_MASTERS:

            MEMCPY (&u4TempValue, args[0], sizeof (UINT4));
            i4RetStatus = PtpCliConfigMaxUnicastMasters (CliHandle,
                                                         i4ContextId,
                                                         i4DomainId,
                                                         u4TempValue);
            break;
        case CLI_PTP_ALT_TIME_SCALE:

            MEMCPY (&u4TempValue, args[0], sizeof (UINT4));
            i4RetStatus = PtpCliConfigAltTimeScale (CliHandle,
                                                    i4ContextId,
                                                    i4DomainId, u4TempValue);

            if (args[1] != NULL)
            {
                MEMCPY (&i4ATSJmpSecs, args[1], sizeof (INT4));

            }
            if (args[2] != NULL)
            {
                MEMCPY (&i4ATSJmpSecs, args[2], sizeof (INT4));
            }

            MEMCPY (&i4RecvValue, args[0], sizeof (INT4));
            i4RetStatus = PtpCliUpdateATSEntryOptParams (CliHandle,
                                                         i4ContextId,
                                                         i4DomainId,
                                                         i4RecvValue,
                                                         i4ATSOffset,
                                                         i4ATSJmpSecs,
                                                         (UINT1 *) (args[3]));
            break;

        case CLI_PTP_NO_ALT_TIME_SCALE:

            MEMCPY (&u4TempValue, args[0], sizeof (UINT4));
            i4RetStatus = PtpCliDelAltTimeScale (CliHandle, i4ContextId,
                                                 i4DomainId, u4TempValue);
            break;

        case CLI_PTP_ACC_MASTER:

            /* args[0] - Network Protocol 
             * args[1] - Ip address / Mac Address
             * args[2] - Alternate Priority */

            /* if IPv4 The Entered Address will be a UINT4 
             * so Convert it to String */

            if (CLI_PTR_TO_U4 (args[0]) == CLI_PTP_IPV4)
            {
                MEMCPY (&u4TempValue, args[1], sizeof (UINT4));
                CLI_CONVERT_IPADDR_TO_STR (Pu1Addr, u4TempValue);
            }
            else
            {
                Pu1Addr = (CHR1 *) (args[1]);

            }

            i4RetStatus = PtpCliConfigAccMaster (CliHandle,
                                                 i4ContextId,
                                                 i4DomainId,
                                                 CLI_PTR_TO_U4 (args[0]),
                                                 STRLEN (Pu1Addr), Pu1Addr);
            /* If Accept Master Priority has to be configured */
            if (args[2] != NULL)
            {
                MEMCPY (&i4RecvValue, args[2], sizeof (INT4));
                i4RetStatus =
                    PtpCliSetAccMasterAltPriority (CliHandle, i4ContextId,
                                                   i4DomainId,
                                                   CLI_PTR_TO_U4 (args[0]),
                                                   STRLEN (Pu1Addr),
                                                   (UINT1 *) Pu1Addr,
                                                   i4RecvValue);
            }

            break;

        case CLI_PTP_DEL_ACC_MASTER:

            /* args[0] - Network Protocol 
             * args[1] - Ip address / Mac Address
             * args[2] - Alternate Priority  Flag */

            /* if IPv4 The Entered Address will be a UINT4 
             * so Convert it to String */

            if (CLI_PTR_TO_U4 (args[0]) == CLI_PTP_IPV4)
            {
                MEMCPY (&u4TempValue, args[1], sizeof (UINT4));
                CLI_CONVERT_IPADDR_TO_STR (Pu1Addr, u4TempValue);
            }
            else
            {
                Pu1Addr = (CHR1 *) (args[1]);

            }

            i4RetStatus = PtpCliDelAccMaster (CliHandle, i4ContextId,
                                              i4DomainId,
                                              CLI_PTR_TO_U4 (args[0]),
                                              STRLEN (Pu1Addr),
                                              (UINT1 *) Pu1Addr,
                                              CLI_PTR_TO_I4 (args[2]));
            break;

        case CLI_PTP_GBL_DEBUG:

            i4RetStatus = PtpCliSetDebugs (CliHandle, ((UINT1 *) (args[0])));

            break;

        case CLI_PTP_CXT_DEBUG:

            /* Assign Context Id and Domain Id as given in the command */
            if (args[0] != NULL)
            {
                CliPrintf (CliHandle, "%% VRF not supported\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            else
            {
                if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[2]),
                                             (UINT4 *) &i4ContextId) ==
                    OSIX_FALSE)
                {
                    CliPrintf (CliHandle, "%% Invalid Context\r\n");
                    i4RetStatus = CLI_FAILURE;
                }
                else
                {
                    i4RetStatus = PtpCliSetCxtDebugs (CliHandle, i4ContextId,
                                                      ((UINT1 *) (args[3])));
                }
            }

            break;

        case CLI_PTP_TRAP_ENABLE:

            i4RetStatus =
                PtpCliSetNotifyStatus (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                       (UINT1) CLI_ENABLE);
            break;

        case CLI_PTP_TRAP_DISABLE:

            i4RetStatus =
                PtpCliSetNotifyStatus (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                       (UINT1) CLI_DISABLE);
            break;

        default:

            CliPrintf (CliHandle, "\r%% PTP : Invalid command\r\n");
            PtpApiUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_PTP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gapc1PtpCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    PtpApiUnLock ();

    CliUnRegisterLock (CliHandle);

    UNUSED_PARAM (i4Inst);
    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : cli_process_ptp_if_cmd
 *
 * DESCRIPTION               : This function is exported to CLI module to
 *                             handle the PTP Cli Interface commands. 
 *
 * INPUT                     : Variable arguments
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
cli_process_ptp_if_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{

    va_list             ap;
    INT4                i4Inst;
    INT4                i4ContextId = 0;
    INT4                i4InterfaceId = 0;
    INT4                i4DomainId = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4PtpSysCtrl = PTP_SHUTDOWN;
    INT4                i4RecvValue = 0;
    UINT4               u4PtpPortNo = 0;
    UINT4               u4TempValue = 0;
    INT4                i4SyncInterval = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4IfType = 0;
    INT4                i4ClockMode = 0;
    UINT1              *args[PTP_CLI_MAX_ARGS];
    INT1                ai1Prompt[MAX_PROMPT_LEN];
    INT1                i1ArgNo = 0;

    /* Get PTP Global System Control */
    nmhGetFsPtpGlobalSysCtrl (&i4PtpSysCtrl);

    if (i4PtpSysCtrl == PTP_SHUTDOWN)
    {
        CliPrintf (CliHandle, "%% PTP is shutdown\r\n");
        return CLI_SUCCESS;
    }

    CliRegisterLock (CliHandle, PtpApiLock, PtpApiUnLock);

    if (PtpApiLock () == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%%PTP Error : Command not executed\r\n");
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        i4InterfaceId = (UINT4) i4Inst;
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store PTP_CLI_MAX_ARGS arguements at the max. 
     */

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[i1ArgNo++] = va_arg (ap, UINT1 *);
        if (i1ArgNo == PTP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    /* Get the Interface Type */
    i4IfType = (CLI_PTR_TO_I4 (args[0]));

    /*if type is PTP_IFACE_VLAN and mode is not vlan
     * then user executed command in if-mode*/

    if (CliGetCurModePromptStr (ai1Prompt) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "%% Fatal error\r\n");
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    if ((i4IfType == PTP_IFACE_VLAN) && (STRSTR (ai1Prompt, "vlan") == NULL))
    {

        /* Change Type to IEEE 802.3 */
        i4IfType = PTP_IFACE_IEEE_802_3;
        i4InterfaceId = CLI_GET_IFINDEX ();
    }

    /* Get  the Domain Id */
    i4DomainId = (CLI_PTR_TO_I4 (args[2]));

    /* If Interface Type is IPV4 or IPv6 it must be in
     * Default context but only when vrf is not supported */

    if ((i4IfType == PTP_IFACE_UDP_IPV4) || (i4IfType == PTP_IFACE_UDP_IPV6))
    {
        i4ContextId = 0;
    }
    else if (i4IfType == PTP_IFACE_VLAN)
    {
        i4ContextId = (CLI_PTR_TO_I4 (args[1]));

        if (i4ContextId == (INT4) PTP_CLI_INVALID_CONTEXT_ID)
        {
            i4ContextId = L2IWF_DEFAULT_CONTEXT;
        }
    }
    else if (PtpPortVcmGetCxtFromIfIndex (i4InterfaceId,
                                          (UINT4 *) &i4ContextId) ==
             OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "%% Interface is not mapped to any context\r\n");
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    if (PtpIfGetPortNumber (i4ContextId, (UINT1) i4DomainId,
                            i4IfType,
                            i4InterfaceId,
                            (INT4 *) &u4PtpPortNo) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "%% No such ptp port in context/domain\r\n");
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    switch (u4Command)
    {
        case CLI_PTP_ANNOUNCE_INTRVL:

            MEMCPY (&i4RecvValue, args[3], sizeof (INT4));
            i4RetStatus = PtpCliSetAnnounceInterval (CliHandle,
                                                     i4ContextId,
                                                     i4DomainId,
                                                     u4PtpPortNo, i4RecvValue);

            break;

        case CLI_PTP_ANNOUNCE_TIME_OUT:

            MEMCPY (&i4RecvValue, args[3], sizeof (INT4));
            i4RetStatus = PtpCliSetAnnounceTimeOut (CliHandle,
                                                    i4ContextId,
                                                    i4DomainId,
                                                    u4PtpPortNo, i4RecvValue);
            break;

        case CLI_PTP_DELAY_REQ_INTRVL:

            /* Get the Clock Mode and then configure Port */
            i4RetStatus = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                                      &i4ClockMode);
            if (i4RetStatus != SNMP_SUCCESS)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
            {
                MEMCPY (&i4RecvValue, args[3], sizeof (INT4));
                i4RetStatus = PtpCliSetDelayReqInterval (CliHandle,
                                                         i4ContextId,
                                                         i4DomainId,
                                                         u4PtpPortNo,
                                                         i4RecvValue,
                                                         OSIX_TRUE);
            }
            else
            {
                MEMCPY (&i4RecvValue, args[3], sizeof (INT4));
                i4RetStatus = PtpCliSetDelayReqInterval (CliHandle,
                                                         i4ContextId,
                                                         i4DomainId,
                                                         u4PtpPortNo,
                                                         i4RecvValue,
                                                         OSIX_FALSE);
            }
            break;

        case CLI_PTP_PORT_ENABLE:

            /* Get the Clock Mode and then configure Port */
            i4RetStatus = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                                      &i4ClockMode);
            if (i4RetStatus != SNMP_SUCCESS)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
            {
                /*Configure Transpaarent Port Admin Status */
                i4RetStatus = PtpCliSetTransPortAdminStatus (CliHandle,
                                                             i4ContextId,
                                                             i4DomainId,
                                                             u4PtpPortNo,
                                                             PTP_ENABLED);
            }
            else
            {

                i4RetStatus = PtpCliSetPortAdminStatus (CliHandle,
                                                        i4ContextId,
                                                        i4DomainId,
                                                        u4PtpPortNo,
                                                        PTP_ENABLED);
            }
            break;

        case CLI_PTP_PORT_DISABLE:

            /* Get the Clock Mode and then configure Port */
            i4RetStatus = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                                      &i4ClockMode);
            if (i4RetStatus != SNMP_SUCCESS)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
            {
                /*Configure Transpaarent Port Admin Status */
                i4RetStatus = PtpCliSetTransPortAdminStatus (CliHandle,
                                                             i4ContextId,
                                                             i4DomainId,
                                                             u4PtpPortNo,
                                                             PTP_DISABLED);
            }
            else
            {

                i4RetStatus = PtpCliSetPortAdminStatus (CliHandle,
                                                        i4ContextId,
                                                        i4DomainId,
                                                        u4PtpPortNo,
                                                        PTP_DISABLED);
            }
            break;

        case CLI_PTP_SYNC_INTRVL:

            if (PtpCliValidateSyncInterval (CliHandle, (INT1 *) (args[3]),
                                            &i4SyncInterval) != CLI_FAILURE)
            {
                i4RetStatus = PtpCliSetSyncInterval (CliHandle, i4ContextId,
                                                     i4DomainId, u4PtpPortNo,
                                                     i4SyncInterval);
            }
            break;

        case CLI_PTP_DELAY_TYPE:

            MEMCPY (&i4RecvValue, args[3], sizeof (INT4));
            i4RetStatus = PtpCliSetDelayType (CliHandle,
                                              i4ContextId,
                                              i4DomainId,
                                              u4PtpPortNo, i4RecvValue);
            break;

        case CLI_PTP_VERSION:

            MEMCPY (&i4RecvValue, args[3], sizeof (INT4));
            i4RetStatus = PtpCliSetVersion (CliHandle,
                                            i4ContextId,
                                            i4DomainId,
                                            u4PtpPortNo, i4RecvValue);
            break;

        case CLI_PTP_SYNC_LIMIT:

            if (PtpCliValidateSyncLimit (CliHandle, (INT1 *) (args[3])) !=
                CLI_FAILURE)
            {
                i4RetStatus = PtpCliSetSyncLimit (CliHandle,
                                                  i4ContextId,
                                                  i4DomainId,
                                                  u4PtpPortNo,
                                                  (UINT1 *) (args[3]));
            }

            break;

        case CLI_PTP_ACCEPT_MASTER_ENABLE:

            PtpCliSetAccMasterFlag (CliHandle, i4ContextId,
                                    i4DomainId, u4PtpPortNo, PTP_ENABLED);
            break;

        case CLI_PTP_ACCEPT_MASTER_DISABLE:

            PtpCliSetAccMasterFlag (CliHandle, i4ContextId,
                                    i4DomainId, u4PtpPortNo, PTP_DISABLED);
            break;

        case CLI_PTP_ALT_MASTER_ENABLE:

            i4RetStatus = PtpCliSetAltMasterFlag (CliHandle,
                                                  i4ContextId,
                                                  i4DomainId,
                                                  u4PtpPortNo, PTP_ENABLED);
            break;

        case CLI_PTP_ALT_MASTER_DISABLE:

            i4RetStatus = PtpCliSetAltMasterFlag (CliHandle,
                                                  i4ContextId,
                                                  i4DomainId,
                                                  u4PtpPortNo, PTP_DISABLED);
            break;

        case CLI_PTP_ALT_MASTER_SYNC_INTVL:

            MEMCPY (&u4TempValue, args[3], sizeof (UINT4));
            i4RetStatus = PtpCliSetAltMcastSyncInterval (CliHandle,
                                                         i4ContextId,
                                                         i4DomainId,
                                                         u4PtpPortNo,
                                                         u4TempValue);
            break;

        case CLI_MAX_ALT_MASTERS:

            MEMCPY (&u4TempValue, args[3], sizeof (UINT4));
            i4RetStatus = PtpCliSetPortNumOfAlternateMasters (CliHandle,
                                                              i4ContextId,
                                                              i4DomainId,
                                                              u4PtpPortNo,
                                                              u4TempValue);
            break;

        default:

            CliPrintf (CliHandle, "%% Invalid Command\r\n");
            break;
    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_PTP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gapc1PtpCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    PtpApiUnLock ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : cli_process_ptp_show_cmd
 *
 * DESCRIPTION               : This function is exported to CLI module to 
 *                                handle the PTP Cli show commands. The display 
 *                                is taken care in this function.
 *
 * INPUT                     : Variable arguments
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
cli_process_ptp_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[PTP_CLI_MAX_ARGS];
    INT1                i1ArgNo = 0;
    INT4                i4ContextId = PTP_DEF_CONTEXT;
    INT4                i4Inst = 0;
    INT4                i4DomainId = PTP_V2_DEFAULT_DOMAIN_ID;
    INT4                i4PtpSysCtrl = 0;
    INT4                i4RetVal = 0;
    INT4                i4ClockMode = 0;

    nmhGetFsPtpGlobalSysCtrl (&i4PtpSysCtrl);

    if (i4PtpSysCtrl == PTP_SHUTDOWN)
    {
        CliPrintf (CliHandle, "%% PTP is shutdown\r\n");
        return CLI_SUCCESS;
    }

    CliRegisterLock (CliHandle, PtpApiLock, PtpApiUnLock);

    if (PtpApiLock () == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% PTP Error : Command not Executed\r\n");
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */

    i4Inst = va_arg (ap, INT4);

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[i1ArgNo++] = va_arg (ap, UINT1 *);
        if (i1ArgNo == PTP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    /* Assign Context Id and Domain Id as given in the command */

    if (args[0] != NULL)
    {
        CliPrintf (CliHandle, "%% VRF not supported\r\n");
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    if ((args[2] == NULL) && (args[3] == NULL))
    {
        i4ContextId = PTP_DEF_CONTEXT;
        i4DomainId = PTP_V2_DEFAULT_DOMAIN_ID;
    }

    else if (args[2] == NULL)
    {
        i4ContextId = PTP_DEF_CONTEXT;
        MEMCPY (&i4DomainId, args[3], sizeof (INT4));
    }

    else if (args[3] == NULL)
    {
        if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[2]),
                                     (UINT4 *) &i4ContextId) == OSIX_FALSE)
        {
            CliPrintf (CliHandle, "%% Invalid Context\r\n");
            i4RetVal = CLI_FAILURE;
        }

        i4DomainId = PTP_V2_DEFAULT_DOMAIN_ID;
    }
    else
    {
        if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[2]),
                                     (UINT4 *) &i4ContextId) == OSIX_FALSE)
        {
            CliPrintf (CliHandle, "%% Invalid Context\r\n");
            i4RetVal = CLI_FAILURE;
        }
        MEMCPY (&i4DomainId, args[3], sizeof (INT4));
    }

    if (i4RetVal == CLI_FAILURE)
    {
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    /* Cases for Show commands */

    switch (u4Command)
    {
        case CLI_PTP_SHOW_CLOCK:

            PtpCliShowClock (CliHandle, i4ContextId, i4DomainId);
            break;

        case CLI_PTP_SHOW_FM_CLK:

            PtpCliShowForeignMaster (CliHandle, i4ContextId, i4DomainId);
            break;

        case CLI_PTP_SHOW_PORT:

            if (CLI_PTR_TO_I4 (args[5]) != PTP_IFACE_UNKNOWN)
            {
                /* In this case, port entry is given directly in the show
                 * command. Domain id will be given in as one of the
                 * input. Get the context id based on the port type
                 * If port type is IPv4 or IPv6, default context id is used
                 * If port type is VLAN, get the context id from args[6]
                 * If port type is physical, get the context id from VCM
                 */
                if ((CLI_PTR_TO_I4 (args[5]) == PTP_IFACE_UDP_IPV4) ||
                    (CLI_PTR_TO_I4 (args[5]) == PTP_IFACE_UDP_IPV6))
                {
                    i4ContextId = PTP_DEF_CONTEXT;
                }
                else if (CLI_PTR_TO_I4 (args[5]) == PTP_IFACE_IEEE_802_3)
                {
                    if (PtpPortVcmGetCxtFromIfIndex (CLI_PTR_TO_U4 (args[4]),
                                                     (UINT4 *) &i4ContextId)
                        == OSIX_FAILURE)
                    {
                        i4RetVal = CLI_FAILURE;
                        break;
                    }
                }
                else
                {
                    if (args[6] == NULL)
                    {
                        i4ContextId = PTP_DEF_CONTEXT;
                    }
                    else
                    {
                        if (PtpPortVcmIsSwitchExist ((UINT1 *) (args[6]),
                                                     (UINT4 *) &i4ContextId)
                            == OSIX_FALSE)
                        {
                            i4RetVal = CLI_FAILURE;
                            break;
                        }
                    }
                }
            }

            /* Get the Clock Mode and then show Port */
            i4RetVal = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                                   &i4ClockMode);
            if (i4RetVal != SNMP_SUCCESS)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
            {
                /*Display TranspaarentPort information */
                PtpCliShowTransClkPort (CliHandle, i4ContextId, i4DomainId,
                                        CLI_PTR_TO_U4 (args[4]),
                                        CLI_PTR_TO_I4 (args[5]));
            }
            else
            {
                /*Display General Port information */
                PtpCliShowPort (CliHandle, i4ContextId, i4DomainId,
                                CLI_PTR_TO_U4 (args[4]),
                                CLI_PTR_TO_I4 (args[5]));
            }
            break;

        case CLI_PTP_SHOW_PORT_COUNTERS:

            /* Display PTP Port Counters */
            PtpCliShowPortCounters (CliHandle, i4ContextId, i4DomainId);
            break;

        case CLI_PTP_SHOW_TIME_PROPERTY:

            PtpCliShowTimeProperty (CliHandle, i4ContextId, i4DomainId);
            break;

        case CLI_PTP_SHOW_CURRENT_DS:

            PtpCliShowCurrentDS (CliHandle, i4ContextId, i4DomainId);
            break;

        case CLI_PTP_SHOW_SA:

            PtpCliShowSecurityAssociations (CliHandle, i4ContextId, i4DomainId);
            break;

        case CLI_PTP_SHOW_ACC_MASTERS:

            PtpCliShowAcceptableMstRecords (CliHandle, i4ContextId, i4DomainId);
            break;

        case CLI_PTP_SHOW_PARENT_STATS:

            PtpCliShowGrandMasterRecords (CliHandle, i4ContextId, i4DomainId);
            break;

        case CLI_PTP_SHOW:

            PtpCliShowDebugging (CliHandle, i4ContextId);
            PtpCliGetContextInfoForShowCmd (CliHandle, NULL,
                                            (UINT4) i4ContextId,
                                            (UINT4 *) &i4ContextId,
                                            (UINT4) i4ContextId);

            break;

        case CLI_PTP_SHOW_VRF:

            PtpCliShowContextInfo (CliHandle, i4ContextId);

            break;

        case CLI_PTP_SHOW_GLOBAL:

            PtpCliShowGlobalInfo (CliHandle);
            break;

        case CLI_PTP_SHOW_UNICAST_MASTERS:

            PtpCliShowUnicastMasterRecords (CliHandle, i4ContextId, i4DomainId);
            break;

        case CLI_PTP_SHOW_ALT_TIME_SCALE:

            PtpCliShowAlternateTimeScale (CliHandle, i4ContextId, i4DomainId);
            break;

        default:

            CliPrintf (CliHandle, "\r%%PTP : Invalid Command \r\n");
            PtpApiUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    CLI_SET_ERR (0);

    CLI_SET_CMD_STATUS (i4RetVal);

    PtpApiUnLock ();

    CliUnRegisterLock (CliHandle);

    UNUSED_PARAM (i4Inst);
    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : PtpGetPtpCfgPrompt
 *
 * DESCRIPTION      : This function returns the prompt to be displayed
 *                    for PTP. It is exported to CLI module.
 *                    This function will be invoked when the System is
 *                    running in SI mode.
 *
 * INPUT                     : None
 *
 *
 * OUTPUT                    : pi1ModeName - Mode String
 *                             pi1DispStr - Display string
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : TRUE/FALSE
 *
 **************************************************************************/
PUBLIC INT1
PtpGetPtpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("(config-ptp)#");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "(config-ptp)#", u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_ptp_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-ptp)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : PtpGetVcmPtpCfgPrompt
 *
 * DESCRIPTION      : This function returns the prompt to be displayed
 *                    for PTP. It is exported to CLI module.
 *                    This function will be invoked when the System is
 *                    running in MI mode.
 *
 * INPUT            : None
 *
 *
 * OUTPUT           : pi1ModeName - Mode String
 *                    pi1DispStr - Display string
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : TRUE/FALSE
 *
 **************************************************************************/
PUBLIC INT1
PtpGetVcmPtpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("ptp");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "ptp", u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_ptp_cmd.
     */

    STRCPY (pi1DispStr, "(config-ptp-vrf)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetGlobalSysControl
 *
 * DESCRIPTION      : This function sets the system control status of Ptp
 *                    module as configured from CLI.
 *
 * INPUT            : i4Status - System control status
 *                    (PTP_START/PTP_SHUTDOWN)
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetGlobalSysControl (tCliHandle CliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPtpGlobalSysCtrl (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpGlobalSysCtrl (i4Status) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetSystemControl
 *
 * DESCRIPTION      : This function sets the system control status of Ptp
 *                    module as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4Status - System control status
 *                    (PTP_START/PTP_SHUTDOWN)
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetSystemControl (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpAdminStatus (&u4ErrorCode,
                                   i4ContextId, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpAdminStatus (i4ContextId, i4Status) == SNMP_FAILURE)

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetTransPortAdminStatus
 *
 * DESCRIPTION      : This function sets the admin status of Ptp
 *                    port as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4Status - System control status
 *                    (PTP_START/PTP_SHUTDOWN)
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliSetTransPortAdminStatus (tCliHandle CliHandle, INT4 i4ContextId,
                               INT4 i4DomainId, UINT4 u4PtpPortNo,
                               INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if ((nmhTestv2FsPtpTransparentPortPtpStatus
         (&u4ErrorCode, i4ContextId, i4DomainId, u4PtpPortNo,
          i4Status) == SNMP_FAILURE))

    {
        return CLI_FAILURE;
    }

    if ((nmhSetFsPtpTransparentPortPtpStatus
         (i4ContextId, i4DomainId, u4PtpPortNo, i4Status) == SNMP_FAILURE))

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetPortAdminStatus
 *
 * DESCRIPTION      : This function sets the admin status of Ptp
 *                    port as configured from CLI.
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4Status - System control status
 *                    (PTP_START/PTP_SHUTDOWN)
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliSetPortAdminStatus (tCliHandle CliHandle, INT4 i4ContextId,
                          INT4 i4DomainId, UINT4 u4PtpPortNo, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if ((nmhTestv2FsPtpPortPtpStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                      u4PtpPortNo, i4Status) == SNMP_FAILURE))

    {
        return CLI_FAILURE;
    }
    if ((nmhSetFsPtpPortPtpStatus
         (i4ContextId, i4DomainId, u4PtpPortNo, i4Status) == SNMP_FAILURE))

    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpCliDeleteContext
 *
 * DESCRIPTION               : This function Deletes a PTP Context
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4Status - PTP Module status
 *                             PTP Module Disable / Enable
 * OUTPUT                    : None
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliDeleteContext (tCliHandle CliHandle, INT4 i4ContextId)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsPtpContextRowStatus (&u4ErrCode, i4ContextId,
                                        DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Delete the Context which will inturn delete all the
       doamins in the context */
    nmhSetFsPtpContextRowStatus (i4ContextId, DESTROY);

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpCliDeleteDomain
 *
 * DESCRIPTION               : This function Deletes a PTP Domain
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 * OUTPUT                    : None
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliDeleteDomain (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId)
{
    UINT4               u4ErrCode = 0;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrCode, i4ContextId,
                                       i4DomainId, DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId, DESTROY) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpCliConfigureContext
 *
 * DESCRIPTION               : This function sets the PTP module status.
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4Status - PTP Module status
 *                             PTP Module Disable / Enable
 * OUTPUT                    : None
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliConfigureContext (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId)
{

    UINT4               u4ErrorCode = 0;
    INT4                i4ContextRowStatus;
    INT4                i4DomainRowStatus;
    INT4                i4RetVal = SNMP_FAILURE;

    i4RetVal = nmhGetFsPtpContextRowStatus (i4ContextId, &i4ContextRowStatus);
    if (i4RetVal != SNMP_FAILURE)
    {
        i4RetVal = nmhGetFsPtpDomainRowStatus (i4ContextId, i4DomainId,
                                               &i4DomainRowStatus);
        if (i4RetVal != SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId,
                                           i4DomainId, CREATE_AND_GO)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId, CREATE_AND_GO)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    if (nmhTestv2FsPtpContextRowStatus
        (&u4ErrorCode, i4ContextId, CREATE_AND_GO) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpContextRowStatus (i4ContextId, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Configure System Control */

    if (PtpCliSetSystemControl (CliHandle, i4ContextId, PTP_ENABLED)
        == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*Configure Domain Row Status */

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId,
                                       i4DomainId, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetPrimaryContext
 *
 * DESCRIPTION               : This function sets the PTP Clock
 *                             Two Step Flag
 *
 * INPUT                     : i4ContextId - Context Identifier
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetPrimaryContext (tCliHandle CliHandle, INT4 i4PrimaryContext)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPrimaryContext (&u4ErrorCode, i4PrimaryContext)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPtpPrimaryContext (i4PrimaryContext) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetPrimaryDomain
 *
 * DESCRIPTION               : This function sets the PTP Clock
 *                             Two Step Flag
 *
 * INPUT                     : i4ContextId - Context Identifier
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetPrimaryDomain (tCliHandle CliHandle, INT4 i4ContextId,
                        INT4 i4PrimaryDomain)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPrimaryDomain (&u4ErrorCode, i4ContextId, i4PrimaryDomain)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPtpPrimaryDomain (i4ContextId, i4PrimaryDomain) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetTwoStepFlag
 *
 * DESCRIPTION               : This function sets the PTP Clock 
 *                             Two Step Flag
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4TwoStepFlag - Two Step Flag
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetTwoStepFlag (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                      INT4 i4TwoStepFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4ClockMode = 0;

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId,
                                       i4DomainId, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId, &i4ClockMode);

    if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        if (nmhTestv2FsPtpTransparentClockTwoStepFlag
            (&u4ErrorCode, i4ContextId,
             i4DomainId, i4TwoStepFlag) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPtpTransparentClockTwoStepFlag (i4ContextId,
                                                    i4DomainId, i4TwoStepFlag)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsPtpClockTwoStepFlag (&u4ErrorCode, i4ContextId,
                                            i4DomainId,
                                            i4TwoStepFlag) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPtpClockTwoStepFlag (i4ContextId,
                                         i4DomainId, i4TwoStepFlag)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId,
                                       i4DomainId, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId, ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetClkMode
 *
 * DESCRIPTION               : This function sets the PTP Clock Mode
 *                             
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4ClkMode - PTP Clock Mode
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetClkMode (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                  INT4 i4ClockMode)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4DelayMechanism = 0;

    if (i4ClockMode == CLI_PTP_ORDINARY_CLK)
    {
        i4ClockMode = PTP_OC_CLOCK_MODE;

    }
    else if (i4ClockMode == CLI_PTP_BOUNDARY_CLK)
    {
        i4ClockMode = PTP_BOUNDARY_CLOCK_MODE;
    }
    else if (i4ClockMode == CLI_PTP_E2ETRANS_CLK)
    {
        i4ClockMode = PTP_TRANSPARENT_CLOCK_MODE;
        i4DelayMechanism = PTP_PORT_DELAY_MECH_END_TO_END;
    }
    else if (i4ClockMode == CLI_PTP_P2PTRANS_CLK)
    {
        i4ClockMode = PTP_TRANSPARENT_CLOCK_MODE;
        i4DelayMechanism = PTP_PORT_DELAY_MECH_PEER_TO_PEER;
    }
    else if (i4ClockMode == CLI_PTP_FORWARD_ONLY_CLK)
    {
        i4ClockMode = PTP_FORWARD_MODE;
    }
    else if (i4ClockMode == CLI_PTP_MANAGEMENT_NODE)
    {
        i4ClockMode = PTP_MANAGEMENT_MODE;
    }

    if (nmhTestv2FsPtpDomainClockMode (&u4ErrorCode, i4ContextId,
                                       i4DomainId, i4ClockMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpDomainClockMode (i4ContextId,
                                    i4DomainId, i4ClockMode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                       ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId,
                                    ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        if (nmhTestv2FsPtpTransparentClockDelaymechanism
            (&u4ErrorCode, i4ContextId, i4DomainId,
             i4DelayMechanism) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsPtpTransparentClockDelaymechanism
            (i4ContextId, i4DomainId, i4DelayMechanism) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetClkPriority1
 *
 * DESCRIPTION               : This function sets the PTP Priority 1
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId  - Domain Identifier
 *                             i4ClkPriority1 - PTP Priority 1
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetClkPriority1 (tCliHandle CliHandle, INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4ClkPriority1)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpClockPriority1 (&u4ErrorCode, i4ContextId,
                                      i4DomainId,
                                      i4ClkPriority1) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpClockPriority1 (i4ContextId,
                                   i4DomainId, i4ClkPriority1) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetClkPriority2
 *
 * DESCRIPTION               : This function sets the PTP Priority 2
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4ClkPriority2 -  PTP Priority 2
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified :None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetClkPriority2 (tCliHandle CliHandle, INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4ClkPriority2)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpClockPriority2 (&u4ErrorCode, i4ContextId,
                                      i4DomainId,
                                      i4ClkPriority2) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpClockPriority2 (i4ContextId,
                                   i4DomainId, i4ClkPriority2) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetClkSlaveOnly
 *
 * DESCRIPTION               : This function sets the PTP Clock Slave only
 *                             flag
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId  - PTP Domain Identifier
 *                             i4SlaveOnly - PTP Slave Only Flag
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetClkSlaveOnly (tCliHandle CliHandle, INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4SlaveOnly)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpClockSlaveOnly (&u4ErrorCode, i4ContextId,
                                      i4DomainId, i4SlaveOnly) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpClockSlaveOnly (i4ContextId,
                                   i4DomainId, i4SlaveOnly) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetAnnounceInterval
 *
 * DESCRIPTION               : This function sets the PTP Announce Interval
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4AnnounceInterval - Announce Interval 
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetAnnounceInterval (tCliHandle CliHandle, INT4 i4ContextId,
                           INT4 i4DomainId,
                           INT4 i4PortId, INT4 i4AnnounceInterval)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortAnnounceInterval (&u4ErrorCode, i4ContextId,
                                            i4DomainId, i4PortId,
                                            i4AnnounceInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortAnnounceInterval (i4ContextId,
                                         i4DomainId, i4PortId,
                                         i4AnnounceInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetAnnounceTimeOut
 *
 * DESCRIPTION               : This function sets the PTP Announce Timeout
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4AnnounceTimeOut - Announce Timeout
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetAnnounceTimeOut (tCliHandle CliHandle, INT4 i4ContextId,
                          INT4 i4DomainId,
                          INT4 i4PortId, INT4 i4AnnounceTimeOut)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortAnnounceReceiptTimeout (&u4ErrorCode,
                                                  i4ContextId,
                                                  i4DomainId,
                                                  i4PortId,
                                                  i4AnnounceTimeOut)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortAnnounceReceiptTimeout (i4ContextId,
                                               i4DomainId,
                                               i4PortId,
                                               i4AnnounceTimeOut)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetDelayReqInterval
 *
 * DESCRIPTION               : This function sets the PTP delay req interval
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4DelayReqInterval - Delay Request Interval
 *                             u1IsTransPort  -  OSIX_TRU / OSIX_FALSE
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetDelayReqInterval (tCliHandle CliHandle, INT4 i4ContextId,
                           INT4 i4DomainId,
                           INT4 i4PortId, INT4 i4DelayReqInterval,
                           UINT1 u1IsTransPort)
{
    UINT4               u4ErrorCode = 0;

    if (u1IsTransPort == OSIX_TRUE)
    {
        if (nmhTestv2FsPtpTransparentPortMinPdelayReqInterval
            (&u4ErrorCode, i4ContextId, i4DomainId, i4PortId,
             i4DelayReqInterval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPtpTransparentPortMinPdelayReqInterval (i4ContextId,
                                                            i4DomainId,
                                                            i4PortId,
                                                            i4DelayReqInterval)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsPtpPortMinDelayReqInterval (&u4ErrorCode, i4ContextId,
                                                   i4DomainId, i4PortId,
                                                   i4DelayReqInterval)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPtpPortMinDelayReqInterval (i4ContextId,
                                                i4DomainId, i4PortId,
                                                i4DelayReqInterval) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetSyncInterval
 *
 * DESCRIPTION               : This function sets the PTP Sync Interval
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier 
 *                             i4SyncInterval - Sync Interval
 *                             
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetSyncInterval (tCliHandle CliHandle, INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4PortId, INT4 i4SyncInterval)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortSyncInterval (&u4ErrorCode, i4ContextId,
                                        i4DomainId,
                                        i4PortId,
                                        i4SyncInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortSyncInterval (i4ContextId,
                                     i4DomainId,
                                     i4PortId, i4SyncInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetDelayType
 *
 * DESCRIPTION               : This function sets the PTP Delay Type
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4DelayType - Delay Type 
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetDelayType (tCliHandle CliHandle, INT4 i4ContextId,
                    INT4 i4DomainId, INT4 i4PortId, INT4 i4DelayType)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortDelayMechanism (&u4ErrorCode, i4ContextId,
                                          i4DomainId, i4PortId,
                                          i4DelayType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to test delay type\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortDelayMechanism (i4ContextId,
                                       i4DomainId,
                                       i4PortId, i4DelayType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetVersion
 *
 * DESCRIPTION               : This function sets the PTP Protocol Version
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4Verion - PTP Verison
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetVersion (tCliHandle CliHandle, INT4 i4ContextId,
                  INT4 i4DomainId, INT4 i4PortId, INT4 i4Version)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortVersionNumber (&u4ErrorCode, i4ContextId,
                                         i4DomainId, i4PortId,
                                         i4Version) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortVersionNumber (i4ContextId,
                                      i4DomainId, i4PortId,
                                      i4Version) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetSyncLimit
 *
 * DESCRIPTION               : This function sets the PTP Sync Limit
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4PortIndex - PTP Port Index
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliSetSyncLimit (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                    INT4 i4PtpPortNo, UINT1 *pu1SyncLimit)
{

    tSNMP_OCTET_STRING_TYPE SyncLimit;
    UINT4               u4ErrorCode = 0;

    MEMSET (&SyncLimit, 0, sizeof (SyncLimit));

    SyncLimit.pu1_OctetList = pu1SyncLimit;
    SyncLimit.i4_Length = STRLEN (pu1SyncLimit);

    if (nmhTestv2FsPtpPortSynclimit (&u4ErrorCode, i4ContextId,
                                     i4DomainId, i4PtpPortNo,
                                     &SyncLimit) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortSynclimit (i4ContextId,
                                  i4DomainId, i4PtpPortNo,
                                  &SyncLimit) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpCliConfigSecurity
 *
 * DESCRIPTION               : This function sets the PTP Security Status
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainID - Domain Identifier
 *                             i4SecStatus - PTP  Security Status
 *                             Enabled / Disabled
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliConfigSecurity (tCliHandle CliHandle, INT4 i4ContextId,
                      INT4 i4DomainId, INT4 i4SecStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpClockSecurityEnabled (&u4ErrorCode, i4ContextId,
                                            i4DomainId,
                                            i4SecStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpClockSecurityEnabled (i4ContextId,
                                         i4DomainId,
                                         i4SecStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliConfigSetMaxSA
 *
 * DESCRIPTION               : This function sets the PTP Maximum Security
 *                             Associations
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4MaxSA -  Maximum Security Associations
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliConfigSetMaxSA (tCliHandle CliHandle, INT4 i4ContextId,
                      INT4 i4DomainId, INT4 i4MaxSA)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpClockNumOfSA (&u4ErrorCode, i4ContextId,
                                    i4DomainId, i4MaxSA) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpClockNumOfSA (i4ContextId, i4DomainId,
                                 i4MaxSA) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliConfigPathTrace
 *
 * DESCRIPTION               : This function sets the PTP Path Trace Option
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4Status - PathTrace Enabled / Disabled
 *                             
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliConfigPathTrace (tCliHandle CliHandle, INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpClockPathTraceOption (&u4ErrorCode, i4ContextId,
                                            i4DomainId,
                                            i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpClockPathTraceOption (i4ContextId,
                                         i4DomainId, i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliSetMaxAlternateMasters
 *
 * DESCRIPTION               : This function sets the PTP Priority 2
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4Status - PTP Module status
 *                             PTP Priority 2
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetPortNumOfAlternateMasters (tCliHandle CliHandle, INT4 i4ContextId,
                                    INT4 i4DomainId, INT4 i4PtpPortId,
                                    INT4 i4MaxAltMasters)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortNumOfAltMaster
        (&u4ErrorCode, i4ContextId, i4DomainId, i4PtpPortId, i4MaxAltMasters)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortNumOfAltMaster (i4ContextId, i4DomainId, i4PtpPortId,
                                       i4MaxAltMasters) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetDebugs
 *
 * DESCRIPTION      : This function sets and resets the Trace input for a
 *                    context
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    pu1TraceInput - Input Trace string
 *
 * OUTPUT           : pi1ModeName - Mode String
 *                    pi1DispStr - Display string
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliSetDebugs (tCliHandle CliHandle, UINT1 *pu1TraceInput)
{

    tSNMP_OCTET_STRING_TYPE TraceInput;
    UINT4               u4ErrorCode = 0;

    MEMSET (&TraceInput, 0, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = STRLEN (pu1TraceInput);

    if (nmhTestv2FsPtpGblTraceOption (&u4ErrorCode, &TraceInput) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPtpGblTraceOption (&TraceInput) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetCxtDebugs
 *
 * DESCRIPTION      : This function sets and resets the Trace input for a
 *                    context
 *
 * INPUT            : 
 *                    u4ContextId - Context Identifier
 *                    pu1TraceInput - Input Trace string
 *
 * OUTPUT           : None.
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliSetCxtDebugs (tCliHandle CliHandle, INT4 i4ContextId,
                    UINT1 *pu1TraceInput)
{
    tSNMP_OCTET_STRING_TYPE TraceInput;
    UINT4               u4ErrorCode = 0;

    MEMSET (&TraceInput, 0, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = STRLEN (pu1TraceInput);

    if (nmhTestv2FsPtpTraceOption (&u4ErrorCode, i4ContextId,
                                   &TraceInput) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPtpTraceOption (i4ContextId, &TraceInput) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpCliConverPortAddToOct
 *
 * DESCRIPTION               : This function used to convert the Prot Address 
 *                             format form string to Octet 
 *                              
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4Status - Alternate Master Status
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE VOID
PtpCliConverPortAddToOct (UINT1 *u1AccMasterAddr, INT4 i4NwProto,
                          tSNMP_OCTET_STRING_TYPE * pAccMasterAddr)
{
    tUtlInAddr          IpAddr;
    UINT4               u4Addr = 0;

    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));

    if (i4NwProto == CLI_PTP_IPV4)
    {
        if (!(CLI_INET_ATON (u1AccMasterAddr, &IpAddr)))
        {
            return;
        }
        u4Addr = OSIX_NTOHL (IpAddr.u4Addr);
        PTP_IPADDR_TO_OCT (u4Addr, pAccMasterAddr->pu1_OctetList);
        pAccMasterAddr->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else if (i4NwProto == CLI_PTP_IPV6)
    {
        INET_ATON6 (u1AccMasterAddr, pAccMasterAddr->pu1_OctetList);
        pAccMasterAddr->i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    else
    {
        CliDotStrToMac ((UINT1 *) u1AccMasterAddr,
                        pAccMasterAddr->pu1_OctetList);
        pAccMasterAddr->i4_Length = CFA_ENET_ADDR_LEN;
    }
}

/***************************************************************************
 * FUNCTION NAME             : PtpCliConfigAccMaster
 *
 * DESCRIPTION               : This function sets the PTP Alternate Masters
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4Status - Alternate Master Status
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliConfigAccMaster (tCliHandle CliHandle, INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                       CHR1 * u1AccMasterAddr)
{
    tSNMP_OCTET_STRING_TYPE AccMasterAddr;
    UINT1               au1AccMasterAddr[PTP_MASTER_MAX_ADDR_LEN];
    UINT4               u4ErrorCode = 0;

    MEMSET (&(au1AccMasterAddr[0]), 0, PTP_MASTER_MAX_ADDR_LEN);
    AccMasterAddr.pu1_OctetList = &au1AccMasterAddr[0];

    PtpCliConverPortAddToOct ((UINT1 *) u1AccMasterAddr, i4NwProto,
                              &AccMasterAddr);
    i4AccLen = AccMasterAddr.i4_Length;

    if (PtpCliUpdateAccMasterEntry (CliHandle, i4ContextId, i4DomainId,
                                    i4NwProto, i4AccLen, AccMasterAddr,
                                    CREATE_AND_GO) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsPtpAccMasterRowStatus
        (&u4ErrorCode, i4ContextId, i4DomainId, i4NwProto,
         i4AccLen, &AccMasterAddr, ACTIVE) == SNMP_FAILURE)
    {
        PtpCliUpdateAccMasterEntry (CliHandle, i4ContextId, i4DomainId,
                                    i4NwProto, i4AccLen,
                                    AccMasterAddr, DESTROY);
        return CLI_FAILURE;
    }

    if (PtpCliUpdateAccMasterEntry (CliHandle, i4ContextId, i4DomainId,
                                    i4NwProto, i4AccLen,
                                    AccMasterAddr, ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsPtpAccMasterRowStatus (i4ContextId, i4DomainId,
                                       i4NwProto, i4AccLen,
                                       &AccMasterAddr, DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME             : PtpCliConfigMaxUnicastMasters
 *
 * DESCRIPTION               : This function sets the Maximum Unicast Masters
 *
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4MaxUcastMasters - Maximum Unicast Masters
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliConfigMaxUnicastMasters (tCliHandle CliHandle, INT4 i4ContextId,
                               INT4 i4DoaminId, INT4 i4MaxUcastMasters)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4PortId = 0;
    if (nmhTestv2FsPtpPortUnicastMasterMaxSize (&u4ErrorCode, i4ContextId,
                                                i4DoaminId,
                                                i4PortId,
                                                i4MaxUcastMasters)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortUnicastMasterMaxSize (i4ContextId,
                                             i4DoaminId,
                                             i4PortId,
                                             i4MaxUcastMasters) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliShowClock
 *
 * DESCRIPTION               : This function displays the Foreign Master
 *                             PTP Clock Properties
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowClock (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId)
{
    tSNMP_OCTET_STRING_TYPE FMClockIdentity;
    tClkSysTimeInfo     ClkSysTimeInfo;
    UINT1              *pu1Temp = NULL;
    INT4                i4PtpClockTwoStepFlag = 0;
    INT4                i4PtpClockNumberPorts = 0;
    INT4                i4PtpClockClass = 0;
    INT4                i4PtpClockAccuracy = 0;
    INT4                i4OffsetScaledLogVar = 0;
    INT4                i4ClockPriority1 = 0;
    INT4                i4ClockPriority2 = 0;
    INT4                i4RetStatus = 0;
    INT4                i4ClockMode = 0;
    INT4                i4DelayMechanism = 0;
    INT4                i4SlaveOnlyFlag = 0;
    INT4                i4TwoStepFlag = PTP_ENABLED;
    INT4                i4MaxAccMaster = 0;
    INT4                i4PathTrcOpt = PTP_ENABLED;
    UINT1               au1ContextName[PTP_CLI_MAX_ALIAS_LEN];
    UINT1               au1RetClockId[PTP_MAX_CLOCK_ID_LEN];
    UINT1               au1TmpRetClockId[(PTP_CLOCK_ID_STR_LEN)];
    UINT1               au1SysSec[CFA_CLI_U8_STR_LENGTH];
    CHR1                ac1SysTime[PTP_TIME_STR_LEN];
    UINT1               u1Byte = 0;

    MEMSET (au1SysSec, 0, CFA_CLI_U8_STR_LENGTH);
    MEMSET (&au1RetClockId, 0, sizeof (au1RetClockId));
    MEMSET (&au1ContextName, 0, sizeof (au1ContextName));
    MEMSET (&au1TmpRetClockId, 0, sizeof (au1TmpRetClockId));
    MEMSET (&ClkSysTimeInfo, 0, sizeof (tClkSysTimeInfo));

    FMClockIdentity.pu1_OctetList = au1RetClockId;
    FMClockIdentity.i4_Length = STRLEN (au1RetClockId);

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " PTP Clock Information\r\n");
    CliPrintf (CliHandle, " ---------------------\r\n");

    i4RetStatus =
        nmhValidateIndexInstanceFsPtpDomainDataSetTable (i4ContextId,
                                                         i4DomainId);
    if (i4RetStatus == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n");
        return;
    }

    nmhGetFsPtpClockIdentity (i4ContextId, i4DomainId, &FMClockIdentity);
    nmhGetFsPtpClockTwoStepFlag (i4ContextId, i4DomainId,
                                 &i4PtpClockTwoStepFlag);

    /* Get Context Name from Context ID */
    PtpPortVcmGetAliasName (i4ContextId, au1ContextName);
    nmhGetFsPtpClockClass (i4ContextId, i4DomainId, &i4PtpClockClass);
    nmhGetFsPtpClockAccuracy (i4ContextId, i4DomainId, &i4PtpClockAccuracy);
    nmhGetFsPtpClockOffsetScaledLogVariance (i4ContextId, i4DomainId,
                                             &i4OffsetScaledLogVar);
    nmhGetFsPtpClockPriority1 (i4ContextId, i4DomainId, &i4ClockPriority1);
    nmhGetFsPtpClockPriority2 (i4ContextId, i4DomainId, &i4ClockPriority2);
    nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId, &i4ClockMode);
    nmhGetFsPtpTransparentClockDelaymechanism (i4ContextId, i4DomainId,
                                               &i4DelayMechanism);

    nmhGetFsPtpClockSlaveOnly (i4ContextId, i4DomainId, &i4SlaveOnlyFlag);
    nmhGetFsPtpClockAccMasterMaxSize (i4ContextId, i4DomainId, &i4MaxAccMaster);
    nmhGetFsPtpClockPathTraceOption (i4ContextId, i4DomainId, &i4PathTrcOpt);

    if (i4ClockMode != PTP_TRANSPARENT_CLOCK_MODE)
    {
        nmhGetFsPtpClockNumberPorts (i4ContextId, i4DomainId,
                                     &i4PtpClockNumberPorts);
        nmhGetFsPtpClockTwoStepFlag (i4ContextId, i4DomainId, &i4TwoStepFlag);

    }
    else
    {
        nmhGetFsPtpTransparentClockNumberPorts (i4ContextId, i4DomainId,
                                                &i4PtpClockNumberPorts);
        nmhGetFsPtpTransparentClockTwoStepFlag (i4ContextId, i4DomainId,
                                                &i4TwoStepFlag);
    }

    CliPrintf (CliHandle, " Clock Context                : %s\r\n",
               au1ContextName);
    pu1Temp = &(au1TmpRetClockId[0]);
    for (u1Byte = 0; u1Byte < PTP_MAX_CLOCK_ID_LEN; u1Byte++)
    {
        pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:",
                            *(FMClockIdentity.pu1_OctetList + u1Byte));
    }
    *(CHR1 *) (pu1Temp - 1) = '\0';

    CliPrintf (CliHandle, " Clock Identity               : %s\r\n",
               au1TmpRetClockId);
    CliPrintf (CliHandle, " Clock Domain                 : %d\r\n", i4DomainId);

    /*Display Primary Domain Information */

    nmhGetFsPtpPrimaryDomain (i4ContextId, &i4DomainId);
    CliPrintf (CliHandle, " Primary Domain               : %d\r\n\r\n",
               i4DomainId);

    if (i4ClockMode == PTP_OC_CLOCK_MODE)
    {
        CliPrintf (CliHandle, " Clock Mode                   : Ordinary \r\n");
    }
    else if (i4ClockMode == PTP_BOUNDARY_CLOCK_MODE)
    {
        CliPrintf (CliHandle, " Clock Mode                   : Boundary \r\n");
    }
    else if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        if (i4DelayMechanism == PTP_PORT_DELAY_MECH_PEER_TO_PEER)
        {
            CliPrintf (CliHandle,
                       " Clock Mode                   : Peer to Peer Transparent \r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Clock Mode                   : End to End Transparent \r\n");
        }
    }
    else if (i4ClockMode == PTP_MANAGEMENT_MODE)
    {
        CliPrintf (CliHandle, " Clock Mode                   : Managment \r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Clock Mode                   : Forward \r\n");
    }
    if (i4TwoStepFlag == PTP_ENABLED)
    {
        CliPrintf (CliHandle, " Type Of Clock                : Two Step \r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Type Of Clock                : One Step \r\n");
    }

    CliPrintf (CliHandle, " Number of PTP ports          : %d\r\n",
               i4PtpClockNumberPorts);

    if ((i4ClockMode == PTP_BOUNDARY_CLOCK_MODE) ||
        (i4ClockMode == PTP_OC_CLOCK_MODE))
    {

        CliPrintf (CliHandle, " Priority1                    : %d\r\n",
                   i4ClockPriority1);
        CliPrintf (CliHandle, " Priority2                    : %d\r\n",
                   i4ClockPriority2);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, " Clock Quality\r\n");
        CliPrintf (CliHandle, "  Class                       : %d\r\n",
                   i4PtpClockClass);
        CliPrintf (CliHandle, "  Accuracy                    : %d\r\n",
                   i4PtpClockAccuracy);

        CliPrintf (CliHandle, "  Offset (log variance)       : %d\r\n",
                   i4OffsetScaledLogVar);

        if (i4SlaveOnlyFlag == OSIX_TRUE)
        {
            CliPrintf (CliHandle, " \n This clock is a Slave only clock\r\n");
        }

        CliPrintf (CliHandle, "\r\n");
    }
    CliPrintf (CliHandle, " Acceptable Master configured : %d\r\n",
               i4MaxAccMaster);
    if (i4PathTrcOpt == PTP_ENABLED)
    {
        CliPrintf (CliHandle, " Path Trace Option            : Enabled\r\n");
    }
    else
    {
        CliPrintf (CliHandle, " Path Trace Option            : Disabled\r\n");
    }
    CliPrintf (CliHandle, "\r\n");

    PtpPortClkIwfGetClock (&ClkSysTimeInfo);

    FSAP_U8_2STR (&(ClkSysTimeInfo.FsClkTimeVal.u8Sec), (CHR1 *) & au1SysSec);

    CliPrintf (CliHandle, " Local Time\r\n");
    CliPrintf (CliHandle, " ----------\r\n");

    MEMSET (ac1SysTime, 0, PTP_TIME_STR_LEN);
    UtlGetTimeStr (ac1SysTime);
    CliPrintf (CliHandle, "  %s\r\n", ac1SysTime);
    CliPrintf (CliHandle, "  %s secs %d nano secs since the Epoch\r\n",
               au1SysSec, ClkSysTimeInfo.FsClkTimeVal.u4Nsec);
    CliPrintf (CliHandle, "\r\n");

    return;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliShowForeignMaster
 *
 * DESCRIPTION               : This function displays the Foreign Master
 *                             PTP Clock Properties
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *
 * OUTPUT                    :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowForeignMaster (tCliHandle CliHandle, INT4 i4Context, INT4 i4Domain)
{
    tSNMP_OCTET_STRING_TYPE FMClockIdentity;
    tSNMP_OCTET_STRING_TYPE NextFMClockID;
    UINT1              *pu1Temp = NULL;
    INT4                i4ContextId = 0;
    INT4                i4NextContextId = 0;
    INT4                i4NextDomainId = 0;
    INT4                i4NextFMPortIndex = 0;
    INT4                i4NextPortIndex = 0;
    INT4                i4DomainId = 0;
    INT4                i4PortIndex = 0;
    INT4                i4NextFMAnnounceMsgs = 0;
    INT4                i4FMPortIndex = 0;
    UINT1               au1RetClockId[PTP_MAX_CLOCK_ID_LEN];
    UINT1               au1NextRetClockId[PTP_MAX_CLOCK_ID_LEN];
    UINT1               au1TmpRetClockId[(PTP_CLOCK_ID_STR_LEN)];
    UINT1               u1Byte = 0;

    MEMSET (&FMClockIdentity, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFMClockID, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1RetClockId, 0, sizeof (au1RetClockId));
    MEMSET (&au1NextRetClockId, 0, sizeof (au1NextRetClockId));
    MEMSET (&au1TmpRetClockId, 0, sizeof (au1TmpRetClockId));

    FMClockIdentity.pu1_OctetList = au1RetClockId;
    FMClockIdentity.i4_Length = STRLEN (au1RetClockId);
    NextFMClockID.pu1_OctetList = au1NextRetClockId;
    NextFMClockID.i4_Length = STRLEN (au1NextRetClockId);

    CliPrintf (CliHandle, " \r\n");
    CliPrintf (CliHandle, " PTP Foreign Master Record\r\n");
    CliPrintf (CliHandle, " --------------------------\r\n");

    i4ContextId = i4Context;
    i4DomainId = i4Domain;

    while (nmhGetNextIndexFsPtpForeignMasterDataSetTable
           (i4ContextId, &i4NextContextId, i4DomainId, &i4NextDomainId,
            &FMClockIdentity, &NextFMClockID, i4FMPortIndex, &i4NextFMPortIndex,
            i4PortIndex, &i4NextPortIndex) != SNMP_FAILURE)
    {

        /* if not the required context and domain break */
        if ((i4NextContextId != i4Context) || (i4NextDomainId != i4Domain))
        {
            break;
        }
        nmhGetFsPtpForeignMasterAnnounceMsgs (i4NextContextId, i4NextDomainId,
                                              &NextFMClockID, i4NextFMPortIndex,
                                              i4NextPortIndex,
                                              &i4NextFMAnnounceMsgs);
        pu1Temp = &(au1TmpRetClockId[0]);
        for (u1Byte = 0; u1Byte < PTP_MAX_CLOCK_ID_LEN; u1Byte++)
        {
            pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:",
                                *(NextFMClockID.pu1_OctetList + u1Byte));
        }
        *(CHR1 *) (pu1Temp - 1) = '\0';

        CliPrintf (CliHandle, "\t Foreign Master Identity\r\n");
        CliPrintf (CliHandle, "\t Clock Identity              :"
                   " %s\r\n", au1TmpRetClockId);
        CliPrintf (CliHandle, "\t Port Identity               : "
                   " %d\r\n", i4NextPortIndex);
        CliPrintf (CliHandle, "\t Number of announce messages : "
                   " %d\r\n", i4NextFMAnnounceMsgs);

        i4ContextId = i4NextContextId;
        i4DomainId = i4NextDomainId;
        MEMCPY (&FMClockIdentity, &NextFMClockID, sizeof (NextFMClockID));
        i4FMPortIndex = i4NextFMPortIndex;
        i4PortIndex = i4NextPortIndex;

    }
    CliPrintf (CliHandle, "\r\n");
    return;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliShowGrandMasterRecords
 *
 * DESCRIPTION               : This function displays the Grand Master
 *                             PTP Clock Properties
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowGrandMasterRecords (tCliHandle CliHandle, INT4 i4Context,
                              INT4 i4Domain)
{

    tSNMP_OCTET_STRING_TYPE ClockIdentity;
    tSNMP_OCTET_STRING_TYPE GMClockIdentity;
    tSNMP_OCTET_STRING_TYPE NextClockIdentity;
    UINT1              *pu1Temp = NULL;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4ParentPortNumber = 0;
    INT4                i4PhaseChangeRate = 0;
    INT4                i4OffsetScaledLogVar = 0;
    INT4                i4GMClockClass = 0;
    INT4                i4GMClockAccuracy = 0;
    INT4                i4OffsetLogVar = 0;
    INT4                i4GMPriority1 = 0;
    INT4                i4GMPriority2 = 0;
    INT4                i4GMClockQuality = 0;
    INT4                i4ClkObservedDrift = 0;
    UINT1               au1RetClockId[PTP_MAX_CLOCK_ID_LEN];
    UINT1               au1NextClockId[PTP_MAX_CLOCK_ID_LEN];
    UINT1               au1GMClockId[PTP_MAX_CLOCK_ID_LEN];
    UINT1               au1TmpRetClockId[(PTP_CLOCK_ID_STR_LEN)];
    UINT1               u1Byte = 0;

    MEMSET (&ClockIdentity, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&GMClockIdentity, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextClockIdentity, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RetClockId, 0, sizeof (au1RetClockId));
    MEMSET (au1NextClockId, 0, sizeof (au1NextClockId));
    MEMSET (au1GMClockId, 0, sizeof (au1GMClockId));

    ClockIdentity.pu1_OctetList = au1RetClockId;
    ClockIdentity.i4_Length = sizeof (au1RetClockId);
    GMClockIdentity.pu1_OctetList = au1GMClockId;
    GMClockIdentity.i4_Length = sizeof (au1GMClockId);
    NextClockIdentity.pu1_OctetList = au1NextClockId;
    NextClockIdentity.i4_Length = sizeof (au1NextClockId);

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " PTP Parent Properties\r\n");
    CliPrintf (CliHandle, " ---------------------\r\n");

    i4RetVal =
        nmhValidateIndexInstanceFsPtpParentDataSetTable (i4Context, i4Domain);

    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n");
        return;
    }

    nmhGetFsPtpParentClockIdentity (i4Context, i4Domain, &NextClockIdentity);
    nmhGetFsPtpParentPortNumber (i4Context, i4Domain, &i4ParentPortNumber);
    nmhGetFsPtpParentObservedOffsetScaledLogVariance (i4Context,
                                                      i4Domain,
                                                      &i4OffsetScaledLogVar);
    nmhGetFsPtpParentObservedClockPhaseChangeRate (i4Context,
                                                   i4Domain,
                                                   &i4PhaseChangeRate);
    nmhGetFsPtpParentGMClockIdentity (i4Context, i4Domain, &GMClockIdentity);
    nmhGetFsPtpParentGMClockClass (i4Context, i4Domain, &i4GMClockClass);
    nmhGetFsPtpParentGMClockAccuracy (i4Context, i4Domain, &i4GMClockAccuracy);
    nmhGetFsPtpParentGMClockOffsetScaledLogVariance (i4Context,
                                                     i4Domain, &i4OffsetLogVar);
    nmhGetFsPtpParentGMPriority1 (i4Context, i4Domain, &i4GMPriority1);
    nmhGetFsPtpParentGMPriority2 (i4Context, i4Domain, &i4GMPriority2);
    nmhGetFsPtpParentClockObservedDrift (i4Context, i4Domain,
                                         &i4ClkObservedDrift);

    CliPrintf (CliHandle, " Parent Clock\r\n");

    pu1Temp = &(au1TmpRetClockId[0]);
    for (u1Byte = 0; u1Byte < PTP_MAX_CLOCK_ID_LEN; u1Byte++)
    {
        pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:",
                            *(NextClockIdentity.pu1_OctetList + u1Byte));
    }
    *(CHR1 *) (pu1Temp - 1) = '\0';

    CliPrintf (CliHandle, "\tParent Clock Identity : %s\r\n", au1TmpRetClockId);
    CliPrintf (CliHandle, "\tParent Port Number: %d\r\n", i4ParentPortNumber);
    CliPrintf (CliHandle, "\tParent Offset (log variance) : %d\r\n",
               i4OffsetScaledLogVar);
    CliPrintf (CliHandle, "\tParent Clock Phase Change Rate : %d\r\n",
               i4PhaseChangeRate);
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, " Grandmaster Clock\r\n");

    MEMSET (&au1TmpRetClockId, 0, sizeof (au1TmpRetClockId));

    pu1Temp = &(au1TmpRetClockId[0]);
    for (u1Byte = 0; u1Byte < PTP_MAX_CLOCK_ID_LEN; u1Byte++)
    {
        pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:",
                            *(GMClockIdentity.pu1_OctetList + u1Byte));
    }

    *(CHR1 *) (pu1Temp - 1) = '\0';

    CliPrintf (CliHandle, "\tGrandmaster Clock Identity : %s\r\n",
               au1TmpRetClockId);
    CliPrintf (CliHandle, "\tGrandmaster Clock Quality  : %d\r\n",
               i4GMClockQuality);
    CliPrintf (CliHandle, " \t Class    : %d\r\n", i4GMClockClass);
    CliPrintf (CliHandle, " \t Accuracy : %d\r\n", i4GMClockAccuracy);
    CliPrintf (CliHandle, " \tPriority1 : %d\r\n", i4GMPriority1);
    CliPrintf (CliHandle, " \tPriority2 : %d\r\n", i4GMPriority2);
    CliPrintf (CliHandle, " \tObserved Drift        : %d\r\n",
               i4ClkObservedDrift);
    CliPrintf (CliHandle, " \tOffset (log variance) : %d\r\n", i4OffsetLogVar);
    CliPrintf (CliHandle, "\r\n");

    return;
}

/***************************************************************************
 * FUNCTION NAME             : PtpCliShowPortCounters
 *
 * DESCRIPTION               : This function displays PTP Clock Port Properties
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *
 * OUTPUT                    : Display PTP Port Counter Information
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowPortCounters (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId)
{

    INT4                i4NextPortIndex = 0;
    INT4                i4NextContextId = 0;
    INT4                i4NextDomainId = 0;
    INT4                i4PortIndex = 0;
    INT4                i4InterfaceNum = 0;
    INT4                i4InterfaceType = 0;
    UINT4               u4Counter = 0;
    UINT1               au1IfName[PTP_CLI_MAX_ALIAS_LEN];

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " PTP Interface Counters \r\n");
    CliPrintf (CliHandle, " ---------------------- \r\n\n");

    while (nmhGetNextIndexFsPtpTransparentPortDataSetTable
           (i4ContextId, &i4NextContextId, i4DomainId,
            &i4NextDomainId, i4PortIndex, &i4NextPortIndex) != SNMP_FAILURE)
    {
        if ((i4ContextId != i4NextContextId) || (i4DomainId != i4NextDomainId))
        {
            break;
        }

        nmhGetFsPtpPortInterfaceType (i4NextContextId, i4NextDomainId,
                                      i4NextPortIndex, &i4InterfaceType);
        nmhGetFsPtpPortIfaceNumber (i4NextContextId, i4NextDomainId,
                                    i4NextPortIndex, &i4InterfaceNum);

        if (i4InterfaceType != PTP_IFACE_VLAN)
        {
            CfaCliConfGetIfName ((UINT4) i4InterfaceNum, (INT1 *) au1IfName);
            CliPrintf (CliHandle, "Interface  %s\r\n", au1IfName);
        }
        /*In case it is a vlan print vlan id */
        else
        {
            CliPrintf (CliHandle, "Vlan  %d\r\n", i4InterfaceNum);
        }

        nmhGetFsPtpPortTransAnnounceMsgCnt (i4NextContextId, i4NextDomainId,
                                            i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tAnnounce Message Transmitted   : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdAnnounceMsgCnt (i4NextContextId, i4NextDomainId,
                                           i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tAnnounce Messages Received     : %u\r\n",
                   u4Counter);

        nmhGetFsPtpPortTransSyncMsgCnt (i4NextContextId, i4NextDomainId,
                                        i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tSync Message Transmitted       : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortTransFollowUpMsgCnt (i4NextContextId, i4NextDomainId,
                                            i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tSync Follow Up Transmitted     : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdSyncMsgCnt (i4NextContextId, i4NextDomainId,
                                       i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tSync Messages Received         : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdFollowUpMsgCnt (i4NextContextId, i4NextDomainId,
                                           i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tSync Follow Up Received        : %u\r\n\n",
                   u4Counter);

        nmhGetFsPtpPortTransDelayReqMsgCnt (i4NextContextId, i4NextDomainId,
                                            i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tDelay Requests Transmitted     : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdDelayRespMsgCnt (i4NextContextId, i4NextDomainId,
                                            i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tDelay Responses Received       : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdDelayReqMsgCnt (i4NextContextId, i4NextDomainId,
                                           i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tDelay Requests Received        : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortTransDelayRespMsgCnt (i4NextContextId, i4NextDomainId,
                                             i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tDelay Responses Transmitted    : %u\r\n\n",
                   u4Counter);

        nmhGetFsPtpPortTransPeerDelayReqMsgCnt (i4NextContextId, i4NextDomainId,
                                                i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Request Transmitted            : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdPeerDelayRespMsgCnt (i4NextContextId, i4NextDomainId,
                                                i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Response Received              : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdPeerDelayRespFollowUpMsgCnt (i4NextContextId,
                                                        i4NextDomainId,
                                                        i4NextPortIndex,
                                                        &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Response Follow Up Received    : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdPeerDelayReqMsgCnt (i4NextContextId, i4NextDomainId,
                                               i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Request Received               : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortTransPeerDelayRespMsgCnt (i4NextContextId,
                                                 i4NextDomainId,
                                                 i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Response Transmitted           : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortTransPeerDelayRespFollowUpMsgCnt (i4NextContextId,
                                                         i4NextDomainId,
                                                         i4NextPortIndex,
                                                         &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Response Follow Up Transmitted : %u\r\n\n",
                   u4Counter);

        nmhGetFsPtpPortDiscardedMsgCnt (i4NextContextId, i4NextDomainId,
                                        i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tDiscarded Messages             : %u\r\n",
                   u4Counter);

        i4ContextId = i4NextContextId;
        i4DomainId = i4NextDomainId;
        i4PortIndex = i4NextPortIndex;

    }
    while (nmhGetNextIndexFsPtpPortConfigDataSetTable
           (i4ContextId, &i4NextContextId, i4DomainId,
            &i4NextDomainId, i4PortIndex, &i4NextPortIndex) != SNMP_FAILURE)
    {
        if ((i4ContextId != i4NextContextId) || (i4DomainId != i4NextDomainId))
        {
            break;
        }

        nmhGetFsPtpPortInterfaceType (i4NextContextId, i4NextDomainId,
                                      i4NextPortIndex, &i4InterfaceType);
        nmhGetFsPtpPortIfaceNumber (i4NextContextId, i4NextDomainId,
                                    i4NextPortIndex, &i4InterfaceNum);

        if (i4InterfaceType != PTP_IFACE_VLAN)
        {
            CfaCliConfGetIfName ((UINT4) i4InterfaceNum, (INT1 *) au1IfName);
            CliPrintf (CliHandle, "Interface  %s\r\n", au1IfName);
        }
        /*In case it is a vlan print vlan id */
        else
        {
            CliPrintf (CliHandle, "Vlan  %d\r\n", i4InterfaceNum);
        }

        nmhGetFsPtpPortTransAnnounceMsgCnt (i4NextContextId, i4NextDomainId,
                                            i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tAnnounce Message Transmitted   : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdAnnounceMsgCnt (i4NextContextId, i4NextDomainId,
                                           i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tAnnounce Messages Received     : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortTransSyncMsgCnt (i4NextContextId, i4NextDomainId,
                                        i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tSync Message Transmitted       : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortTransFollowUpMsgCnt (i4NextContextId, i4NextDomainId,
                                            i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tSync Follow Up Transmitted     : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdSyncMsgCnt (i4NextContextId, i4NextDomainId,
                                       i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tSync Messages Received         : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdFollowUpMsgCnt (i4NextContextId, i4NextDomainId,
                                           i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tSync Follow Up Received        : %u\r\n\n",
                   u4Counter);

        nmhGetFsPtpPortTransDelayReqMsgCnt (i4NextContextId, i4NextDomainId,
                                            i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tDelay Requests Transmitted     : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdDelayRespMsgCnt (i4NextContextId, i4NextDomainId,
                                            i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tDelay Responses Received       : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdDelayReqMsgCnt (i4NextContextId, i4NextDomainId,
                                           i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tDelay Requests Received        : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortTransDelayRespMsgCnt (i4NextContextId, i4NextDomainId,
                                             i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tDelay Responses Transmitted    : %u\r\n\n",
                   u4Counter);

        nmhGetFsPtpPortTransPeerDelayReqMsgCnt (i4NextContextId, i4NextDomainId,
                                                i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Request Transmitted            : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdPeerDelayRespMsgCnt (i4NextContextId, i4NextDomainId,
                                                i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Response Received              : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdPeerDelayRespFollowUpMsgCnt (i4NextContextId,
                                                        i4NextDomainId,
                                                        i4NextPortIndex,
                                                        &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Response Follow Up Received    : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortRcvdPeerDelayReqMsgCnt (i4NextContextId, i4NextDomainId,
                                               i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Request Received               : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortTransPeerDelayRespMsgCnt (i4NextContextId,
                                                 i4NextDomainId,
                                                 i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Response Transmitted           : %u\r\n",
                   u4Counter);
        nmhGetFsPtpPortTransPeerDelayRespFollowUpMsgCnt (i4NextContextId,
                                                         i4NextDomainId,
                                                         i4NextPortIndex,
                                                         &u4Counter);
        CliPrintf (CliHandle,
                   "\tPeer Delay Response Follow Up Transmitted : %u\r\n\n",
                   u4Counter);

        nmhGetFsPtpPortDiscardedMsgCnt (i4NextContextId, i4NextDomainId,
                                        i4NextPortIndex, &u4Counter);
        CliPrintf (CliHandle, "\tDiscarded Messages             : %u\r\n",
                   u4Counter);

        i4ContextId = i4NextContextId;
        i4DomainId = i4NextDomainId;
        i4PortIndex = i4NextPortIndex;

    }

    CliPrintf (CliHandle, "\r\n");
    return;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliShowTransClkPort
 *
 * DESCRIPTION               : This function displays PTP Clock Port Properties
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *
 * OUTPUT                    : Display PTP Port Information
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowTransClkPort (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                        UINT4 u4IfIndex, INT4 i4IfType)
{
    tSNMP_OCTET_STRING_TYPE ClkIdentity;
    UINT1              *pu1Temp = NULL;
    INT4                i4NextPortIndex = 0;
    INT4                i4NextContextId = 0;
    INT4                i4NextDomainId = 0;
    INT4                i4InterfaceNum = 0;
    INT4                i4InterfaceType = 0;
    INT4                i4count = 0;
    INT4                i4PortIndex = 0;
    INT4                i4PtpStatus = 0;
    INT4                i4RowStatus = 0;
    UINT1               au1ClockId[PTP_MAX_CLOCK_ID_LEN];
    UINT1               au1IfName[PTP_CLI_MAX_ALIAS_LEN];
    UINT1               au1ContextName[PTP_CLI_MAX_ALIAS_LEN];
    UINT1               au1TmpRetClockId[(PTP_CLOCK_ID_STR_LEN)];
    UINT1               u1Byte = 0;

    MEMSET (&ClkIdentity, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ClockId, 0, PTP_MAX_CLOCK_ID_LEN);
    MEMSET (au1IfName, 0, PTP_CLI_MAX_ALIAS_LEN);
    MEMSET (au1TmpRetClockId, 0, PTP_CLOCK_ID_STR_LEN);
    MEMSET (au1ContextName, 0, PTP_CLI_MAX_ALIAS_LEN);

    ClkIdentity.pu1_OctetList = au1ClockId;
    ClkIdentity.i4_Length = STRLEN (au1ClockId);

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " PTP Transparent Port Properties\r\n");
    CliPrintf (CliHandle, " -------------------------------\r\n");

    while (nmhGetNextIndexFsPtpTransparentPortDataSetTable
           (i4ContextId, &i4NextContextId, i4DomainId,
            &i4NextDomainId, i4PortIndex, &i4NextPortIndex) != SNMP_FAILURE)
    {
        if ((i4ContextId != i4NextContextId) || (i4DomainId != i4NextDomainId))
        {
            break;
        }

        if (i4IfType != PTP_IFACE_UNKNOWN)
        {
            /* User has specified a port to be displayed */
            if (PtpIfGetPortNumber (i4ContextId, (UINT1) i4DomainId, i4IfType,
                                    u4IfIndex,
                                    &i4NextPortIndex) == OSIX_FAILURE)
            {
                return;
            }

            if (i4count > 0)
            {
                /* Used provided port has been displayed already */
                break;
            }
        }

        i4ContextId = i4NextContextId;
        i4DomainId = i4NextDomainId;
        i4PortIndex = i4NextPortIndex;
        /* If the row status is not active, can the next row */
        nmhGetFsPtpTransparentPortRowStatus (i4NextContextId, i4NextDomainId,
                                             i4NextPortIndex, &i4RowStatus);

        if (i4RowStatus != ACTIVE)
        {
            continue;
        }

        nmhGetFsPtpTransparentPortClockIdentity (i4NextContextId,
                                                 i4NextDomainId,
                                                 i4NextPortIndex, &ClkIdentity);
        nmhGetFsPtpTransparentPortInterfaceType (i4NextContextId,
                                                 i4NextDomainId,
                                                 i4NextPortIndex,
                                                 &i4InterfaceType);
        nmhGetFsPtpTransparentPortIfaceNumber (i4NextContextId, i4NextDomainId,
                                               i4NextPortIndex,
                                               &i4InterfaceNum);
        /* Get Context Name from Context ID */
        PtpPortVcmGetAliasName (i4ContextId, au1ContextName);

        nmhGetFsPtpTransparentPortPtpStatus (i4NextContextId, i4NextDomainId,
                                             i4NextPortIndex, &i4PtpStatus);

        CliPrintf (CliHandle, "\r\n Record # %d\r\n", ++i4count);
        CliPrintf (CliHandle, "\tDevice type      :");

        /* Print the Device type based on interface type */
        switch (i4InterfaceType)
        {
            case PTP_IFACE_UDP_IPV4:
                CliPrintf (CliHandle, " UDP/IP Version 4\r\n");
                break;

            case PTP_IFACE_UDP_IPV6:
                CliPrintf (CliHandle, " UDP/IP Version 6\r\n");
                break;

            case PTP_IFACE_IEEE_802_3:
                CliPrintf (CliHandle, " IEEE 802.3\r\n");
                break;

            case PTP_IFACE_VLAN:
                CliPrintf (CliHandle, " VLAN\r\n");
                break;

            default:
                CliPrintf (CliHandle, "\r\n");

        }

        CliPrintf (CliHandle, "\tSwitch / Vrf     : %s\r\n", au1ContextName);

        /* Get Interface Name from Interface Index */
        if (i4InterfaceType != PTP_IFACE_VLAN)
        {
            CfaCliConfGetIfName ((UINT4) i4InterfaceNum, (INT1 *) au1IfName);
            CliPrintf (CliHandle, "\tInterface        : %s\r\n", au1IfName);
        }
        /*In case it is a vlan print vlan id */
        else
        {
            CliPrintf (CliHandle, "\tVlan id          : %d\r\n",
                       i4InterfaceNum);
        }
        /*Port ID is a combination of Clock ID and PTP Port Number */

        pu1Temp = &(au1TmpRetClockId[0]);
        for (u1Byte = 0; u1Byte < PTP_MAX_CLOCK_ID_LEN; u1Byte++)
        {
            pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:",
                                *(ClkIdentity.pu1_OctetList + u1Byte));
        }
        *(CHR1 *) (pu1Temp - 1) = '\0';

        CliPrintf (CliHandle, "\tPort identity    : %s :%d\r\n",
                   au1TmpRetClockId, i4NextPortIndex);
        CliPrintf (CliHandle, "\tPTP Status       :");

        if (i4PtpStatus == PTP_ENABLED)
        {
            CliPrintf (CliHandle, " Enable\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " Disable\r\n");
        }
    }
    return;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliShowPort
 *
 * DESCRIPTION               : This function displays PTP Clock Port Properties
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *
 * OUTPUT                    : Display PTP Port Information
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowPort (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                UINT4 u4IfIndex, INT4 i4IfType)
{
    tSNMP_OCTET_STRING_TYPE ClkIdentity;
    tSNMP_OCTET_STRING_TYPE SyncLimit;
    UINT1              *pu1Temp = NULL;
    INT4                i4NextPortIndex = 0;
    INT4                i4NextContextId = 0;
    INT4                i4NextDomainId = 0;
    INT4                i4InterfaceNum = 0;
    INT4                i4InterfaceType = 0;
    INT4                i4DelayMechanism = 0;
    INT4                i4UcastNegOption = 0;
    INT4                i4VersionNumber = 0;
    INT4                i4count = 0;
    INT4                i4PortIndex = 0;
    INT4                i4PtpStatus = 0;
    INT4                i4PtpState = 0;
    INT4                i4RowStatus = 0;
    INT4                i4AltMcastSyncInterval = 0;
    INT4                i4NumOfAltMst = 0;
    INT4                i4AccMstStatus = 0;
    UINT1               au1ClockId[PTP_MAX_CLOCK_ID_LEN];
    UINT1               au1SyncLimit[PTP_U8_STR_LEN];
    UINT1               au1IfName[PTP_CLI_MAX_ALIAS_LEN];
    UINT1               au1ContextName[PTP_CLI_MAX_ALIAS_LEN];
    UINT1               au1TmpRetClockId[(PTP_CLOCK_ID_STR_LEN)];
    UINT1               u1Byte = 0;
    UINT1               u1AltMcastSync = 0;

    MEMSET (&ClkIdentity, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SyncLimit, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ClockId, 0, sizeof (au1ClockId));
    MEMSET (au1IfName, 0, sizeof (au1IfName));
    MEMSET (au1SyncLimit, 0, sizeof (au1SyncLimit));
    MEMSET (au1TmpRetClockId, 0, PTP_CLOCK_ID_STR_LEN);
    MEMSET (au1ContextName, 0, PTP_CLI_MAX_ALIAS_LEN);

    ClkIdentity.pu1_OctetList = au1ClockId;
    ClkIdentity.i4_Length = sizeof (au1ClockId);
    SyncLimit.pu1_OctetList = au1SyncLimit;
    SyncLimit.i4_Length = sizeof (au1SyncLimit);

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " PTP Port Properties\r\n");
    CliPrintf (CliHandle, " -------------------\r\n");

    while (nmhGetNextIndexFsPtpPortConfigDataSetTable
           (i4ContextId, &i4NextContextId, i4DomainId,
            &i4NextDomainId, i4PortIndex, &i4NextPortIndex) != SNMP_FAILURE)
    {
        if ((i4ContextId != i4NextContextId) || (i4DomainId != i4NextDomainId))
        {
            break;
        }

        if (i4IfType != PTP_IFACE_UNKNOWN)
        {
            /* User has specified a port to be displayed */
            if (PtpIfGetPortNumber (i4ContextId, (UINT1) i4DomainId, i4IfType,
                                    u4IfIndex,
                                    &i4NextPortIndex) == OSIX_FAILURE)
            {
                return;
            }

            if (i4count > 0)
            {
                /* Used provided port has been displayed already */
                break;
            }
        }

        i4ContextId = i4NextContextId;
        i4DomainId = i4NextDomainId;
        i4PortIndex = i4NextPortIndex;

        /* If the row status is not active, can the next row */
        nmhGetFsPtpPortRowStatus (i4NextContextId, i4NextDomainId,
                                  i4NextPortIndex, &i4RowStatus);

        if (i4RowStatus != ACTIVE)
        {
            continue;
        }

        nmhGetFsPtpPortClockIdentity (i4NextContextId, i4NextDomainId,
                                      i4NextPortIndex, &ClkIdentity);
        nmhGetFsPtpPortInterfaceType (i4NextContextId, i4NextDomainId,
                                      i4NextPortIndex, &i4InterfaceType);
        nmhGetFsPtpPortIfaceNumber (i4NextContextId, i4NextDomainId,
                                    i4NextPortIndex, &i4InterfaceNum);

        nmhGetFsPtpPortDelayMechanism (i4NextContextId, i4NextDomainId,
                                       i4NextPortIndex, &i4DelayMechanism);
        nmhGetFsPtpPortVersionNumber (i4NextContextId, i4NextDomainId,
                                      i4NextPortIndex, &i4VersionNumber);
        nmhGetFsPtpPortUnicastNegOption (i4NextContextId, i4NextDomainId,
                                         i4NextPortIndex, &i4UcastNegOption);
        nmhGetFsPtpPortSynclimit (i4NextContextId, i4NextDomainId,
                                  i4NextPortIndex, &SyncLimit);
        /* Get Context Name from Context ID */
        PtpPortVcmGetAliasName (i4ContextId, au1ContextName);

        nmhGetFsPtpPortPtpStatus (i4NextContextId, i4NextDomainId,
                                  i4NextPortIndex, &i4PtpStatus);

        nmhGetFsPtpPortState (i4NextContextId, i4NextDomainId,
                              i4NextPortIndex, &i4PtpState);

        nmhGetFsPtpPortAltMulcastSyncInterval (i4NextContextId,
                                               i4NextDomainId,
                                               i4NextPortIndex,
                                               &i4AltMcastSyncInterval);

        nmhGetFsPtpPortAltMulcastSync (i4NextContextId, i4NextDomainId,
                                       i4NextPortIndex,
                                       (INT4 *) (VOID *) &u1AltMcastSync);

        nmhGetFsPtpPortAccMasterEnabled (i4NextContextId, i4NextDomainId,
                                         i4NextPortIndex, &i4AccMstStatus);

        nmhGetFsPtpPortNumOfAltMaster (i4NextContextId, i4NextDomainId,
                                       i4NextPortIndex, &i4NumOfAltMst);

        CliPrintf (CliHandle, "\r\n Record # %d\r\n", ++i4count);
        CliPrintf (CliHandle, "\tDevice type           :");

        /* Print the Device type based on interface type */
        switch (i4InterfaceType)
        {
            case PTP_IFACE_UDP_IPV4:
                CliPrintf (CliHandle, " UDP/IP Version 4\r\n");
                break;

            case PTP_IFACE_UDP_IPV6:
                CliPrintf (CliHandle, " UDP/IP Version 6\r\n");
                break;

            case PTP_IFACE_IEEE_802_3:
                CliPrintf (CliHandle, " IEEE 802.3\r\n");
                break;

            case PTP_IFACE_VLAN:
                CliPrintf (CliHandle, " VLAN\r\n");
                break;

            default:
                CliPrintf (CliHandle, "\r\n");

        }

        CliPrintf (CliHandle, "\tSwitch / Vrf          : %s\r\n",
                   au1ContextName);

        /* Get Interface Name from Interface Index */
        if (i4InterfaceType != PTP_IFACE_VLAN)
        {
            CfaCliConfGetIfName ((UINT4) i4InterfaceNum, (INT1 *) au1IfName);
            CliPrintf (CliHandle, "\tInterface             : %s\r\n",
                       au1IfName);
        }
        /*In case it is a vlan ,print vlan id */
        else
        {
            CliPrintf (CliHandle, "\tVlan id               : %d\r\n",
                       i4InterfaceNum);
        }
        /*Port ID is a combination of Clock ID and PTP Port Number */

        pu1Temp = &(au1TmpRetClockId[0]);
        for (u1Byte = 0; u1Byte < PTP_MAX_CLOCK_ID_LEN; u1Byte++)
        {
            pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:",
                                *(ClkIdentity.pu1_OctetList + u1Byte));
        }
        *(CHR1 *) (pu1Temp - 1) = '\0';

        CliPrintf (CliHandle, "\tPort identity         : %s :%d\r\n",
                   au1TmpRetClockId, i4NextPortIndex);
        CliPrintf (CliHandle, "\tPTP version           : %d\r\n",
                   i4VersionNumber);

        CliPrintf (CliHandle, "\tDelay mechanism       :");

        switch (i4DelayMechanism)
        {
            case PTP_PORT_DELAY_MECH_END_TO_END:

                CliPrintf (CliHandle, " End to End \r\n");
                break;

            case PTP_PORT_DELAY_MECH_PEER_TO_PEER:
                CliPrintf (CliHandle, " Peer to Peer \r\n");
                break;

            case PTP_PORT_DELAY_MECH_DISABLED:
                CliPrintf (CliHandle, " None\r\n");
                break;

            default:
                CliPrintf (CliHandle, "\r\n");
                break;

        }

        CliPrintf (CliHandle, "\tSync Fault Limit      : %s\r\n",
                   SyncLimit.pu1_OctetList);

        CliPrintf (CliHandle, "\tPTP Status            :");

        if (i4PtpStatus == OSIX_TRUE)
        {
            CliPrintf (CliHandle, " Enable\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " Disable\r\n");
        }

        CliPrintf (CliHandle, "\tPTP State             :");

        switch (i4PtpState)
        {
            case PTP_STATE_FAULTY:
                CliPrintf (CliHandle, " Faulty\r\n");
                break;

            case PTP_STATE_DISABLED:
                CliPrintf (CliHandle, " Disabled\r\n");
                break;

            case PTP_STATE_INITIALIZING:
                CliPrintf (CliHandle, " Initializing\r\n");
                break;

            case PTP_STATE_LISTENING:
                CliPrintf (CliHandle, " Listening\r\n");
                break;

            case PTP_STATE_UNCALIBRATED:
                CliPrintf (CliHandle, " Uncalibrated\r\n");
                break;

            case PTP_STATE_SLAVE:
                CliPrintf (CliHandle, " Slave\r\n");
                break;

            case PTP_STATE_PREMASTER:
                CliPrintf (CliHandle, " Pre Master\r\n");
                break;

            case PTP_STATE_MASTER:
                CliPrintf (CliHandle, " Master\r\n");
                break;

            case PTP_STATE_PASSIVE:
                CliPrintf (CliHandle, " Passive\r\n");
                break;

            default:
                CliPrintf (CliHandle, "\r\n");
                break;
        }

        if (u1AltMcastSync == PTP_ENABLED)
        {
            CliPrintf (CliHandle, "\tAlternate Master      : Enabled\r\n");
            CliPrintf (CliHandle, "\tAlternate SyncInterval : %u \r\n",
                       i4AltMcastSyncInterval);

        }

        CliPrintf (CliHandle, "\tMax Alternate Masters : %u\r\n",
                   i4NumOfAltMst);
        if (i4AccMstStatus == PTP_ENABLED)
        {
            CliPrintf (CliHandle, "\tAcceptable Master     : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\tAcceptable Master     : Disabled\r\n");
        }

        /* Log mean of Intervals */
        /* Display PTP Port Timer Information */
        PtpCliShowPortTimers (CliHandle, i4NextContextId, i4NextDomainId,
                              i4NextPortIndex);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliShowPortTimers
 *
 * DESCRIPTION      : This function displays the PTP Port Timers
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 *
 * RETURNS          : VOID
 *
 **************************************************************************/

PRIVATE VOID
PtpCliShowPortTimers (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                      INT4 i4PortIndex)
{

    tSNMP_OCTET_STRING_TYPE PeerMPD;
    FS_UINT8            u8DisplayConst;
    FS_UINT8            u8Reminder;
    FS_UINT8            u8Result;
    INT4                i4AnnounceIntvl = 0;
    INT4                i4DelayReqIntvl = 0;
    INT4                i4PDelayReqIntvl = 0;
    INT4                i4AnnounceTimeOut = 0;
    INT4                i4SyncIntrvl = 0;
    UINT1               au1PeerMPD[PTP_U8_STR_LEN];

    FSAP_U8_CLR (&u8DisplayConst);
    FSAP_U8_CLR (&u8Reminder);
    FSAP_U8_CLR (&u8Result);
    MEMSET (&PeerMPD, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1PeerMPD, 0, sizeof (au1PeerMPD));

    UINT8_LO (&u8DisplayConst) = PTP_DISPLAY_CONSTANT;

    PeerMPD.pu1_OctetList = au1PeerMPD;
    PeerMPD.i4_Length = sizeof (au1PeerMPD);

    nmhGetFsPtpPortMinDelayReqInterval (i4ContextId, i4DomainId,
                                        i4PortIndex, &i4DelayReqIntvl);
    nmhGetFsPtpPortPeerMeanPathDelay (i4ContextId, i4DomainId,
                                      i4PortIndex, &PeerMPD);
    nmhGetFsPtpPortAnnounceInterval (i4ContextId, i4DomainId,
                                     i4PortIndex, &i4AnnounceIntvl);
    nmhGetFsPtpPortAnnounceReceiptTimeout (i4ContextId,
                                           i4DomainId,
                                           i4PortIndex, &i4AnnounceTimeOut);
    nmhGetFsPtpPortSyncInterval (i4ContextId, i4DomainId,
                                 i4PortIndex, &i4SyncIntrvl);
    nmhGetFsPtpPortMinPdelayReqInterval (i4ContextId, i4DomainId,
                                         i4PortIndex, &i4PDelayReqIntvl);

    CliPrintf (CliHandle, "\tPort Timers :\r\n");
    CliPrintf (CliHandle, "\tAnnounce receipt Timeout     : %d\r\n",
               i4AnnounceTimeOut);

    /* Peer mean Delay in Scaled Nano second */
    CliPrintf (CliHandle, "\tPeer scaled mean path delay  : %s\r\n",
               PeerMPD.pu1_OctetList);

    /* Divding the Scalining factor 2^16 to get Mean delay in nanoseconds */
    FSAP_STR2_U8 ((CHR1 *) PeerMPD.pu1_OctetList, &u8Result);
    FSAP_U8_DIV (&u8Result, &u8Reminder, &u8Result, &u8DisplayConst);
    FSAP_U8_2STR (&u8Result, (CHR1 *) PeerMPD.pu1_OctetList);

    CliPrintf (CliHandle, "\tPeer mean path delay         : %s\r\n",
               PeerMPD.pu1_OctetList);
    CliPrintf (CliHandle, "\tAnnounce Interval            : %d\r\n",
               i4AnnounceIntvl);
    CliPrintf (CliHandle, "\tSync Interval                : %d\r\n",
               i4SyncIntrvl);

    CliPrintf (CliHandle, "\tDelay request Interval       : %d\r\n",
               i4DelayReqIntvl);
    CliPrintf (CliHandle, "\tPeer delay request Interval  : %d\r\n",
               i4PDelayReqIntvl);

    CliPrintf (CliHandle, "\r\n");
    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliShowTimeProperty
 *
 * DESCRIPTION      : This function displays the PTP Clock Time Properties
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowTimeProperty (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId)
{

    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4UTCOffset = 0;
    INT4                i4Leap59 = 0;
    INT4                i4Leap61 = 0;
    INT4                i4TimeSource = 0;
    INT4                i4Traceable = 0;
    INT4                i4TimeTraceable = 0;
    INT4                i4CurrentOffset = 0;

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " PTP Clock Time Property\r\n");
    CliPrintf (CliHandle, " -----------------------\r\n");

    i4RetVal =
        nmhValidateIndexInstanceFsPtpTimeDataSetTable (i4ContextId, i4DomainId);

    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n");
        return;
    }

    nmhGetFsPtpTimeCurrentUTCOffset (i4ContextId, i4DomainId, &i4CurrentOffset);
    nmhGetFsPtpTimeCurrentUTCOffsetValid (i4ContextId, i4DomainId,
                                          &i4UTCOffset);
    nmhGetFsPtpTimeLeap59 (i4ContextId, i4DomainId, &i4Leap59);
    nmhGetFsPtpTimeLeap61 (i4ContextId, i4DomainId, &i4Leap61);
    nmhGetFsPtpTimeTimeTraceable (i4ContextId, i4DomainId, &i4TimeTraceable);
    nmhGetFsPtpTimeFrequencyTraceable (i4ContextId, i4DomainId, &i4Traceable);
    nmhGetFsPtpTimeTimeSource (i4ContextId, i4DomainId, &i4TimeSource);

    CliPrintf (CliHandle, " Current UTC offset valid : %d\r\n",
               i4CurrentOffset);
    CliPrintf (CliHandle, " Current UTC offset       : %d\r\n", i4UTCOffset);
    CliPrintf (CliHandle, " Leap 59                  : %d\r\n", i4Leap59);
    CliPrintf (CliHandle, " Leap 61                  : %d\r\n", i4Leap61);
    CliPrintf (CliHandle, " Time traceable           : %d\r\n",
               i4TimeTraceable);
    CliPrintf (CliHandle, " Frequency traceable      : %d\r\n", i4Traceable);
    CliPrintf (CliHandle, " Time source              : %d\r\n\r\n",
               i4TimeSource);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliShowCurrentDS
 *
 * DESCRIPTION      : This function displays the PTP Clock Current
 *                    Time Properties
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *
 * OUTPUT           : Prints the PTP Current DataSet
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 * 
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowCurrentDS (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId)
{

    tSNMP_OCTET_STRING_TYPE OffsetFromMaster;
    tSNMP_OCTET_STRING_TYPE PeerMPD;
    FS_UINT8            u8DisplayConst;
    FS_UINT8            u8Reminder;
    FS_UINT8            u8Result;
    INT4                i4StepsRemoved = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT1               au1Offset[PTP_U8_STR_LEN];
    UINT1               au1PeerMPD[PTP_U8_STR_LEN];

    FSAP_U8_CLR (&u8DisplayConst);
    FSAP_U8_CLR (&u8Reminder);
    FSAP_U8_CLR (&u8Result);
    MEMSET (&OffsetFromMaster, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PeerMPD, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Offset, 0, sizeof (au1Offset));
    MEMSET (au1PeerMPD, 0, sizeof (au1PeerMPD));

    UINT8_LO (&u8DisplayConst) = PTP_DISPLAY_CONSTANT;

    OffsetFromMaster.pu1_OctetList = au1Offset;
    OffsetFromMaster.i4_Length = sizeof (au1Offset);
    PeerMPD.pu1_OctetList = au1PeerMPD;
    PeerMPD.i4_Length = sizeof (au1PeerMPD);

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " PTP Clock Current Information\r\n");
    CliPrintf (CliHandle, " -----------------------------\r\n");

    i4RetVal = nmhValidateIndexInstanceFsPtpCurrentDataSetTable
        (i4ContextId, i4DomainId);

    if (i4RetVal == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n");
        return;
    }

    nmhGetFsPtpCurrentStepsRemoved (i4ContextId, i4DomainId, &i4StepsRemoved);
    nmhGetFsPtpCurrentOffsetFromMaster (i4ContextId, i4DomainId,
                                        &OffsetFromMaster);

    nmhGetFsPtpCurrentMeanPathDelay (i4ContextId, i4DomainId, &PeerMPD);

    CliPrintf (CliHandle, " Current Steps Removed             : %d\r\n",
               i4StepsRemoved);
    CliPrintf (CliHandle, " Current Scaled Offset from Master : %s\r\n",
               OffsetFromMaster.pu1_OctetList);

    /* Divding the Scalining factor 2^16 to get Offset in nanoseconds */
    FSAP_STR2_U8 ((CHR1 *) OffsetFromMaster.pu1_OctetList, &u8Result);
    FSAP_U8_DIV (&u8Result, &u8Reminder, &u8Result, &u8DisplayConst);
    FSAP_U8_2STR (&u8Result, (CHR1 *) OffsetFromMaster.pu1_OctetList);
    CliPrintf (CliHandle, " Current Offset from Master        : %s\r\n",
               OffsetFromMaster.pu1_OctetList);

    CliPrintf (CliHandle, " Current Scaled Mean Path Delay    : %s\r\n",
               PeerMPD.pu1_OctetList);

    FSAP_U8_CLR (&u8Reminder);
    FSAP_U8_CLR (&u8Result);
    /* Divding the Scalining factor 2^16 to get Mean delay in nanoseconds */
    FSAP_STR2_U8 ((CHR1 *) PeerMPD.pu1_OctetList, &u8Result);
    FSAP_U8_DIV (&u8Result, &u8Reminder, &u8Result, &u8DisplayConst);
    FSAP_U8_2STR (&u8Result, (CHR1 *) PeerMPD.pu1_OctetList);

    CliPrintf (CliHandle, " Current Mean Path Delay           : %s\r\n\r\n",
               PeerMPD.pu1_OctetList);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliNoSetAltTimeDispName
 *
 * DESCRIPTION      : This function used to set default name the PTP Clock
 *                    Alternate Time Scale
 *
 * INPUT            : CliHandle   - Clihandle      
 *                    i4ContextId - Context Identifier
 *                    i4DomainId  - Domain Identifier
 *                    i4KeyIndex  - Domain Identifier
 *                    pu1AltTimeDispName - default name of the Timescale 
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE INT4
PtpCliNoSetAltTimeDispName (tCliHandle CliHandle, INT4 i4ContextId,
                            INT4 i4DomainId, INT4 i4KeyIndex,
                            UINT1 *pu1AltTimeDispName)
{
    tSNMP_OCTET_STRING_TYPE AltTimeDispName;
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;

    MEMSET (&AltTimeDispName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                             i4DomainId, i4KeyIndex,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if ((nmhGetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                           i4KeyIndex, &i4RowStatus)) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    AltTimeDispName.pu1_OctetList = pu1AltTimeDispName;
    AltTimeDispName.i4_Length = STRLEN (pu1AltTimeDispName);

    if ((nmhTestv2FsPtpAltTimeScaledisplayName (&u4ErrCode, i4ContextId,
                                                i4DomainId, i4KeyIndex,
                                                &AltTimeDispName)) ==
        SNMP_FAILURE)
    {
        if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                              i4KeyIndex, i4RowStatus) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
        }
        return (CLI_FAILURE);
    }

    if ((nmhSetFsPtpAltTimeScaledisplayName (i4ContextId, i4DomainId,
                                             i4KeyIndex,
                                             &AltTimeDispName)) == SNMP_FAILURE)
    {
        nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex, i4RowStatus);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                             i4DomainId, i4KeyIndex,
                                             i4RowStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex, i4RowStatus) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetAltTimeDispName
 *
 * DESCRIPTION      : This function displays the PTP Clock
 *                    Alternate Time Scale
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE INT4
PtpCliSetAltTimeDispName (tCliHandle CliHandle, INT4 i4ContextId,
                          INT4 i4DomainId, INT4 i4KeyIndex,
                          UINT1 *pu1AltTimeDispName)
{
    tSNMP_OCTET_STRING_TYPE AltTimeDispName;
    UINT4               u4ErrCode = 0;
    INT4                i4Status = 0;

    MEMSET (&AltTimeDispName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhGetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex, &i4Status)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                             i4DomainId,
                                             i4KeyIndex,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (pu1AltTimeDispName != NULL)
    {
        AltTimeDispName.pu1_OctetList = pu1AltTimeDispName;
        AltTimeDispName.i4_Length = STRLEN (pu1AltTimeDispName);

        if ((nmhTestv2FsPtpAltTimeScaledisplayName (&u4ErrCode, i4ContextId,
                                                    i4DomainId, i4KeyIndex,
                                                    &AltTimeDispName)) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if ((nmhSetFsPtpAltTimeScaledisplayName (i4ContextId, i4DomainId,
                                                 i4KeyIndex,
                                                 &AltTimeDispName)) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                             i4DomainId,
                                             i4KeyIndex,
                                             i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex, i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliUpdateATSEntryOptParams
 *
 * DESCRIPTION      : This function displays the PTP Clock
 *                    Alternate Time Scale
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *
 * OUTPUT           : Prints the Alternate Time Scale
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE INT4
PtpCliUpdateATSEntryOptParams (tCliHandle CliHandle, INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4KeyIndex,
                               INT4 i4ATSOffset, INT4 i4ATSJmpSecs,
                               UINT1 *pu1AltTimeScaleJump)
{
    tSNMP_OCTET_STRING_TYPE AltTimeScaletimeOfNextJump;
    UINT4               u4ErrCode = 0;
    INT4                i4Status = 0;

    MEMSET (&AltTimeScaletimeOfNextJump, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhGetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex, &i4Status)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                             i4DomainId,
                                             i4KeyIndex,
                                             NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsPtpAltTimeScalecurrentOffset (&u4ErrCode, i4ContextId,
                                                 i4DomainId,
                                                 i4KeyIndex,
                                                 i4ATSOffset) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScalecurrentOffset (i4ContextId, i4DomainId,
                                              i4KeyIndex,
                                              i4ATSOffset) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsPtpAltTimeScalejumpSeconds (&u4ErrCode,
                                               i4ContextId, i4DomainId,
                                               i4KeyIndex,
                                               i4ATSJmpSecs) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScalejumpSeconds (i4ContextId, i4DomainId,
                                            i4KeyIndex,
                                            i4ATSJmpSecs) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    if (pu1AltTimeScaleJump != NULL)
    {
        AltTimeScaletimeOfNextJump.pu1_OctetList = pu1AltTimeScaleJump;
        AltTimeScaletimeOfNextJump.i4_Length = STRLEN (pu1AltTimeScaleJump);

        if ((nmhTestv2FsPtpAltTimeScaletimeOfNextJump (&u4ErrCode,
                                                       i4ContextId,
                                                       i4DomainId,
                                                       i4KeyIndex,
                                                       &AltTimeScaletimeOfNextJump))
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPtpAltTimeScaletimeOfNextJump (i4ContextId,
                                                   i4DomainId,
                                                   i4KeyIndex,
                                                   &AltTimeScaletimeOfNextJump)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                             i4DomainId,
                                             i4KeyIndex,
                                             i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4KeyIndex, i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliShowAlternateTimeScale
 *
 * DESCRIPTION      : This function displays the PTP Clock
 *                    Alternate Time Scale
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *
 * OUTPUT           : Prints the Alternate Time Scale
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowAlternateTimeScale (tCliHandle CliHandle, INT4 i4ContextId,
                              INT4 i4DomainId)
{
    tSNMP_OCTET_STRING_TYPE AltTimeScaledisplayName;
    tSNMP_OCTET_STRING_TYPE AltTimeScaletimeOfNextJump;
    UINT4               u4TmpContextId = 0;
    UINT1               u1TmpDomainId = 0;
    INT4                i4NextContextId = 0;
    INT4                i4AltTimeScalecurrentOffset = 0;
    INT4                i4AltTimeScalejumpSeconds = 0;
    INT4                i4NextDomainId = 0;
    INT4                i4NextKeyIndex = 0;
    INT4                i4KeyIndex = 0xFF;
    INT4                i4Count = 0;
    INT4                i4RetVal = SNMP_FAILURE;
    INT4                i4RowStatus = 0;
    UINT1               au1AltTimeDispName[PTP_MAX_CLOCK_ID_LEN];
    UINT1               au1AltTimeScaleJump[PTP_U8_STR_LEN];
    BOOL1               bIsFirst = TRUE;

    MEMSET (&AltTimeScaledisplayName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&AltTimeScaletimeOfNextJump, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1AltTimeDispName, 0, sizeof (au1AltTimeDispName));
    MEMSET (au1AltTimeScaleJump, 0, sizeof (au1AltTimeScaleJump));

    AltTimeScaledisplayName.pu1_OctetList = au1AltTimeDispName;
    AltTimeScaledisplayName.i4_Length = sizeof (au1AltTimeDispName);
    AltTimeScaletimeOfNextJump.pu1_OctetList = au1AltTimeScaleJump;
    AltTimeScaletimeOfNextJump.i4_Length = sizeof (au1AltTimeScaleJump);

    CliPrintf (CliHandle, "\r\n PTP Alternate Time Scale");
    CliPrintf (CliHandle, "\r\n ------------------------\r\n");

    PtpUtilFindStartIndex ((UINT4) i4ContextId, &u4TmpContextId,
                           (UINT1) i4DomainId, &u1TmpDomainId, &bIsFirst);

    if (bIsFirst == OSIX_TRUE)
    {
        i4RetVal =
            nmhGetFirstIndexFsPtpAltTimeScaleDataSetTable
            (&i4NextContextId, &i4NextDomainId, &i4NextKeyIndex);
    }
    else
    {
        i4RetVal =
            nmhGetNextIndexFsPtpAltTimeScaleDataSetTable
            (u4TmpContextId, &i4NextContextId, u1TmpDomainId, &i4NextDomainId,
             i4KeyIndex, &i4NextKeyIndex);
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        return;
    }

    do
    {
        /* if not the requested context or domain exit */
        if ((i4NextContextId != i4ContextId) || (i4NextDomainId != i4DomainId))
        {
            break;
        }

        nmhGetFsPtpAltTimeScalecurrentOffset (i4NextContextId, i4NextDomainId,
                                              i4NextKeyIndex,
                                              &i4AltTimeScalecurrentOffset);
        nmhGetFsPtpAltTimeScalejumpSeconds (i4NextContextId, i4NextDomainId,
                                            i4NextKeyIndex,
                                            &i4AltTimeScalejumpSeconds);
        nmhGetFsPtpAltTimeScaletimeOfNextJump (i4NextContextId, i4NextDomainId,
                                               i4NextKeyIndex,
                                               &AltTimeScaletimeOfNextJump);
        nmhGetFsPtpAltTimeScaledisplayName (i4NextContextId, i4NextDomainId,
                                            i4NextKeyIndex,
                                            &AltTimeScaledisplayName);

        nmhGetFsPtpAltTimeScaleRowStatus (i4NextContextId, i4NextDomainId,
                                          i4NextKeyIndex, &i4RowStatus);

        if (i4RowStatus != ACTIVE)
            break;

        CliPrintf (CliHandle, " Record # %d\r\n", ++i4Count);

        CliPrintf (CliHandle, "\tAlternate Time Scale Key Index    : "
                   "%d\r\n", i4NextKeyIndex);
        CliPrintf (CliHandle, "\tAlternate Time Scale offset       : "
                   "%u\r\n", i4AltTimeScalecurrentOffset);
        CliPrintf (CliHandle, "\tAlternate Time Scale jump Seconds : "
                   "%u\r\n", i4AltTimeScalejumpSeconds);
        CliPrintf (CliHandle, "\tAlternate Time Scale Next jump    : "
                   "%s\r\n", AltTimeScaletimeOfNextJump.pu1_OctetList);

        /* If Display Name is Configured Display */
        if ((AltTimeScaledisplayName.pu1_OctetList != NULL))
        {
            CliPrintf (CliHandle, "\tAlternate Time Scale Display Name : "
                       "%s\r\n", AltTimeScaledisplayName.pu1_OctetList);
        }

        MEMSET (au1AltTimeDispName, 0, sizeof (au1AltTimeDispName));
        MEMSET (au1AltTimeScaleJump, 0, sizeof (au1AltTimeScaleJump));

        i4KeyIndex = i4NextKeyIndex;
        i4ContextId = i4NextContextId;
        i4DomainId = i4NextDomainId;
    }
    while ((nmhGetNextIndexFsPtpAltTimeScaleDataSetTable
            (i4ContextId, &i4NextContextId, i4DomainId, &i4NextDomainId,
             i4KeyIndex, &i4NextKeyIndex)) != SNMP_FAILURE);

    CliPrintf (CliHandle, "\r\n");
    return;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliShowContextInfo
 *
 * DESCRIPTION      : This function displays Ptp context Information
 *
 * INPUT            : CliHandle   - tCliHandle identifier
 *                  : i4ContextId - Context Identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliShowContextInfo (tCliHandle CliHandle, INT4 i4ContextId)
{
    tSNMP_OCTET_STRING_TYPE *pDebugOpt = NULL;
    INT4                i4AdminStatus = 0;
    INT4                i4PtpPrimaryDomain = 0;
    INT4                i4RowStatus = 0;

    nmhGetFsPtpContextRowStatus (i4ContextId, &i4RowStatus);

    if (i4RowStatus == ACTIVE)
    {
        pDebugOpt = allocmem_octetstring ((PTP_TRC_MAX_SIZE + 1));
        if (pDebugOpt == NULL)
        {
            return (CLI_FAILURE);
        }
        MEMSET (pDebugOpt->pu1_OctetList, 0, PTP_TRC_MAX_SIZE + 1);
        pDebugOpt->i4_Length = 0;

        nmhGetFsPtpAdminStatus (i4ContextId, &i4AdminStatus);
        nmhGetFsPtpPrimaryDomain (i4ContextId, &i4PtpPrimaryDomain);
        nmhGetFsPtpTraceOption (i4ContextId, pDebugOpt);
        pDebugOpt->pu1_OctetList[pDebugOpt->i4_Length] = '\0';

        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, " PTP Context Information \r\n");
        CliPrintf (CliHandle, " ----------------------- \r\n");

        if (i4AdminStatus == PTP_ENABLED)
        {
            CliPrintf (CliHandle, " PTP Status     : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " PTP Status     : Disabled\r\n");
        }
        CliPrintf (CliHandle, " Primary Domain : %d\r\n", i4PtpPrimaryDomain);
        CliPrintf (CliHandle, " Debug Status   : %s\r\n",
                   pDebugOpt->pu1_OctetList);

        free_octetstring (pDebugOpt);
    }
    CliPrintf (CliHandle, " \r\n");
    return (CLI_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliShowGlobalInfo
 *
 * DESCRIPTION      : This function displays the PTP Module Status 
 *                    Information 
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliShowGlobalInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE PtpNotification;
    INT4                i4ModuleStatus = 0;
    INT4                i4PrimaryCxtId = 0;
    UINT1               au1ContextName[PTP_CLI_MAX_ALIAS_LEN];
    UINT1               au1Trap[PTP_TRAP_MAX_LEN];
    UINT1               u1Val = 0;

    PtpNotification.pu1_OctetList = &(au1Trap[0]);
    PtpNotification.i4_Length = sizeof (au1Trap);

    MEMSET (&(au1Trap[0]), 0, sizeof (au1Trap));
    MEMSET (&au1ContextName, 0, sizeof (au1ContextName));
    nmhGetFsPtpGlobalSysCtrl (&i4ModuleStatus);

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " PTP System Status\r\n");
    CliPrintf (CliHandle, " -----------------\r\n");

    if (i4ModuleStatus == PTP_START)
    {
        CliPrintf (CliHandle, " Global Status : Start\r\n");
    }

    /*Display Primary Context Information */

    nmhGetFsPtpPrimaryContext (&i4PrimaryCxtId);

    PtpPortVcmGetAliasName (i4PrimaryCxtId, au1ContextName);

    CliPrintf (CliHandle, " Primary Context : %s\r\n", au1ContextName);

    nmhGetFsPtpNotification (&PtpNotification);

    u1Val = PtpNotification.pu1_OctetList[0];
    CliPrintf (CliHandle, " Enabled Notifications : \r\n");

    if ((u1Val & PTP_TRAP_MASK_GLOB_ERR) == PTP_TRAP_MASK_GLOB_ERR)
    {
        CliPrintf (CliHandle, "\t- global error\r\n");
    }

    if ((u1Val & PTP_TRAP_MASK_SYS_CTRL_CHNG) == PTP_TRAP_MASK_SYS_CTRL_CHNG)
    {
        CliPrintf (CliHandle, "\t- system control\r\n");
    }

    if ((u1Val & PTP_TRAP_MASK_SYS_ADMIN_CHNG) == PTP_TRAP_MASK_SYS_ADMIN_CHNG)
    {
        CliPrintf (CliHandle, "\t- system admin change\r\n");
    }

    if ((u1Val & PTP_TRAP_MASK_PORT_STATE_CHNG) ==
        PTP_TRAP_MASK_PORT_STATE_CHNG)
    {
        CliPrintf (CliHandle, "\t- port state change\r\n");
    }

    if ((u1Val & PTP_TRAP_MASK_PORT_ADMIN_CHNG) ==
        PTP_TRAP_MASK_PORT_ADMIN_CHNG)
    {
        CliPrintf (CliHandle, "\t- port admin change\r\n");
    }

    if ((u1Val & PTP_TRAP_MASK_SYNC_FAULT) == PTP_TRAP_MASK_SYNC_FAULT)
    {
        CliPrintf (CliHandle, "\t- sync fault\r\n");
    }

    if ((u1Val & PTP_TRAP_MASK_GM_FAULT) == PTP_TRAP_MASK_GM_FAULT)
    {
        CliPrintf (CliHandle, "\t- grand master fault\r\n");
    }

    if ((u1Val & PTP_TRAP_MASK_ACC_MASTER_FAULT) ==
        PTP_TRAP_MASK_ACC_MASTER_FAULT)
    {
        CliPrintf (CliHandle, "\t- acceptable master fault\r\n");
    }

    /* Trap Octet[1] Mask values */
    u1Val = PtpNotification.pu1_OctetList[1];
    if ((u1Val & PTP_TRAP_MASK_UCAST_ADMIN_CHNG) ==
        PTP_TRAP_MASK_UCAST_ADMIN_CHNG)
    {
        CliPrintf (CliHandle, "\t- unicast admin change\r\n");
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliShowUnicastMasterRecords
 *
 * DESCRIPTION      : This function displays the PTP Clock
 *                    Unicast Master Records
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *
 * OUTPUT           : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowUnicastMasterRecords (tCliHandle CliHandle,
                                INT4 i4ContextId, INT4 i4DomainId)
{
    tSNMP_OCTET_STRING_TYPE UcastMasterAddr;
    tSNMP_OCTET_STRING_TYPE NextUcastMasterAddr;
    INT4                i4NextDomainId = 0;
    INT4                i4NextContextId = 0;
    INT4                i4NextAddrLen = 0;
    INT4                i4AddrLen = 0;
    INT4                i4AccMasterProto = 0;
    INT4                i4NextAccMasterProto = 0;
    UINT1               au1UcastMaster[PTP_MAX_ADDR_LEN];
    UINT1               au1NextUcastMaster[PTP_MAX_ADDR_LEN];

    UcastMasterAddr.pu1_OctetList = au1UcastMaster;
    UcastMasterAddr.i4_Length = sizeof (au1UcastMaster);
    NextUcastMasterAddr.pu1_OctetList = au1NextUcastMaster;
    NextUcastMasterAddr.i4_Length = sizeof (au1NextUcastMaster);

    CliPrintf (CliHandle, "PTP Unicast Masters\r\n");
    CliPrintf (CliHandle, " -------------------\r\n");

    while (nmhGetNextIndexFsPtpAccMasterDataSetTable
           (i4ContextId, &i4NextContextId, i4DomainId,
            &i4NextDomainId, i4AddrLen, &i4NextAddrLen,
            i4AccMasterProto, &i4NextAccMasterProto,
            &UcastMasterAddr, &NextUcastMasterAddr) != SNMP_FAILURE)
    {
        if ((i4ContextId != i4NextContextId) || (i4DomainId != i4NextDomainId))
        {
            break;
        }

        CliPrintf (CliHandle, "Unicast Master Network Protocol:"
                   "%d\r\n", i4NextAccMasterProto);
        CliPrintf (CliHandle, "Unicast Master Address: "
                   "%s\r\n", NextUcastMasterAddr.pu1_OctetList);

        i4AddrLen = i4NextAddrLen;
        i4AccMasterProto = i4NextAccMasterProto;

        MEMCPY (&UcastMasterAddr, &NextUcastMasterAddr,
                sizeof (NextUcastMasterAddr));

    }
    return;

}

/***************************************************************************
 * FUNCTION NAME             : PtpCliShowAcceptableMstRecords
 *
 * DESCRIPTION               : This function displays the PTP Clock
 *                             Acceptable Master Records
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowAcceptableMstRecords (tCliHandle CliHandle,
                                INT4 i4ContextId, INT4 i4DomainId)
{
    tSNMP_OCTET_STRING_TYPE accMasterAddr;
    tSNMP_OCTET_STRING_TYPE NextaccMasterAddr;
    UINT1              *pu1Temp = NULL;
    INT4                i4NextDomainId = 0;
    INT4                i4NextContextId = 0;
    INT4                i4AddrLen = 0;
    INT4                i4NextAddrLen = 0;
    INT4                i4AccMasterProto = 0;
    INT4                i4NextAccMasterProto = 0;
    INT4                i4Count = 0;
    INT4                i4AlternatePriority = 0;
    UINT1               au1Accmaster[PTP_MAX_ADDR_LEN];
    UINT1               au1NextAccMaster[PTP_MAX_ADDR_LEN];
    UINT1               au1TmpAccAddr[PTP_MAX_ADDR_LEN];
    UINT1               u1Byte = 0;
    tIp6Addr            Ip6Address;

    MEMSET (&accMasterAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextaccMasterAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1Accmaster, 0, PTP_MAX_ADDR_LEN);
    MEMSET (&au1NextAccMaster, 0, PTP_MAX_ADDR_LEN);
    MEMSET (&au1TmpAccAddr, 0, PTP_MAX_ADDR_LEN);

    accMasterAddr.pu1_OctetList = au1Accmaster;
    accMasterAddr.i4_Length = STRLEN (au1Accmaster);
    NextaccMasterAddr.pu1_OctetList = au1NextAccMaster;
    NextaccMasterAddr.i4_Length = STRLEN (au1NextAccMaster);

    CliPrintf (CliHandle, "\r\n PTP Acceptable Masters");
    CliPrintf (CliHandle, "\r\n ----------------------\r\n");

    while (nmhGetNextIndexFsPtpAccMasterDataSetTable
           (i4ContextId, &i4NextContextId, i4DomainId,
            &i4NextDomainId, i4AccMasterProto, &i4NextAccMasterProto,
            i4AddrLen, &i4NextAddrLen, &accMasterAddr,
            &NextaccMasterAddr) != SNMP_FAILURE)
    {
        if ((i4ContextId != i4NextContextId) || (i4DomainId != i4NextDomainId))
        {
            break;
        }
        nmhGetFsPtpAccMasterAlternatePriority (i4NextContextId,
                                               i4NextDomainId,
                                               i4NextAccMasterProto,
                                               i4NextAddrLen,
                                               &NextaccMasterAddr,
                                               &i4AlternatePriority);

        MEMSET (&au1TmpAccAddr[0], 0, PTP_MAX_ADDR_LEN);
        pu1Temp = &au1TmpAccAddr[0];

        CliPrintf (CliHandle, "Record # %d\r\n", (++i4Count));

        CliPrintf (CliHandle, "\tProtocol     : ");
        if (i4NextAccMasterProto == CLI_PTP_IPV4)
        {
            CliPrintf (CliHandle, " UDP /IP Version 4\r\n");
            for (u1Byte = 0; u1Byte < NextaccMasterAddr.i4_Length; u1Byte++)
            {
                pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%d.",
                                    *(NextaccMasterAddr.pu1_OctetList +
                                      u1Byte));
            }
            *(CHR1 *) (pu1Temp - 1) = '\0';
            CliPrintf (CliHandle, "\tAddress      :  %s\r\n", au1TmpAccAddr);
        }
        else if (i4NextAccMasterProto == CLI_PTP_IPV6)
        {
            MEMCPY (&Ip6Address, NextaccMasterAddr.pu1_OctetList,
                    NextaccMasterAddr.i4_Length);
            CliPrintf (CliHandle, " UDP /IP Version 6\r\n");
            CliPrintf (CliHandle, "\tAddress      :  %s\r\n",
                       Ip6PrintNtop (&Ip6Address));
        }
        else if (i4NextAccMasterProto == CLI_PTP_ETH)
        {
            CliPrintf (CliHandle, " Ethernet\r\n");
            CliMacToStr (NextaccMasterAddr.pu1_OctetList, au1TmpAccAddr);
            CliPrintf (CliHandle, "\tAddress      :  %s\r\n", au1TmpAccAddr);
        }
        else
        {
            CliPrintf (CliHandle, " Unknown\r\n");
        }

        CliPrintf (CliHandle, "\tAltPriority  :  %d\r\n", i4AlternatePriority);

        i4AddrLen = i4NextAddrLen;
        i4AccMasterProto = i4NextAccMasterProto;
        accMasterAddr.pu1_OctetList = NextaccMasterAddr.pu1_OctetList;
        accMasterAddr.i4_Length = NextaccMasterAddr.i4_Length;
    }

    CliPrintf (CliHandle, "\r\n");
    return;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliShowSecurityAssociations
 *
 * DESCRIPTION      : This function displays the PTP Clock
 *                    Security Associations
 *
 * INPUT                     : i4ContextId - Context Identifier
 *
 *
 * OUTPUT                    :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : VOID
 *
 **************************************************************************/
PRIVATE VOID
PtpCliShowSecurityAssociations (tCliHandle CliHandle,
                                INT4 i4ContextId, INT4 i4DomainId)
{

    CliPrintf (CliHandle, "PTP Security Associations\r\n");
    CliPrintf (CliHandle, "---------------------------\r\n");

    if ((i4ContextId == 0) || (i4DomainId == 0))
    {
        return;
    }
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliGetContextInfoForShowCmd
 *
 * DESCRIPTION      : This Routine validates the string input for the switch
 *                    name or loop through for all the context. It also checks
 *                    if PTP is Started or not.
 *
 * INPUT            : u4CurrContextId - Context Identifier
 *                    pu1ContextName - Entered Context Name
 *                    u4Command      - Command Identifier
 *
 * OUTPUT           : pu4ContextId - Next Context Id or
 *                                   Context Id of the entered Context name
 
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliGetContextInfoForShowCmd (tCliHandle CliHandle, UINT1 *pu1ContextName,
                                UINT4 u4CurrContextId, UINT4 *pu4ContextId,
                                UINT4 u4Command)
{
    UINT4               u4ContextId = 0;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    if (pu1ContextName != NULL)
    {
        if (PtpPortVcmIsSwitchExist (pu1ContextName, &u4ContextId) != OSIX_TRUE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1ContextName);
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPtpTable ((INT4) u4CurrContextId,
                                       (INT4 *) &u4ContextId) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    do
    {
        u4CurrContextId = u4ContextId;

        MEMSET (au1ContextName, 0, sizeof (au1ContextName));

        if (pu1ContextName == NULL)
        {
            PtpPortVcmGetAliasName (u4ContextId, au1ContextName);
        }
        else
        {
            MEMCPY (au1ContextName, pu1ContextName, STRLEN (pu1ContextName));
        }

        if (PtpUtilIsPtpStarted (u4ContextId) != OSIX_TRUE)
        {
            if (u4Command != CLI_PTP_CLEAR_STATS_ALL)
            {
                CliPrintf (CliHandle, "\r\n %%PTP is ShutDown\r\n");
            }

            if (pu1ContextName != NULL)
            {
                break;
            }

            continue;
        }

        *pu4ContextId = u4ContextId;
        return OSIX_SUCCESS;
    }
    while (nmhGetNextIndexFsPtpTable ((INT4) u4CurrContextId,
                                      (INT4 *) &u4ContextId) == SNMP_SUCCESS);

    return OSIX_FAILURE;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliShowDebugging 
 *
 * DESCRIPTION      : This function displays the Trace options enabled
 *                    in a context.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None 
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
PtpCliShowDebugging (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4DbgLevel = 0;
    tPtpCxt            *pContextInfo = NULL;

    pContextInfo = PtpCxtGetNode (u4ContextId);

    if (pContextInfo == NULL)
    {
        return CLI_FAILURE;
    }

    if (PtpUtilIsPtpStarted (u4ContextId) == OSIX_FALSE)
    {
        return CLI_FAILURE;
    }

    u4DbgLevel = (UINT4) pContextInfo->u2Trace;

    if (u4DbgLevel == 0)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "PTP :\r\n");
    if ((u4DbgLevel & PTP_ALL_TRC) == PTP_ALL_TRC)
    {
        CliPrintf (CliHandle, "  PTP All debugging is on\r\n");
    }
    if ((u4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "  PTP init and shutdown debugging is on\r\n");
    }
    if ((u4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "  PTP management debugging is on\r\n");
    }
    if ((u4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "  PTP control plane debugging is on\r\n");
    }
    if ((u4DbgLevel & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "  PTP packet dump debugging is on\r\n");
    }
    if ((u4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "  PTP resources debugging is on\r\n");
    }
    if ((u4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "  PTP all fail debugging is on\r\n");
    }
    if ((u4DbgLevel & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "  PTP buffer debugging is on\r\n");
    }
    if ((u4DbgLevel & PTP_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, "  PTP critical debugging is on\r\n");
    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliUpdateAccMasterEntry
 *
 * DESCRIPTION      :  This function creates or updates a Acc master entry
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpTransparentPortIndex);
    *pi4RetValFsPtpTransparentPortPtpStatus = OSIX_TRUE;
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliUpdateAccMasterEntry (tCliHandle CliHandle, INT4 i4ContextId,
                            INT4 i4DomainId, INT4 i4NwProto,
                            INT4 i4AddrLen,
                            tSNMP_OCTET_STRING_TYPE pAccMasterAddr,
                            INT4 i4RowStatus)
{
    UINT4               u4ErrCode = 0;
    INT4                i4Status = 0;
    INT4                i4Result = 0;
    /* Check if the Domain config entry already exists for the
     * given indices in the database.
     * If the Get function returns Success, for CREATE and WAIT then a
     * user already exists. Otherwise we need to create an entry.
     */
    nmhGetFsPtpAccMasterRowStatus (i4ContextId, i4DomainId, i4NwProto,
                                   i4AddrLen, &pAccMasterAddr, &i4Status);

    switch (i4RowStatus)
    {
        case CREATE_AND_GO:
            i4Status = CREATE_AND_GO;
            break;
        case NOT_IN_SERVICE:

            i4Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            i4Status = DESTROY;
            break;

        case ACTIVE:
            i4Status = ACTIVE;
            break;

        default:
            CliPrintf (CliHandle,
                       "\r%% Invalid Rowstatus for the Acc Master"
                       "Config table\r\n");
            return CLI_FAILURE;
    }

    i4Result = nmhTestv2FsPtpAccMasterRowStatus (&u4ErrCode, i4ContextId,
                                                 i4DomainId, i4NwProto,
                                                 i4AddrLen, &pAccMasterAddr,
                                                 i4Status);
    if (i4Result == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (i4Result);
    UNUSED_PARAM (u4ErrCode);

    if (nmhSetFsPtpAccMasterRowStatus (i4ContextId, i4DomainId, i4NwProto,
                                       i4AddrLen, &pAccMasterAddr,
                                       i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

 /***************************************************************************
 * FUNCTION NAME    : PtpCliDeletePortFromDomain
 *
 * DESCRIPTION      :  This function deletes a Port from a PTP Domain
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliDeletePortFromDomain (tCliHandle CliHandle, INT4 i4ContextId,
                            INT4 i4DomainId, INT4 i4PtpPortNo)
{
    INT4                i4ClockMode = 0;
    INT4                i4RetStatus = CLI_FAILURE;

    /* Get the Clock Mode and then Delete the respective Port */

    i4RetStatus = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                              &i4ClockMode);
    if (i4RetStatus != SNMP_SUCCESS)
    {
        return i4RetStatus;
    }

    if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
    }
    else
    {
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId, i4PtpPortNo,
                                  DESTROY);
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliMapPortToDomain
 *
 * DESCRIPTION      :  This function maps a Port to a PTP Domain
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliMapPortToDomain (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                       INT4 i4IfType, INT4 i4IfIndex, INT4 i4PtpPortNo)
{
    INT4                i4ClockMode = 0;
    INT4                i4RetStatus = CLI_FAILURE;

    /* Get the Clock Mode and then Map Port */

    i4RetStatus = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                              &i4ClockMode);
    if (i4RetStatus != SNMP_SUCCESS)
    {
        return i4RetStatus;
    }

    if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        i4RetStatus = PtpCliMapPortToTransClk (CliHandle, i4ContextId,
                                               i4DomainId,
                                               i4IfType, i4IfIndex,
                                               i4PtpPortNo);
    }
    else
    {
        i4RetStatus = PtpCliMapPortToClk (CliHandle, i4ContextId, i4DomainId,
                                          i4IfType, i4IfIndex, i4PtpPortNo);
    }

    return i4RetStatus;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliMapPortToTransClk
 *
 * DESCRIPTION      :  This function maps a Port to a PTP Domain for TC
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliMapPortToTransClk (tCliHandle CliHandle, INT4 i4ContextId,
                         INT4 i4DomainId, INT4 i4IfType, INT4 i4IfIndex,
                         INT4 i4PtpPortNo)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpTransparentPortRowStatus (&u4ErrorCode, i4ContextId,
                                                i4DomainId,
                                                i4PtpPortNo,
                                                CREATE_AND_WAIT) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo,
                                             CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Configure Interface Type for PTP Port */

    if (nmhTestv2FsPtpTransparentPortInterfaceType
        (&u4ErrorCode, i4ContextId, i4DomainId, i4PtpPortNo,
         i4IfType) != SNMP_SUCCESS)
    {
        /* Destroy Whenever Failure Occurs */
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpTransparentPortInterfaceType (i4ContextId, i4DomainId,
                                                 i4PtpPortNo,
                                                 i4IfType) != SNMP_SUCCESS)
    {
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Configure Interface Number for PTP Port */

    if (nmhTestv2FsPtpTransparentPortIfaceNumber
        (&u4ErrorCode, i4ContextId, i4DomainId, i4PtpPortNo,
         i4IfIndex) != SNMP_SUCCESS)
    {
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpTransparentPortIfaceNumber (i4ContextId, i4DomainId,
                                               i4PtpPortNo,
                                               i4IfIndex) != SNMP_SUCCESS)
    {
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Make the Port Row Status Active */

    if (nmhTestv2FsPtpTransparentPortRowStatus (&u4ErrorCode, i4ContextId,
                                                i4DomainId,
                                                i4PtpPortNo,
                                                ACTIVE) != SNMP_SUCCESS)
    {
        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo,
                                             ACTIVE) != SNMP_SUCCESS)
    {
        /* If Activation Fails Destroy */

        nmhSetFsPtpTransparentPortRowStatus (i4ContextId, i4DomainId,
                                             i4PtpPortNo, DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliMapPortToClk
 *
 * DESCRIPTION      :  This function maps a Port to a PTP Domain for OC,BC
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliMapPortToClk (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId,
                    INT4 i4IfType, INT4 i4IfIndex, INT4 i4PtpPortNo)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpPortRowStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                     i4PtpPortNo,
                                     CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Configure Interface Type for PTP Port */

    if (nmhTestv2FsPtpPortInterfaceType (&u4ErrorCode, i4ContextId, i4DomainId,
                                         i4PtpPortNo, i4IfType) != SNMP_SUCCESS)
    {
        /* Destroy Whenever Failure Occurs */
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);
        return CLI_FAILURE;

    }

    if (nmhSetFsPtpPortInterfaceType (i4ContextId, i4DomainId,
                                      i4PtpPortNo, i4IfType) != SNMP_SUCCESS)
    {
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Configure Interface Number for PTP Port */

    if (nmhTestv2FsPtpPortIfaceNumber (&u4ErrorCode, i4ContextId, i4DomainId,
                                       i4PtpPortNo, i4IfIndex) != SNMP_SUCCESS)
    {
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortIfaceNumber (i4ContextId, i4DomainId,
                                    i4PtpPortNo, i4IfIndex) != SNMP_SUCCESS)
    {
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Make the Port Row Status Active */
    if (nmhTestv2FsPtpPortRowStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                     i4PtpPortNo, ACTIVE) != SNMP_SUCCESS)
    {
        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, ACTIVE) != SNMP_SUCCESS)
    {
        /* If Activation Fails Destroy */

        nmhSetFsPtpPortRowStatus (i4ContextId, i4DomainId,
                                  i4PtpPortNo, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    :PtpCliUpdateClockNumberPorts 
 *
 * DESCRIPTION      :This function Configures the Number of Ports in a Clock
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetClockNumberPorts (tCliHandle CliHandle, INT4 i4ContextId,
                           INT4 i4DomainId, INT4 i4NumberPorts)
{
    INT4                i4ClockMode = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetStatus = CLI_FAILURE;

    /* Get the Clock Mode and then Map Port */

    i4RetStatus = nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId,
                                              &i4ClockMode);
    if (i4RetStatus != SNMP_SUCCESS)
    {
        return i4RetStatus;
    }

    /* Set the Domain Row Status as NOT_IN_SERVICE */

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                       (INT4) NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId,
                                    (INT4) NOT_IN_SERVICE) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4ClockMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        i4RetStatus = PtpCliUpdateTransClkNumberPorts (CliHandle, i4ContextId,
                                                       i4DomainId,
                                                       i4NumberPorts);
    }
    else
    {
        i4RetStatus = PtpCliUpdateClockNumberPorts (CliHandle, i4ContextId,
                                                    i4DomainId, i4NumberPorts);
    }

    if (i4RetStatus == CLI_FAILURE)
    {
        return i4RetStatus;
    }

    /* Set the Domain Row Status as ACTIVE */

    if (nmhTestv2FsPtpDomainRowStatus (&u4ErrorCode, i4ContextId, i4DomainId,
                                       (INT4) ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPtpDomainRowStatus (i4ContextId, i4DomainId,
                                    (INT4) ACTIVE) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    :PtpCliUpdateClockNumberPorts 
 *
 * DESCRIPTION      :This function Configures the Number of Ports in a Clock
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliUpdateClockNumberPorts (tCliHandle CliHandle, INT4 i4ContextId,
                              INT4 i4DomainId, INT4 i4NumberPorts)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPtpClockNumberPorts (&u4ErrCode, i4ContextId, i4DomainId,
                                        i4NumberPorts) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPtpClockNumberPorts (i4ContextId, i4DomainId,
                                     i4NumberPorts) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    :PtpCliUpdateTransClkNumberPorts 
 *
 * DESCRIPTION      :This function Configures the Number of Ports in a TC
 *
 * INPUT            : i4ContextId - Context Identifier
 *                    i4DomainId - Domain Identifier
 *                    
 *
 * OUTPUT           : 
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliUpdateTransClkNumberPorts (tCliHandle CliHandle, INT4 i4ContextId,
                                 INT4 i4DomainId, INT4 i4NumberPorts)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPtpTransparentClockNumberPorts
        (&u4ErrCode, i4ContextId, i4DomainId, i4NumberPorts) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpTransparentClockNumberPorts (i4ContextId, i4DomainId,
                                                i4NumberPorts) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliConfigAltTimeScale 
 *
 * DESCRIPTION      : This function Configures the Alternate Time Scale
 *                    Parameters
 *
 * INPUT            : i4DomainId - Doamin Identifier
 *                    i4ContextId - Context Identifier   
 *
 *
 * OUTPUT           : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliConfigAltTimeScale (tCliHandle CliHandle, INT4 i4ContextId,
                          INT4 i4DomainId, INT4 i4AltTimeScaleKeyId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Status = 0;

    if (nmhGetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4AltTimeScaleKeyId, &i4Status)
        == SNMP_FAILURE)
    {
        /* First time the entry is being created */
        i4Status = ACTIVE;
    }

    if (PtpCliUpdateAltTimeScaleEntry (CliHandle, i4ContextId, i4DomainId,
                                       i4AltTimeScaleKeyId,
                                       CREATE_AND_GO) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrorCode, i4ContextId,
                                             i4DomainId,
                                             i4AltTimeScaleKeyId,
                                             i4Status) == SNMP_FAILURE)
    {
        if (PtpCliUpdateAltTimeScaleEntry (CliHandle, i4ContextId, i4DomainId,
                                           i4AltTimeScaleKeyId,
                                           DESTROY) == CLI_FAILURE)
            return CLI_FAILURE;
    }

    if (PtpCliUpdateAltTimeScaleEntry (CliHandle, i4ContextId, i4DomainId,
                                       i4AltTimeScaleKeyId,
                                       i4Status) != CLI_SUCCESS)
    {
        nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                          i4AltTimeScaleKeyId, DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliDelAltTimeScale    
 *
 * DESCRIPTION      : This function delete the Alternate Time Scale Entry
 *
 * INPUT            : CliHandle  - CliHandle          
 *                    i4DomainId - Doamin Identifier
 *                    i4ContextId - Context Identifier   
 *                    i4AltTimeScaleKeyId - Alternate timescale KeyId
 *
 *
 * OUTPUT           : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliDelAltTimeScale (tCliHandle CliHandle, INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4AltTimeScaleKeyId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrorCode, i4ContextId,
                                             i4DomainId, i4AltTimeScaleKeyId,
                                             DESTROY) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if ((nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                           i4AltTimeScaleKeyId, DESTROY)) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliUpdateAltTimeScaleEntry
 *
 * DESCRIPTION      :  This function creates or updates a Alt Time scal
 *                     entry
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliUpdateAltTimeScaleEntry (tCliHandle CliHandle, INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4AltTimeScaleKeyId,
                               INT4 i4RowStatus)
{
    UINT4               u4ErrCode = 0;
    INT4                i4Status = 0;
    INT4                i4Result = 0;

    /* Check if the Alternate Time scale entry already exists for the
     * given indices in the database.
     * If the Get function returns Success, for CREATE and WAIT then a
     * user already exists. Otherwise we need to create an entry.
     */
    nmhGetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId,
                                      i4AltTimeScaleKeyId, &i4Status);
    switch (i4RowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (i4Status == 0)
            {
                i4Status = CREATE_AND_GO;
            }
            else
            {
                i4Status = NOT_IN_SERVICE;
            }
            break;
        case NOT_IN_SERVICE:

            i4Status = NOT_IN_SERVICE;
            break;

        case DESTROY:
            i4Status = DESTROY;
            break;

        case ACTIVE:
            i4Status = ACTIVE;
            break;

        default:
            CliPrintf (CliHandle,
                       "\r%% Invalid Rowstatus for the Alternate Time"
                       "Scale Config table\r\n");
            return CLI_FAILURE;
    }

    i4Result = nmhTestv2FsPtpAltTimeScaleRowStatus (&u4ErrCode, i4ContextId,
                                                    i4DomainId,
                                                    i4AltTimeScaleKeyId,
                                                    i4Status);
    if (i4Result == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpAltTimeScaleRowStatus (i4ContextId,
                                          i4DomainId,
                                          i4AltTimeScaleKeyId,
                                          i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetAccMasterFlag
 *
 * DESCRIPTION      :  This function Disables /Enables  Accept Master
 *                     option flag on a port , this enables the Slave port
 *                     to transmit the Sync Message with ALT MASTER FLAG
 *                     set
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliSetAccMasterFlag (tCliHandle CliHandle, INT4 i4ContextId,
                        INT4 i4DomainId, INT4 i4PortId, INT4 i4Status)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPtpPortAccMasterEnabled (&u4ErrCode, i4ContextId, i4DomainId,
                                            i4PortId, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortAccMasterEnabled (i4ContextId, i4DomainId,
                                         i4PortId, i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_FAILURE;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetAltMasterFlag
 *
 * DESCRIPTION      :  This function Disables /Enables  Alternate Master
 *                     option flag on a port , this enables the Slave port 
 *                     to transmit the Sync Message with ALT MASTER FLAG 
 *                     set
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliSetAltMasterFlag (tCliHandle CliHandle, INT4 i4ContextId,
                        INT4 i4DomainId, INT4 i4PortId, INT4 i4Status)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPtpPortAltMulcastSync (&u4ErrCode, i4ContextId, i4DomainId,
                                          i4PortId, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortAltMulcastSync (i4ContextId, i4DomainId,
                                       i4PortId, i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_FAILURE;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetAltMcastSyncInterval
 *
 * DESCRIPTION      :  This function Disables /Enables  Alt Time
 *                     in a Port
 *
 * INPUT            : i4ContextId - Context Identifier
 *
 *
 * OUTPUT           :
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliSetAltMcastSyncInterval (tCliHandle CliHandle, INT4 i4ContextId,
                               INT4 i4DomainId,
                               INT4 i4PortId, INT4 i4SyncInterval)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsPtpPortAltMulcastSyncInterval
        (&u4ErrCode, i4ContextId, i4DomainId, i4PortId,
         i4SyncInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpPortAltMulcastSyncInterval (i4ContextId, i4DomainId,
                                               i4PortId,
                                               i4SyncInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PtpShowRunningConfig                                 */
/*                                                                           */
/* Description        : Displays configurations done in PTP module           */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
PtpShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    CliRegisterLock (CliHandle, PtpApiLock, PtpApiUnLock);
    PtpApiLock ();

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return (CLI_FAILURE);
    }

    if (PtpSrcScalars (CliHandle) == CLI_FAILURE)
    {
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return (CLI_FAILURE);
    }

    PtpSrcClocks (CliHandle);
 
    PtpApiUnLock ();
    CliUnRegisterLock (CliHandle);
    UNUSED_PARAM (u4Module);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : PtpCliValidateSyncInterval                           */
/*                                                                           */
/* Description        : Validates ptp Sync Interval Entered by user          */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/
PUBLIC INT4
PtpCliValidateSyncInterval (tCliHandle CliHandle, INT1 *pu1SyncInterval,
                            INT4 *pi4SyncInterval)
{
    INT4                i4IsNeg = 0;
    INT4                i4Index = 0;
    INT4                i4len = STRLEN (pu1SyncInterval);

    if (pu1SyncInterval[0] == '-')
    {
        i4IsNeg = -1;
        i4Index = 1;
    }
    else
    {
        i4IsNeg = 1;
    }

    while (i4Index < i4len)
    {

        (*pi4SyncInterval) = (*pi4SyncInterval) * 10;

        /* Check Whether it is Digit */
        if ((pu1SyncInterval[i4Index] - '0' > 9)
            || (pu1SyncInterval[i4Index] - '0' < 0))
        {
            CliPrintf (CliHandle, "%% Invalid sync interval\r\n");
            return CLI_FAILURE;
        }

        *pi4SyncInterval += (pu1SyncInterval[i4Index++]) - '0';

    }

    (*pi4SyncInterval) = (*pi4SyncInterval) * i4IsNeg;

    /* Validate Sync Interval */

    if ((*pi4SyncInterval > 1) || (*pi4SyncInterval < -1))
    {
        CliPrintf (CliHandle, "%% Invalid sync interval\r\n");
        return CLI_FAILURE;

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PtpCliValidateSyncLimit                              */
/*                                                                           */
/* Description        : Validates ptp Sync Limit Entered by user             */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/

PUBLIC INT4
PtpCliValidateSyncLimit (tCliHandle CliHandle, INT1 *pu1SyncLimit)
{
    INT4                i4Index = 0;
    INT4                i4len = STRLEN (pu1SyncLimit);

    while (i4Index < i4len)
    {

        /* Check Whether it is Digit */
        if ((pu1SyncLimit[i4Index] - '0' > 9)
            || (pu1SyncLimit[i4Index] - '0' < 0))
        {
            CliPrintf (CliHandle, "%% Invalid Sync Limit\r\n");
            return CLI_FAILURE;
        }
        i4Index++;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetNotifyStatus
 *
 * DESCRIPTION      : This function enables or disables the trap notification
 *                    for PTP module.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
PtpCliSetNotifyStatus (tCliHandle CliHandle, UINT4 u4TrapValue,
                       UINT1 u1TrapFlag)
{
    tSNMP_OCTET_STRING_TYPE TrapOption;
    UINT4               u4ErrorCode = 0;
    UINT2               u2TrapOption = 0;

    MEMSET (&TrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    TrapOption.pu1_OctetList = (UINT1 *) &u2TrapOption;
    TrapOption.i4_Length = 2;

    /* Set the corresponding Trap value which is to be set */

    nmhGetFsPtpNotification (&TrapOption);

    MEMCPY (&u2TrapOption, TrapOption.pu1_OctetList, sizeof (UINT2));

    if (u1TrapFlag == CLI_ENABLE)
    {
        u2TrapOption = (UINT2) (u2TrapOption | u4TrapValue);
    }
    else
    {
        u2TrapOption = (UINT2) (u2TrapOption & (~u4TrapValue));

        if (u2TrapOption == 0)
        {
            /* None of the trace option is set */
            TrapOption.i4_Length = 0;
        }
    }

    if (nmhTestv2FsPtpNotification (&u4ErrorCode, &TrapOption) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpNotification (&TrapOption) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetAccMasterAltPriority
 *
 * DESCRIPTION      : This Function Sets the Acceptable Master's Alternate 
 *                    Priority Attribute
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
PtpCliSetAccMasterAltPriority (tCliHandle CliHandle, INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                               UINT1 *u1AccMasterAddr, INT4 i4AlternatePriority)
{
    tSNMP_OCTET_STRING_TYPE AccMasterAddr;
    tSNMP_OCTET_STRING_TYPE *pAccMasterAddr = &AccMasterAddr;
    UINT1               au1AccMasterAddr[PTP_MASTER_MAX_ADDR_LEN];
    UINT4               u4ErrCode = 0;

    MEMSET (&(au1AccMasterAddr[0]), 0, PTP_MASTER_MAX_ADDR_LEN);
    AccMasterAddr.pu1_OctetList = &au1AccMasterAddr[0];

    PtpCliConverPortAddToOct (u1AccMasterAddr, i4NwProto, &AccMasterAddr);
    i4AccLen = AccMasterAddr.i4_Length;

    if (nmhTestv2FsPtpAccMasterAlternatePriority (&u4ErrCode, i4ContextId,
                                                  i4DomainId, i4NwProto,
                                                  i4AccLen, pAccMasterAddr,
                                                  i4AlternatePriority)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPtpAccMasterAlternatePriority (i4ContextId,
                                               i4DomainId, i4NwProto, i4AccLen,
                                               &AccMasterAddr,
                                               i4AlternatePriority)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliDelAccMaster            
 *
 * DESCRIPTION      : This Function Del the Acceptable Master's or
 *                     Reset Alternate Priority Attribute
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliDelAccMaster (tCliHandle CliHandle, INT4 i4ContextId,
                    INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                    UINT1 *u1AccMasterAddr, INT4 i4PriResetFlag)
{
    tSNMP_OCTET_STRING_TYPE AccMasterAddr;
    tSNMP_OCTET_STRING_TYPE *pAccMasterAddr = &AccMasterAddr;
    UINT1               au1AccMasterAddr[PTP_MASTER_MAX_ADDR_LEN];
    UINT4               u4ErrCode = 0;

    MEMSET (&(au1AccMasterAddr[0]), 0, PTP_MASTER_MAX_ADDR_LEN);
    AccMasterAddr.pu1_OctetList = &au1AccMasterAddr[0];

    PtpCliConverPortAddToOct (u1AccMasterAddr, i4NwProto, &AccMasterAddr);
    i4AccLen = AccMasterAddr.i4_Length;

    if (i4PriResetFlag == OSIX_TRUE)
    {
        if (nmhTestv2FsPtpAccMasterAlternatePriority (&u4ErrCode, i4ContextId,
                                                      i4DomainId, i4NwProto,
                                                      i4AccLen, pAccMasterAddr,
                                                      PTP_ACMST_DEF_ALT_PRI1)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPtpAccMasterAlternatePriority (i4ContextId,
                                                   i4DomainId, i4NwProto,
                                                   i4AccLen, &AccMasterAddr,
                                                   PTP_ACMST_DEF_ALT_PRI1)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        return (PtpCliUpdateAccMasterEntry (CliHandle, i4ContextId, i4DomainId,
                                            i4NwProto, i4AccLen,
                                            AccMasterAddr, DESTROY));
    }

    return CLI_SUCCESS;
}

#endif

/*-----------------------------------------------------------------------*/
/*                       End of the file  ptpcli.c                      */
/*-----------------------------------------------------------------------*/
