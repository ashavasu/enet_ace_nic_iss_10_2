/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpudpv6.c,v 1.13 2015/03/14 02:16:51 siva Exp $
 *
 * Description: This file contains PTP UDP IPv6 related utilities 
 *              implementation.       
 *********************************************************************/
#ifndef _PTPUDPV6_C_
#define _PTPUDPV6_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function                  : PtpUdpV6BindWithUdpV6                         */
/*                                                                           */
/* Description               : This routine initialises the socket related   */
/*                             parameters. This function should be invoked   */
/*                             during PTP module boot up. This takes care of */
/*                             creating sockets for IPv6.                    */
/*                                                                           */
/* Input                     : pPtpPort -   Pointer to Port   that needs to  */
/*                                          be deleted.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.i4Ipv6GnlSockId                */
/*                             gPtpGlobalInfo.i4Ipv6EvtSockId                */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUdpV6BindWithUdpV6 (VOID)
{
#ifdef IP6_WANTED
    struct sockaddr_in6 PtpV6SrcAddr;
    tIp6Addr            Ip6Addr;
    UINT1               u1Optval = 1;

    PTP_FN_ENTRY ();

    MEMSET (&PtpV6SrcAddr, 0, sizeof (struct sockaddr_in6));

    /* Open a socket with the standard port provided in the standard.
     * Refer Section D.2; Annex E of IEEE Std 1588 2008. This is for the
     * reception of event messages.
     * */
    gPtpGlobalInfo.i4Ipv6EvtSockId = socket (AF_INET6, SOCK_DGRAM, 0);
    if (gPtpGlobalInfo.i4Ipv6EvtSockId < 0)
    {
        perror ("Socket creation failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Failure in opening the Event UDP socket with "
                  "port number 319\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
 #ifdef LNXIP6_WANTED
     if (setsockopt (gPtpGlobalInfo.i4Ipv6EvtSockId, IPPROTO_IPV6, IPV6_V6ONLY,
                     &u1Optval, sizeof (INT4)) < 0)
     {
         /* setsockopt Failed for IPV6_V6ONLY */
         perror ("BFD - setsockopt for IPV6_V6ONLY  fails!!");
         return OSIX_FAILURE;
     }
 #endif


    PtpV6SrcAddr.sin6_family = AF_INET6;
    PtpV6SrcAddr.sin6_port = OSIX_HTONS (UDP_PTP_EVENT_PORT);
    PtpV6SrcAddr.sin6_flowinfo = 0;

    SET_ADDR_UNSPECIFIED (Ip6Addr);
    Ip6AddrCopy ((tIp6Addr *) (VOID *) &PtpV6SrcAddr.sin6_addr.s6_addr,
                 &Ip6Addr);

    /* Bind with the socket */
    if (bind (gPtpGlobalInfo.i4Ipv6EvtSockId, (struct sockaddr *) &PtpV6SrcAddr,
              sizeof (struct sockaddr_in6)) < 0)
    {
        perror ("Socket bind failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC,
                  "Failure in binding the Event UDP socket.\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
 #ifndef LNXIP6_WANTED
    if (setsockopt (gPtpGlobalInfo.i4Ipv6EvtSockId, IPPROTO_IPV6,
                    IP_PKTINFO, &u1Optval, sizeof (UINT1)) < 0)
    {
        perror ("Socket set options failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC,
                  "Failure in Setting Socket Option for "
                  "IPv6 Event UDP socket.\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
#endif
    PtpUdpInitSockParams (gPtpGlobalInfo.i4Ipv6EvtSockId);

    /* Open a socket with the standard port provided in the standard.
     * Refer Section D.2; Annex E of IEEE Std 1588 2008. This is for the 
     * reception of general messages.
     * */
    gPtpGlobalInfo.i4Ipv6GnlSockId = socket (AF_INET6, SOCK_DGRAM, 0);
    if (gPtpGlobalInfo.i4Ipv6GnlSockId < 0)
    {
        perror ("Socket creation failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC,
                  "Failure in opening the General UDP socket with port number "
                  "320.\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
#ifdef LNXIP6_WANTED
     if (setsockopt (gPtpGlobalInfo.i4Ipv6GnlSockId, IPPROTO_IPV6, IPV6_V6ONLY,
                     &u1Optval, sizeof (INT4)) < 0)
     {
         /* setsockopt Failed for IPV6_V6ONLY */
         perror ("BFD - setsockopt for IPV6_V6ONLY  fails!!");
         return OSIX_FAILURE;
     }
#endif

    PtpV6SrcAddr.sin6_family = AF_INET6;
    PtpV6SrcAddr.sin6_port = OSIX_HTONS (UDP_PTP_GENERAL_PORT);
    PtpV6SrcAddr.sin6_flowinfo = 0;

    SET_ADDR_UNSPECIFIED (Ip6Addr);
    Ip6AddrCopy ((tIp6Addr *) (VOID *) &PtpV6SrcAddr.sin6_addr.s6_addr,
                 &Ip6Addr);

    /* Bind with the socket */
    if (bind (gPtpGlobalInfo.i4Ipv6GnlSockId, (struct sockaddr *) &PtpV6SrcAddr,
              sizeof (struct sockaddr_in6)) < 0)
    {
        perror ("Socket bind failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC,
                  "Failure in binding the Event UDP socket.\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
#ifndef LNXIP6_WANTED
    if (setsockopt (gPtpGlobalInfo.i4Ipv6GnlSockId, IPPROTO_IPV6,
                    IP_PKTINFO, &u1Optval, sizeof (UINT1)) < 0)
    {
        perror ("Socket set options failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC,
                  "Failure in Setting Socket Option for "
                  "IPv6 Event UDP socket.\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
#endif
    PtpUdpInitSockParams (gPtpGlobalInfo.i4Ipv6GnlSockId);

    PTP_FN_EXIT ();
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpUdpV6TransmitPtpMsg                        */
/*                                                                           */
/* Description               : This routine transmits the given PTP PDU over */
/*                             UDP IPv6 interface.                           */
/*                                                                           */
/* Input                     : pu1PtpPdu  - Pointer to the linear buffer.    */
/*                             pPtpPort - Pointer to the port structure.     */
/*                             u4PktLen - Packet Length.                     */
/*                             u1MsgType- Message Type.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpUdpV6TransmitPtpMsg (UINT1 *pu1PtpPdu, tPtpPort * pPtpPort,
                        UINT4 u4PktLen, UINT1 u1MsgType)
{
    tNetIpv6IfInfo      NetIpv6IfInfo;
    struct sockaddr_in6 PeerAddr;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
#ifdef BSDCOMP_SLI_WANTED
    UINT1               CmsgInfo[PTP_IP6_MSG_HEADER_LEN];
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
#else
    struct cmsghdr      CmsgInfo;
    tIp6Addr            SrcAddr;
    tIp6Addr           *pSrcAddr = NULL;
#endif
    INT4                i4SockId = 0;
    UINT2               u2PtpPort = 0;
    INT4                i4HopLimit = 1;
    INT4                i4Optval = 0;

    PTP_FN_ENTRY ();

    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in6));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));

    /* Get the link local address */
    if (PtpPortNetIpv6GetIfInfo (pPtpPort->u4IfIndex, &NetIpv6IfInfo)
        == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "Unable to get the IPv6 information\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* Destination port differs based on the type of the message transmitted
     * */
    if ((u1MsgType == PTP_ANNC_MSG) || (u1MsgType == PTP_SIGNALLING_MSG))
    {
        u2PtpPort = UDP_PTP_GENERAL_PORT;
        i4SockId = gPtpGlobalInfo.i4Ipv6GnlSockId;
    }
    else
    {
        u2PtpPort = UDP_PTP_EVENT_PORT;
        i4SockId = gPtpGlobalInfo.i4Ipv6EvtSockId;
    }

    PeerAddr.sin6_family = AF_INET6;
    PeerAddr.sin6_port = (UINT2) OSIX_HTONS (u2PtpPort);
    PeerAddr.sin6_scope_id = pPtpPort->u4IfIndex;

    if (setsockopt (i4SockId, IPPROTO_IPV6, IPV6_MULTICAST_IF,
                    &(pPtpPort->u4IfIndex), sizeof (UINT4)) < 0)
    {
        perror ("Socket set options failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC,
                  "Failure in Setting Socket Option for "
                  "IPv6 Event UDP socket.\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* Set the option not to receive multicast packet on the interface on which
     * it is sent*/
    i4Optval = FALSE;
    if (setsockopt (i4SockId, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
                    &i4Optval, sizeof (i4Optval)) < 0)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, ALL_FAILURE_TRC,
                  "Unable to set setsockopt for IPV6_MULTICAST_LOOP.\r\n"));
        return;
    }

#ifndef BSDCOMP_SLI_WANTED
    /* The destination address should be FF0X:0:0:0:0:0:0:181 for all messages 
     * and for PDELAY type of messages, it should be FF02:0:0:0:0:0:0:6B
     * */
    if ((u1MsgType == PTP_PEER_DELAY_REQ_MSG) ||
        (u1MsgType == PTP_PEER_DELAY_RESP_MSG) ||
        (u1MsgType == PTP_PDELAY_RESP_FOLLOWUP_MSG))
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) &PeerAddr.sin6_addr.s6_addr,
                     &gV6PtpP2PAddr);
        /* In case of transmitting Peer Delay Request/Response messages over 
         * UDP IPv6, Hop limit should be set to 1.
         * Refer Section E.3, Annexe E of IEEE Std 1588 2008
         * */
        if (setsockopt (i4SockId, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                        &i4HopLimit, sizeof (INT4)) < 0)
        {
            perror ("Socket set options failed");
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                      INIT_SHUT_TRC,
                      "Failure in Setting Socket Option for "
                      "IPv6 Event UDP socket.\r\n"));
            PTP_FN_EXIT ();
            return;
        }
    }
    else
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) &PeerAddr.sin6_addr.s6_addr,
                     &gV6PtpAddr);
    }

    Ip6AddrCopy (&SrcAddr, &NetIpv6IfInfo.Ip6Addr);
    pSrcAddr = &SrcAddr;
    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;
    pIn6Pktinfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    if (pIn6Pktinfo == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Failed pIn6Pktinfo!!!.\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    MEMCPY (&(pIn6Pktinfo->ipi6_ifindex), &(pPtpPort->u4IfIndex),
            sizeof (INT4));

    Ip6AddrCopy (((tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr),
                 pSrcAddr);

    sendmsg (i4SockId, &Ip6MsgHdr, 0);

    if (sendto (i4SockId, pu1PtpPdu, u4PktLen, 0,
                (struct sockaddr *) &PeerAddr, sizeof (struct sockaddr)) < 0)
    {
        perror ("Error in sendmsg of PTP\n");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "SendMsg failed for PTP!!!.\r\n"));
        PTP_FN_EXIT ();
        return;
    }
#else
    pCmsgInfo = (struct cmsghdr *) (VOID *) CmsgInfo;
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    /* EVALRIPNG cc below :: changed from sizeof (Cmsginfo) */
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    Iov.iov_base = pu1PtpPdu;
    Iov.iov_len = u4PktLen;

    Ip6MsgHdr.msg_name = (VOID *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

    pIn6Pktinfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    pIn6Pktinfo->ipi6_ifindex = pPtpPort->u4IfIndex;

    if ((u1MsgType == PTP_PEER_DELAY_REQ_MSG) ||
        (u1MsgType == PTP_PEER_DELAY_RESP_MSG) ||
        (u1MsgType == PTP_PDELAY_RESP_FOLLOWUP_MSG))
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) &(pIn6Pktinfo->ipi6_addr.s6_addr),
                     &gV6PtpP2PAddr);
        /* In case of transmitting Peer Delay Request/Response messages over 
         * UDP IPv6, Hop limit should be set to 1.
         * Refer Section E.3, Annexe E of IEEE Std 1588 2008
         * */
        if (setsockopt (i4SockId, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                        &i4HopLimit, sizeof (INT4)) < 0)
        {
            perror ("Socket set options failed");
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                      INIT_SHUT_TRC,
                      "Failure in Setting Socket Option for "
                      "IPv6 Event UDP socket.\r\n"));
            PTP_FN_EXIT ();
            return;
        }
    }
    else
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) &(pIn6Pktinfo->ipi6_addr.s6_addr),
                     &gV6PtpAddr);
    }

    if (sendmsg (i4SockId, &Ip6MsgHdr, 0) < 0)
    {
        perror ("Error in sendmsg of PTP\n");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "SendMsg failed for PTP!!!.\r\n"));
        PTP_FN_EXIT ();
        return;
    }
#endif /* BSDCOMP_SLI_WANTED */
}

/*****************************************************************************/
/* Function                  : PtpUdpV6ProcessPktOnSocket                    */
/*                                                                           */
/* Description               : This routine reads the data present in the    */
/*                             given socket and processes the received data. */
/*                                                                           */
/* Input                     : i4SockId - Socket Identifier.                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUdpV6ProcessPktOnSocket (INT4 i4SockId)
{
    struct sockaddr_in6 PtpV6Addr;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tPtpPktParams       PtpPktParams;
    tClkSysTimeInfo     PtpSysTimeInfo;
    tPtpPortAddress     PtpPortAddress;
    tIp6Addr            Ip6Addr;
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
    UINT1               au1CmsgInfo[IP6_DEFAULT_MTU];
#else
    struct cmsghdr      CmsgInfo;
#endif
    tPtpPort           *pPtpPort = NULL;
    UINT1              *pu1RxPdu = NULL;
    UINT4               u4Port = 0;
    INT4                i4PktLen = 0;
#ifndef BSDCOMP_SLI_WANTED
    INT4                i4AddrLen = 0;
#endif
    UINT1               u1DomainId = 0;
#ifdef L2RED_WANTED
    tRmMsg             *pMsg = NULL;
    UINT2               u2DataLen = 0;
#endif
    PTP_FN_ENTRY ();

    MEMSET (&PtpV6Addr, 0, sizeof (struct sockaddr_in6));
    MEMSET (&Ip6MsgHdr, 0, sizeof (struct msghdr));
    MEMSET (&PtpPktParams, 0, sizeof (tPtpPktParams));
    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    MEMSET (&PtpPortAddress, 0, sizeof (tPtpPortAddress));

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              CONTROL_PLANE_TRC, "Received Data on Socket....\r\n"));

    pu1RxPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                              PTP_MAX_PDU_LEN);

    if (pu1RxPdu == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "PtpUdpV6ProcessPktOnSocket : "
                  "Buddy Memory Allocation for PDU Failed!!!!\r\n"));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    SET_ADDR_UNSPECIFIED (Ip6Addr);
    Ip6AddrCopy ((tIp6Addr *) (VOID *) PtpV6Addr.sin6_addr.s6_addr, &Ip6Addr);
    PtpV6Addr.sin6_port = 0;

#ifndef BSDCOMP_SLI_WANTED
    while ((i4PktLen = recvfrom (i4SockId, pu1RxPdu, PTP_MAX_PDU_LEN, 0,
                                 (struct sockaddr *) &PtpV6Addr,
                                 &i4AddrLen)) > 0)
    {
        Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

        if (recvmsg (i4SockId, &Ip6MsgHdr, 0) < 0)
        {
            /* Intialising for receiving from any address */
            MEMSET (&PtpV6Addr, 0, sizeof (struct sockaddr_in));
            break;
        }

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "Processing PTP pakcet received "
                  "on socket...\r\n"));

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "Obtaining the Interface from the socket "
                  "identifier...\r\n"));

        /* Get the time stamp for the packet received from the socket
         * Need to understand whether this option is provided by the
         * FSIP case*/

#else
    MEMSET (au1CmsgInfo, 0, sizeof (au1CmsgInfo));

    Ip6MsgHdr.msg_name = (void *) &PtpV6Addr;
    Ip6MsgHdr.msg_namelen = sizeof (PtpV6Addr);
    Iov.iov_base = pu1RxPdu;
    Iov.iov_len = IP6_DEFAULT_MTU;
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_control = (VOID *) &au1CmsgInfo;
    Ip6MsgHdr.msg_controllen = IP6_DEFAULT_MTU;

    while ((i4PktLen = recvmsg (i4SockId, &Ip6MsgHdr, 0) > 0))
    {
        pIn6Pktinfo = NULL;
#endif
        pIn6Pktinfo = (struct in6_pktinfo *) (VOID *) CMSG_DATA
            (CMSG_FIRSTHDR (&Ip6MsgHdr));
        if (pIn6Pktinfo == NULL)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                      PTP_MAX_DOMAINS, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "PtpUdpV6ProcessPktOnSocket:Failed To Read Data From Socket\r\n"));
            return OSIX_FAILURE;
        }

        u4Port = (UINT4) pIn6Pktinfo->ipi6_ifindex;

        Ip6AddrCopy ((tIp6Addr *) (VOID *) &(PtpPortAddress.ai1Addr),
                     (tIp6Addr *) (VOID *) &PtpV6Addr.sin6_addr.s6_addr);

        PtpPortAddress.u2NetworkProtocol = PTP_IFACE_UDP_IPV6;
        PtpPortAddress.u2AddrLength = IPVX_IPV6_ADDR_LEN;

        if ((PtpCmUtlGetDomainIdFromPTPv1Msg (pu1RxPdu, &u1DomainId)) ==
            OSIX_FAILURE)
        {
            u1DomainId = pu1RxPdu[PTP_MSG_DOMAIN_OFFSET];
        }
        /* Get the port entry. Since VRF support is not present in IP
         * module, Default context is used
         */
        pPtpPort = PtpIfGetPortFromIfTypeAndIndex
            ((UINT4) L2IWF_DEFAULT_CONTEXT, u1DomainId,
             u4Port, PTP_IFACE_UDP_IPV6);

        if (pPtpPort == NULL)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                      PTP_MAX_DOMAINS, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Port Entry is not available for Index : %u and"
                      " Domain : %u\r\n", u4Port, u1DomainId));
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }

        /* Allocation for CRU buffer */
        pBuf = CRU_BUF_Allocate_MsgBufChain (PTP_MAX_PDU_LEN, 0);

        if (pBuf != NULL)
        {
            /* Copy data from linear buffer into CRU buffer */
            if (CRU_BUF_Copy_OverBufChain (pBuf, pu1RxPdu, 0, PTP_MAX_PDU_LEN)
                != CRU_FAILURE)
            {
                /* Get  Packet received time */
                PtpClkGetTimeStamp (pPtpPort, pBuf, &PtpSysTimeInfo, u4Port,
                                    PTP_MODE_RX);
            }
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }
        PtpPktParams.pu1RcvdPdu = pu1RxPdu;
        PtpPktParams.pPtpPort = pPtpPort;
        PtpPktParams.pPtpIngressTimeStamp = &PtpSysTimeInfo;
        PtpPktParams.pPtpPortAddress = &PtpPortAddress;
        PtpPktParams.u4PktLen = (UINT4) i4PktLen;
#ifdef L2RED_WANTED
        /*Formaing RM packet and sending to StandBy node */
        if ((gPtpRedGlobalInfo.u1NodeStatus == RM_ACTIVE) &&
            (gPtpRedGlobalInfo.u1NumOfStandbyNodesUp != 0))
        {

            PtpRedFormIPvXPtpPktForStandby (&PtpPktParams, &pMsg);
            if (pMsg != NULL)
            {
                pMsg->ModuleData.u4Reserved1 = PTP_RED_IPv6_PTP_PKT;
                u2DataLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pMsg);
                PtpRedProcessAndRelayPkt (pMsg, u2DataLen);
            }
            else
            {
                PTP_TRC ((0, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpApiIncomingPktHdlr: Unable to duplicate the packet for RM.\r\n"));
            }
        }
#endif

        PtpQueProcessPtpPdu (&PtpPktParams);

    }
    MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif /*_PTPUDPV6_C_*/
