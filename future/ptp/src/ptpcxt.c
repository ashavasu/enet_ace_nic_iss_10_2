/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *$Id: ptpcxt.c,v 1.4 2014/01/24 12:20:08 siva Exp $
 *
 * Description: This file contains PTP context related utilities 
 *              implementation.       
 *********************************************************************/
#ifndef _PTPCXT_C_
#define _PTPCXT_C_

#include "ptpincs.h"
/*****************************************************************************/
/* Function                  : PtpCxtCreateContext                           */
/*                                                                           */
/* Description               : This routine creates the virtual context in   */
/*                             PTP module with the provided context          */
/*                             identifier.                                   */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier.             */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.ContextPoolId                  */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.ContextTree                    */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpCxt     *
PtpCxtCreateContext (UINT4 u4ContextId)
{
    tPtpCxt            *pPtpCxt = NULL;
    tPtpNotifyInfo      PtpNotifyInfo;

    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));

    PTP_FN_ENTRY ();

    pPtpCxt = (tPtpCxt *) MemAllocMemBlk (gPtpGlobalInfo.ContextPoolId);

    if (pPtpCxt == NULL)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Memory Allocation failed " "for context creation\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MAX_CONTEXT);
        PTP_FN_EXIT ();
        return NULL;
    }

    pPtpCxt->u4ContextId = u4ContextId;

    /* Get the context type from the virtual context manager and update the
     * type. Since only L2 context is available now, the type is updated as L2
     */
    pPtpCxt->u1CxtType = (UINT1) PTP_L2_CONTEXT;

    /* By default PTP is disabled in context */
    pPtpCxt->u1AdminStatus = PTP_DISABLED;

    /* Initiliaze Trace option to All Zeros */
    pPtpCxt->u2Trace = PTP_CRITICAL_TRC;

    /*Adding it to the global data base */
    if (PtpDbAddNode (gPtpGlobalInfo.ContextTree, pPtpCxt,
                      PTP_CONTEXT_DATA_SET) == OSIX_FAILURE)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpCxtCreateContext:Context Creation Failed\r\n"));
        MemReleaseMemBlock (gPtpGlobalInfo.ContextPoolId, (UINT1 *) pPtpCxt);
        PTP_FN_EXIT ();
        return NULL;
    }

    /* Send Notification for Context Creation */
    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));
    PtpNotifyInfo.u4ContextId = u4ContextId;
    PtpNotifyInfo.u4ContextRowState = (UINT4) CREATE_AND_GO;
    PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                  (UINT1) PTP_TRAP_SYS_CNTL_CHANGE);
    PTP_FN_EXIT ();
    return pPtpCxt;
}

/*****************************************************************************/
/* Function                  : PtpCxtDeleteContext                           */
/*                                                                           */
/* Description               : This routine deletes the virtual context in   */
/*                             PTP module with the provided context          */
/*                             identifier.                                   */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier.             */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.ContextPoolId                  */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.ContextTree                    */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpCxtDeleteContext (UINT4 u4ContextId)
{
    tPtpCxt            *pPtpCxt = NULL;
    tPtpNotifyInfo      PtpNotifyInfo;

    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));
    PTP_FN_ENTRY ();

    pPtpCxt = PtpCxtGetNode (u4ContextId);

    if (pPtpCxt == NULL)
    {
        /* Invalid context identifier being passed */
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "Invalid context delete!!!!\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* PTP needs to be disabled in this context first */
    PtpCxtDisable (u4ContextId);

    /* All the ports and domain in this context needs to be freed. */
    PtpDmnDelAllDomainsInCxt (u4ContextId);

    PtpDbDeleteNode (gPtpGlobalInfo.ContextTree, pPtpCxt, PTP_CONTEXT_DATA_SET);

    PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
              INIT_SHUT_TRC | MGMT_TRC, "Context is deleted successfully\r\n"));

    MemReleaseMemBlock (gPtpGlobalInfo.ContextPoolId, (UINT1 *) pPtpCxt);
    pPtpCxt = NULL;

    /* Send Notification for Context Deletion */
    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));
    PtpNotifyInfo.u4ContextId = u4ContextId;
    PtpNotifyInfo.u4ContextRowState = (UINT4) DESTROY;
    PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                  (UINT1) PTP_TRAP_SYS_CNTL_CHANGE);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpCxtEnable                                  */
/*                                                                           */
/* Description               : This routine enables PTP in the provided      */
/*                             virtual context. It searches for all the      */
/*                             domains that are associated with the context  */
/*                             and disables PTP over these domains.          */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier.             */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpCxtEnable (UINT4 u4ContextId)
{
    tPtpCxt            *pPtpCxt = NULL;
    tPtpPort           *pPtpPort = NULL;
    tPtpNotifyInfo      PtpNotifyInfo;
    UINT4               u4PortId = 0;
    UINT1               u1DomainId = 0;

    PTP_FN_ENTRY ();

    pPtpCxt = PtpCxtGetNode (u4ContextId);

    if ((pPtpCxt == NULL) || (pPtpCxt->u1AdminStatus == PTP_ENABLED))
    {
        /* Invalid context identifier being passed */
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "Invalid context enable or context is "
                  "already enabled!!!!\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* Scan through all the ports in this context and enable ptp in all
     * the ports. This will in turn initialise the state machine
     */
    while (((pPtpPort = PtpIfGetNextPortEntry
             (u4ContextId, u1DomainId, u4PortId))
            != NULL) && (pPtpPort->u4ContextId == u4ContextId))
    {
        if (pPtpPort->PortDs.u1PtpEnabledStatus == PTP_ENABLED)
        {
            PtpIfEnablePort (pPtpPort);
        }

        u1DomainId = pPtpPort->u1DomainId;
        u4PortId = pPtpPort->PortDs.u4PtpPortNumber;
    }

    pPtpCxt->u1AdminStatus = PTP_ENABLED;

    /* Send Notification for Context Admin Status */
    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));
    PtpNotifyInfo.u4ContextId = u4ContextId;
    PtpNotifyInfo.u4ContextAdminState = (UINT4) pPtpCxt->u1AdminStatus;
    PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                  (UINT1) PTP_TRAP_SYS_ADMIN_CHANGE);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpCxtDisable                                 */
/*                                                                           */
/* Description               : This routine disables PTP in the provided     */
/*                             virtual context. It searches for all the      */
/*                             domains that are associated with the context  */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier.             */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.PortTree                       */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpCxtDisable (UINT4 u4ContextId)
{
    tPtpCxt            *pPtpCxt = NULL;
    tPtpPort           *pPtpPort = NULL;
    tPtpCompDBEntry    *pPtpCompDB = NULL;
    tPtpCompDBEntry    *pNextPtpCompDB = NULL;
    tPtpNotifyInfo      PtpNotifyInfo;
    UINT4               u4PortId = 0;
    UINT4               u4RemainingTime = 0;
    UINT1               u1DomainId = 0;
    UINT1               u1PtpEnabledStatus = PTP_DISABLED;

    PTP_FN_ENTRY ();

    pPtpCxt = PtpCxtGetNode (u4ContextId);

    if ((pPtpCxt == NULL) || (pPtpCxt->u1AdminStatus == PTP_DISABLED))
    {
        /* Invalid context identifier being passed */
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "Invalid context disable or context is "
                  "already disabled!!!!\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* Stop the Compatibility timer running, if any */
    pPtpCompDB = RBTreeGetFirst (gPtpGlobalInfo.CompTree);
    while ((pPtpCompDB != NULL) && (pPtpCompDB->u4ContextId == u4ContextId))
    {
        if (TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                                 &(pPtpCompDB->CompDBTmrNode.TimerNode),
                                 &u4RemainingTime) == TMR_SUCCESS)

        {
            TmrStop (gPtpGlobalInfo.PtpTmrListId, &(pPtpCompDB->CompDBTmrNode));
        }
        pNextPtpCompDB = RBTreeGetNext (gPtpGlobalInfo.CompTree,
                                        pPtpCompDB, NULL);
        pPtpCompDB = pNextPtpCompDB;

    }

    /* Scan through all the ports in this context and disable ptp in all
     * the ports. This will in turn initialise the state machine and stop
     * all the timers ruuning in each port
     */

    while (((pPtpPort = PtpIfGetNextPortEntry
             (u4ContextId, u1DomainId, u4PortId))
            != NULL) && (pPtpPort->u4ContextId == u4ContextId))
    {

        u1PtpEnabledStatus = pPtpPort->PortDs.u1PtpEnabledStatus;

        /* Stop the PTP Operation on the interface */
        PtpIfDisablePort (pPtpPort);

        /* restore the configstatus */
        pPtpPort->PortDs.u1PtpEnabledStatus = u1PtpEnabledStatus;

        u1DomainId = pPtpPort->u1DomainId;
        u4PortId = pPtpPort->PortDs.u4PtpPortNumber;
    }

    pPtpCxt->u1AdminStatus = PTP_DISABLED;

    PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
              INIT_SHUT_TRC | MGMT_TRC, "Context is disabled\r\n"));

    /* Send Notification for Context Admin Status */
    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));
    PtpNotifyInfo.u4ContextId = u4ContextId;
    PtpNotifyInfo.u4ContextAdminState = (UINT4) pPtpCxt->u1AdminStatus;
    PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                  (UINT1) PTP_TRAP_SYS_ADMIN_CHANGE);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpCxtDeleteAllCxt                            */
/*                                                                           */
/* Description               : This routine scans an deletes all the context */
/*                                                                           */
/* Input                     : None                                          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo                                */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Pointer to Cxt Node/NULL.                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpCxtDeleteAllCxt (VOID)
{
    tPtpCxt            *pPtpCxt = NULL;

    PTP_FN_ENTRY ();

    pPtpCxt = (tPtpCxt *) PtpDbGetFirstNode (gPtpGlobalInfo.ContextTree,
                                             PTP_CONTEXT_DATA_SET);

    while (pPtpCxt != NULL)
    {
        PtpCxtDeleteContext (pPtpCxt->u4ContextId);

        pPtpCxt = (tPtpCxt *)
            PtpDbGetNextNode (gPtpGlobalInfo.ContextTree,
                              pPtpCxt, PTP_CONTEXT_DATA_SET);

    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpCxtGetNode                                 */
/*                                                                           */
/* Description               : This routine returns the pointer to the       */
/*                             Context info structure.                       */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier.             */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Pointer to Cxt Node/NULL.                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpCxt     *
PtpCxtGetNode (UINT4 u4ContextId)
{
    tPtpCxt             PtpCxt;
    tPtpCxt            *pPtpCxt = NULL;

    PTP_FN_ENTRY ();

    PtpCxt.u4ContextId = u4ContextId;
    pPtpCxt = PtpDbGetNode (gPtpGlobalInfo.ContextTree, &PtpCxt,
                            PTP_CONTEXT_DATA_SET);

    PTP_FN_EXIT ();
    return pPtpCxt;
}

/*****************************************************************************/
/* Function                  : PtpCxtIsPrimaryCxtAndDmn                      */
/*                                                                           */
/* Description               : This routine returns OSIX_TRUE, if the port   */
/*                             provided belongs to primary context and       */
/*                             primary domain. Otherwise, returns OSIX_FALSE.*/
/*                                                                           */
/* Input                     : pPtpPort - Pointer to Port.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC              BOOL1
PtpCxtIsPrimaryCxtAndDmn (tPtpPort * pPtpPort)
{
    BOOL1               bRetVal = OSIX_FALSE;
    tPtpCxt            *pPtpCxt = NULL;

    pPtpCxt = PtpCxtGetNode (gPtpGlobalInfo.u4PrimaryContextId);

    if (pPtpCxt == NULL)
    {
        return bRetVal;
    }

    if ((pPtpPort->u4ContextId == gPtpGlobalInfo.u4PrimaryContextId) &&
        (pPtpPort->u1DomainId == pPtpCxt->u1PrimaryDomainId))
    {
        bRetVal = OSIX_TRUE;
    }

    return bRetVal;
}

#endif /*_PTPCXT_C_*/
