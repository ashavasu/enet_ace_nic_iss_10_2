/***************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpannc.c,v 1.5 2014/01/24 12:20:08 siva Exp $
 * Description: This file contains PTP task announce message handler related 
 *              routines.
 ****************************************************************************/
#ifndef _PTPANNC_C_
#define _PTPANNC_C_

#include "ptpincs.h"

PRIVATE INT4        PtpAnncValidateAnncMsg (tPtpPort * pPtpPort);

PRIVATE INT4
 
     PtpAnncProcessAlTsTlv (UINT1 *pu1Pdu, UINT4 u4MsgLen, tPtpPort * pPtpPort);

PRIVATE INT4        PtpAnncHandleAnncMsg (tPtpPktParams * pPtpPktParams);

PRIVATE tPtpForeignMasterDs *PtpAnncIsForeignMasterExist (tPtpPort * pPtpPort,
                                                          tClkId ClkId,
                                                          UINT2 u2Port);

PRIVATE tPtpForeignMasterDs *PtpAnncCreateForeignMaster (UINT1 *pu1Pdu,
                                                         tPtpPort * pPtpPort,
                                                         tClkId ClkId,
                                                         UINT2 u2Port);

PRIVATE VOID        PtpAnncUpdateForenMasterDetails (UINT1 *pu1Pdu,
                                                     tPtpHdrInfo * pPtpHdrInfo,
                                                     tPtpForeignMasterDs *
                                                     pPtpForeignMasterDs);
/*****************************************************************************/
/* Function                  : PtpAnncHandleRxAnncMsg                        */
/*                                                                           */
/* Description               : This routine will be invoked to handle the    */
/*                             announce messages. This routine validates and */
/*                             processes the received announce message.      */
/*                                                                           */
/* Input                     : pPtpPktParams - Pointer to received Packet    */
/*                                             Parameters.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAnncHandleRxAnncMsg (tPtpPktParams * pPtpPktParams)
{
    PTP_FN_ENTRY ();

    /* Announce message is a general message. Hence time stamp generated along
     * with the message will not be used here.
     * */
    if (pPtpPktParams->pPtpPort->pPtpDomain->ClkMode ==
        PTP_TRANSPARENT_CLOCK_MODE)
    {
        /* Clock is operating in transparent mode. Do not consume the 
         * packet. Give it to transparent clock handler for forwarding
         * Since, Announce message is not event message, time stamp is not
         * generated for announce message.
         * */
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Clock is operating in Transparent "
                  "mode.\r\n"));
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Forwarding the received announce"
                  "message.\r\n"));

        PtpTransHandleRcvdPtpPkt (pPtpPktParams->pu1RcvdPdu,
                                  pPtpPktParams->pPtpPort,
                                  pPtpPktParams->u4PktLen, PTP_ANNC_MSG, NULL);
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if (PtpAnncValidateAnncMsg (pPtpPktParams->pPtpPort) != OSIX_SUCCESS)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC, "PtpQueHandleRxPdu: "
                  "Announce Message Validation returned Failure!!\r\n"));

        /* Incrementing the dropped message count */
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpAnncHandleAnncMsg (pPtpPktParams) != OSIX_SUCCESS)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC, "PtpQueHandleRxPdu: "
                  "PtpAnncHandleRxAnncMsg returned Failure!!\r\n"));

        /* Incrementing the dropped message count */
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Incrementing the received Announce message count */
    pPtpPktParams->pPtpPort->PtpPortStats.u4RcvdAnnounceMsgCnt++;

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpAnncValidateAnncMsg                        */
/*                                                                           */
/* Description               : This routine validates the received PTP       */
/*                             announce messages.                            */
/*                                                                           */
/* Input                       pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpAnncValidateAnncMsg (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    /* The received announce message should be discarded on any one of the
     * following criterias
     * 1) Port is in INITIALIZING or DISABLED state
     * 2) Port is in FAULTY state
     * */
    if ((pPtpPort->PortDs.u1PortState == PTP_STATE_INITIALIZING) ||
        (pPtpPort->PortDs.u1PortState == PTP_STATE_DISABLED) ||
        (pPtpPort->PortDs.u1PortState == PTP_STATE_FAULTY))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Port is in INITIALIZING/DISABLED/FAULTY" " State\r\n",
                  pPtpPort));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Discarding the received Announce" "Message\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpAnncProcessAlTsTlv                         */
/*                                                                           */
/* Description               : This routine used to collect  the received    */
/*                             alternate timescal tlvs from announce messages*/
/*                             and update the Domain Tlvs only if the clock  */
/*                             is not in grandmaster and message form current*/
/*                             parent.                                       */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             u4MsgLen - Received message Length            */
/*                             pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpAnncProcessAlTsTlv (UINT1 *pu1Pdu, UINT4 u4MsgLen, tPtpPort * pPtpPort)
{
    UINT1              *pu1ATSTlv = NULL;
    INT4                i4Status = OSIX_FAILURE;
    UINT2               u2TotTlvLen = 0;

    PTP_FN_ENTRY ();

    if ((PtpUtilIsGrandMasterClk (pPtpPort->pPtpDomain)) != OSIX_FALSE)
    {
        /* Clock is a grand master clock */
        PTP_FN_EXIT ();
        return (OSIX_SUCCESS);
    }

    /* Allocate the Memory for the PTP_MAX_ALT_TIME_PER_CONTEXT */
    /* Find the All Alternate Timescale tlvs */
    pu1ATSTlv = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4AltTimeBuddyId,
                               PTP_MAX_ALT_TIME_TLV_BLOCK_SIZE);
    if (pu1ATSTlv == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpAnncProcessAlTsTlv : Alternate Timescale "
                  "MemBuddyAlloc Failed.\r\n"));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    i4Status = PtpAlTsFindATSTlvs (pu1Pdu, u4MsgLen, pu1ATSTlv, &u2TotTlvLen);

    if ((u2TotTlvLen <= PTP_MAX_ALT_TIME_TLV_BLOCK_SIZE))
    {
        i4Status = PtpAlTsUpdateTlv (pPtpPort->u4ContextId,
                                     pPtpPort->u1DomainId,
                                     pu1ATSTlv, u2TotTlvLen);
        if (i4Status == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "PtpAnncProcessAlTsTlv : PtpAlTsUpdateTlv"
                      " Failed.\r\n"));
        }
    }

    if ((MemBuddyFree ((UINT1) gPtpGlobalInfo.i4AltTimeBuddyId,
                       pu1ATSTlv) == BUDDY_FAILURE))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpAnncProcessAlTsTlv : MemBuddyFree Failed. \r\n"));

        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function                  : PtpAnncHandleAnncMsg                          */
/*                                                                           */
/* Description               : This routine handles the received announce    */
/*                             messages. The PTP messages should be validated*/
/*                             earlier itself before invoking this routine.  */
/*                                                                           */
/* Input                     : pPtpPktParams - Pointer Received Pkt info     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpAnncHandleAnncMsg (tPtpPktParams * pPtpPktParams)
{
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;
    tPtpHdrInfo         PtpHdrInfo;
    UINT4               u4MilliSec = 0;
    INT4                i4Status = OSIX_FAILURE;
    UINT1               u1AltPriority1 = 0;
    BOOL1               bCurrentMstFlag = OSIX_FALSE;

    PTP_FN_ENTRY ();
    /* On receiving an announce message, PTP module should update the Foreign
     * Master data set. This is done as follows
     * 1) If the Source Port Identity does not match the existing ParentDs, then
     *    the same is taken as new Foreign Master record.
     * 2) If the Source Port Identity matches with any of the existing 
     *    Foreign Master, then the foreignMasterAnnounceMessages field shall
     *    be incremented.
     * 3) If Source port port id does not match with that of any of the foreign
     *    master records, and Domain already has maximum number of foreign
     *    masters created, then the announce message shall be discarded.
     * */
    MEMSET (&PtpHdrInfo, 0, sizeof (tPtpHdrInfo));

    i4Status = PtpAcMstIsAccMasterEnabled (pPtpPktParams->pPtpPort);
    if (i4Status == OSIX_TRUE)
    {
        /* Acceptable Master is enabled this prot check the source of the 
         * Announce message is valid master. 
         * Update the Configured u1AltPriority1 in the 
         * Foreign Master entry' u1AltPri1, it will be used in BMC calculatuion 
         */
        i4Status = PtpAcMstValidateMaster (pPtpPktParams->pPtpPort->u4ContextId,
                                           pPtpPktParams->pPtpPort->u1DomainId,
                                           pPtpPktParams->pPtpPortAddress,
                                           &u1AltPriority1);
        if (i4Status == OSIX_FAILURE)
        {
            /* Discard the announce messsage */
            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "PtpAnncHandleAnncMsg:"
                      "PtpAcMstValidateMaster returned Failure "
                      " Discarding the received Announce Message !!!\r\n"));

            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }
    }

    pPtpPktParams->pu1RcvdPdu =
        pPtpPktParams->pu1RcvdPdu +
        PTP_GET_MEDIA_HDR_LEN (pPtpPktParams->pPtpPort);

    PtpUtilExtractHdrFromMsg (pPtpPktParams->pu1RcvdPdu, &PtpHdrInfo);

    if (PtpHdrInfo.u1PtpVersion != PTP_VERSION_TWO)
    {
        /* This not a version two Announce message drop the same
         * */
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Announce message does not have"
                  "valid Version id for port%u\r\n",
                  pPtpPktParams->pPtpPort->u4IfIndex));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpUtilIsMsgFromCurrentParent (pPtpPktParams->pPtpPort,
                                       PtpHdrInfo.ClkId, PtpHdrInfo.u2Port)
        == OSIX_TRUE)
    {
        /* Announce message received from current Master 
         * Update the ParentDs & start the announce timer */
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Announce message received from "
                  "current master over port %u\r\n",
                  pPtpPktParams->pPtpPort->u4IfIndex));

        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Restarting Announce receipt timer\r\n",
                  pPtpPktParams->pPtpPort));

        u4MilliSec =
            PtpUtilLogBase2ToMilliSec ((INT1) pPtpPktParams->pPtpPort->PortDs.
                                       u1AnnounceInterval);
        if (PtpTmrStartTmr (&(pPtpPktParams->pPtpPort->
                              AnnounceReciptTimeOutTimer),
                            (pPtpPktParams->pPtpPort->
                             PortDs.u1AnnounceReceiptTimeOut *
                             u4MilliSec), PTP_ANNC_RECEIPT_TMR, OSIX_FALSE)
            == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpAnncHandleAnncMsg: "
                      "PtpTmrStartTmr returned Failure!!!\r\n",
                      pPtpPktParams->pPtpPort));

            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Unable to start Announce Receipt "
                      "Timer\r\n", pPtpPktParams->pPtpPort));
        }

        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Updating the ParentDs\r\n",
                  pPtpPktParams->pPtpPort));

        bCurrentMstFlag = OSIX_TRUE;
    }

    pPtpForeignMasterDs = PtpAnncIsForeignMasterExist (pPtpPktParams->pPtpPort,
                                                       PtpHdrInfo.ClkId,
                                                       PtpHdrInfo.u2Port);

    if (pPtpForeignMasterDs == NULL)
    {
        /* Foreign Master does not exist. Create a New Record 
         * If maximum creatable records are reached, then discard the message */
        pPtpForeignMasterDs =
            PtpAnncCreateForeignMaster (pPtpPktParams->pu1RcvdPdu,
                                        pPtpPktParams->pPtpPort,
                                        PtpHdrInfo.ClkId, PtpHdrInfo.u2Port);
        if (pPtpForeignMasterDs == NULL)
        {
            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "PtpAnncCreateForeignMaster returned Failure!!!\r\n",
                      pPtpPktParams->pPtpPort));

            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* Existing foreign Master. Update the details and increment the
         * foreignMasterAnnounceMessages field*/
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
                  "Updating the existing Foreign Master Record...\r\n",
                  pPtpPktParams->pPtpPort));

        pPtpForeignMasterDs->u4AnnounceMsgsCnt =
            pPtpForeignMasterDs->u4AnnounceMsgsCnt + 1;
        pPtpForeignMasterDs->u1FornMastRxCnt++;

        if (PtpHdrInfo.u2SeqId < pPtpForeignMasterDs->u2RxAnncSeqId)
        {
            /* This does not have a valid sequence ID
             * */
            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Announce message does not have"
                      "valid Sequence id for port%u\r\n",
                      pPtpPktParams->pPtpPort->u4IfIndex));

            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
    }

    pPtpForeignMasterDs->u2RxAnncSeqId = PtpHdrInfo.u2SeqId;

    PtpAnncUpdateForenMasterDetails (pPtpPktParams->pu1RcvdPdu, &PtpHdrInfo,
                                     pPtpForeignMasterDs);
    /* Update the AltPri1 with configured u1AltPriority1 */
    pPtpForeignMasterDs->u1AltPri1 = u1AltPriority1;

    /* Update the bIsAltMstAnnc */
    if ((PtpHdrInfo.u2Flags & PTP_FLAG_ALT_MST_MASK) == PTP_FLAG_ALT_MST_MASK)
    {
        pPtpForeignMasterDs->bIsAltMstAnnc = OSIX_TRUE;
    }
    /* Process the Alternate timescale tlv */
    if (bCurrentMstFlag == OSIX_TRUE)
    {
        if ((PtpAnncProcessAlTsTlv (pPtpPktParams->pu1RcvdPdu,
                                    pPtpPktParams->u4PktLen,
                                    pPtpPktParams->pPtpPort)) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                      pPtpPktParams->pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "PtpAnncHandleAnncMsg: "
                      "PtpAnncProcessAlTsTlv returned Failure!!!\r\n"));

            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }
    }

    /* The announce interval in the log messages need not be copied. It will
     * be the responsibility of the administrator to configure the value of
     * Announce interval so that Announce receipt time out does not disrupt
     * the settled network
     * */
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpAnncIsForeignMasterExist                   */
/*                                                                           */
/* Description               : This routine identifies whether the foreign   */
/*                             master exists for given clock identifier and  */
/*                             port identifier. If exists, it returns the    */
/*                             corresponding function pointer. Or else, it   */
/*                             returns NULL.                                 */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                             ClkId - Clock Identifier whose presence needs */
/*                                     to be validated.                      */
/*                             u2Port - Port identifier                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Pointer to existing foreign master/NULL.      */
/*                                                                           */
/*****************************************************************************/
PRIVATE tPtpForeignMasterDs *
PtpAnncIsForeignMasterExist (tPtpPort * pPtpPort, tClkId ClkId, UINT2 u2Port)
{
    tPtpForeignMasterDs PtpForeignMasterDs;
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;

    PTP_FN_ENTRY ();

    MEMSET (&PtpForeignMasterDs, 0, sizeof (tPtpForeignMasterDs));

    PtpForeignMasterDs.u4ContextId = pPtpPort->u4ContextId;
    PtpForeignMasterDs.u1DomainId = pPtpPort->u1DomainId;
    PtpForeignMasterDs.u4PortIndex = pPtpPort->PortDs.u4PtpPortNumber;
    MEMCPY (PtpForeignMasterDs.ClkId, ClkId, PTP_MAX_CLOCK_ID_LEN);
    PtpForeignMasterDs.u4SrcPortIndex = (UINT4) u2Port;

    pPtpForeignMasterDs = (tPtpForeignMasterDs *)
        PtpDbGetNode (gPtpGlobalInfo.FMTree,
                      &(PtpForeignMasterDs),
                      (UINT1) PTP_FOREIGN_MASTER_DATA_SET);

    PTP_FN_EXIT ();
    return pPtpForeignMasterDs;
}

/*****************************************************************************/
/* Function                  : PtpAnncCreateForeignMaster                    */
/*                                                                           */
/* Description               : This routine creates the foreign Master       */
/*                             record for the received PDU. The PDU should   */
/*                             have been validated earlier before processing */
/*                             and its presence in the data structure shoule */
/*                             be checked before invoking this function.     */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                             ClkId - Clock Identifier whose presence needs */
/*                                     to be validated.                      */
/*                             u2Port - Port identifier                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE tPtpForeignMasterDs *
PtpAnncCreateForeignMaster (UINT1 *pu1Pdu, tPtpPort * pPtpPort, tClkId ClkId,
                            UINT2 u2Port)
{
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;

    PTP_FN_ENTRY ();

    UNUSED_PARAM (pu1Pdu);

    pPtpForeignMasterDs = (tPtpForeignMasterDs *)
        MemAllocMemBlk (gPtpGlobalInfo.ForeignMasterPoolId);

    if (pPtpForeignMasterDs == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpAnncCreateForeignMaster: Memory Allocation for Foreign "
                  "Master failed!!!\r\n"));

        PTP_FN_EXIT ();
        return NULL;
    }

    MEMSET (pPtpForeignMasterDs, 0, sizeof (tPtpForeignMasterDs));
    MEMCPY (&(pPtpForeignMasterDs->ClkId), ClkId, sizeof (tClkId));

    pPtpForeignMasterDs->pPtpPort = pPtpPort;
    pPtpForeignMasterDs->u4ContextId = pPtpPort->u4ContextId;
    pPtpForeignMasterDs->u1DomainId = pPtpPort->u1DomainId;
    pPtpForeignMasterDs->u4SrcPortIndex = (UINT4) u2Port;
    pPtpForeignMasterDs->u4PortIndex = pPtpPort->PortDs.u4PtpPortNumber;
    pPtpForeignMasterDs->bIsAltMstAnnc = OSIX_FALSE;
    pPtpForeignMasterDs->u1AltPri1 = 0;
    /* Has received an announce msg. */
    pPtpForeignMasterDs->u1FornMastRxCnt = 1;
    pPtpForeignMasterDs->u4AnnounceMsgsCnt = 1;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Updating Grand Master details for the"
              " Foreign Master.\r\n"));

    if (PtpDbAddNode (gPtpGlobalInfo.FMTree,
                      pPtpForeignMasterDs,
                      PTP_FOREIGN_MASTER_DATA_SET) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "Unable to add Foreign Master node into "
                  "data structure!!!!!\r\n"));

        MemReleaseMemBlock (gPtpGlobalInfo.ForeignMasterPoolId,
                            (UINT1 *) pPtpForeignMasterDs);
        pPtpForeignMasterDs = NULL;
    }

    PTP_FN_EXIT ();
    return (pPtpForeignMasterDs);
}

/*****************************************************************************/
/* Function                  : PtpAnncUpdateForenMasterDetails               */
/*                                                                           */
/* Description               : This routine updates the foreign Master       */
/*                             record for the grand master details from the  */
/*                             received PDU.                                 */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpForeignMasterDs - Pointer to the foreign  */
/*                                                   master record whose     */
/*                                                   grand master details    */
/*                                                   need to be updated.     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpAnncUpdateForenMasterDetails (UINT1 *pu1Pdu, tPtpHdrInfo * pPtpHdrInfo,
                                 tPtpForeignMasterDs * pPtpForeignMasterDs)
{
    UINT1              *pu1Tmp = pu1Pdu + PTP_MSG_ANNC_UTC_OFFSET;
    UINT2               u2Val = 0;

    PTP_FN_ENTRY ();

    /* For the format of the received announce message, refer Table 25
     * of IEEE Std 1588 2008
     * */
    /* UTC Offset */
    PTP_LBUF_GET_2_BYTES (pu1Tmp,
                          pPtpForeignMasterDs->AnnounceMsg.u2CurrentUtcOffset);

    /* GM Priority 1 */
    pu1Tmp = pu1Pdu + PTP_MSG_ANNC_GM_PRIO1_OFFSET;
    PTP_LBUF_GET_1_BYTE (pu1Tmp,
                         pPtpForeignMasterDs->AnnounceMsg.u1GMPriority1);

    /* Clock Quality */
    PTP_LBUF_GET_1_BYTE (pu1Tmp,
                         pPtpForeignMasterDs->AnnounceMsg.GMClkQuality.u1Class);
    PTP_LBUF_GET_1_BYTE (pu1Tmp,
                         pPtpForeignMasterDs->AnnounceMsg.GMClkQuality.
                         u1Accuracy);
    PTP_LBUF_GET_2_BYTES (pu1Tmp,
                          pPtpForeignMasterDs->AnnounceMsg.GMClkQuality.
                          u2OffsetScaledLogVariance);

    /* GM Priority 2 */
    PTP_LBUF_GET_1_BYTE (pu1Tmp,
                         pPtpForeignMasterDs->AnnounceMsg.u1GMPriority2);

    /* Grand MAster Identity */
    PTP_LBUF_GET_N_BYTES (pu1Tmp, pPtpForeignMasterDs->AnnounceMsg.GMClkId,
                          PTP_MAX_CLOCK_ID_LEN);

    /* Steps Removed */
    PTP_LBUF_GET_2_BYTES (pu1Tmp, u2Val);
    pPtpForeignMasterDs->AnnounceMsg.u4StepsRemoved = (UINT4) u2Val;

    /* Time Source */
    PTP_LBUF_GET_1_BYTE (pu1Tmp, pPtpForeignMasterDs->AnnounceMsg.u1TimeSource);

    pPtpForeignMasterDs->AnnounceMsg.bLeap59 =
        PTP_GET_FLAG_VALUE (pPtpHdrInfo->u2Flags, PTP_FLAG_LEAP59_MASK);
    pPtpForeignMasterDs->AnnounceMsg.bLeap61 =
        PTP_GET_FLAG_VALUE (pPtpHdrInfo->u2Flags, PTP_FLAG_LEAP59_MASK);
    pPtpForeignMasterDs->AnnounceMsg.bPtpTimeScale =
        PTP_GET_FLAG_VALUE (pPtpHdrInfo->u2Flags, PTP_FLAG_PTP_TIMESCALE_MASK);
    pPtpForeignMasterDs->AnnounceMsg.bTimeTraceable =
        PTP_GET_FLAG_VALUE (pPtpHdrInfo->u2Flags, PTP_FLAG_TIME_TRC_MASK);
    pPtpForeignMasterDs->AnnounceMsg.bFreqTraceable =
        PTP_GET_FLAG_VALUE (pPtpHdrInfo->u2Flags, PTP_FLAG_FREQ_TRC_MASK);
    pPtpForeignMasterDs->AnnounceMsg.bCurrentUtcOffsetValid =
        PTP_GET_FLAG_VALUE (pPtpHdrInfo->u2Flags, PTP_FLAG_UTC_OFFSET_MASK);

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpAnncValidateForeignMaster                  */
/*                                                                           */
/* Description               : This routine validates the foreign Master     */
/*                             record as per section 9.3.2.5 of IEEE Std     */
/*                             1588-2008.                                    */
/*                                                                           */
/* Input                     : pPtpForeignMasterDs - Pointer to the foreign  */
/*                                                   master record whose     */
/*                                                   grand master details    */
/*                                                   need to be validated.   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAnncValidateForeignMaster (tPtpForeignMasterDs * pPtpForeignMasterDs)
{
    tPtpDomain         *pPtpDomain = pPtpForeignMasterDs->pPtpPort->pPtpDomain;

    PTP_FN_ENTRY ();

    /* Refer section 9.3.2.5 of IEEE Std 1588-2008 */
    /* If S was sent from port "r" or from any other port on the clock 
     * containing port "r", then S shall not be qualified. This can be verified
     * by comparing the clock identities (first 6 bytes) of the received 
     * announce message with that of clock's own.
     * */
    if (MEMCMP (&(pPtpForeignMasterDs->ClkId),
                &(pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId),
                PTP_MAX_CLOCK_ID_LEN) == 0)
    {
        PTP_TRC ((pPtpForeignMasterDs->pPtpPort->u4ContextId,
                  pPtpForeignMasterDs->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpAnncValidateForeignMaster:"
                  "Invalid Master Information!!!!!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* If S is not the announce message most recently received on port "r"
     * from a given clock, S shall not be qualified. This would never happen 
     * in our case, as we will update the data base for various announce 
     * messages received.
     * */

    if ((pPtpForeignMasterDs->u1BmcCnt == PTP_FOREIGN_MASTER_TIME_WINDOW) &&
        (pPtpForeignMasterDs->u1FornMastRxCnt < PTP_FOREIGN_MASTER_THRESHOLD))
    {
        /* If sender of S is a foreign Master clock F and fewer than 
         * FOREIGN_MASTER_THRESHOLD nonidentical Announce messages from F have
         * been received within the most recent FOREIGN_MASTER_TIME_WINDOW 
         * interval, then S shall not be qualified.
         * */
        PTP_TRC ((pPtpForeignMasterDs->pPtpPort->u4ContextId,
                  pPtpForeignMasterDs->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpAnncValidateForeignMaster:"
                  "Master Not qualified,Unidentical Announce Message from Master!!!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
    else if (pPtpForeignMasterDs->u1BmcCnt == PTP_FOREIGN_MASTER_TIME_WINDOW)
    {
        /* Sender has transmitted more than FOREIGN_MASTER_THRESHOLD non 
         * identitcal Announce messages within FOREIGN_MASTER_TIME_WINDOW. Now
         * reset the values.
         * */
        pPtpForeignMasterDs->u1BmcCnt = 0;
        pPtpForeignMasterDs->u1FornMastRxCnt = 0;
    }

    /* If the stepsRemoved field is 255 or greater, then the announce message
     * needs to be discarded */

    if (pPtpForeignMasterDs->AnnounceMsg.u4StepsRemoved >=
        PTP_BMC_MAX_STEP_REMOVED)
    {
        PTP_TRC ((pPtpForeignMasterDs->pPtpPort->u4ContextId,
                  pPtpForeignMasterDs->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpAnncValidateForeignMaster:"
                  "Announce Message Discarded !!!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpAnncTxAnncMsg                              */
/*                                                                           */
/* Description               : This routine transmits an  announce message   */
/*                             over the given interface.                     */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure.     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAnncTxAnncMsg (tPtpPort * pPtpPort)
{
    UINT1              *pPtpPdu = NULL;
    UINT1              *pTmpPdu = NULL;
    UINT1              *pPtpPdu1 = NULL;
    tClkSysTimeInfo     PtpSysTimeInfo;
    tPtpTxParams        PtpTxParams;
    UINT4               u4MsgLen = 0;
    UINT4               u4MediaHdrLen = PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    UINT2               u2StepsRemoved = 0;
    UINT2               u2Flags = 0;
    UINT2               u2TwoByteVal = 0;
    UINT1               u1Reserved = 0;

    PTP_FN_ENTRY ();

    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    MEMSET (&PtpTxParams, 0, sizeof (tPtpTxParams));

#ifdef L2RED_WANTED
    /*Only in Slave mode packet need to send out
     *otherwise no need to process the packet */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        if (pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }
    }
#endif

    pPtpPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                             PTP_MAX_PDU_LEN);
    if (pPtpPdu == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | PTP_CRITICAL_TRC,
                  "Unable to allocate memory for transmitting pkt over "
                  "port\r\n", pPtpPort));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (pPtpPdu, 0, PTP_MAX_PDU_LEN);
    u4MsgLen = PTP_ANNC_MSG_SIZE;
    pTmpPdu = pPtpPdu;

    /* Refer Annex F of IEEE Std 1588 2008 */
    PtpHdrAddMediaHdrForIntf (&pTmpPdu, pPtpPort, PTP_ANNC_MSG, &u4MsgLen);

    /* Refer Table 25 of IEEE 1588-2008 for Announce Message format
     * */
    PtpHdrFillPtpHeader (&pTmpPdu, pPtpPort, PTP_ANNC_MSG);

    if (PtpPortClkIwfGetClock (&PtpSysTimeInfo) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | PTP_CRITICAL_TRC,
                  "Unable to obtain Time stamp while transmitting "
                  "Announce message over interface\r\n", pPtpPort));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Origin Time stamp needs to be added after header */
    u2TwoByteVal = (UINT2) UINT8_HI (&(PtpSysTimeInfo.FsClkTimeVal.u8Sec));
    PTP_LBUF_PUT_2_BYTES (pTmpPdu, u2TwoByteVal);
    PTP_LBUF_PUT_4_BYTES (pTmpPdu,
                          UINT8_LO (&(PtpSysTimeInfo.FsClkTimeVal.u8Sec)));
    PTP_LBUF_PUT_4_BYTES (pTmpPdu, (PtpSysTimeInfo.FsClkTimeVal.u4Nsec));

    /* TimePropertiesDs.CurrentUTCoffset */
    PTP_LBUF_PUT_2_BYTES (pTmpPdu,
                          (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.
                           u2CurrentUtcOffset));
    /* Reserved */
    PTP_LBUF_PUT_1_BYTE (pTmpPdu, u1Reserved);

    /* if Alternate master flag is true then this is message will send
     * send out by Slave port. So set the Default Properties as
     * GrandMaster Properties. 
     */
    pPtpPdu1 = pPtpPdu + u4MediaHdrLen + PTP_MSG_FLAGS_OFFSET;
    PTP_LBUF_GET_2_BYTES (pPtpPdu1, u2Flags);

    if ((u2Flags & PTP_FLAG_ALT_MST_MASK) == PTP_FLAG_ALT_MST_MASK)
    {
        /* ParentDs.GrandMasterPriority1 */
        PTP_LBUF_PUT_1_BYTE (pTmpPdu,
                             (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                              ClkDs.u1ClkPriority1));
        /* ParentDs.GrandMasterClockQuality - 
         * 1) Class
         * 2) Accuracy
         * 3) OffsetScaledLogVariance
         * */
        PTP_LBUF_PUT_1_BYTE (pTmpPdu,
                             (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                              ClkDs.ClkQuality.u1Class));
        PTP_LBUF_PUT_1_BYTE (pTmpPdu,
                             (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                              ClkDs.ClkQuality.u1Accuracy));
        PTP_LBUF_PUT_2_BYTES (pTmpPdu,
                              (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                               ClkDs.ClkQuality.u2OffsetScaledLogVariance));
        /* ClkDs.GrandMasterPriority2 */
        PTP_LBUF_PUT_1_BYTE (pTmpPdu,
                             (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                              ClkDs.u1ClkPriority2));
        /* ParentDs.grandmasterIdentity */
        PTP_LBUF_PUT_N_BYTES (pTmpPdu,
                              (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                               ClkDs.ClkId), PTP_MAX_CLOCK_ID_LEN);
        /* currentDS.stepsRemoved */
        u2StepsRemoved = 0;
        PTP_LBUF_PUT_2_BYTES (pTmpPdu, u2StepsRemoved);
    }
    else
    {
        /* ParentDs.GrandMasterPriority1 */
        PTP_LBUF_PUT_1_BYTE (pTmpPdu,
                             (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                              ParentDs.u1GMPriority1));

        /* ParentDs.GrandMasterClockQuality - 
         * 1) Class
         * 2) Accuracy
         * 3) OffsetScaledLogVariance
         * */
        PTP_LBUF_PUT_1_BYTE (pTmpPdu,
                             (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                              ParentDs.GMClkQuality.u1Class));
        PTP_LBUF_PUT_1_BYTE (pTmpPdu,
                             (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                              ParentDs.GMClkQuality.u1Accuracy));
        PTP_LBUF_PUT_2_BYTES (pTmpPdu,
                              (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                               ParentDs.GMClkQuality.
                               u2OffsetScaledLogVariance));

        /* ParentDs.GrandMasterPriority2 */
        PTP_LBUF_PUT_1_BYTE (pTmpPdu,
                             (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                              ParentDs.u1GMPriority2));

        /* ParentDs.grandmasterIdentity */
        PTP_LBUF_PUT_N_BYTES (pTmpPdu,
                              (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                               ParentDs.GMClkId), PTP_MAX_CLOCK_ID_LEN);

        /* currentDS.stepsRemoved */
        u2StepsRemoved =
            (UINT2) pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
            u4StepsRemoved;
        PTP_LBUF_PUT_2_BYTES (pTmpPdu, u2StepsRemoved);
    }
    /* timeProperties.timeSource */
    PTP_LBUF_PUT_1_BYTE (pTmpPdu,
                         (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.
                          u1TimeSource));

    /* Add the Alternate timescale tlv */
    PtpAlTsAddAltTimeTLV (pPtpPort->pPtpDomain, pPtpPdu, &u4MsgLen);

    /* Check the Path Trace Option for this domain and processes Packet */
    if ((PtpPTrcIsPathTraceEnabled (pPtpPort->pPtpDomain)) == OSIX_TRUE)
    {
        /* Add the Path Trace Tlv in the Announce PDU */
        PtpPTrcAddPTrcTLV (pPtpPort, pPtpPdu, &u4MsgLen);
    }

    PtpTxParams.pu1PtpPdu = pPtpPdu;
    PtpTxParams.pPtpPort = pPtpPort;
    PtpTxParams.u4MsgLen = u4MsgLen;
    PtpTxParams.u1MsgType = (UINT1) PTP_ANNC_MSG;

    if (PtpTxTransmitPtpMessage (&PtpTxParams) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpTxTransmitPtpMessage returned " "Failure!!!!!\r\n",
                  pPtpPort));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpAnncInitAnncHandler                        */
/*                                                                           */
/* Description               : This routine initialses the announce handler  */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpAnncInitAnncHandler (VOID)
{
    PTP_FN_ENTRY ();

    /* Initialize the message handler function pointer */
    gPtpGlobalInfo.PtpMsgFn[PTP_ANNC_FN_PTR].pMsgRxHandler =
        PtpAnncHandleRxAnncMsg;

    PTP_FN_EXIT ();
}
#endif /* _PTPANNC_C_ */
