/* $Id: ptpdreq.c,v 1.5 2014/01/24 12:20:08 siva Exp $ */
/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains PTP task delay request message handler 
 *              related routines.
 *********************************************************************/
#ifndef _PTPDREQ_C_
#define _PTPDREQ_C_

#include "ptpincs.h"

PRIVATE INT4        PtpDreqValidateDelayReqMsg (UINT1 *pu1Pdu,
                                                tPtpPort * pPtpPort);
PRIVATE INT4        PtpDreqHandleDelayReqMsg (UINT1 *pu1Pdu,
                                              tPtpPort * pPtpPort);

/*****************************************************************************/
/* Function                  : PtpDreqHandleRxDelayReqMsg                    */
/*                                                                           */
/* Description               : This routine will be invoked to handle the    */
/*                             delay request messages. This routine validates*/
/*                             and processes the received delay request      */
/*                             message.                                      */
/*                                                                           */
/* Input                     : pPtpPktParams - Pointer to received Packet    */
/*                                             Parameters.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpDreqHandleRxDelayReqMsg (tPtpPktParams * pPtpPktParams)
{
    tClkSysTimeInfo     PtpSysTimeInfo;

    PTP_FN_ENTRY ();

    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));

    if (pPtpPktParams->pPtpPort->pPtpDomain->ClkMode
        == PTP_TRANSPARENT_CLOCK_MODE)
    {
        if (pPtpPktParams->pPtpPort->pPtpDomain->ClockDs.TransClkDs.TransClkType
            != PTP_E2E_TRANSPARENT_CLOCK)
        {
            /* The device is operating as P2P Transparent clock. Hence
             * drop the received Delay request and response messages.
             * Refer Section 10.3 of IEEE Std 1588-2008
             * */
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Clock is not operating in End-to-"
                      "End transparent mode.\r\n"));
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Hence discarding the received "
                      "Delay request message.\r\n"));

            /* Incrementing the dropped message count */
            pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;

            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }

        /* Clock is operating in transparent mode E2E. Do not consume the 
         * packet. Give it to transparent clock handler for forwarding
         * Since, Delay request message is an event message, generate 
         * the Ingress event time stamp.  
         * */
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "Clock is operating in Transparent "
                  "mode.\r\n"));

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "TimeStamping for ingress event "
                  "message.\r\n"));

        /* The timestamping for ingress and egress events along with correction
         * field updation should happen only when software time stamping is 
         * enabled in the system.
         * The packet should be handled at the hardware level itself if hardware
         * time stamping is enabled and hardware supprts updation of residence
         * time for the transparent clocks.
         * */

        if (gPtpGlobalInfo.TimeStamp == PTP_HARDWARE_TRANS_TIMESTAMPING)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "Forwarding the received dleay request"
                  "message.\r\n"));

        PtpTransHandleRcvdPtpPkt (pPtpPktParams->pu1RcvdPdu,
                                  pPtpPktParams->pPtpPort,
                                  pPtpPktParams->u4PktLen, PTP_DELAY_REQ_MSG,
                                  &(pPtpPktParams->pPtpIngressTimeStamp->
                                    FsClkTimeVal));

        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    pPtpPktParams->pu1RcvdPdu =
        pPtpPktParams->pu1RcvdPdu +
        PTP_GET_MEDIA_HDR_LEN (pPtpPktParams->pPtpPort);

    /* Copying the delay request ingress time stamp i.e t4 */
    MEMCPY (&pPtpPktParams->pPtpPort->DelayReqIngressTime,
            &pPtpPktParams->pPtpIngressTimeStamp->FsClkTimeVal,
            sizeof (tFsClkTimeVal));

    if (PtpDreqValidateDelayReqMsg (pPtpPktParams->pu1RcvdPdu,
                                    pPtpPktParams->pPtpPort) != OSIX_SUCCESS)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpDreqValidateDelayReqMsg: "
                  "Delay Request Message Validation returned Failure!!\r\n"));

        /* Incrementing the dropped message count */
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpDreqHandleDelayReqMsg (pPtpPktParams->pu1RcvdPdu,
                                  pPtpPktParams->pPtpPort) != OSIX_SUCCESS)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpDreqHandleDelayReqMsg: "
                  "PtpDreqHandleDelayReqMsg returned Failure!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Incrementing the delay request message received count */
    pPtpPktParams->pPtpPort->PtpPortStats.u4RcvdDelayReqMsgCnt++;

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpDreqValidateDelayReqMsg                    */
/*                                                                           */
/* Description               : This routine validates the received PTP       */
/*                             delay request message.                        */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpDreqValidateDelayReqMsg (UINT1 *pu1Pdu, tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    UNUSED_PARAM (pu1Pdu);

    /* The received delay request message should be discarded on any one of the
     * following criterias
     * 1) Port is in INITIALIZING or DISABLED state
     * 2) Port is in FAULTY state
     * 3) Port is not in MASTER state
     * */
    if ((pPtpPort->PortDs.u1PortState == PTP_STATE_INITIALIZING) ||
        (pPtpPort->PortDs.u1PortState == PTP_STATE_DISABLED) ||
        (pPtpPort->PortDs.u1PortState == PTP_STATE_FAULTY))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Port %d is in INITIALIZING/DISABLED/FAULTY" " State\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Discarding the received Delay Request " "Message\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (pPtpPort->PortDs.u1PortState != PTP_STATE_MASTER)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Port is not in MASTER State\r\n"));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Discarding the received Delay Request " "Message\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpDreqHandleDelayReqMsg                      */
/*                                                                           */
/* Description               : This routine handles the received delay req   */
/*                             messages. The PTP messages should be validated*/
/*                             earlier itself before invoking this routine.  */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpDreqHandleDelayReqMsg (UINT1 *pu1Pdu, tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    /* On receiving a delay request message, a master should respond with a
     * delay response message. Trigger the delay response handler to transmit
     * a message back to the port. 
     * */
    if (PtpDrespTxDelayResp (pu1Pdu, pPtpPort) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpDreqHandleDelayReqMsg: "
                  "PtpDrespTxDelayResp returned failure\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpDreqTxDelayReqMsg                          */
/*                                                                           */
/* Description               : This routine transmits an Delay request mess  */
/*                             over the given interface.                     */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure.     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpDreqTxDelayReqMsg (tPtpPort * pPtpPort)
{
    UINT1              *pu1PtpPdu = NULL;
    UINT1              *pu1TmpPdu = NULL;
    tPtpTxParams        PtpTxParams;
    UINT4               u4MsgLen = PTP_DELAY_REQ_MSG_LEN;
    UINT1               au1Reserved[PTP_SRC_PORT_ID_LEN];

    PTP_FN_ENTRY ();

    MEMSET (au1Reserved, 0, PTP_SRC_PORT_ID_LEN);
    MEMSET (&PtpTxParams, 0, sizeof (tPtpTxParams));

#ifdef L2RED_WANTED
    /*only in Slave mode packet need to send out
     *otherwise no need to process the packet */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        if (pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }
    }
#endif

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Transmitting Delay Request Message "
              "over interface\r\n"));

    if (pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Delay request timer expired for "
                  "non-slave port.\r\n"));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Not transmitting delay request "
                  "message.\r\n"));
        PTP_FN_EXIT ();

        return OSIX_SUCCESS;
    }

    /* Delay request and response is applicable only for boundary
     * clocks and ordinary clocks.
     * */
    if ((pPtpPort->pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE) &&
        (pPtpPort->pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Delay request cannot be transmitted for"
                  "clock modes that are not Boundary or ordinary.\r\n"));

        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    /* If the mechanism used by the port is P2P, then the Delay request message
     * should not be sent
     * */
    if (pPtpPort->PortDs.u1DelayMechanism != PTP_PORT_DELAY_MECH_END_TO_END)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Delay request message cannot be transmitted for ports "
                  "not having END-TO-END Delay mechanism enabled....\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    pu1PtpPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                               PTP_MAX_PDU_LEN);
    if (pu1PtpPdu == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                  "Unable to allocate memory for transmitting pkt over "
                  "port\r\n"));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (pu1PtpPdu, 0, PTP_MAX_PDU_LEN);

    pu1TmpPdu = pu1PtpPdu;

    PtpHdrAddMediaHdrForIntf (&pu1TmpPdu, pPtpPort, PTP_DELAY_REQ_MSG,
                              &u4MsgLen);

    /* Refer Table 26 of IEEE 1588-2008 for Delay request Message format
     * */
    PtpHdrFillPtpHeader (&pu1TmpPdu, pPtpPort, (UINT1) PTP_DELAY_REQ_MSG);

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Filling PTP header for the message.\r\n"));

    PtpTxParams.pu1PtpPdu = pu1PtpPdu;
    PtpTxParams.pPtpPort = pPtpPort;
    PtpTxParams.u4MsgLen = u4MsgLen;
    PtpTxParams.u1MsgType = (UINT1) PTP_DELAY_REQ_MSG;

#ifdef L2RED_WANTED
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        /*Changing clock identity. Making 5th byte as 0xFF from 0xFE */
        PTP_RED_CHANGE_CLK_IDENTITY (pu1PtpPdu);
    }
#endif
    if (PtpTxTransmitPtpMessage (&PtpTxParams) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpTxTransmitPtpMessage returned "
                  "Failure!!!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpDreqInitDreqHandler                        */
/*                                                                           */
/* Description               : This routine initialses the Delay Request     */
/*                             Message handler.                              */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDreqInitDreqHandler (VOID)
{
    PTP_FN_ENTRY ();

    /* Initialize the message handler function pointer */
    gPtpGlobalInfo.PtpMsgFn[PTP_DREQ_FN_PTR].pMsgRxHandler =
        PtpDreqHandleRxDelayReqMsg;
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpDreqStartDelayReqAndTxPkt                  */
/*                                                                           */
/* Description               : This routine updates the delay request timer  */
/*                             and starts the same, if this is first sync    */
/*                             message received over this port and delay req */
/*                             timer is not running.                         */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to Port.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpDreqStartDelayReqAndTxPkt (tPtpPort * pPtpPort)
{
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    /* If the mechanism used by the port is P2P, then the Delay request message
     * should not be sent
     * */
    if (pPtpPort->PortDs.u1DelayMechanism != PTP_PORT_DELAY_MECH_END_TO_END)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Delay request message cannot be transmitted for ports "
                  "having P2P Delay mechanism enabled....\r\n"));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if ((FSAP_U8_FETCH_LO (&(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                             CurrentDs.u8MeanPathDelay)) != 0) ||
        (FSAP_U8_FETCH_HI (&(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                             CurrentDs.u8MeanPathDelay)) != 0))
    {
        /* Since mean path delay is known already no need to start timer
         * and do this exercise
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Mean Path Delay is calculated already.....\r\n"));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if (PtpDreqTxDelayReqMsg (pPtpPort) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "Unable to transmit Delay Request message on receipt "
                  "of sync message...\r\n"));
    }

    u4MilliSec = PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                            u1MinDelayReqInterval);

    if (PtpTmrStartTmr (&(pPtpPort->DelayReqTimer),
                        u4MilliSec, PTP_DREQ_TMR, OSIX_FALSE) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "Port %u PtpTmrDelayReqTmrExp: "
                  "PtpTmrStartTmr returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Unable to restart Delay Request timer\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif /* _PTPDREQ_C_ */
