/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpval.c,v 1.4 2014/01/24 12:20:09 siva Exp $
 * Description: This file contains PTP task main loop and initialization
 *              routines.
 *********************************************************************/
#ifndef _PTPVAL_C_
#define _PTPVAL_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function                  : PtpValValidateDomainDS                        */
/*                                                                           */
/* Description               : This routine validates the index for the      */
/*                             table. This routine will be called from the   */
/*                             low level test routines                       */
/*                                                                           */
/* Input                     : u4ContextId   -  Contex identifier            */
/*                             u1DomainId    -  Domain identifier            */
/*                                                                           */
/* Output                    : pu4ErrorCode  -  SNMP_ERR_NO_CREATION if the  */
/*                                              index value is outside the   */
/*                                              range                        */
/*                                              SNMP_ERR_INCONSISTENT_NAME   */
/*                                              if the index value is        */
/*                                              within the range but does    */
/*                                              not exist in the system      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpValValidateDomainDS (UINT4 u4ContextId, UINT1 u1DomainId,
                        UINT4 *pu4ErrorCode)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    if ((u4ContextId > (UINT4) PTP_MAX_CONTEXT_ID) ||
        (u1DomainId >= (UINT1) PTP_MAX_DOMAINS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetVal = OSIX_FAILURE;
    }

    if ((i4RetVal == OSIX_SUCCESS) &&
        (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN))
    {
        PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                  "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i4RetVal = OSIX_FAILURE;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        pPtpDomain = PtpDmnGetDomainEntry (u4ContextId, u1DomainId);

        if (pPtpDomain == NULL)
        {
            PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                      "Domain %d Data Set does not exist\r\n", u1DomainId));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PTP_ERR_DOMAIN_NOT_PRESENT);
            i4RetVal = OSIX_FAILURE;
        }
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpValValidatePortConfigDS                    */
/*                                                                           */
/* Description               : This routine validates the index for the      */
/*                             table. This routine will be called from the   */
/*                             low level test routines                       */
/*                                                                           */
/* Input                     : u4ContextId   -  Contex identifier            */
/*                             u1DomainId    -  Domain identifier            */
/*                             u4PortIndex   -  Port Identifier              */
/*                             u1IsTransPort -  OSIX_TRUE / OSIX_FALSE       */
/*                                                                           */
/* Output                    : pu4ErrorCode  -  SNMP_ERR_NO_CREATION if the  */
/*                                              index value is outside the   */
/*                                              range                        */
/*                                              SNMP_ERR_INCONSISTENT_NAME   */
/*                                              if the index value is        */
/*                                              within the range but does    */
/*                                              not exist in the system      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpValValidatePortConfigDS (UINT4 u4ContextId, UINT1 u1DomainId,
                            UINT4 u4PortIndex, UINT1 u1IsTransPort,
                            UINT4 *pu4ErrorCode)
{
    tPtpPort           *pPtpPort = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    if ((u4ContextId > (UINT4) PTP_MAX_CONTEXT_ID) ||
        (u1DomainId >= (UINT1) PTP_MAX_DOMAINS) ||
        (u4PortIndex > (UINT4) PTP_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetVal = OSIX_FAILURE;
    }

    if ((i4RetVal == OSIX_SUCCESS) &&
        (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN))
    {
        PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                  "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i4RetVal = OSIX_FAILURE;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        pPtpPort = PtpIfGetPortEntry (u4ContextId, u1DomainId, u4PortIndex);

        if ((pPtpPort == NULL) || (pPtpPort->bIsTranPort != u1IsTransPort))
        {
            PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                      "Domain %d Port %d Data Set does not exist\r\n",
                      u1DomainId, u4PortIndex));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PTP_ERR_PORT_NOT_PRESENT);
            i4RetVal = OSIX_FAILURE;
        }
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpValValidateGrandMaster                     */
/*                                                                           */
/* Description               : This routine validates the index for the      */
/*                             table. This routine will be called from the   */
/*                             low level test routines                       */
/*                                                                           */
/* Input                     : u4ContextId      -  Contex identifier         */
/*                             u1DomainId       -  Domain identifier         */
/*                             u4NwProtocol     -  Network protocol          */
/*                             pFsPtpMasterAddr -  Master address            */
/*                                                                           */
/* Output                    : pu4ErrorCode  -  SNMP_ERR_NO_CREATION if the  */
/*                                              index value is outside the   */
/*                                              range                        */
/*                                              SNMP_ERR_INCONSISTENT_NAME   */
/*                                              if the index value is        */
/*                                              within the range but does    */
/*                                              not exist in the system      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpValValidateGrandMaster (UINT4 u4ContextId, UINT1 u1DomainId,
                           UINT4 u4NwProtocol,
                           tSNMP_OCTET_STRING_TYPE * pFsPtpMasterAddr,
                           UINT4 *pu4ErrorCode)
{
    tPtpGrandMasterTable *pPtpGrandMasterTable = NULL;
    tPtpGrandMasterTable *pTmpPtpGrandMasterTable = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    if ((u4ContextId > (UINT4) PTP_MAX_CONTEXT_ID) ||
        (u1DomainId >= (UINT1) PTP_MAX_DOMAINS) ||
        (u4NwProtocol >= (UINT4) PTP_MAX_NW_PROTOCOL) ||
        (pFsPtpMasterAddr->i4_Length < PTP_MASTER_MIN_ADDR_LEN) ||
        (pFsPtpMasterAddr->i4_Length > PTP_MASTER_MIN_ADDR_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetVal = OSIX_FAILURE;
    }

    if ((i4RetVal == OSIX_SUCCESS) &&
        (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN))
    {
        PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                  "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i4RetVal = OSIX_FAILURE;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        pTmpPtpGrandMasterTable = (tPtpGrandMasterTable *)
            MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);

        if (pTmpPtpGrandMasterTable == NULL)
        {
            PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                      ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                      "Memory Allocation failed for "
                      "Grand master creation\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return OSIX_FAILURE;
        }

        MEMSET (pTmpPtpGrandMasterTable, 0, sizeof (tPtpGrandMasterTable));

        pTmpPtpGrandMasterTable->u4ContextId = u4ContextId;
        pTmpPtpGrandMasterTable->GMAddr.u2NetworkProtocol
            = (UINT2) u4NwProtocol;
        pTmpPtpGrandMasterTable->GMAddr.u2AddrLength
            = (UINT2) pFsPtpMasterAddr->i4_Length;
        pTmpPtpGrandMasterTable->u1DomainId = u1DomainId;
        MEMCPY (pTmpPtpGrandMasterTable->GMAddr.ai1Addr,
                pFsPtpMasterAddr->pu1_OctetList, pFsPtpMasterAddr->i4_Length);

        pPtpGrandMasterTable = (tPtpGrandMasterTable *)
            PtpDbGetNode (gPtpGlobalInfo.GrandMastClustLst,
                          pTmpPtpGrandMasterTable,
                          (UINT1) PTP_GRAND_MASTER_DATA_SET);

        if (pPtpGrandMasterTable == NULL)
        {
            PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                      "Grand master data set does not exists\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PTP_ERR_GRAND_MASTER_NOT_PRESENT);
            i4RetVal = OSIX_FAILURE;
        }

        MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                            (UINT1 *) pTmpPtpGrandMasterTable);
        pTmpPtpGrandMasterTable = NULL;
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpValValidateUnicastMaster                   */
/*                                                                           */
/* Description               : This routine validates the index for the      */
/*                             table. This routine will be called from the   */
/*                             low level test routines                       */
/*                                                                           */
/* Input                     : u4ContextId      -  Contex identifier         */
/*                             u1DomainId       -  Domain identifier         */
/*                             u4PortNum        -  PTP port num              */
/*                             u4NwProtocol     -  Network protocol          */
/*                             pFsPtpMasterAddr -  Master address            */
/*                                                                           */
/* Output                    : pu4ErrorCode  -  SNMP_ERR_NO_CREATION if the  */
/*                                              index value is outside the   */
/*                                              range                        */
/*                                              SNMP_ERR_INCONSISTENT_NAME   */
/*                                              if the index value is        */
/*                                              within the range but does    */
/*                                              not exist in the system      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpValValidateUnicastMaster (UINT4 u4ContextId, UINT1 u1DomainId,
                             UINT4 u4PortNum, UINT4 u4NwProtocol,
                             tSNMP_OCTET_STRING_TYPE * pFsPtpMasterAddr,
                             UINT4 *pu4ErrorCode)
{
    tPtpAccUnicastMasterTable *pPtpAccUnicastMasterTable = NULL;
    tPtpAccUnicastMasterTable *pTmpPtpAccUniCastMasterTable = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    if ((u4ContextId > (UINT4) PTP_MAX_CONTEXT_ID) ||
        (u1DomainId >= (UINT1) PTP_MAX_DOMAINS) ||
        (u4PortNum > (UINT1) PTP_MAX_PORTS) ||
        (u4NwProtocol >= (UINT4) PTP_MAX_NW_PROTOCOL) ||
        (pFsPtpMasterAddr->i4_Length < PTP_MASTER_MIN_ADDR_LEN) ||
        (pFsPtpMasterAddr->i4_Length > PTP_MASTER_MIN_ADDR_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetVal = OSIX_FAILURE;
    }

    if ((i4RetVal == OSIX_SUCCESS) &&
        (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN))
    {
        PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                  "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i4RetVal = OSIX_FAILURE;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        pTmpPtpAccUniCastMasterTable =
            (tPtpAccUnicastMasterTable *)
            MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);

        if (pTmpPtpAccUniCastMasterTable == NULL)
        {
            PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                      OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                      "Memory Allocation failed " "for master creation\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return (INT1) SNMP_FAILURE;
        }

        MEMSET (pTmpPtpAccUniCastMasterTable, 0,
                sizeof (tPtpAccUnicastMasterTable));

        pTmpPtpAccUniCastMasterTable->u4ContextId = u4ContextId;
        pTmpPtpAccUniCastMasterTable->u4PtpPortNumber = u4PortNum;
        pTmpPtpAccUniCastMasterTable->UMAddr.u2NetworkProtocol
            = (UINT2) u4NwProtocol;
        pTmpPtpAccUniCastMasterTable->UMAddr.u2AddrLength
            = (UINT2) pFsPtpMasterAddr->i4_Length;
        pTmpPtpAccUniCastMasterTable->u1DomainId = u1DomainId;
        MEMCPY (pTmpPtpAccUniCastMasterTable->UMAddr.ai1Addr,
                pFsPtpMasterAddr->pu1_OctetList, pFsPtpMasterAddr->i4_Length);

        pPtpAccUnicastMasterTable = (tPtpAccUnicastMasterTable *)
            PtpDbGetNode (gPtpGlobalInfo.UnicastMastLst,
                          pTmpPtpAccUniCastMasterTable,
                          (UINT1) PTP_UNICAST_MASTER_DATA_SET);
        if (pPtpAccUnicastMasterTable == NULL)
        {
            PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                      "Unicast master data set does not exists\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PTP_ERR_UNICAST_MASTER_NOT_PRESENT);
            i4RetVal = OSIX_FAILURE;
        }

        MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                            (UINT1 *) pTmpPtpAccUniCastMasterTable);
        pTmpPtpAccUniCastMasterTable = NULL;
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpValValidateAccMaster                       */
/*                                                                           */
/* Description               : This routine validates the index for the      */
/*                             table. This routine will be called from the   */
/*                             low level test routines                       */
/*                                                                           */
/* Input                     : u4ContextId      -  Contex identifier         */
/*                             u1DomainId       -  Domain identifier         */
/*                             u4NwProtocol     -  Network protocol          */
/*                             pFsPtpMasterAddr -  Master address            */
/*                                                                           */
/* Output                    : pu4ErrorCode  -  SNMP_ERR_NO_CREATION if the  */
/*                                              index value is outside the   */
/*                                              range                        */
/*                                              SNMP_ERR_INCONSISTENT_NAME   */
/*                                              if the index value is        */
/*                                              within the range but does    */
/*                                              not exist in the system      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpValValidateAccMaster (UINT4 u4ContextId, UINT1 u1DomainId,
                         UINT4 u4NwProtocol,
                         tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr,
                         UINT4 *pu4ErrorCode)
{
    tPtpAccMasterTable *pPtpAccMasterTable = NULL;
    tPtpAccMasterTable  PtpAccMasterTable;
    INT4                i4RetVal = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    if ((u4ContextId > (UINT4) PTP_MAX_CONTEXT_ID) ||
        (u1DomainId >= (UINT1) PTP_MAX_DOMAINS) ||
        (u4NwProtocol >= (UINT4) PTP_MAX_NW_PROTOCOL) ||
        (pFsPtpAccMasterAddr->i4_Length < PTP_MASTER_MIN_ADDR_LEN) ||
        (pFsPtpAccMasterAddr->i4_Length > PTP_MASTER_MAX_ADDR_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetVal = OSIX_FAILURE;
    }

    if ((i4RetVal == OSIX_SUCCESS) &&
        (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN))
    {
        PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                  "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i4RetVal = OSIX_FAILURE;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        MEMSET (&PtpAccMasterTable, 0, sizeof (tPtpAccMasterTable));

        PtpAccMasterTable.u4ContextId = u4ContextId;
        PtpAccMasterTable.AccMstAddr.u2NetworkProtocol = (UINT2) u4NwProtocol;
        PtpAccMasterTable.AccMstAddr.u2AddrLength
            = (UINT2) pFsPtpAccMasterAddr->i4_Length;
        PtpAccMasterTable.u1DomainId = u1DomainId;
        MEMCPY (PtpAccMasterTable.AccMstAddr.ai1Addr,
                pFsPtpAccMasterAddr->pu1_OctetList,
                MEM_MAX_BYTES (sizeof (PtpAccMasterTable.AccMstAddr.ai1Addr),
                               (UINT4) pFsPtpAccMasterAddr->i4_Length));

        pPtpAccMasterTable = (tPtpAccMasterTable *)
            PtpDbGetNode (gPtpGlobalInfo.AccMastLst,
                          &(PtpAccMasterTable),
                          (UINT1) PTP_ACC_MASTER_DATA_SET);

        if (pPtpAccMasterTable == NULL)
        {
            PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                      "Acceptable master data set does not exists\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PTP_ERR_ACCEPT_MASTER_NOT_PRESENT);
            i4RetVal = OSIX_FAILURE;
        }
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpValValidateSecKeyDS                        */
/*                                                                           */
/* Description               : This routine validates the index for the      */
/*                             table. This routine will be called from the   */
/*                             low level test routines                       */
/*                                                                           */
/* Input                     : u4ContextId   -  Contex identifier            */
/*                             u1DomainId    -  Domain identifier            */
/*                             u4SecKeyId    -  Security Key Id              */
/*                                                                           */
/* Output                    : pu4ErrorCode  -  SNMP_ERR_NO_CREATION if the  */
/*                                              index value is outside the   */
/*                                              range                        */
/*                                              SNMP_ERR_INCONSISTENT_NAME   */
/*                                              if the index value is        */
/*                                              within the range but does    */
/*                                              not exist in the system      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpValValidateSecKeyDS (UINT4 u4ContextId, UINT1 u1DomainId,
                        UINT4 u4SecKeyId, UINT4 *pu4ErrorCode)
{
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpSecKeyDs        PtpSecKeyDs;
    INT4                i4RetVal = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    if ((u4ContextId > (UINT4) PTP_MAX_CONTEXT_ID) ||
        (u1DomainId >= (UINT1) PTP_MAX_DOMAINS) ||
        (u4SecKeyId > (UINT4) PTP_SEC_MAX_KEY_ID))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetVal = OSIX_FAILURE;
    }

    if ((i4RetVal == OSIX_SUCCESS) &&
        (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN))
    {
        PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                  "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i4RetVal = OSIX_FAILURE;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        MEMSET (&PtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

        PtpSecKeyDs.u4ContextId = u4ContextId;
        PtpSecKeyDs.u1DomainId = u1DomainId;
        PtpSecKeyDs.u4SecKeyId = u4SecKeyId;

        pPtpSecKeyDs = (tPtpSecKeyDs *)
            PtpDbGetNode (gPtpGlobalInfo.SecKeyLst,
                          &(PtpSecKeyDs), (UINT1) PTP_SEC_KEY_DATA_SET);

        if (pPtpSecKeyDs == NULL)
        {
            PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                      "Security key Data Set does not exist\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PTP_ERR_SEC_KEY_NOT_PRESENT);
            i4RetVal = OSIX_FAILURE;
        }
        else if (pPtpSecKeyDs->u1RowStatus != (UINT1) NOT_IN_SERVICE)
        {
            PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                      "Security key Data Set should not be in service\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i4RetVal = OSIX_FAILURE;
        }
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpValValidateSADS                            */
/*                                                                           */
/* Description               : This routine validates the index for the      */
/*                             table. This routine will be called from the   */
/*                             low level test routines                       */
/*                                                                           */
/* Input                     : u4ContextId   -  Contex identifier            */
/*                             u1DomainId    -  Domain identifier            */
/*                             u4SaId        -  Security Association Id      */
/*                                                                           */
/* Output                    : pu4ErrorCode  -  SNMP_ERR_NO_CREATION if the  */
/*                                              index value is outside the   */
/*                                              range                        */
/*                                              SNMP_ERR_INCONSISTENT_NAME   */
/*                                              if the index value is        */
/*                                              within the range but does    */
/*                                              not exist in the system      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpValValidateSADS (UINT4 u4ContextId, UINT1 u1DomainId,
                    UINT4 u4SaId, UINT4 *pu4ErrorCode)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    if ((u4ContextId > (UINT4) PTP_MAX_CONTEXT_ID) ||
        (u1DomainId >= (UINT1) PTP_MAX_DOMAINS) ||
        (u4SaId > (UINT4) PTP_SA_MAX_ID))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetVal = OSIX_FAILURE;
    }

    if ((i4RetVal == OSIX_SUCCESS) &&
        (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN))
    {
        PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                  "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i4RetVal = OSIX_FAILURE;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {

        pPtpSADs = PtpSecSAGetSAEntry (u4ContextId, u1DomainId, u4SaId);

        if (pPtpSADs == NULL)
        {
            PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                      "Security Association Data Set does not exist\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PTP_ERR_SA_NOT_PRESENT);
            i4RetVal = OSIX_FAILURE;
        }
        else if (pPtpSADs->u1RowStatus != (UINT1) NOT_IN_SERVICE)
        {
            PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                      "Security Association Data Set should not be in "
                      "service\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i4RetVal = OSIX_FAILURE;
        }

    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpValValidateAltTimeDS                       */
/*                                                                           */
/* Description               : This routine validates the index for the      */
/*                             table. This routine will be called from the   */
/*                             low level test routines                       */
/*                                                                           */
/* Input                     : u4ContextId          -  Contex identifier     */
/*                             u1DomainId           -  Domain identifier     */
/*                             u1AltTimeScaleKeyId  -  Alternate time scale  */
/*                                                     key id                */
/*                                                                           */
/* Output                    : pu4ErrorCode  -  SNMP_ERR_NO_CREATION if the  */
/*                                              index value is outside the   */
/*                                              range                        */
/*                                              SNMP_ERR_INCONSISTENT_NAME   */
/*                                              if the index value is        */
/*                                              within the range but does    */
/*                                              not exist in the system      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpValValidateAltTimeDS (UINT4 u4ContextId, UINT1 u1DomainId,
                         UINT1 u1AltTimeScaleKeyId, UINT4 *pu4ErrorCode)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry PtpAltTimescaleEntry;
    INT4                i4RetVal = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    if (u4ContextId > (UINT4) PTP_MAX_CONTEXT_ID)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetVal = OSIX_FAILURE;
    }

    if (u1DomainId >= (UINT1) PTP_MAX_DOMAINS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetVal = OSIX_FAILURE;
    }

    if (u1AltTimeScaleKeyId > (UINT1) PTP_ALT_TIME_MAX_KEY_ID)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetVal = OSIX_FAILURE;
    }

    if ((i4RetVal == OSIX_SUCCESS) &&
        (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN))
    {
        PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                  "PTP module is shutdown\r\n"));

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i4RetVal = OSIX_FAILURE;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        MEMSET (&PtpAltTimescaleEntry, 0, sizeof (tPtpAltTimescaleEntry));
        PtpAltTimescaleEntry.u4ContextId = u4ContextId;
        PtpAltTimescaleEntry.u1DomainId = u1DomainId;
        PtpAltTimescaleEntry.u1KeyId = u1AltTimeScaleKeyId;

        pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
            PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                          &(PtpAltTimescaleEntry),
                          (UINT1) PTP_ALT_TIME_DATA_SET);

        if (pPtpAltTimescaleEntry == NULL)
        {
            PTP_TRC ((u4ContextId, u1DomainId, MGMT_TRC | ALL_FAILURE_TRC,
                      "Alternate time scale Data Set does not exist\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_PTP_ERR_ATS_NOT_PRESENT);
            i4RetVal = OSIX_FAILURE;
        }
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}
#endif /* _PTPVAL_C_ */
