/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpalts.c,v 1.5 2014/02/07 13:27:42 siva Exp $
 *
 * Description: This file contains PTP Alternate timescale Sub module 
 *              functionality routines.
 *********************************************************************/
#ifndef _PTPALTS_C_
#define _PTPALTS_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function     : PtpAlTsHandleUpdate                                        */
/*                                                                           */
/* Description  : This function is called from the configuration sub module  */
/*                to update of the alternate timescale tlv in the domain     */
/*                table.It will scan the Alternate Timescale Table all active*/
/*                entries are collected and form the Alternate timescale     */
/*                tlv and update the Domain specific Alternate               */
/*                                                                           */
/* Input        : u4ContextId - Context Identifier.                          */
/*              : u1DomainId  - Domain Identifier.                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/ OSIX_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAlTsHandleUpdate (UINT4 u4ContextId, UINT1 u1DomainId)
{
    tPtpAltTimescaleEntry ATSEntry;
    tPtpAltTimescaleEntry *pATSEntry = NULL;
    UINT1              *pu1ATSTlv = NULL;
    UINT1              *pu1TmpATSTlv = NULL;
    UINT4               u4TmpContextId = 0;
    INT4                i4Status = OSIX_FAILURE;
    UINT2               u2TotLen = 0;
    UINT2               u2Val = 0;
    UINT2               u2TlvLen = 0;
    UINT1               u1KeyId = 0xFF;
    UINT1               u1Pad = 0;
    UINT1               u1NameLen = 0;
    UINT1               u1TmpDomainId = 0;
    UINT1               u1EntCount = 0;
    BOOL1               bIsFirst = OSIX_FALSE;

    PTP_FN_ENTRY ();

    /* Scan the Alternate Timescale Table update the Domain Speific
     * AlternateTimescal TVL
     */
    /* Build the TLV */
    /* Set pu1ATSTlv[0-1]  as    PTP_TLV_TYPE_ALT_TIME_OFFSET_IND */
    /* Set pu1ATSTlv[2-3]  as    (length of the tlv)              */
    /* Set pu1ATSTlv[4]    as   u1KeyId                          */
    /* Set pu1ATSTlv[5-8]  as   currentOffset                    */
    /* Set pu1ATSTlv[9-12] as   jumpSeconds                      */
    /* Set pu1ATSTlv[13-18]as   timeOfNextJump                   */
    /* Set pu1ATSTlv[19-(lenofname)  displayName                 */
    /* Set pu1ATSTlv[19+(lenofname)] pad 0 for an octet if the 
     *                               namelen is even             */

    MEMSET (&ATSEntry, 0, sizeof (tPtpAltTimescaleEntry));

    PtpUtilFindStartIndex (u4ContextId, &u4TmpContextId,
                           u1DomainId, &u1TmpDomainId, &bIsFirst);
    if (bIsFirst == OSIX_TRUE)
    {
        pATSEntry = (tPtpAltTimescaleEntry *)
            PtpDbGetFirstNode (gPtpGlobalInfo.AltTimSclTree,
                               (UINT1) PTP_ALT_TIME_DATA_SET);
    }
    else
    {
        ATSEntry.u4ContextId = u4TmpContextId;
        ATSEntry.u1DomainId = u1TmpDomainId;
        ATSEntry.u1KeyId = u1KeyId;

        pATSEntry = (tPtpAltTimescaleEntry *)
            PtpDbGetNextNode (gPtpGlobalInfo.AltTimSclTree,
                              &(ATSEntry), (UINT1) PTP_ALT_TIME_DATA_SET);
    }

    if (pATSEntry == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpAlTsHandleUpdate : PtpDbGetNextNode Failed. \r\n"));

        PTP_FN_EXIT ();

        return (OSIX_FAILURE);
    }

    pu1ATSTlv = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4AltTimeBuddyId,
                               PTP_MAX_ALT_TIME_TLV_BLOCK_SIZE);
    if (pu1ATSTlv == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpAlTsHandleUpdate : MemBuddyAlloc Failed. \r\n"));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);

        PTP_FN_EXIT ();

        return (OSIX_FAILURE);
    }
    pu1TmpATSTlv = pu1ATSTlv;

    do
    {
        if ((pATSEntry->u4ContextId == u4ContextId) &&
            (pATSEntry->u1DomainId == u1DomainId))
        {
            /* Collect the Active Time scales */
            if (pATSEntry->u1Status == ACTIVE)
            {
                u2Val = PTP_TLV_TYPE_ALT_TIME_OFFSET_IND;
                PTP_LBUF_PUT_2_BYTES (pu1TmpATSTlv, u2Val);

                u1NameLen = (UINT1) STRLEN (pATSEntry->au1DisplayName);
                if ((u1NameLen % PTP_ALTS_NAME_PAD_CONST_DIV) != 0)
                {
                    /* Pad one byte */
                    u2TlvLen = (UINT2)
                        (u1NameLen + PTP_ALT_TIME_NON_VAR_LEN + 1);
                }
                else
                {
                    u2TlvLen = (UINT2) (u1NameLen + PTP_ALT_TIME_NON_VAR_LEN);
                }

                u2TotLen = (UINT2) (u2TotLen + u2TlvLen +
                                    PTP_TLV_DATA_FIELD_OFFSET);

                PTP_LBUF_PUT_2_BYTES (pu1TmpATSTlv, u2TlvLen);
                PTP_LBUF_PUT_1_BYTE (pu1TmpATSTlv, pATSEntry->u1KeyId);
                PTP_LBUF_PUT_4_BYTES (pu1TmpATSTlv, pATSEntry->u4CurrentOffset);
                PTP_LBUF_PUT_4_BYTES (pu1TmpATSTlv, pATSEntry->u4JumpSeconds);

                u2Val = (UINT2) pATSEntry->u8TimeOfNextJump.u4Hi;
                PTP_LBUF_PUT_2_BYTES (pu1TmpATSTlv, u2Val);

                PTP_LBUF_PUT_4_BYTES (pu1TmpATSTlv,
                                      (pATSEntry->u8TimeOfNextJump.u4Lo));
                PTP_LBUF_PUT_1_BYTE (pu1TmpATSTlv, u1NameLen);
                PTP_LBUF_PUT_N_BYTES (pu1TmpATSTlv, pATSEntry->au1DisplayName,
                                      u1NameLen);
                if ((u1NameLen % PTP_ALTS_NAME_PAD_CONST_DIV) != 0)
                {
                    PTP_LBUF_PUT_1_BYTE (pu1TmpATSTlv, u1Pad);
                }

                u1EntCount++;

                if (PTP_MAX_ALT_TIME_PER_DOMAIN == u1EntCount)
                {
                    /* Max AlternateTimescale per domain is reached */
                    break;
                }
            }

        }

        ATSEntry.u4ContextId = pATSEntry->u4ContextId;
        ATSEntry.u1DomainId = pATSEntry->u1DomainId;
        ATSEntry.u1KeyId = pATSEntry->u1KeyId;
    }
    while ((pATSEntry = PtpDbGetNextNode (gPtpGlobalInfo.AltTimSclTree,
                                          &(ATSEntry),
                                          (UINT1) PTP_ALT_TIME_DATA_SET)) !=
           NULL);
    /* Update the Tlv in the Domain specific structure */
    i4Status = PtpAlTsUpdateTlv (u4ContextId, u1DomainId, pu1ATSTlv, u2TotLen);

    if ((MemBuddyFree ((UINT1) gPtpGlobalInfo.i4AltTimeBuddyId, pu1ATSTlv) ==
         BUDDY_FAILURE))
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpAlTsHandleUpdate : MemBuddyFree Failed. \r\n"));

        PTP_FN_EXIT ();

        return (OSIX_FAILURE);
    }
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpAlTsHandleUpdate: PtpAlTsUpdateTlv Failed. \r\n"));
    }

    PTP_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpAlTsUpdateTlv                                           */
/*                                                                           */
/* Description  : This function is used to update the all valid timescale    */
/*                tlvs for the context,Domain. Update the total length of    */
/*                all tlvs                                                   */
/*                                                                           */
/* Input        : u4ContextId - Context Identifier.                          */
/*              : u1DomainId  - Domain Identifier.                           */
/*              : pu1ATSclTlv - All Valid Alternate timescale tlvs           */
/*              : u2TotTlvLen - Total Length of the all tlvs                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/ OSIX_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAlTsUpdateTlv (UINT4 u4ContextId, UINT1 u1DomainId, UINT1 *pu1ATSclTlv,
                  UINT2 u2TotTlvLen)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    pPtpDomain = PtpDmnGetDomainEntry (u4ContextId, u1DomainId);
    if (pPtpDomain != NULL)
    {
        if (pPtpDomain->pu1AltTimeSclTLV != NULL)
        {
            /* Free the Memory */
            i4Status = MemBuddyFree ((UINT1) gPtpGlobalInfo.i4AltTimeBuddyId,
                                     pPtpDomain->pu1AltTimeSclTLV);
            if (i4Status == BUDDY_FAILURE)
            {
                PTP_TRC ((u4ContextId, u1DomainId,
                          PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                          "PtpAlTsUpdateTlv : Alternate Timescale TLV BuddyFree"
                          " Failed. \r\n"));

                PTP_FN_EXIT ();

                return (OSIX_FAILURE);
            }
            pPtpDomain->pu1AltTimeSclTLV = NULL;
            pPtpDomain->u2AltTimeTotLen = 0;
        }

        if (u2TotTlvLen == 0)
        {
            /* All Entries are disabled */
            PTP_TRC ((u4ContextId, u1DomainId, CONTROL_PLANE_TRC,
                      "PtpAlTsUpdateTlv : All Entries are disabled.\r\n"));

            PTP_FN_EXIT ();

            return (OSIX_SUCCESS);
        }

        pPtpDomain->pu1AltTimeSclTLV =
            MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4AltTimeBuddyId,
                           u2TotTlvLen);
        if (pPtpDomain->pu1AltTimeSclTLV != NULL)
        {
            MEMSET (pPtpDomain->pu1AltTimeSclTLV, 0, sizeof (u2TotTlvLen));
            MEMCPY (pPtpDomain->pu1AltTimeSclTLV, pu1ATSclTlv, u2TotTlvLen);
            pPtpDomain->u2AltTimeTotLen = u2TotTlvLen;

            PTP_FN_EXIT ();

            return (OSIX_SUCCESS);
        }
        else
        {
            PTP_TRC ((u4ContextId, u1DomainId,
                      PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpAlTsUpdateTlv: Alternate TimescaleTLV MemBuddyAlloc "
                      "Failed.\r\n"));
            PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        }
    }

    PTP_FN_EXIT ();
    PTP_TRC ((u4ContextId, u1DomainId, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
              "PtpAlTsUpdateTlv : Invalid domain.\r\n"));

    return (OSIX_FAILURE);
}

/*****************************************************************************/
/* Function     : PtpAlTsAddAltTimeTLV                                       */
/*                                                                           */
/* Description  : This function is called from the announce message handler  */
/*                to add the Alternate Timescale TLV on transmitting         */
/*                announce message. Update the header of the announce message*/
/*                                                                           */
/* Input        : pPtpDomain  - Domain Pointer                               */
/*              : pu1PtpPdu   - Pointer to the Announce message PDU.         */
/*              : pu4MsgLen   - Length of the PDU size.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/ OSIX_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAlTsAddAltTimeTLV (tPtpDomain * pPtpDomain, UINT1 *pu1PtpPdu,
                      UINT4 *pu4MsgLen)
{
    UINT1              *pu1TmpPtpPdu = NULL;
    UINT2               u2TotTlvLen = 0;

    PTP_FN_ENTRY ();

    pu1TmpPtpPdu = pu1PtpPdu + (*pu4MsgLen);

    if (pPtpDomain->pu1AltTimeSclTLV != NULL)
    {
        /* Get the tlvs total length */
        u2TotTlvLen = pPtpDomain->u2AltTimeTotLen;
        /* Add the tlvs into the PtpPdu */
        PTP_LBUF_PUT_N_BYTES (pu1TmpPtpPdu, pPtpDomain->pu1AltTimeSclTLV,
                              u2TotTlvLen);
        *pu4MsgLen += u2TotTlvLen;
        PTP_TRC ((pPtpDomain->u4ContextId, pPtpDomain->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "PtpAlTsAddAltTimeTLV : Alternate Timescale "
                  "TLV Added.\r\n"));
    }

    PTP_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpAlTsFindATSTlvs                                         */
/*                                                                           */
/* Description  : This function is to collect all alternate timescale tlvs   */
/*                from the announce message                                  */
/*                                                                           */
/* Input        : pu1PtpPdu   - PTP Message                                  */
/*              : u4MsgLen    - PTP Message Length                           */
/*              : pu1ATSclTlv - Pointer to the TLV                           */
/*              : pu2TotTlvLen - Length of the TLV                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/ OSIX_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAlTsFindATSTlvs (UINT1 *pu1PtpPdu, UINT4 u4MsgLen, UINT1 *pu1ATSclTlv,
                    UINT2 *pu2TotTlvLen)
{
    UINT1              *pu1Tlv = NULL;
    INT4                i4Status = 0;
    UINT2               u2TlvLen = 0;
    UINT2               u2OffSet = 0;

    PTP_FN_ENTRY ();

    i4Status = PtpUtilFindTlv (pu1PtpPdu, (UINT2) u4MsgLen, &u2OffSet,
                               (UINT2) PTP_TLV_TYPE_ALT_TIME_OFFSET_IND,
                               &pu1Tlv, &u2TlvLen);

    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpAlTsFindATSTlvs : PtpUtilFindTlv Failed.\r\n"));

        PTP_FN_EXIT ();

        return (OSIX_FAILURE);
    }

    do
    {
        u2TlvLen += PTP_TLV_DATA_FIELD_OFFSET;
        MEMCPY (pu1ATSclTlv, pu1Tlv, u2TlvLen);
        pu1ATSclTlv += u2TlvLen;
        *pu2TotTlvLen += u2TlvLen;
        pu1PtpPdu = pu1Tlv;
    }
    while ((PtpUtilFindTlv (pu1PtpPdu, (UINT2) u4MsgLen, &u2OffSet,
                            (UINT2) PTP_TLV_TYPE_ALT_TIME_OFFSET_IND,
                            &pu1Tlv, &u2TlvLen)) == OSIX_SUCCESS);

    PTP_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function                  : PtpAlTsCreate                                 */
/*                                                                           */
/* Description               : This routine creates the alternate time       */
/*                             scale data set                                */
/*                                                                           */
/* Input                     : u4ContextId   -  Context identifier           */
/*                             u1DomainId    -  Domain identifier            */
/*                             u1PtpAlTsKeyId -  KeyId                       */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.AltTimSclPoolId                */
/*                             gPtpGlobalInfo.AltTimSclTree                  */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpAltTimescaleEntry *
PtpAlTsCreate (UINT4 u4ContextId, UINT1 u1DomainId, UINT1 u1PtpAlTsKeyId)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpDomain         *pPtpDomain = NULL;

    PTP_FN_ENTRY ();

    /* Check whether the domain exists */
    pPtpDomain = PtpDmnGetDomainEntry (u4ContextId, u1DomainId);

    if (pPtpDomain == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Domain %d does not exists\r\n"));

        PTP_FN_EXIT ();

        return NULL;
    }

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        MemAllocMemBlk (gPtpGlobalInfo.AltTimSclPoolId);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Memory Allocation failed " "for Alternate time scale\r\n"));

        PTP_FN_EXIT ();

        return NULL;
    }

    MEMSET (pPtpAltTimescaleEntry, 0, sizeof (tPtpAltTimescaleEntry));

    pPtpAltTimescaleEntry->u4ContextId = u4ContextId;
    pPtpAltTimescaleEntry->u1DomainId = u1DomainId;
    pPtpAltTimescaleEntry->u1KeyId = u1PtpAlTsKeyId;

    if (PtpDbAddNode (gPtpGlobalInfo.AltTimSclTree, pPtpAltTimescaleEntry,
                      (UINT1) PTP_ALT_TIME_DATA_SET) == OSIX_FAILURE)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Unable to add alternate time scale node "
                  "into data structure\r\n"));

        MemReleaseMemBlock (gPtpGlobalInfo.AltTimSclPoolId,
                            (UINT1 *) pPtpAltTimescaleEntry);
        pPtpAltTimescaleEntry = NULL;

        PTP_FN_EXIT ();

        return NULL;
    }

    PTP_TRC ((u4ContextId, u1DomainId,
              INIT_SHUT_TRC | MGMT_TRC,
              "Alternate time scale is created successfully\r\n"));

    PTP_FN_EXIT ();

    return pPtpAltTimescaleEntry;
}

/*****************************************************************************/
/* Function                  : PtpAlTsDelete                                 */
/*                                                                           */
/* Description               : This routine deletes the alternate time       */
/*                             scale data set                                */
/*                                                                           */
/* Input                     : pPtpAltTimescaleEntry - pointer to node       */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.AltTimSclPoolId                */
/*                             gPtpGlobalInfo.AltTimSclTree                  */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpAlTsDelete (tPtpAltTimescaleEntry * pPtpAltTimescaleEntry)
{
    UINT4               u4ContextId = 0;
    UINT1               u1DomainId = 0;

    PTP_FN_ENTRY ();

    u4ContextId = pPtpAltTimescaleEntry->u4ContextId;
    u1DomainId = pPtpAltTimescaleEntry->u1DomainId;

    PtpDbDeleteNode (gPtpGlobalInfo.AltTimSclTree, pPtpAltTimescaleEntry,
                     (UINT1) PTP_ALT_TIME_DATA_SET);

    MemReleaseMemBlock (gPtpGlobalInfo.AltTimSclPoolId,
                        (UINT1 *) pPtpAltTimescaleEntry);
    pPtpAltTimescaleEntry = NULL;

    PTP_TRC ((u4ContextId, u1DomainId, INIT_SHUT_TRC | MGMT_TRC,
              "Alternate time scale is deleted successfully\r\n"));

    PTP_FN_EXIT ();

}

/*****************************************************************************/
/* Function                  : PtpAlTsDeleteAlTsEntriesInDomain              */
/*                                                                           */
/* Description               : This routine scans the alternate time scale   */
/*                             entries  and delete the entries in this port  */
/*                                                                           */
/* Input                     : pPtpDomain - Domain whose value of alternate  */
/*                                         time scale needs to be deleted    */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpAlTsDeleteAlTsEntriesInDomain (tPtpDomain * pPtpDomain)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry PtpAltTimescaleEntry;

    PTP_FN_ENTRY ();
    MEMSET (&PtpAltTimescaleEntry, 0, sizeof (tPtpAltTimescaleEntry));

    PtpAltTimescaleEntry.u4ContextId = pPtpDomain->u4ContextId;
    PtpAltTimescaleEntry.u1DomainId = pPtpDomain->u1DomainId;

    while (((pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
             PtpDbGetNextNode (gPtpGlobalInfo.AltTimSclTree,
                               &PtpAltTimescaleEntry,
                               (UINT1) PTP_ALT_TIME_DATA_SET))
            != NULL) &&
           (pPtpDomain->u4ContextId == pPtpAltTimescaleEntry->u4ContextId) &&
           (pPtpDomain->u1DomainId == pPtpAltTimescaleEntry->u1DomainId))
    {
        PtpAltTimescaleEntry.u1KeyId = pPtpAltTimescaleEntry->u1KeyId;
        PtpAlTsDelete (pPtpAltTimescaleEntry);
    }

    PTP_FN_EXIT ();
}
#endif /*  _PTPALTS_C_ */

/***************************************************************************
 *                         END OF FILE ptpalts.c                           *
 ***************************************************************************/
