/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpcmutl.c,v 1.4 2014/01/24 12:20:08 siva Exp $
 *
 * Description: This file contains PTP Compatability Sub module's
 *              utility functionality routines.
 *********************************************************************/
#ifndef _PTPCMUTL_C_
#define _PTPCMUTL_C_

#include "ptpincs.h"

PUBLIC              BOOL1
PtpCmUtlCheckStratumClkId (UINT1 u1v1Stratum, INT1 *pu1v1ClkId);

/*****************************************************************************/
/* Function     : PtpCmUtlv1SubDomainTov2Domain                              */
/*                                                                           */
/* Description  : This function used to map the v1 subdomain to v2 domain id */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : pu1v1SubDomain - V1 Message SubDomain Value                */
/*              : pu1v2DomainId  - V2 Domain Identifier.                     */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1v2DomainId  - V2 Domain Identifier.                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv1SubDomainTov2Domain (INT1 *pi1v1SubDomain, UINT1 *pu1v2DomainId)
{
    PTP_FN_ENTRY ();

    if ((MEMCMP (pi1v1SubDomain, DEFAULT_PTP_DOMAIN_NAME,
                 PTP_SUBDOMAIN_NAME_LENGTH)) == 0)
    {
        *pu1v2DomainId = PTP_V2_DEFAULT_DOMAIN_ID;
    }
    else if ((MEMCMP (pi1v1SubDomain, ALTERNATE_PTP_DOMAIN1_NAME,
                      PTP_SUBDOMAIN_NAME_LENGTH)) == 0)
    {
        *pu1v2DomainId = PTP_V2_ALTERNATE_DOMAIN1_ID;
    }
    else if ((MEMCMP (pi1v1SubDomain, ALTERNATE_PTP_DOMAIN2_NAME,
                      PTP_SUBDOMAIN_NAME_LENGTH)) == 0)
    {
        *pu1v2DomainId = PTP_V2_ALTERNATE_DOMAIN2_ID;
    }
    else if ((MEMCMP (pi1v1SubDomain, ALTERNATE_PTP_DOMAIN3_NAME,
                      PTP_SUBDOMAIN_NAME_LENGTH)) == 0)
    {
        *pu1v2DomainId = PTP_V2_ALTERNATE_DOMAIN3_ID;
    }
    else
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpCmUtlv1SubDomainTov2Domain : Mapping Failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv2DomainTov1SubDomain                              */
/*                                                                           */
/* Description  : This function used to map the v2 domain id to v1 subdomain */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : u1v2DomainId   - V2 Domain Identifier.                     */
/*              : pi1v1SubDomain - V1 Message SubDomain Value                */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pi1v1SubDomain - V1 Message SubDomain Value                */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv2DomainTov1SubDomain (UINT1 u1v2DomainId, INT1 *pi1v1SubDomain)
{
    PTP_FN_ENTRY ();

    switch (u1v2DomainId)
    {
        case PTP_V2_DEFAULT_DOMAIN_ID:
            MEMCPY (pi1v1SubDomain, DEFAULT_PTP_DOMAIN_NAME,
                    PTP_SUBDOMAIN_NAME_LENGTH);
            break;

        case PTP_V2_ALTERNATE_DOMAIN1_ID:
            MEMCPY (pi1v1SubDomain, ALTERNATE_PTP_DOMAIN1_NAME,
                    PTP_SUBDOMAIN_NAME_LENGTH);
            break;

        case PTP_V2_ALTERNATE_DOMAIN2_ID:
            MEMCPY (pi1v1SubDomain, ALTERNATE_PTP_DOMAIN2_NAME,
                    PTP_SUBDOMAIN_NAME_LENGTH);
            break;

        case PTP_V2_ALTERNATE_DOMAIN3_ID:
            MEMCPY (pi1v1SubDomain, ALTERNATE_PTP_DOMAIN3_NAME,
                    PTP_SUBDOMAIN_NAME_LENGTH);
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, u1v2DomainId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "PtpCmUtlv2DomainTov1SubDomain: Invalid Sub Domain.\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv1StratumTov2ClkCls                                */
/*                                                                           */
/* Description  : This function used to map the v1 Stratum to v2 Clock Class */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : u1v1Stratum    - V1 Stratum Value.                         */
/*              : pu1v2ClkCls    - V2 Class Value                            */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1v2ClkCls    - V2 Class Value                            */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv1StratumTov2ClkCls (UINT1 u1v1Stratum, UINT1 *pu1v2ClkCls)
{
    PTP_FN_ENTRY ();

    switch (u1v1Stratum)
    {
        case PTP_V1_STRATUM_VAL_0:
            *pu1v2ClkCls = PTP_V2_CLK_CLASS_VAL_6;
            break;

        case PTP_V1_STRATUM_VAL_1:
            *pu1v2ClkCls = PTP_V2_CLK_CLASS_VAL_9;
            break;

        case PTP_V1_STRATUM_VAL_2:
            *pu1v2ClkCls = PTP_V2_CLK_CLASS_VAL_10;
            break;

        case PTP_V1_STRATUM_VAL_3:
            *pu1v2ClkCls = PTP_V2_CLK_CLASS_VAL_248;
            break;

        case PTP_V1_STRATUM_VAL_4:
            *pu1v2ClkCls = PTP_V2_CLK_CLASS_VAL_251;
            break;

        case PTP_V1_STRATUM_VAL_255:
            *pu1v2ClkCls = PTP_V2_CLK_CLASS_VAL_255;
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "PtpCmUtlv1StratumTov2ClkCls: Invalid V2 Clock Class.\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv2ClkClsTov1Stratum                                */
/*                                                                           */
/* Description  : This function used to map the v2 Clock Class to v1 Stratum */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : u1v2ClkCls     - V2 Clock Class Value.                     */
/*              : pu1v1Stratum   - V1 Stratum Value                          */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1v1Stratum   - V1 Stratum Value                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv2ClkClsTov1Stratum (UINT1 u1v2ClkCls, UINT1 *pu1v1Stratum)
{
    PTP_FN_ENTRY ();

    switch (u1v2ClkCls)
    {
        case PTP_V2_CLK_CLASS_VAL_6:
        case PTP_V2_CLK_CLASS_VAL_7:
            *pu1v1Stratum = PTP_V1_STRATUM_VAL_0;
            break;

        case PTP_V2_CLK_CLASS_VAL_9:
            *pu1v1Stratum = PTP_V1_STRATUM_VAL_1;
            break;

        case PTP_V2_CLK_CLASS_VAL_10:
            *pu1v1Stratum = PTP_V1_STRATUM_VAL_2;
            break;

        case PTP_V2_CLK_CLASS_VAL_251:
            *pu1v1Stratum = PTP_V1_STRATUM_VAL_4;
            break;

        case PTP_V2_CLK_CLASS_VAL_255:
            *pu1v1Stratum = PTP_V1_STRATUM_VAL_255;
            break;

        default:
            if ((u1v2ClkCls > PTP_V2_CLK_CLASS_VAL_12) &&
                (u1v2ClkCls < PTP_V2_CLK_CLASS_VAL_249))
            {
                *pu1v1Stratum = PTP_V1_STRATUM_VAL_3;
                PTP_FN_EXIT ();
                return (OSIX_SUCCESS);
            }
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "PtpCmUtlv2ClkClsTov1Stratum: Invalid V1 Clock Class.\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv2Pri1Tov1PrefClkStratum                           */
/*                                                                           */
/* Description  : This function used to map the v2 Priority1 to  v1          */
/*                grandmasterIsPreferred and grandmasterClockStratum         */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : u1v2ClkCls     - V2 Clock Class Value.                     */
/*              : pu1v1Stratum   - V1 Stratum Value                          */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1v1Stratum   - V1 Stratum Value                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv2Pri1Tov1PrefClkStratum (UINT1 u1v2Pri1, UINT1 u1v2ClkCls,
                                  BOOL1 * pb1v1Pref, UINT1 *pu1v1ClkStratum)
{
    INT4                i4Status = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    if (u1v2Pri1 < PTP_V2_PRIORITY1_VAL_127)
    {
        *pb1v1Pref = PTP_V1_GM_IS_PREFERRED_TRUE;
        *pu1v1ClkStratum = PTP_V1_STRATUM_VAL_0;
    }
    else if (u1v2Pri1 == PTP_V2_PRIORITY1_VAL_127)
    {
        *pb1v1Pref = PTP_V1_GM_IS_PREFERRED_TRUE;
        i4Status = PtpCmUtlv2ClkClsTov1Stratum (u1v2ClkCls, pu1v1ClkStratum);
    }
    else if (u1v2Pri1 == PTP_V2_PRIORITY1_VAL_128)
    {
        *pb1v1Pref = PTP_V1_GM_IS_PREFERRED_FALSE;
        i4Status = PtpCmUtlv2ClkClsTov1Stratum (u1v2ClkCls, pu1v1ClkStratum);
    }
    else
    {
        *pb1v1Pref = PTP_V1_GM_IS_PREFERRED_FALSE;
        *pu1v1ClkStratum = PTP_V1_STRATUM_VAL_255;
    }
    PTP_FN_EXIT ();
    return (i4Status);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv1ClkIdTov2ClkAcTimSrc                             */
/*                                                                           */
/* Description  : This function used to map the v1 Clock Id  to  v2 Clock    */
/*                Accuracy and timeSource                                    */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : pi1v1ClkId       - V1 Clock Id                             */
/*              : pu1v2ClkAccuracy - V2 Clock Accuracy                       */
/*              : pu1v2TimSrc      - V2 Time Source                          */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1v2ClkAccuracy - V2 Clock Accuracy                       */
/*                pu1v2TimSrc      - V2 Time Source                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv1ClkIdTov2ClkAcTimSrc (INT1 *pi1v1ClkId, UINT1 *pu1v2ClkAccuracy,
                                UINT1 *pu1v2TimSrc)
{
    PTP_FN_ENTRY ();

    if ((MEMCMP (pi1v1ClkId, PTP_V1_CLK_ID_ATOM, PTP_V1_CLK_ID_LEN)) == 0)
    {
        *pu1v2ClkAccuracy = PTP_V2_CLK_ACCURACY_HEX_VAL_22;
        *pu1v2TimSrc = PTP_TIME_SRC_ATOMIC_CLOCK;
    }
    else if ((MEMCMP (pi1v1ClkId, PTP_V1_CLK_ID_GPS, PTP_V1_CLK_ID_LEN)) == 0)
    {
        *pu1v2ClkAccuracy = PTP_V2_CLK_ACCURACY_HEX_VAL_22;
        *pu1v2TimSrc = PTP_TIME_SRC_GPS;
    }
    else if ((MEMCMP (pi1v1ClkId, PTP_V1_CLK_ID_NTP, PTP_V1_CLK_ID_LEN)) == 0)
    {
        *pu1v2ClkAccuracy = PTP_V2_CLK_ACCURACY_HEX_VAL_2F;
        *pu1v2TimSrc = PTP_TIME_SRC_NTP;
    }
    else if ((MEMCMP (pi1v1ClkId, PTP_V1_CLK_ID_HAND, PTP_V1_CLK_ID_LEN)) == 0)
    {
        *pu1v2ClkAccuracy = PTP_V2_CLK_ACCURACY_HEX_VAL_30;
        *pu1v2TimSrc = PTP_TIME_SRC_HAND_SET;
    }
    else if ((MEMCMP (pi1v1ClkId, PTP_V1_CLK_ID_INIT, PTP_V1_CLK_ID_LEN)) == 0)
    {
        *pu1v2ClkAccuracy = PTP_V2_CLK_ACCURACY_HEX_VAL_FD;
        *pu1v2TimSrc = PTP_TIME_SRC_OTHER;
    }
    else if ((MEMCMP (pi1v1ClkId, PTP_V1_CLK_ID_DFLT, PTP_V1_CLK_ID_LEN)) == 0)
    {
        *pu1v2ClkAccuracy = PTP_V2_CLK_ACCURACY_HEX_VAL_FE;
        *pu1v2TimSrc = PTP_TIME_SRC_INTERNAL_OSCILLATOR;
    }
    else
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpCmUtlv1ClkIdTov2ClkAcTimSrc: Could Not Map Clock Accuracy V1 to V2.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv2ClkAccuracyTov1ClkId                             */
/*                                                                           */
/* Description  : This function used to map the v2 Clock Accuracy to v1 Clock*/
/*                Identifier                                                 */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : u1v2ClkAccuracy  - V2 Clock Accuracy                       */
/*              : pi1v1ClkId       - V1 Clock Identifier                     */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pi1v1ClkId       - V1 Clock Identifier                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv2ClkAccuracyTov1ClkId (UINT1 u1v2ClkAccuracy, INT1 *pi1v1ClkId)
{
    PTP_FN_ENTRY ();

    MEMSET (pi1v1ClkId, 0, PTP_V1_CLK_ID_LEN);

    if ((u1v2ClkAccuracy > PTP_V2_CLK_ACCURACY_HEX_VAL_19) &&
        (u1v2ClkAccuracy < PTP_V2_CLK_ACCURACY_HEX_VAL_23))
    {
        MEMCPY (pi1v1ClkId, PTP_V1_CLK_ID_ATOM, PTP_V1_CLK_ID_LEN);
    }
    else if ((u1v2ClkAccuracy > PTP_V2_CLK_ACCURACY_HEX_VAL_22) &&
             (u1v2ClkAccuracy < PTP_V2_CLK_ACCURACY_HEX_VAL_30))
    {
        MEMCPY (pi1v1ClkId, PTP_V1_CLK_ID_NTP, PTP_V1_CLK_ID_LEN);
    }
    else if (u1v2ClkAccuracy == PTP_V2_CLK_ACCURACY_HEX_VAL_30)
    {
        MEMCPY (pi1v1ClkId, PTP_V1_CLK_ID_HAND, PTP_V1_CLK_ID_LEN);
    }
    else if ((u1v2ClkAccuracy > PTP_V2_CLK_ACCURACY_HEX_VAL_30) &&
             (u1v2ClkAccuracy < PTP_V2_CLK_ACCURACY_HEX_VAL_FE))
    {
        MEMCPY (pi1v1ClkId, PTP_V1_CLK_ID_INIT, PTP_V1_CLK_ID_LEN);
    }
    else if (u1v2ClkAccuracy == PTP_V2_CLK_ACCURACY_HEX_VAL_FE)
    {
        MEMCPY (pi1v1ClkId, PTP_V1_CLK_ID_DFLT, PTP_V1_CLK_ID_LEN);
    }
    else
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpCmUtlv2ClkAccuracyTov1ClkId: Could Not Map Clock Accuracy V2 to V1.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv1ContMsgTypeTov2MsgType                           */
/*                                                                           */
/* Description  : This function used to map the v1 Control and Message Type  */
/*                to V2 message Type                                         */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : u1v1Cont         - V1 Control Field value                  */
/*              : u1v1MsgType      - V1 Message Type value                   */
/*              : pu1v2MsgType     - V2 Message Type value                   */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1v2MsgType     - V2 Message Type value                   */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv1ContMsgTypeTov2MsgType (UINT1 u1v1Cont, UINT1 u1v1MsgType,
                                  UINT1 *pu1v2MsgType)
{
    PTP_FN_ENTRY ();

    if ((u1v1MsgType == PTP_V1_MSG_TYPE_EVENT_MSG) &&
        (u1v1Cont == PTP_V1_SYNC_MESSAGE))
    {
        *pu1v2MsgType = PTP_SYNC_MSG;
    }
    else if ((u1v1MsgType == PTP_V1_MSG_TYPE_EVENT_MSG) &&
             (u1v1Cont == PTP_V1_DELAY_REQ_MESSAGE))
    {
        *pu1v2MsgType = PTP_DELAY_REQ_MSG;
    }
    else if ((u1v1MsgType == PTP_V1_MSG_TYPE_GENERAL_MSG) &&
             (u1v1Cont == PTP_V1_FOLLOWUP_MESSAGE))
    {
        *pu1v2MsgType = PTP_FOLLOW_UP_MSG;
    }
    else if ((u1v1MsgType == PTP_V1_MSG_TYPE_GENERAL_MSG) &&
             (u1v1Cont == PTP_V1_DELAY_RESP_MESSAGE))
    {
        *pu1v2MsgType = PTP_DELAY_RESP_MSG;
    }
    else
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpCmUtlv1ContMsgTypeTov2MsgType: Invalid Message Type.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv2MsgTypeTov1ContMsgType                           */
/*                                                                           */
/* Description  : This function used to map the v2 Message Type to v1 Control*/
/*                Type and v1 Message Type                                   */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : u1v2MsgType     - V2 Message Type value                    */
/*              : pu1v2Cont       - Pointer to V1 Control Type value         */
/*              : pu1v1MsgType    - Pointer to V1 Message Type value         */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1v2Cont       - V1 Control Type value                    */
/*                pu1v1MsgType    - V1 Message Type value                    */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv2MsgTypeTov1ContMsgType (UINT1 u1v2MsgType, UINT1 *pu1v2Cont,
                                  UINT1 *pu1v1MsgType)
{
    PTP_FN_ENTRY ();

    switch (u1v2MsgType)
    {
        case PTP_SYNC_MSG:
            *pu1v2Cont = PTP_V1_SYNC_MESSAGE;
            *pu1v1MsgType = PTP_V1_MSG_TYPE_EVENT_MSG;
            break;

        case PTP_DELAY_REQ_MSG:
            *pu1v2Cont = PTP_V1_DELAY_REQ_MESSAGE;
            *pu1v1MsgType = PTP_V1_MSG_TYPE_EVENT_MSG;
            break;

        case PTP_FOLLOW_UP_MSG:
            *pu1v2Cont = PTP_V1_FOLLOWUP_MESSAGE;
            *pu1v1MsgType = PTP_V1_MSG_TYPE_GENERAL_MSG;
            break;

        case PTP_DELAY_RESP_MSG:
            *pu1v2Cont = PTP_V1_DELAY_RESP_MESSAGE;
            *pu1v1MsgType = PTP_V1_MSG_TYPE_GENERAL_MSG;
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "PtpCmUtlv2MsgTypeTov1ContMsgType: Invalid Message Type.\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv1SrcPortIdTov2SrcPortId                           */
/*                                                                           */
/* Description  : This function used to map the v1 Source Port Id to         */
/*                V2 Source Port Identifier.                                 */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : u1v1SrcComTech  - V1 Source Communication Technology       */
/*              : pu1v1SrcUuid    - V1 Source Clock UUID                     */
/*              : u2v1SrcPortId   - V1 Source Port Id                        */
/*              : pu1v2ClkId      - Pointer to v2 Clock Id                   */
/*              : pu2v2PortId     - Pointer to v2 Port Id                    */
/*                                                                           */
/* Output       : On Success                                                 */
/*              : pu1v2ClkId      - v2 Clock Id                              */
/*              : pu2v2PortId     - v2 Port Id                               */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv1SrcPortIdTov2SrcPortId (UINT1 u1v1SrcComTech, INT1 *pi1v1SrcUuid,
                                  UINT2 u2v1SrcPortId, tClkId * pClkId,
                                  UINT2 *pu2v2PortId)
{
    INT1               *pi1v2ClkId = NULL;

    PTP_FN_ENTRY ();

    pi1v2ClkId = (INT1 *) (pClkId);

    if (u1v1SrcComTech != PTP_V1_COMMUNICATION_TECHNOLOGY)
    {
        PTP_FN_EXIT ();
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpCmUtlv1SrcPortIdTov2SrcPortId: Invalid Communication Technology.\r\n"));
        return (OSIX_FAILURE);
    }
    /* Set the First 0,1,2 Octets with pi1v1SrcUuid */
    MEMCPY (pi1v2ClkId, pi1v1SrcUuid, (PTP_THREE_BYTE));
    pi1v2ClkId += PTP_THREE_BYTE;
    /* Set the 0xFF and 0xFE */
    *pi1v2ClkId = (INT1) PTP_CLK_ID_MAP_OCTET_THREE;
    pi1v2ClkId++;
    *pi1v2ClkId = (INT1) PTP_CLK_ID_MAP_OCTET_FOUR;
    pi1v2ClkId++;
    /* Set the remaining 3,4,5 Octets with pi1v1SrcUuid */
    MEMCPY (pi1v2ClkId, (pi1v1SrcUuid + PTP_THREE_BYTE), (PTP_THREE_BYTE));
    *pu2v2PortId = u2v1SrcPortId;
    PTP_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv2SrcPortIdTov1SrcPortId                           */
/*                                                                           */
/* Description  : This function to map the v2 Source Port Id to V1 Source    */
/*                Port Identifier.                                           */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : pi1v2ClkId      - V2 Source Clock Id                       */
/*              : u2v2PortId      - V2 Source Port Id                        */
/*              : pu1v1SrcComTech - Pointer to v1 Source Communication       */
/*              :                   Technology                               */
/*              : pi1v1SrcUuid    - Pointer to v1 Clock UUID                 */
/*              : pu2v1SrcPortId  - Pointer to v1 Port Id                    */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpCmUtlv2SrcPortIdTov1SrcPortId (tClkId * pClkId, UINT2 u2v2PortId,
                                  UINT1 *pu1v1SrcComTech, INT1 *pi1v1SrcUuid,
                                  UINT2 *pu2v1SrcPortId)
{
    INT1               *pi1v2ClkId = (INT1 *) pClkId;

    PTP_FN_ENTRY ();

    /* Set the Source Communication Technology  */
    *pu1v1SrcComTech = PTP_V1_COMMUNICATION_TECHNOLOGY;

    /* Cpory the least 0-2 and 5-7 Octets of pi1v2ClkI to pi1v1SrcUuid */
    MEMCPY (pi1v1SrcUuid, &(pi1v2ClkId[0]), PTP_THREE_BYTE);
    MEMCPY ((pi1v1SrcUuid + PTP_THREE_BYTE), &(pi1v2ClkId[PTP_FIFTH_BYTE]),
            PTP_THREE_BYTE);
    *pu2v1SrcPortId = u2v2PortId;

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpCmUtlCheckStratumClkId                                  */
/*                                                                           */
/* Description  : This function retrun TRUE if stratum = 1 or 2 and clock    */
/*                identifier is not INIT or DFLT. Otherwise FALSE            */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : pu1v1Stratum    - V1 Stratum value                         */
/*              : pu1v1ClkId      - V1 Clock Identifier                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC              BOOL1
PtpCmUtlCheckStratumClkId (UINT1 u1v1Stratum, INT1 *pu1v1ClkId)
{
    PTP_FN_ENTRY ();

    if ((u1v1Stratum == PTP_V1_STRATUM_VAL_1) ||
        (u1v1Stratum == PTP_V1_STRATUM_VAL_2))
    {
        if (!(((MEMCMP (pu1v1ClkId, PTP_V1_CLK_ID_INIT, PTP_V1_CLK_ID_LEN)) ==
               0) ||
              ((MEMCMP (pu1v1ClkId, PTP_V1_CLK_ID_DFLT, PTP_V1_CLK_ID_LEN)) ==
               0)))
        {
            PTP_FN_EXIT ();
            return (OSIX_TRUE);
        }
    }
    PTP_FN_EXIT ();
    return (OSIX_FALSE);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv1FlagTov2Flag                                     */
/*                                                                           */
/* Description  : This function used to map the v1 Flag Fields to v2         */
/*                Flag Fields                                                */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : pu1v1Flag       - v1 Flag Fields                           */
/*              : pu1v1Stratum    - v1 Stratum value                         */
/*              : pi1v1ClkId      - v1 Clock Identifier.                     */
/*              : pu1v2Flag       - Pointer to v2 Flag Fields                */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1v2Flag       - v2 Flag Fields                           */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpCmUtlv1FlagTov2Flag (INT1 *pi1v1Flag, UINT1 u1v1Stratum,
                        INT1 *pi1v1ClkId, INT1 *pi1v2Flag)
{
    INT1                i1v1Flag = OSIX_FALSE;
    BOOL1               bFlag = OSIX_FALSE;

    PTP_FN_ENTRY ();

    /* Map the following flags and reset remaining flags 
       v2f[0][1] = twoStepFlag;             - v1f[1][3]
       v2f[1][0] = leap61;                  - v1f[1][0]
       v2f[1][1] = leap59;                  - v1f[1][1]
       v2f[1][2] = currentUtcOffsetValid;   - from PtpCmUtlCheckStratumClkId;
       v2f[1][3] = ptpTimescale;            - ClkId (!INIT || ! DEFL)
       v2f[1][4] = timeTraceable;           - from PtpCmUtlCheckStratumClkId;
       v2f[1][5] = frequencyTraceable;      - from PtpCmUtlCheckStratumClkId;
     */

    MEMSET (pi1v2Flag, 0, PTP_V2_FLAG_FIELD_LEN);

    /* The V1 Flags Bits are in pi1v1Flag[1] */
    pi1v1Flag++;
    i1v1Flag = *pi1v1Flag;

    /* Octct[0] Flags */

    if (i1v1Flag & (1 << PTP_V1_FLAG_ASSIST))
    {
        *pi1v2Flag |= (1 << PTP_V2_FLAG_TWO_STEP);
    }

    if (i1v1Flag & (1 << PTP_V1_FLAG_LI_61))
    {
        *pi1v2Flag |= (1 << PTP_V2_FLAG_LEAP61);
    }

    if (i1v1Flag & (1 << PTP_V1_FLAG_LI_59))
    {
        *pi1v2Flag |= (1 << PTP_V2_FLAG_LEAP59);
    }

    /* Octct[1] Flags */
    pi1v2Flag++;
    bFlag = PtpCmUtlCheckStratumClkId (u1v1Stratum, pi1v1ClkId);
    if (bFlag == OSIX_TRUE)
    {
        *pi1v2Flag |= (1 << PTP_V2_FLAG_CURR_UTC_OFF_SET);
        *pi1v2Flag |= (1 << PTP_V2_FLAG_TIME_TRACEABLE);
        *pi1v2Flag |= (1 << PTP_V2_FLAG_FREQ_TRACEABLE);
    }

    if (!(((MEMCMP (pi1v1ClkId, PTP_V1_CLK_ID_INIT, PTP_V1_CLK_ID_LEN)) ==
           0) ||
          ((MEMCMP (pi1v1ClkId, PTP_V1_CLK_ID_DFLT, PTP_V1_CLK_ID_LEN)) == 0)))
    {
        *pi1v2Flag |= (1 << PTP_V2_FLAG_PTP_TIMESCALE);
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpCmUtlv2FlagTov1Flag                                     */
/*                                                                           */
/* Description  : This function used to map the v2 Flag Fields to v1         */
/*                Flag Fields                                                */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : pu1v2Flag       - v2 Flag Fields                           */
/*              : pu1v1Flag       - Pointer to v1 Flag Fields                */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1v1Flag       - v1 Flag Fields                           */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv2FlagTov1Flag (INT1 *pi1v2Flag, INT1 *pi1v1Flag)
{
    INT1                i1Flag = 0;

    PTP_FN_ENTRY ();

    /* If the alternateMasterFlag or unicastFlag set return failure 
     * else get the  twoStepFlag,  leap61, leap59 flags value update the
     * v1 FlagFields and remaining fields reset */

    /* v2f[0][0] = alternateMasterFlag;     - FALSE
       v2f[0][1] = twoStepFlag;             - v1f[1][3]
       v2f[0][2] = unicastFlag;             - FALSE
       v2f[1][0] = leap61;                  - v1f[1][0]
       v2f[1][1] = leap59;                  - v1f[1][1]
     */
    MEMSET (pi1v1Flag, 0, PTP_V1_FLAG_FIELD_LEN);

    /* The V1 Flags Bits are in pi1v1Flag[1] */
    pi1v1Flag++;

    /* Octet[0] Flags */
    i1Flag = *pi1v2Flag;
    if ((i1Flag & (1 << PTP_V2_FLAG_ALT_MST)))
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpCmUtlv2FlagTov1Flag: Alternate Message Is Set.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }
    if ((i1Flag & (1 << PTP_V2_FLAG_UNICAST)))
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpCmUtlv2FlagTov1Flag: Unicast Flag Is Set.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }
    if ((i1Flag & (1 << PTP_V2_FLAG_TWO_STEP)))
    {
        *pi1v1Flag |= (1 << PTP_V1_FLAG_ASSIST);
    }

    /* Octet[1] Flags */
    pi1v2Flag = pi1v2Flag + 1;

    i1Flag = *pi1v2Flag;

    if ((i1Flag & (1 << PTP_V2_FLAG_LEAP61)))
    {
        *pi1v1Flag |= (1 << PTP_V1_FLAG_LI_61);
    }
    if ((i1Flag & (1 << PTP_V2_FLAG_LEAP59)))
    {
        *pi1v1Flag |= (1 << PTP_V1_FLAG_LI_59);
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv2ClkQualTov1Fields                                */
/*                                                                           */
/* Description  : This function used to map the v2 Clock Quality to v1       */
/*                Stratum, ClkId, Variance                                   */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : pv2ClkQuality   - v2 Clock Quality value                   */
/*              : pu1v1Stratum    - Pointer to v1 Stratum                    */
/*              : pi1v1ClkId      - Pointer to v1 Clock Id                   */
/*              : pi2v1Variance   - Pointer to v1 Variance                   */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1v1Stratum    -  v1 Stratum                              */
/*                pi1v1ClkId      -  v1 Clock Id                             */
/*                pi2v1Variance   -  v1 Variance                             */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv2ClkQualTov1Fields (tPtpClkQuality * pv2ClkQuality,
                             UINT1 *pu1v1Stratum, INT1 *pi1v1ClkId,
                             INT2 *pi2v1Variance)
{
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    i4Status = PtpCmUtlv2ClkClsTov1Stratum (pv2ClkQuality->u1Class,
                                            pu1v1Stratum);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpCmUtlv2ClkQualTov1Fields: Could Not Map Clock Quality V2 to V1.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    i4Status = PtpCmUtlv2ClkAccuracyTov1ClkId (pv2ClkQuality->u1Accuracy,
                                               pi1v1ClkId);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpCmUtlv2ClkQualTov1Fields: Could Not Map Clock Accuracy V2 to V1.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* IEEE1588: 18.3.12 says-> correction for the offset of 0x8000; */
    *pi2v1Variance = (INT2) (pv2ClkQuality->u2OffsetScaledLogVariance);

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv1TimStpTov2TimStp                                 */
/*                                                                           */
/* Description  : This function used to map the v2 Timestatmp to v1          */
/*                Timestamp                                                  */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : pv1Timestamp    - v1 Timestamp value                       */
/*              : u2v1EpochNum    - v1 Epoch value                           */
/*              : pv2Timestamp    - Pointer to v2 Timestamp                  */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pv2Timestamp    -  v2 STimestamp value                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlv1TimStpTov2TimStp (tPtpV1TimeRep * pv1Timestamp, UINT2 u2v1EpochNum,
                            tPtpTimeStamp * pv2Timestamp)
{
    PTP_FN_ENTRY ();

    /*  if nanosec is negative return error; */
    /*  v1 nanosec -> v2 nanosec  */
    /*  v1 epcho   -> v2 sec[0-1]  */
    /*  v1 sec     -> v2 sec[2-7]  */
    if (pv1Timestamp->i4NanoSec < 0)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpCmUtlv1TimStpTov2TimStp: Nano Second Cannot Be Negative.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    pv2Timestamp->u4NanoSec = (UINT4) pv1Timestamp->i4NanoSec;
    UINT8_LO (&(pv2Timestamp->u8Sec)) = pv1Timestamp->u4Sec;
    UINT8_HI (&(pv2Timestamp->u8Sec)) = (UINT4) u2v1EpochNum;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCmUtlv2TimStpTov1TimStp                                  */
/*                                                                           */
/* Description  : This function used to map the v2 Timestatmp to v1          */
/*                Timestamp                                                  */
/*                using the IEEE1588 - 18.3 Message formats and data types.  */
/*                                                                           */
/* Input        : pv2Timestamp    - v2 Timestamp value                       */
/*              : pv1Timestamp    - Pointer to v1 Timestamp value            */
/*              : pu2v1EpochNum   - Pointer to v1 Epoch value                */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pv1Timestamp    -  v1 STimestamp value                     */
/*                pu2v1EpochNum   -  v1 Epoch value                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpCmUtlv2TimStpTov1TimStp (tPtpTimeStamp * pv2Timestamp,
                            tPtpV1TimeRep * pv1Timestamp, UINT2 *pu2v1EpochNum)
{
    PTP_FN_ENTRY ();

    /* v2 nanosec  -> v1 nanosec */
    /* v2 sec[4-5] -> v1 epcho   */
    /* v2 sec[0-3] -> v1 sec     */
    pv1Timestamp->i4NanoSec = (INT4) pv2Timestamp->u4NanoSec;
    pv1Timestamp->u4Sec = (UINT4) UINT8_LO (&(pv2Timestamp->u8Sec));
    *pu2v1EpochNum = (UINT2) UINT8_HI (&(pv2Timestamp->u8Sec));
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpCmUtlGetDomainIdFromPTPv1Msg                             */
/*                                                                           */
/* Description  : This function used to map v1 domain to v2 domainId if the  */
/*                PTP Msg is version 1 otherwise failure.                    */
/*                                                                           */
/* Input        : pu1PtpPdu       - PTP Msg Pdu                              */
/*              : pu1DomainId     - Pointer to v2 DomainId                   */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pu1DomainId     -  v2 DomainId                             */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCmUtlGetDomainIdFromPTPv1Msg (UINT1 *pu1PtpPdu, UINT1 *pu1DomainId)
{
    INT1                ai1v1SubDomain[PTP_SUBDOMAIN_NAME_LENGTH];
    UINT2               u2PtpVersion = PTP_MESSAGE_VERSION_TWO;

    MEMSET (&(ai1v1SubDomain[0]), 0, PTP_SUBDOMAIN_NAME_LENGTH);

    PTP_LBUF_GET_2_BYTES (pu1PtpPdu, u2PtpVersion);

    if (u2PtpVersion == PTP_MESSAGE_VERSION_ONE)
    {
        /* Find the SubDomain and Get v2 Domain Mapping */
        MEMCPY (&(ai1v1SubDomain[0]), (pu1PtpPdu + 2),
                PTP_SUBDOMAIN_NAME_LENGTH);
        if ((PtpCmUtlv1SubDomainTov2Domain (&(ai1v1SubDomain[0]), pu1DomainId))
            == OSIX_SUCCESS)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC,
                      "PtpCmUtlGetDomainIdFromPTPv1Msg : v2 Domain Id %d\r\n",
                      *pu1DomainId));
            PTP_FN_EXIT ();
            return (OSIX_SUCCESS);
        }
    }
    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
              "PtpCmUtlGetDomainIdFromPTPv1Msg : Packet is not v1 or Invalid"
              "v1 Domain Value.\r\n"));
    PTP_FN_EXIT ();
    return (OSIX_FAILURE);
}
#endif /*  _PTPCMUTL_C_ */

/***************************************************************************
 *                         END OF FILE ptpcomp.c                           *
 ***************************************************************************/
