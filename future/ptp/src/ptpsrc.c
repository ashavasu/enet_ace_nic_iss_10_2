/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpsrc.c,v 1.6 2016/04/05 13:23:59 siva Exp $
 *
 * Description: This file contains the PTP CLI related routines and
 * utility functions.
 *****************************************************************************/

#ifndef _PTPSRC_C_
#define _PTPSRC_C_

#include "ptpincs.h"
#include "ptpsrcpt.h"

PRIVATE VOID
    PtpSrcClockOptionalDetails PROTO ((tCliHandle CliHandle, INT4 i4ContextId,
                                       INT4 i4DomainId));

/***************************************************************************
 * FUNCTION NAME             : PtpSrcScalars
 *
 * DESCRIPTION               : This function displays scalar objects in PTP
 *                             for show running comfiguration command
 *
 * INPUT                     :
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
PtpSrcScalars (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE PtpTrapOption;
    INT4                i4PtpSysCntrl = 0;
    INT4                i4PtpPrimaryContext = 0;
    INT4                i4ContextId = 0;
    INT4                i4AdminStatus =0;    
    UINT1               u1TrapOption = 0;
    UINT1               u1CurTrapOption = 0;
    UINT1               au1ContextName[PTP_CLI_MAX_ALIAS_LEN];

    MEMSET (&PtpTrapOption, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    PtpTrapOption.pu1_OctetList = &u1TrapOption;
    PtpTrapOption.i4_Length = 1;

    /* Get Notification and Print */
    nmhGetFsPtpNotification (&PtpTrapOption);

    if (PtpTrapOption.i4_Length != 0)
    {

        u1CurTrapOption = PtpTrapOption.pu1_OctetList[0];

        if (u1CurTrapOption != 0)
        {
            if ((u1CurTrapOption & PTP_TRAP_MASK_GLOB_ERR) ==
                PTP_TRAP_MASK_GLOB_ERR)
            {
                CliPrintf (CliHandle, "ptp notification global-error\r\n");
            }

            if ((u1CurTrapOption & PTP_TRAP_MASK_SYS_CTRL_CHNG) ==
                PTP_TRAP_MASK_SYS_CTRL_CHNG)
            {
                CliPrintf (CliHandle, "ptp notification sys-ctrl-change\r\n");
            }

            if ((u1CurTrapOption & PTP_TRAP_MASK_SYS_ADMIN_CHNG) ==
                PTP_TRAP_MASK_SYS_ADMIN_CHNG)
            {
                CliPrintf (CliHandle, "ptp notification sys-admin-change\r\n");
            }

            if ((u1CurTrapOption & PTP_TRAP_MASK_PORT_STATE_CHNG) ==
                PTP_TRAP_MASK_PORT_STATE_CHNG)
            {
                CliPrintf (CliHandle, "ptp notification port-state-change\r\n");
            }

            if ((u1CurTrapOption & PTP_TRAP_MASK_PORT_ADMIN_CHNG) ==
                PTP_TRAP_MASK_PORT_ADMIN_CHNG)
            {
                CliPrintf (CliHandle, "ptp notification port-admin-change\r\n");
            }

            if ((u1CurTrapOption & PTP_TRAP_MASK_SYNC_FAULT) ==
                PTP_TRAP_MASK_SYNC_FAULT)
            {
                CliPrintf (CliHandle, "ptp notification sync-fault\r\n");
            }

            if ((u1CurTrapOption & PTP_TRAP_MASK_GM_FAULT) ==
                PTP_TRAP_MASK_GM_FAULT)
            {
                CliPrintf (CliHandle, "ptp notification gm-fault\r\n");
            }

            if ((u1CurTrapOption & PTP_TRAP_MASK_ACC_MASTER_FAULT) ==
                PTP_TRAP_MASK_ACC_MASTER_FAULT)
            {
                CliPrintf (CliHandle, "ptp notification acc-master-fault\r\n");
            }

            /* Trap Octet[1] Mask values */

            u1CurTrapOption = PtpTrapOption.pu1_OctetList[0];  
            if ((u1CurTrapOption & PTP_TRAP_MASK_UCAST_ADMIN_CHNG) ==
                PTP_TRAP_MASK_UCAST_ADMIN_CHNG)
            {
                CliPrintf (CliHandle,
                           "ptp notification unicast-admin-change\r\n");
            }
            CliPrintf (CliHandle, "! \r\n");
        }
    }

    nmhGetFsPtpGlobalSysCtrl (&i4PtpSysCntrl);

    /* Print PTP Global System Status */

    if (i4PtpSysCntrl != PTP_DISABLED)
    {
        CliPrintf (CliHandle, "no shutdown ptp\r\n");
    }

    /* Print PTP Primary Context Information */

    nmhGetFsPtpPrimaryContext (&i4PtpPrimaryContext);

    /* Get Context Name from Context ID */
    PtpPortVcmGetAliasName (i4PtpPrimaryContext, au1ContextName);

    if (i4PtpPrimaryContext != 0)
    {
        CliPrintf (CliHandle, "ptp primary-context switch %s\r\n",
                   au1ContextName);
    }

     /* Print PTP admin status */

    nmhGetFsPtpAdminStatus (i4ContextId, &i4AdminStatus);
    PtpPortVcmGetAliasName ((UINT4)i4AdminStatus, au1ContextName);
   
        if (i4AdminStatus == PTP_ENABLED)
         {
              CliPrintf (CliHandle, "ptp enable switch %s\r\n", au1ContextName);
         }

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME             : PtpSrcInterfaces
 *
 * DESCRIPTION               : This function displays interface objects
 *                             for show running comfiguration command
 *
 * INPUT                     : 
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
VOID
PtpSrcInterfaces (tCliHandle CliHandle,INT4 i4CurrDomainId)
{
    INT4                i4ContextId = 0;
    INT4                i4DomainNumber = 0;
    INT4                i4Port = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4IfType = 0;
    INT4                i4IfIndex = 0;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1CxtName[PTP_SWITCH_ALIAS_LEN];
    tPtpPort           *pPtpPort = NULL;

    pPtpPort = (tPtpPort *)
        PtpDbGetFirstNode (gPtpGlobalInfo.PortTree,
                           (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET);


while (pPtpPort != NULL)
    {
        if(i4CurrDomainId == pPtpPort->pPtpDomain->u1DomainId)
        {
            break;
        }

        pPtpPort = (tPtpPort *)
            PtpDbGetNextNode (gPtpGlobalInfo.PortTree,
                    pPtpPort,
                    (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET);
    }

   if (pPtpPort == NULL)
    {
        return;
    }
    do
    {
        i4ContextId = (INT4) pPtpPort->pPtpDomain->pPtpCxt->u4ContextId;
        i4DomainNumber = (INT4) pPtpPort->pPtpDomain->u1DomainId;
        i4Port = (INT4) pPtpPort->PortDs.u4PtpPortNumber;

        if (pPtpPort->u1RowStatus != ACTIVE)
        {
            pPtpPort = PtpIfGetNextPortEntry ((UINT4) i4ContextId,
                                              (UINT1) i4DomainNumber,
                                              (UINT4) i4Port);
            continue;
        }
        nmhGetFsPtpPortInterfaceType (i4ContextId, i4DomainNumber,
                                      i4Port, &i4IfType);
        nmhGetFsPtpPortIfaceNumber (i4ContextId, i4DomainNumber,
                                    i4Port, &i4IfIndex);

    if(i4CurrDomainId != pPtpPort->pPtpDomain->u1DomainId)
   {
       break;
   }
       CfaCliConfGetIfName (i4IfIndex, ai1IfName);

        /* Get Context Name from Context ID */
        PtpPortVcmGetAliasName (i4ContextId, (UINT1 *) au1CxtName);

       switch (i4IfType)
        {
            case PTP_IFACE_IEEE_802_3:
                CliPrintf (CliHandle, " ptp port ");
                CliPrintf (CliHandle, " %s \r\n", ai1IfName);
                break;
            case PTP_IFACE_UDP_IPV4:
                CliPrintf (CliHandle, " ptp port ipv4 ");
                CliPrintf (CliHandle, " %s \r\n", ai1IfName);
                break;
            case PTP_IFACE_UDP_IPV6:
                CliPrintf (CliHandle, " ptp port ipv6 ");
                CliPrintf (CliHandle, " %s \r\n", ai1IfName);
                break;
            case PTP_IFACE_VLAN:
                CliPrintf (CliHandle, " ptp port vlan %u\r\n", i4IfIndex);
                break;
            default:
                break;
        }
        CliPrintf (CliHandle, "!\r\n");

        switch (i4IfType)
        {
            case PTP_IFACE_IEEE_802_3:
                CliPrintf (CliHandle, "interface ");
                CliPrintf (CliHandle, " %s\r\n", ai1IfName);
                break;
            case PTP_IFACE_UDP_IPV4:
                CliPrintf (CliHandle, "interface ");
                CliPrintf (CliHandle, " %s\r\n", ai1IfName);
                break;
            case PTP_IFACE_UDP_IPV6:
                CliPrintf (CliHandle, "interface ");
                CliPrintf (CliHandle, " %s\r\n", ai1IfName);
                break;
            case PTP_IFACE_VLAN:
                PtpPortVcmGetAliasName (i4ContextId, (UINT1 *) au1CxtName);
                CliPrintf (CliHandle, "switch %s\r\n", au1CxtName);
                CliPrintf (CliHandle, " vlan %u\n\n", i4IfIndex);
                break;
        }

        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);

        PtpSrcIfDetails (CliHandle, i4ContextId, i4DomainNumber, i4Port);

        CliRegisterLock (CliHandle, PtpApiLock, PtpApiUnLock);
        PtpApiLock ();

        u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Pressed 'q' at the prompt, so break! */
            break;
        }

        pPtpPort = PtpIfGetNextPortEntry ((UINT4) i4ContextId,
                                          (UINT1) i4DomainNumber,
                                          (UINT4) i4Port);
    }
    while (pPtpPort != NULL);

    return;
}

/***************************************************************************
 * FUNCTION NAME             : PtpSrcIfDetails
 *
 * DESCRIPTION               : This function displays Interface objects 
 *                             in PTP for show running comfiguration 
 *                             command
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             i4IfIndex - Interface Index
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PUBLIC VOID
PtpSrcIfDetails (tCliHandle CliHandle, INT4 i4ContextId,
                 INT4 i4DomainId, INT4 i4IfIndex)
{
        
    INT4                i4RowStatus = 0;
    INT4                i4AnnounceIntvl = 0;
    INT4                i4AnnounceTimeout = 0;
    INT4                i4SyncInterval = 0;
    INT4                i4DelayType = 0;
    INT4                i4DelayReqIntvl = 0;
    INT4                i4Version = 0;
    INT4                i4Status = 0;
    INT4                i4RetVal = 0;
     

    CliRegisterLock (CliHandle, PtpApiLock, PtpApiUnLock);
    PtpApiLock ();

    if (nmhValidateIndexInstanceFsPtpPortConfigDataSetTable
        (i4ContextId, i4DomainId, i4IfIndex) != SNMP_SUCCESS)
    {
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    nmhGetFsPtpPortRowStatus (i4ContextId, i4DomainId, i4IfIndex, &i4RowStatus);
    if (i4RowStatus != ACTIVE)
    {
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    nmhGetFsPtpPortPtpStatus (i4ContextId, i4DomainId, i4IfIndex, &i4Status);

    if (i4Status == OSIX_TRUE)
    {
        CliPrintf (CliHandle, " ptp enable domain %u\r\n", i4DomainId);
    }

    nmhGetFsPtpPortAnnounceInterval (i4ContextId,
                                     i4DomainId, i4IfIndex, &i4AnnounceIntvl);
   
    if (i4AnnounceIntvl != PTP_PORT_DEF_ANNOUNCE_INTERVAL)
    {
        CliPrintf (CliHandle, " ptp announce interval %u domain %u\r\n",
                   i4AnnounceIntvl, i4DomainId);
    }

    nmhGetFsPtpPortAnnounceReceiptTimeout (i4ContextId,
                                           i4DomainId, i4IfIndex,
                                           &i4AnnounceTimeout);

    if (i4AnnounceTimeout != PTP_PORT_DEF_ANNOUNCE_RECEIPT_TIMEOUT)
    {
        CliPrintf (CliHandle, " ptp announce timeout %u domain %u\r\n",
                   i4AnnounceTimeout, i4DomainId);
    }

   nmhGetFsPtpPortSyncInterval (i4ContextId,
                        i4DomainId, i4IfIndex, 
                        &i4SyncInterval);

    if (i4SyncInterval != PTP_PORT_DEF_SYNC_TIMEOUT)
    {
        CliPrintf (CliHandle, " ptp sync interval %d domain %u\r\n",
                   i4SyncInterval, i4DomainId);
    }
   
    nmhGetFsPtpPortDelayMechanism (i4ContextId, i4DomainId,
                                   i4IfIndex, &i4DelayType);

    if (i4DelayType != PTP_PORT_DELAY_MECH_DISABLED)
    {
    
           nmhGetFsPtpPortMinDelayReqInterval (i4ContextId, 
                                                i4DomainId, i4IfIndex, 
                                                &i4DelayReqIntvl);
            if (i4DelayReqIntvl != 0 )
            {
            
                CliPrintf (CliHandle, " ptp delay-req interval %u domain %u\r\n",
                        i4DelayReqIntvl, i4DomainId);
             }

     
          nmhGetFsPtpPortDelayMechanism (i4ContextId, i4DomainId,
                                       i4IfIndex, &i4DelayType);

        if (i4DelayType != PTP_PORT_DELAY_MECH_END_TO_END)
        {
            CliPrintf (CliHandle, " ptp delay mechanism %u domain %u\r\n",
                       i4DelayType, i4DomainId);
        }
    }

    nmhGetFsPtpPortVersionNumber (i4ContextId, i4DomainId,
                                  i4IfIndex, &i4Version);

    if (i4Version != PTP_VERSION_TWO)
    {
        CliPrintf (CliHandle, " ptp version %u domain %u\r\n",
                   i4Version, i4DomainId);
    }

    /* Optional features command */

    nmhGetFsPtpPortAltMulcastSync (i4ContextId, i4DomainId,
                                   i4IfIndex, &i4RetVal);

    if (i4RetVal != PTP_DISABLED)
    {
        CliPrintf (CliHandle, " ptp alternate-master alternate-multisync");

        if (i4DomainId != 0)
        {
            CliPrintf (CliHandle, " domain %u", i4DomainId);
        }

        CliPrintf (CliHandle, "\r\n");
    }

    nmhGetFsPtpPortAltMulcastSyncInterval (i4ContextId, i4DomainId,
                                           i4IfIndex, &i4RetVal);

    if (i4RetVal != 0)
    {
        CliPrintf (CliHandle,
                   " ptp alternate-master multisync-interval %u", i4RetVal);

        if (i4DomainId != 0)
        {
            CliPrintf (CliHandle, " domain %u", i4DomainId);
        }

        CliPrintf (CliHandle, "\r\n");
    }

    nmhGetFsPtpPortAccMasterEnabled (i4ContextId, i4DomainId,
                                     i4IfIndex, &i4RetVal);

    if (i4RetVal != PTP_DISABLED)
    {
        CliPrintf (CliHandle, " ptp acceptable-master enable");

        if (i4DomainId != 0)
        {
            CliPrintf (CliHandle, " domain %u", i4DomainId);
        }

        CliPrintf (CliHandle, "\r\n");
    }

    nmhGetFsPtpPortNumOfAltMaster (i4ContextId, i4DomainId,
                                   i4IfIndex, &i4RetVal);

    if (i4RetVal != 0)
    {
        CliPrintf (CliHandle, " ptp max alternate-masters %u", i4RetVal);

        if (i4DomainId != 0)
        {
            CliPrintf (CliHandle, " domain %u", i4DomainId);
        }

        CliPrintf (CliHandle, "\r\n");
    }

    PtpApiUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}

/***************************************************************************
 * FUNCTION NAME             : PtpSrcClocks
 *
 * DESCRIPTION               : This function Clock Table objects in PTP
 *                             for show running comfiguration command
 *
 * INPUT                     : i4ContextId - Context Identifier
 *                             i4DomainId - Domain Identifier
 *                             
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
VOID
PtpSrcClocks (tCliHandle CliHandle)
{
    INT4                i4NextContextId = 0;
    INT4                i4NextDomainId = 0;
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4ContextId = 0;
    INT4                i4DomainId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT1                ai1CxtName[PTP_SWITCH_ALIAS_LEN];

    MEMSET (ai1CxtName, 0, PTP_SWITCH_ALIAS_LEN);

    i4RetVal = nmhGetFirstIndexFsPtpDomainDataSetTable
        (&i4NextContextId, &i4NextDomainId);

    while (i4RetVal != SNMP_FAILURE)
    {

        /* Get Context Name from Context ID */
        PtpPortVcmGetAliasName (i4NextContextId, (UINT1 *) ai1CxtName);
        CliPrintf (CliHandle, "ptp switch %s domain %u\r\n", ai1CxtName,
                   i4NextDomainId);

        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);

        PtpSrcClockDetails (CliHandle, i4NextContextId, i4NextDomainId);
        PtpSrcInterfaces (CliHandle, i4NextDomainId);

        CliRegisterLock (CliHandle, PtpApiLock, PtpApiUnLock);
        PtpApiLock ();
        CliPrintf (CliHandle, "!");
        u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        if (u4PagingStatus == CLI_FAILURE)
        {
            /* User Pressed 'q' at the prompt, so break! */
            break;
        }

        i4ContextId = i4NextContextId;
        i4DomainId = i4NextDomainId;

        i4RetVal =
            nmhGetNextIndexFsPtpDomainDataSetTable
            (i4ContextId, &i4NextContextId, i4DomainId, &i4NextDomainId);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME             : PtpSrcClockDetails
 *
 * DESCRIPTION               : This function displays Tabular objects in PTP
 *                             for show running comfiguration command
 *
 * INPUT                     : CliHandle   - CLI Handle
 *                             i4ContextId - Context Identifier
 *                             i4DomainId  - Domain Identifier
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
VOID
PtpSrcClockDetails (tCliHandle CliHandle, INT4 i4ContextId, INT4 i4DomainId)
{

    tPtpDomain         *pPtpDomain = NULL;
    INT4                i4RowStatus = 0;
    INT4                i4ClkMode = 0;
    INT4                i4ClkPriority1 = 0;
    INT4                i4ClkPriority2 = 0;
    INT4                i4SlaveFlag = 0;
    INT4                i4PathTrcFlag = 0;
    INT4                i4TwoStepFlag = 0;
    INT4                i4PrimaryDomain = 0;
    INT4                i4NumberPorts = 0;
    INT4                i4Delaymechanism = 0;

    CliRegisterLock (CliHandle, PtpApiLock, PtpApiUnLock);
    PtpApiLock ();

    if (nmhValidateIndexInstanceFsPtpDomainDataSetTable
        (i4ContextId, i4DomainId) != SNMP_SUCCESS)
    {
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return;
    }
    nmhGetFsPtpDomainRowStatus (i4ContextId, i4DomainId, &i4RowStatus);

    if (i4RowStatus == NOT_IN_SERVICE)
    {
        PtpApiUnLock ();
        CliUnRegisterLock (CliHandle);
        return;
    }

    nmhGetFsPtpDomainClockMode (i4ContextId, i4DomainId, &i4ClkMode);

    switch (i4ClkMode)
    {

        case PTP_BOUNDARY_CLOCK_MODE:

            CliPrintf (CliHandle, " ptp mode boundary\r\n");
            break;

        case PTP_OC_CLOCK_MODE:

            CliPrintf (CliHandle, " ptp mode ordinary\r\n");
            break;

        case PTP_TRANSPARENT_CLOCK_MODE:

            /*get Delay mechanism and print accordingly */
            nmhGetFsPtpTransparentClockDelaymechanism (i4ContextId,
                                                       i4DomainId,
                                                       &i4Delaymechanism);

            if (i4Delaymechanism == PTP_PORT_DELAY_MECH_END_TO_END)
            {
                CliPrintf (CliHandle, " ptp mode e2etransparent\r\n");
            }

            if (i4Delaymechanism == PTP_PORT_DELAY_MECH_PEER_TO_PEER)
            {
                CliPrintf (CliHandle, " ptp mode p2ptransparent\r\n");
            }
            break;
       
        default:
            break;
    }

    nmhGetFsPtpClockPriority1 (i4ContextId, i4DomainId, &i4ClkPriority1);

    if (i4ClkMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        if (i4ClkPriority1 != 0)
        {
            CliPrintf (CliHandle, " ptp priority1 %u\r\n", i4ClkPriority1);
        }
    }
    else if ((i4ClkPriority1 != PTP_DEF_CLK_PRIORITY) && 
             ((i4ClkMode == PTP_BOUNDARY_CLOCK_MODE) || 
               (i4ClkMode == PTP_OC_CLOCK_MODE)))
    {
        CliPrintf (CliHandle, " ptp priority1 %u\r\n", i4ClkPriority1);
    }

    nmhGetFsPtpClockPriority2 (i4ContextId, i4DomainId, &i4ClkPriority2);

    if (i4ClkMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        if (i4ClkPriority2 != 0)
        {
            CliPrintf (CliHandle, " ptp priority2 %u\r\n", i4ClkPriority2);
        }
    }
    else if ((i4ClkPriority2 != PTP_DEF_CLK_PRIORITY)&&
             ((i4ClkMode == PTP_BOUNDARY_CLOCK_MODE) ||
              (i4ClkMode == PTP_OC_CLOCK_MODE)))
    {
        CliPrintf (CliHandle, " ptp priority2 %u\r\n", i4ClkPriority2);
    }

    nmhGetFsPtpClockSlaveOnly (i4ContextId, i4DomainId, &i4SlaveFlag);

    if (i4SlaveFlag == OSIX_TRUE)
    {
        CliPrintf (CliHandle, " ptp slave\r\n");
    }

    nmhGetFsPtpClockPathTraceOption (i4ContextId, i4DomainId, &i4PathTrcFlag);

    nmhGetFsPtpClockTwoStepFlag (i4ContextId, i4DomainId, &i4TwoStepFlag);

    if (i4TwoStepFlag == PTP_ENABLED)
    {
        CliPrintf (CliHandle, " ptp two-step-clock\r\n");
    }

    nmhGetFsPtpPrimaryDomain (i4ContextId, &i4PrimaryDomain);

    if (i4PrimaryDomain != PTP_DEFAULT_DOMAIN)
    {
        CliPrintf (CliHandle, " ptp primary-domain\r\n");
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4ContextId, (UINT1) i4DomainId);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4ContextId, (UINT1) i4DomainId,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        return;
    }

    if (i4ClkMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        i4NumberPorts = (INT4) pPtpDomain->ClockDs.TransClkDs.u4NumberOfPorts;
        if (i4NumberPorts != 0)
        {
            CliPrintf (CliHandle, " ptp clock ports %d\r\n", i4NumberPorts);
        }
    }
    else if ((i4ClkMode == PTP_OC_CLOCK_MODE)
             || (i4ClkMode == PTP_BOUNDARY_CLOCK_MODE))
    {
        i4NumberPorts =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u4NumberOfPorts;
        if (i4NumberPorts != PTP_DEF_CLOCK_NUMBER_PORTS)
        {
            CliPrintf (CliHandle, " ptp clock ports %d\r\n", i4NumberPorts);
        }
    }

    PtpSrcClockOptionalDetails (CliHandle, i4ContextId, i4DomainId);
    PtpApiUnLock ();
    CliUnRegisterLock (CliHandle);
    return;
}

/***************************************************************************
* FUNCTION NAME             : PtpSrcClockOptionalDetails
*
* DESCRIPTION               : This function displays Tabular objects in PTP
*                             optional features for show running
*                             configuration command
*
* INPUT                     : CliHandle   - CLI Handle
*                             i4ContextId - Context Identifier
*                             i4DomainId  - Domain Identifier
*
* OUTPUT                    : None
*
* Global Variables Referred : None
*
* Global variables Modified : None
*
* Use of Recursion          : None
*
* RETURNS                   : CLI_SUCCESS/CLI_FAILURE
*
**************************************************************************/
PRIVATE VOID
PtpSrcClockOptionalDetails (tCliHandle CliHandle, INT4 i4ContextId,
                            INT4 i4DomainId)
{
    tSNMP_OCTET_STRING_TYPE AltTimeDispName;
    tSNMP_OCTET_STRING_TYPE AltTimeScaletimeOfNextJump;
    tSNMP_OCTET_STRING_TYPE AccMasterAddr;
    tSNMP_OCTET_STRING_TYPE NextAccMasterAddr;
    tIp6Addr            ip6Addr;
    UINT1               au1AltTimeScaleNextJump[PTP_U8_STR_LEN];
    UINT1               au1AltTimeScaleName[PTP_MAX_CLOCK_ID_LEN];
    UINT1               au1AccMasterAddr[PTP_MAX_ADDR_LEN];
    UINT1               au1NextAccMasterAddr[PTP_MAX_ADDR_LEN];
    INT4                i4RetVal = 0;
    INT4                i4NextContextId = 0;
    INT4                i4NextDomainId = 0;
    INT4                i4KeyId = 0;
    INT4                i4NextKeyId = 0;
    INT4                i4NwProtocol = 0;
    INT4                i4NextNwProtocol = 0;
    INT4                i4AccMstLen = 0;
    INT4                i4NextAccMstLen = 0;

    nmhGetFsPtpClockPathTraceOption (i4ContextId, i4DomainId, &i4RetVal);

    if (i4RetVal == PTP_ENABLED)
    {
        CliPrintf (CliHandle, " ptp pathtrace\r\n");
    }

    AltTimeDispName.pu1_OctetList = au1AltTimeScaleName;
    AltTimeScaletimeOfNextJump.pu1_OctetList = au1AltTimeScaleNextJump;
    AccMasterAddr.pu1_OctetList = au1AccMasterAddr;
    NextAccMasterAddr.pu1_OctetList = au1NextAccMasterAddr;

    while (nmhGetNextIndexFsPtpAltTimeScaleDataSetTable
           (i4ContextId, &i4NextContextId, i4DomainId, &i4NextDomainId,
            i4KeyId, &i4NextKeyId) == SNMP_SUCCESS)
    {
        if ((i4ContextId != i4NextContextId) || (i4DomainId != i4NextDomainId))
        {
            break;
        }

        i4KeyId = i4NextKeyId;

        MEMSET (au1AltTimeScaleName, 0, PTP_MAX_CLOCK_ID_LEN);
        MEMSET (au1AltTimeScaleNextJump, 0, PTP_U8_STR_LEN);
        AltTimeDispName.i4_Length = 0;
        AltTimeScaletimeOfNextJump.i4_Length = 0;

        nmhGetFsPtpAltTimeScaledisplayName (i4ContextId, i4DomainId,
                                            i4KeyId, &AltTimeDispName);

        if (AltTimeDispName.i4_Length != 0)
        {
            CliPrintf (CliHandle, " ptp alternate-time-scale key %d name %s\r\n",
                       i4KeyId, au1AltTimeScaleName);
        }

        nmhGetFsPtpAltTimeScaleRowStatus (i4ContextId, i4DomainId, i4KeyId,
                                          &i4RetVal);

        if (i4RetVal == ACTIVE)
      
        {
            CliPrintf (CliHandle, " ptp alternate-time-scale enable key %d\r\n",
                       i4KeyId);
        }
        
     
        CliPrintf (CliHandle, " ptp alternate-time-scale key %d", i4KeyId);

       nmhGetFsPtpAltTimeScalecurrentOffset (i4ContextId, i4DomainId, i4KeyId,
                                              &i4RetVal);
       if (i4RetVal != 0)
        { 
            CliPrintf (CliHandle, " current-offset %d", i4RetVal);
        }

        nmhGetFsPtpAltTimeScalejumpSeconds (i4ContextId, i4DomainId, i4KeyId,
                                            &i4RetVal);

        if (i4RetVal != 0)
        {
            CliPrintf (CliHandle, " jump-seconds %d", i4RetVal);
        }

        nmhGetFsPtpAltTimeScaletimeOfNextJump (i4ContextId, i4DomainId, i4KeyId,
                                               &AltTimeScaletimeOfNextJump);

        if (AltTimeScaletimeOfNextJump.i4_Length != 0)
        {
            CliPrintf (CliHandle, " next-jump %s",
                       AltTimeScaletimeOfNextJump.pu1_OctetList);
        }

        CliPrintf (CliHandle, "\r\n");
    }

    /* Acceptable master */
    while (nmhGetNextIndexFsPtpAccMasterDataSetTable
           (i4ContextId, &i4NextContextId, i4DomainId, &i4NextDomainId,
            i4NwProtocol, &i4NextNwProtocol, i4AccMstLen, &i4NextAccMstLen,
            &AccMasterAddr, &NextAccMasterAddr) == SNMP_SUCCESS)
    {
        if ((i4ContextId != i4NextContextId) || (i4DomainId != i4NextDomainId))
        {
            break;
        }

        i4NwProtocol = i4NextNwProtocol;
        i4AccMstLen = i4NextAccMstLen;
        AccMasterAddr.i4_Length = NextAccMasterAddr.i4_Length;
        MEMSET (au1AccMasterAddr, 0, PTP_MAX_ADDR_LEN);
        MEMCPY (au1AccMasterAddr, au1NextAccMasterAddr,
                AccMasterAddr.i4_Length);
        MEMSET (au1NextAccMasterAddr, 0, PTP_MAX_ADDR_LEN);

        nmhGetFsPtpAccMasterAlternatePriority (i4ContextId, i4DomainId,
                                               i4NwProtocol, i4AccMstLen,
                                               &AccMasterAddr, &i4RetVal);
        if (i4NwProtocol == CLI_PTP_IPV4)
        {
                CliPrintf (CliHandle, " ptp acceptable-master protocol ipv4 %d.%d.%d.%d",
                       AccMasterAddr.pu1_OctetList[0],
                       AccMasterAddr.pu1_OctetList[1],
                       AccMasterAddr.pu1_OctetList[2],
                       AccMasterAddr.pu1_OctetList[3]);
        }
        else if (i4NwProtocol == CLI_PTP_ETH)
        {
            CliPrintf (CliHandle, " ptp acceptable-master protocol ethernet %02x:%02x:%02x:%02x:%02x:%02x",
                       AccMasterAddr.pu1_OctetList[0],
                       AccMasterAddr.pu1_OctetList[1],
                       AccMasterAddr.pu1_OctetList[2],
                       AccMasterAddr.pu1_OctetList[3],
                       AccMasterAddr.pu1_OctetList[4],
                       AccMasterAddr.pu1_OctetList[5]);
        }
        else if (i4NwProtocol == CLI_PTP_IPV6)
        {
            MEMCPY (&ip6Addr, AccMasterAddr.pu1_OctetList,
                    AccMasterAddr.i4_Length);
            CliPrintf (CliHandle, " ptp acceptable-master protocol ipv6 %s", Ip6PrintAddr (&ip6Addr));
        }

        if (i4RetVal != 0)
        {
            CliPrintf (CliHandle, " alternatePriority1 %d", i4RetVal);
        }

        CliPrintf (CliHandle, "\r\n");
    }
}

#endif

