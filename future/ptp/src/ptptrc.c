/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptptrc.c,v 1.4 2014/02/03 12:27:08 siva Exp $
 *
 * Description: This file contains PTP trace implementation.       
 *********************************************************************/

#ifndef _PTPTRC_C_
#define _PTPTRC_C_

#include "ptpincs.h"
/*****************************************************************************/
/* Function                  : PtpTrcTrace                                   */
/*                                                                           */
/* Description               : This routine displays the trace message       */
/*                             If the context id is invalid, the context     */
/*                             name will be displayed as None                */
/*                             If domain id is not known in a function       */
/*                             invalid domain id should be passed            */
/*                                                                           */
/* Input                     : u4ContextId   -  Context identifier           */
/*                             u1DomainId    -  Domain identifier            */
/*                             u4Flag        -  Trace flag                   */
/*                             pc1Fmt        -  Format string                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpTrcTrace (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4Flag,
             CHR1 * pc1Fmt, ...)
{
    tPtpCxt            *pPtpCxt = NULL;
    UINT1               au1CtxtName[VCM_ALIAS_MAX_LEN];
    static CHR1         ac1ModuleString[PTP_MAX_TRC_LEN];
    va_list             ap;
    UINT4               u4OffSet = 0;

    if (u4Flag == 0)
    {
        return;
    }

    MEMSET (ac1ModuleString, 0, PTP_MAX_TRC_LEN);

    if (u4ContextId != PTP_INV_CONTEXT_ID)
    {
        pPtpCxt = PtpCxtGetNode (u4ContextId);

        if (pPtpCxt == NULL)
        {
            return;
        }

        if (!(u4Flag & pPtpCxt->u2Trace))
        {
            return;
        }

        MEMSET (au1CtxtName, 0, VCM_ALIAS_MAX_LEN);

        PtpPortVcmGetAliasName (u4ContextId, au1CtxtName);

        SNPRINTF (ac1ModuleString, PTP_MAX_TRC_LEN,
                  "PTP: Context: %s: Domain %u: ", au1CtxtName, u1DomainId);
    }
    else
    {

        if (!(u4Flag & gPtpGlobalInfo.u2GblTrace))
        {
            return;
        }

        SNPRINTF (ac1ModuleString, PTP_MAX_TRC_LEN, "PTP Global: ");
    }

    u4OffSet = STRLEN (ac1ModuleString);
    va_start (ap, pc1Fmt);
    vsprintf (&ac1ModuleString[u4OffSet], pc1Fmt, ap);
    va_end (ap);

    UtlTrcPrint ((const char *) ac1ModuleString);
}

/*****************************************************************************/
/* Function                  : PtpTrcDisplayBetterParam                      */
/*                                                                           */
/* Description               : This routine displays the trace message for   */
/*                             the corresponding BMC output generated.       */
/*                                                                           */
/* Input                     : pPtpForeignMaster - Pointer to Foreign Master */
/*                             u4DiffDataSet     - Data Set that differed.   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpTrcDisplayBetterParam (tPtpForeignMasterDs * pPtpForeignMaster,
                          UINT4 u4DiffDataSet)
{
#ifndef TRACE_WANTED
    UNUSED_PARAM (pPtpForeignMaster);
#endif
    switch (u4DiffDataSet)
    {
        case PTP_GM_CLK_ID:

            PTP_TRC ((pPtpForeignMaster->u4ContextId,
                      pPtpForeignMaster->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Data set has better Grand Master Clock Identity.\r\n"));
            break;

        case PTP_GM_PRIO_1:

            PTP_TRC ((pPtpForeignMaster->u4ContextId,
                      pPtpForeignMaster->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Data set has better Grand Master Priority 1.\r\n"));
            break;

        case PTP_GM_PRIO_2:

            PTP_TRC ((pPtpForeignMaster->u4ContextId,
                      pPtpForeignMaster->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Data set has better Grand Master Priority 2.\r\n"));
            break;

        case PTP_GM_CLASS:

            PTP_TRC ((pPtpForeignMaster->u4ContextId,
                      pPtpForeignMaster->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Data set has better Grand Master Clock Class.\r\n"));
            break;

        case PTP_GM_ACCURACY:

            PTP_TRC ((pPtpForeignMaster->u4ContextId,
                      pPtpForeignMaster->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Data set has better Grand Master Clock Accuracy.\r\n"));
            break;

        case PTP_GM_VARIANCE:

            PTP_TRC ((pPtpForeignMaster->u4ContextId,
                      pPtpForeignMaster->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Data set has better Grand Master Clock Variance.\r\n"));
            break;

        case PTP_STEPS_REMOVED:

            PTP_TRC ((pPtpForeignMaster->u4ContextId,
                      pPtpForeignMaster->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Steps removed for Data set is better...\r\n"));
            break;

        case PTP_RCV_SEND_IDENTITIES:

            PTP_TRC ((pPtpForeignMaster->u4ContextId,
                      pPtpForeignMaster->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Data set has better Receiver Clock Identity.\r\n"));
            break;

        default:
            break;
    }
}
#endif /*_PTPTRC_C_*/
