/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpport.c,v 1.5 2014/01/24 12:20:09 siva Exp $
 * Description: This file contains the portable routines for PTP module 
 *              that calls APIs given by other modules.
 *********************************************************************/
#ifndef _PTPPORT_C_
#define _PTPPORT_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function     : PtpPortCfaRegLL                                            */
/*                                                                           */
/* Description  : PTP module during initialization will invoke this API after*/
/*                loading the structure CfaRegParams with necessary callback */
/*                functions. On receiving a PTP packet, interface status     */
/*                changes, CFA module should invoke the mentioned call back  */
/*                functions appropriately.                                   */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CFA_SUCCESS/CFA_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortCfaRegLL (VOID)
{
    tCfaRegParams       CfaRegParams;
    INT4                i4RetVal = OSIX_SUCCESS;
    MEMSET (&CfaRegParams, 0, sizeof (tCfaRegParams));

    CfaRegParams.u2LenOrType = CFA_ENET_PTP;
    CfaRegParams.u2RegMask =
        CFA_IF_DELETE | CFA_IF_OPER_ST_CHG | CFA_IF_RCV_PKT;
    CfaRegParams.pIfCreate = NULL;
    CfaRegParams.pIfDelete = PtpApiIfDeleteCallBack;
    CfaRegParams.pIfUpdate = NULL;
    CfaRegParams.pIfOperStChg = PtpApiIfOperChgCallBack;
    CfaRegParams.pIfRcvPkt = PtpApiPktRxCallBack;

    if (CfaRegisterHL (&CfaRegParams) != CFA_SUCCESS)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortCfaRegLL:"
                  "CfaRegisterHL FAILED!!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function     : PtpPortCfaDeRegisterLL                                     */
/*                                                                           */
/* Description  : This API will be invoked by PTP whenever the module is     */
/*                de-initialized. This API should relinquish all the         */
/*                registrations earlier done by PTP module during            */
/*                initialization.                                            */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : CFA_SUCCESS/CFA_FAILURE                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortCfaDeRegisterLL (UINT2 u2LenOrType)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (CfaDeregisterHL (u2LenOrType) != CFA_SUCCESS)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortCfaDeRegisterLL:"
                  "CfaDeregisterHL FAILED!!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function     : PtpPortRegisterClock                                       */
/*                                                                           */
/* Description  : This API will be invoked by PTP whenever the module is     */
/*                initialized. This API will register the call backs with    */
/*                the clockiwf module.                                       */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortRegisterClock (VOID)
{
    tClkRegInfo         ClkRegInfo;
    INT4                i4RetVal = OSIX_SUCCESS;

    MEMSET (&ClkRegInfo, 0, sizeof (tClkRegInfo));

    ClkRegInfo.pClkChgIndiFn = PtpApiClkIwfCbkFn;
    ClkRegInfo.u1ModuleId = CLK_MOD_PTP;

    ClkRegRegisterProtocol (&ClkRegInfo);

    return i4RetVal;
}

/*****************************************************************************/
/* Function     : PtpPortDeRegisterClock                                     */
/*                                                                           */
/* Description  : This API will be invoked by PTP whenever the module is     */
/*                de-initialized. The CLOCKIWF module should stop invoking   */
/*                the callbacks for the PTP.                                 */
/*                                                                           */
/* Input        : u1Protocol - Protocol Id                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPortDeRegisterClock (UINT1 u1Protocol)
{
    ClkRegDeRegisterProtocol (u1Protocol);
}

/*****************************************************************************/
/* Function     : PtpPortClkIwfGetClock                                      */
/*                                                                           */
/* Description  : This API will be invoked by PTP whenever the module needs  */
/*                perform  time stamping on the PTP messages that are either */
/*                received or transmitted.                                   */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortClkIwfGetClock (tClkSysTimeInfo * pPtpSysTimeInfo)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tUtlSysPreciseTime  SysPreciseTime;
    tClkSysTimeInfo    *pClkSysTimeInfo = NULL;

    if (ClkIwfGetClock (CLK_MOD_PTP, pPtpSysTimeInfo) != OSIX_SUCCESS)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortClkIwfGetClock:"
                  "Failed To Get System Time!!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }
    else
    {
        /* to synchronize the FASP clock time with system time.         */
        /* FSAP clock lags behind(in secs) system clock after arround   */
        /* 8-10 hours. so for that a trigger is added to update FSAP    */
        /* clock time with system time read */

        MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
        pClkSysTimeInfo = ((tClkSysTimeInfo *) pPtpSysTimeInfo);

        SysPreciseTime.u4Sec = pClkSysTimeInfo->FsClkTimeVal.u8Sec.u4Lo;
        SysPreciseTime.u4NanoSec = pClkSysTimeInfo->FsClkTimeVal.u4Nsec;
        /* system time updation is not required */
        SysPreciseTime.u4SysTimeUpdate = OSIX_FALSE;

        if (UtlSetPreciseSysTime (&SysPreciseTime) != OSIX_SUCCESS)
        {
            PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC, "PtpPortClkIwfGetClock:"
                      "Failed To Set System Time!!!!!\r\n"));
            i4RetVal = OSIX_FAILURE;
        }
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortValidateIfaceTypeAndNum                */
/*                                                                           */
/* Description               : This routine checks the interface type and    */
/*                             validates whether the corresponding interface */
/*                             number is present in the lower layer          */
/*                                                                           */
/* Input                     : u4ContextId        -  Context identifier      */
/*                             i4DeviceType       -  Device type             */
/*                             u4IfIndex          -  Interface number        */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortValidateIfaceTypeAndNum (UINT4 u4ContextId, INT4 i4DeviceType,
                                UINT4 u4IfIndex)
{
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tNetIpv4IfInfo      NetIpIfInfo;
    tCfaIfInfo          IfInfo;
    UINT4               u4IpIndex = 0;
    UINT4               u4PortContextId = 0;

    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    switch (i4DeviceType)
    {
        case PTP_IFACE_UDP_IPV4:

            if ((NetIpv4GetPortFromIfIndex (u4IfIndex, &u4IpIndex)) ==
                NETIPV4_FAILURE)
            {
                PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC, "PtpPortValidateIfaceTypeAndNum:"
                          "Invalid Port Information %d!!!!!\r\n", u4IfIndex));
                CLI_SET_ERR (CLI_PTP_ERR_PORT_INVALID);
                return OSIX_FAILURE;
            }

            if (NetIpv4GetIfInfo (u4IpIndex, &NetIpIfInfo) == NETIPV4_FAILURE)
            {
                PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC, "PtpPortValidateIfaceTypeAndNum:"
                          "Invalid Port Information %d!!!!!\r\n", u4IfIndex));
                CLI_SET_ERR (CLI_PTP_ERR_PORT_INVALID);
                return OSIX_FAILURE;
            }

            break;

        case PTP_IFACE_UDP_IPV6:
#ifdef IP6_WANTED
            if (NetIpv6GetIfInfo (u4IfIndex, &NetIpv6IfInfo) == NETIPV6_FAILURE)
            {
                PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC, "PtpPortValidateIfaceTypeAndNum:"
                          "Invalid Port Information %d!!!!!\r\n", u4IfIndex));
                CLI_SET_ERR (CLI_PTP_ERR_PORT_INVALID);
                return OSIX_FAILURE;
            }
#endif
            break;

        case PTP_IFACE_IEEE_802_3:

            if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_FAILURE)
            {
                PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC, "PtpPortValidateIfaceTypeAndNum:"
                          "Invalid Port Information %d!!!!!\r\n", u4IfIndex));
                CLI_SET_ERR (CLI_PTP_ERR_PORT_INVALID);
                return OSIX_FAILURE;
            }

            /* Check the VCM Context of the Port and Given
             * Context are same
             */
            if (PtpPortVcmGetCxtFromIfIndex
                (u4IfIndex, &u4PortContextId) == OSIX_FAILURE)
            {
                PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC, "PtpPortValidateIfaceTypeAndNum:"
                          "Port %d Not Mapped To Any Context!!!!!\r\n",
                          u4IfIndex));
                CLI_SET_ERR (CLI_PTP_ERR_PORT_INVALID);
                return OSIX_FAILURE;
            }

            if (u4ContextId != u4PortContextId)
            {
                PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC, "PtpPortValidateIfaceTypeAndNum:"
                          "Port %d Mapped Not Mapped to this Context!!!!!\r\n",
                          u4IfIndex));
                CLI_SET_ERR (CLI_PTP_ERR_IF_CXT_DIFFERENT);
                return OSIX_FAILURE;
            }

            break;

        case PTP_IFACE_VLAN:

            if (L2IwfMiIsVlanActive (u4ContextId, (tVlanId) u4IfIndex)
                == OSIX_FALSE)
            {
                PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC, "PtpPortValidateIfaceTypeAndNum:"
                          "Vlan Is Not Active In Port %d!!!!!\r\n", u4IfIndex));
                CLI_SET_ERR (CLI_PTP_ERR_PORT_INVALID);
                return OSIX_FAILURE;
            }

            break;

        default:
            PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC, "PtpPortValidateIfaceTypeAndNum:"
                      "Invalid Device Type %d!!!!!\r\n", i4DeviceType));
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpPortIsVcExists                             */
/*                                                                           */
/* Description               : This routine checks whether the virtual       */
/*                             context is created in the system              */
/*                                                                           */
/* Input                     : u4ContextId        -  Context identifier      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortIsVcExists (UINT4 u4ContextId)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (VcmIsVcExist (u4ContextId) == VCM_FALSE)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortIsVcExists:"
                  "Context Doesnot Exist!!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortHandleOutgoingPktOnPort                */
/*                                                                           */
/* Description               : This routine invokes the L2IWF call to        */
/*                             transmit packet out of the port.              */
/*                                                                           */
/* Input(s)                  : pBuf        - Pointer to the CRU Buffer.      */
/*                             u4IfIndex   - ifIndex of the driver port.     */
/*                             u4PktSize   - Size of the data buffer         */
/*                             u2Protocol  - Protocol type                   */
/*                             u1EncapType - Encapsulation Type              */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                                UINT4 u4PktSize, UINT2 u2Protocol,
                                UINT1 u1EncapType)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT4               u4ContextId = PTP_INV_CONTEXT_ID;

    if (CfaPostPktFromL2 (pBuf, u4IfIndex, u4PktSize, u2Protocol, u1EncapType)
        == CFA_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  DUMP_TRC | ALL_FAILURE_TRC, "PtpPortHandleOutgoingPktOnPort:"
                  "Failed To Post Packet!!!!!\r\n"));
        if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId) ==
            OSIX_SUCCESS)
        {
            PTP_DUMP_TRC (u4ContextId, pBuf, u4PktSize);
        }
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function     : PtpPortCfaGetIfInfo                                        */
/*                                                                           */
/* Description  : This function calls the Cfa module to get the interface    */
/*                informations                                               */
/*                                                                           */
/* Input        : u4IfIndex   - Cfa interface Index                          */
/*              : pIfInfod    - Pointer to tCfaIfInfo                        */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : L2IWF_SUCCESS/L2IWF_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaGetIfInfo (u4IfIndex, pIfInfo));
}

/*****************************************************************************/
/* Function                  : PtpPortTransmitPktOverVlan                    */
/*                                                                           */
/* Description               : This routine invokes transmits the packet over*/
/*                             the interfaces that are members of the given  */
/*                             vlan.                                         */
/*                                                                           */
/* Input(s)                  : pBuf        - Pointer to the CRU Buffer.      */
/*                             u4ContextId - Context Identifier.             */
/*                             VlanId      - ifIndex of the driver port.     */
/*                             u4PktSize   - Size of the data buffer         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortTransmitPktOverVlan (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                            tVlanId VlanId, UINT4 u4PktLen)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    UNUSED_PARAM (u4PktLen);

    if (CfaPostPktOverVlan (pBuf, u4ContextId, VlanId) != CFA_SUCCESS)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  DUMP_TRC | ALL_FAILURE_TRC, "PtpPortTransmitPktOverVlan:"
                  "Failed To Post Packet!!!!!\r\n"));
        PTP_DUMP_TRC (u4ContextId, pBuf, u4PktLen);
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortSetClk                                 */
/*                                                                           */
/* Description               : PTP Module invokes this API to set system time*/
/*                             This routine will invoke the system call to   */
/*                             program the system time.                      */
/*                                                                           */
/* Input(s)                  : pPtpSysTimeInfo - Pointer to timing info.     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortSetClk (tClkSysTimeInfo * pPtpSysTimeInfo, INT4 i4TimeSource)
{
    return (ClkIwfSetClock (pPtpSysTimeInfo, i4TimeSource));
}

/*****************************************************************************/
/* Function                  : PtpPortGetClkParams                           */
/*                                                                           */
/* Description               : PTP Module invokes this API to get the clock  */
/*                             params that includes class, accuracy, variance*/
/*                                                                           */
/* Input(s)                  : None.                                         */
/*                                                                           */
/* Output                    : pClkIwfPrimParams - Primary Parameters.       */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPortGetClkParams (tClkIwfPrimParams * pClkIwfPrimParams)
{
    ClkIwfGetParams (pClkIwfPrimParams);
}

/*****************************************************************************/
/* Function                  : PtpGetAliasName                               */
/*                                                                           */
/* Description               : PTP Module invokes this function to get       */
/*                             Context Alias Name                            */
/* Input(s)                  : u4ContextId - Context identifier              */
/*                             pu1Alias - Context Alias Name                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (VcmGetAliasName (u4ContextId, pu1Alias) == VCM_FAILURE)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortVcmGetAliasName:"
                  "Context Doesnot Exist!!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortGetCfaIfIndexFromPort                  */
/*                                                                           */
/* Description               : This routine will be invoked to obtain the    */
/*                             interface index for the IP interface number   */
/* Input(s)                  : u4port - port identifier                      */
/*                             pu4Ifindex - Interface Index                  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortGetCfaIfIndexFromPort (UINT4 u4Port, UINT4 *pu4IfIndex)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (NetIpv4GetCfaIfIndexFromPort (u4Port, pu4IfIndex) != NETIPV4_SUCCESS)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortGetCfaIfIndexFromPort:"
                  "Local Port %d is not Present!!!!!\r\n", u4Port));
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortVcmIsSwitchExist                       */
/*                                                                           */
/* Description               : This routine will be invoked to obtain the    */
/*                             interface index for the IP interface number   */
/* Input(s)                  : u4ContextId - Context identifier              */
/*                             pu1Alias - Context Alias Name                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4ContextId)
{
    INT4                i4RetVal = OSIX_TRUE;

    if (VcmIsSwitchExist (pu1Alias, pu4ContextId) == VCM_FALSE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortVcmIsSwitchExist:"
                  "Context Doesnot Exist!!!!!\r\n"));
        i4RetVal = OSIX_FALSE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortFmNotifyFaults                         */
/*                                                                           */
/* Description               : This function Sends the trap message to the   */
/*                             Fault Manager.                                */
/*                                                                           */
/* Input(s)                  : u4port - port identifier                      */
/*                             pu4Ifindex - Interface Index                  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPortFmNotifyFaults (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                       UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pTrapMsg,
                       UINT1 *pu1Msg, tSNMP_OCTET_STRING_TYPE * pContextName)
{
#ifdef FM_WANTED
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));
    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);
    UNUSED_PARAM (pContextName);
    /* TOS Code
       FmFaultMsg.pEnterpriseOid = pEnterpriseOid;
     */
    FmFaultMsg.pTrapMsg = pTrapMsg;
    /*
       FmFaultMsg.pCxtName = pContextName;
     */
    FmFaultMsg.pSyslogMsg = pu1Msg;
    /*
       FmFaultMsg.u4GenTrapType = u4GenTrapType;
       FmFaultMsg.u4SpecTrapType = u4SpecTrapType;
     */
    FmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_PTP;

    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, ALL_FAILURE_TRC,
                  "PtpPortFmNotifyFaults : Sending "
                  "Notification to FM failed.\r\n"));
    }
#else
    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);
    SNMP_AGT_FreeVarBindList (pTrapMsg);
    UNUSED_PARAM (pu1Msg);
    UNUSED_PARAM (pContextName);
#endif
    return;
}

/*****************************************************************************/
/* Function                  : PtpPortVcmGetCxtFromIfIndex                   */
/*                                                                           */
/* Description               : This function obtains the virtual context     */
/*                             identifier that is mapped to the interface    */
/*                             index given. This will invoke the VCM API to  */
/*                             obtain the necessary information.             */
/*                                                                           */
/* Input(s)                  : u4port - port identifier                      */
/*                                                                           */
/* Output                    : pu4ContextId - Pointer to context identifier. */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortVcmGetCxtFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2LocalPortId = 0;

    if (VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                      &u2LocalPortId) == VCM_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortGetCfaIfIndexFromPort:"
                  "Port %d Is Mapped To Any Context!!!!!\r\n", u4IfIndex));
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortNetIpv4GetIfInfo                       */
/*                                                                           */
/* Description               : This routine acquires the IP Address for the  */
/*                             given interface index.                        */
/*                                                                           */
/* Input                     : u4IfIndexI   - Interface index.               */
/*                                                                           */
/* Output                    : pNetIpIfInfo - Pointer to NetIp information   */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortNetIpv4GetIfInfo (UINT4 u4IfIndex, tNetIpv4IfInfo * pNetIpIfInfo)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (NetIpv4GetIfInfo (u4IfIndex, pNetIpIfInfo) == NETIPV4_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortNetIpv4GetIfInfo:"
                  "Port %d Is Not Present!!!!!\r\n", u4IfIndex));
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortRegisterNetIpv4                        */
/*                                                                           */
/* Description               : This routine registers the NETIPv4 with the   */
/*                             PTP module.                                   */
/*                                                                           */
/* Input                     : pRegInfo - Registration Structure.            */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortRegisterNetIpv4 (VOID)
{
    tNetIpRegInfo       RegInfo;
    INT4                i4RetVal = OSIX_SUCCESS;

    MEMSET (&RegInfo, 0, sizeof (tNetIpRegInfo));

    RegInfo.u2InfoMask |= NETIPV4_IFCHG_REQ;
    RegInfo.pIfStChng = PtpApiIPIfStatusChgHdlr;
    RegInfo.pRtChng = NULL;
    RegInfo.pProtoPktRecv = NULL;
    RegInfo.u1ProtoId = FS_PROTO_PTP;

    if (NetIpv4RegisterHigherLayerProtocol (&RegInfo) == NETIPV4_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortRegisterNetIpv4:"
                  "NetIpv4RegisterHigherLayerProtocol FAILED!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortDeRegisterNetIpv4                      */
/*                                                                           */
/* Description               : This routine de-registers the NETIPv4 with the*/
/*                             PTP module.                                   */
/*                                                                           */
/* Input                     : u1ProtoId - PTP Protocol Id.                  */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortDeRegisterNetIpv4 (UINT1 u1ProtoId)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    if (NetIpv4DeRegisterHigherLayerProtocol (u1ProtoId) == NETIPV4_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortDeRegisterNetIpv4:"
                  "NetIpv4DeRegisterHigherLayerProtocol FAILED!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortGetSysMacAddress                       */
/*                                                                           */
/* Description               : This routine gets the system MAC Address for  */
/*                             the given context identifier.                 */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier.              */
/*                                                                           */
/* Output                    : SwitchMac - MAC Address.                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPortGetSysMacAddress (UINT4 u4ContextId, tMacAddr SwitchMac)
{
    IssGetContextMacAddress (u4ContextId, SwitchMac);
}

/*****************************************************************************/
/* Function                  : PtpPortGetTimeStampMethod                     */
/*                                                                           */
/* Description               : This routine gets the time stamping method for*/
/*                             the operation of PTP. This will be invoked    */
/*                             during intialization of PTP module. This value*/
/*                             cannot be changed till PTP module is shutdown */
/*                             and restrated. When integrated with ISS, this */
/*                             change will take effect only when ISS is      */
/*                             restarted.                                    */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : TimeStamp - Time Stamp Method.                */
/*                                                                           */
/*****************************************************************************/
PUBLIC              eTimeStamp
PtpPortGetTimeStampMethod (VOID)
{
    UINT2               u2TimeStamp = 0;

    u2TimeStamp = IssGetTimeStampMethodFromNvRam ();

    /* To ensure that the value does not cross the enum max */
    switch (u2TimeStamp)
    {
        case ISS_HARDWARE_TIME_STAMP:

            return PTP_HARDWARE_TIMESTAMPING;

        case ISS_SOFTWARE_TIME_STAMP:
        default:
            /* Intentional fall through */

            return PTP_SOFTWARE_TIMESTAMPING;
    }
}

/*****************************************************************************/
/* Function                  : PtpPortUpdtClkId                              */
/*                                                                           */
/* Description               : This portable API provided by the PTP module  */
/*                             can be used to update the value of clock ID.  */
/*                             Currently PTP module uses only option7.5.2.2.2*/
/*                             of IEEE Std 1588 2008. If options need to be  */
/*                             exploited for generating the clock identifier,*/
/*                             this API can be ported accordingly to produce */
/*                             the desired values of Clock identifier. This  */
/*                             API will be invoked during port creation.     */
/*                                                                           */
/* Input                     : pPtpDomain- Pointer to Domain.                */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPortUpdtClkId (tPtpDomain * pPtpDomain)
{
    UNUSED_PARAM (pPtpDomain);
}

/*****************************************************************************/
/* Function                  : PtpPortNetIpv4GetPortFromIfIndex              */
/*                                                                           */
/* Description               : This routine gets the IP port number for the  */
/*                             corresponding CFA interface index             */
/*                                                                           */
/* Input                     : u4IfIndex  -  CFA Interface index             */
/*                                                                           */
/* Output                    : pu4IpPort  -  IP port number                  */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : NETIPV4_SUCCESS / NETIPV4_FAILURE             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortNetIpv4GetPortFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4IpPort)
{
    return NetIpv4GetPortFromIfIndex (u4IfIndex, pu4IpPort);
}

/*****************************************************************************/
/* Function                  : PtpPortNetIpv6GetIfInfo                       */
/*                                                                           */
/* Description               : This routine acquires the IPv6 Address for    */
/*                             the given interface index.                    */
/*                                                                           */
/* Input                     : u4IfIndexI   - Interface index.               */
/*                                                                           */
/* Output                    : pNetIpIfInfo - Pointer to NetIp information   */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortNetIpv6GetIfInfo (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    INT4                i4RetVal = OSIX_SUCCESS;

#ifdef IP6_WANTED
    if (NetIpv6GetIfInfo (u4IfIndex, pNetIpv6IfInfo) == NETIPV4_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortNetIpv6GetIfInfo:"
                  "Port %d Is Not Present!!!!\r\n", u4IfIndex));
        i4RetVal = OSIX_FAILURE;
    }

#else

    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6IfInfo);
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortRegisterNetIpv6                        */
/*                                                                           */
/* Description               : This routine is used to register the call back*/
/*                             for the PTP module with IPv6 to notify iface  */
/*                             status Change.                                */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortRegisterNetIpv6 (VOID)
{
    INT4                i4RetVal = OSIX_FAILURE;

#ifdef IP6_WANTED
    if (NetIpv6RegisterHigherLayerProtocol (IP6_PTP_PROTOID,
                                            (UINT4)
                                            NETIPV6_INTERFACE_PARAMETER_CHANGE,
                                            PtpApiIpV6IfStatusChgNotify)
        != NETIPV6_SUCCESS)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortRegisterNetIpv6:"
                  "NetIpv6RegisterHigherLayerProtocol FAILED!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortDeRegisterNetIpv6                      */
/*                                                                           */
/* Description               : This routine de-registers the NETIPv6 with the*/
/*                             PTP module.                                   */
/*                                                                           */
/* Input                     : u1ProtoId - PTP Protocol Id.                  */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortDeRegisterNetIpv6 (UINT1 u1ProtoId)
{
    INT4                i4RetVal = OSIX_SUCCESS;

#ifdef IP6_WANTED
    if (NetIpv6DeRegisterHigherLayerProtocol (u1ProtoId) == NETIPV4_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortDeRegisterNetIpv6:"
                  "NetIpv6DeRegisterHigherLayerProtocol FAILED!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u1ProtoId);
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortNetIpv6McastJoin                       */
/*                                                                           */
/* Description               : This routine used to join the multicast       */
/*                             group.                                        */
/*                                                                           */
/* Input                     : u4IfIndex - Inerface Index.                   */
/*                           : pPTPMCastAddr- Ipv6 Mcast address             */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPortNetIpv6McastJoin (UINT4 u4IfIndex, tIp6Addr * pPTPMCastAddr)
{
    INT4                i4RetVal = OSIX_SUCCESS;

#ifdef IP6_WANTED
    if ((NetIpv6McastJoin (u4IfIndex, pPTPMCastAddr)) == NETIPV4_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortNetIpv6McastJoin:"
                  "Failed to Register the Multicast Address To The Port %d !!!!\r\n",
                  u4IfIndex));
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pPTPMCastAddr);
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpPortNetIpv6McastLeave                      */
/*                                                                           */
/* Description               : This routine used leave from the multicast    */
/*                             group.                                        */
/*                                                                           */
/* Input                     : u4IfIndex - Inerface Index.                   */
/*                           : pPTPMCastAddr- Ipv6 Mcast address             */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
PtpPortNetIpv6McastLeave (UINT4 u4IfIndex, tIp6Addr * pPTPMCastAddr)
{
    INT4                i4RetVal = OSIX_SUCCESS;

#ifdef IP6_WANTED
    if ((NetIpv6McastLeave (u4IfIndex, pPTPMCastAddr)) == NETIPV4_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortNetIpv6McastJoin:"
                  "Failed to DeRegister the Multicast Address From The Port %d !!!!\r\n",
                  u4IfIndex));
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pPTPMCastAddr);
#endif

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : PtpPortRmRegisterProtocols                           */
/* Description        : This function calls the RM module to register.       */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
PUBLIC UINT4
PtpPortRmRegisterProtocols (tRmRegParams * pRmReg)
{
    UINT4               u4RetVal = OSIX_SUCCESS;

#ifdef L2RED_WANTED
    if (RmRegisterProtocols (pRmReg) == RM_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortRmRegisterProtocols:"
                  "RmRegisterProtocols FAILED!!!!\r\n"));
        u4RetVal = OSIX_FAILURE;
    }
    /* Default value of Node Status is IDLE */
    gPtpRedGlobalInfo.u1NodeStatus = RM_INIT;
    gPtpRedGlobalInfo.u1NumOfStandbyNodesUp = PtpPortRmGetStandbyNodeCount ();
#else
    UNUSED_PARAM (pRmReg);
#endif

    return u4RetVal;
}

/*****************************************************************************/
/* Function Name      : PtpPortRmDeRegisterProtocols                        */
/* Description        : This function calls the RM module to deregister.     */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
UINT4
PtpPortRmDeRegisterProtocols (VOID)
{
    UINT4               u4RetVal = OSIX_SUCCESS;

#ifdef L2RED_WANTED
    if (RmDeRegisterProtocols (RM_PTP_APP_ID) == RM_FAILURE)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpPortRmDeRegisterProtocols:"
                  "RmDeRegisterProtocols FAILED!!!!\r\n"));
        u4RetVal = OSIX_FAILURE;
    }
#endif
    return u4RetVal;
}

/****************************************************************************
 * Function Name      : PtpPortRmGetStandbyNodeCount
 * Description        : This function calls the RM module to get the number
 *                      of peer nodes that are up.
 * Input(s)           : None
 * Output(s)          : None
 * Return Value(s)    : Number of Booted up standby nodes
 ***************************************************************************/
PUBLIC UINT1
PtpPortRmGetStandbyNodeCount (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return 0;
#endif
}

/*********************************************************************
 * Function Name      : PtpPortRmApiHandleProtocolEvent
 *
 * Description        : This function calls the RM module to intimate about
 *                      the protocol operations
 *
 * Input(s)           : pEvt->u4AppId - Application Id
 *                      pEvt->u4Event - Event send by protocols to RM
 *                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED
 *                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED
 *                                      RM_STANDBY_EVT_PROCESSED)
 *                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL
 *                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *
 *********************************************************************/
PUBLIC VOID
PtpPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt)
{
#ifdef L2RED_WANTED
    RmApiHandleProtocolEvent (pEvt);
#else
    UNUSED_PARAM (pEvt);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : PtpPortRmEnqMsgToRm                                 */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
PtpPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen,
                     UINT4 u4SrcEntId, UINT4 u4DestEntId)
{

#ifdef L2RED_WANTED

    if (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen,
                              u4SrcEntId, u4DestEntId) == RM_FAILURE)
    {

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpRmEnqMsgToRm: Enqueue" "message to RM Failed!!!!\r\n"));

        RM_FREE (pRmMsg);
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    UNUSED_PARAM (u4SrcEntId);
    UNUSED_PARAM (u4DestEntId);
#endif
    return OSIX_SUCCESS;
}

/*********************************************************
 * Function Name      : PtpPortRmGetNodeState
 *
 * Description        : This function calls the RM module
 *                      to get the node state.
 *
 * Input(s)           : None
 *
 * Output(s)          : None
 *
 * Return Value(s)    : RM_ACTIVE/RM_INIT_/RM_STANDBY
 *
 ****************************************************************/
PUBLIC UINT4
PtpPortRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetNodeState ());
#else
    return RM_ACTIVE;
#endif
}

/***************************************************************************
 * FUNCTION NAME    : PtpPortRmApiSendProtoAckToRM
 *
 * DESCRIPTION      : This is the function used by protocols/applications
 *                    to send acknowledgement to RM after processing the
 *                    sync-up message.
 *
 * INPUT            : tRmProtoAck contains
 *                    u4AppId  - Protocol Identifier
 *                    u4SeqNum - Sequence number of the RM message for
 *                    which this ACK is generated.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
PtpPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
    INT4                i4RetVal = OSIX_SUCCESS;
#ifdef L2RED_WANTED
    if (RmApiSendProtoAckToRM (pProtoAck) == RM_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpPortRmApiSendProtoAckToRM:"
                  "Failed To Send Ack To RM!!!!\r\n"));
        i4RetVal = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (pProtoAck);
#endif
    return i4RetVal;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PtpPortRedSendMsgToRm
 *
 *    DESCRIPTION      : This function sends the given protocol(LLDP) message
 *                       to RM
 *
 *    INPUT            : u1MsgType - Message type
 *                       u2MsgLen - Message length
 *                       pRmMsg - Pointer to message
 *
 *    OUTPUT           : NONE
 *
 *    RETURNS          : NONE
 *
 ****************************************************************************/
VOID
PtpPortRedSendMsgToRm (UINT1 u1MsgType, UINT2 u2MsgLen, tRmMsg * pRmMsg)
{
    tRmProtoEvt         ProtoEvt;

#ifdef L2RED_WANTED
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    if (PtpPortRmEnqMsgToRm (pRmMsg, u2MsgLen, RM_PTP_APP_ID, RM_PTP_APP_ID) !=
        OSIX_SUCCESS)
    {
        /* if msg type is,
         * bulk update request or
         * bulk update or
         * bulk update tail
         * then notify about send to fail to RM */
        if ((u1MsgType == RM_BULK_UPDT_REQ_MSG) ||
            (u1MsgType == RM_BULK_UPDATE_MSG) ||
            (u1MsgType == RM_BULK_UPDT_TAIL_MSG) ||
            (u1MsgType == PTP_RED_RELAY_REQUEST_PKT_MSG))
        {
            /* Send bulk update failure event to RM */
            ProtoEvt.u4AppId = RM_PTP_APP_ID;
            ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
            ProtoEvt.u4Error = RM_SENDTO_FAIL;

            PtpPortRmApiHandleProtocolEvent (&ProtoEvt);

            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC | DATA_PATH_TRC |
                      BUFFER_TRC,
                      "PtpPortRedSendMsgToRm: Sending bulk update msg/sync up msg"
                      "to RM failed\r\n"));
        }
    }
#else
    UNUSED_PARAM (ProtoEvt);
    UNUSED_PARAM (u1MsgType);
    UNUSED_PARAM (u2MsgLen);
    UNUSED_PARAM (pRmMsg);
#endif
    return;
}

#endif /*_PTP_C_*/
