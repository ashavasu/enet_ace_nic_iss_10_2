/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpapi.c,v 1.19 2014/05/29 13:19:57 siva Exp $
 *
 * Description: This file contains the APIs exported by PTP module.
 *****************************************************************************/
#ifndef _PTPAPI_C_
#define _PTPAPI_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function                  : PtpApiLock                                    */
/*                                                                           */
/* Description               : This API shall be used to take the mutual     */
/*                             protocol semaphore.                           */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.SemId                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : SNMP_SUCCESS/SNMP_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiLock (VOID)
{
    PTP_FN_ENTRY ();

    if (OsixSemTake (gPtpGlobalInfo.SemId) == OSIX_FAILURE)
    {
        PTP_FN_EXIT ();
        return SNMP_FAILURE;
    }

    PTP_FN_EXIT ();
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpApiUnLock                                  */
/*                                                                           */
/* Description               : This API shall be used to release the mutual  */
/*                             protocol semaphore.                           */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.SemId                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : SNMP_SUCCESS/SNMP_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiUnLock (VOID)
{
    PTP_FN_ENTRY ();

    if (OsixSemGive (gPtpGlobalInfo.SemId) == OSIX_FAILURE)
    {
        PTP_FN_EXIT ();

        return SNMP_FAILURE;
    }

    PTP_FN_EXIT ();

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpApiIfStatusChgHdlr                         */
/*                                                                           */
/* Description               : This API should be invoked by the Interface   */
/*                             manager and VLAN modules to indicate the      */
/*                             interface operational status change and VLAN  */
/*                             status change to the PTP module.              */
/*                                                                           */
/* Input                     : pPtpL2IfStatusChgInfo - Interface related     */
/*                                                     parameters.           */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpApiIfStatusChgHdlr (tPtpL2IfStatusChgInfo * pPtpL2IfStatusChgInfo)
{
    tPtpQMsg           *pMsg = NULL;

    PTP_FN_ENTRY ();

    /* This API will be invoked by the CFA module whenever the operational 
     * status of the interface changes. The same API will be invoked by VLAN 
     * module whenever a VLAN status is changed in the System. This will happen
     * through L2IWF in ISS. This API handles the following 
     * 1) Interface delete
     * 2) Interface operational status change.
     * */

    if (PtpUtilIsPtpStarted (pPtpL2IfStatusChgInfo->u4ContextId) == OSIX_FALSE)
    {
        PTP_FN_EXIT ();
        return;
    }

    if ((pMsg = (tPtpQMsg *) MemAllocMemBlk (gPtpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpApiIfStatusChgHdlr: Allocation "
                  "of memory for Queue Message FAILED !!!\r\n"));

        PTP_FN_EXIT ();
        return;
    }

    MEMSET (pMsg, 0, sizeof (tPtpQMsg));
    /* Message type can take either CFA_IF_UP/CFA_IF_DOWN/CFA_IF_DEL/CFA_IF_CRT
     * */
    pMsg->u1PortOperStatus = pPtpL2IfStatusChgInfo->u1Status;

    if ((pPtpL2IfStatusChgInfo->u1Status == CFA_IF_UP) ||
        (pPtpL2IfStatusChgInfo->u1Status == CFA_IF_DOWN))
    {
        pMsg->u4MsgType = PTP_PORT_STATUS_MSG;
    }
    else if (pPtpL2IfStatusChgInfo->u1Status == CFA_IF_DEL)
    {
        pMsg->u4MsgType = PTP_PORT_DEL_MSG;
    }

    pMsg->u4ContextId = pPtpL2IfStatusChgInfo->u4ContextId;
    pMsg->u4Port = pPtpL2IfStatusChgInfo->u4IfIndex;
    pMsg->PtpDeviceType = pPtpL2IfStatusChgInfo->u1IfType;

    if (PtpQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | BUFFER_TRC,
                  "PtpApiIfStatusChgHdlr: PtpQueEnqMsg "
                  "returned Failure!!!\r\n"));
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpApiIncomingPktHdlr                         */
/*                                                                           */
/* Description               : This API should be invoked to hand over PTP   */
/*                             messages to the PTP module. This API will     */
/*                             indicate the Packet arrival event to the PTP  */
/*                             by an event trigger and post the Packet to the*/
/*                             PTP Queue.                                    */
/*                                                                           */
/* Input                     : pBuf - Pointer to the CRU Buffer containing   */
/*                                    the PTP Packet.                        */
/*                             u4IfIndex - Interface index.                  */
/*                             u4ContextId - Context Identifier              */
/*                             u4PktLen - Received Packet Length.            */
/*                             VlanId   - Vlan Identifier of the received pkt*/
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpApiIncomingPktHdlr (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                       UINT4 u4ContextId, UINT4 u4PktLen, tVlanId VlanId)
{
    tPtpCxt            *pPtpCxt = NULL;
    tPtpQMsg           *pMsg = NULL;
    tPtpPort           *pPtpPort = NULL;
    UINT1               u1Flag = 0;
#ifdef L2RED_WANTED
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2DataLen = 0;
#endif
    UINT1               u1MsgType = 0;
    UINT4               u4MediaHdrLen = 0;

    PTP_FN_ENTRY ();

    if (PtpUtilIsPtpStarted (u4ContextId) == OSIX_FALSE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        PTP_FN_EXIT ();
        return;
    }

    pPtpCxt = PtpCxtGetNode (u4ContextId);
    if (pPtpCxt == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        PTP_FN_EXIT ();
        return;
    }
    if (pPtpCxt->u1AdminStatus == PTP_DISABLED)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpApiIncomingPktHdlr:PTP disabled in Context.Packet discarded\r\n"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        PTP_FN_EXIT ();
        return;
    }
#ifdef L2RED_WANTED
    /*Formaing RM packet and sending to StandBy node */
    if ((gPtpRedGlobalInfo.u1NodeStatus == RM_ACTIVE) &&
        (gPtpRedGlobalInfo.u1NumOfStandbyNodesUp != 0))
    {
        if (PtpProcessValidPtpPktForRm (pBuf) == OSIX_SUCCESS)
        {
            PtpRedFormL2PtpPktForStandby (pBuf, u4IfIndex, u4ContextId,
                                          VlanId, u4PktLen, &pRmMsg);
            if (pRmMsg != NULL)
            {
                u2DataLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pRmMsg);
                PtpRedProcessAndRelayPkt (pRmMsg, u2DataLen);
            }
        }
    }
#endif
    if ((pMsg = (tPtpQMsg *) MemAllocMemBlk (gPtpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpApiIncomingPktHdlr: Allocation "
                  "of memory for Queue Message FAILED !!!\r\n"));

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        PTP_FN_EXIT ();
        return;
    }

    /*Added for pseudo wire support */

    pPtpPort = PtpGetDeviceTypeFromIfIndex (pBuf, u4ContextId, u4IfIndex,
                                            VlanId, &u1Flag);

    if (pPtpPort == NULL)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "pPtpPort is NULL!!!\r\n"));
        MemReleaseMemBlock (gPtpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        PTP_FN_EXIT ();
        return;
    }

    if (u1Flag == PTP_IFACE_IEEE_802_3)
    {
        VlanId = 0;
    }
    MEMSET (pMsg, 0, sizeof (tPtpQMsg));

    pMsg->u4ContextId = u4ContextId;
    pMsg->u4MsgType = PTP_PDU_ENQ_MSG;
    pMsg->u4Port = u4IfIndex;
    pMsg->u4PktLen = u4PktLen;
    pMsg->VlanId = VlanId;
    pMsg->PtpDeviceType = PTP_IFACE_IEEE_802_3;
    if (pMsg->VlanId != VLAN_NULL_VLAN_ID)
    {
        pMsg->PtpDeviceType = PTP_IFACE_VLAN;
    }
    pMsg->uPtpMsgParam.pPtpPdu = pBuf;

    u4MediaHdrLen = PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1MsgType,
                               (PTP_MSG_TYPE_OFFSET + u4MediaHdrLen), 1);

    /* We have received a PTP message. Generate a time stamp for this message   
     * The time stamp will be used within PTP, if the message is an event   
     * message. In case of general messages, the time stamp can be ignored.   
     * */
    if ((u1MsgType == PTP_SYNC_MSG) || (u1MsgType == PTP_DELAY_REQ_MSG) ||
        (u1MsgType == PTP_PEER_DELAY_REQ_MSG)
        || (u1MsgType == PTP_PEER_DELAY_RESP_MSG))
    {
        if (PtpClkGetTimeStamp (pPtpPort, (pMsg->uPtpMsgParam.pPtpPdu),
                                &(pMsg->PtpSysTimeInfo), u4IfIndex,
                                PTP_MODE_RX) == OSIX_FAILURE)
        {
            PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | DUMP_TRC,
                      "PtpApiIncomingPktHdlr: Unable to "
                      "time stamp the PTP packet!!!\r\n"));
            PTP_DUMP_TRC (u4ContextId, pBuf, u4PktLen);
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            MemReleaseMemBlock (gPtpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);

            PTP_FN_EXIT ();
            return;
        }
    }
    if (PtpQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | BUFFER_TRC,
                  "PtpApiIncomingPktHdlr: PtpQueEnqMsg "
                  "returned Failure!!!\r\n"));
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpApiContextStatusChgHdlr                    */
/*                                                                           */
/* Description               : This API will be registered with VCM to rx the*/
/*                             context create or delete indication. This API */
/*                             will trigger an event to the PTP module       */
/*                             regarding the context create or delete        */
/*                             indication.                                   */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier.             */
/*                             u4Operation - Indicates the context create or */
/*                                           delete operation.               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiContextStatusChgHdlr (UINT4 u4ContextId, UINT4 u4Operation)
{
    tPtpQMsg           *pMsg = NULL;

    PTP_FN_ENTRY ();

    if (PtpUtilIsPtpStarted (u4ContextId) == OSIX_FALSE)
    {
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    if ((pMsg = (tPtpQMsg *) MemAllocMemBlk (gPtpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "PtpApiContextStatusChgHdlr: "
                  "Allocation of memory for Queue Message FAILED !!!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tPtpQMsg));

    pMsg->u4ContextId = u4ContextId;
    pMsg->u4MsgType = u4Operation;

    /* Used for L3 context
       if (u4Operation == VCM_CONTEXT_DELETE)
       {
       pMsg->u4MsgType = PTP_CTXT_DELETE_MSG;
       }
     */

    if (PtpQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | BUFFER_TRC,
                  "PtpApiContextStatusChgHdlr: PtpQueEnqMsg "
                  "returned Failure!!!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpApiIPIfStatusChgHdlr                       */
/*                                                                           */
/* Description               : This API should be invoked by the IP module to*/
/*                             indicate the IP interface operational status  */
/*                             change to the PTP module. This API will be    */
/*                             invoked by the IP module whenever the         */
/*                             operational status of the IP interfaces       */
/*                             changes.                                      */
/*                                                                           */
/* Input                     : pNetIpv4IfInfo - Pointer to IP interface.     */
/*                             u4BitMap       - Bit map indicating the option*/
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpApiIPIfStatusChgHdlr (tNetIpv4IfInfo * pNetIpv4IfInfo, UINT4 u4BitMap)
{
    tPtpL2IfStatusChgInfo PtpL2IfStatusChgInfo;

    PTP_FN_ENTRY ();

    if (PtpUtilIsPtpStarted (L2IWF_DEFAULT_CONTEXT) == OSIX_FALSE)
    {
        PTP_TRC ((L2IWF_DEFAULT_CONTEXT, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpApiIPIfStatusChgHdlr: Module Not Started!!!\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    if (((u4BitMap & OPER_STATE) != OPER_STATE) &&
        ((u4BitMap & IFACE_DELETED) != IFACE_DELETED))
    {
        PTP_TRC ((L2IWF_DEFAULT_CONTEXT, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpApiIPIfStatusChgHdlr: Invalid Indicaion from IP Module!!!\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* For PTP, IP is also another type of interface and hence just invoking 
     * the interface status change handler */

    MEMSET (&(PtpL2IfStatusChgInfo), 0, sizeof (tPtpL2IfStatusChgInfo));

    PtpL2IfStatusChgInfo.u4IfIndex = pNetIpv4IfInfo->u4IfIndex;
    if ((u4BitMap & OPER_STATE) == OPER_STATE)
    {
        PtpL2IfStatusChgInfo.u1Status = (UINT1) pNetIpv4IfInfo->u4Oper;
    }
    else
    {
        PtpL2IfStatusChgInfo.u1Status = CFA_IF_DEL;
    }
    PtpL2IfStatusChgInfo.u4ContextId = L2IWF_DEFAULT_CONTEXT;
    PtpL2IfStatusChgInfo.u1IfType = PTP_IFACE_UDP_IPV4;

    PtpApiIfStatusChgHdlr (&PtpL2IfStatusChgInfo);

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpApiClkIwfCbkFn                             */
/*                                                                           */
/* Description               : This API should be invoked by Clock Iwf module*/
/*                             whenever there is a change in the primary     */
/*                             parameters. These include variance, accuracy, */
/*                             class, utcoffset  and holdover specification  */
/*                             of the clock.                                 */
/*                                                                           */
/* Input                     : pPtpPrimaryParams - Primary parameters.       */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiClkIwfCbkFn (tClkIwfPrimParams * pClkIwfPrimParams)
{
    tPtpQMsg           *pMsg = NULL;

    PTP_FN_ENTRY ();

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_FN_EXIT ();
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "PtpApiClkIwfCbkFn: "
                  "Module Is ShutDown!!!\r\n"));
        return OSIX_SUCCESS;
    }

    if ((pMsg = (tPtpQMsg *) MemAllocMemBlk (gPtpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpApiClkIwfCbkFn: "
                  "Allocation of memory for Queue Message FAILED !!!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tPtpQMsg));

    pMsg->uPtpMsgParam.pPtpMsg =
        (void *) MemBuddyAlloc
        ((UINT1) gPtpGlobalInfo.i4PktBuddyId, sizeof (tClkIwfPrimParams));

    if (pMsg->uPtpMsgParam.pPtpMsg == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpApiClkIwfCbkFn: "
                  "Allocation of memory for PTP Message FAILED !!!\r\n"));
        MemReleaseMemBlock (gPtpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMCPY ((tClkIwfPrimParams *) pMsg->uPtpMsgParam.pPtpMsg,
            pClkIwfPrimParams, sizeof (tClkIwfPrimParams));

    pMsg->u4MsgType = PTP_PRIMARY_PARAMS_CHG;

    if (PtpQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | BUFFER_TRC,
                  "PtpApiClkIwfCbkFn: PtpQueEnqMsg "
                  "returned Failure!!!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpApiPktRcvdOnSocket                         */
/*                                                                           */
/* Description               : This routine should be invoked when packet is */
/*                             received on the socket. This message posts    */
/*                             an event with the socket identifier to the    */
/*                             PTP Task.                                     */
/*                                                                           */
/* Input                     : i4SockId - Socket Identifier.                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpApiPktRcvdOnSocket (INT4 i4SockId)
{
    INT4                i4Event = 0;

    PTP_FN_ENTRY ();

    i4Event = PtpUdpDetEventForSocket (i4SockId);
    PtpQuePostEventToPtpTask (i4Event);

    PTP_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function                  : PtpApiIfDeleteCallBack                        */
/*                                                                           */
/* Description               : This is the callback API provided by the PTP  */
/*                             module to the Interface manager in the system */
/*                             to notify about interface deletion that has   */
/*                             occured in the system. In ISS, this will be   */
/*                             invoked by the CFA module.                    */
/*                                                                           */
/* Input                     : pCfaRegInfo - Pointer to the CFA interface    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : CFA_SUCCESS/CFA_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiIfDeleteCallBack (tCfaRegInfo * pCfaRegInfo)
{
    tPtpL2IfStatusChgInfo PtpL2IfStatusChgInfo;

    PTP_FN_ENTRY ();

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        /* PTP module itself is not initialized. */
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "PtpApiIfDeleteCallBack: "
                  "Module Is ShutDown!!!\r\n"));
        PTP_FN_EXIT ();
        return CFA_SUCCESS;
    }

    MEMSET (&PtpL2IfStatusChgInfo, 0, sizeof (tPtpL2IfStatusChgInfo));

    PtpL2IfStatusChgInfo.u4IfIndex = pCfaRegInfo->u4IfIndex;
    PtpL2IfStatusChgInfo.u1IfType = PTP_IFACE_IEEE_802_3;
    PtpL2IfStatusChgInfo.u1Status = CFA_IF_DEL;

    if (PtpPortVcmGetCxtFromIfIndex (pCfaRegInfo->u4IfIndex,
                                     &(PtpL2IfStatusChgInfo.u4ContextId))
        == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpApiIfDeleteCallBack: "
                  "Port Is Not Mapped With Any Context!!!\r\n"));
        PTP_FN_EXIT ();

        return CFA_FAILURE;
    }

    PtpApiIfStatusChgHdlr (&PtpL2IfStatusChgInfo);

    PTP_FN_EXIT ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpApiIfOperChgCallBack                       */
/*                                                                           */
/* Description               : This is the callback API provided by the PTP  */
/*                             module to the Interface manager in the system */
/*                             to notify about interface status change that  */
/*                             occured for the interface.                    */
/*                                                                           */
/* Input                     : pCfaRegInfo - Pointer to the CFA interface    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : CFA_SUCCESS/CFA_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiIfOperChgCallBack (tCfaRegInfo * pCfaRegInfo)
{
    tPtpL2IfStatusChgInfo PtpL2IfStatusChgInfo;

    PTP_FN_ENTRY ();

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        /* PTP module itself is not initialized. */
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "PtpApiIfOperChgCallBack: "
                  "Module Is ShutDown!!!\r\n"));
        PTP_FN_EXIT ();
        return CFA_SUCCESS;
    }

    MEMSET (&PtpL2IfStatusChgInfo, 0, sizeof (tPtpL2IfStatusChgInfo));

    PtpL2IfStatusChgInfo.u4IfIndex = pCfaRegInfo->u4IfIndex;
    PtpL2IfStatusChgInfo.u1IfType = PTP_IFACE_IEEE_802_3;
    /* Oper up indication will be carried in CFA_IF_UP &
     * Oper down indication will be carried in CFA_IF_DOWN
     * */
    PtpL2IfStatusChgInfo.u1Status =
        pCfaRegInfo->uCfaRegParams.CfaInterfaceInfo.u1IfOperStatus;

    if (PtpPortVcmGetCxtFromIfIndex (pCfaRegInfo->u4IfIndex,
                                     &(PtpL2IfStatusChgInfo.u4ContextId))
        == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpApiIfOperChgCallBack: "
                  "Port Is Not Mapped With Any Context!!!\r\n"));
        PTP_FN_EXIT ();

        return CFA_FAILURE;
    }

    PtpApiIfStatusChgHdlr (&PtpL2IfStatusChgInfo);

    PTP_FN_EXIT ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpApiPktRxCallBack                           */
/*                                                                           */
/* Description               : This is the callback API provided by the PTP  */
/*                             module to the Interface manager in the system */
/*                             to notify about PTP Packet reception event    */
/*                             occurence.                                    */
/*                                                                           */
/* Input                     : pCfaRegInfo - Pointer to the CFA interface    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : CFA_SUCCESS/CFA_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiPktRxCallBack (tCfaRegInfo * pCfaRegInfo)
{
    UINT4               u4ContextId = 0;
    UINT4               u4PktLen = 0;

    PTP_FN_ENTRY ();

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        /* PTP module itself is not initialized. */
        /* Should we invoke DeInit here? */
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "PtpApiPktRxCallBack: "
                  "Module Is ShutDown!!!\r\n"));
        CRU_BUF_Release_MsgBufChain (pCfaRegInfo->uCfaRegParams.CfaBufInfo.pBuf,
                                     FALSE);
        PTP_FN_EXIT ();
        return CFA_SUCCESS;
    }

    if (PtpPortVcmGetCxtFromIfIndex (pCfaRegInfo->u4IfIndex, &u4ContextId)
        == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpApiPktRxCallBack: "
                  "Port Is Not Mapped With Any Context!!!\r\n"));
        CRU_BUF_Release_MsgBufChain (pCfaRegInfo->uCfaRegParams.CfaBufInfo.pBuf,
                                     FALSE);
        PTP_FN_EXIT ();
        return CFA_FAILURE;
    }

    /* The length of the received PDU is placed in the message itself.
     * Message length offset equals 2. This API will be invoked, only when
     * PTP operates over Layer 2 medium. Hence, adding the LLC Header size
     * also.
     * */
    u4PktLen =
        CRU_BUF_Get_ChainValidByteCount
        (pCfaRegInfo->uCfaRegParams.CfaBufInfo.pBuf);

    PtpApiIncomingPktHdlr (pCfaRegInfo->uCfaRegParams.CfaBufInfo.pBuf,
                           pCfaRegInfo->u4IfIndex, u4ContextId, u4PktLen,
                           pCfaRegInfo->uCfaRegParams.CfaBufInfo.VlanId);
    PTP_FN_EXIT ();

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpApiTriggerPtpFollowUp                      */
/*                                                                           */
/* Description               : This is the callback API provided by the PTP  */
/*                             module to the Interface manager in the system */
/*                             to notify about PTP Packet reception event    */
/*                             occurence.                                    */
/*                                                                           */
/* Input                     : pCfaRegInfo - Pointer to the CFA interface    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : CFA_SUCCESS/CFA_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiTriggerPtpFollowUp (UINT4 u4ContextId, UINT4 u4IfIndex,
                          tFsClkTimeVal * pFsClkTimeVal, UINT1 u1DomainId,
                          UINT1 u1MsgType)
{
    tPtpQMsg           *pMsg = NULL;

    PTP_FN_ENTRY ();

    if ((pMsg = (tPtpQMsg *) MemAllocMemBlk (gPtpGlobalInfo.QMsgPoolId))
        == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | PTP_CRITICAL_TRC,
                  "PtpApiTriggerPtpFollowUp : Memory allocation "
                  "for Queue Message FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    pMsg->u4ContextId = u4ContextId;
    pMsg->u4Port = u4IfIndex;
    MEMCPY (&(pMsg->PtpSysTimeInfo.FsClkTimeVal), pFsClkTimeVal,
            sizeof (tFsClkTimeVal));

    pMsg->u4MsgType = PTP_FOLLOW_UP_TRIGGER;
    pMsg->u1DomainId = u1DomainId;
    pMsg->uPtpMsgParam.u1MsgType = u1MsgType;

    if (PtpQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                  PTP_MAX_DOMAINS, ALL_FAILURE_TRC,
                  "PtpQueEnqMsg function failed\r\n"));
    }

    PTP_FN_EXIT ();

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpApiIpV6IfStatusChgNotify                   */
/*                                                                           */
/* Description               : This is the callback API provided by the PTP  */
/*                             module to the IPV6 Module. This API will be   */
/*                             invoked whenever there is a change in the IPv6*/
/*                             interface status change.                      */
/*                                                                           */
/* Input                     : pNetIpv6HlParams - Ipv6 Parms.                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : CFA_SUCCESS/CFA_FAILURE                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpApiIpV6IfStatusChgNotify (tNetIpv6HliParams * pNetIpv6HlParams)
{
    tPtpL2IfStatusChgInfo PtpL2IfStatusChgInfo;

    PTP_FN_ENTRY ();

    MEMSET (&PtpL2IfStatusChgInfo, 0, sizeof (tPtpL2IfStatusChgInfo));

    /* Currently IPV6 support is present only for default context */
    if (PtpUtilIsPtpStarted (L2IWF_DEFAULT_CONTEXT) == OSIX_FALSE)
    {
        PTP_TRC ((L2IWF_DEFAULT_CONTEXT, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "PtpApiIpV6IfStatusChgNotify: "
                  "Module Is ShutDown!!!\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* For PTP, IP is also another type of interface and hence just invoking 
     * the interface status change handler */

    MEMSET (&(PtpL2IfStatusChgInfo), 0, sizeof (tPtpL2IfStatusChgInfo));

    PtpL2IfStatusChgInfo.u4IfIndex =
        pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4Index;

    if (pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4IfStat ==
        NETIPV6_IF_UP)
    {
        PtpL2IfStatusChgInfo.u1Status = CFA_IF_UP;
    }
    else if (pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4IfStat ==
             NETIPV6_IF_DOWN)
    {
        PtpL2IfStatusChgInfo.u1Status = CFA_IF_DOWN;
    }
    else if (pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4IfStat ==
             NETIPV6_IF_DELETE)
    {
        PtpL2IfStatusChgInfo.u1Status = CFA_IF_DEL;
    }

    PtpL2IfStatusChgInfo.u4ContextId = L2IWF_DEFAULT_CONTEXT;
    PtpL2IfStatusChgInfo.u1IfType = PTP_IFACE_UDP_IPV6;

    PtpApiIfStatusChgHdlr (&PtpL2IfStatusChgInfo);

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpApiIsTwoStepClock                          */
/*                                                                           */
/* Description               : This routine is used to verify if two-step    */
/*                             flag is enabled on a port or not              */
/*                                                                           */
/* Input                     : u4Port - Port Id.                             */
/*                             u1Domain - Domain Id.                         */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiIsTwoStepClock (UINT4 u4CxtId, UINT4 u4Port, UINT1 u1Domain)
{

    tPtpPort           *pPtpPort = NULL;

    PTP_FN_ENTRY ();
    pPtpPort = PtpIfGetPortEntry (u4CxtId, u1Domain, u4Port);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "PtpApiPktIsTwoStepClock: "
                  "Port Entry does not exist !!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FALSE;
    }

    if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag ==
        PTP_ENABLED)
    {
        PTP_FN_EXIT ();
        return OSIX_TRUE;
    }
    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              ALL_FAILURE_TRC, "PtpApiPktIsTwoStepClock: "
              "PTP Is Disabled In Port!!!\r\n"));
    PTP_FN_EXIT ();
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function                  : PtpApiG8261ComputeDelay                       */
/*                                                                           */
/* Description               : This routine is used to compute the delay     */
/*                             based on delay parametes in G8261 test        */
/*                             simulator                                     */
/*                                                                           */
/* Input                     :                                               */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
PtpApiG8261ComputeDelay (FS_UINT8 * pu8Delay)
{
    FS_UINT8            u8FwdDelay;
    FS_UINT8            u8ReverseDelay;
    FS_UINT8            u8Temp;
    FS_UINT8            u8Result;

    FSAP_U8_CLR (&u8Temp);
    FSAP_U8_CLR (&u8FwdDelay);
    FSAP_U8_CLR (&u8ReverseDelay);
    FSAP_U8_CLR (&u8Result);

    PTP_FN_ENTRY ();

    if ((gDelayParams[G8261TS_FORWARD - 1].u4NwDisturbance != 0) ||
        (gDelayParams[G8261TS_FORWARD - 1].u4Load != 0))
    {

        if (gDelayParams[G8261TS_FORWARD - 1].u4NwDisturbance != 0)
        {
            u8Temp.u4Lo =
                ((gDelayParams[G8261TS_FORWARD - 1].u4NwDisturbance *
                  G8261TS_NWDIST_FACTOR) / G8261TS_PERCENT);
            FSAP_U8_ADD (&u8FwdDelay, &u8FwdDelay, &u8Temp);
        }

        if (gDelayParams[G8261TS_FORWARD - 1].u4Load != 0)
        {
            u8Temp.u4Lo =
                ((gDelayParams[G8261TS_FORWARD - 1].u4Load *
                  G8261TS_LOAD_FACTOR) / G8261TS_PERCENT);
            FSAP_U8_ADD (&u8FwdDelay, &u8FwdDelay, &u8Temp);
        }

        if (gDelayParams[G8261TS_FORWARD - 1].u4PacketSize != 0)
        {
            u8Temp.u4Lo = (G8261TS_PKTSIZE_FACTOR *
                           (gDelayParams[G8261TS_FORWARD - 1].u4PacketSize /
                            G8261TS_DEF_PKTSIZE));
            FSAP_U8_ADD (&u8FwdDelay, &u8FwdDelay, &u8Temp);
        }

        if (gDelayParams[G8261TS_FORWARD - 1].u1FlowVariation ==
            G8261TS_INCREASING_FLOW)
        {
            u8Temp.u4Lo = G8261TS_INCREASING_FLOW;
            FSAP_U8_MUL (&u8Result, &u8FwdDelay, &u8Temp);
            FSAP_U8_ADD (&u8FwdDelay, &u8FwdDelay, &u8Result);
        }

        else if (gDelayParams[G8261TS_FORWARD - 1].u1FlowVariation ==
                 G8261TS_DECREASING_FLOW)
        {
            u8Temp.u4Lo = G8261TS_DECREASING_FLOW;
            FSAP_U8_MUL (&u8Result, &u8FwdDelay, &u8Temp);
            FSAP_U8_ADD (&u8FwdDelay, &u8FwdDelay, &u8Result);
        }

        if (gDelayParams[G8261TS_FORWARD - 1].u4SwitchCount != 0)
        {
            u8Temp.u4Lo = gDelayParams[G8261TS_FORWARD - 1].u4SwitchCount;
            FSAP_U8_MUL (&u8FwdDelay, &u8FwdDelay, &u8Temp);
        }
    }

    if ((gDelayParams[G8261TS_REVERSE - 1].u4NwDisturbance != 0) ||
        (gDelayParams[G8261TS_REVERSE - 1].u4Load != 0))
    {

        if (gDelayParams[G8261TS_REVERSE - 1].u4NwDisturbance != 0)
        {
            u8Result.u4Lo =
                ((gDelayParams[G8261TS_REVERSE - 1].u4NwDisturbance *
                  G8261TS_NWDIST_FACTOR) / G8261TS_PERCENT);
            FSAP_U8_CLR (&u8Temp);
            u8Temp.u4Lo = G8261TS_REVERSE_FACTOR;
            FSAP_U8_MUL (&u8ReverseDelay, &u8Result, &u8Temp);
        }
        if (gDelayParams[G8261TS_REVERSE - 1].u4Load != 0)
        {
            FSAP_U8_CLR (&u8Result);
            u8Result.u4Lo =
                ((gDelayParams[G8261TS_REVERSE - 1].u4Load *
                  G8261TS_LOAD_FACTOR) / G8261TS_PERCENT);
            FSAP_U8_CLR (&u8Temp);
            u8Temp.u4Lo = G8261TS_REVERSE_FACTOR;
            FSAP_U8_MUL (&u8ReverseDelay, &u8Result, &u8Temp);
        }
        if (gDelayParams[G8261TS_REVERSE - 1].u4PacketSize != 0)
        {
            FSAP_U8_CLR (&u8Result);
            u8Result.u4Lo = (G8261TS_PKTSIZE_FACTOR *
                             (gDelayParams[G8261TS_REVERSE - 1].u4PacketSize /
                              G8261TS_DEF_PKTSIZE));
            FSAP_U8_ADD (&u8ReverseDelay, &u8ReverseDelay, &u8Result);
        }
        if (gDelayParams[G8261TS_REVERSE - 1].u1FlowVariation ==
            G8261TS_INCREASING_FLOW)
        {
            FSAP_U8_CLR (&u8Result);
            FSAP_U8_CLR (&u8Temp);
            u8Temp.u4Lo = G8261TS_INCREASING_FLOW;
            FSAP_U8_MUL (&u8ReverseDelay, &u8ReverseDelay, &u8Temp);
        }

        else if (gDelayParams[G8261TS_REVERSE - 1].u1FlowVariation ==
                 G8261TS_DECREASING_FLOW)
        {
            FSAP_U8_CLR (&u8Temp);
            u8Temp.u4Lo = G8261TS_DECREASING_FLOW;
            FSAP_U8_MUL (&u8ReverseDelay, &u8ReverseDelay, &u8Temp);
        }

        else if (gDelayParams[G8261TS_REVERSE - 1].u4SwitchCount != 0)
        {
            FSAP_U8_CLR (&u8Temp);
            u8Temp.u4Lo = gDelayParams[G8261TS_REVERSE - 1].u4SwitchCount;
            FSAP_U8_MUL (&u8ReverseDelay, &u8ReverseDelay, &u8Temp);
        }
    }
    FSAP_U8_ADD (&(gDelayOutput.u8Delay), &u8FwdDelay, &u8ReverseDelay);
    pu8Delay = &(gDelayOutput.u8Delay);

    UNUSED_PARAM (pu8Delay);
    PTP_FN_EXIT ();
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function                  : PtpApiG8261GetDelay                           */
/*                                                                           */
/* Description               : This routine is used to get the delay         */
/*                             calculated by the G8261 test simulator        */
/*                                                                           */
/* Input                     :                                               */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_TRUE/OSIX_FALSE.                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiG8261GetDelay (FS_UINT8 * pu8Delay)
{

    PTP_FN_ENTRY ();
    pu8Delay->u4Lo = gDelayOutput.u8Delay.u4Lo;
    PTP_FN_EXIT ();
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function                  : PtpApiUpdateDelayOriginTime                   */
/*                                                                           */
/* Description               : This routine is used to update the delay      */
/*                             origin time in PtpPort data structure         */
/*                                                                           */
/* Input                     : DelayOriginTime - Time at which PTP           */
/*                             delay request message was transmitted.        */
/*                             u1MsgType - Delay request /Peer delay request */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiUpdateDelayOriginTime (UINT4 u4CxtId, UINT4 u4Port,
                             tFsClkTimeVal FsClkTimeVal, UINT1 u1Domain,
                             UINT1 u1MsgType)
{
    tPtpPort           *pPtpPort = NULL;

    PTP_FN_ENTRY ();
    pPtpPort = PtpIfGetPortEntry (u4CxtId, u1Domain, u4Port);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpApiUpdateDelayOriginTime: "
                  "Port Entry does not exist !!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (u1MsgType == PTP_DELAY_REQ_MSG)
    {
        MEMCPY (&(pPtpPort->DelayReqOriginTime), &FsClkTimeVal,
                sizeof (tFsClkTimeVal));
    }
    else if (u1MsgType == PTP_PEER_DELAY_REQ_MSG)
    {
        MEMCPY (&(pPtpPort->PeerDelayOriginTime), &FsClkTimeVal,
                sizeof (tFsClkTimeVal));
    }
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpApiGetStatus                               */
/*                                                                           */
/* Description               : This routine is used to get the admin  status */
/*                             of PTP                                        */
/*                                                                           */
/* Input                     : *pi4Status - Pointer to status of  PTP        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpApiGetStatus (INT4 *pi4Status)
{
    tPtpCxt            *pPtpCxt = NULL;

    pPtpCxt = PtpCxtGetNode (L2IWF_DEFAULT_CONTEXT);

    if (pPtpCxt != NULL)
    {
        *pi4Status = (INT4) pPtpCxt->u1AdminStatus;
        return OSIX_SUCCESS;
    }

    PTP_TRC ((L2IWF_DEFAULT_CONTEXT, PTP_MAX_DOMAINS,
              ALL_FAILURE_TRC | CONTROL_PLANE_TRC, "PtpApiGetStatus: "
              "Context Not Present !!!\r\n"));
    return OSIX_FAILURE;
}
#endif /*_PTPAPI_C_*/
