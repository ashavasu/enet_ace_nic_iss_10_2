
/**********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fsG826wr.c,v 1.2 2012/08/13 09:23:28 siva Exp $
 *
 * Description: This has wrapper functions
 *
 ***********************************************************************/

#include  "ptpincs.h"
# include  "fsG826wr.h"
# include  "fsG826db.h"


VOID RegisterFSG826 ()
{
        SNMPRegisterMibWithLock (&fsG826OID, &fsG826Entry, PtpApiLock, PtpApiUnLock,
                             SNMP_MSR_TGR_TRUE);
	SNMPAddSysorEntry (&fsG826OID, (const UINT1 *) "fsG8261Ts");
}



VOID UnRegisterFSG826 ()
{
	SNMPUnRegisterMib (&fsG826OID, &fsG826Entry);
	SNMPDelSysorEntry (&fsG826OID, (const UINT1 *) "fsG8261Ts");
}

INT4 FsG8261TsModuleStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsG8261TsModuleStatus(&(pMultiData->i4_SLongValue)));
}
INT4 FsG8261TsModuleStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhSetFsG8261TsModuleStatus(pMultiData->i4_SLongValue));
}


INT4 FsG8261TsModuleStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhTestv2FsG8261TsModuleStatus(pu4Error, pMultiData->i4_SLongValue));
}


INT4 FsG8261TsModuleStatusDep(UINT4 *pu4Error,tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsG8261TsModuleStatus(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsG8261TsParamsTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsG8261TsParamsTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsG8261TsParamsTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsG8261TsSwitchCountGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsG8261TsParamsTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsG8261TsSwitchCount(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsG8261TsPacketSizeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsG8261TsParamsTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsG8261TsPacketSize(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsG8261TsLoadGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsG8261TsParamsTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsG8261TsLoad(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsG8261TsNetworkDisturbanceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsG8261TsParamsTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsG8261TsNetworkDisturbance(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsG8261TsDurationGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsG8261TsParamsTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsG8261TsDuration(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsG8261TsFlowVariationFactorGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsG8261TsParamsTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsG8261TsFlowVariationFactor(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsG8261TsFlowIntervalGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsG8261TsParamsTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsG8261TsFlowInterval(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsG8261TsFrequencyGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsG8261TsParamsTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsG8261TsFrequency(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsG8261TsSwitchCountSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsG8261TsSwitchCount(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsPacketSizeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsG8261TsPacketSize(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsLoadSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsG8261TsLoad(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsNetworkDisturbanceSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsG8261TsNetworkDisturbance(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsDurationSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsG8261TsDuration(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsFlowVariationFactorSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsG8261TsFlowVariationFactor(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsG8261TsFlowIntervalSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsG8261TsFlowInterval(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsFrequencySet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsG8261TsFrequency(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsSwitchCountTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsG8261TsSwitchCount(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsPacketSizeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsG8261TsPacketSize(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsLoadTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsG8261TsLoad(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsNetworkDisturbanceTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsG8261TsNetworkDisturbance(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsDurationTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsG8261TsDuration(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsFlowVariationFactorTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsG8261TsFlowVariationFactor(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsG8261TsFlowIntervalTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsG8261TsFlowInterval(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsFrequencyTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsG8261TsFrequency(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsG8261TsParamsTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsG8261TsParamsTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4 FsG8261TsMeanVarianceGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsG8261TsMeanVariance(pMultiData->pOctetStrValue));
}
INT4 FsG8261TsAccuracyGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsG8261TsAccuracy(&(pMultiData->i4_SLongValue)));
}
INT4 FsG8261TsDelayGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	UNUSED_PARAM(pMultiIndex);
	return(nmhGetFsG8261TsDelay(pMultiData->pOctetStrValue));
}
