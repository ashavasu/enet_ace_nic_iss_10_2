/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpsem.c,v 1.6 2014/01/30 12:20:35 siva Exp $
 *
 * Description: This file contains PTP State Event machine implementation 
 *********************************************************************/
#ifndef _PTPSEM_C_
#define _PTPSEM_C_
#include "ptpincs.h"
#include "ptpsem.h"

PRIVATE VOID        PtpSemInitDomainPorts (tPtpDomain * pPtpDomain,
                                           UINT4 u4PortNumber);
PRIVATE VOID        PtpSemMakeUnCalibrated (tPtpPort * pPtpPort);
PRIVATE VOID        PtpSemUpdtDataSetOnAnncRcpTmOut (tPtpPort * pPtpPort);
PRIVATE VOID        PtpSemMakeMaster (tPtpPort * pPtpPort);

/*****************************************************************************/
/* Function                  : PtpSemPortSemHandler                          */
/*                                                                           */
/* Description               : This State-Event handler invokes the exact    */
/*                             action handler routine based on the current   */
/*                             State and the provisioned event.              */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                             u1Event  - Event occurred                     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gau1_psm_table,ga_psm_func                    */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpSemPortSemHandler (tPtpPort * pPtpPort, UINT1 u1Event)
{
    UINT1               u1CurrentState = pPtpPort->PortDs.u1PortState;
    tPtpNotifyInfo      PtpNotifyInfo;

    PTP_FN_ENTRY ();

    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));
    if (u1Event >= PTP_MAX_EVENTS)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "Port %u PtpSemPortSemHandler invoked with invalid "
                  "event.\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_FN_EXIT ();
        return;
    }
    /* Do we need this? Seems Klockworks might need this */
    if (u1CurrentState >= PTP_STATE_MAX_STATES)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "Port %u PtpSemPortSemHandler "
                  "invoked with invalid "
                  "state.\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "Exiting....\r\n"));
        PTP_FN_EXIT ();
        return;
    }
    if (gau1_psm_table[u1Event][u1CurrentState] >= PTP_MAX_PSM_FUNC)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "Port %u PtpSemPortSemHandler encoded with invalid "
                  "Action handler.\r\n", pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "Exiting....\r\n"));
        PTP_FN_EXIT ();
        return;
    }
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u PTP SEM invoked with Event: %s"
              " Current State: %s\r\n", pPtpPort->PortDs.u4PtpPortNumber,
              gac1PtpEvtStr[u1Event], gac1PtpStateStr[u1CurrentState]));

    if (u1Event == PTP_POWERUP_EVENT)
    {
        /* Power up event itself will make all the ports to be in 
         * INITIALIZING state. Hence, the Action handler should not repeat
         * the same when domain mode is boundary clock
         * */
        pPtpPort->pPtpDomain->u1InitInProgress = OSIX_FALSE;
    }

    /* Invoking the corresponding Action handler */
    (*ga_psm_func[(gau1_psm_table[u1Event][u1CurrentState])]) (pPtpPort);

    if (u1CurrentState != pPtpPort->PortDs.u1PortState)
    {
        /* Port State change occurred. Send a Notification */
        PtpNotifyInfo.u4ContextId = pPtpPort->u4ContextId;
        PtpNotifyInfo.u4DomainId = (UINT4) pPtpPort->u1DomainId;
        PtpNotifyInfo.u4PortId = pPtpPort->PortDs.u4PtpPortNumber;
        PtpNotifyInfo.u4PortState = (UINT4) pPtpPort->PortDs.u1PortState;
        PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                      (UINT1) PTP_TRAP_PORT_STATE_CHANGE);
        /*Handle Port state change */
        PtpAlMstHandlePortStateChange (pPtpPort);
    }

}

/*****************************************************************************/
/* Function                  : PtpSemPsmInvalid                              */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to indicate that either the State     */
/*                             event combination is invalid or the event does*/
/*                             not require any specific action to be done.   */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemPsmInvalid (tPtpPort * pPtpPort)
{
#ifndef TRACE_WANTED
    UNUSED_PARAM (pPtpPort);
#endif
    PTP_FN_ENTRY ();

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
              "Invalid Event occurred or No Action to perform.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Exiting....\r\n"));

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpSemMakeDisabled                            */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to move the state of the port to      */
/*                             DISABLED.                                     */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemMakeDisabled (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    pPtpPort->PortDs.u1PortState = PTP_STATE_DISABLED;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Moved to state DISABLED.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));
    if (PtpTmrStopAllTimersForPort (pPtpPort) == OSIX_FAILURE)
    {
        /* Failures are being ignored inside SEM as failure handling may
         * affect the states inside SEM */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "Port %u PtpSemMakeDisabled: "
                  "PtpTmrStopAllTimers returned Failure!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
              "Stopped All the timers for the port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));
    pPtpPort->pPtpErBestMsg = NULL;
}

/*****************************************************************************/
/* Function                  : PtpSemMakeFaulty                              */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to move the state of the port to      */
/*                             FAULTY.                                       */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemMakeFaulty (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    pPtpPort->PortDs.u1PortState = PTP_STATE_FAULTY;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              INIT_SHUT_TRC, "Port %u Entering PtpSemMakeFaulty...\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Moved to state FAULTY.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    if (PtpTmrStopAllTimersForPort (pPtpPort) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "Port %u PtpSemMakeFaulty: "
                  "PtpTmrStopAllTimers returned Failure!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Port %u Stopped all the timers for the port.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              INIT_SHUT_TRC, "Port %u Exiting PtpSemMakeFaulty...\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));
}

/*****************************************************************************/
/* Function                  : PtpSemMakeInitialize                          */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to move the state of the port to      */
/*                             INITIALIZE.                                   */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemMakeInitialize (tPtpPort * pPtpPort)
{
    tPtpNotifyInfo      PtpNotifyInfo;

    PTP_FN_ENTRY ();

    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));

    pPtpPort->PortDs.u1PortState = PTP_STATE_INITIALIZING;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Moved to state INITIALIZING.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Port %u Stopping all the timers for the port.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    if (PtpTmrStopAllTimersForPort (pPtpPort) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "Port %u PtpSemMakeInitialize: "
                  "PtpTmrStopAllTimers returned Failure!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Initializing Port Data Set.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    /* When one port of the boundary clock is in initializing mode
     * then all the ports need to be in initializing mode.
     * * u1InitInProgress will be set to TRUE in the following two cases
     * 1) POWER_UP event is triggered
     * 2) A port has started making all other ports in the domain to
     *    INITIALIZING state.
     * In both the cases, there is no need to trigger for all other ports
     * present in the domain.
     * */
    if ((pPtpPort->pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE) &&
        (pPtpPort->pPtpDomain->u1InitInProgress != OSIX_TRUE))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Domain mode is Boundary clock\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Triggering Init event for all other ports "
                  "in the domain.\r\n", pPtpPort->PortDs.u4PtpPortNumber));

        pPtpPort->pPtpDomain->u1InitInProgress = OSIX_TRUE;
        PtpSemInitDomainPorts (pPtpPort->pPtpDomain,
                               pPtpPort->PortDs.u4PtpPortNumber);
        pPtpPort->pPtpDomain->u1InitInProgress = OSIX_FALSE;
    }

    /* Port State change occurred. Send a Notification */
    PtpNotifyInfo.u4ContextId = pPtpPort->u4ContextId;
    PtpNotifyInfo.u4DomainId = (UINT4) pPtpPort->u1DomainId;
    PtpNotifyInfo.u4PortId = pPtpPort->PortDs.u4PtpPortNumber;
    PtpNotifyInfo.u4PortState = (UINT4) pPtpPort->PortDs.u1PortState;
    PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                  (UINT1) PTP_TRAP_PORT_STATE_CHANGE);
    /* UCT to listening */
    PtpSemMakeListening (pPtpPort);
}

/*****************************************************************************/
/* Function                  : PtpSemInitDomainPorts                         */
/*                                                                           */
/* Description               : This routine moves all other ports present in */
/*                             the given domain to INITIALIZE state. This    */
/*                             should be invoked only when the domain mode is*/
/*                             boundary clock.                               */
/*                                                                           */
/* Input                     : pPtpDomain - Pointer to Domain.               */
/*                             u4PortNumber - Port that invoked this routine.*/
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemInitDomainPorts (tPtpDomain * pPtpDomain, UINT4 u4PortNumber)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4Port = 0;
    BOOL1               bResult = OSIX_FALSE;

    PTP_FN_ENTRY ();

    for (u4Port = 1; u4Port <= PTP_MAX_PORTS; u4Port++)
    {
        if (u4Port == u4PortNumber)
        {
            continue;
        }

        OSIX_BITLIST_IS_BIT_SET (pPtpDomain->DomainPortList, u4Port,
                                 PTP_PORT_LIST_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {
            /* Port is member of the domain. */
            /* Get the corresponding port Entry */
            pPtpPort = PtpIfGetPortEntry (pPtpDomain->u4ContextId,
                                          pPtpDomain->u1DomainId, u4Port);
            if (pPtpPort == NULL)
            {
                /* Added for KlocWork warning */
                continue;
            }
            PtpSemMakeInitialize (pPtpPort);
        }
    }
}

/*****************************************************************************/
/* Function                  : PtpSemMakeListening                           */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to move the state of the port to      */
/*                             LISTENING                                     */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpSemMakeListening (tPtpPort * pPtpPort)
{
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    /* A port in Listening state should start the Announce receipt time out
     * and should wait for the reception of Announce messages.
     * */
    if (PtpTmrStopAllTimersForPort (pPtpPort) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "Port %u PtpSemMakeInitialize: "
                  "PtpTmrStopAllTimers returned Failure!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Starting Announce Receipt Timer\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    u4MilliSec =
        PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.u1AnnounceInterval);

    if (PtpTmrStartTmr (&(pPtpPort->AnnounceReciptTimeOutTimer),
                        (pPtpPort->PortDs.u1AnnounceReceiptTimeOut *
                         u4MilliSec), PTP_ANNC_RECEIPT_TMR, OSIX_FALSE)
        == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "Port %u PtpSemMakeListening: PtpTmrStartTmr "
                  "returned Failure!!!\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Port %u Unable to start Announce Receipt "
                  "Timer\r\n", pPtpPort->PortDs.u4PtpPortNumber));
    }

    pPtpPort->PortDs.u1PortState = PTP_STATE_LISTENING;

    /* The State Decision Algorithm should drive the port based on the output 
     * of the calculated BMC. Refer 9.2.6.8 of IEEE Std 1588 2008. Here, we are
     * not using a seperate timer for BMC trigger. We are using the announce 
     * timer expiry itself to trigger the BMC calculation. Hence, we need to 
     * start the announce timer for this port, as when it is enabled
     * */
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Starting Announce Timer\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    u4MilliSec =
        PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.u1AnnounceInterval);

    if (PtpTmrStartTmr (&(pPtpPort->AnnounceTimer),
                        u4MilliSec, PTP_ANNC_TMR, OSIX_FALSE) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "Port %u PtpSemMakeListening: "
                  "PtpTmrStartTmr returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Unable to start Announce Timer\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
    }
    /* Have to initialise all values again */
    PtpDmnInitDomainEntryCurrentDS (pPtpPort->pPtpDomain);
    /* If the delay mechanism is P2P, then start peer delay request timer
     * here
     * */
    if (pPtpPort->PortDs.u1DelayMechanism == PTP_PORT_DELAY_MECH_PEER_TO_PEER)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Configured delay mechanism in Port %u "
                  "is Peer to peer\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Starting Peer Delay Request timer.....\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        u4MilliSec = PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                                u1MinPdelayReqInterval);
        if (PtpTmrStartTmr (&(pPtpPort->PdelayReqTimer),
                            u4MilliSec, PTP_PDREQ_TMR, OSIX_TRUE)
            == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Unable to Start Peer delay "
                      "request timer for port %u\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
        }
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Moved to state LISTENING.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));
}

/*****************************************************************************/
/* Function                  : PtpSemProcessAnncRcptTmOut                    */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler whenever Announce receipt time out    */
/*                             ocurs. This function will move the state of   */
/*                             port to MASTER.                               */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemProcessAnncRcptTmOut (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    /* This indicates that the current Parent is no more valid for this port.
     * Hence, remove the parent entry from the data base 
     * */

    PtpIfDestroyBestMsgOnPort (pPtpPort);

    if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly ==
        OSIX_TRUE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Clock is configured as Slave only\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PtpSemMakeListening (pPtpPort);
        return;
    }

    PtpSemUpdtDataSetOnAnncRcpTmOut (pPtpPort);

    pPtpPort->PortDs.u1RecommendedState = PTP_STATE_MASTER;
    PtpSemProcessStateDecision (pPtpPort);
}

/*****************************************************************************/
/* Function                  : PtpSemProcessQlyTmExpiry                      */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler whenever Qualification timer expiry   */
/*                             ocurs. This function will move the state of   */
/*                             port to PRE-MASTER.                           */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemProcessQlyTmExpiry (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    PtpSemMakeMaster (pPtpPort);

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpSemMakePreMaster                           */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to move the state of the port to      */
/*                             PREMASTER.                                    */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemMakePreMaster (tPtpPort * pPtpPort)
{
    UINT4               u4QulfnTime = 0;
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly ==
        OSIX_TRUE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Clock is configured as Slave only\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Hence, not entering Pre Master state "
                  "for port.\r\n", pPtpPort->PortDs.u4PtpPortNumber));

        PTP_FN_EXIT ();
        return;
    }

    /* BMC will not recommend state as PRE_MASTER, instead will recommend
     * only as MASTER. Hence to send a trap out, verify that the current state
     * is not Premaster and if the recommended state is MASTER.
     * */
    if (pPtpPort->PortDs.u1PortState != PTP_STATE_PREMASTER)
    {
        PtpTrapSendPreMastNotification (pPtpPort);
    }

    pPtpPort->PortDs.u1PortState = PTP_STATE_PREMASTER;
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Moved to state PREMASTER.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    if (pPtpPort->PortDs.DecisionCode == M3)
    {
        /* Refer Section 9.2.6.10 of IEEE 1588 Version 2008.
         * Start Qualification timer only when the Decision code is M3
         * Value should be equal to N * Announce interval,
         * where N = CurrentDS.StepsRemoved + 1
         * */
        u4MilliSec =
            PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                       u1AnnounceInterval);

        u4QulfnTime = ((pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
                        u4StepsRemoved + 1) * u4MilliSec);

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Port %u Starting Qualification Timer for "
                  "time interval\r\n", pPtpPort->PortDs.u4PtpPortNumber));

        if (PtpTmrStartTmr (&(pPtpPort->QualificationExpiryTimer), u4QulfnTime,
                            PTP_QULFN_TMR, OSIX_FALSE) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "Port %u PtpSemMakePreMaster: "
                      "PtpTmrStartTmr returned Failure!!!\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Unable to start Qualification "
                      "Timer for port\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        }
    }
    else if ((pPtpPort->PortDs.DecisionCode == M1) ||
             (pPtpPort->PortDs.DecisionCode == M2))
    {
        /* Refer Section 9.2.6.10 of IEEE 1588 Version 2008.
         * The value of qualification timer is ZERO. And on expiry, it should
         * move to MASTER State.
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Decision code of Port is M1/M2. Hence moving to Master "
                  "State for port %u.\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        /* Triggering Qualification timer expiry will move the state to
         * Master Properly
         * */
        PtpSemProcessQlyTmExpiry (pPtpPort);
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Port %u Stopping Announce Receipt Time out.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    TmrStop (gPtpGlobalInfo.PtpTmrListId,
             &(pPtpPort->AnnounceReciptTimeOutTimer));

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpSemProcessSynFault                         */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to process the occurred Synchronization*/
/*                             fault event.                                  */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemProcessSyncFault (tPtpPort * pPtpPort)
{
    tPtpNotifyInfo      PtpNotifyInfo;

    PTP_FN_ENTRY ();
    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));

    PtpSemMakeUnCalibrated (pPtpPort);

    /* Send one notification for sync fault */
    PtpNotifyInfo.u4ContextId = pPtpPort->u4ContextId;
    PtpNotifyInfo.u4DomainId = (UINT4) pPtpPort->u1DomainId;
    PtpNotifyInfo.u4ErrType = (UINT4) PTP_GLOBAL_ERR_SYNC_FAULT;
    PtpTrapSendTrapNotifications (&PtpNotifyInfo, (UINT1) PTP_TRAP_SYNC_FAULT);

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpSemMakeUnCalibrated                        */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to process the occurred Synchronization*/
/*                             fault event.                                  */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemMakeUnCalibrated (tPtpPort * pPtpPort)
{
    UINT4               u4MilliSec = 0;
    UINT4               u4RemainingTime = 0;
    INT4                i4RetVal = 0;

    PTP_FN_ENTRY ();

    pPtpPort->PortDs.u1PortState = PTP_STATE_UNCALIBRATED;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Moved to state UNCALIBRATED.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    i4RetVal = TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                                    &(pPtpPort->AnnounceReciptTimeOutTimer.
                                      TimerNode), &u4RemainingTime);
    if ((u4RemainingTime != 0) && (i4RetVal == TMR_SUCCESS))
    {
        /* Announce receipt time out is running already. Do not restart the
         * same
         * */
        PTP_FN_EXIT ();
        return;
    }

    /* Announce timer should be started on entering Uncalibrated state */
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Starting Announce Receipt Timer\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    u4MilliSec =
        PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.u1AnnounceInterval);

    if (PtpTmrStartTmr (&(pPtpPort->AnnounceReciptTimeOutTimer),
                        (pPtpPort->PortDs.u1AnnounceReceiptTimeOut *
                         u4MilliSec), PTP_ANNC_RECEIPT_TMR, OSIX_FALSE)
        == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "Port %u PtpSemMakeUnCalibrated: "
                  "PtpTmrStartTmr returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Port %u Unable to start Announce Receipt "
                  "Timer\r\n", pPtpPort->PortDs.u4PtpPortNumber));
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpSemMakeSlave                               */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to move the state of the port to SLAVE*/
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemMakeSlave (tPtpPort * pPtpPort)
{
    UINT4               u4MilliSec = 0;
    UINT4               u4RemainingTime = 0;
    INT4                i4RetVal = 0;

    PTP_FN_ENTRY ();

    pPtpPort->PortDs.u1PortState = PTP_STATE_SLAVE;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Moved to state SLAVE.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    pPtpPort->bDelayReqInProgress = OSIX_FALSE;

    TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                         &(pPtpPort->AnnounceReciptTimeOutTimer.TimerNode),
                         &u4RemainingTime);
    if ((u4RemainingTime != 0) && (i4RetVal == TMR_SUCCESS))
    {
        /* Announce receipt time out is running already. Do not restart the
         * same
         * */
        PTP_FN_EXIT ();
        return;
    }

    /* Announce timer should be started on entering SLAVE state */
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Starting Announce Receipt Timer\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    u4MilliSec =
        PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.u1AnnounceInterval);

    if (PtpTmrStartTmr (&(pPtpPort->AnnounceReciptTimeOutTimer),
                        (pPtpPort->PortDs.u1AnnounceReceiptTimeOut *
                         u4MilliSec), PTP_ANNC_RECEIPT_TMR, OSIX_FALSE)
        == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC, "Port %u PtpSemMakeSlave: "
                  "PtpTmrStartTmr returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Port %u Unable to start Announce Receipt "
                  "Timer\r\n", pPtpPort->PortDs.u4PtpPortNumber));
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpSemMakePassive                             */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to move the state of the port to      */
/*                             PASSIVE.                                      */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpSemMakePassive (tPtpPort * pPtpPort)
{
    UINT4               u4MilliSec = 0;
    UINT4               u4RemainingTime = 0;
    INT4                i4RetVal = 0;

    PTP_FN_ENTRY ();

    pPtpPort->PortDs.u1PortState = PTP_STATE_PASSIVE;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Moved to state SLAVE.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                         &(pPtpPort->AnnounceReciptTimeOutTimer.TimerNode),
                         &u4RemainingTime);
    if ((u4RemainingTime != 0) && (i4RetVal == TMR_SUCCESS))
    {
        /* Announce receipt time out is running already. Do not restart the
         * same
         * */
        PTP_FN_EXIT ();
        return;
    }

    /* Announce timer should be started on entering PASSIVE state */

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Starting Announce Receipt Timer\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    u4MilliSec =
        PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.u1AnnounceInterval);

    if (PtpTmrStartTmr (&(pPtpPort->AnnounceReciptTimeOutTimer),
                        (pPtpPort->PortDs.u1AnnounceReceiptTimeOut *
                         u4MilliSec), PTP_ANNC_RECEIPT_TMR, OSIX_FALSE)
        == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "Port %u PtpSemMakePassive: "
                  "PtpTmrStartTmr returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Port %u Unable to start Announce Receipt "
                  "Timer\r\n", pPtpPort->PortDs.u4PtpPortNumber));
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpSemUpdtDataSetOnAnncRcpTmOut               */
/*                                                                           */
/* Description               : This routine updates the different data sets  */
/*                             on the expiry of Announce receipt timer. Refer*/
/*                             Section 9.2.6.11 of IEEE 1588 2008.           */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemUpdtDataSetOnAnncRcpTmOut (tPtpPort * pPtpPort)
{
    UINT1               u1IsBoundarySlave = OSIX_FALSE;

    PTP_FN_ENTRY ();

    /* If the clock is ordinary clock, update as per decision code M1
     * If the clock is boundary clock and there are no other slave ports,
     * update as per M1
     * If the clock is boundary clock and there are some other slave ports,
     * update as per M3
     * */

    u1IsBoundarySlave = (UINT1) PtpUtilIsSlavePortInBoundaryClk (pPtpPort);

    if (u1IsBoundarySlave == OSIX_TRUE)
    {
        /* Clock is boundary clock and is having some other ports as Slaves. 
         * Update as per M3
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Clock is boundary clock and is having "
                  "Some other ports as Slave ports\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Updating Data Set as per Decision " "Code M3.\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        pPtpPort->PortDs.DecisionCode = M3;
        PtpBmcDsUpdtDataSetForM3 (pPtpPort);
    }
    else if ((pPtpPort->pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
             (pPtpPort->pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
    {
        /* Clock is boundary clock and does not contain any other port as 
         * Slave.
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Clock is boundary/ordinary clock\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Port %u Updating Data Set as per Decision " "Code M1.\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        pPtpPort->PortDs.DecisionCode = M1;
        PtpBmcDsUpdtDataSetForM1andM2 (pPtpPort);
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpSemMakeMaster                              */
/*                                                                           */
/* Description               : This routine will be invoked by the SEM       */
/*                             handler to move the state of the port to      */
/*                             MASTER.                                       */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemMakeMaster (tPtpPort * pPtpPort)
{
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    pPtpPort->PortDs.u1PortState = PTP_STATE_MASTER;

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Moved to state MASTER.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Port %u Stopping Announce Receipt Time out.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    TmrStop (gPtpGlobalInfo.PtpTmrListId,
             &(pPtpPort->AnnounceReciptTimeOutTimer));

    /* Starting Sync timer. Announce timer would have been started earlier
     * to facilitate BMC Calculation
     * */
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Starting Sync Timer.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    u4MilliSec =
        PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.i1SyncInterval);

    if (PtpTmrStartTmr (&(pPtpPort->SyncTimer),
                        u4MilliSec, PTP_SYNC_TMR, OSIX_TRUE) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "Port %u PtpSemMakeMaster: "
                  "PtpTmrStartTmr returned Failure!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Port %u Unable to start sync Timer\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Port %u Starting Delay Request Timer.\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpSemProcessStateDecision                    */
/*                                                                           */
/* Description               : This routine moves the state of the given port*/
/*                             to the state recommended by the State Decision*/
/*                             Algorithm.                                    */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpSemProcessStateDecision (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Port %u BMC computed new state for the port\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    /* Updates for the state decision code could have been done by the
     * BMC Machine itself */

    PtpTmrStopTmrsOnStateChg (pPtpPort);

    switch (pPtpPort->PortDs.u1RecommendedState)
    {
        case PTP_STATE_MASTER:

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Newly elected state is MASTER.\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly ==
                OSIX_TRUE)
            {
                /* Slave Only implementation. Refer Figure 24 of IEEE Std
                 * 1588-2008*/
                pPtpPort->PortDs.u1RecommendedState = PTP_STATE_LISTENING;
                PtpSemMakeListening (pPtpPort);
            }
            PtpSemPortSemHandler (pPtpPort, PTP_BMC_MASTER_EVENT);
            break;

        case PTP_STATE_SLAVE:

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Newly elected state is SLAVE.\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            PtpSemPortSemHandler (pPtpPort, PTP_BMC_SLAVE_EVENT);
            break;

        case PTP_STATE_PREMASTER:

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Port %u Newly elected state is "
                      "PRE-MASTER.\r\n", pPtpPort->PortDs.u4PtpPortNumber));
            if (pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly ==
                OSIX_TRUE)
            {
                /* Slave Only implementation. Refer Figure 24 of IEEE Std
                 * 1588-2008*/
                pPtpPort->PortDs.u1RecommendedState = PTP_STATE_LISTENING;
                PtpSemMakeListening (pPtpPort);
            }
            else
            {
                PtpSemMakePreMaster (pPtpPort);
            }
            break;

        case PTP_STATE_PASSIVE:

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Port %u Newly elected state is PASSIVE.\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            PtpSemPortSemHandler (pPtpPort, PTP_BMC_PASSIVE_EVENT);
            break;

        case PTP_STATE_LISTENING:

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Port %u Newly elected state is "
                      "LISTENING.\r\n", pPtpPort->PortDs.u4PtpPortNumber));

            PtpSemMakeListening (pPtpPort);
            break;

        default:
            /* State Event Algorithm cannot compute other states as
             * outputs */
            break;
    }
    PTP_FN_EXIT ();
}
#endif /*_PTPSEM_C_ */
