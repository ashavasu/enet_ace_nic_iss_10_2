/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpudp.c,v 1.17 2014/05/29 13:19:58 siva Exp $
 *
 * Description: This file contains PTP UDP related utilities 
 *              implementation.       
 *********************************************************************/
#ifndef _PTPUDP_C_
#define _PTPUDP_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function                  : PtpUdpBindWithUdp                             */
/*                                                                           */
/* Description               : This routine initialises the socket related   */
/*                             parameters. This function should be invoked   */
/*                             during PTP module boot up.                    */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.i4Ipv4GnlSockId                */
/*                             gPtpGlobalInfo.i4Ipv4EvtSockId                */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUdpBindWithUdp (VOID)
{
    struct sockaddr_in  PtpLocalAddr;

    PTP_FN_ENTRY ();

    /* Open a socket with the standard port provided in the standard.
     * Refer Section D.2; annex D of IEEE Std 1588 2008. This is for the
     * reception of event messages.
     * */
    gPtpGlobalInfo.i4Ipv4EvtSockId = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (gPtpGlobalInfo.i4Ipv4EvtSockId < 0)
    {
        perror ("Socket creation failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "Failure in opening the Event UDP socket with port "
                  "number 319\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PtpLocalAddr.sin_family = AF_INET;
    PtpLocalAddr.sin_addr.s_addr = 0;
    PtpLocalAddr.sin_port = OSIX_HTONS (UDP_PTP_EVENT_PORT);

    /* Bind with the socket */
    if (bind (gPtpGlobalInfo.i4Ipv4EvtSockId, (struct sockaddr *) &PtpLocalAddr,
              sizeof (struct sockaddr_in)) < 0)
    {
        perror ("Socket bind failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC,
                  "Failure in binding the Event UDP socket.\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpUdpInitSockParams (gPtpGlobalInfo.i4Ipv4EvtSockId) == OSIX_FAILURE)
    {
        perror ("Socket initialization failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC, "Failure in initialising the UDP socket\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Open a socket with the standard port provided in the standard.
     * Refer Section D.2; annex D of IEEE Std 1588 2008. This is for the 
     * reception of general messages.
     * */
    gPtpGlobalInfo.i4Ipv4GnlSockId = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (gPtpGlobalInfo.i4Ipv4GnlSockId < 0)
    {
        perror ("Socket creation failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC,
                  "Failure in opening the General UDP socket with port number "
                  "320.\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PtpLocalAddr.sin_family = AF_INET;
    PtpLocalAddr.sin_addr.s_addr = 0;
    PtpLocalAddr.sin_port = OSIX_HTONS (UDP_PTP_GENERAL_PORT);

    /* Bind with the socket */
    if (bind (gPtpGlobalInfo.i4Ipv4GnlSockId, (struct sockaddr *) &PtpLocalAddr,
              sizeof (struct sockaddr_in)) < 0)
    {
        perror ("Socket bind failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC,
                  "Failure in binding the General UDP socket.\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpUdpInitSockParams (gPtpGlobalInfo.i4Ipv4GnlSockId) == OSIX_FAILURE)
    {
        perror ("Socket initialization failed");
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                  INIT_SHUT_TRC, "Failure in initialising the UDP socket\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpUdpInitSockParams                          */
/*                                                                           */
/* Description               : This routine initialises the socket related   */
/*                             parameters for the provided socket. This will */
/*                             also be responsible for adding the call back  */
/*                             function with the SLI module.                 */
/*                                                                           */
/* Input                     : i4SockId - Socket Identifier.                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUdpInitSockParams (INT4 i4SockId)
{
    INT4                i4Flags = 0;
    INT4                i4OpnVal = 1;
    UINT1               u1OpnVal = 1;

#ifdef BSDCOMP_SLI_WANTED
    INT1                i1MCloopVal = 0;    /*Reset multicast looping */
#endif

    PTP_FN_ENTRY ();

    if ((i4Flags = fcntl (i4SockId, F_GETFL, 0)) < 0)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "Acquiring the flags for the socket.\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (i4SockId, F_SETFL, i4Flags) < 0)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "Acquiring the flags for the socket.\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (setsockopt (i4SockId, IPPROTO_IP, IP_PKTINFO, &u1OpnVal,
                    sizeof (UINT1)) < 0)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "setsockopt Failed for IP_PKTINFO.\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

#ifdef BSDCOMP_SLI_WANTED
    /*Set the option not to receive packet on the interface on which 
     * it is sent*/
    if (setsockopt (i4SockId, IPPROTO_IP, IP_MULTICAST_LOOP,
                    &i1MCloopVal, sizeof (INT1)) < 0)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "Unable to set multicast loop avoidance option.\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
#endif

    i4OpnVal = PTP_IPV4_DSCP_VAL;

    /* Shift the Value to the TOS bits (7-2) set TOS bits (1,0) as 0  */
    i4OpnVal = (INT4) (i4OpnVal << 2);

    if (setsockopt (i4SockId, IPPROTO_IP, IP_TOS, &i4OpnVal, sizeof (INT4)) < 0)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "setsockopt Failed for IP_TOS.\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpUdpTransmitPtpMsg                          */
/*                                                                           */
/* Description               : This routine transmits the given PTP PDU over */
/*                             UDP interface.                                */
/*                                                                           */
/* Input                     : pu1PtpPdu  - Pointer to the linear buffer.    */
/*                             pPtpPort - Pointer to the port structure.     */
/*                             u4PktLen - Packet Length.                     */
/*                             u1MsgType- Message Type.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpUdpTransmitPtpMsg (UINT1 *pu1PtpPdu, tPtpPort * pPtpPort,
                      UINT4 u4PktLen, UINT1 u1MsgType)
{
    struct ip_mreqn     Mreqn;
    struct sockaddr_in  PeerAddr;
    struct in_addr      IfAddr;
    struct in_addr      MultiCastIfAddr;
    struct msghdr       PktInfo;
    struct cmsghdr     *pCmsgInfo = NULL;
    struct in_pktinfo  *pIpPktInfo = NULL;
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif
    tNetIpv4IfInfo      NetIpv4IfInfo;
    UINT1               au1Cmsg[PTP_IP_HEADER_LEN];
    UINT4               u4IpPort = 0;
    INT4                i4SockId = 0;
    INT4                i4RetVal = 0;
    UINT2               u2PtpPort = 0;
    INT4                i4Ttl = 1;

    PTP_FN_ENTRY ();

    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (&Mreqn, 0, sizeof (struct ip_mreqn));
    MEMSET (&IfAddr, 0, sizeof (struct in_addr));
    MEMSET (&NetIpv4IfInfo, 0, sizeof (tNetIpv4IfInfo));

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    PktInfo.msg_name = (void *) &PeerAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);

#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = pu1PtpPdu;
    Iov.iov_len = u4PktLen;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#endif

    /* Destination port differs based on the type of the message transmitted
     * */
    if ((u1MsgType == PTP_ANNC_MSG) || (u1MsgType == PTP_SIGNALLING_MSG))
    {
        u2PtpPort = UDP_PTP_GENERAL_PORT;
        i4SockId = gPtpGlobalInfo.i4Ipv4GnlSockId;
    }
    else
    {
        u2PtpPort = UDP_PTP_EVENT_PORT;
        i4SockId = gPtpGlobalInfo.i4Ipv4EvtSockId;
    }

    if ((u1MsgType == PTP_PEER_DELAY_REQ_MSG) ||
        (u1MsgType == PTP_PEER_DELAY_RESP_MSG) ||
        (u1MsgType == PTP_PDELAY_RESP_FOLLOWUP_MSG))
    {
        MultiCastIfAddr.s_addr = PTP_PEER_DELAY_MSG_IP;
    }
    else
    {
        MultiCastIfAddr.s_addr = PTP_MULTICAST_IP;
    }

    /* Get the IP port number and pass the port number */
    if (PtpPortNetIpv4GetPortFromIfIndex (pPtpPort->u4IfIndex,
                                          &u4IpPort) == NETIPV4_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "Unable to get the IP port number from CFA interface"
                  "index %u\r\n", pPtpPort->u4IfIndex));
        PTP_FN_EXIT ();
        return;
    }

    if (PtpPortNetIpv4GetIfInfo (u4IpPort, &NetIpv4IfInfo) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "Unable to get the IP Port info for "
                  "index %u\r\n", pPtpPort->u4IfIndex));
        PTP_FN_EXIT ();
        return;
    }

    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo =
        (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
    pIpPktInfo->ipi_ifindex = u4IpPort;
    pIpPktInfo->ipi_addr.s_addr = MultiCastIfAddr.s_addr;

    IfAddr.s_addr = OSIX_HTONL (NetIpv4IfInfo.u4Addr);
    Mreqn.imr_address = IfAddr;
    Mreqn.imr_multiaddr = MultiCastIfAddr;
    Mreqn.imr_ifindex = u4IpPort;

    i4RetVal = setsockopt (i4SockId, IPPROTO_IP, IP_MULTICAST_IF,
                           (char *) &(Mreqn), sizeof (Mreqn));
    if (i4RetVal < 0)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "setsockopt failed for Multicast Ip.\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    PeerAddr.sin_family = AF_INET;
    /* The destination address should be 224.0.1.129 for all messages and for
     * PDELAY type of messages, it should be 224.0.0.107
     * */
    if ((u1MsgType == PTP_PEER_DELAY_REQ_MSG) ||
        (u1MsgType == PTP_PEER_DELAY_RESP_MSG) ||
        (u1MsgType == PTP_PDELAY_RESP_FOLLOWUP_MSG))
    {
        PeerAddr.sin_addr.s_addr = OSIX_HTONL (PTP_PEER_DELAY_MSG_IP);
        /* In case of transmitting Peer Delay Request/Response messages over 
         * UDP, the value of TTL should be set to 1. Refer Section D.3 of 
         * Annex D of IEEE Std 1588 2008
         * */
        if (setsockopt (i4SockId, IPPROTO_IP, IP_TTL, &i4Ttl, sizeof (INT4))
            < 0)
        {
            perror ("Error in sendmsg of PTP\n");
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                      "Unable to set TTL as 1 for P2P Messages....\r\n"));
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                      "Proceeding with Packet transmission...\r\n"));
        }
    }
    else
    {
        PeerAddr.sin_addr.s_addr = OSIX_HTONL (PTP_MULTICAST_IP);
    }

    PeerAddr.sin_port = (UINT2) OSIX_HTONS (u2PtpPort);

    if (sendmsg (i4SockId, &PktInfo, 0) < 0)
    {
        perror ("Error in sendmsg of PTP\n");

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "SendMsg failed for PTP!!!.\r\n"));
        PTP_FN_EXIT ();
        return;
    }

#ifndef BSDCOMP_SLI_WANTED
    if (sendto (i4SockId, pu1PtpPdu, (UINT4) u4PktLen, 0,
                (struct sockaddr *) &PeerAddr, sizeof (struct sockaddr_in)) < 0)
    {

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "sendto failed for PTP!!!.\r\n"));
        PTP_FN_EXIT ();
        return;
    }
#else
    UNUSED_PARAM (pu1PtpPdu);
    UNUSED_PARAM (u4PktLen);
#endif /* BSDCOMP_SLI_WANTED */
}

/*****************************************************************************/
/* Function                  : PtpUdpDetEventForSocket                       */
/*                                                                           */
/* Description               : This routine determines the event that needs  */
/*                             to be posted to the PTP module. This will be  */
/*                             determined based on the socket identifier.    */
/*                             This function should be invoked when a packet */
/*                             arrives at the given socket.                  */
/*                                                                           */
/* Input                     : i4SockId - Socket Identifier.                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Event to be posted to the PTP task.           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUdpDetEventForSocket (INT4 i4SockId)
{
    INT4                i4Event = 0;

    PTP_FN_ENTRY ();

    if (i4SockId == gPtpGlobalInfo.i4Ipv4EvtSockId)
    {
        i4Event = PTP_IPv4_EVENTSOCK_PKT_ARRIVAL_EVENT;
    }
    else if (i4SockId == gPtpGlobalInfo.i4Ipv4GnlSockId)
    {
        i4Event = PTP_IPv4_GENERALSOCK_PKT_ARRIVAL_EVENT;
    }
    if (i4SockId == gPtpGlobalInfo.i4Ipv6EvtSockId)
    {
        i4Event = PTP_IPv6_EVENTSOCK_PKT_ARRIVAL_EVENT;
    }
    else if (i4SockId == gPtpGlobalInfo.i4Ipv6GnlSockId)
    {
        i4Event = PTP_IPv6_GENERALSOCK_PKT_ARRIVAL_EVENT;
    }

    PTP_FN_EXIT ();
    return i4Event;
}

/*****************************************************************************/
/* Function                  : PtpUdpProcessPktOnSocket                      */
/*                                                                           */
/* Description               : This routine reads the data present in the    */
/*                             given socket and processes the received data. */
/*                                                                           */
/* Input                     : i4SockId - Socket Identifier.                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUdpProcessPktOnSocket (INT4 i4SockId)
{
    struct sockaddr_in  PtpPeerAddr;
    struct msghdr       PktInfo;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tPtpPktParams       PtpPktParams;
    tClkSysTimeInfo     PtpSysTimeInfo;
    tPtpPortAddress     PtpPortAddress;
    struct in_pktinfo  *pIpPktInfo = NULL;
#ifdef BSDCOMP_SLI_WANTED
    struct cmsghdr     *pCmsg = NULL;
    UINT1               au1Cmsg[PTP_IP_HEADER_LEN];
    struct iovec        Iov;
#else
    struct cmsghdr      CmsgInfo;
#endif
    tPtpPort           *pPtpPort = NULL;
    UINT1              *pu1RxPdu = NULL;
    UINT4               u4Port = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4PktLen = 0;
#ifndef BSDCOMP_SLI_WANTED
    INT4                i4AddrLen = 0;
#endif
    UINT1               u1DomainId = 0;
#ifdef L2RED_WANTED
    tRmMsg             *pMsg = NULL;
    UINT2               u2DataLen = 0;
#endif
    PTP_FN_ENTRY ();

    MEMSET (&PtpPeerAddr, 0, sizeof (struct sockaddr_in));
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET (&PtpPktParams, 0, sizeof (tPtpPktParams));
    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    MEMSET (&PtpPortAddress, 0, sizeof (tPtpPortAddress));

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              CONTROL_PLANE_TRC, "Received Data on Socket....\r\n"));

    pu1RxPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                              PTP_MAX_PDU_LEN);

    if (pu1RxPdu == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "PtpUdpProcessPktOnSocket: "
                  "Buddy Memory Allocation for PDU Failed!!!!\r\n"));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (pu1RxPdu, 0, PTP_MAX_PDU_LEN);

#ifndef BSDCOMP_SLI_WANTED
    MEMSET (&CmsgInfo, 0, sizeof (struct cmsghdr));
    PktInfo.msg_control = (VOID *) &CmsgInfo;

    while ((i4PktLen = recvfrom (i4SockId, pu1RxPdu, PTP_MAX_PDU_LEN, 0,
                                 (struct sockaddr *) &PtpPeerAddr,
                                 &i4AddrLen)) > 0)
    {
        if (recvmsg (i4SockId, &PktInfo, 0) < 0)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC, "Recv message failed\r\n"));

            MEMSET (&PtpPeerAddr, 0, sizeof (struct sockaddr_in));
            /* Intialising for receiving from any address */
            continue;
        }
        pIpPktInfo =
            (tInPktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
        u4Port = (UINT4) pIpPktInfo->ipi_ifindex;
#else
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    PktInfo.msg_name = (void *) &PtpPeerAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pu1RxPdu;
    Iov.iov_len = PTP_MAX_PDU_LEN;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsg = CMSG_FIRSTHDR (&PktInfo);
    pCmsg->cmsg_level = SOL_IP;
    pCmsg->cmsg_type = IP_PKTINFO;
    pCmsg->cmsg_len = sizeof (au1Cmsg);
    while ((i4PktLen = recvmsg (i4SockId, &PktInfo, 0)) > 0)
    {
        pIpPktInfo =
            (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
        u4Port = (UINT4) pIpPktInfo->ipi_ifindex;
#endif
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "Processing PTP pakcet received "
                  "on socket...\r\n"));

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC, "Obtaining the Interface from the socket "
                  "identifier...\r\n"));

        if (PtpPortGetCfaIfIndexFromPort (u4Port, &u4IfIndex) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                      PTP_MAX_DOMAINS, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Unable to get the CFA ifindex from port\r\n"));
            MEMSET (&PtpPeerAddr, 0, sizeof (struct sockaddr_in));
            continue;
        }

        if ((PtpCmUtlGetDomainIdFromPTPv1Msg (pu1RxPdu, &u1DomainId)) ==
            OSIX_FAILURE)
        {
            u1DomainId = pu1RxPdu[PTP_MSG_DOMAIN_OFFSET];
        }

        /* Get the port entry. Since VRF support is not present in IP
         * module, Default context is used
         */
        pPtpPort = PtpIfGetPortFromIfTypeAndIndex
            ((UINT4) L2IWF_DEFAULT_CONTEXT, u1DomainId,
             u4IfIndex, PTP_IFACE_UDP_IPV4);

        if (pPtpPort == NULL)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                      PTP_MAX_DOMAINS, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Port Entry is not available for Index : %u and"
                      " Domain : %u\r\n", u4IfIndex, u1DomainId));
            MEMSET (&PtpPeerAddr, 0, sizeof (struct sockaddr_in));
            continue;
        }

        /* Allocation for CRU buffer failed */
        pBuf = CRU_BUF_Allocate_MsgBufChain (PTP_MAX_PDU_LEN, 0);

        if (pBuf != NULL)
        {
            /* Copy data from linear buffer into CRU buffer */
            if (CRU_BUF_Copy_OverBufChain (pBuf, pu1RxPdu, 0, PTP_MAX_PDU_LEN)
                != CRU_FAILURE)
            {
                /* Do a software timestamping now */
                PtpClkGetTimeStamp (pPtpPort, (VOID *) pBuf, &PtpSysTimeInfo,
                                    u4IfIndex, PTP_MODE_RX);
            }
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }

        PtpPortAddress.u2NetworkProtocol = PTP_IFACE_UDP_IPV4;
        PtpPortAddress.u2AddrLength = IPVX_IPV4_ADDR_LEN;
        PtpPeerAddr.sin_addr.s_addr = OSIX_NTOHL (PtpPeerAddr.sin_addr.s_addr);

        PTP_IPADDR_TO_OCT (PtpPeerAddr.sin_addr.s_addr, PtpPortAddress.ai1Addr);
        PtpPktParams.pu1RcvdPdu = pu1RxPdu;
        PtpPktParams.pPtpPort = pPtpPort;
        PtpPktParams.pPtpIngressTimeStamp = &PtpSysTimeInfo;
        PtpPktParams.pPtpPortAddress = &PtpPortAddress;
        PtpPktParams.u4PktLen = (UINT4) i4PktLen;
        PtpQueProcessPtpPdu (&PtpPktParams);

#ifdef L2RED_WANTED
        /*Formaing RM packet and sending to StandBy node */
        if ((gPtpRedGlobalInfo.u1NodeStatus == RM_ACTIVE) &&
            (gPtpRedGlobalInfo.u1NumOfStandbyNodesUp != 0))
        {

            PtpRedFormIPvXPtpPktForStandby (&PtpPktParams, &pMsg);
            if (pMsg != NULL)
            {
                u2DataLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pMsg);
                PtpRedProcessAndRelayPkt (pMsg, u2DataLen);
            }
            else
            {
                PTP_TRC ((0, PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpUdpProcessPktOnSocket: Unable to duplicate the packet for RM.\r\n"));
            }
        }
#endif

    }                            /* Packet received on the socket */

    MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpUdpDeInitParams                            */
/*                                                                           */
/* Description               : This routine de-initializes the UDP data that */
/*                             are needed by the PTP module. This should be  */
/*                             invoked whenever PTP module is shutdown. This */
/*                             close all the socktes and reset the options   */
/*                             created for the sockets.                      */
/*                                                                           */
/* Input                     : i4SockId - Socket Identifier.                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUdpDeInitParams (INT4 i4SockId)
{
    PTP_FN_ENTRY ();

    /* Remove the FD association from SLI */
    SelRemoveFd (i4SockId);

    /* Close the socket identifier. */
    close (i4SockId);
    PTP_FN_EXIT ();

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpUdpAddOrRemoveFd                           */
/*                                                                           */
/* Description               : This routine is called during module start    */
/*                             or shutdown. This routine adds/removes the    */
/*                             FD from the socket                            */
/*                                                                           */
/* Input                     : u1AddFd  - OSIX_TRUE  - Add Fd                */
/*                                        OSIX_FALSE - Add Fd                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpUdpAddOrRemoveFd (UINT1 u1AddFd)
{
    INT4                i4SockId = -1;

    PTP_FN_ENTRY ();

    if (u1AddFd == (UINT1) OSIX_TRUE)
    {
        i4SockId = gPtpGlobalInfo.i4Ipv4EvtSockId;

        if ((i4SockId != -1) &&
            (SelAddFd (i4SockId, PtpApiPktRcvdOnSocket) != OSIX_SUCCESS))
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "PtpUdpAddOrRemoveFd: Failed to Register Socket\r\n"));
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }

        i4SockId = gPtpGlobalInfo.i4Ipv4GnlSockId;

        if ((i4SockId != -1) &&
            (SelAddFd (i4SockId, PtpApiPktRcvdOnSocket) != OSIX_SUCCESS))
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "PtpUdpAddOrRemoveFd: Failed to Register Socket\r\n"));
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }

        i4SockId = gPtpGlobalInfo.i4Ipv6EvtSockId;

        if ((i4SockId != -1) &&
            (SelAddFd (i4SockId, PtpApiPktRcvdOnSocket) != OSIX_SUCCESS))
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "PtpUdpAddOrRemoveFd: Failed to Register Socket\r\n"));
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }

        i4SockId = gPtpGlobalInfo.i4Ipv6GnlSockId;

        if ((i4SockId != -1) &&
            (SelAddFd (i4SockId, PtpApiPktRcvdOnSocket) != OSIX_SUCCESS))
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "PtpUdpAddOrRemoveFd: Failed to Register Socket\r\n"));
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (gPtpGlobalInfo.i4Ipv4EvtSockId != -1)
        {
            SelRemoveFd (gPtpGlobalInfo.i4Ipv4EvtSockId);
        }

        if (gPtpGlobalInfo.i4Ipv4GnlSockId != -1)
        {
            SelRemoveFd (gPtpGlobalInfo.i4Ipv4GnlSockId);
        }

        if (gPtpGlobalInfo.i4Ipv6GnlSockId != -1)
        {
            SelRemoveFd (gPtpGlobalInfo.i4Ipv6GnlSockId);
        }

        if (gPtpGlobalInfo.i4Ipv6EvtSockId != -1)
        {
            SelRemoveFd (gPtpGlobalInfo.i4Ipv6EvtSockId);
        }
    }

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              CONTROL_PLANE_TRC, "PtpUdpAddOrRemoveFd Returns Success\r\n"));
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpUdpAddMembership                           */
/*                                                                           */
/* Description               : This routine is called during PTP enable      */
/*                             on port. This routine adds the membership     */
/*                             for the port                                  */
/*                                                                           */
/* Input                     : u4Port   - IP port number                     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.i4Ipv4EvtSockId                */
/*                             gPtpGlobalInfo.i4Ipv4GnlSockId                */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpUdpAddMembership (UINT4 u4Port)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ip_mreqn     mreqn;

    memset (&mreqn, 0, sizeof (mreqn));

    MEMCPY (&mreqn.imr_multiaddr.s_addr, gau1PtpMcastGroupAddr1,
            sizeof (UINT4));

    mreqn.imr_ifindex = u4Port;

    if (setsockopt (gPtpGlobalInfo.i4Ipv4EvtSockId, IPPROTO_IP,
                    IP_ADD_MEMBERSHIP, (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        perror ("setsockopt (224.0.1.129): gPtpGlobalInfo.i4Ipv4EvtSockId");
    }

    if (setsockopt (gPtpGlobalInfo.i4Ipv4GnlSockId, IPPROTO_IP,
                    IP_ADD_MEMBERSHIP, (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        perror ("setsockopt (224.0.1.129): gPtpGlobalInfo.i4Ipv4GnlSockId");
    }

    memset (&mreqn, 0, sizeof (mreqn));

    MEMCPY (&mreqn.imr_multiaddr.s_addr, gau1PtpMcastGroupAddr2,
            sizeof (UINT4));

    mreqn.imr_ifindex = u4Port;

    if (setsockopt (gPtpGlobalInfo.i4Ipv4EvtSockId, IPPROTO_IP,
                    IP_ADD_MEMBERSHIP, (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        perror ("setsockopt (224.0.0.107): gPtpGlobalInfo.i4Ipv4EvtSockId");
    }

    if (setsockopt (gPtpGlobalInfo.i4Ipv4GnlSockId, IPPROTO_IP,
                    IP_ADD_MEMBERSHIP, (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        perror ("setsockopt (224.0.0.107): gPtpGlobalInfo.i4Ipv4GnlSockId");
    }

#else
    UNUSED_PARAM (u4Port);
#endif
}

/*****************************************************************************/
/* Function                  : PtpUdpRemoveMembership                        */
/*                                                                           */
/* Description               : This routine is called during PTP disables    */
/*                             on port. This routine drops the membership    */
/*                             for the port                                  */
/*                                                                           */
/* Input                     : u4Port   - IP port number                     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.i4Ipv4EvtSockId                */
/*                             gPtpGlobalInfo.i4Ipv4GnlSockId                */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpUdpRemoveMembership (UINT4 u4Port)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ip_mreqn     mreqn;

    memset (&mreqn, 0, sizeof (mreqn));

    MEMCPY (&mreqn.imr_multiaddr.s_addr, gau1PtpMcastGroupAddr1,
            sizeof (UINT4));
    mreqn.imr_ifindex = u4Port;

    if (setsockopt (gPtpGlobalInfo.i4Ipv4EvtSockId, IPPROTO_IP,
                    IP_DROP_MEMBERSHIP, (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        perror ("setsockopt (224.0.1.129): gPtpGlobalInfo.i4Ipv4EvtSockId");
    }

    if (setsockopt (gPtpGlobalInfo.i4Ipv4GnlSockId, IPPROTO_IP,
                    IP_DROP_MEMBERSHIP, (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        perror ("setsockopt (224.0.1.129): gPtpGlobalInfo.i4Ipv4GnlSockId");
    }

    memset (&mreqn, 0, sizeof (mreqn));

    MEMCPY (&mreqn.imr_multiaddr.s_addr, gau1PtpMcastGroupAddr2,
            sizeof (UINT4));

    mreqn.imr_ifindex = u4Port;

    if (setsockopt (gPtpGlobalInfo.i4Ipv4EvtSockId, IPPROTO_IP,
                    IP_DROP_MEMBERSHIP, (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        perror ("setsockopt (224.0.0.107): gPtpGlobalInfo.i4Ipv4EvtSockId");
    }

    if (setsockopt (gPtpGlobalInfo.i4Ipv4GnlSockId, IPPROTO_IP,
                    IP_DROP_MEMBERSHIP, (void *) &mreqn, sizeof (mreqn)) < 0)
    {
        perror ("setsockopt (224.0.0.107): gPtpGlobalInfo.i4Ipv4GnlSockId");
    }

#else
    UNUSED_PARAM (u4Port);
#endif
}

#endif /* _PTPUDP_C */
