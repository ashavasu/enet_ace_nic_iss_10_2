/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpmast.c,v 1.5 2014/01/30 12:20:35 siva Exp $
 *
 * Description: This file contains PTP context related utilities 
 *              implementation.       
 *********************************************************************/
#ifndef _PTPMAST_C_
#define _PTPMAST_C_

#include "ptpincs.h"
/*****************************************************************************/
/* Function                  : PtpMastCreate                                 */
/*                                                                           */
/* Description               : This routine creates the grand/               */
/*                             acceptable master                             */
/*                                                                           */
/* Input                     : pvPtpNode       - Pointer to node with index  */
/*                             u1MasterDataSet - acc / grand master          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.GMClusterPoolId                */
/*                             gPtpGlobalInfo.AccMasterPoolId                */
/*                             gPtpGlobalInfo.DomainTree                     */
/*                             gPtpGlobalInfo.GrandMastClustLst              */
/*                             gPtpGlobalInfo.AccMastLst                     */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
PtpMastCreate (VOID *pvPtpNode, UINT1 u1MasterDataSet)
{
    tPtpDomain         *pPtpDomain = NULL;
    UINT1              *pu1Master = NULL;
    VOID               *pvPtpTblPtr = NULL;
    tMemPoolId          MemPoolId = 0;
    UINT4               u4StructSize = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1DomainId = 0;

    PTP_FN_ENTRY ();

    if ((u1MasterDataSet != (UINT1) PTP_ACC_MASTER_DATA_SET) &&
        (u1MasterDataSet != (UINT1) PTP_GRAND_MASTER_DATA_SET))
    {
        /* Invalid Master Data set Passed */
        return NULL;
    }

    if (u1MasterDataSet == (UINT1) PTP_ACC_MASTER_DATA_SET)
    {
        MemPoolId = gPtpGlobalInfo.AccMasterPoolId;
        u4StructSize = sizeof (tPtpAccMasterTable);
        pvPtpTblPtr = gPtpGlobalInfo.AccMastLst;
        u4ContextId = ((tPtpAccMasterTable *) (pvPtpNode))->u4ContextId;
        u1DomainId = ((tPtpAccMasterTable *) (pvPtpNode))->u1DomainId;
    }
    else if (u1MasterDataSet == (UINT1) PTP_GRAND_MASTER_DATA_SET)
    {
        MemPoolId = gPtpGlobalInfo.GMClusterPoolId;
        u4StructSize = sizeof (tPtpGrandMasterTable);
        pvPtpTblPtr = gPtpGlobalInfo.GrandMastClustLst;
        u4ContextId = ((tPtpGrandMasterTable *) (pvPtpNode))->u4ContextId;
        u1DomainId = ((tPtpGrandMasterTable *) (pvPtpNode))->u1DomainId;
    }

    /* Check whether the domain exists */

    pPtpDomain = PtpDmnGetDomainEntry (u4ContextId, u1DomainId);

    if (pPtpDomain == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId, ALL_FAILURE_TRC,
                  "Domain %d does not exists\r\n"));
        PTP_FN_EXIT ();
        return NULL;
    }

    pu1Master = MemAllocMemBlk (MemPoolId);

    if (pu1Master == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Memory Allocation failed for master creation\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MAX_MASTER);
        PTP_FN_EXIT ();
        return NULL;
    }

    MEMSET (pu1Master, 0, u4StructSize);
    MEMCPY (pu1Master, (UINT1 *) pvPtpNode, u4StructSize);

    if (PtpDbAddNode (pvPtpTblPtr, pu1Master, u1MasterDataSet) == OSIX_FAILURE)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "Unable to add master node into data structure\r\n"));

        MemReleaseMemBlock (MemPoolId, pu1Master);
        pu1Master = NULL;
        PTP_FN_EXIT ();
        return NULL;
    }
    if (u1MasterDataSet == (UINT1) PTP_ACC_MASTER_DATA_SET)
    {
        ((tPtpAccMasterTable *) (VOID *) pu1Master)->pPtpDomain = pPtpDomain;
    }
    else
    {
        ((tPtpGrandMasterTable *) (VOID *) pu1Master)->pPtpDomain = pPtpDomain;
    }
    PTP_TRC ((u4ContextId, u1DomainId, INIT_SHUT_TRC | MGMT_TRC,
              "Master is created successfully\r\n"));

    PTP_FN_EXIT ();
    return pu1Master;
}

/*****************************************************************************/
/* Function                  : PtpMastDelete                                 */
/*                                                                           */
/* Description               : This routine deletes the grand/               */
/*                             acceptable master                             */
/*                                                                           */
/* Input                     : pvPtpNode       - Node pointer                */
/*                             u1MasterDataSet - acc / grand master          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.GMClusterPoolId                */
/*                             gPtpGlobalInfo.AccMasterPoolId                */
/*                             gPtpGlobalInfo.GrandMastClustLst              */
/*                             gPtpGlobalInfo.AccMastLst                     */
/*                             gPtpGlobalInfo.UnicastMastLst                 */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpMastDelete (VOID *pvPtpNode, UINT1 u1MasterDataSet)
{
    VOID               *pvPtpTblPtr = NULL;
    UINT1              *pu1NodePtr = NULL;
    tMemPoolId          MemPoolId = 0;

    PTP_FN_ENTRY ();

    if (u1MasterDataSet == (UINT1) PTP_ACC_MASTER_DATA_SET)
    {
        MemPoolId = gPtpGlobalInfo.AccMasterPoolId;
        pvPtpTblPtr = gPtpGlobalInfo.AccMastLst;
    }
    else if (u1MasterDataSet == (UINT1) PTP_GRAND_MASTER_DATA_SET)
    {
        MemPoolId = gPtpGlobalInfo.GMClusterPoolId;
        pvPtpTblPtr = gPtpGlobalInfo.GrandMastClustLst;
    }

    if (pvPtpTblPtr != NULL)
    {
        /* A valid list has been obtained either Acceptable or grandmaster
         * Free the node 
         * */
        pu1NodePtr = PtpDbDeleteNode (pvPtpTblPtr, pvPtpNode, u1MasterDataSet);
        MemReleaseMemBlock (MemPoolId, pu1NodePtr);
        pvPtpTblPtr = NULL;
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpMastAccUnicastMasterCreate                 */
/*                                                                           */
/* Description               : This routine creates the unicast master       */
/*                                                                           */
/* Input                     : pPtpAccUnicastMasterTable - Node to be added  */
/*                                                         with index        */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.UnicMasterPoolId               */
/*                             gPtpGlobalInfo.UnicastMastLst                 */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpAccUnicastMasterTable *
PtpMastAccUnicastMasterCreate (tPtpAccUnicastMasterTable *
                               pPtpAccUnicastMasterTable)
{
    tPtpDomain         *pPtpDomain = NULL;
    tPtpAccUnicastMasterTable *pPtpAllocAccUnicastMasterTable = NULL;

    PTP_FN_ENTRY ();

    /* Check whether the domain exists */
    pPtpDomain = PtpDmnGetDomainEntry (pPtpAccUnicastMasterTable->u4ContextId,
                                       pPtpAccUnicastMasterTable->u1DomainId);

    if (pPtpDomain == NULL)
    {
        PTP_TRC ((pPtpAccUnicastMasterTable->u4ContextId,
                  pPtpAccUnicastMasterTable->u1DomainId,
                  ALL_FAILURE_TRC, "Domain does not exists\r\n"));
        PTP_FN_EXIT ();
        return NULL;
    }

    pPtpAllocAccUnicastMasterTable =
        (tPtpAccUnicastMasterTable *) MemAllocMemBlk (gPtpGlobalInfo.
                                                      UnicMasterPoolId);

    if (pPtpAllocAccUnicastMasterTable == NULL)
    {
        PTP_TRC ((pPtpAccUnicastMasterTable->u4ContextId,
                  pPtpAccUnicastMasterTable->u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Memory Allocation failed " "for master creation\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MAX_MASTER);
        PTP_FN_EXIT ();
        return NULL;
    }

    MEMSET (pPtpAllocAccUnicastMasterTable, 0,
            sizeof (tPtpAccUnicastMasterTable));

    MEMCPY (pPtpAllocAccUnicastMasterTable,
            pPtpAccUnicastMasterTable, sizeof (tPtpAccUnicastMasterTable));

    if (PtpDbAddNode (gPtpGlobalInfo.UnicastMastLst,
                      pPtpAllocAccUnicastMasterTable,
                      (UINT1) PTP_UNICAST_MASTER_DATA_SET) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpAccUnicastMasterTable->u4ContextId,
                  pPtpAccUnicastMasterTable->u1DomainId,
                  ALL_FAILURE_TRC,
                  "Unable to add master node into data structure\r\n"));

        MemReleaseMemBlock (gPtpGlobalInfo.UnicMasterPoolId,
                            (UINT1 *) pPtpAllocAccUnicastMasterTable);
        pPtpAllocAccUnicastMasterTable = NULL;
        PTP_FN_EXIT ();
        return NULL;
    }

    PTP_TRC ((pPtpAccUnicastMasterTable->u4ContextId,
              pPtpAccUnicastMasterTable->u1DomainId,
              INIT_SHUT_TRC | MGMT_TRC, "Master is created successfully\r\n"));

    PTP_FN_EXIT ();
    return pPtpAllocAccUnicastMasterTable;
}

/*****************************************************************************/
/* Function                  : PtpMastAccUnicastMasterDelete                 */
/*                                                                           */
/* Description               : This routine deletes the acceptable master    */
/*                                                                           */
/* Input                     : pPtpAccUnicastMasterTable - Node to be added  */
/*                                                         with index        */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.UnicMasterPoolId               */
/*                             gPtpGlobalInfo.UnicastMastLst                 */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpMastAccUnicastMasterDelete (tPtpAccUnicastMasterTable *
                               pPtpAccUnicastMasterTable)
{
    UINT1              *pu1NodePtr = NULL;
#ifdef TRACE_WANTED
    UINT4               u4ContextId = pPtpAccUnicastMasterTable->u4ContextId;
    UINT1               u1DomainId = pPtpAccUnicastMasterTable->u1DomainId;
#endif

    PTP_FN_ENTRY ();

    pu1NodePtr = PtpDbDeleteNode (gPtpGlobalInfo.UnicastMastLst,
                                  pPtpAccUnicastMasterTable,
                                  (UINT1) PTP_UNICAST_MASTER_DATA_SET);
    MemReleaseMemBlock (gPtpGlobalInfo.UnicMasterPoolId, pu1NodePtr);
    pPtpAccUnicastMasterTable = NULL;

    PTP_TRC ((u4ContextId, u1DomainId, INIT_SHUT_TRC | MGMT_TRC,
              "Master is deleted successfully\r\n"));
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpMastGetFirstFornMasterOnPort               */
/*                                                                           */
/* Description               : This routine finds the first Foreign Master   */
/*                             Data Set received over the given interface.   */
/*                                                                           */
/* Input                     : pPtpPort  - Port whose value of Foreign Master*/
/*                                         needs to be found.                */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpForeignMasterDs *
PtpMastGetFirstFornMasterOnPort (tPtpPort * pPtpPort)
{
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;
    tPtpForeignMasterDs PtpForeignMasterDs;

    PTP_FN_ENTRY ();
    MEMSET (&PtpForeignMasterDs, 0, sizeof (tPtpForeignMasterDs));

    PtpForeignMasterDs.u4ContextId = pPtpPort->u4ContextId;
    PtpForeignMasterDs.u1DomainId = pPtpPort->u1DomainId;

    do
    {
        pPtpForeignMasterDs = (tPtpForeignMasterDs *)
            PtpDbGetNextNode (gPtpGlobalInfo.FMTree, &PtpForeignMasterDs,
                              (UINT1) PTP_FOREIGN_MASTER_DATA_SET);

        if (pPtpForeignMasterDs == NULL)
        {
            /* Port has no Foreign MAster Records */
            PTP_FN_EXIT ();
            return pPtpForeignMasterDs;
        }
        PtpForeignMasterDs.u4ContextId = pPtpForeignMasterDs->u4ContextId;
        PtpForeignMasterDs.u1DomainId = pPtpForeignMasterDs->u1DomainId;
        PtpForeignMasterDs.u4PortIndex = pPtpForeignMasterDs->u4PortIndex;
        MEMCPY (PtpForeignMasterDs.ClkId, pPtpForeignMasterDs->ClkId,
                PTP_MAX_CLOCK_ID_LEN);
        PtpForeignMasterDs.u4SrcPortIndex = pPtpForeignMasterDs->u4SrcPortIndex;

    }
    while (pPtpForeignMasterDs->u4PortIndex !=
           pPtpPort->PortDs.u4PtpPortNumber);

    /* Now, pPtpForeignMasterDs represents the first Foreign Master Data Set
     * received over the given interface. 
     * */

    if ((pPtpForeignMasterDs->u4ContextId != pPtpPort->u4ContextId) ||
        (pPtpForeignMasterDs->u1DomainId != pPtpPort->u1DomainId))
    {
        /* Port has no Foreign MAster Records */
        PTP_FN_EXIT ();
        return NULL;
    }

    PTP_FN_EXIT ();
    return pPtpForeignMasterDs;
}

/*****************************************************************************/
/* Function                  : PtpMastDeleteFMEntriesInPort                  */
/*                                                                           */
/* Description               : This routine scans the foreign master entries */
/*                             and delete the entries in this port           */
/*                                                                           */
/* Input                     : pPtpPort  - Port whose value of Foreign Master*/
/*                                         needs to be deleted               */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpMastDeleteFMEntriesInPort (tPtpPort * pPtpPort)
{
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;
    tPtpForeignMasterDs PtpForeignMasterDs;

    PTP_FN_ENTRY ();
    MEMSET (&PtpForeignMasterDs, 0, sizeof (tPtpForeignMasterDs));

    PtpForeignMasterDs.u4ContextId = pPtpPort->u4ContextId;
    PtpForeignMasterDs.u1DomainId = pPtpPort->u1DomainId;

    while (((pPtpForeignMasterDs = (tPtpForeignMasterDs *)
             PtpDbGetNextNode (gPtpGlobalInfo.FMTree, &PtpForeignMasterDs,
                               (UINT1) PTP_FOREIGN_MASTER_DATA_SET))
            != NULL) &&
           (pPtpPort->u4ContextId == pPtpForeignMasterDs->u4ContextId) &&
           (pPtpPort->u1DomainId == pPtpForeignMasterDs->u1DomainId))
    {
        PtpForeignMasterDs.u4PortIndex = pPtpForeignMasterDs->u4PortIndex;
        MEMCPY (PtpForeignMasterDs.ClkId, pPtpForeignMasterDs->ClkId,
                PTP_MAX_CLOCK_ID_LEN);
        PtpForeignMasterDs.u4SrcPortIndex = pPtpForeignMasterDs->u4SrcPortIndex;

        if (pPtpForeignMasterDs->u4PortIndex ==
            pPtpPort->PortDs.u4PtpPortNumber)
        {
            PtpDbDeleteNode (gPtpGlobalInfo.FMTree, pPtpForeignMasterDs,
                             (UINT1) PTP_FOREIGN_MASTER_DATA_SET);

            MemReleaseMemBlock (gPtpGlobalInfo.ForeignMasterPoolId,
                                (UINT1 *) pPtpForeignMasterDs);
            pPtpForeignMasterDs = NULL;
        }
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpMastDeleteAccEntriesInDmn                  */
/*                                                                           */
/* Description               : This routine scans the acceptable master      */
/*                             entries and delete the entries in this port   */
/*                                                                           */
/* Input                     : pPtpDomain - Domain whose value of acceptable */
/*                                          Master needs to be deleted       */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.AccMastLst.                    */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpMastDeleteAccEntriesInDmn (tPtpDomain * pPtpDomain)
{
    /* Delete the Acceptable Master Table Entries */
    tPtpAccMasterTable  PtpAccMasterTable;
    tPtpAccMasterTable *pPtpAccMasterTable = NULL;

    MEMSET (&PtpAccMasterTable, 0, sizeof (tPtpAccMasterTable));

    PtpAccMasterTable.u4ContextId = pPtpDomain->u4ContextId;
    PtpAccMasterTable.u1DomainId = pPtpDomain->u1DomainId;

    while ((pPtpAccMasterTable = (tPtpAccMasterTable *)
            PtpDbGetNextNode (gPtpGlobalInfo.AccMastLst,
                              &(PtpAccMasterTable),
                              (UINT1) PTP_ACC_MASTER_DATA_SET)) != NULL)
    {
        MEMCPY (&PtpAccMasterTable, pPtpAccMasterTable,
                sizeof (tPtpAccMasterTable));

        if ((pPtpDomain->u4ContextId == pPtpAccMasterTable->u4ContextId) &&
            (pPtpDomain->u1DomainId == pPtpAccMasterTable->u1DomainId))
        {
            PtpMastDelete (pPtpAccMasterTable, (UINT1) PTP_ACC_MASTER_DATA_SET);
        }
        else
        {
            break;
        }
    }
}
#endif /*_PTPMAST_C_*/
