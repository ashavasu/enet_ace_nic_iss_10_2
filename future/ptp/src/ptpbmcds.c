/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains PTP Best Master Clock Algorithm 
 *              Data Set updation Implementation. 
 *********************************************************************/
#ifndef _PTPBMCDS_C_
#define _PTPBMCDS_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function                  : PtpBmcDsUpdtDataSetForM1andM2                 */
/*                                                                           */
/* Description               : This routine updates the data sets of the     */
/*                             port when the decision code for those ports   */
/*                             are set to M1 and M2.                         */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpBmcDsUpdtDataSetForM1andM2 (tPtpPort * pPtpPort)
{
    tPtpCurrentDs      *pPtpCurrentDs = NULL;
    tPtpParentDs       *pPtpParentDs = NULL;
    tPtpTimeDs         *pPtpTimeDs = NULL;
    tPtpDomain         *pPtpDomain = pPtpPort->pPtpDomain;
    BOOL1               bResult = OSIX_FALSE;

    PTP_FN_ENTRY ();

    /* Refer Table 13; Section 9.3.5 of IEEE Std 1588-2008 */

    /* Current Data Set updations */
    pPtpCurrentDs = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs);

    pPtpCurrentDs->u4StepsRemoved = 0;
    FSAP_U8_CLR (&(pPtpCurrentDs->u8OffsetFromMaster));
    FSAP_U8_CLR (&(pPtpCurrentDs->u8MeanPathDelay));

    /* Parent Data Set Updations */
    pPtpParentDs = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs);

    pPtpParentDs->u4PortIndex = 0;

    MEMCPY (&(pPtpParentDs->ClkId),
            &(pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId),
            PTP_MAX_CLOCK_ID_LEN);
    MEMCPY (&(pPtpParentDs->GMClkId),
            &(pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId),
            PTP_MAX_CLOCK_ID_LEN);

    pPtpParentDs->u1GMPriority1 =
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority1;
    pPtpParentDs->u1GMPriority2 =
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority2;

    MEMCPY (&(pPtpParentDs->GMClkQuality),
            &(pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality),
            sizeof (tPtpClkQuality));

    /* Time Data Set Updations */
    pPtpTimeDs = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs);

    PTP_IS_TIMESCALE_PTP (pPtpPort->pPtpDomain, bResult);

    if (bResult == OSIX_TRUE)
    {
        /* PTP Time Scale:- Refer Section 9.4 of IEEE 1588-2008 */
        /* leap59, leap61 & currentUtcOffset are values that will be obtained
         * from the primary reference clock. If it had been obtained, then we
         * need to use the obtained value, or else it can be left as FALSE. 
         * */
        if (pPtpTimeDs->u2CurrentUtcOffset == OSIX_FALSE)
        {
            pPtpTimeDs->bCurrentUtcOffsetValid = OSIX_TRUE;
        }
        else
        {
            pPtpTimeDs->bCurrentUtcOffsetValid = OSIX_FALSE;
        }
        pPtpTimeDs->u1TimeSource = PTP_TIME_SRC_PTP;
        pPtpTimeDs->bTimeTraceable = OSIX_FALSE;
        pPtpTimeDs->bFreqTraceable = OSIX_FALSE;
        pPtpTimeDs->bPtpTimeScale = OSIX_TRUE;
    }

    PTP_IS_TIMESCALE_ARB ((pPtpPort->pPtpDomain), bResult);
    if (bResult == OSIX_TRUE)
    {
        /* Arbitary time scale:- Refer Section 9.4 of IEEE 1588-2008 */
        pPtpTimeDs->bLeap59 = OSIX_FALSE;
        pPtpTimeDs->bLeap61 = OSIX_FALSE;
        pPtpTimeDs->u1TimeSource = PTP_INTERNAL_OSCILLATOR;
        pPtpTimeDs->u1TimeSource = PTP_TIME_SRC_PTP;
        pPtpTimeDs->bTimeTraceable = OSIX_FALSE;
        pPtpTimeDs->bFreqTraceable = OSIX_FALSE;
        pPtpTimeDs->bPtpTimeScale = OSIX_TRUE;
    }

    /* Init the Path Trace Tlv for M1 and M2 state */
    if ((PtpPTrcIsPathTraceEnabled (pPtpPort->pPtpDomain)) == OSIX_TRUE)
    {
        PtpPTrcInitPTrcList (pPtpPort->pPtpDomain);
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "PtpBmcDsUpdtDataSetForM1andM2 "
                  ": Empty the Path Trace Tlv for M1/M2 State\r\n"));
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpBmcDsUpdtDataSetForM3                      */
/*                                                                           */
/* Description               : This routine updates the data sets of the     */
/*                             port when the decision code for those ports   */
/*                             are set to M3.                                */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpBmcDsUpdtDataSetForM3 (tPtpPort * pPtpPort)
{
    tPtpNotifyInfo      PtpNotifyInfo;

    PTP_FN_ENTRY ();

    /* Refer Table 14; Section 9.3.5 of IEEE Std 1588-2008 */
    if (pPtpPort->PortDs.u1PortState == PTP_STATE_LISTENING)
    {
        /* This may occur when Announce Receipt out timer expires in 
         * LISTENING state. Unless a port comes out of LISTENING state BMC
         * will not be executer over the port. Hence the value of 
         * u1RecommendedState will be invalid in this case
         * */
        PTP_FN_EXIT ();
        return;
    }

    if ((pPtpPort->PortDs.u1PortState == PTP_STATE_PREMASTER) &&
        (pPtpPort->PortDs.u1RecommendedState == PTP_STATE_MASTER))
    {
        /* Wait for Qualification timer expiry event
         * */
        PTP_FN_EXIT ();
        return;
    }

    if (pPtpPort->PortDs.u1PortState != pPtpPort->PortDs.u1RecommendedState)
    {

        /* Port State change occured. Send a Notification */
        PtpNotifyInfo.u4ContextId = pPtpPort->u4ContextId;
        PtpNotifyInfo.u4DomainId = (UINT4) pPtpPort->u1DomainId;
        PtpNotifyInfo.u4PortId = pPtpPort->PortDs.u4PtpPortNumber;
        PtpNotifyInfo.u4PortState = (UINT4) pPtpPort->PortDs.u1PortState;
        PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                      (UINT1) PTP_TRAP_PORT_STATE_CHANGE);
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpBmcDsUpdtDataSetForP1AndP2                 */
/*                                                                           */
/* Description               : This routine updates the data sets of the     */
/*                             port when the decision code for those ports   */
/*                             are set to P1 & P2                            */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpBmcDsUpdtDataSetForP1AndP2 (tPtpPort * pPtpPort)
{
    tPtpNotifyInfo      PtpNotifyInfo;

    PTP_FN_ENTRY ();

    /* Refer Table 15; Section 9.3.5 of IEEE Std 1588-2008 */
    if (pPtpPort->PortDs.u1PortState != PTP_STATE_LISTENING)
    {
        /* This may occur when Announce Receipt out timer expires in 
         * LISTENING state. Unless a port comes out of LISTENING state BMC
         * will not be executer over the port. Hence the value of 
         * u1RecommendedState will be invalid in this case
         * */
        if (pPtpPort->PortDs.u1PortState != pPtpPort->PortDs.u1RecommendedState)
        {

            /* Port State change occured. Send a Notification */
            PtpNotifyInfo.u4ContextId = pPtpPort->u4ContextId;
            PtpNotifyInfo.u4DomainId = (UINT4) pPtpPort->u1DomainId;
            PtpNotifyInfo.u4PortId = pPtpPort->PortDs.u4PtpPortNumber;
            PtpNotifyInfo.u4PortState = (UINT4) pPtpPort->PortDs.u1PortState;
            PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                          (UINT1) PTP_TRAP_PORT_STATE_CHANGE);
        }
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpBmcDsUpdtDataSetForS1                      */
/*                                                                           */
/* Description               : This routine updates the data sets of the     */
/*                             port when the decision code for those ports   */
/*                             are set to S1. This updates the port data set */
/*                             based on the selected Ebset message.          */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port                */
/*                             pPtpEbestMsg - Message Choosen as Ebest Msg.  */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpBmcDsUpdtDataSetForS1 (tPtpPort * pPtpPort,
                          tPtpForeignMasterDs * pPtpEbestMsg)
{
    tPtpNotifyInfo      PtpNotifyInfo;
    tPtpCurrentDs      *pPtpCurrentDs = NULL;
    tPtpParentDs       *pPtpParentDs = NULL;
    tPtpTimeDs         *pPtpTimeDs = NULL;

    PTP_FN_ENTRY ();

    /* Refer Table 16; Section 9.3.5 of IEEE Std 1588-2008 */
    /* Current Data Set updations */
    pPtpCurrentDs = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs);
    pPtpCurrentDs->u4StepsRemoved =
        pPtpEbestMsg->AnnounceMsg.u4StepsRemoved + 1;

    /* Parent Data Set Updations */
    pPtpParentDs = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs);

    MEMCPY (&(pPtpParentDs->ClkId), &(pPtpEbestMsg->ClkId),
            PTP_MAX_CLOCK_ID_LEN);
    pPtpParentDs->u4PortIndex = pPtpEbestMsg->u4SrcPortIndex;

    MEMCPY (&(pPtpParentDs->GMClkId), &(pPtpEbestMsg->AnnounceMsg.GMClkId),
            PTP_MAX_CLOCK_ID_LEN);
    MEMCPY (&(pPtpParentDs->GMClkQuality),
            &(pPtpEbestMsg->AnnounceMsg.GMClkQuality), sizeof (tPtpClkQuality));

    pPtpParentDs->u1GMPriority1 = pPtpEbestMsg->AnnounceMsg.u1GMPriority1;
    pPtpParentDs->u1GMPriority2 = pPtpEbestMsg->AnnounceMsg.u1GMPriority2;

    /* Time Data Set Updations */
    pPtpTimeDs = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs);

    pPtpTimeDs->u2CurrentUtcOffset =
        pPtpEbestMsg->AnnounceMsg.u2CurrentUtcOffset;

    /* Getting the values from FLAGs of the received messages */
    pPtpTimeDs->bCurrentUtcOffsetValid =
        pPtpEbestMsg->AnnounceMsg.bCurrentUtcOffsetValid;
    pPtpTimeDs->bLeap59 = pPtpEbestMsg->AnnounceMsg.bLeap59;
    pPtpTimeDs->bLeap61 = pPtpEbestMsg->AnnounceMsg.bLeap61;
    pPtpTimeDs->bTimeTraceable = pPtpEbestMsg->AnnounceMsg.bTimeTraceable;
    pPtpTimeDs->bFreqTraceable = pPtpEbestMsg->AnnounceMsg.bFreqTraceable;

    if (pPtpPort->PortDs.u1PortState != PTP_STATE_LISTENING)
    {
        /* This may occur when Announce Receipt out timer expires in 
         * LISTENING state. Unless a port comes out of LISTENING state BMC
         * will not be executer over the port. Hence the value of 
         * u1RecommendedState will be invalid in this case
         * */
        if (pPtpPort->PortDs.u1PortState != pPtpPort->PortDs.u1RecommendedState)
        {

            /* Port State change occured. Send a Notification */
            PtpNotifyInfo.u4ContextId = pPtpPort->u4ContextId;
            PtpNotifyInfo.u4DomainId = (UINT4) pPtpPort->u1DomainId;
            PtpNotifyInfo.u4PortId = pPtpPort->PortDs.u4PtpPortNumber;
            PtpNotifyInfo.u4PortState = (UINT4) pPtpPort->PortDs.u1PortState;
            PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                          (UINT1) PTP_TRAP_PORT_STATE_CHANGE);
        }
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpBmcDsUpdtDataSets                          */
/*                                                                           */
/* Description               : This routine updates the data sets of the     */
/*                             port when the configuration for those ports   */
/*                             gets changed so that port states are being    */
/*                             calculated exactly on next BMC interval.      */
/*                                                                           */
/* Input                     : pPtpDomain-Pointer to the domain.             */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpBmcDsUpdtDataSets (tPtpDomain * pPtpDomain)
{
    tPtpParentDs       *pPtpParentDs = NULL;
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4Port = 0;
    BOOL1               bResult = OSIX_FALSE;

    for (u4Port = 1; u4Port <= PTP_MAX_PORTS; u4Port++)
    {
        OSIX_BITLIST_IS_BIT_SET (pPtpDomain->DomainPortList, u4Port,
                                 PTP_PORT_LIST_SIZE, bResult);

        if (bResult != OSIX_TRUE)
        {
            continue;
        }
        /* Port is member of the domain. */
        pPtpPort = PtpIfGetPortEntry (pPtpDomain->u4ContextId,
                                      pPtpDomain->u1DomainId, u4Port);
        if (pPtpPort == NULL)
        {
            continue;
        }

        /* ASSUMPTION:
         * This will be invoked when there is only one port and that too
         * did not receive a valid ErBest message
         * */
        if ((pPtpPort->PortDs.u1PortState == PTP_STATE_MASTER) ||
            (pPtpPort->PortDs.DecisionCode == M1) ||
            (pPtpPort->PortDs.DecisionCode == M2))
        {
            /* Parent Data Set Updations. Only Priority1 and Priority2 are 
             * Configurable for Parent Data Set
             * */
            pPtpParentDs = &(pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.
                             ParentDs);
            pPtpParentDs->u1GMPriority1 =
                pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority1;
            pPtpParentDs->u1GMPriority2 =
                pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority2;

            /* Configurable CLKIWF Params */
            pPtpParentDs->GMClkQuality.u1Class =
                pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class;
            pPtpParentDs->GMClkQuality.u1Accuracy =
                pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Accuracy;
            pPtpParentDs->GMClkQuality.u2OffsetScaledLogVariance =
                pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.
                u2OffsetScaledLogVariance;
        }
    }                            /* For all the ports in the domain */
}
#endif /* _PTPBMCDS_C_ */
