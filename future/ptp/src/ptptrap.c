/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptptrap.c,v 1.7 2014/03/01 11:39:18 siva Exp $
 *
 * Description: This file contains PTP trap implementation.       
 *********************************************************************/
#ifndef _PTPTRAP_C_
#define _PTPTRAP_C_

#include "ptpincs.h"
#include "fsptp.h"
#include "ptptrap.h"
#include "snmputil.h"

PRIVATE INT4 PtpTrapIsTrapEnabled PROTO ((UINT2 u2TrapType));

CHR1               *gac1PtpTrapMsg[] = {
    NULL,
    "PortStateChanged",
    "MemAllocFailed",
    "ContextAdminStateChanged",
    "ClockSysControlChanged",
    "ClockUnicastStateChanged",
    "PortPtpAdminStateChanged",
    "PortSyncFault",
    "AccepatbleMasterFault",
    "GrandMasterFault"
};

CHR1               *gac1PtpSyslogMsg[] = {
    NULL,
    "Port state Changed by BMC",
    "Global Memory/Buffer Allocation failed",
    "Context Admin state changed",
    "Clock System Control Status Changed",
    "Clock Unicast Option Status Changed",
    "Port Ptp Admin State changed",
    "Synchronization fault on Port",
    "Grand Master failed",
    "Acceptable Master failed"
};

/**************************************************************************
 *  Function                  : PtpTrapMakeObjIdFrmString
 * 
 *  Description               : This Function retuns the OID  
 *                              of the given string for the
 *                              proprietary MIB.
 * 
 *  Input                     : pi1TextStr - pointer to the string.
 *                              pTableName - TableName has to be fetched.
 * 
 *  Output                    : None
 *
 *  Global Variables Referred : None
 *
 *  Global variables Modified : None
 *
 *  Use of Recursion          : None
 *
 *  Returns                   : pOidPtr or NULL
 * ***********************************************************************/

PRIVATE tSNMP_OID_TYPE *
PtpTrapMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount = 0;
    INT1                ai1TempBuffer[PTP_OBJECT_NAME_LEN + 1];
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    PTP_FN_ENTRY ();

    MEMSET (ai1TempBuffer, 0, sizeof (ai1TempBuffer));

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*pi1TextStr) != 0)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pu1TempPtr = (UINT1 *) pi1TextStr;

        for (u2Index = 0;
             ((pu1TempPtr < (UINT1 *) pi1DotPtr) &&
              (u2Index < PTP_OBJECT_NAME_LEN)); u2Index++)
        {
            ai1TempBuffer[u2Index] = *pu1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0; pTableName[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP (pTableName[u2Index].pName, (INT1 *) ai1TempBuffer)
                 == 0) && (STRLEN ((INT1 *) ai1TempBuffer) ==
                           STRLEN (pTableName[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                         STRLEN (pTableName[u2Index].pNumber) + 1);
                break;
            }
        }
        if (pTableName[u2Index].pName == NULL)
        {
            PTP_FN_EXIT ();
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN (pi1DotPtr) + 1);
    }
    else
    {
        /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    for (u2Index = 0; ((u2Index <= PTP_OBJECT_NAME_LEN) &&
                       (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidPtr == NULL)
    {
        PTP_FN_EXIT ();
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (PtpTrapParseSubIdNew
            (&pu1TempPtr, &(pOidPtr->pu4_OidList[u2Index])) == OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            PTP_FN_EXIT ();
            return (NULL);
        }

        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            PTP_FN_EXIT ();
            return (NULL);
        }
    }                            /* end of for loop */

    PTP_FN_EXIT ();
    return (pOidPtr);
}

/******************************************************************************
 *  Function                 : PtpTrapParseSubIdNew
 *
 *  Description              : Parse the string format in 
 *                             number.number..format.
 *
 *  Input                    : ppu1TempPtr - pointer to the string.
 *                             pu4Value    - Pointer the OID List value.
 *
 *  Output                   : value of ppu1TempPtr
 *
 *  Global Variables Referred: None
 *
 *  Global variables Modified: None
 *
 *  Use of Recursion         : None
 *
 *  Returns                  : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/
PRIVATE INT4
PtpTrapParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    PTP_FN_EXIT ();
    return (i4RetVal);
}

/*****************************************************************************
 * Function Name             : PtpTrapSendTrapNotifications               
 *                                                                          
 * Description               : This function will send an SNMP trap to the
 *                             administrator for various PTP conditions.
 *                                                                          
 * Input(s)                  : pNotifyInfo - Information to be sent in 
 *                               the Trap message
 *                               
 *                             u2TrapType - Specific Type for Trap Message      
 *                                                                          
 * Output(s)                 : None
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * Return Value(s)           : VOID
*****************************************************************************/
VOID
PtpTrapSendTrapNotifications (tPtpNotifyInfo * pNotifyInfo, UINT2 u2TrapType)
{
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    UINT4               u4GenTrapType = ENTERPRISE_SPECIFIC;
    UINT4               u4SpecTrapType = PTP_TRAP_INVALID;
    UINT1               au1ContextName[PTP_SWITCH_ALIAS_LEN];
    UINT1               au1Buf[PTP_OBJECT_NAME_LEN];

    PTP_FN_ENTRY ();

    if (PtpTrapIsTrapEnabled (u2TrapType) == OSIX_FAILURE)
    {
        return;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    PTP_TRC ((pNotifyInfo->u4ContextId,
              (UINT1) pNotifyInfo->u4DomainId,
              CONTROL_PLANE_TRC, "Ptp Trap to be generated: %u\n", u2TrapType));

    MEMSET (au1ContextName, 0, sizeof (au1ContextName));
    pContextName = SNMP_AGT_FormOctetString (au1ContextName,
                                             (PTP_SWITCH_ALIAS_LEN - 1));
    if (pContextName == NULL)
    {
        PTP_TRC ((pNotifyInfo->u4ContextId,
                  (UINT1) pNotifyInfo->u4DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpTrapSendNotification: "
                  "OID Memory Allocation Failed for Context Name \r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* Filling the Enterprise OID */
    pEnterpriseOid = alloc_oid (PTP_SNMPV2_TRAP_OID_LEN + 1);

    if (pEnterpriseOid == NULL)
    {
        SNMP_AGT_FreeOctetString (pContextName);
        PTP_TRC ((pNotifyInfo->u4ContextId,
                  (UINT1) pNotifyInfo->u4DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpTrapSendNotification: "
                  "OID Memory Allocation Failed\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, gau4PtpTrapOid,
            sizeof (gau4PtpTrapOid));
    pEnterpriseOid->u4_Length = (UINT4) PTP_SNMPV2_TRAP_OID_LEN + 1;

    /* gau4PtpTrapOid contains the base PTP trap OID
     * 1.3.6.1.4.1.29601.2.45.2.0
     * Each Trap notification type is extended from the base trap OID
     * The Trap takes the value u2TrapType and inherits the OID from the
     * parent OID, gau4PtpTrapOid. So, the last octet is added with u2TrapType
     */
    pEnterpriseOid->pu4_OidList[PTP_SNMPV2_TRAP_OID_LEN] = u2TrapType;

    pSnmpTrapOid = alloc_oid (PTP_SNMPV2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        PTP_TRC ((pNotifyInfo->u4ContextId,
                  (UINT1) pNotifyInfo->u4DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "PtpTrapSendNotification: "
                  "OID Memory Allocation Failed\r\n"));
        SNMP_AGT_FreeOctetString (pContextName);
        SNMP_FreeOid (pEnterpriseOid);
        PTP_FN_EXIT ();
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, gau4PtpSnmpTrapOid,
            sizeof (gau4PtpSnmpTrapOid));
    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pContextName);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        PTP_FN_EXIT ();
        return;
    }

    pStartVb = pVbList;

    /* Filling the Context-Name for all trap type excpet 
     * fsPtpGlobalErrorTrap  */
    if (u2TrapType != PTP_TRAP_GLOB_ERR)
    {
        MEMSET (au1Buf, 0, sizeof (au1Buf));
        SPRINTF ((char *) au1Buf, "fsPtpTrapContextName");

        pOid = PtpTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                          (UINT1 *) fs_ptp_orig_mib_oid_table);

        if (pOid == NULL)
        {
            SNMP_AGT_FreeOctetString (pContextName);
            SNMP_AGT_FreeVarBindList (pStartVb);

            PTP_TRC ((pNotifyInfo->u4ContextId,
                      (UINT1) pNotifyInfo->u4DomainId,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "PtpTrapMakeObjIdFrmString: OID "
                      "for fsPtpTrapContextName Not Found. Failed!!!\r\n"));
            PTP_FN_EXIT ();
            return;
        }

        PtpPortVcmGetAliasName (pNotifyInfo->u4ContextId, au1ContextName);
        pOstring = SNMP_AGT_FormOctetString (au1ContextName,
                                             (PTP_SWITCH_ALIAS_LEN - 1));

        if (pOstring == NULL)
        {
            SNMP_AGT_FreeOctetString (pContextName);
            SNMP_FreeOid (pOid);
            SNMP_AGT_FreeVarBindList (pStartVb);

            PTP_TRC ((pNotifyInfo->u4ContextId,
                      (UINT1) pNotifyInfo->u4DomainId,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "PtpTrapMakeObjIdFrmString: Failed "
                      "to form Octet string for fsPtpTrapContextName.\r\n"));
            PTP_FN_EXIT ();
            return;
        }

        pVbList->pNextVarBind =
            SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                  pOstring, NULL, SnmpCnt64Type);

        if (pVbList->pNextVarBind == NULL)
        {
            SNMP_AGT_FreeOctetString (pContextName);
            SNMP_FreeOid (pOid);
            SNMP_AGT_FreeOctetString (pOstring);
            SNMP_AGT_FreeVarBindList (pStartVb);

            PTP_TRC ((pNotifyInfo->u4ContextId,
                      (UINT1) pNotifyInfo->u4DomainId,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "PtpTrapMakeObjIdFrmString: Failed "
                      "to form VarBind for fsPtpTrapContextName.\r\n"));
            PTP_FN_EXIT ();
            return;
        }

        pVbList = pVbList->pNextVarBind;
    }

    switch (u2TrapType)
    {
        case PTP_TRAP_SYS_CNTL_CHANGE:
            /* Filling System Control Change */
            MEMSET (au1Buf, 0, sizeof (au1Buf));
            SPRINTF ((char *) au1Buf, "fsPtpContextRowStatus");

            pOid =
                PtpTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                           (UINT1 *) fs_ptp_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_TRC ((pNotifyInfo->u4ContextId,
                          (UINT1) pNotifyInfo->u4DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "PtpTrapMakeObjIdFrmString: OID "
                          "for fsPtpContextRowStatus Not Found. Failed !\r\n"));
                PTP_FN_EXIT ();
                return;
            }

            /* Append the Index : fsPtpContextRowStatus.ContextId */
            pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4ContextId;
            pOid->u4_Length++;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_INTEGER32, 0,
                                      pNotifyInfo->u4ContextRowState,
                                      NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_FN_EXIT ();
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case PTP_TRAP_SYS_ADMIN_CHANGE:
            /* Filling Admin Status */

            MEMSET (au1Buf, 0, sizeof (au1Buf));
            SPRINTF ((char *) au1Buf, "fsPtpAdminStatus");

            pOid =
                PtpTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                           (UINT1 *) fs_ptp_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_TRC ((pNotifyInfo->u4ContextId,
                          (UINT1) pNotifyInfo->u4DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "PtpTrapMakeObjIdFrmString: OID "
                          "for fsPtpAdminStatus Not Found. Failed !\r\n"));
                PTP_FN_EXIT ();
                return;
            }

            /* Append the Index : fsPtpAdminStatus.ContextId */
            pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4ContextId;
            pOid->u4_Length++;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_INTEGER32, 0,
                                      pNotifyInfo->u4ContextAdminState,
                                      NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_FN_EXIT ();
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case PTP_TRAP_PORT_STATE_CHANGE:
            /* Filling the Port State Change */
            MEMSET (au1Buf, 0, sizeof (au1Buf));
            SPRINTF ((char *) au1Buf, "fsPtpPortState");

            pOid =
                PtpTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                           (UINT1 *) fs_ptp_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_TRC ((pNotifyInfo->u4ContextId,
                          (UINT1) pNotifyInfo->u4DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "PtpTrapMakeObjIdFrmString: OID "
                          "for fsPtpPortState Not Found. Failed !\r\n"));
                PTP_FN_EXIT ();
                return;
            }

            /* Append the Index : fsPtpPortState.ContextId.DomainId.PortId */
            pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4ContextId;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4DomainId;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4PortId;
            pOid->u4_Length++;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_INTEGER32, 0,
                                      pNotifyInfo->u4PortState,
                                      NULL, NULL, SnmpCnt64Type);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_FN_EXIT ();
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case PTP_TRAP_PORT_ADMIN_CHANGE:
            /* Filling the Port Admin Status */
            MEMSET (au1Buf, 0, sizeof (au1Buf));
            SPRINTF ((char *) au1Buf, "fsPtpPortPtpStatus");
            pOid =
                PtpTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                           (UINT1 *) fs_ptp_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_AGT_FreeVarBindList (pStartVb);
                PTP_TRC ((pNotifyInfo->u4ContextId,
                          (UINT1) pNotifyInfo->u4DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "PtpTrapMakeObjIdFrmString: OID "
                          "for fsPtpPortPtpStatus Not Found. Failed !\r\n"));
                PTP_FN_EXIT ();
                return;
            }

            /* Append the Index : fsPtpPortPtpStatus.ContextId.
             * DomainId.PortId */
            pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4ContextId;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4DomainId;
            pOid->u4_Length++;
            pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4PortId;
            pOid->u4_Length++;

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_INTEGER32, 0,
                                      pNotifyInfo->u4PortAdminStatus,
                                      NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_FN_EXIT ();
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case PTP_TRAP_GLOB_ERR:

            /* Filling the Global Err Type */
            MEMSET (au1Buf, 0, sizeof (au1Buf));
            SPRINTF ((char *) au1Buf, "fsPtpGlobalErrTrapType");
            pOid =
                PtpTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                           (UINT1 *) fs_ptp_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_TRC ((pNotifyInfo->u4ContextId,
                          (UINT1) pNotifyInfo->u4DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "PtpTrapMakeObjIdFrmString: OID "
                          "for fsPtpGlobalErrorTrap Not Found. Failed !\r\n"));
                PTP_FN_EXIT ();
                return;
            }

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_INTEGER32, 0,
                                      pNotifyInfo->u4ErrType,
                                      NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_FN_EXIT ();
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        case PTP_TRAP_SYNC_FAULT:
        case PTP_TRAP_GRAND_MASTER_FAULT:
        case PTP_TRAP_ACCEPT_MASTER_FAULT:

            /* Add  fsPtpTrapDomainNumber Objects */
            MEMSET (au1Buf, 0, sizeof (au1Buf));
            SPRINTF ((char *) au1Buf, "fsPtpTrapDomainNumber");
            pOid =
                PtpTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                           (UINT1 *) fs_ptp_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_AGT_FreeVarBindList (pStartVb);
                PTP_TRC ((pNotifyInfo->u4ContextId,
                          (UINT1) pNotifyInfo->u4DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "PtpTrapMakeObjIdFrmString: OID "
                          "for fsPtpTrapDomainNumber Not Found. Failed !\r\n"));
                PTP_FN_EXIT ();
                return;
            }

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_INTEGER32, 0,
                                      pNotifyInfo->u4DomainId,
                                      NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_FN_EXIT ();
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;

            /* Filling the Global Err Type */

            MEMSET (au1Buf, 0, sizeof (au1Buf));
            SPRINTF ((char *) au1Buf, "fsPtpGlobalErrTrapType");

            pOid =
                PtpTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                           (UINT1 *) fs_ptp_orig_mib_oid_table);
            if (pOid == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_TRC ((pNotifyInfo->u4ContextId,
                          (UINT1) pNotifyInfo->u4DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "PtpTrapMakeObjIdFrmString: OID "
                          "for fsPtpGlobalErrorTrap Not Found. Failed !\r\n"));
                PTP_FN_EXIT ();
                return;
            }

            pVbList->pNextVarBind = (tSNMP_VAR_BIND *)
                SNMP_AGT_FormVarBind (pOid,
                                      SNMP_DATA_TYPE_INTEGER32, 0,
                                      pNotifyInfo->u4ErrType,
                                      NULL, NULL, SnmpCnt64Type);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_AGT_FreeOctetString (pContextName);
                SNMP_FreeOid (pOid);
                SNMP_AGT_FreeVarBindList (pStartVb);

                PTP_FN_EXIT ();
                return;
            }

            pVbList = pVbList->pNextVarBind;
            pVbList->pNextVarBind = NULL;
            break;

        default:
            SNMP_AGT_FreeOctetString (pContextName);
            SNMP_AGT_FreeVarBindList (pStartVb);
            PTP_TRC (((UINT4) pNotifyInfo->u4ContextId,
                      (UINT1) pNotifyInfo->u4DomainId,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "Ptp Trap type out of range: %u\n", u2TrapType));
            return;
            break;
    }

    PtpPortFmNotifyFaults (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                           pStartVb, (UINT1 *) gac1PtpSyslogMsg[u2TrapType],
                           pContextName);

    SNMP_AGT_FreeOctetString (pContextName);
    PTP_FN_EXIT ();
}

/*****************************************************************************
 * Function Name             : PtpTrapIsTrapEnabled
 *                                                                          
 * Description               : This function will check whether the trap is
 *                             enabled or not.
 *                                                                          
 * Input(s)                  : u2TrapType  - Trap Type
 *                               
 * Output(s)                 : None
 *
 * Global Variables Referred : gPtpGlobalInfo.u2TrapControl
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * Return Value(s)           : OSIX_SUCCESS / OSIX_FAILURE
*****************************************************************************/
PRIVATE INT4
PtpTrapIsTrapEnabled (UINT2 u2TrapType)
{
    switch (u2TrapType)
    {
        case PTP_TRAP_SYS_CNTL_CHANGE:
            if ((gPtpGlobalInfo.u2TrapControl & PTP_TRAP_MASK_SYS_CTRL_CHNG)
                == PTP_TRAP_MASK_SYS_CTRL_CHNG)
            {
                return OSIX_SUCCESS;
            }

            break;

        case PTP_TRAP_SYS_ADMIN_CHANGE:
            if ((gPtpGlobalInfo.u2TrapControl & PTP_TRAP_MASK_SYS_ADMIN_CHNG)
                == PTP_TRAP_MASK_SYS_ADMIN_CHNG)
            {
                return OSIX_SUCCESS;
            }

            break;

        case PTP_TRAP_PORT_STATE_CHANGE:
            if ((gPtpGlobalInfo.u2TrapControl & PTP_TRAP_MASK_PORT_STATE_CHNG)
                == PTP_TRAP_MASK_PORT_STATE_CHNG)
            {
                return OSIX_SUCCESS;
            }

            break;

        case PTP_TRAP_PORT_ADMIN_CHANGE:
            if ((gPtpGlobalInfo.u2TrapControl & PTP_TRAP_MASK_PORT_ADMIN_CHNG)
                == PTP_TRAP_MASK_PORT_ADMIN_CHNG)
            {
                return OSIX_SUCCESS;
            }

            break;

        case PTP_TRAP_GLOB_ERR:
            if ((gPtpGlobalInfo.u2TrapControl & PTP_TRAP_MASK_GLOB_ERR)
                == PTP_TRAP_MASK_GLOB_ERR)
            {
                return OSIX_SUCCESS;
            }

            break;

        case PTP_TRAP_SYNC_FAULT:
            if ((gPtpGlobalInfo.u2TrapControl & PTP_TRAP_MASK_SYNC_FAULT)
                == PTP_TRAP_MASK_SYNC_FAULT)
            {
                return OSIX_SUCCESS;
            }

            break;

        case PTP_TRAP_GRAND_MASTER_FAULT:
            if ((gPtpGlobalInfo.u2TrapControl & PTP_TRAP_MASK_GM_FAULT)
                == PTP_TRAP_MASK_GM_FAULT)
            {
                return OSIX_SUCCESS;
            }

            break;

        case PTP_TRAP_ACCEPT_MASTER_FAULT:
            if ((gPtpGlobalInfo.u2TrapControl & PTP_TRAP_MASK_ACC_MASTER_FAULT)
                == PTP_TRAP_MASK_ACC_MASTER_FAULT)
            {
                return OSIX_SUCCESS;
            }

            break;

        default:
            break;
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function                  : PtpTrapSendGlobErrTrap                        */
/*                                                                           */
/* Description               : This routine triggers a trap to be generated  */
/*                             whenever memory allocation failure of buffer  */
/*                             allocation failure occurs in the PTP module.  */
/*                                                                           */
/* Input                     : u1ErrType - Type of failure occured.          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpTrapSendGlobErrTrap (UINT1 u1ErrType)
{
    tPtpNotifyInfo      PtpNotifyInfo;

    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));

    PtpNotifyInfo.u4ErrType = (UINT4) u1ErrType;
    PtpTrapSendTrapNotifications (&PtpNotifyInfo, (UINT1) PTP_TRAP_GLOB_ERR);
}

/*****************************************************************************
 * Function Name             : PtpTrapSendPreMastNotification
 *                                                                          
 * Description               : This function egresses a trap when the port 
 *                             first moves to PRE-MASTER state.
 *                                                                          
 * Input(s)                  : pPtpPort - Pointer to Port Information.
 *                               
 * Output(s)                 : None
 *
 * Global Variables Referred : None.                          
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * Return Value(s)           : None.                       
*****************************************************************************/
PUBLIC VOID
PtpTrapSendPreMastNotification (tPtpPort * pPtpPort)
{
    tPtpNotifyInfo      PtpNotifyInfo;

    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));

    PtpNotifyInfo.u4ContextId = pPtpPort->u4ContextId;
    PtpNotifyInfo.u4DomainId = (UINT4) pPtpPort->u1DomainId;
    PtpNotifyInfo.u4PortId = pPtpPort->PortDs.u4PtpPortNumber;
    PtpNotifyInfo.u4PortState = (UINT4) PTP_STATE_PREMASTER;
    PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                  (UINT1) PTP_TRAP_PORT_STATE_CHANGE);
}
#endif /*_PTPTRAP_C_*/
