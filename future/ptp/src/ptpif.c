/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *  $Id: ptpif.c,v 1.5 2014/02/07 13:27:43 siva Exp $
 *
 * Description: This file contains PTP interface related utilities 
 *              implementation.       
 *********************************************************************/
#ifndef _PTPIF_C_
#define _PTPIF_C_

#include "ptpincs.h"

/*****************************************************************************/
/* Function                  : PtpIfDelPort                                  */
/*                                                                           */
/* Description               : This routine deletes the given interface from */
/*                             PTP. This also frees the memory allocated for */
/*                             this port.                                    */
/*                                                                           */
/* Input                     : pPtpPort -   Pointer to Port   that needs to  */
/*                                          be deleted.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.PortPoolId                     */
/*                             gPtpGlobalInfo.PortTree                       */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpIfDelPort (tPtpPort * pPtpPort)
{
    UINT4               u4ContextId;
    UINT1               u1DomainId;

    PTP_FN_ENTRY ();

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC, "Port Deletion Failed!!!\r\n"));
        return;

    }

    u4ContextId = pPtpPort->u4ContextId;
    u1DomainId = pPtpPort->u1DomainId;

    /* All the State Event machines need to be brought to disable state */
    PtpIfDisablePort (pPtpPort);

    if (pPtpPort->PortDs.u1DelayMechanism == PTP_PORT_DELAY_MECH_PEER_TO_PEER)
    {
        PtpTmrStopPdelayReqTimer (pPtpPort);
    }
    /* Scan through the foreign master table in this context and domain
     * and delete the entries whose u4PortIndex is equal to the port
     * number
     */
    PtpMastDeleteFMEntriesInPort (pPtpPort);

    PtpDbDeleteNode (gPtpGlobalInfo.PortTree, pPtpPort,
                     (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET);

    PtpDbDeleteNode (gPtpGlobalInfo.pPortHashTable, pPtpPort,
                     (UINT1) PTP_PORT_CONFIG_HASH_DATA_SET);

    PTP_TRC ((u4ContextId, u1DomainId, INIT_SHUT_TRC | MGMT_TRC,
              "Port %d is deleted\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    if (pPtpPort->bIsTranPort == OSIX_FALSE)
    {
        pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u4CurrentNoOfPorts--;
    }
    else
    {
        pPtpPort->pPtpDomain->ClockDs.TransClkDs.u4CurrentNoOfPorts--;
    }

    MemReleaseMemBlock (gPtpGlobalInfo.PortPoolId, (UINT1 *) pPtpPort);
    pPtpPort = NULL;

    /* Do not update the free index here. The free index linearly allocates
     * the interface index. On reaching the maximum number, it will 
     * automatically wrap up and start searching from index "1".
     * */
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpIfEnablePort                               */
/*                                                                           */
/* Description               : This routine enables PTP over the given port. */
/*                                                                           */
/* Input                     : pPtpPort -   Pointer to interface             */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpIfEnablePort (tPtpPort * pPtpPort)
{
    tCfaIfInfo          IfInfo;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    tIp6Addr            PTPMCastAddr;
    tPtpNotifyInfo      PtpNotifyInfo;
    UINT1               u1OperStatus = (UINT1) CFA_IF_UP;
    UINT4               u4Port = 0;
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    /* Check the row status of the port entry
     * 1. If the row status is not active, return without enabling the port
     * 2. If the row status is active, then check the oper status if the
     *    device type is ipv4/ipv6/port. If the oper status is up, then
     *    enable the port, else this sem will be activated during port
     *    oper up indication
     */

    if (pPtpPort->PtpDeviceType == PTP_IFACE_IEEE_802_3)
    {
        MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

        if (PtpPortCfaGetIfInfo (pPtpPort->u4IfIndex, &IfInfo) == L2IWF_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "PtpIfEnablePort: Port %d Not Created In CFA\r\n",
                      pPtpPort->u4IfIndex));
            return;
        }

        u1OperStatus = (UINT1) IfInfo.u1IfOperStatus;
    }
    else if (pPtpPort->PtpDeviceType == PTP_IFACE_UDP_IPV4)
    {
        MEMSET (&NetIpv4IfInfo, 0, sizeof (tNetIpv4IfInfo));

        if (PtpPortNetIpv4GetPortFromIfIndex (pPtpPort->u4IfIndex,
                                              &u4Port) == NETIPV4_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "PtpIfEnablePort: Port %d Not Created In IP\r\n",
                      pPtpPort->u4IfIndex));
            return;
        }

        if (PtpPortNetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "PtpIfEnablePort: Local Port %d Not Created In IP\r\n",
                      u4Port));
            return;
        }

        u1OperStatus = (UINT1) NetIpv4IfInfo.u4Oper;
    }
    else if (pPtpPort->PtpDeviceType == PTP_IFACE_UDP_IPV6)
    {
        MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));

        if (PtpPortNetIpv6GetIfInfo (pPtpPort->u4IfIndex,
                                     &NetIpv6IfInfo) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "PtpIfEnablePort: Port %d Not Created In IP6\r\n",
                      pPtpPort->u4IfIndex));
            return;
        }

        u1OperStatus = (UINT1) NetIpv6IfInfo.u4Oper;

        /* Join the Mcast groups
         * FF02:0:0:0:0:0:0:6B 
         * FF0X:0:0:0:0:0:0:b5 MCAST address to this interface */

        MEMCPY (&PTPMCastAddr, &gV6PtpAddr, sizeof (tIp6Addr));

        if ((PtpPortNetIpv6McastJoin (pPtpPort->u4IfIndex, &PTPMCastAddr)) ==
            OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "MCast Group Join for the Port %d is failde\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
        }

        MEMCPY (&PTPMCastAddr, &gV6PtpP2PAddr, sizeof (tIp6Addr));
        if ((PtpPortNetIpv6McastJoin (pPtpPort->u4IfIndex, &PTPMCastAddr)) ==
            OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "MCast Group Join for the Port %d is failde\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
        }
    }

    if (u1OperStatus != (UINT1) CFA_IF_UP)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpIfEnablePort: Port %d Is Oper Down \r\n",
                  pPtpPort->u4IfIndex));
        pPtpPort->PortDs.u1PtpEnabledStatus = PTP_ENABLED;
        return;
    }

    /* Add the port in the domain */
    OSIX_BITLIST_SET_BIT (pPtpPort->pPtpDomain->DomainPortList,
                          pPtpPort->PortDs.u4PtpPortNumber, PTP_PORT_LIST_SIZE);

    if (pPtpPort->bIsTranPort == OSIX_FALSE)
    {
        if (pPtpPort->pPtpDomain->ClkMode != PTP_FORWARD_MODE)
        {
            /* Trigger the SEM to move to enabled state */
            PtpSemPortSemHandler (pPtpPort, PTP_DESG_ENABLED_EVENT);
        }
    }

    pPtpPort->PortDs.u1PtpEnabledStatus = PTP_ENABLED;
    /* Program the status in NPAPI */

    if (pPtpPort->PtpDeviceType == PTP_IFACE_UDP_IPV4)
    {
        PtpUdpAddMembership (u4Port);
    }

    PtpNpWrConfigPtpStatus (pPtpPort, PTP_ENABLED);
    PtpNpWrInitPortParams (pPtpPort);

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              INIT_SHUT_TRC | MGMT_TRC,
              "Port %d is enabled\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    /* Send Notification for Port Ptp Admin Enabled */
    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));
    PtpNotifyInfo.u4ContextId = pPtpPort->u4ContextId;
    PtpNotifyInfo.u4PortId = pPtpPort->PortDs.u4PtpPortNumber;
    PtpNotifyInfo.u4PortAdminStatus = PTP_ENABLED;
    PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                  (UINT1) PTP_TRAP_PORT_ADMIN_CHANGE);

    /* If the delay mechanism is P2P, then start peer delay request timer
     * here
     * */
    if ((pPtpPort->pPtpDomain->ClkMode != PTP_FORWARD_MODE) &&
        (pPtpPort->PortDs.u1DelayMechanism == PTP_PORT_DELAY_MECH_PEER_TO_PEER))
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Configured delay mechanism in Port %u "
                  "is Peer to peer\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "Starting Peer Delay Request timer.....\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        u4MilliSec = PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                                u1MinPdelayReqInterval);
        if (PtpTmrStartTmr (&(pPtpPort->PdelayReqTimer),
                            u4MilliSec, PTP_PDREQ_TMR, OSIX_TRUE)
            == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Unable to Start Peer delay "
                      "request timer for port %u\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
        }
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpIfDisablePort                              */
/*                                                                           */
/* Description               : This routine disables PTP over the given port.*/
/*                                                                           */
/* Input                     : pPtpPort -   Pointer to interface             */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpIfDisablePort (tPtpPort * pPtpPort)
{
    tIp6Addr            PTPMCastAddr;
    tPtpNotifyInfo      PtpNotifyInfo;
    UINT4               u4Port = 0;

    PTP_FN_ENTRY ();

    if (pPtpPort->bIsTranPort == OSIX_FALSE)
    {
        /* Trigger the SEM to move to disabled state */
        PtpSemPortSemHandler (pPtpPort, PTP_DESG_DISABLED_EVENT);
    }

    /* Stop all the timers that are running over this interface */
    PtpTmrStopAllTimersForPort (pPtpPort);

    pPtpPort->PortDs.u1PtpEnabledStatus = PTP_DISABLED;

    /* Program the status in NPAPI */
    PtpNpWrConfigPtpStatus (pPtpPort, PTP_DISABLED);

    /* Remove the port in the domain */
    OSIX_BITLIST_RESET_BIT (pPtpPort->pPtpDomain->DomainPortList,
                            pPtpPort->PortDs.u4PtpPortNumber,
                            PTP_PORT_LIST_SIZE);

    if (pPtpPort->PtpDeviceType == PTP_IFACE_UDP_IPV6)
    {
        /* Leave the Mcast groups
         * FF02:0:0:0:0:0:0:6B 
         * FF0X:0:0:0:0:0:0:b5 MCAST address to this interface */
        MEMCPY (&PTPMCastAddr, &gV6PtpAddr, sizeof (tIp6Addr));

        if ((PtpPortNetIpv6McastLeave (pPtpPort->u4IfIndex, &PTPMCastAddr)) ==
            OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "MCast Group Leave for the Port %d is failde\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
        }

        MEMCPY (&PTPMCastAddr, &gV6PtpP2PAddr, sizeof (tIp6Addr));

        if ((PtpPortNetIpv6McastLeave (pPtpPort->u4IfIndex, &PTPMCastAddr)) ==
            OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "MCast Group Leave for the Port %d is failde\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
        }
    }

    if (pPtpPort->PtpDeviceType == PTP_IFACE_UDP_IPV4)
    {
        if (PtpPortNetIpv4GetPortFromIfIndex (pPtpPort->u4IfIndex, &u4Port)
            == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC,
                      "Port %d is not present in IP\r\n", pPtpPort->u4IfIndex));
            return;
        }

        PtpUdpRemoveMembership (u4Port);
    }

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              INIT_SHUT_TRC | MGMT_TRC,
              "Port %d is disabled\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    /* Send Notification for Port Ptp Admin Disabled */
    MEMSET (&PtpNotifyInfo, 0, sizeof (tPtpNotifyInfo));
    PtpNotifyInfo.u4ContextId = pPtpPort->u4ContextId;
    PtpNotifyInfo.u4PortId = pPtpPort->PortDs.u4PtpPortNumber;
    PtpNotifyInfo.u4PortAdminStatus = PTP_DISABLED;
    PtpTrapSendTrapNotifications (&PtpNotifyInfo,
                                  (UINT1) PTP_TRAP_PORT_ADMIN_CHANGE);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpIfCreatePort                               */
/*                                                                           */
/* Description               : This routine creates the given port in the PTP*/
/*                             module. It also adds the port into the data   */
/*                             structure of the PTP module.                  */
/*                                                                           */
/* Input                     : u4ContextId  - Pointer to Domain.             */
/*                             u1DomainId   - Pointer to Domain.             */
/*                             u4PtpPortNum - Ptp port identifier            */
/*                                                                           */
/* Output                    : gPtpGlobalInfo.PortPoolId                     */
/*                             gPtpGlobalInfo.DomainTree                     */
/*                             gPtpGlobalInfo.PortTree                       */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpPort    *
PtpIfCreatePort (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4PtpPortNum)
{
    tPtpPort           *pPtpPort = NULL;
    tPtpDomain         *pPtpDomain = NULL;

    PTP_FN_ENTRY ();

    /* Check whether the domain exists in the module */
    pPtpDomain = PtpDmnGetDomainEntry (u4ContextId, u1DomainId);

    if (pPtpDomain == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId, ALL_FAILURE_TRC | MGMT_TRC,
                  "The domain does not exists\r\n"));
        PTP_FN_EXIT ();
        return NULL;
    }

    pPtpPort = (tPtpPort *) MemAllocMemBlk (gPtpGlobalInfo.PortPoolId);

    if (pPtpPort == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Unable to allocate memory for port in context\r\n"));

        CLI_SET_ERR (CLI_PTP_ERR_MAX_PORTS);
        PTP_FN_EXIT ();
        return NULL;
    }

    /* Initialize the allocated memory */
    MEMSET (pPtpPort, 0, sizeof (tPtpPort));

    pPtpPort->pPtpDomain = pPtpDomain;

    pPtpPort->PortDs.u4PtpPortNumber = u4PtpPortNum;

    pPtpPort->pPtpErBestMsg = NULL;

    MEMSET (&(pPtpPort->PtpPortStats), 0, sizeof (tPtpPortStats));

    pPtpPort->u4ContextId = pPtpDomain->u4ContextId;
    pPtpPort->u1DomainId = pPtpDomain->u1DomainId;

    if (PtpDbAddNode (gPtpGlobalInfo.PortTree, pPtpPort,
                      (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET) == OSIX_FAILURE)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Unable to add port node into data structure.\r\n"));

        MemReleaseMemBlock (gPtpGlobalInfo.PortPoolId, (UINT1 *) pPtpPort);
        pPtpPort = NULL;
        PTP_FN_EXIT ();
        return NULL;
    }

    /* Initialise the port config entry */
    pPtpPort->PortDs.u1MinDelayReqInterval = (UINT1) PTP_PORT_DEF_DELAY_REQ_INT;
    pPtpPort->PortDs.u1AnnounceInterval =
        (UINT1) PTP_PORT_DEF_ANNOUNCE_INTERVAL;
    pPtpPort->PortDs.u1AnnounceReceiptTimeOut =
        (UINT1) PTP_PORT_DEF_ANNOUNCE_RECEIPT_TIMEOUT;
    pPtpPort->PortDs.i1SyncInterval = (INT1) PTP_PORT_DEF_SYNC_TIMEOUT;

    FSAP_STR2_U8 ((CHR1 *) PTP_DEF_SYNC_LIMIT, &(pPtpPort->PortDs.u8SyncLimit));

    if (pPtpPort->pPtpDomain->ClockDs.TransClkDs.TransClkType
        == PTP_P2P_TRANSPARENT_CLOCK)
    {
        pPtpPort->PortDs.u1DelayMechanism =
            (UINT1) PTP_PORT_DELAY_MECH_PEER_TO_PEER;
    }
    else
    {
        pPtpPort->PortDs.u1DelayMechanism =
            (UINT1) PTP_PORT_DELAY_MECH_END_TO_END;
    }
    pPtpPort->PortDs.u1MinPdelayReqInterval =
        (UINT1) PTP_PORT_DEF_PEER_DELAY_REQ_INT;
    pPtpPort->PortDs.u4VersionNumber = PTP_VERSION_TWO;
    pPtpPort->unicastNegOption = (BOOL1) OSIX_FALSE;
    pPtpPort->u2UnicastMastMaxSize = (UINT2) PTP_PORT_DEF_MASTER_SIZE;
    pPtpPort->bAccMastEnabled = (BOOL1) PTP_DISABLED;
    pPtpPort->bAltMulcastSync = PTP_DISABLED;
    pPtpPort->bIsEligibleAltMst = (BOOL1) OSIX_FALSE;
    pPtpPort->u1AltMulcastSynInterval = 0;
    pPtpPort->PortDs.u1PtpEnabledStatus = PTP_DISABLED;

    pPtpPort->PortDs.u1PortState = PTP_STATE_DISABLED;

    MEMCPY (pPtpPort->PortDs.ClkId,
            pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId,
            sizeof (tClkId));

    PTP_TRC ((u4ContextId, u1DomainId, INIT_SHUT_TRC | MGMT_TRC,
              "Port %d is created\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    PTP_FN_EXIT ();
    return pPtpPort;
}

/*****************************************************************************/
/* Function                  : PtpIfGetNextAvailableIndex                    */
/*                                                                           */
/* Description               : This routine gives the next free port that    */
/*                             can be used for the creation of port config   */
/*                             entry                                         */
/*                                                                           */
/* Input                     : u4ContextId   -   Context identifier          */
/*                             u1DomainId    -   Domain identifier           */
/*                                                                           */
/* Output                    : pu4PortNum    -   Free port number            */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpIfGetNextAvailableIndex (UINT4 u4ContextId, UINT1 u1DomainId,
                            UINT4 *pu4PortNum)
{
    tPtpPort           *pPtpPort = NULL;
    tPtpPort           *pTmpPtpPort = NULL;

    PTP_FN_ENTRY ();

    *pu4PortNum = 0;

    /* Scan all the port config entries in this context and domain
     * and return the next available free index
     */
    pTmpPtpPort = (tPtpPort *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);
    if (pTmpPtpPort == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpIfGetNextAvailableIndex: Memory Allocation for "
                  "Port Table failed!!!\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* Assign Index */
    pTmpPtpPort->u4ContextId = u4ContextId;
    pTmpPtpPort->u1DomainId = u1DomainId;
    pTmpPtpPort->PortDs.u4PtpPortNumber = 0;

    while ((pPtpPort = (tPtpPort *)
            PtpDbGetNextNode (gPtpGlobalInfo.PortTree,
                              pTmpPtpPort,
                              (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET)) != NULL)
    {
        if (pPtpPort->PortDs.u4PtpPortNumber != (*pu4PortNum + 1))
        {
            break;
        }
        pTmpPtpPort->PortDs.u4PtpPortNumber = pPtpPort->PortDs.u4PtpPortNumber;
        *pu4PortNum = (*pu4PortNum) + 1;
    }

    *pu4PortNum = (*pu4PortNum) + 1;

    /* Release Memory allocated for the Index */
    MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId, (UINT1 *) pTmpPtpPort);

    pTmpPtpPort = NULL;
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpIfGetPortNumber                            */
/*                                                                           */
/* Description               : This routine Gets the PTP Port Number of port */
/*                             in PTP. This can be used to delete port entry */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpIfGetPortNumber (INT4 i4ContextId, UINT1 u1DomainId, INT4 i4IfType,
                    UINT4 u4IfIndex, INT4 *pi4PtpPortNumber)
{

    tPtpPort           *pPtpPort = NULL;
    tPtpPort           *pTmpPtpPort = NULL;

    PTP_FN_ENTRY ();

    /* Scan all the port config entries in this context and domain
     * and return the required Port Number
     */
    pTmpPtpPort = (tPtpPort *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);
    if (pTmpPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4ContextId, u1DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpIfGetPortNumber: Memory Allocation for "
                  "Port Table failed!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Assign Index */
    pTmpPtpPort->u4ContextId = (UINT4) i4ContextId;
    pTmpPtpPort->u1DomainId = u1DomainId;
    pTmpPtpPort->PortDs.u4PtpPortNumber = 0;

    while ((pPtpPort = (tPtpPort *)
            PtpDbGetNextNode (gPtpGlobalInfo.PortTree,
                              pTmpPtpPort,
                              (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET)) != NULL)
    {
        if ((pPtpPort->u4ContextId != (UINT4) i4ContextId) ||
            (pPtpPort->u1DomainId != u1DomainId))
        {
            break;
        }

        if ((u4IfIndex == pPtpPort->u4IfIndex) &&
            (i4IfType == (INT4) pPtpPort->PtpDeviceType))
        {
            *pi4PtpPortNumber = (INT4) pPtpPort->PortDs.u4PtpPortNumber;
            /* Release Memory allocated for the Index */
            MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                                (UINT1 *) pTmpPtpPort);
            return OSIX_SUCCESS;
        }
        pTmpPtpPort->PortDs.u4PtpPortNumber = pPtpPort->PortDs.u4PtpPortNumber;
    }

    /* Release Memory allocated for the Index */
    MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId, (UINT1 *) pTmpPtpPort);

    pTmpPtpPort = NULL;
    PTP_FN_EXIT ();

    return OSIX_FAILURE;

}

/*****************************************************************************/
/* Function                  : PtpIfHandleOperStatusChg                      */
/*                                                                           */
/* Description               : This routine handles the operationl status    */
/*                             change for the interface.                     */
/*                                                                           */
/* Input                     : u4ContextId   - Context Id                    */
/*                             u4Port        - Ifindex / Vlan Id             */
/*                             PtpDeviceType - Port Type                     */
/*                             u4Status - Operational status.                */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpIfHandleOperStatusChg (UINT4 u4ContextId, UINT4 u4Port,
                          ePtpDeviceType PtpDeviceType, UINT1 u1Status)
{
    tPtpPort           *pPtpPort = NULL;
    tPtpPort           *pTmpPtpPort = NULL;
    UINT4               u4IfIndex = u4Port;

    PTP_FN_ENTRY ();

    /* If the device type is IPv4, then u4Port contains the IP port
     * number. Get the CFA ifindex from the IP port
     */
    if (PtpDeviceType == PTP_IFACE_UDP_IPV4)
    {
        if (PtpPortGetCfaIfIndexFromPort (u4Port, &u4IfIndex) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) u4ContextId, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpIfHandleOperStatusChg:Local Port %d Not Present!!!\r\n",
                      u4Port));
            return;
        }
    }

    /* Allocate Memory for the Index */
    pTmpPtpPort = (tPtpPort *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);
    if (pTmpPtpPort == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, PTP_MAX_DOMAINS,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpIfHandleOperStatusChg: Memory Allocation for Port Table "
                  "failed!!!\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* Assign Index */
    pTmpPtpPort->u4ContextId = u4ContextId;
    pTmpPtpPort->u1DomainId = 0;
    pTmpPtpPort->PortDs.u4PtpPortNumber = 0;

    while (((pPtpPort = (tPtpPort *) PtpDbGetNextNode
             (gPtpGlobalInfo.PortTree, pTmpPtpPort,
              (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET))
            != NULL) && (pPtpPort->u4ContextId == pTmpPtpPort->u4ContextId))
    {
        pTmpPtpPort->u1DomainId = pPtpPort->u1DomainId;
        pTmpPtpPort->PortDs.u4PtpPortNumber = pPtpPort->PortDs.u4PtpPortNumber;

        if ((pPtpPort->PortDs.u1PtpEnabledStatus == PTP_ENABLED) &&
            (pPtpPort->u4IfIndex == u4IfIndex) &&
            (pPtpPort->PtpDeviceType == PtpDeviceType))
        {
            switch (u1Status)
            {
                case CFA_IF_UP:

                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              CONTROL_PLANE_TRC | MGMT_TRC,
                              "Enabling the port %u\r\n",
                              pPtpPort->PortDs.u4PtpPortNumber));
                    PtpIfEnablePort (pPtpPort);
                    break;

                case CFA_IF_DOWN:

                    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                              CONTROL_PLANE_TRC | MGMT_TRC,
                              "Disabling the port %u\r\n",
                              pPtpPort->PortDs.u4PtpPortNumber));
                    if (pPtpPort->bIsTranPort == OSIX_FALSE)
                    {
                        /* Trigger the SEM to move to disabled state */
                        PtpSemPortSemHandler (pPtpPort,
                                              PTP_DESG_DISABLED_EVENT);
                    }

                    /* Stop all the timers that are running over
                     * this interface */
                    PtpTmrStopAllTimersForPort (pPtpPort);

                    if (pPtpPort->PtpDeviceType == PTP_IFACE_UDP_IPV4)
                    {
                        PtpUdpRemoveMembership (u4Port);
                    }

                    /* Program the status in NPAPI */
                    PtpNpWrConfigPtpStatus (pPtpPort, PTP_DISABLED);
                    break;

                default:
                    break;
            }
        }
    }

    MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId, (UINT1 *) pTmpPtpPort);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpIfDeleteInterface                          */
/*                                                                           */
/* Description               : This routine finds the set of domains that the*/
/*                             given interface is part of and deletes the    */
/*                             interface in all the domains.                 */
/*                                                                           */
/* Input                     : u4ContextId   - Context Id                    */
/*                             u4Port        - Ifindex / Vlan Id             */
/*                             PtpDeviceType - Port Type                     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpIfDeleteInterface (UINT4 u4ContextId, UINT4 u4Port,
                      ePtpDeviceType PtpDeviceType)
{
    tPtpPort           *pPtpPort = NULL;
    tPtpPort           *pTmpPtpPort = NULL;
    UINT4               u4IfIndex = u4Port;

    PTP_FN_ENTRY ();

    /* If the device type is IPv4, then u4Port contains the IP port
     * number. Get the CFA ifindex from the IP port
     */
    if (PtpDeviceType == PTP_IFACE_UDP_IPV4)
    {
        if (PtpPortGetCfaIfIndexFromPort (u4Port, &u4IfIndex) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) u4ContextId, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpIfDeleteInterface: Port %d Not Present!!!\r\n",
                      u4Port));
            return;
        }
    }

    /* Allocate Memory for the Index */
    pTmpPtpPort = (tPtpPort *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);
    if (pTmpPtpPort == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, PTP_MAX_DOMAINS,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | ALL_FAILURE_TRC,
                  "PtpIfDeleteInterface: Memory Allocation for Port Table "
                  "failed!!!\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    /* Assign Index */
    pTmpPtpPort->u4ContextId = u4ContextId;
    pTmpPtpPort->u1DomainId = 0;
    pTmpPtpPort->PortDs.u4PtpPortNumber = 0;

    while (((pPtpPort = (tPtpPort *) PtpDbGetNextNode
             (gPtpGlobalInfo.PortTree, pTmpPtpPort,
              (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET))
            != NULL) && (pPtpPort->u4ContextId == pTmpPtpPort->u4ContextId))
    {
        pTmpPtpPort->u1DomainId = pPtpPort->u1DomainId;
        pTmpPtpPort->PortDs.u4PtpPortNumber = pPtpPort->PortDs.u4PtpPortNumber;

        if ((pPtpPort->u4IfIndex == u4IfIndex) &&
            (pPtpPort->PtpDeviceType == PtpDeviceType))
        {
            PtpIfDelPort (pPtpPort);
        }
    }

    MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId, (UINT1 *) pTmpPtpPort);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpIfGetPortEntry                             */
/*                                                                           */
/* Description               : This routine gets the Domain Entry from       */
/*                             the corresponding data base                   */
/*                                                                           */
/* Input                     : u4ContextId   - Context identifier            */
/*                             u1DomainId    - Domain identifier             */
/*                             u4PortId      - Port identifier               */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.UtilityPoolId                  */
/*                             gPtpGlobalInfo.PortPoolId                     */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpPort    *
PtpIfGetPortEntry (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4PortId)
{
    tPtpPort           *pPtpPort = NULL;
    tPtpPort           *pTmpPtpPort = NULL;

    PTP_FN_ENTRY ();

    /* Allocate Memory for the Index */
    pPtpPort = (tPtpPort *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);
    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpIfGetPortEntry : Memory Allocation for Port Table "
                  "failed!!!\r\n"));
        PTP_FN_EXIT ();
        return (NULL);
    }

    /* Assign Index */
    pPtpPort->u4ContextId = u4ContextId;
    pPtpPort->u1DomainId = u1DomainId;
    pPtpPort->PortDs.u4PtpPortNumber = u4PortId;

    /* Get the Node from the Index */
    pTmpPtpPort = (tPtpPort *)
        PtpDbGetNode (gPtpGlobalInfo.PortTree,
                      pPtpPort, (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET);
    /* Release Memory allocated for the Index */
    if ((MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                             (UINT1 *) pPtpPort)) == MEM_FAILURE)
    {
        PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpIfGetPortEntry : MemReleaseMemBlock Failed \r\n"));
    }
    pPtpPort = NULL;
    PTP_FN_EXIT ();
    return (pTmpPtpPort);
}

/*****************************************************************************/
/* Function                  : PtpIfGetNextPortEntry                         */
/*                                                                           */
/* Description               : This routine gets next the Domain Entry from  */
/*                             the corresponding data base                   */
/*                                                                           */
/* Input                     : u4ContextId   - Context identifier            */
/*                             u1DomainId    - Domain identifier             */
/*                             u4PortId      - Port identifier               */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.UtilityPoolId                  */
/*                             gPtpGlobalInfo.PortPoolId                     */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpPort    *
PtpIfGetNextPortEntry (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4PortId)
{
    tPtpPort           *pPtpPort = NULL;
    tPtpPort           *pTmpPtpPort = NULL;

    PTP_FN_ENTRY ();

    /* Allocate Memory for the Index */
    pPtpPort = (tPtpPort *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);
    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpIfGetNextPortEntry : Memory Allocation for Port Table "
                  "failed!!!\r\n"));
        PTP_FN_EXIT ();
        return (NULL);
    }

    /* Assign Index */
    pPtpPort->u4ContextId = u4ContextId;
    pPtpPort->u1DomainId = u1DomainId;
    pPtpPort->PortDs.u4PtpPortNumber = u4PortId;

    /* Get the Next Node from the Index */
    pTmpPtpPort = (tPtpPort *)
        PtpDbGetNextNode (gPtpGlobalInfo.PortTree,
                          pPtpPort, (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET);

    /* Release Memory allocated for the Index */
    if ((MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                             (UINT1 *) pPtpPort)) == MEM_FAILURE)
    {
        PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpIfGetNextPortEntry : MemReleaseMemBlock Failed \r\n"));
    }
    pPtpPort = NULL;
    PTP_FN_EXIT ();
    return (pTmpPtpPort);
}

/*****************************************************************************/
/* Function                  : PtpIfGetPortFromIfTypeAndIndex                */
/*                                                                           */
/* Description               : This routine Gets the PTP Port Number of port */
/*                             in PTP. This can be used to delete port entry */
/*                                                                           */
/* Input                     : i4ContextId   -  Context Id                   */
/*                             u1DomainId    -  Domain identifier            */
/*                             u4IfIndex     -  Interface index / Vlan Id    */
/*                             i4IfType      -  Interface type               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.UtilityPoolId                  */
/*                             gPtpGlobalInfo.pPortHashTable                 */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : Pointer to tPtpPort / NULL                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpPort    *
PtpIfGetPortFromIfTypeAndIndex (UINT4 u4ContextId, UINT1 u1DomainId,
                                UINT4 u4IfIndex, ePtpDeviceType PtpDeviceType)
{
    tPtpPort           *pPtpPort = NULL;
    tPtpPort           *pTmpPtpPort = NULL;

    PTP_FN_ENTRY ();

    pTmpPtpPort = (tPtpPort *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);

    if (pTmpPtpPort == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, u1DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpIfGetPortFromIfTypeAndIndex : Memory Allocation "
                  "for Port Table failed!!!\r\n"));
        PTP_FN_EXIT ();
        return NULL;
    }

    /* Assign Index */
    pTmpPtpPort->u4ContextId = u4ContextId;
    pTmpPtpPort->u1DomainId = u1DomainId;
    pTmpPtpPort->PtpDeviceType = PtpDeviceType;
    pTmpPtpPort->u4IfIndex = u4IfIndex;

    pPtpPort = (tPtpPort *)
        PtpDbGetNode (gPtpGlobalInfo.pPortHashTable, pTmpPtpPort,
                      (UINT1) PTP_PORT_CONFIG_HASH_DATA_SET);

    MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId, (UINT1 *) pTmpPtpPort);

    PTP_FN_EXIT ();
    return pPtpPort;
}

/*****************************************************************************/
/* Function                  : PtpIfDestroyBestMsgOnPort                     */
/*                                                                           */
/* Description               : This routine will destroy the best message    */
/*                             received over the interface. This routine     */
/*                             should be invoked whenever announce receipt   */
/*                             time out occurs for the interface.            */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port.               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpIfDestroyBestMsgOnPort (tPtpPort * pPtpPort)
{
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;

    PTP_FN_ENTRY ();

    if (pPtpPort->pPtpErBestMsg == NULL)
    {
        /* This port does not have a valid Erbest message */
        PTP_FN_EXIT ();
        return;
    }

    /* Find and destroy the entry in the parent 
     * */
    pPtpForeignMasterDs =
        (tPtpForeignMasterDs *) PtpDbDeleteNode (gPtpGlobalInfo.FMTree,
                                                 pPtpPort->pPtpErBestMsg,
                                                 PTP_FOREIGN_MASTER_DATA_SET);
    if (pPtpForeignMasterDs != NULL)
    {
        /* This node would have been added initially into the RBTree. Now, the
         * memory needs to be freed
         * */
        MemReleaseMemBlock (gPtpGlobalInfo.ForeignMasterPoolId,
                            (UINT1 *) pPtpForeignMasterDs);
        pPtpForeignMasterDs = NULL;
    }
    pPtpPort->pPtpErBestMsg = NULL;

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpIfDelayMechanismChg                        */
/*                                                                           */
/* Description               : This routine will stop the peer delay request */
/*                             timers in all the ports in this domain if the */
/*                             delay mechanism is end-to-end and stop all    */
/*                             the peer delay request timer if the delay     */
/*                             mechanism is peer-to-peer                     */
/*                                                                           */
/* Input                     : pPtpDomain  -  Pointer to the Domain          */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None                                          */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
PtpIfDelayMechanismChg (tPtpDomain * pPtpDomain)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4PortId = 0;
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();

    while ((pPtpPort = PtpIfGetNextPortEntry (pPtpDomain->u4ContextId,
                                              pPtpDomain->u1DomainId,
                                              u4PortId)) != NULL)
    {
        /* If the port entry returns the port from the next context or
         * domain, break the loop
         */
        if ((pPtpPort->u4ContextId != pPtpDomain->u4ContextId) ||
            (pPtpPort->u1DomainId != pPtpDomain->u1DomainId))
        {
            break;
        }

        u4PortId = pPtpPort->PortDs.u4PtpPortNumber;

        if (pPtpDomain->ClockDs.TransClkDs.TransClkType ==
            PTP_PORT_DELAY_MECH_PEER_TO_PEER)
        {
            u4MilliSec = PtpUtilLogBase2ToMilliSec
                ((INT1) pPtpPort->PortDs.u1MinPdelayReqInterval);

            if (PtpTmrStartTmr (&(pPtpPort->PdelayReqTimer),
                                u4MilliSec, PTP_PDREQ_TMR,
                                OSIX_TRUE == OSIX_FAILURE))
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, ALL_FAILURE_TRC,
                          "PtpTmrStartTmr function failed !!! \r\n"));
            }

        }
        else
        {
            TmrStop (gPtpGlobalInfo.PtpTmrListId, &(pPtpPort->PdelayReqTimer));
        }
    }

    PTP_FN_EXIT ();
}
#endif /* _PTPIF_C_ */
