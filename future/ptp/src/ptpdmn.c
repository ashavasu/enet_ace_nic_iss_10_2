/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *  $Id: ptpdmn.c,v 1.6 2014/01/24 12:20:08 siva Exp $
 *
 * Description: This file contains PTP domain related utilities 
 *              implementation.       
 *********************************************************************/
#ifndef _PTPDMN_C_
#define _PTPDMN_C_

#include "ptpincs.h"

PRIVATE VOID        PtpDmnGenerateClkId (tPtpDomain * pPtpDomain);

/*****************************************************************************/
/* Function                  : PtpDmnDelDomain                               */
/*                                                                           */
/* Description               : This routine destroys the given domain. It    */
/*                             also destroys the interfaces that are part of */
/*                             the domain.                                   */
/*                                                                           */
/* Input                     : pPtpDomain - Pointer to domain that needs to  */
/*                                          be deleted.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.DomainPoolId                   */
/*                             gPtpGlobalInfo.PortTree                       */
/*                             gPtpGlobalInfo.DomainTree                     */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDmnDelDomain (tPtpDomain * pPtpDomain)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4PortId = 0;

    PTP_FN_ENTRY ();

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Domain Deletion Failed!!!\r\n"));
        return;

    }

    /* Delete All the time scale entries in the domain */
    PtpAlTsDeleteAlTsEntriesInDomain (pPtpDomain);

    /* Delete the Acceptable Master Table Entries */
    PtpMastDeleteAccEntriesInDmn (pPtpDomain);

    /* Scan through all the ports in this domain and delete all
     * the ports. This will in turn initialise the state machine and stop
     * all the timers ruuning in each port
     */

    pPtpPort = PtpIfGetNextPortEntry (pPtpDomain->u4ContextId,
                                      pPtpDomain->u1DomainId, u4PortId);
    while ((pPtpPort != NULL) &&
           (pPtpPort->u4ContextId == pPtpDomain->u4ContextId) &&
           (pPtpPort->u1DomainId == pPtpDomain->u1DomainId))
    {
        u4PortId = pPtpPort->PortDs.u4PtpPortNumber;

        PtpIfDelPort (pPtpPort);

        pPtpPort = PtpIfGetNextPortEntry (pPtpDomain->u4ContextId,
                                          pPtpDomain->u1DomainId, u4PortId);
    }

    PtpDbDeleteNode (gPtpGlobalInfo.DomainTree, pPtpDomain,
                     (UINT1) PTP_DOMAIN_DATA_SET);

    PTP_TRC (((UINT4) pPtpDomain->u4ContextId, (UINT1) pPtpDomain->u1DomainId,
              INIT_SHUT_TRC | MGMT_TRC, "Domain deleted\r\n"));

    MemReleaseMemBlock (gPtpGlobalInfo.DomainPoolId, (UINT1 *) pPtpDomain);
    pPtpDomain = NULL;
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpDmnDelAllDomainsInCxt                      */
/*                                                                           */
/* Description               : This routine destroys all the domains that are*/
/*                             present in the given context identifier. It   */
/*                             also destroys the ports that are being part   */
/*                             of the domains when destroying the domain.    */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier.             */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.DomainTree                     */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDmnDelAllDomainsInCxt (UINT4 u4ContextId)
{
    tPtpDomain         *pPtpDomain = NULL;
    UINT1               u1DomainId = 0;

    PTP_FN_ENTRY ();

    /* Since Domain 0 is aloso valid, get node is called before
     * get next. If the node is not present, then get next node is called
     */
    pPtpDomain = PtpDmnGetDomainEntry (u4ContextId, u1DomainId);

    if (pPtpDomain == NULL)
    {
        pPtpDomain = PtpDmnGetNextDomainEntry (u4ContextId, u1DomainId);
    }

    while ((pPtpDomain != NULL) && (pPtpDomain->u4ContextId == u4ContextId))
    {
        u1DomainId = pPtpDomain->u1DomainId;

        PtpDmnDelDomain (pPtpDomain);

        pPtpDomain = PtpDmnGetNextDomainEntry (u4ContextId, u1DomainId);
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpDmnCreateDomain                            */
/*                                                                           */
/* Description               : This routine creates domain in the given      */
/*                             context identifier with the domain id.        */
/*                                                                           */
/* Input                     : u4ContextId - Context Identifier.             */
/*                             u1DomainId  - Domain Identifier.              */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.DomainPoolId                   */
/*                             gPtpGlobalInfo.DomainTree                     */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to domain / NULL                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpDomain  *
PtpDmnCreateDomain (UINT4 u4ContextId, UINT1 u1DomainId)
{
    tPtpCxt            *pPtpCxt = NULL;
    tPtpDomain         *pPtpDomain = NULL;

    PTP_FN_ENTRY ();

    pPtpCxt = PtpCxtGetNode (u4ContextId);
    if (pPtpCxt == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, (UINT1) u1DomainId,
                  ALL_FAILURE_TRC, "Invalid context identifier passed while "
                  "creating the domain.\r\n"));

        PTP_FN_EXIT ();
        return NULL;
    }

    pPtpDomain = (tPtpDomain *) MemAllocMemBlk (gPtpGlobalInfo.DomainPoolId);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, (UINT1) u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Unable to allocate memory for domain in context\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MAX_DMN);

        PTP_FN_EXIT ();
        return NULL;
    }

    MEMSET (pPtpDomain, 0, sizeof (tPtpDomain));

    pPtpDomain->pPtpCxt = pPtpCxt;
    pPtpDomain->u4ContextId = u4ContextId;

    /* By default PTP operates on Forward mode. */
    pPtpDomain->ClkMode = PTP_FORWARD_MODE;
    pPtpDomain->u1InitInProgress = OSIX_FALSE;

    /* Initialization of data sets are not needed now, as we are operating
     * in the FORWARD mode */

    pPtpDomain->u1DomainId = u1DomainId;

    if (PtpDbAddNode (gPtpGlobalInfo.DomainTree, pPtpDomain,
                      PTP_DOMAIN_DATA_SET) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) u4ContextId, (UINT1) u1DomainId,
                  ALL_FAILURE_TRC,
                  "Unable to add domain node into data structure.\r\n"));

        MemReleaseMemBlock (gPtpGlobalInfo.DomainPoolId, (UINT1 *) pPtpDomain);
        pPtpDomain = NULL;
        PTP_FN_EXIT ();
        return NULL;
    }

    PTP_TRC ((u4ContextId, u1DomainId, INIT_SHUT_TRC | MGMT_TRC,
              "Clock identifer for domain is generated....\r\n"));

    PtpDmnGenerateClkId (pPtpDomain);

    /* Update the clock Mode as Forward in NP */
    PtpNpWrConfigDomainMode (pPtpDomain);

    PTP_TRC (((UINT4) u4ContextId, (UINT1) u1DomainId,
              INIT_SHUT_TRC | MGMT_TRC, "is created\r\n"));

    PTP_FN_EXIT ();

    return pPtpDomain;
}

/*****************************************************************************/
/* Function                  : PtpDmnGetDomainEntry                          */
/*                                                                           */
/* Description               : This routine gets the Domain Entry from the   */
/*                             corresponding data base                       */
/*                                                                           */
/* Input                     : u4ContextId   - Context identifier            */
/*                             u1DomainId    - Domain identifier             */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.UtilityPoolId                  */
/*                             gPtpGlobalInfo.DomainTree                     */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpDomain  *
PtpDmnGetDomainEntry (UINT4 u4ContextId, UINT1 u1DomainId)
{
    tPtpDomain         *pPtpDomain = NULL;
    tPtpDomain         *pTmpPtpDomain = NULL;

    PTP_FN_ENTRY ();

    /* Allocate Memory for the Index */
    pPtpDomain = (tPtpDomain *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, (UINT1) u1DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpDmnGetDomainEntry: Memory Allocation for Domain "
                  "Table failed!!!\r\n"));
        PTP_FN_EXIT ();
        return (NULL);
    }

    /* Assign Index */
    pPtpDomain->u4ContextId = u4ContextId;
    pPtpDomain->u1DomainId = u1DomainId;

    /* Get the Node from the Index */
    pTmpPtpDomain = (tPtpDomain *) PtpDbGetNode (gPtpGlobalInfo.DomainTree,
                                                 pPtpDomain,
                                                 (UINT1) PTP_DOMAIN_DATA_SET);

    /* Release Memory allocated for the Index */
    if ((MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                             (UINT1 *) pPtpDomain)) == MEM_FAILURE)
    {
        PTP_TRC (((UINT4) u4ContextId, (UINT1) u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpDmnGetDomainEntry : MemReleaseMemBlock Failed \r\n"));
    }
    pPtpDomain = NULL;
    PTP_FN_EXIT ();
    return (pTmpPtpDomain);
}

/*****************************************************************************/
/* Function                  : PtpDmnGetNextDomainEntry                      */
/*                                                                           */
/* Description               : This routine gets next the Domain Entry from  */
/*                             the corresponding data base                   */
/*                                                                           */
/* Input                     : u4ContextId   - Context identifier            */
/*                             u1DomainId    - Domain identifier             */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.UtilityPoolId                  */
/*                             gPtpGlobalInfo.DomainTree                     */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpDomain  *
PtpDmnGetNextDomainEntry (UINT4 u4ContextId, UINT1 u1DomainId)
{
    tPtpDomain         *pPtpDomain = NULL;
    tPtpDomain         *pTmpPtpDomain = NULL;

    PTP_FN_ENTRY ();

    /* Allocate Memory for the Index */
    pPtpDomain = (tPtpDomain *) MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, (UINT1) u1DomainId,
                  OS_RESOURCE_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "PtpDmnGetDomainEntry: Memory Allocation for Domain "
                  "Table failed!!!\r\n"));
        PTP_FN_EXIT ();
        return (NULL);
    }

    /* Assign Index */
    pPtpDomain->u4ContextId = u4ContextId;
    pPtpDomain->u1DomainId = u1DomainId;

    /* Get the Next Node from the Index */
    pTmpPtpDomain = (tPtpDomain *)
        PtpDbGetNextNode (gPtpGlobalInfo.DomainTree,
                          pPtpDomain, (UINT1) PTP_DOMAIN_DATA_SET);

    /* Release Memory allocated for the Index */
    if ((MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                             (UINT1 *) pPtpDomain)) == MEM_FAILURE)
    {
        PTP_TRC (((UINT4) u4ContextId, (UINT1) u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpDmnGetNextDomainEntry : MemReleaseMemBlock Failed \r\n"));
    }
    pPtpDomain = NULL;
    PTP_FN_EXIT ();
    return (pTmpPtpDomain);
}

/*****************************************************************************/
/* Function                  : PtpDmnConfigSlaveOnDmnPorts                   */
/*                                                                           */
/* Description               : This routine configures the Slave only option */
/*                             for all the ports present in the domain and   */
/*                             context.                                      */
/*                                                                           */
/* Input                     : u4ContextId   - Context identifier            */
/*                             u1DomainId    - Domain identifier             */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDmnConfigSlaveOnDmnPorts (UINT4 u4ContextId, UINT1 u1DomainId)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4Port = 0;

    PTP_FN_ENTRY ();

    while ((pPtpPort =
            PtpIfGetNextPortEntry (u4ContextId, u1DomainId, u4Port)) != NULL)
    {
        if ((pPtpPort->u4ContextId == u4ContextId) &&
            (pPtpPort->u1DomainId == u1DomainId))
        {
            /* Reinitialize the SEMs */
            PtpIfDisablePort (pPtpPort);

            PtpIfEnablePort (pPtpPort);
            /* Port is member of the domain. Configure the value in NPAPI
             * */
            PtpNpWrInitPortParams (pPtpPort);

        }
        u4ContextId = pPtpPort->u4ContextId;
        u1DomainId = pPtpPort->u1DomainId;
        u4Port = pPtpPort->PortDs.u4PtpPortNumber;
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpDmnInitDomainEntry                         */
/*                                                                           */
/* Description               : This routine initialises the domain entry     */
/*                             based on the configured clock mode            */
/*                                                                           */
/* Input                     : pPtpDomain - Pointer to domain that needs to  */
/*                                          be deleted.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDmnInitDomainEntry (tPtpDomain * pPtpDomain)
{
    tClkIwfPrimParams   ClkIwfPrimParams;

    PTP_FN_ENTRY ();

    if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
        (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
    {
        PtpPortGetClkParams (&ClkIwfPrimParams);

        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag =
            (BOOL1) PTP_DISABLED;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u4NumberOfPorts = (UINT4) 1;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class =
            ClkIwfPrimParams.u1Class;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Accuracy =
            ClkIwfPrimParams.u1Accuracy;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.
            u2OffsetScaledLogVariance =
            (UINT2) ClkIwfPrimParams.u2OffsetScaledLogVariance;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority1 =
            (UINT1) PTP_DEF_CLK_PRIORITY;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority2 =
            (UINT1) PTP_DEF_CLK_PRIORITY;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly = (BOOL1) OSIX_FALSE;
        pPtpDomain->bPathTraceOption = (BOOL1) OSIX_FALSE;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u2AccMasterMaxSize = (UINT2) 0;
        pPtpDomain->bSecurityEnabled = (UINT2) OSIX_FALSE;
        pPtpDomain->u4NumberOfSA = (UINT4) PTP_DEF_SA;

        /* The default Clock quality should be derived from the domain's value
         */
        /* Initialising PTP ParentDS */
        MEMCPY (&(pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.GMClkQuality),
                &(pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality),
                sizeof (tPtpClkQuality));

        MEMCPY (&(pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.GMClkId),
                &(pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId),
                sizeof (tClkId));

        pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u4ObservedClkPhaseChangeRate =
            PTP_CLK_DEF_PHASE_CHANGE_RATE;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u4PortIndex = 0;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u4ObservedDrift = 0;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.
            u2ObservedOffsetScaledLogVariance = PTP_CLK_DEF_LOG_VARIANCE;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u1GMPriority1 =
            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority1;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u1GMPriority2 =
            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority2;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.bParentStats = FALSE;

        /* Initialising PTP TimeDS */
        pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.u2CurrentUtcOffset = (UINT2) 0;
        pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.u1TimeSource = CLK_PTP_CLK;
        pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bCurrentUtcOffsetValid = FALSE;
        pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bLeap59 = FALSE;
        pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bLeap61 = FALSE;
        pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bTimeTraceable = FALSE;
        pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bFreqTraceable = FALSE;
        pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bPtpTimeScale = FALSE;

    }
    else if (pPtpDomain->ClkMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        pPtpDomain->ClockDs.TransClkDs.bTwoStepFlag = (BOOL1) PTP_DISABLED;
        pPtpDomain->ClockDs.TransClkDs.TransClkType =
            PTP_PORT_DELAY_MECH_END_TO_END;

    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpIfGenerateClkId                            */
/*                                                                           */
/* Description               : This routine generates the clock identifier   */
/*                             for the created domain    in the system. Refer*/
/*                             section 7.5.2.2. of IEEE Standard 1588 2008.  */
/*                                                                           */
/* Input                     : pPtpDomain - Pointer to Domain.               */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpDmnGenerateClkId (tPtpDomain * pPtpDomain)
{
    tMacAddr            CxtMacAddr;
    /* IEEE Std 1588 supports, three methods to generate clock identity for the
     * port.
     * 1) IEEE EUI-64 Clock Identity
     * 2) IEEE EUI-48 Clock Identity
     * 3) Non IEEE EUI-64 Clock Identity
     * Aricent IEEE 1588 implementation is compliant with 2nd option IEEE EUI-48
     * Clock identity based generation. A port function will be invoked at the
     * end of this funtion to facilitate other implementations. If other methods
     * needs to be implemented, than the port function can be ported that 
     * overwrites the generated clock identity values.
     * */

    PTP_FN_ENTRY ();
    MEMSET (&CxtMacAddr, 0, sizeof (tMacAddr));

    PtpPortGetSysMacAddress (pPtpDomain->u4ContextId, CxtMacAddr);

    /* MAC Address is of 6 bytes in length. Clock identity will be of 8 bytes 
     * in length. The 6 bytes will be enlarged into 8 bytes by inserting 
     * 0xFF and 0xFE in bytes 3 & 4
     * */

    MEMCPY (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId, CxtMacAddr,
            PTP_3_OCTETS);
    pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId[PTP_VAL_THREE] =
        (INT1) PTP_CLKID_FOURTH_BYTE_VAL;
    pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId[PTP_VAL_FOUR] =
        (INT1) PTP_CLKID_FIFTH_BYTE_VAL;
    pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId[PTP_VAL_FIVE] =
        CxtMacAddr[PTP_VAL_THREE];
    pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId[PTP_VAL_SIX] =
        CxtMacAddr[PTP_VAL_FOUR];
    pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId[PTP_VAL_SEVEN] =
        CxtMacAddr[PTP_VAL_FIVE];

    PtpPortUpdtClkId (pPtpDomain);

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpDmnUpdtPrimaryParams                       */
/*                                                                           */
/* Description               : This routine updates the primary parameters   */
/*                             in all the domains present in the system.     */
/*                                                                           */
/* Input                     : pPtpQMsg - Pointer to Q Message.              */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDmnUpdtPrimaryParams (tPtpQMsg * pPtpQMsg)
{
    tPtpDomain         *pPtpDomain = NULL;
    tClkIwfPrimParams   ClkIwfPrimParams;
    UINT4               u4ContextId = PTP_INV_CONTEXT_ID;
    UINT1               u1DomainId = (UINT1) PTP_MAX_DOMAINS;

    PTP_FN_ENTRY ();

    MEMSET (&ClkIwfPrimParams, 0, sizeof (tClkIwfPrimParams));
    MEMCPY (&ClkIwfPrimParams, (tClkIwfPrimParams *)
            pPtpQMsg->uPtpMsgParam.pPtpMsg, sizeof (tClkIwfPrimParams));

    pPtpDomain = (tPtpDomain *) PtpDbGetFirstNode (gPtpGlobalInfo.DomainTree,
                                                   (UINT1) PTP_DOMAIN_DATA_SET);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) u4ContextId, (UINT1) u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpDmnUpdtPrimaryParams: Domain Not Present\r\n"));
        PTP_FN_EXIT ();
        return;
    }

    do
    {
        if (ClkIwfPrimParams.u1Class == PTP_SLAVEONLY_CLK_CLASS)
        {
            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class =
                ClkIwfPrimParams.u1Class;
            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly = PTP_ENABLED;
            PtpDmnConfigSlaveOnDmnPorts (pPtpDomain->u4ContextId,
                                         pPtpDomain->u1DomainId);
        }
        else if ((pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class ==
                  PTP_SLAVEONLY_CLK_CLASS)
                 && (ClkIwfPrimParams.u1Class != PTP_SLAVEONLY_CLK_CLASS))
        {
            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class =
                ClkIwfPrimParams.u1Class;

            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly = PTP_DISABLED;
            PtpDmnConfigSlaveOnDmnPorts (pPtpDomain->u4ContextId,
                                         pPtpDomain->u1DomainId);

        }

        else
        {
            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class =
                ClkIwfPrimParams.u1Class;

        }

        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Accuracy =
            ClkIwfPrimParams.u1Accuracy;
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.
            u2OffsetScaledLogVariance =
            (UINT2) ClkIwfPrimParams.u2OffsetScaledLogVariance;

        /* Update the parent data set */
        PtpBmcDsUpdtDataSets (pPtpDomain);

        u4ContextId = pPtpDomain->u4ContextId;
        u1DomainId = pPtpDomain->u1DomainId;
    }
    while ((pPtpDomain = PtpDmnGetNextDomainEntry (u4ContextId, u1DomainId))
           != NULL);
}

/*****************************************************************************/
/* Function                  : PtpDmnDisablePorts                            */
/*                                                                           */
/* Description               : This routine disables all the ports in the    */
/*                             domain                                        */
/*                                                                           */
/* Input                     : u4ContextId   -  Context identifier           */
/*                             u1DomainId    -  Domain identifier            */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDmnDisablePorts (UINT4 u4ContextId, UINT1 u1DomainId)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4PortId = 0;

    PTP_FN_ENTRY ();

    while ((pPtpPort =
            PtpIfGetNextPortEntry (u4ContextId, u1DomainId, u4PortId)) != NULL)
    {
        if ((pPtpPort->u4ContextId != u4ContextId) ||
            (pPtpPort->u1DomainId != u1DomainId))
        {
            break;
        }

        u4PortId = pPtpPort->PortDs.u4PtpPortNumber;

        PtpIfDisablePort (pPtpPort);
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpDmnInitDomainEntryCurrentDS                */
/*                                                                           */
/* Description               : This routine initialises the current DS       */
/*                members                       */
/*                                                                           */
/* Input                     : pPtpDomain - Pointer to domain that needs to  */
/*                                          be intialised.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None                                          */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpDmnInitDomainEntryCurrentDS (tPtpDomain * pPtpDomain)
{

    PTP_FN_ENTRY ();

    pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.u8MeanPathDelay.u4Hi = 0;
    pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.u8MeanPathDelay.u4Lo = 0;
    pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.u4StepsRemoved = 0;
    pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.u4CurrentMasterToSlaveDelay = 0;
    pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.u4CurrentSlaveToMasterDelay = 0;
    pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.i1OffsetSign = 0;
    pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.i1DelaySign = 0;

    PTP_FN_EXIT ();
}
#endif /* _PTPDMN_C_ */
