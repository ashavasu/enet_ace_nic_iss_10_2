/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpred.c,v 1.5 2014/05/29 13:19:58 siva Exp $
 *
 * Description: This file contains PTP-HA function.
 *********************************************************************/

#ifndef _PTPRED_C_
#define _PTPRED_C_
#include "ptpincs.h"
/***************************************************************************
 * FUNCTION NAME    : PtpRedInit
 *
 * DESCRIPTION      : Initializes redundancy global variables.This function
 *                    will get invoked during Ptp Initialization.
 *                    It registers with RM.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : ALIX_SUCCESS/OSES_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
PtpRedInit (VOID)
{
    tRmRegParams        RmRegParams;

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));
    MEMSET (&(gPtpRedGlobalInfo), 0, sizeof (tPtpRedGlobalInfo));

    RmRegParams.u4EntId = RM_PTP_APP_ID;
    RmRegParams.pFnRcvPkt = PtpRedRmCallBack;

    /* Registers the PTP protocol with RM */
    if (PtpPortRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "PtpRedInit: Registration with RM FAILED\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : PtpRedDeInit
 *
 * DESCRIPTION      : Deinitializes redundancy global variables.This function
 *                    will get invoked during BOOTUP failure. Also De-registers
 *                    with RM.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 *****************************************************************************/
PUBLIC INT4
PtpRedDeInit (VOID)
{
    /* De Register the PTP protocol with RM */
    if (PtpPortRmDeRegisterProtocols () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "PtpRedDeInit: PtpPortRmDeRegisterProtocols FAILED\r\n"));
        return OSIX_FAILURE;
    }
    MEMSET (&(gPtpRedGlobalInfo), 0, sizeof (tPtpRedGlobalInfo));
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PtpRedRmCallBack                                     */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the PTP*/
/*                      queue.                                               */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : If msg is enqueued and event sent then OSIX_SUCCESS  */
/*                      Otherwise OSIX_FAILURE                               */
/*****************************************************************************/
PUBLIC INT4
PtpRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tPtpQMsg           *pMsg = NULL;

    /* Check if Data received is valid or not */
    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "PtpRedRmCallBack: Rcvd invalid message with pointer "
                  "to buffer as NULL.\r\n"));

        /* Message absent and hence no need to process */
        return OSIX_FAILURE;
    }

    /*Allocate memory for QueueMsg */
    if ((pMsg = (tPtpQMsg *) MemAllocMemBlk
         (gPtpGlobalInfo.QMsgPoolId)) == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "PtpRedRmCallBack: PTP Queue Message memory Allocation "
                  "Failed !!!\r\n"));

        return OSIX_FAILURE;
    }

    /*Initialize the data structure */
    MEMSET (pMsg, 0, sizeof (tPtpQMsg));
    pMsg->u4MsgType = PTP_RM_MSG_TYPE;
    pMsg->PtpRmMsg.pData = pData;
    pMsg->PtpRmMsg.u2DataLen = u2DataLen;
    pMsg->PtpRmMsg.u1Event = u1Event;

    /* Send the RM message to the PTP queue */
    if (OsixQueSend (gPtpGlobalInfo.PtpQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        /* Release the allocated memory for the queue if the posting
         * message to the queue fails */
        MemReleaseMemBlock (gPtpGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }

    /* Send a EVENT to PTP task */
    if (OsixEvtSend (gPtpGlobalInfo.TaskId, PTP_RM_MSG_EVENT) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                  PTP_MAX_DOMAINS, ALL_FAILURE_TRC,
                  "OsixEvtSend function Failed !!!\r\n"));
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpRedHandleRmEvents
 *
 * DESCRIPTION      : This function process all the events and messages from
 *                    RM module.
 *
 * INPUT            : None
 *
 * OUTPUT           : None.
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC VOID
PtpRedHandleRmEvents (VOID)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    tPtpQMsg           *pPtpQMsg = NULL;
    UINT4               u4SeqNum = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));

    while (OsixQueRecv (gPtpGlobalInfo.PtpQId, (UINT1 *) &pPtpQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pPtpQMsg == NULL)
        {

            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpRedHandleRmEvents: zero messages received from RM module\r\n"));
            return;
        }
        switch (pPtpQMsg->PtpRmMsg.u1Event)
        {
            case GO_STANDBY:
                PtpRedHandleGoStandby ();
                break;

            case GO_ACTIVE:
                PtpRedHandleGoActive ();
                break;

            case RM_CONFIG_RESTORE_COMPLETE:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                          "PtpRedHandleRmEvents: "
                          "RM_CONFIG_RESTORE_COMPLETE event received "
                          "from RM\r\n"));
                if (PTP_NODE_STATUS () == RM_INIT)
                {
                    if (PtpPortRmGetNodeState () == RM_STANDBY)
                    {
                        PtpRedHandleIdleToStandby ();
                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                        PtpPortRmApiHandleProtocolEvent (&ProtoEvt);
                    }
                }
                break;

            case L2_INITIATE_BULK_UPDATES:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                          "PtpRedHandleRmEvents: "
                          "L2_INITIATE_BULK_UPDATES event received"
                          " from RM \r\n"));
                ProtoEvt.u4AppId = RM_PTP_APP_ID;
                ProtoEvt.u4Error = RM_NONE;
                ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
                PtpPortRmApiHandleProtocolEvent (&ProtoEvt);

                break;

            case RM_STANDBY_UP:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                          "PtpRedHandleRmEvents: RM_STANDBY_UP event "
                          "reached Updating Standby Nodes Count.\r\n"));
                pData = (tRmNodeInfo *) pPtpQMsg->PtpRmMsg.pData;

                if (pPtpQMsg->PtpRmMsg.u2DataLen == RM_NODE_COUNT_MSG_SIZE)
                {
                    gPtpRedGlobalInfo.u1NumOfStandbyNodesUp =
                        pData->u1NumStandby;
                }
                else
                {
                    /* Data length is incorrect */

                    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "PtpRedHandleRmEvents: RM_STANDBY_UP event "
                              "reached Data Lenght Incorrect.Failed "
                              "processing the event \r\n"));

                }
                RmReleaseMemoryForMsg ((UINT1 *) pData);
                break;

            case RM_STANDBY_DOWN:

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "PtpRedHandleRmEvents: RM_STANDBY_DOWN "
                          "event reached Updating Standby Nodes Count.\r\n"));

                pData = (tRmNodeInfo *) pPtpQMsg->PtpRmMsg.pData;

                if (pPtpQMsg->PtpRmMsg.u2DataLen == RM_NODE_COUNT_MSG_SIZE)
                {
                    gPtpRedGlobalInfo.u1NumOfStandbyNodesUp =
                        pData->u1NumStandby;
                }
                else
                {
                    /* Data length is incorrect */
                    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "PtpRedHandleRmEvents: RM_STANDBY_DOWN event "
                              "received. Data Lenght Incorrect.Failed "
                              "processing the event\r\n"));

                }
                RmReleaseMemoryForMsg ((UINT1 *) pData);
                break;

            case RM_MESSAGE:

                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pPtpQMsg->PtpRmMsg.pData, &u4SeqNum);

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "PtpRedHandleRmEvents Processing " "RM_Msg.\r\n"));

                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pPtpQMsg->PtpRmMsg.pData,
                                     pPtpQMsg->PtpRmMsg.u2DataLen);

                ProtoAck.u4AppId = RM_PTP_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (gPtpRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
                {

                    /* Process the RM Messages at the Active Node */
                    PtpRedProcessPeerMsgAtActive (pPtpQMsg->PtpRmMsg.pData,
                                                  pPtpQMsg->PtpRmMsg.u2DataLen);
                }
                else if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
                {

                    /* Process the RM Messages at the Standby Node */
                    PtpRedProcessPeerMsgAtStandby (pPtpQMsg->PtpRmMsg.pData,
                                                   pPtpQMsg->PtpRmMsg.
                                                   u2DataLen);
                }
                else
                {

                    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                              "PtpRedHandleRmEvents: Sync-up message "
                              "received at Idle Node!!!!\r\n"));

                }
                RM_FREE (pPtpQMsg->PtpRmMsg.pData);

                /* Sending ACK to RM */
                PtpPortRmApiSendProtoAckToRM (&ProtoAck);
                break;

            default:
                break;
        }
        MemReleaseMemBlock (gPtpGlobalInfo.QMsgPoolId, (UINT1 *) pPtpQMsg);

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC,
                  "PtpRedHandleRmEvents: retruning from function\r\n"));

    }
    return;
}

/***********************************************************************
 * FUNCTION NAME    : PtpRedHandleGoStandBy
 *
 * DESCRIPTION      : This routine handles the GO_STANDBY event and responds
 *                    to RM with an acknowledgment.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **********************************************************************/
PUBLIC VOID
PtpRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_PTP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              CONTROL_PLANE_TRC,
              "PtpRedHandleGoStandby: Entered PtpRedHandleGoStandby \r\n"));

    /*check for node status */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpRedHandleGoStandby:GO_STANDBY event"
                  "reached when node is already in standby state.\r\n"));
    }

    /* Idle to  Stand By received */
    else if (gPtpRedGlobalInfo.u1NodeStatus == RM_INIT)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpRedHandleGoStandby:GO_STANDBY event received when"
                  "node state is Idle.Ignoring this event, node will "
                  "become standby when CONFIG_RESTORE_COMPLETE is received \r\n"));
    }

    /*Active to StandBy transition */
    else if (gPtpRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpRedHandleGoStandby:Active to Standby transition...\r\n"));

        /* Update RM Event */
        PtpRedHandleActiveToStandby ();

        /* Update the node status */
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        PtpPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpRedHandleActiveToStandby
 *
 * DESCRIPTION      : On Active to Standby transition, the following actions
 *                    are performed,
 *                    1. Update the Node Status
 *                    2. ReInitialise the Number of standBy nodes to Zero.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : None
 *
 **************************************************************************/
PUBLIC VOID
PtpRedHandleActiveToStandby (VOID)
{

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              CONTROL_PLANE_TRC, "Entered PtpRedHandleActiveToStandby.\r\n"));
    /* Process the Active to Standby Event */
    gPtpRedGlobalInfo.u1NodeStatus = RM_STANDBY;
    gPtpRedGlobalInfo.u1NumOfStandbyNodesUp = 0;
    return;
}

/***********************************************************************
 * FUNCTION NAME    : PtpRedHandleGoActive
 *
 * DESCRIPTION      : This routine handles the GO_ACTIVE event and responds
 *                    to RM with an acknowledgment.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **********************************************************************/
PUBLIC VOID
PtpRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_PTP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /*check for node status */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpRedHandleGoActive:GO_ACTIVE event"
                  "reached when node is already in active state.\r\n"));
        return;
    }

    /* Idle to Active node status transition */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_INIT)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpRedHandleGoActive: Idle to Active status"
                  "transition.\r\n"));

        /* Update RM event */
        PtpRedHandleIdleToActive ();

        /* Update the node status */
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;

    }
    /* Stand By to Active node transition */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpRedHandleGoActive: StandBy to Active"
                  "status transition.\r\n"));

        /* Update RM Event */
        PtpRedHandleStandByToActive ();

        /* Update the node status */
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    /* Bulk request not need to send when it become only the states 
     * active because its already sync up master */
    PtpPortRmApiHandleProtocolEvent (&ProtoEvt);
    return;
}

/*****************************************************************************
 * FUNCTION NAME    : PtpRedHandleIdleToActive
 *
 * DESCRIPTION      : This routine updates the node status on transition from
 *                    Idle to Active.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 ****************************************************************************/
PUBLIC VOID
PtpRedHandleIdleToActive (VOID)
{
    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              CONTROL_PLANE_TRC, "Entered PtpRedHandleIdleToActive.\r\n"));
    /* Process Idle to Active Event */
    gPtpRedGlobalInfo.u1NodeStatus = RM_ACTIVE;
    gPtpRedGlobalInfo.u1NumOfStandbyNodesUp = PtpPortRmGetStandbyNodeCount ();
    return;
}

/*****************************************************************************
 * FUNCTION NAME    : PtpRedHandleStandByToActive
 *
 * DESCRIPTION      : This routine updates the node status on transition from
 *                    Stand By to Active.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 ****************************************************************************/
PUBLIC VOID
PtpRedHandleStandByToActive (VOID)
{
    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              CONTROL_PLANE_TRC, "Entered PtpRedHandleStandByToActive\r\n"));
    /* Process Standby to Active event */
    gPtpRedGlobalInfo.u1NodeStatus = RM_ACTIVE;
    gPtpRedGlobalInfo.u1NumOfStandbyNodesUp = PtpPortRmGetStandbyNodeCount ();
    return;
}

/******************************************************************************
 * FUNCTION NAME    : PtpRedHandleIdleToStandby
 *
 * DESCRIPTION      : This routine updates the node status from idle to standby.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 *****************************************************************************/
PUBLIC VOID
PtpRedHandleIdleToStandby (VOID)
{

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              CONTROL_PLANE_TRC, "Entered PtpRedHandleIdleToStandby\r\n"));
    /* Process the Idle to Standby Event */
    PTP_NODE_STATUS () = RM_STANDBY;
    gPtpRedGlobalInfo.u1NumOfStandbyNodesUp = 0;
    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpRedProcessPeerMsgAtActive
 *
 * DESCRIPTION      : This routine handles messages from the other node
 *                    (Standby) at Active. The messages that are handled in
 *                    Active node are
 *                    1. PTP_RED_L2_STDBY_TO_ACT_RELAY_PKT
 *                    2. PTP_RED_IPv4_PTP_PKT
 *                    3. PTP_RED_IPv6_PTP_PKT 
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
PtpRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    PTP_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    PTP_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u2DataLen < PTP_RED_MIN_MSG_LEN)
    {
        /*Packet is Malformed packet. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | DATA_PATH_TRC,
                  "PtpRedProcessPeerMsgAtStandby:Malformed"
                  "packet is received.\r\n"));
        return;
    }

    switch (u1MsgType)
    {
            /*All case are falling in same condition */
        case PTP_RED_L2_STDBY_TO_ACT_RELAY_PKT:
        case PTP_RED_IPvX_STDBY_TO_ACT_RELAY_PKT:
            u4OffSet = 0;
            PtpRedProcessRelayPktAtActive (pMsg, &u4OffSet);
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC | DATA_PATH_TRC,
                      "PtpRedProcessPeerMsgAtStandby:Malformed"
                      "packet is received.\r\n"));
            break;
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpRedProcessPeerMsgAtStandby
 *
 * DESCRIPTION      : This routine handles messages from the other node
 *                    (Active) at standby. The messages that are handled in
 *                    Standby node are
 *                    1. PTP_RED_L2_ACT_TO_STDBY_RELAY_PKT
 *                    2. PTP_RED_IPvX_PTP_PKT
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
PtpRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    PTP_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    PTP_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u2DataLen < PTP_RED_MIN_MSG_LEN)
    {
        /*Packet is Malformed packet. */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC | DATA_PATH_TRC,
                  "PtpRedProcessPeerMsgAtStandby:Malformed"
                  "packet is received.\r\n"));
        return;
    }
    switch (u1MsgType)
    {
        case PTP_RED_L2_ACT_TO_STDBY_RELAY_PKT:
        case PTP_RED_IPvX_ACT_TO_STDBY_RELAY_PKT:
            u4OffSet = 0;
            PtpRedProcessRelayPktAtStandby (pMsg, &u4OffSet);
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC | DATA_PATH_TRC,
                      "PtpRedProcessPeerMsgAtStandby:Malformed"
                      "packet is received.\r\n"));

            break;
    }
    return;
}

/*****************************************************************************
 * FUNCTION NAME    : PtpRedProcessAndRelayPkt
 *
 * DESCRIPTION      : This routine send PTP event/general packet to RM module
 *                    from active and standby Node.
 *
 * INPUT            : pMsg -RM msg
 *                    u2DataLen - length of message.
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 ****************************************************************************/
PUBLIC VOID
PtpRedProcessAndRelayPkt (tRmMsg * pMsg, UINT2 u2DataLen)
{

    UINT4               u4OffSet = 0;
    UINT1               u1MsgType = 0;

    if (u2DataLen < PTP_RED_MIN_MSG_LEN)
    {
        return;
    }
    /*Get The MsgType */
    PTP_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);

    /*RM packet Need to send to Standby node. */
    if ((u1MsgType == PTP_RED_L2_ACT_TO_STDBY_RELAY_PKT) ||
        (u1MsgType == PTP_RED_IPvX_ACT_TO_STDBY_RELAY_PKT) ||
        (u1MsgType == PTP_RED_IPvX_STDBY_TO_ACT_RELAY_PKT) ||
        (u1MsgType == PTP_RED_L2_STDBY_TO_ACT_RELAY_PKT))
    {
        /*When packet recived here rest of the fileds are already filled. */
        PTP_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2DataLen);
    }
    else
    {
        RM_FREE (pMsg);
        return;
    }
    PtpPortRedSendMsgToRm (PTP_RED_RELAY_REQUEST_PKT_MSG, u2DataLen, pMsg);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpRedProcessRelayPktAtStandby 
 *
 * DESCRIPTION      : This routine process the relay packet in stand by node  
 *                    that was received by active node.
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    pu4OffSet - Offset Value
 *
 * OUTPUT           : None
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
PtpRedProcessRelayPktAtStandby (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tCRU_BUF_CHAIN_HEADER *pPtpPkt = NULL;
    tPtpQMsg           *pPtpQMsg = NULL;
    tPtpPktParams       PtpPktParams;
    tClkSysTimeInfo     PtpSysTimeInfo;
    tPtpPortAddress     PtpPortAddress;
    tPtpPort           *pPtpPort = NULL;
    tVlanId             VlanId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4PtpDeviceType = 0;
    UINT4               u4OffSet2 = 0;
    UINT2               u2Length = 0;
    UINT2               u2PtpPktLength = 0;
    UINT1               u1MsgType = 0;
    UINT1              *pu1RxPdu = NULL;
    UINT1               u1DomainId = 0;
    UINT1               u1Flag = 0;

    MEMSET (&PtpPktParams, 0, sizeof (tPtpPktParams));
    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    MEMSET (&PtpPortAddress, 0, sizeof (tPtpPortAddress));

    PTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1MsgType);
    PTP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2Length);
    PTP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2PtpPktLength);
    /*allocating memory for ptp pkt. */
    pu1RxPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                              u2PtpPktLength);

    if (pu1RxPdu == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "PtpRedProcessRelayPktAtStandby: "
                  "Buddy Memory Allocation for PDU Failed!!!!\r\n"));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return;
    }

    MEMSET (pu1RxPdu, 0, u2PtpPktLength);
    /* In standby packet is received from physical/vlan interface 
       then need to send process pdu */
    if (u1MsgType == PTP_RED_L2_ACT_TO_STDBY_RELAY_PKT)
    {
        /*Allocating memory for ptp packet. */
        pPtpPkt = CRU_BUF_Allocate_MsgBufChain (u2PtpPktLength, 0);
        if (pPtpPkt == NULL)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC |
                      BUFFER_TRC,
                      "PtpRedProcessRelayPktAtStandby:CRU BUFFER Allocation for PDU Failed!!!!\r\n"));
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
            return;
        }

        /*Copying the ptp pkt from Rm buffer */
        PTP_RM_GET_N_BYTE (pMsg, pu1RxPdu, pu4OffSet, u2PtpPktLength);
        PTP_RM_PUT_N_BYTE (pPtpPkt, pu1RxPdu, &u4OffSet2, u2PtpPktLength);
        PTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);
        PTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4ContextId);
        PTP_RM_GET_2_BYTE (pMsg, pu4OffSet, VlanId);

        if ((pPtpQMsg = (tPtpQMsg *) MemAllocMemBlk (gPtpGlobalInfo.QMsgPoolId))
            == NULL)
        {
            PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                      "PtpApiIncomingPktHdlr: Allocation "
                      "of memory for Queue Message FAILED !!!\r\n"));
            CRU_BUF_Release_MsgBufChain (pPtpPkt, FALSE);
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
            return;
        }
        /*This check is used for pseudowire. */
        pPtpPort = PtpGetDeviceTypeFromIfIndex (pPtpPkt, u4ContextId, u4IfIndex,
                                                VlanId, &u1Flag);

        if ((pPtpPort != NULL) && (u1Flag == PTP_IFACE_IEEE_802_3))
        {
            VlanId = 0;
        }

        MEMSET (pPtpQMsg, 0, sizeof (tPtpQMsg));

        pPtpQMsg->u4ContextId = u4ContextId;
        pPtpQMsg->u4MsgType = PTP_PDU_ENQ_MSG;
        pPtpQMsg->u4Port = u4IfIndex;
        pPtpQMsg->u4PktLen = (UINT4) u2PtpPktLength;
        pPtpQMsg->VlanId = VlanId;
        pPtpQMsg->PtpDeviceType = PTP_IFACE_IEEE_802_3;
        if (pPtpQMsg->VlanId != VLAN_NULL_VLAN_ID)
        {
            pPtpQMsg->PtpDeviceType = PTP_IFACE_VLAN;
        }
        pPtpQMsg->uPtpMsgParam.pPtpPdu = pPtpPkt;

        /* We have received a PTP message. Generate a time stamp for this message
         * The time stamp will be used within PTP, if the message is an event
         * message. In case of general messages, the time stamp can be ignored.
         * this is need when packet is received from active to standby node.*/
        if (PtpClkGetTimeStamp (pPtpPort, (pPtpQMsg->uPtpMsgParam.pPtpPdu),
                                &(pPtpQMsg->PtpSysTimeInfo), u4IfIndex,
                                PTP_MODE_RX) == OSIX_FAILURE)
        {
            PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC, "PtpApiIncomingPktHdlr: Unable to "
                      "time stamp the PTP packet!!!\r\n"));
            CRU_BUF_Release_MsgBufChain (pPtpPkt, FALSE);
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
            MemReleaseMemBlock (gPtpGlobalInfo.QMsgPoolId, (UINT1 *) pPtpQMsg);
            return;
        }
        /*Send ptp pdu for processing in standby node. */
        PtpQueHandleRxPdu (pPtpQMsg);
        /*After processing packet memory should be released. */
        PtpMainReleaseQueMemory (pPtpQMsg);
    }
    else
    {
        /*pkt fall in this part is L3 interface ptp packet. */
        /*Filling information into structure */
        PtpPktParams.u4PktLen = u2PtpPktLength;
        PTP_RM_GET_N_BYTE (pMsg, pu1RxPdu, pu4OffSet, PtpPktParams.u4PktLen);
        PtpPktParams.pu1RcvdPdu = pu1RxPdu;

        PTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4ContextId);
        PTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1DomainId);
        PTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4PtpDeviceType);
        PTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

        /*getting ptp port information. */
        pPtpPort = PtpIfGetPortFromIfTypeAndIndex
            (u4ContextId, u1DomainId, u4IfIndex, u4PtpDeviceType);

        if (pPtpPort == NULL)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                      PTP_MAX_DOMAINS, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Port Entry is not available for Index : %u and"
                      " Domain : %u\r\n", u4IfIndex, u1DomainId));
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
            return;
        }

        PtpPktParams.pPtpPort = pPtpPort;
        /* Do a software time stamping now after packet is received in standby node. */
        PtpClkGetTimeStamp (pPtpPort, pu1RxPdu, &PtpSysTimeInfo, u4IfIndex,
                            PTP_MODE_RX);
        PtpPktParams.pPtpIngressTimeStamp = &PtpSysTimeInfo;

        PTP_RM_GET_N_BYTE (pMsg, &PtpPortAddress, pu4OffSet,
                           sizeof (tPtpPortAddress));
        PtpPktParams.pPtpPortAddress = &PtpPortAddress;
        /* Sending L3 interface ptp pdu for processing. */
        PtpQueProcessPtpPdu (&PtpPktParams);
    }
    /*releasing memory for Ptp Pkt. */
    MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpRedFormIPvXPtpPktForStandby
 *
 * DESCRIPTION      : This routine process PtpPktParams packet and change it into
 *                    RM packet.
 *
 * INPUT            : pPtpPktParams  - pointer to tPtpPktParams structure.
 *
 * OUTPUT           : pMsg - pointer to Rm msg
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
PtpRedFormIPvXPtpPktForStandby (tPtpPktParams * pPtpPktParams, tRmMsg ** pMsg)
{
    UINT4               u4Offset = 0;

    *pMsg = RM_ALLOC_TX_BUF (sizeof (tPtpPktParams));

    if (*pMsg == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  BUFFER_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC |
                  PTP_CRITICAL_TRC,
                  "PtpRedProcessPeerMsgAtActive : "
                  "Allocation of memory from RM  Failed\r\n"));

        return;
    }
    /*Filling information from ptp pkt params structure to RM msg 
       and this formed packe will be given ptp relay function. */
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset,
                       (UINT1) PTP_RED_IPvX_ACT_TO_STDBY_RELAY_PKT);
    /*This field will update in relay function. */
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, 0);
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, (UINT2) pPtpPktParams->u4PktLen);
    PTP_RM_PUT_N_BYTE (*pMsg, pPtpPktParams->pu1RcvdPdu, &u4Offset,
                       pPtpPktParams->u4PktLen);
    PTP_RM_PUT_4_BYTE (*pMsg, &u4Offset, pPtpPktParams->pPtpPort->u4ContextId);
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset, pPtpPktParams->pPtpPort->u1DomainId);
    PTP_RM_PUT_4_BYTE (*pMsg, &u4Offset,
                       pPtpPktParams->pPtpPort->PtpDeviceType);
    PTP_RM_PUT_4_BYTE (*pMsg, &u4Offset, pPtpPktParams->pPtpPort->u4IfIndex);
    PTP_RM_PUT_N_BYTE (*pMsg, pPtpPktParams->pPtpPortAddress, &u4Offset,
                       sizeof (tPtpPortAddress));

    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpRedFormIpvXPtpPktForActive
 *
 * DESCRIPTION      : This routine process PtpPktParams packet and change it into
 *                    RM packet.
 *
 * INPUT            : pu1PtpPdu - pointer to ptp pdu
 *                    pPtpPort - pointer to ptp port
 *                    u4PktLen - packet length
 *                    u1MsgType - Msg Type
 *
 * OUTPUT           : pMsg  - pointer to Rm msg
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
PtpRedFormIpvXPtpPktForActive (UINT1 *pu1PtpPdu, tPtpPort * pPtpPort,
                               UINT4 u4PktLen, UINT1 u1MsgType,
                               UINT1 u1IpVersion, tRmMsg ** pMsg)
{
    UINT4               u4Offset = 0;
    UINT4               u4MsgLen = 0;

    /* Calculating size of packet */
    u4MsgLen = (u4PktLen + PTP_RED_MSG_TYPE_SIZE +
                PTP_RED_MSG_LENGTH_SIZE + PTP_RED_MSG_TYPE_SIZE +
                PTP_RED_PTP_PKT_SIZE + PTP_RED_PTP_CONTEXT_SIZE +
                PTP_RED_PORT_DOMAIN_SIZE +
                PTP_RED_PTP_DEVICE_TYPE_SIZE +
                PTP_RED_PORT_IFINDEX_SIZE + PTP_RED_MSG_TYPE_SIZE);

    *pMsg = RM_ALLOC_TX_BUF (u4MsgLen);

    if (*pMsg == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  BUFFER_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC |
                  PTP_CRITICAL_TRC,
                  "PtpRedProcessPeerMsgAtActive : "
                  "Allocation of memory from RM  Failed\r\n"));

        return;
    }
    /*Filling information from ptp information into Rm msg 
     *this will process from relay function*/
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset, PTP_RED_IPvX_STDBY_TO_ACT_RELAY_PKT);
    /*this feild will update in relay function */
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, 0);
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset, u1IpVersion);
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, (UINT2) u4PktLen);
    PTP_RM_PUT_N_BYTE (*pMsg, pu1PtpPdu, &u4Offset, u4PktLen);
    PTP_RM_PUT_4_BYTE (*pMsg, &u4Offset, pPtpPort->u4ContextId);
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset, pPtpPort->u1DomainId);
    PTP_RM_PUT_4_BYTE (*pMsg, &u4Offset, pPtpPort->PtpDeviceType);
    PTP_RM_PUT_4_BYTE (*pMsg, &u4Offset, pPtpPort->u4IfIndex);
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset, u1MsgType);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : PtpRedProcessRelayPktAtActive
 *
 * DESCRIPTION      : This routine process the reply pkt from Standby at
 *                    Active node.
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *
 * OUTPUT           : pu4OffSet - Offset Value
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
PUBLIC VOID
PtpRedProcessRelayPktAtActive (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tPtpPort           *pPtpPort = NULL;
    tVlanId             VlanId;
    tCRU_BUF_CHAIN_HEADER *pPtpPkt = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4PtpDeviceType = 0;
    UINT4               u4OffSet2 = 0;
    UINT2               u2Length = 0;
    UINT2               u2PktSize = 0;
    UINT2               u2Protocol = 0;
    UINT1               u1DomainId = 0;
    UINT1               u1EncapType = 0;
    UINT1               u1MsgType = 0;
    UINT1              *pu1RxPdu = NULL;
    UINT1               u1SubMsgType = 0;

    MEMSET (&VlanId, 0, sizeof (tVlanId));
    PTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1MsgType);
    PTP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2Length);
    PTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1SubMsgType);
    PTP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2PktSize);

    pu1RxPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId, u2PktSize);

    if (pu1RxPdu == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | PTP_CRITICAL_TRC,
                  "PtpRedProcessRelayPktAtStandby: "
                  "Buddy Memory Allocation for PDU Failed!!!!\r\n"));
        return;
    }

    MEMSET (pu1RxPdu, 0, u2PktSize);

    /*If falls in this condition it means its L2 interface/vlan packet. */
    if ((u1MsgType == PTP_RED_L2_STDBY_TO_ACT_RELAY_PKT) &&
        (u1SubMsgType == PTP_RED_L2_RELAY_PKT))
    {
        pPtpPkt = CRU_BUF_Allocate_MsgBufChain ((UINT4) u2PktSize, 0);
        if (pPtpPkt == NULL)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      BUFFER_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC |
                      PTP_CRITICAL_TRC,
                      "PtpRedProcessRelayPktAtStandby: "
                      "CRU BUFFER Allocation for PDU Failed!!!!\r\n"));
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
            return;
        }
        /*Copying the ptp pkt from Rm buffer */
        PTP_RM_GET_N_BYTE (pMsg, pu1RxPdu, pu4OffSet, u2PktSize);
        PTP_RM_PUT_N_BYTE (pPtpPkt, pu1RxPdu, &u4OffSet2, u2PktSize);
        PTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);
        PTP_RM_GET_2_BYTE (pMsg, pu4OffSet, u2Protocol);
        PTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1EncapType);

        if (PtpPortHandleOutgoingPktOnPort (pPtpPkt,
                                            u4IfIndex,
                                            u2PktSize,
                                            u2Protocol,
                                            u1EncapType) != OSIX_SUCCESS)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | DUMP_TRC,
                      "PtpRedProcessRelayPktAtStandby: "
                      "Failed To Send Packet On Port %d!!!!\r\n", u4IfIndex));
            if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, &u4ContextId) ==
                OSIX_SUCCESS)
            {
                PTP_DUMP_TRC (u4ContextId, pPtpPkt, u2PktSize);
            }
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
            return;
        }
    }
    /*if falls through this condition than its L2 vlan packet. */
    else if ((u1MsgType == PTP_RED_L2_STDBY_TO_ACT_RELAY_PKT) &&
             (u1SubMsgType == PTP_RED_VLAN_PTP_PKT))
    {
        pPtpPkt = CRU_BUF_Allocate_MsgBufChain ((UINT4) u2PktSize, 0);
        if (pPtpPkt == NULL)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      BUFFER_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC |
                      PTP_CRITICAL_TRC,
                      "PtpRedProcessRelayPktAtStandby: "
                      "CRU BUFFER Allocation for PDU Failed!!!!\r\n"));
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
            return;
        }
        /*Copying the ptp pkt from Rm buffer */
        PTP_RM_GET_N_BYTE (pMsg, pu1RxPdu, pu4OffSet, u2PktSize);
        PTP_RM_PUT_N_BYTE (pPtpPkt, pu1RxPdu, &u4OffSet2, u2PktSize);
        PTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4ContextId);
        PTP_RM_GET_2_BYTE (pMsg, pu4OffSet, VlanId);
        if (PtpPortTransmitPktOverVlan (pPtpPkt, u4ContextId,
                                        VlanId, u2PktSize) != OSIX_SUCCESS)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | DUMP_TRC,
                      "PtpRedProcessRelayPktAtStandby: "
                      "Failed To Send Packet On Vlan %d!!!!\r\n", VlanId));
            PTP_DUMP_TRC (u4ContextId, pPtpPkt, u2PktSize);
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
            return;
        }
    }
    else if (u1MsgType == PTP_RED_IPvX_STDBY_TO_ACT_RELAY_PKT)
    {

        PTP_RM_GET_N_BYTE (pMsg, pu1RxPdu, pu4OffSet, (UINT4) u2PktSize);
        PTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4ContextId);
        PTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1DomainId);
        PTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4PtpDeviceType);
        PTP_RM_GET_4_BYTE (pMsg, pu4OffSet, u4IfIndex);

        pPtpPort = PtpIfGetPortFromIfTypeAndIndex
            (u4ContextId, u1DomainId, u4IfIndex, u4PtpDeviceType);

        if (pPtpPort == NULL)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                      PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                      "Port Entry is not available for Index : %u and"
                      " Domain : %u\r\n", u4IfIndex, u1DomainId));
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
            return;
        }

        PTP_RM_GET_1_BYTE (pMsg, pu4OffSet, u1MsgType);
        if (u1SubMsgType == PTP_RED_IPv4_PTP_PKT)
        {
            PtpUdpTransmitPtpMsg (pu1RxPdu, pPtpPort, u2Length, u1MsgType);
        }
        else
        {
            PtpUdpV6TransmitPtpMsg (pu1RxPdu, pPtpPort, u2Length, u1MsgType);
        }
    }
    MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1RxPdu);
}

/***************************************************************************
 * FUNCTION NAME    : PtpRedFormL2PtpPktForStandby
 *
 * DESCRIPTION      : This routine will process the ptp packet and convert 
 *                     into RM packet. 
 *
 * INPUT            : pBuf - pointer to chain buffer 
 *                    u4IfIndex - Interface Index
 *                    u4ContextId - Context Id
                      VlanId  - Vlan ID
                      pMsg - double pointer to RM msg 
 *
 * OUTPUT           : RM packet
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
VOID
PtpRedFormL2PtpPktForStandby (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                              UINT4 u4ContextId, tVlanId VlanId, UINT4 u4PktLen,
                              tRmMsg ** pMsg)
{
    UINT4               u4Offset = 0;
    UINT4               u4MsgLen = 0;
    UINT1              *pu1Frame = 0;

    u4MsgLen = (u4PktLen + PTP_RED_MSG_TYPE_SIZE + PTP_RED_PTP_PKT_SIZE +
                PTP_RED_PORT_IFINDEX_SIZE + PTP_RED_PTP_CONTEXT_SIZE +
                PTP_RED_PTP_VLAN_ID_SIZE);

    *pMsg = RM_ALLOC_TX_BUF ((UINT4) u4MsgLen);

    if (*pMsg == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | BUFFER_TRC |
                  OS_RESOURCE_TRC,
                  "PtpRedProcessPeerMsgAtActive : "
                  "Allocation of memory from RM  Failed\r\n"));

        return;
    }
    /*Filling information from ptp information into Rm msg
     *this will process from relay function*/
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset,
                       (UINT1) PTP_RED_L2_ACT_TO_STDBY_RELAY_PKT);
    /*this feild will update in relay function */
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, 0);
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, (UINT2) u4PktLen);

    /*Getting liner buffer pointer */
    pu1Frame = CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, u4PktLen);
    if (pu1Frame == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | BUFFER_TRC |
                  OS_RESOURCE_TRC,
                  "PtpRedProcessPeerMsgAtActive : "
                  "CRU BUFFER Allocation Failed\r\n"));
        RM_FREE (*pMsg);
        return;
    }

    /*Filling data ptp pkt ine RM msg */
    PTP_RM_PUT_N_BYTE (*pMsg, pu1Frame, &u4Offset, u4PktLen);
    PTP_RM_PUT_4_BYTE (*pMsg, &u4Offset, u4IfIndex);
    PTP_RM_PUT_4_BYTE (*pMsg, &u4Offset, u4ContextId);
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, VlanId);
    return;

}

/***************************************************************************
 * FUNCTION NAME    : PtpRedFormL2PtpPktForActive
 *
 * DESCRIPTION      : This routine will process the ptp packet and convert
 *                     into RM packet.
 *
 * INPUT            : pBuf - pointer to chain buffer
 *                    u4IfIndex - Interface Index
 *                    u4ContextId - Context Id
                      VlanId  - Vlan ID
                      pMsg - double pointer to RM msg
 *
 * OUTPUT           : RM packet
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
VOID
PtpRedFormL2PtpPktForActive (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                             UINT4 u4PktLen, UINT2 u2Protocol,
                             UINT1 u1EncapType, tRmMsg ** pMsg)
{
    UINT4               u4Offset = 0;
    UINT4               u4MsgLen = 0;
    UINT1              *pu1Frame = 0;

    u4MsgLen = (u4PktLen + PTP_RED_MSG_TYPE_SIZE + PTP_RED_MSG_LENGTH_SIZE +
                PTP_RED_MSG_TYPE_SIZE + PTP_RED_PTP_PKT_SIZE +
                PTP_RED_PORT_IFINDEX_SIZE + PTP_RED_PTP_PROTOCOL_SIZE +
                PTP_RED_PTP_ENCP_TYPE_SIZE);

    *pMsg = RM_ALLOC_TX_BUF (u4MsgLen);

    if (*pMsg == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | BUFFER_TRC |
                  OS_RESOURCE_TRC,
                  "PtpRedProcessPeerMsgAtActive : "
                  "Allocation of memory from RM  Failed\r\n"));

        return;
    }
    /*Filling information from ptp information into Rm msg
     *      *this will process from relay function*/
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset, PTP_RED_L2_STDBY_TO_ACT_RELAY_PKT);
    /*this feild will update in relay function */
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, 0);
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset, (UINT1) PTP_RED_L2_RELAY_PKT);
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, (UINT2) u4PktLen);

    /*Getting liner buffer pointer */
    pu1Frame = CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, u4PktLen);
    if (pu1Frame == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | BUFFER_TRC |
                  OS_RESOURCE_TRC,
                  "PtpRedFormL2PtpPktForActive: "
                  "CRU BUFFER Allocation Failed\r\n"));
        RM_FREE (*pMsg);
        return;
    }

    /*Filling data ptp pkt ine RM msg */
    PTP_RM_PUT_N_BYTE (*pMsg, pu1Frame, &u4Offset, u4PktLen);
    PTP_RM_PUT_4_BYTE (*pMsg, &u4Offset, u4IfIndex);
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, u2Protocol);
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset, u1EncapType);
    return;

}

/***************************************************************************
 * FUNCTION NAME    : PtpRedFormL2PtpPktForActive
 *
 * DESCRIPTION      : This routine will process the ptp packet and convert
 *                     into RM packet.
 *
 * INPUT            : pBuf - pointer to chain buffer
 *                    u4IfIndex - Interface Index
 *                    u4ContextId - Context Id
                      VlanId  - Vlan ID
                      pMsg - double pointer to RM msg
 *
 * OUTPUT           : RM packet
 *
 * RETURNS          : VOID
 *
 **************************************************************************/
VOID
PtpRedFormVlanPtpPktForActive (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
                               tVlanId VlanId, UINT4 u4PktLen, tRmMsg ** pMsg)
{
    UINT4               u4Offset = 0;
    UINT4               u4MsgLen = 0;
    UINT1              *pu1Frame = 0;

    u4MsgLen = (u4PktLen + PTP_RED_MSG_TYPE_SIZE + PTP_RED_MSG_LENGTH_SIZE +
                PTP_RED_MSG_TYPE_SIZE + PTP_RED_PTP_PKT_SIZE +
                PTP_RED_PTP_CONTEXT_SIZE + PTP_RED_PTP_VLAN_ID_SIZE);

    *pMsg = RM_ALLOC_TX_BUF (u4MsgLen);

    if (*pMsg == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | BUFFER_TRC |
                  OS_RESOURCE_TRC,
                  "PtpRedProcessPeerMsgAtActive : "
                  "Allocation of memory from RM  Failed\r\n"));

        return;
    }
    /*Filling information from ptp information into Rm msg
     *this will process from relay function*/
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset,
                       (UINT1) PTP_RED_L2_STDBY_TO_ACT_RELAY_PKT);
    /*this feild will update in relay function */
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, 0);
    PTP_RM_PUT_1_BYTE (*pMsg, &u4Offset, PTP_RED_VLAN_PTP_PKT);
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, (UINT2) u4PktLen);

    /*Getting liner buffer pointer */
    pu1Frame = CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, u4PktLen);
    if (pu1Frame == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | BUFFER_TRC |
                  OS_RESOURCE_TRC,
                  "PtpRedFormVlanPtpPktForActive: "
                  "CRU BUFFER Allocation Failed\r\n"));
        RM_FREE (*pMsg);
        return;
    }

    /*Filling data ptp pkt ine RM msg */
    PTP_RM_PUT_N_BYTE (*pMsg, pu1Frame, &u4Offset, u4PktLen);
    PTP_RM_PUT_4_BYTE (*pMsg, &u4Offset, u4ContextId);
    PTP_RM_PUT_2_BYTE (*pMsg, &u4Offset, VlanId);
    return;
}
#endif /* _PTPRED_C_ */
