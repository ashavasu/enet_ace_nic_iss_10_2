/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: fsptplw.c,v 1.16 2014/05/29 13:19:56 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "ptpincs.h"

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsPtpGlobalSysCtrl
 Input       :  The Indices

                The Object 
                retValFsPtpGlobalSysCtrl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpGlobalSysCtrl (INT4 *pi4RetValFsPtpGlobalSysCtrl)
{
    *pi4RetValFsPtpGlobalSysCtrl = (INT4) gPtpGlobalInfo.u1SysCtrlStatus;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPtpGblTraceOption
 Input       :  The Indices

                The Object
                retValFsPtpGblTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpGblTraceOption (tSNMP_OCTET_STRING_TYPE * pRetValFsPtpGblTraceOption)
{
    tPtpUtlValidTraces  PtpUtlValidTraces;
    UINT2               u2TraceLen = 0;

    PtpUtlValidTraces.pu1TraceStrings = (UINT1 *) &gau1PtpTraceTypes;
    PtpUtlValidTraces.u2MaxTrcTypes = (UINT2) PTP_MAX_TRC_TYPE;

    PtpUtilGetTraceOptionString (&PtpUtlValidTraces,
                                 gPtpGlobalInfo.u2GblTrace,
                                 pRetValFsPtpGblTraceOption->pu1_OctetList,
                                 &u2TraceLen);

    pRetValFsPtpGblTraceOption->i4_Length = (INT4) u2TraceLen;

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPrimaryContext
 Input       :  The Indices

                The Object 
                retValFsPtpPrimaryContext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPrimaryContext (INT4 *pi4RetValFsPtpPrimaryContext)
{

    *pi4RetValFsPtpPrimaryContext = gPtpGlobalInfo.u4PrimaryContextId;
    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsPtpGlobalSysCtrl
 Input       :  The Indices

                The Object 
                setValFsPtpGlobalSysCtrl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpGlobalSysCtrl (INT4 i4SetValFsPtpGlobalSysCtrl)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus != i4SetValFsPtpGlobalSysCtrl)
    {
        if (i4SetValFsPtpGlobalSysCtrl == PTP_SHUTDOWN)
        {
            PtpCxtDeleteAllCxt ();
            PtpMainModuleShutDown ();
        }
        else
        {
            if (PtpMainModuleStart () == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Failed to start the PTP module\r\n"));
                i1RetVal = (INT1) SNMP_FAILURE;
            }
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpGblTraceOption
 Input       :  The Indices

                The Object
                setValFsPtpGblTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpGblTraceOption (tSNMP_OCTET_STRING_TYPE * pSetValFsPtpGblTraceOption)
{
    tPtpUtlValidTraces  PtpUtlValidTraces;
    tPtpCxt            *pPtpCxt = NULL;
    UINT4               u4TrcOption = 0;
    UINT1               u1TrcStatus = 0;

    MEMSET (&PtpUtlValidTraces, 0, sizeof (tPtpUtlValidTraces));

    PtpUtlValidTraces.pu1TraceStrings = (UINT1 *) &gau1PtpTraceTypes;
    PtpUtlValidTraces.u2MaxTrcTypes = (UINT2) PTP_MAX_TRC_TYPE;

    PtpUtilGetTraceOptionValue (&PtpUtlValidTraces,
                                pSetValFsPtpGblTraceOption->pu1_OctetList,
                                (UINT2) pSetValFsPtpGblTraceOption->i4_Length,
                                &u4TrcOption, &u1TrcStatus);

    if (u1TrcStatus == PTP_UTL_TRACE_ENABLE)
    {
        gPtpGlobalInfo.u2GblTrace |= u4TrcOption;
    }
    else
    {
        gPtpGlobalInfo.u2GblTrace &= ~u4TrcOption;
    }

    pPtpCxt = (tPtpCxt *)
        PtpDbGetFirstNode (gPtpGlobalInfo.ContextTree, PTP_CONTEXT_DATA_SET);

    while (pPtpCxt != NULL)
    {
        if (u1TrcStatus == PTP_UTL_TRACE_ENABLE)
        {
            pPtpCxt->u2Trace |= u4TrcOption;
        }
        else
        {
            pPtpCxt->u2Trace &= ~u4TrcOption;
        }

        pPtpCxt = (tPtpCxt *)
            PtpDbGetNextNode (gPtpGlobalInfo.ContextTree,
                              pPtpCxt, PTP_CONTEXT_DATA_SET);
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPrimaryContext
 Input       :  The Indices

                The Object 
                setValFsPtpPrimaryContext
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpPrimaryContext (INT4 i4SetValFsPtpPrimaryContext)
{
    gPtpGlobalInfo.u4PrimaryContextId = i4SetValFsPtpPrimaryContext;
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsPtpGlobalSysCtrl
 Input       :  The Indices

                The Object 
                testValFsPtpGlobalSysCtrl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpGlobalSysCtrl (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsPtpGlobalSysCtrl)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if ((i4TestValFsPtpGlobalSysCtrl != PTP_START) &&
        (i4TestValFsPtpGlobalSysCtrl != PTP_SHUTDOWN))
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for systemt control\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpGblTraceOption
 Input       :  The Indices

                The Object
                testValFsPtpGblTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpGblTraceOption (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE
                              * pTestValFsPtpGblTraceOption)
{
    tPtpUtlValidTraces  PtpUtlValidTraces;
    UINT4               u4TrcOption = 0;
    UINT1               u1TrcStatus = 0;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if ((pTestValFsPtpGblTraceOption->i4_Length < PTP_TRC_MIN_SIZE) ||
        (pTestValFsPtpGblTraceOption->i4_Length > PTP_TRC_MAX_SIZE))
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for error string\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        PtpUtlValidTraces.pu1TraceStrings = (UINT1 *) &gau1PtpTraceTypes;
        PtpUtlValidTraces.u2MaxTrcTypes = (UINT2) PTP_MAX_TRC_TYPE;

        PtpUtilGetTraceOptionValue
            (&PtpUtlValidTraces,
             pTestValFsPtpGblTraceOption->pu1_OctetList,
             (UINT2) pTestValFsPtpGblTraceOption->i4_Length,
             &u4TrcOption, &u1TrcStatus);

        if (!u4TrcOption)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                      MGMT_TRC | ALL_FAILURE_TRC, "Invalid error string\r\n"));

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPrimaryContext
 Input       :  The Indices

                The Object 
                testValFsPtpPrimaryContext
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpPrimaryContext (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsPtpPrimaryContext)
{

    if ((i4TestValFsPtpPrimaryContext < 0) ||
        (i4TestValFsPtpPrimaryContext > PTP_MAX_CONTEXT_ID))
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "Invalid context id\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpGlobalSysCtrl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPtpGlobalSysCtrl (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPtpGblTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsPtpGblTraceOption
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPtpPrimaryContext
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPtpPrimaryContext (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsPtpTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpTable
 Input       :  The Indices
                FsPtpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPtpTable (INT4 i4FsPtpContextId)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    if ((i4FsPtpContextId < 0) || (i4FsPtpContextId > PTP_MAX_CONTEXT_ID))
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "Invalid context id\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (PtpPortIsVcExists ((UINT4) i4FsPtpContextId) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Virtual context does not exist\r\n"));
            i1RetVal = (INT1) SNMP_FAILURE;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpTable
 Input       :  The Indices
                FsPtpContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPtpTable (INT4 *pi4FsPtpContextId)
{
    tPtpCxt            *pPtpCxt = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    pPtpCxt = (tPtpCxt *) PtpDbGetFirstNode (gPtpGlobalInfo.ContextTree,
                                             PTP_CONTEXT_DATA_SET);

    if (pPtpCxt == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pi4FsPtpContextId = (INT4) pPtpCxt->u4ContextId;
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPtpTable (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId)
{
    tPtpCxt             PtpCxt;
    tPtpCxt            *pPtpCxt = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    PtpCxt.u4ContextId = (UINT4) i4FsPtpContextId;
    pPtpCxt = (tPtpCxt *) PtpDbGetNextNode (gPtpGlobalInfo.ContextTree,
                                            &PtpCxt, PTP_CONTEXT_DATA_SET);

    if (pPtpCxt == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    *pi4NextFsPtpContextId = (INT4) pPtpCxt->u4ContextId;
    return (INT1) SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpContextRowStatus
 Input       :  The Indices
                FsPtpContextId

                The Object
                retValFsPtpContextRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpContextRowStatus (INT4 i4FsPtpContextId,
                             INT4 *pi4RetValFsPtpContextRowStatus)
{
    tPtpCxt            *pPtpCxt = NULL;
    INT1                i1RetVal = (INT1) SNMP_FAILURE;

    pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);

    if (pPtpCxt != NULL)
    {
        *pi4RetValFsPtpContextRowStatus = (INT4) pPtpCxt->u1RowStatus;
        i1RetVal = (INT1) SNMP_SUCCESS;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpAdminStatus
 Input       :  The Indices
                FsPtpContextId

                The Object 
                retValFsPtpAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpAdminStatus (INT4 i4FsPtpContextId, INT4 *pi4RetValFsPtpAdminStatus)
{
    tPtpCxt            *pPtpCxt = NULL;
    INT1                i1RetVal = (INT1) SNMP_FAILURE;

    pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);

    if (pPtpCxt != NULL)
    {
        *pi4RetValFsPtpAdminStatus = (INT4) pPtpCxt->u1AdminStatus;
        i1RetVal = (INT1) SNMP_SUCCESS;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTraceOption
 Input       :  The Indices
                FsPtpContextId

                The Object 
                retValFsPtpTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpTraceOption (INT4 i4FsPtpContextId,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsPtpTraceOption)
{
    tPtpCxt            *pPtpCxt = NULL;
    tPtpUtlValidTraces  PtpUtlValidTraces;
    UINT2               u2TraceLen = 0;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);

    if (pPtpCxt == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {

        PtpUtlValidTraces.pu1TraceStrings = (UINT1 *) &gau1PtpTraceTypes;
        PtpUtlValidTraces.u2MaxTrcTypes = (UINT2) PTP_MAX_TRC_TYPE;

        PtpUtilGetTraceOptionString (&PtpUtlValidTraces,
                                     pPtpCxt->u2Trace,
                                     pRetValFsPtpTraceOption->pu1_OctetList,
                                     &u2TraceLen);

        pRetValFsPtpTraceOption->i4_Length = (INT4) u2TraceLen;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpContextType
 Input       :  The Indices
                FsPtpContextId

                The Object 
                retValFsPtpContextType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpContextType (INT4 i4FsPtpContextId, INT4 *pi4RetValFsPtpContextType)
{
    tPtpCxt            *pPtpCxt = NULL;
    INT1                i1RetVal = (INT1) SNMP_FAILURE;

    pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);

    if (pPtpCxt != NULL)
    {
        *pi4RetValFsPtpContextType = (INT4) pPtpCxt->u1CxtType;
        i1RetVal = (INT1) SNMP_SUCCESS;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPrimaryDomain
 Input       :  The Indices
                FsPtpContextId

                The Object 
                retValFsPtpPrimaryDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPrimaryDomain (INT4 i4FsPtpContextId,
                          INT4 *pi4RetValFsPtpPrimaryDomain)
{
    tPtpCxt            *pPtpCxt = NULL;
    INT1                i1RetVal = (INT1) SNMP_FAILURE;

    pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);
    if (pPtpCxt != NULL)
    {
        *pi4RetValFsPtpPrimaryDomain = pPtpCxt->u1PrimaryDomainId;
        i1RetVal = (INT1) SNMP_SUCCESS;
    }
    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsPtpContextRowStatus
 Input       :  The Indices
                FsPtpContextId

                The Object
                setValFsPtpContextRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpContextRowStatus (INT4 i4FsPtpContextId,
                             INT4 i4SetValFsPtpContextRowStatus)
{
    tPtpCxt            *pPtpCxt = NULL;
    INT1                i1RetVal = (INT1) SNMP_FAILURE;

    pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);

    switch (i4SetValFsPtpContextRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpCxt != NULL)
            {
                pPtpCxt->u1RowStatus = (UINT1) i4SetValFsPtpContextRowStatus;
                i1RetVal = (INT1) SNMP_SUCCESS;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if ((pPtpCxt == NULL) &&
                ((pPtpCxt =
                  PtpCxtCreateContext ((UINT4) i4FsPtpContextId)) != NULL))
            {
                i1RetVal = (INT1) SNMP_SUCCESS;

                if (i4SetValFsPtpContextRowStatus == CREATE_AND_WAIT)
                {
                    pPtpCxt->u1RowStatus = (UINT1) NOT_IN_SERVICE;
                }
                else
                {
                    pPtpCxt->u1RowStatus = (UINT1) ACTIVE;
                }
            }
            break;
        }
        case DESTROY:
        {
            i1RetVal = (INT1) SNMP_SUCCESS;
            if (pPtpCxt != NULL)
            {
                PtpCxtDeleteContext ((UINT4) i4FsPtpContextId);
            }
            break;
        }

        default:
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpAdminStatus
 Input       :  The Indices
                FsPtpContextId

                The Object 
                setValFsPtpAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpAdminStatus (INT4 i4FsPtpContextId, INT4 i4SetValFsPtpAdminStatus)
{
    tPtpCxt            *pPtpCxt = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);

    if (pPtpCxt == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context %d does not exists\r\n", i4FsPtpContextId));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if ((pPtpCxt->u1AdminStatus != PTP_ENABLED) &&
            (i4SetValFsPtpAdminStatus == PTP_ENABLED))
        {
            PtpCxtEnable (pPtpCxt->u4ContextId);
        }
        else if ((pPtpCxt->u1AdminStatus != PTP_DISABLED) &&
                 (i4SetValFsPtpAdminStatus == PTP_DISABLED))
        {
            PtpCxtDisable (pPtpCxt->u4ContextId);
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpTraceOption
 Input       :  The Indices
                FsPtpContextId

                The Object 
                setValFsPtpTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpTraceOption (INT4 i4FsPtpContextId,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsPtpTraceOption)
{
    tPtpCxt            *pPtpCxt = NULL;
    tPtpUtlValidTraces  PtpUtlValidTraces;
    UINT4               u4TrcOption = 0;
    UINT1               u1TrcStatus = 0;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);

    if (pPtpCxt == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context %d does not exists\r\n", i4FsPtpContextId));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        MEMSET (&PtpUtlValidTraces, 0, sizeof (tPtpUtlValidTraces));

        PtpUtlValidTraces.pu1TraceStrings = (UINT1 *) &gau1PtpTraceTypes;
        PtpUtlValidTraces.u2MaxTrcTypes = (UINT2) PTP_MAX_TRC_TYPE;

        PtpUtilGetTraceOptionValue (&PtpUtlValidTraces,
                                    pSetValFsPtpTraceOption->pu1_OctetList,
                                    (UINT2) pSetValFsPtpTraceOption->i4_Length,
                                    &u4TrcOption, &u1TrcStatus);

        if (u1TrcStatus == PTP_UTL_TRACE_ENABLE)
        {
            pPtpCxt->u2Trace |= u4TrcOption;
        }
        else
        {
            pPtpCxt->u2Trace &= ~u4TrcOption;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPrimaryDomain
 Input       :  The Indices
                FsPtpContextId

                The Object 
                setValFsPtpPrimaryDomain
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpPrimaryDomain (INT4 i4FsPtpContextId,
                          INT4 i4SetValFsPtpPrimaryDomain)
{

    tPtpCxt            *pPtpCxt = NULL;
    INT1                i1RetVal = (INT1) SNMP_FAILURE;

    pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);
    if (pPtpCxt != NULL)
    {
        pPtpCxt->u1PrimaryDomainId = (UINT1) i4SetValFsPtpPrimaryDomain;
        i1RetVal = (INT1) SNMP_SUCCESS;
    }
    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpContextRowStatus
 Input       :  The Indices
                FsPtpContextId

                The Object
                testValFsPtpContextRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpContextRowStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsPtpContextId,
                                INT4 i4TestValFsPtpContextRowStatus)
{
    tPtpCxt            *pPtpCxt = NULL;
    UINT4               u4Count = 0;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        return (INT1) SNMP_FAILURE;
    }

    if (i4TestValFsPtpContextRowStatus == DESTROY)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if (i4FsPtpContextId > PTP_MAX_CONTEXT_ID)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "Invalid context Id\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    RBTreeCount (gPtpGlobalInfo.ContextTree, &u4Count);

    if (u4Count == PTP_MAX_CONTEXTS)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Maximum context in PTP reached\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MAX_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);

    switch (i4TestValFsPtpContextRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpCxt == NULL)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                return (INT1) SNMP_FAILURE;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if ((pPtpCxt != NULL) ||
                (PtpPortIsVcExists ((UINT4) i4FsPtpContextId) == OSIX_FAILURE))
            {
                if (i4TestValFsPtpContextRowStatus == CREATE_AND_GO)
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                }
                else
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
                }

                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Context %d already exists\r\n", i4FsPtpContextId));

                return (INT1) SNMP_FAILURE;
            }
            break;
        }

        default:
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpAdminStatus
 Input       :  The Indices
                FsPtpContextId

                The Object 
                testValFsPtpAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpAdminStatus (UINT4 *pu4ErrorCode,
                           INT4 i4FsPtpContextId,
                           INT4 i4TestValFsPtpAdminStatus)
{
    tPtpCxt            *pPtpCxt = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus != (UINT1) PTP_START)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPtpAdminStatus != PTP_ENABLED) &&
        (i4TestValFsPtpAdminStatus != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid Context PTP Admin State\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    else
    {
        pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);

        if (pPtpCxt == NULL)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId,
                      (UINT1) PTP_MAX_DOMAINS, MGMT_TRC | ALL_FAILURE_TRC,
                      "Context does not exists\r\n"));
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_PTP_ERR_CONTEXT_NOT_PRESENT);
            i1RetVal = (INT1) SNMP_FAILURE;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpTraceOption
 Input       :  The Indices
                FsPtpContextId

                The Object 
                testValFsPtpTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpTraceOption (UINT4 *pu4ErrorCode,
                           INT4 i4FsPtpContextId,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsPtpTraceOption)
{
    tPtpCxt            *pPtpCxt = NULL;
    tPtpUtlValidTraces  PtpUtlValidTraces;
    UINT4               u4TrcOption = 0;
    UINT1               u1TrcStatus = 0;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus != (UINT1) PTP_START)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    if ((pTestValFsPtpTraceOption->i4_Length < PTP_TRC_MIN_SIZE) ||
        (pTestValFsPtpTraceOption->i4_Length > PTP_TRC_MAX_SIZE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for error string\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    else
    {
        pPtpCxt = PtpCxtGetNode ((UINT4) i4FsPtpContextId);

        if (pPtpCxt == NULL)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) PTP_MAX_DOMAINS,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Context does not exists\r\n"));
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_PTP_ERR_CONTEXT_NOT_PRESENT);
            i1RetVal = (INT1) SNMP_FAILURE;
        }
        else
        {
            PtpUtlValidTraces.pu1TraceStrings = (UINT1 *) &gau1PtpTraceTypes;
            PtpUtlValidTraces.u2MaxTrcTypes = (UINT2) PTP_MAX_TRC_TYPE;

            PtpUtilGetTraceOptionValue
                (&PtpUtlValidTraces,
                 pTestValFsPtpTraceOption->pu1_OctetList,
                 (UINT2) pTestValFsPtpTraceOption->i4_Length,
                 &u4TrcOption, &u1TrcStatus);

            if (!u4TrcOption)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Invalid error string\r\n"));

                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
            }
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPrimaryDomain
 Input       :  The Indices
                FsPtpContextId

                The Object 
                testValFsPtpPrimaryDomain
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpPrimaryDomain (UINT4 *pu4ErrorCode,
                             INT4 i4FsPtpContextId,
                             INT4 i4TestValFsPtpPrimaryDomain)
{
    UNUSED_PARAM (i4FsPtpContextId);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPtpPrimaryDomain < 0) ||
        (i4TestValFsPtpPrimaryDomain >= PTP_MAX_DOMAINS))
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for Primary Domain \r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }
    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpTable
 Input       :  The Indices
                FsPtpContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPtpTable (UINT4 *pu4ErrorCode,
                    tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpDomainDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpDomainDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpDomainDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpDomainDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpDomainDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;

    }

    pPtpDomain = (tPtpDomain *)
        PtpDbGetFirstNode (gPtpGlobalInfo.DomainTree,
                           (UINT1) PTP_DOMAIN_DATA_SET);

    if (pPtpDomain == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
    }

    return i1RetVal;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPtpDomainDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpDomainDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;

    }

    pPtpDomain = PtpDmnGetNextDomainEntry ((UINT4) i4FsPtpContextId,
                                           (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4NextFsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        *pi4NextFsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpDomainClockMode
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpDomainClockMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpDomainClockMode (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 *pi4RetValFsPtpDomainClockMode)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpDomainClockMode = (INT4) pPtpDomain->ClkMode;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpDomainClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpDomainClockIdentity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpDomainClockIdentity (INT4 i4FsPtpContextId,
                                INT4 i4FsPtpDomainNumber,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsPtpDomainClockIdentity)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpDomainClockIdentity->i4_Length = PTP_MAX_CLOCK_ID_LEN;
        MEMCPY (pRetValFsPtpDomainClockIdentity->pu1_OctetList,
                pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId,
                PTP_MAX_CLOCK_ID_LEN);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpDomainGMClusterQueryInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpDomainGMClusterQueryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpDomainGMClusterQueryInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpDomainGMClusterQueryInterval)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpDomainGMClusterQueryInterval
            = (INT4) pPtpDomain->u1GrandMasterlogQueryInterval;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpDomainRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpDomainRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpDomainRowStatus (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 *pi4RetValFsPtpDomainRowStatus)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpDomainRowStatus = (INT4) pPtpDomain->u1RowStatus;
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpDomainClockMode
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpDomainClockMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpDomainClockMode (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4SetValFsPtpDomainClockMode)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        PtpDmnDelDomain (pPtpDomain);
        pPtpDomain = PtpDmnCreateDomain ((UINT4) i4FsPtpContextId,
                                         (UINT1) i4FsPtpDomainNumber);

        if (pPtpDomain != NULL)
        {
            pPtpDomain->ClkMode = (UINT4) i4SetValFsPtpDomainClockMode;
            PtpDmnInitDomainEntry (pPtpDomain);
            PtpNpWrConfigDomainMode (pPtpDomain);
            pPtpDomain->u1RowStatus = (UINT1) NOT_IN_SERVICE;
        }
        else
        {
            i1RetVal = (INT1) SNMP_FAILURE;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpDomainGMClusterQueryInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpDomainGMClusterQueryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpDomainGMClusterQueryInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4SetValFsPtpDomainGMClusterQueryInterval)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->u1GrandMasterlogQueryInterval !=
            (UINT1) i4SetValFsPtpDomainGMClusterQueryInterval)
        {
            pPtpDomain->u1GrandMasterlogQueryInterval =
                (UINT1) i4SetValFsPtpDomainGMClusterQueryInterval;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpDomainRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpDomainRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpDomainRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4SetValFsPtpDomainRowStatus)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_FAILURE;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    switch (i4SetValFsPtpDomainRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpDomain != NULL)
            {
                pPtpDomain->u1RowStatus = (UINT1) i4SetValFsPtpDomainRowStatus;

                if (i4SetValFsPtpDomainRowStatus == ACTIVE)
                {
                    PtpUtilInitPortsInDmn ((UINT4) i4FsPtpContextId,
                                           (UINT1) i4FsPtpDomainNumber);
                }
                else
                {
                    PtpDmnDisablePorts ((UINT4) i4FsPtpContextId,
                                        (UINT1) i4FsPtpDomainNumber);
                }

                i1RetVal = (INT1) SNMP_SUCCESS;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if ((pPtpDomain == NULL) &&
                ((pPtpDomain =
                  PtpDmnCreateDomain ((UINT4) i4FsPtpContextId,
                                      (UINT1) i4FsPtpDomainNumber)) != NULL))
            {
                i1RetVal = (INT1) SNMP_SUCCESS;

                if (i4SetValFsPtpDomainRowStatus == CREATE_AND_WAIT)
                {
                    pPtpDomain->u1RowStatus = (UINT1) NOT_IN_SERVICE;
                }
                else
                {
                    pPtpDomain->u1RowStatus = (UINT1) ACTIVE;
                }
            }
            break;
        }
        case DESTROY:
        {
            i1RetVal = (INT1) SNMP_SUCCESS;
            if (pPtpDomain != NULL)
            {
                PtpDmnDelDomain (pPtpDomain);
            }
            break;
        }
        default:
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpDomainClockMode
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpDomainClockMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpDomainClockMode
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4TestValFsPtpDomainClockMode)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        return i1RetVal;

    }
    if ((i4TestValFsPtpDomainClockMode > PTP_MANAGEMENT_MODE) ||
        (i4TestValFsPtpDomainClockMode < PTP_BOUNDARY_CLOCK_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Invalid Domain Clock Mode\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpDomainGMClusterQueryInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpDomainGMClusterQueryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpDomainGMClusterQueryInterval
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4TestValFsPtpDomainGMClusterQueryInterval)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4TestValFsPtpDomainGMClusterQueryInterval);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        return i1RetVal;

    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpDomainRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpDomainRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpDomainRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4TestValFsPtpDomainRowStatus)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        return i1RetVal;

    }

    if (i4TestValFsPtpDomainRowStatus == DESTROY)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if ((i4FsPtpContextId > PTP_MAX_CONTEXT_ID) ||
        (i4FsPtpDomainNumber >= PTP_MAX_DOMAINS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    switch (i4TestValFsPtpDomainRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpDomain == NULL)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Context Id : %d Domain Id %d does not exist\r\n",
                          i4FsPtpContextId, i4FsPtpDomainNumber));
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if ((i1RetVal == (INT1) SNMP_FAILURE) || (pPtpDomain != NULL))
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Context Id : %d Domain Id %d already exist\r\n",
                          i4FsPtpContextId, i4FsPtpDomainNumber));

                if (i4TestValFsPtpDomainRowStatus == CREATE_AND_GO)
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                }
                else
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
                }
                CLI_SET_ERR (CLI_PTP_ERR_DOMAIN_PRESENT);
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }

        default:
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpDomainDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsPtpDomainDataSetTable
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpClockDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpClockDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPtpClockDataSetTable (INT4 i4FsPtpContextId,
                                                INT4 i4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if ((pPtpDomain == NULL) ||
        ((pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE) &&
         (pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE)))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist or the clock mode is "
                  "neither ordinary nor boundary\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpClockDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPtpClockDataSetTable (INT4 *pi4FsPtpContextId,
                                        INT4 *pi4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    UINT4               u4PtpContextId = 0;
    UINT1               u1PtpDomainNumber = 0;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    pPtpDomain = (tPtpDomain *)
        PtpDbGetFirstNode (gPtpGlobalInfo.DomainTree,
                           (UINT1) PTP_DOMAIN_DATA_SET);

    if (pPtpDomain == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
        (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
    {
        *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
        return (INT1) SNMP_SUCCESS;
    }

    u4PtpContextId = pPtpDomain->pPtpCxt->u4ContextId;
    u1PtpDomainNumber = pPtpDomain->u1DomainId;

    while ((pPtpDomain = PtpDmnGetNextDomainEntry (u4PtpContextId,
                                                   u1PtpDomainNumber)) != NULL)
    {
        if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
            (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
        {
            *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
            *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
            return (INT1) SNMP_SUCCESS;
        }

        u4PtpContextId = pPtpDomain->pPtpCxt->u4ContextId;
        u1PtpDomainNumber = pPtpDomain->u1DomainId;
    }

    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpClockDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpClockDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    while ((pPtpDomain = PtpDmnGetNextDomainEntry ((UINT4) i4FsPtpContextId,
                                                   (UINT1) i4FsPtpDomainNumber))
           != NULL)
    {
        if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
            (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
        {
            *pi4NextFsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
            *pi4NextFsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
            return (INT1) SNMP_SUCCESS;
        }
        i4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        i4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
    }

    return (INT1) SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockIdentity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpClockIdentity (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsPtpClockIdentity)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpClockIdentity->i4_Length = PTP_MAX_CLOCK_ID_LEN;
        MEMCPY (pRetValFsPtpClockIdentity->pu1_OctetList,
                pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId,
                PTP_MAX_CLOCK_ID_LEN);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockTwoStepFlag
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockTwoStepFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockTwoStepFlag
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockTwoStepFlag)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockTwoStepFlag =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag;

    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockNumberPorts
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockNumberPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockNumberPorts
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockNumberPorts)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockNumberPorts =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u4NumberOfPorts;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockClass
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockClass
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockClass)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockClass =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Class;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockAccuracy
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockAccuracy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockAccuracy
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockAccuracy)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockAccuracy =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.u1Accuracy;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockOffsetScaledLogVariance
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockOffsetScaledLogVariance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockOffsetScaledLogVariance
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockOffsetScaledLogVariance)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockOffsetScaledLogVariance =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkQuality.
            u2OffsetScaledLogVariance;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockPriority1
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockPriority1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockPriority1
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockPriority1)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockPriority1 =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority1;

    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockPriority2
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockPriority2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockPriority2
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockPriority2)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockPriority2 =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority2;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockSlaveOnly
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockSlaveOnly
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockSlaveOnly
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockSlaveOnly)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockSlaveOnly =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockPathTraceOption
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockPathTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockPathTraceOption
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockPathTraceOption)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockPathTraceOption =
            (INT4) pPtpDomain->bPathTraceOption;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockAccMasterMaxSize
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockAccMasterMaxSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockAccMasterMaxSize
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockAccMasterMaxSize)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockAccMasterMaxSize =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u2AccMasterMaxSize;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockSecurityEnabled
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockSecurityEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpClockSecurityEnabled
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpClockSecurityEnabled)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpClockSecurityEnabled =
            (INT4) pPtpDomain->bSecurityEnabled;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpClockNumOfSA
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpClockNumOfSA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpClockNumOfSA (INT4 i4FsPtpContextId,
                         INT4 i4FsPtpDomainNumber,
                         UINT4 *pu4RetValFsPtpClockNumOfSA)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpClockNumOfSA = pPtpDomain->u4NumberOfSA;
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpClockTwoStepFlag
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpClockTwoStepFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpClockTwoStepFlag
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4SetValFsPtpClockTwoStepFlag)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag !=
            (BOOL1) i4SetValFsPtpClockTwoStepFlag)
        {
            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag =
                (BOOL1) i4SetValFsPtpClockTwoStepFlag;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpClockNumberPorts
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpClockNumberPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpClockNumberPorts
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4SetValFsPtpClockNumberPorts)
{
    tPtpDomain         *pPtpDomain = NULL;
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4PortId = 0;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    if (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u4NumberOfPorts !=
        (UINT4) i4SetValFsPtpClockNumberPorts)
    {
        pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u4NumberOfPorts =
            (UINT4) i4SetValFsPtpClockNumberPorts;

        /* Delete all the ports in this domain */
        while (((pPtpPort = PtpIfGetNextPortEntry (pPtpDomain->u4ContextId,
                                                   pPtpDomain->u1DomainId,
                                                   u4PortId)) != NULL) &&
               (pPtpPort->u4ContextId == pPtpDomain->u4ContextId) &&
               (pPtpPort->u1DomainId == pPtpDomain->u1DomainId))
        {
            u4PortId = pPtpPort->PortDs.u4PtpPortNumber;

            PtpIfDelPort (pPtpPort);
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPtpClockPriority1
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpClockPriority1
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpClockPriority1
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4SetValFsPtpClockPriority1)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority1 !=
            (UINT1) i4SetValFsPtpClockPriority1)
        {
            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority1 =
                (UINT1) i4SetValFsPtpClockPriority1;

            /* A domain may consist of various ports. Problem may come based
             * on the priority that port is MASTER. This indicates
             * that this port did not receive a valid best message before. 
             * Since the priority got changed, that may not be valid now. We
             * need to update the parent data set accordingly.
             * BMC will not update the data sets as there is only one port and
             * that too that port is in MASTER State and pPtpErBestMsg = NULL.
             * */
            PtpBmcDsUpdtDataSets (pPtpDomain);
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpClockPriority2
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpClockPriority2
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpClockPriority2 (INT4 i4FsPtpContextId,
                           INT4 i4FsPtpDomainNumber,
                           INT4 i4SetValFsPtpClockPriority2)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority2 !=
            (UINT1) i4SetValFsPtpClockPriority2)
        {
            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u1ClkPriority2 =
                (UINT1) i4SetValFsPtpClockPriority2;

            /* A domain may consist of various ports. Problem may come based
             * on the priority that port is MASTER. This indicates
             * that this port did not receive a valid best message before. 
             * Since the priority got changed, that may not be valid now. We
             * need to update the parent data set accordingly.
             * BMC will not update the data sets as there is only one port and
             * that too that port is in MASTER State and pPtpErBestMsg = NULL.
             * */
            PtpBmcDsUpdtDataSets (pPtpDomain);
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpClockSlaveOnly
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpClockSlaveOnly
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpClockSlaveOnly (INT4 i4FsPtpContextId,
                           INT4 i4FsPtpDomainNumber,
                           INT4 i4SetValFsPtpClockSlaveOnly)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly !=
            (BOOL1) i4SetValFsPtpClockSlaveOnly)
        {
            pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bSlaveOnly =
                (BOOL1) i4SetValFsPtpClockSlaveOnly;

            PtpDmnConfigSlaveOnDmnPorts (pPtpDomain->u4ContextId,
                                         pPtpDomain->u1DomainId);
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpClockPathTraceOption
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpClockPathTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpClockPathTraceOption (INT4 i4FsPtpContextId,
                                 INT4 i4FsPtpDomainNumber,
                                 INT4 i4SetValFsPtpClockPathTraceOption)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->bPathTraceOption !=
            (BOOL1) i4SetValFsPtpClockPathTraceOption)
        {
            pPtpDomain->bPathTraceOption =
                (BOOL1) i4SetValFsPtpClockPathTraceOption;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpClockSecurityEnabled
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpClockSecurityEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpClockSecurityEnabled (INT4 i4FsPtpContextId,
                                 INT4 i4FsPtpDomainNumber,
                                 INT4 i4SetValFsPtpClockSecurityEnabled)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->bSecurityEnabled !=
            (BOOL1) i4SetValFsPtpClockSecurityEnabled)
        {
            pPtpDomain->bSecurityEnabled =
                (BOOL1) i4SetValFsPtpClockSecurityEnabled;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpClockNumOfSA
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpClockNumOfSA
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpClockNumOfSA (INT4 i4FsPtpContextId,
                         INT4 i4FsPtpDomainNumber,
                         UINT4 u4SetValFsPtpClockNumOfSA)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->u4NumberOfSA != u4SetValFsPtpClockNumOfSA)
        {
            pPtpDomain->u4NumberOfSA = u4SetValFsPtpClockNumOfSA;
        }
    }

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpClockTwoStepFlag
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpClockTwoStepFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpClockTwoStepFlag (UINT4 *pu4ErrorCode,
                                INT4 i4FsPtpContextId,
                                INT4 i4FsPtpDomainNumber,
                                INT4 i4TestValFsPtpClockTwoStepFlag)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpClockTwoStepFlag != PTP_ENABLED) &&
        (i4TestValFsPtpClockTwoStepFlag != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid Truth Value for Clock Two-Step Flag \r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if ((pPtpDomain == NULL) ||
        (pPtpDomain->u1RowStatus != (UINT1) NOT_IN_SERVICE) ||
        ((pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE) &&
         (pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE)))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId,
                  (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Row status should be de-activated\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PTP_ERR_FORWARD_CLK);
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpClockNumberPorts
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpClockNumberPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpClockNumberPorts (UINT4 *pu4ErrorCode,
                                INT4 i4FsPtpContextId,
                                INT4 i4FsPtpDomainNumber,
                                INT4 i4TestValFsPtpClockNumberPorts)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain != NULL)
    {
        if (pPtpDomain->u1RowStatus != (UINT1) NOT_IN_SERVICE)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId,
                      (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Row status should be de-activated\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }

        if ((pPtpDomain->ClkMode == (UINT4) PTP_OC_CLOCK_MODE) &&
            (i4TestValFsPtpClockNumberPorts > 1))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_PTP_ERR_ORDINARY_CLK);
            return (INT1) SNMP_FAILURE;
        }

    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpClockPriority1
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpClockPriority1
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpClockPriority1 (UINT4 *pu4ErrorCode,
                              INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 i4TestValFsPtpClockPriority1)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpClockPriority1 > PTP_MAX_CLK_PRIORITY) ||
        (i4TestValFsPtpClockPriority1 < PTP_MIN_CLK_PRIORITY))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid PTP Clock Priority1\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain != NULL)
    {
        if ((pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE) &&
            (pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE))
        {
            PTP_TRC (((UINT4) i4FsPtpContextId,
                      (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Clock Mode is neither ordinary nor " "boundary\r\n"));
            CLI_SET_ERR (CLI_PTP_ERR_NOT_ORDINARY_BOUNDARY_CLK);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpClockPriority2
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpClockPriority2
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpClockPriority2 (UINT4 *pu4ErrorCode,
                              INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 i4TestValFsPtpClockPriority2)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpClockPriority2 > PTP_MAX_CLK_PRIORITY) ||
        (i4TestValFsPtpClockPriority2 < PTP_MIN_CLK_PRIORITY))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid PTP Clock Priority2\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain != NULL)
    {
        if ((pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE) &&
            (pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE))
        {
            PTP_TRC (((UINT4) i4FsPtpContextId,
                      (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Clock Mode is neither ordinary nor " "boundary\r\n"));
            CLI_SET_ERR (CLI_PTP_ERR_NOT_ORDINARY_BOUNDARY_CLK);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpClockSlaveOnly
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpClockSlaveOnly
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpClockSlaveOnly (UINT4 *pu4ErrorCode,
                              INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 i4TestValFsPtpClockSlaveOnly)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpClockSlaveOnly != PTP_ENABLED) &&
        (i4TestValFsPtpClockSlaveOnly != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid Truth Vaue for PTP Slave Only Clock\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    /* If Clock is not in ordinary mode, it cannot be configured
     * as Slave Only Clock */
    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain != NULL)
    {
        if (pPtpDomain->ClkMode != (UINT4) PTP_OC_CLOCK_MODE)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Only ordinary Clock can be configured as "
                      "Slave Only Clock\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_PTP_ERR_NOT_ORDINARY_CLK);
            return (INT1) SNMP_FAILURE;
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpClockPathTraceOption
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpClockPathTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpClockPathTraceOption
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4TestValFsPtpClockPathTraceOption)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpClockPathTraceOption != PTP_ENABLED) &&
        (i4TestValFsPtpClockPathTraceOption != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid Truth Value for Clock Path Trace Option \r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain != NULL)
    {
        if ((pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE) &&
            (pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE))
        {
            PTP_TRC (((UINT4) i4FsPtpContextId,
                      (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Clock Mode is neither ordinary nor " "boundary\r\n"));
            CLI_SET_ERR (CLI_PTP_ERR_NOT_ORDINARY_BOUNDARY_CLK);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpClockSecurityEnabled
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpClockSecurityEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1                nmhTestv2FsPtpClockSecurityEnabled
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4TestValFsPtpClockSecurityEnabled)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpClockSecurityEnabled != PTP_ENABLED) &&
        (i4TestValFsPtpClockSecurityEnabled != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid Truth Value for Clock Security Enabled flag\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain != NULL)
    {
        if ((pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE) &&
            (pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE))
        {
            PTP_TRC (((UINT4) i4FsPtpContextId,
                      (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Clock Mode is neither ordinary nor " "boundary\r\n"));
            CLI_SET_ERR (CLI_PTP_ERR_NOT_ORDINARY_BOUNDARY_CLK);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpClockNumOfSA
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpClockNumOfSA
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpClockNumOfSA
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, UINT4 u4TestValFsPtpClockNumOfSA)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if (u4TestValFsPtpClockNumOfSA > (UINT4) PTP_MAX_SA)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain != NULL)
    {
        if ((pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE) &&
            (pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE))
        {
            PTP_TRC (((UINT4) i4FsPtpContextId,
                      (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Clock Mode is neither ordinary nor " "boundary\r\n"));
            CLI_SET_ERR (CLI_PTP_ERR_NOT_ORDINARY_BOUNDARY_CLK);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpClockDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsPtpClockDataSetTable
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpCurrentDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpCurrentDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpCurrentDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if ((pPtpDomain == NULL) ||
        ((pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE) &&
         (pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE)))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist or the clock mode is "
                  "neither ordinary nor boundary\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpCurrentDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpCurrentDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    UINT4               u4PtpContextId = 0;
    UINT1               u1PtpDomainNumber = 0;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = (tPtpDomain *)
        PtpDbGetFirstNode (gPtpGlobalInfo.DomainTree,
                           (UINT1) PTP_DOMAIN_DATA_SET);

    if (pPtpDomain == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
        (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
    {
        *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
        return (INT1) SNMP_SUCCESS;
    }

    u4PtpContextId = pPtpDomain->pPtpCxt->u4ContextId;
    u1PtpDomainNumber = pPtpDomain->u1DomainId;

    while ((pPtpDomain = PtpDmnGetNextDomainEntry (u4PtpContextId,
                                                   u1PtpDomainNumber)) != NULL)
    {
        if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
            (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
        {
            *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
            *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
            return (INT1) SNMP_SUCCESS;
        }

        u4PtpContextId = pPtpDomain->pPtpCxt->u4ContextId;
        u1PtpDomainNumber = pPtpDomain->u1DomainId;
    }

    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpCurrentDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpCurrentDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    while ((pPtpDomain = PtpDmnGetNextDomainEntry ((UINT4) i4FsPtpContextId,
                                                   (UINT1) i4FsPtpDomainNumber))
           != NULL)
    {
        if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
            (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
        {
            *pi4NextFsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
            *pi4NextFsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
            return (INT1) SNMP_SUCCESS;
        }
        i4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        i4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
    }

    return (INT1) SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpCurrentStepsRemoved
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpCurrentStepsRemoved
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpCurrentStepsRemoved (INT4 i4FsPtpContextId,
                                INT4 i4FsPtpDomainNumber,
                                INT4 *pi4RetValFsPtpCurrentStepsRemoved)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpCurrentStepsRemoved =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.u4StepsRemoved;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpCurrentOffsetFromMaster
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpCurrentOffsetFromMaster
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpCurrentOffsetFromMaster (INT4 i4FsPtpContextId,
                                    INT4 i4FsPtpDomainNumber,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsPtpCurrentOffsetFromMaster)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        PtpDbDispTimeInterval (PTP_OFFSET_FROM_MASTER, pPtpDomain,
                               pRetValFsPtpCurrentOffsetFromMaster);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpCurrentMeanPathDelay
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpCurrentMeanPathDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpCurrentMeanPathDelay (INT4 i4FsPtpContextId,
                                 INT4 i4FsPtpDomainNumber,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsPtpCurrentMeanPathDelay)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        PtpDbDispTimeInterval (PTP_MEAN_PATH_DELAY, pPtpDomain,
                               pRetValFsPtpCurrentMeanPathDelay);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpCurrentMasterToSlaveDelay
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpCurrentMasterToSlaveDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpCurrentMasterToSlaveDelay
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpCurrentMasterToSlaveDelay)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpCurrentMasterToSlaveDelay = (INT4)
            pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
            u4CurrentMasterToSlaveDelay;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpCurrentSlaveToMasterDelay
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpCurrentSlaveToMasterDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpCurrentSlaveToMasterDelay
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpCurrentSlaveToMasterDelay)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpCurrentSlaveToMasterDelay = (INT4)
            pPtpDomain->ClockDs.PtpOcBcClkDs.CurrentDs.
            u4CurrentSlaveToMasterDelay;
    }

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsPtpParentDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpParentDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpParentDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if ((pPtpDomain == NULL) ||
        ((pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE) &&
         (pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE)))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist or the clock mode is "
                  "neither ordinary nor boundary\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpParentDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpParentDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    UINT4               u4PtpContextId = 0;
    UINT1               u1PtpDomainNumber = 0;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    pPtpDomain = (tPtpDomain *)
        PtpDbGetFirstNode (gPtpGlobalInfo.DomainTree,
                           (UINT1) PTP_DOMAIN_DATA_SET);

    if (pPtpDomain == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
        (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
    {
        *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
        return (INT1) SNMP_SUCCESS;
    }

    u4PtpContextId = pPtpDomain->pPtpCxt->u4ContextId;
    u1PtpDomainNumber = pPtpDomain->u1DomainId;

    while ((pPtpDomain = PtpDmnGetNextDomainEntry (u4PtpContextId,
                                                   u1PtpDomainNumber)) != NULL)
    {
        if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
            (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
        {
            *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
            *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
            return (INT1) SNMP_SUCCESS;
        }

        u4PtpContextId = pPtpDomain->pPtpCxt->u4ContextId;
        u1PtpDomainNumber = pPtpDomain->u1DomainId;
    }

    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpParentDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpParentDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    while ((pPtpDomain = PtpDmnGetNextDomainEntry ((UINT4) i4FsPtpContextId,
                                                   (UINT1) i4FsPtpDomainNumber))
           != NULL)
    {
        if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
            (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
        {
            *pi4NextFsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
            *pi4NextFsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
            return (INT1) SNMP_SUCCESS;
        }
        i4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        i4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
    }

    return (INT1) SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpParentClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentClockIdentity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpParentClockIdentity
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPtpParentClockIdentity)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpParentClockIdentity->i4_Length = PTP_MAX_CLOCK_ID_LEN;
        MEMCPY (pRetValFsPtpParentClockIdentity->pu1_OctetList,
                pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.ClkId,
                PTP_MAX_CLOCK_ID_LEN);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentPortNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpParentPortNumber
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpParentPortNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpParentPortNumber =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u4PortIndex;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentStats
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpParentStats (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                        INT4 *pi4RetValFsPtpParentStats)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpParentStats =
            (INT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.bParentStats;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentObservedOffsetScaledLogVariance
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentObservedOffsetScaledLogVariance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpParentObservedOffsetScaledLogVariance
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpParentObservedOffsetScaledLogVariance)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpParentObservedOffsetScaledLogVariance
            = pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.
            u2ObservedOffsetScaledLogVariance;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentObservedClockPhaseChangeRate
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentObservedClockPhaseChangeRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpParentObservedClockPhaseChangeRate
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpParentObservedClockPhaseChangeRate)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpParentObservedClockPhaseChangeRate
            = pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.
            u4ObservedClkPhaseChangeRate;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentGMClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentGMClockIdentity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpParentGMClockIdentity (INT4 i4FsPtpContextId,
                                  INT4 i4FsPtpDomainNumber,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsPtpParentGMClockIdentity)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpParentGMClockIdentity->i4_Length = PTP_MAX_CLOCK_ID_LEN;
        MEMCPY (pRetValFsPtpParentGMClockIdentity->pu1_OctetList,
                pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.GMClkId,
                PTP_MAX_CLOCK_ID_LEN);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentGMClockClass
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentGMClockClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpParentGMClockClass (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 *pi4RetValFsPtpParentGMClockClass)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpParentGMClockClass =
            pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.GMClkQuality.u1Class;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentGMClockAccuracy
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentGMClockAccuracy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpParentGMClockAccuracy
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpParentGMClockAccuracy)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpParentGMClockAccuracy
            = pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.GMClkQuality.u1Accuracy;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentGMClockOffsetScaledLogVariance
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentGMClockOffsetScaledLogVariance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpParentGMClockOffsetScaledLogVariance
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpParentGMClockOffsetScaledLogVariance)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpParentGMClockOffsetScaledLogVariance =
            pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.
            u2ObservedOffsetScaledLogVariance;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentGMPriority1
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentGMPriority1
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpParentGMPriority1 (INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 *pi4RetValFsPtpParentGMPriority1)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpParentGMPriority1
            = pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u1GMPriority1;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentGMPriority2
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentGMPriority2
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpParentGMPriority2 (INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 *pi4RetValFsPtpParentGMPriority2)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpParentGMPriority2
            = pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u1GMPriority2;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpParentClockObservedDrift
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpParentClockObservedDrift
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpParentClockObservedDrift
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpParentClockObservedDrift)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpParentClockObservedDrift
            = (UINT4) pPtpDomain->ClockDs.PtpOcBcClkDs.ParentDs.u4ObservedDrift;
    }

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsPtpTimeDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpTimeDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpTimeDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if ((pPtpDomain == NULL) ||
        ((pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE) &&
         (pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE)))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist or the clock mode is "
                  "neither ordinary nor boundary\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpTimeDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpTimeDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    UINT4               u4PtpContextId = 0;
    UINT1               u1PtpDomainNumber = 0;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    pPtpDomain = (tPtpDomain *)
        PtpDbGetFirstNode (gPtpGlobalInfo.DomainTree,
                           (UINT1) PTP_DOMAIN_DATA_SET);

    if (pPtpDomain == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
        (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
    {
        *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
        return (INT1) SNMP_SUCCESS;
    }

    u4PtpContextId = pPtpDomain->pPtpCxt->u4ContextId;
    u1PtpDomainNumber = pPtpDomain->u1DomainId;

    while ((pPtpDomain = PtpDmnGetNextDomainEntry (u4PtpContextId,
                                                   u1PtpDomainNumber)) != NULL)
    {
        if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
            (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
        {
            *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
            *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
            return (INT1) SNMP_SUCCESS;
        }

        u4PtpContextId = pPtpDomain->pPtpCxt->u4ContextId;
        u1PtpDomainNumber = pPtpDomain->u1DomainId;
    }

    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpTimeDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpTimeDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    while ((pPtpDomain = PtpDmnGetNextDomainEntry ((UINT4) i4FsPtpContextId,
                                                   (UINT1) i4FsPtpDomainNumber))
           != NULL)
    {
        if ((pPtpDomain->ClkMode == PTP_OC_CLOCK_MODE) ||
            (pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
        {
            *pi4NextFsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
            *pi4NextFsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
            return (INT1) SNMP_SUCCESS;
        }
        i4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        i4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
    }

    return (INT1) SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpTimeCurrentUTCOffset
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTimeCurrentUTCOffset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpTimeCurrentUTCOffset (INT4 i4FsPtpContextId,
                                 INT4 i4FsPtpDomainNumber,
                                 INT4 *pi4RetValFsPtpTimeCurrentUTCOffset)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTimeCurrentUTCOffset
            = pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.u2CurrentUtcOffset;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTimeCurrentUTCOffsetValid
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTimeCurrentUTCOffsetValid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTimeCurrentUTCOffsetValid
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpTimeCurrentUTCOffsetValid)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTimeCurrentUTCOffsetValid
            = pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bCurrentUtcOffsetValid;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTimeLeap59
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTimeLeap59
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpTimeLeap59 (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                       INT4 *pi4RetValFsPtpTimeLeap59)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTimeLeap59
            = pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bLeap59;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTimeLeap61
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTimeLeap61
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpTimeLeap61 (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                       INT4 *pi4RetValFsPtpTimeLeap61)
{
    tPtpDomain         *pPtpDomain = NULL;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
    }
    else
    {
        *pi4RetValFsPtpTimeLeap61
            = pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bLeap61;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTimeTimeTraceable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTimeTimeTraceable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpTimeTimeTraceable (INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 *pi4RetValFsPtpTimeTimeTraceable)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTimeTimeTraceable
            = pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bTimeTraceable;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTimeFrequencyTraceable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTimeFrequencyTraceable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTimeFrequencyTraceable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpTimeFrequencyTraceable)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTimeFrequencyTraceable
            = pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.bFreqTraceable;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTimeTimeSource
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTimeTimeSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTimeTimeSource
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpTimeTimeSource)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTimeTimeSource
            = pPtpDomain->ClockDs.PtpOcBcClkDs.TimeDs.u1TimeSource;
    }

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsPtpPortConfigDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpPortConfigDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpPortConfigDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpPortConfigDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpPortConfigDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber,
     INT4 *pi4FsPtpPortIndex)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    pPtpPort = (tPtpPort *)
        PtpDbGetFirstNode (gPtpGlobalInfo.PortTree,
                           (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET);
    while (pPtpPort != NULL)
    {
        if (pPtpPort->bIsTranPort == OSIX_FALSE)
        {
            *pi4FsPtpContextId = (INT4) pPtpPort->pPtpDomain->pPtpCxt->
                u4ContextId;
            *pi4FsPtpDomainNumber = (INT4) pPtpPort->pPtpDomain->u1DomainId;
            *pi4FsPtpPortIndex = (INT4) pPtpPort->PortDs.u4PtpPortNumber;
            return (INT1) SNMP_SUCCESS;
        }

        pPtpPort = (tPtpPort *)
            PtpDbGetNextNode (gPtpGlobalInfo.PortTree,
                              pPtpPort,
                              (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET);
    }

    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpPortConfigDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
                FsPtpPortIndex
                nextFsPtpPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpPortConfigDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 *pi4NextFsPtpPortIndex)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    while ((pPtpPort =
            PtpIfGetNextPortEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpPortIndex)) != NULL)
    {
        if (pPtpPort->bIsTranPort == OSIX_FALSE)
        {
            *pi4NextFsPtpContextId = (INT4) pPtpPort->pPtpDomain->pPtpCxt->
                u4ContextId;
            *pi4NextFsPtpDomainNumber = (INT4) pPtpPort->pPtpDomain->u1DomainId;
            *pi4NextFsPtpPortIndex = (INT4) pPtpPort->PortDs.u4PtpPortNumber;
            return (INT1) SNMP_SUCCESS;
        }
        i4FsPtpPortIndex = (INT4) pPtpPort->PortDs.u4PtpPortNumber;
        i4FsPtpDomainNumber = (INT4) pPtpPort->pPtpDomain->u1DomainId;
        i4FsPtpContextId = (INT4) pPtpPort->pPtpDomain->pPtpCxt->u4ContextId;
    }

    return (INT1) SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpPortClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortClockIdentity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortClockIdentity (INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 i4FsPtpPortIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsPtpPortClockIdentity)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);
    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpPortClockIdentity->i4_Length = PTP_MAX_CLOCK_ID_LEN;
        MEMCPY (pRetValFsPtpPortClockIdentity->pu1_OctetList,
                pPtpPort->PortDs.ClkId, PTP_MAX_CLOCK_ID_LEN);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortInterfaceType
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortInterfaceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortInterfaceType (INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 i4FsPtpPortIndex,
                              INT4 *pi4RetValFsPtpPortInterfaceType)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);
    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortInterfaceType = (INT4) pPtpPort->PtpDeviceType;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortIfaceNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortIfaceNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortIfaceNumber (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpPortIndex,
                            INT4 *pi4RetValFsPtpPortIfaceNumber)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);
    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortIfaceNumber = (INT4) pPtpPort->u4IfIndex;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortState
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortState (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                      INT4 i4FsPtpPortIndex, INT4 *pi4RetValFsPtpPortState)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);
    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortState = (INT4) pPtpPort->PortDs.u1PortState;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortMinDelayReqInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortMinDelayReqInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortMinDelayReqInterval (INT4 i4FsPtpContextId,
                                    INT4 i4FsPtpDomainNumber,
                                    INT4 i4FsPtpPortIndex,
                                    INT4 *pi4RetValFsPtpPortMinDelayReqInterval)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortMinDelayReqInterval =
            (INT4) pPtpPort->PortDs.u1MinDelayReqInterval;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortPeerMeanPathDelay
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortPeerMeanPathDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortPeerMeanPathDelay (INT4 i4FsPtpContextId,
                                  INT4 i4FsPtpDomainNumber,
                                  INT4 i4FsPtpPortIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsPtpPortPeerMeanPathDelay)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        PtpDbDispTimeInterval (PTP_PEER_MEAN_PATH_DELAY, pPtpPort,
                               pRetValFsPtpPortPeerMeanPathDelay);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortAnnounceInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortAnnounceInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortAnnounceInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 *pi4RetValFsPtpPortAnnounceInterval)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortAnnounceInterval =
            (INT4) pPtpPort->PortDs.u1AnnounceInterval;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortAnnounceReceiptTimeout
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortAnnounceReceiptTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortAnnounceReceiptTimeout
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 *pi4RetValFsPtpPortAnnounceReceiptTimeout)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortAnnounceReceiptTimeout =
            (INT4) pPtpPort->PortDs.u1AnnounceReceiptTimeOut;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortSyncInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortSyncInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortSyncInterval (INT4 i4FsPtpContextId,
                             INT4 i4FsPtpDomainNumber,
                             INT4 i4FsPtpPortIndex,
                             INT4 *pi4RetValFsPtpPortSyncInterval)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortSyncInterval =
            (INT4) pPtpPort->PortDs.i1SyncInterval;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortSynclimit
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortSynclimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortSynclimit (INT4 i4FsPtpContextId,
                          INT4 i4FsPtpDomainNumber,
                          INT4 i4FsPtpPortIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsPtpPortSynclimit)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        FSAP_U8_2STR (&(pPtpPort->PortDs.u8SyncLimit),
                      (CHR1 *) (pRetValFsPtpPortSynclimit->pu1_OctetList));

        pRetValFsPtpPortSynclimit->i4_Length =
            STRLEN ((pRetValFsPtpPortSynclimit->pu1_OctetList));
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortDelayMechanism
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortDelayMechanism
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortDelayMechanism (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpPortIndex,
                               INT4 *pi4RetValFsPtpPortDelayMechanism)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortDelayMechanism =
            (INT4) pPtpPort->PortDs.u1DelayMechanism;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortMinPdelayReqInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortMinPdelayReqInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortMinPdelayReqInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 *pi4RetValFsPtpPortMinPdelayReqInterval)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortMinPdelayReqInterval =
            (INT4) pPtpPort->PortDs.u1MinPdelayReqInterval;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortVersionNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortVersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortVersionNumber
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 *pi4RetValFsPtpPortVersionNumber)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortVersionNumber =
            (INT4) pPtpPort->PortDs.u4VersionNumber;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortUnicastNegOption
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortUnicastNegOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortUnicastNegOption (INT4 i4FsPtpContextId,
                                 INT4 i4FsPtpDomainNumber,
                                 INT4 i4FsPtpPortIndex,
                                 INT4 *pi4RetValFsPtpPortUnicastNegOption)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortUnicastNegOption = (INT4) pPtpPort->unicastNegOption;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortUnicastMasterMaxSize
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortUnicastMasterMaxSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortUnicastMasterMaxSize
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 *pi4RetValFsPtpPortUnicastMasterMaxSize)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortUnicastMasterMaxSize =
            (INT4) pPtpPort->u2UnicastMastMaxSize;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortAccMasterEnabled
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortAccMasterEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortAccMasterEnabled
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 *pi4RetValFsPtpPortAccMasterEnabled)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortAccMasterEnabled = (INT4) pPtpPort->bAccMastEnabled;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortNumOfAltMaster
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortNumOfAltMaster
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortNumOfAltMaster (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpPortIndex,
                               INT4 *pi4RetValFsPtpPortNumOfAltMaster)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortNumOfAltMaster =
            (INT4) pPtpPort->u1NumberOfAltMaster;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortAltMulcastSync
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortAltMulcastSync
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortAltMulcastSync (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpPortIndex,
                               INT4 *pi4RetValFsPtpPortAltMulcastSync)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortAltMulcastSync = (INT4) pPtpPort->bAltMulcastSync;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortAltMulcastSyncInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortAltMulcastSyncInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortAltMulcastSyncInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 *pi4RetValFsPtpPortAltMulcastSyncInterval)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortAltMulcastSyncInterval =
            (INT4) pPtpPort->u1AltMulcastSynInterval;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortPtpStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortPtpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortPtpStatus (INT4 i4FsPtpContextId,
                          INT4 i4FsPtpDomainNumber,
                          INT4 i4FsPtpPortIndex,
                          INT4 *pi4RetValFsPtpPortPtpStatus)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortPtpStatus =
            (INT4) pPtpPort->PortDs.u1PtpEnabledStatus;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortRcvdAnnounceMsgCnt
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortRcvdAnnounceMsgCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortRcvdAnnounceMsgCnt
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, UINT4 *pu4RetValFsPtpPortRcvdAnnounceMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortRcvdAnnounceMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4RcvdAnnounceMsgCnt;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortRcvdSyncMsgCnt
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortRcvdSyncMsgCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortRcvdSyncMsgCnt
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, UINT4 *pu4RetValFsPtpPortRcvdSyncMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortRcvdSyncMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4RcvdSyncMsgCnt;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortRcvdDelayReqMsgCnt
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortRcvdDelayReqMsgCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortRcvdDelayReqMsgCnt
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, UINT4 *pu4RetValFsPtpPortRcvdDelayReqMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortRcvdDelayReqMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4RcvdDelayReqMsgCnt;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortRcvdDelayRespMsgCnt
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPort2RcvdDelayRespMsgCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortRcvdDelayRespMsgCnt
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, UINT4 *pu4RetValFsPtpPortRcvdDelayRespMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortRcvdDelayRespMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4RcvdDelayRespMsgCnt;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortTransDelayReqMsgCnt
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortTransDelayReqMsgCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortTransDelayReqMsgCnt
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, UINT4 *pu4RetValFsPtpPortTransDelayReqMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT1) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortTransDelayReqMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4TransDelayReqMsgCnt;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpPortDiscardedMsgCnt
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortDiscardedMsgCnt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpPortDiscardedMsgCnt
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, UINT4 *pu4RetValFsPtpPortDiscardedMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortDiscardedMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt;
    }

    return i1RetVal;
}

/****************************************************************************
 *  Function    :  nmhGetFsPtpPortTransAnnounceMsgCnt
 *  Input       :  The Indices
 *                 FsPtpContextId
 *                 FsPtpDomainNumber
 *                 FsPtpPortIndex
 *  
 *                 The Object
 *                 retValFsPtpPortTransAnnounceMsgCnt
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPtpPortTransAnnounceMsgCnt (INT4 i4FsPtpContextId,
                                    INT4 i4FsPtpDomainNumber,
                                    INT4 i4FsPtpPortIndex,
                                    UINT4
                                    *pu4RetValFsPtpPortTransAnnounceMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortTransAnnounceMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4TransAnnounceMsgCnt;
    }

    return i1RetVal;

}

/****************************************************************************
 *  Function    :  nmhGetFsPtpPortTransSyncMsgCnt
 *  Input       :  The Indices
 *                 FsPtpContextId
 *                 FsPtpDomainNumber
 *                 FsPtpPortIndex
 *                 
 *                 The Object
 *                 retValFsPtpPortTransSyncMsgCnt
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsPtpPortTransSyncMsgCnt (INT4 i4FsPtpContextId,
                                INT4 i4FsPtpDomainNumber,
                                INT4 i4FsPtpPortIndex,
                                UINT4 *pu4RetValFsPtpPortTransSyncMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortTransSyncMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4TransSyncMsgCnt;
    }

    return i1RetVal;

}

/****************************************************************************
 *  Function    :  nmhGetFsPtpPortRcvdPeerDelayReqMsgCnt
 *  Input       :  The Indices
 *                 FsPtpContextId
 *                 FsPtpDomainNumber
 *                 FsPtpPortIndex
 *
 *                 The Object
 *                 retValFsPtpPortRcvdPeerDelayReqMsgCnt
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsPtpPortRcvdPeerDelayReqMsgCnt (INT4 i4FsPtpContextId,
                                       INT4 i4FsPtpDomainNumber,
                                       INT4 i4FsPtpPortIndex,
                                       UINT4
                                       *pu4RetValFsPtpPortRcvdPeerDelayReqMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortRcvdPeerDelayReqMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4RcvdPeerDelayReqMsgCnt;
    }

    return i1RetVal;
}

/****************************************************************************
 *  Function    :  nmhGetFsPtpPortTransPeerDelayReqMsgCnt
 *  Input       :  The Indices
 *                 FsPtpContextId
 *                 FsPtpDomainNumber
 *                 FsPtpPortIndex
 *
 *                 The Object
 *                 retValFsPtpPortTransPeerDelayReqMsgCnt
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsPtpPortTransPeerDelayReqMsgCnt (INT4 i4FsPtpContextId,
                                        INT4 i4FsPtpDomainNumber,
                                        INT4 i4FsPtpPortIndex,
                                        UINT4
                                        *pu4RetValFsPtpPortTransPeerDelayReqMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortTransPeerDelayReqMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4TransPeerDelayReqMsgCnt;
    }

    return i1RetVal;

}

/****************************************************************************
 *  Function    :  nmhGetFsPtpPortRcvdPeerDelayRespMsgCnt
 *  Input       :  The Indices
 *                 FsPtpContextId
 *                 FsPtpDomainNumber
 *                 FsPtpPortIndex
 *
 *                 The Object
 *                 retValFsPtpPortRcvdPeerDelayRespMsgCnt
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsPtpPortRcvdPeerDelayRespMsgCnt (INT4 i4FsPtpContextId,
                                        INT4 i4FsPtpDomainNumber,
                                        INT4 i4FsPtpPortIndex,
                                        UINT4
                                        *pu4RetValFsPtpPortRcvdPeerDelayRespMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortRcvdPeerDelayRespMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4RcvdPeerDelayRespMsgCnt;
    }

    return i1RetVal;

}

/****************************************************************************
 *  Function    :  nmhGetFsPtpPortTransPeerDelayRespMsgCnt
 *  Input       :  The Indices
 *                 FsPtpContextId
 *                 FsPtpDomainNumber
 *                 FsPtpPortIndex
 *
 *                 The Object
 *                 retValFsPtpPortTransPeerDelayRespMsgCnt
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsPtpPortTransPeerDelayRespMsgCnt (INT4 i4FsPtpContextId,
                                         INT4 i4FsPtpDomainNumber,
                                         INT4 i4FsPtpPortIndex,
                                         UINT4
                                         *pu4RetValFsPtpPortTransPeerDelayRespMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortTransPeerDelayRespMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4TransPeerDelayRespMsgCnt;
    }

    return i1RetVal;

}

/****************************************************************************
 * Function    :  nmhGetFsPtpPortTransDelayRespMsgCnt
 * Input       :  The Indices
 *                FsPtpContextId
 *                FsPtpDomainNumber
 *                FsPtpPortIndex
 *
 *                The Object
 *                retValFsPtpPortTransDelayRespMsgCnt
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsPtpPortTransDelayRespMsgCnt (INT4 i4FsPtpContextId,
                                     INT4 i4FsPtpDomainNumber,
                                     INT4 i4FsPtpPortIndex,
                                     UINT4
                                     *pu4RetValFsPtpPortTransDelayRespMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortTransDelayRespMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4TransDelayRespMsgCnt;
    }
    return i1RetVal;
}

/****************************************************************************
 * Function    :  nmhGetFsPtpPortTransPeerDelayRespFollowUpMsgCnt
 * Input       :  The Indices
 *                FsPtpContextId
 *                FsPtpDomainNumber
 *                FsPtpPortIndex
 *
 *                The Object
 *                retValFsPtpPortTransPeerDelayRespFollowUpMsgCnt
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 * Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsPtpPortTransPeerDelayRespFollowUpMsgCnt (INT4 i4FsPtpContextId,
                                                 INT4 i4FsPtpDomainNumber,
                                                 INT4 i4FsPtpPortIndex,
                                                 UINT4
                                                 *pu4RetValFsPtpPortTransPeerDelayRespFollowUpMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortTransPeerDelayRespFollowUpMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4TransPeerDelayRespFollowUpMsgCnt;
    }
    return i1RetVal;
}

/****************************************************************************
 *  Function    :  nmhGetFsPtpPortTransFollowUpMsgCnt
 *  Input       :  The Indices
 *                 FsPtpContextId
 *                 FsPtpDomainNumber
 *                 FsPtpPortIndex
 *
 *                 The Object
 *                 retValFsPtpPortTransFollowUpMsgCnt
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsPtpPortTransFollowUpMsgCnt (INT4 i4FsPtpContextId,
                                    INT4 i4FsPtpDomainNumber,
                                    INT4 i4FsPtpPortIndex,
                                    UINT4
                                    *pu4RetValFsPtpPortTransFollowUpMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortTransFollowUpMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4TransFollowUpMsgCnt;
    }
    return i1RetVal;

}

/****************************************************************************
 *  Function    :  nmhGetFsPtpPortRcvdPeerDelayRespFollowUpMsgCnt
 *  Input       :  The Indices
 *                 FsPtpContextId
 *                 FsPtpDomainNumber
 *                 FsPtpPortIndex
 *
 *                 The Object
 *                 retValFsPtpPortRcvdPeerDelayRespFollowUpMsgCnt
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsPtpPortRcvdPeerDelayRespFollowUpMsgCnt (INT4 i4FsPtpContextId,
                                                INT4 i4FsPtpDomainNumber,
                                                INT4 i4FsPtpPortIndex,
                                                UINT4
                                                *pu4RetValFsPtpPortRcvdPeerDelayRespFollowUpMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortRcvdPeerDelayRespFollowUpMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4RcvdPeerDelayRespFollowUpMsgCnt;
    }
    return i1RetVal;
}

/****************************************************************************
 *  Function    :  nmhGetFsPtpPortRcvdFollowUpMsgCnt
 *  Input       :  The Indices
 *                 FsPtpContextId
 *                 FsPtpDomainNumber
 *                 FsPtpPortIndex
 *
 *                 The Object
 *                 retValFsPtpPortRcvdFollowUpMsgCnt
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsPtpPortRcvdFollowUpMsgCnt (INT4 i4FsPtpContextId,
                                   INT4 i4FsPtpDomainNumber,
                                   INT4 i4FsPtpPortIndex,
                                   UINT4 *pu4RetValFsPtpPortRcvdFollowUpMsgCnt)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpPortRcvdFollowUpMsgCnt =
            (UINT4) pPtpPort->PtpPortStats.u4RcvdFollowUpMsgCnt;
    }
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFsPtpPortRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                retValFsPtpPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpPortRowStatus (INT4 i4FsPtpContextId,
                          INT4 i4FsPtpDomainNumber,
                          INT4 i4FsPtpPortIndex,
                          INT4 *pi4RetValFsPtpPortRowStatus)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpPortRowStatus = (INT4) pPtpPort->u1RowStatus;
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpPortClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortClockIdentity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpPortClockIdentity (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                              INT4 i4FsPtpPortIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsPtpPortClockIdentity)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if ((MEMCMP (pPtpPort->PortDs.ClkId,
                     pSetValFsPtpPortClockIdentity->pu1_OctetList,
                     PTP_MAX_CLOCK_ID_LEN)) != 0)
        {
            MEMCPY (pPtpPort->PortDs.ClkId,
                    pSetValFsPtpPortClockIdentity->pu1_OctetList,
                    PTP_MAX_CLOCK_ID_LEN);
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortInterfaceType
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortInterfaceType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpPortInterfaceType (INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 i4FsPtpPortIndex,
                              INT4 i4SetValFsPtpPortInterfaceType)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (i4SetValFsPtpPortInterfaceType != (INT4) pPtpPort->PtpDeviceType)
        {
            pPtpPort->PtpDeviceType = (UINT4) i4SetValFsPtpPortInterfaceType;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortIfaceNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortIfaceNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpPortIfaceNumber (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpPortIndex,
                            INT4 i4SetValFsPtpPortIfaceNumber)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pPtpPort->u4IfIndex = (UINT4) i4SetValFsPtpPortIfaceNumber;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortMinDelayReqInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortMinDelayReqInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortMinDelayReqInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortMinDelayReqInterval)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpPort->PortDs.u1MinDelayReqInterval !=
            (UINT1) i4SetValFsPtpPortMinDelayReqInterval)
        {
            pPtpPort->PortDs.u1MinDelayReqInterval =
                (UINT1) i4SetValFsPtpPortMinDelayReqInterval;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortAnnounceInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortAnnounceInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortAnnounceInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortAnnounceInterval)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4MilliSec = 0;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    if (pPtpPort->PortDs.u1AnnounceInterval !=
        (UINT1) i4SetValFsPtpPortAnnounceInterval)
    {
        pPtpPort->PortDs.u1AnnounceInterval =
            (UINT1) i4SetValFsPtpPortAnnounceInterval;

        if (pPtpPort->PortDs.u1PortState == PTP_STATE_MASTER)
        {
            u4MilliSec =
                PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                           u1AnnounceInterval);

            if (PtpTmrStartTmr (&(pPtpPort->AnnounceTimer),
                                u4MilliSec, PTP_ANNC_TMR, OSIX_FALSE)
                == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Failed to start the timer\r\n"));
                return (INT1) SNMP_FAILURE;
            }
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortAnnounceReceiptTimeout
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortAnnounceReceiptTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortAnnounceReceiptTimeout
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortAnnounceReceiptTimeout)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4MilliSec = 0;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpPort->PortDs.u1AnnounceReceiptTimeOut !=
            (UINT1) i4SetValFsPtpPortAnnounceReceiptTimeout)
        {
            pPtpPort->PortDs.u1AnnounceReceiptTimeOut =
                (UINT1) i4SetValFsPtpPortAnnounceReceiptTimeout;

            /* Timer should be started only in certain port states */
            if ((pPtpPort->PortDs.u1PortState == PTP_STATE_LISTENING) ||
                (pPtpPort->PortDs.u1PortState == PTP_STATE_UNCALIBRATED) ||
                (pPtpPort->PortDs.u1PortState == PTP_STATE_SLAVE) ||
                (pPtpPort->PortDs.u1PortState == PTP_STATE_PASSIVE))
            {
                u4MilliSec =
                    PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                               u1AnnounceInterval);
                if (PtpTmrStartTmr (&(pPtpPort->AnnounceReciptTimeOutTimer),
                                    (pPtpPort->PortDs.u1AnnounceReceiptTimeOut
                                     * u4MilliSec),
                                    PTP_ANNC_RECEIPT_TMR, OSIX_FALSE)
                    == OSIX_FAILURE)
                {
                    PTP_TRC (((UINT4) i4FsPtpContextId,
                              (UINT1) i4FsPtpDomainNumber,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Failed to start the timer\r\n"));
                    i1RetVal = (INT1) SNMP_FAILURE;
                }
            }
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortSyncInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortSyncInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortSyncInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortSyncInterval)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4MilliSec = 0;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    if (pPtpPort->PortDs.i1SyncInterval != (INT1) i4SetValFsPtpPortSyncInterval)
    {
        pPtpPort->PortDs.i1SyncInterval = (INT1) i4SetValFsPtpPortSyncInterval;

        if (pPtpPort->PortDs.u1PortState == PTP_STATE_MASTER)
        {
            u4MilliSec =
                PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                           i1SyncInterval);
            if (PtpTmrStartTmr (&(pPtpPort->SyncTimer),
                                u4MilliSec, PTP_SYNC_TMR, OSIX_FALSE)
                == OSIX_FAILURE)
            {
                {
                    PTP_TRC (((UINT4) i4FsPtpContextId,
                              (UINT1) i4FsPtpDomainNumber,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Failed to start the timer\r\n"));
                    return (INT1) SNMP_FAILURE;
                }
            }
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortSynclimit
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortSynclimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortSynclimit
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, tSNMP_OCTET_STRING_TYPE * pSetValFsPtpPortSynclimit)
{
    tPtpPort           *pPtpPort = NULL;
    FS_UINT8            u8TmpSyncLimit;
    UINT1               u1U8Str[PTP_U8_STR_LEN];
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&u1U8Str[0], '\0', PTP_U8_STR_LEN);
    MEMCPY (&u1U8Str[0], pSetValFsPtpPortSynclimit->pu1_OctetList,
            pSetValFsPtpPortSynclimit->i4_Length);

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        FSAP_STR2_U8 ((CHR1 *) (&u1U8Str[0]), &(u8TmpSyncLimit));

        if (FSAP_U8_CMP (&(u8TmpSyncLimit),
                         &(pPtpPort->PortDs.u8SyncLimit)) != 0)
        {
            FSAP_U8_ASSIGN (&(pPtpPort->PortDs.u8SyncLimit), &(u8TmpSyncLimit));
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortDelayMechanism
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortDelayMechanism
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpPortDelayMechanism (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpPortIndex,
                               INT4 i4SetValFsPtpPortDelayMechanism)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4MilliSec = 0;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    if (pPtpPort->PortDs.u1DelayMechanism !=
        (UINT1) i4SetValFsPtpPortDelayMechanism)
    {
        pPtpPort->PortDs.u1DelayMechanism =
            (UINT1) i4SetValFsPtpPortDelayMechanism;

        if (i4SetValFsPtpPortDelayMechanism == PTP_PORT_DELAY_MECH_PEER_TO_PEER)
        {
            u4MilliSec =
                PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                           u1MinPdelayReqInterval);
            if (PtpTmrStartTmr (&(pPtpPort->PdelayReqTimer),
                                u4MilliSec, PTP_PDREQ_TMR, OSIX_TRUE)
                == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) i4FsPtpContextId,
                          (UINT1) i4FsPtpDomainNumber,
                          CONTROL_PLANE_TRC, "Unable to restart Peer delay "
                          "request timer\r\n", pPtpPort, PTP_PDREQ_TMR));
                return (INT1) SNMP_FAILURE;
            }
        }
        else
        {
            /* Stop the peer delay request timer */
            TmrStop (gPtpGlobalInfo.PtpTmrListId, &(pPtpPort->PdelayReqTimer));
        }

        /* Send delay mechanism change indication to NPAPI */
        PtpNpWrInitPortParams (pPtpPort);

    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortMinPdelayReqInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortMinPdelayReqInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortMinPdelayReqInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortMinPdelayReqInterval)
{
    tPtpPort           *pPtpPort = NULL;
    UINT4               u4MilliSec = 0;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpPort->PortDs.u1MinPdelayReqInterval !=
            (UINT1) i4SetValFsPtpPortMinPdelayReqInterval)
        {
            pPtpPort->PortDs.u1MinPdelayReqInterval =
                (UINT1) i4SetValFsPtpPortMinPdelayReqInterval;

            u4MilliSec =
                PtpUtilLogBase2ToMilliSec ((INT1) pPtpPort->PortDs.
                                           u1MinDelayReqInterval);
            if (PtpTmrStartTmr (&(pPtpPort->DelayReqTimer),
                                u4MilliSec, PTP_PDREQ_TMR, OSIX_TRUE)
                == OSIX_FAILURE)
            {
                {
                    PTP_TRC (((UINT4) i4FsPtpContextId,
                              (UINT1) i4FsPtpDomainNumber,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Failed to start the timer\r\n"));
                    i1RetVal = (INT1) SNMP_FAILURE;
                }
            }
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortVersionNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortVersionNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortVersionNumber
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortVersionNumber)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpPort->PortDs.u4VersionNumber !=
            (UINT4) i4SetValFsPtpPortVersionNumber)
        {
            pPtpPort->PortDs.u4VersionNumber =
                (UINT4) i4SetValFsPtpPortVersionNumber;

            if (pPtpPort->PortDs.u1PortState != PTP_STATE_DISABLED)
            {
                PtpSemPortSemHandler (pPtpPort, PTP_INIT_EVENT);
            }

            PtpNpWrInitPortParams (pPtpPort);
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortUnicastNegOption
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortUnicastNegOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortUnicastNegOption
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortUnicastNegOption)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpPort->unicastNegOption !=
            (BOOL1) i4SetValFsPtpPortUnicastNegOption)
        {
            pPtpPort->unicastNegOption =
                (BOOL1) i4SetValFsPtpPortUnicastNegOption;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortUnicastMasterMaxSize
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortUnicastMasterMaxSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortUnicastMasterMaxSize
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortUnicastMasterMaxSize)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pPtpPort->u2UnicastMastMaxSize =
            (UINT2) i4SetValFsPtpPortUnicastMasterMaxSize;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortAccMasterEnabled
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortAccMasterEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortAccMasterEnabled
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortAccMasterEnabled)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpPort->bAccMastEnabled !=
            (BOOL1) i4SetValFsPtpPortAccMasterEnabled)

        {
            pPtpPort->bAccMastEnabled =
                (BOOL1) i4SetValFsPtpPortAccMasterEnabled;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortNumOfAltMaster
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortNumOfAltMaster
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpPortNumOfAltMaster (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpPortIndex,
                               INT4 i4SetValFsPtpPortNumOfAltMaster)
{
    tPtpAltMstInfo      PtpAltMstInfo;
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        /* Call the Protocol Configuration handler for the Alternate 
         * Master option */
        MEMSET (&PtpAltMstInfo, 0, sizeof (tPtpAltMstInfo));

        PtpAltMstInfo.u1NumberOfAltMaster =
            (UINT1) i4SetValFsPtpPortNumOfAltMaster;
        PtpAltMstInfo.u1Flag = PTP_AM_UPDATE_MST_COUNT;

        if ((PtpAlMstHandleUpdateConfOpt ((UINT4) i4FsPtpContextId,
                                          (UINT1) i4FsPtpDomainNumber,
                                          (UINT4) i4FsPtpPortIndex,
                                          &PtpAltMstInfo)) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "PtpAlMstHandleUpdateConfOpt failed.\r\n"));
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortAltMulcastSync
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortAltMulcastSync
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpPortAltMulcastSync (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpPortIndex,
                               INT4 i4SetValFsPtpPortAltMulcastSync)
{
    tPtpAltMstInfo      PtpAltMstInfo;
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        /* Call the Protocol Configuration handler for the Alternate 
         * Master option */
        MEMSET (&PtpAltMstInfo, 0, sizeof (tPtpAltMstInfo));

        PtpAltMstInfo.bAltMCSync = (BOOL1) i4SetValFsPtpPortAltMulcastSync;
        PtpAltMstInfo.u1Flag = PTP_AM_UPDATE_MUL_SYNC;

        if ((PtpAlMstHandleUpdateConfOpt ((UINT4) i4FsPtpContextId,
                                          (UINT1) i4FsPtpDomainNumber,
                                          (UINT4) i4FsPtpPortIndex,
                                          &PtpAltMstInfo)) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "PtpAlMstHandleUpdateConfOpt failed.\r\n"));
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortAltMulcastSyncInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortAltMulcastSyncInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortAltMulcastSyncInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortAltMulcastSyncInterval)
{
    tPtpAltMstInfo      PtpAltMstInfo;
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        /* Call the Protocol Configuration handler for the Alternate 
         * Master option */
        MEMSET (&PtpAltMstInfo, 0, sizeof (tPtpAltMstInfo));

        PtpAltMstInfo.u1AltMCSyncInterval =
            (UINT1) i4SetValFsPtpPortAltMulcastSyncInterval;
        PtpAltMstInfo.u1Flag = PTP_AM_UPDATE_MUL_SYNC_INTERVAL;

        if ((PtpAlMstHandleUpdateConfOpt ((UINT4) i4FsPtpContextId,
                                          (UINT1) i4FsPtpDomainNumber,
                                          (UINT4) i4FsPtpPortIndex,
                                          &PtpAltMstInfo)) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "PtpAlMstHandleUpdateConfOpt failed.\r\n"));
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortPtpStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortPtpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpPortPtpStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4SetValFsPtpPortPtpStatus)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpPort->PortDs.u1PtpEnabledStatus !=
            (UINT1) i4SetValFsPtpPortPtpStatus)
        {
            pPtpPort->PortDs.u1PtpEnabledStatus =
                (UINT1) i4SetValFsPtpPortPtpStatus;

            if ((pPtpPort->pPtpDomain->pPtpCxt->u1AdminStatus)
                == (UINT1) PTP_ENABLED)
            {
                if (pPtpPort->PortDs.u1PtpEnabledStatus == (UINT1) PTP_ENABLED)
                {
                    PtpIfEnablePort (pPtpPort);
                }
                else
                {
                    PtpIfDisablePort (pPtpPort);
                }
            }
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpPortRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                setValFsPtpPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpPortRowStatus (INT4 i4FsPtpContextId,
                          INT4 i4FsPtpDomainNumber,
                          INT4 i4FsPtpPortIndex,
                          INT4 i4SetValFsPtpPortRowStatus)
{
    tPtpPort           *pPtpPort = NULL;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    switch (i4SetValFsPtpPortRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpPort != NULL)
            {
                if (pPtpPort->u1RowStatus == (UINT1) i4SetValFsPtpPortRowStatus)
                {
                    return (INT1) SNMP_SUCCESS;
                }
                pPtpPort->u1RowStatus = (UINT1) i4SetValFsPtpPortRowStatus;

                if (i4SetValFsPtpPortRowStatus == ACTIVE)
                {
                    /* Add the port to the hash table */
                    PtpDbAddNode (gPtpGlobalInfo.pPortHashTable, pPtpPort,
                                  (UINT1) PTP_PORT_CONFIG_HASH_DATA_SET);
                }
                else
                {
                    /* Delete the port from the hash table */
                    PtpDbDeleteNode (gPtpGlobalInfo.pPortHashTable, pPtpPort,
                                     (UINT1) PTP_PORT_CONFIG_HASH_DATA_SET);
                }
                return (INT1) SNMP_SUCCESS;
            }
            break;
        }
        case CREATE_AND_WAIT:
        {
            if ((pPtpPort == NULL) &&
                ((pPtpPort =
                  PtpIfCreatePort ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpPortIndex)) != NULL))
            {
                pPtpPort->u1RowStatus = (UINT1) NOT_READY;
                pPtpPort->bIsTranPort = OSIX_FALSE;

                /* Update the current no of ports */
                pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.
                    u4CurrentNoOfPorts++;
                return (INT1) SNMP_SUCCESS;
            }
            break;
        }
        case DESTROY:
        {
            if (pPtpPort != NULL)
            {
                PtpIfDelPort (pPtpPort);
            }
            return (INT1) SNMP_SUCCESS;
        }

        default:
            break;
    }

    return (INT1) SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortClockIdentity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortClockIdentity
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     tSNMP_OCTET_STRING_TYPE * pTestValFsPtpPortClockIdentity)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if (pTestValFsPtpPortClockIdentity->i4_Length != PTP_MAX_CLOCK_ID_LEN)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Wrong length is specified for the given clock"
                  " identity\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_LENGTH;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain != NULL)
    {
        if (MEMCMP (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.ClkId,
                    pTestValFsPtpPortClockIdentity->pu1_OctetList,
                    PTP_MAX_CLOCK_ID_LEN) != 0)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Clock identity did not match the clock identity present"
                      " in the domain entry\r\n"));
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_PTP_ERR_CLK_ID_MISMATCH);
            return (INT1) SNMP_FAILURE;
        }

        if (pPtpDomain->ClkMode == PTP_FORWARD_MODE)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Configurations are not allowed in forward mode\r\n"));
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortInterfaceType
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortInterfaceType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpPortInterfaceType (UINT4 *pu4ErrorCode,
                                 INT4 i4FsPtpContextId,
                                 INT4 i4FsPtpDomainNumber,
                                 INT4 i4FsPtpPortIndex,
                                 INT4 i4TestValFsPtpPortInterfaceType)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpPortInterfaceType < PTP_IFACE_UDP_IPV4) ||
        ((i4TestValFsPtpPortInterfaceType != PTP_IFACE_UNKNOWN) &&
         (i4TestValFsPtpPortInterfaceType > PTP_IFACE_VLAN)))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for Interface type\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if ((i4TestValFsPtpPortInterfaceType != PTP_IFACE_VLAN) &&
        (i4TestValFsPtpPortInterfaceType > PTP_IFACE_IEEE_802_3))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "The value is currently not supported\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);
    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Row status is not active\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if ((pPtpPort->u1RowStatus != (UINT1) NOT_IN_SERVICE) &&
            (pPtpPort->u1RowStatus != (UINT1) NOT_READY))
        {
            PTP_TRC (((UINT4) i4FsPtpContextId,
                      (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Row status is not active\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortIfaceNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortIfaceNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpPortIfaceNumber (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpPortIndex,
                               INT4 i4TestValFsPtpPortIfaceNumber)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpPortIfaceNumber < PTP_PORT_MIN_IFACE_NUM) ||
        (i4TestValFsPtpPortIfaceNumber > PTP_PORT_MAX_IFACE_NUM))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for Interface Number\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Row status is not active\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }
    else
    {
        if ((pPtpPort->u1RowStatus != (UINT1) NOT_IN_SERVICE) &&
            (pPtpPort->u1RowStatus != (UINT1) NOT_READY))
        {
            PTP_TRC (((UINT4) i4FsPtpContextId,
                      (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Row status is not active\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortMinDelayReqInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortMinDelayReqInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortMinDelayReqInterval
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4TestValFsPtpPortMinDelayReqInterval)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortMinDelayReqInterval < PTP_PORT_MIN_DELAY_REQ_INT) ||
        (i4TestValFsPtpPortMinDelayReqInterval > PTP_PORT_MAX_DELAY_REQ_INT))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for minimum delay request\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortAnnounceInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortAnnounceInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortAnnounceInterval
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortAnnounceInterval)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortAnnounceInterval < PTP_PORT_MIN_ANNOUNCE_INTERVAL) ||
        (i4TestValFsPtpPortAnnounceInterval > PTP_PORT_MAX_ANNOUNCE_INTERVAL))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for port announce interval\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortAnnounceReceiptTimeout
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortAnnounceReceiptTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortAnnounceReceiptTimeout
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortAnnounceReceiptTimeout)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortAnnounceReceiptTimeout <
         PTP_PORT_MIN_ANNOUNCE_RECEIPT_TIMEOUT) ||
        (i4TestValFsPtpPortAnnounceReceiptTimeout >
         PTP_PORT_MAX_ANNOUNCE_RECEIPT_TIMEOUT))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for announce receipt interval\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }
    else
    {
        if (i4TestValFsPtpPortAnnounceReceiptTimeout ==
            PTP_PORT_NON_RECOM_ANN_INTERVAL)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                      "Warning : Configuring announce interval as 2 is "
                      "not recommended\r\n"));
        }
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortSyncInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortSyncInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortSyncInterval
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortSyncInterval)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortSyncInterval < PTP_PORT_MIN_SYNC_TIMEOUT) ||
        (i4TestValFsPtpPortSyncInterval > PTP_PORT_MAX_SYNC_TIMEOUT))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for port sync interval\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port entry does not exists\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortSynclimit
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortSynclimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpPortSynclimit (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
                             INT4 i4FsPtpDomainNumber,
                             INT4 i4FsPtpPortIndex,
                             tSNMP_OCTET_STRING_TYPE
                             * pTestValFsPtpPortSynclimit)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if (pTestValFsPtpPortSynclimit->i4_Length >= PTP_U8_STR_LEN)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for sync limit\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortDelayMechanism
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortDelayMechanism
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpPortDelayMechanism (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPtpContextId,
                                  INT4 i4FsPtpDomainNumber,
                                  INT4 i4FsPtpPortIndex,
                                  INT4 i4TestValFsPtpPortDelayMechanism)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortDelayMechanism != PTP_PORT_DELAY_MECH_END_TO_END) &&
        (i4TestValFsPtpPortDelayMechanism != PTP_PORT_DELAY_MECH_PEER_TO_PEER)
        && (i4TestValFsPtpPortDelayMechanism != PTP_PORT_DELAY_MECH_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for port delay mechanism\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);
    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_TRANSPARENT_CLOCK_MODE))
    {
        if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                        (UINT1) i4FsPtpDomainNumber,
                                        (UINT4) i4FsPtpPortIndex,
                                        OSIX_TRUE,
                                        pu4ErrorCode) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Port config data set does not exist\r\n"));
            return (INT1) SNMP_FAILURE;
        }
    }
    else
    {
        if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                        (UINT1) i4FsPtpDomainNumber,
                                        (UINT4) i4FsPtpPortIndex,
                                        OSIX_FALSE,
                                        pu4ErrorCode) == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Port config data set does not exist\r\n"));
            return (INT1) SNMP_FAILURE;
        }
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortMinPdelayReqInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortMinPdelayReqInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortMinPdelayReqInterval
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortMinPdelayReqInterval)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortMinPdelayReqInterval <
         PTP_PORT_MIN_PEER_DELAY_REQ_INT) ||
        (i4TestValFsPtpPortMinPdelayReqInterval >
         PTP_PORT_MAX_PEER_DELAY_REQ_INT))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for port delay request interval\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortVersionNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortVersionNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortVersionNumber
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortVersionNumber)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortVersionNumber != PTP_VERSION_ONE) &&
        (i4TestValFsPtpPortVersionNumber != PTP_VERSION_TWO))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for PTP version\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }
    else
    {
        pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                           (UINT1) i4FsPtpDomainNumber);
        if (pPtpDomain == NULL)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId,
                      (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Get Domain entry failed\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_PTP_ERR_DOMAIN_NOT_PRESENT);
            return (INT1) SNMP_FAILURE;
        }

        if (i4TestValFsPtpPortVersionNumber == PTP_VERSION_ONE)
        {
            if (pPtpDomain->ClkMode != (UINT4) PTP_BOUNDARY_CLOCK_MODE)
            {
                PTP_TRC (((UINT4) i4FsPtpContextId,
                          (UINT1) i4FsPtpDomainNumber,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "The clock should be in boundary mode\r\n"));
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_PTP_ERR_PORT_VERSION_ONE);
                return (INT1) SNMP_FAILURE;
            }
        }

        if (pPtpDomain->ClkMode == PTP_FORWARD_MODE)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Configurations are not allowed in forward mode\r\n"));
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortUnicastNegOption
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortUnicastNegOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortUnicastNegOption
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortUnicastNegOption)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortUnicastNegOption != PTP_ENABLED) &&
        (i4TestValFsPtpPortUnicastNegOption != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid truth value for PTP Unicast negotiation\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortUnicastMasterMaxSize
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortUnicastMasterMaxSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortUnicastMasterMaxSize
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortUnicastMasterMaxSize)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortUnicastMasterMaxSize < PTP_PORT_MIN_MASTER_SIZE) ||
        (i4TestValFsPtpPortUnicastMasterMaxSize > PTP_PORT_MAX_MASTER_SIZE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for PTP Unicast master max size\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortAccMasterEnabled
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortAccMasterEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpPortAccMasterEnabled (UINT4 *pu4ErrorCode,
                                    INT4 i4FsPtpContextId,
                                    INT4 i4FsPtpDomainNumber,
                                    INT4 i4FsPtpPortIndex,
                                    INT4 i4TestValFsPtpPortAccMasterEnabled)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortAccMasterEnabled != PTP_ENABLED) &&
        (i4TestValFsPtpPortAccMasterEnabled != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid truth value for Port acc master\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortNumOfAltMaster
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortNumOfAltMaster
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortNumOfAltMaster
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortNumOfAltMaster)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortNumOfAltMaster < PTP_PORT_MIN_ALT_MASTER) ||
        (i4TestValFsPtpPortNumOfAltMaster > PTP_PORT_MAX_ALT_MASTER))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for number of alternate master\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortAltMulcastSync
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortAltMulcastSync
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpPortAltMulcastSync (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPtpContextId,
                                  INT4 i4FsPtpDomainNumber,
                                  INT4 i4FsPtpPortIndex,
                                  INT4 i4TestValFsPtpPortAltMulcastSync)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    if ((i4TestValFsPtpPortAltMulcastSync != PTP_ENABLED) &&
        (i4TestValFsPtpPortAltMulcastSync != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid truth value for Alternate master"
                  " multicast sync-up\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    if (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortAltMulcastSyncInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortAltMulcastSyncInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortAltMulcastSyncInterval
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortAltMulcastSyncInterval)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpPortAltMulcastSyncInterval < PTP_PORT_MIN_ALT_MASTER) ||
        (i4TestValFsPtpPortAltMulcastSyncInterval > PTP_PORT_MAX_ALT_MASTER))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for multicast sync interval with"
                  " alternate master flag set to true\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    if ((pPtpPort != NULL)
        && (pPtpPort->pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Configurations are not allowed in forward mode\r\n"));
        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortPtpStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortPtpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortPtpStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortPtpStatus)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpPortPtpStatus != PTP_DISABLED) &&
        (i4TestValFsPtpPortPtpStatus != PTP_ENABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid truth value Port Status\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpPortIndex,
                                    OSIX_FALSE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpPortRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex

                The Object 
                testValFsPtpPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpPortRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4TestValFsPtpPortRowStatus)
{
    tPtpPort           *pPtpPort = NULL;
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        return (INT1) SNMP_FAILURE;
    }

    if (i4TestValFsPtpPortRowStatus == DESTROY)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if ((i4FsPtpContextId > PTP_MAX_CONTEXT_ID) ||
        (i4FsPtpDomainNumber >= PTP_MAX_DOMAINS) ||
        (i4FsPtpPortIndex > PTP_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Domain does not exists\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpPortIndex);

    switch (i4TestValFsPtpPortRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if ((pPtpPort == NULL) || (pPtpPort->bIsTranPort == OSIX_TRUE))
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                return (INT1) SNMP_FAILURE;
            }

            if (((i4TestValFsPtpPortRowStatus == ACTIVE) &&
                 (PtpPortValidateIfaceTypeAndNum
                  ((UINT4) i4FsPtpContextId, pPtpPort->PtpDeviceType,
                   pPtpPort->u4IfIndex) == OSIX_FAILURE)))
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                return (INT1) SNMP_FAILURE;
            }

            /* If the node is to be set to active, check if there is a
             * similar entry with same device type and interface index
             */
            if (i4TestValFsPtpPortRowStatus == ACTIVE)
            {
                if (PtpIfGetPortFromIfTypeAndIndex
                    ((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                     pPtpPort->u4IfIndex, pPtpPort->PtpDeviceType) != NULL)
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                    return (INT1) SNMP_FAILURE;
                }
            }

            break;
        }
        case CREATE_AND_GO:
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
        case CREATE_AND_WAIT:
        {
            if ((pPtpPort != NULL) && (pPtpPort->bIsTranPort == OSIX_FALSE))
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
                return (INT1) SNMP_FAILURE;
            }

            /* If the maximum no of ports allowed in clock is reached, return
             * failure
             */
            if (pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u4CurrentNoOfPorts ==
                pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u4NumberOfPorts)
            {
                PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Maximum number of ports reached\r\n"));
                CLI_SET_ERR (CLI_PTP_ERR_MAX_CLK_PORTS);
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                return (INT1) SNMP_FAILURE;
            }
            break;
        }

        default:
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpPortConfigDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPtpPortConfigDataSetTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpForeignMasterDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpForeignMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpForeignMasterClockIdentity
                FsPtpForeignMasterPortIndex
                FsPtpPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpForeignMasterDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     tSNMP_OCTET_STRING_TYPE * pFsPtpForeignMasterClockIdentity,
     INT4 i4FsPtpForeignMasterPortIndex, INT4 i4FsPtpPortIndex)
{
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;
    tPtpForeignMasterDs PtpForeignMasterDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    MEMSET (&PtpForeignMasterDs, 0, sizeof (tPtpForeignMasterDs));

    PtpForeignMasterDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpForeignMasterDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpForeignMasterDs.ClkId,
            pFsPtpForeignMasterClockIdentity->pu1_OctetList,
            PTP_MAX_CLOCK_ID_LEN);
    PtpForeignMasterDs.u4SrcPortIndex = (UINT4) i4FsPtpForeignMasterPortIndex;
    PtpForeignMasterDs.u4PortIndex = (UINT4) i4FsPtpPortIndex;

    pPtpForeignMasterDs = (tPtpForeignMasterDs *)
        PtpDbGetNode (gPtpGlobalInfo.FMTree,
                      &(PtpForeignMasterDs),
                      (UINT1) PTP_FOREIGN_MASTER_DATA_SET);

    if (pPtpForeignMasterDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Foreign master data set does not exists\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpForeignMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpForeignMasterClockIdentity
                FsPtpForeignMasterPortIndex
                FsPtpPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpForeignMasterDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber,
     tSNMP_OCTET_STRING_TYPE * pFsPtpForeignMasterClockIdentity,
     INT4 *pi4FsPtpForeignMasterPortIndex, INT4 *pi4FsPtpPortIndex)
{
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;

    }

    pPtpForeignMasterDs = (tPtpForeignMasterDs *)
        PtpDbGetFirstNode (gPtpGlobalInfo.FMTree, (UINT1)
                           PTP_FOREIGN_MASTER_DATA_SET);

    if (pPtpForeignMasterDs == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Foreign master data set does not exists\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4FsPtpContextId = (INT4) pPtpForeignMasterDs->u4ContextId;
        *pi4FsPtpDomainNumber = (INT4) pPtpForeignMasterDs->u1DomainId;
        MEMCPY (pFsPtpForeignMasterClockIdentity->pu1_OctetList,
                pPtpForeignMasterDs->ClkId, PTP_MAX_CLOCK_ID_LEN);
        pFsPtpForeignMasterClockIdentity->i4_Length =
            sizeof (pPtpForeignMasterDs->ClkId);
        *pi4FsPtpForeignMasterPortIndex =
            (INT4) pPtpForeignMasterDs->u4SrcPortIndex;
        *pi4FsPtpPortIndex = (INT4) pPtpForeignMasterDs->u4PortIndex;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpForeignMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
                FsPtpForeignMasterClockIdentity
                nextFsPtpForeignMasterClockIdentity
                FsPtpForeignMasterPortIndex
                nextFsPtpForeignMasterPortIndex
                FsPtpPortIndex
                nextFsPtpPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpForeignMasterDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber,
     tSNMP_OCTET_STRING_TYPE * pFsPtpForeignMasterClockIdentity,
     tSNMP_OCTET_STRING_TYPE * pNextFsPtpForeignMasterClockIdentity,
     INT4 i4FsPtpForeignMasterPortIndex,
     INT4 *pi4NextFsPtpForeignMasterPortIndex,
     INT4 i4FsPtpPortIndex, INT4 *pi4NextFsPtpPortIndex)
{
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;
    tPtpForeignMasterDs PtpForeignMasterDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    MEMSET (&PtpForeignMasterDs, 0, sizeof (tPtpForeignMasterDs));

    PtpForeignMasterDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpForeignMasterDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpForeignMasterDs.ClkId,
            pFsPtpForeignMasterClockIdentity->pu1_OctetList,
            PTP_MAX_CLOCK_ID_LEN);
    PtpForeignMasterDs.u4SrcPortIndex = (UINT4) i4FsPtpForeignMasterPortIndex;
    PtpForeignMasterDs.u4PortIndex = (UINT4) i4FsPtpPortIndex;

    pPtpForeignMasterDs = (tPtpForeignMasterDs *)
        PtpDbGetNextNode (gPtpGlobalInfo.FMTree,
                          &(PtpForeignMasterDs),
                          (UINT1) PTP_FOREIGN_MASTER_DATA_SET);

    if (pPtpForeignMasterDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Foreign master data set does not exists\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4NextFsPtpContextId = (INT4) pPtpForeignMasterDs->u4ContextId;
        *pi4NextFsPtpDomainNumber = (INT4) pPtpForeignMasterDs->u1DomainId;
        MEMCPY (pNextFsPtpForeignMasterClockIdentity->pu1_OctetList,
                pPtpForeignMasterDs->ClkId, PTP_MAX_CLOCK_ID_LEN);
        pNextFsPtpForeignMasterClockIdentity->i4_Length =
            sizeof (pPtpForeignMasterDs->ClkId);
        *pi4NextFsPtpForeignMasterPortIndex =
            (INT4) pPtpForeignMasterDs->u4SrcPortIndex;
        *pi4NextFsPtpPortIndex = (INT4) pPtpForeignMasterDs->u4PortIndex;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpForeignMasterAnnounceMsgs
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpForeignMasterClockIdentity
                FsPtpForeignMasterPortIndex
                FsPtpPortIndex

                The Object 
                retValFsPtpForeignMasterAnnounceMsgs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpForeignMasterAnnounceMsgs
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     tSNMP_OCTET_STRING_TYPE * pFsPtpForeignMasterClockIdentity,
     INT4 i4FsPtpForeignMasterPortIndex, INT4 i4FsPtpPortIndex,
     INT4 *pi4RetValFsPtpForeignMasterAnnounceMsgs)
{
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;
    tPtpForeignMasterDs PtpForeignMasterDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpForeignMasterDs, 0, sizeof (tPtpForeignMasterDs));

    PtpForeignMasterDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpForeignMasterDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpForeignMasterDs.ClkId,
            pFsPtpForeignMasterClockIdentity->pu1_OctetList,
            PTP_MAX_CLOCK_ID_LEN);
    PtpForeignMasterDs.u4SrcPortIndex = (UINT4) i4FsPtpForeignMasterPortIndex;
    PtpForeignMasterDs.u4PortIndex = (UINT4) i4FsPtpPortIndex;

    pPtpForeignMasterDs = (tPtpForeignMasterDs *)
        PtpDbGetNode (gPtpGlobalInfo.FMTree,
                      &(PtpForeignMasterDs),
                      (UINT1) PTP_FOREIGN_MASTER_DATA_SET);

    if (pPtpForeignMasterDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Foreign master data set does not exists\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpForeignMasterAnnounceMsgs =
            (INT4) pPtpForeignMasterDs->u4AnnounceMsgsCnt;
    }

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsPtpTransparentDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpTransparentDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpTransparentDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if ((pPtpDomain == NULL) ||
        (pPtpDomain->ClkMode != PTP_TRANSPARENT_CLOCK_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist or the clock mode is "
                  "neither ordinary nor boundary\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpTransparentDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpTransparentDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;
    UINT4               u4PtpContextId = 0;
    UINT1               u1PtpDomainNumber = 0;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    pPtpDomain = (tPtpDomain *)
        PtpDbGetFirstNode (gPtpGlobalInfo.DomainTree,
                           (UINT1) PTP_DOMAIN_DATA_SET);

    if (pPtpDomain == NULL)
    {
        return (INT1) SNMP_FAILURE;
    }

    if (pPtpDomain->ClkMode == PTP_TRANSPARENT_CLOCK_MODE)
    {
        *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
        return (INT1) SNMP_SUCCESS;
    }

    u4PtpContextId = pPtpDomain->pPtpCxt->u4ContextId;
    u1PtpDomainNumber = pPtpDomain->u1DomainId;

    while ((pPtpDomain = PtpDmnGetNextDomainEntry (u4PtpContextId,
                                                   u1PtpDomainNumber)) != NULL)
    {
        if (pPtpDomain->ClkMode == PTP_TRANSPARENT_CLOCK_MODE)
        {
            *pi4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
            *pi4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
            return (INT1) SNMP_SUCCESS;
        }

        u4PtpContextId = pPtpDomain->pPtpCxt->u4ContextId;
        u1PtpDomainNumber = pPtpDomain->u1DomainId;
    }

    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpTransparentDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpTransparentDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    while ((pPtpDomain = PtpDmnGetNextDomainEntry ((UINT4) i4FsPtpContextId,
                                                   (UINT1) i4FsPtpDomainNumber))
           != NULL)
    {
        if (pPtpDomain->ClkMode == PTP_TRANSPARENT_CLOCK_MODE)
        {
            *pi4NextFsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
            *pi4NextFsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
            return (INT1) SNMP_SUCCESS;
        }
        i4FsPtpContextId = (INT4) pPtpDomain->pPtpCxt->u4ContextId;
        i4FsPtpDomainNumber = (INT4) pPtpDomain->u1DomainId;
    }

    return (INT1) SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTransparentClockIdentity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentClockIdentity
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPtpTransparentClockIdentity)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpTransparentClockIdentity->i4_Length = PTP_MAX_CLOCK_ID_LEN;
        MEMCPY (pRetValFsPtpTransparentClockIdentity->pu1_OctetList,
                pPtpDomain->ClockDs.TransClkDs.ClkId, PTP_MAX_CLOCK_ID_LEN);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentClockTwoStepFlag
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTransparentClockTwoStepFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentClockTwoStepFlag
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpTransparentClockTwoStepFlag)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTransparentClockTwoStepFlag =
            (INT4) pPtpDomain->ClockDs.TransClkDs.bTwoStepFlag;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentClockNumberPorts
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTransparentClockNumberPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentClockNumberPorts
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpTransparentClockNumberPorts)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTransparentClockNumberPorts =
            (INT4) pPtpDomain->ClockDs.TransClkDs.u4NumberOfPorts;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentClockDelaymechanism
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                retValFsPtpTransparentClockDelaymechanism
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentClockDelaymechanism
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpTransparentClockDelaymechanism)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTransparentClockDelaymechanism =
            pPtpDomain->ClockDs.TransClkDs.TransClkType;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentClockPrimaryDomain
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object
                retValFsPtpTransparentClockPrimaryDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentClockPrimaryDomain
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 *pi4RetValFsPtpTransparentClockPrimaryDomain)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTransparentClockPrimaryDomain =
            pPtpDomain->ClockDs.TransClkDs.u1PrimaryDomainId;
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpTransparentClockDelaymechanism
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpTransparentClockDelaymechanism
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpTransparentClockDelaymechanism
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4SetValFsPtpTransparentClockDelaymechanism)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->ClockDs.TransClkDs.TransClkType !=
            (UINT4) i4SetValFsPtpTransparentClockDelaymechanism)
        {
            pPtpDomain->ClockDs.TransClkDs.TransClkType
                = i4SetValFsPtpTransparentClockDelaymechanism;

            /* The delay mechanism can be peer to peer or end to end.
             * Depending on the delay mechanism, stop or start the peer
             * delay request timer
             */
            PtpIfDelayMechanismChg (pPtpDomain);
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpTransparentClockPrimaryDomain
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object
                setValFsPtpTransparentClockPrimaryDomain
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpTransparentClockPrimaryDomain
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4SetValFsPtpTransparentClockPrimaryDomain)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->ClockDs.TransClkDs.u1PrimaryDomainId !=
            (UINT1) i4SetValFsPtpTransparentClockPrimaryDomain)
        {
            pPtpDomain->ClockDs.TransClkDs.TransClkType
                = (UINT1) i4SetValFsPtpTransparentClockPrimaryDomain;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpTransparentClockTwoStepFlag
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpTransparentClockTwoStepFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpTransparentClockTwoStepFlag
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4SetValFsPtpTransparentClockTwoStepFlag)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->ClockDs.TransClkDs.bTwoStepFlag !=
            (BOOL1) i4SetValFsPtpTransparentClockTwoStepFlag)
        {
            pPtpDomain->ClockDs.TransClkDs.bTwoStepFlag =
                (BOOL1) i4SetValFsPtpTransparentClockTwoStepFlag;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpTransparentClockNumberPorts
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                setValFsPtpTransparentClockNumberPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpTransparentClockNumberPorts
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4SetValFsPtpTransparentClockNumberPorts)
{
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpDomain->ClockDs.TransClkDs.u4NumberOfPorts !=
            (UINT4) i4SetValFsPtpTransparentClockNumberPorts)
        {
            pPtpDomain->ClockDs.TransClkDs.u4NumberOfPorts =
                (UINT4) i4SetValFsPtpTransparentClockNumberPorts;
        }
    }

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpTransparentClockTwoStepFlag
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpTransparentClockTwoStepFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpTransparentClockTwoStepFlag
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4TestValFsPtpTransparentClockTwoStepFlag)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpTransparentClockTwoStepFlag != PTP_ENABLED) &&
        (i4TestValFsPtpTransparentClockTwoStepFlag != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for two step flag\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if ((pPtpDomain == NULL) ||
        (pPtpDomain->u1RowStatus != (UINT1) NOT_IN_SERVICE) ||
        (pPtpDomain->ClkMode != PTP_TRANSPARENT_CLOCK_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId,
                  (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Row status should be de-activated\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PTP_ERR_NOT_TRANSPARENT_CLK);
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpTransparentClockNumberPorts
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpTransparentClockNumberPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpTransparentClockNumberPorts
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4TestValFsPtpTransparentClockNumberPorts)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    UNUSED_PARAM (i4TestValFsPtpTransparentClockNumberPorts);
    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if ((pPtpDomain == NULL) ||
        (pPtpDomain->ClkMode != PTP_TRANSPARENT_CLOCK_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId,
                  (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Row status should be de-activated\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PTP_ERR_NOT_TRANSPARENT_CLK);
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpTransparentClockDelaymechanism
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object 
                testValFsPtpTransparentClockDelaymechanism
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpTransparentClockDelaymechanism
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber,
     INT4 i4TestValFsPtpTransparentClockDelaymechanism)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpTransparentClockDelaymechanism !=
         PTP_PORT_DELAY_MECH_END_TO_END) &&
        (i4TestValFsPtpTransparentClockDelaymechanism !=
         PTP_PORT_DELAY_MECH_PEER_TO_PEER))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for port delay mechanism\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if ((pPtpDomain == NULL) ||
        (pPtpDomain->ClkMode != PTP_TRANSPARENT_CLOCK_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId,
                  (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Row status should be de-activated\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PTP_ERR_NOT_TRANSPARENT_CLK);
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpTransparentClockPrimaryDomain
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber

                The Object
                testValFsPtpTransparentClockPrimaryDomain
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpTransparentClockPrimaryDomain
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4TestValFsPtpTransparentClockPrimaryDomain)
{
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpTransparentClockPrimaryDomain < 0) ||
        (i4TestValFsPtpTransparentClockPrimaryDomain >= PTP_MAX_DOMAINS))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for port delay mechanism\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidateDomainDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Context Id : %d Domain Id %d is not valid\r\n",
                  i4FsPtpContextId, i4FsPtpDomainNumber));
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if ((pPtpDomain == NULL) ||
        (pPtpDomain->ClkMode != PTP_TRANSPARENT_CLOCK_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId,
                  (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Row status should be de-activated\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PTP_ERR_NOT_TRANSPARENT_CLK);
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpTransparentDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsPtpTransparentDataSetTable
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpTransparentPortDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpTransparentPortDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpTransparentPortDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpTransparentPortIndex);
    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpTransparentPortDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpTransparentPortDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber,
     INT4 *pi4FsPtpTransparentPortIndex)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    pPtpPort = (tPtpPort *)
        PtpDbGetFirstNode (gPtpGlobalInfo.PortTree,
                           (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET);

    while (pPtpPort != NULL)
    {
        if (pPtpPort->bIsTranPort == OSIX_TRUE)
        {
            *pi4FsPtpContextId = (INT4) pPtpPort->pPtpDomain->pPtpCxt->
                u4ContextId;
            *pi4FsPtpDomainNumber = (INT4) pPtpPort->pPtpDomain->u1DomainId;
            *pi4FsPtpTransparentPortIndex =
                (INT4) pPtpPort->PortDs.u4PtpPortNumber;
            return (INT1) SNMP_SUCCESS;
        }

        pPtpPort = (tPtpPort *)
            PtpDbGetNextNode (gPtpGlobalInfo.PortTree,
                              pPtpPort,
                              (UINT1) PTP_PORT_CONFIG_RBTREE_DATA_SET);
    }
    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
              MGMT_TRC | ALL_FAILURE_TRC,
              "Port config data set does not exist\r\n"));

    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpTransparentPortDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
                FsPtpTransparentPortIndex
                nextFsPtpTransparentPortIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpTransparentPortDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex, INT4 *pi4NextFsPtpTransparentPortIndex)
{
    tPtpPort           *pPtpPort = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;

    }

    while ((pPtpPort =
            PtpIfGetNextPortEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpTransparentPortIndex)) !=
           NULL)
    {
        if (pPtpPort->bIsTranPort == OSIX_TRUE)
        {
            *pi4NextFsPtpContextId = (INT4) pPtpPort->pPtpDomain->pPtpCxt->
                u4ContextId;
            *pi4NextFsPtpDomainNumber = (INT4) pPtpPort->pPtpDomain->u1DomainId;
            *pi4NextFsPtpTransparentPortIndex =
                (INT4) pPtpPort->PortDs.u4PtpPortNumber;
            return (INT1) SNMP_SUCCESS;
        }
        i4FsPtpTransparentPortIndex = (INT4) pPtpPort->PortDs.u4PtpPortNumber;
        i4FsPtpDomainNumber = (INT4) pPtpPort->pPtpDomain->u1DomainId;
        i4FsPtpContextId = (INT4) pPtpPort->pPtpDomain->pPtpCxt->u4ContextId;
    }
    PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
              MGMT_TRC | ALL_FAILURE_TRC,
              "Port config data set does not exist\r\n"));

    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentPortInterfaceType
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                retValFsPtpTransparentPortInterfaceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentPortInterfaceType
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     INT4 *pi4RetValFsPtpTransparentPortInterfaceType)
{
    return nmhGetFsPtpPortInterfaceType
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex,
         pi4RetValFsPtpTransparentPortInterfaceType);
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentPortIfaceNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                retValFsPtpTransparentPortIfaceNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentPortIfaceNumber
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     INT4 *pi4RetValFsPtpTransparentPortIfaceNumber)
{
    return nmhGetFsPtpPortIfaceNumber
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex, pi4RetValFsPtpTransparentPortIfaceNumber);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentPortClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                retValFsPtpTransparentPortClockIdentity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentPortClockIdentity
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPtpTransparentPortClockIdentity)
{
    return nmhGetFsPtpPortClockIdentity
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex, pRetValFsPtpTransparentPortClockIdentity);
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentPortMinPdelayReqInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                retValFsPtpTransparentPortMinPdelayReqInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentPortMinPdelayReqInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     INT4 *pi4RetValFsPtpTransparentPortMinPdelayReqInterval)
{
    return nmhGetFsPtpPortMinPdelayReqInterval
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex,
         pi4RetValFsPtpTransparentPortMinPdelayReqInterval);
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentPortFaultyFlag
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                retValFsPtpTransparentPortFaultyFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentPortFaultyFlag
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     INT4 *pi4RetValFsPtpTransparentPortFaultyFlag)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpTransparentPortIndex);

    if (pPtpPort == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpTransparentPortFaultyFlag =
            (INT4) pPtpPort->bPortFaultyFlag;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentPortPeerMeanPathDelay
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                retValFsPtpTransparentPortPeerMeanPathDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentPortPeerMeanPathDelay
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPtpTransparentPortPeerMeanPathDelay)
{
    return nmhGetFsPtpPortPeerMeanPathDelay
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex,
         pRetValFsPtpTransparentPortPeerMeanPathDelay);
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentPortPtpStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                retValFsPtpTransparentPortPtpStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpTransparentPortPtpStatus (INT4 i4FsPtpContextId,
                                     INT4 i4FsPtpDomainNumber,
                                     INT4 i4FsPtpTransparentPortIndex,
                                     INT4
                                     *pi4RetValFsPtpTransparentPortPtpStatus)
{
    return nmhGetFsPtpPortPtpStatus
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex, pi4RetValFsPtpTransparentPortPtpStatus);
}

/****************************************************************************
 Function    :  nmhGetFsPtpTransparentPortRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                retValFsPtpTransparentPortRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpTransparentPortRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     INT4 *pi4RetValFsPtpTransparentPortRowStatus)
{
    return nmhGetFsPtpPortRowStatus
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex, pi4RetValFsPtpTransparentPortRowStatus);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpTransparentPortInterfaceType
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                setValFsPtpTransparentPortInterfaceType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpTransparentPortInterfaceType
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     INT4 i4SetValFsPtpTransparentPortInterfaceType)
{
    return nmhSetFsPtpPortInterfaceType
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex,
         i4SetValFsPtpTransparentPortInterfaceType);
}

/****************************************************************************
 Function    :  nmhSetFsPtpTransparentPortIfaceNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                setValFsPtpTransparentPortIfaceNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpTransparentPortIfaceNumber
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     INT4 i4SetValFsPtpTransparentPortIfaceNumber)
{
    return nmhSetFsPtpPortIfaceNumber
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex, i4SetValFsPtpTransparentPortIfaceNumber);
}

/****************************************************************************
 Function    :  nmhSetFsPtpTransparentPortClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                setValFsPtpTransparentPortClockIdentity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpTransparentPortClockIdentity
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     tSNMP_OCTET_STRING_TYPE * pSetValFsPtpTransparentPortClockIdentity)
{
    return nmhSetFsPtpPortClockIdentity
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex, pSetValFsPtpTransparentPortClockIdentity);
}

/****************************************************************************
 Function    :  nmhSetFsPtpTransparentPortMinPdelayReqInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                setValFsPtpTransparentPortMinPdelayReqInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpTransparentPortMinPdelayReqInterval
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     INT4 i4SetValFsPtpTransparentPortMinPdelayReqInterval)
{
    return nmhSetFsPtpPortMinPdelayReqInterval
        (i4FsPtpContextId, i4FsPtpDomainNumber,
         i4FsPtpTransparentPortIndex,
         i4SetValFsPtpTransparentPortMinPdelayReqInterval);
}

/****************************************************************************
 Function    :  nmhSetFsPtpTransparentPortPtpStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                setValFsPtpTransparentPortPtpStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpTransparentPortPtpStatus (INT4 i4FsPtpContextId,
                                     INT4 i4FsPtpDomainNumber,
                                     INT4 i4FsPtpTransparentPortIndex,
                                     INT4 i4SetValFsPtpTransparentPortPtpStatus)
{
    return (nmhSetFsPtpPortPtpStatus (i4FsPtpContextId, i4FsPtpDomainNumber,
                                      i4FsPtpTransparentPortIndex,
                                      i4SetValFsPtpTransparentPortPtpStatus));
}

/****************************************************************************
 Function    :  nmhSetFsPtpTransparentPortRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                setValFsPtpTransparentPortRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpTransparentPortRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpTransparentPortIndex,
     INT4 i4SetValFsPtpTransparentPortRowStatus)
{
    tPtpPort           *pPtpPort = NULL;

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpTransparentPortIndex);

    switch (i4SetValFsPtpTransparentPortRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpPort != NULL)
            {
                if ((UINT1) i4SetValFsPtpTransparentPortRowStatus ==
                    pPtpPort->u1RowStatus)
                {
                    return (INT1) SNMP_SUCCESS;
                }

                pPtpPort->u1RowStatus =
                    (UINT1) i4SetValFsPtpTransparentPortRowStatus;

                if (i4SetValFsPtpTransparentPortRowStatus == ACTIVE)
                {
                    /* Add the port to the hash table */
                    PtpDbAddNode (gPtpGlobalInfo.pPortHashTable, pPtpPort,
                                  (UINT1) PTP_PORT_CONFIG_HASH_DATA_SET);
                }
                else
                {
                    /* Delete the port from the hash table */
                    PtpDbDeleteNode (gPtpGlobalInfo.pPortHashTable, pPtpPort,
                                     (UINT1) PTP_PORT_CONFIG_HASH_DATA_SET);
                }

                return (INT1) SNMP_SUCCESS;
            }

            break;
        }
        case CREATE_AND_WAIT:
        {
            if ((pPtpPort == NULL) &&
                ((pPtpPort =
                  PtpIfCreatePort ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpTransparentPortIndex))
                 != NULL))
            {
                pPtpPort->u1RowStatus = (UINT1) NOT_READY;
                pPtpPort->bIsTranPort = OSIX_TRUE;

                /* Update the current no of ports */
                pPtpPort->pPtpDomain->ClockDs.TransClkDs.u4CurrentNoOfPorts++;
                return (INT1) SNMP_SUCCESS;
            }
            break;
        }
        case DESTROY:
        {
            if (pPtpPort != NULL)
            {
                PtpIfDelPort (pPtpPort);
            }
            return (INT1) SNMP_SUCCESS;
        }

        default:
            break;
    }

    return (INT1) SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpTransparentPortInterfaceType
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                testValFsPtpTransparentPortInterfaceType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpTransparentPortInterfaceType
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpTransparentPortIndex,
     INT4 i4TestValFsPtpTransparentPortInterfaceType)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpTransparentPortInterfaceType < PTP_IFACE_UDP_IPV4) ||
        ((i4TestValFsPtpTransparentPortInterfaceType != PTP_IFACE_UNKNOWN) &&
         (i4TestValFsPtpTransparentPortInterfaceType > PTP_IFACE_VLAN)))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for Interface type\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if ((i4TestValFsPtpTransparentPortInterfaceType != PTP_IFACE_VLAN) &&
        (i4TestValFsPtpTransparentPortInterfaceType > PTP_IFACE_IEEE_802_3))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "The value is currently not supported\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpTransparentPortIndex,
                                    OSIX_TRUE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpTransparentPortIndex);

    if ((pPtpPort != NULL) &&
        (pPtpPort->u1RowStatus != (UINT1) NOT_IN_SERVICE) &&
        (pPtpPort->u1RowStatus != (UINT1) NOT_READY))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Row status is not active\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpTransparentPortIfaceNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                testValFsPtpTransparentPortIfaceNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpTransparentPortIfaceNumber
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpTransparentPortIndex,
     INT4 i4TestValFsPtpTransparentPortIfaceNumber)
{
    tPtpPort           *pPtpPort = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpTransparentPortIfaceNumber < PTP_PORT_MIN_IFACE_NUM) ||
        (i4TestValFsPtpTransparentPortIfaceNumber > PTP_PORT_MAX_IFACE_NUM))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for Interface Number\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpTransparentPortIndex,
                                    OSIX_TRUE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpTransparentPortIndex);

    if ((pPtpPort != NULL) &&
        (pPtpPort->u1RowStatus != (UINT1) NOT_IN_SERVICE) &&
        (pPtpPort->u1RowStatus != (UINT1) NOT_READY))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Row status is not active\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpTransparentPortClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                testValFsPtpTransparentPortClockIdentity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpTransparentPortClockIdentity
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpTransparentPortIndex,
     tSNMP_OCTET_STRING_TYPE * pTestValFsPtpTransparentPortClockIdentity)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (pTestValFsPtpTransparentPortClockIdentity);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpTransparentPortIndex,
                                    OSIX_TRUE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpTransparentPortMinPdelayReqInterval
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                testValFsPtpTransparentPortMinPdelayReqInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpTransparentPortMinPdelayReqInterval
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpTransparentPortIndex,
     INT4 i4TestValFsPtpTransparentPortMinPdelayReqInterval)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpTransparentPortMinPdelayReqInterval <
         PTP_PORT_MIN_PEER_DELAY_REQ_INT) ||
        (i4TestValFsPtpTransparentPortMinPdelayReqInterval >
         PTP_PORT_MAX_PEER_DELAY_REQ_INT))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for port delay request interval\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpTransparentPortIndex,
                                    OSIX_TRUE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpTransparentPortPtpStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                testValFsPtpTransparentPortPtpStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpTransparentPortPtpStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsPtpContextId,
                                        INT4 i4FsPtpDomainNumber,
                                        INT4 i4FsPtpTransparentPortIndex,
                                        INT4
                                        i4TestValFsPtpTransparentPortPtpStatus)
{

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;

    }

    if ((i4TestValFsPtpTransparentPortPtpStatus != PTP_ENABLED) &&
        (i4TestValFsPtpTransparentPortPtpStatus != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for port ptp status \r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    if (PtpValValidatePortConfigDS ((UINT4) i4FsPtpContextId,
                                    (UINT1) i4FsPtpDomainNumber,
                                    (UINT4) i4FsPtpTransparentPortIndex,
                                    OSIX_TRUE, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Port config data set does not exist\r\n"));
        return (INT1) SNMP_FAILURE;
    }
    return (INT1) SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpTransparentPortRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex

                The Object 
                testValFsPtpTransparentPortRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpTransparentPortRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpTransparentPortIndex,
     INT4 i4TestValFsPtpTransparentPortRowStatus)
{
    tPtpPort           *pPtpPort = NULL;
    tPtpDomain         *pPtpDomain = NULL;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        return (INT1) SNMP_FAILURE;
    }

    if (i4TestValFsPtpTransparentPortRowStatus == DESTROY)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if ((i4FsPtpContextId > PTP_MAX_CONTEXT_ID) ||
        (i4FsPtpDomainNumber >= PTP_MAX_DOMAINS) ||
        (i4FsPtpTransparentPortIndex > PTP_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Domain does not exists\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    pPtpPort = PtpIfGetPortEntry ((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  (UINT4) i4FsPtpTransparentPortIndex);

    switch (i4TestValFsPtpTransparentPortRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if ((pPtpPort == NULL) || (pPtpPort->bIsTranPort != OSIX_TRUE))
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                return (INT1) SNMP_FAILURE;
            }

            if ((i4TestValFsPtpTransparentPortRowStatus == ACTIVE) &&
                (PtpPortValidateIfaceTypeAndNum
                 ((UINT4) i4FsPtpContextId, pPtpPort->PtpDeviceType,
                  pPtpPort->u4IfIndex) == OSIX_FAILURE))
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                return (INT1) SNMP_FAILURE;
            }

            /* If the node is to be set to active, check if there is a
             * similar entry with same device type and interface index
             */
            if (i4TestValFsPtpTransparentPortRowStatus == ACTIVE)
            {
                if (PtpIfGetPortFromIfTypeAndIndex
                    ((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                     pPtpPort->u4IfIndex, pPtpPort->PtpDeviceType) != NULL)
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                    return (INT1) SNMP_FAILURE;
                }
            }

            break;
        }
        case CREATE_AND_GO:
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            return (INT1) SNMP_FAILURE;
        }
        case CREATE_AND_WAIT:
        {
            if ((pPtpPort != NULL) && (pPtpPort->bIsTranPort == OSIX_TRUE))
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_PTP_ERR_PORT_PRESENT);
                return (INT1) SNMP_FAILURE;
            }

            /* If the maximum no of ports allowed in clock is reached, return
             * failure
             */
            if (pPtpDomain->ClockDs.TransClkDs.u4CurrentNoOfPorts ==
                pPtpDomain->ClockDs.TransClkDs.u4NumberOfPorts)
            {
                PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                          MGMT_TRC | ALL_FAILURE_TRC,
                          "Maximum number of ports reached\r\n"));
                CLI_SET_ERR (CLI_PTP_ERR_MAX_CLK_PORTS);
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                return (INT1) SNMP_FAILURE;
            }
            break;
        }

        default:
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpTransparentPortDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpTransparentPortIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsPtpTransparentPortDataSetTable
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpGrandMasterClusterDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpGrandMasterClusterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpGrandMasterClusterNetworkProtocol
                FsPtpGrandMasterClusterAddLength
                FsPtpGrandMasterClusterAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpGrandMasterClusterDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpGrandMasterClusterNetworkProtocol,
     INT4 i4FsPtpGrandMasterClusterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpGrandMasterClusterAddr)
{
    tPtpGrandMasterTable *pPtpGrandMasterTable = NULL;
    tPtpGrandMasterTable *pTmpPtpGrandMasterTable = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pTmpPtpGrandMasterTable = (tPtpGrandMasterTable *)
        MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);

    if (pTmpPtpGrandMasterTable == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "Memory Allocation failed for Grand master creation\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    pTmpPtpGrandMasterTable->u4ContextId = (UINT4) i4FsPtpContextId;
    pTmpPtpGrandMasterTable->GMAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpGrandMasterClusterNetworkProtocol;
    pTmpPtpGrandMasterTable->GMAddr.u2AddrLength
        = (UINT2) i4FsPtpGrandMasterClusterAddLength;
    pTmpPtpGrandMasterTable->u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (pTmpPtpGrandMasterTable->GMAddr.ai1Addr,
            pFsPtpGrandMasterClusterAddr->pu1_OctetList,
            pTmpPtpGrandMasterTable->GMAddr.u2AddrLength);

    pPtpGrandMasterTable = (tPtpGrandMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.GrandMastClustLst,
                      pTmpPtpGrandMasterTable,
                      (UINT1) PTP_GRAND_MASTER_DATA_SET);

    if (pPtpGrandMasterTable == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                        (UINT1 *) pTmpPtpGrandMasterTable);
    pTmpPtpGrandMasterTable = NULL;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpGrandMasterClusterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpGrandMasterClusterNetworkProtocol
                FsPtpGrandMasterClusterAddLength
                FsPtpGrandMasterClusterAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpGrandMasterClusterDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber,
     INT4 *pi4FsPtpGrandMasterClusterNetworkProtocol,
     INT4 *pi4FsPtpGrandMasterClusterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpGrandMasterClusterAddr)
{
    UNUSED_PARAM (pi4FsPtpContextId);
    UNUSED_PARAM (pi4FsPtpDomainNumber);
    UNUSED_PARAM (pi4FsPtpGrandMasterClusterNetworkProtocol);
    UNUSED_PARAM (pi4FsPtpGrandMasterClusterAddLength);
    UNUSED_PARAM (pFsPtpGrandMasterClusterAddr);
    return (INT1) SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpGrandMasterClusterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
                FsPtpGrandMasterClusterNetworkProtocol
                nextFsPtpGrandMasterClusterNetworkProtocol
                FsPtpGrandMasterClusterAddLength
                nextFsPtpGrandMasterClusterAddLength
                FsPtpGrandMasterClusterAddr
                nextFsPtpGrandMasterClusterAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpGrandMasterClusterDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber,
     INT4 i4FsPtpGrandMasterClusterNetworkProtocol,
     INT4 *pi4NextFsPtpGrandMasterClusterNetworkProtocol,
     INT4 i4FsPtpGrandMasterClusterAddLength,
     INT4 *pi4NextFsPtpGrandMasterClusterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpGrandMasterClusterAddr,
     tSNMP_OCTET_STRING_TYPE * pNextFsPtpGrandMasterClusterAddr)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (pi4NextFsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (pi4NextFsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpGrandMasterClusterNetworkProtocol);
    UNUSED_PARAM (pi4NextFsPtpGrandMasterClusterNetworkProtocol);
    UNUSED_PARAM (i4FsPtpGrandMasterClusterAddLength);
    UNUSED_PARAM (pi4NextFsPtpGrandMasterClusterAddLength);
    UNUSED_PARAM (pFsPtpGrandMasterClusterAddr);
    UNUSED_PARAM (pNextFsPtpGrandMasterClusterAddr);
    return (INT1) SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpGrandMasterClusterRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpGrandMasterClusterNetworkProtocol
                FsPtpGrandMasterClusterAddLength
                FsPtpGrandMasterClusterAddr

                The Object 
                retValFsPtpGrandMasterClusterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpGrandMasterClusterRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpGrandMasterClusterNetworkProtocol,
     INT4 i4FsPtpGrandMasterClusterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpGrandMasterClusterAddr,
     INT4 *pi4RetValFsPtpGrandMasterClusterRowStatus)
{
    tPtpGrandMasterTable *pPtpGrandMasterTable = NULL;
    tPtpGrandMasterTable *pTmpPtpGrandMasterTable = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pTmpPtpGrandMasterTable = (tPtpGrandMasterTable *)
        MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);

    if (pTmpPtpGrandMasterTable == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "Memory Allocation failed for Grand master creation\r\n"));
        return (INT1) SNMP_FAILURE;
    }
    MEMSET (pTmpPtpGrandMasterTable, 0, sizeof (tPtpGrandMasterTable));

    pTmpPtpGrandMasterTable->u4ContextId = (UINT4) i4FsPtpContextId;
    pTmpPtpGrandMasterTable->GMAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpGrandMasterClusterNetworkProtocol;
    pTmpPtpGrandMasterTable->GMAddr.u2AddrLength
        = (UINT2) i4FsPtpGrandMasterClusterAddLength;
    pTmpPtpGrandMasterTable->u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (pTmpPtpGrandMasterTable->GMAddr.ai1Addr,
            pFsPtpGrandMasterClusterAddr->pu1_OctetList,
            pTmpPtpGrandMasterTable->GMAddr.u2AddrLength);

    pPtpGrandMasterTable = (tPtpGrandMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.GrandMastClustLst,
                      pTmpPtpGrandMasterTable,
                      (UINT1) PTP_GRAND_MASTER_DATA_SET);

    MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                        (UINT1 *) pTmpPtpGrandMasterTable);
    pTmpPtpGrandMasterTable = NULL;

    if (pPtpGrandMasterTable == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpGrandMasterClusterRowStatus =
            (INT4) pPtpGrandMasterTable->u1RowStatus;
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpGrandMasterClusterRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpGrandMasterClusterNetworkProtocol
                FsPtpGrandMasterClusterAddLength
                FsPtpGrandMasterClusterAddr

                The Object 
                setValFsPtpGrandMasterClusterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpGrandMasterClusterRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpGrandMasterClusterNetworkProtocol,
     INT4 i4FsPtpGrandMasterClusterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpGrandMasterClusterAddr,
     INT4 i4SetValFsPtpGrandMasterClusterRowStatus)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpGrandMasterClusterNetworkProtocol);
    UNUSED_PARAM (i4FsPtpGrandMasterClusterAddLength);
    UNUSED_PARAM (pFsPtpGrandMasterClusterAddr);
    UNUSED_PARAM (i4SetValFsPtpGrandMasterClusterRowStatus);
    return (INT1) SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpGrandMasterClusterRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpGrandMasterClusterNetworkProtocol
                FsPtpGrandMasterClusterAddLength
                FsPtpGrandMasterClusterAddr

                The Object 
                testValFsPtpGrandMasterClusterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpGrandMasterClusterRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpGrandMasterClusterNetworkProtocol,
     INT4 i4FsPtpGrandMasterClusterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpGrandMasterClusterAddr,
     INT4 i4TestValFsPtpGrandMasterClusterRowStatus)
{
    tPtpGrandMasterTable *pPtpGrandMasterTable = NULL;
    tPtpDomain         *pPtpDomain = NULL;
    tPtpGrandMasterTable *pTmpPtpGrandMasterTable = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (i4TestValFsPtpGrandMasterClusterRowStatus == DESTROY)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if ((i4FsPtpContextId > PTP_MAX_CONTEXT_ID) ||
        (i4FsPtpDomainNumber >= PTP_MAX_DOMAINS) ||
        (i4FsPtpGrandMasterClusterNetworkProtocol >=
         PTP_MAX_NW_PROTOCOL) ||
        (pFsPtpGrandMasterClusterAddr->i4_Length < PTP_MASTER_MIN_ADDR_LEN) ||
        (pFsPtpGrandMasterClusterAddr->i4_Length > PTP_MASTER_MIN_ADDR_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);

    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Domain does not exists\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pTmpPtpGrandMasterTable = (tPtpGrandMasterTable *)
        MemAllocMemBlk (gPtpGlobalInfo.UtilityPoolId);

    if (pTmpPtpGrandMasterTable == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "Memory Allocation failed for Grand master creation\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    MEMSET (pTmpPtpGrandMasterTable, 0, sizeof (tPtpGrandMasterTable));

    pTmpPtpGrandMasterTable->u4ContextId = (UINT4) i4FsPtpContextId;
    pTmpPtpGrandMasterTable->GMAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpGrandMasterClusterNetworkProtocol;
    pTmpPtpGrandMasterTable->GMAddr.u2AddrLength
        = (UINT2) pFsPtpGrandMasterClusterAddr->i4_Length;
    pTmpPtpGrandMasterTable->u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (pTmpPtpGrandMasterTable->GMAddr.ai1Addr,
            pFsPtpGrandMasterClusterAddr->pu1_OctetList,
            pFsPtpGrandMasterClusterAddr->i4_Length);

    pPtpGrandMasterTable = (tPtpGrandMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.GrandMastClustLst,
                      pTmpPtpGrandMasterTable,
                      (UINT1) PTP_GRAND_MASTER_DATA_SET);

    MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                        (UINT1 *) pTmpPtpGrandMasterTable);
    pTmpPtpGrandMasterTable = NULL;

    UNUSED_PARAM (i4FsPtpGrandMasterClusterAddLength);

    switch (i4TestValFsPtpGrandMasterClusterRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpGrandMasterTable == NULL)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if ((i1RetVal == (INT1) SNMP_FAILURE) ||
                (pPtpGrandMasterTable != NULL))
            {
                if (i4TestValFsPtpGrandMasterClusterRowStatus == CREATE_AND_GO)
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                }
                else
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
                }
                CLI_SET_ERR (CLI_PTP_ERR_GRAND_MASTER_PRESENT);
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }

        default:
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    return i1RetVal;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpGrandMasterClusterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpGrandMasterClusterNetworkProtocol
                FsPtpGrandMasterClusterAddLength
                FsPtpGrandMasterClusterAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsPtpGrandMasterClusterDataSetTable
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpUnicastMasterDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpUnicastMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex
                FsPtpUnicastMasterNetworkProtocol
                FsPtpUnicastMasterAddLength
                FsPtpUnicastMasterAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpUnicastMasterDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4FsPtpUnicastMasterNetworkProtocol,
     INT4 i4FsPtpUnicastMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpUnicastMasterAddr)
{
    tPtpAccUnicastMasterTable *pPtpAccUniCastMasterTable = NULL;
    tPtpAccUnicastMasterTable *pTmpPtpAccUniCastMasterTable = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4FsPtpPortIndex);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pTmpPtpAccUniCastMasterTable =
        (tPtpAccUnicastMasterTable *) MemAllocMemBlk (gPtpGlobalInfo.
                                                      UnicMasterPoolId);

    if (pTmpPtpAccUniCastMasterTable == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "Memory Allocation failed " "for master creation\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    MEMSET (pTmpPtpAccUniCastMasterTable, 0,
            sizeof (tPtpAccUnicastMasterTable));

    pTmpPtpAccUniCastMasterTable->u4ContextId = (UINT4) i4FsPtpContextId;
    pTmpPtpAccUniCastMasterTable->UMAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpUnicastMasterNetworkProtocol;
    pTmpPtpAccUniCastMasterTable->UMAddr.u2AddrLength
        = (UINT2) i4FsPtpUnicastMasterAddLength;
    pTmpPtpAccUniCastMasterTable->u1DomainId = (UINT1) i4FsPtpDomainNumber;
    pTmpPtpAccUniCastMasterTable->UMAddr.u2AddrLength =
        (UINT2) i4FsPtpUnicastMasterAddLength;
    MEMCPY (pTmpPtpAccUniCastMasterTable->UMAddr.ai1Addr,
            pFsPtpUnicastMasterAddr->pu1_OctetList,
            pTmpPtpAccUniCastMasterTable->UMAddr.u2AddrLength);

    pPtpAccUniCastMasterTable = (tPtpAccUnicastMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.UnicastMastLst,
                      pTmpPtpAccUniCastMasterTable,
                      (UINT1) PTP_UNICAST_MASTER_DATA_SET);

    if (pPtpAccUniCastMasterTable == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                        (UINT1 *) pTmpPtpAccUniCastMasterTable);
    pTmpPtpAccUniCastMasterTable = NULL;
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpUnicastMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex
                FsPtpUnicastMasterNetworkProtocol
                FsPtpUnicastMasterAddLength
                FsPtpUnicastMasterAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpUnicastMasterDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber,
     INT4 *pi4FsPtpPortIndex, INT4 *pi4FsPtpUnicastMasterNetworkProtocol,
     INT4 *pi4FsPtpUnicastMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpUnicastMasterAddr)
{
    UNUSED_PARAM (pi4FsPtpContextId);
    UNUSED_PARAM (pi4FsPtpDomainNumber);
    UNUSED_PARAM (pi4FsPtpPortIndex);
    UNUSED_PARAM (pi4FsPtpUnicastMasterNetworkProtocol);
    UNUSED_PARAM (pi4FsPtpUnicastMasterAddLength);
    UNUSED_PARAM (pFsPtpUnicastMasterAddr);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpUnicastMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
                FsPtpPortIndex
                nextFsPtpPortIndex
                FsPtpUnicastMasterNetworkProtocol
                nextFsPtpUnicastMasterNetworkProtocol
                FsPtpUnicastMasterAddLength
                nextFsPtpUnicastMasterAddLength
                FsPtpUnicastMasterAddr
                nextFsPtpUnicastMasterAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpUnicastMasterDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 *pi4NextFsPtpPortIndex,
     INT4 i4FsPtpUnicastMasterNetworkProtocol,
     INT4 *pi4NextFsPtpUnicastMasterNetworkProtocol,
     INT4 i4FsPtpUnicastMasterAddLength,
     INT4 *pi4NextFsPtpUnicastMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpUnicastMasterAddr,
     tSNMP_OCTET_STRING_TYPE * pNextFsPtpUnicastMasterAddr)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (pi4NextFsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (pi4NextFsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpPortIndex);
    UNUSED_PARAM (pi4NextFsPtpPortIndex);
    UNUSED_PARAM (i4FsPtpUnicastMasterNetworkProtocol);
    UNUSED_PARAM (pi4NextFsPtpUnicastMasterNetworkProtocol);
    UNUSED_PARAM (i4FsPtpUnicastMasterAddLength);
    UNUSED_PARAM (pi4NextFsPtpUnicastMasterAddLength);
    UNUSED_PARAM (pFsPtpUnicastMasterAddr);
    UNUSED_PARAM (pNextFsPtpUnicastMasterAddr);
    return (INT1) SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpUnicastMasterRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex
                FsPtpUnicastMasterNetworkProtocol
                FsPtpUnicastMasterAddLength
                FsPtpUnicastMasterAddr

                The Object 
                retValFsPtpUnicastMasterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpUnicastMasterRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4FsPtpUnicastMasterNetworkProtocol,
     INT4 i4FsPtpUnicastMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpUnicastMasterAddr,
     INT4 *pi4RetValFsPtpUnicastMasterRowStatus)
{
    tPtpAccUnicastMasterTable *pPtpAccUniCastMasterTable = NULL;
    tPtpAccUnicastMasterTable *pTmpPtpAccUniCastMasterTable = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pTmpPtpAccUniCastMasterTable =
        (tPtpAccUnicastMasterTable *) MemAllocMemBlk (gPtpGlobalInfo.
                                                      UnicMasterPoolId);

    if (pTmpPtpAccUniCastMasterTable == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC,
                  "Memory Allocation failed " "for master creation\r\n"));
        return (INT1) SNMP_FAILURE;
    }

    MEMSET (pTmpPtpAccUniCastMasterTable, 0,
            sizeof (tPtpAccUnicastMasterTable));

    pTmpPtpAccUniCastMasterTable->u4ContextId = (UINT4) i4FsPtpContextId;
    pTmpPtpAccUniCastMasterTable->u4PtpPortNumber = (UINT4) i4FsPtpPortIndex;
    pTmpPtpAccUniCastMasterTable->UMAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpUnicastMasterNetworkProtocol;
    pTmpPtpAccUniCastMasterTable->UMAddr.u2AddrLength
        = (UINT2) i4FsPtpUnicastMasterAddLength;
    pTmpPtpAccUniCastMasterTable->u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (pTmpPtpAccUniCastMasterTable->UMAddr.ai1Addr,
            pFsPtpUnicastMasterAddr->pu1_OctetList,
            pTmpPtpAccUniCastMasterTable->UMAddr.u2AddrLength);

    pPtpAccUniCastMasterTable = (tPtpAccUnicastMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.UnicastMastLst,
                      pTmpPtpAccUniCastMasterTable,
                      (UINT1) PTP_UNICAST_MASTER_DATA_SET);

    MemReleaseMemBlock (gPtpGlobalInfo.UtilityPoolId,
                        (UINT1 *) pTmpPtpAccUniCastMasterTable);
    pTmpPtpAccUniCastMasterTable = NULL;

    if (pPtpAccUniCastMasterTable == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpUnicastMasterRowStatus =
            (INT4) pPtpAccUniCastMasterTable->u1RowStatus;
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpUnicastMasterRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex
                FsPtpUnicastMasterNetworkProtocol
                FsPtpUnicastMasterAddLength
                FsPtpUnicastMasterAddr

                The Object 
                setValFsPtpUnicastMasterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpUnicastMasterRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpPortIndex, INT4 i4FsPtpUnicastMasterNetworkProtocol,
     INT4 i4FsPtpUnicastMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpUnicastMasterAddr,
     INT4 i4SetValFsPtpUnicastMasterRowStatus)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpPortIndex);
    UNUSED_PARAM (i4FsPtpUnicastMasterNetworkProtocol);
    UNUSED_PARAM (i4FsPtpUnicastMasterAddLength);
    UNUSED_PARAM (pFsPtpUnicastMasterAddr);
    UNUSED_PARAM (i4SetValFsPtpUnicastMasterRowStatus);
    return (INT1) SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpUnicastMasterRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex
                FsPtpUnicastMasterNetworkProtocol
                FsPtpUnicastMasterAddLength
                FsPtpUnicastMasterAddr

                The Object 
                testValFsPtpUnicastMasterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpUnicastMasterRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpPortIndex,
     INT4 i4FsPtpUnicastMasterNetworkProtocol,
     INT4 i4FsPtpUnicastMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpUnicastMasterAddr,
     INT4 i4TestValFsPtpUnicastMasterRowStatus)
{
    tPtpAccUnicastMasterTable PtpAccUnicastMasterTable;
    tPtpAccUnicastMasterTable *pPtpAccUnicastMasterTable = NULL;
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4FsPtpUnicastMasterAddLength);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (i4TestValFsPtpUnicastMasterRowStatus == DESTROY)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if ((i4FsPtpContextId > PTP_MAX_CONTEXT_ID) ||
        (i4FsPtpDomainNumber >= PTP_MAX_DOMAINS) ||
        (i4FsPtpPortIndex > (UINT1) PTP_MAX_PORTS) ||
        (i4FsPtpUnicastMasterNetworkProtocol >=
         PTP_MAX_NW_PROTOCOL) ||
        (pFsPtpUnicastMasterAddr->i4_Length <
         PTP_MASTER_MIN_ADDR_LEN) ||
        (pFsPtpUnicastMasterAddr->i4_Length > PTP_MASTER_MIN_ADDR_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Domain does not exists\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    MEMSET (&PtpAccUnicastMasterTable, 0, sizeof (tPtpAccUnicastMasterTable));

    PtpAccUnicastMasterTable.u4ContextId = i4FsPtpContextId;
    PtpAccUnicastMasterTable.u4PtpPortNumber = i4FsPtpPortIndex;
    PtpAccUnicastMasterTable.UMAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpUnicastMasterNetworkProtocol;
    PtpAccUnicastMasterTable.UMAddr.u2AddrLength
        = (UINT2) pFsPtpUnicastMasterAddr->i4_Length;
    PtpAccUnicastMasterTable.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpAccUnicastMasterTable.UMAddr.ai1Addr,
            pFsPtpUnicastMasterAddr->pu1_OctetList,
            pFsPtpUnicastMasterAddr->i4_Length);

    pPtpAccUnicastMasterTable = (tPtpAccUnicastMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.UnicastMastLst,
                      &(PtpAccUnicastMasterTable),
                      (UINT1) PTP_UNICAST_MASTER_DATA_SET);

    switch (i4TestValFsPtpUnicastMasterRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpAccUnicastMasterTable == NULL)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if ((i1RetVal == (INT1) SNMP_FAILURE) ||
                (pPtpAccUnicastMasterTable != NULL))
            {
                if (i4TestValFsPtpUnicastMasterRowStatus == CREATE_AND_GO)
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                }
                else
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
                }
                CLI_SET_ERR (CLI_PTP_ERR_UNICAST_MASTER_PRESENT);
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }

        default:
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpUnicastMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpPortIndex
                FsPtpUnicastMasterNetworkProtocol
                FsPtpUnicastMasterAddLength
                FsPtpUnicastMasterAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsPtpUnicastMasterDataSetTable
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpAccMasterDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpAccMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAccMasterNetworkProtocol
                FsPtpAccMasterAddLength
                FsPtpAccMasterAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpAccMasterDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAccMasterNetworkProtocol,
     INT4 i4FsPtpAccMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr)
{
    tPtpAccMasterTable *pPtpAccMasterTable = NULL;
    tPtpAccMasterTable  PtpAccMasterTable;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    MEMSET (&PtpAccMasterTable, 0, sizeof (tPtpAccMasterTable));

    PtpAccMasterTable.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpAccMasterTable.AccMstAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpAccMasterNetworkProtocol;
    PtpAccMasterTable.AccMstAddr.u2AddrLength
        = (UINT2) i4FsPtpAccMasterAddLength;
    PtpAccMasterTable.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpAccMasterTable.AccMstAddr.ai1Addr,
            pFsPtpAccMasterAddr->pu1_OctetList,
            PtpAccMasterTable.AccMstAddr.u2AddrLength);

    pPtpAccMasterTable = (tPtpAccMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.AccMastLst,
                      &(PtpAccMasterTable), (UINT1) PTP_ACC_MASTER_DATA_SET);

    if (pPtpAccMasterTable == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Acceptable master data set does not exists\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpAccMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAccMasterNetworkProtocol
                FsPtpAccMasterAddLength
                FsPtpAccMasterAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpAccMasterDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber,
     INT4 *pi4FsPtpAccMasterNetworkProtocol,
     INT4 *pi4FsPtpAccMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr)
{
    tPtpAccMasterTable *pPtpAccMasterTable = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;

    }

    pPtpAccMasterTable = (tPtpAccMasterTable *)
        PtpDbGetFirstNode (gPtpGlobalInfo.AccMastLst,
                           (UINT1) PTP_ACC_MASTER_DATA_SET);

    if (pPtpAccMasterTable == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4FsPtpContextId = (INT4) pPtpAccMasterTable->pPtpDomain->
            pPtpCxt->u4ContextId;
        *pi4FsPtpDomainNumber = (INT4) pPtpAccMasterTable->
            pPtpDomain->u1DomainId;
        *pi4FsPtpAccMasterNetworkProtocol =
            (INT4) pPtpAccMasterTable->AccMstAddr.u2NetworkProtocol;
        *pi4FsPtpAccMasterAddLength =
            (INT4) pPtpAccMasterTable->AccMstAddr.u2AddrLength;
        pFsPtpAccMasterAddr->i4_Length = *pi4FsPtpAccMasterAddLength;
        MEMCPY (pFsPtpAccMasterAddr->pu1_OctetList,
                pPtpAccMasterTable->AccMstAddr.
                ai1Addr, pFsPtpAccMasterAddr->i4_Length);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpAccMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
                FsPtpAccMasterNetworkProtocol
                nextFsPtpAccMasterNetworkProtocol
                FsPtpAccMasterAddLength
                nextFsPtpAccMasterAddLength
                FsPtpAccMasterAddr
                nextFsPtpAccMasterAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpAccMasterDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber,
     INT4 i4FsPtpAccMasterNetworkProtocol,
     INT4 *pi4NextFsPtpAccMasterNetworkProtocol,
     INT4 i4FsPtpAccMasterAddLength,
     INT4 *pi4NextFsPtpAccMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr,
     tSNMP_OCTET_STRING_TYPE * pNextFsPtpAccMasterAddr)
{
    tPtpAccMasterTable *pPtpAccMasterTable = NULL;
    tPtpAccMasterTable  PtpAccMasterTable;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;

    }

    MEMSET (&PtpAccMasterTable, 0, sizeof (tPtpAccMasterTable));

    PtpAccMasterTable.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpAccMasterTable.AccMstAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpAccMasterNetworkProtocol;
    PtpAccMasterTable.AccMstAddr.u2AddrLength
        = (UINT2) i4FsPtpAccMasterAddLength;
    PtpAccMasterTable.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpAccMasterTable.AccMstAddr.ai1Addr,
            pFsPtpAccMasterAddr->pu1_OctetList,
            PtpAccMasterTable.AccMstAddr.u2AddrLength);

    pPtpAccMasterTable = (tPtpAccMasterTable *)
        PtpDbGetNextNode (gPtpGlobalInfo.AccMastLst,
                          &(PtpAccMasterTable),
                          (UINT1) PTP_ACC_MASTER_DATA_SET);

    if (pPtpAccMasterTable == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4NextFsPtpContextId = (INT4) pPtpAccMasterTable->pPtpDomain->
            pPtpCxt->u4ContextId;
        *pi4NextFsPtpDomainNumber = (INT4) pPtpAccMasterTable->
            pPtpDomain->u1DomainId;
        *pi4NextFsPtpAccMasterNetworkProtocol =
            (INT4) pPtpAccMasterTable->AccMstAddr.u2NetworkProtocol;
        *pi4NextFsPtpAccMasterAddLength =
            (INT4) pPtpAccMasterTable->AccMstAddr.u2AddrLength;
        pNextFsPtpAccMasterAddr->i4_Length = *pi4NextFsPtpAccMasterAddLength;
        MEMCPY (pNextFsPtpAccMasterAddr->pu1_OctetList,
                pPtpAccMasterTable->AccMstAddr.ai1Addr,
                pNextFsPtpAccMasterAddr->i4_Length);
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpAccMasterAlternatePriority
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAccMasterNetworkProtocol
                FsPtpAccMasterAddLength
                FsPtpAccMasterAddr

                The Object 
                retValFsPtpAccMasterAlternatePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpAccMasterAlternatePriority
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAccMasterNetworkProtocol,
     INT4 i4FsPtpAccMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr,
     INT4 *pi4RetValFsPtpAccMasterAlternatePriority)
{
    tPtpAccMasterTable *pPtpAccMasterTable = NULL;
    tPtpAccMasterTable  PtpAccMasterTable;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpAccMasterTable, 0, sizeof (tPtpAccMasterTable));

    PtpAccMasterTable.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpAccMasterTable.AccMstAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpAccMasterNetworkProtocol;
    PtpAccMasterTable.AccMstAddr.u2AddrLength
        = (UINT2) i4FsPtpAccMasterAddLength;
    PtpAccMasterTable.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpAccMasterTable.AccMstAddr.ai1Addr,
            pFsPtpAccMasterAddr->pu1_OctetList,
            PtpAccMasterTable.AccMstAddr.u2AddrLength);

    pPtpAccMasterTable = (tPtpAccMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.AccMastLst,
                      &(PtpAccMasterTable), (UINT1) PTP_ACC_MASTER_DATA_SET);

    if (pPtpAccMasterTable == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Acceptable master data set does not exists\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpAccMasterAlternatePriority =
            (INT4) pPtpAccMasterTable->u1AltPri1;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpAccMasterRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAccMasterNetworkProtocol
                FsPtpAccMasterAddLength
                FsPtpAccMasterAddr

                The Object 
                retValFsPtpAccMasterRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpAccMasterRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAccMasterNetworkProtocol,
     INT4 i4FsPtpAccMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr,
     INT4 *pi4RetValFsPtpAccMasterRowStatus)
{
    tPtpAccMasterTable *pPtpAccMasterTable = NULL;
    tPtpAccMasterTable  PtpAccMasterTable;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpAccMasterTable, 0, sizeof (tPtpAccMasterTable));

    PtpAccMasterTable.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpAccMasterTable.AccMstAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpAccMasterNetworkProtocol;
    PtpAccMasterTable.AccMstAddr.u2AddrLength
        = (UINT2) i4FsPtpAccMasterAddLength;
    PtpAccMasterTable.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpAccMasterTable.AccMstAddr.ai1Addr,
            pFsPtpAccMasterAddr->pu1_OctetList,
            PtpAccMasterTable.AccMstAddr.u2AddrLength);

    pPtpAccMasterTable = (tPtpAccMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.AccMastLst,
                      &(PtpAccMasterTable), (UINT1) PTP_ACC_MASTER_DATA_SET);

    if (pPtpAccMasterTable == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Acceptable master data set does not exists\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpAccMasterRowStatus =
            (INT4) pPtpAccMasterTable->u1RowStatus;
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpAccMasterAlternatePriority
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAccMasterNetworkProtocol
                FsPtpAccMasterAddLength
                FsPtpAccMasterAddr

                The Object 
                setValFsPtpAccMasterAlternatePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpAccMasterAlternatePriority
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAccMasterNetworkProtocol,
     INT4 i4FsPtpAccMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr,
     INT4 i4SetValFsPtpAccMasterAlternatePriority)
{
    tPtpAccMasterTable *pPtpAccMasterTable = NULL;
    tPtpAccMasterTable  PtpAccMasterTable;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpAccMasterTable, 0, sizeof (tPtpAccMasterTable));

    PtpAccMasterTable.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpAccMasterTable.AccMstAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpAccMasterNetworkProtocol;
    PtpAccMasterTable.AccMstAddr.u2AddrLength
        = (UINT2) i4FsPtpAccMasterAddLength;
    PtpAccMasterTable.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpAccMasterTable.AccMstAddr.ai1Addr,
            pFsPtpAccMasterAddr->pu1_OctetList,
            PtpAccMasterTable.AccMstAddr.u2AddrLength);

    pPtpAccMasterTable = (tPtpAccMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.AccMastLst,
                      &(PtpAccMasterTable), (UINT1) PTP_ACC_MASTER_DATA_SET);

    if (pPtpAccMasterTable == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Acceptable master data set does not exists\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pPtpAccMasterTable->u1AltPri1 =
            (UINT1) i4SetValFsPtpAccMasterAlternatePriority;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpAccMasterRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAccMasterNetworkProtocol
                FsPtpAccMasterAddLength
                FsPtpAccMasterAddr

                The Object 
                setValFsPtpAccMasterRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpAccMasterRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAccMasterNetworkProtocol,
     INT4 i4FsPtpAccMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr,
     INT4 i4SetValFsPtpAccMasterRowStatus)
{
    tPtpDomain         *pPtpDomain = NULL;
    tPtpAccMasterTable *pPtpAccMasterTable = NULL;
    tPtpAccMasterTable  PtpAccMasterTable;
    INT1                i1RetVal = (INT1) SNMP_FAILURE;

    MEMSET (&PtpAccMasterTable, 0, sizeof (tPtpAccMasterTable));

    PtpAccMasterTable.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpAccMasterTable.AccMstAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpAccMasterNetworkProtocol;
    PtpAccMasterTable.AccMstAddr.u2AddrLength
        = (UINT2) i4FsPtpAccMasterAddLength;
    PtpAccMasterTable.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpAccMasterTable.AccMstAddr.ai1Addr,
            pFsPtpAccMasterAddr->pu1_OctetList,
            PtpAccMasterTable.AccMstAddr.u2AddrLength);

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Domain Data Set does not exist\r\n"));
        return (SNMP_FAILURE);
    }

    pPtpAccMasterTable = (tPtpAccMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.AccMastLst,
                      &(PtpAccMasterTable), (UINT1) PTP_ACC_MASTER_DATA_SET);

    switch (i4SetValFsPtpAccMasterRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpAccMasterTable != NULL)
            {
                pPtpAccMasterTable->u1RowStatus =
                    (UINT1) i4SetValFsPtpAccMasterRowStatus;
                i1RetVal = (INT1) SNMP_SUCCESS;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if ((pPtpAccMasterTable == NULL) &&
                ((pPtpAccMasterTable = (tPtpAccMasterTable *)
                  (VOID *) PtpMastCreate (&PtpAccMasterTable,
                                          (UINT1) PTP_ACC_MASTER_DATA_SET)) !=
                 NULL))
            {
                i1RetVal = (INT1) SNMP_SUCCESS;

                pPtpAccMasterTable->u1AltPri1 = (UINT1) PTP_ACMST_DEF_ALT_PRI1;

                if (i4SetValFsPtpAccMasterRowStatus == CREATE_AND_WAIT)
                {
                    pPtpAccMasterTable->u1RowStatus = (UINT1) NOT_IN_SERVICE;
                }
                else
                {
                    pPtpAccMasterTable->u1RowStatus = (UINT1) ACTIVE;
                }

                /*  Update the Acceptable master count */
                pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u2AccMasterMaxSize++;
            }
            break;
        }
        case DESTROY:
        {
            i1RetVal = (INT1) SNMP_SUCCESS;
            if (pPtpAccMasterTable != NULL)
            {
                PtpMastDelete (pPtpAccMasterTable,
                               (UINT1) PTP_ACC_MASTER_DATA_SET);

                /*  Update the Acceptable master count */
                pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.u2AccMasterMaxSize--;
            }

            break;
        }
        default:
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpAccMasterAlternatePriority
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAccMasterNetworkProtocol
                FsPtpAccMasterAddLength
                FsPtpAccMasterAddr

                The Object 
                testValFsPtpAccMasterAlternatePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpAccMasterAlternatePriority
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpAccMasterNetworkProtocol,
     INT4 i4FsPtpAccMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr,
     INT4 i4TestValFsPtpAccMasterAlternatePriority)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4FsPtpAccMasterAddLength);
    UNUSED_PARAM (i4TestValFsPtpAccMasterAlternatePriority);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateAccMaster ((UINT4) i4FsPtpContextId,
                                 (UINT1) i4FsPtpDomainNumber,
                                 (UINT4) i4FsPtpAccMasterNetworkProtocol,
                                 pFsPtpAccMasterAddr,
                                 pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Acceptable master is invalid\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpAccMasterRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAccMasterNetworkProtocol
                FsPtpAccMasterAddLength
                FsPtpAccMasterAddr

                The Object 
                testValFsPtpAccMasterRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpAccMasterRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpAccMasterNetworkProtocol,
     INT4 i4FsPtpAccMasterAddLength,
     tSNMP_OCTET_STRING_TYPE * pFsPtpAccMasterAddr,
     INT4 i4TestValFsPtpAccMasterRowStatus)
{
    tPtpAccMasterTable  PtpAccMasterTable;
    tPtpDomain         *pPtpDomain = NULL;
    tPtpAccMasterTable *pPtpAccMasterTable = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        return (INT1) SNMP_FAILURE;
    }

    if (i4TestValFsPtpAccMasterRowStatus == DESTROY)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if ((i4FsPtpContextId > PTP_MAX_CONTEXT_ID) ||
        (i4FsPtpDomainNumber >= PTP_MAX_DOMAINS) ||
        (i4FsPtpAccMasterNetworkProtocol >= PTP_MAX_NW_PROTOCOL) ||
        (pFsPtpAccMasterAddr->i4_Length < PTP_MASTER_MIN_ADDR_LEN) ||
        (pFsPtpAccMasterAddr->i4_Length > PTP_MASTER_MAX_ADDR_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (INT1) SNMP_FAILURE;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Domain does not exists\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (INT1) SNMP_FAILURE;
    }

    if ((pPtpDomain->ClkMode != PTP_BOUNDARY_CLOCK_MODE) &&
        (pPtpDomain->ClkMode != PTP_OC_CLOCK_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Clock mode should be ordinary Clock "
                  "or boundary Clock\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PTP_ERR_ACCEPT_MAST_CLK_MODE);
        return (SNMP_FAILURE);
    }

    MEMSET (&PtpAccMasterTable, 0, sizeof (PtpAccMasterTable));

    PtpAccMasterTable.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpAccMasterTable.AccMstAddr.u2NetworkProtocol
        = (UINT2) i4FsPtpAccMasterNetworkProtocol;
    PtpAccMasterTable.AccMstAddr.u2AddrLength
        = (UINT2) pFsPtpAccMasterAddr->i4_Length;
    PtpAccMasterTable.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    MEMCPY (PtpAccMasterTable.AccMstAddr.ai1Addr,
            pFsPtpAccMasterAddr->pu1_OctetList, pFsPtpAccMasterAddr->i4_Length);

    pPtpAccMasterTable = (tPtpAccMasterTable *)
        PtpDbGetNode (gPtpGlobalInfo.AccMastLst,
                      &(PtpAccMasterTable), (UINT1) PTP_ACC_MASTER_DATA_SET);

    UNUSED_PARAM (i4FsPtpAccMasterAddLength);

    switch (i4TestValFsPtpAccMasterRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpAccMasterTable == NULL)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if ((i1RetVal == (INT1) SNMP_FAILURE) ||
                (pPtpAccMasterTable != NULL))
            {
                if (i4TestValFsPtpAccMasterRowStatus == CREATE_AND_GO)
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                }
                else
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
                }
                CLI_SET_ERR (CLI_PTP_ERR_ACCEPT_MASTER_PRESENT);
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }

        default:
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpAccMasterDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAccMasterNetworkProtocol
                FsPtpAccMasterAddLength
                FsPtpAccMasterAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsPtpAccMasterDataSetTable
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpSecKeyDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpSecKeyDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpSecKeyDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSecKeyId)
{
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpSecKeyDs        PtpSecKeyDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    MEMSET (&PtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

    PtpSecKeyDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpSecKeyDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    PtpSecKeyDs.u4SecKeyId = (UINT4) i4FsPtpSecKeyId;

    pPtpSecKeyDs = (tPtpSecKeyDs *)
        PtpDbGetNode (gPtpGlobalInfo.SecKeyLst,
                      &(gPtpGlobalInfo.SecKeyPoolId),
                      (UINT1) PTP_SEC_KEY_DATA_SET);

    if (pPtpSecKeyDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpSecKeyDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpSecKeyDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber,
     INT4 *pi4FsPtpSecKeyId)
{
    UNUSED_PARAM (pi4FsPtpContextId);
    UNUSED_PARAM (pi4FsPtpDomainNumber);
    UNUSED_PARAM (pi4FsPtpSecKeyId);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpSecKeyDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
                FsPtpSecKeyId
                nextFsPtpSecKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpSecKeyDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber,
     INT4 i4FsPtpSecKeyId, INT4 *pi4NextFsPtpSecKeyId)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (pi4NextFsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (pi4NextFsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSecKeyId);
    UNUSED_PARAM (pi4NextFsPtpSecKeyId);
    return (INT1) SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpSecKeyAlgorithmId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                retValFsPtpSecKeyAlgorithmId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSecKeyAlgorithmId (INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 i4FsPtpSecKeyId,
                              INT4 *pi4RetValFsPtpSecKeyAlgorithmId)
{
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpSecKeyDs        PtpSecKeyDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

    PtpSecKeyDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpSecKeyDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    PtpSecKeyDs.u4SecKeyId = (UINT4) i4FsPtpSecKeyId;

    pPtpSecKeyDs = (tPtpSecKeyDs *)
        PtpDbGetNode (gPtpGlobalInfo.SecKeyLst,
                      &(gPtpGlobalInfo.SecKeyPoolId),
                      (UINT1) PTP_SEC_KEY_DATA_SET);

    if (pPtpSecKeyDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSecKeyAlgorithmId = (INT4) pPtpSecKeyDs->u4SecKeyAlgId;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSecKeyLength
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                retValFsPtpSecKeyLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSecKeyLength (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                         INT4 i4FsPtpSecKeyId, INT4 *pi4RetValFsPtpSecKeyLength)
{
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpSecKeyDs        PtpSecKeyDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

    PtpSecKeyDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpSecKeyDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    PtpSecKeyDs.u4SecKeyId = (UINT4) i4FsPtpSecKeyId;

    pPtpSecKeyDs = (tPtpSecKeyDs *)
        PtpDbGetNode (gPtpGlobalInfo.SecKeyLst,
                      &(gPtpGlobalInfo.SecKeyPoolId),
                      (UINT1) PTP_SEC_KEY_DATA_SET);

    if (pPtpSecKeyDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSecKeyLength = (INT4) pPtpSecKeyDs->u4SecKeyLength;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSecKey
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                retValFsPtpSecKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSecKey (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                   INT4 i4FsPtpSecKeyId, tSNMP_OCTET_STRING_TYPE *
                   pRetValFsPtpSecKey)
{
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpSecKeyDs        PtpSecKeyDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

    PtpSecKeyDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpSecKeyDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    PtpSecKeyDs.u4SecKeyId = (UINT4) i4FsPtpSecKeyId;

    pPtpSecKeyDs = (tPtpSecKeyDs *)
        PtpDbGetNode (gPtpGlobalInfo.SecKeyLst,
                      &(gPtpGlobalInfo.SecKeyPoolId),
                      (UINT1) PTP_SEC_KEY_DATA_SET);

    if (pPtpSecKeyDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpSecKey->i4_Length = STRLEN (pPtpSecKeyDs->SecKey);
        MEMCPY (pRetValFsPtpSecKey->pu1_OctetList, pPtpSecKeyDs->SecKey,
                STRLEN (pPtpSecKeyDs->SecKey));
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSecKeyStartTime
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                retValFsPtpSecKeyStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSecKeyStartTime (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSecKeyId,
                            UINT4 *pu4RetValFsPtpSecKeyStartTime)
{
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpSecKeyDs        PtpSecKeyDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

    PtpSecKeyDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpSecKeyDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    PtpSecKeyDs.u4SecKeyId = (UINT4) i4FsPtpSecKeyId;

    pPtpSecKeyDs = (tPtpSecKeyDs *)
        PtpDbGetNode (gPtpGlobalInfo.SecKeyLst,
                      &(gPtpGlobalInfo.SecKeyPoolId),
                      (UINT1) PTP_SEC_KEY_DATA_SET);

    if (pPtpSecKeyDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpSecKeyStartTime = pPtpSecKeyDs->u4SecKeyStartTime;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSecKeyExpirationTime
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                retValFsPtpSecKeyExpirationTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSecKeyExpirationTime (INT4 i4FsPtpContextId,
                                 INT4 i4FsPtpDomainNumber,
                                 INT4 i4FsPtpSecKeyId,
                                 UINT4 *pu4RetValFsPtpSecKeyExpirationTime)
{
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpSecKeyDs        PtpSecKeyDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

    PtpSecKeyDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpSecKeyDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    PtpSecKeyDs.u4SecKeyId = (UINT4) i4FsPtpSecKeyId;

    pPtpSecKeyDs = (tPtpSecKeyDs *)
        PtpDbGetNode (gPtpGlobalInfo.SecKeyLst,
                      &(gPtpGlobalInfo.SecKeyPoolId),
                      (UINT1) PTP_SEC_KEY_DATA_SET);

    if (pPtpSecKeyDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pu4RetValFsPtpSecKeyExpirationTime = pPtpSecKeyDs->u4SecKeyExpiryTime;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSecKeyValid
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                retValFsPtpSecKeyValid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSecKeyValid (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                        INT4 i4FsPtpSecKeyId, INT4 *pi4RetValFsPtpSecKeyValid)
{
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpSecKeyDs        PtpSecKeyDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

    PtpSecKeyDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpSecKeyDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    PtpSecKeyDs.u4SecKeyId = (UINT4) i4FsPtpSecKeyId;

    pPtpSecKeyDs = (tPtpSecKeyDs *)
        PtpDbGetNode (gPtpGlobalInfo.SecKeyLst,
                      &(gPtpGlobalInfo.SecKeyPoolId),
                      (UINT1) PTP_SEC_KEY_DATA_SET);

    if (pPtpSecKeyDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSecKeyValid = (INT4) pPtpSecKeyDs->bSecKeyValid;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSecKeyRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                retValFsPtpSecKeyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpSecKeyRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpSecKeyId, INT4 *pi4RetValFsPtpSecKeyRowStatus)
{
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpSecKeyDs        PtpSecKeyDs;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&PtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

    PtpSecKeyDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpSecKeyDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    PtpSecKeyDs.u4SecKeyId = (UINT4) i4FsPtpSecKeyId;

    pPtpSecKeyDs = (tPtpSecKeyDs *)
        PtpDbGetNode (gPtpGlobalInfo.SecKeyLst,
                      &(gPtpGlobalInfo.SecKeyPoolId),
                      (UINT1) PTP_SEC_KEY_DATA_SET);

    if (pPtpSecKeyDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSecKeyRowStatus = pPtpSecKeyDs->u1RowStatus;
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpSecKeyAlgorithmId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                setValFsPtpSecKeyAlgorithmId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpSecKeyAlgorithmId
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpSecKeyId, INT4 i4SetValFsPtpSecKeyAlgorithmId)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSecKeyId);
    UNUSED_PARAM (i4SetValFsPtpSecKeyAlgorithmId);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSecKeyLength
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                setValFsPtpSecKeyLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpSecKeyLength
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpSecKeyId, INT4 i4SetValFsPtpSecKeyLength)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSecKeyId);
    UNUSED_PARAM (i4SetValFsPtpSecKeyLength);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSecKey
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                setValFsPtpSecKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSecKey (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                   INT4 i4FsPtpSecKeyId,
                   tSNMP_OCTET_STRING_TYPE * pSetValFsPtpSecKey)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSecKeyId);
    UNUSED_PARAM (pSetValFsPtpSecKey);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSecKeyStartTime
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                setValFsPtpSecKeyStartTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpSecKeyStartTime
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpSecKeyId, UINT4 u4SetValFsPtpSecKeyStartTime)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSecKeyId);
    UNUSED_PARAM (u4SetValFsPtpSecKeyStartTime);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSecKeyExpirationTime
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                setValFsPtpSecKeyExpirationTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpSecKeyExpirationTime
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpSecKeyId, UINT4 u4SetValFsPtpSecKeyExpirationTime)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSecKeyId);
    UNUSED_PARAM (u4SetValFsPtpSecKeyExpirationTime);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSecKeyValid
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                setValFsPtpSecKeyValid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSecKeyValid (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                        INT4 i4FsPtpSecKeyId, INT4 i4SetValFsPtpSecKeyValid)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSecKeyId);
    UNUSED_PARAM (i4SetValFsPtpSecKeyValid);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSecKeyRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                setValFsPtpSecKeyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSecKeyRowStatus (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSecKeyId,
                            INT4 i4SetValFsPtpSecKeyRowStatus)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSecKeyId);
    UNUSED_PARAM (i4SetValFsPtpSecKeyRowStatus);
    return (INT1) SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpSecKeyAlgorithmId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                testValFsPtpSecKeyAlgorithmId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSecKeyAlgorithmId (UINT4 *pu4ErrorCode,
                                 INT4 i4FsPtpContextId,
                                 INT4 i4FsPtpDomainNumber,
                                 INT4 i4FsPtpSecKeyId,
                                 INT4 i4TestValFsPtpSecKeyAlgorithmId)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        return i1RetVal;

    }

    if ((i4TestValFsPtpSecKeyAlgorithmId < PTP_PORT_MIN_SEC_ALGO_ID) ||
        (i4TestValFsPtpSecKeyAlgorithmId > PTP_PORT_MAX_SEC_ALGO_ID))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for security key algorithm id\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSecKeyDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                (UINT4) i4FsPtpSecKeyId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSecKeyLength
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                testValFsPtpSecKeyLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSecKeyLength (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSecKeyId,
                            INT4 i4TestValFsPtpSecKeyLength)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpSecKeyLength < 0) ||
        (i4TestValFsPtpSecKeyLength > PTP_MAX_SEC_KEY_LENGTH))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for security key\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSecKeyDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                (UINT4) i4FsPtpSecKeyId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSecKey
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                testValFsPtpSecKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSecKey (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
                      INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSecKeyId,
                      tSNMP_OCTET_STRING_TYPE * pTestValFsPtpSecKey)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (pTestValFsPtpSecKey->i4_Length >= PTP_MAX_SEC_KEY_LENGTH)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for security key\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSecKeyDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                (UINT4) i4FsPtpSecKeyId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSecKeyStartTime
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                testValFsPtpSecKeyStartTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSecKeyStartTime (UINT4 *pu4ErrorCode,
                               INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSecKeyId,
                               UINT4 u4TestValFsPtpSecKeyStartTime)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (u4TestValFsPtpSecKeyStartTime);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateSecKeyDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                (UINT4) i4FsPtpSecKeyId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSecKeyExpirationTime
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                testValFsPtpSecKeyExpirationTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpSecKeyExpirationTime
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSecKeyId,
     UINT4 u4TestValFsPtpSecKeyExpirationTime)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (u4TestValFsPtpSecKeyExpirationTime);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateSecKeyDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                (UINT4) i4FsPtpSecKeyId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSecKeyValid
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                testValFsPtpSecKeyValid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSecKeyValid (UINT4 *pu4ErrorCode,
                           INT4 i4FsPtpContextId,
                           INT4 i4FsPtpDomainNumber,
                           INT4 i4FsPtpSecKeyId, INT4 i4TestValFsPtpSecKeyValid)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpSecKeyValid != PTP_ENABLED) &&
        (i4TestValFsPtpSecKeyValid != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for security key validity\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSecKeyDS ((UINT4) i4FsPtpContextId,
                                (UINT1) i4FsPtpDomainNumber,
                                (UINT4) i4FsPtpSecKeyId,
                                pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key data set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSecKeyRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId

                The Object 
                testValFsPtpSecKeyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpSecKeyRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSecKeyId,
     INT4 i4TestValFsPtpSecKeyRowStatus)
{
    tPtpSecKeyDs        PtpSecKeyDs;
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpDomain         *pPtpDomain = NULL;
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetStatus = OSIX_FAILURE;
        return (INT1) SNMP_FAILURE;
    }

    if (i4TestValFsPtpSecKeyRowStatus == DESTROY)
    {
        return (INT1) SNMP_SUCCESS;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Domain does not exists\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        i4RetStatus = OSIX_FAILURE;
        return (INT1) SNMP_FAILURE;
    }

    MEMSET (&PtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

    PtpSecKeyDs.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpSecKeyDs.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    PtpSecKeyDs.u4SecKeyId = (UINT4) i4FsPtpSecKeyId;

    pPtpSecKeyDs = (tPtpSecKeyDs *)
        PtpDbGetNode (gPtpGlobalInfo.SecKeyLst,
                      &(gPtpGlobalInfo.SecKeyPoolId),
                      (UINT1) PTP_SEC_KEY_DATA_SET);

    if (pPtpSecKeyDs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security key Data Set does not exist\r\n"));
        i4RetStatus = OSIX_FAILURE;
    }

    switch (i4TestValFsPtpSecKeyRowStatus)
    {
        case ACTIVE:
        {
            if (i4RetStatus == OSIX_FAILURE)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            else
            {
                if (pPtpSecKeyDs != NULL)
                {
                    if ((pPtpSecKeyDs->u4SecKeyLength !=
                         STRLEN (pPtpSecKeyDs->SecKey)) ||
                        (pPtpSecKeyDs->u4SecKeyExpiryTime <
                         pPtpSecKeyDs->u4SecKeyStartTime))
                    {
                        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;

                        PTP_TRC (((UINT4) i4FsPtpContextId,
                                  (UINT1) i4FsPtpDomainNumber,
                                  MGMT_TRC | ALL_FAILURE_TRC,
                                  "Security key length mismatch or "
                                  "key expiry time is less than key "
                                  "start time\r\n"));

                        i1RetVal = (INT1) SNMP_FAILURE;
                    }
                }
            }
            break;
        }
        case NOT_IN_SERVICE:
        {
            if (i4RetStatus == OSIX_FAILURE)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if (i4RetStatus == OSIX_SUCCESS)
            {
                if (i4TestValFsPtpSecKeyRowStatus == CREATE_AND_GO)
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                }
                else
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
                }
                CLI_SET_ERR (CLI_PTP_ERR_SEC_KEY_PRESENT);
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            else
            {
                pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                                   (UINT1) i4FsPtpDomainNumber);
                if (pPtpDomain == NULL)
                {
                    PTP_TRC (((UINT4) i4FsPtpContextId,
                              (UINT1) i4FsPtpDomainNumber,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Domain does not exists\r\n"));
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    i1RetVal = (INT1) SNMP_FAILURE;
                }
            }
            break;
        }

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpSecKeyDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSecKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsPtpSecKeyDataSetTable
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpSADataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpSADataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpSADataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSAId)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpSADataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpSADataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber, INT4 *pi4FsPtpSAId)
{
    UNUSED_PARAM (pi4FsPtpContextId);
    UNUSED_PARAM (pi4FsPtpDomainNumber);
    UNUSED_PARAM (pi4FsPtpSAId);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpSADataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
                FsPtpSAId
                nextFsPtpSAId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpSADataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber,
     INT4 i4FsPtpSAId, INT4 *pi4NextFsPtpSAId)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;

    }

    pPtpSADs = PtpSecSAGetNextSAEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber,
                                       (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4NextFsPtpContextId = (INT4) pPtpSADs->pPtpDomain->pPtpCxt->
            u4ContextId;
        *pi4NextFsPtpDomainNumber = (INT4) pPtpSADs->pPtpDomain->u1DomainId;
        *pi4NextFsPtpSAId = (INT4) pPtpSADs->u4SAId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpSASrcPortNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSASrcPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSASrcPortNumber (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSAId,
                            INT4 *pi4RetValFsPtpSASrcPortNumber)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSASrcPortNumber = (INT4) pPtpSADs->u2SASrcPortIndex;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSASrcAddrLength
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSASrcAddrLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSASrcAddrLength (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSAId,
                            INT4 *pi4RetValFsPtpSASrcAddrLength)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSASrcAddrLength =
            (INT4) pPtpSADs->SASrcAddr.u2AddrLength;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSASrcAddr
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSASrcAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSASrcAddr (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                      INT4 i4FsPtpSAId,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsPtpSASrcAddr)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpSASrcAddr->i4_Length =
            (INT4) pPtpSADs->SASrcAddr.u2AddrLength;
        MEMCPY (pRetValFsPtpSASrcAddr->pu1_OctetList,
                pPtpSADs->SASrcAddr.ai1Addr, PTP_MAX_ADDR_LENGTH);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSADstPortNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSADstPortNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSADstPortNumber (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSAId,
                            INT4 *pi4RetValFsPtpSADstPortNumber)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSADstPortNumber = (INT4) pPtpSADs->u2SADstPortIndex;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSADstAddrLength
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSADstAddrLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSADstAddrLength (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSAId,
                            INT4 *pi4RetValFsPtpSADstAddrLength)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSADstAddrLength =
            (INT4) pPtpSADs->SADstAddr.u2AddrLength;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSADstAddr
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSADstAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSADstAddr (INT4 i4FsPtpContextId,
                      INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSAId,
                      tSNMP_OCTET_STRING_TYPE * pRetValFsPtpSADstAddr)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpSADstAddr->i4_Length =
            (INT4) pPtpSADs->SADstAddr.u2AddrLength;
        MEMCPY (pRetValFsPtpSADstAddr->pu1_OctetList,
                pPtpSADs->SADstAddr.ai1Addr, PTP_MAX_ADDR_LENGTH);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSASrcClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSASrcClockIdentity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSASrcClockIdentity (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsPtpSASrcClockIdentity)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpSASrcClockIdentity->i4_Length = PTP_MAX_CLOCK_ID_LEN;
        MEMCPY (pRetValFsPtpSASrcClockIdentity->pu1_OctetList,
                pPtpSADs->SASrcClkId, PTP_MAX_CLOCK_ID_LEN);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSADstClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSADstClockIdentity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSADstClockIdentity (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSAId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsPtpSADstClockIdentity)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpSADstClockIdentity->i4_Length = PTP_MAX_CLOCK_ID_LEN;
        MEMCPY (pRetValFsPtpSADstClockIdentity->pu1_OctetList,
                pPtpSADs->SADstClkId, PTP_MAX_CLOCK_ID_LEN);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSAReplayCounter
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSAReplayCounter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSAReplayCounter (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSAId,
                            INT4 *pi4RetValFsPtpSAReplayCounter)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSAReplayCounter = (INT4) pPtpSADs->u4SAReplayCntr;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSALifeTimeId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSALifeTimeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSALifeTimeId (INT4 i4FsPtpContextId,
                         INT4 i4FsPtpDomainNumber,
                         INT4 i4FsPtpSAId, INT4 *pi4RetValFsPtpSALifeTimeId)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSALifeTimeId = (INT4) pPtpSADs->u4SALifeTimeId;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSAKeyId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSAKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSAKeyId (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                    INT4 i4FsPtpSAId, INT4 *pi4RetValFsPtpSAKeyId)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSAKeyId = (INT4) pPtpSADs->u4SAKeyId;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSANextLifeTimeId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSANextLifeTimeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSANextLifeTimeId (INT4 i4FsPtpContextId,
                             INT4 i4FsPtpDomainNumber,
                             INT4 i4FsPtpSAId,
                             INT4 *pi4RetValFsPtpSANextLifeTimeId)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSANextLifeTimeId = (INT4) pPtpSADs->u4SANextLifeTimeId;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSANextKeyId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSANextKeyId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSANextKeyId (INT4 i4FsPtpContextId,
                        INT4 i4FsPtpDomainNumber,
                        INT4 i4FsPtpSAId, INT4 *pi4RetValFsPtpSANextKeyId)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSANextKeyId = (INT4) pPtpSADs->u4SANextKeyId;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSATrustState
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSATrustState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSATrustState (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                         INT4 i4FsPtpSAId, INT4 *pi4RetValFsPtpSATrustState)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSATrustState = (INT4) pPtpSADs->SATrustState;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSATrustTimer
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSATrustTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSATrustTimer (INT4 i4FsPtpContextId,
                         INT4 i4FsPtpDomainNumber,
                         INT4 i4FsPtpSAId, INT4 *pi4RetValFsPtpSATrustTimer)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSATrustTimer = 0;
        TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                             &(pPtpSADs->SATrustTimer.TimerNode),
                             (UINT4 *) pi4RetValFsPtpSATrustTimer);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSATrustTimeout
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSATrustTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSATrustTimeout (INT4 i4FsPtpContextId,
                           INT4 i4FsPtpDomainNumber,
                           INT4 i4FsPtpSAId, INT4 *pi4RetValFsPtpSATrustTimeout)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSATrustTimeout = (INT4) pPtpSADs->u2SATrustTimeOut;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSAChallengeState
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSAChallengeState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSAChallengeState (INT4 i4FsPtpContextId,
                             INT4 i4FsPtpDomainNumber,
                             INT4 i4FsPtpSAId,
                             INT4 *pi4RetValFsPtpSAChallengeState)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSAChallengeState = (INT4) pPtpSADs->SAChallengeState;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSAChallengeTimer
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSAChallengeTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSAChallengeTimer (INT4 i4FsPtpContextId,
                             INT4 i4FsPtpDomainNumber,
                             INT4 i4FsPtpSAId,
                             INT4 *pi4RetValFsPtpSAChallengeTimer)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSAChallengeTimer = 0;
        TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                             &(pPtpSADs->SAChallengeTimer.TimerNode),
                             (UINT4 *) pi4RetValFsPtpSAChallengeTimer);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSAChallengeTimeOut
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSAChallengeTimeOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSAChallengeTimeOut (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               INT4 *pi4RetValFsPtpSAChallengeTimeOut)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSAChallengeTimeOut =
            (INT4) pPtpSADs->u2SAChallengeTimeOut;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSARequestNonce
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSARequestNonce
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSARequestNonce (INT4 i4FsPtpContextId,
                           INT4 i4FsPtpDomainNumber,
                           INT4 i4FsPtpSAId, INT4 *pi4RetValFsPtpSARequestNonce)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSARequestNonce = (INT4) pPtpSADs->u4SARequestNonce;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSAResponseNonce
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSAResponseNonce
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSAResponseNonce (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSAId,
                            INT4 *pi4RetValFsPtpSAResponseNonce)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSAResponseNonce = (INT4) pPtpSADs->u4SAResponseNonce;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSAChallengeRequired
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSAChallengeRequired
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSAChallengeRequired (INT4 i4FsPtpContextId,
                                INT4 i4FsPtpDomainNumber,
                                INT4 i4FsPtpSAId,
                                INT4 *pi4RetValFsPtpSAChallengeRequired)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSAChallengeRequired =
            (INT4) pPtpSADs->bSAChallengeRequired;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSAResponseRequired
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSAResponseRequired
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSAResponseRequired (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               INT4 *pi4RetValFsPtpSAResponseRequired)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSAResponseRequired =
            (INT4) pPtpSADs->bSAResponseRequired;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSATypeField
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSATypeField
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSATypeField (INT4 i4FsPtpContextId,
                        INT4 i4FsPtpDomainNumber,
                        INT4 i4FsPtpSAId, INT4 *pi4RetValFsPtpSATypeField)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSATypeField = (INT4) pPtpSADs->SAType;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSADirection
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSADirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSADirection (INT4 i4FsPtpContextId,
                        INT4 i4FsPtpDomainNumber,
                        INT4 i4FsPtpSAId, INT4 *pi4RetValFsPtpSADirection)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSADirection = (INT4) pPtpSADs->u1SADirection;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpSARowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                retValFsPtpSARowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpSARowStatus (INT4 i4FsPtpContextId,
                        INT4 i4FsPtpDomainNumber,
                        INT4 i4FsPtpSAId, INT4 *pi4RetValFsPtpSARowStatus)
{
    tPtpSADs           *pPtpSADs = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                   (UINT1) i4FsPtpDomainNumber,
                                   (UINT4) i4FsPtpSAId);

    if (pPtpSADs == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Security Association Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpSARowStatus = (INT4) pPtpSADs->u1RowStatus;
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpSASrcPortNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSASrcPortNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSASrcPortNumber (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSAId, INT4 i4SetValFsPtpSASrcPortNumber)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSASrcPortNumber);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSASrcAddrLength
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSASrcAddrLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSASrcAddrLength (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSAId, INT4 i4SetValFsPtpSASrcAddrLength)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSASrcAddrLength);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSASrcAddr
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSASrcAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSASrcAddr (INT4 i4FsPtpContextId,
                      INT4 i4FsPtpDomainNumber,
                      INT4 i4FsPtpSAId,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsPtpSASrcAddr)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (pSetValFsPtpSASrcAddr);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSADstPortNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSADstPortNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSADstPortNumber (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSAId, INT4 i4SetValFsPtpSADstPortNumber)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSADstPortNumber);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSADstAddrLength
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSADstAddrLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSADstAddrLength (INT4 i4FsPtpContextId,
                            INT4 i4FsPtpDomainNumber,
                            INT4 i4FsPtpSAId, INT4 i4SetValFsPtpSADstAddrLength)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSADstAddrLength);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSADstAddr
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSADstAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSADstAddr (INT4 i4FsPtpContextId,
                      INT4 i4FsPtpDomainNumber,
                      INT4 i4FsPtpSAId,
                      tSNMP_OCTET_STRING_TYPE * pSetValFsPtpSADstAddr)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (pSetValFsPtpSADstAddr);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSASrcClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSASrcClockIdentity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSASrcClockIdentity (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsPtpSASrcClockIdentity)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (pSetValFsPtpSASrcClockIdentity);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSADstClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSADstClockIdentity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSADstClockIdentity (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsPtpSADstClockIdentity)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (pSetValFsPtpSADstClockIdentity);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSAKeyId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSAKeyId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSAKeyId (INT4 i4FsPtpContextId,
                    INT4 i4FsPtpDomainNumber,
                    INT4 i4FsPtpSAId, INT4 i4SetValFsPtpSAKeyId)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSAKeyId);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSANextKeyId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSANextKeyId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSANextKeyId (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
                        INT4 i4FsPtpSAId, INT4 i4SetValFsPtpSANextKeyId)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSANextKeyId);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSATrustTimeout
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSATrustTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSATrustTimeout (INT4 i4FsPtpContextId,
                           INT4 i4FsPtpDomainNumber,
                           INT4 i4FsPtpSAId, INT4 i4SetValFsPtpSATrustTimeout)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSATrustTimeout);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSAChallengeTimeOut
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSAChallengeTimeOut
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSAChallengeTimeOut (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               INT4 i4SetValFsPtpSAChallengeTimeOut)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSAChallengeTimeOut);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSAChallengeRequired
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSAChallengeRequired
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSAChallengeRequired (INT4 i4FsPtpContextId,
                                INT4 i4FsPtpDomainNumber,
                                INT4 i4FsPtpSAId,
                                INT4 i4SetValFsPtpSAChallengeRequired)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSAChallengeRequired);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSAResponseRequired
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSAResponseRequired
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSAResponseRequired (INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               INT4 i4SetValFsPtpSAResponseRequired)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSAResponseRequired);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSADirection
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSADirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSADirection (INT4 i4FsPtpContextId,
                        INT4 i4FsPtpDomainNumber,
                        INT4 i4FsPtpSAId, INT4 i4SetValFsPtpSADirection)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSADirection);
    return (INT1) SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsPtpSARowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                setValFsPtpSARowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpSARowStatus (INT4 i4FsPtpContextId,
                        INT4 i4FsPtpDomainNumber,
                        INT4 i4FsPtpSAId, INT4 i4SetValFsPtpSARowStatus)
{
    UNUSED_PARAM (i4FsPtpContextId);
    UNUSED_PARAM (i4FsPtpDomainNumber);
    UNUSED_PARAM (i4FsPtpSAId);
    UNUSED_PARAM (i4SetValFsPtpSARowStatus);
    return (INT1) SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpSASrcPortNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSASrcPortNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSASrcPortNumber (UINT4 *pu4ErrorCode,
                               INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               INT4 i4TestValFsPtpSASrcPortNumber)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4TestValFsPtpSASrcPortNumber);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSASrcAddrLength
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSASrcAddrLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSASrcAddrLength (UINT4 *pu4ErrorCode,
                               INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               INT4 i4TestValFsPtpSASrcAddrLength)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpSASrcAddrLength < 0) ||
        (i4TestValFsPtpSASrcAddrLength > PTP_MAX_ADDR_LENGTH))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for source address length\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSASrcAddr
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSASrcAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSASrcAddr (UINT4 *pu4ErrorCode,
                         INT4 i4FsPtpContextId,
                         INT4 i4FsPtpDomainNumber,
                         INT4 i4FsPtpSAId,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsPtpSASrcAddr)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((pTestValFsPtpSASrcAddr->i4_Length < 0) ||
        (pTestValFsPtpSASrcAddr->i4_Length > PTP_MAX_ADDR_LENGTH))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for source address length\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSADstPortNumber
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSADstPortNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSADstPortNumber (UINT4 *pu4ErrorCode,
                               INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               INT4 i4TestValFsPtpSADstPortNumber)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4TestValFsPtpSADstPortNumber);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSADstAddrLength
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSADstAddrLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSADstAddrLength (UINT4 *pu4ErrorCode,
                               INT4 i4FsPtpContextId,
                               INT4 i4FsPtpDomainNumber,
                               INT4 i4FsPtpSAId,
                               INT4 i4TestValFsPtpSADstAddrLength)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpSADstAddrLength < 0) ||
        (i4TestValFsPtpSADstAddrLength > PTP_MAX_ADDR_LENGTH))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for destination address length\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSADstAddr
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSADstAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSADstAddr (UINT4 *pu4ErrorCode,
                         INT4 i4FsPtpContextId,
                         INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSAId,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsPtpSADstAddr)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((pTestValFsPtpSADstAddr->i4_Length < 0) ||
        (pTestValFsPtpSADstAddr->i4_Length > PTP_MAX_ADDR_LENGTH))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for destination address length\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSASrcClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSASrcClockIdentity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSASrcClockIdentity (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPtpContextId,
                                  INT4 i4FsPtpDomainNumber,
                                  INT4 i4FsPtpSAId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsPtpSASrcClockIdentity)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((pTestValFsPtpSASrcClockIdentity->i4_Length < 0) ||
        (pTestValFsPtpSASrcClockIdentity->i4_Length > PTP_MAX_CLOCK_ID_LEN))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for clock id\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSADstClockIdentity
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSADstClockIdentity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSADstClockIdentity (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPtpContextId,
                                  INT4 i4FsPtpDomainNumber,
                                  INT4 i4FsPtpSAId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsPtpSADstClockIdentity)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((pTestValFsPtpSADstClockIdentity->i4_Length < 0) ||
        (pTestValFsPtpSADstClockIdentity->i4_Length >= PTP_MAX_CLOCK_ID_LEN))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for clock id\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSAKeyId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSAKeyId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSAKeyId (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
                       INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSAId,
                       INT4 i4TestValFsPtpSAKeyId)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4TestValFsPtpSAKeyId);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSANextKeyId
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSANextKeyId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSANextKeyId (UINT4 *pu4ErrorCode,
                           INT4 i4FsPtpContextId,
                           INT4 i4FsPtpDomainNumber,
                           INT4 i4FsPtpSAId, INT4 i4TestValFsPtpSANextKeyId)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4TestValFsPtpSANextKeyId);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSATrustTimeout
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSATrustTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSATrustTimeout (UINT4 *pu4ErrorCode,
                              INT4 i4FsPtpContextId,
                              INT4 i4FsPtpDomainNumber,
                              INT4 i4FsPtpSAId,
                              INT4 i4TestValFsPtpSATrustTimeout)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4TestValFsPtpSATrustTimeout);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSAChallengeTimeOut
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSAChallengeTimeOut
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSAChallengeTimeOut (UINT4 *pu4ErrorCode,
                                  INT4 i4FsPtpContextId,
                                  INT4 i4FsPtpDomainNumber,
                                  INT4 i4FsPtpSAId,
                                  INT4 i4TestValFsPtpSAChallengeTimeOut)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4TestValFsPtpSAChallengeTimeOut);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSAChallengeRequired
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSAChallengeRequired
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpSAChallengeRequired
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSAId,
     INT4 i4TestValFsPtpSAChallengeRequired)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpSAChallengeRequired != PTP_ENABLED) &&
        (i4TestValFsPtpSAChallengeRequired != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid truth value for challenge required\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSAResponseRequired
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSAResponseRequired
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpSAResponseRequired
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpSAId,
     INT4 i4TestValFsPtpSAResponseRequired)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpSAResponseRequired != PTP_ENABLED) &&
        (i4TestValFsPtpSAResponseRequired != PTP_DISABLED))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid truth value for response required\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSADirection
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSADirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSADirection (UINT4 *pu4ErrorCode,
                           INT4 i4FsPtpContextId,
                           INT4 i4FsPtpDomainNumber,
                           INT4 i4FsPtpSAId, INT4 i4TestValFsPtpSADirection)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((i4TestValFsPtpSADirection != PTP_SA_IN) &&
        (i4TestValFsPtpSADirection != PTP_SA_OUT))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid value for SA direction\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                            (UINT1) i4FsPtpDomainNumber,
                            (UINT4) i4FsPtpSAId, pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "SA Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsPtpSARowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId

                The Object 
                testValFsPtpSARowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpSARowStatus (UINT4 *pu4ErrorCode,
                           INT4 i4FsPtpContextId,
                           INT4 i4FsPtpDomainNumber,
                           INT4 i4FsPtpSAId, INT4 i4TestValFsPtpSARowStatus)
{
    tPtpSADs           *pPtpSADs = NULL;
    tPtpDomain         *pPtpDomain = NULL;
    INT4                i4RetStatus = OSIX_SUCCESS;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i4RetStatus = OSIX_FAILURE;
    }

    if (i4TestValFsPtpSARowStatus == DESTROY)
    {
        return (INT1) SNMP_SUCCESS;
    }

    i4RetStatus = PtpValValidateSADS ((UINT4) i4FsPtpContextId,
                                      (UINT1) i4FsPtpDomainNumber,
                                      (UINT4) i4FsPtpSAId, pu4ErrorCode);

    if (i4RetStatus == OSIX_SUCCESS)
    {
        pPtpSADs = PtpSecSAGetSAEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber,
                                       (UINT4) i4FsPtpSAId);

        if (pPtpSADs == NULL)
        {
            PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                      MGMT_TRC | ALL_FAILURE_TRC,
                      "Security Association Data Set does not exist\r\n"));
            i4RetStatus = OSIX_FAILURE;
        }
    }

    switch (i4TestValFsPtpSARowStatus)
    {
        case ACTIVE:
        {
            if (i4RetStatus == OSIX_FAILURE)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            else
            {
                if (pPtpSADs != NULL)
                {
                    if ((pPtpSADs->SASrcAddr.u2AddrLength == 0) ||
                        (pPtpSADs->SADstAddr.u2AddrLength == 0) ||
                        (pPtpSADs->u2SASrcPortIndex == 0) ||
                        (pPtpSADs->u2SADstPortIndex == 0) ||
                        (pPtpSADs->u1SADirection == 0))
                    {
                        *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                        i1RetVal = (INT1) SNMP_FAILURE;
                    }
                }
            }
            break;
        }
        case NOT_IN_SERVICE:
        {
            if (i4RetStatus == OSIX_FAILURE)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }
        case CREATE_AND_GO:
        {
            *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
        }
        case CREATE_AND_WAIT:
        {
            if (i4RetStatus == OSIX_SUCCESS)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
                CLI_SET_ERR (CLI_PTP_ERR_SA_PRESENT);
            }
            else
            {
                pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                                   (UINT1) i4FsPtpDomainNumber);
                if (pPtpDomain == NULL)
                {
                    PTP_TRC (((UINT4) i4FsPtpContextId,
                              (UINT1) i4FsPtpDomainNumber,
                              MGMT_TRC | ALL_FAILURE_TRC,
                              "Domain does not exists\r\n"));
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    i1RetVal = (INT1) SNMP_FAILURE;
                }
            }
            break;
        }

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpSADataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpSAId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPtpSADataSetTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPtpAltTimeScaleDataSetTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPtpAltTimeScaleDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1                nmhValidateIndexInstanceFsPtpAltTimeScaleDataSetTable
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPtpAltTimeScaleDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1                nmhGetFirstIndexFsPtpAltTimeScaleDataSetTable
    (INT4 *pi4FsPtpContextId, INT4 *pi4FsPtpDomainNumber,
     INT4 *pi4FsPtpAltTimeScaleKeyId)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;

    }

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetFirstNode (gPtpGlobalInfo.AltTimSclTree,
                           (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4FsPtpContextId = (INT4) pPtpAltTimescaleEntry->u4ContextId;
        *pi4FsPtpDomainNumber = (INT4) pPtpAltTimescaleEntry->u1DomainId;
        *pi4FsPtpAltTimeScaleKeyId = (INT4) pPtpAltTimescaleEntry->u1KeyId;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPtpAltTimeScaleDataSetTable
 Input       :  The Indices
                FsPtpContextId
                nextFsPtpContextId
                FsPtpDomainNumber
                nextFsPtpDomainNumber
                FsPtpAltTimeScaleKeyId
                nextFsPtpAltTimeScaleKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1                nmhGetNextIndexFsPtpAltTimeScaleDataSetTable
    (INT4 i4FsPtpContextId, INT4 *pi4NextFsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 *pi4NextFsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId, INT4 *pi4NextFsPtpAltTimeScaleKeyId)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;

    }

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNextNode (gPtpGlobalInfo.AltTimSclTree,
                          &(ptpAltTimescaleEntry),
                          (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4NextFsPtpContextId = (INT4) pPtpAltTimescaleEntry->u4ContextId;
        *pi4NextFsPtpDomainNumber = (INT4) pPtpAltTimescaleEntry->u1DomainId;
        *pi4NextFsPtpAltTimeScaleKeyId = (INT4) pPtpAltTimescaleEntry->u1KeyId;
    }

    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpAltTimeScalecurrentOffset
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                retValFsPtpAltTimeScalecurrentOffset
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpAltTimeScalecurrentOffset
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId,
     INT4 *pi4RetValFsPtpAltTimeScalecurrentOffset)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpAltTimeScalecurrentOffset
            = (INT4) pPtpAltTimescaleEntry->u4CurrentOffset;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpAltTimeScalejumpSeconds
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                retValFsPtpAltTimeScalejumpSeconds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpAltTimeScalejumpSeconds
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId, INT4 *pi4RetValFsPtpAltTimeScalejumpSeconds)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpAltTimeScalejumpSeconds =
            (INT4) pPtpAltTimescaleEntry->u4JumpSeconds;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpAltTimeScaletimeOfNextJump
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                retValFsPtpAltTimeScaletimeOfNextJump
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpAltTimeScaletimeOfNextJump
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPtpAltTimeScaletimeOfNextJump)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        FSAP_U8_2STR (&(pPtpAltTimescaleEntry->u8TimeOfNextJump),
                      (CHR1 *) (pRetValFsPtpAltTimeScaletimeOfNextJump->
                                pu1_OctetList));

        pRetValFsPtpAltTimeScaletimeOfNextJump->i4_Length =
            STRLEN ((pRetValFsPtpAltTimeScaletimeOfNextJump->pu1_OctetList));
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpAltTimeScaledisplayName
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                retValFsPtpAltTimeScaledisplayName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpAltTimeScaledisplayName
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId,
     tSNMP_OCTET_STRING_TYPE * pRetValFsPtpAltTimeScaledisplayName)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        pRetValFsPtpAltTimeScaledisplayName->i4_Length =
            STRLEN (pPtpAltTimescaleEntry->au1DisplayName);

        MEMCPY (pRetValFsPtpAltTimeScaledisplayName->pu1_OctetList,
                pPtpAltTimescaleEntry->au1DisplayName,
                pRetValFsPtpAltTimeScaledisplayName->i4_Length);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsPtpAltTimeScaleRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                retValFsPtpAltTimeScaleRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetFsPtpAltTimeScaleRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId, INT4 *pi4RetValFsPtpAltTimeScaleRowStatus)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        *pi4RetValFsPtpAltTimeScaleRowStatus =
            (INT4) pPtpAltTimescaleEntry->u1Status;
    }

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpAltTimeScalecurrentOffset
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                setValFsPtpAltTimeScalecurrentOffset
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpAltTimeScalecurrentOffset
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId, INT4 i4SetValFsPtpAltTimeScalecurrentOffset)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpAltTimescaleEntry->u4CurrentOffset !=
            (UINT4) i4SetValFsPtpAltTimeScalecurrentOffset)
        {
            pPtpAltTimescaleEntry->u4CurrentOffset =
                (UINT4) i4SetValFsPtpAltTimeScalecurrentOffset;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpAltTimeScalejumpSeconds
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                setValFsPtpAltTimeScalejumpSeconds
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpAltTimeScalejumpSeconds
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId, INT4 i4SetValFsPtpAltTimeScalejumpSeconds)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (pPtpAltTimescaleEntry->u4JumpSeconds !=
            (UINT4) i4SetValFsPtpAltTimeScalejumpSeconds)
        {
            pPtpAltTimescaleEntry->u4JumpSeconds =
                (UINT4) i4SetValFsPtpAltTimeScalejumpSeconds;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpAltTimeScaletimeOfNextJump
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                setValFsPtpAltTimeScaletimeOfNextJump
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpAltTimeScaletimeOfNextJump
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId,
     tSNMP_OCTET_STRING_TYPE * pSetValFsPtpAltTimeScaletimeOfNextJump)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    FS_UINT8            u8TmpTimeOfNextJump;
    UINT1               u1U8Str[PTP_U8_STR_LEN];
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    MEMSET (&u1U8Str[0], '\0', PTP_U8_STR_LEN);
    MEMCPY (&u1U8Str[0], pSetValFsPtpAltTimeScaletimeOfNextJump->pu1_OctetList,
            pSetValFsPtpAltTimeScaletimeOfNextJump->i4_Length);

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        FSAP_STR2_U8 ((CHR1 *) (&u1U8Str[0]), &(u8TmpTimeOfNextJump));

        if (FSAP_U8_CMP (&(u8TmpTimeOfNextJump),
                         &(pPtpAltTimescaleEntry->u8TimeOfNextJump)) != 0)
        {
            FSAP_U8_ASSIGN (&(pPtpAltTimescaleEntry->u8TimeOfNextJump),
                            &(u8TmpTimeOfNextJump));
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpAltTimeScaledisplayName
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                setValFsPtpAltTimeScaledisplayName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpAltTimeScaledisplayName
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId,
     tSNMP_OCTET_STRING_TYPE * pSetValFsPtpAltTimeScaledisplayName)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    if (pPtpAltTimescaleEntry == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale Data Set does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }
    else
    {
        if (MEMCMP (pSetValFsPtpAltTimeScaledisplayName->pu1_OctetList,
                    pPtpAltTimescaleEntry->au1DisplayName,
                    pSetValFsPtpAltTimeScaledisplayName->i4_Length) != 0)
        {
            MEMSET (pPtpAltTimescaleEntry->au1DisplayName, 0,
                    sizeof (pPtpAltTimescaleEntry->au1DisplayName));
            MEMCPY (pPtpAltTimescaleEntry->au1DisplayName,
                    pSetValFsPtpAltTimeScaledisplayName->pu1_OctetList,
                    pSetValFsPtpAltTimeScaledisplayName->i4_Length);
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsPtpAltTimeScaleRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                setValFsPtpAltTimeScaleRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFsPtpAltTimeScaleRowStatus
    (INT4 i4FsPtpContextId, INT4 i4FsPtpDomainNumber,
     INT4 i4FsPtpAltTimeScaleKeyId, INT4 i4SetValFsPtpAltTimeScaleRowStatus)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpAltTimescaleEntry ptpAltTimescaleEntry;
    INT1                i1RetVal = (INT1) SNMP_FAILURE;

    ptpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    ptpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    ptpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(ptpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    switch (i4SetValFsPtpAltTimeScaleRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpAltTimescaleEntry != NULL)
            {
                pPtpAltTimescaleEntry->u1Status =
                    (UINT1) i4SetValFsPtpAltTimeScaleRowStatus;
                i1RetVal = (INT1) SNMP_SUCCESS;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if ((pPtpAltTimescaleEntry == NULL) &&
                ((pPtpAltTimescaleEntry =
                  PtpAlTsCreate ((UINT4) i4FsPtpContextId,
                                 (UINT1) i4FsPtpDomainNumber,
                                 (UINT1) i4FsPtpAltTimeScaleKeyId)) != NULL))
            {
                i1RetVal = (INT1) SNMP_SUCCESS;

                MEMCPY (pPtpAltTimescaleEntry->au1DisplayName,
                        PTP_ALT_TIME_DEF_NAME, sizeof (PTP_ALT_TIME_DEF_NAME));

                if (i4SetValFsPtpAltTimeScaleRowStatus == CREATE_AND_WAIT)
                {
                    pPtpAltTimescaleEntry->u1Status = (UINT1) NOT_IN_SERVICE;
                }
                else
                {
                    pPtpAltTimescaleEntry->u1Status = (UINT1) ACTIVE;
                }
            }
            break;
        }
        case DESTROY:
        {
            i1RetVal = (INT1) SNMP_SUCCESS;
            if (pPtpAltTimescaleEntry != NULL)
            {
                pPtpAltTimescaleEntry->u1Status =
                    (UINT1) i4SetValFsPtpAltTimeScaleRowStatus;

                if ((PtpAlTsHandleUpdate ((UINT4) i4FsPtpContextId,
                                          (UINT1) i4FsPtpDomainNumber)) ==
                    OSIX_FAILURE)
                {
                    i1RetVal = (INT1) SNMP_FAILURE;
                }

                PtpAlTsDelete (pPtpAltTimescaleEntry);
            }
        }
            break;

        default:
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    /* Call the Protocol Configuration handler for the Alternate 
     * timescale option */
    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4SetValFsPtpAltTimeScaleRowStatus != DESTROY)
        {
            if ((PtpAlTsHandleUpdate ((UINT4) i4FsPtpContextId,
                                      (UINT1) i4FsPtpDomainNumber)) ==
                OSIX_FAILURE)
            {
                i1RetVal = (INT1) SNMP_FAILURE;
            }
        }
    }

    return i1RetVal;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpAltTimeScalecurrentOffset
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                testValFsPtpAltTimeScalecurrentOffset
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpAltTimeScalecurrentOffset
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpAltTimeScaleKeyId,
     INT4 i4TestValFsPtpAltTimeScalecurrentOffset)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4TestValFsPtpAltTimeScalecurrentOffset);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateAltTimeDS ((UINT4) i4FsPtpContextId,
                                 (UINT1) i4FsPtpDomainNumber,
                                 (UINT1) i4FsPtpAltTimeScaleKeyId,
                                 pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale entry does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpAltTimeScalejumpSeconds
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                testValFsPtpAltTimeScalejumpSeconds
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpAltTimeScalejumpSeconds
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpAltTimeScaleKeyId,
     INT4 i4TestValFsPtpAltTimeScalejumpSeconds)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    UNUSED_PARAM (i4TestValFsPtpAltTimeScalejumpSeconds);

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (PtpValValidateAltTimeDS ((UINT4) i4FsPtpContextId,
                                 (UINT1) i4FsPtpDomainNumber,
                                 (UINT1) i4FsPtpAltTimeScaleKeyId,
                                 pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale entry does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpAltTimeScaletimeOfNextJump
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                testValFsPtpAltTimeScaletimeOfNextJump
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpAltTimeScaletimeOfNextJump
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpAltTimeScaleKeyId,
     tSNMP_OCTET_STRING_TYPE * pTestValFsPtpAltTimeScaletimeOfNextJump)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if (pTestValFsPtpAltTimeScaletimeOfNextJump->i4_Length >= PTP_U8_STR_LEN)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for scale time of next jump\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateAltTimeDS ((UINT4) i4FsPtpContextId,
                                 (UINT1) i4FsPtpDomainNumber,
                                 (UINT1) i4FsPtpAltTimeScaleKeyId,
                                 pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale entry does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpAltTimeScaledisplayName
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                testValFsPtpAltTimeScaledisplayName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpAltTimeScaledisplayName
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpAltTimeScaleKeyId,
     tSNMP_OCTET_STRING_TYPE * pTestValFsPtpAltTimeScaledisplayName)
{
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return i1RetVal;

    }

    if ((pTestValFsPtpAltTimeScaledisplayName->i4_Length
         < PTP_ALT_TIME_DISPLAY_MIN_LEN) ||
        (pTestValFsPtpAltTimeScaledisplayName->i4_Length >
         PTP_ALT_TIME_NAME_MAX_LEN))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Invalid length for scale time of next jump\r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (PtpValValidateAltTimeDS ((UINT4) i4FsPtpContextId,
                                 (UINT1) i4FsPtpDomainNumber,
                                 (UINT1) i4FsPtpAltTimeScaleKeyId,
                                 pu4ErrorCode) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC,
                  "Alternate time scale entry does not exist\r\n"));
        i1RetVal = (INT1) SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsPtpAltTimeScaleRowStatus
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId

                The Object 
                testValFsPtpAltTimeScaleRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhTestv2FsPtpAltTimeScaleRowStatus
    (UINT4 *pu4ErrorCode, INT4 i4FsPtpContextId,
     INT4 i4FsPtpDomainNumber, INT4 i4FsPtpAltTimeScaleKeyId,
     INT4 i4TestValFsPtpAltTimeScaleRowStatus)
{
    tPtpAltTimescaleEntry PtpAltTimescaleEntry;
    tPtpAltTimescaleEntry *pPtpAltTimescaleEntry = NULL;
    tPtpDomain         *pPtpDomain = NULL;
    INT1                i1RetVal = (INT1) SNMP_SUCCESS;

    if (gPtpGlobalInfo.u1SysCtrlStatus == (UINT1) PTP_SHUTDOWN)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, (UINT1) PTP_MAX_DOMAINS,
                  MGMT_TRC | ALL_FAILURE_TRC, "PTP module is shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_PTP_ERR_MODULE_SHUTDOWN);
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if (i4TestValFsPtpAltTimeScaleRowStatus == DESTROY)
    {
        return (INT1) SNMP_SUCCESS;
    }

    if ((i4FsPtpContextId > PTP_MAX_CONTEXT_ID) ||
        (i4FsPtpDomainNumber >= PTP_MAX_DOMAINS) ||
        (i4FsPtpAltTimeScaleKeyId < (UINT1) PTP_ALT_TIME_MIN_KEY_ID) ||
        (i4FsPtpAltTimeScaleKeyId > (UINT1) PTP_ALT_TIME_MAX_KEY_ID))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    pPtpDomain = PtpDmnGetDomainEntry ((UINT4) i4FsPtpContextId,
                                       (UINT1) i4FsPtpDomainNumber);
    if (pPtpDomain == NULL)
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Domain does not exists\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        i1RetVal = (INT1) SNMP_FAILURE;
        return i1RetVal;
    }

    if ((pPtpDomain->ClkMode == PTP_TRANSPARENT_CLOCK_MODE) ||
        (pPtpDomain->ClkMode == PTP_FORWARD_MODE))
    {
        PTP_TRC (((UINT4) i4FsPtpContextId, (UINT1) i4FsPtpDomainNumber,
                  MGMT_TRC | ALL_FAILURE_TRC, "Clock mode is not "
                  "Ordinary or Boundary\r\n"));

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_PTP_ERR_NOT_ORDINARY_BOUNDARY_CLK);

        return SNMP_FAILURE;
    }

    PtpAltTimescaleEntry.u4ContextId = (UINT4) i4FsPtpContextId;
    PtpAltTimescaleEntry.u1DomainId = (UINT1) i4FsPtpDomainNumber;
    PtpAltTimescaleEntry.u1KeyId = (UINT1) i4FsPtpAltTimeScaleKeyId;

    pPtpAltTimescaleEntry = (tPtpAltTimescaleEntry *)
        PtpDbGetNode (gPtpGlobalInfo.AltTimSclTree,
                      &(PtpAltTimescaleEntry), (UINT1) PTP_ALT_TIME_DATA_SET);

    switch (i4TestValFsPtpAltTimeScaleRowStatus)
    {
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pPtpAltTimescaleEntry == NULL)
            {
                *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                i1RetVal = (INT1) SNMP_FAILURE;
                CLI_SET_ERR (CLI_PTP_ERR_ATS_NOT_PRESENT);
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if ((i1RetVal == (INT1) SNMP_FAILURE) ||
                (pPtpAltTimescaleEntry != NULL))
            {
                if (i4TestValFsPtpAltTimeScaleRowStatus == CREATE_AND_GO)
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_INCONSISTENT_VALUE;
                }
                else
                {
                    *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
                }
                CLI_SET_ERR (CLI_PTP_ERR_ATS_PRESENT);
                i1RetVal = (INT1) SNMP_FAILURE;
            }
            break;
        }

        default:
            *pu4ErrorCode = (UINT4) SNMP_ERR_WRONG_VALUE;
            i1RetVal = (INT1) SNMP_FAILURE;
            break;
    }

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpAltTimeScaleDataSetTable
 Input       :  The Indices
                FsPtpContextId
                FsPtpDomainNumber
                FsPtpAltTimeScaleKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhDepv2FsPtpAltTimeScaleDataSetTable
    (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPtpGlobalErrTrapType
 Input       :  The Indices

                The Object 
                retValFsPtpGlobalErrTrapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpGlobalErrTrapType (INT4 *pi4RetValFsPtpGlobalErrTrapType)
{
    UNUSED_PARAM (pi4RetValFsPtpGlobalErrTrapType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPtpNotification
 Input       :  The Indices

                The Object 
                retValFsPtpNotification
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPtpNotification (tSNMP_OCTET_STRING_TYPE * pRetValFsPtpNotification)
{

    MEMCPY (pRetValFsPtpNotification->pu1_OctetList,
            (UINT1 *) &gPtpGlobalInfo.u2TrapControl, sizeof (UINT2));

    if (gPtpGlobalInfo.u2TrapControl != 0)
    {
        pRetValFsPtpNotification->i4_Length = sizeof (UINT2);
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPtpNotification
 Input       :  The Indices

                The Object 
                setValFsPtpNotification
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPtpNotification (tSNMP_OCTET_STRING_TYPE * pSetValFsPtpNotification)
{
    MEMCPY (&gPtpGlobalInfo.u2TrapControl,
            pSetValFsPtpNotification->pu1_OctetList, sizeof (UINT2));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPtpNotification
 Input       :  The Indices

                The Object 
                testValFsPtpNotification
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPtpNotification (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsPtpNotification)
{
    UINT2               u2TmpTrapValue = 0;

    if (pTestValFsPtpNotification->i4_Length > (INT4) sizeof (UINT2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        /*Invalid Value */
        return (INT1) SNMP_FAILURE;
    }

    /* Only eight traps are supported currently in PTP, so if any bit outside
     * the first 8 bits is set, then failure is returned
     */
    u2TmpTrapValue = (UINT2) pTestValFsPtpNotification->pu1_OctetList[1];

    if (u2TmpTrapValue > PTP_POSITIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (INT1) SNMP_FAILURE;
    }

    return (INT1) SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPtpNotification
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPtpNotification (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
