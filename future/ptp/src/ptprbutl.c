/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptprbutl.c,v 1.3 2014/01/24 12:20:09 siva Exp $
 *
 * Description: This file contains PTP trace implementation.       
 *********************************************************************/
#ifndef _PTPRBUTL_C_
#define _PTPRBUTL_C_

#include "ptpincs.h"

PRIVATE INT4
    PtpRbUtlCompCxtAndDmn PROTO ((UINT4 u4CxtIdInput, UINT4 u4CxtIdNode,
                                  UINT1 u1DmdIdInput, UINT1 u1DmdIdNode));

PRIVATE INT4
    PtpRbUtlCompMasterDS PROTO ((tPtpMasterIndex * pPtpMasterIndexInput,
                                 tPtpMasterIndex * pPtpMasterIndexNode));

/*****************************************************************************/
/* Function                  : PtpRbUtlCreateRbTrees                         */
/*                                                                           */
/* Description               : This routine creates RBTrees for PTP module.  */
/*                             For each RBTree created, it also disables the */
/*                             RBTree's properietary mutual exclusion lock.  */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.                               */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS / OSIX_FAILURE                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCreateRbTrees (VOID)
{
    UINT4               u4OffSet = 0;

    PTP_FN_ENTRY ();

    /* RBTree Creation follows the following steps.
     * 1) Calculate the offset for the RBNode present in the structure
     * 2) Create a RBTree with the calculated offset and cmp utility
     * 3) Disable RBTree's own mutual exclusion lock
     * */

    u4OffSet = FSAP_OFFSETOF (tPtpCxt, RbNode);
    gPtpGlobalInfo.ContextTree = RBTreeCreateEmbedded (u4OffSet,
                                                       PtpRbUtlCompCxtDS);
    if (gPtpGlobalInfo.ContextTree == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "RBTree Creation for Context Tree Failed!!!!\r\n"));
        return OSIX_FAILURE;
    }

    RBTreeDisableMutualExclusion (gPtpGlobalInfo.ContextTree);

    u4OffSet = FSAP_OFFSETOF (tPtpPort, RbNode);
    gPtpGlobalInfo.PortTree = RBTreeCreateEmbedded (u4OffSet,
                                                    PtpRbUtlCompPortDS);
    if (gPtpGlobalInfo.PortTree == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "RBTree Creation for Port Tree Failed!!!!\r\n"));
        return OSIX_FAILURE;
    }

    RBTreeDisableMutualExclusion (gPtpGlobalInfo.PortTree);

    /* Foreign Master Tree */
    u4OffSet = FSAP_OFFSETOF (tPtpForeignMasterDs, RbNode);
    gPtpGlobalInfo.FMTree = RBTreeCreateEmbedded (u4OffSet,
                                                  PtpRbUtlCompFgnMasterDS);
    if (gPtpGlobalInfo.FMTree == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "RBTree Creation for Foreign Master Tree Failed!!!!\r\n"));
        return OSIX_FAILURE;
    }

    RBTreeDisableMutualExclusion (gPtpGlobalInfo.FMTree);

    /* Domain Tree */
    u4OffSet = FSAP_OFFSETOF (tPtpDomain, RbNode);

    gPtpGlobalInfo.DomainTree = RBTreeCreateEmbedded (u4OffSet,
                                                      PtpRbUtlCompDomainDS);
    if (gPtpGlobalInfo.DomainTree == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "RBTree Creation for Domain Tree Failed!!!!\r\n"));
        return OSIX_FAILURE;
    }

    RBTreeDisableMutualExclusion (gPtpGlobalInfo.DomainTree);

    /* GrandMasterCluster Table */
    u4OffSet = FSAP_OFFSETOF (tPtpGrandMasterTable, RbNode);

    gPtpGlobalInfo.GrandMastClustLst =
        RBTreeCreateEmbedded (u4OffSet, PtpRbUtlCompGrandMasterDS);
    if (gPtpGlobalInfo.GrandMastClustLst == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "RBTree Creation for Grand Master Tree Failed!!!!\r\n"));
        return OSIX_FAILURE;
    }

    RBTreeDisableMutualExclusion (gPtpGlobalInfo.GrandMastClustLst);

    /* Acceptable MasterCluster Table */
    u4OffSet = FSAP_OFFSETOF (tPtpAccMasterTable, RbNode);

    gPtpGlobalInfo.AccMastLst = RBTreeCreateEmbedded (u4OffSet,
                                                      PtpRbUtlCompAccMasterDS);
    if (gPtpGlobalInfo.AccMastLst == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "RBTree Creation for Acceptable Master Tree Failed!!!!\r\n"));
        return OSIX_FAILURE;
    }
    RBTreeDisableMutualExclusion (gPtpGlobalInfo.AccMastLst);

    /* Security Key Table */
    u4OffSet = FSAP_OFFSETOF (tPtpSecKeyDs, RbNode);

    gPtpGlobalInfo.SecKeyLst = RBTreeCreateEmbedded (u4OffSet,
                                                     PtpRbUtlCompSecKeyDS);
    if (gPtpGlobalInfo.SecKeyLst == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "RBTree Creation for Security Association Tree Failed!\r\n"));
        return OSIX_FAILURE;
    }
    RBTreeDisableMutualExclusion (gPtpGlobalInfo.SecKeyLst);

    /* Unicast Master Table */
    u4OffSet = FSAP_OFFSETOF (tPtpAccUnicastMasterTable, RbNode);

    gPtpGlobalInfo.UnicastMastLst =
        RBTreeCreateEmbedded (u4OffSet, PtpRbUtlCompUnicastMasterDS);
    if (gPtpGlobalInfo.UnicastMastLst == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "RBTree Creation for Unicast Master Tree Failed!!!\r\n"));
        return OSIX_FAILURE;
    }
    RBTreeDisableMutualExclusion (gPtpGlobalInfo.UnicastMastLst);

    /* Compatability Table */
    u4OffSet = FSAP_OFFSETOF (tPtpCompDBEntry, RbNode);

    gPtpGlobalInfo.CompTree = RBTreeCreateEmbedded (u4OffSet,
                                                    PtpRbUtlCompVersionDb);

    if (gPtpGlobalInfo.CompTree == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "RBTree Creation for Compatability Tree Failed!!!\r\n"));
        return OSIX_FAILURE;
    }
    RBTreeDisableMutualExclusion (gPtpGlobalInfo.CompTree);

    /* Alternate Time scale Table */
    u4OffSet = FSAP_OFFSETOF (tPtpAltTimescaleEntry, RbNode);

    gPtpGlobalInfo.AltTimSclTree =
        RBTreeCreateEmbedded (u4OffSet, PtpRbUtlCompAltTimescaleDS);

    if (gPtpGlobalInfo.AltTimSclTree == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                  "RBTree Creation for AlternateTime "
                  "scale Tree Failed!!!\r\n"));
        return OSIX_FAILURE;
    }
    RBTreeDisableMutualExclusion (gPtpGlobalInfo.CompTree);

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpRbUtlDestroyRbTrees                        */
/*                                                                           */
/* Description               : This routine destroy RBTrees for PTP module.  */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.                               */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpRbUtlDestroyRbTrees (VOID)
{
    PTP_FN_ENTRY ();

    if (gPtpGlobalInfo.ContextTree != NULL)
    {
        RBTreeDestroy (gPtpGlobalInfo.ContextTree, NULL, 0);
        gPtpGlobalInfo.ContextTree = NULL;
    }

    if (gPtpGlobalInfo.PortTree != NULL)
    {
        RBTreeDestroy (gPtpGlobalInfo.PortTree, NULL, 0);
        gPtpGlobalInfo.PortTree = NULL;
    }

    if (gPtpGlobalInfo.FMTree != NULL)
    {
        RBTreeDestroy (gPtpGlobalInfo.FMTree, NULL, 0);
        gPtpGlobalInfo.FMTree = NULL;
    }

    if (gPtpGlobalInfo.DomainTree != NULL)
    {
        RBTreeDestroy (gPtpGlobalInfo.DomainTree, NULL, 0);
        gPtpGlobalInfo.DomainTree = NULL;
    }

    if (gPtpGlobalInfo.GrandMastClustLst != NULL)
    {
        RBTreeDestroy (gPtpGlobalInfo.GrandMastClustLst, NULL, 0);
        gPtpGlobalInfo.GrandMastClustLst = NULL;
    }

    if (gPtpGlobalInfo.AccMastLst != NULL)
    {
        RBTreeDestroy (gPtpGlobalInfo.AccMastLst, NULL, 0);
        gPtpGlobalInfo.AccMastLst = NULL;
    }

    if (gPtpGlobalInfo.SecKeyLst != NULL)
    {
        RBTreeDestroy (gPtpGlobalInfo.SecKeyLst, NULL, 0);
        gPtpGlobalInfo.SecKeyLst = NULL;
    }

    if (gPtpGlobalInfo.UnicastMastLst != NULL)
    {
        RBTreeDestroy (gPtpGlobalInfo.UnicastMastLst, NULL, 0);
        gPtpGlobalInfo.UnicastMastLst = NULL;
    }

    if (gPtpGlobalInfo.CompTree != NULL)
    {
        RBTreeDestroy (gPtpGlobalInfo.CompTree, NULL, 0);
        gPtpGlobalInfo.CompTree = NULL;
    }

    if (gPtpGlobalInfo.AltTimSclTree != NULL)
    {
        RBTreeDestroy (gPtpGlobalInfo.AltTimSclTree, NULL, 0);
        gPtpGlobalInfo.AltTimSclTree = NULL;
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompCxtDS                             */
/*                                                                           */
/* Description               : Comparison function for PTP context           */
/*                                                                           */
/* Input                     : pRbInput   -   Node given as input to RB Tree */
/*                             pRbNode    -   Node present in RB Tree        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCompCxtDS (tRBElem * pRbInput, tRBElem * pRbNode)
{
    tPtpCxt            *pPtpCxtInput = (tPtpCxt *) pRbInput;
    tPtpCxt            *pPtpCxtNode = (tPtpCxt *) pRbNode;

    PTP_FN_ENTRY ();

    if (pPtpCxtInput->u4ContextId < pPtpCxtNode->u4ContextId)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpCxtInput->u4ContextId > pPtpCxtNode->u4ContextId)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    PTP_FN_EXIT ();
    return RB_EQUAL;
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompDomainDS                          */
/*                                                                           */
/* Description               : Comparison function for Domain data set       */
/*                                                                           */
/* Input                     : pRbInput   -   Node given as input to RB Tree */
/*                             pRbNode    -   Node present in RB Tree        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCompDomainDS (tRBElem * pRbInput, tRBElem * pRbNode)
{
    tPtpDomain         *pPtpDomainInput = (tPtpDomain *) pRbInput;
    tPtpDomain         *pPtpDomainNode = (tPtpDomain *) pRbNode;
    INT4                i4RetVal = 0;

    PTP_FN_ENTRY ();

    i4RetVal = PtpRbUtlCompCxtAndDmn (pPtpDomainInput->u4ContextId,
                                      pPtpDomainNode->u4ContextId,
                                      pPtpDomainInput->u1DomainId,
                                      pPtpDomainNode->u1DomainId);
    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompPortDS                            */
/*                                                                           */
/* Description               : Comparison function for Port config data set  */
/*                                                                           */
/* Input                     : pRbInput   -   Node given as input to RB Tree */
/*                             pRbNode    -   Node present in RB Tree        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCompPortDS (tRBElem * pRbInput, tRBElem * pRbNode)
{
    tPtpPort           *pPtpPortInput = (tPtpPort *) pRbInput;
    tPtpPort           *pPtpPortNode = (tPtpPort *) pRbNode;
    INT4                i4RetVal = RB_LESSER;

    PTP_FN_ENTRY ();

    i4RetVal = PtpRbUtlCompCxtAndDmn (pPtpPortInput->u4ContextId,
                                      pPtpPortNode->u4ContextId,
                                      pPtpPortInput->u1DomainId,
                                      pPtpPortNode->u1DomainId);

    if (i4RetVal != RB_EQUAL)
    {
        PTP_FN_EXIT ();
        return i4RetVal;
    }

    if (pPtpPortInput->PortDs.u4PtpPortNumber <
        pPtpPortNode->PortDs.u4PtpPortNumber)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpPortInput->PortDs.u4PtpPortNumber >
        pPtpPortNode->PortDs.u4PtpPortNumber)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    PTP_FN_EXIT ();
    return RB_EQUAL;
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompFgnMasterDS                       */
/*                                                                           */
/* Description               : Comparison function for Foreign master        */
/*                             data set                                      */
/*                                                                           */
/* Input                     : pRbInput   -   Node given as input to RB Tree */
/*                             pRbNode    -   Node present in RB Tree        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCompFgnMasterDS (tRBElem * pRbInput, tRBElem * pRbNode)
{
    tPtpForeignMasterDs *pPtpFgnMasterInput = (tPtpForeignMasterDs *) pRbInput;
    tPtpForeignMasterDs *pPtpFgnMasterNode = (tPtpForeignMasterDs *) pRbNode;
    INT4                i4RetVal = RB_LESSER;

    PTP_FN_ENTRY ();

    i4RetVal = PtpRbUtlCompCxtAndDmn (pPtpFgnMasterInput->u4ContextId,
                                      pPtpFgnMasterNode->u4ContextId,
                                      pPtpFgnMasterInput->u1DomainId,
                                      pPtpFgnMasterNode->u1DomainId);

    if (i4RetVal != RB_EQUAL)
    {
        PTP_FN_EXIT ();
        return i4RetVal;
    }

    if (MEMCMP (pPtpFgnMasterInput->ClkId,
                pPtpFgnMasterNode->ClkId, PTP_MAX_CLOCK_ID_LEN) < 0)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (MEMCMP (pPtpFgnMasterInput->ClkId,
                pPtpFgnMasterNode->ClkId, PTP_MAX_CLOCK_ID_LEN) > 0)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    if (pPtpFgnMasterInput->u4SrcPortIndex < pPtpFgnMasterNode->u4SrcPortIndex)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpFgnMasterInput->u4SrcPortIndex > pPtpFgnMasterNode->u4SrcPortIndex)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    PTP_FN_EXIT ();
    return RB_EQUAL;
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompAccMasterDS                       */
/*                                                                           */
/* Description               : Comparison function for Acceptable master     */
/*                             data set                                      */
/*                                                                           */
/* Input                     : pRbInput   -   Node given as input to RB Tree */
/*                             pRbNode    -   Node present in RB Tree        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCompAccMasterDS (tRBElem * pRbInput, tRBElem * pRbNode)
{
    tPtpAccMasterTable *pPtpAccMasterInput = (tPtpAccMasterTable *) pRbInput;
    tPtpAccMasterTable *pPtpAccMasterNode = (tPtpAccMasterTable *) pRbNode;
    tPtpMasterIndex     RbMasterCompInput;
    tPtpMasterIndex     RbMasterCompNode;
    INT4                i4RetVal = RB_LESSER;

    PTP_FN_ENTRY ();

    MEMSET (&RbMasterCompInput, 0, sizeof (tPtpMasterIndex));
    MEMSET (&RbMasterCompNode, 0, sizeof (tPtpMasterIndex));

    RbMasterCompInput.u4ContextId = pPtpAccMasterInput->u4ContextId;
    RbMasterCompInput.u2NetworkProtocol =
        pPtpAccMasterInput->AccMstAddr.u2NetworkProtocol;
    RbMasterCompInput.u2AddrLength =
        pPtpAccMasterInput->AccMstAddr.u2AddrLength;
    RbMasterCompInput.u1DomainId = pPtpAccMasterInput->u1DomainId;
    MEMCPY (RbMasterCompInput.au1Addr,
            pPtpAccMasterInput->AccMstAddr.ai1Addr,
            RbMasterCompInput.u2AddrLength);

    RbMasterCompNode.u4ContextId = pPtpAccMasterNode->u4ContextId;
    RbMasterCompNode.u2NetworkProtocol =
        pPtpAccMasterNode->AccMstAddr.u2NetworkProtocol;
    RbMasterCompNode.u2AddrLength = pPtpAccMasterNode->AccMstAddr.u2AddrLength;
    RbMasterCompNode.u1DomainId = pPtpAccMasterNode->u1DomainId;
    MEMCPY (RbMasterCompNode.au1Addr,
            pPtpAccMasterNode->AccMstAddr.ai1Addr,
            RbMasterCompNode.u2AddrLength);

    i4RetVal = PtpRbUtlCompMasterDS (&RbMasterCompInput, &RbMasterCompNode);
    PTP_FN_EXIT ();
    return (i4RetVal);
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompUnicastMasterDS                   */
/*                                                                           */
/* Description               : Comparison function for unicast master        */
/*                             data set                                      */
/*                                                                           */
/* Input                     : pRbInput   -   Node given as input to RB Tree */
/*                             pRbNode    -   Node present in RB Tree        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCompUnicastMasterDS (tRBElem * pRbInput, tRBElem * pRbNode)
{
    tPtpAccUnicastMasterTable *pPtpUnicastMasterInput =
        (tPtpAccUnicastMasterTable *) pRbInput;
    tPtpAccUnicastMasterTable *pPtpUnicastMasterNode =
        (tPtpAccUnicastMasterTable *) pRbNode;
    INT4                i4RetVal = RB_LESSER;

    PTP_FN_ENTRY ();

    i4RetVal = PtpRbUtlCompCxtAndDmn (pPtpUnicastMasterInput->u4ContextId,
                                      pPtpUnicastMasterNode->u4ContextId,
                                      pPtpUnicastMasterInput->u1DomainId,
                                      pPtpUnicastMasterNode->u1DomainId);

    if (i4RetVal != RB_EQUAL)
    {
        PTP_FN_EXIT ();
        return i4RetVal;
    }

    if (pPtpUnicastMasterInput->u4PtpPortNumber >
        pPtpUnicastMasterNode->u4PtpPortNumber)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }
    if (pPtpUnicastMasterInput->u4PtpPortNumber >
        pPtpUnicastMasterNode->u4PtpPortNumber)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpUnicastMasterInput->UMAddr.u2NetworkProtocol
        < pPtpUnicastMasterNode->UMAddr.u2NetworkProtocol)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpUnicastMasterInput->UMAddr.u2NetworkProtocol
        > pPtpUnicastMasterNode->UMAddr.u2NetworkProtocol)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    if (pPtpUnicastMasterInput->UMAddr.u2AddrLength
        < pPtpUnicastMasterNode->UMAddr.u2AddrLength)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpUnicastMasterInput->UMAddr.u2AddrLength
        > pPtpUnicastMasterNode->UMAddr.u2AddrLength)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    if (MEMCMP (pPtpUnicastMasterInput->UMAddr.ai1Addr,
                pPtpUnicastMasterNode->UMAddr.ai1Addr,
                pPtpUnicastMasterNode->UMAddr.u2AddrLength) < 0)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (MEMCMP (pPtpUnicastMasterInput->UMAddr.ai1Addr,
                pPtpUnicastMasterNode->UMAddr.ai1Addr,
                pPtpUnicastMasterNode->UMAddr.u2AddrLength) > 0)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    PTP_FN_EXIT ();
    return RB_EQUAL;
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompGrandMasterDS                     */
/*                                                                           */
/* Description               : Comparison function for grand master          */
/*                             data set                                      */
/*                                                                           */
/* Input                     : pRbInput   -   Node given as input to RB Tree */
/*                             pRbNode    -   Node present in RB Tree        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCompGrandMasterDS (tRBElem * pRbInput, tRBElem * pRbNode)
{
    tPtpGrandMasterTable *pPtpGrandMasterInput =
        (tPtpGrandMasterTable *) pRbInput;
    tPtpGrandMasterTable *pPtpGrandMasterNode =
        (tPtpGrandMasterTable *) pRbNode;
    tPtpMasterIndex     RbMasterCompInput;
    tPtpMasterIndex     RbMasterCompNode;
    INT4                i4RetVal = RB_LESSER;

    PTP_FN_ENTRY ();

    MEMSET (&RbMasterCompInput, 0, sizeof (tPtpMasterIndex));
    MEMSET (&RbMasterCompNode, 0, sizeof (tPtpMasterIndex));

    RbMasterCompInput.u4ContextId = pPtpGrandMasterInput->u4ContextId;
    RbMasterCompInput.u2NetworkProtocol =
        pPtpGrandMasterInput->GMAddr.u2NetworkProtocol;
    RbMasterCompInput.u2AddrLength = pPtpGrandMasterInput->GMAddr.u2AddrLength;
    RbMasterCompInput.u1DomainId = pPtpGrandMasterInput->u1DomainId;
    MEMCPY (RbMasterCompInput.au1Addr,
            pPtpGrandMasterInput->GMAddr.ai1Addr,
            RbMasterCompInput.u2AddrLength);

    RbMasterCompNode.u4ContextId = pPtpGrandMasterNode->u4ContextId;
    RbMasterCompNode.u2NetworkProtocol =
        pPtpGrandMasterNode->GMAddr.u2NetworkProtocol;
    RbMasterCompNode.u2AddrLength = pPtpGrandMasterNode->GMAddr.u2AddrLength;
    RbMasterCompNode.u1DomainId = pPtpGrandMasterNode->u1DomainId;
    MEMCPY (RbMasterCompNode.au1Addr,
            pPtpGrandMasterNode->GMAddr.ai1Addr, RbMasterCompNode.u2AddrLength);

    i4RetVal = PtpRbUtlCompMasterDS (&RbMasterCompInput, &RbMasterCompNode);
    PTP_FN_EXIT ();
    return (i4RetVal);
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompSecKeyDS                          */
/*                                                                           */
/* Description               : Comparison function for security key          */
/*                             data set                                      */
/*                                                                           */
/* Input                     : pRbInput   -   Node given as input to RB Tree */
/*                             pRbNode    -   Node present in RB Tree        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCompSecKeyDS (tRBElem * pRbInput, tRBElem * pRbNode)
{
    tPtpSecKeyDs       *pPtpSecKeyInput = (tPtpSecKeyDs *) pRbInput;
    tPtpSecKeyDs       *pPtpSecKeyNode = (tPtpSecKeyDs *) pRbNode;
    INT4                i4RetVal = RB_LESSER;

    PTP_FN_ENTRY ();

    i4RetVal = PtpRbUtlCompCxtAndDmn (pPtpSecKeyInput->u4ContextId,
                                      pPtpSecKeyNode->u4ContextId,
                                      pPtpSecKeyInput->u1DomainId,
                                      pPtpSecKeyNode->u1DomainId);

    if (i4RetVal != RB_EQUAL)
    {
        PTP_FN_EXIT ();
        return i4RetVal;
    }

    if (pPtpSecKeyInput->u4SecKeyId < pPtpSecKeyNode->u4SecKeyId)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpSecKeyInput->u4SecKeyId > pPtpSecKeyNode->u4SecKeyId)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    PTP_FN_EXIT ();
    return RB_EQUAL;
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompVersionDb                         */
/*                                                                           */
/* Description               : Comparison function for version compatibility */
/*                                                                           */
/* Input                     : pRbInput   -   Node given as input to RB Tree */
/*                             pRbNode    -   Node present in RB Tree        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCompVersionDb (tRBElem * pRbInput, tRBElem * pRbNode)
{
    tPtpCompDBEntry    *pPtpCompDBInput = (tPtpCompDBEntry *) pRbInput;
    tPtpCompDBEntry    *pPtpCompDBNode = (tPtpCompDBEntry *) pRbNode;
    INT4                i4RetVal = RB_LESSER;

    PTP_FN_ENTRY ();

    i4RetVal = PtpRbUtlCompCxtAndDmn (pPtpCompDBInput->u4ContextId,
                                      pPtpCompDBNode->u4ContextId,
                                      pPtpCompDBInput->u1DomainId,
                                      pPtpCompDBNode->u1DomainId);

    if (i4RetVal != RB_EQUAL)
    {
        PTP_FN_EXIT ();
        return i4RetVal;
    }

    if (pPtpCompDBInput->u2PortId < pPtpCompDBNode->u2PortId)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpCompDBInput->u2PortId > pPtpCompDBNode->u2PortId)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    if (MEMCMP (pPtpCompDBInput->ai1v1SrcUuid,
                pPtpCompDBNode->ai1v1SrcUuid, PTP_UUID_LENGTH) < 0)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (MEMCMP (pPtpCompDBInput->ai1v1SrcUuid,
                pPtpCompDBNode->ai1v1SrcUuid, PTP_UUID_LENGTH) > 0)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    if (pPtpCompDBInput->u2SrcPortId < pPtpCompDBNode->u2SrcPortId)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpCompDBInput->u2SrcPortId > pPtpCompDBNode->u2SrcPortId)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }
    if (MEMCMP (pPtpCompDBInput->ai1v1SubDomain,
                pPtpCompDBNode->ai1v1SubDomain, PTP_SUBDOMAIN_NAME_LENGTH) < 0)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (MEMCMP (pPtpCompDBInput->ai1v1SubDomain,
                pPtpCompDBNode->ai1v1SubDomain, PTP_SUBDOMAIN_NAME_LENGTH) > 0)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    PTP_FN_EXIT ();
    return RB_EQUAL;
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompAltTimeScaleDS                    */
/*                                                                           */
/* Description               : Comparison function for alternate time scale  */
/*                             functionality                                 */
/*                                                                           */
/* Input                     : pRbInput   -   Node given as input to RB Tree */
/*                             pRbNode    -   Node present in RB Tree        */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpRbUtlCompAltTimescaleDS (tRBElem * pRbInput, tRBElem * pRbNode)
{
    tPtpAltTimescaleEntry *pPtpAltTimescaleInput =
        (tPtpAltTimescaleEntry *) pRbInput;
    tPtpAltTimescaleEntry *pPtpAltTimescaleNode =
        (tPtpAltTimescaleEntry *) pRbNode;
    INT4                i4RetVal = RB_LESSER;

    PTP_FN_ENTRY ();

    i4RetVal = PtpRbUtlCompCxtAndDmn (pPtpAltTimescaleInput->u4ContextId,
                                      pPtpAltTimescaleNode->u4ContextId,
                                      pPtpAltTimescaleInput->u1DomainId,
                                      pPtpAltTimescaleNode->u1DomainId);

    if (i4RetVal != RB_EQUAL)
    {
        PTP_FN_EXIT ();
        return i4RetVal;
    }

    if (pPtpAltTimescaleInput->u1KeyId < pPtpAltTimescaleNode->u1KeyId)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpAltTimescaleInput->u1KeyId > pPtpAltTimescaleNode->u1KeyId)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    PTP_FN_EXIT ();
    return RB_EQUAL;
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompCxtAndDmn                         */
/*                                                                           */
/* Description               : Comparison function for Context and domain    */
/*                             common for all RBTree comparison              */
/*                                                                           */
/* Input                     : u4CxtIdInput - Context id in input node       */
/*                             u4CxtIdNode  - Context id in RB node          */
/*                             u1DmdIdInput - Domain id in input node        */
/*                             u1DmdIdNode  - Domain id in RB node           */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpRbUtlCompCxtAndDmn (UINT4 u4CxtIdInput, UINT4 u4CxtIdNode,
                       UINT1 u1DmdIdInput, UINT1 u1DmdIdNode)
{
    PTP_FN_ENTRY ();

    if (u4CxtIdInput < u4CxtIdNode)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (u4CxtIdInput > u4CxtIdNode)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    if (u1DmdIdInput < u1DmdIdNode)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (u1DmdIdInput > u1DmdIdNode)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    PTP_FN_EXIT ();
    return RB_EQUAL;
}

/*****************************************************************************/
/* Function                  : PtpRbUtlCompMasterDS                          */
/*                                                                           */
/* Description               : Comparison function common for acceptable,    */
/*                             unicast and grand master DS                   */
/*                                                                           */
/* Input                     : pPtpMasterIndexInput - Input node             */
/*                             pPtpMasterIndexNode  - RB Tree node           */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : RB_EQUAL / RB_LESSER / RB_GREATER             */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpRbUtlCompMasterDS (tPtpMasterIndex * pPtpMasterIndexInput,
                      tPtpMasterIndex * pPtpMasterIndexNode)
{
    INT4                i4RetVal = RB_LESSER;

    PTP_FN_ENTRY ();

    i4RetVal = PtpRbUtlCompCxtAndDmn (pPtpMasterIndexInput->u4ContextId,
                                      pPtpMasterIndexNode->u4ContextId,
                                      pPtpMasterIndexInput->u1DomainId,
                                      pPtpMasterIndexNode->u1DomainId);

    if (i4RetVal != RB_EQUAL)
    {
        PTP_FN_EXIT ();
        return i4RetVal;
    }

    if (pPtpMasterIndexInput->u2NetworkProtocol
        < pPtpMasterIndexNode->u2NetworkProtocol)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpMasterIndexInput->u2NetworkProtocol
        > pPtpMasterIndexNode->u2NetworkProtocol)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    if (pPtpMasterIndexInput->u2AddrLength < pPtpMasterIndexNode->u2AddrLength)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (pPtpMasterIndexInput->u2AddrLength > pPtpMasterIndexNode->u2AddrLength)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    if (MEMCMP (pPtpMasterIndexInput->au1Addr, pPtpMasterIndexNode->au1Addr,
                pPtpMasterIndexNode->u2AddrLength) < 0)
    {
        PTP_FN_EXIT ();
        return RB_LESSER;
    }

    if (MEMCMP (pPtpMasterIndexInput->au1Addr, pPtpMasterIndexNode->au1Addr,
                pPtpMasterIndexNode->u2AddrLength) > 0)
    {
        PTP_FN_EXIT ();
        return RB_GREATER;
    }

    PTP_FN_EXIT ();
    return RB_EQUAL;
}
#endif /*_PTPRBUTL_C_*/
