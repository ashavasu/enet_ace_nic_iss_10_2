/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: g8261cli.c,v 1.4 2014/02/03 12:27:07 siva Exp $
 *
 * Description: This file contains the G8261 CLI related routines and 
 * utility functions.
 *****************************************************************************/

#ifndef _G8261CLI_C_
#define _G8261CLI_C_

#include "ptpincs.h"
#include "g8261clipt.h"
#include "fsptpcli.h"
#include "ptpsrcpt.h"

/***************************************************************************
 * FUNCTION NAME             : cli_process_g8261_cmd 
 *
 * DESCRIPTION               : This function is exported to CLI module to 
 *                             handle the PTP cli commands to take the 
 *                             corresponding action. 
 *                             Only SNMP Low level routines are called 
 *                             from CLI.
 *
 * INPUT                     : Variable arguments
 *
 * OUTPUT                    : None
 *
 * Global Variables Referred : None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 * 
 * RETURNS                   : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
cli_process_g8261_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst = 0;
    INT4                i4ContextId = 0;
    INT4                i4InterfaceId = 0;
    INT4                i4DomainId = PTP_V2_DEFAULT_DOMAIN_ID;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4PtpSysCtrl = PTP_SHUTDOWN;
    UINT4               u4SwitchCount = 0;
    UINT4               u4ReverseSwitchCount = 0;
    UINT4               u4PacketSize = 0;
    UINT4               u4ReversePacketSize = 0;
    UINT4               u4Load = 0;
    UINT4               u4ReverseLoad = 0;
    UINT4               u4NwDist = 0;
    UINT4               u4ReverseNwDist = 0;
    UINT4               u4Duration = 0;
    UINT4               u4ReverseDuration = 0;
    UINT4               u4Variation = 0;
    UINT4               u4ReverseVariation = 0;
    UINT4               u4Frequency = 0;
    UINT4               u4ReverseFrequency = 0;
    UINT4               u4FlowInterval = 0;
    UINT4               u4ReverseFlowInterval = 0;
    UINT4              *args[PTP_CLI_MAX_ARGS];
    INT1                i1ArgNo = 0;

    nmhGetFsPtpGlobalSysCtrl (&i4PtpSysCtrl);

    if ((u4Command != CLI_PTP_START) && (i4PtpSysCtrl == PTP_SHUTDOWN))
    {
        CliPrintf (CliHandle, "%% PTP is shutdown\r\n");
        return CLI_SUCCESS;
    }

    CliRegisterLock (CliHandle, PtpApiLock, PtpApiUnLock);

    if (PtpApiLock () == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%%PTP Error : Command not executed\r\n");
        return CLI_FAILURE;
    }

    if ((u4Command != CLI_PTP_VRF_START) && (u4Command != CLI_PTP_VRF_SHUTDOWN))
    {
        i4ContextId = CLI_GET_CXT_ID ();
        i4DomainId = CLI_GET_DOMAIN_ID ();
    }

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        i4InterfaceId = (UINT4) i4Inst;
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store PTP_CLI_MAX_ARGS arguements at the max. 
     */

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[i1ArgNo++] = va_arg (ap, UINT4 *);
        if (i1ArgNo == PTP_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_PTP_SET_G8261_SWITCHCOUNT:

            if (args[0] != NULL)
            {
                MEMCPY (&u4SwitchCount, args[0], sizeof (UINT4));
            }

            if (args[1] != NULL)
            {
                MEMCPY (&u4ReverseSwitchCount, args[1], sizeof (UINT4));
            }

            i4RetStatus = PtpCliSetG8261SwitchCount (CliHandle,
                                                     u4SwitchCount,
                                                     u4ReverseSwitchCount);
            break;

        case CLI_PTP_SET_G8261_PACKETSIZE:

            if (args[0] != NULL)
            {
                MEMCPY (&u4PacketSize, args[0], sizeof (UINT4));
            }

            if (args[1] != NULL)
            {
                MEMCPY (&u4ReversePacketSize, args[1], sizeof (UINT4));
            }
            i4RetStatus = PtpCliSetG8261PacketSize (CliHandle, u4PacketSize,
                                                    u4ReversePacketSize);
            break;

        case CLI_PTP_SET_G8261_LOAD:

            if (args[0] != NULL)
            {
                MEMCPY (&u4Load, args[0], sizeof (UINT4));
            }

            if (args[1] != NULL)
            {
                MEMCPY (&u4ReverseLoad, args[1], sizeof (UINT4));
            }

            i4RetStatus = PtpCliSetG8261Load (CliHandle, u4Load, u4ReverseLoad);
            break;

        case CLI_PTP_SET_G8261_NWDIST:

            if (args[0] != NULL)
            {
                MEMCPY (&u4NwDist, args[0], sizeof (UINT4));
            }

            if (args[1] != NULL)
            {
                MEMCPY (&u4ReverseNwDist, args[1], sizeof (UINT4));
            }

            i4RetStatus =
                PtpCliSetG8261NwDisturbance (CliHandle, u4NwDist,
                                             u4ReverseNwDist);
            break;

        case CLI_PTP_SET_G8261_DELAY_DURATION:

            if (args[0] != NULL)
            {
                MEMCPY (&u4Duration, args[0], sizeof (UINT4));
            }

            if (args[1] != NULL)
            {
                MEMCPY (&u4ReverseDuration, args[1], sizeof (UINT4));
            }

            i4RetStatus =
                PtpCliSetDelayDuration (CliHandle, u4Duration,
                                        u4ReverseDuration);
            break;

        case CLI_PTP_SET_G8261_FLOWVAR:

            if (args[0] != NULL)
            {
                MEMCPY (&u4Variation, args[0], sizeof (UINT4));
            }

            if (args[1] != NULL)
            {
                MEMCPY (&u4ReverseVariation, args[1], sizeof (UINT4));
            }
            i4RetStatus = PtpCliSetG8261FlowVariation (CliHandle, u4Variation,
                                                       u4ReverseVariation);
            break;

        case CLI_PTP_SET_G8261_FREQUENCY:

            if (args[0] != NULL)
            {
                MEMCPY (&u4Frequency, args[0], sizeof (UINT4));
            }

            if (args[1] != NULL)
            {
                MEMCPY (&u4ReverseFrequency, args[1], sizeof (UINT4));
            }

            i4RetStatus = PtpCliSetG8261Frequency (CliHandle,
                                                   u4Frequency,
                                                   u4ReverseFrequency);
            break;

        case CLI_PTP_SET_G8261_FLOW_INTERVAL:

            if (args[0] != NULL)
            {
                MEMCPY (&u4FlowInterval, args[0], sizeof (UINT4));
            }

            if (args[1] != NULL)
            {
                MEMCPY (&u4ReverseFlowInterval, args[1], sizeof (UINT4));
            }

            i4RetStatus = PtpCliSetG8261FlowInterval (CliHandle,
                                                      u4FlowInterval,
                                                      u4ReverseFlowInterval);
            break;

        case CLI_PTP_SHOW_G8261_DELAY_OUTPUT:
            i4RetStatus = PtpCliShowG8261DelayOutput (CliHandle);
            break;

        case CLI_PTP_SET_G8261_ENABLE:
            i4RetStatus = PtpCliSetG8261ModuleStatus (CliHandle, PTP_ENABLED);
            break;

        case CLI_PTP_SET_G8261_DISABLE:
            i4RetStatus = PtpCliSetG8261ModuleStatus (CliHandle, PTP_DISABLED);
            break;

        default:
            CliPrintf (CliHandle, "\r%% PTP : Invalid command\r\n");
            PtpApiUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_G8261_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gapc1G8261CliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    PtpApiUnLock ();

    CliUnRegisterLock (CliHandle);

    UNUSED_PARAM (i4ContextId);
    UNUSED_PARAM (i4InterfaceId);
    UNUSED_PARAM (i4DomainId);

    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetG8261ModuleStatus
 *
 * DESCRIPTION      : This function enables/disables the G8261 test 
 *                    simulator module. 
 *
 * INPUT            : CliHandle - tCliHandle
 *                    i4ModuleStatus - Enable/Disable
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetG8261ModuleStatus (tCliHandle CliHandle, INT4 i4ModuleStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsG8261TsModuleStatus (&u4ErrorCode, i4ModuleStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsModuleStatus (i4ModuleStatus);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetG8261SwitchCount
 *
 * DESCRIPTION      : This function sets the switch count in G8261 test 
 *                    simulator as configured from CLI.
 *
 * INPUT            : i4SwitchCount - switch count to be configured
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetG8261SwitchCount (tCliHandle CliHandle, UINT4 u4FwdSwitchCount,
                           UINT4 u4ReverseSwitchCount)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsG8261TsSwitchCount (&u4ErrorCode, G8261TS_FORWARD,
                                       u4FwdSwitchCount) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsSwitchCount (G8261TS_FORWARD, u4FwdSwitchCount);
    if (nmhTestv2FsG8261TsSwitchCount (&u4ErrorCode, G8261TS_REVERSE,
                                       u4ReverseSwitchCount) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsSwitchCount (G8261TS_REVERSE, u4ReverseSwitchCount);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetG8261PacketSize
 *
 * DESCRIPTION      : This function sets the packet size in G8261 test 
 *                    simulator as configured from CLI.
 *
 * INPUT            : i4SwitchCount - switch count to be configured
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetG8261PacketSize (tCliHandle CliHandle, INT4 i4FwdPacketSize,
                          INT4 i4ReversePktSize)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsG8261TsPacketSize (&u4ErrorCode, G8261TS_FORWARD,
                                      i4FwdPacketSize) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsPacketSize (G8261TS_FORWARD, i4FwdPacketSize);

    if (i4ReversePktSize != 0)
    {
        if (nmhTestv2FsG8261TsPacketSize (&u4ErrorCode, G8261TS_REVERSE,
                                          i4ReversePktSize) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetFsG8261TsPacketSize (G8261TS_REVERSE, i4ReversePktSize);
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetG8261Load
 *
 * DESCRIPTION      : This function sets the packet size in G8261 test 
 *                    simulator as configured from CLI.
 *
 * INPUT            : i4SwitchCount - switch count to be configured
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetG8261Load (tCliHandle CliHandle, INT4 i4Load, INT4 i4ReverseLoad)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsG8261TsLoad (&u4ErrorCode, G8261TS_FORWARD, i4Load) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsLoad (G8261TS_FORWARD, i4Load);

    if (nmhTestv2FsG8261TsLoad (&u4ErrorCode, G8261TS_REVERSE, i4ReverseLoad) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsLoad (G8261TS_REVERSE, i4ReverseLoad);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetG8261NwDisturbance
 *
 * DESCRIPTION      : This function sets the percentage of network 
 *                    disturbance in G8261 test simulator as configured
 *                    from CLI.
 *
 * INPUT            : i4NwDisturbance - percentage of network disturbance
 *                                      to be configured
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetG8261NwDisturbance (tCliHandle CliHandle, INT4 i4NwDist,
                             INT4 i4ReverseNwDist)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsG8261TsNetworkDisturbance (&u4ErrorCode, G8261TS_FORWARD,
                                              i4NwDist) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsNetworkDisturbance (G8261TS_FORWARD, i4NwDist);

    if (nmhTestv2FsG8261TsNetworkDisturbance (&u4ErrorCode, G8261TS_REVERSE,
                                              i4ReverseNwDist) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsNetworkDisturbance (G8261TS_REVERSE, i4ReverseNwDist);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetDelayDuration
 *
 * DESCRIPTION      : This function sets the duration of network 
 *                    disturbance/load in G8261 test simulator as configured
 *                    from CLI.
 *
 * INPUT            : i4DelayDuration - Duration till when the 
 *                                      network disturbance/load should be
 *                                      applied.
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetDelayDuration (tCliHandle CliHandle, INT4 i4DelayDuration,
                        INT4 i4ReverseDuration)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsG8261TsDuration (&u4ErrorCode, G8261TS_FORWARD,
                                    i4DelayDuration) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsDuration (G8261TS_FORWARD, i4DelayDuration);

    if (nmhTestv2FsG8261TsDuration (&u4ErrorCode, G8261TS_REVERSE,
                                    i4ReverseDuration) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsDuration (G8261TS_REVERSE, i4ReverseDuration);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetG8261FlowVariation
 *
 * DESCRIPTION      : This function sets the flow variation with which the
 *                    network disturbance/load should be applied in G8261 test 
 *                    simulator as configured from CLI.
 *
 * INPUT            : i4FlowVariation - Flow variation with which 
 *                                      network disturbance/load should be 
 *                                      applied
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetG8261FlowVariation (tCliHandle CliHandle, INT4 i4FlowVariation,
                             INT4 i4ReverseVariation)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsG8261TsFlowVariationFactor (&u4ErrorCode, G8261TS_FORWARD,
                                               i4FlowVariation) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsFlowVariationFactor (G8261TS_FORWARD, i4FlowVariation);

    if (i4ReverseVariation != 0)
    {
        if (nmhTestv2FsG8261TsFlowVariationFactor
            (&u4ErrorCode, G8261TS_REVERSE, i4ReverseVariation) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetFsG8261TsFlowVariationFactor (G8261TS_REVERSE,
                                            i4ReverseVariation);
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetG8261Frequency
 *
 * DESCRIPTION      : This function sets the frequency with which the
 *                    network disturbance/load should be applied in G8261 test 
 *                    simulator as configured from CLI.
 *
 * INPUT            : i4FlowVariation - Flow variation with which 
 *                                      network disturbance/load should be 
 *                                      applied
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetG8261Frequency (tCliHandle CliHandle, UINT4 u4Frequency,
                         UINT4 u4ReverseFrequency)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsG8261TsFrequency (&u4ErrorCode, G8261TS_FORWARD,
                                     u4Frequency) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsFrequency (G8261TS_FORWARD, u4Frequency);

    if (u4ReverseFrequency != 0)
    {
        if (nmhTestv2FsG8261TsFrequency (&u4ErrorCode, G8261TS_REVERSE,
                                         u4ReverseFrequency) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetFsG8261TsFrequency (G8261TS_REVERSE, u4ReverseFrequency);
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliSetG8261FlowInterval
 *
 * DESCRIPTION      : This function sets the interval till when the traffic flow
 *                    should be applied in G8261 test simulator as
 *                    configured from CLI.
 *
 * INPUT            : i4FlowVariation - Flow variation with which 
 *                                      network disturbance/load should be 
 *                                      applied
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/

PRIVATE INT4
PtpCliSetG8261FlowInterval (tCliHandle CliHandle, UINT4 u4FlowInterval,
                            UINT4 u4ReverseFlowInterval)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsG8261TsFlowInterval (&u4ErrorCode, G8261TS_FORWARD,
                                        u4FlowInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsG8261TsFlowInterval (G8261TS_FORWARD, u4FlowInterval);

    if (u4ReverseFlowInterval != 0)
    {
        if (nmhTestv2FsG8261TsFlowInterval (&u4ErrorCode, G8261TS_REVERSE,
                                            u4ReverseFlowInterval) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        nmhSetFsG8261TsFlowInterval (G8261TS_REVERSE, u4ReverseFlowInterval);
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : PtpCliShowG8261DelayOutput
 *
 * DESCRIPTION      : This function displays the delay calculated by the G8261
 *                    test simulator
 *
 * INPUT            :  
 *
 * OUTPUT           : None
 *
 *
 * Global Variables Referred :  None
 *
 * Global variables Modified : None
 *
 * Use of Recursion          : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PRIVATE INT4
PtpCliShowG8261DelayOutput (tCliHandle CliHandle)
{
    FS_UINT8            u8Delay;
    tSNMP_OCTET_STRING_TYPE PeerMPD;
    tSNMP_OCTET_STRING_TYPE OffsetFromMaster;

    FSAP_U8_CLR (&u8Delay);
    MEMSET (&PeerMPD, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OffsetFromMaster, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    CliPrintf (CliHandle, "Delay Parameters Configured:\r\n");
    CliPrintf (CliHandle, "----------------------------\r\n");
    CliPrintf (CliHandle, "SwitchCount   : %d\r\n",
               gDelayParams[G8261TS_FORWARD - 1].u4SwitchCount);
    CliPrintf (CliHandle, "PacketSize    : %d\r\n",
               gDelayParams[G8261TS_FORWARD - 1].u4PacketSize);
    CliPrintf (CliHandle, "Load          : %d\r\n",
               gDelayParams[G8261TS_FORWARD - 1].u4Load);
    CliPrintf (CliHandle, "NwDisturbance : %d\r\n",
               gDelayParams[G8261TS_FORWARD - 1].u4NwDisturbance);
    CliPrintf (CliHandle, "DelayDuration : %d\r\n",
               gDelayParams[G8261TS_FORWARD - 1].u4DelayDuration);
    CliPrintf (CliHandle, "NwDirection   : %d\r\n",
               gDelayParams[G8261TS_FORWARD - 1].u1NwDirection);
    CliPrintf (CliHandle, "FlowVariation : %d\r\n",
               gDelayParams[G8261TS_FORWARD - 1].u1FlowVariation);
    CliPrintf (CliHandle, "FlowInterval : %d\r\n",
               gDelayParams[G8261TS_FORWARD - 1].u4FlowInterval);
    CliPrintf (CliHandle, "Frequency : %d\r\n",
               gDelayParams[G8261TS_FORWARD - 1].u4Frequency);
    CliPrintf (CliHandle,
               "Delay Parameters Configured in reverse direction:\r\n");
    CliPrintf (CliHandle,
               "-------------------------------------------------\r\n");
    CliPrintf (CliHandle, "SwitchCount   : %d\r\n",
               gDelayParams[G8261TS_REVERSE - 1].u4SwitchCount);
    CliPrintf (CliHandle, "PacketSize    : %d\r\n",
               gDelayParams[G8261TS_REVERSE - 1].u4PacketSize);
    CliPrintf (CliHandle, "Load          : %d\r\n",
               gDelayParams[G8261TS_REVERSE - 1].u4Load);
    CliPrintf (CliHandle, "NwDisturbance : %d\r\n",
               gDelayParams[G8261TS_REVERSE - 1].u4NwDisturbance);
    CliPrintf (CliHandle, "DelayDuration : %d\r\n",
               gDelayParams[G8261TS_REVERSE - 1].u4DelayDuration);
    CliPrintf (CliHandle, "NwDirection   : %d\r\n",
               gDelayParams[G8261TS_REVERSE - 1].u1NwDirection);
    CliPrintf (CliHandle, "FlowVariation : %d\r\n",
               gDelayParams[G8261TS_REVERSE - 1].u1FlowVariation);
    CliPrintf (CliHandle, "FlowInterval : %d\r\n",
               gDelayParams[G8261TS_REVERSE - 1].u4FlowInterval);
    CliPrintf (CliHandle, "Frequency : %d\r\n",
               gDelayParams[G8261TS_REVERSE - 1].u4Frequency);

    PtpApiG8261ComputeDelay (&u8Delay);
    CliPrintf (CliHandle, "Dleay Computed : %u\r\n", gDelayOutput.u8Delay.u4Lo);
    return CLI_SUCCESS;
}
#endif
