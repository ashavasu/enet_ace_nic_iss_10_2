/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpcomp.c,v 1.3 2014/01/24 12:20:08 siva Exp $
 *
 * Description: This file contains PTP Compatability Sub module functionality
 *              routines.
 *********************************************************************/
#ifndef _PTPCOMP_C_
#define _PTPCOMP_C_

#include "ptpincs.h"

PRIVATE tPtpCompDBEntry *PtpCompAddCompDbEntry (tPtpCompDBEntry * pPtpCompDB);

/*****************************************************************************/
/* Function     : PtpCompUpdateCompDbFromV1Info                              */
/*                                                                           */
/* Description  : This function is used to update the v1 informantion in the */
/*                Compatibility table to store the v1 related info.          */
/*                                                                           */
/* Input        : u4ContextId - Context Identifier.                          */
/*              : u1DomainId  - Domain Identifier.                           */
/*              : u4PortId    - PTP Port Identifier.                         */
/*              : pPtpv1Info  - Pointer to tPtpv1Info Type                   */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCompUpdateCompDbFromV1Info (UINT4 u4ContextId, UINT1 u1DomainId,
                               UINT4 u4PortId, tPtpv1Info * pPtpv1Info)
{
    tPtpCompDBEntry     PtpCompDBEntry;
    tPtpCompDBEntry    *pPtpCompDB = NULL;
    UINT4               u4MilliSec = 0;
    UINT1               u1TmrFlag = OSIX_FALSE;

    PTP_FN_ENTRY ();

    MEMSET (&PtpCompDBEntry, 0, sizeof (tPtpCompDBEntry));

    /* Fill the Index of the  CompDB Table */
    PtpCompDBEntry.u4ContextId = u4ContextId;
    PtpCompDBEntry.u1DomainId = u1DomainId;
    PtpCompDBEntry.u2PortId = (UINT2) u4PortId;
    PtpCompDBEntry.u2SrcPortId = pPtpv1Info->ComHdr.u2SrcPortId;
    MEMCPY (&(PtpCompDBEntry.ai1v1SrcUuid),
            &(pPtpv1Info->ComHdr.ai1SourceUuid), PTP_UUID_LENGTH);
    MEMCPY (&(PtpCompDBEntry.ai1v1SubDomain),
            &(pPtpv1Info->ComHdr.ai1SubDomain), PTP_SUBDOMAIN_NAME_LENGTH);

    /* Get the following from the DB */
    pPtpCompDB = PtpCompGetCompDbEntry (&PtpCompDBEntry);
    if (pPtpCompDB == NULL)
    {
        u1TmrFlag = OSIX_TRUE;

        pPtpCompDB = PtpCompAddCompDbEntry (&PtpCompDBEntry);
        if (pPtpCompDB == NULL)
        {
            PTP_TRC ((u4ContextId, u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpCompUpdateCompDbFromV1Info: "
                      "PtpCompAddCompDBEntry Failed!!\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }
    }
    /* Get the Info from the pPtpv1Info */
    pPtpCompDB->u1SyncInterval = pPtpv1Info->SyncDReqHdr.i1SyncInterval;
    pPtpCompDB->u2EpochNumber = pPtpv1Info->SyncDReqHdr.u2EpochNum;
    pPtpCompDB->u1v1Stratum = pPtpv1Info->SyncDReqHdr.u1LocalClkStratum;
    pPtpCompDB->u2GMSeqId = pPtpv1Info->SyncDReqHdr.u2GMSequenceId;
    MEMCPY (pPtpCompDB->ai1ClkId, pPtpv1Info->SyncDReqHdr.ai1LocalClkId,
            PTP_CODE_STRING_LENGTH);

    /* Start the Timer to remove this entry after
     * (u1SyncInterval in Sec * PTP_V1_SYNC_RETRY_COUNT) 
     */
    u4MilliSec = (pPtpCompDB->u1SyncInterval * PTP_V1_SYNC_RETRY_COUNT * 1000);

    if (PtpTmrStartTmr (&(pPtpCompDB->CompDBTmrNode),
                        u4MilliSec, PTP_COMPDB_TMR, u1TmrFlag) == OSIX_FAILURE)
    {
        PTP_TRC ((u4ContextId, u1DomainId, ALL_FAILURE_TRC,
                  "PtpCompUpdateCompDbFromV1Info: PtpTmrStartTmr Failed!!\r\n"));

        PTP_TRC ((u4ContextId, u1DomainId, CONTROL_PLANE_TRC,
                  "Unable to start CompatibilityDb Table timer\r\n"));
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCompUpdateV1InfoFromCompDb                              */
/*                                                                           */
/* Description  : This function is used to update the v1 informantion from   */
/*                the Compatibility table info.                              */
/*                                                                           */
/* Input        : u4ContextId - Context Identifier.                          */
/*              : u1DomainId  - Domain Identifier.                           */
/*              : u4PortId    - PTP Port Identifier.                         */
/*              : pPtpv1Info  - Pointer to tPtpv1Info Type                   */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCompUpdateV1InfoFromCompDb (UINT4 u4ContextId, UINT1 u1DomainId,
                               UINT4 u4PortId, tPtpv1Info * pPtpv1Info)
{
    tPtpCompDBEntry     PtpCompDBEntry;
    tPtpCompDBEntry    *pPtpCompDB = NULL;

    PTP_FN_ENTRY ();

    /* Update the following from the DB */

    MEMSET (&PtpCompDBEntry, 0, sizeof (tPtpCompDBEntry));

    /* Fill the Index of the  CompDB Table */
    PtpCompDBEntry.u4ContextId = u4ContextId;
    PtpCompDBEntry.u1DomainId = u1DomainId;
    PtpCompDBEntry.u2PortId = (UINT2) u4PortId;
    PtpCompDBEntry.u2SrcPortId = pPtpv1Info->ComHdr.u2SrcPortId;
    MEMCPY (&(PtpCompDBEntry.ai1v1SrcUuid),
            &(pPtpv1Info->ComHdr.ai1SourceUuid), PTP_UUID_LENGTH);
    MEMCPY (&(PtpCompDBEntry.ai1v1SubDomain),
            &(pPtpv1Info->ComHdr.ai1SubDomain), PTP_SUBDOMAIN_NAME_LENGTH);

    pPtpCompDB = PtpCompGetCompDbEntry (&PtpCompDBEntry);
    if (pPtpCompDB == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpCompUpdateV1InfoFromCompDb : Entry is not found "
                  "in the Compatibility table.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* Get the Info from the shadow table */
    pPtpv1Info->SyncDReqHdr.i1SyncInterval = pPtpCompDB->u1SyncInterval;
    pPtpv1Info->SyncDReqHdr.u2EpochNum = pPtpCompDB->u2EpochNumber;
    pPtpv1Info->SyncDReqHdr.u1LocalClkStratum = pPtpCompDB->u1v1Stratum;
    pPtpv1Info->SyncDReqHdr.u2GMSequenceId = pPtpCompDB->u2GMSeqId;
    MEMCPY (pPtpv1Info->SyncDReqHdr.ai1LocalClkId, pPtpCompDB->ai1ClkId,
            PTP_CODE_STRING_LENGTH);

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCompAddCompDbEntry                                      */
/*                                                                           */
/* Description  : This function is used to add the Compatibility DB Table    */
/*                Entry.                                                     */
/*                                                                           */
/* Input        : pPtpCompDB  - Index filled PtpCompDB Entry to be added     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : NULL/ Pointer to the Node found                            */
/*                                                                           */
/*****************************************************************************/
PRIVATE tPtpCompDBEntry *
PtpCompAddCompDbEntry (tPtpCompDBEntry * pPtpCompDB)
{
    tPtpCompDBEntry    *pPtpCompDBEntry = NULL;
    UINT4               u4Status = 0;

    PTP_FN_ENTRY ();

    /* 1. Allocate Memory from the MemPool */
    pPtpCompDBEntry = (tPtpCompDBEntry *)
        MemAllocMemBlk (gPtpGlobalInfo.CompPoolId);
    if (pPtpCompDBEntry == NULL)
    {
        PTP_TRC ((pPtpCompDB->u4ContextId, pPtpCompDB->u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpCompAddCompDbEntry : MemAllocMemBlk Failed \r\n"));
        PTP_FN_EXIT ();
        return (NULL);
    }

    /* 2. Copy the Index */
    MEMCPY (pPtpCompDBEntry, pPtpCompDB, sizeof (tPtpCompDBEntry));

    /* 3. Add the Node in to the tPtpCompDb Table */
    u4Status = PtpDbAddNode (gPtpGlobalInfo.CompTree, pPtpCompDBEntry,
                             PTP_COMP_DB_DATA_SET);
    if (u4Status == OSIX_FAILURE)
    {
        u4Status = MemReleaseMemBlock (gPtpGlobalInfo.CompPoolId,
                                       (UINT1 *) pPtpCompDBEntry);
        if (u4Status == MEM_FAILURE)
        {
            PTP_TRC ((pPtpCompDB->u4ContextId, pPtpCompDB->u1DomainId,
                      PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpCompAddCompDbEntry : MemReleaseMemBlock Failed \r\n"));
        }
        PTP_TRC ((pPtpCompDB->u4ContextId, pPtpCompDB->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpCompAddCompDbEntry : PtpDbAddNode Failed \r\n"));
        PTP_FN_EXIT ();
        return (NULL);
    }
    PTP_FN_EXIT ();
    return (pPtpCompDBEntry);
}

/*****************************************************************************/
/* Function     : PtpCompGetCompDbEntry                                      */
/*                                                                           */
/* Description  : This function is used to get the Compatibility DB Table    */
/*                Entry.                                                     */
/*                                                                           */
/* Input        : pPtpCompDB  - Index filled PtpCompDB Entry to be searched  */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : NULL/ Pointer to the Node found                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpCompDBEntry *
PtpCompGetCompDbEntry (tPtpCompDBEntry * pPtpCompDB)
{
    VOID               *pNode = NULL;

    PTP_FN_ENTRY ();

    pNode = PtpDbGetNode (gPtpGlobalInfo.CompTree, pPtpCompDB,
                          PTP_COMP_DB_DATA_SET);

    PTP_FN_EXIT ();
    return ((tPtpCompDBEntry *) pNode);
}

/*****************************************************************************/
/* Function     : PtpCompDelCompDbEntry                                      */
/*                                                                           */
/* Description  : This function is used to delete the Compatibility DB Table */
/*                Entry.                                                     */
/*                                                                           */
/* Input        : pPtpCompDB  - Pointer to the node to be deleted.           */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCompDelCompDbEntry (tPtpCompDBEntry * pPtpCompDB)
{
    UINT4               u4Status = OSIX_FAILURE;
    tPtpCompDBEntry    *pTmpPtpCompDB = NULL;

    PTP_FN_ENTRY ();

    /* 1. Delete the Node from the tPtpCompDB Table */
    pTmpPtpCompDB = PtpDbDeleteNode (gPtpGlobalInfo.CompTree, pPtpCompDB,
                                     PTP_COMP_DB_DATA_SET);
    if (pTmpPtpCompDB == NULL)
    {
        PTP_TRC ((pPtpCompDB->u4ContextId, pPtpCompDB->u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpCompDelCompDbEntry : PtpDbDeleteNode Failed \r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* 2. Relase the Memory to MemPool */
    MEMSET (pTmpPtpCompDB, 0, sizeof (tPtpCompDBEntry));

    /* 3. Release the Removed Entry's memory */
    u4Status = MemReleaseMemBlock (gPtpGlobalInfo.CompPoolId,
                                   (UINT1 *) pTmpPtpCompDB);
    if (u4Status == MEM_FAILURE)
    {
        PTP_TRC ((pPtpCompDB->u4ContextId, pPtpCompDB->u1DomainId,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpCompDelCompDbEntry : MemReleaseMemBlock Failed \r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCompAllocBuddy                                          */
/*                                                                           */
/* Description  : This function is used to allocate the buffer for the v2    */
/*                messages. If the received v1 message is Sync allocate      */
/*                two buffer for v2 Announce and v2 Sync message.            */
/*                                                                           */
/* Input        : pPtpCompRxInfo  - PTP Tx informantion structure            */
/*                u1PtpV1Control  - PTP V1 Message Type                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpCompAllocBuddy (tPtpCompRxInfo * pPtpCompRxInfo, UINT1 u1PtpV1Control)
{
    PTP_FN_ENTRY ();

    if (u1PtpV1Control == PTP_V1_SYNC_MESSAGE)
    {
        /* Allocate the V2 Announce message buffer */
        pPtpCompRxInfo->pu1Ptpv2AnncPdu =
            MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                           PTP_V1_TO_V2_MAX_MSG_LEN);
        if (pPtpCompRxInfo->pu1Ptpv2AnncPdu == NULL)
        {
            PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpCompAllocBuddy: Buddy Memory Allocation Failed \r\n"));
            PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }
        MEMSET (pPtpCompRxInfo->pu1Ptpv2AnncPdu, 0, PTP_V1_TO_V2_MAX_MSG_LEN);
    }

    /* Allocat the V2 message specific buffer */
    pPtpCompRxInfo->Rx.pu1Ptpv2Pdu =
        MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                       PTP_V1_TO_V2_MAX_MSG_LEN);
    if (pPtpCompRxInfo->Rx.pu1Ptpv2Pdu == NULL)
    {
        PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpCompAllocBuddy: PDU Memory Allocation Failed \r\n"));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }
    MEMSET (pPtpCompRxInfo->Rx.pu1Ptpv2Pdu, 0, PTP_V1_TO_V2_MAX_MSG_LEN);
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpCompFreeBuddy                                           */
/*                                                                           */
/* Description  : This function is used to free the the buffer allocate for  */
/*                v1 and v2 messages                                         */
/*                                                                           */
/* Input        : pPtpCompRxInfo  - PTP Tx informantion structure            */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpCompFreeBuddy (tPtpCompRxInfo * pPtpCompRxInfo)
{
    PTP_FN_ENTRY ();

    /* Release the V2 Announce message buffer */
    if (pPtpCompRxInfo->pu1Ptpv2AnncPdu != NULL)
    {
        if ((MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                           pPtpCompRxInfo->pu1Ptpv2AnncPdu)) == BUDDY_FAILURE)
        {
            PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpCompFreeBuddy: Buddy Memory DeAllocation Failed \r\n"));
            PTP_FN_EXIT ();
            return;
        }
        pPtpCompRxInfo->pu1Ptpv2AnncPdu = NULL;
    }

    /* Release the V2 message specific buffer */
    if (pPtpCompRxInfo->Rx.pu1Ptpv2Pdu != NULL)
    {
        if ((MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                           pPtpCompRxInfo->Rx.pu1Ptpv2Pdu)) == BUDDY_FAILURE)
        {
            PTP_TRC ((PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpCompFreeBuddy: PDU Memory DeAllocation Failed \r\n"));
            PTP_FN_EXIT ();
            return;
        }
        pPtpCompRxInfo->Rx.pu1Ptpv2Pdu = NULL;
    }

    PTP_FN_EXIT ();
}

#endif /*  _PTPCOMP_C_ */

/***************************************************************************
 *                         END OF FILE ptpcomp.c                           *
 ***************************************************************************/
