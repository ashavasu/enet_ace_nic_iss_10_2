/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpcomtx.c,v 1.4 2014/02/03 12:27:08 siva Exp $
 *
 * Description: This file contains PTP Compatability Sub module functionality
 *              routines for Tx.
 *********************************************************************/
#ifndef _PTPCOMTX_C_
#define _PTPCOMTX_C_

#include "ptpincs.h"

PRIVATE INT4
 
 
 
 PtpComTxV2SyncToV1Sync (tPtpCompTxInfo * pPtpCompTxInfo,
                         tPtpv2Info * pPtpv2Info, tPtpv1Info * pPtpv1Info);
PRIVATE INT4        PtpComTxV2DReqToV1DReq (tPtpCompTxInfo * pPtpCompTxInfo,
                                            tPtpv2Info * pPtpv2Info,
                                            tPtpv1Info * pPtpv1Info);
PRIVATE INT4        PtpComTxV2FolloUpToV1FollowUp (tPtpCompTxInfo *
                                                   pPtpCompTxInfo,
                                                   tPtpv2Info * pPtpv2Info,
                                                   tPtpv1Info * pPtpv1Info);
PRIVATE INT4        PtpComTxV2DRespToV1DResp (tPtpCompTxInfo * pPtpCompTxInfo,
                                              tPtpv2Info * pPtpv2Info,
                                              tPtpv1Info * pPtpv1Info);
PRIVATE VOID        PtpComTxGetV2InfoFromPkt (UINT1 *pu1v2Msg,
                                              tPtpv2Info * pPtpv2Info);
PRIVATE INT4        PtpComTxMapV2InfoToV1Info (tPtpv2Info * pPtpv2Info,
                                               tPtpv1Info * pPtpv1Info);
PRIVATE VOID        PtpComTxPackV1PtpPkt (UINT2 u2PktType,
                                          tPtpv1Info * pPtpv1Info,
                                          UINT1 *pu1v1Msg);
PRIVATE INT4        PtpComTxGetV2InfoFromPtpDs (UINT4 u4ContextId,
                                                UINT1 u1DomainId,
                                                UINT4 u4PortId,
                                                tPtpv2Info * pPtpv2Info,
                                                tPtpv1Info * pPtpv1Info);
PRIVATE INT4        PtpComTxMapV2InfoToV1CommonInfo (tPtpv2Info * pPtpv2Info,
                                                     tPtpv1Info * pPtpv1Info);

/*****************************************************************************/
/* Function     : PtpComTxV2ToV1PtpPdu                                       */
/*                                                                           */
/* Description  : This function is used to form v1 Sync message from the     */
/*                v2 Sync message                                            */
/*                                                                           */
/* Input        : pPtpCompTxInfo - Compatability Tx information values       */
/*                                                                           */
/* Output       : Trigger the following                                      */
/*              : 1.PtpComTxGetV2InfoFromPkt                                 */
/*              : 2.PtpComTxMapV2InfoToV1Info                                */
/*              : 3.PtpComTxPackV1PtpPkt                                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpComTxV2ToV1PtpPdu (tPtpCompTxInfo * pPtpCompTxInfo)
{
    tPtpv2Info         *pPtpv2Info = NULL;
    tPtpv1Info         *pPtpv1Info = NULL;
    UINT1              *pu1TmpPtpv2Pdu = NULL;
    UINT4               u4MediaHdrLen =
        PTP_GET_MEDIA_HDR_LEN (pPtpCompTxInfo->Tx.pPtpPort);
    INT4                i4Status = OSIX_FAILURE;
    UINT1               u1MsgType = 0;

    PTP_FN_ENTRY ();

    pu1TmpPtpv2Pdu = pPtpCompTxInfo->Tx.pu1Ptpv2Pdu + u4MediaHdrLen;

    if (u4MediaHdrLen != 0)
    {
        MEMCPY (pPtpCompTxInfo->Tx.pu1Ptpv1Pdu, pPtpCompTxInfo->Tx.pu1Ptpv2Pdu,
                u4MediaHdrLen);
    }

    /* Assign the Globle V1 and V2 info */
    pPtpv1Info = &(gPtpGlobalInfo.Ptpv1Info);
    pPtpv2Info = &(gPtpGlobalInfo.Ptpv2Info);

    /* Translate the Version2 of the outgoing packet to Version1
     * of the type of received Message type.
     */
    PTP_LBUF_GET_1_BYTE (pu1TmpPtpv2Pdu, u1MsgType);
    u1MsgType = (u1MsgType & PTP_MSG_TYPE_MASK);

    switch (u1MsgType)
    {
        case PTP_SYNC_MSG:
            i4Status = PtpComTxV2SyncToV1Sync (pPtpCompTxInfo, pPtpv2Info,
                                               pPtpv1Info);
            break;

        case PTP_DELAY_REQ_MSG:
            i4Status = PtpComTxV2DReqToV1DReq (pPtpCompTxInfo, pPtpv2Info,
                                               pPtpv1Info);
            break;

        case PTP_FOLLOW_UP_MSG:
            i4Status = PtpComTxV2FolloUpToV1FollowUp (pPtpCompTxInfo,
                                                      pPtpv2Info, pPtpv1Info);
            break;

        case PTP_DELAY_RESP_MSG:
            i4Status = PtpComTxV2DRespToV1DResp (pPtpCompTxInfo, pPtpv2Info,
                                                 pPtpv1Info);
            break;

        case PTP_ANNC_MSG:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      CONTROL_PLANE_TRC,
                      "PtpComTxV2ToV1PtpPdu : This Announce Message is "
                      "not valid in the v1 domain -Dropped \r\n"));
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "PtpComTxV2ToV1PtpPdu : This type of v2 messages are "
                      "not handled by Ptp compatibility sub module. \r\n"));
            break;
    }
    PTP_FN_EXIT ();
    return (i4Status);
}

/*****************************************************************************/
/* Function     : PtpComTxV2SyncToV1Sync                                     */
/*                                                                           */
/* Description  : This function is used to form v1 Sync message from the     */
/*                v2 Sync message                                            */
/*                                                                           */
/* Input        : pPtpCompTxInfo - Compatability Tx information values       */
/*              : pPtpv2Info     - PtpV2 information Pointer                 */
/*              : pPtpv1Info     - PtpV1 information Pointer                 */
/*                                                                           */
/* Output       : Trigger the following                                      */
/*              : 1.PtpComTxGetV2InfoFromPkt                                 */
/*              : 2.PtpComTxMapV2InfoToV1Info                                */
/*              : 3.PtpComTxPackV1PtpPkt                                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComTxV2SyncToV1Sync (tPtpCompTxInfo * pPtpCompTxInfo,
                        tPtpv2Info * pPtpv2Info, tPtpv1Info * pPtpv1Info)
{
    INT4                i4Status = OSIX_FAILURE;
    UINT4               u4MediaHdrLen =
        PTP_GET_MEDIA_HDR_LEN (pPtpCompTxInfo->Tx.pPtpPort);

    PTP_FN_ENTRY ();

    /* Get the v2 info to from v2 Msg Header */
    PtpComTxGetV2InfoFromPkt (pPtpCompTxInfo->Tx.pu1Ptpv2Pdu + u4MediaHdrLen,
                              pPtpv2Info);

    /* Get the Compatability Table info to form the v1 message */
    i4Status =
        PtpComTxGetV2InfoFromPtpDs (pPtpCompTxInfo->Tx.pPtpPort->u4ContextId,
                                    pPtpCompTxInfo->Tx.pPtpPort->u1DomainId,
                                    pPtpCompTxInfo->Tx.pPtpPort->PortDs.
                                    u4PtpPortNumber, pPtpv2Info, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxV2SyncToV1Sync : "
                  "PtpComTxGetV2InfoFromPtpDs failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }
    /* Convert the v1 info to v2 info */
    i4Status = PtpComTxMapV2InfoToV1Info (pPtpv2Info, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxV2SyncToV1Sync: "
                  "PtpComTxMapV2InfoToV1Info failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* form the v1 Packet from v1 info */
    PtpComTxPackV1PtpPkt (PTP_V1_SYNC_MESSAGE, pPtpv1Info,
                          pPtpCompTxInfo->Tx.pu1Ptpv1Pdu + u4MediaHdrLen);

    pPtpCompTxInfo->Tx.u4Ptpv1PduLen = PTP_V1_SYNC_MSG_LEN;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComTxV2DReqToV1DReq                                     */
/*                                                                           */
/* Description  : This function is used to form v1 Delay Request message     */
/*                from the v2 Delay Request message.                         */
/*                                                                           */
/* Input        : pPtpCompTxInfo - Compatability Tx information values       */
/*              : pPtpv2Info     - PtpV2 information Pointer                 */
/*              : pPtpv1Info     - PtpV1 information Pointer                 */
/*                                                                           */
/* Output       : Trigger the following                                      */
/*              : 1.PtpComTxGetV2InfoFromPkt                                 */
/*              : 2.PtpComTxMapV2InfoToV1Info                                */
/*              : 3.PtpComTxPackV1PtpPkt                                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComTxV2DReqToV1DReq (tPtpCompTxInfo * pPtpCompTxInfo,
                        tPtpv2Info * pPtpv2Info, tPtpv1Info * pPtpv1Info)
{
    INT4                i4Status = OSIX_FAILURE;
    UINT4               u4MediaHdrLen =
        PTP_GET_MEDIA_HDR_LEN (pPtpCompTxInfo->Tx.pPtpPort);

    PTP_FN_ENTRY ();

    /* Get the v1 info to from v2 Msg Header */
    PtpComTxGetV2InfoFromPkt (pPtpCompTxInfo->Tx.pu1Ptpv2Pdu + u4MediaHdrLen,
                              pPtpv2Info);

    /* Get the Compatability Table info to form the v1 message */
    i4Status =
        PtpComTxGetV2InfoFromPtpDs (pPtpCompTxInfo->Tx.pPtpPort->u4ContextId,
                                    pPtpCompTxInfo->Tx.pPtpPort->u1DomainId,
                                    pPtpCompTxInfo->Tx.pPtpPort->PortDs.
                                    u4PtpPortNumber, pPtpv2Info, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxV2DReqToV1DReq : "
                  "PtpComTxGetV2InfoFromPtpDs failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* Convert the v2 info to v1 info */
    i4Status = PtpComTxMapV2InfoToV1Info (pPtpv2Info, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxV2DReqToV1DReq :"
                  " PtpComTxMapV2InfoToV1Info failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* Form the v1 Packet from v1 info */
    PtpComTxPackV1PtpPkt (PTP_V1_DELAY_REQ_MESSAGE, pPtpv1Info,
                          pPtpCompTxInfo->Tx.pu1Ptpv1Pdu + u4MediaHdrLen);

    pPtpCompTxInfo->Tx.u4Ptpv1PduLen = PTP_V1_DELAY_REQ_MSG_LEN;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComTxV2FolloUpToV1FollowUp                              */
/*                                                                           */
/* Description  : This function is used to form v1 Follow up message         */
/*                from the v2 Follow up message.                             */
/*                                                                           */
/* Input        : pPtpCompTxInfo - Compatability Tx information values       */
/*              : pPtpv2Info     - PtpV2 information Pointer                 */
/*              : pPtpv1Info     - PtpV1 information Pointer                 */
/*                                                                           */
/* Output       : Trigger the following                                      */
/*              : 1.PtpComTxGetV2InfoFromPkt                                 */
/*              : 2.PtpComTxMapV2InfoToV1Info                                */
/*              : 3.PtpComTxPackV1PtpPkt                                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComTxV2FolloUpToV1FollowUp (tPtpCompTxInfo * pPtpCompTxInfo,
                               tPtpv2Info * pPtpv2Info, tPtpv1Info * pPtpv1Info)
{
    INT4                i4Status = OSIX_FAILURE;
    UINT4               u4MediaHdrLen =
        PTP_GET_MEDIA_HDR_LEN (pPtpCompTxInfo->Tx.pPtpPort);

    PTP_FN_ENTRY ();

    /* Get the v2 info to from v1 Msg Header */
    PtpComTxGetV2InfoFromPkt (pPtpCompTxInfo->Tx.pu1Ptpv2Pdu + u4MediaHdrLen,
                              pPtpv2Info);

    /* Convert the v2 info to v1 info */
    i4Status = PtpComTxMapV2InfoToV1Info (pPtpv2Info, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxV2FolloUpToV1FollowUp :"
                  " PtpComTxMapV2InfoToV1Info failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }
    /* Form the v1 Packet from v1 info */
    PtpComTxPackV1PtpPkt (PTP_V1_FOLLOWUP_MESSAGE, pPtpv1Info,
                          pPtpCompTxInfo->Tx.pu1Ptpv1Pdu + u4MediaHdrLen);

    pPtpCompTxInfo->Tx.u4Ptpv1PduLen = PTP_V1_FOLLOW_UP_MSG_LEN;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComTxV2DRespToV1DResp                                   */
/*                                                                           */
/* Description  : This function is used to form v1 Delay Response message    */
/*                from the v2 Delay Response message.                        */
/*                                                                           */
/* Input        : pPtpCompTxInfo - Compatability Tx information values       */
/*              : pPtpv2Info     - PtpV2 information Pointer                 */
/*              : pPtpv1Info     - PtpV1 information Pointer                 */
/*                                                                           */
/* Output       : Trigger the following                                      */
/*              : 1.PtpComTxGetV2InfoFromPkt                                 */
/*              : 2.PtpComTxMapV2InfoToV1Info                                */
/*              : 3.PtpComTxPackV1PtpPkt                                     */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComTxV2DRespToV1DResp (tPtpCompTxInfo * pPtpCompTxInfo,
                          tPtpv2Info * pPtpv2Info, tPtpv1Info * pPtpv1Info)
{
    INT4                i4Status = OSIX_FAILURE;
    UINT4               u4MediaHdrLen =
        PTP_GET_MEDIA_HDR_LEN (pPtpCompTxInfo->Tx.pPtpPort);

    PTP_FN_ENTRY ();

    /* Get the v2 info to from v2 Msg Header */
    PtpComTxGetV2InfoFromPkt (pPtpCompTxInfo->Tx.pu1Ptpv2Pdu + u4MediaHdrLen,
                              pPtpv2Info);

    /* Get the Compatability Table info to form the v1 message */
    i4Status =
        PtpComTxGetV2InfoFromPtpDs (pPtpCompTxInfo->Tx.pPtpPort->u4ContextId,
                                    pPtpCompTxInfo->Tx.pPtpPort->u1DomainId,
                                    pPtpCompTxInfo->Tx.pPtpPort->PortDs.
                                    u4PtpPortNumber, pPtpv2Info, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxV2DReqToV1DReq : "
                  "PtpComTxGetV2InfoFromPtpDs failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* Convert the v2 info to v1 info */
    i4Status = PtpComTxMapV2InfoToV1Info (pPtpv2Info, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxV2DRespToV1DResp :"
                  " PtpComTxMapV2InfoToV1Info failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    /* Form the v1 Packet from v1 info */
    PtpComTxPackV1PtpPkt (PTP_V1_DELAY_RESP_MESSAGE, pPtpv1Info,
                          pPtpCompTxInfo->Tx.pu1Ptpv1Pdu + u4MediaHdrLen);

    pPtpCompTxInfo->Tx.u4Ptpv1PduLen = PTP_V1_DELAY_RES_MSG_LEN;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComTxGetV2InfoFromPkt                                   */
/*                                                                           */
/* Description  : This function is used to get the v2 information form the   */
/*                v2 Message                                                 */
/*                                                                           */
/* Input        : pu1v2Msg    - V2 Message                                   */
/*              : pPtpv2Info  - Pointer to tPtpv2Info Type                   */
/*                                                                           */
/* Output       : On Success                                                 */
/*              : pPtpv2Info - Ptpv2Info information                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpComTxGetV2InfoFromPkt (UINT1 *pu1v2Msg, tPtpv2Info * pPtpv2Info)
{
    UINT4               u4Resvd = 0;
    UINT2               u2Val = 0;
    UINT1               u1Val = 0;

    PTP_FN_ENTRY ();

    /* Get v2 info from the V2 Common header fields */
    PTP_LBUF_GET_1_BYTE (pu1v2Msg, u1Val);

    pPtpv2Info->ComHdr.u1TranSpc = (u1Val & PTP_TRANS_SPE_MASK);
    pPtpv2Info->ComHdr.u1MsgType = (u1Val & PTP_MSG_TYPE_MASK);
    PTP_LBUF_GET_1_BYTE (pu1v2Msg, pPtpv2Info->ComHdr.u1PtpVersion);
    PTP_LBUF_GET_2_BYTES (pu1v2Msg, pPtpv2Info->ComHdr.u2MsgLen);
    PTP_LBUF_GET_1_BYTE (pu1v2Msg, pPtpv2Info->ComHdr.u1DomainId);

    PTP_LBUF_GET_1_BYTE (pu1v2Msg, u1Val);
    PTP_LBUF_GET_N_BYTES (pu1v2Msg, pPtpv2Info->ComHdr.ai1FlagField,
                          PTP_V2_FLAG_FIELD_LEN);

    PTP_LBUF_GET_4_BYTES (pu1v2Msg,
                          (pPtpv2Info->ComHdr.u8CorrectionField.u4Hi));
    PTP_LBUF_GET_4_BYTES (pu1v2Msg,
                          (pPtpv2Info->ComHdr.u8CorrectionField.u4Lo));

    PTP_LBUF_GET_4_BYTES (pu1v2Msg, u4Resvd);
    PTP_LBUF_GET_N_BYTES (pu1v2Msg, pPtpv2Info->ComHdr.SrcPortId.ClkId,
                          PTP_MAX_CLOCK_ID_LEN);
    PTP_LBUF_GET_2_BYTES (pu1v2Msg, pPtpv2Info->ComHdr.SrcPortId.u2PortNumber);
    PTP_LBUF_GET_2_BYTES (pu1v2Msg, pPtpv2Info->ComHdr.u2SeqId);
    PTP_LBUF_GET_1_BYTE (pu1v2Msg, pPtpv2Info->ComHdr.u1ContField);
    PTP_LBUF_GET_1_BYTE (pu1v2Msg, pPtpv2Info->ComHdr.i1LogMsgInter);

    switch (pPtpv2Info->ComHdr.u1MsgType)
    {
        case PTP_SYNC_MSG:
        case PTP_DELAY_REQ_MSG:
            PTP_LBUF_GET_2_BYTES (pu1v2Msg, u2Val);
            pPtpv2Info->SyncDReqHdr.OrigTimestamp.u8Sec.u4Hi = (UINT2) u2Val;

            PTP_LBUF_GET_4_BYTES (pu1v2Msg, (pPtpv2Info->SyncDReqHdr.
                                             OrigTimestamp.u8Sec.u4Lo));
            PTP_LBUF_GET_4_BYTES (pu1v2Msg, pPtpv2Info->SyncDReqHdr.
                                  OrigTimestamp.u4NanoSec);
            break;

        case PTP_FOLLOW_UP_MSG:
            PTP_LBUF_GET_2_BYTES (pu1v2Msg, u2Val);
            pPtpv2Info->FollowUpHdr.PreOrigTimestamp.u8Sec.u4Hi = (UINT2) u2Val;
            PTP_LBUF_GET_4_BYTES (pu1v2Msg, (pPtpv2Info->FollowUpHdr.
                                             PreOrigTimestamp.u8Sec.u4Lo));
            PTP_LBUF_GET_4_BYTES (pu1v2Msg, pPtpv2Info->FollowUpHdr.
                                  PreOrigTimestamp.u4NanoSec);
            break;

        case PTP_DELAY_RESP_MSG:
            PTP_LBUF_GET_2_BYTES (pu1v2Msg, u2Val);
            pPtpv2Info->DRespHdr.RecTimestamp.u8Sec.u4Hi = (UINT2) u2Val;
            PTP_LBUF_GET_4_BYTES (pu1v2Msg, (pPtpv2Info->DRespHdr.
                                             RecTimestamp.u8Sec.u4Lo));
            PTP_LBUF_GET_4_BYTES (pu1v2Msg, pPtpv2Info->DRespHdr.
                                  RecTimestamp.u4NanoSec);
            PTP_LBUF_GET_N_BYTES (pu1v2Msg, pPtpv2Info->DRespHdr.
                                  ReqPortId.ClkId, PTP_MAX_CLOCK_ID_LEN);
            PTP_LBUF_GET_2_BYTES (pu1v2Msg, pPtpv2Info->DRespHdr.
                                  ReqPortId.u2PortNumber);
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpComTxGetV2InfoFromPkt : Invalid Message Type.\r\n"));
            break;
    }
    UNUSED_PARAM (u4Resvd);
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpComTxMapV2InfoToV1CommonInfo                            */
/*                                                                           */
/* Description  : This function is used to map the v2 information to         */
/*                corressponding v1 information which are used to form       */
/*                comman header for the v1 Messages                          */
/*                                                                           */
/* Input        : pPtpv2Info  - Pointer to tPtpv2Info Type                   */
/*              : pPtpv1Info  - Pointer to tPtpv1Info Type                   */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComTxMapV2InfoToV1CommonInfo (tPtpv2Info * pPtpv2Info,
                                 tPtpv1Info * pPtpv1Info)
{
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    i4Status =
        PtpCmUtlv2DomainTov1SubDomain (pPtpv2Info->ComHdr.u1DomainId,
                                       pPtpv1Info->ComHdr.ai1SubDomain);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxMapV2InfoToV1CommonInfo :"
                  " PtpCmUtlv2DomainTov1SubDomain failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    i4Status =
        PtpCmUtlv2MsgTypeTov1ContMsgType (pPtpv2Info->ComHdr.u1MsgType,
                                          &(pPtpv1Info->ComHdr.u1Control),
                                          &(pPtpv1Info->ComHdr.u1MsgType));
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxMapV2InfoToV1CommonInfo :"
                  " PtpCmUtlv2MsgTypeTov1ContMsgType failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    PtpCmUtlv2SrcPortIdTov1SrcPortId (&(pPtpv2Info->ComHdr.SrcPortId.ClkId),
                                      pPtpv2Info->ComHdr.SrcPortId.
                                      u2PortNumber,
                                      &(pPtpv1Info->ComHdr.u1SrcComTech),
                                      (pPtpv1Info->ComHdr.ai1SourceUuid),
                                      &(pPtpv1Info->ComHdr.u2SrcPortId));

    pPtpv1Info->ComHdr.u2SeqId = pPtpv2Info->ComHdr.u2SeqId;
    pPtpv1Info->ComHdr.u1Control = pPtpv2Info->ComHdr.u1ContField;

    i4Status = PtpCmUtlv2FlagTov1Flag (pPtpv2Info->ComHdr.ai1FlagField,
                                       pPtpv1Info->ComHdr.ai1Flags);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxMapV2InfoToV1CommonInfo :"
                  " PtpCmUtlv2FlagTov1Flag failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    pPtpv1Info->ComHdr.u2ver = PTP_VERSION_ONE;
    pPtpv1Info->ComHdr.u2verNetwork = PTP_V1_MSG_NETWORK_VAL;

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComTxMapV2InfoToV1Info                                  */
/*                                                                           */
/* Description  : This function is used to map the v2 information to         */
/*                corressponding v1 information.                             */
/*                                                                           */
/* Input        : pPtpv2Info  - Ptpv2Info                                    */
/*              : pPtpv1Info  - Pointer to tPtpv1Info Type                   */
/*                                                                           */
/* Output       : On Success                                                 */
/*                pPtpv1Info Ptpv1Info                                       */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComTxMapV2InfoToV1Info (tPtpv2Info * pPtpv2Info, tPtpv1Info * pPtpv1Info)
{
    tPtpv1SyncDReqHdr  *pSyncDReqHdr = NULL;
    INT4                i4Status = OSIX_FAILURE;

    PTP_FN_ENTRY ();

    i4Status = PtpComTxMapV2InfoToV1CommonInfo (pPtpv2Info, pPtpv1Info);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxMapV2InfoToV1Info : PtpComTxMapV2InfoToV1CommonInfo"
                  " failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    switch (pPtpv2Info->ComHdr.u1MsgType)
    {
        case PTP_SYNC_MSG:
        case PTP_DELAY_REQ_MSG:
            /* Sync / Delay Req */
            pSyncDReqHdr = &(pPtpv1Info->SyncDReqHdr);

            PtpCmUtlv2TimStpTov1TimStp (&(pPtpv2Info->SyncDReqHdr.
                                          OrigTimestamp),
                                        &(pSyncDReqHdr->OrigTimestamp),
                                        &(pSyncDReqHdr->u2EpochNum));

            pSyncDReqHdr->i2CurrUTCOffset = pPtpv2Info->AnncHdr.i2CurrUTCOffset;

            PtpCmUtlv2SrcPortIdTov1SrcPortId (&(pPtpv2Info->AnncHdr.GMClkId),
                                              pPtpv2Info->u2GMPortId,
                                              &(pSyncDReqHdr->u1GMCommTech),
                                              pSyncDReqHdr->ai1GMClkUuid,
                                              &(pSyncDReqHdr->u2GMPortId));

            pSyncDReqHdr->u2GMSequenceId = pPtpv2Info->u2GMSeqId;
            i4Status =
                PtpCmUtlv2ClkQualTov1Fields (&(pPtpv2Info->AnncHdr.
                                               GMClkQuality),
                                             &(pSyncDReqHdr->u1GMClkStratum),
                                             pSyncDReqHdr->ai1GMClkId,
                                             &(pSyncDReqHdr->i2GMClkVar));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComTxMapV2InfoToV1Info :"
                          " PtpCmUtlv2ClkQualTov1Fields failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }

            i4Status =
                PtpCmUtlv2Pri1Tov1PrefClkStratum (pPtpv2Info->AnncHdr.u1GMPri1,
                                                  pPtpv2Info->AnncHdr.
                                                  GMClkQuality.u1Class,
                                                  &(pSyncDReqHdr->bGMPreferred),
                                                  &(pSyncDReqHdr->
                                                    u1GMClkStratum));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComTxMapV2InfoToV1Info :"
                          " PtpCmUtlv2Pri1Tov1PrefClkStratum failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }

            pSyncDReqHdr->bGMIsBoundaryClk =
                ((pPtpv2Info->AnncHdr.u1GMPri2 > PTP_V2_PRIORITY2_VAL_128) ?
                 PTP_V1_IS_BOUNDARY_CLK_TRUE : PTP_V1_IS_BOUNDARY_CLK_FALSE);

            pSyncDReqHdr->i1SyncInterval = pPtpv2Info->ComHdr.i1LogMsgInter;

            i4Status =
                PtpCmUtlv2ClkQualTov1Fields (&(pPtpv2Info->LocalClkQuality),
                                             &(pSyncDReqHdr->u1LocalClkStratum),
                                             pSyncDReqHdr->ai1LocalClkId,
                                             &(pSyncDReqHdr->i2LocalClkVar));
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS,
                          ALL_FAILURE_TRC,
                          "PtpComTxMapV2InfoToV1Info :"
                          " PtpCmUtlv2ClkQualTov1Fields failed.\r\n"));
                PTP_FN_EXIT ();
                return (OSIX_FAILURE);
            }

            pSyncDReqHdr->u2LocalStepsRemoved =
                pPtpv2Info->AnncHdr.u2StepsRemoved;

            PtpCmUtlv2SrcPortIdTov1SrcPortId (&(pPtpv2Info->ParentPortId.ClkId),
                                              pPtpv2Info->ParentPortId.
                                              u2PortNumber,
                                              &(pSyncDReqHdr->u1ParentCommTech),
                                              pSyncDReqHdr->ai1ParentUuid,
                                              &(pSyncDReqHdr->
                                                u2ParentPortField));

            pSyncDReqHdr->i2EstMstVar =
                pPtpv2Info->u2ObsParentOffsetScaledLogVar;

            pSyncDReqHdr->i4EstMstDrift =
                pPtpv2Info->i4ObsParentClkPhaseCngRate;

            pSyncDReqHdr->bUtcReasonable = pPtpv2Info->bCurrUtcOffsetValid;

            break;

        case PTP_DELAY_RESP_MSG:
            /* V1 Delay Resp */
            PtpCmUtlv2TimStpTov1TimStp (&(pPtpv2Info->DRespHdr.RecTimestamp),
                                        &(pPtpv1Info->DRespHdr.DRecTimestamp),
                                        &(pPtpv1Info->SyncDReqHdr.u2EpochNum));

            PtpCmUtlv2SrcPortIdTov1SrcPortId (&(pPtpv2Info->DRespHdr.
                                                ReqPortId.ClkId),
                                              pPtpv2Info->DRespHdr.
                                              ReqPortId.u2PortNumber,
                                              &(pPtpv1Info->DRespHdr.
                                                u1ReqSrcCommTech),
                                              (pPtpv1Info->DRespHdr.
                                               ai1ReqSrcUuid),
                                              &(pPtpv1Info->DRespHdr.
                                                u2ReqSrcPortId));

            pPtpv1Info->DRespHdr.u2ReqSrcSeqId = pPtpv2Info->ComHdr.u2SeqId;

            break;

        case PTP_FOLLOW_UP_MSG:
            /* Form V1 Follow up */
            pPtpv1Info->FollowUpHdr.u2AssoSeqId = pPtpv2Info->ComHdr.u2SeqId;

            PtpCmUtlv2TimStpTov1TimStp (&(pPtpv2Info->FollowUpHdr.
                                          PreOrigTimestamp),
                                        &(pPtpv1Info->FollowUpHdr.
                                          PreOrigTimestamp),
                                        &(pPtpv1Info->SyncDReqHdr.u2EpochNum));

            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpComTxMapV2InfoToV1Info : Invalid Message Type.\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
            break;
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpComTxPackV1PtpPkt                                       */
/*                                                                           */
/* Description  : This function is used to form the v1 Message form the      */
/*                corressponding v1 information.                             */
/*                                                                           */
/* Input        : u2PktType   - V1 Message Type                              */
/*              : pPtpv1Info  - Ptpv1Info                                    */
/*              : pu1Ptpv1Msg - Pointer to V1 Message                        */
/*                                                                           */
/* Output       : pu1Ptpv1Msg  -  V1 Message                                 */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpComTxPackV1PtpPkt (UINT2 u2PktType, tPtpv1Info * pPtpv1Info, UINT1 *pu1v1Msg)
{
    UINT1              *pu1Tmpv1Msg = pu1v1Msg;
    UINT2               u2Resvd = 0;
    UINT1               u1Resvd = 0;

    PTP_FN_ENTRY ();

    /* form the V1 Common header fields */
    PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->ComHdr.u2ver);
    PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->ComHdr.u2verNetwork);
    PTP_LBUF_PUT_N_BYTES (pu1Tmpv1Msg, pPtpv1Info->ComHdr.ai1SubDomain,
                          PTP_SUBDOMAIN_NAME_LENGTH);
    PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->ComHdr.u1MsgType);
    PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->ComHdr.u1SrcComTech);
    PTP_LBUF_PUT_N_BYTES (pu1Tmpv1Msg, pPtpv1Info->ComHdr.ai1SourceUuid,
                          PTP_UUID_LENGTH);
    PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->ComHdr.u2SrcPortId);
    PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->ComHdr.u2SeqId);
    PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->ComHdr.u1Control);
    PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, u1Resvd);
    PTP_LBUF_PUT_N_BYTES (pu1Tmpv1Msg, pPtpv1Info->ComHdr.ai1Flags,
                          PTP_V1_FLAG_FIELD_LEN);

    switch (u2PktType)
    {
        case PTP_V1_SYNC_MESSAGE:
        case PTP_V1_DELAY_REQ_MESSAGE:
            /* Collect the V1 Sync/Delay Request header fields */
            pu1Tmpv1Msg = pu1Tmpv1Msg + PTP_V1_COMM_SYNC_HEADER_PAD;

            PTP_LBUF_PUT_4_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  OrigTimestamp.u4Sec);
            PTP_LBUF_PUT_4_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  OrigTimestamp.i4NanoSec);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  u2EpochNum);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  i2CurrUTCOffset);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 u1GMCommTech);
            PTP_LBUF_PUT_N_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  ai1GMClkUuid, PTP_UUID_LENGTH);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  u2GMPortId);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  u2GMSequenceId);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 u1GMClkStratum);
            PTP_LBUF_PUT_N_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  ai1GMClkId, PTP_CODE_STRING_LENGTH);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  i2GMClkVar);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 bGMPreferred);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 bGMIsBoundaryClk);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 i1SyncInterval);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  i2LocalClkVar);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  u2LocalStepsRemoved);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 u1LocalClkStratum);
            PTP_LBUF_PUT_N_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  ai1LocalClkId, PTP_CODE_STRING_LENGTH);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 u1ParentCommTech);
            PTP_LBUF_PUT_N_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  ai1ParentUuid, PTP_UUID_LENGTH);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  u2ParentPortField);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  i2EstMstVar);
            PTP_LBUF_PUT_4_BYTES (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                  i4EstMstDrift);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, u1Resvd);    /* Reserved 1Byte */
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, u2Resvd);    /* Reserved 2Byte */
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->SyncDReqHdr.
                                 bUtcReasonable);
            break;

        case PTP_V1_FOLLOWUP_MESSAGE:
            /* Collect the V1 Delay Response header fields */
            pu1Tmpv1Msg = pu1Tmpv1Msg + PTP_V1_COMM_FOLLOW_UP_HEADER_PAD;

            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->FollowUpHdr.
                                  u2AssoSeqId);
            PTP_LBUF_PUT_4_BYTES (pu1Tmpv1Msg, pPtpv1Info->FollowUpHdr.
                                  PreOrigTimestamp.u4Sec);
            PTP_LBUF_PUT_4_BYTES (pu1Tmpv1Msg, pPtpv1Info->FollowUpHdr.
                                  PreOrigTimestamp.i4NanoSec);
            break;

        case PTP_V1_DELAY_RESP_MESSAGE:
            /* Collect the V1 Delay Response header fields */
            pu1Tmpv1Msg = pu1Tmpv1Msg + PTP_V1_COMM_D_RES_HEADER_PAD;

            PTP_LBUF_PUT_4_BYTES (pu1Tmpv1Msg, pPtpv1Info->DRespHdr.
                                  DRecTimestamp.u4Sec);
            PTP_LBUF_PUT_4_BYTES (pu1Tmpv1Msg, pPtpv1Info->DRespHdr.
                                  DRecTimestamp.i4NanoSec);
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, u1Resvd);    /* Reserved Byte */
            PTP_LBUF_PUT_1_BYTE (pu1Tmpv1Msg, pPtpv1Info->DRespHdr.
                                 u1ReqSrcCommTech);
            PTP_LBUF_PUT_N_BYTES (pu1Tmpv1Msg, pPtpv1Info->DRespHdr.
                                  ai1ReqSrcUuid, PTP_UUID_LENGTH);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->DRespHdr.
                                  u2ReqSrcPortId);
            PTP_LBUF_PUT_2_BYTES (pu1Tmpv1Msg, pPtpv1Info->DRespHdr.
                                  u2ReqSrcSeqId);
            break;

        default:
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpComTxPackV1PtpPkt : Invalid Message Type\r\n"));
            break;
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpComTxGetV2InfoFromPtpDs                                 */
/*                                                                           */
/* Description  : This function is used to get the v2 PTP data set values    */
/*                to form the v1 Message.                                    */
/*                                                                           */
/* Input        : u4ContextId - Context Identifier.                          */
/*              : u1DomainId  - Domain Identifier.                           */
/*              : u4PortId    - PTP Port Identifier.                         */
/*              : pPtpv2Info  - Pointer to tPtpv2Info Type                   */
/*              : pPtpv1Info  - Pointer to tPtpv1Info Type                   */
/*                                                                           */
/* Output       : pu1Ptpv1Msg  -  V1 Message                                 */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpComTxGetV2InfoFromPtpDs (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4PortId,
                            tPtpv2Info * pPtpv2Info, tPtpv1Info * pPtpv1Info)
{
    tPtpCompDBEntry     PtpCompDBEntry;
    tPtpCompDBEntry    *pPtpCompDB = NULL;
    tPtpPort           *pPtpPort = NULL;

    PTP_FN_ENTRY ();

    pPtpPort = PtpIfGetPortEntry (u4ContextId, u1DomainId, u4PortId);
    if (pPtpPort == NULL)
    {
        PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                  ALL_FAILURE_TRC,
                  "PtpComTxGetV2InfoFromPtpDs : "
                  "PtpDbGetPortEntry failed.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    PtpUtilGetV2AnncInfo (pPtpPort, pPtpv2Info);

    if (pPtpv2Info->ComHdr.u1MsgType == PTP_DELAY_RESP_MSG)
    {
        MEMSET (&PtpCompDBEntry, 0, sizeof (tPtpCompDBEntry));
        PtpCompDBEntry.u4ContextId = u4ContextId;
        PtpCompDBEntry.u1DomainId = u1DomainId;
        PtpCompDBEntry.u2PortId = (UINT2) u4PortId;
        PtpCompDBEntry.u2SrcPortId = pPtpv1Info->ComHdr.u2SrcPortId;
        MEMCPY (&(PtpCompDBEntry.ai1v1SrcUuid),
                &(pPtpv1Info->ComHdr.ai1SourceUuid), PTP_UUID_LENGTH);
        MEMCPY (&(PtpCompDBEntry.ai1v1SubDomain),
                &(pPtpv1Info->ComHdr.ai1SubDomain), PTP_SUBDOMAIN_NAME_LENGTH);

        /* Update the u2GMSeqId from the CompDB Table */
        pPtpCompDB = PtpCompGetCompDbEntry (&PtpCompDBEntry);
        if (pPtpCompDB == NULL)
        {
            PTP_TRC ((u4ContextId, PTP_MAX_DOMAINS,
                      ALL_FAILURE_TRC,
                      "PtpComTxGetV2InfoFromPtpDs : PtpCompGetCompDbEntry "
                      " failed.\r\n"));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }

        /* Get the Info from the shadow table */
        pPtpv2Info->u2GMSeqId = pPtpCompDB->u2GMSeqId;
    }

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

#endif /*  _PTPCOMTX_C_ */

/***************************************************************************
 *                         END OF FILE ptpcomtx.c                          *
 ***************************************************************************/
