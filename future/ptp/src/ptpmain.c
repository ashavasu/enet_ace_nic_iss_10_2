/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *$Id: ptpmain.c,v 1.10 2014/01/24 12:20:08 siva Exp $
 * Description: This file contains PTP task main loop and initialization
 *              routines.
 *********************************************************************/
#ifndef _PTPMAIN_C_
#define _PTPMAIN_C_

#include "ptpincs.h"

PRIVATE INT4        PtpMainUnRegWithExtModules (VOID);
PRIVATE VOID        PtpMainDelMemPools (VOID);
PRIVATE INT4        PtpMainRegWithExtModules (VOID);
PRIVATE INT4        PtpMainCreateMemPools (VOID);
PRIVATE VOID        PtpMainDeleteBuddyMemPool (VOID);
PRIVATE INT4        PtpMainCreateBuddyMemPool (VOID);
PRIVATE VOID        PtpMainAssignMemPools (VOID);
/*****************************************************************************/
/* Function                  : PtpMainTask                                   */
/*                                                                           */
/* Description               : This is the initialization function for the   */
/*                             PTP module called during boot up time. This   */
/*                             function will wait for the various PTP events */
/*                             that are being triggered by external modules  */
/*                             and process those events once triggered.      */
/*                                                                           */
/* Input                     : pArg - Pointer to the arguments.              */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.TaskId                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpMainTask (INT1 *pArg)
{
    UINT4               u4Events = 0;

    UNUSED_PARAM (pArg);

    PTP_FN_ENTRY ();

    gPtpGlobalInfo.u2GblTrace = PTP_CRITICAL_TRC;
    gPtpGlobalInfo.i4Ipv4EvtSockId = -1;
    gPtpGlobalInfo.i4Ipv4GnlSockId = -1;
    gPtpGlobalInfo.i4Ipv6EvtSockId = -1;
    gPtpGlobalInfo.i4Ipv6GnlSockId = -1;

    if (PtpMainInit () == OSIX_FAILURE)
    {
        PtpMainDeInit ();
        /* Indicate the status of initialization to the main routine */
        lrInitComplete (OSIX_FAILURE);
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                  PTP_MAX_DOMAINS, ALL_FAILURE_TRC | INIT_SHUT_TRC,
                  "PtpMainTask: Module Initializaion Failed\r\n"));
    }
    /* Register the MIBs with SNMP Agent */
    RegisterFSPTP ();

    lrInitComplete (OSIX_SUCCESS);

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
              CONTROL_PLANE_TRC, "MSG: PTP Task context running ...\r\n"));

    while (PTP_ALWAYS)
    {
        if (OsixEvtRecv (gPtpGlobalInfo.TaskId,
                         (PTP_TMR_EXP_EVENT |
                          PTP_CONF_MSG_ENQ_EVENT |
                          PTP_IPv4_EVENTSOCK_PKT_ARRIVAL_EVENT |
                          PTP_IPv4_GENERALSOCK_PKT_ARRIVAL_EVENT |
                          PTP_IPv6_EVENTSOCK_PKT_ARRIVAL_EVENT |
                          PTP_IPv6_GENERALSOCK_PKT_ARRIVAL_EVENT) |
                         PTP_RM_MSG_EVENT,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            PtpApiLock ();

            if (u4Events & PTP_IPv4_EVENTSOCK_PKT_ARRIVAL_EVENT)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpMainTask: Recvd Ptp PDU Enqueue Event \r\n"));
                PtpUdpProcessPktOnSocket (gPtpGlobalInfo.i4Ipv4EvtSockId);

                if (SelAddFd (gPtpGlobalInfo.i4Ipv4EvtSockId,
                              PtpApiPktRcvdOnSocket) != OSIX_SUCCESS)
                {
                    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                              PTP_MAX_DOMAINS,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                              INIT_SHUT_TRC, "SelAddFd returned failure.\r\n"));
                }
            }

            if (u4Events & PTP_IPv4_GENERALSOCK_PKT_ARRIVAL_EVENT)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpMainTask: Recvd Ptp PDU Enqueue Event \r\n"));
                PtpUdpProcessPktOnSocket (gPtpGlobalInfo.i4Ipv4GnlSockId);

                if (SelAddFd (gPtpGlobalInfo.i4Ipv4GnlSockId,
                              PtpApiPktRcvdOnSocket) != OSIX_SUCCESS)
                {
                    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                              PTP_MAX_DOMAINS,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                              INIT_SHUT_TRC, "SelAddFd returned failure.\r\n"));
                }
            }

            if (u4Events & PTP_IPv6_EVENTSOCK_PKT_ARRIVAL_EVENT)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpMainTask: Recvd Ptp PDU Enqueue Event \r\n"));
                PtpUdpV6ProcessPktOnSocket (gPtpGlobalInfo.i4Ipv6EvtSockId);

                if (SelAddFd (gPtpGlobalInfo.i4Ipv6EvtSockId,
                              PtpApiPktRcvdOnSocket) != OSIX_SUCCESS)
                {
                    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                              PTP_MAX_DOMAINS,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                              INIT_SHUT_TRC, "SelAddFd returned failure.\r\n"));
                }
            }

            if (u4Events & PTP_IPv6_GENERALSOCK_PKT_ARRIVAL_EVENT)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpMainTask: Recvd Ptp PDU Enqueue Event \r\n"));
                PtpUdpV6ProcessPktOnSocket (gPtpGlobalInfo.i4Ipv6GnlSockId);

                if (SelAddFd (gPtpGlobalInfo.i4Ipv6GnlSockId,
                              PtpApiPktRcvdOnSocket) != OSIX_SUCCESS)
                {
                    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                              PTP_MAX_DOMAINS,
                              CONTROL_PLANE_TRC | ALL_FAILURE_TRC |
                              INIT_SHUT_TRC, "SelAddFd returned failure.\r\n"));
                }
            }
            if (u4Events & PTP_TMR_EXP_EVENT)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpMainTask: Recvd Ptp Tmr Exp Event \r\n"));
                PtpTmrExpHandler ();
            }

            if (u4Events & PTP_CONF_MSG_ENQ_EVENT)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpMainTask: Recvd Ptp Cfg Msg Event \r\n"));
                PtpQueMsgHandler ();
            }
#ifdef L2RED_WANTED
            if (u4Events & PTP_RM_MSG_EVENT)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID,
                          PTP_MAX_DOMAINS, CONTROL_PLANE_TRC,
                          "PtpMainTask: Recvd Ptp RM Event \r\n"));
                PtpRedHandleRmEvents ();
            }
#endif
            PtpApiUnLock ();
        }                        /* Received Event */
    }
}

/*****************************************************************************/
/* Function                  : PtpMainInit                                   */
/*                                                                           */
/* Description               : This routine takes care of initializing the   */
/*                             PTP module data structures and variables. This*/
/*                             routine creates the task queue, semaphores and*/
/*                             allocates Mempools.                           */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : gPtpGlobalInfo.PtpQId, gPtpGlobalInfo.TaskId  */
/*                             gPtpGlobalInfo.QMsgPoolId,                    */
/*                             gPtpGlobalInfo.SemId                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpMainInit (VOID)
{
    PTP_FN_ENTRY ();

    if (OsixSemCrt (PTP_SEM_NAME, &(gPtpGlobalInfo.SemId)) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainInit: Semaphore creation Failed \r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpApiUnLock () == SNMP_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainInit: Semaphore give Failed \r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (OsixQueCrt (PTP_PDU_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    PTP_PDU_Q_DEPTH, &(gPtpGlobalInfo.PtpQId)) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainInit: Queue creation Failed \r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PtpQueLoadMsgHandlers ();

    /* Create a socket and bind with udp and udp6 */
    if (PtpUdpBindWithUdp () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainInit: UDP socket creation failed!!!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpUdpV6BindWithUdpV6 () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainInit: UDP6 socket creation failed!!!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (OsixGetTaskId (SELF, PTP_TASK, &(gPtpGlobalInfo.TaskId))
        == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  OS_RESOURCE_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainInit: Obtaining Task Identifier failed!!! \r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    gPtpGlobalInfo.u1SysCtrlStatus = (UINT1) PTP_SHUTDOWN;
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : PtpMainModuleStart                                         */
/*                                                                           */
/* Description  : This routine allocates memory pools for all the required   */
/*                resources in PTP module. The resources include memory,     */
/*                timer.                                                     */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None                                                       */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpMainModuleStart (VOID)
{
    UINT4               u4Offset = 0;
    /* The structure tPtpUNegGranteeTbl and tPtpUNegGrantorTbl  is declared here locally,
     * to ensure that the sizing parameter structure is visible in debugger. 
     * This will be removed in the next subsequent changes in the sizing parameters.*/
    tPtpUNegGranteeTbl *pPtpUNegGteeTbl = NULL;
    tPtpUNegGrantorTbl *pPtpUNegGtorTbl = NULL;
    UNUSED_PARAM (pPtpUNegGteeTbl);
    UNUSED_PARAM (pPtpUNegGtorTbl);

    PTP_FN_ENTRY ();

    /* Store the time stamping value */
    gPtpGlobalInfo.TimeStamp = PtpPortGetTimeStampMethod ();

    /* Initialize memory pools */
    if (PtpMainCreateMemPools () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainModuleStart: PtpMainCreateMemPools returned "
                  "Failure!!!\r\n"));

        PtpMainDelMemPools ();
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpTmrInit () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainModuleStart: PtpTmrInit returned " "Failure!!!\r\n"));

        PtpMainDelMemPools ();
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpRbUtlCreateRbTrees () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainModuleStart: PtpRbUtlCreateRbTrees  returned "
                  "Failure!!!\r\n"));

        PtpRbUtlDestroyRbTrees ();
        PtpTmrDeInit ();
        PtpMainDelMemPools ();
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Create the port hash Table */
    if ((gPtpGlobalInfo.pPortHashTable =
         UTL_HASH_Create_Table (PTP_HASH_TABLE_SIZE,
                                NULL, FALSE,
                                FSAP_OFFSETOF (tPtpPort, nextHashNode)))
        == NULL)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainModuleStart: Hash Table creation Failure!!!\r\n"));

        PtpRbUtlDestroyRbTrees ();
        PtpTmrDeInit ();
        PtpMainDelMemPools ();
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    u4Offset = FSAP_OFFSETOF (tPtpSADs, nextSANode);

    UTL_SLL_Init (&gPtpGlobalInfo.saList, u4Offset);

    /* Register with external modules */
    if (PtpMainRegWithExtModules () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainModuleStart: PtpMainRegWithExtModules"
                  " returned Failure!!!\r\n"));

        TMO_HASH_Delete_Table (gPtpGlobalInfo.pPortHashTable, NULL);
        gPtpGlobalInfo.pPortHashTable = NULL;
        PtpRbUtlDestroyRbTrees ();
        PtpTmrDeInit ();
        PtpMainDelMemPools ();
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    if (PtpUdpAddOrRemoveFd ((UINT1) OSIX_TRUE) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainModuleStart: SelAddFd failed !!!\r\n"));

        PtpMainUnRegWithExtModules ();
        TMO_HASH_Delete_Table (gPtpGlobalInfo.pPortHashTable, NULL);
        gPtpGlobalInfo.pPortHashTable = NULL;
        PtpRbUtlDestroyRbTrees ();
        PtpTmrDeInit ();
        PtpMainDelMemPools ();
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    /* Add filters in the hardware */
    if (IssGetHRFlagFromNvRam () == ISS_HITLESS_RESTART_DISABLE)
    {
        if (PtpNpWrInit () == OSIX_FAILURE)
        {
            PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                      INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                      "PtpMainModuleStart: PTP filter addition failed !!!\r\n"));

            PtpUdpAddOrRemoveFd ((UINT1) OSIX_FALSE);
            PtpMainUnRegWithExtModules ();
            TMO_HASH_Delete_Table (gPtpGlobalInfo.pPortHashTable, NULL);
            gPtpGlobalInfo.pPortHashTable = NULL;
            PtpRbUtlDestroyRbTrees ();
            PtpTmrDeInit ();
            PtpMainDelMemPools ();
            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }
    }

    MEMSET (&gFsPtpSizingInfo, 0, sizeof (tFsModSizingInfo));
    MEMCPY (gFsPtpSizingInfo.ModName, "PTP", STRLEN ("PTP"));
    gFsPtpSizingInfo.u4ModMemPreAllocated = 0;
    gFsPtpSizingInfo.ModSizingParams = gaFsPtpSizingParams;

    FsUtlSzCalculateModulePreAllocatedMemory (&gFsPtpSizingInfo);

#ifdef L2RED_WANTED
    /* Register with RM */
    if (PtpRedInit () == OSIX_FAILURE)
    {

        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  OS_RESOURCE_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainInit:RM Registration Failed!!!!!! \r\n"));
        return OSIX_FAILURE;
    }
#endif
    gPtpGlobalInfo.u1SysCtrlStatus = (UINT1) PTP_START;
    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : PtpMainCreateMemPools                                      */
/*                                                                           */
/* Description  : This routine creates memory pools for the PTP module.      */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : gPtpGlobalInfo (All memory pool identifiers)               */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpMainCreateMemPools (VOID)
{

    PTP_FN_ENTRY ();

    if (PtpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainCreateMemPools:  PtpSizingMemCreateMemPools(): memory pools "
                  "creation Failed!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
    /* To assign the mempools for the created mempools for the PTP module */
    PtpMainAssignMemPools ();
    if (PtpMainCreateBuddyMemPool () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "Unable to create buddy memory!!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : PtpMainRegWithExtModules                                   */
/*                                                                           */
/* Description  : This routine registers the PTP module's APIs with the      */
/*                external modules. It also provides the corresponding       */
/*                call back functions so that the same is invoked on the     */
/*                appropriate events.                                        */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpMainRegWithExtModules (VOID)
{
    PTP_FN_ENTRY ();

    /* Since PTP is initialised, it should indicate to the external modules
     * CFA & CLKIWF, so that PTP module is indicated promptly for the 
     * external events.
     * */

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, INIT_SHUT_TRC,
              "Registering Call backs with CFA module...\r\n"));

    if (PtpPortCfaRegLL () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "Unable to register call backs with CFA module.\r\n"));

        return OSIX_FAILURE;
    }

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, INIT_SHUT_TRC,
              "Registering Call backs with CLKIWF module...\r\n"));

    /* Register CLKIWF API for obtaining changes in primary parameters of
     * the primary clock*/
    PtpPortRegisterClock ();

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, INIT_SHUT_TRC,
              "Registering Call backs with NETIP module...\r\n"));

    PtpPortRegisterNetIpv4 ();
    PtpPortRegisterNetIpv6 ();

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : PtpMainDeInit                                              */
/*                                                                           */
/* Description  : This routine takes care of de-initializing the PTP module  */
/*                data structures and variables. This routine deletes the    */
/*                task queue, semaphores and de-allocates Mempools.          */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : gPtpGlobalInfo                                             */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpMainDeInit (VOID)
{
    PTP_FN_ENTRY ();

    if (gPtpGlobalInfo.QMsgPoolId != 0)
    {
        MemDeleteMemPool (gPtpGlobalInfo.QMsgPoolId);
        gPtpGlobalInfo.QMsgPoolId = 0;
    }

    if (gPtpGlobalInfo.PtpQId != 0)
    {
        OsixQueDel (gPtpGlobalInfo.PtpQId);
        gPtpGlobalInfo.PtpQId = 0;
    }

    if (gPtpGlobalInfo.SemId != 0)
    {
        OsixSemDel (gPtpGlobalInfo.SemId);
        gPtpGlobalInfo.SemId = 0;
    }

    if (gPtpGlobalInfo.i4Ipv4EvtSockId > 0)
    {
        PtpUdpDeInitParams (gPtpGlobalInfo.i4Ipv4EvtSockId);
        gPtpGlobalInfo.i4Ipv4EvtSockId = -1;
    }

    if (gPtpGlobalInfo.i4Ipv4GnlSockId > 0)
    {
        PtpUdpDeInitParams (gPtpGlobalInfo.i4Ipv4GnlSockId);
        gPtpGlobalInfo.i4Ipv4GnlSockId = -1;
    }

    if (gPtpGlobalInfo.i4Ipv6EvtSockId > 0)
    {
        PtpUdpDeInitParams (gPtpGlobalInfo.i4Ipv6EvtSockId);
        gPtpGlobalInfo.i4Ipv6EvtSockId = -1;
    }

    if (gPtpGlobalInfo.i4Ipv6GnlSockId > 0)
    {
        PtpUdpDeInitParams (gPtpGlobalInfo.i4Ipv6GnlSockId);
        gPtpGlobalInfo.i4Ipv6GnlSockId = -1;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : PtpMainModuleShutDown                                      */
/*                                                                           */
/* Description  : This routine releases  memory pools for all the required   */
/*                resources in PTP module. The resources include memory,     */
/*                timer.                                                     */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpMainModuleShutDown (VOID)
{
    tPtpQMsg           *pPtpQMsg = NULL;
    tPtpCompDBEntry    *pPtpCompDB = NULL;
    tPtpCompDBEntry    *pNextPtpCompDB = NULL;
    UINT4               u4RemainingTime = 0;

    PTP_FN_ENTRY ();

    PtpUdpAddOrRemoveFd ((UINT1) OSIX_FALSE);

    /* Remove the filters added in the hardware */
    PtpNpWrDeInit ();

    pPtpCompDB = RBTreeGetFirst (gPtpGlobalInfo.CompTree);

    while (pPtpCompDB != NULL)
    {
        if (TmrGetRemainingTime (gPtpGlobalInfo.PtpTmrListId,
                                 &(pPtpCompDB->CompDBTmrNode.TimerNode),
                                 &u4RemainingTime) == TMR_SUCCESS)

        {
            TmrStop (gPtpGlobalInfo.PtpTmrListId, &(pPtpCompDB->CompDBTmrNode));
        }
        pNextPtpCompDB = RBTreeGetNext (gPtpGlobalInfo.CompTree,
                                        pPtpCompDB, NULL);
        PtpCompDelCompDbEntry (pPtpCompDB);
        pPtpCompDB = pNextPtpCompDB;
    }

    if (PtpTmrDeInit () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainModuleShutDown: PtpTmrDeInit returned "
                  "Failure!!!\r\n"));
    }

    if (gPtpGlobalInfo.pPortHashTable != NULL)
    {
        /* Destroy the HASH Table */
        TMO_HASH_Delete_Table (gPtpGlobalInfo.pPortHashTable, NULL);
        gPtpGlobalInfo.pPortHashTable = NULL;
    }

    PtpRbUtlDestroyRbTrees ();

    while (OsixQueRecv (gPtpGlobalInfo.PtpQId, (UINT1 *) &pPtpQMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        PtpMainReleaseQueMemory (pPtpQMsg);
    }

    PtpMainDelMemPools ();

    /* Un Register with external modules */
    if (PtpMainUnRegWithExtModules () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainModuleShutDown: PtpMainUnRegWithExtModules "
                  "returned Failure!!!\r\n"));
    }

    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, INIT_SHUT_TRC,
              "Closing all the sockets, "));
    PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, INIT_SHUT_TRC,
              "IPV4/IPV6 Event and General Sockets....\r\n"));
#ifdef L2RED_WANTED
    if (PtpRedDeInit () == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS, INIT_SHUT_TRC,
                  "Not able to DeInit with RM, "));
    }
#endif
    gPtpGlobalInfo.u1SysCtrlStatus = (UINT1) PTP_SHUTDOWN;
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpMainDelMemPools                                         */
/*                                                                           */
/* Description  : This routine deletes memory pools for the PTP module.      */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : gPtpGlobalInfo                                             */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpMainDelMemPools (VOID)
{
    PTP_FN_ENTRY ();

    PtpSizingMemDeleteMemPools ();
    PtpMainDeleteBuddyMemPool ();

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpMainReleaseQueMemory                                    */
/*                                                                           */
/* DESCRIPTION  : This routine releases the memory of the message in the     */
/*                queue. The data packet are released before releasing         */
/*                the Queue msg memory.                         */
/*                                                                           */
/* Input        : pMsg - Pointer to the message to be released.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : gPtpGlobalInfo.QMsgPoolId                                  */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpMainReleaseQueMemory (tPtpQMsg * pQMsg)
{
    PTP_FN_ENTRY ();

    if (pQMsg->u4MsgType == (UINT4) PTP_PRIMARY_PARAMS_CHG)
    {
        MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                      (UINT1 *) pQMsg->uPtpMsgParam.pPtpPdu);
        pQMsg->uPtpMsgParam.pPtpPdu = NULL;

    }
    else if (pQMsg->u4MsgType == PTP_PDU_ENQ_MSG)
    {
        /* Releasing the CRU Buffer */
        if (pQMsg->uPtpMsgParam.pPtpPdu != NULL)
        {
            /* Messsage contains a valid CRU Buffer. Release the same here */
            if (CRU_BUF_Release_MsgBufChain (pQMsg->uPtpMsgParam.pPtpPdu, FALSE)
                == CRU_FAILURE)
            {
                PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                          PTP_CRITICAL_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                          "PtpMainReleaseQueMemory: "
                          "Unable to release CRU Buffer!!!!\r\n"));
            }
            pQMsg->uPtpMsgParam.pPtpPdu = NULL;
        }
    }

    /* Releasing the Q Messge */
    MemReleaseMemBlock (gPtpGlobalInfo.QMsgPoolId, (UINT1 *) pQMsg);
    pQMsg = NULL;

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpMainUnRegWithExtModules                                 */
/*                                                                           */
/* Description  : This routine de-registers the PTP module's APIs with the   */
/*                external modules.                                          */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None                                                       */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpMainUnRegWithExtModules (VOID)
{
    PTP_FN_ENTRY ();

    if (PtpPortCfaDeRegisterLL (CFA_ENET_PTP) == OSIX_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                  "PtpMainUnRegWithExtModules: PtpPortCfaDeRegisterLL returned "
                  "Failure!!!\r\n"));
        return OSIX_FAILURE;
    }

    PtpPortDeRegisterClock (CLK_MOD_PTP);

    PtpPortDeRegisterNetIpv4 (FS_PROTO_PTP);
    PtpPortDeRegisterNetIpv6 (FS_PROTO_PTP);

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpMainCreateBuddyMemPool                     */
/*                                                                           */
/* Description               : This routine creates the buddy memory pools   */
/*                             that are needed for PTP operation.            */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpMainCreateBuddyMemPool (VOID)
{
    UINT4               u4MinBlkAdjusted = 0;
    UINT4               u4MaxBlkAdjusted = 0;
    UINT2               u2HdrSize = 0;
    /* Used to calculate the memory used for buddy pool. Corresponds to header
     * size calculation in MemBuddyCreate
     * */

    PTP_FN_ENTRY ();

    UtilCalculateBuddyMemPoolAdjust (PTP_PKT_BUDDY_DEF_VAL,
                                     PTP_PKT_BUDDY_MAX_VAL,
                                     &u4MinBlkAdjusted, &u4MaxBlkAdjusted);

    gPtpGlobalInfo.i4PktBuddyId = MemBuddyCreate (u4MaxBlkAdjusted,
                                                  u4MinBlkAdjusted,
                                                  PTP_MAX_PKT_BUDDY,
                                                  BUDDY_CONT_BUF);

    if (gPtpGlobalInfo.i4PktBuddyId == BUDDY_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC |
                  PTP_CRITICAL_TRC, "MemBuddyCreate failed for Packet data\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_CALCULATE_BUDDY_MEMORY (u2HdrSize, u4MinBlkAdjusted, u4MaxBlkAdjusted,
                                PTP_MAX_PKT_BUDDY, PTP_PKT_SIZING_ID);

    /* Create Memory Buddy for PathTraceTlv data */
    UtilCalculateBuddyMemPoolAdjust (PTP_MIN_PTRC_TLV_BLOCK_SIZE,
                                     PTP_MAX_PTRC_TLV_BLOCK_SIZE,
                                     &u4MinBlkAdjusted, &u4MaxBlkAdjusted);
    gPtpGlobalInfo.i4PTrcBuddyId =
        MemBuddyCreate (u4MaxBlkAdjusted,
                        u4MinBlkAdjusted, PTP_MAX_PTRC_BLOCKS, BUDDY_CONT_BUF);

    if (gPtpGlobalInfo.i4PTrcBuddyId == BUDDY_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC |
                  OS_RESOURCE_TRC,
                  "MemBuddyCreate failed for PathTraceTlv data\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    PTP_CALCULATE_BUDDY_MEMORY (u2HdrSize, u4MinBlkAdjusted, u4MaxBlkAdjusted,
                                PTP_MAX_PTRC_BLOCKS, PTP_PTRC_SIZING_ID);

    /* Create Memory Buddy for Alternate Timescale Tlv data */
    UtilCalculateBuddyMemPoolAdjust (PTP_MIN_ALT_TIME_TLV_BLOCK_SIZE,
                                     PTP_MAX_ALT_TIME_TLV_BLOCK_SIZE,
                                     &u4MinBlkAdjusted, &u4MaxBlkAdjusted);
    gPtpGlobalInfo.i4AltTimeBuddyId =
        MemBuddyCreate (u4MaxBlkAdjusted, u4MinBlkAdjusted,
                        PTP_MAX_ALT_TIME_BLOCKS, BUDDY_CONT_BUF);

    if (gPtpGlobalInfo.i4AltTimeBuddyId == BUDDY_FAILURE)
    {
        PTP_TRC (((UINT4) PTP_INV_CONTEXT_ID, PTP_MAX_DOMAINS,
                  INIT_SHUT_TRC | ALL_FAILURE_TRC | PTP_CRITICAL_TRC |
                  OS_RESOURCE_TRC,
                  "MemBuddyCreate failed for Alternate  Timescale Tlv data\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_CALCULATE_BUDDY_MEMORY (u2HdrSize, u4MinBlkAdjusted, u4MaxBlkAdjusted,
                                PTP_MAX_ALT_TIME_BLOCKS,
                                PTP_ALT_TIME_SIZING_ID);

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpMainDeleteBuddyMemPool                     */
/*                                                                           */
/* Description               : This routine deletes the buddy memory pools   */
/*                             that are needed for PTP operation.            */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpMainDeleteBuddyMemPool (VOID)
{
    PTP_FN_ENTRY ();

    if (gPtpGlobalInfo.i4PktBuddyId != BUDDY_FAILURE)
    {
        MemBuddyDestroy ((UINT1) gPtpGlobalInfo.i4PktBuddyId);
        gPtpGlobalInfo.i4PktBuddyId = BUDDY_FAILURE;
    }

    if (gPtpGlobalInfo.i4PTrcBuddyId != BUDDY_FAILURE)
    {
        MemBuddyDestroy ((UINT1) gPtpGlobalInfo.i4PTrcBuddyId);
        gPtpGlobalInfo.i4PktBuddyId = BUDDY_FAILURE;
    }

    if (gPtpGlobalInfo.i4AltTimeBuddyId != BUDDY_FAILURE)
    {
        MemBuddyDestroy ((UINT1) gPtpGlobalInfo.i4AltTimeBuddyId);
        gPtpGlobalInfo.i4PktBuddyId = BUDDY_FAILURE;
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpMainAssignMemPools                         */
/*                                                                           */
/* Description               : This routine to Assign the  memory pools      */
/*                             that are created for PTP operation.           */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None.                                         */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpMainAssignMemPools (VOID)
{

    gPtpGlobalInfo.QMsgPoolId = PTPMemPoolIds[MAX_PTP_Q_MESG_SIZING_ID];
    gPtpGlobalInfo.ContextPoolId = PTPMemPoolIds[MAX_PTP_CONTEXTS_SIZING_ID];
    gPtpGlobalInfo.DomainPoolId = PTPMemPoolIds[MAX_PTP_DOMAINS_SIZING_ID];
    gPtpGlobalInfo.PortPoolId = PTPMemPoolIds[MAX_PTP_PORTS_SIZING_ID];
    gPtpGlobalInfo.ForeignMasterPoolId =
        PTPMemPoolIds[MAX_PTP_FOREIGN_MASTERS_SIZING_ID];
    gPtpGlobalInfo.GMClusterPoolId =
        PTPMemPoolIds[MAX_PTP_GRAND_MASTER_TABLE_SIZING_ID];
    gPtpGlobalInfo.AccMasterPoolId =
        PTPMemPoolIds[MAX_PTP_ACCEPTABLE_MASTER_TABLE_SIZING_ID];
    gPtpGlobalInfo.UnicMasterPoolId =
        PTPMemPoolIds[MAX_PTP_ACCEP_UCAST_MASTER_TABLE_SIZING_ID];
    gPtpGlobalInfo.SAPoolId = PTPMemPoolIds[MAX_PTP_SECURITY_ASSOC_SIZING_ID];
    gPtpGlobalInfo.SecKeyPoolId =
        PTPMemPoolIds[MAX_PTP_SECURITY_KEY_LIST_SIZING_ID];
    gPtpGlobalInfo.CompPoolId =
        PTPMemPoolIds[MAX_PTP_VERSION_COMP_DB_ENTRIES_SIZING_ID];
    gPtpGlobalInfo.AltTimSclPoolId =
        PTPMemPoolIds[MAX_PTP_ALT_TIME_SCALE_ENTRIES_SIZING_ID];
    gPtpGlobalInfo.UNegGranteeTblPoolId =
        PTPMemPoolIds[MAX_PTP_UCAST_NEG_GRANTEE_TABLE_SIZING_ID];
    gPtpGlobalInfo.UNegGrantorTblPoolId =
        PTPMemPoolIds[MAX_PTP_UCAST_NEG_GRANTOR_TABLE_SIZING_ID];
    gPtpGlobalInfo.UtilityPoolId =
        PTPMemPoolIds[MAX_PTP_UTILITY_ENTRY_SIZING_ID];
    return;

}

#endif /*_PTPMAIN_C_ */
