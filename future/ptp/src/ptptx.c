/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptptx.c,v 1.21 2014/07/03 10:21:33 siva Exp $
 *
 * Description: This file contains PTP module transmission related
 *              routines.
 *********************************************************************/
#ifndef _PTPTX_C_
#define _PTPTX_C_

#include "ptpincs.h"

PRIVATE INT4        PtpTxFillCompTxInfo (UINT1 *pu1v2Pdu, tPtpPort * pPtpPort,
                                         UINT4 u4PktLen,
                                         tPtpCompTxInfo * pPtpCompTxInfo);

PRIVATE VOID        PtpTxFreeCompTxInfo (tPtpCompTxInfo * pPtpCompTxInfo);

PRIVATE VOID        PtpTxUpdtCorrnFldForNonTrnsClk (tPtpTxParams * pPtpTxParams,
                                                    UINT4 u4MediaHdrLen,
                                                    UINT1 u1MsgType);

PRIVATE VOID        PtpTxUpdtCorrnFieldForTransClk (UINT1 *pu1PtpPdu,
                                                    UINT4 u4MediaHdrLen,
                                                    UINT1 u1MsgType,
                                                    tFsClkTimeVal *
                                                    pEventTimeStamp);

/*****************************************************************************/
/* Function                  : PtpTxFillCompTxInfo                           */
/*                                                                           */
/* Description               : This routine used to allocate Buddy memory    */
/*                             for the V1 Packet which tx to the version 1   */
/*                             configured Outgoing port and fill the         */
/*                             v2 Packet , v2 Packet length, context,        */
/*                             DomainId , PTP Port                           */
/*                                                                           */
/* Input                     : pPv2tpPdu - Pointer to the linear buffer.     */
/*                             pPtpPort - Pointer to the port structure.     */
/*                             u4PktLen - Packet Length.                     */
/*                             pPtpCompTxInfo - PTP Compatibility Tx info    */
/*                             structure.                                    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpTxFillCompTxInfo (UINT1 *pu1v2Pdu, tPtpPort * pPtpPort, UINT4 u4PktLen,
                     tPtpCompTxInfo * pPtpCompTxInfo)
{
    PTP_FN_ENTRY ();
    /* PTP version 2 Pdu Message          */
    pPtpCompTxInfo->Tx.pu1Ptpv2Pdu = pu1v2Pdu;
    /* PTP version 2 Pdu Length           */
    pPtpCompTxInfo->Tx.u4Ptpv2PduLen = u4PktLen;
    pPtpCompTxInfo->Tx.pPtpPort = pPtpPort;

    /* Allocate the Memory for the Version1 buffer from the Packet Buddy */
    /* PTP version 1 Pdu Message  */
    pPtpCompTxInfo->Tx.pu1Ptpv1Pdu =
        MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                       PTP_V2_TO_V1_MAX_MSG_LEN);

    if (pPtpCompTxInfo->Tx.pu1Ptpv1Pdu == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "PtpTxFillCompTxInfo : Buddy Memory Allocation for "
                  "v1 Announce Msg PDU  Failed!!\r\n"));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function                  : PtpTxFreeCompTxInfo                           */
/*                                                                           */
/* Description               : This routine used to Free the Buddy memory    */
/*                             for the V2 Packet which tx to the version 1   */
/*                             configured Outgoing port                      */
/*                                                                           */
/* Input                     : pPtpCompTxInfo - PTP Compatibility Tx info    */
/*                             structure.                                    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTxFreeCompTxInfo (tPtpCompTxInfo * pPtpCompTxInfo)
{
    PTP_FN_ENTRY ();

    /* Free the Memory for the Version1 buffer allocated from 
     * the Packet Buddy */
    if (pPtpCompTxInfo->Tx.pu1Ptpv2Pdu != NULL)
    {
        if ((MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                           pPtpCompTxInfo->Tx.pu1Ptpv2Pdu)) == BUDDY_FAILURE)
        {
            PTP_TRC ((pPtpCompTxInfo->Tx.pPtpPort->u4ContextId,
                      pPtpCompTxInfo->Tx.pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpTxFreeCompTxInfo : MemBuddyFree for V2 PDU "
                      " Failed!!\r\n"));
            PTP_FN_EXIT ();
            return;
        }
        pPtpCompTxInfo->Tx.pu1Ptpv2Pdu = NULL;
    }
}

/*****************************************************************************/
/* Function                  : PtpTxTransmitPtpMessage                       */
/*                                                                           */
/* Description               : This routine transmits the given PTP PDU over */
/*                             the interface. If the interface is UDP, it    */
/*                             transmits over the socket as UDP data. In     */
/*                             case the interface is other type, it transmits*/
/*                             over physical port. If it is not transmitted  */
/*                             over socket, this routine converts the linear */
/*                             buffer into CRU buffer and transmits the same.*/
/*                                                                           */
/* Input                     : pu1PtpPdu  - Pointer to the linear buffer.    */
/*                             pPtpPort - Pointer to the port structure.     */
/*                             u4PktLen - Packet Length.                     */
/*                             u1MsgType- Message Type.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpTxTransmitPtpMessage (tPtpTxParams * pPtpTxParams)
{
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
    tPtpCompTxInfo      PtpCompTxInfo;
    tClkSysTimeInfo     PtpSysTimeInfo;
    tPtpHwLog           PtpHwLog;
    UINT1              *pu1TmpPdu = NULL;
    UINT1              *pu1PtpPdu = pPtpTxParams->pu1PtpPdu;
    tPtpPort           *pPtpPort = pPtpTxParams->pPtpPort;
    UINT4               u4PktLen = pPtpTxParams->u4MsgLen;
    UINT4               u4MediaHdrLen = 0;
    UINT2               u2Protocol = CFA_DATA;
    UINT2               u2TwoByteVal = (UINT2) u4PktLen;
    UINT1               u1MsgType = pPtpTxParams->u1MsgType;
    FS_UINT8            u8Delay;
    FS_UINT8           *pu8Delay = &u8Delay;
#ifdef L2RED_WANTED
    tRmMsg             *pMsg = NULL;
    UINT2               u2DataLen = 0;
#endif

    PTP_FN_ENTRY ();
    u8Delay.u4Hi = u8Delay.u4Lo = 0;

    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    MEMSET (&PtpCompTxInfo, 0, sizeof (tPtpCompTxInfo));
    MEMSET (&PtpHwLog, 0, sizeof (tPtpHwLog));

    u4MediaHdrLen = PTP_GET_MEDIA_HDR_LEN (pPtpPort);

    /* Update the PTP Msg Header Length */
    u2TwoByteVal -= u4MediaHdrLen;
    pu1TmpPdu = pPtpTxParams->pu1PtpPdu + u4MediaHdrLen + PTP_MSG_LEN_OFFSET;
    PTP_LBUF_PUT_2_BYTES (pu1TmpPdu, u2TwoByteVal);

    if ((u1MsgType == PTP_PEER_DELAY_RESP_MSG) ||
        (u1MsgType == PTP_PEER_DELAY_REQ_MSG) ||
        (u1MsgType == PTP_PDELAY_RESP_FOLLOWUP_MSG))
    {
        u2Protocol = CFA_PROT_BPDU;
    }

    if (pPtpPort->pPtpDomain->ClkMode != PTP_FORWARD_MODE)
    {
        if (pPtpPort->pPtpDomain->ClkMode != PTP_TRANSPARENT_CLOCK_MODE)
        {
            PtpTxUpdtCorrnFldForNonTrnsClk (pPtpTxParams, u4MediaHdrLen,
                                            u1MsgType);
        }
        else
        {
            PtpTxUpdtCorrnFieldForTransClk (pu1PtpPdu, u4MediaHdrLen,
                                            u1MsgType,
                                            pPtpTxParams->pEventTimeStamp);
        }
    }

    /* Check the Outgoing port version if the it is V1 and Clock mode is 
     * Boundary Clock Transform the V2 Packet to V1 Packet and Process further
     */
    if ((pPtpPort->PortDs.u4VersionNumber == PTP_MESSAGE_VERSION_ONE) &&
        (pPtpPort->pPtpDomain->ClkMode == PTP_BOUNDARY_CLOCK_MODE))
    {
        /* Fill the PtpCompTxInfo */
        MEMSET (&PtpCompTxInfo, 0, sizeof (tPtpCompTxInfo));

        if ((PtpTxFillCompTxInfo
             (pu1PtpPdu, pPtpPort, u4PktLen, &PtpCompTxInfo)) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "PtpTxTransmitPtpMessage: "
                      "PtpTxFillCompTxInfo returned Failure!!\r\n"));
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1PtpPdu);
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }

        if ((PtpComTxV2ToV1PtpPdu (&PtpCompTxInfo)) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                      "PtpTxTransmitPtpMessage: "
                      "PtpComTxV2ToV1PtpPdu returned Failure!!\r\n"));
            PtpTxFreeCompTxInfo (&PtpCompTxInfo);
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                          PtpCompTxInfo.Tx.pu1Ptpv1Pdu);
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }
        /* Update the PtpPdu and length with v1 Packet values */
        pu1PtpPdu = PtpCompTxInfo.Tx.pu1Ptpv1Pdu;
        u4PktLen = PtpCompTxInfo.Tx.u4Ptpv1PduLen;
        PtpTxFreeCompTxInfo (&PtpCompTxInfo);
    }                            /* End of the PTP V2 to V1 Process */

    if (gDelaySystem.u4G8261ModuleStatus == PTP_ENABLED)
    {
        PtpApiG8261ComputeDelay (pu8Delay);

        if (!
            ((gDelayOutput.u8Delay.u4Hi == 0)
             && (gDelayOutput.u8Delay.u4Lo == 0)))
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Introduce delay for %u nanosecs \r\n ",
                      gDelayOutput.u8Delay.u4Lo));
            OsixDelay (gDelayOutput.u8Delay.u4Lo, OSIX_NANO_SECONDS);
        }
    }

    if (u1MsgType == PTP_PEER_DELAY_REQ_MSG)
    {
        /* Save the time stamp for Peer Delay calculations */
        PtpSysTimeInfo.u4ContextId = pPtpPort->u4ContextId;
        PtpSysTimeInfo.u1DomainId = pPtpPort->u1DomainId;
        PtpPortClkIwfGetClock (&PtpSysTimeInfo);
        MEMCPY (&(pPtpPort->PeerDelayOriginTime),
                &(PtpSysTimeInfo.FsClkTimeVal), sizeof (tFsClkTimeVal));
    }
    else if (u1MsgType == PTP_SYNC_MSG)
    {
        /* Save the Sync Origin time stamp for  Sync follow up msg */
        PtpSysTimeInfo.u4ContextId = pPtpPort->u4ContextId;
        PtpSysTimeInfo.u1DomainId = pPtpPort->u1DomainId;
        PtpPortClkIwfGetClock (&PtpSysTimeInfo);
    }
    switch (pPtpPort->PtpDeviceType)
    {
        case PTP_IFACE_UDP_IPV4:

            /* Transmit over the UDP interface */
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Transmitting packet over UDP "
                      "IPv4 Interface%u\r\n", pPtpPort->u4IfIndex));
#ifdef L2RED_WANTED
            /*Formaing RM packet and sending to StandBy node */
            if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
            {
                PtpRedFormIpvXPtpPktForActive (pu1PtpPdu, pPtpPort,
                                               u4PktLen, u1MsgType,
                                               (UINT1) PTP_RED_IPv4_PTP_PKT,
                                               &pMsg);
                if (pMsg != NULL)
                {
                    u2DataLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pMsg);
                    PtpRedProcessAndRelayPkt (pMsg, u2DataLen);
                }
                else
                {
                    PTP_TRC ((0, PTP_MAX_DOMAINS,
                              ALL_FAILURE_TRC,
                              "PtpTxTransmitPtpMessage: Unable to"
                              " duplicate the packet for RM.\r\n"));
                }

            }
#endif
            PtpUdpTransmitPtpMsg (pu1PtpPdu, pPtpPort, u4PktLen, u1MsgType);
            break;

        case PTP_IFACE_UDP_IPV6:

            /* Transmit over the UDP/IPv6 interface */
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Transmitting packet over UDP "
                      "IPv6 Interface%u\r\n", pPtpPort->u4IfIndex));
#ifdef L2RED_WANTED
            /*Formaing RM packet and sending to StandBy node */
            if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
            {
                PtpRedFormIpvXPtpPktForActive (pu1PtpPdu, pPtpPort,
                                               u4PktLen, u1MsgType,
                                               (UINT1) PTP_RED_IPv6_PTP_PKT,
                                               &pMsg);
                if (pMsg != NULL)
                {
                    u2DataLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pMsg);
                    PtpRedProcessAndRelayPkt (pMsg, u2DataLen);
                }
                else
                {
                    PTP_TRC ((0, PTP_MAX_DOMAINS,
                              ALL_FAILURE_TRC,
                              "PtpTxTransmitPtpMessage: Unable to"
                              " duplicate the packet for RM.\r\n"));
                }

            }
#endif
            PtpUdpV6TransmitPtpMsg (pu1PtpPdu, pPtpPort, u4PktLen, u1MsgType);
            break;

        case PTP_IFACE_IEEE_802_3:

            /* This always represents either CFA Physical interface
             * or Vlan interface. Packet should be transmitted as CRU 
             * buffer for Layer 2 VLANs and physical interface 
             * */
            pCruBuf = CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0);
            if (pCruBuf == NULL)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          BUFFER_TRC | PTP_CRITICAL_TRC | ALL_FAILURE_TRC,
                          "Allocation of CRU Buffer " "FAILED!!\r\n"));
                MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1PtpPdu);
                PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_BUF_FAIL);

                PTP_FN_EXIT ();
                return OSIX_FAILURE;
            }

            if (CRU_BUF_Copy_OverBufChain (pCruBuf, pu1PtpPdu, 0, u4PktLen)
                == CRU_FAILURE)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          ALL_FAILURE_TRC,
                          "Copying over CRU Buffer failed!!\r\n"));

                MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1PtpPdu);
                CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                PTP_FN_EXIT ();
                return OSIX_FAILURE;
            }
            /* Physical port */
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Transmitting packet over interface %u\r\n",
                      pPtpPort->u4IfIndex));

#ifdef L2RED_WANTED
            /*Formaing RM packet and sending to StandBy node */
            if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
            {
                PtpRedFormL2PtpPktForActive (pCruBuf, pPtpPort->u4IfIndex,
                                             u4PktLen, u2Protocol,
                                             (UINT1) CFA_ENCAP_NONE, &pMsg);

                if (pMsg != NULL)
                {
                    u2DataLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pMsg);
                    PtpRedProcessAndRelayPkt (pMsg, u2DataLen);
                }
                else
                {
                    PTP_TRC ((0, PTP_MAX_DOMAINS,
                              ALL_FAILURE_TRC,
                              "PtpTxTransmitPtpMessage: Unable to "
                              "duplicate the packet for RM.\r\n"));
                }

            }
#endif

            if (PtpPortHandleOutgoingPktOnPort (pCruBuf,
                                                pPtpPort->u4IfIndex,
                                                u4PktLen, u2Protocol,
                                                (UINT1) CFA_ENCAP_NONE)
                != OSIX_SUCCESS)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          DUMP_TRC | ALL_FAILURE_TRC,
                          "PtpPortHandleOutgoingPktOnPort "
                          "returned Failure!!\r\n"));
                PTP_DUMP_TRC (pPtpPort->u4ContextId, pCruBuf, u4PktLen);
                MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1PtpPdu);
                PTP_FN_EXIT ();
                return OSIX_FAILURE;
            }
            break;

        case PTP_IFACE_VLAN:
            /* Running over VLAN. 
             * The VLAN API will find the set of interfaces associated 
             * with the VLAN and then transmit the packet over these 
             * interfaces.*/
            pCruBuf = CRU_BUF_Allocate_MsgBufChain (u4PktLen, 0);
            if (pCruBuf == NULL)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          OS_RESOURCE_TRC | PTP_CRITICAL_TRC | BUFFER_TRC |
                          ALL_FAILURE_TRC,
                          "Allocation of CRU Buffer FAILED!!\r\n"));
                MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1PtpPdu);
                PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_BUF_FAIL);

                PTP_FN_EXIT ();
                return OSIX_FAILURE;
            }

            if (CRU_BUF_Copy_OverBufChain (pCruBuf, pu1PtpPdu, 0, u4PktLen)
                == CRU_FAILURE)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          ALL_FAILURE_TRC,
                          "Copying over CRU Buffer failed!!\r\n"));

                MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1PtpPdu);
                CRU_BUF_Release_MsgBufChain (pCruBuf, FALSE);
                PTP_FN_EXIT ();
                return OSIX_FAILURE;
            }

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC, "Transmitting packet over vlan %u\r\n",
                      pPtpPort->u4IfIndex));

#ifdef L2RED_WANTED
            /*Forming RM packet and sending to Active Node */
            if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
            {
                PtpRedFormVlanPtpPktForActive (pCruBuf, pPtpPort->u4ContextId,
                                               (UINT2) pPtpPort->u4IfIndex,
                                               u4PktLen, &pMsg);

                if (pMsg != NULL)
                {
                    u2DataLen = (UINT2) CRU_BUF_Get_ChainValidByteCount (pMsg);
                    PtpRedProcessAndRelayPkt (pMsg, u2DataLen);
                }
                else
                {
                    PTP_TRC ((0, PTP_MAX_DOMAINS,
                              ALL_FAILURE_TRC,
                              "PtpTxTransmitPtpMessage: Unable to duplicate"
                              " the packet for RM.\r\n"));
                }
            }
#endif

            if (PtpPortTransmitPktOverVlan (pCruBuf, pPtpPort->u4ContextId,
                                            (UINT2) pPtpPort->u4IfIndex,
                                            u4PktLen) != OSIX_SUCCESS)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          DUMP_TRC | ALL_FAILURE_TRC, "PtpPortTransmitOverVlan"
                          "returned Failure!!\r\n"));
                PTP_DUMP_TRC (pPtpPort->u4ContextId, pCruBuf, u4PktLen);
                MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1PtpPdu);
                PTP_FN_EXIT ();
                return OSIX_FAILURE;
            }
            break;

        default:
            break;
    }

    switch (u1MsgType)
    {
        case PTP_SYNC_MSG:
            pPtpPort->PtpPortStats.u4TransSyncMsgCnt++;
            break;
        case PTP_DELAY_REQ_MSG:
            pPtpPort->PtpPortStats.u4TransDelayReqMsgCnt++;
            break;
        case PTP_PEER_DELAY_REQ_MSG:
            pPtpPort->PtpPortStats.u4TransPeerDelayReqMsgCnt++;
            break;
        case PTP_PEER_DELAY_RESP_MSG:
            pPtpPort->PtpPortStats.u4TransPeerDelayRespMsgCnt++;
            break;
        case PTP_FOLLOW_UP_MSG:
            pPtpPort->PtpPortStats.u4TransFollowUpMsgCnt++;
            break;
        case PTP_DELAY_RESP_MSG:
            pPtpPort->PtpPortStats.u4TransDelayRespMsgCnt++;
            break;
        case PTP_PDELAY_RESP_FOLLOWUP_MSG:
            pPtpPort->PtpPortStats.u4TransPeerDelayRespFollowUpMsgCnt++;
            break;
        case PTP_ANNC_MSG:
            pPtpPort->PtpPortStats.u4TransAnnounceMsgCnt++;
            break;
        default:
            break;
    }

    if ((u1MsgType == PTP_PEER_DELAY_RESP_MSG) &&
        ((pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag ==
          PTP_ENABLED) ||
         ((pPtpPort->pPtpDomain->ClockDs.TransClkDs.bTwoStepFlag ==
           PTP_ENABLED)
          && (pPtpPort->pPtpDomain->ClockDs.TransClkDs.TransClkType !=
              PTP_E2E_TRANSPARENT_CLOCK))))
    {
        /* For Peer delay response messages, do not free the buddy memory
         * The same packet will be used for follow  up cases
         * */
        /* Precautionary check, if trigger is not sent for a particular 
         * delay response in a port and if another delay response is triggered
         * free the previously allocated memory */
        if (pPtpPort->pu1PeerDelayResp != NULL)
        {
            MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                          pPtpPort->pu1PeerDelayResp);
            pPtpPort->pu1PeerDelayResp = NULL;
        }

        pPtpPort->pu1PeerDelayResp = pu1PtpPdu;
        pPtpPort->u4PeerDelayRespLen = u4PktLen;
    }
    else
    {
        /* Now free the buddy memory */
        MemBuddyFree ((UINT1) gPtpGlobalInfo.i4PktBuddyId, pu1PtpPdu);
    }
    if (gPtpGlobalInfo.TimeStamp == PTP_SOFTWARE_TIMESTAMPING)
    {
        /* if clock is configured as two-step-clock, follow up messsage should be
         * sent for peer delay resposne and sync message.
         * */
        if (((pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag ==
              PTP_ENABLED) && ((u1MsgType ==
                                PTP_PEER_DELAY_RESP_MSG)
                               || (u1MsgType == PTP_SYNC_MSG)))
            ||
            ((pPtpPort->pPtpDomain->ClockDs.TransClkDs.bTwoStepFlag ==
              PTP_ENABLED) &&
             (u1MsgType == PTP_PEER_DELAY_RESP_MSG) &&
             (pPtpPort->pPtpDomain->ClockDs.TransClkDs.TransClkType
              != PTP_E2E_TRANSPARENT_CLOCK)))
        {
            if (u1MsgType == PTP_PEER_DELAY_RESP_MSG)
            {
                PtpSysTimeInfo.FsClkTimeVal.u8Sec.u4Hi = 0;
                PtpSysTimeInfo.FsClkTimeVal.u8Sec.u4Lo = 0;
                PtpSysTimeInfo.FsClkTimeVal.u4Nsec = 0;
            }

            /* The clock is a two step clock. Need to feed time stamp for sending
             * the Follow up message.
             * */
            if (PtpTxTriggerFollowUp (pPtpPort, &PtpSysTimeInfo, u1MsgType)
                != OSIX_SUCCESS)
            {
                PTP_FN_EXIT ();
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                          "PtpTxTransmitPtpMessage:Failed to Transmit"
                          "Follow Up Message..\r\n"));
                return OSIX_FAILURE;
            }
        }
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpTxTriggerFollowUp                          */
/*                                                                           */
/* Description               : This routine update the exact origin timestamp*/
/*                             provided by PHY and triggers the follow up    */
/*                             in case of two step clocks.                   */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure.     */
/*                             PtpSysTimeInfo - Time info                    */
/*                             u1MsgType- Message Type.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpTxTriggerFollowUp (tPtpPort * pPtpPort, tClkSysTimeInfo * pPtpSysTimeInfo,
                      UINT1 u1MsgType)
{
    INT4                i4RetVal = OSIX_FAILURE;
    BOOL1               bTwoStepFlag = PTP_DISABLED;

    PTP_FN_ENTRY ();

    /* Follow up is applicable only for Sync and Peer Delay Response messages */

    /* This is the driver level or PHY level for software time stamping
     * Now time stamp if Software time stamping is enabled and feed the time
     * to the corresponding message handler. It should be able to form the
     * follow up message and send it out
     * */

    /* Get the clock mode for the incoming ptp Port */
    if ((pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag ==
         PTP_ENABLED) || (pPtpPort->pPtpDomain->ClockDs.TransClkDs.
                          bTwoStepFlag == PTP_ENABLED))
    {
        bTwoStepFlag = PTP_ENABLED;
    }

    switch (u1MsgType)
    {
        case PTP_SYNC_MSG:

            /* Updating the Exact Origin time provided by the PHY */
            MEMCPY (&(pPtpPort->IngressSyncTimeStamp),
                    &(pPtpSysTimeInfo->FsClkTimeVal), sizeof (tFsClkTimeVal));

            if (bTwoStepFlag == PTP_ENABLED)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          CONTROL_PLANE_TRC,
                          "Clock is a two step clock...\r\n"));
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          CONTROL_PLANE_TRC,
                          "Transmitting Sync follow up....\r\n"));
                i4RetVal = PtpSyncTransmitFollowUp (pPtpPort, pPtpSysTimeInfo);
            }
            break;

        case PTP_DELAY_REQ_MSG:

            /* Updating the Exact Origin time provided by the PHY */
            MEMCPY (&(pPtpPort->DelayReqOriginTime),
                    &(pPtpSysTimeInfo->FsClkTimeVal), sizeof (tFsClkTimeVal));

            break;

        case PTP_PEER_DELAY_REQ_MSG:

            /* Updating the Exact Origin time provided by the PHY */
            MEMCPY (&(pPtpPort->PeerDelayOriginTime),
                    &(pPtpSysTimeInfo->FsClkTimeVal), sizeof (tFsClkTimeVal));

            break;

        case PTP_PEER_DELAY_RESP_MSG:

            /* Peer delay response origin is required only to update the correction
             * field, hence calculating the (t3-t2) value. this is applicable only
             * for 2 step clock as peer delay response is already transmitted */

            if (bTwoStepFlag == PTP_ENABLED)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          CONTROL_PLANE_TRC,
                          "Clock is a two step clock...\r\n"));
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          CONTROL_PLANE_TRC,
                          "Transmitting Peer delay response follow up....\r\n"));
                i4RetVal = PtpPdresTransmitFollowUp (pPtpPort, pPtpSysTimeInfo);
            }
            break;

        default:
            /* Exception handling. Can never come here. The message type has
             * already been validated. Only the above three messages can 
             * come.
             * */
            i4RetVal = OSIX_SUCCESS;
            break;
    }

    PTP_FN_EXIT ();
    return i4RetVal;
}

/*****************************************************************************/
/* Function                  : PtpTxUpdtCorrnFieldForTransClk                */
/*                                                                           */
/* Description               : This routine is used to update the Correction */
/*                             field of the received PDU in case of transpar */
/*                             clocks. For transparent clocks, the PDU should*/
/*                             be updated for residence time calculations.   */
/*                             The ingress time stamp would have been subtrac*/
/*                             subtracted from the correction field by PTP   */
/*                             TransParent Sub Module. Here just put the     */
/*                             time stamp and add the same.                  */
/*                                                                           */
/* Input                     : pu1PtpPdu- Pointer to the PDU.                */
/*                             u1MsgType- Message Type.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTxUpdtCorrnFieldForTransClk (UINT1 *pu1PtpPdu, UINT4 u4MediaHdrLen,
                                UINT1 u1MsgType,
                                tFsClkTimeVal * pEventTimeStamp)
{
    tClkSysTimeInfo     PtpSysTimeInfo;
    tClkSysTimeInfo     CorrectionTime;
    FS_UINT8            u8CorrnField;
    FS_UINT8            u8Val;
    FS_UINT8            u8NanoSecVal;
    FS_UINT8            u8NanoSecConversionFactor;
    UINT1              *pu1TmpPdu = NULL;
    UINT4               u4Val = 0;
    INT1                i1Sign = 0;

    PTP_FN_ENTRY ();

    if (u1MsgType == PTP_ANNC_MSG)
    {
        PTP_FN_EXIT ();
        return;
    }
    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    FSAP_U8_CLR (&u8CorrnField);
    FSAP_U8_CLR (&u8Val);
    FSAP_U8_CLR (&u8NanoSecVal);
    FSAP_U8_CLR (&u8NanoSecConversionFactor);

    pu1TmpPdu = pu1PtpPdu + PTP_MSG_CORRECTION_FIELD_OFFSET + u4MediaHdrLen;

    PTP_LBUF_GET_4_BYTES (pu1TmpPdu, u4Val);

    u8Val.u4Hi = u4Val;
    PTP_LBUF_GET_4_BYTES (pu1TmpPdu, u4Val);
    u8Val.u4Lo = u4Val;

    PtpPortClkIwfGetClock (&PtpSysTimeInfo);

    if (pEventTimeStamp != NULL)
    {
        /* Message handler has generated a time stamp. Hence egress 
         * time stamp needs to be generated and correction field
         * needs to be updated.
         * */
        PtpClkSubtractTimeStamps (&(PtpSysTimeInfo.FsClkTimeVal),
                                  pEventTimeStamp,
                                  &(CorrectionTime.FsClkTimeVal), &i1Sign);
        u8CorrnField.u4Hi = CorrectionTime.FsClkTimeVal.u8Sec.u4Hi;
        u8CorrnField.u4Lo = CorrectionTime.FsClkTimeVal.u8Sec.u4Lo;
        UINT8_LO (&(u8NanoSecConversionFactor)) =
            PTP_NANO_SEC_TO_SEC_CONV_FACTOR;
        UINT8_LO (&(u8NanoSecVal)) = CorrectionTime.FsClkTimeVal.u4Nsec;
        FSAP_U8_MUL (&u8CorrnField, &u8CorrnField, &u8NanoSecConversionFactor);
        FSAP_U8_ADD (&u8NanoSecVal, &u8NanoSecVal, &u8CorrnField);

        FSAP_U8_CLR (&u8CorrnField);
        PtpClkCalcScalNanoSecs (u8NanoSecVal, &u8CorrnField);
    }
    if (i1Sign == PTP_NEGATIVE)
    {
        PtpClkPerf8ByteSubtraction (&u8CorrnField, &u8Val, &u8CorrnField,
                                    &i1Sign);
    }
    else
    {
        FSAP_U8_ADD (&u8CorrnField, &u8CorrnField, &u8Val);
    }
    pu1TmpPdu = pu1PtpPdu + PTP_MSG_CORRECTION_FIELD_OFFSET + u4MediaHdrLen;

    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, u8CorrnField.u4Hi);
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, u8CorrnField.u4Lo);
}

/*****************************************************************************/
/* Function                  : PtpTxUpdtCorrnFldForNonTrnsClk                */
/*                                                                           */
/* Description               : This routine is used to update the Correction */
/*                             field of the received PDU in case of non      */
/*                             transparent clocks.                           */
/*                                                                           */
/* Input                     : pPtpTxParams  - Pointer to the Pkt Params.   .*/
/*                             u4MediaHdrLen - Media Header Length.          */
/*                             u1MsgType     - Message Type.                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpTxUpdtCorrnFldForNonTrnsClk (tPtpTxParams * pPtpTxParams,
                                UINT4 u4MediaHdrLen, UINT1 u1MsgType)
{
    tPtpPort           *pPtpPort = pPtpTxParams->pPtpPort;
    UINT1              *pu1PtpPdu = pPtpTxParams->pu1PtpPdu;
    UINT1              *pu1TmpPdu = NULL;
    tClkSysTimeInfo     PtpSysTimeInfo;
    tClkSysTimeInfo     CorrectionTime;
    FS_UINT8            u8CorrnField;
    FS_UINT8            u8NanoSecVal;
    FS_UINT8            u8NanoSecConversionFactor;
    UINT4               u4Val = 0;
    UINT2               u2Val = 0;
    INT1                i1Sign = 0;

    PTP_FN_ENTRY ();

    MEMSET (&PtpSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    MEMSET (&CorrectionTime, 0, sizeof (tClkSysTimeInfo));
    MEMSET (&u8CorrnField, 0, sizeof (FS_UINT8));
    FSAP_U8_CLR (&u8CorrnField);
    FSAP_U8_CLR (&u8NanoSecVal);
    FSAP_U8_CLR (&u8NanoSecConversionFactor);

    pu1TmpPdu = pu1PtpPdu + PTP_SYNC_TIME_STAMP_OFFSET + u4MediaHdrLen;

    switch (u1MsgType)
    {
        case PTP_SYNC_MSG:
        case PTP_DELAY_REQ_MSG:
            /* Intentional Fall through */
            /* Offset value is same for all these messages.
             * Refer Section 13.6 - 13.11 of IEEE Std 1588 2008
             * */
            PtpSysTimeInfo.u4ContextId =
                pPtpPort->pPtpDomain->pPtpCxt->u4ContextId;
            PtpSysTimeInfo.u1DomainId = pPtpPort->pPtpDomain->u1DomainId;

            PtpPortClkIwfGetClock (&PtpSysTimeInfo);

            if (u1MsgType == PTP_DELAY_REQ_MSG)
            {
                /* Copy the Delay Request Egress Time Stamp */
                MEMCPY (&(pPtpPort->DelayReqOriginTime),
                        &(PtpSysTimeInfo.FsClkTimeVal), sizeof (tFsClkTimeVal));
            }

        case PTP_PEER_DELAY_REQ_MSG:

            /* Intentional Fall through */
            /* For Peer Delay mechanism values the time stamp should be
             * set as ZERO (Peer Delay Request Message). Refer Section
             * 11.4.3 of IEEE Std 1588 2008 */

            u2Val = (UINT2) UINT8_HI (&(PtpSysTimeInfo.FsClkTimeVal.u8Sec));
            PTP_LBUF_PUT_2_BYTES (pu1TmpPdu, u2Val);

            u4Val = UINT8_LO (&(PtpSysTimeInfo.FsClkTimeVal.u8Sec));
            PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, u4Val);
            PTP_LBUF_PUT_4_BYTES (pu1TmpPdu,
                                  (PtpSysTimeInfo.FsClkTimeVal.u4Nsec));

            /* Save the time stamp for Peer Delay calculations */
            PtpSysTimeInfo.u4ContextId = pPtpPort->u4ContextId;
            PtpSysTimeInfo.u1DomainId = pPtpPort->u1DomainId;
            PtpPortClkIwfGetClock (&PtpSysTimeInfo);
            MEMCPY (&(pPtpPort->PeerDelayOriginTime),
                    &(PtpSysTimeInfo.FsClkTimeVal), sizeof (tFsClkTimeVal));
            break;

        case PTP_PEER_DELAY_RESP_MSG:
            /* Refer Section 11.4.3 7) (iii) of IEEE Std 1588-2008. 
             * The correction field needs to be updated for the turnaround time
             * of the Peer delay request/response messages. PtpSysTimeInfo 
             * represents the Peer Delay Response transmitted time, wherein 
             * pPtpTxParams->pPeerDelayReqIngressTs represents the ingress time
             * stamp of Peer Delay Request message
             * */
            PtpSysTimeInfo.u4ContextId = pPtpPort->u4ContextId;
            PtpSysTimeInfo.u1DomainId = pPtpPort->u1DomainId;
            PtpPortClkIwfGetClock (&PtpSysTimeInfo);
            PtpClkSubtractTimeStamps (&(PtpSysTimeInfo.FsClkTimeVal),
                                      &(pPtpTxParams->pPeerDelayReqIngressTs->
                                        FsClkTimeVal),
                                      &(CorrectionTime.FsClkTimeVal), &i1Sign);
            u8CorrnField.u4Hi = CorrectionTime.FsClkTimeVal.u8Sec.u4Hi;
            u8CorrnField.u4Lo = CorrectionTime.FsClkTimeVal.u8Sec.u4Lo;

            /* Calculated TurnAround time is added with the correction field
             * obtained from the PeerDelayRequest as mentioned in the standard */
            FSAP_U8_ADD (&u8CorrnField, &u8CorrnField,
                         &(pPtpPort->u8PDelCorrectionField));

            UINT8_LO (&(u8NanoSecConversionFactor)) =
                PTP_NANO_SEC_TO_SEC_CONV_FACTOR;
            UINT8_LO (&(u8NanoSecVal)) = CorrectionTime.FsClkTimeVal.u4Nsec;
            FSAP_U8_MUL (&u8CorrnField, &u8CorrnField,
                         &u8NanoSecConversionFactor);
            FSAP_U8_ADD (&u8NanoSecVal, &u8NanoSecVal, &u8CorrnField);

            FSAP_U8_CLR (&u8CorrnField);
            PtpClkCalcScalNanoSecs (u8NanoSecVal, &u8CorrnField);
            pu1TmpPdu = pu1PtpPdu + PTP_MSG_CORRECTION_FIELD_OFFSET +
                u4MediaHdrLen;
            if (!((pPtpPort->pPtpDomain->ClockDs.PtpOcBcClkDs.ClkDs.bTwoStepFlag
                   == PTP_ENABLED) || (pPtpPort->pPtpDomain->ClockDs.
                                       TransClkDs.bTwoStepFlag == PTP_ENABLED)))

            {
                /* Correction field is of 8 bytes.  */
                PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, u8CorrnField.u4Hi);
                PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, u8CorrnField.u4Lo);
            }
            else
            {
                if (gPtpGlobalInfo.TimeStamp != PTP_HARDWARE_TIMESTAMPING)
                {
                    /* Follow up messages. Turn Around time(t3-t2) + Peer delay
                     * Request correction field is added and scaled nanosecond
                     * value is stored */
                    MEMCPY (&(pPtpPort->u8PDelCorrectionField),
                            &(u8CorrnField), sizeof (FS_UINT8));
                }
                else
                {
                    /* Copying the peer delay request incase of 
                     * two step hardware to calculate the turn around time 
                     * when exact peer delay response time is given by the 
                     * hardware for triggering the follow-up */
                    MEMCPY (&(pPtpPort->DelayReqIngressTime),
                            &(pPtpTxParams->pPeerDelayReqIngressTs->
                              FsClkTimeVal), sizeof (tFsClkTimeVal));

                    /* Note: u8PDelCorrectionField field will have the 
                     * Peer delay request correction field */
                }
            }
            break;

        case PTP_PDELAY_RESP_FOLLOWUP_MSG:
            /* Refer Section 11.4.3.c.7) (iii) of IEEE Std 1588-2008. 
             * The correction field needs to be updated for the turnaround time
             * of the Peer delay request/response messages. 
             * */
            pu1TmpPdu = pu1PtpPdu + PTP_MSG_CORRECTION_FIELD_OFFSET +
                u4MediaHdrLen;

            PTP_LBUF_PUT_4_BYTES (pu1TmpPdu,
                                  pPtpPort->u8PDelCorrectionField.u4Hi);
            PTP_LBUF_PUT_4_BYTES (pu1TmpPdu,
                                  pPtpPort->u8PDelCorrectionField.u4Lo);

            /* Clear the Peer delay Correction field as the value is sent in 
             * peer delay followup */
            FSAP_U8_CLR (&(pPtpPort->u8PDelCorrectionField));
            break;

        default:
            break;
    }

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpTxForwardPktOnDmnPorts                     */
/*                                                                           */
/* Description               : This routine forwards the given PTP message   */
/*                             to all ports that are part of the domain other*/
/*                             than the calling port.                        */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                             u4PktLen - Packet length.                     */
/*                             u1MsgType- Message type.                      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpTxForwardPktOnDmnPorts (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                           UINT4 u4PktLen, UINT1 u1MsgType,
                           tFsClkTimeVal * pEventTimeStamp)
{
    tPtpTxParams        PtpTxParams;
    UINT1              *pu1PtpPdu = NULL;
    tPtpPort           *pDmnPort = NULL;
    UINT4               u4Port = 0;
    INT4                i4RetVal = OSIX_SUCCESS;

    PTP_FN_ENTRY ();

    MEMSET (&PtpTxParams, 0, sizeof (tPtpTxParams));

    while (((pDmnPort = PtpIfGetNextPortEntry (pPtpPort->u4ContextId,
                                               pPtpPort->u1DomainId,
                                               u4Port)) != NULL) &&
           (pDmnPort->u4ContextId == pPtpPort->u4ContextId) &&
           (pDmnPort->u1DomainId == pPtpPort->u1DomainId))
    {
        u4Port = pDmnPort->PortDs.u4PtpPortNumber;

        if (pPtpPort->PtpDeviceType != PTP_IFACE_VLAN)
        {
            if (u4Port == pPtpPort->PortDs.u4PtpPortNumber)
            {
                continue;
            }
        }

        /* If the PTP is disabled in the port, skip the port */
        if (pPtpPort->PortDs.u1PtpEnabledStatus == PTP_DISABLED)
        {
            continue;
        }

        pu1PtpPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                                   u4PktLen);
        if (pu1PtpPdu == NULL)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                      "Unable to allocate memory for transmitting pkt over "
                      "port\r\n", pPtpPort));
            PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);

            PTP_FN_EXIT ();
            return OSIX_FAILURE;
        }

        MEMSET (pu1PtpPdu, 0, u4PktLen);

        MEMCPY (pu1PtpPdu, pu1Pdu, u4PktLen);

        PtpTxParams.pu1PtpPdu = pu1PtpPdu;
        PtpTxParams.pPtpPort = pDmnPort;
        PtpTxParams.u4MsgLen = u4PktLen;
        PtpTxParams.u1MsgType = u1MsgType;
        PtpTxParams.pEventTimeStamp = pEventTimeStamp;

        if (PtpTxTransmitPtpMessage (&PtpTxParams) == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC, "Unable to forward received PTP "
                      "message over port%d\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            i4RetVal = OSIX_FAILURE;
        }
        /* pu1PtpPdu will be freed by the above function */
    }                            /* For all ports that are part of the domain */

    PTP_FN_EXIT ();
    return i4RetVal;
}

#endif /* _PTPTX_C_ */
