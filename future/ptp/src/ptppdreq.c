/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptppdreq.c,v 1.9 2014/03/15 14:18:20 siva Exp $
 * Description: This file contains PTP task peer delay request message 
 *              handler related routines.
 *********************************************************************/
#ifndef _PTPPDREQ_C_
#define _PTPPDREQ_C_

#include "ptpincs.h"
PRIVATE INT4        PtpPdreqValidatePeerDelayReqMsg (tPtpPort * pPtpPort);

PRIVATE INT4        PtpPdreqProcessPeerDelayReqMsg (UINT1 *pu1Pdu,
                                                    tPtpPort * pPtpPort,
                                                    tPtpHdrInfo * pPtpHdrInfo,
                                                    tClkSysTimeInfo *
                                                    pPtpIngressTimeStamp);

/*****************************************************************************/
/* Function                  : PtpPdreqHandlePeerDelayReqMsg                 */
/*                                                                           */
/* Description               : This routine will be invoked to handle the    */
/*                             peer delay request messages. This routine will*/
/*                             validate and processes the received peer delay*/
/*                             request message.                              */
/*                                                                           */
/* Input                     : pPtpPktParams - Pointer to received Packet    */
/*                                             Parameters.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPdreqHandlePeerDelayReqMsg (tPtpPktParams * pPtpPktParams)
{
    tPtpHdrInfo         PtpHdrInfo;

    PTP_FN_ENTRY ();

    MEMSET (&PtpHdrInfo, 0, sizeof (tPtpHdrInfo));

    PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
              pPtpPktParams->pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Received Peer Delay Request Message "
              "for port %u ... \r\n",
              pPtpPktParams->pPtpPort->PortDs.u4PtpPortNumber));

    if ((pPtpPktParams->pPtpPort->pPtpDomain->ClkMode
         == PTP_TRANSPARENT_CLOCK_MODE) &&
        (pPtpPktParams->pPtpPort->pPtpDomain->ClockDs.TransClkDs.TransClkType
         == PTP_E2E_TRANSPARENT_CLOCK))
    {

        /* The timestamping for ingress and egress events along with correction
         * field updation should happen only when software time stamping is
         * enabled in the system.
         * The packet should be handled at the hardware level itself if hardware
         * time stamping is enabled and hardware supprts updation of residence
         * time for the transparent clocks.
         * */
        if (gPtpGlobalInfo.TimeStamp == PTP_HARDWARE_TRANS_TIMESTAMPING)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }

        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
                  "Forwarding the received Peer" "delay request message.\r\n"));

        PtpTransHandleRcvdPtpPkt (pPtpPktParams->pu1RcvdPdu,
                                  pPtpPktParams->pPtpPort,
                                  pPtpPktParams->u4PktLen, PTP_DELAY_REQ_MSG,
                                  &(pPtpPktParams->pPtpIngressTimeStamp->
                                    FsClkTimeVal));

        PTP_FN_EXIT ();
        return OSIX_SUCCESS;

    }

    pPtpPktParams->pu1RcvdPdu = pPtpPktParams->pu1RcvdPdu +
        PTP_GET_MEDIA_HDR_LEN (pPtpPktParams->pPtpPort);

    /* P2P messages will be operational only in case of transparent clocks
     * that support the delay mechanism as P2P. On all other cases, the 
     * messages need to be dropped.
     * */
    PtpUtilExtractHdrFromMsg (pPtpPktParams->pu1RcvdPdu, &PtpHdrInfo);

    if (PtpPdreqValidatePeerDelayReqMsg (pPtpPktParams->pPtpPort)
        != OSIX_SUCCESS)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpPdreqHandlePeerDelayReqMsg: "
                  "Peer Delay Request Message Validation returned "
                  "Failure!\r\n"));

        /* Incrementing the dropped message count */
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
    else
    {
        /* Incrementing the received Peer Delay request message count */
        pPtpPktParams->pPtpPort->PtpPortStats.u4RcvdPeerDelayReqMsgCnt++;
    }

    if (PtpPdreqProcessPeerDelayReqMsg (pPtpPktParams->pu1RcvdPdu,
                                        pPtpPktParams->pPtpPort, &PtpHdrInfo,
                                        pPtpPktParams->pPtpIngressTimeStamp)
        != OSIX_SUCCESS)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpDreqHandleDelayReqMsg: "
                  "Peer Delay Request Message Processing returned "
                  "Failure!\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpPdreqValidatePeerDelayReqMsg               */
/*                                                                           */
/* Description               : This routine validates the received PTP       */
/*                             peer delay request message.                   */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpPdreqValidatePeerDelayReqMsg (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();

    /* The peer delay request message scope is node to node only so when the
     * node receives peer delay request message and the clock is E2E 
     * transparent clock, drop the packet otherwise. If the clock mode is 
     * ordinary or boundary or transparent clock and delay mechanism is peer to 
     * peer, then the packet needs to be processed. If the clock is P2P
     * transparent clock this meanpath delay will be used to update the 
     * correction field of the sync message which is passing through the P2P
     * transparent clock
     */
    if ((pPtpPort->pPtpDomain->ClkMode == PTP_TRANSPARENT_CLOCK_MODE) &&
        (pPtpPort->PortDs.u1DelayMechanism != PTP_PORT_DELAY_MECH_PEER_TO_PEER))
    {
        /* If the clock mode is not transparent clock, then the port should
         * have peer delay mechanism enabled to process this message. Otherwise
         * drop it.
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Port does not support P2P Delay Mechanism...\r\n"));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Discarding the message....\r\n"));

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpPdreqProcessPeerDelayReqMsg                */
/*                                                                           */
/* Description               : This routine processes the received peer delay*/
/*                             request messages.                             */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                             pPtpHdrInfo - Pointer to the header struct    */
/*                             pPtpingresstimestamp - ingress time stamp of  */
/*                                                    the peer delay request */
/*                                                    message.               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpPdreqProcessPeerDelayReqMsg (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                                tPtpHdrInfo * pPtpHdrInfo,
                                tClkSysTimeInfo * pPtpIngressTimeStamp)
{
    PTP_FN_ENTRY ();

    UNUSED_PARAM (pPtpHdrInfo);

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Transmitting Peer Delay response message "
              "for the received Peer delay request message over port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    if (PtpPdresTxPeerDelayResp (pu1Pdu, pPtpPort, pPtpIngressTimeStamp)
        == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "Unable to transmit Peer delay response over"
                  " Port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_FN_EXIT ();

        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpPdreqTxPeerDelayReqMsg                     */
/*                                                                           */
/* Description               : This routine transmits delay request message  */
/*                             over the given interface.                     */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPdreqTxPeerDelayReqMsg (tPtpPort * pPtpPort)
{
    UINT1              *pu1PtpPdu = NULL;
    UINT1              *pu1TmpPdu = NULL;
    FS_UINT8            u8Time;
    UINT4               u4MsgLen = 0;
    UINT1               au1Reserved[PTP_SRC_PORT_ID_LEN];
    tPtpTxParams        PtpTxParams;

    PTP_FN_ENTRY ();

#ifdef L2RED_WANTED
    /*only in Slave mode packet need to send out
     *otherwise no need to process the packet */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        if (pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE)
        {
            /* Returning  because in standby node
             * only slave mode will work*/
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }
    }
#endif

    FSAP_U8_CLR (&u8Time);
    MEMSET (au1Reserved, 0, PTP_SRC_PORT_ID_LEN);
    MEMSET (&PtpTxParams, 0, sizeof (tPtpTxParams));

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Transmitting Delay Request Message "
              "over interface %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    if (pPtpPort->PortDs.u1DelayMechanism != PTP_PORT_DELAY_MECH_PEER_TO_PEER)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Configured delay mechanism in Port %u "
                  "is not Peer to peer\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Not transmitting Peer Delay Request "
                  "message through Port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    /* P2P mechanism needs to happen in each of the ports irrespective of
     * the port state */
    pu1PtpPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                               PTP_MAX_PDU_LEN);

    if (pu1PtpPdu == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | CONTROL_PLANE_TRC,
                  "Unable to allocate memory for transmitting pkt over "
                  "port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (pu1PtpPdu, 0, PTP_MAX_PDU_LEN);

    pu1TmpPdu = pu1PtpPdu;

    PtpHdrAddMediaHdrForIntf (&pu1TmpPdu, pPtpPort, PTP_PEER_DELAY_REQ_MSG,
                              &u4MsgLen);

    /* Refer Table 29 of IEEE 1588-2008 for Delay request Message format
     * */
    PtpHdrFillPtpHeader (&pu1TmpPdu, pPtpPort, PTP_PEER_DELAY_REQ_MSG);

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Filling PTP Header information for "
              "Peer Delay Request Message over port %u\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    /* Time stamp will be added in the PTP TX Sub module */
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, (u8Time.u4Hi));
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, (u8Time.u4Lo));

    /* Put 10 bytes of padding data all zeros
     * The reserved field in Pdelay_Req message is to make the message length 
     * match the length of the Pdelay_Resp message. In some networks and
     * bridges, messages with unequal lengths have different transit times that
     * introduce asymmetry errors */

    PTP_LBUF_PUT_N_BYTES (pu1TmpPdu, au1Reserved, PTP_SRC_PORT_ID_LEN);

    PtpTxParams.pu1PtpPdu = pu1PtpPdu;
    PtpTxParams.pPtpPort = pPtpPort;
    PtpTxParams.u4MsgLen = u4MsgLen + PTP_PEER_DELAY_REQ_MSG_LEN;
    PtpTxParams.u1MsgType = (UINT1) PTP_PEER_DELAY_REQ_MSG;

#ifdef L2RED_WANTED
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        /*Changing clock identity. Making 5th byte as 0xFF from 0xFE */
        PTP_RED_CHANGE_CLK_IDENTITY (pu1PtpPdu);
    }
#endif

    if (PtpTxTransmitPtpMessage (&PtpTxParams) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpTxTransmitPtpMessage through Port %u"
                  "returned Failure!!!!!\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpPdreqInitPdreqHandler                      */
/*                                                                           */
/* Description               : This routine initialses the Peer Delay Request*/
/*                             Message handler.                              */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPdreqInitPdreqHandler (VOID)
{
    PTP_FN_ENTRY ();

    /* Initialize the message handler function pointer */
    gPtpGlobalInfo.PtpMsgFn[PTP_PDREQ_FN_PTR].pMsgRxHandler =
        PtpPdreqHandlePeerDelayReqMsg;
    PTP_FN_EXIT ();
}
#endif /* _PTPPDREQ_C_ */
