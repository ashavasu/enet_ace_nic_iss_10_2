/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 *  $Id: ptppdres.c,v 1.13 2014/07/03 10:21:33 siva Exp $
 *
 * Description: This file contains PTP task peer delay response message 
 *              handler related routines.
 *********************************************************************/
#ifndef _PTPPDRES_C
#define _PTPPDRES_C

#include "ptpincs.h"

PRIVATE INT4        PtpPdresValidatePeerDelayRespMsg (UINT1 *pu1Pdu,
                                                      tPtpPort * pPtpPort,
                                                      UINT4 u4PktLen,
                                                      tClkSysTimeInfo *
                                                      pPtpIngressTimeStamp);

PRIVATE VOID        PtpPdresProcessPeerDelayRespMsg (UINT1 *pu1Pdu,
                                                     tPtpPort * pPtpPort,
                                                     tPtpHdrInfo * pPtpHdrInfo,
                                                     tClkSysTimeInfo *
                                                     pPtpSysTimeInfo);

/*****************************************************************************/
/* Function                  : PtpPdresHandlePeerDelayRespMsg                */
/*                                                                           */
/* Description               : This routine will be invoked to handle the    */
/*                             peer delay responsemessages. This routine will*/
/*                             validate and processes the received peer delay*/
/*                             response message.                             */
/*                                                                           */
/* Input                     : pPtpPktParams - Pointer to received Packet    */
/*                                             Parameters.                   */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPdresHandlePeerDelayRespMsg (tPtpPktParams * pPtpPktParams)
{
    tPtpHdrInfo         PtpHdrInfo;

    PTP_FN_ENTRY ();

    MEMSET (&PtpHdrInfo, 0, sizeof (tPtpHdrInfo));

    PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
              pPtpPktParams->pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Received Peer Delay Response Message for "
              "port %u....\r\n",
              pPtpPktParams->pPtpPort->PortDs.u4PtpPortNumber));

    if ((pPtpPktParams->pPtpPort->pPtpDomain->ClkMode
         == PTP_TRANSPARENT_CLOCK_MODE) &&
        (pPtpPktParams->pPtpPort->pPtpDomain->ClockDs.TransClkDs.TransClkType
         != PTP_P2P_TRANSPARENT_CLOCK))
    {
        /* Clock follows end to end transparent delay mechanism. Hence
         * forward the message transparently through the node after
         * residence time correction.
         * Refer 11.5.4 of IEEE Std 1588 2008
         */
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
                  "Clock is operating in End to End " "Transparent mode.\r\n"));

        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
                  "TimeStamping for ingress event " "message.\r\n"));

        /* The timestamping for ingress and egress events along with correction
         * field updation should happen only when software time stamping is
         * enabled in the system.
         * The packet should be handled at the hardware level itself if hardware
         * time stamping is enabled.
         */
        if (gPtpGlobalInfo.TimeStamp == PTP_HARDWARE_TRANS_TIMESTAMPING)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }

        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId, CONTROL_PLANE_TRC,
                  "Forwarding the received Peer"
                  "delay response message.\r\n"));

        PtpTransHandleRcvdPtpPkt (pPtpPktParams->pu1RcvdPdu,
                                  pPtpPktParams->pPtpPort,
                                  pPtpPktParams->u4PktLen,
                                  PTP_PEER_DELAY_RESP_MSG,
                                  &(pPtpPktParams->pPtpIngressTimeStamp->
                                    FsClkTimeVal));
        PTP_FN_EXIT ();
        return OSIX_SUCCESS;
    }

    pPtpPktParams->pu1RcvdPdu = pPtpPktParams->pu1RcvdPdu +
        PTP_GET_MEDIA_HDR_LEN (pPtpPktParams->pPtpPort);

    /* P2P messages will be operational only in case of transparent clocks
     * that support the delay mechanism as P2P. On all other cases, the 
     * messages need to be dropped.
     * */
    PtpUtilExtractHdrFromMsg (pPtpPktParams->pu1RcvdPdu, &PtpHdrInfo);

    if (PtpPdresValidatePeerDelayRespMsg (pPtpPktParams->pu1RcvdPdu,
                                          pPtpPktParams->pPtpPort,
                                          pPtpPktParams->u4PktLen,
                                          pPtpPktParams->pPtpIngressTimeStamp)
        != OSIX_SUCCESS)
    {
        PTP_TRC ((pPtpPktParams->pPtpPort->u4ContextId,
                  pPtpPktParams->pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpPdresHandlePeerDelayRespMsg: "
                  "Peer Delay Response Message Validation returned "
                  "Failure!!!\r\n"));

        /* Incrementing the dropped message count */
        pPtpPktParams->pPtpPort->PtpPortStats.u4DroppedPtpMsgCnt++;
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
    PtpPdresProcessPeerDelayRespMsg (pPtpPktParams->pu1RcvdPdu,
                                     pPtpPktParams->pPtpPort, &PtpHdrInfo,
                                     pPtpPktParams->pPtpIngressTimeStamp);

    /*Updating PTP Message counters */
    if (PtpHdrInfo.u1MsgType == PTP_PEER_DELAY_RESP_MSG)
    {
        /* Incrementing Peer delay response message count */
        pPtpPktParams->pPtpPort->PtpPortStats.u4RcvdPeerDelayRespMsgCnt++;
    }
    else if (PtpHdrInfo.u1MsgType == PTP_PDELAY_RESP_FOLLOWUP_MSG)
    {
        /* Incrementing Peer delay response Follow up message count */
        pPtpPktParams->pPtpPort->PtpPortStats.
            u4RcvdPeerDelayRespFollowUpMsgCnt++;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpPdresValidatePeerDelayRespMsg              */
/*                                                                           */
/* Description               : This routine validates the received PTP       */
/*                             peer delay response message.                  */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                             pPtpHdrInfo - Pointer to Header info.         */
/*                             u4PktLen - Length of the received PDU         */
/*                             pPtpIngressTimeStamp - Ingress Time Stamp     */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpPdresValidatePeerDelayRespMsg (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                                  UINT4 u4PktLen,
                                  tClkSysTimeInfo * pPtpIngressTimeStamp)
{
    PTP_FN_ENTRY ();
    UNUSED_PARAM (pu1Pdu);
    UNUSED_PARAM (u4PktLen);
    UNUSED_PARAM (pPtpIngressTimeStamp);
    if ((pPtpPort->pPtpDomain->ClkMode != PTP_TRANSPARENT_CLOCK_MODE) &&
        (pPtpPort->PortDs.u1DelayMechanism != PTP_PORT_DELAY_MECH_PEER_TO_PEER))
    {
        /* If the clock mode is not transparent clock, then the message should
         * not be consumed.
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpPdresValidatePeerDelayRespMsg:Clock Mode is Transparent"
                  "Message Should be Consumed.\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpPdresProcessPeerDelayRespMsg               */
/*                                                                           */
/* Description               : This routine processes the received peer delay*/
/*                             response messages.                            */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                             pPtpHdrInfo - Pointer to the header struct    */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpPdresProcessPeerDelayRespMsg (UINT1 *pu1Pdu, tPtpPort * pPtpPort,
                                 tPtpHdrInfo * pPtpHdrInfo,
                                 tClkSysTimeInfo * pPtpSysTimeInfo)
{
    FS_UINT8            u8CorrectionField;
    FS_UINT8            u8Mean;
    FS_UINT8            u8Reminder;
    FS_UINT8            u8NanoSecValue;
    tClkSysTimeInfo     CorrectionTime;
    tPtpNpWrPeerDelay   PtpNpWrPeerDelay;

    PTP_FN_ENTRY ();

    UNUSED_PARAM (pu1Pdu);

    FSAP_U8_CLR (&u8CorrectionField);
    FSAP_U8_CLR (&u8Mean);
    FSAP_U8_CLR (&u8Reminder);
    FSAP_U8_CLR (&u8NanoSecValue);

    MEMSET (&PtpNpWrPeerDelay, 0, sizeof (tPtpNpWrPeerDelay));
    MEMSET (&CorrectionTime, 0, sizeof (tClkSysTimeInfo));
    UINT8_LO (&u8Mean) = PTP_MEAN_LINK_DELAY_VALUE;
    UINT8_LO (&(u8NanoSecValue)) = PTP_NANO_SEC_TO_SEC_CONV_FACTOR;
    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Processing received Peer delay response "
              "message through Port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));

    if ((pPtpHdrInfo->u2Flags & PTP_TWO_STEP_CLK_MASK) != PTP_TWO_STEP_CLK_MASK)
    {
        /* Follow up message will not be received for this delay response 
         * message. Compute meandelay as per Section 11.4.3 of 
         * IEEE Std 1588 2008
         * <meanPathDelay> = [(t4-t1) - CorrectionField of Pdelay_Resp]
         *                   _________________________________________
         *                                  2
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Peer delay response message sent from "
                  "one step clock in Port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Calculating the Peer mean path "
                  "delay in Port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PtpClkSubtractTimeStamps (&(pPtpSysTimeInfo->FsClkTimeVal),
                                  &(pPtpPort->PeerDelayOriginTime),
                                  &(CorrectionTime.FsClkTimeVal),
                                  &(pPtpPort->PortDs.i1PeerDelaySign));
        FSAP_U8_MUL (&u8CorrectionField, &u8NanoSecValue,
                     &(CorrectionTime.FsClkTimeVal.u8Sec));
        u8CorrectionField.u4Lo += CorrectionTime.FsClkTimeVal.u4Nsec;

        if (pPtpPort->PortDs.i1PeerDelaySign == PTP_POSITIVE)
        {

            PtpClkPerf8ByteSubtraction (&u8CorrectionField,
                                        &(pPtpHdrInfo->u8CorrectionField),
                                        &u8CorrectionField,
                                        &(pPtpPort->PortDs.i1PeerDelaySign));
        }
        else
        {
            FSAP_U8_ADD (&u8CorrectionField, &u8CorrectionField,
                         &(pPtpHdrInfo->u8CorrectionField));
        }
        PTP_U8_DIV (&(pPtpPort->PortDs.u8PeerMeanPathDelay), &u8Reminder,
                    &u8CorrectionField, &u8Mean);
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Updated the Peer mean path "
                  "delay in Port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
    }
    else if ((pPtpHdrInfo->u1MsgType & PTP_FOLLOW_UP_MSG) == 0)
    {
        /* Clock is a two step clock and has received a peer delay
         * response message. Expect a follow up message 
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Peer delay response message transmitted "
                  "from two step clock through Port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Waiting for the follow up message.\r\n"));

        /* Store the received correction field in Data base as it should be
         * used upon receipt of follow up message 
         * */
        FSAP_U8_ASSIGN (&(pPtpPort->PortDs.u8PeerMeanPathDelay),
                        &(pPtpHdrInfo->u8CorrectionField));
        /* Store the time at which peer delay response message was received */
        MEMCPY (&(pPtpPort->PeerDelayRespIngressTime),
                &(pPtpSysTimeInfo->FsClkTimeVal), sizeof (tFsClkTimeVal));
    }
    else if ((pPtpHdrInfo->u1MsgType & PTP_FOLLOW_UP_MSG) != 0)
    {
        /* Follow up message will not be received for this delay response 
         * message. Compute meandelay as per Section 11.4.3 of 
         * IEEE Std 1588 2008
         * <meanPathDelay> = [(t4-t1) - CorrectionField of Pdelay_Resp 
         *                            - CorrectionField of Follow up]
         *                   _________________________________________
         *                                  2
         * */
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Peer delay response follow up message "
                  "sent from two step clock through Port %u\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Calculating the Peer mean path "
                  "delay ...\r\n"));
        PtpClkSubtractTimeStamps (&(pPtpPort->PeerDelayRespIngressTime),
                                  &(pPtpPort->PeerDelayOriginTime),
                                  &(CorrectionTime.FsClkTimeVal),
                                  &(pPtpPort->PortDs.i1PeerDelaySign));
        FSAP_U8_MUL (&u8CorrectionField, &u8NanoSecValue,
                     &(CorrectionTime.FsClkTimeVal.u8Sec));
        u8CorrectionField.u4Lo += CorrectionTime.FsClkTimeVal.u4Nsec;

        /* The correction field received in the Pdelay_Resp message would
         * have been stored in the data base already. Use the same now for
         * calculating the mean path delay
         * */
        if (pPtpPort->PortDs.i1PeerDelaySign == PTP_POSITIVE)
        {
            PtpClkPerf8ByteSubtraction (&u8CorrectionField,
                                        &(pPtpPort->PortDs.u8PeerMeanPathDelay),
                                        &u8CorrectionField,
                                        &(pPtpPort->PortDs.i1PeerDelaySign));
        }
        else
        {
            FSAP_U8_ADD (&u8CorrectionField, &u8CorrectionField,
                         &(pPtpPort->PortDs.u8PeerMeanPathDelay));
        }

        /* Header info contains the correction field of the follow up
         * message.*/
        if (pPtpPort->PortDs.i1PeerDelaySign == PTP_POSITIVE)
        {
            PtpClkPerf8ByteSubtraction (&u8CorrectionField,
                                        &(pPtpHdrInfo->u8CorrectionField),
                                        &u8CorrectionField,
                                        &(pPtpPort->PortDs.i1PeerDelaySign));
        }
        else
        {
            FSAP_U8_ADD (&u8CorrectionField, &u8CorrectionField,
                         &(pPtpHdrInfo->u8CorrectionField));
        }
        PTP_U8_DIV (&(pPtpPort->PortDs.u8PeerMeanPathDelay), &u8Reminder,
                    &u8CorrectionField, &u8Mean);

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC, "Updated the Peer mean path "
                  "delay...\r\n"));
    }

#ifdef NPAPI_WANTED
    /* call NPAPI to program the peer mean path delay */
    PtpNpWrPeerDelay.u4ContextId = pPtpPort->u4ContextId;
    PtpNpWrPeerDelay.u1DomainId = pPtpPort->u1DomainId;
    PtpNpWrPeerDelay.u4IfIndex = pPtpPort->u4IfIndex;
    FSAP_U8_ASSIGN (&(PtpNpWrPeerDelay.u8PeerMeanPathDelay),
                    &(pPtpPort->PortDs.u8PeerMeanPathDelay));

    PtpNpWrConfigPeerDelay (&PtpNpWrPeerDelay);
#endif

    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpPdresTxPeerDelayResp                       */
/*                                                                           */
/* Description               : This routine validates the received PTP       */
/*                             peer delay request message.                   */
/*                                                                           */
/* Input                     : pu1Pdu - Pointer to the received PDU          */
/*                             pPtpPort - Pointer to the port structure      */
/*                             pPtpingresstimestamp - ingress time stamp of  */
/*                                                    the peer delay request */
/*                                                    message.               */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPdresTxPeerDelayResp (UINT1 *pu1RxPdu, tPtpPort * pPtpPort,
                         tClkSysTimeInfo * pPtpIngressTimeStamp)
{
    UINT1              *pu1PtpPdu = NULL;
    UINT1              *pu1TmpPdu = NULL;
    tPtpTxParams        PtpTxParams;
    UINT4               u4MsgLen = 0;

    PTP_FN_ENTRY ();

    MEMSET (&PtpTxParams, 0, sizeof (tPtpTxParams));

#ifdef L2RED_WANTED
    /*only in Slave mode packet need to send out
     *otherwise no need to process the packet */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        if (pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }
    }
#endif

    pu1PtpPdu = MemBuddyAlloc ((UINT1) gPtpGlobalInfo.i4PktBuddyId,
                               PTP_MAX_PDU_LEN);
    if (pu1PtpPdu == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | PTP_CRITICAL_TRC,
                  "Unable to allocate memory for transmitting pkt over "
                  "port %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PtpTrapSendGlobErrTrap ((UINT1) PTP_GLOBAL_ERR_MEM_FAIL);

        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    MEMSET (pu1PtpPdu, 0, PTP_MAX_PDU_LEN);

    pu1TmpPdu = pu1PtpPdu;

    /* Refer Annex F of IEEE Std 1588 2008 */
    PtpHdrAddMediaHdrForIntf (&pu1TmpPdu, pPtpPort, PTP_PEER_DELAY_RESP_MSG,
                              &u4MsgLen);

    /* Refer Table 30 of IEEE 1588-2008 for Peer Delay Response message
     * */
    PtpHdrFillPtpHeader (&pu1TmpPdu, pPtpPort, PTP_PEER_DELAY_RESP_MSG);

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC, "Filling PTP header for the peer delay "
              "response message\r\n"));

    /* Refer Section 11.4.3 (c) for changes in header of the transmitted
     * Delay Response message
     * */
    PtpDresCopyHdrFieldFrmDelayReq (pu1RxPdu, pu1PtpPdu, pPtpPort,
                                    PTP_PEER_DELAY_RESP_MSG);

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Changing header field of Delay Response message based "
              "on the received Delay Request message...\r\n"));

    /* Time stamp will be added in the PTP TX Sub module */
    /* Embed the requesting port identity. */
    MEMCPY ((pu1TmpPdu + PTP_SECONDS_LEN + PTP_NANO_SECONDS_LEN),
            pu1RxPdu + PTP_SRC_PORT_ID_OFFSET, PTP_SRC_PORT_ID_LEN);

    PtpTxParams.pu1PtpPdu = pu1PtpPdu;
    PtpTxParams.pPtpPort = pPtpPort;
    /* Length of the peer delay request and response is same */
    PtpTxParams.u4MsgLen = (UINT4) u4MsgLen + PTP_PEER_DELAY_REQ_MSG_LEN;
    PtpTxParams.u1MsgType = (UINT1) PTP_PEER_DELAY_RESP_MSG;
    PtpTxParams.pPeerDelayReqIngressTs = pPtpIngressTimeStamp;

    if (PtpTxTransmitPtpMessage (&PtpTxParams) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC, "PtpTxTransmitPtpMessage returned "
                  "Failure!!!!!\r\n", pPtpPort->PortDs.u4PtpPortNumber));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function                  : PtpPdresInitPeerDelayRespHandler              */
/*                                                                           */
/* Description               : This routine initialses the Peer Delay Resp   */
/*                             Message handler.                              */
/*                                                                           */
/* Input                     : None.                                         */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpPdresInitPeerDelayRespHandler (VOID)
{
    PTP_FN_ENTRY ();

    /* Initialize the message handler function pointer */
    gPtpGlobalInfo.PtpMsgFn[PTP_PDRES_FN_PTR].pMsgRxHandler =
        PtpPdresHandlePeerDelayRespMsg;
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function                  : PtpPdresTransmitFollowUp                      */
/*                                                                           */
/* Description               : This routine transmits a FOLLOWUP for the Peer*/
/*                             Delay Response message transmitted earlier    */
/*                             over the interface.                           */
/*                                                                           */
/* Input                     : pPtpPort - Pointer to the port structure.     */
/*                             pPtpSysTimeInfo - Pointer to time stamp.      */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : None.                                         */
/*                                                                           */
/* Global Variables Modified : None.                                         */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : OSIX_SUCCESS/OSIX_FAILURE.                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpPdresTransmitFollowUp (tPtpPort * pPtpPort,
                          tClkSysTimeInfo * pPtpSysTimeInfo)
{
    tPtpTxParams        PtpTxParams;
    UINT1              *pu1TmpPdu = pPtpPort->pu1PeerDelayResp;
    UINT4               u4MediaHdrLen = PTP_GET_MEDIA_HDR_LEN (pPtpPort);
    UINT2               u2TwoByteVal = 0;
    UINT1               u1OneByteVal = 0;
    tClkSysTimeInfo     CorrectionTime;
    FS_UINT8            u8CorrnField;
    FS_UINT8            u8NanoSecVal;
    FS_UINT8            u8NanoSecConversionFactor;
    INT1                i1Sign = 0;

    PTP_FN_ENTRY ();

    MEMSET (&PtpTxParams, 0, sizeof (tPtpTxParams));
    MEMSET (&CorrectionTime, 0, sizeof (tClkSysTimeInfo));
    FSAP_U8_CLR (&u8CorrnField);
    FSAP_U8_CLR (&u8NanoSecVal);
    FSAP_U8_CLR (&u8NanoSecConversionFactor);

#ifdef L2RED_WANTED
    /*only in Slave mode packet need to send out
     *otherwise no need to process the packet */
    if (gPtpRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        if (pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE)
        {
            PTP_FN_EXIT ();
            return OSIX_SUCCESS;
        }
    }
#endif

    PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
              CONTROL_PLANE_TRC,
              "Transmitting Follow up for Peer Delay Response "
              "Message over interface %u.....\r\n",
              pPtpPort->PortDs.u4PtpPortNumber));

    if (pu1TmpPdu == NULL)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                  "Unable to acquire transmitted Peer Delay Response "
                  "Message...\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }
    /* Message type needs to be altered. The earlier transmitted message itself
     * is stored. Hence, it will be having message type as PEER_DELAY_RESP.
     * Hence, it needs to be changed to PEER_DELAY_RESP_FOLLOW_UP message
     * */
    pu1TmpPdu = pu1TmpPdu + u4MediaHdrLen;

    u1OneByteVal = PTP_TRANSPORT_SPECIFIC_VALUE;
    u1OneByteVal = (UINT1) (u1OneByteVal << PTP_TRANS_SPC_BITS_SHIFT);
    u1OneByteVal = u1OneByteVal |
        (UINT1) (PTP_PDELAY_RESP_FOLLOWUP_MSG & PTP_MSG_TYPE_MASK);
    PTP_LBUF_PUT_1_BYTE (pu1TmpPdu, u1OneByteVal);

    /* The value of two step flag will be set to TRUE for PEER DELAY RESP 
     * messages in case the clock is two step clock. But the flag is not
     * applicable for follow up messages. RESET the same
     * */

    pu1TmpPdu = pPtpPort->pu1PeerDelayResp + u4MediaHdrLen +
        PTP_MSG_FLAGS_OFFSET;
    PTP_LBUF_GET_2_BYTES (pu1TmpPdu, u2TwoByteVal);
    PTP_RESET_TWO_STEP_FLAG (u2TwoByteVal);
    PTP_LBUF_PUT_2_BYTES (pu1TmpPdu, u2TwoByteVal);

    /* The port structure contains the back pointer for the Transmitted
     * Peer delay request message. Use the same and free here
     * */
    pu1TmpPdu = pu1TmpPdu + PTP_PDELAY_RESP_TIME_STAMP_OFFSET;

    /* Incase of Hardware Timestamping incoming timestamp will have 
     * the exact Transmit time of peer delay response, for 
     * software timestamp this value will be zero for updating the 
     * origin timestamp of the follow-up message */
    if (gPtpGlobalInfo.TimeStamp == PTP_HARDWARE_TIMESTAMPING)
    {
        /* Calculate the Turn around time and update in the 
         * u8PDelCorrectionField field so that TransmitPtpMessage  
         * function will update this value in the correction field 
         * of follow-up message same as that of software timestamping */

        PtpClkSubtractTimeStamps (&(pPtpSysTimeInfo->FsClkTimeVal),
                                  &(pPtpPort->DelayReqIngressTime),
                                  &(CorrectionTime.FsClkTimeVal), &i1Sign);
        u8CorrnField.u4Hi = CorrectionTime.FsClkTimeVal.u8Sec.u4Hi;
        u8CorrnField.u4Lo = CorrectionTime.FsClkTimeVal.u8Sec.u4Lo;

        /* Calculated TurnAround time is added with the correction field
         * obtained from the PeerDelayRequest as mentioned in the standard */
        FSAP_U8_ADD (&u8CorrnField, &u8CorrnField,
                     &(pPtpPort->u8PDelCorrectionField));

        UINT8_LO (&(u8NanoSecConversionFactor)) =
            PTP_NANO_SEC_TO_SEC_CONV_FACTOR;
        UINT8_LO (&(u8NanoSecVal)) = CorrectionTime.FsClkTimeVal.u4Nsec;
        FSAP_U8_MUL (&u8CorrnField, &u8CorrnField, &u8NanoSecConversionFactor);
        FSAP_U8_ADD (&u8NanoSecVal, &u8NanoSecVal, &u8CorrnField);

        FSAP_U8_CLR (&u8CorrnField);
        PtpClkCalcScalNanoSecs (u8NanoSecVal, &u8CorrnField);

        /* Updating the calculated value in u8PDelCorrectionField */
        MEMCPY (&(pPtpPort->u8PDelCorrectionField),
                &(u8CorrnField), sizeof (FS_UINT8));

        /* Clearing the incoming timestamp to update the origin 
         * timestamp of the followup message */
        pPtpSysTimeInfo->FsClkTimeVal.u8Sec.u4Hi = 0;
        pPtpSysTimeInfo->FsClkTimeVal.u8Sec.u4Lo = 0;
        pPtpSysTimeInfo->FsClkTimeVal.u4Nsec = 0;
    }

    /* Origin Time stamp needs to be added after header */
    u2TwoByteVal = (UINT2) UINT8_HI (&(pPtpSysTimeInfo->FsClkTimeVal.u8Sec));
    PTP_LBUF_PUT_2_BYTES (pu1TmpPdu, u2TwoByteVal);
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu,
                          UINT8_LO (&(pPtpSysTimeInfo->FsClkTimeVal.u8Sec)));
    PTP_LBUF_PUT_4_BYTES (pu1TmpPdu, (pPtpSysTimeInfo->FsClkTimeVal.u4Nsec));

    PtpTxParams.pu1PtpPdu = pPtpPort->pu1PeerDelayResp;
    PtpTxParams.pPtpPort = pPtpPort;
    PtpTxParams.u4MsgLen = pPtpPort->u4PeerDelayRespLen;
    PtpTxParams.u1MsgType = (UINT1) PTP_PDELAY_RESP_FOLLOWUP_MSG;

    /* Packet will be released after sending the follow up message
     * Clearing the dangling pointer */
    pPtpPort->pu1PeerDelayResp = NULL;

    if (PtpTxTransmitPtpMessage (&PtpTxParams) == OSIX_FAILURE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  ALL_FAILURE_TRC | BUFFER_TRC,
                  "PtpTxTransmitPtpMessage returned " "Failure!!!!!\r\n"));
        PTP_FN_EXIT ();
        return OSIX_FAILURE;
    }

    PTP_FN_EXIT ();
    return OSIX_SUCCESS;
}
#endif /*_PTPPDRES_C */
