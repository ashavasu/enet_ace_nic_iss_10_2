/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpalmst.c,v 1.3 2014/01/24 12:20:08 siva Exp $
 *
 * Description: This file contains PTP Alternate Master Sub module
 *              functionality routines.
 *********************************************************************/
#ifndef _PTPALMST_C_
#define _PTPALMST_C_

#include "ptpincs.h"

PRIVATE INT4
 
 
   PtpAlMstCalAltMstEligibility (tPtpPort * pPtpPort, UINT1 *pu1EligibleState);
PRIVATE INT4        PtpAlMstStartTmrs (tPtpPort * pPtpPort);
PRIVATE INT4        PtpAlMstStopTmrs (tPtpPort * pPtpPort);
PRIVATE VOID
 
     PtpAlMstUpdatConfig (tPtpPort * pPtpPort, tPtpAltMstInfo * pPtpAltMstInfo);

/*****************************************************************************/
/* Function     : PtpAlMstStopTmrs                                           */
/*                                                                           */
/* Description  : This function is used to stop the  Alternate master        */
/*                related timers                                             */
/*                1. Stop the announce message timer                         */
/*                2. Stop the Multi Sync message timer, if sync flag set     */
/*                                                                           */
/* Input        : pPtpPort    - Pointer to PTP Port                          */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpAlMstStopTmrs (tPtpPort * pPtpPort)
{
    PTP_FN_ENTRY ();
    /* Stop the Sync message if already running. */
    if (pPtpPort->bAltMulcastSync == PTP_ENABLED)
    {
        if (TmrStop (gPtpGlobalInfo.PtpTmrListId,
                     &(pPtpPort->MulSyncTimer)) == TMR_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpAlMstStopTmrs : TmrStop - Multi Sync failed for Port "
                      ": %u\r\n", pPtpPort->PortDs.u4PtpPortNumber));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }
    }
    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpAlMstStartTmrs                                          */
/*                                                                           */
/* Description  : This function is used to start the Alternate master        */
/*                related timers                                             */
/*                1. Start the announce message timer                        */
/*                2. Start the Multi Sync message timer, if sync flag set    */
/*                                                                           */
/* Input        : pPtpPort    - Pointer to PTP Port                          */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpAlMstStartTmrs (tPtpPort * pPtpPort)
{
    UINT4               u4MilliSec = 0;

    PTP_FN_ENTRY ();
    /* Start the Sync message timer. */
    if (pPtpPort->bAltMulcastSync == PTP_ENABLED)
    {
        u4MilliSec =
            PtpUtilLogBase2ToMilliSec ((INT1)
                                       pPtpPort->u1AltMulcastSynInterval);

        if (PtpTmrStartTmr (&(pPtpPort->MulSyncTimer),
                            u4MilliSec, PTP_MULT_SYNC_TMR, OSIX_TRUE) ==
            OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpAlMstStartTmrs : PtpTmrStartTmr -"
                      " Multi Sync Timer failed for Port : %u!!!\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "Unable to start Multi Sync timer for Port : %u\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
            PTP_FN_EXIT ();
            return (OSIX_FAILURE);
        }
    }

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpAlMstUpdatConfig                                        */
/*                                                                           */
/* Description  : This function is used to update the Alternate master       */
/*                Options for the given port                                 */
/*                                                                           */
/* Input        : pPtpPort    - Pointer to PTP Port                          */
/*              : pPtpAltMstInfo - Alternate Master information.             */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
PtpAlMstUpdatConfig (tPtpPort * pPtpPort, tPtpAltMstInfo * pPtpAltMstInfo)
{
    PTP_FN_ENTRY ();

    if ((pPtpAltMstInfo->u1Flag | PTP_AM_UPDATE_MST_COUNT) ==
        PTP_AM_UPDATE_MST_COUNT)
    {
        pPtpPort->u1NumberOfAltMaster = pPtpAltMstInfo->u1NumberOfAltMaster;
    }
    else if ((pPtpAltMstInfo->u1Flag | PTP_AM_UPDATE_MUL_SYNC) ==
             PTP_AM_UPDATE_MUL_SYNC)
    {
        pPtpPort->bAltMulcastSync = pPtpAltMstInfo->bAltMCSync;
    }
    else if ((pPtpAltMstInfo->u1Flag | PTP_AM_UPDATE_MUL_SYNC_INTERVAL) ==
             PTP_AM_UPDATE_MUL_SYNC_INTERVAL)
    {
        pPtpPort->u1AltMulcastSynInterval = pPtpAltMstInfo->u1AltMCSyncInterval;
    }
    PTP_FN_EXIT ();
}

/*****************************************************************************/
/* Function     : PtpAlMstCalAltMstEligibility                               */
/*                                                                           */
/* Description  : This function is used to calculate the clock is eligible   */
/*                alternate master or not by using the BMC algorithm         */
/*                                                                           */
/* Input        : u4ContextId - Context Identifier.                          */
/*              : u1DomainId  - Domain Identifier.                           */
/*              : u4PortId    - PTP Prot Identifier.                         */
/*              : pu1State    - Pointer to the eligiblity                    */
/*                                                                           */
/* Output       : On Success pu1State will have this node is eligible        */
/*              : alternate master or not                                    */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PtpAlMstCalAltMstEligibility (tPtpPort * pPtpPort, UINT1 *pu1EligibleState)
{
    tPtpForeignMasterDs *pPtpForeignMasterDs = NULL;
    tPtpForeignMasterDs PtpDefaultDs;
    INT4                i4BmcOutPut = 0;
    UINT2               u2Count = 0;

    PTP_FN_ENTRY ();

    *pu1EligibleState = OSIX_FALSE;
    /* use the BMC algo find the the this node is eligible 
     * alternate master or not  */

    /* 1. Get default Data Set */
    MEMSET (&PtpDefaultDs, 0, sizeof (tPtpForeignMasterDs));

    PTP_FORM_DEFAULT_DS (pPtpPort->pPtpDomain, PtpDefaultDs);

    /* 2. Get the foreign master table entry with Altrnate Master flag set */
    /* 3. Compare the 2 Datasets */
    /* 4. Own Announce message is not greater increment count */
    pPtpForeignMasterDs = (tPtpForeignMasterDs *)
        PtpDbGetFirstNode (gPtpGlobalInfo.FMTree, (UINT1)
                           PTP_FOREIGN_MASTER_DATA_SET);
    if (pPtpForeignMasterDs == NULL)
    {
        *pu1EligibleState = OSIX_TRUE;

        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "PtpAlMstCalAltMstEligibility : No Announce Message.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_SUCCESS);
    }

    do
    {
        if ((pPtpForeignMasterDs->u4PortIndex == pPtpPort->PortDs.
             u4PtpPortNumber) &&
            (pPtpForeignMasterDs->bIsAltMstAnnc == OSIX_TRUE))
        {
            if (PtpAnncValidateForeignMaster (pPtpForeignMasterDs) ==
                OSIX_FAILURE)
            {
                continue;
            }

            i4BmcOutPut = PtpBmcDataSetCmpAlgo (&PtpDefaultDs,
                                                pPtpForeignMasterDs);

            if ((i4BmcOutPut == PTP_LESSER) ||
                (i4BmcOutPut == PTP_LESSER_BY_TOPOLOGY))
            {
                u2Count++;
            }
        }

    }
    while ((pPtpForeignMasterDs = (tPtpForeignMasterDs *)
            PtpDbGetNextNode (gPtpGlobalInfo.FMTree, pPtpForeignMasterDs,
                              (UINT1) PTP_FOREIGN_MASTER_DATA_SET)) != NULL);

    /* 5. if count <  u1NumberOfAltMaster  eligible else not eligible */
    if (u2Count < pPtpPort->u1NumberOfAltMaster)
    {
        *pu1EligibleState = OSIX_TRUE;
    }

    PTP_FN_EXIT ();
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpAlMstGetAltMstInfo                                      */
/*                                                                           */
/* Description  : This function is called from the Announce message sub      */
/*                module. It will return the Alternate Master Option for     */
/*                the give port if the entry present in the table return     */
/*                success otherwise failure.                                 */
/*                                                                           */
/* Input        : u4ContextId - Context Identifier.                          */
/*              : u1DomainId  - Domain Identifier.                           */
/*              : u4PortId    - PTP Prot Identifier.                         */
/*              : pPtpAltMstInfo - Pointer to Alternate Master information.  */
/*                                                                           */
/* Output       : On success                                                 */
/*              : pPtpAltMstInfo - Alternate Master information.             */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAlMstGetAltMstInfo (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4PortId,
                       tPtpAltMstInfo * pPtpAltMstInfo)
{
    tPtpPort           *pPtpPort = NULL;

    PTP_FN_ENTRY ();

    if (pPtpAltMstInfo == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpAlMstGetAltMstInfo : NULL Pointer - Failed!.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    pPtpPort = PtpIfGetPortEntry (u4ContextId, u1DomainId, u4PortId);
    if (pPtpPort == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpAlMstGetAltMstInfo : PtpDbGetPortEntry failed. \r\n"));
        PTP_FN_EXIT ();
        return (OSIX_FAILURE);
    }

    pPtpAltMstInfo->u1NumberOfAltMaster = pPtpPort->u1NumberOfAltMaster;
    pPtpAltMstInfo->u1AltMCSyncInterval = pPtpPort->u1AltMulcastSynInterval;
    pPtpAltMstInfo->bAltMCSync = pPtpPort->bAltMulcastSync;
    pPtpAltMstInfo->u1Flag = (PTP_AM_UPDATE_MST_COUNT |
                              PTP_AM_UPDATE_MUL_SYNC |
                              PTP_AM_UPDATE_MUL_SYNC_INTERVAL);

    PTP_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpAlMstTriggerAltMstMsg                                   */
/*                                                                           */
/* Description  : This function is used to find the the alternate master     */
/*                eligiblility and trigger the timer to send the  Announce   */
/*                and Sync message with alternate master flage set           */
/*                                                                           */
/* Input        : pPtpPort    - PTP Port information                         */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAlMstTriggerAltMstMsg (tPtpPort * pPtpPort)
{
    INT4                i4Status = OSIX_FAILURE;
    UINT1               u1EligibleState = OSIX_FALSE;

    PTP_FN_ENTRY ();

    if (pPtpPort->PortDs.u1PortState != PTP_STATE_SLAVE)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpAlMstTriggerAltMstMsg : Port %u is Master State\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_FN_EXIT ();

        return (OSIX_FAILURE);
    }

    if (pPtpPort->u1NumberOfAltMaster == 0)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                  "PtpAlMstTriggerAltMstMsg : Number Of Alternate Master"
                  " is zero.\r\n"));
        PTP_FN_EXIT ();
        return (OSIX_SUCCESS);
    }

    /* It will calculate the alternate master eligibility using the 
     * foreign master table entries received with alternate flag true. 
     */
    PtpAlMstCalAltMstEligibility (pPtpPort, &u1EligibleState);

    if (u1EligibleState == pPtpPort->bIsEligibleAltMst)
    {
        PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                  CONTROL_PLANE_TRC,
                  "PtpAlMstTriggerAltMstMsg : Port : %u Same state\r\n",
                  pPtpPort->PortDs.u4PtpPortNumber));
        PTP_FN_EXIT ();

        return (OSIX_SUCCESS);
    }

    if (u1EligibleState == OSIX_TRUE)
    {
        /* If it is eligible do the following. */
        i4Status = PtpAlMstStartTmrs (pPtpPort);
        if (i4Status == OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                      "PtpAlMstTriggerAltMstMsg : Port : %u PtpAlMstStartTmrs "
                      "failed!!\r\n", pPtpPort->PortDs.u4PtpPortNumber));
            PTP_FN_EXIT ();

            return (OSIX_FAILURE);
        }
    }
    else
    {
        /* If it is not eligible do the following. */
        if (pPtpPort->bIsEligibleAltMst == OSIX_TRUE)
        {
            i4Status = PtpAlMstStopTmrs (pPtpPort);
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                          "PtpAlMstTriggerAltMstMsg : Port : %u "
                          "PtpAlMstStopTmrs "
                          "failed!!\r\n", pPtpPort->PortDs.u4PtpPortNumber));
                return (OSIX_FAILURE);
            }
        }
    }

    /* Update the new state */
    pPtpPort->bIsEligibleAltMst = u1EligibleState;

    PTP_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpAlMstHandleUpdateConfOpt                                */
/*                                                                           */
/* Description  : This function is called from the Configuration sub module. */
/*                Update the Port Alternate Master Options                   */
/*                If the u1AltMasterCount is 0 it mean disabling the         */
/*                feature it will do the following                           */
/*                1. Stop the announce message if already running            */
/*                2. Stop the Sync message if already running                */
/*                3. Update the given values in the tPtpPort table and return*/
/*                If the u1AltMasterCount is non 0, Update the given values  */
/*                in the tPtpPort table and call the                         */
/*                PtpAlMstTriggerAltMstMsg function to trigger the           */
/*                message based on clock eligibility.                        */
/*                                                                           */
/* Input        : u4ContextId - Context Identifier.                          */
/*              : u1DomainId  - Domain Identifier.                           */
/*              : u4PortId    - PTP Prot Identifier.                         */
/*              : pPtpAltMstInfo - Alternate Master information.             */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
PtpAlMstHandleUpdateConfOpt (UINT4 u4ContextId, UINT1 u1DomainId,
                             UINT4 u4PortId, tPtpAltMstInfo * pPtpAltMstInfo)
{
    tPtpPort           *pPtpPort = NULL;
    INT4                i4Status = OSIX_FAILURE;
    UINT4               u4MilliSec = 0;
    UINT1               u1PrevAltMultSynInterval = 0;
    BOOL1               bPrevElgibleState = OSIX_FALSE;
    BOOL1               bPrevAltMultSyncState = OSIX_FALSE;

    PTP_FN_ENTRY ();

    pPtpPort = PtpIfGetPortEntry (u4ContextId, u1DomainId, u4PortId);
    if (pPtpPort == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC,
                  "PtpAlMstHandleUpdateConfOpt : PtpDbGetPortEntry -"
                  " Failed!\r\n"));

        PTP_FN_EXIT ();

        return (OSIX_FAILURE);
    }
    bPrevElgibleState = pPtpPort->bIsEligibleAltMst;
    bPrevAltMultSyncState = pPtpPort->bAltMulcastSync;
    u1PrevAltMultSynInterval = pPtpPort->u1AltMulcastSynInterval;

    /* If the u1AltMasterCount is 0 it means disabling the feature 
     * it will do  the following 
     */
    if ((pPtpAltMstInfo->u1Flag == PTP_AM_UPDATE_MST_COUNT) &&
        (pPtpAltMstInfo->u1NumberOfAltMaster == 0))
    {
        if (pPtpPort->bIsEligibleAltMst == OSIX_TRUE)
        {
            i4Status = PtpAlMstStopTmrs (pPtpPort);
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC ((u4ContextId, u1DomainId,
                          ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                          "PtpAlMstHandleUpdateConfOpt : Port : %u "
                          "PtpAlMstStopTmrs failed. \r\n",
                          pPtpPort->PortDs.u4PtpPortNumber));

                PTP_FN_EXIT ();

                return (OSIX_FAILURE);
            }
        }

        PtpAlMstUpdatConfig (pPtpPort, pPtpAltMstInfo);

        pPtpPort->bIsEligibleAltMst = OSIX_FALSE;

        PTP_FN_EXIT ();

        return (OSIX_SUCCESS);
    }

    PtpAlMstUpdatConfig (pPtpPort, pPtpAltMstInfo);

    i4Status = PtpAlMstTriggerAltMstMsg (pPtpPort);
    if (i4Status == OSIX_FAILURE)
    {
        PTP_TRC ((u4ContextId, u1DomainId, ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                  "PtpAlMstHandleUpdateConfOpt : PtpAlMstTriggerAltMstMsg "
                  "failed. \r\n"));

        PTP_FN_EXIT ();

        return (OSIX_FAILURE);
    }

    if ((pPtpPort->bIsEligibleAltMst == OSIX_TRUE) &&
        (bPrevElgibleState == OSIX_TRUE) &&
        (((bPrevAltMultSyncState == PTP_DISABLED) &&
          (pPtpPort->bAltMulcastSync == PTP_ENABLED)) ||
         ((pPtpPort->bAltMulcastSync == PTP_ENABLED) &&
          (u1PrevAltMultSynInterval != pPtpPort->u1AltMulcastSynInterval))))
    {
        u4MilliSec =
            PtpUtilLogBase2ToMilliSec ((INT1)
                                       pPtpPort->u1AltMulcastSynInterval);

        if (PtpTmrStartTmr (&(pPtpPort->MulSyncTimer),
                            u4MilliSec, PTP_MULT_SYNC_TMR, OSIX_FALSE) ==
            OSIX_FAILURE)
        {
            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "PtpAlMstStartTmrs : PtpTmrStartTmr -"
                      " Multi Sync Timer failed for Port : %u!!!\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));

            PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                      CONTROL_PLANE_TRC,
                      "Unable to start Multi Sync timer for Port : %u\r\n",
                      pPtpPort->PortDs.u4PtpPortNumber));
        }
    }

    PTP_FN_EXIT ();

    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : PtpAlMstHandlePortStateChange                              */
/*                                                                           */
/* Description  : This function is called from port state decision sub module*/
/*                If the new state is Master Stop the Multi Announce message */
/*                and Mulit Sync message otherwise trigger to send           */
/*                alternate master messages.                                 */
/*                                                                           */
/* Input        : pPtpPort    - Pointer to the PtpPort structuere            */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Referred     : None.                                                      */
/*                                                                           */
/* Global                                                                    */
/* Variables                                                                 */
/* Modified     : None.                                                      */
/*                                                                           */
/* Use of                                                                    */
/* Recursion    : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpAlMstHandlePortStateChange (tPtpPort * pPtpPort)
{
    INT4                i4Status = 0;

    PTP_FN_ENTRY ();

    if (pPtpPort->PortDs.u1PortState == PTP_STATE_SLAVE)
    {
        if (pPtpPort->u1NumberOfAltMaster != 0)
        {
            /* Port state changes to None Master state */
            i4Status = PtpAlMstTriggerAltMstMsg (pPtpPort);
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "PtpAlMstHandlePortStateChange : Port :%u"
                          " PtpAlMstTriggerAltMstMsg failed. \r\n",
                          pPtpPort->PortDs.u4PtpPortNumber));
            }
        }
    }
    else
    {
        if (pPtpPort->bIsEligibleAltMst == OSIX_TRUE)
        {
            i4Status = PtpAlMstStopTmrs (pPtpPort);
            if (i4Status == OSIX_FAILURE)
            {
                PTP_TRC ((pPtpPort->u4ContextId, pPtpPort->u1DomainId,
                          CONTROL_PLANE_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC,
                          "PtpAlMstHandlePortStateChange : Port %u "
                          "PtpAlMstStopTmrs failed. \r\n",
                          pPtpPort->PortDs.u4PtpPortNumber));
            }
            pPtpPort->bIsEligibleAltMst = OSIX_FALSE;
        }
    }

    PTP_FN_EXIT ();

}
#endif /*  _PTPALMST_C_ */

/***************************************************************************
 *                         END OF FILE ptpalmst.c                          *
 ***************************************************************************/
