/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: ptpsecky.c,v 1.4 2014/01/30 12:20:35 siva Exp $
 *
 * Description: This file contains PTP context related utilities 
 *              implementation.       
 *********************************************************************/
#ifndef _PTPSECKY_C_
#define _PTPSECKY_C_

#include "ptpincs.h"
/*****************************************************************************/
/* Function                  : PtpSecKyCreate                                */
/*                                                                           */
/* Description               : This routine creates the security key         */
/*                                                                           */
/* Input                     : u4ContextId   -  Context identifier           */
/*                             u1DomainId    -  Domain identifier            */
/*                             u4KeyId       -  KeyId                        */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.SecKeyPoolId                   */
/*                             gPtpGlobalInfo.SecKeyLst                      */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : Pointer to node / NULL                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC tPtpSecKeyDs *
PtpSecKyCreate (UINT4 u4ContextId, UINT1 u1DomainId, UINT4 u4KeyId)
{
    tPtpSecKeyDs       *pPtpSecKeyDs = NULL;
    tPtpDomain         *pPtpDomain = NULL;

    PTP_FN_ENTRY ();

    /* Check whether the domain exists */
    pPtpDomain = PtpDmnGetDomainEntry (u4ContextId, u1DomainId);
    if (pPtpDomain == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId, ALL_FAILURE_TRC,
                  "Domain does not exists\r\n"));
        PTP_FN_EXIT ();
        return NULL;
    }

    pPtpSecKeyDs = (tPtpSecKeyDs *)
        MemAllocMemBlk (gPtpGlobalInfo.SecKeyPoolId);

    if (pPtpSecKeyDs == NULL)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | OS_RESOURCE_TRC | PTP_CRITICAL_TRC,
                  "Memory Allocation failed for security key\r\n"));
        PTP_FN_EXIT ();
        return NULL;
    }

    MEMSET (pPtpSecKeyDs, 0, sizeof (tPtpSecKeyDs));

    pPtpSecKeyDs->u4ContextId = u4ContextId;
    pPtpSecKeyDs->u1DomainId = u1DomainId;
    pPtpSecKeyDs->u4SecKeyId = u4KeyId;

    if (PtpDbAddNode (gPtpGlobalInfo.SecKeyLst, pPtpSecKeyDs,
                      (UINT1) PTP_SEC_KEY_DATA_SET) == OSIX_FAILURE)
    {
        PTP_TRC ((u4ContextId, u1DomainId,
                  ALL_FAILURE_TRC | PTP_CRITICAL_TRC | OS_RESOURCE_TRC,
                  "Unable to add security key node "
                  "into data structure\r\n"));

        MemReleaseMemBlock (gPtpGlobalInfo.SecKeyPoolId,
                            (UINT1 *) pPtpSecKeyDs);
        pPtpSecKeyDs = NULL;
        PTP_FN_EXIT ();
        return NULL;
    }

    PTP_TRC ((u4ContextId, u1DomainId, INIT_SHUT_TRC | MGMT_TRC,
              "Security key node is created successfully\r\n"));

    PTP_FN_EXIT ();
    return pPtpSecKeyDs;
}

/*****************************************************************************/
/* Function                  : PtpSecKyDelete                                */
/*                                                                           */
/* Description               : This routine deletes the security key node    */
/*                                                                           */
/* Input                     : pPtpSecKeyDs          - pointer to node       */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Global Variables Referred : gPtpGlobalInfo.SecKeyPoolId                   */
/*                             gPtpGlobalInfo.SecKeyLst                      */
/*                                                                           */
/* Global Variables Modified : None                                          */
/*                                                                           */
/* Use of Recursion          : None.                                         */
/*                                                                           */
/* Returns                   : None                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
PtpSecKyDelete (tPtpSecKeyDs * pPtpSecKeyDs)
{
#ifdef TRACE_WANTED
    UINT4               u4ContextId = pPtpSecKeyDs->u4ContextId;
    UINT1               u1DomainId = pPtpSecKeyDs->u1DomainId;
#endif

    PTP_FN_ENTRY ();

    PtpDbDeleteNode (gPtpGlobalInfo.SecKeyLst, pPtpSecKeyDs,
                     (UINT1) PTP_SEC_KEY_DATA_SET);

    MemReleaseMemBlock (gPtpGlobalInfo.SecKeyPoolId, (UINT1 *) pPtpSecKeyDs);
    pPtpSecKeyDs = NULL;

    PTP_TRC ((u4ContextId, u1DomainId, INIT_SHUT_TRC | MGMT_TRC,
              "Security key is deleted successfully\r\n"));
}

#endif /*_PTPSECKY_C_*/
